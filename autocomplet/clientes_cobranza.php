<?php
	include("../config.php");
	
	$Filtro = "%".strtoupper(utf8_decode($_GET['q']))."%";
	$op 	= $_GET["Op"];
	$codsuc = $_GET["codsuc"];
	
	$cont 	= 1;
	
	$sql = "select c.nroinscripcion,c.propietario,t.descripcion,c.nrodocumento,
			c.codtipodocumento,c.codsector,c.codcalle,c.nrocalle,c.codcliente
			from catastro.clientes as c
			inner join public.tipodocumento as t on(c.codtipodocumento=t.codtipodocumento)
			where CONVERT(CHAR,c.nroinscripcion) LIKE ? and c.estareg=1 and c.codsuc=?
			group by c.nroinscripcion,c.propietario,t.descripcion,c.nrodocumento,c.nroinscripcion,
			c.codtipodocumento,c.codsector,c.codcalle,c.nrocalle,c.codcliente order by c.propietario limit 5";
			
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($Filtro,$codsuc));
	$items = $consulta->fetchAll();

	foreach($items as $row)
	{
		$t  = $row["nroinscripcion"]."|".strtoupper(utf8_decode($row["propietario"]))."|".$row["descripcion"]."|".$row["nrodocumento"]."|".$row["codsector"];
		$t .= "|".$row["codtipodocumento"]."|".$row["codcalle"]."|".$row["codcliente"]."\n";

		echo $t;
	}
	
?>