<?php
	include("../config.php");
	
	$Filtro = "%".strtoupper(utf8_decode($_GET['q']))."%";
	$op 	= $_GET["Op"];
	$codsuc = $_GET["codsuc"];
	
	$cont 	= 1;
	
	$limit = 'limit 5';
		
	$sql = "SELECT c.nroinscripcion,c.propietario,t.descripcion,c.nrodocumento,
			c.codtipodocumento,c.codsector,c.codcalle,c.nrocalle,c.codcliente
			from catastro.clientes as c
			inner join public.tipodocumento as t on(c.codtipodocumento=t.codtipodocumento)
			where upper(c.propietario) LIKE ? and c.estareg=1 and c.codsuc=?
			group by c.nroinscripcion,c.propietario,t.descripcion,c.nrodocumento,c.nroinscripcion,
			c.codtipodocumento,c.codsector,c.codcalle,c.nrocalle,c.codcliente 
			order by c.propietario ".$limit;
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($Filtro,$codsuc));
	$items = $consulta->fetchAll();

	foreach($items as $row)
	{
		$t  = $row["codcliente"]."|".strtoupper(utf8_encode($row["propietario"]))."|".$row["descripcion"]."|".$row["nrodocumento"]."|".$row["codsector"];
		$t .= "|".$row["codtipodocumento"]."|".$row["codcalle"]."|".$row["nroinscripcion"]."\n";
		
		$arr[$cont]=$row["codcliente"];
		$cont=$cont+1;
		
		echo $t;
	}
	
	if($op==0)
	{
		$sql = "SELECT s.codcliente,s.propietario,t.descripcion,s.nrodocumento,s.codemp,s.codtipodocumento,c.nroinscripcion
				from solicitudes.solicitudes as s
				inner join public.tipodocumento as t on(s.codtipodocumento=t.codtipodocumento)
				where upper(s.propietario) LIKE ? and s.codemp=1 and s.codsuc=?
				group by s.codcliente,s.propietario,t.descripcion,s.nrodocumento,s.codemp,s.codtipodocumento,c.nroinscripcion
                order by s.propietario  ".$limit;
		$consulta1 = $conexion->prepare($sql);
		$consulta1->execute(array($Filtro,$codsuc));
		$items1 = $consulta1->fetchAll();
		foreach($items1 as $row1)
		{
			$e=0;
			for($i=1;$i<=count($arr);$i++)
			{
				if($row1["codcliente"]==$arr[$i])
				{
					$e=1;
				}
			}
			if($e==0)
			{
				$t   = $row1["codcliente"]."|".strtoupper(utf8_encode($row1["propietario"]))."|".$row1["descripcion"]."|".$row1["nrodocumento"]."|".$row1["codemp"]."|";
				$t	.= $row1["codtipodocumento"]."|".$row1["nroinscripcion"]."\n";
				
				echo $t."\n";
			}
		}
	}
	
?>