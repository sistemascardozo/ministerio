$(document).ready(function() {
    $(".focusselect").click(function() {
        $(this).select()
    })
    // $(".button").button();
    //PARA TIPO DE DATOS//
    $(".entero").numeric();
    $('.numeric').numeric({allow: "."});
    $("button, input:button, input:submit, #BtnActivo, #BtnInacActivo ").button();
    $(".buttonset").buttonset();
    $(".fecha").datepicker({
        //showAnim: 'scale' ,
        showOn: 'button',
        direction: 'up',
        yearRange: '2010:2100',
        buttonImage: urldir + 'images/iconos/calendar.png',
        buttonImageOnly: true,
        //showOn: 'both', //mostrar en el input
		firstDay: 0,
        showButtonPanel: true
    });

});

function ver(z) {
}

var Id2 = ''

function SeleccionaId(obj)
{
    Id = 'Id=' + obj.id;
    IdAnt = Id2;
    Id2 = obj.id;

    if (IdAnt != '')
    {
        Limpiar(IdAnt);
    }

    $("#"+obj.id).addClass("ui-state-active");
}

function Limpiar(Fila)
{
    $("#" + Fila).removeClass("ui-state-active");
}

function VeriEnter(e)
{
    if (!e)
        e = window.event;
    if (/*e && e.keyCode == 9 ||*/ e && e.keyCode == 13)
        return true
    else
        return false
}
function MostrarSucursal(idsucursal)
{
    $.ajax({
        url: "../include/sucursales.php",
        type: 'POST',
        async: true,
        data: 'IdSucursal=' + idsucursal,
        success: function(datos) {
            $("#sucursal").html(datos)
        }
    })
}
function NombreModulos(Cambio, idsistema)
{
    if (Cambio != '' && Cambio != 1)
    {
        $.ajax({
            url: '../include/modulos.php',
            type: 'POST',
            async: true,
            data: 'idsistema=' + idsistema,
            success: function(datos) {
                $("#NombreModulo").html(datos)
            }
        })
    } else {
        $("#NombreModulo").html('')
        $("#sucursal").html('')
    }
}

/**/
	function cargar_sectores(codsuc, codzona, seleccion, opcion)
	{
		$.ajax({
			url: urldir + 'ajax/sectores.php',
			type: 'POST',
			async: true,
			data: 'codsuc=' + codsuc + '&codzona=' + codzona + "&seleccion=" + seleccion + "&opcion=" + opcion,
			success: function(datos) {
				//alert(codsuc);
            	$("#div_sector").html(datos);
            	//$("#zonas").change(function(){vCatastro()});
			}
		})
	}

function cargar_manzana(codsuc, codsector, seleccion, opcion)
{
    $.ajax({
        url: urldir + 'ajax/manzanas.php',
        type: 'POST',
        async: true,
        data: 'codsuc=' + codsuc + '&codsector=' + codsector + "&seleccion=" + seleccion + "&opcion=" + opcion,
        success: function(datos) {
            $("#div_manzanas").html(datos)
            $("#manzanas").change(function() {
                vCatastro()
            });
        }
    })
}

function vCatastro()
{

}

	function cargar_calles(codsector, codsuc, codzona, seleccion)
	{
		if (codsector != "")
		{
			if (seleccion == undefined)
			{
            	seleccion = 1;
			}

			$.ajax({
				url: urldir + 'ajax/calles.php',
				type: 'POST',
				async: true,
				data: 'codsector=' + codsector + '&codsuc=' + codsuc + '&codzona=' + codzona + "&seleccion=" + seleccion,
				success: function(datos) {
					//alert(datos);
					$("#div_calles").html(datos);
            	}
        	})
    	}
	}
/**/
function cargar_provincia(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/provincia.php',
        type: 'POST',
        async: true,
        data: 'departamento='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            $("#div_provincia").html(datos)
        }
    })
}
function cargar_distrito(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/distrito.php',
        type: 'POST',
        async: true,
        data: 'distrito='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            $("#div_distrito").html(datos)
        }
    })
}

/*--------- REPRESENTANTE --------*/
function cargar_provinciarep(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/provinciarep.php',
        type: 'POST',
        async: true,
        data: 'departamentorep='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            $("#div_provinciarep").html(datos)
        }
    })
}
function cargar_distritorep(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/distritorep.php',
        type: 'POST',
        async: true,
        data: 'distritorep='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            $("#div_distritorep").html(datos)
        }
    })
}
/**/

/*--------- PREDIOS --------*/
function cargar_provinciapred(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/provinciapred.php',
        type: 'POST',
        async: true,
        data: 'departamentopred='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            $("#div_provinciapred").html(datos)
        }
    })
}
function cargar_distritopred(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/distritopred.php',
        type: 'POST',
        async: true,
        data: 'distritopred='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            $("#div_distritopred").html(datos)
        }
    })
}
/**/

function cargar_diametros(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/diametros.php',
        type: 'POST',
        async: true,
        data: 'tiposervicio='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            $("#div_diametros").html(datos)
        }
    })
}

function permite(elEvento, permitidos) {
    var numeros = "0123456789.,";
    var caracteres = " abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ-/";
    var numeros_caracteres = numeros+caracteres;
    var teclas_especiales = [8, 37, 39, 46, 13];

    switch (permitidos) {
        case 'num':
            permitidos = numeros;
            break;
        case 'car':
            permitidos = caracteres;
            break;
        case 'num_car':
            permitidos = numeros_caracteres;
            break;
    }
    var evento = elEvento || window.event;
    var codigoCaracter = evento.charCode || evento.keyCode;
    var caracter = String.fromCharCode(codigoCaracter);
    var tecla_especial = false;
    for (var i in teclas_especiales) {
        if (codigoCaracter == teclas_especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    return permitidos.indexOf(caracter) != -1 || tecla_especial;
}

function CambiarFoco(e, Obj)
{
    if (!e)
        e = window.event;
    if (e && e.keyCode == 13)
    {
        Siguiente(Obj)
        return false
    }
}

function Siguiente(Obj)
{
    try {
        document.getElementById(Obj).focus();
    } catch (err) {

    }
}

function CambiarEstado(Check, Obj)
{
    if (Check.checked == true)
    {
        $("#"+Obj).val(1)
    } else {
        $("#"+Obj).val(0)
    }
}

function CalculaIgv(tot, importeigv)
{
    //tot = str_replace(tot,",","")
	if (tot == 0)
	{
		return "0.00";
	}
	
    return Round(tot * (importeigv / 100), 2);
}

function str_replace(cadena, cambia_esto, por_esto)
{
    return cadena.split(cambia_esto).join(por_esto);
}

function AbrirPopupBusqueda(url, width, height)
{
    var n = Math.random() * 100
    var ventana = window.open(url, n, 'width='+width+', height='+height+', resizable=no, scrollbars=no, status=yes,location=yes');
    ventana.focus();
}

function AbrirPopupImpresion(url, width, height)
{
    var n = Math.random() * 100
    var ventana = window.open(url, n, 'width='+width+', height='+height+', resizable=yes, scrollbars=yes, status=yes,location=yes');
    ventana.focus();
}

function Round(num, RLENGTH) {
    var newnumber = Math.round(num * Math.pow(10, RLENGTH)) / Math.pow(10, RLENGTH);
    return parseFloat(newnumber);
}

function CalcularRedondeo(Imp)
{
    red1 = Round(Imp, 1)
    red2 = Round(Imp, 2)
    redondeo = parseFloat(red1) - parseFloat(red2)

    return redondeo;
}

function FechaVecimiento(mes, anio)
{
    dia = UltimoDia(mes)
    mes = mes.toString();
    if (mes.length == 1)
    {
        mes = "0"+mes
    }
    return dia+"/"+mes+"/"+anio;
}

function UltimoDia(Mes)
{
    var NroDias;

    switch (Mes)
    {
        case 1:
            NroDias = 31;
            break;
        case 2:
            NroDias = 28;
            break;
        case 3:
            NroDias = 31;
            break;
        case 4:
            NroDias = 30;
            break;
        case 5:
            NroDias = 31;
            break;
        case 6:
            NroDias = 30;
            break;
        case 7:
            NroDias = 31;
            break;
        case 8:
            NroDias = 31;
            break;
        case 9:
            NroDias = 30;
            break;
        case 10:
            NroDias = 31;
            break;
        case 11:
            NroDias = 30;
            break;
        case 12:
            NroDias = 31;
            break;
    }

    return NroDias;
}

function meses(mes)
{
    var meses = "";

    switch (mes)
    {
        case "1":
            meses = "ENERO";
            break;
        case "2":
            meses = "FEBRERO";
            break;
        case "3":
            meses = "MARZO";
            break;
        case "4":
            meses = "ABRIL";
            break;
        case "5":
            meses = "MAYO";
            break;
        case "6":
            meses = "JUNIO";
            break;
        case "7":
            meses = "JULIO";
            break;
        case "8":
            meses = "AGOSTO";
            break;
        case "9":
            meses = "SETIEMBRE";
            break;
        case "10":
            meses = "OCTUBRE";
            break;
        case "11":
            meses = "NOVIEMBRE";
            break;
        case "12":
            meses = "DICIEMBRE";
            break;
    }

    return meses;
}
function Sobre(obj)
{
    obj.style.width = 90;
    $("#"+obj.id).css('background-color', '#0047E0');
    $("#"+obj.id).css('color', '#FFF');
    //$("#"+obj.id).addClass( "ui-state-active" );
    //background-color:#FFF; color:#000; :center;width:85px

}
function Fuera(obj)
{
    obj.style.width = 85;
    $("#"+obj.id).css('background-color', '#FFF');
    $("#"+obj.id).css('color', '#000');
}

function Msj(Id, Mensaje, color, ubicacion, error, duracion) //Msj(Id,Mensaje,duracion,ubicacion,tipo,clase)
{
    //alert(Id);
    duracion = (duracion) ? duracion : 1000;
    color = (color) ? color : '';
    ubicacion = (ubicacion) ? ubicacion : 'above';
    error = (error) ? error : false;
	
	clase = 'ui-state-highlight';
	
    switch (ubicacion)
    {
        case 'left':
            $(Id).feedback(Mensaje, {duration: duracion, left: true, type: color});
            $(Id).addClass("ui-state-error");
            break;
        case 'below':
            $(Id).feedback(Mensaje, {duration: duracion, below: true, type: color});
            $(Id).addClass("ui-state-error");
            break;
        case 'right':
            $(Id).feedback(Mensaje, {duration: duracion, right: true, type: color});
            $(Id).addClass("ui-state-error");
            break;
        case 'above':
            $(Id).feedback(Mensaje, {duration: duracion, above: true, type: color});
            $(Id).addClass("ui-state-error"); 
            break;
    }
    if (error)
        //$(Id).addClass("ui-state-error");        
        {$(Id).focus()}
}
function Trim(Cadena)
{
    if (Cadena == undefined)
        return ""
    else
        return Cadena.replace(/^\s+|\s+$/g, "");
}
function escorreo(email)
{
    if (!/^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/.test(email))
    {
        return(false);
    }
    else
    {
        return (true);
    }
}
function incDate(sFec0)
{
    var nDia = parseInt(sFec0.substr(0, 2), 10);
    var nMes = parseInt(sFec0.substr(3, 2), 10);
    var nAno = parseInt(sFec0.substr(6, 4), 10);
    nDia += 1;
    if (nDia > finMes(nMes, nAno))
    {
        nDia = 1;
        nMes += 1;
        if (nMes == 13)
        {
            nMes = 1;
            nAno += 1;
        }
    }
    return makeDateFormat(nDia, nMes, nAno);
}

function decDate(sFec0)
{
    var nDia = Number(sFec0.substr(0, 2));
    var nMes = Number(sFec0.substr(3, 2));
    var nAno = Number(sFec0.substr(6, 4));
    nDia -= 1;
    if (nDia == 0)
    {
        nMes -= 1;
        if (nMes == 0)
        {
            nMes = 12;
            nAno -= 1;
        }
        nDia = finMes(nMes, nAno);
    }
    return makeDateFormat(nDia, nMes, nAno);
}

function addToDate(sFec0, sInc)
{
    var nInc = Math.abs(parseInt(sInc));
    var sRes = sFec0;
    if (parseInt(sInc) >= 0)
        for (var i = 0; i < nInc; i++)
            sRes = incDate(sRes);
    else
        for (var i = 0; i < nInc; i++)
            sRes = decDate(sRes);
    return sRes;
}
var aFinMes = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
function finMes(nMes, nAno)
{
    return aFinMes[nMes - 1]+(((nMes == 2) && (nAno % 4) == 0) ? 1 : 0);
}
function makeDateFormat(nDay, nMonth, nYear)
{
    var sRes;
    sRes = padNmb(nDay, 2, "0")+"/"+padNmb(nMonth, 2, "0")+"/"+padNmb(nYear, 4, "0");
    return sRes;
}
function padNmb(nStr, nLen, sChr)
{
    var sRes = String(nStr);
    for (var i = 0; i < nLen - String(nStr).length; i++)
        sRes = sChr+sRes;
    return sRes;
}
function number_format(numero, decimales)
{
    //function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    decimales = 2
    separador_decimal = "."
    separador_miles = ","
    numero = parseFloat(numero);
    if (isNaN(numero)) {
        return "";
    }

    if (decimales !== undefined) {
        // Redondeamos
        numero = numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero = numero.toString().replace(".", separador_decimal !== undefined ? separador_decimal : ",");

    if (separador_miles) {
        // Añadimos los separadores de miles
        var miles = new RegExp("(-?[0-9]+)([0-9]{3})");
        while (miles.test(numero)) {
            numero = numero.replace(miles, "$1"+separador_miles+"$2");
        }
    }

    return numero;

}

function ValidarCodAntiguo(e)
{
    if (VeriEnter(e))
    {
        if ($("#codantiguo").val() != "")
        {
            cargar_datos_usuario_anti($("#codantiguo").val())

        }
    }
}
function cargar_datos_usuario_anti(codantiguo)
{
    $.ajax({
        url: urldir+'ajax/clientes_antiguo.php',
        type: 'POST',
        async: true,
        dataType: 'json',
        data: 'codsuc='+codsuc+'&codantiguo='+codantiguo+'&nrorecibo='+$('#nrorecibo').val(),
        success: function(datos) {
            //var r=datos.split("|")
            if (datos.nroinscripcion == null)
            {
                Msj($("#codantiguo"), "No existe Usuario")
                return false;
            }
            MostrarDatosUsuario(datos)

        }
    })
}

function TamVentana() {
    var Tamanyo = [0, 0];
    if (typeof window.innerWidth != 'undefined')
    {
        Tamanyo = [
            window.innerWidth,
            window.innerHeight
        ];
    }
    else if (typeof document.documentElement != 'undefined'
            && typeof document.documentElement.clientWidth !=
            'undefined' && document.documentElement.clientWidth != 0)
    {
        Tamanyo = [
            document.documentElement.clientWidth,
            document.documentElement.clientHeight
        ];
    }
    else {
        Tamanyo = [
            /*document.getElementsByTagName('body')[0].clientWidth,
             document.getElementsByTagName('body')[0].clientHeight*/
            $(window).width(), $(window).height()
        ];
    }
    return Tamanyo;
}

/*-------- GIS ---------*/
function c_distrito(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/distritoGis.php',
        type: 'POST',
        async: true,
        data: 'distritoGis='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            //alert(datos);
            $("#div_distritoGis").html(datos)
        }
    })
}

function c_localidad(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/localidadGis.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            //alert(datos);
            $("#div_localidadGis").html(datos)
        }
    })
}

function c_zonas(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/zonasGis.php',
        type: 'POST',
        async: true,
        data: 'codlocalidad='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            //alert(datos);
            $("#div_zonasGis").html(datos)
        }
    })
}

function c_sectores(obj, seleccion)
{
    $.ajax({
        url: urldir+'ajax/sectorGis.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&codzona='+obj+'&seleccion='+seleccion,
        success: function(datos) {
            //alert(datos);
            $("#div_sector").html(datos)
        }
    })
}

function c_manzanas(codsuc, codsector, seleccion)
{
    $.ajax({
        url: urldir+'ajax/manzanasGis.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&codsector='+codsector+'&seleccion='+seleccion,
        success: function(datos) {
            //alert(datos);
            $("#div_manzanas").html(datos)
            /*$("#manzanas").change(function() {
                vCatastro()
            });*/
        }
    })
}

function c_calles(codsector, codsuc, codzona, seleccion)
{
    if (codsector != "")
    {
        if (seleccion == undefined)
            seleccion = 1
        $.ajax({
            url: urldir+'ajax/callesGis.php',
            type: 'POST',
            async: true,
            data: 'codsector='+codsector+'&codsuc='+codsuc+'&codzona='+codzona+"&seleccion="+seleccion,
            success: function(datos) {
                $("#div_calles").html(datos)
            }
        })
    }
}

function cargar_urbanizacion(codsuc, codzona, seleccion, opcion)
{
    $.ajax({
        url: urldir + 'ajax/urbanizacion.php',
        type: 'POST',
        async: true,
        data: 'codsuc=' + codsuc + '&codzona=' + codzona + "&seleccion=" + seleccion + "&opcion=" + opcion,
        success: function(datos) {
            $("#div_urbanizacion").html(datos)
            
        }
    })
}
