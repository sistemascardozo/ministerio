// JavaScript Document
function TamVentana()
{
    Tamanyo = [0, 0];
    if (typeof window.innerWidth != 'undefined')
    {
        Tamanyo = [window.innerWidth, window.innerHeight];
    }
    else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)
    {
        Tamanyo = [document.documentElement.clientWidth, document.documentElement.clientHeight];
    }
    else
    {
        Tamanyo = [document.getElementsByTagName('body')[0].clientWidth, document.getElementsByTagName('body')[0].clientHeight];
    }
    return Tamanyo;
}

function CargarMenu(Op, Urldir)
{
    $('#siteMenu').css('display', 'none');

    if (Op == 1)
    {
        $.ajax({
            url: Urldir + 'include/menu.php',
            type: 'POST',
            async: true,
            data: '',
            success: function (data) {
                $('#siteMenu').css('display', 'block');
                $('#siteMenu').html(data);
            }
        });
    }
}

function FechaSistema()
{
    var fecha = new Date();
    var diames = fecha.getDate();
    var diasemana = fecha.getDay();
    var mes = fecha.getMonth() + 1;
    var ano = fecha.getFullYear();

    var textosemana = new Array(7);
    textosemana[0] = "Domingo";
    textosemana[1] = "Lunes";
    textosemana[2] = "Martes";
    textosemana[3] = "Miercoles";
    textosemana[4] = "Jueves";
    textosemana[5] = "Viernes";
    textosemana[6] = "Sabado";

    var textomes = new Array(12);
    textomes[1] = "Enero";
    textomes[2] = "Febrero";
    textomes[3] = "Marzo";
    textomes[4] = "Abril";
    textomes[5] = "Mayo";
    textomes[6] = "Junio";
    textomes[7] = "Julio";
    textomes[8] = "Agosto";
    textomes[9] = "Septiembre";
    textomes[10] = "Octubre";
    textomes[11] = "Noviembre";
    textomes[12] = "Diciembre";

    $('#siteCabeceraFecha').html($('#siteCabeceraFecha').html() + " - " + textosemana[diasemana] + ", " + diames + " de " + textomes[mes] + " de " + ano);
}