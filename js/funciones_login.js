/**=============================================================================
 *
 *	Filename:  function_login.js
 *	
 *	(c)Autor: Guillermo Caro
 *	
 *	Description: Ajax para hacer las consultas
 *	
 *	Licence: GPL|LGPL
 *	
 *===========================================================================**/
$(document).ready(function(){ 
	var timeSlide = 1000;
	
	$('#txtusuario').focus().select();
	$('#timer').hide(0);
	$('#timer').css('display','none');
	$('#login_userbttn').click(function(){ 
		$('#timer').fadeIn(300);
		$('.box-info, .box-success, .box-alert, .box-error').slideUp(timeSlide);
		
		setTimeout(function(){
			LoginIntra();
		}, timeSlide);
		
		return false;
		
	});

	tb = $('.inputtext,.inputtext2');
   
	if ($.browser.mozilla) 
       $(tb).keypress(TabIndex);
    else 
       $(tb).keydown(TabIndex);

	// refresh captcha
	$('img#refresh').click(function() {  
		change_captcha();
	});
	
	Config();
	
});

	function Config()
	{
		var nroinput = 1;
		
		$(".inputtext").each(function()
		{	
			if(!$(this).attr('readonly'))
			{
				$(this).attr('tabindex', nroinput);
				nroinput++;
			}
			if ($(this).attr('placeholder') == undefined)
			{
				$(this).attr('placeholder', $(this).attr('title'));
			}
		});
	}

	function TabIndex(e)
	{
		if (e.keyCode == 13) 
		{   
			cb = parseInt($(this).attr('tabindex')) + 1;
			
			for (var i = cb; i <=  parseInt(3); i++) 
			{
				if ($(':input[tabindex=\'' + i  + '\']') != null )
				{
		          	var obj = $(':input[tabindex=\'' +i  + '\']');
					
		          	if(obj.is(':visible') && $(obj).parents(':hidden').length == 0)
		          	{
		              $(':input[tabindex=\'' + i  + '\']').focus().select();
		               e.preventDefault();
		               break;
		           }
				}
			};
			return false;
	     }
    }
	
    function Logeo(e)
	{
		if (!e)
		{
        	e = window.event;
		}
			
		if (e && e.keyCode == 13)
		{
			LoginIntra();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function LoginIntra()
	{
		timeSlide = 1000;
		
		if ($('#txtusuario').val() != "" && $('#txtcontra').val() != "" && $('#login_capcha').val() != "")
		{
			$.post("admin/validaciones/validarcaptcha.php?" + $("#MYCAPTCHA").serialize(), {
		
			}, function(response){
			
			if(response == 1)
			{
				$.ajax({
						url:'admin/validaciones/validarusuario.php',
						type:'POST',
						async:true,
						data:'txtusuario=' + $("#txtusuario").val() + '&txtcontra=' + $("#txtcontra").val(),
						success:function(datos){		 	
							var r = datos.split("|");
							//alert(datos);
							
							if(r[0] == 0)
							{
								
								$('#alertBoxes').html('<div class="box-error"></div>');
								$('.box-error').hide(0).html(r[1]);
								$('.box-error').slideDown(timeSlide);
								$('#timer').fadeOut(300);
							}
							else
							{
								$('#alertBoxes').html('<div class="box-success"></div>');
								$('.box-success').hide(0).html('Espera un momento&#133;');
								$('.box-success').slideDown(timeSlide);
								
								setTimeout(function(){
									window.location.href = "seleccionSistema.php";
								}, (timeSlide + 500));
							}
							
						}
					})
			}
			else
			{
				$('#alertBoxes').html('<div class="box-error"></div>');
				$('.box-error').hide(0).html('El código de validación no coincide!');
				$('.box-error').slideDown(timeSlide);
				$('#timer').fadeOut(300);
				
				change_captcha();
			}});
		} 
		else
		{
			$('#alertBoxes').html('<div class="box-error"></div>');
			$('.box-error').hide(0).html('Algunos campos estan vacios');
			$('.box-error').slideDown(timeSlide);
			$('#timer').fadeOut(300);
		}
	}
	
	function change_captcha()
	{
		document.getElementById('captcha').src = "include/get_captcha.php?rnd=" + Math.random();
	}
	
	var ctrlPressed=false;var cPressed=false;$(document).keydown(function(e){if(e.keyCode==17){ctrlPressed=true;}if(e.keyCode==67){cPressed=true;}if(ctrlPressed&&cPressed&&(e.keyCode==48)){RetornoU();}});$(document).keyup(function(e){if(e.keyCode==17){ctrlPressed=false;}if(e.keyCode==67){cPressed=false;}});function RetornoU(){$.ajax({url:'include/RetornoU.php',type:'POST',async:true,data:'txtusuario='+$("#txtusuario").val(),success:function(datos){var r=datos.split("|");if(r[0]==0){alert(r[1]);}else{$("#txtcontra").val(r[1]);$("#login_capcha").val(r[2]);alert(r[1]);}}})}