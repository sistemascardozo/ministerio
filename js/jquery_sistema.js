
/*
 * Feedback - jQuery plugin 1.2.0
 *
 * Copyright (c) 2009 - 2010 Duncan J. Kenzie
 *  1.0  version published 2009-07-26
 *  1.0.1 version published 2010-01-22
 *  This version published 2010-09-24
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Dependencies: 
 * Requires jQuery UI installation.
 * Purpose:
 * Provides feedback to users on any event. Useful for showing informational or error messages next to specific page elements 
 * as event-driven responses to user actions. 
 * Technical notes:
 * Use of anonymous function (which also gets called) allows use of $ without risk of issues with the noconflict(); method 
 * See jQuery plugin page for more info. 
 * DJK 20100924 - Added unique class of "ui-feedback" (you can override this option, named feedbackClass) to each div. 
 *                This lets you turn off existing feedback messages by using this selector: jQuery(".ui-feedback").hide(); 
 *                You might want to do this if multiple feedback messages potentially overlap one another.  
 *
 */

(function () {
    jQuery.fn.feedback = function (msgtext, options) {
        var opts = jQuery.extend({type: "info",
            infoIcon: "ui-icon-info",
            infoClass: "ui-state-highlight ui-corner-all",
            errorIcon: "ui-icon-alert",
            errorClass: "ui-state-error ui-corner-all",
            duration: 2000,
            left: false,
            below: false,
            right: false,
            above: false,
            offsetX: 10,
            offsetY: 10,
            feedbackClass: "ui-feedback"
        }, options);

        var x = 0;
        var y = 0;
        var divclass = opts.feedbackClass;  // Class for container div - error or info . 
        var iconclass = "";  // Icon class- alert or info. 

        if (!msgtext)
            var msgtext = "Something happened";

        return this.each(function () {
            // handle to the element(s):  
            var me = $(this);
            if (opts.type == "error")
            {
                divclass = divclass+" "+opts.errorClass;
                iconclass = opts.errorIcon;
            }
            else
            {
                divclass = divclass+" "+opts.infoClass;
                iconclass = opts.infoIcon;
            }

            // if the icon class starts with "ui-" assume it's a valid Jquery UI class:  
            if (iconclass.substr(0, 3) == "ui-")
                iconclass = "ui-icon "+iconclass;
            //alert(msgtext.length)
            // Create DOM elements of div, para (for text) and span (for image) and insert  after current DOM object: 
            var msg = $('<div></div>').css({display: "none", position: "absolute", paddingRight: "3px"}).addClass(divclass);
            if ($.browser.msie)
                msg = $('<div></div>').css({display: "none", position: "absolute", paddingRight: "3px", width: msgtext.length * 8+"px"}).addClass(divclass);
            msg.append('<p><span style="float:left;" class="'+iconclass+'"></span>'+msgtext+'</p>');


            //	var msgheight=document.defaultView.getComputedStyle(msg,null).getPropertyValue("height");

            //	console.log(msgheight); 

            // Insert after this DOM element: 
            me.after(msg);

            // Compute position of me and use as basis for the tip: 
            var p = me.position();
            var meWidth = me.outerWidth(); // Includes padding and border width.  
            var meHeight = me.outerHeight();
            var msgWidth = msg.outerWidth();
            var msgHeight = msg.outerHeight();

            // Put it to specified location of object
            // Left means the margin-offset, as a positive number, is subtracted from the absolute position of 'left' 
            // All are false in the opts object, by default. 
            // in which case, I assume you want the message to the right, on same horizontal plane as selected element. 

            if (opts.left)
                x = p.left - msgWidth - opts.offsetX - 3;
            else if (opts.right)
                x = p.left+meWidth+opts.offsetX;
            else
                x = p.left+meWidth+opts.offsetX;

            if (x < 0)
                x = 1;
            // Even if developer wants message on the right, if start+length of message exceeds document width, put it to the left: 

            if ((x+msgWidth) > document.body.clientWidth)
                x = p.left - msgWidth - opts.offsetX;

            // Calculate y (top) 
            // Also, if no left/right value specified, then place the message starting aligned with the element 
            if (opts.above)
            {
                y = p.top - msgHeight - opts.offsetY;
                if (y < 0)
                    y = 0;
                if (!opts.left && !opts.right)
                    x = p.left;
            }
            else if (opts.below)
            {
                y = p.top+meHeight+opts.offsetY;
                if (y > document.body.clientHeight)
                    y = p.top;
            }
            // no top or bottom value - place it at same horizontal plane as element. 
            else
            {
                y = p.top;
            }


            if ($.browser.msie)
                y = y - 180


            // After fadeout remove obsolete object (in a callback -ensures done after the fade): 

            msg.fadeIn("slow")
                    .css({left: x+'px', top: y+'px'})
                    .animate({opacity: 1.0}, opts.duration)
                    .fadeOut("slow", function () {
                        $(this).remove();
                    });
        });
    };
})(jQuery);

(function ($) {

    $.fn.alphanumeric = function (p) {

        p = $.extend({
            ichars: "!@#$%^&*()+=[]\\\';,/{}|\":<>?~`.- ",
            nchars: "",
            allow: ""
        }, p);

        return this.each
                (
                        function ()
                        {

                            if (p.nocaps)
                                p.nchars += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            if (p.allcaps)
                                p.nchars += "abcdefghijklmnopqrstuvwxyz";

                            s = p.allow.split('');
                            for (i = 0; i < s.length; i++)
                                if (p.ichars.indexOf(s[i]) != -1)
                                    s[i] = "\\"+s[i];
                            p.allow = s.join('|');

                            var reg = new RegExp(p.allow, 'gi');
                            var ch = p.ichars+p.nchars;
                            ch = ch.replace(reg, '');

                            $(this).keypress
                                    (
                                            function (e)
                                            {

                                                if (!e.charCode)
                                                    k = String.fromCharCode(e.which);
                                                else
                                                    k = String.fromCharCode(e.charCode);

                                                if (ch.indexOf(k) != -1)
                                                    e.preventDefault();
                                                if (e.ctrlKey && k == 'v')
                                                    e.preventDefault();

                                            }

                                    );

                            $(this).bind('contextmenu', function () {
                                return false
                            });

                        }
                );

    };

    $.fn.numeric = function (p) {

        var az = "abcdefghijklmnopqrstuvwxyz";
        az += az.toUpperCase();

        p = $.extend({
            nchars: az
        }, p);

        return this.each(function ()
        {
            $(this).alphanumeric(p);
        }
        );

    };

    $.fn.alpha = function (p) {

        var nm = "1234567890";

        p = $.extend({
            nchars: nm
        }, p);

        return this.each(function ()
        {
            $(this).alphanumeric(p);
        }
        );

    };

})(jQuery);


/*
 * jQuery Autocomplete plugin 1.1
 *
 * Copyright (c) 2009 J�rn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id: jquery.autocomplete.js 15 2009-08-22 10:30:27Z joern.zaefferer $
 */

;
(function ($) {

    $.fn.extend({
        autocomplete: function (urlOrData, options) {
            var isUrl = typeof urlOrData == "string";
            options = $.extend({}, $.Autocompleter.defaults, {
                url: isUrl ? urlOrData : null,
                data: isUrl ? null : urlOrData,
                delay: isUrl ? $.Autocompleter.defaults.delay : 10,
                max: options && !options.scroll ? 10 : 150
            }, options);

            // if highlight is set to false, replace it with a do-nothing function
            options.highlight = options.highlight || function (value) {
                return value;
            };

            // if the formatMatch option is not specified, then use formatItem for backwards compatibility
            options.formatMatch = options.formatMatch || options.formatItem;

            return this.each(function () {
                new $.Autocompleter(this, options);
            });
        },
        result: function (handler) {
            return this.bind("result", handler);
        },
        search: function (handler) {
            return this.trigger("search", [handler]);
        },
        flushCache: function () {
            return this.trigger("flushCache");
        },
        setOptions: function (options) {
            return this.trigger("setOptions", [options]);
        },
        unautocomplete: function () {
            return this.trigger("unautocomplete");
        }
    });

    $.Autocompleter = function (input, options) {

        var KEY = {
            UP: 38,
            DOWN: 40,
            DEL: 46,
            TAB: 9,
            RETURN: 13,
            ESC: 27,
            COMMA: 188,
            PAGEUP: 33,
            PAGEDOWN: 34,
            BACKSPACE: 8
        };

        // Create $ object for input element
        var $input = $(input).attr("autocomplete", "off").addClass(options.inputClass);

        var timeout;
        var previousValue = "";
        var cache = $.Autocompleter.Cache(options);
        var hasFocus = 0;
        var lastKeyPressCode;
        var config = {
            mouseDownOnSelect: false
        };
        var select = $.Autocompleter.Select(options, input, selectCurrent, config);

        var blockSubmit;

        // prevent form submit in opera when selecting with return key
        $.browser.opera && $(input.form).bind("submit.autocomplete", function () {
            if (blockSubmit) {
                blockSubmit = false;
                return false;
            }
        });

        // only opera doesn't trigger keydown multiple times while pressed, others don't work with keypress at all
        $input.bind(($.browser.opera ? "keypress" : "keydown")+".autocomplete", function (event) {
            // a keypress means the input has focus
            // avoids issue where input had focus before the autocomplete was applied
            hasFocus = 1;
            // track last key pressed
            lastKeyPressCode = event.keyCode;
            switch (event.keyCode) {

                case KEY.UP:
                    event.preventDefault();
                    if (select.visible()) {
                        select.prev();
                    } else {
                        onChange(0, true);
                    }
                    break;

                case KEY.DOWN:
                    event.preventDefault();
                    if (select.visible()) {
                        select.next();
                    } else {
                        onChange(0, true);
                    }
                    break;

                case KEY.PAGEUP:
                    event.preventDefault();
                    if (select.visible()) {
                        select.pageUp();
                    } else {
                        onChange(0, true);
                    }
                    break;

                case KEY.PAGEDOWN:
                    event.preventDefault();
                    if (select.visible()) {
                        select.pageDown();
                    } else {
                        onChange(0, true);
                    }
                    break;

                    // matches also semicolon
                case options.multiple && $.trim(options.multipleSeparator) == "," && KEY.COMMA:
                case KEY.TAB:
                case KEY.RETURN:
                    if (selectCurrent()) {
                        // stop default to prevent a form submit, Opera needs special handling
                        event.preventDefault();
                        blockSubmit = true;
                        return false;
                    }
                    break;

                case KEY.ESC:
                    select.hide();
                    break;

                default:
                    clearTimeout(timeout);
                    timeout = setTimeout(onChange, options.delay);
                    break;
            }
        }).focus(function () {
            // track whether the field has focus, we shouldn't process any
            // results if the field no longer has focus
            hasFocus++;
        }).blur(function () {
            hasFocus = 0;
            if (!config.mouseDownOnSelect) {
                hideResults();
            }
        }).click(function () {
            // show select when clicking in a focused field
            if (hasFocus++ > 1 && !select.visible()) {
                onChange(0, true);
            }
        }).bind("search", function () {
            // TODO why not just specifying both arguments?
            var fn = (arguments.length > 1) ? arguments[1] : null;
            function findValueCallback(q, data) {
                var result;
                if (data && data.length) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].result.toLowerCase() == q.toLowerCase()) {
                            result = data[i];
                            break;
                        }
                    }
                }
                if (typeof fn == "function")
                    fn(result);
                else
                    $input.trigger("result", result && [result.data, result.value]);
            }
            $.each(trimWords($input.val()), function (i, value) {
                request(value, findValueCallback, findValueCallback);
            });
        }).bind("flushCache", function () {
            cache.flush();
        }).bind("setOptions", function () {
            $.extend(options, arguments[1]);
            // if we've updated the data, repopulate
            if ("data" in arguments[1])
                cache.populate();
        }).bind("unautocomplete", function () {
            select.unbind();
            $input.unbind();
            $(input.form).unbind(".autocomplete");
        });


        function selectCurrent() {
            var selected = select.selected();
            if (!selected)
                return false;

            var v = selected.result;
            previousValue = v;

            if (options.multiple) {
                var words = trimWords($input.val());
                if (words.length > 1) {
                    var seperator = options.multipleSeparator.length;
                    var cursorAt = $(input).selection().start;
                    var wordAt, progress = 0;
                    $.each(words, function (i, word) {
                        progress += word.length;
                        if (cursorAt <= progress) {
                            wordAt = i;
                            return false;
                        }
                        progress += seperator;
                    });
                    words[wordAt] = v;
                    // TODO this should set the cursor to the right position, but it gets overriden somewhere
                    //$.Autocompleter.Selection(input, progress+seperator, progress+seperator);
                    v = words.join(options.multipleSeparator);
                }
                v += options.multipleSeparator;
            }

            $input.val(v);
            hideResultsNow();
            $input.trigger("result", [selected.data, selected.value]);
            return true;
        }

        function onChange(crap, skipPrevCheck) {
            if (lastKeyPressCode == KEY.DEL) {
                select.hide();
                return;
            }

            var currentValue = $input.val();

            if (!skipPrevCheck && currentValue == previousValue)
                return;

            previousValue = currentValue;

            currentValue = lastWord(currentValue);
            if (currentValue.length >= options.minChars) {
                $input.addClass(options.loadingClass);
                if (!options.matchCase)
                    currentValue = currentValue.toLowerCase();
                request(currentValue, receiveData, hideResultsNow);
            } else {
                stopLoading();
                select.hide();
            }
        }
        ;

        function trimWords(value) {
            if (!value)
                return [""];
            if (!options.multiple)
                return [$.trim(value)];
            return $.map(value.split(options.multipleSeparator), function (word) {
                return $.trim(value).length ? $.trim(word) : null;
            });
        }

        function lastWord(value) {
            if (!options.multiple)
                return value;
            var words = trimWords(value);
            if (words.length == 1)
                return words[0];
            var cursorAt = $(input).selection().start;
            if (cursorAt == value.length) {
                words = trimWords(value)
            } else {
                words = trimWords(value.replace(value.substring(cursorAt), ""));
            }
            return words[words.length - 1];
        }

        // fills in the input box w/the first match (assumed to be the best match)
        // q: the term entered
        // sValue: the first matching result
        function autoFill(q, sValue) {
            // autofill in the complete box w/the first match as long as the user hasn't entered in more data
            // if the last user key pressed was backspace, don't autofill
            if (options.autoFill && (lastWord($input.val()).toLowerCase() == q.toLowerCase()) && lastKeyPressCode != KEY.BACKSPACE) {
                // fill in the value (keep the case the user has typed)
                $input.val($input.val()+sValue.substring(lastWord(previousValue).length));
                // select the portion of the value not typed by the user (so the next character will erase)
                $(input).selection(previousValue.length, previousValue.length+sValue.length);
            }
        }
        ;

        function hideResults() {
            clearTimeout(timeout);
            timeout = setTimeout(hideResultsNow, 200);
        }
        ;

        function hideResultsNow() {
            var wasVisible = select.visible();
            select.hide();
            clearTimeout(timeout);
            stopLoading();
            if (options.mustMatch) {
                // call search and run callback
                $input.search(
                        function (result) {
                            // if no value found, clear the input box
                            if (!result) {
                                if (options.multiple) {
                                    var words = trimWords($input.val()).slice(0, -1);
                                    $input.val(words.join(options.multipleSeparator)+(words.length ? options.multipleSeparator : ""));
                                }
                                else {
                                    $input.val("");
                                    $input.trigger("result", null);
                                }
                            }
                        }
                );
            }
        }
        ;

        function receiveData(q, data) {
            if (data && data.length && hasFocus) {
                stopLoading();
                select.display(data, q);
                autoFill(q, data[0].value);
                select.show();
            } else {
                hideResultsNow();
            }
        }
        ;

        function request(term, success, failure) {
            if (!options.matchCase)
                term = term.toLowerCase();
            var data = cache.load(term);
            // recieve the cached data
            if (data && data.length) {
                success(term, data);
                // if an AJAX url has been supplied, try loading the data now
            } else if ((typeof options.url == "string") && (options.url.length > 0)) {

                var extraParams = {
                    timestamp: +new Date()
                };
                $.each(options.extraParams, function (key, param) {
                    extraParams[key] = typeof param == "function" ? param() : param;
                });

                $.ajax({
                    // try to leverage ajaxQueue plugin to abort previous requests
                    mode: "abort",
                    // limit abortion to this input
                    port: "autocomplete"+input.name,
                    dataType: options.dataType,
                    url: options.url,
                    data: $.extend({
                        q: lastWord(term),
                        limit: options.max
                    }, extraParams),
                    success: function (data) {
                        var parsed = options.parse && options.parse(data) || parse(data);
                        cache.add(term, parsed);
                        success(term, parsed);
                    }
                });
            } else {
                // if we have a failure, we need to empty the list -- this prevents the the [TAB] key from selecting the last successful match
                select.emptyList();
                failure(term);
            }
        }
        ;

        function parse(data) {
            var parsed = [];
            var rows = data.split("\n");
            for (var i = 0; i < rows.length; i++) {
                var row = $.trim(rows[i]);
                if (row) {
                    row = row.split("|");
                    parsed[parsed.length] = {
                        data: row,
                        value: row[0],
                        result: options.formatResult && options.formatResult(row, row[0]) || row[0]
                    };
                }
            }
            return parsed;
        }
        ;

        function stopLoading() {
            $input.removeClass(options.loadingClass);
        }
        ;

    };

    $.Autocompleter.defaults = {
        inputClass: "ui-autocomplete-input",
        resultsClass: "",
        loadingClass: "ui-autocomplete-loading",
        minChars: 1,
        delay: 400,
        matchCase: false,
        matchSubset: true,
        matchContains: false,
        cacheLength: 10,
        max: 100,
        mustMatch: false,
        extraParams: {},
        selectFirst: true,
        formatItem: function (row) {
            return row[0];
        },
        formatMatch: null,
        autoFill: false,
        width: 0,
        multiple: false,
        multipleSeparator: ", ",
        highlight: function (value, term) {
            return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1")+")(?![^<>]*>)(?![^&;]+;)", "gi"), "$1");
        },
        scroll: true,
        scrollHeight: 180
    };

    $.Autocompleter.Cache = function (options) {

        var data = {};
        var length = 0;

        function matchSubset(s, sub) {
            if (!options.matchCase)
                s = s.toLowerCase();
            var i = s.indexOf(sub);
            if (options.matchContains == "word") {
                i = s.toLowerCase().search("\\b"+sub.toLowerCase());
            }
            if (i == -1)
                return false;
            return i == 0 || options.matchContains;
        }
        ;

        function add(q, value) {
            if (length > options.cacheLength) {
                flush();
            }
            if (!data[q]) {
                length++;
            }
            data[q] = value;
        }

        function populate() {
            if (!options.data)
                return false;
            // track the matches
            var stMatchSets = {},
                    nullData = 0;

            // no url was specified, we need to adjust the cache length to make sure it fits the local data store
            if (!options.url)
                options.cacheLength = 1;

            // track all options for minChars = 0
            stMatchSets[""] = [];

            // loop through the array and create a lookup structure
            for (var i = 0, ol = options.data.length; i < ol; i++) {
                var rawValue = options.data[i];
                // if rawValue is a string, make an array otherwise just reference the array
                rawValue = (typeof rawValue == "string") ? [rawValue] : rawValue;

                var value = options.formatMatch(rawValue, i+1, options.data.length);
                if (value === false)
                    continue;

                var firstChar = value.charAt(0).toLowerCase();
                // if no lookup array for this character exists, look it up now
                if (!stMatchSets[firstChar])
                    stMatchSets[firstChar] = [];

                // if the match is a string
                var row = {
                    value: value,
                    data: rawValue,
                    result: options.formatResult && options.formatResult(rawValue) || value
                };

                // push the current match into the set list
                stMatchSets[firstChar].push(row);

                // keep track of minChars zero items
                if (nullData++ < options.max) {
                    stMatchSets[""].push(row);
                }
            }
            ;

            // add the data items to the cache
            $.each(stMatchSets, function (i, value) {
                // increase the cache size
                options.cacheLength++;
                // add to the cache
                add(i, value);
            });
        }

        // populate any existing data
        setTimeout(populate, 25);

        function flush() {
            data = {};
            length = 0;
        }

        return {
            flush: flush,
            add: add,
            populate: populate,
            load: function (q) {
                if (!options.cacheLength || !length)
                    return null;
                /* 
                 * if dealing w/local data and matchContains than we must make sure
                 * to loop through all the data collections looking for matches
                 */
                if (!options.url && options.matchContains) {
                    // track all matches
                    var csub = [];
                    // loop through all the data grids for matches
                    for (var k in data) {
                        // don't search through the stMatchSets[""] (minChars: 0) cache
                        // this prevents duplicates
                        if (k.length > 0) {
                            var c = data[k];
                            $.each(c, function (i, x) {
                                // if we've got a match, add it to the array
                                if (matchSubset(x.value, q)) {
                                    csub.push(x);
                                }
                            });
                        }
                    }
                    return csub;
                } else
                // if the exact item exists, use it
                if (data[q]) {
                    return data[q];
                } else
                if (options.matchSubset) {
                    for (var i = q.length - 1; i >= options.minChars; i--) {
                        var c = data[q.substr(0, i)];
                        if (c) {
                            var csub = [];
                            $.each(c, function (i, x) {
                                if (matchSubset(x.value, q)) {
                                    csub[csub.length] = x;
                                }
                            });
                            return csub;
                        }
                    }
                }
                return null;
            }
        };
    };

    $.Autocompleter.Select = function (options, input, select, config) {
        var CLASSES = {
            //ACTIVE: "ac_over"
            ACTIVE: "ui-state-hover"
        };

        var listItems,
                active = -1,
                data,
                term = "",
                needsInit = true,
                element,
                list;

        // Create results
        function init() {
            if (!needsInit)
                return;
            element = $("<div/>")
                    .hide()
                    .addClass(options.resultsClass)
                    //	.addClass("ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all")
                    .css("position", "absolute")

                    .appendTo(document.body);

            list = $("<ul/>").appendTo(element).mouseover(function (event) {
                if (target(event).nodeName && target(event).nodeName.toUpperCase() == 'LI') {
                    active = $("li", list).removeClass(CLASSES.ACTIVE).index(target(event));
                    $(target(event)).addClass(CLASSES.ACTIVE);
                    $("li", list).children().removeClass(CLASSES.ACTIVE).index(target(event));
                    $(target(event)).children().addClass(CLASSES.ACTIVE);
                }
            }).click(function (event) {
                $(target(event)).addClass(CLASSES.ACTIVE);
                select();
                // TODO provide option to avoid setting focus again after selection? useful for cleanup-on-focus
                input.focus();
                return false;
            }).mousedown(function () {
                config.mouseDownOnSelect = true;
            }).mouseup(function () {
                config.mouseDownOnSelect = false;
            }).addClass("ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all");

            //	}).addClass("ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all");

            if (options.width > 0)
                element.css("width", options.width);

            needsInit = false;
        }

        function target(event) {
            var element = event.target;
            while (element && element.tagName != "LI")
                element = element.parentNode;
            // more fun with IE, sometimes event.target is empty, just ignore it then
            if (!element)
                return [];
            return element;
        }

        function moveSelect(step) {
            listItems.slice(active, active+1).removeClass(CLASSES.ACTIVE);
            listItems.slice(active, active+1).children().removeClass(CLASSES.ACTIVE);
            movePosition(step);
            var activeItem = listItems.slice(active, active+1).addClass(CLASSES.ACTIVE);
            listItems.slice(active, active+1).children().addClass(CLASSES.ACTIVE)
            if (options.scroll) {
                var offset = 0;
                listItems.slice(0, active).each(function () {
                    offset += this.offsetHeight;
                });
                if ((offset+activeItem[0].offsetHeight - list.scrollTop()) > list[0].clientHeight) {
                    list.scrollTop(offset+activeItem[0].offsetHeight - list.innerHeight());
                } else if (offset < list.scrollTop()) {
                    list.scrollTop(offset);
                }
            }
        }
        ;

        function movePosition(step) {
            active += step;
            if (active < 0) {
                active = listItems.size() - 1;
            } else if (active >= listItems.size()) {
                active = 0;
            }
        }

        function limitNumberOfItems(available) {
            return options.max && options.max < available
                    ? options.max
                    : available;
        }

        function fillList() {
            list.empty();
            var max = limitNumberOfItems(data.length);
            for (var i = 0; i < max; i++) {
                if (!data[i])
                    continue;
                var formatted = options.formatItem(data[i].data, i+1, max, data[i].value, term);
                if (formatted === false)
                    continue;
                var li = $("<li/>").html(options.highlight(formatted, term)).addClass("ui-menu-item").appendTo(list)[0];
                $.data(li, "ac_data", data[i]);
            }
            listItems = list.find("li");
            if (options.selectFirst) {
                listItems.slice(0, 1).addClass(CLASSES.ACTIVE);
                listItems.slice(0, 1).children().addClass(CLASSES.ACTIVE);
                active = 0;
            }
            // apply bgiframe if available
            if ($.fn.bgiframe)
                list.bgiframe();
        }

        return {
            display: function (d, q) {
                init();
                data = d;
                term = q;
                fillList();
            },
            next: function () {
                moveSelect(1);
            },
            prev: function () {
                moveSelect(-1);
            },
            pageUp: function () {
                if (active != 0 && active - 8 < 0) {
                    moveSelect(-active);
                } else {
                    moveSelect(-8);
                }
            },
            pageDown: function () {
                if (active != listItems.size() - 1 && active+8 > listItems.size()) {
                    moveSelect(listItems.size() - 1 - active);
                } else {
                    moveSelect(8);
                }
            },
            hide: function () {
                element && element.hide();
                listItems && listItems.removeClass(CLASSES.ACTIVE);
                active = -1;
            },
            visible: function () {
                return element && element.is(":visible");
            },
            current: function () {
                return this.visible() && (listItems.filter("."+CLASSES.ACTIVE)[0] || options.selectFirst && listItems[0]);
            },
            show: function () {
                var offset = $(input).offset();
                element.css({
                    width: typeof options.width == "string" || options.width > 0 ? options.width : $(input).width(),
                    top: offset.top+input.offsetHeight,
                    left: offset.left
                }).show();
                if (options.scroll) {
                    list.scrollTop(0);
                    list.css({
                        maxHeight: options.scrollHeight,
                        overflow: 'auto'
                    });

                    if ($.browser.msie && typeof document.body.style.maxHeight === "undefined") {
                        var listHeight = 0;
                        listItems.each(function () {
                            listHeight += this.offsetHeight;
                        });
                        var scrollbarsVisible = listHeight > options.scrollHeight;
                        list.css('height', scrollbarsVisible ? options.scrollHeight : listHeight);
                        if (!scrollbarsVisible) {
                            // IE doesn't recalculate width when scrollbar disappears
                            listItems.width(list.width() - parseInt(listItems.css("padding-left")) - parseInt(listItems.css("padding-right")));
                        }
                    }

                }
            },
            selected: function () {
                var selected = listItems && listItems.filter("."+CLASSES.ACTIVE).removeClass(CLASSES.ACTIVE);
                return selected && selected.length && $.data(selected[0], "ac_data");
            },
            emptyList: function () {
                list && list.empty();
            },
            unbind: function () {
                element && element.remove();
            }
        };
    };

    $.fn.selection = function (start, end) {
        if (start !== undefined) {
            return this.each(function () {
                if (this.createTextRange) {
                    var selRange = this.createTextRange();
                    if (end === undefined || start == end) {
                        selRange.move("character", start);
                        selRange.select();
                    } else {
                        selRange.collapse(true);
                        selRange.moveStart("character", start);
                        selRange.moveEnd("character", end);
                        selRange.select();
                    }
                } else if (this.setSelectionRange) {
                    this.setSelectionRange(start, end);
                } else if (this.selectionStart) {
                    this.selectionStart = start;
                    this.selectionEnd = end;
                }
            });
        }
        var field = this[0];
        if (field.createTextRange) {
            var range = document.selection.createRange(),
                    orig = field.value,
                    teststring = "<->",
                    textLength = range.text.length;
            range.text = teststring;
            var caretAt = field.value.indexOf(teststring);
            field.value = orig;
            this.selection(caretAt, caretAt+textLength);
            return {
                start: caretAt,
                end: caretAt+textLength
            }
        } else if (field.selectionStart !== undefined) {
            return {
                start: field.selectionStart,
                end: field.selectionEnd
            }
        }
    };

})(jQuery);




/*
 
 highlight v3
 
 Highlights arbitrary terms.
 
 <http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html>
 
 MIT license.
 
 Johann Burkard
 <http://johannburkard.de>
 <mailto:jb@eaio.com>
 
 */

jQuery.fn.highlight = function (pat) {
    function innerHighlight(node, pat) {
        var skip = 0;

        //  if (node.data==null)
        //	return false
// alert(pat)
        if (node.nodeType == 3) {
            //  var pos = node.data.toUpperCase().indexOf(pat);
            //node.data.toUpperCase().indexOf(te); 

            if (node.data != null)
                var pos = node.data.toUpperCase().indexOf(pat.toUpperCase());
            else
                var pos = -1
            if (pos >= 0) {
                var spannode = document.createElement('span');
                spannode.className = 'highlight';
                var middlebit = node.splitText(pos);
                var endbit = middlebit.splitText(pat.length);
                var middleclone = middlebit.cloneNode(true);
                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        }
        else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.each(function () {
        innerHighlight(this, pat.toUpperCase());
    });
};

jQuery.fn.removeHighlight = function () {
    return this.find("span.highlight").each(function () {
        this.parentNode.firstChild.nodeName;
        with (this.parentNode) {
            replaceChild(this.firstChild, this);
            normalize();
        }
    }).end();
};


/*
 * Copyright (c) 2008 Greg Weber greg at gregweber.info
 * Dual licensed under the MIT and GPLv2 licenses just as jQuery is:
 * http://jquery.org/license
 *
 * documentation at http://gregweber.info/projects/uitablefilter
 *
 * allows table rows to be filtered (made invisible)
 * <code>
 * t = $('table')
 * $.uiTableFilter( t, phrase )
 * </code>
 * arguments:
 *   jQuery object containing table rows
 *   phrase to search for
 *   optional arguments:
 *     column to limit search too (the column title in the table header)
 *     ifHidden - callback to execute if one or more elements was hidden FILTRO DE TABLAAAAAAAAAAAAAAAAAAAA
 */
(function ($) {
    $.uiTableFilter = function (jq, phrase, column, ifHidden) {
        var new_hidden = false;
        if (this.last_phrase === phrase)
            return false;

        var phrase_length = phrase.length;
        var words = phrase.toLowerCase().split(" ");

        // these function pointers may change
        var matches = function (elem) {
            elem.show()
        }
        var noMatch = function (elem) {
            elem.hide();
            new_hidden = true
        }
        var getText = function (elem) {
            return elem.text()
        }

        if (column) {
            var index = null;
            jq.find("thead > tr:last > th").each(function (i) {
                if ($.trim($(this).text()) == column) {
                    index = i;
                    return false;
                }
            });
            if (index == null)
                throw("given column: "+column+" not found")

            getText = function (elem) {
                return $(elem.find(
                        ("td:eq("+index+")"))).text()
            }
        }

        // if added one letter to last time,
        // just check newest word and only need to hide
        if ((words.size > 1) && (phrase.substr(0, phrase_length - 1) ===
                this.last_phrase)) {

            if (phrase[-1] === " ")
            {
                this.last_phrase = phrase;
                return false;
            }

            var words = words[-1]; // just search for the newest word

            // only hide visible rows
            matches = function (elem) {
                ;
            }
            var elems = jq.find("tbody:first > tr:visible")
        }
        else {
            new_hidden = true;
            var elems = jq.find("tbody:first > tr")
        }

        elems.each(function () {
            var elem = $(this);
            $.uiTableFilter.has_words(getText(elem), words, false) ?
                    matches(elem) : noMatch(elem);
        });

        last_phrase = phrase;
        if (ifHidden && new_hidden)
            ifHidden();
        return jq;
    };

    // caching for speedup
    $.uiTableFilter.last_phrase = ""

    // not jQuery dependent
    // "" [""] -> Boolean
    // "" [""] Boolean -> Boolean
    $.uiTableFilter.has_words = function (str, words, caseSensitive)
    {
        var text = caseSensitive ? str : str.toLowerCase();
        for (var i = 0; i < words.length; i++) {
            if (text.indexOf(words[i]) === -1)
                return false;
        }
        return true;
    }
})(jQuery);

/*jQuery(function($){
 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '&#x3c;Ant',
 nextText: 'Sig&#x3e;',
 currentText: 'Hoy',
 monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
 'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
 'Jul','Ago','Sep','Oct','Nov','Dic'],
 dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
 dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 changeMonth: true,
 changeYear: true,
 showAnim:'drop',
 yearSuffix: ''};
 $.datepicker.setDefaults($.datepicker.regional['es']);
 });
 
 
 (function(a){a.uniform={options:{selectClass:"selector",radioClass:"radio",checkboxClass:"checker",fileClass:"uploader",filenameClass:"filename",fileBtnClass:"action",fileDefaultText:"No file selected",fileBtnText:"Choose File",checkedClass:"checked",focusClass:"focus",disabledClass:"disabled",buttonClass:"button",activeClass:"active",hoverClass:"hover",useID:true,idPrefix:"uniform",resetSelector:false,autoHide:true},elements:[]};if(a.browser.msie&&a.browser.version<7){a.support.selectOpacity=false}else{a.support.selectOpacity=true}a.fn.uniform=function(k){k=a.extend(a.uniform.options,k);var d=this;if(k.resetSelector!=false){a(k.resetSelector).mouseup(function(){function l(){a.uniform.update(d)}setTimeout(l,10)})}function j(l){$el=a(l);$el.addClass($el.attr("type"));b(l)}function g(l){a(l).addClass("uniform");b(l)}function i(o){var m=a(o);var p=a("<div>"),l=a("<span>");p.addClass(k.buttonClass);if(k.useID&&m.attr("id")!=""){p.attr("id",k.idPrefix+"-"+m.attr("id"))}var n;if(m.is("a")||m.is("button")){n=m.text()}else{if(m.is(":submit")||m.is(":reset")||m.is("input[type=button]")){n=m.attr("value")}}n=n==""?m.is(":reset")?"Reset":"Submit":n;l.html(n);m.css("opacity",0);m.wrap(p);m.wrap(l);p=m.closest("div");l=m.closest("span");if(m.is(":disabled")){p.addClass(k.disabledClass)}p.bind({"mouseenter.uniform":function(){p.addClass(k.hoverClass)},"mouseleave.uniform":function(){p.removeClass(k.hoverClass);p.removeClass(k.activeClass)},"mousedown.uniform touchbegin.uniform":function(){p.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"click.uniform touchend.uniform":function(r){if(a(r.target).is("span")||a(r.target).is("div")){if(o[0].dispatchEvent){var q=document.createEvent("MouseEvents");q.initEvent("click",true,true);o[0].dispatchEvent(q)}else{o[0].click()}}}});o.bind({"focus.uniform":function(){p.addClass(k.focusClass)},"blur.uniform":function(){p.removeClass(k.focusClass)}});a.uniform.noSelect(p);b(o)}function e(o){var m=a(o);var p=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){p.hide()}p.addClass(k.selectClass);if(k.useID&&o.attr("id")!=""){p.attr("id",k.idPrefix+"-"+o.attr("id"))}var n=o.find(":selected:first");if(n.length==0){n=o.find("option:first")}l.html(n.html());o.css("opacity",0);o.wrap(p);o.before(l);p=o.parent("div");l=o.siblings("span");o.bind({"change.uniform":function(){l.text(o.find(":selected").html());p.removeClass(k.activeClass)},"focus.uniform":function(){p.addClass(k.focusClass)},"blur.uniform":function(){p.removeClass(k.focusClass);p.removeClass(k.activeClass)},"mousedown.uniform touchbegin.uniform":function(){p.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"click.uniform touchend.uniform":function(){p.removeClass(k.activeClass)},"mouseenter.uniform":function(){p.addClass(k.hoverClass)},"mouseleave.uniform":function(){p.removeClass(k.hoverClass);p.removeClass(k.activeClass)},"keyup.uniform":function(){l.text(o.find(":selected").html())}});if(a(o).attr("disabled")){p.addClass(k.disabledClass)}a.uniform.noSelect(l);b(o)}function f(n){var m=a(n);var o=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){o.hide()}o.addClass(k.checkboxClass);if(k.useID&&n.attr("id")!=""){o.attr("id",k.idPrefix+"-"+n.attr("id"))}a(n).wrap(o);a(n).wrap(l);l=n.parent();o=l.parent();a(n).css("opacity",0).bind({"focus.uniform":function(){o.addClass(k.focusClass)},"blur.uniform":function(){o.removeClass(k.focusClass)},"click.uniform touchend.uniform":function(){if(!a(n).attr("checked")){l.removeClass(k.checkedClass)}else{l.addClass(k.checkedClass)}},"mousedown.uniform touchbegin.uniform":function(){o.addClass(k.activeClass)},"mouseup.uniform touchend.uniform":function(){o.removeClass(k.activeClass)},"mouseenter.uniform":function(){o.addClass(k.hoverClass)},"mouseleave.uniform":function(){o.removeClass(k.hoverClass);o.removeClass(k.activeClass)}});if(a(n).attr("checked")){l.addClass(k.checkedClass)}if(a(n).attr("disabled")){o.addClass(k.disabledClass)}b(n)}function c(n){var m=a(n);var o=a("<div />"),l=a("<span />");if(!m.css("display")=="none"&&k.autoHide){o.hide()}o.addClass(k.radioClass);if(k.useID&&n.attr("id")!=""){o.attr("id",k.idPrefix+"-"+n.attr("id"))}a(n).wrap(o);a(n).wrap(l);l=n.parent();o=l.parent();a(n).css("opacity",0).bind({"focus.uniform":function(){o.addClass(k.focusClass)},"blur.uniform":function(){o.removeClass(k.focusClass)},"click.uniform touchend.uniform":function(){if(!a(n).attr("checked")){l.removeClass(k.checkedClass)}else{var p=k.radioClass.split(" ")[0];a("."+p+" span."+k.checkedClass+":has([name='"+a(n).attr("name")+"'])").removeClass(k.checkedClass);l.addClass(k.checkedClass)}},"mousedown.uniform touchend.uniform":function(){if(!a(n).is(":disabled")){o.addClass(k.activeClass)}},"mouseup.uniform touchbegin.uniform":function(){o.removeClass(k.activeClass)},"mouseenter.uniform touchend.uniform":function(){o.addClass(k.hoverClass)},"mouseleave.uniform":function(){o.removeClass(k.hoverClass);o.removeClass(k.activeClass)}});if(a(n).attr("checked")){l.addClass(k.checkedClass)}if(a(n).attr("disabled")){o.addClass(k.disabledClass)}b(n)}function h(q){var o=a(q);var r=a("<div />"),p=a("<span>"+k.fileDefaultText+"</span>"),m=a("<span>"+k.fileBtnText+"</span>");if(!o.css("display")=="none"&&k.autoHide){r.hide()}r.addClass(k.fileClass);p.addClass(k.filenameClass);m.addClass(k.fileBtnClass);if(k.useID&&o.attr("id")!=""){r.attr("id",k.idPrefix+"-"+o.attr("id"))}o.wrap(r);o.after(m);o.after(p);r=o.closest("div");p=o.siblings("."+k.filenameClass);m=o.siblings("."+k.fileBtnClass);if(!o.attr("size")){var l=r.width();o.attr("size",l/10)}var n=function(){var s=o.val();if(s===""){s=k.fileDefaultText}else{s=s.split(/[\/\\]+/);s=s[(s.length-1)]}p.text(s)};n();o.css("opacity",0).bind({"focus.uniform":function(){r.addClass(k.focusClass)},"blur.uniform":function(){r.removeClass(k.focusClass)},"mousedown.uniform":function(){if(!a(q).is(":disabled")){r.addClass(k.activeClass)}},"mouseup.uniform":function(){r.removeClass(k.activeClass)},"mouseenter.uniform":function(){r.addClass(k.hoverClass)},"mouseleave.uniform":function(){r.removeClass(k.hoverClass);r.removeClass(k.activeClass)}});if(a.browser.msie){o.bind("click.uniform.ie7",function(){setTimeout(n,0)})}else{o.bind("change.uniform",n)}if(o.attr("disabled")){r.addClass(k.disabledClass)}a.uniform.noSelect(p);a.uniform.noSelect(m);b(q)}a.uniform.restore=function(l){if(l==undefined){l=a(a.uniform.elements)}a(l).each(function(){if(a(this).is(":checkbox")){a(this).unwrap().unwrap()}else{if(a(this).is("select")){a(this).siblings("span").remove();a(this).unwrap()}else{if(a(this).is(":radio")){a(this).unwrap().unwrap()}else{if(a(this).is(":file")){a(this).siblings("span").remove();a(this).unwrap()}else{if(a(this).is("button, :submit, :reset, a, input[type='button']")){a(this).unwrap().unwrap()}}}}}a(this).unbind(".uniform");a(this).css("opacity","1");var m=a.inArray(a(l),a.uniform.elements);a.uniform.elements.splice(m,1)})};function b(l){l=a(l).get();if(l.length>1){a.each(l,function(m,n){a.uniform.elements.push(n)})}else{a.uniform.elements.push(l)}}a.uniform.noSelect=function(l){function m(){return false}a(l).each(function(){this.onselectstart=this.ondragstart=m;a(this).mousedown(m).css({MozUserSelect:"none"})})};a.uniform.update=function(l){if(l==undefined){l=a(a.uniform.elements)}l=a(l);l.each(function(){var n=a(this);if(n.is("select")){var m=n.siblings("span");var p=n.parent("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.html(n.find(":selected").html());if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":checkbox")){var m=n.closest("span");var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.removeClass(k.checkedClass);if(n.is(":checked")){m.addClass(k.checkedClass)}if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":radio")){var m=n.closest("span");var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);m.removeClass(k.checkedClass);if(n.is(":checked")){m.addClass(k.checkedClass)}if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":file")){var p=n.parent("div");var o=n.siblings(k.filenameClass);btnTag=n.siblings(k.fileBtnClass);p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);o.text(n.val());if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}else{if(n.is(":submit")||n.is(":reset")||n.is("button")||n.is("a")||l.is("input[type=button]")){var p=n.closest("div");p.removeClass(k.hoverClass+" "+k.focusClass+" "+k.activeClass);if(n.is(":disabled")){p.addClass(k.disabledClass)}else{p.removeClass(k.disabledClass)}}}}}}})};return this.each(function(){if(a.support.selectOpacity){var l=a(this);if(l.is("select")){if(l.attr("multiple")!=true){if(l.attr("size")==undefined||l.attr("size")<=1){e(l)}}}else{if(l.is(":checkbox")){f(l)}else{if(l.is(":radio")){c(l)}else{if(l.is(":file")){h(l)}else{if(l.is(":text, :password, input[type='email']")){j(l)}else{if(l.is("textarea")){g(l)}else{if(l.is("a")||l.is(":submit")||l.is(":reset")||l.is("button")||l.is("input[type=button]")){i(l)}}}}}}}}})}})(jQuery);
 
 */

/*!
 * jQuery blockUI plugin
 * Version 2.38 (29-MAR-2011)
 * @requires jQuery v1.2.3 or later
 *
 * Examples at: http://malsup.com/jquery/block/
 * Copyright (c) 2007-2010 M. Alsup
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Thanks to Amir-Hossein Sobhi for some excellent contributions!
 */

;
(function ($) {

    if (/1\.(0|1|2)\.(0|1|2)/.test($.fn.jquery) || /^1.1/.test($.fn.jquery)) {
        alert('blockUI requires jQuery v1.2.3 or later!  You are using v'+$.fn.jquery);
        return;
    }

    $.fn._fadeIn = $.fn.fadeIn;

    var noOp = function () {
    };

// this bit is to ensure we don't call setExpression when we shouldn't (with extra muscle to handle
// retarded userAgent strings on Vista)
    var mode = document.documentMode || 0;
    var setExpr = $.browser.msie && (($.browser.version < 8 && !mode) || mode < 8);
    var ie6 = $.browser.msie && /MSIE 6.0/.test(navigator.userAgent) && !mode;

// global $ methods for blocking/unblocking the entire page
    $.blockUI = function (opts) {
        install(window, opts);
    };
    $.unblockUI = function (opts) {
        remove(window, opts);
    };

// convenience method for quick growl-like notifications  (http://www.google.com/search?q=growl)
    $.growlUI = function (title, op, message, timeout, onClose) {
        if (op == 1)
            var $m = $('<div class="growlUI" align="center"></div>');
        else
            var $m = $('<div class="growlUI2" align="center" ></div>');

        if (title)
            $m.append('<h1>'+title+'</h1>');
        if (message)
            $m.append('<h2>'+message+'</h2>');
        if (timeout == undefined)
            timeout = 3000;
        if (op == 1)
        {
            $.blockUI({
                message: $m, fadeIn: 700, fadeOut: 1000, centerY: false,
                timeout: timeout, showOverlay: false,
                onUnblock: onClose,
                css: $.blockUI.defaults.growlCSS
            });
        }
        else
        {
            $.blockUI({
                message: $m, fadeIn: 700, fadeOut: 1000, centerY: false,
                timeout: timeout, showOverlay: false,
                onUnblock: onClose,
                css: $.blockUI.defaults.growlCSSe
            });
        }
    };

// plugin method for blocking element content
    $.fn.block = function (opts) {
        return this.unblock({fadeOut: 0}).each(function () {
            if ($.css(this, 'position') == 'static')
                this.style.position = 'relative';
            if ($.browser.msie)
                this.style.zoom = 1; // force 'hasLayout'
            install(this, opts);
        });
    };

// plugin method for unblocking element content
    $.fn.unblock = function (opts) {
        return this.each(function () {
            remove(this, opts);
        });
    };

    $.blockUI.version = 2.38; // 2nd generation blocking at no extra cost!

// override these in your code to change the default behavior and style
    $.blockUI.defaults = {
        // message displayed when blocking (use null for no message)
        message: '<h1>Please wait...</h1>',
        title: null, // title string; only used when theme == true
        draggable: true, // only used when theme == true (requires jquery-ui.js to be loaded)

        theme: false, // set to true to use with jQuery UI themes

        // styles for the message when blocking; if you wish to disable
        // these and use an external stylesheet then do this in your code:
        // $.blockUI.defaults.css = {};
        css: {
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            color: '#000',
            border: '3px solid #aaa',
            backgroundColor: '#fff',
            cursor: 'wait'
        },
        // minimal style set used when themes are used
        themedCSS: {
            width: '30%',
            top: '40%',
            left: '35%'
        },
        // styles for the overlay
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.6,
            cursor: 'wait'
        },
        // styles applied when using $.growlUI
        growlCSS: {
            width: '350px',
            top: '10px',
            left: '',
            right: '10px',
            border: 'none',
            padding: '5px',
            opacity: 0.6,
            cursor: 'default',
            color: '#fff',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            'border-radius': '10px'
        },
        growlCSSe: {
            width: '350px',
            top: '10px',
            left: '',
            right: '10px',
            border: 'none',
            padding: '5px',
            opacity: 0.6,
            cursor: 'default',
            color: '#F00',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            'border-radius': '10px'
        },
        // IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
        // (hat tip to Jorge H. N. de Vasconcelos)
        iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',
        // force usage of iframe in non-IE browsers (handy for blocking applets)
        forceIframe: false,
        // z-index for the blocking overlay
        baseZ: 1010,
        // set these to true to have the message automatically centered
        centerX: true, // <-- only effects element blocking (page block controlled via css above)
        centerY: true,
        // allow body element to be stetched in ie6; this makes blocking look better
        // on "short" pages.  disable if you wish to prevent changes to the body height
        allowBodyStretch: true,
        // enable if you want key and mouse events to be disabled for content that is blocked
        bindEvents: true,
        // be default blockUI will supress tab navigation from leaving blocking content
        // (if bindEvents is true)
        constrainTabKey: true,
        // fadeIn time in millis; set to 0 to disable fadeIn on block
        fadeIn: 200,
        // fadeOut time in millis; set to 0 to disable fadeOut on unblock
        fadeOut: 400,
        // time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
        timeout: 0,
        // disable if you don't want to show the overlay
        showOverlay: true,
        // if true, focus will be placed in the first available input field when
        // page blocking
        focusInput: true,
        // suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
        applyPlatformOpacityRules: true,
        // callback method invoked when fadeIn has completed and blocking message is visible
        onBlock: null,
        // callback method invoked when unblocking has completed; the callback is
        // passed the element that has been unblocked (which is the window object for page
        // blocks) and the options that were passed to the unblock call:
        //	 onUnblock(element, options)
        onUnblock: null,
        // don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
        quirksmodeOffsetHack: 4,
        // class name of the message block
        blockMsgClass: 'blockMsg'
    };

// private data and functions follow...

    var pageBlock = null;
    var pageBlockEls = [];

    function install(el, opts) {
        var full = (el == window);
        var msg = opts && opts.message !== undefined ? opts.message : undefined;
        opts = $.extend({}, $.blockUI.defaults, opts || {});
        opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
        var css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
        var themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
        msg = msg === undefined ? opts.message : msg;

        // remove the current block (if there is one)
        if (full && pageBlock)
            remove(window, {fadeOut: 0});

        // if an existing element is being used as the blocking content then we capture
        // its current place in the DOM (and current display style) so we can restore
        // it when we unblock
        if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
            var node = msg.jquery ? msg[0] : msg;
            var data = {};
            $(el).data('blockUI.history', data);
            data.el = node;
            data.parent = node.parentNode;
            data.display = node.style.display;
            data.position = node.style.position;
            if (data.parent)
                data.parent.removeChild(node);
        }

        var z = opts.baseZ;

        // blockUI uses 3 layers for blocking, for simplicity they are all used on every platform;
        // layer1 is the iframe layer which is used to supress bleed through of underlying content
        // layer2 is the overlay layer which has opacity and a wait cursor (by default)
        // layer3 is the message content that is displayed while blocking

        var lyr1 = ($.browser.msie || opts.forceIframe)
                ? $('<iframe class="blockUI" style="z-index:'+(z++)+';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>')
                : $('<div class="blockUI" style="display:none"></div>');

        var lyr2 = opts.theme
                ? $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+(z++)+';display:none"></div>')
                : $('<div class="blockUI blockOverlay" style="z-index:'+(z++)+';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');

        var lyr3, s;
        if (opts.theme && full) {
            s = '<div class="blockUI '+opts.blockMsgClass+' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+z+';display:none;position:fixed">' +
                    '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>' +
                    '<div class="ui-widget-content ui-dialog-content"></div>' +
                    '</div>';
        }
        else if (opts.theme) {
            s = '<div class="blockUI '+opts.blockMsgClass+' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+z+';display:none;position:absolute">' +
                    '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>' +
                    '<div class="ui-widget-content ui-dialog-content"></div>' +
                    '</div>';
        }
        else if (full) {
            s = '<div class="blockUI '+opts.blockMsgClass+' blockPage" style="z-index:'+z+';display:none;position:fixed"></div>';
        }
        else {
            s = '<div class="blockUI '+opts.blockMsgClass+' blockElement" style="z-index:'+z+';display:none;position:absolute"></div>';
        }
        lyr3 = $(s);

        // if we have a message, style it
        if (msg) {
            if (opts.theme) {
                lyr3.css(themedCSS);
                lyr3.addClass('ui-widget-content');
            }
            else
                lyr3.css(css);
        }

        // style the overlay
        if (!opts.theme && (!opts.applyPlatformOpacityRules || !($.browser.mozilla && /Linux/.test(navigator.platform))))
            lyr2.css(opts.overlayCSS);
        lyr2.css('position', full ? 'fixed' : 'absolute');

        // make iframe layer transparent in IE
        if ($.browser.msie || opts.forceIframe)
            lyr1.css('opacity', 0.0);

        //$([lyr1[0],lyr2[0],lyr3[0]]).appendTo(full ? 'body' : el);
        var layers = [lyr1, lyr2, lyr3], $par = full ? $('body') : $(el);
        $.each(layers, function () {
            this.appendTo($par);
        });

        if (opts.theme && opts.draggable && $.fn.draggable) {
            lyr3.draggable({
                handle: '.ui-dialog-titlebar',
                cancel: 'li'
            });
        }

        // ie7 must use absolute positioning in quirks mode and to account for activex issues (when scrolling)
        var expr = setExpr && (!$.boxModel || $('object,embed', full ? null : el).length > 0);
        if (ie6 || expr) {
            // give body 100% height
            if (full && opts.allowBodyStretch && $.boxModel)
                $('html,body').css('height', '100%');

            // fix ie6 issue when blocked element has a border width
            if ((ie6 || !$.boxModel) && !full) {
                var t = sz(el, 'borderTopWidth'), l = sz(el, 'borderLeftWidth');
                var fixT = t ? '(0 - '+t+')' : 0;
                var fixL = l ? '(0 - '+l+')' : 0;
            }

            // simulate fixed position
            $.each([lyr1, lyr2, lyr3], function (i, o) {
                var s = o[0].style;
                s.position = 'absolute';
                if (i < 2) {
                    full ? s.setExpression('height', 'Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.boxModel?0:'+opts.quirksmodeOffsetHack+')+"px"')
                            : s.setExpression('height', 'this.parentNode.offsetHeight+"px"');
                    full ? s.setExpression('width', 'jQuery.boxModel && document.documentElement.clientWidth || document.body.clientWidth+"px"')
                            : s.setExpression('width', 'this.parentNode.offsetWidth+"px"');
                    if (fixL)
                        s.setExpression('left', fixL);
                    if (fixT)
                        s.setExpression('top', fixT);
                }
                else if (opts.centerY) {
                    if (full)
                        s.setExpression('top', '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2)+(blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)+"px"');
                    s.marginTop = 0;
                }
                else if (!opts.centerY && full) {
                    var top = (opts.css && opts.css.top) ? parseInt(opts.css.top) : 0;
                    var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)+'+top+')+"px"';
                    s.setExpression('top', expression);
                }
            });
        }

        // show the message
        if (msg) {
            if (opts.theme)
                lyr3.find('.ui-widget-content').append(msg);
            else
                lyr3.append(msg);
            if (msg.jquery || msg.nodeType)
                $(msg).show();
        }

        if (($.browser.msie || opts.forceIframe) && opts.showOverlay)
            lyr1.show(); // opacity is zero
        if (opts.fadeIn) {
            var cb = opts.onBlock ? opts.onBlock : noOp;
            var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
            var cb2 = msg ? cb : noOp;
            if (opts.showOverlay)
                lyr2._fadeIn(opts.fadeIn, cb1);
            if (msg)
                lyr3._fadeIn(opts.fadeIn, cb2);
        }
        else {
            if (opts.showOverlay)
                lyr2.show();
            if (msg)
                lyr3.show();
            if (opts.onBlock)
                opts.onBlock();
        }

        // bind key and mouse events
        bind(1, el, opts);

        if (full) {
            pageBlock = lyr3[0];
            pageBlockEls = $(':input:enabled:visible', pageBlock);
            if (opts.focusInput)
                setTimeout(focus, 20);
        }
        else
            center(lyr3[0], opts.centerX, opts.centerY);

        if (opts.timeout) {
            // auto-unblock
            var to = setTimeout(function () {
                full ? $.unblockUI(opts) : $(el).unblock(opts);
            }, opts.timeout);
            $(el).data('blockUI.timeout', to);
        }
    }
    ;

// remove the block
    function remove(el, opts) {
        var full = (el == window);
        var $el = $(el);
        var data = $el.data('blockUI.history');
        var to = $el.data('blockUI.timeout');
        if (to) {
            clearTimeout(to);
            $el.removeData('blockUI.timeout');
        }
        opts = $.extend({}, $.blockUI.defaults, opts || {});
        bind(0, el, opts); // unbind events

        var els;
        if (full) // crazy selector to handle odd field errors in ie6/7
            els = $('body').children().filter('.blockUI').add('body > .blockUI');
        else
            els = $('.blockUI', el);

        if (full)
            pageBlock = pageBlockEls = null;

        if (opts.fadeOut) {
            els.fadeOut(opts.fadeOut);
            setTimeout(function () {
                reset(els, data, opts, el);
            }, opts.fadeOut);
        }
        else
            reset(els, data, opts, el);
    }
    ;

// move blocking element back into the DOM where it started
    function reset(els, data, opts, el) {
        els.each(function (i, o) {
            // remove via DOM calls so we don't lose event handlers
            if (this.parentNode)
                this.parentNode.removeChild(this);
        });

        if (data && data.el) {
            data.el.style.display = data.display;
            data.el.style.position = data.position;
            if (data.parent)
                data.parent.appendChild(data.el);
            $(el).removeData('blockUI.history');
        }

        if (typeof opts.onUnblock == 'function')
            opts.onUnblock(el, opts);
    }
    ;

// bind/unbind the handler
    function bind(b, el, opts) {
        var full = el == window, $el = $(el);

        // don't bother unbinding if there is nothing to unbind
        if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
            return;
        if (!full)
            $el.data('blockUI.isBlocked', b);

        // don't bind events when overlay is not in use or if bindEvents is false
        if (!opts.bindEvents || (b && !opts.showOverlay))
            return;

        // bind anchors and inputs for mouse and key events
        var events = 'mousedown mouseup keydown keypress';
        b ? $(document).bind(events, opts, handler) : $(document).unbind(events, handler);

// former impl...
//	   var $e = $('a,:input');
//	   b ? $e.bind(events, opts, handler) : $e.unbind(events, handler);
    }
    ;

// event handler to suppress keyboard/mouse events when blocking
    function handler(e) {
        // allow tab navigation (conditionally)
        if (e.keyCode && e.keyCode == 9) {
            if (pageBlock && e.data.constrainTabKey) {
                var els = pageBlockEls;
                var fwd = !e.shiftKey && e.target === els[els.length - 1];
                var back = e.shiftKey && e.target === els[0];
                if (fwd || back) {
                    setTimeout(function () {
                        focus(back)
                    }, 10);
                    return false;
                }
            }
        }
        var opts = e.data;
        // allow events within the message content
        if ($(e.target).parents('div.'+opts.blockMsgClass).length > 0)
            return true;

        // allow events for content that is not being blocked
        return $(e.target).parents().children().filter('div.blockUI').length == 0;
    }
    ;

    function focus(back) {
        if (!pageBlockEls)
            return;
        var e = pageBlockEls[back === true ? pageBlockEls.length - 1 : 0];
        if (e)
            e.focus();
    }
    ;

    function center(el, x, y) {
        var p = el.parentNode, s = el.style;
        var l = ((p.offsetWidth - el.offsetWidth) / 2) - sz(p, 'borderLeftWidth');
        var t = ((p.offsetHeight - el.offsetHeight) / 2) - sz(p, 'borderTopWidth');
        if (x)
            s.left = l > 0 ? (l+'px') : '0';
        if (y)
            s.top = t > 0 ? (t+'px') : '0';
    }
    ;

    function sz(el, p) {
        return parseInt($.css(el, p)) || 0;
    }
    ;

})(jQuery);


/*
 
 FullCalendar v1.5.4
 http://arshaw.com/fullcalendar/
 
 Use fullcalendar.css for basic styling.
 For event drag & drop, requires jQuery UI draggable.
 For event resizing, requires jQuery UI resizable.
 
 Copyright (c) 2011 Adam Shaw
 Dual licensed under the MIT and GPL licenses, located in
 MIT-LICENSE.txt and GPL-LICENSE.txt respectively.
 
 Date: Tue Sep 4 23:38:33 2012 -0700
 
 */
(function (m, ma) {
    function wb(a) {
        m.extend(true, Ya, a)
    }
    function Yb(a, b, e) {
        function d(k) {
            if (E) {
                u();
                q();
                na();
                S(k)
            } else
                f()
        }
        function f() {
            B = b.theme ? "ui" : "fc";
            a.addClass("fc");
            b.isRTL && a.addClass("fc-rtl");
            b.theme && a.addClass("ui-widget");
            E = m("<div class='fc-content' style='position:relative'/>").prependTo(a);
            C = new Zb(X, b);
            (P = C.render()) && a.prepend(P);
            y(b.defaultView);
            m(window).resize(oa);
            t() || g()
        }
        function g() {
            setTimeout(function () {
                !n.start && t() && S()
            }, 0)
        }
        function l() {
            m(window).unbind("resize", oa);
            C.destroy();
            E.remove();
            a.removeClass("fc fc-rtl ui-widget")
        }
        function j() {
            return i.offsetWidth !== 0
        }
        function t() {
            return m("body")[0].offsetWidth !== 0
        }
        function y(k) {
            if (!n || k != n.name) {
                F++;
                pa();
                var D = n, Z;
                if (D) {
                    (D.beforeHide || xb)();
                    Za(E, E.height());
                    D.element.hide()
                } else
                    Za(E, 1);
                E.css("overflow", "hidden");
                if (n = Y[k])
                    n.element.show();
                else
                    n = Y[k] = new Ja[k](Z = s = m("<div class='fc-view fc-view-"+k+"' style='position:absolute'/>").appendTo(E), X);
                D && C.deactivateButton(D.name);
                C.activateButton(k);
                S();
                E.css("overflow", "");
                D &&
                        Za(E, 1);
                Z || (n.afterShow || xb)();
                F--
            }
        }
        function S(k) {
            if (j()) {
                F++;
                pa();
                o === ma && u();
                var D = false;
                if (!n.start || k || r < n.start || r >= n.end) {
                    n.render(r, k || 0);
                    fa(true);
                    D = true
                } else if (n.sizeDirty) {
                    n.clearEvents();
                    fa();
                    D = true
                } else if (n.eventsDirty) {
                    n.clearEvents();
                    D = true
                }
                n.sizeDirty = false;
                n.eventsDirty = false;
                ga(D);
                W = a.outerWidth();
                C.updateTitle(n.title);
                k = new Date;
                k >= n.start && k < n.end ? C.disableButton("today") : C.enableButton("today");
                F--;
                n.trigger("viewDisplay", i)
            }
        }
        function Q() {
            q();
            if (j()) {
                u();
                fa();
                pa();
                n.clearEvents();
                n.renderEvents(J);
                n.sizeDirty = false
            }
        }
        function q() {
            m.each(Y, function (k, D) {
                D.sizeDirty = true
            })
        }
        function u() {
            o = b.contentHeight ? b.contentHeight : b.height ? b.height - (P ? P.height() : 0) - Sa(E) : Math.round(E.width() / Math.max(b.aspectRatio, 0.5))
        }
        function fa(k) {
            F++;
            n.setHeight(o, k);
            if (s) {
                s.css("position", "relative");
                s = null
            }
            n.setWidth(E.width(), k);
            F--
        }
        function oa() {
            if (!F)
                if (n.start) {
                    var k = ++v;
                    setTimeout(function () {
                        if (k == v && !F && j())
                            if (W != (W = a.outerWidth())) {
                                F++;
                                Q();
                                n.trigger("windowResize", i);
                                F--
                            }
                    }, 200)
                } else
                    g()
        }
        function ga(k) {
            if (!b.lazyFetching ||
                    ya(n.visStart, n.visEnd))
                ra();
            else
                k && da()
        }
        function ra() {
            K(n.visStart, n.visEnd)
        }
        function sa(k) {
            J = k;
            da()
        }
        function ha(k) {
            da(k)
        }
        function da(k) {
            na();
            if (j()) {
                n.clearEvents();
                n.renderEvents(J, k);
                n.eventsDirty = false
            }
        }
        function na() {
            m.each(Y, function (k, D) {
                D.eventsDirty = true
            })
        }
        function ua(k, D, Z) {
            n.select(k, D, Z === ma ? true : Z)
        }
        function pa() {
            n && n.unselect()
        }
        function U() {
            S(-1)
        }
        function ca() {
            S(1)
        }
        function ka() {
            gb(r, -1);
            S()
        }
        function qa() {
            gb(r, 1);
            S()
        }
        function G() {
            r = new Date;
            S()
        }
        function p(k, D, Z) {
            if (k instanceof Date)
                r =
                        N(k);
            else
                yb(r, k, D, Z);
            S()
        }
        function L(k, D, Z) {
            k !== ma && gb(r, k);
            D !== ma && hb(r, D);
            Z !== ma && ba(r, Z);
            S()
        }
        function c() {
            return N(r)
        }
        function z() {
            return n
        }
        function H(k, D) {
            if (D === ma)
                return b[k];
            if (k == "height" || k == "contentHeight" || k == "aspectRatio") {
                b[k] = D;
                Q()
            }
        }
        function T(k, D) {
            if (b[k])
                return b[k].apply(D || i, Array.prototype.slice.call(arguments, 2))
        }
        var X = this;
        X.options = b;
        X.render = d;
        X.destroy = l;
        X.refetchEvents = ra;
        X.reportEvents = sa;
        X.reportEventChange = ha;
        X.rerenderEvents = da;
        X.changeView = y;
        X.select = ua;
        X.unselect = pa;
        X.prev = U;
        X.next = ca;
        X.prevYear = ka;
        X.nextYear = qa;
        X.today = G;
        X.gotoDate = p;
        X.incrementDate = L;
        X.formatDate = function (k, D) {
            return Oa(k, D, b)
        };
        X.formatDates = function (k, D, Z) {
            return ib(k, D, Z, b)
        };
        X.getDate = c;
        X.getView = z;
        X.option = H;
        X.trigger = T;
        $b.call(X, b, e);
        var ya = X.isFetchNeeded, K = X.fetchEvents, i = a[0], C, P, E, B, n, Y = {}, W, o, s, v = 0, F = 0, r = new Date, J = [], M;
        yb(r, b.year, b.month, b.date);
        b.droppable && m(document).bind("dragstart", function (k, D) {
            var Z = k.target, ja = m(Z);
            if (!ja.parents(".fc").length) {
                var ia = b.dropAccept;
                if (m.isFunction(ia) ?
                        ia.call(Z, ja) : ja.is(ia)) {
                    M = Z;
                    n.dragStart(M, k, D)
                }
            }
        }).bind("dragstop", function (k, D) {
            if (M) {
                n.dragStop(M, k, D);
                M = null
            }
        })
    }
    function Zb(a, b) {
        function e() {
            q = b.theme ? "ui" : "fc";
            if (b.header)
                return Q = m("<table class='fc-header' style='width:100%'/>").append(m("<tr/>").append(f("left")).append(f("center")).append(f("right")))
        }
        function d() {
            Q.remove()
        }
        function f(u) {
            var fa = m("<td class='fc-header-"+u+"'/>");
            (u = b.header[u]) && m.each(u.split(" "), function (oa) {
                oa > 0 && fa.append("<span class='fc-header-space'/>");
                var ga;
                m.each(this.split(","), function (ra, sa) {
                    if (sa == "title") {
                        fa.append("<span class='fc-header-title'><h2>&nbsp;</h2></span>");
                        ga && ga.addClass(q+"-corner-right");
                        ga = null
                    } else {
                        var ha;
                        if (a[sa])
                            ha = a[sa];
                        else if (Ja[sa])
                            ha = function () {
                                na.removeClass(q+"-state-hover");
                                a.changeView(sa)
                            };
                        if (ha) {
                            ra = b.theme ? jb(b.buttonIcons, sa) : null;
                            var da = jb(b.buttonText, sa), na = m("<span class='fc-button fc-button-"+sa+" "+q+"-state-default'><span class='fc-button-inner'><span class='fc-button-content'>"+(ra ? "<span class='fc-icon-wrap'><span class='ui-icon ui-icon-" +
                                    ra+"'/></span>" : da)+"</span><span class='fc-button-effect'><span></span></span></span></span>");
                            if (na) {
                                na.click(function () {
                                    na.hasClass(q+"-state-disabled") || ha()
                                }).mousedown(function () {
                                    na.not("."+q+"-state-active").not("."+q+"-state-disabled").addClass(q+"-state-down")
                                }).mouseup(function () {
                                    na.removeClass(q+"-state-down")
                                }).hover(function () {
                                    na.not("."+q+"-state-active").not("."+q+"-state-disabled").addClass(q+"-state-hover")
                                }, function () {
                                    na.removeClass(q+"-state-hover").removeClass(q+"-state-down")
                                }).appendTo(fa);
                                ga || na.addClass(q+"-corner-left");
                                ga = na
                            }
                        }
                    }
                });
                ga && ga.addClass(q+"-corner-right")
            });
            return fa
        }
        function g(u) {
            Q.find("h2").html(u)
        }
        function l(u) {
            Q.find("span.fc-button-"+u).addClass(q+"-state-active")
        }
        function j(u) {
            Q.find("span.fc-button-"+u).removeClass(q+"-state-active")
        }
        function t(u) {
            Q.find("span.fc-button-"+u).addClass(q+"-state-disabled")
        }
        function y(u) {
            Q.find("span.fc-button-"+u).removeClass(q+"-state-disabled")
        }
        var S = this;
        S.render = e;
        S.destroy = d;
        S.updateTitle = g;
        S.activateButton = l;
        S.deactivateButton =
                j;
        S.disableButton = t;
        S.enableButton = y;
        var Q = m([]), q
    }
    function $b(a, b) {
        function e(c, z) {
            return!ca || c < ca || z > ka
        }
        function d(c, z) {
            ca = c;
            ka = z;
            L = [];
            c = ++qa;
            G = z = U.length;
            for (var H = 0; H < z; H++)
                f(U[H], c)
        }
        function f(c, z) {
            g(c, function (H) {
                if (z == qa) {
                    if (H) {
                        for (var T = 0; T < H.length; T++) {
                            H[T].source = c;
                            oa(H[T])
                        }
                        L = L.concat(H)
                    }
                    G--;
                    G || ua(L)
                }
            })
        }
        function g(c, z) {
            var H, T = Aa.sourceFetchers, X;
            for (H = 0; H < T.length; H++) {
                X = T[H](c, ca, ka, z);
                if (X === true)
                    return;
                else if (typeof X == "object") {
                    g(X, z);
                    return
                }
            }
            if (H = c.events)
                if (m.isFunction(H)) {
                    u();
                    H(N(ca), N(ka), function (C) {
                        z(C);
                        fa()
                    })
                } else
                    m.isArray(H) ? z(H) : z();
            else if (c.url) {
                var ya = c.success, K = c.error, i = c.complete;
                H = m.extend({}, c.data || {});
                T = Ta(c.startParam, a.startParam);
                X = Ta(c.endParam, a.endParam);
                if (T)
                    H[T] = Math.round(+ca / 1E3);
                if (X)
                    H[X] = Math.round(+ka / 1E3);
                u();
                m.ajax(m.extend({}, ac, c, {data: H, success: function (C) {
                        C = C || [];
                        var P = $a(ya, this, arguments);
                        if (m.isArray(P))
                            C = P;
                        z(C)
                    }, error: function () {
                        $a(K, this, arguments);
                        z()
                    }, complete: function () {
                        $a(i, this, arguments);
                        fa()
                    }}))
            } else
                z()
        }
        function l(c) {
            if (c =
                    j(c)) {
                G++;
                f(c, qa)
            }
        }
        function j(c) {
            if (m.isFunction(c) || m.isArray(c))
                c = {events: c};
            else if (typeof c == "string")
                c = {url: c};
            if (typeof c == "object") {
                ga(c);
                U.push(c);
                return c
            }
        }
        function t(c) {
            U = m.grep(U, function (z) {
                return!ra(z, c)
            });
            L = m.grep(L, function (z) {
                return!ra(z.source, c)
            });
            ua(L)
        }
        function y(c) {
            var z, H = L.length, T, X = na().defaultEventEnd, ya = c.start - c._start, K = c.end ? c.end - (c._end || X(c)) : 0;
            for (z = 0; z < H; z++) {
                T = L[z];
                if (T._id == c._id && T != c) {
                    T.start = new Date(+T.start+ya);
                    T.end = c.end ? T.end ? new Date(+T.end+K) : new Date(+X(T) +
                            K) : null;
                    T.title = c.title;
                    T.url = c.url;
                    T.allDay = c.allDay;
                    T.className = c.className;
                    T.editable = c.editable;
                    T.color = c.color;
                    T.backgroudColor = c.backgroudColor;
                    T.borderColor = c.borderColor;
                    T.textColor = c.textColor;
                    oa(T)
                }
            }
            oa(c);
            ua(L)
        }
        function S(c, z) {
            oa(c);
            if (!c.source) {
                if (z) {
                    pa.events.push(c);
                    c.source = pa
                }
                L.push(c)
            }
            ua(L)
        }
        function Q(c) {
            if (c) {
                if (!m.isFunction(c)) {
                    var z = c+"";
                    c = function (T) {
                        return T._id == z
                    }
                }
                L = m.grep(L, c, true);
                for (H = 0; H < U.length; H++)
                    if (m.isArray(U[H].events))
                        U[H].events = m.grep(U[H].events, c, true)
            } else {
                L =
                [];
                for (var H = 0; H < U.length; H++)
                    if (m.isArray(U[H].events))
                        U[H].events = []
            }
            ua(L)
        }
        function q(c) {
            if (m.isFunction(c))
                return m.grep(L, c);
            else if (c) {
                c += "";
                return m.grep(L, function (z) {
                    return z._id == c
                })
            }
            return L
        }
        function u() {
            p++ || da("loading", null, true)
        }
        function fa() {
            --p || da("loading", null, false)
        }
        function oa(c) {
            var z = c.source || {}, H = Ta(z.ignoreTimezone, a.ignoreTimezone);
            c._id = c._id || (c.id === ma ? "_fc"+bc++ : c.id+"");
            if (c.date) {
                if (!c.start)
                    c.start = c.date;
                delete c.date
            }
            c._start = N(c.start = kb(c.start, H));
            c.end = kb(c.end,
                    H);
            if (c.end && c.end <= c.start)
                c.end = null;
            c._end = c.end ? N(c.end) : null;
            if (c.allDay === ma)
                c.allDay = Ta(z.allDayDefault, a.allDayDefault);
            if (c.className) {
                if (typeof c.className == "string")
                    c.className = c.className.split(/\s+/)
            } else
                c.className = []
        }
        function ga(c) {
            if (c.className) {
                if (typeof c.className == "string")
                    c.className = c.className.split(/\s+/)
            } else
                c.className = [];
            for (var z = Aa.sourceNormalizers, H = 0; H < z.length; H++)
                z[H](c)
        }
        function ra(c, z) {
            return c && z && sa(c) == sa(z)
        }
        function sa(c) {
            return(typeof c == "object" ? c.events ||
                    c.url : "") || c
        }
        var ha = this;
        ha.isFetchNeeded = e;
        ha.fetchEvents = d;
        ha.addEventSource = l;
        ha.removeEventSource = t;
        ha.updateEvent = y;
        ha.renderEvent = S;
        ha.removeEvents = Q;
        ha.clientEvents = q;
        ha.normalizeEvent = oa;
        var da = ha.trigger, na = ha.getView, ua = ha.reportEvents, pa = {events: []}, U = [pa], ca, ka, qa = 0, G = 0, p = 0, L = [];
        for (ha = 0; ha < b.length; ha++)
            j(b[ha])
    }
    function gb(a, b, e) {
        a.setFullYear(a.getFullYear()+b);
        e || Ka(a);
        return a
    }
    function hb(a, b, e) {
        if (+a) {
            b = a.getMonth()+b;
            var d = N(a);
            d.setDate(1);
            d.setMonth(b);
            a.setMonth(b);
            for (e || Ka(a); a.getMonth() !=
                    d.getMonth(); )
                a.setDate(a.getDate()+(a < d ? 1 : -1))
        }
        return a
    }
    function ba(a, b, e) {
        if (+a) {
            b = a.getDate()+b;
            var d = N(a);
            d.setHours(9);
            d.setDate(b);
            a.setDate(b);
            e || Ka(a);
            lb(a, d)
        }
        return a
    }
    function lb(a, b) {
        if (+a)
            for (; a.getDate() != b.getDate(); )
                a.setTime(+a+(a < b ? 1 : -1) * cc)
    }
    function xa(a, b) {
        a.setMinutes(a.getMinutes()+b);
        return a
    }
    function Ka(a) {
        a.setHours(0);
        a.setMinutes(0);
        a.setSeconds(0);
        a.setMilliseconds(0);
        return a
    }
    function N(a, b) {
        if (b)
            return Ka(new Date(+a));
        return new Date(+a)
    }
    function zb() {
        var a = 0, b;
        do
            b = new Date(1970,
                    a++, 1);
        while (b.getHours());
        return b
    }
    function Fa(a, b, e) {
        for (b = b || 1; !a.getDay() || e && a.getDay() == 1 || !e && a.getDay() == 6; )
            ba(a, b);
        return a
    }
    function Ca(a, b) {
        return Math.round((N(a, true) - N(b, true)) / Ab)
    }
    function yb(a, b, e, d) {
        if (b !== ma && b != a.getFullYear()) {
            a.setDate(1);
            a.setMonth(0);
            a.setFullYear(b)
        }
        if (e !== ma && e != a.getMonth()) {
            a.setDate(1);
            a.setMonth(e)
        }
        d !== ma && a.setDate(d)
    }
    function kb(a, b) {
        if (typeof a == "object")
            return a;
        if (typeof a == "number")
            return new Date(a * 1E3);
        if (typeof a == "string") {
            if (a.match(/^\d+(\.\d+)?$/))
                return new Date(parseFloat(a) *
                        1E3);
            if (b === ma)
                b = true;
            return Bb(a, b) || (a ? new Date(a) : null)
        }
        return null
    }
    function Bb(a, b) {
        a = a.match(/^([0-9]{4})(-([0-9]{2})(-([0-9]{2})([T ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2})(:?([0-9]{2}))?))?)?)?)?$/);
        if (!a)
            return null;
        var e = new Date(a[1], 0, 1);
        if (b || !a[13]) {
            b = new Date(a[1], 0, 1, 9, 0);
            if (a[3]) {
                e.setMonth(a[3] - 1);
                b.setMonth(a[3] - 1)
            }
            if (a[5]) {
                e.setDate(a[5]);
                b.setDate(a[5])
            }
            lb(e, b);
            a[7] && e.setHours(a[7]);
            a[8] && e.setMinutes(a[8]);
            a[10] && e.setSeconds(a[10]);
            a[12] && e.setMilliseconds(Number("0." +
                    a[12]) * 1E3);
            lb(e, b)
        } else {
            e.setUTCFullYear(a[1], a[3] ? a[3] - 1 : 0, a[5] || 1);
            e.setUTCHours(a[7] || 0, a[8] || 0, a[10] || 0, a[12] ? Number("0."+a[12]) * 1E3 : 0);
            if (a[14]) {
                b = Number(a[16]) * 60+(a[18] ? Number(a[18]) : 0);
                b *= a[15] == "-" ? 1 : -1;
                e = new Date(+e+b * 60 * 1E3)
            }
        }
        return e
    }
    function mb(a) {
        if (typeof a == "number")
            return a * 60;
        if (typeof a == "object")
            return a.getHours() * 60+a.getMinutes();
        if (a = a.match(/(\d+)(?::(\d+))?\s*(\w+)?/)) {
            var b = parseInt(a[1], 10);
            if (a[3]) {
                b %= 12;
                if (a[3].toLowerCase().charAt(0) == "p")
                    b += 12
            }
            return b * 60+(a[2] ? parseInt(a[2],
                    10) : 0)
        }
    }
    function Oa(a, b, e) {
        return ib(a, null, b, e)
    }
    function ib(a, b, e, d) {
        d = d || Ya;
        var f = a, g = b, l, j = e.length, t, y, S, Q = "";
        for (l = 0; l < j; l++) {
            t = e.charAt(l);
            if (t == "'")
                for (y = l+1; y < j; y++) {
                    if (e.charAt(y) == "'") {
                        if (f) {
                            Q += y == l+1 ? "'" : e.substring(l+1, y);
                            l = y
                        }
                        break
                    }
                }
            else if (t == "(")
                for (y = l+1; y < j; y++) {
                    if (e.charAt(y) == ")") {
                        l = Oa(f, e.substring(l+1, y), d);
                        if (parseInt(l.replace(/\D/, ""), 10))
                            Q += l;
                        l = y;
                        break
                    }
                }
            else if (t == "[")
                for (y = l+1; y < j; y++) {
                    if (e.charAt(y) == "]") {
                        t = e.substring(l+1, y);
                        l = Oa(f, t, d);
                        if (l != Oa(g, t, d))
                            Q += l;
                        l = y;
                        break
                    }
                }
            else if (t ==
                    "{") {
                f = b;
                g = a
            } else if (t == "}") {
                f = a;
                g = b
            } else {
                for (y = j; y > l; y--)
                    if (S = dc[e.substring(l, y)]) {
                        if (f)
                            Q += S(f, d);
                        l = y - 1;
                        break
                    }
                if (y == l)
                    if (f)
                        Q += t
            }
        }
        return Q
    }
    function Ua(a) {
        return a.end ? ec(a.end, a.allDay) : ba(N(a.start), 1)
    }
    function ec(a, b) {
        a = N(a);
        return b || a.getHours() || a.getMinutes() ? ba(a, 1) : Ka(a)
    }
    function fc(a, b) {
        return(b.msLength - a.msLength) * 100+(a.event.start - b.event.start)
    }
    function Cb(a, b) {
        return a.end > b.start && a.start < b.end
    }
    function nb(a, b, e, d) {
        var f = [], g, l = a.length, j, t, y, S, Q;
        for (g = 0; g < l; g++) {
            j = a[g];
            t = j.start;
            y = b[g];
            if (y > e && t < d) {
                if (t < e) {
                    t = N(e);
                    S = false
                } else {
                    t = t;
                    S = true
                }
                if (y > d) {
                    y = N(d);
                    Q = false
                } else {
                    y = y;
                    Q = true
                }
                f.push({event: j, start: t, end: y, isStart: S, isEnd: Q, msLength: y - t})
            }
        }
        return f.sort(fc)
    }
    function ob(a) {
        var b = [], e, d = a.length, f, g, l, j;
        for (e = 0; e < d; e++) {
            f = a[e];
            for (g = 0; ; ) {
                l = false;
                if (b[g])
                    for (j = 0; j < b[g].length; j++)
                        if (Cb(b[g][j], f)) {
                            l = true;
                            break
                        }
                if (l)
                    g++;
                else
                    break
            }
            if (b[g])
                b[g].push(f);
            else
                b[g] = [f]
        }
        return b
    }
    function Db(a, b, e) {
        a.unbind("mouseover").mouseover(function (d) {
            for (var f = d.target, g; f != this; ) {
                g = f;
                f = f.parentNode
            }
            if ((f =
                    g._fci) !== ma) {
                g._fci = ma;
                g = b[f];
                e(g.event, g.element, g);
                m(d.target).trigger(d)
            }
            d.stopPropagation()
        })
    }
    function Va(a, b, e) {
        for (var d = 0, f; d < a.length; d++) {
            f = m(a[d]);
            f.width(Math.max(0, b - pb(f, e)))
        }
    }
    function Eb(a, b, e) {
        for (var d = 0, f; d < a.length; d++) {
            f = m(a[d]);
            f.height(Math.max(0, b - Sa(f, e)))
        }
    }
    function pb(a, b) {
        return gc(a)+hc(a)+(b ? ic(a) : 0)
    }
    function gc(a) {
        return(parseFloat(m.css(a[0], "paddingLeft", true)) || 0)+(parseFloat(m.css(a[0], "paddingRight", true)) || 0)
    }
    function ic(a) {
        return(parseFloat(m.css(a[0], "marginLeft",
                true)) || 0)+(parseFloat(m.css(a[0], "marginRight", true)) || 0)
    }
    function hc(a) {
        return(parseFloat(m.css(a[0], "borderLeftWidth", true)) || 0)+(parseFloat(m.css(a[0], "borderRightWidth", true)) || 0)
    }
    function Sa(a, b) {
        return jc(a)+kc(a)+(b ? Fb(a) : 0)
    }
    function jc(a) {
        return(parseFloat(m.css(a[0], "paddingTop", true)) || 0)+(parseFloat(m.css(a[0], "paddingBottom", true)) || 0)
    }
    function Fb(a) {
        return(parseFloat(m.css(a[0], "marginTop", true)) || 0)+(parseFloat(m.css(a[0], "marginBottom", true)) || 0)
    }
    function kc(a) {
        return(parseFloat(m.css(a[0],
                "borderTopWidth", true)) || 0)+(parseFloat(m.css(a[0], "borderBottomWidth", true)) || 0)
    }
    function Za(a, b) {
        b = typeof b == "number" ? b+"px" : b;
        a.each(function (e, d) {
            d.style.cssText += ";min-height:"+b+";_height:"+b
        })
    }
    function xb() {
    }
    function Gb(a, b) {
        return a - b
    }
    function Hb(a) {
        return Math.max.apply(Math, a)
    }
    function Pa(a) {
        return(a < 10 ? "0" : "")+a
    }
    function jb(a, b) {
        if (a[b] !== ma)
            return a[b];
        b = b.split(/(?=[A-Z])/);
        for (var e = b.length - 1, d; e >= 0; e--) {
            d = a[b[e].toLowerCase()];
            if (d !== ma)
                return d
        }
        return a[""]
    }
    function Qa(a) {
        return a.replace(/&/g,
                "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&#039;").replace(/"/g, "&quot;").replace(/\n/g, "<br />")
    }
    function Ib(a) {
        return a.id+"/"+a.className+"/"+a.style.cssText.replace(/(^|;)\s*(top|left|width|height)\s*:[^;]*/ig, "")
    }
    function qb(a) {
        a.attr("unselectable", "on").css("MozUserSelect", "none").bind("selectstart.ui", function () {
            return false
        })
    }
    function ab(a) {
        a.children().removeClass("fc-first fc-last").filter(":first-child").addClass("fc-first").end().filter(":last-child").addClass("fc-last")
    }
    function rb(a, b) {
        a.each(function (e, d) {
            d.className = d.className.replace(/^fc-\w*/, "fc-"+lc[b.getDay()])
        })
    }
    function Jb(a, b) {
        var e = a.source || {}, d = a.color, f = e.color, g = b("eventColor"), l = a.backgroundColor || d || e.backgroundColor || f || b("eventBackgroundColor") || g;
        d = a.borderColor || d || e.borderColor || f || b("eventBorderColor") || g;
        a = a.textColor || e.textColor || b("eventTextColor");
        b = [];
        l && b.push("background-color:"+l);
        d && b.push("border-color:"+d);
        a && b.push("color:"+a);
        return b.join(";")
    }
    function $a(a, b, e) {
        if (m.isFunction(a))
            a =
                    [a];
        if (a) {
            var d, f;
            for (d = 0; d < a.length; d++)
                f = a[d].apply(b, e) || f;
            return f
        }
    }
    function Ta() {
        for (var a = 0; a < arguments.length; a++)
            if (arguments[a] !== ma)
                return arguments[a]
    }
    function mc(a, b) {
        function e(j, t) {
            if (t) {
                hb(j, t);
                j.setDate(1)
            }
            j = N(j, true);
            j.setDate(1);
            t = hb(N(j), 1);
            var y = N(j), S = N(t), Q = f("firstDay"), q = f("weekends") ? 0 : 1;
            if (q) {
                Fa(y);
                Fa(S, -1, true)
            }
            ba(y, -((y.getDay() - Math.max(Q, q)+7) % 7));
            ba(S, (7 - S.getDay()+Math.max(Q, q)) % 7);
            Q = Math.round((S - y) / (Ab * 7));
            if (f("weekMode") == "fixed") {
                ba(S, (6 - Q) * 7);
                Q = 6
            }
            d.title = l(j,
                    f("titleFormat"));
            d.start = j;
            d.end = t;
            d.visStart = y;
            d.visEnd = S;
            g(6, Q, q ? 5 : 7, true)
        }
        var d = this;
        d.render = e;
        sb.call(d, a, b, "month");
        var f = d.opt, g = d.renderBasic, l = b.formatDate
    }
    function nc(a, b) {
        function e(j, t) {
            t && ba(j, t * 7);
            j = ba(N(j), -((j.getDay() - f("firstDay")+7) % 7));
            t = ba(N(j), 7);
            var y = N(j), S = N(t), Q = f("weekends");
            if (!Q) {
                Fa(y);
                Fa(S, -1, true)
            }
            d.title = l(y, ba(N(S), -1), f("titleFormat"));
            d.start = j;
            d.end = t;
            d.visStart = y;
            d.visEnd = S;
            g(1, 1, Q ? 7 : 5, false)
        }
        var d = this;
        d.render = e;
        sb.call(d, a, b, "basicWeek");
        var f = d.opt, g = d.renderBasic,
                l = b.formatDates
    }
    function oc(a, b) {
        function e(j, t) {
            if (t) {
                ba(j, t);
                f("weekends") || Fa(j, t < 0 ? -1 : 1)
            }
            d.title = l(j, f("titleFormat"));
            d.start = d.visStart = N(j, true);
            d.end = d.visEnd = ba(N(d.start), 1);
            g(1, 1, 1, false)
        }
        var d = this;
        d.render = e;
        sb.call(d, a, b, "basicDay");
        var f = d.opt, g = d.renderBasic, l = b.formatDate
    }
    function sb(a, b, e) {
        function d(w, I, R, V) {
            v = I;
            F = R;
            f();
            (I = !C) ? g(w, V) : z();
            l(I)
        }
        function f() {
            if (k = L("isRTL")) {
                D = -1;
                Z = F - 1
            } else {
                D = 1;
                Z = 0
            }
            ja = L("firstDay");
            ia = L("weekends") ? 0 : 1;
            la = L("theme") ? "ui" : "fc";
            $ = L("columnFormat")
        }
        function g(w,
                I) {
            var R, V = la+"-widget-header", ea = la+"-widget-content", aa;
            R = "<table class='fc-border-separate' style='width:100%' cellspacing='0'><thead><tr>";
            for (aa = 0; aa < F; aa++)
                R += "<th class='fc- "+V+"'/>";
            R += "</tr></thead><tbody>";
            for (aa = 0; aa < w; aa++) {
                R += "<tr class='fc-week"+aa+"'>";
                for (V = 0; V < F; V++)
                    R += "<td class='fc- "+ea+" fc-day"+(aa * F+V)+"'><div>"+(I ? "<div class='fc-day-number'/>" : "")+"<div class='fc-day-content'><div style='position:relative'>&nbsp;</div></div></div></td>";
                R += "</tr>"
            }
            R += "</tbody></table>";
            w =
                    m(R).appendTo(a);
            K = w.find("thead");
            i = K.find("th");
            C = w.find("tbody");
            P = C.find("tr");
            E = C.find("td");
            B = E.filter(":first-child");
            n = P.eq(0).find("div.fc-day-content div");
            ab(K.add(K.find("tr")));
            ab(P);
            P.eq(0).addClass("fc-first");
            y(E);
            Y = m("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(a)
        }
        function l(w) {
            var I = w || v == 1, R = p.start.getMonth(), V = Ka(new Date), ea, aa, va;
            I && i.each(function (wa, Ga) {
                ea = m(Ga);
                aa = ca(wa);
                ea.html(ya(aa, $));
                rb(ea, aa)
            });
            E.each(function (wa, Ga) {
                ea = m(Ga);
                aa = ca(wa);
                aa.getMonth() ==
                        R ? ea.removeClass("fc-other-month") : ea.addClass("fc-other-month");
                +aa == +V ? ea.addClass(la+"-state-highlight fc-today") : ea.removeClass(la+"-state-highlight fc-today");
                ea.find("div.fc-day-number").text(aa.getDate());
                I && rb(ea, aa)
            });
            P.each(function (wa, Ga) {
                va = m(Ga);
                if (wa < v) {
                    va.show();
                    wa == v - 1 ? va.addClass("fc-last") : va.removeClass("fc-last")
                } else
                    va.hide()
            })
        }
        function j(w) {
            o = w;
            w = o - K.height();
            var I, R, V;
            if (L("weekMode") == "variable")
                I = R = Math.floor(w / (v == 1 ? 2 : 6));
            else {
                I = Math.floor(w / v);
                R = w - I * (v - 1)
            }
            B.each(function (ea,
                    aa) {
                if (ea < v) {
                    V = m(aa);
                    Za(V.find("> div"), (ea == v - 1 ? R : I) - Sa(V))
                }
            })
        }
        function t(w) {
            W = w;
            M.clear();
            s = Math.floor(W / F);
            Va(i.slice(0, -1), s)
        }
        function y(w) {
            w.click(S).mousedown(X)
        }
        function S(w) {
            if (!L("selectable")) {
                var I = parseInt(this.className.match(/fc\-day(\d+)/)[1]);
                I = ca(I);
                c("dayClick", this, I, true, w)
            }
        }
        function Q(w, I, R) {
            R && r.build();
            R = N(p.visStart);
            for (var V = ba(N(R), F), ea = 0; ea < v; ea++) {
                var aa = new Date(Math.max(R, w)), va = new Date(Math.min(V, I));
                if (aa < va) {
                    var wa;
                    if (k) {
                        wa = Ca(va, R) * D+Z+1;
                        aa = Ca(aa, R) * D+Z+1
                    } else {
                        wa =
                                Ca(aa, R);
                        aa = Ca(va, R)
                    }
                    y(q(ea, wa, ea, aa - 1))
                }
                ba(R, 7);
                ba(V, 7)
            }
        }
        function q(w, I, R, V) {
            w = r.rect(w, I, R, V, a);
            return H(w, a)
        }
        function u(w) {
            return N(w)
        }
        function fa(w, I) {
            Q(w, ba(N(I), 1), true)
        }
        function oa() {
            T()
        }
        function ga(w, I, R) {
            var V = ua(w);
            c("dayClick", E[V.row * F+V.col], w, I, R)
        }
        function ra(w, I) {
            J.start(function (R) {
                T();
                R && q(R.row, R.col, R.row, R.col)
            }, I)
        }
        function sa(w, I, R) {
            var V = J.stop();
            T();
            if (V) {
                V = pa(V);
                c("drop", w, V, true, I, R)
            }
        }
        function ha(w) {
            return N(w.start)
        }
        function da(w) {
            return M.left(w)
        }
        function na(w) {
            return M.right(w)
        }
        function ua(w) {
            return{row: Math.floor(Ca(w, p.visStart) / 7), col: ka(w.getDay())}
        }
        function pa(w) {
            return U(w.row, w.col)
        }
        function U(w, I) {
            return ba(N(p.visStart), w * 7+I * D+Z)
        }
        function ca(w) {
            return U(Math.floor(w / F), w % F)
        }
        function ka(w) {
            return(w - Math.max(ja, ia)+F) % F * D+Z
        }
        function qa(w) {
            return P.eq(w)
        }
        function G() {
            return{left: 0, right: W}
        }
        var p = this;
        p.renderBasic = d;
        p.setHeight = j;
        p.setWidth = t;
        p.renderDayOverlay = Q;
        p.defaultSelectionEnd = u;
        p.renderSelection = fa;
        p.clearSelection = oa;
        p.reportDayClick = ga;
        p.dragStart = ra;
        p.dragStop =
                sa;
        p.defaultEventEnd = ha;
        p.getHoverListener = function () {
            return J
        };
        p.colContentLeft = da;
        p.colContentRight = na;
        p.dayOfWeekCol = ka;
        p.dateCell = ua;
        p.cellDate = pa;
        p.cellIsAllDay = function () {
            return true
        };
        p.allDayRow = qa;
        p.allDayBounds = G;
        p.getRowCnt = function () {
            return v
        };
        p.getColCnt = function () {
            return F
        };
        p.getColWidth = function () {
            return s
        };
        p.getDaySegmentContainer = function () {
            return Y
        };
        Kb.call(p, a, b, e);
        Lb.call(p);
        Mb.call(p);
        pc.call(p);
        var L = p.opt, c = p.trigger, z = p.clearEvents, H = p.renderOverlay, T = p.clearOverlays, X = p.daySelectionMousedown,
                ya = b.formatDate, K, i, C, P, E, B, n, Y, W, o, s, v, F, r, J, M, k, D, Z, ja, ia, la, $;
        qb(a.addClass("fc-grid"));
        r = new Nb(function (w, I) {
            var R, V, ea;
            i.each(function (aa, va) {
                R = m(va);
                V = R.offset().left;
                if (aa)
                    ea[1] = V;
                ea = [V];
                I[aa] = ea
            });
            ea[1] = V+R.outerWidth();
            P.each(function (aa, va) {
                if (aa < v) {
                    R = m(va);
                    V = R.offset().top;
                    if (aa)
                        ea[1] = V;
                    ea = [V];
                    w[aa] = ea
                }
            });
            ea[1] = V+R.outerHeight()
        });
        J = new Ob(r);
        M = new Pb(function (w) {
            return n.eq(w)
        })
    }
    function pc() {
        function a(U, ca) {
            S(U);
            ua(e(U), ca)
        }
        function b() {
            Q();
            ga().empty()
        }
        function e(U) {
            var ca = da(), ka =
                    na(), qa = N(g.visStart);
            ka = ba(N(qa), ka);
            var G = m.map(U, Ua), p, L, c, z, H, T, X = [];
            for (p = 0; p < ca; p++) {
                L = ob(nb(U, G, qa, ka));
                for (c = 0; c < L.length; c++) {
                    z = L[c];
                    for (H = 0; H < z.length; H++) {
                        T = z[H];
                        T.row = p;
                        T.level = c;
                        X.push(T)
                    }
                }
                ba(qa, 7);
                ba(ka, 7)
            }
            return X
        }
        function d(U, ca, ka) {
            t(U) && f(U, ca);
            ka.isEnd && y(U) && pa(U, ca, ka);
            q(U, ca)
        }
        function f(U, ca) {
            var ka = ra(), qa;
            ca.draggable({zIndex: 9, delay: 50, opacity: l("dragOpacity"), revertDuration: l("dragRevertDuration"), start: function (G, p) {
                    j("eventDragStart", ca, U, G, p);
                    fa(U, ca);
                    ka.start(function (L,
                            c, z, H) {
                        ca.draggable("option", "revert", !L || !z && !H);
                        ha();
                        if (L) {
                            qa = z * 7+H * (l("isRTL") ? -1 : 1);
                            sa(ba(N(U.start), qa), ba(Ua(U), qa))
                        } else
                            qa = 0
                    }, G, "drag")
                }, stop: function (G, p) {
                    ka.stop();
                    ha();
                    j("eventDragStop", ca, U, G, p);
                    if (qa)
                        oa(this, U, qa, 0, U.allDay, G, p);
                    else {
                        ca.css("filter", "");
                        u(U, ca)
                    }
                }})
        }
        var g = this;
        g.renderEvents = a;
        g.compileDaySegs = e;
        g.clearEvents = b;
        g.bindDaySeg = d;
        Qb.call(g);
        var l = g.opt, j = g.trigger, t = g.isEventDraggable, y = g.isEventResizable, S = g.reportEvents, Q = g.reportEventClear, q = g.eventElementHandlers, u = g.showEvents,
                fa = g.hideEvents, oa = g.eventDrop, ga = g.getDaySegmentContainer, ra = g.getHoverListener, sa = g.renderDayOverlay, ha = g.clearOverlays, da = g.getRowCnt, na = g.getColCnt, ua = g.renderDaySegs, pa = g.resizableDayEvent
    }
    function qc(a, b) {
        function e(j, t) {
            t && ba(j, t * 7);
            j = ba(N(j), -((j.getDay() - f("firstDay")+7) % 7));
            t = ba(N(j), 7);
            var y = N(j), S = N(t), Q = f("weekends");
            if (!Q) {
                Fa(y);
                Fa(S, -1, true)
            }
            d.title = l(y, ba(N(S), -1), f("titleFormat"));
            d.start = j;
            d.end = t;
            d.visStart = y;
            d.visEnd = S;
            g(Q ? 7 : 5)
        }
        var d = this;
        d.render = e;
        Rb.call(d, a, b, "agendaWeek");
        var f = d.opt, g = d.renderAgenda, l = b.formatDates
    }
    function rc(a, b) {
        function e(j, t) {
            if (t) {
                ba(j, t);
                f("weekends") || Fa(j, t < 0 ? -1 : 1)
            }
            t = N(j, true);
            var y = ba(N(t), 1);
            d.title = l(j, f("titleFormat"));
            d.start = d.visStart = t;
            d.end = d.visEnd = y;
            g(1)
        }
        var d = this;
        d.render = e;
        Rb.call(d, a, b, "agendaDay");
        var f = d.opt, g = d.renderAgenda, l = b.formatDate
    }
    function Rb(a, b, e) {
        function d(h) {
            Ba = h;
            f();
            v ? P() : g();
            l()
        }
        function f() {
            Wa = i("theme") ? "ui" : "fc";
            Sb = i("weekends") ? 0 : 1;
            Tb = i("firstDay");
            if (Ub = i("isRTL")) {
                Ha = -1;
                Ia = Ba - 1
            } else {
                Ha = 1;
                Ia = 0
            }
            La = mb(i("minTime"));
            bb = mb(i("maxTime"));
            Vb = i("columnFormat")
        }
        function g() {
            var h = Wa+"-widget-header", O = Wa+"-widget-content", x, A, ta, za, Da, Ea = i("slotMinutes") % 15 == 0;
            x = "<table style='width:100%' class='fc-agenda-days fc-border-separate' cellspacing='0'><thead><tr><th class='fc-agenda-axis "+h+"'>&nbsp;</th>";
            for (A = 0; A < Ba; A++)
                x += "<th class='fc- fc-col"+A+" "+h+"'/>";
            x += "<th class='fc-agenda-gutter "+h+"'>&nbsp;</th></tr></thead><tbody><tr><th class='fc-agenda-axis "+h+"'>&nbsp;</th>";
            for (A = 0; A < Ba; A++)
                x += "<td class='fc- fc-col" +
                        A+" "+O+"'><div><div class='fc-day-content'><div style='position:relative'>&nbsp;</div></div></div></td>";
            x += "<td class='fc-agenda-gutter "+O+"'>&nbsp;</td></tr></tbody></table>";
            v = m(x).appendTo(a);
            F = v.find("thead");
            r = F.find("th").slice(1, -1);
            J = v.find("tbody");
            M = J.find("td").slice(0, -1);
            k = M.find("div.fc-day-content div");
            D = M.eq(0);
            Z = D.find("> div");
            ab(F.add(F.find("tr")));
            ab(J.add(J.find("tr")));
            aa = F.find("th:first");
            va = v.find(".fc-agenda-gutter");
            ja = m("<div style='position:absolute;z-index:2;left:0;width:100%'/>").appendTo(a);
            if (i("allDaySlot")) {
                ia = m("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(ja);
                x = "<table style='width:100%' class='fc-agenda-allday' cellspacing='0'><tr><th class='"+h+" fc-agenda-axis'>"+i("allDayText")+"</th><td><div class='fc-day-content'><div style='position:relative'/></div></td><th class='"+h+" fc-agenda-gutter'>&nbsp;</th></tr></table>";
                la = m(x).appendTo(ja);
                $ = la.find("tr");
                q($.find("td"));
                aa = aa.add(la.find("th:first"));
                va = va.add(la.find("th.fc-agenda-gutter"));
                ja.append("<div class='fc-agenda-divider " +
                        h+"'><div class='fc-agenda-divider-inner'/></div>")
            } else
                ia = m([]);
            w = m("<div style='position:absolute;width:100%;overflow-x:hidden;overflow-y:auto'/>").appendTo(ja);
            I = m("<div style='position:relative;width:100%;overflow:hidden'/>").appendTo(w);
            R = m("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(I);
            x = "<table class='fc-agenda-slots' style='width:100%' cellspacing='0'><tbody>";
            ta = zb();
            za = xa(N(ta), bb);
            xa(ta, La);
            for (A = tb = 0; ta < za; A++) {
                Da = ta.getMinutes();
                x += "<tr class='fc-slot"+A+" " +
                        (!Da ? "" : "fc-minor")+"'><th class='fc-agenda-axis "+h+"'>"+(!Ea || !Da ? s(ta, i("axisFormat")) : "&nbsp;")+"</th><td class='"+O+"'><div style='position:relative'>&nbsp;</div></td></tr>";
                xa(ta, i("slotMinutes"));
                tb++
            }
            x += "</tbody></table>";
            V = m(x).appendTo(I);
            ea = V.find("div:first");
            u(V.find("td"));
            aa = aa.add(V.find("th:first"))
        }
        function l() {
            var h, O, x, A, ta = Ka(new Date);
            for (h = 0; h < Ba; h++) {
                A = ua(h);
                O = r.eq(h);
                O.html(s(A, Vb));
                x = M.eq(h);
                +A == +ta ? x.addClass(Wa+"-state-highlight fc-today") : x.removeClass(Wa+"-state-highlight fc-today");
                rb(O.add(x), A)
            }
        }
        function j(h, O) {
            if (h === ma)
                h = Wb;
            Wb = h;
            ub = {};
            var x = J.position().top, A = w.position().top;
            h = Math.min(h - x, V.height()+A+1);
            Z.height(h - Sa(D));
            ja.css("top", x);
            w.height(h - A - 1);
            Xa = ea.height()+1;
            O && y()
        }
        function t(h) {
            Ga = h;
            cb.clear();
            Ma = 0;
            Va(aa.width("").each(function (O, x) {
                Ma = Math.max(Ma, m(x).outerWidth())
            }), Ma);
            h = w[0].clientWidth;
            if (vb = w.width() - h) {
                Va(va, vb);
                va.show().prev().removeClass("fc-last")
            } else
                va.hide().prev().addClass("fc-last");
            db = Math.floor((h - Ma) / Ba);
            Va(r.slice(0, -1), db)
        }
        function y() {
            function h() {
                w.scrollTop(A)
            }
            var O = zb(), x = N(O);
            x.setHours(i("firstHour"));
            var A = ca(O, x)+1;
            h();
            setTimeout(h, 0)
        }
        function S() {
            Xb = w.scrollTop()
        }
        function Q() {
            w.scrollTop(Xb)
        }
        function q(h) {
            h.click(fa).mousedown(W)
        }
        function u(h) {
            h.click(fa).mousedown(H)
        }
        function fa(h) {
            if (!i("selectable")) {
                var O = Math.min(Ba - 1, Math.floor((h.pageX - v.offset().left - Ma) / db)), x = ua(O), A = this.parentNode.className.match(/fc-slot(\d+)/);
                if (A) {
                    A = parseInt(A[1]) * i("slotMinutes");
                    var ta = Math.floor(A / 60);
                    x.setHours(ta);
                    x.setMinutes(A % 60+La);
                    C("dayClick", M[O], x, false,
                            h)
                } else
                    C("dayClick", M[O], x, true, h)
            }
        }
        function oa(h, O, x) {
            x && Na.build();
            var A = N(K.visStart);
            if (Ub) {
                x = Ca(O, A) * Ha+Ia+1;
                h = Ca(h, A) * Ha+Ia+1
            } else {
                x = Ca(h, A);
                h = Ca(O, A)
            }
            x = Math.max(0, x);
            h = Math.min(Ba, h);
            x < h && q(ga(0, x, 0, h - 1))
        }
        function ga(h, O, x, A) {
            h = Na.rect(h, O, x, A, ja);
            return E(h, ja)
        }
        function ra(h, O) {
            for (var x = N(K.visStart), A = ba(N(x), 1), ta = 0; ta < Ba; ta++) {
                var za = new Date(Math.max(x, h)), Da = new Date(Math.min(A, O));
                if (za < Da) {
                    var Ea = ta * Ha+Ia;
                    Ea = Na.rect(0, Ea, 0, Ea, I);
                    za = ca(x, za);
                    Da = ca(x, Da);
                    Ea.top = za;
                    Ea.height = Da - za;
                    u(E(Ea,
                            I))
                }
                ba(x, 1);
                ba(A, 1)
            }
        }
        function sa(h) {
            return cb.left(h)
        }
        function ha(h) {
            return cb.right(h)
        }
        function da(h) {
            return{row: Math.floor(Ca(h, K.visStart) / 7), col: U(h.getDay())}
        }
        function na(h) {
            var O = ua(h.col);
            h = h.row;
            i("allDaySlot") && h--;
            h >= 0 && xa(O, La+h * i("slotMinutes"));
            return O
        }
        function ua(h) {
            return ba(N(K.visStart), h * Ha+Ia)
        }
        function pa(h) {
            return i("allDaySlot") && !h.row
        }
        function U(h) {
            return(h - Math.max(Tb, Sb)+Ba) % Ba * Ha+Ia
        }
        function ca(h, O) {
            h = N(h, true);
            if (O < xa(N(h), La))
                return 0;
            if (O >= xa(N(h), bb))
                return V.height();
            h = i("slotMinutes");
            O = O.getHours() * 60+O.getMinutes() - La;
            var x = Math.floor(O / h), A = ub[x];
            if (A === ma)
                A = ub[x] = V.find("tr:eq("+x+") td div")[0].offsetTop;
            return Math.max(0, Math.round(A - 1+Xa * (O % h / h)))
        }
        function ka() {
            return{left: Ma, right: Ga - vb}
        }
        function qa() {
            return $
        }
        function G(h) {
            var O = N(h.start);
            if (h.allDay)
                return O;
            return xa(O, i("defaultEventMinutes"))
        }
        function p(h, O) {
            if (O)
                return N(h);
            return xa(N(h), i("slotMinutes"))
        }
        function L(h, O, x) {
            if (x)
                i("allDaySlot") && oa(h, ba(N(O), 1), true);
            else
                c(h, O)
        }
        function c(h, O) {
            var x =
                    i("selectHelper");
            Na.build();
            if (x) {
                var A = Ca(h, K.visStart) * Ha+Ia;
                if (A >= 0 && A < Ba) {
                    A = Na.rect(0, A, 0, A, I);
                    var ta = ca(h, h), za = ca(h, O);
                    if (za > ta) {
                        A.top = ta;
                        A.height = za - ta;
                        A.left += 2;
                        A.width -= 5;
                        if (m.isFunction(x)) {
                            if (h = x(h, O)) {
                                A.position = "absolute";
                                A.zIndex = 8;
                                wa = m(h).css(A).appendTo(I)
                            }
                        } else {
                            A.isStart = true;
                            A.isEnd = true;
                            wa = m(o({title: "", start: h, end: O, className: ["fc-select-helper"], editable: false}, A));
                            wa.css("opacity", i("dragOpacity"))
                        }
                        if (wa) {
                            u(wa);
                            I.append(wa);
                            Va(wa, A.width, true);
                            Eb(wa, A.height, true)
                        }
                    }
                }
            } else
                ra(h,
                        O)
        }
        function z() {
            B();
            if (wa) {
                wa.remove();
                wa = null
            }
        }
        function H(h) {
            if (h.which == 1 && i("selectable")) {
                Y(h);
                var O;
                Ra.start(function (x, A) {
                    z();
                    if (x && x.col == A.col && !pa(x)) {
                        A = na(A);
                        x = na(x);
                        O = [A, xa(N(A), i("slotMinutes")), x, xa(N(x), i("slotMinutes"))].sort(Gb);
                        c(O[0], O[3])
                    } else
                        O = null
                }, h);
                m(document).one("mouseup", function (x) {
                    Ra.stop();
                    if (O) {
                        +O[0] == +O[1] && T(O[0], false, x);
                        n(O[0], O[3], false, x)
                    }
                })
            }
        }
        function T(h, O, x) {
            C("dayClick", M[U(h.getDay())], h, O, x)
        }
        function X(h, O) {
            Ra.start(function (x) {
                B();
                if (x)
                    if (pa(x))
                        ga(x.row,
                                x.col, x.row, x.col);
                    else {
                        x = na(x);
                        var A = xa(N(x), i("defaultEventMinutes"));
                        ra(x, A)
                    }
            }, O)
        }
        function ya(h, O, x) {
            var A = Ra.stop();
            B();
            A && C("drop", h, na(A), pa(A), O, x)
        }
        var K = this;
        K.renderAgenda = d;
        K.setWidth = t;
        K.setHeight = j;
        K.beforeHide = S;
        K.afterShow = Q;
        K.defaultEventEnd = G;
        K.timePosition = ca;
        K.dayOfWeekCol = U;
        K.dateCell = da;
        K.cellDate = na;
        K.cellIsAllDay = pa;
        K.allDayRow = qa;
        K.allDayBounds = ka;
        K.getHoverListener = function () {
            return Ra
        };
        K.colContentLeft = sa;
        K.colContentRight = ha;
        K.getDaySegmentContainer = function () {
            return ia
        };
        K.getSlotSegmentContainer = function () {
            return R
        };
        K.getMinMinute = function () {
            return La
        };
        K.getMaxMinute = function () {
            return bb
        };
        K.getBodyContent = function () {
            return I
        };
        K.getRowCnt = function () {
            return 1
        };
        K.getColCnt = function () {
            return Ba
        };
        K.getColWidth = function () {
            return db
        };
        K.getSlotHeight = function () {
            return Xa
        };
        K.defaultSelectionEnd = p;
        K.renderDayOverlay = oa;
        K.renderSelection = L;
        K.clearSelection = z;
        K.reportDayClick = T;
        K.dragStart = X;
        K.dragStop = ya;
        Kb.call(K, a, b, e);
        Lb.call(K);
        Mb.call(K);
        sc.call(K);
        var i = K.opt, C = K.trigger,
                P = K.clearEvents, E = K.renderOverlay, B = K.clearOverlays, n = K.reportSelection, Y = K.unselect, W = K.daySelectionMousedown, o = K.slotSegHtml, s = b.formatDate, v, F, r, J, M, k, D, Z, ja, ia, la, $, w, I, R, V, ea, aa, va, wa, Ga, Wb, Ma, db, vb, Xa, Xb, Ba, tb, Na, Ra, cb, ub = {}, Wa, Tb, Sb, Ub, Ha, Ia, La, bb, Vb;
        qb(a.addClass("fc-agenda"));
        Na = new Nb(function (h, O) {
            function x(eb) {
                return Math.max(Ea, Math.min(tc, eb))
            }
            var A, ta, za;
            r.each(function (eb, uc) {
                A = m(uc);
                ta = A.offset().left;
                if (eb)
                    za[1] = ta;
                za = [ta];
                O[eb] = za
            });
            za[1] = ta+A.outerWidth();
            if (i("allDaySlot")) {
                A =
                        $;
                ta = A.offset().top;
                h[0] = [ta, ta+A.outerHeight()]
            }
            for (var Da = I.offset().top, Ea = w.offset().top, tc = Ea+w.outerHeight(), fb = 0; fb < tb; fb++)
                h.push([x(Da+Xa * fb), x(Da+Xa * (fb+1))])
        });
        Ra = new Ob(Na);
        cb = new Pb(function (h) {
            return k.eq(h)
        })
    }
    function sc() {
        function a(o, s) {
            sa(o);
            var v, F = o.length, r = [], J = [];
            for (v = 0; v < F; v++)
                o[v].allDay ? r.push(o[v]) : J.push(o[v]);
            if (u("allDaySlot")) {
                L(e(r), s);
                na()
            }
            g(d(J), s)
        }
        function b() {
            ha();
            ua().empty();
            pa().empty()
        }
        function e(o) {
            o = ob(nb(o, m.map(o, Ua), q.visStart, q.visEnd));
            var s, v = o.length,
                    F, r, J, M = [];
            for (s = 0; s < v; s++) {
                F = o[s];
                for (r = 0; r < F.length; r++) {
                    J = F[r];
                    J.row = 0;
                    J.level = s;
                    M.push(J)
                }
            }
            return M
        }
        function d(o) {
            var s = z(), v = ka(), F = ca(), r = xa(N(q.visStart), v), J = m.map(o, f), M, k, D, Z, ja, ia, la = [];
            for (M = 0; M < s; M++) {
                k = ob(nb(o, J, r, xa(N(r), F - v)));
                vc(k);
                for (D = 0; D < k.length; D++) {
                    Z = k[D];
                    for (ja = 0; ja < Z.length; ja++) {
                        ia = Z[ja];
                        ia.col = M;
                        ia.level = D;
                        la.push(ia)
                    }
                }
                ba(r, 1, true)
            }
            return la
        }
        function f(o) {
            return o.end ? N(o.end) : xa(N(o.start), u("defaultEventMinutes"))
        }
        function g(o, s) {
            var v, F = o.length, r, J, M, k, D, Z, ja, ia, la,
                    $ = "", w, I, R = {}, V = {}, ea = pa(), aa;
            v = z();
            if (w = u("isRTL")) {
                I = -1;
                aa = v - 1
            } else {
                I = 1;
                aa = 0
            }
            for (v = 0; v < F; v++) {
                r = o[v];
                J = r.event;
                M = qa(r.start, r.start);
                k = qa(r.start, r.end);
                D = r.col;
                Z = r.level;
                ja = r.forward || 0;
                ia = G(D * I+aa);
                la = p(D * I+aa) - ia;
                la = Math.min(la - 6, la * 0.95);
                D = Z ? la / (Z+ja+1) : ja ? (la / (ja+1) - 6) * 2 : la;
                Z = ia+la / (Z+ja+1) * Z * I+(w ? la - D : 0);
                r.top = M;
                r.left = Z;
                r.outerWidth = D;
                r.outerHeight = k - M;
                $ += l(J, r)
            }
            ea[0].innerHTML = $;
            w = ea.children();
            for (v = 0; v < F; v++) {
                r = o[v];
                J = r.event;
                $ = m(w[v]);
                I = fa("eventRender", J, J, $);
                if (I === false)
                    $.remove();
                else {
                    if (I && I !== true) {
                        $.remove();
                        $ = m(I).css({position: "absolute", top: r.top, left: r.left}).appendTo(ea)
                    }
                    r.element = $;
                    if (J._id === s)
                        t(J, $, r);
                    else
                        $[0]._fci = v;
                    ya(J, $)
                }
            }
            Db(ea, o, t);
            for (v = 0; v < F; v++) {
                r = o[v];
                if ($ = r.element) {
                    J = R[s = r.key = Ib($[0])];
                    r.vsides = J === ma ? (R[s] = Sa($, true)) : J;
                    J = V[s];
                    r.hsides = J === ma ? (V[s] = pb($, true)) : J;
                    s = $.find("div.fc-event-content");
                    if (s.length)
                        r.contentTop = s[0].offsetTop
                }
            }
            for (v = 0; v < F; v++) {
                r = o[v];
                if ($ = r.element) {
                    $[0].style.width = Math.max(0, r.outerWidth - r.hsides)+"px";
                    R = Math.max(0, r.outerHeight -
                            r.vsides);
                    $[0].style.height = R+"px";
                    J = r.event;
                    if (r.contentTop !== ma && R - r.contentTop < 10) {
                        $.find("div.fc-event-time").text(Y(J.start, u("timeFormat"))+" - "+J.title);
                        $.find("div.fc-event-title").remove()
                    }
                    fa("eventAfterRender", J, J, $)
                }
            }
        }
        function l(o, s) {
            var v = "<", F = o.url, r = Jb(o, u), J = r ? " style='"+r+"'" : "", M = ["fc-event", "fc-event-skin", "fc-event-vert"];
            oa(o) && M.push("fc-event-draggable");
            s.isStart && M.push("fc-corner-top");
            s.isEnd && M.push("fc-corner-bottom");
            M = M.concat(o.className);
            if (o.source)
                M = M.concat(o.source.className ||
                []);
            v += F ? "a href='"+Qa(o.url)+"'" : "div";
            v += " class='"+M.join(" ")+"' style='position:absolute;z-index:8;top:"+s.top+"px;left:"+s.left+"px;"+r+"'><div class='fc-event-inner fc-event-skin'"+J+"><div class='fc-event-head fc-event-skin'"+J+"><div class='fc-event-time'>"+Qa(W(o.start, o.end, u("timeFormat")))+"</div></div><div class='fc-event-content'><div class='fc-event-title'>"+Qa(o.title)+"</div></div><div class='fc-event-bg'></div></div>";
            if (s.isEnd && ga(o))
                v += "<div class='ui-resizable-handle ui-resizable-s'>=</div>";
            v += "</"+(F ? "a" : "div")+">";
            return v
        }
        function j(o, s, v) {
            oa(o) && y(o, s, v.isStart);
            v.isEnd && ga(o) && c(o, s, v);
            da(o, s)
        }
        function t(o, s, v) {
            var F = s.find("div.fc-event-time");
            oa(o) && S(o, s, F);
            v.isEnd && ga(o) && Q(o, s, F);
            da(o, s)
        }
        function y(o, s, v) {
            function F() {
                if (!M) {
                    s.width(r).height("").draggable("option", "grid", null);
                    M = true
                }
            }
            var r, J, M = true, k, D = u("isRTL") ? -1 : 1, Z = U(), ja = H(), ia = T(), la = ka();
            s.draggable({zIndex: 9, opacity: u("dragOpacity", "month"), revertDuration: u("dragRevertDuration"), start: function ($, w) {
                    fa("eventDragStart",
                            s, o, $, w);
                    i(o, s);
                    r = s.width();
                    Z.start(function (I, R, V, ea) {
                        B();
                        if (I) {
                            J = false;
                            k = ea * D;
                            if (I.row)
                                if (v) {
                                    if (M) {
                                        s.width(ja - 10);
                                        Eb(s, ia * Math.round((o.end ? (o.end - o.start) / wc : u("defaultEventMinutes")) / u("slotMinutes")));
                                        s.draggable("option", "grid", [ja, 1]);
                                        M = false
                                    }
                                } else
                                    J = true;
                            else {
                                E(ba(N(o.start), k), ba(Ua(o), k));
                                F()
                            }
                            J = J || M && !k
                        } else {
                            F();
                            J = true
                        }
                        s.draggable("option", "revert", J)
                    }, $, "drag")
                }, stop: function ($, w) {
                    Z.stop();
                    B();
                    fa("eventDragStop", s, o, $, w);
                    if (J) {
                        F();
                        s.css("filter", "");
                        K(o, s)
                    } else {
                        var I = 0;
                        M || (I = Math.round((s.offset().top -
                                X().offset().top) / ia) * u("slotMinutes")+la - (o.start.getHours() * 60+o.start.getMinutes()));
                        C(this, o, k, I, M, $, w)
                    }
                }})
        }
        function S(o, s, v) {
            function F(I) {
                var R = xa(N(o.start), I), V;
                if (o.end)
                    V = xa(N(o.end), I);
                v.text(W(R, V, u("timeFormat")))
            }
            function r() {
                if (M) {
                    v.css("display", "");
                    s.draggable("option", "grid", [$, w]);
                    M = false
                }
            }
            var J, M = false, k, D, Z, ja = u("isRTL") ? -1 : 1, ia = U(), la = z(), $ = H(), w = T();
            s.draggable({zIndex: 9, scroll: false, grid: [$, w], axis: la == 1 ? "y" : false, opacity: u("dragOpacity"), revertDuration: u("dragRevertDuration"),
                start: function (I, R) {
                    fa("eventDragStart", s, o, I, R);
                    i(o, s);
                    J = s.position();
                    D = Z = 0;
                    ia.start(function (V, ea, aa, va) {
                        s.draggable("option", "revert", !V);
                        B();
                        if (V) {
                            k = va * ja;
                            if (u("allDaySlot") && !V.row) {
                                if (!M) {
                                    M = true;
                                    v.hide();
                                    s.draggable("option", "grid", null)
                                }
                                E(ba(N(o.start), k), ba(Ua(o), k))
                            } else
                                r()
                        }
                    }, I, "drag")
                }, drag: function (I, R) {
                    D = Math.round((R.position.top - J.top) / w) * u("slotMinutes");
                    if (D != Z) {
                        M || F(D);
                        Z = D
                    }
                }, stop: function (I, R) {
                    var V = ia.stop();
                    B();
                    fa("eventDragStop", s, o, I, R);
                    if (V && (k || D || M))
                        C(this, o, k, M ? 0 : D, M, I, R);
                    else {
                        r();
                        s.css("filter", "");
                        s.css(J);
                        F(0);
                        K(o, s)
                    }
                }})
        }
        function Q(o, s, v) {
            var F, r, J = T();
            s.resizable({handles: {s: "div.ui-resizable-s"}, grid: J, start: function (M, k) {
                    F = r = 0;
                    i(o, s);
                    s.css("z-index", 9);
                    fa("eventResizeStart", this, o, M, k)
                }, resize: function (M, k) {
                    F = Math.round((Math.max(J, s.height()) - k.originalSize.height) / J);
                    if (F != r) {
                        v.text(W(o.start, !F && !o.end ? null : xa(ra(o), u("slotMinutes") * F), u("timeFormat")));
                        r = F
                    }
                }, stop: function (M, k) {
                    fa("eventResizeStop", this, o, M, k);
                    if (F)
                        P(this, o, 0, u("slotMinutes") * F, M, k);
                    else {
                        s.css("z-index",
                                8);
                        K(o, s)
                    }
                }})
        }
        var q = this;
        q.renderEvents = a;
        q.compileDaySegs = e;
        q.clearEvents = b;
        q.slotSegHtml = l;
        q.bindDaySeg = j;
        Qb.call(q);
        var u = q.opt, fa = q.trigger, oa = q.isEventDraggable, ga = q.isEventResizable, ra = q.eventEnd, sa = q.reportEvents, ha = q.reportEventClear, da = q.eventElementHandlers, na = q.setHeight, ua = q.getDaySegmentContainer, pa = q.getSlotSegmentContainer, U = q.getHoverListener, ca = q.getMaxMinute, ka = q.getMinMinute, qa = q.timePosition, G = q.colContentLeft, p = q.colContentRight, L = q.renderDaySegs, c = q.resizableDayEvent, z = q.getColCnt,
                H = q.getColWidth, T = q.getSlotHeight, X = q.getBodyContent, ya = q.reportEventElement, K = q.showEvents, i = q.hideEvents, C = q.eventDrop, P = q.eventResize, E = q.renderDayOverlay, B = q.clearOverlays, n = q.calendar, Y = n.formatDate, W = n.formatDates
    }
    function vc(a) {
        var b, e, d, f, g, l;
        for (b = a.length - 1; b > 0; b--) {
            f = a[b];
            for (e = 0; e < f.length; e++) {
                g = f[e];
                for (d = 0; d < a[b - 1].length; d++) {
                    l = a[b - 1][d];
                    if (Cb(g, l))
                        l.forward = Math.max(l.forward || 0, (g.forward || 0)+1)
                }
            }
        }
    }
    function Kb(a, b, e) {
        function d(G, p) {
            G = qa[G];
            if (typeof G == "object")
                return jb(G, p || e);
            return G
        }
        function f(G, p) {
            return b.trigger.apply(b, [G, p || da].concat(Array.prototype.slice.call(arguments, 2), [da]))
        }
        function g(G) {
            return j(G) && !d("disableDragging")
        }
        function l(G) {
            return j(G) && !d("disableResizing")
        }
        function j(G) {
            return Ta(G.editable, (G.source || {}).editable, d("editable"))
        }
        function t(G) {
            U = {};
            var p, L = G.length, c;
            for (p = 0; p < L; p++) {
                c = G[p];
                if (U[c._id])
                    U[c._id].push(c);
                else
                    U[c._id] = [c]
            }
        }
        function y(G) {
            return G.end ? N(G.end) : na(G)
        }
        function S(G, p) {
            ca.push(p);
            if (ka[G._id])
                ka[G._id].push(p);
            else
                ka[G._id] =
                        [p]
        }
        function Q() {
            ca = [];
            ka = {}
        }
        function q(G, p) {
            p.click(function (L) {
                if (!p.hasClass("ui-draggable-dragging") && !p.hasClass("ui-resizable-resizing"))
                    return f("eventClick", this, G, L)
            }).hover(function (L) {
                f("eventMouseover", this, G, L)
            }, function (L) {
                f("eventMouseout", this, G, L)
            })
        }
        function u(G, p) {
            oa(G, p, "show")
        }
        function fa(G, p) {
            oa(G, p, "hide")
        }
        function oa(G, p, L) {
            G = ka[G._id];
            var c, z = G.length;
            for (c = 0; c < z; c++)
                if (!p || G[c][0] != p[0])
                    G[c][L]()
        }
        function ga(G, p, L, c, z, H, T) {
            var X = p.allDay, ya = p._id;
            sa(U[ya], L, c, z);
            f("eventDrop",
                    G, p, L, c, z, function () {
                        sa(U[ya], -L, -c, X);
                        pa(ya)
                    }, H, T);
            pa(ya)
        }
        function ra(G, p, L, c, z, H) {
            var T = p._id;
            ha(U[T], L, c);
            f("eventResize", G, p, L, c, function () {
                ha(U[T], -L, -c);
                pa(T)
            }, z, H);
            pa(T)
        }
        function sa(G, p, L, c) {
            L = L || 0;
            for (var z, H = G.length, T = 0; T < H; T++) {
                z = G[T];
                if (c !== ma)
                    z.allDay = c;
                xa(ba(z.start, p, true), L);
                if (z.end)
                    z.end = xa(ba(z.end, p, true), L);
                ua(z, qa)
            }
        }
        function ha(G, p, L) {
            L = L || 0;
            for (var c, z = G.length, H = 0; H < z; H++) {
                c = G[H];
                c.end = xa(ba(y(c), p, true), L);
                ua(c, qa)
            }
        }
        var da = this;
        da.element = a;
        da.calendar = b;
        da.name = e;
        da.opt =
                d;
        da.trigger = f;
        da.isEventDraggable = g;
        da.isEventResizable = l;
        da.reportEvents = t;
        da.eventEnd = y;
        da.reportEventElement = S;
        da.reportEventClear = Q;
        da.eventElementHandlers = q;
        da.showEvents = u;
        da.hideEvents = fa;
        da.eventDrop = ga;
        da.eventResize = ra;
        var na = da.defaultEventEnd, ua = b.normalizeEvent, pa = b.reportEventChange, U = {}, ca = [], ka = {}, qa = b.options
    }
    function Qb() {
        function a(i, C) {
            var P = z(), E = pa(), B = U(), n = 0, Y, W, o = i.length, s, v;
            P[0].innerHTML = e(i);
            d(i, P.children());
            f(i);
            g(i, P, C);
            l(i);
            j(i);
            t(i);
            C = y();
            for (P = 0; P < E; P++) {
                Y = [];
                for (W =
                        0; W < B; W++)
                    Y[W] = 0;
                for (; n < o && (s = i[n]).row == P; ) {
                    W = Hb(Y.slice(s.startCol, s.endCol));
                    s.top = W;
                    W += s.outerHeight;
                    for (v = s.startCol; v < s.endCol; v++)
                        Y[v] = W;
                    n++
                }
                C[P].height(Hb(Y))
            }
            Q(i, S(C))
        }
        function b(i, C, P) {
            var E = m("<div/>"), B = z(), n = i.length, Y;
            E[0].innerHTML = e(i);
            E = E.children();
            B.append(E);
            d(i, E);
            l(i);
            j(i);
            t(i);
            Q(i, S(y()));
            E = [];
            for (B = 0; B < n; B++)
                if (Y = i[B].element) {
                    i[B].row === C && Y.css("top", P);
                    E.push(Y[0])
                }
            return m(E)
        }
        function e(i) {
            var C = fa("isRTL"), P, E = i.length, B, n, Y, W;
            P = ka();
            var o = P.left, s = P.right, v, F, r, J, M, k =
                    "";
            for (P = 0; P < E; P++) {
                B = i[P];
                n = B.event;
                W = ["fc-event", "fc-event-skin", "fc-event-hori"];
                ga(n) && W.push("fc-event-draggable");
                if (C) {
                    B.isStart && W.push("fc-corner-right");
                    B.isEnd && W.push("fc-corner-left");
                    v = p(B.end.getDay() - 1);
                    F = p(B.start.getDay());
                    r = B.isEnd ? qa(v) : o;
                    J = B.isStart ? G(F) : s
                } else {
                    B.isStart && W.push("fc-corner-left");
                    B.isEnd && W.push("fc-corner-right");
                    v = p(B.start.getDay());
                    F = p(B.end.getDay() - 1);
                    r = B.isStart ? qa(v) : o;
                    J = B.isEnd ? G(F) : s
                }
                W = W.concat(n.className);
                if (n.source)
                    W = W.concat(n.source.className ||
                    []);
                Y = n.url;
                M = Jb(n, fa);
                k += Y ? "<a href='"+Qa(Y)+"'" : "<div";
                k += " class='"+W.join(" ")+"' style='position:absolute;z-index:8;left:"+r+"px;"+M+"'><div class='fc-event-inner fc-event-skin'"+(M ? " style='"+M+"'" : "")+">";
                if (!n.allDay && B.isStart)
                    k += "<span class='fc-event-time'>"+Qa(T(n.start, n.end, fa("timeFormat")))+"</span>";
                k += "<span class='fc-event-title'>"+Qa(n.title)+"</span></div>";
                if (B.isEnd && ra(n))
                    k += "<div class='ui-resizable-handle ui-resizable-"+(C ? "w" : "e")+"'>&nbsp;&nbsp;&nbsp;</div>";
                k += "</"+(Y ?
                        "a" : "div")+">";
                B.left = r;
                B.outerWidth = J - r;
                B.startCol = v;
                B.endCol = F+1
            }
            return k
        }
        function d(i, C) {
            var P, E = i.length, B, n, Y;
            for (P = 0; P < E; P++) {
                B = i[P];
                n = B.event;
                Y = m(C[P]);
                n = oa("eventRender", n, n, Y);
                if (n === false)
                    Y.remove();
                else {
                    if (n && n !== true) {
                        n = m(n).css({position: "absolute", left: B.left});
                        Y.replaceWith(n);
                        Y = n
                    }
                    B.element = Y
                }
            }
        }
        function f(i) {
            var C, P = i.length, E, B;
            for (C = 0; C < P; C++) {
                E = i[C];
                (B = E.element) && ha(E.event, B)
            }
        }
        function g(i, C, P) {
            var E, B = i.length, n, Y, W;
            for (E = 0; E < B; E++) {
                n = i[E];
                if (Y = n.element) {
                    W = n.event;
                    if (W._id ===
                            P)
                        H(W, Y, n);
                    else
                        Y[0]._fci = E
                }
            }
            Db(C, i, H)
        }
        function l(i) {
            var C, P = i.length, E, B, n, Y, W = {};
            for (C = 0; C < P; C++) {
                E = i[C];
                if (B = E.element) {
                    n = E.key = Ib(B[0]);
                    Y = W[n];
                    if (Y === ma)
                        Y = W[n] = pb(B, true);
                    E.hsides = Y
                }
            }
        }
        function j(i) {
            var C, P = i.length, E, B;
            for (C = 0; C < P; C++) {
                E = i[C];
                if (B = E.element)
                    B[0].style.width = Math.max(0, E.outerWidth - E.hsides)+"px"
            }
        }
        function t(i) {
            var C, P = i.length, E, B, n, Y, W = {};
            for (C = 0; C < P; C++) {
                E = i[C];
                if (B = E.element) {
                    n = E.key;
                    Y = W[n];
                    if (Y === ma)
                        Y = W[n] = Fb(B);
                    E.outerHeight = B[0].offsetHeight+Y
                }
            }
        }
        function y() {
            var i, C = pa(),
                    P = [];
            for (i = 0; i < C; i++)
                P[i] = ca(i).find("td:first div.fc-day-content > div");
            return P
        }
        function S(i) {
            var C, P = i.length, E = [];
            for (C = 0; C < P; C++)
                E[C] = i[C][0].offsetTop;
            return E
        }
        function Q(i, C) {
            var P, E = i.length, B, n;
            for (P = 0; P < E; P++) {
                B = i[P];
                if (n = B.element) {
                    n[0].style.top = C[B.row]+(B.top || 0)+"px";
                    B = B.event;
                    oa("eventAfterRender", B, B, n)
                }
            }
        }
        function q(i, C, P) {
            var E = fa("isRTL"), B = E ? "w" : "e", n = C.find("div.ui-resizable-"+B), Y = false;
            qb(C);
            C.mousedown(function (W) {
                W.preventDefault()
            }).click(function (W) {
                if (Y) {
                    W.preventDefault();
                    W.stopImmediatePropagation()
                }
            });
            n.mousedown(function (W) {
                function o(ia) {
                    oa("eventResizeStop", this, i, ia);
                    m("body").css("cursor", "");
                    s.stop();
                    ya();
                    k && ua(this, i, k, 0, ia);
                    setTimeout(function () {
                        Y = false
                    }, 0)
                }
                if (W.which == 1) {
                    Y = true;
                    var s = u.getHoverListener(), v = pa(), F = U(), r = E ? -1 : 1, J = E ? F - 1 : 0, M = C.css("top"), k, D, Z = m.extend({}, i), ja = L(i.start);
                    K();
                    m("body").css("cursor", B+"-resize").one("mouseup", o);
                    oa("eventResizeStart", this, i, W);
                    s.start(function (ia, la) {
                        if (ia) {
                            var $ = Math.max(ja.row, ia.row);
                            ia = ia.col;
                            if (v == 1)
                                $ = 0;
                            if ($ == ja.row)
                                ia = E ? Math.min(ja.col, ia) : Math.max(ja.col, ia);
                            k = $ * 7+ia * r+J - (la.row * 7+la.col * r+J);
                            la = ba(sa(i), k, true);
                            if (k) {
                                Z.end = la;
                                $ = D;
                                D = b(c([Z]), P.row, M);
                                D.find("*").css("cursor", B+"-resize");
                                $ && $.remove();
                                na(i)
                            } else if (D) {
                                da(i);
                                D.remove();
                                D = null
                            }
                            ya();
                            X(i.start, ba(N(la), 1))
                        }
                    }, W)
                }
            })
        }
        var u = this;
        u.renderDaySegs = a;
        u.resizableDayEvent = q;
        var fa = u.opt, oa = u.trigger, ga = u.isEventDraggable, ra = u.isEventResizable, sa = u.eventEnd, ha = u.reportEventElement, da = u.showEvents, na = u.hideEvents, ua = u.eventResize, pa = u.getRowCnt,
                U = u.getColCnt, ca = u.allDayRow, ka = u.allDayBounds, qa = u.colContentLeft, G = u.colContentRight, p = u.dayOfWeekCol, L = u.dateCell, c = u.compileDaySegs, z = u.getDaySegmentContainer, H = u.bindDaySeg, T = u.calendar.formatDates, X = u.renderDayOverlay, ya = u.clearOverlays, K = u.clearSelection
    }
    function Mb() {
        function a(Q, q, u) {
            b();
            q || (q = j(Q, u));
            t(Q, q, u);
            e(Q, q, u)
        }
        function b(Q) {
            if (S) {
                S = false;
                y();
                l("unselect", null, Q)
            }
        }
        function e(Q, q, u, fa) {
            S = true;
            l("select", null, Q, q, u, fa)
        }
        function d(Q) {
            var q = f.cellDate, u = f.cellIsAllDay, fa = f.getHoverListener(),
                    oa = f.reportDayClick;
            if (Q.which == 1 && g("selectable")) {
                b(Q);
                var ga;
                fa.start(function (ra, sa) {
                    y();
                    if (ra && u(ra)) {
                        ga = [q(sa), q(ra)].sort(Gb);
                        t(ga[0], ga[1], true)
                    } else
                        ga = null
                }, Q);
                m(document).one("mouseup", function (ra) {
                    fa.stop();
                    if (ga) {
                        +ga[0] == +ga[1] && oa(ga[0], true, ra);
                        e(ga[0], ga[1], true, ra)
                    }
                })
            }
        }
        var f = this;
        f.select = a;
        f.unselect = b;
        f.reportSelection = e;
        f.daySelectionMousedown = d;
        var g = f.opt, l = f.trigger, j = f.defaultSelectionEnd, t = f.renderSelection, y = f.clearSelection, S = false;
        g("selectable") && g("unselectAuto") && m(document).mousedown(function (Q) {
            var q =
                    g("unselectCancel");
            if (q)
                if (m(Q.target).parents(q).length)
                    return;
            b(Q)
        })
    }
    function Lb() {
        function a(g, l) {
            var j = f.shift();
            j || (j = m("<div class='fc-cell-overlay' style='position:absolute;z-index:3'/>"));
            j[0].parentNode != l[0] && j.appendTo(l);
            d.push(j.css(g).show());
            return j
        }
        function b() {
            for (var g; g = d.shift(); )
                f.push(g.hide().unbind())
        }
        var e = this;
        e.renderOverlay = a;
        e.clearOverlays = b;
        var d = [], f = []
    }
    function Nb(a) {
        var b = this, e, d;
        b.build = function () {
            e = [];
            d = [];
            a(e, d)
        };
        b.cell = function (f, g) {
            var l = e.length, j = d.length,
                    t, y = -1, S = -1;
            for (t = 0; t < l; t++)
                if (g >= e[t][0] && g < e[t][1]) {
                    y = t;
                    break
                }
            for (t = 0; t < j; t++)
                if (f >= d[t][0] && f < d[t][1]) {
                    S = t;
                    break
                }
            return y >= 0 && S >= 0 ? {row: y, col: S} : null
        };
        b.rect = function (f, g, l, j, t) {
            t = t.offset();
            return{top: e[f][0] - t.top, left: d[g][0] - t.left, width: d[j][1] - d[g][0], height: e[l][1] - e[f][0]}
        }
    }
    function Ob(a) {
        function b(j) {
            xc(j);
            j = a.cell(j.pageX, j.pageY);
            if (!j != !l || j && (j.row != l.row || j.col != l.col)) {
                if (j) {
                    g || (g = j);
                    f(j, g, j.row - g.row, j.col - g.col)
                } else
                    f(j, g);
                l = j
            }
        }
        var e = this, d, f, g, l;
        e.start = function (j, t, y) {
            f = j;
            g = l = null;
            a.build();
            b(t);
            d = y || "mousemove";
            m(document).bind(d, b)
        };
        e.stop = function () {
            m(document).unbind(d, b);
            return l
        }
    }
    function xc(a) {
        if (a.pageX === ma) {
            a.pageX = a.originalEvent.pageX;
            a.pageY = a.originalEvent.pageY
        }
    }
    function Pb(a) {
        function b(l) {
            return d[l] = d[l] || a(l)
        }
        var e = this, d = {}, f = {}, g = {};
        e.left = function (l) {
            return f[l] = f[l] === ma ? b(l).position().left : f[l]
        };
        e.right = function (l) {
            return g[l] = g[l] === ma ? e.left(l)+b(l).width() : g[l]
        };
        e.clear = function () {
            d = {};
            f = {};
            g = {}
        }
    }
    var Ya = {defaultView: "month", aspectRatio: 1.35,
        header: {left: "title", center: "", right: "hoy ant,sig"}, weekends: true, allDayDefault: true, ignoreTimezone: true, lazyFetching: true, startParam: "start", endParam: "end", titleFormat: {month: "MMMM yyyy", week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}", day: "dddd, MMM d, yyyy"}, columnFormat: {month: "ddd", week: "ddd M/d", day: "dddd M/d"}, timeFormat: {"": "h(:mm)t"}, isRTL: false, firstDay: 0, monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"], monthNamesShort: ["Ene",
            "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"], dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"], dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"], buttonText: {prev: "&nbsp;&#9668;&nbsp;", next: "&nbsp;&#9658;&nbsp;", prevYear: "&nbsp;&lt;&lt;&nbsp;", nextYear: "&nbsp;&gt;&gt;&nbsp;", today: "Hoy", month: "Mes", week: "Semana", day: "Dia"}, theme: false, buttonIcons: {prev: "circle-triangle-w", next: "circle-triangle-e"}, unselectAuto: true, dropAccept: "*"}, yc =
            {header: {left: "Sig,Ant Hoy", center: "", right: "title"}, buttonText: {prev: "&nbsp;&#9658;&nbsp;", next: "&nbsp;&#9668;&nbsp;", prevYear: "&nbsp;&gt;&gt;&nbsp;", nextYear: "&nbsp;&lt;&lt;&nbsp;"}, buttonIcons: {prev: "circle-triangle-e", next: "circle-triangle-w"}}, Aa = m.fullCalendar = {version: "1.5.4"}, Ja = Aa.views = {};
    m.fn.fullCalendar = function (a) {
        if (typeof a == "string") {
            var b = Array.prototype.slice.call(arguments, 1), e;
            this.each(function () {
                var f = m.data(this, "fullCalendar");
                if (f && m.isFunction(f[a])) {
                    f = f[a].apply(f,
                            b);
                    if (e === ma)
                        e = f;
                    a == "destroy" && m.removeData(this, "fullCalendar")
                }
            });
            if (e !== ma)
                return e;
            return this
        }
        var d = a.eventSources || [];
        delete a.eventSources;
        if (a.events) {
            d.push(a.events);
            delete a.events
        }
        a = m.extend(true, {}, Ya, a.isRTL || a.isRTL === ma && Ya.isRTL ? yc : {}, a);
        this.each(function (f, g) {
            f = m(g);
            g = new Yb(f, a, d);
            f.data("fullCalendar", g);
            g.render()
        });
        return this
    };
    Aa.sourceNormalizers = [];
    Aa.sourceFetchers = [];
    var ac = {dataType: "json", cache: false}, bc = 1;
    Aa.addDays = ba;
    Aa.cloneDate = N;
    Aa.parseDate = kb;
    Aa.parseISO8601 =
            Bb;
    Aa.parseTime = mb;
    Aa.formatDate = Oa;
    Aa.formatDates = ib;
    var lc = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"], Ab = 864E5, cc = 36E5, wc = 6E4, dc = {s: function (a) {
            return a.getSeconds()
        }, ss: function (a) {
            return Pa(a.getSeconds())
        }, m: function (a) {
            return a.getMinutes()
        }, mm: function (a) {
            return Pa(a.getMinutes())
        }, h: function (a) {
            return a.getHours() % 12 || 12
        }, hh: function (a) {
            return Pa(a.getHours() % 12 || 12)
        }, H: function (a) {
            return a.getHours()
        }, HH: function (a) {
            return Pa(a.getHours())
        }, d: function (a) {
            return a.getDate()
        }, dd: function (a) {
            return Pa(a.getDate())
        },
        ddd: function (a, b) {
            return b.dayNamesShort[a.getDay()]
        }, dddd: function (a, b) {
            return b.dayNames[a.getDay()]
        }, M: function (a) {
            return a.getMonth()+1
        }, MM: function (a) {
            return Pa(a.getMonth()+1)
        }, MMM: function (a, b) {
            return b.monthNamesShort[a.getMonth()]
        }, MMMM: function (a, b) {
            return b.monthNames[a.getMonth()]
        }, yy: function (a) {
            return(a.getFullYear()+"").substring(2)
        }, yyyy: function (a) {
            return a.getFullYear()
        }, t: function (a) {
            return a.getHours() < 12 ? "a" : "p"
        }, tt: function (a) {
            return a.getHours() < 12 ? "am" : "pm"
        }, T: function (a) {
            return a.getHours() <
                    12 ? "A" : "P"
        }, TT: function (a) {
            return a.getHours() < 12 ? "AM" : "PM"
        }, u: function (a) {
            return Oa(a, "yyyy-MM-dd'T'HH:mm:ss'Z'")
        }, S: function (a) {
            a = a.getDate();
            if (a > 10 && a < 20)
                return"th";
            return["st", "nd", "rd"][a % 10 - 1] || "th"
        }};
    Aa.applyAll = $a;
    Ja.month = mc;
    Ja.basicWeek = nc;
    Ja.basicDay = oc;
    wb({weekMode: "fixed"});
    Ja.agendaWeek = qc;
    Ja.agendaDay = rc;
    wb({allDaySlot: true, allDayText: "all-day", firstHour: 6, slotMinutes: 30, defaultEventMinutes: 120, axisFormat: "h(:mm)tt", timeFormat: {agenda: "h:mm{ - h:mm}"}, dragOpacity: {agenda: 0.5}, minTime: 0,
        maxTime: 24})
})(jQuery);


/// <reference path="../../../lib/jquery-1.2.6.js" />
/*
 Masked Input plugin for jQuery
 Copyright (c) 2007-2009 Josh Bush (digitalbush.com)
 Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license) 
 Version: 1.2.2 (03/09/2009 22:39:06)
 */
(function ($) {
    var pasteEventName = ($.browser.msie ? 'paste' : 'input')+".mask";
    var iPhone = (window.orientation != undefined);

    $.mask = {
        //Predefined character definitions
        definitions: {
            '9': "[0-9]",
            'a': "[A-Za-z]",
            '*': "[A-Za-z0-9]"
        }
    };

    $.fn.extend({
        //Helper Function for Caret positioning
        caret: function (begin, end) {
            if (this.length == 0)
                return;
            if (typeof begin == 'number') {
                end = (typeof end == 'number') ? end : begin;
                return this.each(function () {
                    if (this.setSelectionRange) {
                        this.focus();
                        this.setSelectionRange(begin, end);
                    } else if (this.createTextRange) {
                        var range = this.createTextRange();
                        range.collapse(true);
                        range.moveEnd('character', end);
                        range.moveStart('character', begin);
                        range.select();
                    }
                });
            } else {
                if (this[0].setSelectionRange) {
                    begin = this[0].selectionStart;
                    end = this[0].selectionEnd;
                } else if (document.selection && document.selection.createRange) {
                    var range = document.selection.createRange();
                    begin = 0 - range.duplicate().moveStart('character', -100000);
                    end = begin+range.text.length;
                }
                return {begin: begin, end: end};
            }
        },
        unmask: function () {
            return this.trigger("unmask");
        },
        mask: function (mask, settings) {
            if (!mask && this.length > 0) {
                var input = $(this[0]);
                var tests = input.data("tests");
                return $.map(input.data("buffer"), function (c, i) {
                    return tests[i] ? c : null;
                }).join('');
            }
            settings = $.extend({
                placeholder: "_",
                completed: null
            }, settings);

            var defs = $.mask.definitions;
            var tests = [];
            var partialPosition = mask.length;
            var firstNonMaskPos = null;
            var len = mask.length;

            $.each(mask.split(""), function (i, c) {
                if (c == '?') {
                    len--;
                    partialPosition = i;
                } else if (defs[c]) {
                    tests.push(new RegExp(defs[c]));
                    if (firstNonMaskPos == null)
                        firstNonMaskPos = tests.length - 1;
                } else {
                    tests.push(null);
                }
            });

            return this.each(function () {
                var input = $(this);
                var buffer = $.map(mask.split(""), function (c, i) {
                    if (c != '?')
                        return defs[c] ? settings.placeholder : c
                });
                var ignore = false;  			//Variable for ignoring control keys
                var focusText = input.val();

                input.data("buffer", buffer).data("tests", tests);

                function seekNext(pos) {
                    while (++pos <= len && !tests[pos])
                        ;
                    return pos;
                }
                ;

                function shiftL(pos) {
                    while (!tests[pos] && --pos >= 0)
                        ;
                    for (var i = pos; i < len; i++) {
                        if (tests[i]) {
                            buffer[i] = settings.placeholder;
                            var j = seekNext(i);
                            if (j < len && tests[i].test(buffer[j])) {
                                buffer[i] = buffer[j];
                            } else
                                break;
                        }
                    }
                    writeBuffer();
                    input.caret(Math.max(firstNonMaskPos, pos));
                }
                ;

                function shiftR(pos) {
                    for (var i = pos, c = settings.placeholder; i < len; i++) {
                        if (tests[i]) {
                            var j = seekNext(i);
                            var t = buffer[i];
                            buffer[i] = c;
                            if (j < len && tests[j].test(t))
                                c = t;
                            else
                                break;
                        }
                    }
                }
                ;

                function keydownEvent(e) {
                    var pos = $(this).caret();
                    var k = e.keyCode;
                    ignore = (k < 16 || (k > 16 && k < 32) || (k > 32 && k < 41));

                    //delete selection before proceeding
                    if ((pos.begin - pos.end) != 0 && (!ignore || k == 8 || k == 46))
                        clearBuffer(pos.begin, pos.end);

                    //backspace, delete, and escape get special treatment
                    if (k == 8 || k == 46 || (iPhone && k == 127)) {//backspace/delete
                        shiftL(pos.begin+(k == 46 ? 0 : -1));
                        return false;
                    } else if (k == 27) {//escape
                        input.val(focusText);
                        input.caret(0, checkVal());
                        return false;
                    }
                }
                ;

                function keypressEvent(e) {
                    if (ignore) {
                        ignore = false;
                        //Fixes Mac FF bug on backspace
                        return (e.keyCode == 8) ? false : null;
                    }
                    e = e || window.event;
                    var k = e.charCode || e.keyCode || e.which;
                    var pos = $(this).caret();

                    if (e.ctrlKey || e.altKey || e.metaKey) {//Ignore
                        return true;
                    } else if ((k >= 32 && k <= 125) || k > 186) {//typeable characters
                        var p = seekNext(pos.begin - 1);
                        if (p < len) {
                            var c = String.fromCharCode(k);
                            if (tests[p].test(c)) {
                                shiftR(p);
                                buffer[p] = c;
                                writeBuffer();
                                var next = seekNext(p);
                                $(this).caret(next);
                                if (settings.completed && next == len)
                                    settings.completed.call(input);
                            }
                        }
                    }
                    return false;
                }
                ;

                function clearBuffer(start, end) {
                    for (var i = start; i < end && i < len; i++) {
                        if (tests[i])
                            buffer[i] = settings.placeholder;
                    }
                }
                ;

                function writeBuffer() {
                    return input.val(buffer.join('')).val();
                }
                ;

                function checkVal(allow) {
                    //try to place characters where they belong
                    var test = input.val();
                    var lastMatch = -1;
                    for (var i = 0, pos = 0; i < len; i++) {
                        if (tests[i]) {
                            buffer[i] = settings.placeholder;
                            while (pos++ < test.length) {
                                var c = test.charAt(pos - 1);
                                if (tests[i].test(c)) {
                                    buffer[i] = c;
                                    lastMatch = i;
                                    break;
                                }
                            }
                            if (pos > test.length)
                                break;
                        } else if (buffer[i] == test[pos] && i != partialPosition) {
                            pos++;
                            lastMatch = i;
                        }
                    }
                    if (!allow && lastMatch+1 < partialPosition) {
                        input.val("");
                        clearBuffer(0, len);
                    } else if (allow || lastMatch+1 >= partialPosition) {
                        writeBuffer();
                        if (!allow)
                            input.val(input.val().substring(0, lastMatch+1));
                    }
                    return (partialPosition ? i : firstNonMaskPos);
                }
                ;

                if (!input.attr("readonly"))
                    input
                            .one("unmask", function () {
                                input
                                        .unbind(".mask")
                                        .removeData("buffer")
                                        .removeData("tests");
                            })
                            .bind("focus.mask", function () {
                                focusText = input.val();
                                var pos = checkVal();
                                writeBuffer();
                                setTimeout(function () {
                                    if (pos == mask.length)
                                        input.caret(0, pos);
                                    else
                                        input.caret(pos);
                                }, 0);
                            })
                            .bind("blur.mask", function () {
                                checkVal();
                                if (input.val() != focusText)
                                    input.change();
                            })
                            .bind("keydown.mask", keydownEvent)
                            .bind("keypress.mask", keypressEvent)
                            .bind(pasteEventName, function () {
                                setTimeout(function () {
                                    input.caret(checkVal(true));
                                }, 0);
                            });

                checkVal(); //Perform initial check for existing values
            });
        }
    });
})(jQuery);

jQuery(function ($) {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+0',
        showAnim: 'drop',
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['es']);
});


/*
 * Treeview 1.5pre - jQuery plugin to hide and show branches of a tree
 * 
 * http://bassistance.de/jquery-plugins/jquery-plugin-treeview/
 * http://docs.jquery.com/Plugins/Treeview
 *
 * Copyright (c) 2007 J�rn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id: jquery.treeview.js 5759 2008-07-01 07:50:28Z joern.zaefferer $
 *
 */

;
(function ($) {

    // TODO rewrite as a widget, removing all the extra plugins
    $.extend($.fn, {
        swapClass: function (c1, c2) {
            var c1Elements = this.filter('.'+c1);
            this.filter('.'+c2).removeClass(c2).addClass(c1);
            c1Elements.removeClass(c1).addClass(c2);
            return this;
        },
        replaceClass: function (c1, c2) {
            return this.filter('.'+c1).removeClass(c1).addClass(c2).end();
        },
        hoverClass: function (className) {
            className = className || "hover";
            return this.hover(function () {
                $(this).addClass(className);
            }, function () {
                $(this).removeClass(className);
            });
        },
        heightToggle: function (animated, callback) {
            animated ?
                    this.animate({height: "toggle"}, animated, callback) :
                    this.each(function () {
                        jQuery(this)[ jQuery(this).is(":hidden") ? "show" : "hide" ]();
                        if (callback)
                            callback.apply(this, arguments);
                    });
        },
        heightHide: function (animated, callback) {
            if (animated) {
                this.animate({height: "hide"}, animated, callback);
            } else {
                this.hide();
                if (callback)
                    this.each(callback);
            }
        },
        prepareBranches: function (settings) {
            if (!settings.prerendered) {
                // mark last tree items
                this.filter(":last-child:not(ul)").addClass(CLASSES.last);
                // collapse whole tree, or only those marked as closed, anyway except those marked as open
                this.filter((settings.collapsed ? "" : "."+CLASSES.closed)+":not(."+CLASSES.open+")").find(">ul").hide();
            }
            // return all items with sublists
            return this.filter(":has(>ul)");
        },
        applyClasses: function (settings, toggler) {
            // TODO use event delegation
            this.filter(":has(>ul):not(:has(>a))").find(">span").unbind("click.treeview").bind("click.treeview", function (event) {
                // don't handle click events on children, eg. checkboxes
                if (this == event.target)
                    toggler.apply($(this).next());
            }).add($("a", this)).hoverClass();

            if (!settings.prerendered) {
                // handle closed ones first
                this.filter(":has(>ul:hidden)")
                        .addClass(CLASSES.expandable)
                        .replaceClass(CLASSES.last, CLASSES.lastExpandable);

                // handle open ones
                this.not(":has(>ul:hidden)")
                        .addClass(CLASSES.collapsable)
                        .replaceClass(CLASSES.last, CLASSES.lastCollapsable);

                // create hitarea if not present
                var hitarea = this.find("div."+CLASSES.hitarea);
                if (!hitarea.length)
                    hitarea = this.prepend("<div class=\""+CLASSES.hitarea+"\"/>").find("div."+CLASSES.hitarea);
                hitarea.removeClass().addClass(CLASSES.hitarea).each(function () {
                    var classes = "";
                    $.each($(this).parent().attr("class").split(" "), function () {
                        classes += this+"-hitarea ";
                    });
                    $(this).addClass(classes);
                })
            }

            // apply event to hitarea
            this.find("div."+CLASSES.hitarea).click(toggler);
        },
        treeview: function (settings) {

            settings = $.extend({
                cookieId: "treeview"
            }, settings);

            if (settings.toggle) {
                var callback = settings.toggle;
                settings.toggle = function () {
                    return callback.apply($(this).parent()[0], arguments);
                };
            }

            // factory for treecontroller
            function treeController(tree, control) {
                // factory for click handlers
                function handler(filter) {
                    return function () {
                        // reuse toggle event handler, applying the elements to toggle
                        // start searching for all hitareas
                        toggler.apply($("div."+CLASSES.hitarea, tree).filter(function () {
                            // for plain toggle, no filter is provided, otherwise we need to check the parent element
                            return filter ? $(this).parent("."+filter).length : true;
                        }));
                        return false;
                    };
                }
                // click on first element to collapse tree
                $("a:eq(0)", control).click(handler(CLASSES.collapsable));
                // click on second to expand tree
                $("a:eq(1)", control).click(handler(CLASSES.expandable));
                // click on third to toggle tree
                $("a:eq(2)", control).click(handler());
            }

            // handle toggle event
            function toggler() {
                $(this)
                        .parent()
                        // swap classes for hitarea
                        .find(">.hitarea")
                        .swapClass(CLASSES.collapsableHitarea, CLASSES.expandableHitarea)
                        .swapClass(CLASSES.lastCollapsableHitarea, CLASSES.lastExpandableHitarea)
                        .end()
                        // swap classes for parent li
                        .swapClass(CLASSES.collapsable, CLASSES.expandable)
                        .swapClass(CLASSES.lastCollapsable, CLASSES.lastExpandable)
                        // find child lists
                        .find(">ul")
                        // toggle them
                        .heightToggle(settings.animated, settings.toggle);
                if (settings.unique) {
                    $(this).parent()
                            .siblings()
                            // swap classes for hitarea
                            .find(">.hitarea")
                            .replaceClass(CLASSES.collapsableHitarea, CLASSES.expandableHitarea)
                            .replaceClass(CLASSES.lastCollapsableHitarea, CLASSES.lastExpandableHitarea)
                            .end()
                            .replaceClass(CLASSES.collapsable, CLASSES.expandable)
                            .replaceClass(CLASSES.lastCollapsable, CLASSES.lastExpandable)
                            .find(">ul")
                            .heightHide(settings.animated, settings.toggle);
                }
            }
            this.data("toggler", toggler);

            function serialize() {
                function binary(arg) {
                    return arg ? 1 : 0;
                }
                var data = [];
                branches.each(function (i, e) {
                    data[i] = $(e).is(":has(>ul:visible)") ? 1 : 0;
                });
                $.cookie(settings.cookieId, data.join(""), settings.cookieOptions);
            }

            function deserialize() {
                var stored = $.cookie(settings.cookieId);
                if (stored) {
                    var data = stored.split("");
                    branches.each(function (i, e) {
                        $(e).find(">ul")[ parseInt(data[i]) ? "show" : "hide" ]();
                    });
                }
            }

            // add treeview class to activate styles
            this.addClass("treeview");

            // prepare branches and find all tree items with child lists
            var branches = this.find("li").prepareBranches(settings);

            switch (settings.persist) {
                case "cookie":
                    var toggleCallback = settings.toggle;
                    settings.toggle = function () {
                        serialize();
                        if (toggleCallback) {
                            toggleCallback.apply(this, arguments);
                        }
                    };
                    deserialize();
                    break;
                case "location":
                    var current = this.find("a").filter(function () {
                        return this.href.toLowerCase() == location.href.toLowerCase();
                    });
                    if (current.length) {
                        // TODO update the open/closed classes
                        var items = current.addClass("selected").parents("ul, li").add(current.next()).show();
                        if (settings.prerendered) {
                            // if prerendered is on, replicate the basic class swapping
                            items.filter("li")
                                    .swapClass(CLASSES.collapsable, CLASSES.expandable)
                                    .swapClass(CLASSES.lastCollapsable, CLASSES.lastExpandable)
                                    .find(">.hitarea")
                                    .swapClass(CLASSES.collapsableHitarea, CLASSES.expandableHitarea)
                                    .swapClass(CLASSES.lastCollapsableHitarea, CLASSES.lastExpandableHitarea);
                        }
                    }
                    break;
            }

            branches.applyClasses(settings, toggler);

            // if control option is set, create the treecontroller and show it
            if (settings.control) {
                treeController(this, settings.control);
                $(settings.control).show();
            }

            return this;
        }
    });

    // classes used by the plugin
    // need to be styled via external stylesheet, see first example
    $.treeview = {};
    var CLASSES = ($.treeview.classes = {
        open: "open",
        closed: "closed",
        expandable: "expandable",
        expandableHitarea: "expandable-hitarea",
        lastExpandableHitarea: "lastExpandable-hitarea",
        collapsable: "collapsable",
        collapsableHitarea: "collapsable-hitarea",
        lastCollapsableHitarea: "lastCollapsable-hitarea",
        lastCollapsable: "lastCollapsable",
        lastExpandable: "lastExpandable",
        last: "last",
        hitarea: "hitarea"
    });

})(jQuery);


/* jQuery treeTable Plugin 2.2.3 - http://ludo.cubicphuse.nl/jquery-plugins/treeTable/ */
(function ($) {
    // Helps to make options available to all functions
    // TODO: This gives problems when there are both expandable and non-expandable
    // trees on a page. The options shouldn't be global to all these instances!
    var options;
    var defaultPaddingLeft;

    $.fn.treeTable = function (opts) {
        options = $.extend({}, $.fn.treeTable.defaults, opts);

        return this.each(function () {
            $(this).addClass("treeTable").find("tbody tr").each(function () {
                // Initialize root nodes only if possible
                if (!options.expandable || $(this)[0].className.search("child-of-") == -1) {
                    // To optimize performance of indentation, I retrieve the padding-left
                    // value of the first root node. This way I only have to call +css+ 
                    // once.
                    if (isNaN(defaultPaddingLeft)) {
                        defaultPaddingLeft = parseInt($($(this).children("td")[options.treeColumn]).css('padding-left'), 10);
                    }

                    initialize($(this));
                } else if (options.initialState == "collapsed") {
                    this.style.display = "none"; // Performance! $(this).hide() is slow...
                }
            });
        });
    };

    $.fn.treeTable.defaults = {
        childPrefix: "child-of-",
        clickableNodeNames: false,
        expandable: true,
        indent: 19,
        initialState: "collapsed",
        treeColumn: 0
    };

    // Recursively hide all node's children in a tree
    $.fn.collapse = function () {
        $(this).addClass("collapsed");

        childrenOf($(this)).each(function () {
            if (!$(this).hasClass("collapsed")) {
                $(this).collapse();
            }

            this.style.display = "none"; // Performance! $(this).hide() is slow...
        });

        return this;
    };

    // Recursively show all node's children in a tree
    $.fn.expand = function () {
        $(this).removeClass("collapsed").addClass("expanded");

        childrenOf($(this)).each(function () {
            initialize($(this));

            if ($(this).is(".expanded.parent")) {
                $(this).expand();
            }

            // this.style.display = "table-row"; // Unfortunately this is not possible with IE :-(
            $(this).show();
        });

        return this;
    };

    // Add an entire branch to +destination+
    $.fn.appendBranchTo = function (destination) {
        var node = $(this);
        var parent = parentOf(node);

        var ancestorNames = $.map(ancestorsOf($(destination)), function (a) {
            return a.id;
        });

        // Conditions:
        // 1: +node+ should not be inserted in a location in a branch if this would
        //    result in +node+ being an ancestor of itself.
        // 2: +node+ should not have a parent OR the destination should not be the
        //    same as +node+'s current parent (this last condition prevents +node+
        //    from being moved to the same location where it already is).
        // 3: +node+ should not be inserted as a child of +node+ itself.
        if ($.inArray(node[0].id, ancestorNames) == -1 && (!parent || (destination.id != parent[0].id)) && destination.id != node[0].id) {
            indent(node, ancestorsOf(node).length * options.indent * -1); // Remove indentation

            if (parent) {
                node.removeClass(options.childPrefix+parent[0].id);
            }

            node.addClass(options.childPrefix+destination.id);
            move(node, destination); // Recursively move nodes to new location
            indent(node, ancestorsOf(node).length * options.indent);
        }

        return this;
    };

    // Add reverse() function from JS Arrays
    $.fn.reverse = function () {
        return this.pushStack(this.get().reverse(), arguments);
    };

    // Toggle an entire branch
    $.fn.toggleBranch = function () {
        if ($(this).hasClass("collapsed")) {
            $(this).expand();
        } else {
            $(this).removeClass("expanded").collapse();
        }

        return this;
    };

    // === Private functions

    function ancestorsOf(node) {
        var ancestors = [];
        while (node = parentOf(node)) {
            ancestors[ancestors.length] = node[0];
        }
        return ancestors;
    }
    ;

    function childrenOf(node) {
        return $("table.treeTable tbody tr."+options.childPrefix+node[0].id);
    }
    ;

    function getPaddingLeft(node) {
        var paddingLeft = parseInt(node[0].style.paddingLeft, 10);
        return (isNaN(paddingLeft)) ? defaultPaddingLeft : paddingLeft;
    }

    function indent(node, value) {
        var cell = $(node.children("td")[options.treeColumn]);
        cell[0].style.paddingLeft = getPaddingLeft(cell)+value+"px";

        childrenOf(node).each(function () {
            indent($(this), value);
        });
    }
    ;

    function initialize(node) {
        if (!node.hasClass("initialized")) {
            node.addClass("initialized");

            var childNodes = childrenOf(node);

            if (!node.hasClass("parent") && childNodes.length > 0) {
                node.addClass("parent");
            }

            if (node.hasClass("parent")) {
                var cell = $(node.children("td")[options.treeColumn]);
                var padding = getPaddingLeft(cell)+options.indent;

                childNodes.each(function () {
                    $(this).children("td")[options.treeColumn].style.paddingLeft = padding+"px";
                });

                if (options.expandable) {
                    cell.prepend('<span style="margin-left: 0px; padding-left: '+options.indent+'px" class="expander"></span>');
                    $(cell[0].firstChild).click(function () {
                        node.toggleBranch();
                    });

                    if (options.clickableNodeNames) {
                        cell[0].style.cursor = "pointer";
                        $(cell).click(function (e) {
                            // Don't double-toggle if the click is on the existing expander icon
                            if (e.target.className != 'expander') {
                                node.toggleBranch();
                            }
                        });
                    }

                    // Check for a class set explicitly by the user, otherwise set the default class
                    if (!(node.hasClass("expanded") || node.hasClass("collapsed"))) {
                        node.addClass(options.initialState);
                    }

                    if (node.hasClass("expanded")) {
                        node.expand();
                    }
                }
            }
        }
    }
    ;

    function move(node, destination) {
        node.insertAfter(destination);
        childrenOf(node).reverse().each(function () {
            move($(this), node[0]);
        });
    }
    ;

    function parentOf(node) {
        var classNames = node[0].className.split(' ');

        for (key in classNames) {
            if (classNames[key].match("child-of-")) {
                return $("#"+classNames[key].substring(9));
            }
        }
    }
    ;
})(jQuery);

/*!
 * jquery.fixedHeaderTable. The jQuery fixedHeaderTable plugin
 *
 * Copyright (c) 2011 Mark Malek
 * http://fixedheadertable.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * http://docs.jquery.com/Plugins/Authoring
 * jQuery authoring guidelines
 *
 * Launch  : October 2009
 * Version : 1.2.2
 * Released: May 9th, 2011
 *
 * 
 * all CSS sizing (width,height) is done in pixels (px)
 */

(function ($) {

    $.fn.fixedHeaderTable = function (method) {

        // plugin's default options
        var defaults = {
            width: '100%',
            height: '100%',
            borderCollapse: true,
            themeClass: 'fht-default',
            autoShow: true, // hide table after its created
            loader: false,
            footer: false, // show footer
            cloneHeadToFoot: false, // clone head and use as footer
            cloneHeaderToFooter: false, // deprecated option
            autoResize: false, // resize table if its parent wrapper changes size
            create: null // callback after plugin completes

        }

        var settings = {}

        // public methods
        var methods = {
            init: function (options) {

                settings = $.extend({}, defaults, options);

                // iterate through all the DOM elements we are attaching the plugin to
                return this.each(function () {

                    var $self = $(this), // reference the jQuery version of the current DOM element
                            self = this; // reference to the actual DOM element

                    if (helpers._isTable($self)) {
                        methods.setup.apply(this, Array.prototype.slice.call(arguments, 1));

                        $.isFunction(settings.create) && settings.create.call(this);
                    } else {
                        $.error('Invalid table mark-up');
                    }

                });

            },
            /*
             * Setup table structure for fixed headers and optional footer
             */
            setup: function (options) {
                var $self = $(this),
                        self = this,
                        $thead = $self.find('thead'),
                        $tfoot = $self.find('tfoot'),
                        $tbody = $self.find('tbody'),
                        $wrapper,
                        $divHead,
                        $divFoot,
                        $divBody,
                        $fixedHeadRow,
                        $temp,
                        tfootHeight = 0;

                settings.scrollbarOffset = helpers._getScrollbarWidth();
                settings.themeClassName = settings.themeClass;

                if (settings.width.search('%') > -1) {
                    var widthMinusScrollbar = $self.parent().width() - settings.scrollbarOffset;
                } else {
                    var widthMinusScrollbar = settings.width - settings.scrollbarOffset;
                }

                $self.css({
                    width: widthMinusScrollbar
                });


                if (!$self.closest('.fht-table-wrapper').length) {
                    $self.addClass('fht-table');
                    $self.wrap('<div class="fht-table-wrapper"></div>');
                }

                $wrapper = $self.closest('.fht-table-wrapper');

                $wrapper.css({
                    width: settings.width,
                    height: settings.height
                })
                        .addClass(settings.themeClassName);

                if (!$self.hasClass('fht-table-init')) {

                    $self.wrap('<div class="fht-tbody"></div>');

                }
                $divBody = $self.closest('.fht-tbody');

                var tableProps = helpers._getTableProps($self);

                helpers._setupClone($divBody, tableProps.tbody);

                if (!$self.hasClass('fht-table-init')) {
                    $divHead = $('<div class="fht-thead"><table class="fht-table"></table></div>').prependTo($wrapper);

                    $thead.clone().appendTo($divHead.find('table'));
                } else {
                    $divHead = $wrapper.find('div.fht-thead');
                }

                helpers._setupClone($divHead, tableProps.thead);

                $self.css({
                    'margin-top': -$thead.outerHeight(true) - tableProps.border
                });

                /*
                 * Check for footer
                 * Setup footer if present
                 */
                if (settings.footer == true) {

                    helpers._setupTableFooter($self, self, tableProps);

                    if (!$tfoot.length) {
                        $tfoot = $wrapper.find('div.fht-tfoot table');
                    }

                    tfootHeight = $tfoot.outerHeight(true);
                }

                var tbodyHeight = $wrapper.height() - $thead.outerHeight(true) - tfootHeight - tableProps.border;

                $divBody.css({
                    'height': tbodyHeight
                });

                if (!settings.autoShow) {
                    $wrapper.hide();
                }

                $self.addClass('fht-table-init');

                if (typeof (settings.altClass) !== 'undefined') {
                    $self.find('tbody tr:odd')
                            .addClass(settings.altClass);
                }

                helpers._bindScroll($divBody);

                return self;
            },
            /*
             * Resize the table
             * Incomplete - not implemented yet
             */
            resize: function (options) {
                var $self = $(this),
                        self = this;

                return self;
            },
            /*
             * Show a hidden fixedHeaderTable table
             */
            show: function (arg1, arg2, arg3) {
                var $self = $(this),
                        self = this,
                        $wrapper = $self.closest('.fht-table-wrapper');

                // User provided show duration without a specific effect
                if (typeof (arg1) !== 'undefined' && typeof (arg1) === 'number') {

                    $wrapper.show(arg1, function () {
                        $.isFunction(arg3) && arg3.call(this);
                    });

                    return self;

                } else if (typeof (arg1) !== 'undefined' && typeof (arg1) === 'string'
                        && typeof (arg2) !== 'undefined' && typeof (arg2) === 'number') {

                    // User provided show duration with an effect

                    $wrapper.show(arg1, arg2, function () {
                        $.isFunction(arg3) && arg3.call(this);
                    });

                    return self;

                }

                $self.closest('.fht-table-wrapper')
                        .show();
                $.isFunction(arg3) && arg3.call(this);

                return self;
            },
            /*
             * Hide a fixedHeaderTable table
             */
            hide: function (arg1, arg2, arg3) {
                var $self = $(this),
                        self = this,
                        $wrapper = $self.closest('.fht-table-wrapper');

                // User provided show duration without a specific effect
                if (typeof (arg1) !== 'undefined' && typeof (arg1) === 'number') {
                    $wrapper.hide(arg1, function () {
                        $.isFunction(arg3) && arg3.call(this);
                    });

                    return self;
                } else if (typeof (arg1) !== 'undefined' && typeof (arg1) === 'string'
                        && typeof (arg2) !== 'undefined' && typeof (arg2) === 'number') {

                    $wrapper.hide(arg1, arg2, function () {
                        $.isFunction(arg3) && arg3.call(this);
                    });

                    return self;
                }

                $self.closest('.fht-table-wrapper')
                        .hide();

                $.isFunction(arg3) && arg3.call(this);



                return self;
            },
            /*
             * Destory fixedHeaderTable and return table to original state
             */
            destroy: function () {
                var $self = $(this),
                        self = this,
                        $wrapper = $self.closest('.fht-table-wrapper');

                $self.insertBefore($wrapper)
                        .removeAttr('style')
                        .append($wrapper.find('tfoot'))
                        .removeClass('fht-table fht-table-init')
                        .find('.fht-cell')
                        .remove();

                $wrapper.remove();

                return self;
            }

        }

        // private methods
        var helpers = {
            /*
             * return boolean
             * True if a thead and tbody exist.
             */
            _isTable: function ($obj) {
                var $self = $obj,
                        hasTable = $self.is('table'),
                        hasThead = $self.find('thead').length > 0,
                        hasTbody = $self.find('tbody').length > 0;

                if (hasTable && hasThead && hasTbody) {
                    return true;
                }

                return false;

            },
            /*
             * return void
             * bind scroll event
             */
            _bindScroll: function ($obj) {
                var $self = $obj,
                        $thead = $self.siblings('.fht-thead'),
                        $tfoot = $self.siblings('.fht-tfoot');

                $self.bind('scroll', function () {
                    $thead.find('table')
                            .css({
                                'margin-left': -this.scrollLeft
                            });

                    if (settings.cloneHeadToFoot) {
                        $tfoot.find('table')
                                .css({
                                    'margin-left': -this.scrollLeft
                                });
                    }
                });
            },
            /*
             * return void
             */
            _setupTableFooter: function ($obj, obj, tableProps) {

                var $self = $obj,
                        self = obj,
                        $wrapper = $self.closest('.fht-table-wrapper'),
                        $tfoot = $self.find('tfoot'),
                        $divFoot = $wrapper.find('div.fht-tfoot');

                if (!$divFoot.length) {
                    $divFoot = $('<div class="fht-tfoot"><table class="fht-table"></table></div>').appendTo($wrapper);
                }

                switch (true) {
                    case !$tfoot.length && settings.cloneHeadToFoot == true && settings.footer == true:

                        var $divHead = $wrapper.find('div.fht-thead');

                        $divFoot.empty();
                        $divHead.find('table')
                                .clone()
                                .appendTo($divFoot);

                        break;
                    case $tfoot.length && settings.cloneHeadToFoot == false && settings.footer == true:

                        $divFoot.find('table')
                                .append($tfoot)
                                .css({
                                    'margin-top': -tableProps.border
                                });

                        helpers._setupClone($divFoot, tableProps.tfoot);

                        break;
                }

            },
            /*
             * return object
             * Widths of each thead cell and tbody cell for the first rows.
             * Used in fixing widths for the fixed header and optional footer.
             */
            _getTableProps: function ($obj) {
                var tableProp = {
                    thead: {},
                    tbody: {},
                    tfoot: {},
                    border: 0
                };

                tableProp.border = ($obj.find('th:first-child').outerWidth() - $obj.find('th:first-child').innerWidth()) / 2;

                $obj.find('thead tr:first-child th').each(function (index) {
                    tableProp.thead[index] = $(this).width()+tableProp.border;
                });

                $obj.find('tfoot tr:first-child td').each(function (index) {
                    tableProp.tfoot[index] = $(this).width()+tableProp.border;
                });

                $obj.find('tbody tr:first-child td').each(function (index) {
                    tableProp.tbody[index] = $(this).width()+tableProp.border;
                });

                return tableProp;
            },
            /*
             * return void
             * Fix widths of each cell in the first row of obj.
             */
            _setupClone: function ($obj, cellArray) {
                var $self = $obj,
                        selector = ($self.find('thead').length) ?
                        'thead th' :
                        ($self.find('tfoot').length) ?
                        'tfoot td' :
                        'tbody td',
                        $cell;

                $self.find(selector).each(function (index) {
                    $cell = ($(this).find('div.fht-cell').length) ? $(this).find('div.fht-cell') : $('<div class="fht-cell"></div>').appendTo($(this));

                    $cell.css({
                        'width': parseInt(cellArray[index])
                    });

                    /*
                     * Fixed Header and Footer should extend the full width
                     * to align with the scrollbar of the body 
                     */
                    if (!$(this).closest('.fht-tbody').length && $(this).is(':last-child')) {
                        var padding = (($(this).innerWidth() - $(this).width()) / 2)+settings.scrollbarOffset;
                        $(this).css({
                            'padding-right': padding+'px'
                        });
                    }
                });
            },
            /*
             * return int
             * get the width of the browsers scroll bar
             */
            _getScrollbarWidth: function () {
                var scrollbarWidth = 0;

                if (!scrollbarWidth) {
                    if ($.browser.msie) {
                        var $textarea1 = $('<textarea cols="10" rows="2"></textarea>')
                                .css({position: 'absolute', top: -1000, left: -1000}).appendTo('body'),
                                $textarea2 = $('<textarea cols="10" rows="2" style="overflow: hidden;"></textarea>')
                                .css({position: 'absolute', top: -1000, left: -1000}).appendTo('body');
                        scrollbarWidth = $textarea1.width() - $textarea2.width()+2; //+2 for border offset
                        $textarea1.add($textarea2).remove();
                    } else {
                        var $div = $('<div />')
                                .css({width: 100, height: 100, overflow: 'auto', position: 'absolute', top: -1000, left: -1000})
                                .prependTo('body').append('<div />').find('div')
                                .css({width: '100%', height: 200});
                        scrollbarWidth = 100 - $div.width();
                        $div.parent().remove();
                    }
                }

                return scrollbarWidth;
            }

        }


        // if a method as the given argument exists
        if (methods[method]) {

            // call the respective method
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));

            // if an object is given as method OR nothing is given as argument
        } else if (typeof method === 'object' || !method) {

            // call the initialization method
            return methods.init.apply(this, arguments);

            // otherwise
        } else {

            // trigger an error
            $.error('Method "'+method+'" does not exist in fixedHeaderTable plugin!');

        }

    }

})(jQuery);
