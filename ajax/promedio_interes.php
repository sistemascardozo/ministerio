<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../config.php");

    $codsuc	= $_POST["codsuc"];
    $nrofact    = $_POST["nro"];
    
    $sql = "SELECT Count(intd.nrofacturacion),
        SUM(intd.tasainteres)
        FROM facturacion.interesdiario AS intd
        WHERE intd.codemp=1 AND intd.codsuc=? AND intd.nrofacturacion=? ";
    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codsuc,$nrofact));
    $items = $consulta->fetch();
    $SUM= $items[1];
    $CON= $items[0];
    $promedio= number_format((($SUM/$CON)/12),2);
    
    echo $promedio."|";
	
?>