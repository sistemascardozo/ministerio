<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../config.php");
		
	$tiporeclamo    = $_POST["tiporeclamo"];
	$seleccion		= $_POST["seleccion"];
	$subtipo		= "";
	
	$sql = "SELECT codconcepto, descripcion, subtiporeclamo ";
	$sql .= "FROM reclamos.conceptosreclamos ";
	$sql .= "WHERE estareg=1 AND tiporeclamo=? ";
	$sql .= "ORDER BY subtiporeclamo, descripcion ";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($tiporeclamo));
	$items = $consulta->fetchAll();

?>
<select id="codconcepto" name="codconcepto" class="select" style="width:650px" <?=$d?> >
    <option value="0">--Seleccione el Concepto del Reclamo--</option>
<?php 
    foreach($items as $row)
    {
		$selected="";
		if($seleccion==$row["codconcepto"])
		{
			$selected="selected='selected'";
		}
	
		if($subtipo != $row["subtiporeclamo"])
		{
			echo "<optgroup label='".strtoupper($subtiporeclamo[$row["subtiporeclamo"]])."'>";
		}
	
?>
    <option value="<?=$row["codconcepto"]?>" <?=$selected?> ><?=strtoupper($row["descripcion"])?></option>
<?php $subtipo=$row["subtiporeclamo"]; } ?>
</select>