<?php

    include("../config.php");

    $codsuc     = $_POST["codsuc"];
    $nroreclamo = $_POST["nroreclamo"];

    $sql = "SELECT * FROM reclamos.reclamos re
    INNER JOIN catastro.clientes c ON (c.codemp = re.codemp AND c.codsuc = re.codsuc AND c.nroinscripcion = re.nroinscripcion)
    WHERE re.codemp = 1 AND re.codsuc = ".$codsuc." AND re.correlativo = ".$nroreclamo;

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array());
    $item = $consulta->fetch();


    $data['correlativonroreclamo'] = $item['correlativo'];
    $data['nroreclamo']            = $item['nroreclamo'];
    $data['codantiguo']            = $item['codantiguo'];
    $data['nroinscripcion']        = $item['nroinscripcion'];
    $data['reclamante']            = $item['reclamante'];
    $data['propietario']           = $item['propietario'];
    $data['fundado']               = $item['fundado'];
    $data['fin_reclamo']           = $item['fin_reclamo'];
    $data['glosa']                 = $item['glosa'];
    echo json_encode($data);

?>