<?php    
    session_name("pnsu");
    if(!session_start()){session_start();}
    
    include("../config.php");

    $Op = $_POST["Op"];
    
    $sql="SELECT
        tp.codtipoensayocaudal,
        tp.descripcion, tp.volumenpatronenservicio,
        tp.permisible, tp.volumenpatronnuevos
        FROM public.tipoensayocaudal AS tp
        WHERE
        tp.estareg = 1 AND tp.tiporetiro = ?";
    $ConsultaD = $conexion->prepare($sql);
    //print_r($consulta);
    $ConsultaD->execute(array($Op));
    $item = $ConsultaD->fetchAll();
    
    //$tr = '';
    foreach ($item as $rowD) {
        $i ++;
        $tr.='<tr id="'.$i.'" class="tr-detalle" style="height: 20px">';
        $tr.='<td>'.$i.'</td>';
        $tr.='<td>'.$rowD["descripcion"].'<input type="hidden" name="codtipoensayocaudal[]" value="'.$rowD['codtipoensayocaudal'].'" /></td>';
        $tr.='<td align="center"><input type="text" name="caudalensayo[]" value="0" size="6" /></td>';
        $tr.='<td align="center"><input type="text" name="presion[]" value="0" size="6" /></td>';
        $tr.='<td align="center"><input type="text" name="temperatura[]" value="0" size="6" /></td>';
        $tr.='<td align="center"><input type="text" name="volumenpatron[]" value="0" size="6" /></td>';
        $tr.='<td align="center"><input type="text" id="lecturainicio_'.$i.'" name="lecturainicio[]" value="0" size="6" onkeyup="CalcularD('.$i.');" /></td>';
        $tr.='<td align="center"><input type="text" id="lecturafin_'.$i.'" name="lecturafin[]" value="0" size="6" onkeyup="CalcularD('.$i.');" /></td>';        
        $tr.='<td align="center"><input type="text" id="diferencialectura_'.$i.'" name="diferencialectura[]" value="0" size="6" /></td>';
        $tr.='<td align="center"><input type="text" name="errorrelativa[]" value="0" size="6" /></td>';
        $tr.='<td align="center"><input type="text" name="errorpermisible[]" value="0" size="6" /></td>';
        $tr.='</tr>';
    }
     echo $tr;
?>