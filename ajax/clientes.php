<?php

    include("../config.php");

    $codsuc = $_POST["codsuc"];
    $nroinscripcion = $_POST["nroinscripcion"];

    $sql = "SELECT c.codcliente,c.propietario,c.codtipodocumento,c.nrodocumento,c.codciclo,tc.descripcioncorta,
        cl.descripcion as calle,c.nrocalle,c.codciclo,td.abreviado,e.descripcion as estadoservicio,c.codsector,
        c.codcalle, se.descripcion AS sector,c.codmanzanas, c.lote, c.codzona, z.descripcion AS zona,
        c.nroinscripcion,c.sublote,c.codantiguo,c.codestadoservicio, m.descripcion AS manzana

        FROM catastro.clientes as c
        INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
        INNER JOIN public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
        INNER JOIN public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
        INNER JOIN public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
        INNER JOIN public.manzanas as m on(c.codmanzanas = m.codmanzanas)
        LEFT JOIN public.sectores AS se ON se.codsector = c.codsector
        INNER JOIN admin.zonas AS z on z.codzona= c.codzona
        WHERE c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

    $consulta = $conexion->prepare($sql);
    //print_r($consulta);
    $consulta->execute(array($codsuc, $nroinscripcion));
    $item = $consulta->fetch();
    $data['codcliente'] = $item['codcliente'];
    $data['propietario'] = $item['propietario'];
    $data['codtipodocumento'] = $item['codtipodocumento'];
    $data['nrodocumento'] = $item['nrodocumento'];
    $data['codciclo'] = $item['codciclo'];
    $data['direccion'] = $item["descripcioncorta"]." ".$item["calle"]." ".$item["nrocalle"];
    $data['tdabreviado'] = $item['abreviado'];
    $data['estadoservicio'] = $item['estadoservicio'];
    $data['codsector'] = $item['codsector'];
    $data['codcalle'] = $item['codcalle'];
    $data['nrocalle'] = $item['nrocalle'];

    $data['calle'] = $item["descripcioncorta"]." ".$item['calle'];        //11
    $data['sector'] = $item['sector'];       //12
    $data['codmanzanas'] = $item['codmanzanas'];  //13
    $data['lote'] = $item['lote'];         //14
    $data['codzona'] = $item['codzona'];
    $data['zona'] = trim($item['zona']);
    $data['nroinscripcion'] = $item['nroinscripcion'];
    $data['sublote'] = $item['sublote'];
    $data['codantiguo'] = $item['codantiguo'];
    $data['telefono'] = $item['telefono'];
    $data['codestadoservicio'] = $item['codestadoservicio'];
    $data['manzana'] = $item['manzana'];
    echo json_encode($data);

?>