<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../config.php");
    
    $codciclo   = $_POST["codciclo"];
    $codsuc	= $_POST["codsuc"];
    $nrofact    = $_POST["nrofact"];
    
    $sql = "SELECT nrofacturacion,anio,mes,saldo,lecturas,tasainteres,facturacion,tasapromint
        FROM facturacion.periodofacturacion
        WHERE codemp=1 and codsuc=? and codciclo=? AND nrofacturacion=? ";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codsuc,$codciclo,$nrofact));
    $items = $consulta->fetch();
    $int= number_format($items['tasainteres'],4);
    $_SESSION["nrofacturacion"] = $items["nrofacturacion"];

    echo $items["nrofacturacion"]."|".$items["anio"]."|".$meses[$items["mes"]]."|".$items["mes"]."|".$items["saldo"]."|".$items["lecturas"]."|".$int."|".$items["tasapromint"]."|";
	
?>