<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../config.php");
    include("../include/main.php");
    
    $codsuc = $_POST["codsuc"];
    $nroinscripcion = $_POST["nroinscripcion"];

    $sql = "SELECT c.codcliente,
        c.propietario,
        c.nroinscripcion,
        tc.descripcioncorta||' '||cl.descripcion AS direccion,
        c.nrocalle,
        c.nromed,co.codmarca,co.coddiametrosmedidor,
        co.codestadomedidor,co.posicionmed,co.codmodelo,
        co.tipolectura,co.aniofabmed,co.codtipomedidor,
        ".$objFunciones->FormatFecha('co.fechainsmed')." AS fechainsmed,        
        co.codcapacidadmedidor,co.codclasemetrologica,c.tipofacturacion,
        co.codubicacion,co.lecturaanterior,
        ".$objFunciones->FormatFecha('c.fechalecturaanterior')." AS fechalecturaanterior,        
        c.lecturaultima, 
        ".$objFunciones->FormatFecha('c.fechalecturaultima')." AS fechalecturaultima,
        c.consumo, c.lecturapromedio, ma.descripcion AS marcamed,c.codantiguo
        
        FROM
        catastro.clientes AS c
        INNER JOIN public.calles AS cl ON (c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona)
        INNER JOIN public.tiposcalle AS tc ON (cl.codtipocalle = tc.codtipocalle)
        LEFT JOIN catastro.conexiones AS co ON (co.codemp = c.codemp AND co.codsuc = c.codsuc AND co.nroinscripcion = c.nroinscripcion)
        INNER JOIN public.marcamedidor AS ma ON ma.codmarca = co.codmarca
        WHERE c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

    $consulta = $conexion->prepare($sql);
    //print_r($consulta);
    $consulta->execute(array($codsuc,$nroinscripcion));
    $item = $consulta->fetch();
    $data['codcliente']        = $item['codcliente'];
    $data['propietario']       = $item['propietario'];    
    $data['direccion']         = $item["direccion"];    
    $data['nrocalle']          = $item['nrocalle'];
    
    $data['nromed']            = $item['nromed'];        
    $data['codmarca']          = $item['codmarca'];       
    $data['coddiametrosmedidor']  = $item['coddiametrosmedidor']; 
    $data['codestadomedidor']     = $item['codestadomedidor'];    
    $data['posicionmed']          = $item['posicionmed'];
    $data['codmodelo']            = $item['codmodelo'];  
    $data['tipolectura']       = $item['tipolectura'];
    $data['aniofabmed']        = $item['aniofabmed'];
    $data['fechainsmed']       = $item['fechainsmed'];
    $data['codcapacidadmedidor']   = $item['codcapacidadmedidor'];
    $data['tipofacturacion']       = $item['tipofacturacion'];
    $data['codubicacion']          = $item['codubicacion'];
    $data['lecturaanterior']       = $item['lecturaanterior'];
    $data['fechalecturaanterior']  = $item['fechalecturaanterior'];
    $data['lecturaultima']     = $item['lecturaultima'];
    $data['fechalecturaultima']    = $item['fechalecturaultima'];
    $data['consumo']           = $item['consumo'];
    $data['lecturapromedio']   = $item['lecturapromedio'];
    $data['nroinscripcion']    = $item['nroinscripcion'];
    $data['codtipomedidor']    = $item['codtipomedidor'];
    $data['marcamed']          = $item['marcamed'];
    $data['codantiguo'] = $item['codantiguo'];
    echo json_encode($data);

?>