<?php 
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../objetos/clsDrop.php");
	
    $objDrop = new clsDrop();
	
    $codexpediente 	= $_POST["codexpediente"];
    $dCredito 		= $_POST["dCredito"]?$_POST["dCredito"]:0;
    $codsuc			= $_POST["codsuc"];
    $Op 			= $_POST["Op"];
	
    $codemp	= 1;

    $sql = "SELECT * FROM solicitudes.expedientes ";
	$sql .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND  codexpediente = ".$codexpediente;
	
    $consulta = $conexion->prepare($sql);
    $consulta->execute(array());
    $items = $consulta->fetch();
	
	$sqlSol = "SELECT nroinspeccion FROM catastro.solicitudinspeccion ";
	$sqlSol .= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$items['nroinscripcion']." AND estado = 4 ";
	
	$consultaSol = $conexion->prepare($sqlSol);
	$consultaSol->execute(array());
	$itemSol = $consultaSol->fetch();
	
	$NroInspeccion = 0;
	
	if (count($itemSol) != 0)
	{
		$NroInspeccion = $itemSol[0];
	}
	
    $direccion = strtoupper($items["direccion"]);
    $vigv = $items["igv"];
    $cuotainicial = $items["cuotainicial"];
    $conigv = 0;
	
    if(floatval($vigv) > 0)
    {
		$conigv = 1;
    }
	
    ///VALIDAR SI EL USUARIO PRESENTA MAS PRESUPUESTOS DEL TIPO SELECCIONADO
    $tr = "";
	
    if($items['tipopago'] == 1) //CONTADO
    {
		$masdeuno = 1;
		
		$codciclo = $_POST["codciclo"]?$_POST["codciclo"]:1;
		
		$sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
		$sql .= "FROM facturacion.periodofacturacion ";
		$sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = ? AND facturacion = 0";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($codsuc, $codciclo));
		$itemsf = $consulta->fetch();
		
		$Sql = "SELECT co.codconcepto, co.descripcion, SUM(c.subtotal) AS subtotal, SUM(c.igv) AS igv, ";
		$Sql .= " SUM(c.total) AS imptotal, 0 AS redondeo, c.nropresupuesto ";
		$Sql .= "FROM solicitudes.expedientesdetalle c ";
		$Sql .= " INNER JOIN solicitudes.expedientes e ON(e.codemp = c.codemp) AND (e.codsuc = c.codsuc) AND (e.codexpediente = c.codexpediente) ";
		$Sql .= " INNER JOIN public.tipopresupuesto tp ON(tp.codtipopresupuesto = c.codtipopresupuesto) ";
		$Sql .= " INNER JOIN facturacion.conceptos co ON(tp.codconcepto = co.codconcepto) AND (c.codsuc = co.codsuc) ";
		$Sql .= "WHERE c.codemp = ? AND c.codsuc = ? AND c.codexpediente = ? ";
		$Sql .= "GROUP BY co.codconcepto, co.descripcion, c.nropresupuesto ";
		$Sql .= "ORDER BY c.nropresupuesto";
		
		$consulta = $conexion->prepare($Sql);
		$consulta->execute(array($codemp, $codsuc, $codexpediente));
		$itemsD = $consulta->fetchAll();
		
		$subtotaltotal = 0;
		$igvtotal      = 0;
		$redondeototal = 0;
		$importetotal  = 0;
		
		$i = 0;
		
		foreach($itemsD as $rowD)
		{
			$subtotaltotal += $rowD['subtotal'];
			$igvtotal      += $rowD['igv'];
			$redondeototal += $rowD['redondeo'];
			$importetotal  += $rowD['imptotal'];

			$i++;
			
			$tr .= "<tr style='background-color:#FFF' id='tr_".$i."'>";
			$tr .= "<td align='center'><input type='hidden' name='nrofacturacion".$i."' id='nrofacturacion".$i."' value='0' />0</td>";
			$tr .= "<td align='center'><input type='hidden' name='anioy".$i."' id='anioy".$i."' value='".$itemsf["anio"]."' />RSP</td>";
			$tr .= "<td align='center'><input type='hidden' name='codcategoria".$i."' id='codcategoria".$i."' value='4' />0</td>";
			$tr .= "<td align='center'><input type='hidden' name='mesy".$i."' id='mesy".$i."' value='".$itemsf["mes"]."' />".$itemsf["anio"]." - ".$meses[$itemsf["mes"]]."</td>";
			$tr .= "<td align='center'><input type='hidden' name='codtipodeuda".$i."' id='codtipodeuda".$i."' value='5' />COBRANZA DIRECTAMENTE EN CAJA</td>";
			$tr .= "<td align='center'><input type='hidden' name='codconceptox".$i."' id='codconceptox".$i."' value='".$rowD['codconcepto']."' />PAGO POR ".$rowD['descripcion']."</td>";
			$tr .= "<td align='center'><input type='hidden' name='detalle".$i."' id='detalle".$i."' value='PAGO POR ".$rowD['descripcion']."' />PRESTACION DE SERVICIOS EN CAJA</td>";
			$tr .= "<td align='right'><input type='hidden' name='npresupuesto".$i."' id='npresupuesto".$i."' value='".$rowD['nropresupuesto']."' />";
			$tr .= "<input type='hidden' name='igvadd".$i."' id='igvadd".$i."' value='".$rowD['igv']."' />";
			$tr .= "<input type='hidden' name='imptotal".$i."' id='imptotal".$i."' value='".$rowD['imptotal']."' /><label id='lblimptotal".$i."' >".number_format($rowD['imptotal'],2)."</label></td>";
			$tr .= "<td><span onclick='open_validar_remover_item(".$i.");' title='Quitar Registro' class='icono-icon-trash'></span> ";
			$tr .= "<span onclick='open_update_importe_item(".$i.");' title='Modificar Importe' class='icono-icon-dinero'></span> </td>";
			$tr .= "</tr>";
      	}
    

		$data['tr'] = $tr;
		$data['cont_cobranza'] = $i;
		$data['subtotaltotal'] = $subtotaltotal;
		$data['igvtotal'] = $igvtotal;
		$data['redondeototal'] = $redondeototal;
		$data['importetotal'] = $importetotal;
	}
	
	$tr = "";
	
    if($dCredito == 1)//else //CREDITO
    {
		$codciclo   = $_POST["codciclo"]?$_POST["codciclo"]:1;
		
		$sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
		$sql .= "FROM facturacion.periodofacturacion ";
		$sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = ? AND facturacion = 0";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($codsuc,$codciclo));
		$itemsf = $consulta->fetch();
		
		/*$Sql="SELECT co.codconcepto, co.descripcion, SUM(c.subtotal) AS subtotal,SUM(c.igv) AS igv ,
		SUM(c.total) AS imptotal, 0 AS redondeo,c.nropresupuesto
		FROM  solicitudes.expedientesdetalle c
		INNER JOIN solicitudes.expedientes e ON (e.codemp = c.codemp AND e.codsuc = c.codsuc AND e.codexpediente = c.codexpediente)
		INNER JOIN public.tipopresupuesto tp ON (tp.codtipopresupuesto = c.codtipopresupuesto)
		INNER JOIN facturacion.conceptos co ON (tp.codconcepto = co.codconcepto) AND (c.codsuc = co.codsuc)
		WHERE c.codemp=? AND c.codsuc=? AND  c.codexpediente=? 
		GROUP BY co.codconcepto, co.descripcion,c.nropresupuesto
		ORDER BY  c.nropresupuesto";*/
		
		$Sql = "SELECT co.codconcepto, co.descripcion, SUM(c.subtotal) AS subtotal, SUM(c.igv) AS igv, ";
		$Sql .= " SUM(c.total) AS imptotal, 0 AS redondeo, 0 ";
		$Sql .= "FROM solicitudes.expedientesdetalle c ";
		$Sql .= " INNER JOIN solicitudes.expedientes e ON(e.codemp = c.codemp) AND (e.codsuc = c.codsuc) AND (e.codexpediente = c.codexpediente) ";
		$Sql .= " INNER JOIN public.tipopresupuesto tp ON(tp.codtipopresupuesto = c.codtipopresupuesto) ";
		$Sql .= " INNER JOIN facturacion.conceptos co ON(c.codsuc = co.codsuc) AND (tp.codconcepto = co.codconcepto) ";
		$Sql .= "WHERE c.codemp = ? AND c.codsuc = ? AND c.codexpediente = ? ";
		$Sql .= "GROUP BY co.codconcepto, co.descripcion ";
		$Sql .= "ORDER BY co.descripcion";
	  
		$consulta = $conexion->prepare($Sql);
		$consulta->execute(array($codemp, $codsuc, $codexpediente));
		$itemsD = $consulta->fetchAll();
		$subtotaltotal = 0;
		$igvtotal      = 0;
		$redondeototal = 0;
		$importetotal  = 0;
		
		$i = 0;
		
		foreach($itemsD as $rowD)
		{
			$subtotaltotal += $rowD['subtotal'];
			$igvtotal      += $rowD['igv'];
			$redondeototal += $rowD['redondeo'];
			$importetotal  += $rowD['imptotal'];
			
			$i++;
			
			$tr .= '<tr id="'.$i.'" onclick="SeleccionaId(this);">';
			$tr .= '<td align="center" ><label class="Item">'.$i.'</label></td>';
			$tr .= '<td align="left"><label class="CodConcepto">'.$rowD['codconcepto'].'</label>';
			$tr .= '<label class="Colateral">'.$rowD['descripcion'].'</label></td>';
			$tr .= '<td align="right"><label class="SubTotal">'.number_format($rowD['subtotal'],2).'</label></td>';
			$tr .= '<td align="right"><label class="IGV">'.number_format($rowD['igv'],2).'</label></td>';
			$tr .= '<td align="right"><label class="Redondeo">'.number_format($rowD['redondeo'],2).'</label></td>';
			$tr .= '<td align="right"><label class="Importe">'.number_format($rowD['imptotal'],2).'</label></td>';
			$tr .= '<td align="center" ><a href="javascript:QuitaItemc('.$i.')" class="Del" >';
			$tr .= '<span class="icono-icon-trash" title="Quitar Colateral"></span> ';
			$tr .= '</a></td></tr>';
		}
    

		$data['tr']     = $tr;
		$data['cont_cobranza']    = $i;
		$data['subtotaltotal']  = $subtotaltotal;
		$data['igvtotal']  = $igvtotal;
		$data['redondeototal']  = $redondeototal;
		$data['importetotal']  = $importetotal;
	}

	///VALIDAR SI EL USUARIO PRESENTA MAS PRESUPUESTOS DEL TIPO SELECCIONADO
	
	$data['direccion']		= $items['direccion']; 
	$data['masdeuno']		= $masdeuno; 
	$data['propietario']	= strtoupper($items["propietario"]);
	$data['codcliente']		= $items['codcliente']; 
	$data['nroexpediente']	= $items['nroexpediente']; 
	$data['tipopago']		= $items['tipopago']; 
	$data['nrocuota']		= $items['nrocuota']; 
	$data['interes']		= $items['interes'];
	$data['cuotames']		= $items['cuotames'];
	//$data['codtipodocumento'] = $items['codtipodocumento']; 
	//$data['nrodocumento']	= $items['nrodocumento']; 
	//$data['abreviado']		= $items['abreviado']; 
	$data['documento']		= $items['documento']; 
	
	$data['totalpresupuesto'] = number_format($items["totalpresupuesto"], 2); 
	$data['cuotainicial']	= number_format($cuotainicial, 2);
	$data['conigv']			= $conigv; 
	$data['codzona']		= $items['codzona']; 
	$data['codmanzanas']	= $items['codmanzanas']; 
	
	$data['nroinscripcion']	= $items['nroinscripcion']; 
	$data['nroinspeccion']	= $NroInspeccion; //$items['nroinspeccion']; 
	$data['correo']			= $items['correo']; 
	$data['codantiguo']		= $objDrop->CodUsuario($codsuc, $items['nroinscripcion']); 
	$data['nuevocliente']	= 0;
	///
	if($items['nroinspeccion'] != 0)
	{
		$sql = "SELECT nuevocliente FROM catastro.solicitudinspeccion ";
		$sql .= "WHERE nroinspeccion = ? AND codsuc = ?";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($items['nroinspeccion'],$codsuc));
		$item = $consulta->fetch();
		$data['nuevocliente'] = $item['nuevocliente'];
	}
	
	echo json_encode($data);
?>