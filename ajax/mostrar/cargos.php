<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
   
    include("../../objetos/clsDrop.php");
	
    $objDrop = new clsDrop();
	
    $cargo = $_POST["cargo"];
    $codsuc	= $_POST["codsuc"];
    $codemp	= 1;

    $sql = "SELECT cb.*, c.nrodocumento, td.abreviado, c.codantiguo ";
	$sql .= "FROM facturacion.cabvarios AS cb ";
	$sql .= " INNER JOIN catastro.clientes AS c ON (c.nroinscripcion = cb.nroinscripcion AND c.codsuc = cb.codsuc) ";
	$sql .= " INNER JOIN public.tipodocumento AS td ON (c.codtipodocumento = td.codtipodocumento) ";
	$sql .= "WHERE cb.codemp = ? AND cb.codsuc = ? AND cb.nrocredito = ?";
		 
    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codemp, $codsuc, $cargo));
	
    $items = $consulta->fetch();
	
    $direccion = strtoupper($items["direccion"]);
    $vigv = $items["igv"];
    $conigv = 0;
	
    if(floatval($vigv) > 0)
    {
		$conigv = 1;
    }
    ///VALIDAR SI EL USUARIO PRESENTA MAS PRESUPUESTOS DEL TIPO SELECCIONADO
    $tr = "";
      
    $codciclo = $_POST["codciclo"]?$_POST["codciclo"]:1;
	
    $sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
	$sql .= "FROM facturacion.periodofacturacion ";
	$sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = ? AND facturacion = 0";
	
    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codsuc, $codciclo));
    $itemsf = $consulta->fetch();

    $Sql = "SELECT co.codconcepto, co.descripcion, ";
	$Sql .= " SUM(d.subtotal) AS subtotal, SUM(d.igv) AS igv, SUM(d.imptotal) AS imptotal, 0 AS redondeo ";
	$Sql .= "FROM facturacion.detcabvarios d ";
	$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codconcepto = co.codconcepto) AND (d.codsuc = co.codsuc) ";
	$Sql .= "WHERE d.codemp = ".$codemp." AND d.codsuc = ".$codsuc." AND  d.nrocredito = ".$cargo." ";
	$Sql .= "GROUP BY co.codconcepto, co.descripcion ";
	
    $consulta = $conexion->prepare($Sql);
    $consulta->execute(array());
    $itemsD = $consulta->fetchAll();
	
    $subtotaltotal = 0;
    $igvtotal      = 0;
    $redondeototal = 0;
    $importetotal  = 0;
    $i = 0;
	
    foreach($itemsD as $rowD)
    {
      //$subtotaltotal += $rowD['subtotal'];
//      $igvtotal      += $rowD['igv'];
//      $redondeototal += $rowD['redondeo'];
//      $importetotal  += $rowD['imptotal'];
	  	$importetotal  += $rowD['subtotal'];
		
  		if ($rowD['codconcepto'] == 5)
		{
			//$ImpTT = $rowD['imptotal'];
			
			$igvtotal      += $rowD['imptotal'];
		}
		else if ($rowD['codconcepto'] == 7 || $rowD['codconcepto'] == 8)
		{
			$redondeototal += $rowD['imptotal'];
		}
		else
		{
			$subtotaltotal += $rowD['subtotal'];
			
			$i++;
			$tr.="<tr style='background-color:#FFF' id='tr_".$i."'>";
			$tr.= "<td align='center'><input type='hidden' name='nrofacturacion".$i."' id='nrofacturacion".$i."' value='0' />0</td>";
			$tr.="<td align='center'><input type='hidden' name='anioy".$i."' id='anioy".$i."' value='".$itemsf["anio"]."' />RSP</td>";
			$tr.="<td align='center'><input type='hidden' name='codcategoria".$i."' id='codcategoria".$i."' value='4' />0</td>";
			$tr.="<td align='center'><input type='hidden' name='mesy".$i."' id='mesy".$i."' value='".$itemsf["mes"]."' />".$itemsf["anio"]." - ".$meses[$itemsf["mes"]]."</td>";
			$tr.="<td align='center'><input type='hidden' name='codtipodeuda".$i."' id='codtipodeuda".$i."' value='5' />COBRANZA DIRECTAMENTE EN CAJA</td>";
			$tr.="<td align='center'><input type='hidden' name='codconceptox".$i."' id='codconceptox".$i."' value='".$rowD['codconcepto']."' />PAGO POR ".$rowD['descripcion']."</td>";
			$tr.="<td align='center'><input type='hidden' name='detalle".$i."' id='detalle".$i."' value='PAGO POR ".$rowD['descripcion']."' />PRESTACION DE SERVICIOS EN CAJA</td>";
			$tr.="<td align='right'><input type='hidden' name='npresupuesto".$i."' id='npresupuesto".$i."' value='".$rowD['nropresupuesto']."' />";
			$tr.="<input type='hidden' name='igvadd".$i."' id='igvadd".$i."' value='".$igvtotal."' />";
			$tr.="<input type='hidden' name='imptotal".$i."' id='imptotal".$i."' value='".$rowD['subtotal']."' /><label id='lblimptotal".$i."' >".number_format($rowD['imptotal'],2)."</label></td>";
			$tr.="<td><span onclick='open_validar_remover_item(".$i.");' title='Quitar Registro' class='icono-icon-trash'></span> ";
			$tr.="<span onclick='open_update_importe_item(".$i.");' title='Modificar Importe' class='icono-icon-dinero'></span> </td>";
			$tr.="</tr>";
		}
    }
    
    $data['tr']            = $tr;
    $data['cont_cobranza'] = $i;
    $data['subtotaltotal'] = $subtotaltotal;
    $data['igvtotal']      = $igvtotal;
    $data['redondeototal'] = $redondeototal;
    $data['importetotal']  = $importetotal;
    
    

    ///VALIDAR SI EL USUARIO PRESENTA MAS PRESUPUESTOS DEL TIPO SELECCIONADO

    $data['direccion']        = $items['direccion']; 
    $data['masdeuno']         = $masdeuno; 
    $data['propietario']      = strtoupper($items["propietario"]);
    $data['codcliente']       = $items['codcliente']; 
    $data['nroexpediente']    = $items['nroexpediente']; 
    $data['tipopago']         = $items['tipopago']; 
    $data['nrocuota']         = $items['nrocuota']; 
    $data['interes']          = $items['interes'];
    $data['cuotames']         = $items['cuotames'];
    $data['codtipodocumento'] = $items['codtipodocumento']; 
    $data['nrodocumento']     = $items['nrodocumento']; 
    $data['abreviado']        = $items['abreviado']; 
    
    $data['totalpresupuesto'] = number_format($items["totalpresupuesto"], 2); 
    $data['cuotainicial']     = number_format($cuotainicial, 2);
    $data['conigv']           = $conigv; 
    $data['codzona']          = $items['codzona']; 
    $data['codmanzanas']      = $items['codmanzanas']; 
    
    $data['nroinscripcion']   = $items['nroinscripcion']; 
    $data['nroinspeccion']    = $items['nroinspeccion']; 
    $data['correo']           = $items['correo']; 
    $data['codantiguo']       = $items['codantiguo'];	//$objDrop->CodUsuario($codsuc, $items['nroinscripcion']); 
    $data['nuevocliente']     = 0;

    echo json_encode($data);


?>