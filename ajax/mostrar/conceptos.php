<?php 
	include("../../config.php");
	
	$codconcepto 	= $_POST["conceptos"];
	$codsuc			= $_POST["codsuc"];

	$consulta = $conexion->prepare("select codconcepto,descripcion,importe,afecto_igv,diasatencion from facturacion.conceptos where codconcepto=? and codsuc=?");
	$consulta->execute(array($codconcepto,$codsuc));
	$items = $consulta->fetch();
	
	//$items["codconcepto"]."|".$items["descripcion"]."|".$items["importe"]."|".$items["afecto_igv"];
	
	$data['codconcepto']    = $items['codconcepto'];  
	$data['descripcion']    = $items['descripcion'];  
	$data['importe']    = $items['importe'];  
	$data['afecto_igv']    = $items['afecto_igv'];  
	$data['diasatencion']    = $items['diasatencion'];  
	echo json_encode($data);

?>