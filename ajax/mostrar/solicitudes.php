<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../config.php");
	
	$nrosolicitud 	= $_POST["nrosolicitud"];
	$codsuc			= $_POST["codsuc"];
	
	$sql = "SELECT s.codcliente,s.propietario,s.codtiposervicio,s.codsector,s.codcalle,s.nrocalle,s.codtipodocumento,
					s.nrodocumento,s.area,s.codzona,s.nroinscripcion,s.nroinspeccion,s.correo
			from solicitudes.solicitudes as s
			WHERE s.codemp=1 and s.codsuc=? and s.nrosolicitud=?";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nrosolicitud));
	$item = $consulta->fetch();

	//$ret  = $item["codcliente"]."|".$item["propietario"]."|".$item["codtiposervicio"]."|".$item["codsector"]."|".$item["codcalle"]."|".$item["nrocalle"];
	//$ret .= "|".$item["codtipodocumento"]."|".$item["nrodocumento"]."|".$item["area"]."|".$item["codzona"];
	
	$data['codcliente']       = $item['codcliente']; 
	$data['propietario']      = $item['propietario']; 
	$data['codtiposervicio']  = $item['codtiposervicio']; 
	$data['codsector']        = $item['codsector']; 
	$data['codcalle']         = $item['codcalle']; 
	$data['nrocalle']         = $item['nrocalle']; 
	$data['codtipodocumento'] = $item['codtipodocumento']; 
	$data['nrodocumento']     = $item['nrodocumento']; 
	$data['area']             = $item['area']; 
	$data['codzona']          = $item['codzona']; 
	$data['nroinscripcion']   = $item['nroinscripcion']; 
	$data['nroinspeccion']    = $item['nroinspeccion']; 
	$data['correo']    		= $item['correo']; 
	$data['tiposervicio']    = $item['codtiposervicio']; 
	echo json_encode($data);

	
?>