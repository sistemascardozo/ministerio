<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../objetos/clsDrop.php");
	
    $objDrop = new clsDrop();
	
    $nropresupuesto = $_POST["nropresupuesto"];
    $codsuc	= $_POST["codsuc"];
    $Op = $_POST["Op"];
    $codemp	= 1;

	$sql = "SELECT c.tipopresupuesto, c.nrosolicitud, c.propietario, tc.descripcioncorta, cl.descripcion, c.nrocalle, c.codsector,
        c.codcalle, c.codtipodocumento, c.codcliente, c.nrodocumento, td.abreviado, c.tipopago, c.totalpresupuesto,
        c.cuotainicial, c.igv, c.subtotal, c.codzona, sol.codmanzanas, sol.lote, c.nroinscripcion, sol.nroinspeccion, sol.correo
        FROM solicitudes.cabpresupuesto as c
        INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
        inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
        inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
        INNER JOIN solicitudes.solicitudes AS sol ON (sol.codemp= c.codemp AND sol.codsuc= c.codsuc AND sol.nrosolicitud= c.nrosolicitud)
        WHERE c.codemp=? and c.codsuc=? and c.nropresupuesto=?";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codemp,$codsuc,$nropresupuesto));
    $items = $consulta->fetch();
	
    if($items=='')
    {
       $sql = "SELECT c.tipopresupuesto,c.nrosolicitud,c.propietario,tc.descripcioncorta,cl.descripcion,c.nrocalle,c.codsector,
        c.codcalle,c.codtipodocumento,c.codcliente,c.nrodocumento,td.abreviado,c.tipopago,c.totalpresupuesto,
        c.cuotainicial,c.igv,c.subtotal, c.codzona,'' as codmanzanas,'' as lote,c.nroinscripcion,'' as nroinspeccion,'' as correo
        FROM solicitudes.cabpresupuesto as c
        INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
        inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
        inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
        /*INNER JOIN solicitudes.solicitudes AS sol ON (sol.codemp= c.codemp AND sol.codsuc= c.codsuc AND sol.nrosolicitud= c.nrosolicitud)*/
        WHERE c.codemp=? and c.codsuc=? and c.nropresupuesto=? ";
      $consulta = $conexion->prepare($sql);
      $consulta->execute(array($codemp,$codsuc,$nropresupuesto));
      $items = $consulta->fetch();
    }
    $calle = strtoupper($items["descripcioncorta"]." ".$items["descripcion"]." ".$items["nrocalle"]);
    $vigv=$items["igv"];
    $cuotainicial = $items["cuotainicial"];
    $conigv=0;
    if(floatval($vigv)>0)
    {
            $conigv=1;

    }
    //CAGAR PRESUPUESTOS PARA EL EXPEDIENTE
    if($Op==1)
    {
         $Sql="SELECT tp.codtipopresupuesto, UPPER(tp.descripcion) AS descripcion , SUM(c.subtotal+c.impgasadm) AS subtotal,SUM(c.igv) AS igv ,
            SUM(c.totalpresupuesto) AS imptotal,
            SUM(c.redondeo) AS redondeo,c.nropresupuesto,tp.codconcepto,c.nrosolicitud
            FROM  public.tipopresupuesto tp
            INNER JOIN solicitudes.cabpresupuesto c ON (tp.codtipopresupuesto = c.codtipopresupuesto)
            INNER JOIN facturacion.conceptos co ON (tp.codconcepto = co.codconcepto)
            AND (co.codemp = c.codemp)  AND (co.codsuc = c.codsuc)
            WHERE c.codemp=? and c.codsuc=? AND c.nroinscripcion=? AND c.estadopresupuesto in(1)
            GROUP BY tp.codtipopresupuesto, tp.descripcion,c.nropresupuesto,tp.codconcepto,c.nrosolicitud
            ORDER BY  c.nropresupuesto ";
        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array($codemp,$codsuc,$items['nroinscripcion']));
        $itemsD = $consulta->fetchAll();
        $subtotaltotal =0;
        $igvtotal      =0;
        $redondeototal =0;
        $importetotal  =0;
        $i=0;
        $codciclo   = $_POST["codciclo"]?$_POST["codciclo"]:1;
        $sql = "SELECT nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
        FROM facturacion.periodofacturacion
        WHERE codemp=1 and codsuc=? and codciclo=? and facturacion=0";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc,$codciclo));
        $itemsf = $consulta->fetch();


        
        
        foreach($itemsD as $rowD)
        {
          $subtotaltotal +=$rowD['subtotal'];
          $igvtotal      +=$rowD['igv'];
          $redondeototal +=$rowD['redondeo'];
          $importetotal  +=$rowD['imptotal'];
      
          $i++;
          $tr.='<tr id="'.$i.'" onclick="SeleccionaId(this);">';
          $tr.='<td align="center" ><label class="Item">'.$i.'</label></td>';
          $tr.='<td align="left"><label class="CodTipoPresupuesto">'.$rowD['codtipopresupuesto'].'</label>';
          $tr.='<label class="Colateral">'.$rowD['descripcion'].'</label>';
          $tr.='<label class="CodConcepto">'.$rowD['codconcepto'].'</label>';
          $tr.='<label class="NroSolicitud">'.$rowD['nrosolicitud'].'</label>';
          $tr.='<label class="NroPresupuesto">'.$rowD['nropresupuesto'].'</label></td>';
          $tr.='<td align="right"><label class="SubTotal">'.number_format($rowD['subtotal'],2).'</label></td>';
          $tr.='<td align="right"><label class="IGV">'.number_format($rowD['igv'],2).'</label></td>';
          $tr.='<td align="right"><label class="Redondeo">'.number_format($rowD['redondeo'],2).'</label></td>';
          $tr.='<td align="right"><label class="Importe">'.number_format($rowD['imptotal'],2).'</label></td>';
          $tr.='<td align="center" ><a href="javascript:QuitaItemc('.$i.')" class="Del" >';
          $tr.='<span class="icono-icon-trash" title="Quitar Presupuesto" style="display:none;"></span> ';
          $tr.='</a></td></tr>';
        }
      

        $data['trexp']     = $tr;
        $data['subtotaltotalexp']  = number_format($subtotaltotal,2);
        $data['igvtotalexp']  = number_format($igvtotal,2);
        $data['redondeototalexp']  = number_format($redondeototal,2);
        $data['importetotalexp']  = number_format($importetotal,2);
    }
    ///VALIDAR SI EL USUARIO PRESENTA MAS PRESUPUESTOS DEL TIPO SELECCIONADO
    $Sql = "SELECT count(*)
        FROM solicitudes.cabpresupuesto as c
        WHERE c.codemp=? and c.codsuc=? AND c.tipopresupuesto=? AND c.estadopresupuesto in(2) and c.tipopago in(1,2) ";
    $consulta = $conexion->prepare($Sql);
    $consulta->execute(array($codemp,$codsuc,$items['tipopresupuesto']));
    $items2 = $consulta->fetch();
    $masdeuno=0;
    if($items2[0]>1)
    {
      $masdeuno=1;
      $Sql="SELECT co.codconcepto, co.descripcion, SUM(c.subtotal) AS subtotal,SUM(c.igv) AS igv ,
            SUM(CASE (c.tipopago) WHEN 1 THEN c.totalpresupuesto WHEN 2 THEN c.cuotainicial END) AS imptotal,
            SUM(c.redondeo) AS redondeo, c.nropresupuesto
            FROM  public.tipopresupuesto tp
            INNER JOIN solicitudes.cabpresupuesto c ON (tp.codtipopresupuesto = c.codtipopresupuesto)
            INNER JOIN facturacion.conceptos co ON (tp.codconcepto = co.codconcepto)
            AND (co.codemp = c.codemp)  AND (co.codsuc = c.codsuc)
            WHERE c.codemp=? and c.codsuc=? AND c.tipopresupuesto=? AND c.estadopresupuesto in(2) and c.tipopago in(1,2)
            GROUP BY co.codconcepto, co.descripcion,c.nropresupuesto
            ORDER BY  co.codconcepto ";
      $consulta = $conexion->prepare($Sql);
      $consulta->execute(array($codemp,$codsuc,$items['tipopresupuesto']));
      $itemsD = $consulta->fetchAll();
      $subtotaltotal =0;
      $igvtotal      =0;
      $redondeototal =0;
      $importetotal  =0;
      $i=0;
      $codciclo   = $_POST["codciclo"]?$_POST["codciclo"]:1;
      $sql = "SELECT nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
      FROM facturacion.periodofacturacion
      WHERE codemp=1 and codsuc=? and codciclo=? and facturacion=0";
      $consulta = $conexion->prepare($sql);
      $consulta->execute(array($codsuc,$codciclo));
      $itemsf = $consulta->fetch();


      
      
      foreach($itemsD as $rowD)
      {
        $subtotaltotal +=$rowD['subtotal'];
        $igvtotal      +=$rowD['igv'];
        $redondeototal +=$rowD['redondeo'];
        $importetotal  +=$rowD['imptotal'];

			//$ImpTT = $rowD['subtotal'];
//					
//			if ($rowD['codconcepto'] == 5 || $rowD['codconcepto'] == 7 || $rowD['codconcepto'] == 8)
//			{
				$ImpTT = $rowD['imptotal'];
			//}
    
        $i++;
        $tr.="<tr style='background-color:#FFF' id='tr_".$i."'>";
        $tr.= "<td align='center'><input type='hidden' name='nrofacturacion".$i."' id='nrofacturacion".$i."' value='0' />0</td>";
        $tr.="<td align='center'><input type='hidden' name='anioy".$i."' id='anioy".$i."' value='".$itemsf["anio"]."' />RSP</td>";
        $tr.="<td align='center'><input type='hidden' name='codcategoria".$i."' id='codcategoria".$i."' value='4' />0</td>";
        $tr.="<td align='center'><input type='hidden' name='mesy".$i."' id='mesy".$i."' value='".$itemsf["mes"]."' />".$itemsf["anio"]." - ".$meses[$itemsf["mes"]]."</td>";
        $tr.="<td align='center'><input type='hidden' name='codtipodeuda".$i."' id='codtipodeuda".$i."' value='5' />COBRANZA DIRECTAMENTE EN CAJA</td>";
        $tr.="<td align='center'><input type='hidden' name='codconceptox".$i."' id='codconceptox".$i."' value='".$rowD['codconcepto']."' />PAGO POR ".$rowD['descripcion']."</td>";
        $tr.="<td align='center'><input type='hidden' name='detalle".$i."' id='detalle".$i."' value='PAGO POR ".$rowD['descripcion']."' />PRESTACION DE SERVICIOS EN CAJA</td>";
        $tr.="<td align='right'><input type='hidden' name='npresupuesto".$i."' id='npresupuesto".$i."' value='".$rowD['nropresupuesto']."' />";
        $tr.="<input type='hidden' name='igvadd".$i."' id='igvadd".$i."' value='".$rowD['igv']."' />";
        $tr.="<input type='hidden' name='imptotal".$i."' id='imptotal".$i."' value='".$ImpTT."' /><label id='lblimptotal".$i."' >".number_format($rowD['imptotal'],2)."</label></td>";
        $tr.="<td><span onclick='open_validar_remover_item(".$i.");' title='Quitar Registro' class='icono-icon-trash'></span> ";
        $tr.="<span onclick='open_update_importe_item(".$i.");' title='Modificar Importe' class='icono-icon-dinero'></span> </td>";
        $tr.="</tr>";
      }
    

      $data['tr']     = $tr;
      $data['cont_cobranza']    = $i;
      $data['subtotaltotal']  = $subtotaltotal;
      $data['igvtotal']  = $igvtotal;
      $data['redondeototal']  = $redondeototal;
      $data['importetotal']  = $importetotal;
    }
    ///VALIDAR SI EL USUARIO PRESENTA MAS PRESUPUESTOS DEL TIPO SELECCIONADO

   $data['tipopresupuesto']  = $items['tipopresupuesto']; 
   $data['masdeuno']         = $masdeuno; 
   $data['nrosolicitud']     = $items['nrosolicitud']; 
   $data['propietario']      = strtoupper($items["propietario"]);
   $data['calle']            = $calle; 
   $data['codsector']        = $items['codsector']; 
   $data['codcalle']         = $items['codcalle']; 
   $data['nrocalle']         = $items['nrocalle']; 
   $data['codcliente']       = $items['codcliente']; 
   $data['codtipodocumento'] = $items['codtipodocumento']; 
   $data['nrodocumento']     = $items['nrodocumento']; 
   $data['abreviado']        = $items['abreviado']; 
   $data['tipopago']         = $items['tipopago']; 
   $data['totalpresupuesto'] = number_format($items["totalpresupuesto"],2); 
   $data['cuotainicial']     = number_format($cuotainicial,2);
   $data['conigv']           = $conigv; 
   $data['codzona']          = $items['codzona']; 
   $data['codmanzanas']      = $items['codmanzanas']; 
   $data['lote']             = $items['lote']; 
   $data['nroinscripcion']   = $items['nroinscripcion']; 
   $data['nroinspeccion']    = $items['nroinspeccion']; 
   $data['correo']           = $items['correo']; 
   $data['nuevocliente']     = 0;
   $data['codantiguo']       = $objDrop->CodUsuario($codsuc, $items['nroinscripcion']);
   ///
   if($items['nroinspeccion']!=0)
   {
    $sql  = "SELECT nuevocliente
          FROM catastro.solicitudinspeccion 
          WHERE nroinspeccion=? AND codsuc=?";
          $consulta = $conexion->prepare($sql);
          $consulta->execute(array($items['nroinspeccion'],$codsuc));
          $item = $consulta->fetch();
          $data['nuevocliente']        = $item['nuevocliente'];
   }
    echo json_encode($data);


?>