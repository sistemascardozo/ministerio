<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../objetos/clsSql.php");

	$objSql = new clsSql();

	$sistema = $_POST["sistema"];
	$seleccion = $_POST["seleccion"];

	$sql = $objSql->Sentencia("subsistema")." WHERE s.codsistema = ? AND s.estareg = 1 ORDER BY s.descripcion";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($sistema));
	$items = $consulta->fetchAll();

	if ($seleccion == 1000)
	{
		$onChange = "onchange='cargar_perfiles(this.value)'";
    }
	else
	{
		$onChange = "onchange='cargar_modulo(this.value, 0)'";
	}
?>
<select name="codsubsistema" id="codsubsistema" style="width:280px" <?php echo $onChange;?>  >
	<option value="0">--Seleccione el Sub. Sistema--</option>
<?php
	foreach ($items as $row)
	{
		$selected = "";
		
		if ($seleccion == $row["codsubsistema"])
		{
			$selected = "selected='selected'";
		}
		
		echo "<option value='".$row["codsubsistema"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
	}
?>
</select>