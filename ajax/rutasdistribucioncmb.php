<?php
include("../config.php");

$codsector = $_POST["codsector"];
$codmanzana = $_POST["codmanzana"];
$codsuc = $_POST["codsuc"];
$codrutdistribucion = $_POST["codrutdistribucion"];

$sql = "SELECT rd.codrutdistribucion, rmd.orden, rmd.descripcion ";
$sql .= "FROM public.rutasdistribucion rd ";
$sql .= " INNER JOIN public.rutasmaedistribucion rmd ON (rd.codemp = rmd.codemp) AND (rd.codsuc = rmd.codsuc) AND (rd.codsector = rmd.codsector) ";
$sql .= "  AND (rd.codrutdistribucion = rmd.codrutdistribucion) ";
$sql .= "WHERE rd.codemp = 1 ";
$sql .= " AND rd.codsuc = ".$codsuc." ";
$sql .= " AND rd.codsector = ".$codsector." ";
$sql .= " AND rd.codmanzanas = '".$codmanzana."' ";

$consulta = $conexion->prepare($sql);
$consulta->execute(array());
//$items = $consulta->fetchAll();

foreach ($consulta->fetchAll() as $row) {
    $Selected = '';
    if ($row['codrutdistribucion'] == $codrutdistribucion) {
        $Selected = 'selected="selected"';
    }
    ?>
    <option value="<?= $row[0] ?>" title="<?= $row[2] ?>" <?= $Selected ?>><?= $row[2] ?></option>
    <?php
}

//echo $items["codrutdistribucion"]."|".$items["orden"]."|".strtoupper($items["descripcion"]);
?>