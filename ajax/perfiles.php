<?php 
	include("../config.php");
	
	$codsubsistema = $_POST["codsubsistema"];
	
	$sql = "select s.codperfil,p.descripcion
			from seguridad.subsistema_perfiles as s 
			inner join seguridad.perfiles as p on(s.codperfil=p.codperfil) where s.codsubsistema=? 
			group by s.codperfil,p.descripcion";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsubsistema));
	$items = $consulta->fetchAll();

?>
<select name="codperfil" id="codperfil" style="width:280px" >
	<option value="0">--Seleccione el Sub. Sistema--</option>
    <?php
		foreach($items as $row)
		{
			echo "<option value='".$row["codperfil"]."' >".strtoupper($row["descripcion"])."</option>";
		}
	?>
</select>