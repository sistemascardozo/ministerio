<?php
    include("../config.php");
    
    $codsector  = $_POST["codsector"];
    $codmanzana = $_POST["codmanzana"];
	$codsuc		= $_POST["codsuc"];
    
    $sql         = "select rl.codrutlecturas,rml.orden,rml.descripcion ";
    $sql        .= "from public.rutaslecturas as rl ";
    $sql        .= "inner join public.rutasmaelecturas as rml on(rl.codsector=rml.codsector 
					and rl.codrutlecturas=rml.codrutlecturas and rl.codemp=rml.codemp and rl.codsuc=rml.codsuc)";
    $sql        .= "where rl.codsector=? and rl.codmanzanas=? and rl.codemp=1 and rl.codsuc=?" ;
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsector,$codmanzana,$codsuc));
	$item = $consulta->fetch();
	
     echo $item["codrutlecturas"]."|".$item["orden"]."|".strtoupper($item["descripcion"]);
?>
