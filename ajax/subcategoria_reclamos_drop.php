<?php 
	$tiporeclamo 	= $_POST["tiporeclamo"];
	$seleccion		= $_POST["seleccion"];

?>
<select name="subtiporeclamo" id="subtiporeclamo" class="select" style="width:500px">
	<option value="0">--Seleccione la Categoria del Reclamo--</option>
	<?php 
		$selectedA="";
		$selectedB="";
		$selectedC="";
		$selectedD="";
		$selectedE="";
			
		if($tiporeclamo==1)
		{
			if($seleccion=="1A"){$selectedA='selected="selected"';}
			if($seleccion=="1B"){$selectedB='selected="selected"';}
			if($seleccion=="1C"){$selectedC='selected="selected"';}
			
			echo '<option value="1A" '.$selectedA.' >1A=>Problemas en el regimen de facturacion y el nivel de consumo</option>';
			echo '<option value="1B" '.$selectedB.' >1B=>Problemas en la tarifa aplicada al usuario</option>';
			echo '<option value="1C" '.$selectedC.' >1C=>Problemas en otros conceptos facturados al usuario</option>';
		}
		if($tiporeclamo==2)
		{
			if($seleccion=="2A"){$selectedA='selected="selected"';}
			if($seleccion=="2B"){$selectedB='selected="selected"';}
			if($seleccion=="2C"){$selectedC='selected="selected"';}
			if($seleccion=="2D"){$selectedD='selected="selected"';}
			if($seleccion=="2E"){$selectedE='selected="selected"';}

			echo '<option value="2A" '.$selectedA.' >2A=>Problemas relativos al acceso al servicio</option>';
			echo '<option value="2B" '.$selectedB.' >2B=>Problemas relativos a la micromedicion</option>';
			echo '<option value="2C" '.$selectedC.' >2C=>Problemas relativos a cortes indebidos</option>';
			echo '<option value="2D" '.$selectedD.' >2D=>Falta de Entrega de Recibo</option>';
			echo '<option value="2E" '.$selectedE.'>2E=>Problemas relativos a la informacion</option>';
		}
		if($tiporeclamo==3)
		{
			if($seleccion=="3A"){$selectedA='selected="selected"';}
			if($seleccion=="3B"){$selectedB='selected="selected"';}
			if($seleccion=="3C"){$selectedC='selected="selected"';}
			
			echo '<option value="3A" '.$selectedA.' >3A=>Filtraciones </option>';
			echo '<option value="3B" '.$selectedB.' >3B=>Problemas en el servicio de agua potable</option>';
			echo '<option value="3C" '.$selectedC.' >3C=>Problemas en el servicio de alcantarillado</option>';
		}
	?>
</select>