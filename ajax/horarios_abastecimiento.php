<?php 
	include("../config.php");
	
	$codsuc 	= $_POST["codsuc"];
	$codzona	= $_POST["codzona"];
	$codsector	= $_POST["codsector"];
	$codrutlectura	= $_POST["codrutlectura"];
	$sql = "select horas_abastecimiento 
			from public.zonasabastecimiento_sectores
			where codsuc=? and codzona=? and codsector=? AND codrutlecturas=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$codzona,$codsector,$codrutlectura));
	$item = $consulta->fetch();

	$ret  = $item["horas_abastecimiento"]?$item["horas_abastecimiento"]:0;
	if(intval(substr($ret, 3,2))==59)
		echo intval(substr($ret, 0,2))+1;
	else
		echo substr($ret, 0,2);
?>