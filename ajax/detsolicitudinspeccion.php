<?php
	session_name("pnsu");if (!session_start()){session_start();}

	include("../objetos/clsDrop.php");
	
	$objDrop = new clsDrop();
	
	$codsuc = $_POST["codsuc"];
	$nroinspeccion = $_POST["nroinspeccion"];
	$codciclo = $_POST["codciclo"] ? $_POST["codciclo"] : 1;
	
	$sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres,tasapromintant ";
	$sql .= "FROM facturacion.periodofacturacion ";
	$sql .= "WHERE codemp = 1 ";
	$sql .= " AND codsuc = ".$codsuc." ";
	$sql .= " AND codciclo = ".$codciclo." ";
	$sql .= " AND facturacion = 0 ";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array());
	$items = $consulta->fetch();
	
	$anioy = $items["anio"];
	$mesy = $items["mes"];
	
	$items["nrofacturacion"]."|".$items["anio"]."|".$meses[$items["mes"]]."|".$items["mes"]."|".$items["saldo"]."|".$items["lecturas"]."|".$items["tasainteres"]."|".$items['tasapromintant'];

	$Sql = "SELECT d.codconcepto, c.descripcion, d.item, d.subtotal, d.igv, d.redondeo, d.imptotal ";
	$Sql .= "FROM  catastro.detsolicitudinspeccion d ";
	$Sql .= " INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto) ";
	$Sql .= "WHERE d.codemp = 1 ";
	$Sql .= " AND d.codsuc = ".$codsuc." ";
	$Sql .= " AND d.nroinspeccion = ".$nroinspeccion." ";
	$Sql .= "ORDER BY d.item";
	
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array());
	$itemsD = $consulta->fetchAll();
	
	$subtotaltotal = 0;
	$igvtotal = 0;
	$redondeototal = 0;
	$importetotal = 0;
	
	$i = 0;
	
	$ImpTT = 0;

	foreach ($itemsD as $rowD)
	{
		$importetotal	+= $rowD['imptotal'];
		$codconceptox	= $rowD['codconcepto'];
		$detallex		= $rowD['descripcion'];
		
		if ($rowD['codconcepto'] == 5)
		{
			$igvtotal += $rowD['imptotal'];
		}
		else if ($rowD['codconcepto'] == 7 || $rowD['codconcepto'] == 8)
		{
			$redondeototal += $rowD['imptotal'];
		}
		else
		{
			$subtotaltotal += $rowD['subtotal'];

			$i++;
			
			$tr .= "<tr style='background-color:#FFF' id='tr_".$i."'>";
			$tr .= " <td align='center'> 0";
			$tr .= "  <input type='hidden' name='detalle".$i."' value='PAGO POR ".$detallex."' />";
			$tr .= "  <input type='hidden' name='codconceptox".$i."' value='".$codconceptox."' />";
			$tr .= "  <input type='hidden' name='imptotal".$i."' value='".$rowD['subtotal']."' />";
			$tr .= "  <input type='hidden' name='nrofacturacion".$i."' id='nrofacturacion".$i."' value='0' />";
			$tr .= "  <input type='hidden' name='codtipodeuda".$i."' id='codtipodeuda".$i."' value='5' />";
			$tr .= "  <input type='hidden' name='anioy".$i."' id='anioy".$i."' value='".$anioy."' />";
			$tr .= "  <input type='hidden' name='mesy".$i."' id='mesy".$i."' value='".$mesy."' />";
			$tr .= "  <input type='hidden' name='codcategoria".$i."' id='codcategoria".$i."' value='4' />";
			$tr .= " </td>";
			$tr .= " <td align='center'>RSP</td>";
			$tr .= " <td align='center'>0</td>";
			$tr .= " <td align='center'>".$items["anio"]." - ".$meses[$items["mes"]]."</td>";
			$tr .= " <td align='center'>COBRANZA DIRECTAMENTE EN CAJA</td>";
			$tr .= " <td align='center'>PAGO POR ".$rowD['descripcion']."</td>";
			$tr .= " <td align='center'>PRESTACION DE SERVICIOS EN CAJA</td>";
			$tr .= " <td align='right'><input type='hidden' name='npresupuesto".$i."' id='npresupuesto".$i."' value='0' />";
			$tr .= "  <input type='hidden' name='igvadd".$i."' id='igvadd".$i."' value='".$rowD['igv']."' />";
			$tr .= "  <input type='hidden' name='redondeo".$i."' id='redondeo".$i."' value='".$rowD['redondeo']."' />";
			$tr .= "  <label id='lblimptotal".$i."' >".number_format($rowD['imptotal'], 2)."</label>";
			$tr .= " </td>";
			$tr .= " <td align='center'>";
			$tr .= "  <span onclick='open_validar_remover_item(".$i.");' title='Quitar Registro' class='icono-icon-trash'></span> ";
			$tr .= "  <span onclick='open_update_importe_item(".$i.");' title='Modificar Importe' class='icono-icon-dinero'></span>";
			$tr .= " </td>";
			$tr .= "</tr>";
		}
	}


	$data['tr'] = $tr;
	
	$data['cont_cobranza'] = $i;
	$data['subtotaltotal'] = $subtotaltotal;
	$data['igvtotal'] = $igvtotal;
	$data['redondeototal'] = $redondeototal;
	$data['importetotal'] = $importetotal;
	
	echo json_encode($data);
?>