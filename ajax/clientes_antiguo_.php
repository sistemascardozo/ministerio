<?php 
	include("../config.php");
	
	$codsuc 	= $_POST["codsuc"];
	$codantiguo = intval($_POST["codantiguo"]);
	
	$sql = "SELECT c.codcliente, c.propietario, c.codtipodocumento, c.nrodocumento, c.codciclo, tc.descripcioncorta, ";
	$sql .= " cl.descripcion AS calle, c.nrocalle, c.codciclo, td.abreviado, e.descripcion AS estadoservicio, c.codsector, ";
	$sql .= " c.codcalle, se.descripcion AS sector, c.codmanzanas, c.lote, c.codzona, z.descripcion AS zona, ";
	$sql .= " c.nroinscripcion, c.sublote, c.codestadoservicio ";
	$sql .= "FROM catastro.clientes c ";
	$sql .= " INNER JOIN public.calles cl ON(c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.codcalle = cl.codcalle) AND (c.codzona = cl.codzona) ";
	$sql .= " INNER JOIN public.tiposcalle tc ON(cl.codtipocalle = tc.codtipocalle) ";
	$sql .= " INNER JOIN public.tipodocumento td ON(c.codtipodocumento = td.codtipodocumento) ";
	$sql .= " INNER JOIN public.estadoservicio e ON(c.codestadoservicio = e.codestadoservicio) ";
	$sql .= " LEFT JOIN public.sectores se ON(se.codsector = c.codsector) ";
	$sql .= " INNER JOIN admin.zonas z ON(z.codzona= c.codzona) ";
	$sql .= "WHERE c.codemp = 1 AND c.codsuc = ? ";
	$sql .= " AND CAST(c.codantiguo AS INTEGER) = ? ";
	$sql .= " AND c.nroinscripcion NOT IN(0)";
	
	$consulta = $conexion->prepare($sql);
    //print_r($consulta);
	$consulta->execute(array($codsuc, $codantiguo));
	$item = $consulta->fetch();
	
	$data['codcliente']			= $item['codcliente'];
	$data['propietario']		= $item['propietario'];
	$data['codtipodocumento']	= $item['codtipodocumento'];
	$data['nrodocumento']		= $item['nrodocumento'];
	$data['codciclo']			= $item['codciclo'];
	$data['direccion']			= $item["descripcioncorta"]." ".$item["calle"]." ".$item["nrocalle"];
	$data['tdabreviado']		= $item['abreviado'];
	$data['estadoservicio']		= $item['estadoservicio'];
	$data['codsector']			= $item['codsector'];
	$data['codcalle']			= $item['codcalle'];
	$data['nrocalle']			= $item['nrocalle'];
    
    $data['calle']				= $item['calle'];        //11
    $data['sector']				= $item['sector'];       //12
    $data['codmanzanas']		= $item['codmanzanas'];  //13
    $data['lote']				= $item['lote'];         //14
    $data['codzona']			= $item['codzona'];
    $data['zona']				= $item['zona'];  
    $data['nroinscripcion']		= $item['nroinscripcion'];  
    $data['sublote']			= $item['sublote']; 
    $data['codestadoservicio']	= $item['codestadoservicio']; 
	
	echo json_encode($data);

?>