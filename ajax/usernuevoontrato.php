<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../objetos/clsDrop.php"); 
	
	$objDrop = new clsDrop();
	
	$codsuc = $_POST["codsuc"];
	$nroinspeccion = $_POST["nroinspeccion"];
	
	$sql  = "SELECT
            sol.codcliente,
            sol.nroinspeccion,
            sol.nroinscripcion,
            sol.propietario,
            sol.direccion,
            cl.codtiposervicio,
            td.abreviado,
            cl.nrodocumento,
            cl.nromed,
            sol.codzona,
            sol.codsector,
            sol.codmanzanas,
            sol.lote,
            sol.sublote,
            sol.nrocalle,
            sol.codcalle
            FROM
            catastro.solicitudinspeccion AS sol
            LEFT JOIN catastro.clientes AS cl ON cl.codemp = sol.codemp AND cl.codsuc = sol.codsuc AND cl.nroinscripcion = sol.nroinscripcion AND cl.codcliente = sol.codcliente
            LEFT JOIN public.tipodocumento as td on(td.codtipodocumento=cl.codtipodocumento)
            WHERE sol.nroinspeccion=".$nroinspeccion." AND sol.codsuc= ".$codsuc;
	
	$consulta = $conexion->query($sql);
	$item = $consulta->fetch();
        
        $CodAnt= $objDrop->CodUsuario($codsuc, $item['nroinscripcion']);
        
	$data['codcliente']     = $item['codcliente'];
	$data['propietario']    = $item['propietario'];
	$data['nroinspeccion']  = $item['nroinspeccion'];
	$data['codconcepto']    = $item['codconcepto'];
	$data['direccion']      = $item["direccion"];
	$data['codtiposervicio']= $item['codtiposervicio'];
	$data['nrodocumento']   = $item['nrodocumento'];
	$data['nromed']         = $item['nromed'];
	$data['nuevocliente']   = $item['nuevocliente'];
	
	$data['lote']           = $item['lote'];         //14
	$data['codzona']        = $item['codzona'];
	$data['codcalle']       = $item['codcalle'];  
	$data['nroinscripcion'] = $item['nroinscripcion'];  
	$data['sublote']        = $item['sublote']; 
	$data['codmanzanas']    = $item['codmanzanas']; 
	$data['nrocalle']       = $item['nrocalle']; 
        $data['codsector']      = $item['codsector']; 
        $data['codantiguo'] = $CodAnt; 
	echo json_encode($data);

?>