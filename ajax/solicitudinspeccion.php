<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../objetos/clsDrop.php"); 
	
	$objDrop = new clsDrop();
	
	$codsuc 		= $_POST["codsuc"];
	$nroinspeccion = $_POST["nroinspeccion"];
	
	$sql = "SELECT sol.nroinspeccion,upper(sol.propietario) as propietario,sol.codcliente,sol.codsector,
		upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || sol.nrocalle ) as direccion,sol.glosa,
		upper(insp.nombres),upper(tdoc.abreviado),insp.nrodocumento,sol.resuelve,sol.horareg,sol.fechaemision,
		sol.diasatencion,co.descripcion as concepto,sol.codconcepto,sol.nrocalle,zo.descripcion as zona, ";
	//$sql .= " ".$objDrop->getCodCatastral("sol.").", ";
	$sql .= " sol.nroinscripcion,sol.codcalle,sol.codmanzanas,sol.lote,sol.codzona,
		sol.sublote,sol.codconcepto,sol.nuevocliente,sol.correo
		from catastro.solicitudinspeccion as sol 
		inner join public.calles as cal on(sol.codemp=cal.codemp and sol.codsuc=cal.codsuc and sol.codcalle=cal.codcalle and sol.codzona=cal.codzona )
		inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
		inner join reglasnegocio.inspectores as insp on(sol.codinspector=insp.codinspector) and (sol.codsuc=insp.codsuc )
		inner join public.tipodocumento as tdoc on(insp.codtipodocumento=tdoc.codtipodocumento)
		inner join facturacion.conceptos as co on(sol.codsuc=co.codsuc AND sol.codconcepto=co.codconcepto)
		inner join admin.zonas as zo on(sol.codemp=zo.codemp AND sol.codsuc=zo.codsuc AND sol.codzona=zo.codzona)
		WHERE sol.nroinspeccion=? AND sol.codsuc=?";
	
	$consulta = $conexion->prepare($sql);
    //print_r($consulta);
	$consulta->execute(array($nroinspeccion, $codsuc));
	$item = $consulta->fetch();
	$data['codcliente']     = $item['codcliente'];
	$data['propietario']    = $item['propietario'];
	$data['nroinspeccion']  = $item['nroinspeccion'];
	$data['codconcepto']    = $item['codconcepto'];
	$data['direccion']      = $item["direccion"];
	$data['codsector']      = $item['codsector'];
	$data['codcalle']       = $item['codcalle'];
	$data['nrocalle']       = $item['nrocalle'];
	$data['nuevocliente']   = $item['nuevocliente'];
	
	$data['lote']           = $item['lote'];         //14
	$data['codzona']        = $item['codzona'];
	$data['zona']           = $item['zona'];  
	$data['nroinscripcion'] = $item['nroinscripcion'];  
	$data['sublote']        = $item['sublote']; 
	$data['codmanzanas']    = $item['codmanzanas']; 
	$data['correo']         = $item['correo']; 
	echo json_encode($data);

?>