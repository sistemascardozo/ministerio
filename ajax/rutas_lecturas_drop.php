<?php 
	include("../config.php");
	
	$codsuc 	= $_POST["codsuc"];
	$codsector 	= $_POST["codsector"];
	$condicion	= $_POST["condicion"];
	$disabled	= "";
	
	if($condicion == 1)
	{
		$disabled = "disabled='disabled'";
	}
	
	$sql = "SELECT codrutlecturas, descripcion FROM public.rutasmaelecturas WHERE codsuc=? and codsector=? ORDER BY codrutlecturas ASC";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$codsector));
	$items = $consulta->fetchAll();

	echo "<select class='select' style='width:220px' name='rutaslecturas' id='rutaslecturas'  ".$disabled." >";
	echo "<option value='%' >--Seleccione la Ruta--</option>";
	
	foreach($items as $row)
	{
		echo "<option value='".$row["codrutlecturas"]."' >".strtoupper($row["descripcion"])."</option>";
	}
	
	echo "</select>";	
	
?>