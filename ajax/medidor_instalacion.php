<?php 
    session_name("pnsu");
    if(!session_start()){session_start();}

    include("../config.php");
    include("../include/main.php");
    
    $codsuc = $_POST["codsuc"];
    $Id = $_POST["Id"];
    $ver= $Id / $Id;
    if($ver==1)
    {
        $Id= $Id;
    }
    else{
        $v= explode( '-', $Id );
        $Id= $v[1];
    } 

    /* DATOS DEL MEDIDOR */
    $sql1 = "SELECT DISTINCT
        d.coddetmedidor, d.codcliente,
        d.nroinscripcion, c.propietario,
        tc.descripcioncorta||' '||cl.descripcion AS direccion,
        c.nrocalle, d.nromed,
        d.codinspector, d.marcamed,
        d.coddiametro, d.estadomedidores,
        d.posicionmed,
        d.modelomed, d.tipolectura,
        d.aniofabmed, d.codtipomedidor,
        d.codcapacidadmedidor,
        d.codclasemetrologica, d.tipofacturacion,
        d.ubicacionllavemedidor, d.lecturaanterior,
        ".$objFunciones->FormatFecha('d.fechalecturaanterior')." AS fechalecturaanterior,
        d.lecturaultima,
        ".$objFunciones->FormatFecha('d.fechalecturaultima')." AS fechalecturaultima,
        d.consumo, d.lecturapromedio, d.filtromed,
        d.precintomed, d.filtromedbueno,
        d.observacion, d.trasladomed, d.estareg,
        d.codtiporetiromed, d.codempcont, d.tpcontrastacion,
        d.codtiporetiromed
        
        FROM
        micromedicion.detmedidor AS d
        INNER JOIN catastro.clientes AS c ON c.codcliente = d.codcliente AND c.nroinscripcion = d.nroinscripcion
        INNER JOIN public.calles AS cl ON (c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona)
        INNER JOIN public.tiposcalle AS tc ON (cl.codtipocalle = tc.codtipocalle)
        LEFT JOIN reglasnegocio.inspectores AS inp ON (inp.codinspector = d.codinspector AND inp.codemp= d.codemp AND  inp.codsuc = d.codsuc) 
        
        WHERE d.codemp=1 and d.codsuc=$codsuc and d.coddetmedidor=$Id ";

    $consulta = $conexion->prepare($sql1);
    //print_r($consulta);
    //$consulta->execute(array($codsuc,$Id));
    $consulta->execute(array());
    $item = $consulta->fetch();
    
    $data['codcliente']   = $item['codcliente'];
    $data['propietario']  = $item['propietario'];    
    $data['direccion']    = $item["direccion"];    
    $data['nrocalle']     = $item['nrocalle'];
    $data['nroinscripcion'] = $item['nroinscripcion'];
    
    $data['coddetmedidor']  = $item['coddetmedidor'];
    $data['codinspector']   = $item['codinspector'];
    $data['nromed']    = $item['nromed'];        
    $data['codmarca']  = $item['marcamed'];       
    $data['coddiametrosmedidor']  = $item['coddiametro']; 
    $data['codestadomedidor']     = $item['estadomedidores'];    
    $data['posicionmed']       = $item['posicionmed'];
    $data['codmodelo']         = $item['modelomed'];  
    $data['tipolectura']       = $item['tipolectura'];
    $data['aniofabmed']        = $item['aniofabmed'];    
    $data['codcapacidadmedidor']   = $item['codcapacidadmedidor'];
    $data['tipofacturacion']   = $item['tipofacturacion'];
    $data['codubicacion']      = $item['ubicacionllavemedidor'];
    $data['lecturaanterior']   = $item['lecturaanterior'];
    $data['fechalecturaanterior']  = $item['fechalecturaanterior'];
    $data['lecturaultima']     = $item['lecturaultima'];
    $data['fechalecturaultima']= $item['fechalecturaultima'];
    $data['consumo']           = $item['consumo'];
    $data['lecturapromedio']   = $item['lecturapromedio'];    
    $data['codtipomedidor']    = $item['codtipomedidor'];
    
    $data['observacion']    = $item['observacion'];
    $data['precintomed']    = $item['precintomed'];
       
    $data['codempcont']     = $item['codempcont']; 
    $data['codtiporetiromed'] = $item['codtiporetiromed'];
    $data['codclasemetrologica'] = $item['codclasemetrologica'];
    
    echo json_encode($data);

?>
