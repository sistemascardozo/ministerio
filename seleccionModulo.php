<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("include/main.php");
		
	$IdUsuario 				= $_SESSION['id_user'];
	$idsistema 				= $objSeguridad->desencriptar($_GET["idsistema"]);
	$_SESSION['idsiste']	= $idsistema;
	
	$consultaC = $conexion->prepare("SELECT carpeta FROM seguridad.sistemas WHERE codsistema = ?");
	$consultaC->execute(array($idsistema));
	$itemC = $consultaC->fetch();

	$_SESSION['carpeta'] = $itemC[0];
	
	$SqlM = "SELECT usubP.codsubsistema, subSist.descripcion, subSist.imagen, usubP.codperfil ";
	$SqlM .= "FROM seguridad.usuarios_subsistemas_perfil AS usubP ";
	$SqlM .= " INNER JOIN seguridad.subsistemas AS subSist ON(usubP.codsubsistema = subSist.codsubsistema) ";
	$SqlM .= "WHERE usubP.codusu = ? AND subSist.codsistema = ? AND subSist.estareg = 1 ORDER BY subSist.orden";
	
	$consulta = $conexion->prepare($SqlM);
	$consulta->execute(array($IdUsuario, $idsistema));
	$items=$consulta->fetchAll();
	
	$IdProceso = 0;
	
	CuerpoSuperior();
?>

<script>
$("#DivTituloCobranza",window.parent.document).html('')	
	function IrModulo(perfil, idsistema, sistema)
	{
		$.ajax({
			 url:'include/session_sistema.php',
			 type:'POST',
			 async:true,
			 data:'idsistema=' + idsistema + '&idperfil=' + perfil + '&sistema=' + sistema,
			 success:function(datos){ 	
				location.href = 'admin/index.php';
			 }
		}) 
	}
</script>
<div style="height:10px">
</div>
<div class="selec-mod2">
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" >
	<tr>
        <td width="61">&nbsp;</td>
        <td width="690">&nbsp;</td>
        <td width="41">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php 
	$tr = 4;
	
	foreach($items as $row)
	{
		if($tr == 4)
		{
			echo "<tr>";
			echo "<td colspan='6'>&nbsp;</td>";
			echo "</tr>";
			echo "<tr>";
			$tr = 0;
		}
?>
                    <td align="center">
                        <div id="tblogin" onclick="IrModulo(<?php echo $row["codperfil"];?>, <?php echo $row["codsubsistema"];?>, '<?php echo strtoupper($row["descripcion"]);?>')">
                            <img src="<?php echo $urldir;?>seguridad/subsistemas/imagenes/<?php echo $row["imagen"];?>" width="45" height="45" />
                            <div><?php echo strtoupper($row["descripcion"]);?></div>
                        </div>  
                    </td>
                    <td style="width:20px"></td>
<?php
		$tr += 1;
	}
?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table> 
		</td>
		<td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><div id="div_progressbar"></div></td>
        <td>&nbsp;</td>
	</tr>
</table>
</div>
<script>
	$("#NombreModulo").html('Seleccione un Modulo');
</script>
<?php
	CuerpoInferior();
?>