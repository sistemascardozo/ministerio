<?php

  include("../config.php");

  $Id = isset($_POST["IdUsuario"])?$_POST["IdUsuario"]:'';

  if($Id!='')
  {
    $Sql   = "SELECT codusu,dni, nombres, login, contra, estareg ";
    $Sql   = $Sql." FROM seguridad.usuarios WHERE codusu = ".$Id."";

    $row    = $conexion->query($Sql)->fetch();
    $Estado = $row['estareg'];

  }

?>
<script type="text/javascript">
function ValidarForm()
  {

     if (Trim($("#Clave").val())=='')
    {

      Error('#Clave','Digite la Clave de Acceso al Sistema',1000,'above','error',true)

      return false;
    }
    $("#Clave").removeClass( "ui-state-error" );
    if (Trim($("#Clave").val())!=Trim($("#ClaveConfirmar").val()))
    {

      Error('#ClaveConfirmar','Las Claves no Coinciden',1000,'above','error',true)

      return false;
    }
    //GuardarForm('Usuario',1)
    var Data = $("#form1").serialize();
    Accion = ["Insertar", "Modificar", "Anular", "Insertado", "Modificado", "Anulado"];
    $.ajax({
       url:'<?=$urldir?>/pnsu/include/PasswordSave.php',
       type:'POST',
       async:true,
       data: Data,
       success:function(data){
         if (parseInt(data)==0)
          {

            OperMensaje(1)
           $( '#DivUpdatePass' ).dialog( "close" );
           setTimeout( CerrarSesion(),2000)

          }
        else
        {
           Msj($("#ClaveActual"),'No se puudo actualizar la contraseña')


        }
       }
       })
  }
</script>
<form id="form1" name="form1">
<table style="font-size:12px; color:#000; background-color:#FFF" width="100%" border="0">
        <tr>
    <td class="TitDetalle">Nombres:</td>
    <td>
    <input type="text" class="inputtext ui-corner-all text"  id="Nombres" size="40"  value="<?=($row['nombres'])?>" readonly="readonly"/>
    <input type="hidden" name="codusu"  value="<?=$row['codusu']?>" id="Id"  />
    <input type="hidden" name="0form1_cambioclave" value="0">
  </td>
  </tr>
   <tr>
    <td class="TitDetalle">Login:</td>
    <td><input type="text" class="inputtext ui-corner-all text" id="Login" size="20" value="<?=($row['login'])?>" readonly="readonly"/></td>
  </tr>
   <tr>
    <td class="TitDetalle">Actual:</td>
    <td><input type="password" class="inputtext ui-corner-all text" name="ClaveActual" id="ClaveActual" size="20"  onkeypress="CambiarFoco(event, 'Clave')"/></td>
  </tr>
   <tr>
    <td class="TitDetalle">Clave:</td>
    <td><input type="password" class="inputtext ui-corner-all text" name="0form1_clave" id="Clave" size="20"  onkeypress="CambiarFoco(event, 'ClaveConfirmar')"/></td>
  </tr>
  <tr>
    <td class="TitDetalle">Confirma Clave:</td>
    <td><input type="password" class="inputtext ui-corner-all text" id="ClaveConfirmar" size="20"  onkeypress="ValidarFormEnt(event)"/></td>
  </tr>
    </table>
  </form>
