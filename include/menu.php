<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include($_SESSION['path']."config.php");
	
	$IdSistema  = $_SESSION['idsistema'];
	$IdPerfil	= $_SESSION['idperfil'];
	$idsucursal	= isset($_SESSION['IdSucursal'])?$_SESSION['IdSucursal']:0;
	$carpeta	= $_SESSION['carpeta'];
	$capetaSystem = $carpeta;

	$menu_1 = MostrarMenu($IdSistema, $IdPerfil);
	
	foreach($menu_1 as $row)
	{
?>
<li><a href="#"><?php echo $row[1];?></a>
<?php 
	echo CargarD($row[0], $IdPerfil, $IdSistema);
?>
</li>
<?php
	}

	function CargarD($idpadre, $idperfil, $idsistema)
	{
		global $conexion, $urldir;
		
		$html = '';
		$html3 = '';
		
		$SqlSP = "SELECT subPerf.codmodulo, mod.descripcion, mod.url ";
		$SqlSP .= "FROM seguridad.subsistema_perfiles AS subPerf ";
		$SqlSP .= " INNER JOIN seguridad.modulos AS mod ON(subPerf.codmodulo = mod.codmodulo AND subPerf.codsubsistema = mod.codsubsistema) ";
		$SqlSP .= "WHERE subPerf.codsubsistema = ? AND subPerf.codperfil = ? AND mod.idpadre = ? AND mod.estareg = 1 ";
		$SqlSP .= "ORDER BY mod.orden";
		
		$consulta = $conexion->prepare($SqlSP);
		$consulta->execute(array($idsistema, $idperfil, $idpadre));
		$menuD = $consulta->fetchAll();
		
		$Cont = '';
		$html2 = '';
		
		foreach($menuD as $row2)
		{			
			$NUmm = CantidadRegistro($idsistema, $idperfil, $row2[0]);
			
			if ($NUmm != 0)
			{
				$html2 = $html2.'<li><a href="#">'.$row2[1].'<img src="'.$_SESSION['urldir'].'images/iconos/arrow_w.png" height="15" /></a>'.CargarD($row2[0], $idperfil, $idsistema).'</li>';
			}
			else
			{
				$html2 = $html2.'<li><a href="'.$_SESSION['urldir'].$row2[2].'?im='.$idsistema.'">'.$row2[1].'</a></li>';
			}
		}
		
		if ($NUmm != '')
		{
			$html = $html.'<ul>';
			$html = $html.$html2;
			$html = $html.'</ul>';
		}
		return $html;
	}
	
	function MostrarMenu($idsistema, $idperfil, $idpadre = 0)
	{
		global $conexion;
		
		$Sql = "SELECT subPerf.codmodulo, mod.descripcion, mod.url ";
		$Sql .= "FROM seguridad.subsistema_perfiles AS subPerf ";
		$Sql .= " INNER JOIN seguridad.modulos AS mod ON(subPerf.codmodulo = mod.codmodulo AND subPerf.codsubsistema = mod.codsubsistema) ";
		$Sql .= "WHERE subPerf.codsubsistema = ".$idsistema." AND subPerf.codperfil = ".$idperfil." AND mod.idpadre = ".$idpadre." AND mod.estareg = 1 ";
		$Sql .= "ORDER BY mod.orden";
		
		$consulta = $conexion->prepare($Sql);
		$consulta->execute(array());
		
		return $consulta->fetchAll();
	}
	
	function CantidadRegistro($idsistema, $idperfil, $idpadre = 0)
	{
		global $conexion;
		
		$SqlSP = "SELECT COUNT(*) ";
		$SqlSP .= "FROM seguridad.subsistema_perfiles AS subPerf ";
		$SqlSP .= " INNER JOIN seguridad.modulos AS mod ON(subPerf.codmodulo = mod.codmodulo AND subPerf.codsubsistema = mod.codsubsistema) ";
		$SqlSP .= "WHERE subPerf.codsubsistema = ? AND subPerf.codperfil = ? AND mod.idpadre = ?";
		
		$consulta = $conexion->prepare($SqlSP);
		$consulta->execute(array($idsistema, $idperfil, $idpadre));
		$item=$consulta->fetch();
		
		return $item[0];		
	}
?>
