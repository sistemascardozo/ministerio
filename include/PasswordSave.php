<?php
session_name("pnsu");
if(!session_start()){session_start();}


include($_SESSION['path'].'objetos/clsFunciones.php');
include($_SESSION['path']."objetos/clsseguridad.php");

$conexion->beginTransaction();
$objFunciones = new clsFunciones();
$objSeguridad = new clsSeguridad();
$codusu       = $_POST["codusu"];
$contra       = strtoupper($_POST["0form1_clave"]);
$contra       = $objSeguridad->encriptar($contra);

$ClaveActual		= strtoupper(trim($_POST["ClaveActual"]));
$ClaveActual = strtoupper($objSeguridad->encriptar($ClaveActual));
$consulta = $conexion->prepare("select * from seguridad.usuarios where codusu=? and trim(upper(contra))=? and estareg=1 ");
$consulta->execute(array($codusu,$ClaveActual));
$row	= $consulta->fetch();

if($row[0]!='')
	{
		$sql  = "update seguridad.usuarios set contra=:contra where codusu=:codusu";
		$result = $conexion->prepare($sql);
		$result->execute(array(":codusu"=>$codusu,":contra"=>trim($contra)));
		if(!$result)
			{
				$conexion->rollBack();
				var_dump($result->errorInfo());
				die('1');
			}else{
				$conexion->commit();
				$mensaje = "El Registro se ha Grabado Correctamente";
				die('0');
			}
	}
	else
	{
		die('10');
	}

	?>
