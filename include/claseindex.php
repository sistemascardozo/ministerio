<?php
	$TAMANO_PAGINA = 10;

    //capturas la pagina en la q estas
	if (isset($_GET['pagina']))
	{
		$pagina = $_GET["pagina"];
	}
	else
	{
		$pagina = '';
	}

    //si estas en la primera pagina le asignas los valores iniciales
	if (!$pagina)
	{
		$inicio = 0;
		$pagina = 1;
	}
	else
	{
		$inicio = ($pagina - 1) * $TAMANO_PAGINA;
	}
	
    function Cabecera($Previo = '', $Tamaño = 700 , $PopUpW = 550, $PopUpH = 450)
	{
		global $urldir, $Fecha, $Valor, $Op, $num_total_registros, $TituloVentana, $IdUsuario, $Criterio, $pagina, $TAMANO_PAGINA, $FormatoGrilla, $codsuc, $total_paginas;
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery-1.8.3.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/pagination.css" />
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/FuncionesGrilla.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery.pagination.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/jquery-ui-1.10.4.custom.min.css" />
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery_sistema.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/interface.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/interface_menu.css">
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones_interface.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/css_base.css" />

<script>
    var total_paginas = '<?php echo $total_paginas;?>';
    var urldir = '<?php echo $_SESSION['urldir'];?>';
	
	$(document).ready(function() {
		if (<?php echo $Op;?> >= 5)
		{
			$("#BtnNuevoB").remove();
		}

		$(document).on('focusin', '#ListaMenu tbody tr', function(event){$(this).addClass('ui-state-hover');});
		$(document).on('focusout', '#ListaMenu tbody tr', function(event){$(this).removeClass('ui-state-hover');});

		$(document).bind('keydown',function(e){
			switch(e.keyCode)
			{
				case $.ui.keyCode.ESCAPE: 
<?php 
	if($Op >= 5)
	{
?>
					window.close();
<?php 
	}
?>
					break;
					
				case $.ui.keyCode.END:
					if(Pagina!=total_paginas)
					{
						Pagina = total_paginas - 1; Buscar(<?php echo $Op;?>);
					}
					
					break;
					
				case $.ui.keyCode.HOME:
					if(Pagina != 1)
					{
						Pagina = 1;
						Buscar(<?php echo $Op;?>);
					}
							
                    break;
					
				case $.ui.keyCode.PAGE_DOWN:
					if(Pagina < '<?php echo $total_paginas;?>')
					{
						Pagina++;
						Buscar(<?php echo $Op;?>);
					}
					
					break;
					
				case $.ui.keyCode.PAGE_UP:
					if(Pagina > 1)
					{
						Pagina--;
						Buscar(<?php echo $Op;?>);
					}
					
					break;
			}
		}); 
		
////////////
		$("#ConfirmaGuardar").dialog({	//Ventanas de Confirmaci�n de Operaci�n
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "Confirmaci&oacute;n de Operaci&oacute;n",
			width: 350,
			height: 155,
			//show: "scale",
			//hide: "scale",
			buttons: {
				"Aceptar": function() {
					ValidarForm(Op);
					
					$(this).dialog("close");
  				},
				Cancelar: function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#ConfirmaEliminacion").dialog({	//Ventanas de Confirmaci�n de Eliminaci�n
			autoOpen: false,
			modal: true,
			resizable: false,
			//title: "Confirmaci�n de Eliminaci�n",
			width: 350,
			height: 155,
			//show: "scale",
			//hide: "scale",
			/*close: function() { 
			$("#button-eliminar").unbind("click" );
			$("#button-eaceptar").click(function(){ValidarForm(2);});
			//$("#button-maceptar").children('span.ui-button-text').text('Actualizar')
			//$("#Modificar").dialog({title:"<?= $TituloVentana ?> - Modificar Registro"});
			},*/
			buttons: {
				"Eliminar": function() {
					ValidarForm(2);
				},
				Cancelar: function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#ConfirmaRestauracion").dialog({	//Ventanas de Confirmaci�n de Restauraci�n
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "Confirmaci&oacute;n de Restauraci&oacute;n",
			width: 350,
			height: 155,
			//show: "scale",
			//hide: "scale",
			buttons: {
				"Restaurar": function() {
					ValidarForm(3);
				},
				Cancelar: function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#ConfirmaImprimir").dialog({	//Ventanas de Confirmaci�n de Operaci�n
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "Confirmaci&oacute;n de Operaci&oacute;n",
			width: 350,
			height: 155,
			//show: "scale",
			//hide: "scale",
			buttons: {
				"Aceptar": function() {
					Imprimir();
					
					$("#ConfirmaImprimir").dialog("close");
				},
				Cancelar: function() {
					$(this).dialog("close");
				}
			}
		});

		$("#Nuevo").dialog({			//Ventanas de Dialogo Modal	para Agregar un Nuevo Registro
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "<?php echo $TituloVentana;?> - Agregar Registro",
			width: <?php echo $PopUpW;?>,
			height: <?php echo $PopUpH;?>,
			show: "scale",
			hide: "scale",
			buttons: {
				"Agregar": function() {
					ValidarForm(0);
				},
				Cancelar: function() {
					$("#DivNuevo").html('');
					$("img#img-cargando_guardar").remove();
					$("#button-maceptar").css("opacity", '1');
					$("#button-maceptar").removeAttr("disabled");
					$("#button-maceptar").children('span.ui-button-text').text('Agregar');
					$(this).dialog("close");
				}
			}
		});
		
		$("#Modificar").dialog({		//Ventanas de Dialogo Modal	para Modificar un Registro
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "<?= $TituloVentana ?> - Modificar Registro",
			width: <?= $PopUpW ?>,
			height: <?= $PopUpH ?>,
			show: "scale",
			hide: "scale",
			close: function() {
				$("#button-maceptar").unbind("click");
				
				$("#button-maceptar").click(function() {
					ValidarForm(1);
				});
				
				$("#button-maceptar").children('span.ui-button-text').text('Actualizar')
				$("#Modificar").dialog({title: "<?php echo $TituloVentana;?> - Modificar Registro"});
			},
			buttons: [
				{
					id: "button-maceptar",
					text: "Actualizar",
					click: function() {
						ValidarForm(1);
					}
				},
				{
					id: "button-mcancelar",
					text: "Cancelar",
					click: function() {
						$("#DivModificar").html('');
						$(this).dialog("close");
					}
				}
			]
		});
		
		Buscar(<?php echo $Op;?>);
		
		$('#Valor').focus();
	});
	
	///////////////
	function TabIndexIndex(e)
	{
		switch(e.keyCode)
        {   
            case $.ui.keyCode.UP: 
               for (var i = parseInt($(this).attr('tabindex')) - 1; i > 0; i--) 
                {
                    var obj = $('tr[tabindex=\'' + i + '\']')
                    if (obj != null )
                      {
                        if(obj.is(':visible') && $(obj).parents(':hidden').length == 0)
                        {
                          $('tr[tabindex=\'' + i + '\']').focus();
                           e.preventDefault();
                           return false;
                       }
                       
                     }
                };
                $('#Valor').focus().select();

            break;
            case $.ui.keyCode.DOWN:
               for (var i = parseInt($(this).attr('tabindex'))+1; i <= 15; i++) 
                {
                    var obj = $('tr[tabindex=\'' +i+ '\']')
                    if (obj != null )
                      {
                        if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                        {
                          $('tr[tabindex=\'' + i  + '\']').focus();
                           e.preventDefault();
                           return false;
                       }
                       
                     }
                };
                $('#Valor').focus().select();
            break;
            case $.ui.keyCode.RIGHT:
                    if(Pagina<'<?php echo $total_paginas;?>')
                    {
                        
                        Pagina++; Buscar();
                    }
            break;
            case $.ui.keyCode.LEFT:
                    if(Pagina>1)
                    {
                        
                        Pagina--; Buscar();
                    }
            break;
            case $.ui.keyCode.LEFT:
                    if(Pagina>1)
                    {
                        
                        Pagina--; Buscar();
                    }
            break;
            
        }
     
}
 function ValidarEnterTr(e,obj)
  {

    
    if (VeriEnter(e) )
      { 
     
        $(obj).children('td').last().children('img.1').click();
                
      }
      e.returnValue = false;
    }
////////////////
        function Mostrar(Id, Op)
        {
            if (Op == 0)
            {
                $("#Nuevo").dialog("open");
                $("#DivNuevo").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
            }
            if (Op == 1)
            {
                $("#Modificar").dialog("open");
                $("#DivModificar").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
            }
            if (Op == 2)
            {
                $("#EliminarDiv").dialog("open");
                $("#DivEliminar").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
            }
            if (Op == 3)
            {
                $("#Restaurar").dialog("open");
                $("#DivRestaurar").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
            }
            if (Op == 4)
            {
                $("#Ver").dialog("open");
                $("#DivVer").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
            }
            $("#form1").remove();
			
			var Id2 = $('#' + Id).data('idreg')?$('#' + Id).data('idreg'):'';
			var CId = $('#' + Id).data('valor')?$('#' + Id).data('valor'):'';
    		var CNombres = $('#' + Id).data('nombres')?$('#' + Id).data('nombres'):'';
			var Campo = '&' + CNombres + '=' + CId;

			if (CId != '')
			{
				Campo = '&' + CNombres + '=' + CId;
			}
			else
			{
				Id2 = Id.substr(Id.indexOf('-') + 1);
			}
			//alert(Id2);
            $.ajax({
                url: 'mantenimiento.php',
                type: 'POST',
                async: true,
                data: 'Op=' + Op + '&Id=' + Id2 + '&codsuc=<?php echo $codsuc;?>' + Campo,
                success: function(data) {
                    //alert(data);
                    if (Op == 0) {
                        $("#DivNuevo").html(data);
                    }
                    if (Op == 1) {
                        $("#DivModificar").html(data);
                    }
                    if (Op == 2) {
                        $("#DivEliminar").html(data);
                    }
                    if (Op == 3) {
                        $("#DivRestaurar").html(data);
                    }
                    if (Op == 4) {
                        $("#DivVer").html(data);
                    }
                    $("#" + Foco).focus();
                }
            })
        }
        function Eliminar(Id)
        {
			var Id2 = $('#' + Id).data('idreg')?$('#' + Id).data('idreg'):'';
			var CId = $('#' + Id).data('valor')?$('#' + Id).data('valor'):'';
    		var CNombres = $('#' + Id).data('nombres')?$('#' + Id).data('nombres'):'';
			var Campo = '';
			//alert(CId);
			if (CId != '')
			{
				Campo = '<input type="hidden" name="' + CNombres + '" id="Id" value="' + CId + '" />';
			}
			else
			{
				Id2 = Id.substr(Id.indexOf('-') + 1);
			}
	
            $("#DivEliminacion").html('<form id="form1" name="form1">' + Campo + '<input type="hidden" name="1form1_id" id="Id" value="' + Id2 + '" /><input type="hidden" name="estareg" id="estareg" value="0" /></form>');
            $("#ConfirmaEliminacion").dialog("open");
        }
        function Restablecer(Id)
        {
			var Id2 = $('#' + Id).data('idreg')?$('#' + Id).data('idreg'):'';
			var CId = $('#' + Id).data('valor')?$('#' + Id).data('valor'):'';
    		var CNombres = $('#' + Id).data('nombres')?$('#' + Id).data('nombres'):'';
			var Campo = '';
			//alert(CId);
			if (CId != '')
			{
				Campo = '<input type="hidden" name="' + Nombres + '" id="Id" value="' + CId + '" />';
			}
			else
			{
				Id2 = Id.substr(Id.indexOf('-') + 1);
			}
			//alert(Id2);
            $("#DivRestaurar").html('<form id="form1" name="form1">' + Campo + '<input type="hidden" name="1form1_id" id="Id" value="' + Id2 + '" /><input type="hidden" name="estareg" id="estareg" value="1" /></form>');
            $("#ConfirmaRestauracion").dialog("open");
        }

        function GuardarP(Op)
        {	//alert(Op)
            $.ajax({
                url: 'guardar.php?Op=' + Op,
                type: 'POST',
                async: false,
                data: $('#form1').serialize(), // + '&0form1_idusuario=<?=$IdUsuario?>&3form1_fechareg=<?=$Fecha?>',
                success: function(data) {
                    OperMensaje(data)
                    $("#Mensajes").html(data);
                    $("#DivNuevo, #DivModificar, #DivEliminar, #DivEliminacion, DivRestaurar").html('');

                    $("#Nuevo").dialog("close");
                    $("#Modificar").dialog("close");
                    //$("#EliminarDiv").dialog("close");
                    $("#ConfirmaEliminacion").dialog("close");
                    $("#ConfirmaRestauracion").dialog("close");
                    Buscar(Op);
                }
            })
        }

        ///////////////
        var Foco = 'Descripcion';
        function ValidarForm(Op)
        {
            GuardarP(Op);
        }
        var Id = ''
        var Id2 = ''
        var IdAnt = ''
        var Pagina = <?php echo $pagina;?>;
        var nPag = <?php echo $TAMANO_PAGINA;?>;

        function Buscar(Op)
        {
            $.ajax({
                url: '<?php echo $_SESSION['urldir'];?>include/grilla.php',
                type: 'POST',
                async: true,
                data: 'Pagina=' + Pagina + '&Valor=' + $('#Valor').val() + '&Criterio=<?php echo $Criterio;?>',
                success: function(data) {
                	//alert(data);
                    $("#DivDetalle").html(data);

                    $('#TotalReg').html($('#NumReg').val());

                    var optInit = getOptionsFromForm();

                    $("#Pagination").pagination($('#NumReg').val(), optInit);

                    $(document).on('mouseover', "#ListaMenu tbody tr", function(event) {
                        $(this).addClass('ui-state-active');
                    });
                    $(document).on('mouseout', "#ListaMenu tbody tr", function(event) {
                        $(this).removeClass('ui-state-active');
                    });
                    $(document).on('dblclick', "#ListaMenu tbody tr", function(event) {
                        $(this).children('td').last().children('img.1').click();
                    });

                    $("#ListaMenu tbody").addClass("ui-widget-content");
                    $('#Valor').focus();

                    var nroinput=0;

                    $('#ListaMenu tbody tr').each(function(){nroinput++; $(this).attr('tabindex', nroinput);
                        $(this).attr('onkeypress', "ValidarEnterTr(event,this);");
                    });

                    tb = $('#ListaMenu tbody tr');

                    if ($.browser.mozilla) $(tb).keypress(TabIndexIndex);
                    else $(tb).keydown(TabIndexIndex);


                }
            })

        }

        function ValidarEnter(e, Op)
        {
        switch(e.keyCode)
            {   
                case $.ui.keyCode.UP: 
                    for (var i = 10; i >= 1; i--) 
                        {
                            if ($('tr[tabindex=\'' + i + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' + i + '\']')
                                if(obj.is(':visible') && $(obj).parents(':hidden').length == 0)
                                {
                                  $('tr[tabindex=\'' + i + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }
                               
                             }
                        };
                        $('#Valor').focus().select();
                break;
                case $.ui.keyCode.DOWN:

                       for (var i = 1; i <= 10; i++) 
                        {
                            if ($('tr[tabindex=\'' + i + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' + i + '\']')
                                if(obj.is(':visible') && $(obj).parents(':hidden').length == 0)
                                {
                                  $('tr[tabindex=\'' + i + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }
                               
                             }
                        };

                        $('#Valor').focus().select();
                break;
                default: Buscar(Op);
                
                
            }
             
        
            //ValidarEnterG(evt, Op)
        }
        function Regresar()
        {
            history.back();
        }
</script>
<div align="center">
	<table width="<?php echo $Tamaño;?>" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" id="ListaMenu" style="display:none;">
					<thead>
						<tr>
							<th scope="col" style="font-size:18px" align="center" height="30"><?php echo $TituloVentana;?>&nbsp;</th>
						</tr>
					</thead>
				</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center">
				<table width="<?php echo $Tamaño;?>" border="0" cellspacing="0" cellpadding="0" id="tbBusqueda">
					<tr>
						<td width="10" height="10" >&nbsp;</td>
						<td>&nbsp;</td>
						<td width="10" >&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2"><?php echo $Previo;?></td>
                    </tr>
                    <tr>
						<td>&nbsp;</td>
						<td>
							<table width="100%" border="0" bgcolor="#E5E5E5">
								<tr style="font-size:14px">
                                    <td valign="middle" align="left" width="65">Buscar :&nbsp;</td>
                                    <td valign="middle" align="left">
										<input type="text" name="Valor" id="Valor" style="width:100%" value="<?php echo $Valor;?>" onkeyup="ValidarEnter(event, '<?php echo $Op;?>');" class="inputtext" />
									</td>
									<td width="150">
										<span class="MljSoft-icon-buscar" onclick="Buscar('<?php echo $Op;?>');" title="Refrescar"></span>&nbsp;
										<span class="MljSoft-icon-nuevo" id="BtnNuevoB" onclick="Mostrar('', 0);" title="Nuevo Registro"></span>
										<div style="display:inline">
											<input type="button" onclick="Regresar()" value="Regresar" id="BtnRegresar" title="Regresar" style="display:none">
										</div>
									</td>
									<td align="right" width="160">Total Registros :&nbsp;<label id="TotalReg"><?php echo $num_total_registros;?></label></td>
								</tr>
							</table>
						</td>
						<td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="15">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
					</tr>
				</table><br />
				<table width="<?php echo $Tamaño;?>" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="4" valign="top">
							<div id = "DivDetalle"></div>
<?php
	}

//*************************************
	function Pie()
	{
		global $urldir, $num_total_registros, $pagina, $total_paginas, $TAMANO_PAGINA, $Op;

		if ($Op == '')
		{
			$Op = 0;
		}
?>
						</td>
                    </tr>
                    <tr>
						<td colspan="4" style="font-size:10px">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<script>
	//Paginaci�n
	function pageselectCallback(page_index, jq)
	{
		// Get number of elements per pagionation page from form
		var max_elem = Math.min((page_index + 1) * <?php echo $TAMANO_PAGINA;?>, $('#NumReg').val());

		// Prevent click event propagation
		return false;
	}

	function getOptionsFromForm()
	{
		var opt = {callback: pageselectCallback};
		// Collect options from the text fields - the fields are named like their option counterparts
		opt['items_per_page'] = <?php echo $TAMANO_PAGINA;?>;
		opt['num_display_entries'] = 5;
		opt['num_edge_entries'] = 1;
		opt['prev_text'] = "<< ";
		opt['next_text'] = " >>";
		opt['current_page'] = Pagina;
		opt['op'] = <?php echo $Op;?>;
		// Avoid html injections in this demo
		return opt;
    }
</script>
<div id="Nuevo" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivNuevo" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="Modificar" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivModificar" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="EliminarDiv" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivEliminar" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="Restaurar" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivRestaurar" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="Ver" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivVer" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="Imprimir" title="Imprimir" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivImprimir" style="width:100%"></div></td>
        </tr>
        <tr>
            <td>�Desea Imprimir?</td>
        </tr>
    </table>
</div>
<div id="ConfirmaEliminacion" title="Confirmaci&oacute;n de Eliminaci&oacute;n" style="display:none;">
    <table width="100%">
        <tr>
            <td style="font-size:10px"><div id="DivEliminacion" style="width:100%">&nbsp;</div></td>
        </tr>
        <tr>
            <td><span class="ui-icon ui-icon-trash" style="float:left; margin:0 7px 20px 0;"></span>&iquest;Desea Anular este Registro?</td>
        </tr>
    </table>
</div>
<div id="ConfirmaRestauracion" title="Confirmaci�n de Restauraci�n" style="display:none;">
    <table width="100%">
        <tr>
            <td style="font-size:10px"><div id="DivRestaurar2" style="width:100%">&nbsp;</div></td>
        </tr>
        <tr>
            <td><span class="ui-icon ui-icon-unlocked" style="float:left; margin:0 7px 20px 0;"></span>&iquest;Desea Restaurar este Registro?</td>
        </tr>
    </table>
</div>
<div id="ConfirmaGuardar" title="Confirmaci�n de Operacion" style="display:none;">
    <table width="100%">
        <tr>
            <td style="font-size:10px"><div id="DivGuardar" style="width:100%">&nbsp;</div></td>
        </tr>
        <tr>
            <td>�Desea Confirmar la Operaci�n?</td>
        </tr>
    </table>
</div>
<div id="ConfirmaImprimir" title="Confirmaci�n de Impresion" style="display:none;">
    <table width="100%">
        <tr>
            <td style="font-size:10px"><div id="DivImprimir" style="width:100%">&nbsp;</div></td>
        </tr>
    </table>
</div>
<?php
	}
?>
