<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
		
    include('../config.php');
    include("../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();
    $Criterio = isset($_POST['Criterio'])?$_POST['Criterio']:'';
    
    $FormatoGrilla = $_SESSION['Formato'.$Criterio];
    //print_r( $_SESSION['Formato']);
    $Sql = $FormatoGrilla[0]; //die($Sql);
    $Campos = $FormatoGrilla[1];
    $Valor = $_POST['Valor']; 
    $Campo = $objFunciones->PreparaSQL($Campos, $Valor);
    $Sql = $objFunciones->VerificarSQL($Sql.$Campo);
    $Sql = $Sql.$FormatoGrilla[8];
   	

   	//die($Sql);
   	//
	//Para Optimizar Paginación
	//
	if ($FormatoGrilla[100] != '')
	{
		$SqlPag = $FormatoGrilla[100];
		$SqlPag = $objFunciones->VerificarSQL($SqlPag.$Campo);
		$SqlPag = substr($SqlPag.$FormatoGrilla[8], 0, strpos($SqlPag.$FormatoGrilla[8], 'ORDER BY'));
		$NRoReg = $conexion->query($SqlPag)->fetch();
		$num_total_registros = $NRoReg[0];
	}
	else
	{
		   //			echo  $Sql;exit();
		$num_total_registros = count($conexion->query($Sql)->fetchAll());
	}


    $TAMANO_PAGINA = $FormatoGrilla[6]['TP'];
    $total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);

    $pagina = $_POST['Pagina'];

    if ($pagina <= $total_paginas)
	{
		$Pag = ($pagina - 1) * $TAMANO_PAGINA;
    } 
	else
	{
        $Pag = 0;
        $pagina = 1;
    }
	
    $Inicio = 0;
	
    $Sql = $Sql." LIMIT ".$FormatoGrilla[6]['TP']." OFFSET ".$Pag;

    $Consulta = $conexion->query($Sql); 
	
    $Tamaño = $FormatoGrilla[7];
    $Columnas = $FormatoGrilla[11]?$FormatoGrilla[11]:($Consulta->columnCount() - 1);

?>
<table width="<?php echo $Tamaño;?>" border="0" cellspacing="1" bordercolor="#000000" id="ListaMenu" rules="all" class="ui-widget-content ">
    <thead>
        <tr title="Cabecera" height="25">
<?php
	for ($i = 1; $i <= $Columnas; $i++)
	{
?>
			<th width="<?php echo $FormatoGrilla[5]['W'.$i];?>" scope="col"><?php echo $FormatoGrilla[3]['T'.$i];?></th>
<?php
	}
	if ($FormatoGrilla[9]['Id'] != '')
	{
?>
			<th width="<?php echo $FormatoGrilla[5]['W'.$i]?>" scope="col">&nbsp;</th>
<?php
	}
?>
        </tr>
    </thead>
    <tbody>
<?php
	$NumRegs = -1;
	
	foreach ($Consulta->fetchAll() as $row)
	{
		$NumRegs = $NumRegs + 1;
		
		if (($Inicio <= $NumRegs) && ($NumRegs <= ($Inicio + $TAMANO_PAGINA - 1))) 
		{
			$ColorF = '#FFF';
					
			if ($FormatoGrilla[12][0]['Estado'] == $row[$FormatoGrilla[12][0]['Col'] - 1])
			{
				$ColorF = $FormatoGrilla[12][0]['Color'];
			}
?>
		<tr id="<?php echo $NumRegs."-".$row[0];?>" style="background-color:<?php echo $ColorF;?>;" tabindex="<?php echo $NumRegs + 1;?>"

<?php
			for ($d = 0; $d < count($FormatoGrilla[10]); $d++) 
			{
				$Dataa = $FormatoGrilla[10][$d]['Col'];
				
				if (is_numeric($FormatoGrilla[10][$d]['Col']))
				{
					$Dataa = $row[$FormatoGrilla[10][$d]['Col'] - 1];
				}
?>
			data-<?php echo $FormatoGrilla[10][$d]['Name'];?>="<?php echo $Dataa;?>"
<?php
			}
?>
		>
<?php
			for ($i = 1; $i <= $Columnas; $i++) 
			{
?>
			<td align="<?php echo $FormatoGrilla[4]['A'.$i];?>" valign="middle" style="padding-left:5px; padding-right:5px;"><?php echo strtoupper($row[$i - 1]);?></td>
<?php
			}

			if ($FormatoGrilla[9]['Id'] != '')
			{
?>
			<td valign="middle" align="center" style="padding-left:5px; padding-right:5px; width:<?php echo $FormatoGrilla[9]['NB'] * 20;?>px">
<?php
				for ($ii = 1; $ii <= $FormatoGrilla[9]['NB']; $ii++)
				{
					$Operador = '==';
					
					if ($FormatoGrilla[9]['BtnOp'.$ii] <> '')
					{
						$Operador = $FormatoGrilla[9]['BtnOp'.$ii];
					}
					
					$Evaluacion = 'if('.$row[$FormatoGrilla[9]['BtnCI'.$ii] - 1].$Operador.$FormatoGrilla[9]['BtnCV'.$ii].') {return 1;}else{return 0;}';
					
					if ($FormatoGrilla[9]['BtnCII'.$ii] != '')
					{
						$Evaluacion = 'if('.$row[$FormatoGrilla[9]['BtnCI'.$ii] - 1].$Operador.$row[$FormatoGrilla[9]['BtnCII'.$ii] - 1].') {return 1;}else{return 0;}';
					}
					
					$test = eval($Evaluacion);
					
					if ($test == 1) 
					{
?>
				<img width="16" id="<?php echo $NumRegs."-".$row[0];?>" class="<?php echo $ii;?>" src="<?php echo $_SESSION['urldir'].'images/iconos/'.$FormatoGrilla[9]['BtnI'.$ii];?>" <?php echo $FormatoGrilla[9]['BtnF'.$ii];?> title="<?php echo $FormatoGrilla[9]['Btn'.$ii];?>" <?php echo $FormatoGrilla[9]['BtnF'.$ii];?> style="cursor:pointer"/>
<?php
					}
					else
					{
?>
				<img width="16" id="<?php echo $NumRegs."-".$row[0];?>" class="<?php echo $ii;?>" src="<?php echo $_SESSION['urldir'];?>images/iconos/blanco.png"/>
<?php
					}
				}
?>
			</td>
<?php
			}
?>
		</tr>
<?php
		}
	}

	$registros = $num_total_registros - ($TAMANO_PAGINA * ($pagina - 1));
	
	$NumColl = $Columnas;
	
	if ($FormatoGrilla[9]['Id'] != '')
	{
		$NumColl = $NumColl + 1;
	}
	
	for ($i = $registros + 1; $i <= $TAMANO_PAGINA; $i++) 
	{
?>
		<tr style="cursor:default;" bgcolor="#ECECEC">
<?php
//		for ($j = 1; $j <= $NumColl; $j++)
//		{
?>
			<td colspan="<?php echo $NumColl;?>">&nbsp;</td>
<?php 
//		}
?>
		</tr>
<?php		
	}
?>
		<tr>
			<th colspan="<?php echo $NumColl;?>"><div id="Pagination" align="center"></div></th>
		</tr>
	</tbody>
</table>
<input type="hidden" id="NumReg" value="<?php echo $num_total_registros;?>">
<input type="hidden" id="Pagina" value="<?php echo $pagina;?>">
<input type="hidden" id="TotalPaginas" value="<?php echo $total_paginas;?>">