<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../objetos/clsseguridad.php");
	
	$objSeguridad = new clsSeguridad();
	
	$objSeguridad->seguimiento(1,$_SESSION['IdSucursal'],0,$_SESSION['id_user'],'CERRO SESION DEL SISTEMA',$_SERVER['REMOTE_ADDR']);
	
	if(session_start()){session_destroy();}
?>