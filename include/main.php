<?php
	header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	session_name("pnsu");
	if (!session_start()){
		session_start();
	}

	$Sede = 'marcona/';
	
	$_SESSION['Sede'] = $Sede;

	include($_SERVER['DOCUMENT_ROOT']."/ministerio/".$Sede."config.php");
	
	$_SESSION['urldir'] = $urldir;
	$_SESSION['path'] = $path;
	
	include($path."objetos/clsseguridad.php");
	include($path."objetos/clsDrop.php");
	
	$objFunciones = new clsDrop();
	$objSeguridad = new clsSeguridad();
	
	$NombreEmpresa = isset($_SESSION['NombreEmpresa']) ? $_SESSION['NombreEmpresa'] : 'CARDOZO SOLUCIONES INTEGRALES';
	$idsucursal = $_SESSION['IdSucursal'];
	$Cambio = isset($_GET["Cambio"]) ? $_GET["Cambio"] : 2;
	$FechaServidor = $objFunciones->FechaServer();

//echo $_SESSION["IP"];
	
	$IdProceso = $_SESSION['idsistema'];
	
	$im = $_GET['im'];
	
	if ($im != '')
	{
		$IdProceso = $im;
	}
	
	function CuerpoSuperior($TituloVentana = "Sistema de Administracion y Gestion de servicios de saneamiento")
	{
		global $urldir, $FechaServidor, $objFunciones, $idsucursal, $path, $meses, $objSeguridad, $NombreEmpresa, $Activo, $Sede, $IdProceso;

		$p = $objFunciones->val_periodofacturacion($idsucursal);
	
		$mensaje = "";
	
		if ($p == 0)
		{
			$mensaje = "NO SE HA ENCONTRADO NINGUN PERIODO DE FACTURACION PARA LA SUCURSAL";
		}
	
		if (!isset($_SESSION["id_user"]))
		{
			session_destroy();
			if (substr_count("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], 'ogin.php') != 1) {
				header("Location: ".$urldir.'login.php');
			}
		}
?>
	<html>
        <head>
            <title><?php echo $TituloVentana; ?></title>
            <link rel="stylesheet" href="<?php echo $_SESSION['urldir'];?>css/css_base.css" type="text/css" />
            <link type="text/css" href="<?php echo $_SESSION['urldir'];?>css/common.css" rel="stylesheet"  />
            <link rel="stylesheet" href="<?php echo $_SESSION['urldir'];?>css/style2.css" type="text/css" />            
            
            <script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery-1.8.3.js"></script>
            <script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery-ui-1.9.2.custom.min.js"></script>

            <script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery_sistema.js"></script>
            <script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js"></script>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link rel="shortcut icon" type="image/x-icon" href="<?php echo $_SESSION['urldir'];?>images/empresa.ico" />

            <!-- Código CheGuimo Inicio -->
            <link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/interface.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/interface_menu.css">
            <script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones_interface.js"></script>

            <script>
                var Tamanyo = [0, 0];
                var Tam = TamVentana();
                var ContenidoMargen = 175;

                $(document).ready(function () {
                    FechaSistema();
                    CargarMenu('<?php echo $Activo; ?>', '<?php echo $_SESSION['urldir'];?>');

                    setTimeout(function () {
                        if ($('#siteMenu').css('display') == 'none') {
                            ContenidoMargen = 145;
                        }
                        $('#siteContenido').css('height', eval(Tam[1] - ContenidoMargen));
                    }, 500);
                });

                $(window).resize(function () {
                    Tam = TamVentana();

                    if ($('#siteMenu').css('display') == 'none') {
                        ContenidoMargen = 145;
                    }
                    $('#siteContenido').css('height', eval(Tam[1] - ContenidoMargen));
                });
            </script>
            <!-- Código CheGuimo Fin -->

        </head>
        <body >
            <style type="text/css">
                div.growlUI {
                    background: url(<?php echo $_SESSION['urldir'];?>images/iconos/Ok.png) no-repeat 15px 15px;
                    padding-top:0px;
                    margin-top:0px;
                    background-color:#069;
                    border-style:outset;
                    border-width:3px;
                }
                div.growlUI h1, div.growlUI h2 { color: white; padding: 0px 0px 0px 75px; text-align: left }
                div.growlUI h1 { font-size: 20px;  font:bold }
                div.growlUI h2 { font-size: 16px; }


                div.growlUI2 {
                    background: url(<?php echo $_SESSION['urldir'];?>images/iconos/error.png) no-repeat 15px 15px;
                    padding-top:0px;
                    margin-top:0px;
                    background-color:#F00;
                    border-style:outset;
                    border-width:3px
                }
                div.growlUI2 h1, div.growlUI h2 { color: white; padding: 0px 0px 0px 75px; text-align: left }
                div.growlUI2 h1 { font-size: 20px;  font:bold }
                div.growlUI2 h2 { font-size: 16px; }

                .EstiloX{
                    font-family:Comic Sans Ms;
                    color:#FFF;
                    padding:4px;
                    text-align:center;
                    font-weight:bold;
                    font-size:12px;
                }
                .EstiloY{
                    font-family:Comic Sans Ms;
                    color:#F00;
                    padding:4px;
                    text-align:center;
                    font-weight:bold;
                    font-size:12px;
                }
            </style>

            <script>
                var urldir = '<?php echo $_SESSION['urldir'];?>';

                jQuery(function () {
                    $("#DivBaner").click(function () {
                        location.reload();
                    });
                    //$("#NombreModulo").hide()
                    $('#NombreModulo').fadeOut(100, function () {
                        $('#NombreModulo').fadeIn(100)
                    });
                    // JavaScript Document


                    $("#dialog:ui-dialog").dialog("destroy");
                    $("#dialog-form").dialog({
                        autoOpen: false,
                        height: 600,
                        width: 500,
                        modal: true,
                        buttons: {
                            "Aceptar": function () {
                                guardaropcion();
                                $(this).dialog("close");
                            },
                            "Cancelar": function () {
                                $(this).dialog("close");
                            }
                        },
                        close: function () {

                        }
                    });
                    $("#DivUpdatePass").dialog({
                        autoOpen: false,
                        modal: true,
                        title: "Cambiar Contrase&ntilde;a",
                        width: 400,
                        height: 250,
                        resizable: true,
                        show: "scale",
                        hide: "scale",
                        buttons: {
                            "Cambiar": function ()
                            {
                                ValidarForm()

                            },
                            "Cerrar": function () {
                                $(this).dialog("close");
                            }
                        }
                    });

                    //$("#NombreModulo").show()
                });

                function escribir_mensaje(mensaje)
                {
                    $("#div_mensaje").html(mensaje)
                }

                function CerrarSesion()
                {
                    $.ajax({
                        url: '<?php echo $_SESSION['urldir'];?>include/cerrar_sesion.php',
                        type: 'POST',
                        async: true,
                        data: '',
                        success: function (datos) {
                            location.href = '<?php echo $_SESSION['urldir'];?>login.php?Cambio=0'
                        }
                    })
                }
                function SeleccionarModulo()
                {
                    $.ajax({
                        url: '<?php echo $_SESSION['urldir'];?>admin/validaciones/seguimiento.php',
                        type: 'POST',
                        async: true,
                        data: 'idsistema=<?php echo $_SESSION['idsiste']; ?>&comentario=CERRO SESION EN EL MODULO',
                        success: function (datos) {
                            location.href = '<?php echo $_SESSION['urldir'];?>seleccionModulo.php?Cambio=1&idsistema=<?php echo $objSeguridad->encriptar($_SESSION['idsiste']); ?>';
                                        }
                                    })
                                }
                                function SeleccionarSistema()
                                {
                                    $.ajax({
                                        url: '<?php echo $_SESSION['urldir'];?>admin/validaciones/seguimiento.php',
                                        type: 'POST',
                                        async: true,
                                        data: 'idsistema=<?php echo $_SESSION['idsiste']; ?>&comentario=CERRO SESION EN EL SUB. SISTEMA',
                                        success: function (datos) {
                                            location.href = '<?php echo $_SESSION['urldir'];?>seleccionSistema.php'
                                        }
                                    })
                                }
                                function OperMensaje(Op)
                                {
                                    if (Op == 1)
                                    {
                                        $('#alertBoxes').html('<div class="box-success"></div>');
                                        $('.box-success').hide(0).html('El Registro se ha Grabado Correctamente');
                                        $('.box-success').slideDown(500);
                                        setTimeout(function () {
                                            $('.box-success').slideUp(1000);
                                        }, 5000);
                                    }
                                    if (Op == 2)
                                    {
                                        $('#alertBoxes').html('<div class="box-error"></div>');
                                        $('.box-error').hide(0).html('Error al Grabar Registro');
                                        $('.box-error').slideDown(500);
                                        setTimeout(function () {
                                            $('.box-error').slideUp(500);
                                        }, 5000);
                                    }
                                }
                                function abrir_popup_menu(codsubsistema, codperfil)
                                {

                                    $.ajax({
                                        url: '<?php echo $_SESSION['urldir'];?>seguridad/perfiles_sistema/vermenu.php',
                                        type: 'POST',
                                        async: true,
                                        data: 'codsubsistema=' + codsubsistema + '&codperfil=' + codperfil,
                                        success: function (datos) {
                                            $("#div_menu").html(datos)
                                        }
                                    })

                                    $("#dialog-form").dialog("open");
                                }
                                function UpdatePass()
                                {
                                    $.ajax({
                                        url: '<?php echo $_SESSION['urldir'];?>include/PasswordUpdateForm.php',
                                        type: 'POST',
                                        async: true,
                                        data: 'IdUsuario=<?php echo $_SESSION["id_user"]; ?>',
                                        success: function (Datos) {
                                            $("#DivUpdatePass").html(Datos)
                                            $("#DivUpdatePass").dialog("open");
                                            $("#ClaveActual").focus().select();

                                        }
                                    })

                                }
                                function ValidarFormEnt(evt)
                                {
                                    if (VeriEnter(evt))
                                    {
                                        ValidarForm()
                                    }
                                }
            </script>
            <div id="siteCabecera">
                <div id="siteCabeceraLogo" title="<?php echo $NombreEmpresa; ?>"></div>
                <!--<div id="siteCabeceraLogo2" title="MULTISERVICIOS CARDOZO E.I.R.L."></div>-->
                <div id="siteCabeceraTitulo"><?php echo $TituloVentana; ?></div>
                <div id="siteCabeceraBarra"><?php include("barra.php"); ?></div>
            </div>
            <ul id="siteMenu" class="Nav"></ul>
            <div id="siteContenido" align="center" style="overflow:auto;">
                <div id="alertBoxes"></div>
<?php
	}

	function CuerpoInferior()
	{
		global $urldir, $Cambio, $idsucursal, $codciclo;
?>
            </div>
            <div id="sitePie">PNSU - MVCS</div>

            <div id="dialog-form" title="Asignar Permisos a Usuarios" >
                <fieldset style="padding: 4px">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr style="padding:4px">
                            <td align="center">
                                <div id="div_menu" style="overflow:auto;"></div>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div id="blokea" style="position:absolute; width:100%; height:100%; top:0; left:0; display:none">
                <div class="ui-overlay">
                    <div class="ui-widget-overlay" >

                    </div>
                </div>
            </div>
            <div id="frm_loading" style="display:none" class="form">
                <div id="imagen"><img src="<?php echo $_SESSION['urldir'];?>images/loading.gif" width="125" height="125" ><br></div>
                <div id="texto">Se esta Realizando el Proceso de Facturacion Espere un Momento Por Favor!</div>
            </div>

            <div id="DivGIS" title="GIS Comercial">
                <div id="DivGISC"></div>
            </div>
            <div id="DivUpdatePass" align="center" style="display:none"></div>
        </body>
    </html>
    <script>
        function blokear_pantalla(text)
        {
            altura = (screen.availHeight / 2);
            ancho = (screen.availWidth / 2);

            altura = altura - 100;
            ancho = ancho - 100;

            $("#frm_loading").css('top', altura);
            $("#frm_loading").css('left', ancho);

            $("#imagen").html("<img src='<?php echo $_SESSION['urldir'];?>images/loading.gif' width='125' height='125' ><br>")
            $("#texto").html("Se esta Realizando el Proceso de Facturacion Espere un Momento Por Favor!")

            document.getElementById("blokea").style.display = "block";
            document.getElementById("frm_loading").style.display = "block";

            $("#texto").html(text)
        }
        function establecer_texto(imagen, texto)
        {
            $("#imagen").html(imagen)
            $("#texto").html(texto)
        }
        function desbloquear_pantalla()
        {
            document.getElementById("blokea").style.display = "none";
            document.getElementById("frm_loading").style.display = "none";
        }


        //GIS
        function IniciarModalGIS()
        {
            altura = (screen.availHeight - 100);
            ancho = (screen.availWidth - 100);

            $("#DivGIS").dialog
                    ({
                        autoOpen: false,
                        modal: true,
                        resizable: false,
                        title: "GIS Comercial",
                        height: altura,
                        width: ancho,
                        show: "scale",
                        hide: "scale",
                        buttons: {
                            "Aceptar": function () {
                                //Guardar(Op);
                                $(this).dialog("close");
                            },
                            Cancelar: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
        }

        function AbrirGIS(Ruta, Tipo)
        {
            $("#DivGIS").dialog('open');
            $("#DivGISC").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
            $.ajax({
                url: Ruta,
                type: 'POST',
                async: true,
                data: '',
                success: function (data) {
                    $("#DivGISC").html(data);
                }
            })
        }
		
		function AbrirHelp(IdProceso)
		{
			//alert(IdProceso);
			var ventana = window.open('<?php echo $_SESSION['urldir'];?>manuales/' + IdProceso + '.pdf', 'Manual de Usuario', 'resizable=yes, scrollbars=yes');
			ventana.focus();
		}
    </script>
<?php
	}
?>
