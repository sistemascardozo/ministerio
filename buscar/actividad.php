<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
    
    include("../include/main.php");
    include("../include/claseindex.php");
    $TituloVentana = "TIPO DE ACTIVIDAD";
    $Criterio = 'actividad';
    $Activo = 1;
    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;

    $FormatoGrilla = array();
    
    $Sql= "SELECT
        tp.codtipoactividad AS codtipoactividad,
        tp.descripcion AS actividad,
        es.descripcion AS estado,
        tp.estareg
        FROM
        public.tipoactividad AS tp
        INNER JOIN public.estadoreg AS es ON es.id = tp.estareg ";

    $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'tp.codtipoactividad','2' => 'tp.descripcion' ); //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;      //Operacion
    $FormatoGrilla[3] = array('T1' => 'Codigo', 'T2' => 'Actividad','T3' => 'Estado');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'center');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '60', 'W2' => '180', 'W3' => '80');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 690;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = " and tp.estareg=1 order by tp.codtipoactividad";                                   //Orden de la Consulta
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '1',                       //Número de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'ok.png', //Imagen a mostrar
        'Btn1' => 'Seleccionar', //Titulo del Botón
        'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
        'BtnCI1' => '4', //Item a Comparar
        'BtnCV1' => '1'    //Valor de comparación
    );
    $FormatoGrilla[10] = array(array('Name' => 'codtipoactividad', 'Col' => 1), array('Name' => 'actividad', 'Col' => 2) ); //DATOS ADICIONALES           
    $FormatoGrilla[11] = 3; //FILAS VISIBLES            
    $_SESSION['Formato'.$Criterio] = $FormatoGrilla;    
    Cabecera('', $FormatoGrilla[7], 1000, 600);
    Pie();

?>
<script>
    function BuscarB(Op)
    {
        var Valor = document.getElementById('Valor').value
        //var Fecha = document.getElementById('Fecha').value
        var Op2 = ''
        if (Op != 0)
        {
            Op2 = '&Op='+Op;
        }
        location.href = 'index.php?Valor='+Valor+'&pagina='+Pagina+Op2;
    }
    
    $(document).ready(function () {

        $("#BtnNuevoB").remove();
    });

    function Enviar(obj)
    {

        var codtipoactividad = $(obj).parent().parent().data('codtipoactividad')
        opener.recibir_actividad(codtipoactividad);
        window.close();
    }
</script>

