<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../include/main.php");
    include("../include/claseindex.php");
	
    $Criterio = 'Presupuesto';
    $TituloVentana = "PRESUPUESTO";
    $Activo = 1;
    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
    $tipo = isset($_GET['tipo']) ? $_GET['tipo'] : 200;
    $codsuc = $_SESSION['IdSucursal'];
    $documento = 2;
    $and = "";
	
    if ($Op == 5) {
        $and = " and c.estadopresupuesto=4";
    }
    //---Para la Pantalla de Creditos------------------
    if ($Op == 7) {
        $and = " and c.estadopresupuesto in(2,5) and tipopago=2 and concredito=0 ";
    }
    //---Para la Pantalla de Pagos------------------
    if ($Op == 6) {
        $and = "and c.estadopresupuesto in(2) and c.tipopago in(1,2) and tipopresupuesto=" . $tipo;
    }
    if ($Op == 8) {
        $and = " and c.estadopresupuesto in(1) ";
    }
    $FormatoGrilla = array();
	
    $Sql = "SELECT c.nropresupuesto, ".$objFunciones->FormatFecha('c.fechaemision')." ,
        CASE WHEN c.tipopresupuesto=1 THEN 'AGUA' ELSE 'DESAGUE' END ,
        c.codcliente,c.propietario,tc.descripcioncorta || ' ' ||	cs.descripcion || ' #' || c.nrocalle AS direccion,
        td.abreviado || ' ' || c.nrodocumento," . $objFunciones->Convert("c.totalpresupuesto", "NUMERIC (18,2)") . ",
        ep.descripcion as destadopresupuesto,c.estadopresupuesto,1
    FROM solicitudes.cabpresupuesto as c
    inner join public.calles as cs on(c.codemp=cs.codemp and c.codsuc=cs.codsuc and c.codcalle=cs.codcalle and c.codzona=cs.codzona)
    inner join public.tiposcalle as tc on(cs.codtipocalle=tc.codtipocalle)
    inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
    inner join public.estadopresupuesto ep on(ep.estadopresupuesto=c.estadopresupuesto)
    /*INNER JOIN catastro.clientes AS clie ON (clie.codemp=c.codemp AND clie.codsuc=c.codsuc AND clie.nroinscripcion=c.nroinscripcion)*/";
    $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL                                                           //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'c.nropresupuesto', '2' => 'c.codcliente', '3' => 'c.propietario', '4' => 'c.nrodocumento');          //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;                                                            //Operacion
    $FormatoGrilla[3] = array('T1' => 'Nro. Presu.', 'T2' => 'F. Emision', 'T3' => 'Categoria', 'T4' => 'Cod. Cliente', 'T5' => 'Propietario',
        'T6' => 'Direccion', 'T7' => 'Documento', 'T8' => 'Total', 'T9' => 'Estado');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'center', 'A4' => 'left', 'A8' => 'right');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '60','W4' => '50', 'W10' => '30');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 1080;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = "  AND (c.codemp=1 and c.codsuc=" . $codsuc . $and . ") ORDER BY c.nropresupuesto DESC ";                                   //Orden de la Consulta
    if ($Op != 5 && $Op != 6 && $Op!=7 && $Op!=8)
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '4', //Número de Botones a agregar
            'BtnId1' => 'BtnModificar', //Nombre del Boton
            'BtnI1' => 'modificar.png', //Imagen a mostrar
            'Btn1' => 'Editar', //Titulo del Botón
            'BtnF1' => 'onclick="Mostrar(this.id, 1);"', //Eventos del Botón
            'BtnCI1' => '10', //Item a Comparar
            'BtnCV1' => '1', //Valor de comparación
            'BtnId2' => 'BtnEliminar',
            'BtnI2' => 'eliminar.png',
            'Btn2' => 'Eliminar',
            'BtnF2' => 'onclick="Eliminar(this.id)"',
            //'BtnF2'=>'onclick="Anular(this)"', 
            'BtnCI2' => '10', //campo 3
            'BtnCV2' => '1', //igua a 1
            'BtnId3' => 'BtnRestablecer', //y aparece este boton
            'BtnI3' => 'imprimir.png',
            'Btn3' => 'Imprimir',
            'BtnF3' => 'onclick="Imprimir(this.id)"',
            'BtnCI3' => '11',
            'BtnCV3' => '1',
            'BtnId4' => 'BtnGenerar', //y aparece este boton
            'BtnI4' => 'documento.png',
            'Btn4' => 'Atender Presupuesto',
            'BtnF4' => 'onclick="AtenderPresupuesto(this)"',
            'BtnCI4' => '10',
            'BtnCV4' => '1');
    else {
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '1', //Número de Botones a agregar
            'BtnId1' => 'BtnModificar', //Nombre del Boton
            'BtnI1' => 'ok.png', //Imagen a mostrar
            'Btn1' => 'Seleccionar', //Titulo del Botón
            'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
            'BtnCI1' => '11', //Item a Comparar
            'BtnCV1' => '1'    //Valor de comparación
        );
    }

    $FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 1), array('Name' => 'estado', 'Col' => 10)); //DATOS ADICIONALES           
    $FormatoGrilla[11] = 9; //FILAS VISIBLES
    $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
	
    Cabecera('', $FormatoGrilla[7], 1000, 600);
    Pie();
?>
<script type="text/javascript">
	var urldir 	= "<?php echo $_SESSION['urldir'];?>";
	
    $(document).ready(function() {
        $("#DivPrint").dialog({
            autoOpen: false,
            height: 250,
            /*width: 250,*/
            modal: true,
            buttons: {
                "Aceptar": function() {
                    ImprimirOk()
                },
                "Cancelar": function() {
                    $(this).dialog("close");
                }
            },
            close: function() {

            }
        });
    })
    /*
    function Anular(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        var html = '<form id="form1" name="form1">';
        html+='<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
        html+='</form>'
        $("#DivEliminacion").html(html);
        $("#ConfirmaEliminacion").dialog("open");
    }
*/
    /* function Operacion(Op)
     {
     if(Op!=4)
     {
     if(est!=3)
     {
     var url = 'mantenimiento.php?Op='+Op;
     if(Op!=1)
     {
     if(Id=="")
     {
     alert('Seleccione un Elemento del Item')
     return
     }
     url=url+'&'+Id;
     }
     if(Op!=5)
     {
     if(est==5 && Op!=1)
     {
     alert('No se puede modificar puede que el presupuesto se encuentre transferido al Catastro de Usuarios');
     return
     }
     location.href=url;
     }else{
     
     }
     }else{
     alert('El Presupuesto se Encuentra Anulado')
     }
     }else{
     location.href='<?= $urldir ?>/admin/indexB.php'
     }
     }*/
    var Row = ''
    function Imprimir(Id)
    {
        Row = Id
        $("#DivPrint").dialog("open");
    }
    function ImprimirOk()
    {
        var url = '';
        var Checked = document.getElementById("contrato").checked;
        if (Checked == true)
        {
            url = 'imprimir_contrato.php';
        } else {
            url = 'imprimir_ficha.php';
        }
        AbrirPopupImpresion(url + '?Id=' + Row + '&codsuc=<?= $codsuc ?>', 800, 600)
    }

    function AtenderPresupuesto(obj)
    {
        var Estado = $(obj).parent().parent().data('estado')
        if (Estado == 2)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Atendido')
            return
        }
        if (Estado == 3)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Anulado')
            return
        }
        if (Estado == 4)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Pagado')
            return
        }
        if (Estado == 5)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Transferido al Catastro de Usuarios')
            return
        }
        var Id = $(obj).parent().parent().data('id')
        //location.href='atender?Id='+Id+'&codsuc=<?= $codsuc ?>';
        $("#Modificar").dialog({title: 'Atender Presupuesto'});

        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function() {
            GuardarOk();
        });
        $("#button-maceptar").children('span.ui-button-text').text('Atender Presupuesto')
        $.ajax({
            url: 'atender/index.php',
            type: 'POST',
            async: true,
            data: 'Id=' + Id + '&codsuc=<?= $codsuc ?>',
            success: function(data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function GuardarOk()
    {
        $.ajax({
            url: 'atender/Guardar.php?Op=0',
            type: 'POST',
            async: true,
            data: $('#form1').serialize(),
            success: function(data) {
                //alert(data);
                OperMensaje(data)
                $("#Mensajes").html(data);
                $("#DivModificar").html('');
                $("#Modificar").dialog("close");
                Buscar(0);
            }
        })
    }
    function Enviar(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        opener.Recibir(Id);
        window.close();
    }
</script>
<div id="DivPrint" title="Opciones de Impresion" style="display: none;"  >
    <table width="100%" border="0">
        <tr>
            <td width="5%" align="center">&nbsp;</td>
            <td width="95%">&nbsp;</td>
        </tr>
        <tr>
            <td width="5%" align="center">
                <input type="radio" name="radio" id="ficha" value="ficha" checked="checked" />
            </td>
            <td width="95%">Imprimir Ficha Presupuestal</td>
        </tr>
        <tr>
            <td align="center">
                <input type="radio" name="radio" id="contrato" value="contrato" />
            </td>
            <td>Imprimir Contrato EPS</td>
        </tr>
    </table>
</div>

    <script type="text/javascript">$("#BtnNuevoB").hide();</script>
