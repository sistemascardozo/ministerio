<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../include/main.php");
    include("../include/claseindex.php");

    $TituloVentana = "ESTADO DE LECTURA";
    $Criterio      = 'calles';
    $Activo        = 1;
    $Op            = isset($_GET['Op']) ? $_GET['Op']:               0;
    $codsuc        = $_GET["codsuc"];
    $contador      = (isset($_GET["contador"])) ? $_GET["contador"]: '' ;

    $FormatoGrilla = array();

    $Sql= "	SELECT e.codestlectura AS codestlectura, UPPER(e.descripcion) AS descripcion, er.descripcion AS descripcion_estado, e.estareg
						FROM public.estadolectura AS e
						INNER JOIN public.estadoreg AS er ON(er.id = e.estareg)";

    $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'e.descripcion', '2' => 'e.codestlectura', '3' => 'er.descripcion' ); //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;      //Operacion
    $FormatoGrilla[3] = array('T1' => 'Codigo', 'T2' => 'Estado Lectura','T3' => 'Estado');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'center', 'A3' => 'center');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '60', 'W2' => '180', 'W3' => '80');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 690;                                                            //Ancho de la Tabla
    // $FormatoGrilla[8] = " AND u.codsuc = " . $codsuc . " AND u.codzona = ".$codzona."  ORDER BY codurbanizacion ";  //Orden de la Consulta
    $FormatoGrilla[8] = " ORDER BY e.codestlectura, e.descripcion";  //Orden de la Consulta
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '1',                       //Número de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'Ok.png', //Imagen a mostrar
        'Btn1' => 'Seleccionar', //Titulo del Botón
        'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
        'BtnCI1' => '4', //Item a Comparar
        'BtnCV1' => '1'    //Valor de comparación
    );
    $FormatoGrilla[10] = array(array('Name' => 'codestlectura', 'Col' => 1), array('Name' => 'estado_lectura', 'Col' => 2) ); //DATOS ADICIONALES
    $FormatoGrilla[11] = 3; //FILAS VISIBLES
    $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 1000, 600);
    Pie();

?>
<script>
	var urldir 	= "<?php echo $_SESSION['urldir'];?>";

    function BuscarB(Op)
    {
        var Valor = document.getElementById('Valor').value
        //var Fecha = document.getElementById('Fecha').value
        var Op2 = ''
        if (Op != 0)
        {
            Op2 = '&Op='+Op;
        }
        location.href = 'index.php?Valor='+Valor+'&pagina='+Pagina+Op2;
    }

    $(document).ready(function () {

        $("#BtnNuevoB").remove();
    });

    function Enviar(obj)
    {
        var codestlectura = $(obj).parent().parent().data('codestlectura')
        var descripcion = $(obj).parent().parent().data('estado_lectura')

        opener.recibir_estado_lectura(codestlectura,descripcion, <?php echo $contador ?>);
        window.close();
    }
</script>
