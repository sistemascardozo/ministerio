<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp			= 1;
	$correlativo	= $_POST["nrocorrelativo"];
	$codsuc      	= $_SESSION['IdSucursal'];
	$documento 		= $Op==0?$_POST["documento"]:$_POST["coddocumento"];
	$serie			= $_POST["serie"];
	$nroinicio		= $_POST["nroinicio"];
	$nrofinal		= $_POST["nrofinal"];
	$nrocorrelativo	= $_POST["correlativo"]?$_POST["correlativo"]:$_POST["1form1_id"];
	$estareg		= $_POST["estareg"];
	$idusuario		= $_SESSION['id_user'];

	

	
	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("correla_documentos",$codsuc,"0");
			$correlativo 	= $id[0];
		
			$sql = "insert into reglasnegocio.correlativos(codemp,codsuc,coddocumento,serie,nroinicio,nrofinal,correlativo,estareg,codusu,nrocorrelativo)
				values(:codemp,:codsuc,:documento,:serie,:nroinicio,:nrofinal,:nrocorrelativo,:estareg,:codusu,:correlativo)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":documento"=>$documento,":serie"=>$serie,":nroinicio"=>$nroinicio,
					":nrofinal"=>$nrofinal,":nrocorrelativo"=>$nrocorrelativo,":estareg"=>$estareg,":codusu"=>$idusuario,":correlativo"=>$correlativo));
		break;
		case 1:
			$sql = "update reglasnegocio.correlativos set coddocumento=:documento,serie=:serie,nroinicio=:nroinicio,
				nrofinal=:nrofinal,correlativo=:nrocorrelativo,estareg=:estareg,codusu=:codusu where codemp=:codemp and codsuc=:codsuc and nrocorrelativo=:correlativo";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":documento"=>$documento,":serie"=>$serie,":nroinicio"=>$nroinicio,
					":nrofinal"=>$nrofinal,":nrocorrelativo"=>$nrocorrelativo,":estareg"=>$estareg,":codusu"=>$idusuario,":correlativo"=>$correlativo));
		break;
		case 2:case 3:
			 $sql = "update reglasnegocio.correlativos set estareg=:estareg
				where codemp=:codemp and codsuc=:codsuc and nrocorrelativo=:nrocorrelativo";
			$result = $conexion->prepare($sql);
			$result->execute(array(":nrocorrelativo"=>$nrocorrelativo,":codsuc"=>$codsuc,":estareg"=>$estareg,":codemp"=>$codemp));
		break;
	
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
