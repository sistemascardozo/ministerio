<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../objetos/clsDrop.php");
	
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$enabled	= "";
	$codsuc = $_SESSION['IdSucursal'];
	if($Op==2){$enabled="disabled='disabled'";}
	
	$objMantenimiento 	= new clsDrop();

	if($Id!="")
	{
		$correlativos 	= $objMantenimiento->setCorrelativosMant(" where c.nrocorrelativo=? AND c.codsuc=".$codsuc,$Id);
		$guardar		= $guardar."&Id2=".$Id;
	}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if($("#sucursal").val()==0)
		{
			alert("Seleccione la Sucursal")
			return false;
		}
		if($("#documento").val()==0)
		{
			alert("Seleccione el Documento")
			return false;
		}
		if($("#nroinicio").val()=="" || $("#nroinicio").val()==0)
		{
			alert("El Nro. de Inicio no es Valido")
			return false;
		}
		if($("#nrofinal").val()=="" || $("#nrofinal").val()==0)
		{
			alert("El Nro. Final no es Valido")
			return false;
		}
		if($("#correlativo").val()=="" || $("#correlativo").val()==0)
		{
			alert("El Correlativo no es Valido")
			return false;
		}

		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function cambiarDocumento()
	{
		$("#documento").val(0)
		
		$("#serie").val(0)
		$("#nroinicio").val(0)
		$("#nrofinal").val(0)
		$("#correlativo").val(0)
	}
	function setCorrelativos()
	{
		$.ajax({
			 url:'<?php echo $_SESSION['urldir'];?>admin/validaciones/correlativos.php',
			 type:'POST',
			 async:true,
			 data:'codsuc='+$("#sucursal").val()+'&coddocumento='+$("#documento").val(),
			 success:function(datos){		 	
				var r=datos.split("|")
				
				$("#serie").val(r[0])
				$("#nroinicio").val(r[1])
				$("#nrofinal").val(r[2])
				$("#correlativo").val(r[3])
				
			 }
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">

   <tbody>
	<tr>
	  <td width="70" class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id</td>
      <td align="center" class="TitDetalle">:</td>
      <td class="CampoDetalle">
			<input name="nrocorrelativo" type="text" id="Id" size="4" maxlength="2" value="<?=$Id?>" readonly="readonly" class="inputtext"/>
      </td>
	</tr>	
	<tr>
	  <td class="TitDetalle">Sucursal</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<? $objMantenimiento->drop_sucursal($correlativos["codsuc"],"onchange='cambiarDocumento();' ".$enabled); ?>
        <input type="hidden" name="codsuc" id="codsuc" value="<?=$correlativos["codsuc"]?>" />
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Documento</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<? $objMantenimiento->drop_documentos($correlativos["coddocumento"],"onchange='setCorrelativos();' ".$enabled,'WHERE codsuc='.$codsuc); ?>
        <input type="hidden" name="coddocumento" id="coddocumento" value="<?=$correlativos["coddocumento"]?>" />
      </td>
	</tr>
	<tr>
	  <td colspan="3" ><hr  /></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Serie</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><input class="inputtext" name="serie" type="text" id="serie" size="20" maxlength="20" value="<?=$correlativos["serie"]?>"/></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Nro. Inicio</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><input class="inputtext" name="nroinicio" type="text" id="nroinicio" size="20" maxlength="20" value="<?=$correlativos["nroinicio"]?>"/></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Nro. Final</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><input class="inputtext" name="nrofinal" type="text" id="nrofinal" size="20" maxlength="20" value="<?=$correlativos["nrofinal"]?>"/></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Correlativo</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><input class="inputtext" name="correlativo" type="text" id="correlativo" size="20" maxlength="20" value="<?=$correlativos["correlativo"]?>"/></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<? include("../../../include/estareg.php"); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  </tr>
  </tbody>
  
 </table>
</form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php
	$est = isset($correlativos["estareg"])?$correlativos["estareg"]:1;
	
	include("../../../admin/validaciones/estareg.php"); 
?>