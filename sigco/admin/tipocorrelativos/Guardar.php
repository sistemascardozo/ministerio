<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codsuc 		= $_SESSION['IdSucursal'];
	$coddocumento 	= $_POST["coddocumento"]?$_POST["coddocumento"]:$_POST["1form1_id"];
	$categoria		= $_POST["categoria"];
	$abreviado		= strtoupper($_POST["abreviado"]);
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	$idusuario		= $_SESSION['id_user'];
	
	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("documentos", $codsuc, "0");
			$coddocumento 	= $id[0];
			
			$sql = "INSERT INTO reglasnegocio.documentos ";
			$sql .= " (coddocumento, tipodocumento, abreviado, descripcion, estareg, codusu, codsuc) ";
			$sql .= "VALUES(".$coddocumento.", ".$categoria.", '".$abreviado."', '".$descripcion."', ".$estareg.", ".$idusuario.", ".$codsuc.")";
			
			$result = $conexion->prepare($sql);
			$result->execute(array());
			
			break;
			
		case 1:
			$sql = "UPDATE reglasnegocio.documentos ";
			$sql .= " SET tipodocumento = ".$categoria.", abreviado = '".$abreviado."', descripcion = '".$descripcion."', estareg = ".$estareg." ";
			$sql .= "WHERE codsuc = ".$codsuc." AND coddocumento = ".$coddocumento;
			
			$result = $conexion->prepare($sql);
			$result->execute(array());
			
			break;
			
		case 2: case 3:
			$sql = "UPDATE reglasnegocio.documentos ";
			$sql .= "SET estareg = ".$estareg." ";
			$sql .= "WHERE codsuc = ".$codsuc." AND coddocumento = ".$coddocumento;
			
			$result = $conexion->prepare($sql);
			$result->execute(array());
			
			break;
	}

	if($result->errorCode() != '00000')
	{
		$conexion->rollBack();
		
		$mensaje = "Error al Grabar Registro";
		echo $res = 2;
		
		var_dump($result->errorInfo());
	}
	else
	{
		$conexion->commit();
		
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res = 1;
	}
?>
