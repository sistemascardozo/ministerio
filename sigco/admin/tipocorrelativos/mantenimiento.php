<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../objetos/clsDrop.php");
	
	$codsuc = $_SESSION['IdSucursal'];
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsDrop();
	
	if($Id != '')
	{
		$documentos 	= $objMantenimiento->setDocumento(" WHERE coddocumento = ? AND codsuc=  ".$codsuc, $Id);
		$guardar		= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if($("#categoria").val()==100)
		{
			alert("Seleccione la Categoria")
			return false;
		}
		if($("#abreviado").val()=="")
		{
			alert("La Abreviatura de la Categoria no puede ser NULO")
			return false;
		}
		if($("#descripcion").val()=="")
		{
			alert("La Descripcion de la Categoria no puede ser NULO")
			return false;
		}

		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td width="101" class="TitDetalle">&nbsp;</td>
	  <td width="30" align="center" class="TitDetalle">&nbsp;</td>
	  <td width="573" class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id</td>
      <td align="center" class="TitDetalle">:</td>
      <td class="CampoDetalle">
			<input type="text" name="coddocumento" id="Id" size="4" maxlength="2" value="<?=$Id?>" readonly="readonly" class="inputtext"/>
	   </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Categoria</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
    	<?=$objMantenimiento->drop_categoria_correlativo($documentos["tipodocumento"])?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Abreviatura</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<input class="inputtext" name="abreviado" type="text" id="abreviado" size="20" maxlength="20" value="<?=$documentos["abreviado"]?>"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<input class="inputtext" name="descripcion" type="text" id="descripcion" maxlength="200"value="<?=$documentos["descripcion"]?>" style="width:400px;"/>
      </td>
    </tr>	
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<? include("../../../include/estareg.php"); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
  </tbody>
  
    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php
	$est = isset($documentos["estareg"])?$documentos["estareg"]:1;
	
	include("../../../admin/validaciones/estareg.php"); 
?>