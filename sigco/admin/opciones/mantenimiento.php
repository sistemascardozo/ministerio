<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../objetos/clsMantenimiento.php");
			
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc 	= $_SESSION['IdSucursal'];
	
	$objMantenimiento 	= new clsMantenimiento();
	
	$paramae = $objMantenimiento->setParamae(" where codpar=? and codsuc=?", array($Id, $codsuc));
	
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if (document.form1.tipo.value == "")
		{
			alert('Digite la Categoria del Parametro');
			return false;
		}
		if ($("#descripcion").val() == '')
		{
			alert('Digite la Descripcion del Parametro');
			return false;
		}
		if (document.form1.valor.value == '')
		{
			alert('Digite el valor del Parametro');
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td width="100" class="TitDetalle">&nbsp;</td>
	  <td width="20">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id</td>
      <td align="center">:</td>
      <td class="CampoDetalle">
		<input name="codpar" type="text" id="Id" size="4" maxlength="2" value="<?=$Id?>" readonly="readonly" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Categoria</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="tippar" type="text" id="tipo" size="20" maxlength="6" value="<?=$paramae["tippar"]?>"/>
	  </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
      	<input class="inputtext" name="despar" type="text" id="descripcion" maxlength="200" value="<?=$paramae["despar"]?>" style="width:400px;"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Valor</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
      	<input class="inputtext" name="valor" type="text" id="valor" maxlength="200" value="<?=$paramae["valor"]?>" style="width:400px;"/>
      </td>
	</tr>	
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	  	<? include("../../../include/estareg.php"); ?>
      </td>
	</tr>
	<tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	</tr>
   </tbody>
    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php
	$est = isset($paramae["estareg"])?$paramae["estareg"]:1;
	
	include("../../../admin/validaciones/estareg.php"); 
?>