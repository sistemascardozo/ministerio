<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codpar 	= isset($_POST["codpar"])?$_POST["codpar"]:$_POST["1form1_id"];
	
	$tippar		= $_POST["tippar"];
	$despar		= $_POST["despar"];
	$valor		= $_POST["valor"];
	$estareg 	= $_POST["estareg"];
	$idusuario	= $_SESSION['id_user'];
	$codsuc 	= $_SESSION['IdSucursal'];
	
	if ($Op==2 || $Op==3)
	{
		$upd = "update reglasnegocio.parame set estareg=?, codusu=? where codpar=? and codsuc=?";
		$result = $conexion->prepare($upd);
		$result->execute(array($estareg, $idusuario, $codpar, $codsuc));
	}
	else
	{
		$upd = "update reglasnegocio.parame set tippar=?,despar=?,valor=?,estareg=?,codusu=? where codpar=? and codsuc=?";
		$result = $conexion->prepare($upd);
		$result->execute(array($tippar, $despar, $valor, $estareg, $idusuario, $codpar, $codsuc));
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Actualizado Correctamente";
		echo $res=1;
	}
	
?>

