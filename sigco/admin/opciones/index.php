<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../include/main.php");
    include("../../../include/claseindex.php");
	
	$TituloVentana = "Mantenimiento de Opciones";
	//$Criterio = 'paramae';
	$Activo=1;
    CuerpoSuperior($TituloVentana);
    
    $codsuc   = $_SESSION['IdSucursal'];
    $Op = isset($_GET['Op'])?$_GET['Op']:0;

    $FormatoGrilla = array ();
    $Sql = "SELECT p.codpar, p.despar, e.descripcion, p.estareg FROM reglasnegocio.parame p INNER JOIN public.estadoreg e ON (p.estareg=e.id)";
    $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql);                                                             //Sentencia SQL
    $FormatoGrilla[1] = array('1'=>'p.codpar', '2'=>'p.despar');  				//Campos por los cuales se har� la b�squeda
    $FormatoGrilla[2] = $Op;                                                            //Operacion
    $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Descripci&oacute;n', 'T3'=>'Estado');   //T�tulos de la Cabecera
    $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center');                        //Alineaci�n por Columna
    $FormatoGrilla[5] = array('W1'=>'60', 'W3'=>'90');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por P�ginas
    $FormatoGrilla[7] = 800;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = " AND codsuc=".$codsuc." ORDER BY p.codpar ASC ";                                   //Orden de la Consulta
	$FormatoGrilla[9] = array('Id'=>'1', 												//Botones de Acciones
							  'NB'=>'3',					//N�mero de Botones a agregar
							  'BtnId1'=>'BtnModificar', 	//Nombre del Boton
							  'BtnI1'=>'modificar.png', 	//Imagen a mostrar
							  'Btn1'=>'Editar', 			//Titulo del Bot�n
							  'BtnF1'=>'onclick="Mostrar(this.id, 1);"',	//Eventos del Bot�n
							  'BtnCI1'=>'4', 	//Item a Comparar
							  'BtnCV1'=>'1',		//Valor de comparaci�n
							  'BtnId2'=>'BtnEliminar', 
							  'BtnI2'=>'eliminar.png', 
							  'Btn2'=>'Eliminar', 
							  'BtnF2'=>'onclick="Eliminar(this.id)"', 
							  'BtnCI2'=>'4', //campo 3
							  'BtnCV2'=>'1',//igua a 1
							  'BtnId3'=>'BtnRestablecer', //y aparece este boton
							  'BtnI3'=>'restablecer.png', 
							  'Btn3'=>'Restablecer', 
							  'BtnF3'=>'onclick="Restablecer(this.id)"', 
							  'BtnCI3'=>'4', 
							  'BtnCV3'=>'0');
							  
    $_SESSION['Formato'] = $FormatoGrilla;

    Cabecera('', $FormatoGrilla[7], 650, 300);
?>
<script>
	$('#BtnNuevoB').css('display', 'none');
</script>
<?
    Pie();
    CuerpoInferior();
?>
