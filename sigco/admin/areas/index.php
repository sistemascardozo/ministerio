<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../include/main.php");
    include("../../../include/claseindex.php");
	
  $TituloVentana = "AREAS";
  $Activo = 1;
  
  CuerpoSuperior($TituloVentana);
  
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];

  $FormatoGrilla = array ();
  $Sql = "SELECT a.codarea, s.descripcion AS sucursal, a.descripcion, a.responsables, e.descripcion, a.estareg, a.codsuc ";
  $Sql .= "from reglasnegocio.areas as a";
  $Sql .= " inner join admin.sucursales as s on(a.codsuc=s.codsuc and a.codemp=s.codemp)";
  $Sql .= " INNER JOIN public.estadoreg e ON (a.estareg=e.id)";
  $FormatoGrilla[0] = $Sql;                                                           //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'a.codarea', '2'=>'a.descripcion','3'=>'a.responsables','4'=>'s.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Sucursal','T3'=>'Area', 'T4'=>'Responsable', 'T5'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A5'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W5'=>'90');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " ORDER BY a.codarea ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Modificar(this);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Anular(this)"', 
              'BtnCI2'=>'6', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Activar(this)"', 
              'BtnCI3'=>'6', 
              'BtnCV3'=>'0');
  $FormatoGrilla[10] = array(array('Name' =>'id','Col'=>1),array('Name' =>'codsucursal','Col'=>7));//DATOS ADICIONALES        
  $FormatoGrilla[11] = 5;//FILAS VISIBLES                 
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 650, 300);
  Pie();
 
?>
<script type="text/javascript">
	function Modificar(obj)
	{
		$("#form1").remove();
		
		var Id = $(obj).parent().parent().data('id');
		var codsucursal = $(obj).parent().parent().data('codsucursal');
		
		$("#Modificar").dialog("open");
		
		$("#DivModificar").html("<center><span class='icono-icon-loading'></span></center>");
		
		$.ajax({
			url:'mantenimiento.php?Op=1',
			type:'POST',
			async:true,
			data:'Id=' + Id+ '&codsuc='+codsucursal,
			success:function(data){
				$("#DivModificar").html(data);
			}
		});
	}
	
	function Anular(obj)
	{
		$("#form1").remove();
		
		var Id = $(obj).parent().parent().data('id');
		var codsucursal = $(obj).parent().parent().data('codsucursal');
		
		var html = '<form id="form1" name="form1">';
			html += '<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
			html += '<input type="hidden" name="sucursal" value="' + codsucursal + '" />';
			html += '<input type="hidden" name="estareg" id="estareg" value="0" />';
			html += '</form>';
		
		$("#DivEliminacion").html(html);
		$("#ConfirmaEliminacion").dialog("open");
	}
	
	function Activar(obj)
	{
		$("#form1").remove();
		
		var Id = $(obj).parent().parent().data('id');
		var codsucursal = $(obj).parent().parent().data('codsucursal');
		
		var html = '<form id="form1" name="form1">';
			html += '<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
			html += '<input type="hidden" name="sucursal" value="' + codsucursal + '" />';
			html += '<input type="hidden" name="estareg" id="estareg" value="1" />';
			html += '</form>';
		
		$("#DivRestaurar").html(html);
		$("#ConfirmaRestauracion").dialog("open");
	}
</script>
<?php  CuerpoInferior();?>