<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../objetos/clsMantenimiento.php");
			
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsMantenimiento();
	$codsuc 		= isset($_POST["codsuc"])?$_POST["codsuc"]:'';

	$sucursal = $objMantenimiento->setSucursales();

	if($Id!='')
	{
		$areas 		= $objMantenimiento->setAreas(" WHERE a.codarea=? AND a.codsuc=? ",$Id, $codsuc);
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if($("#sucursal").val()=="")
		{
			alert("Seleccione la Sucursal");
			return false
		}
		if ($("#descripcion").val() == '')
		{
			alert('La Descripcion del Area no puede ser NULO');
			return false;
		}

		if ($("#responsables").val() ==  '')
		{
			alert('El nombre del responsable del area no puede ser NULO');
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	 <td width="101" class="TitDetalle">&nbsp;</td>
	 <td width="30" class="TitDetalle">&nbsp;</td>
	 <td width="573" class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
     <td class="TitDetalle">Id</td>
     <td align="center" class="TitDetalle">:</td>
     <td class="CampoDetalle">
	   <input name="codarea" type="text" id="Id" size="4" maxlength="2" value="<?=$Id?>" readonly="readonly" class="inputtext"/>
     </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Sucursal</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><select name="sucursal" id="sucursal" class="select" style="width:310px">
        <option value="0">--Seleccione la Sucursal--</option>
        <?php
				foreach($sucursal as $rowsucursal)
				{
					$selected="";
					if($rowsucursal["codsuc"]==$areas["codsuc"])
					{
						$selected="selected='selected'";
					}
					
					echo "<option value='".$rowsucursal["codsuc"]."' ".$selected." >".$rowsucursal["descripcion"]."</option>";
				}
			?>
      </select></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<input class="inputtext" name="descripcion" type="text" id="descripcion" maxlength="200" value="<?=$areas["descripcion"]?>" style="width:400px;"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Responsable</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<input class="inputtext" name="responsables" type="text" id="responsables" maxlength="200" value="<?=$areas["responsables"]?>" style="width:400px;"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
		<? include("../../../include/estareg.php"); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	 </tr>
   </tbody>
   
 </table>
 </form>
</div>
<script>
	$("#Nombre").focus();
</script>
<?php
	$est = isset($areas["estareg"])?$areas["estareg"]:1;
	
	include("../../../admin/validaciones/estareg.php"); 
?>