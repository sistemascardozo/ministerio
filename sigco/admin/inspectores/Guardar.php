<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp			= 1;
	$codinspector 	= $_POST["codinspector"]?$_POST["codinspector"]:$_POST["1form1_id"];
	$sucursal		= $_SESSION["IdSucursal"];
	$nombres 		= strtoupper($_POST["nombres"]);
	$tipodocumento	= $_POST["tipodocumento"];
	$nrodocumento	= $_POST["nrodocumento"];
	$estareg		= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("inspectores",$sucursal,"0");
			$codinspector 	= $id[0];
			$sql = "insert into reglasnegocio.inspectores(codinspector,codemp,codsuc,nombres,codtipodocumento,nrodocumento,estareg) 
					values(:codinspector,:codemp,:codsuc,:nombres,:codtipodocumento,:nrodocumento,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codinspector"=>$codinspector,":codemp"=>$codemp,":codsuc"=>$sucursal,":nombres"=>$nombres,":codtipodocumento"=>$tipodocumento,
					":estareg"=>$estareg,":nrodocumento"=>$nrodocumento));
		break;
		case 1:
			$sql = "update reglasnegocio.inspectores set nombres=:nombres,codtipodocumento=:codtipodocumento,nrodocumento=:nrodocumento,estareg=:estareg
				where codemp=:codemp and codsuc=:codsuc AND codinspector=:codinspector";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codinspector"=>$codinspector,":codemp"=>$codemp,":codsuc"=>$sucursal,":nombres"=>$nombres,":codtipodocumento"=>$tipodocumento,
					":estareg"=>$estareg,":nrodocumento"=>$nrodocumento));
		break;
		case 2:case 3:
			$sql = "update reglasnegocio.inspectores set estareg=:estareg
				where codemp=:codemp and codsuc=:codsuc AND codinspector=:codinspector";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codinspector"=>$codinspector,":estareg"=>$estareg,":codemp"=>$codemp,":codsuc"=>$sucursal));
		break;
	
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
