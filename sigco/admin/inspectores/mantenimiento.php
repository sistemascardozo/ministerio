<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../objetos/clsDrop.php");
	
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsDrop();
	$codsuc 		= $_SESSION["IdSucursal"];
	
	if($Id != '')
	{
		$inspectores 	= $objMantenimiento->setInspectores(" where i.codinspector=? AND i.codsuc=".$codsuc,$Id);
		$guardar		= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
	
		if($("#descripcion").val()=="")
		{
			Msj($("#descripcion"),"Ingrese Nombre y Apellido del Inspector");
			return false
		}
		if($("#tipodocumento").val()=="")
		{
			Msj($("#tipodocumento"),"Seleccione el Tipo de Documento");
			return false
		}
		if($("#nrodocumento").val()=="")
		{
			Msj($("#nrodocumento"),"Ingrese el Nro. Documento");
			return false
		}

		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td width="139" class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td width="460" class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id</td>
      <td align="center" class="TitDetalle">:</td>
      <td class="CampoDetalle">
		<input name="codinspector" type="text" id="Id" size="4" maxlength="2" value="<?=$Id?>" readonly="readonly" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Nombres y Apellidos</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<input class="inputtext" name="nombres" type="text" id="descripcion" maxlength="200" value="<?=$inspectores["nombres"]?>" style="width:400px;"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Documento</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<?=$objMantenimiento->drop_tipodocumento($inspectores["codtipodocumento"]); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Nro. Documento</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<input class="inputtext entero" name="nrodocumento" type="text" id="nrodocumento" size="15" maxlength="15" value="<?=$inspectores["nrodocumento"]?>"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<? include("../../../include/estareg.php"); ?>
      </td>
	</tr>
	<tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	</tr>
   </tbody>
  </table>
 </form>
</div>
<script>
 $("#descripcion").focus();
</script>
<?php
	$est = isset($inspectores["estareg"])?$inspectores["estareg"]:1;
	
	include("../../../admin/validaciones/estareg.php"); 
?>