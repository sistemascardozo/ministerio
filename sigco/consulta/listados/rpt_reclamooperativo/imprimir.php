<?php 
    if(!session_start()){session_start();}

	include("../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses,$fechaactua;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral($fechaactua);

            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RESUMEN DE RECLAMOS OPERATIVOS";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(20);
			$this->SetFillColor(18,157,176);
			$this->SetTextColor(255);
			$h=5;
            $h1= 10;
			$h2= 15;

			$this->SetFont('Arial','B',10);
            $this->Cell($Dim[1],$h,utf8_decode('Descripcion'),1,0,'C',true);
            $this->Cell($Dim[8],$h,utf8_decode('Estados'),1,1,'C',true);

			$this->Cell($Dim[6],$h,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Sectores'),1,0,'C',true);
			$this->Cell($Dim[9],$h,utf8_decode('Conceptos'),1,0,'C',true);
            $this->Cell($Dim[4],$h,utf8_decode('Cant. Otros'),1,0,'C',true);
            $this->Cell($Dim[4],$h,utf8_decode('Cant. Atendidos'),1,1,'C',true);

		}

        function Contenido($cont,$sect, $conp, $num1, $num2, $dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',10);
					$this->Cell($Dim[1],$h,utf8_decode($conp),1,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode($num1),1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($num2),1,0,'C',true);
					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',9);
					$this->Cell($Dim[6],$h,utf8_decode($cont),1,0,'C',true);
                    $this->Cell($Dim[7],$h,utf8_decode($sect),1,0,'L',true);
					$this->Cell($Dim[9],$h,utf8_decode($conp),1,0,'L',true);
					$this->Cell($Dim[4],$h,$num1,1,0,'C',true);
					$this->Cell($Dim[4],$h,$num2,1,1,'C',true);
					break;
			}

		}
    }

    $Dim = array('1'=>200,'2'=>40,'3'=>40,'4'=>40,'5'=>25,'6'=>10,'7'=>70,'8'=>80,'9'=>120);

	// Actual
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = $_GET['mes'];
    $mestexto = $_GET['mestexto'];
    $ciclo    = $_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];

    if($mes < 10):
        $mess = str_pad($mes, 2, 0,STR_PAD_LEFT);
    endif;

    $fechaactua = '01/'.$mess.'/'.$anio; 

    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
    $primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);

    $sql = "    ( 
                    SELECT 
                    sec.codsector as codsector,
                    c.codconcepto as codconcepto, 
                    c.descripcion as concepto, 
                    sec.descripcion as sector,
                    count(c.codconcepto) as cantidad, 
                    1 as item 
                    FROM reclamos.solicitud r 
                    INNER JOIN reclamos.conceptosreclamos c ON (c.codconcepto = r.codconcepto) 
                    LEFT JOIN public.sectores sec ON (r.codemp = sec.codemp AND r.codsuc = sec.codsuc AND r.codsector = sec.codsector)
                    WHERE r.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}' 
                    AND r.codestadoreclamo = 3 
                    GROUP BY sec.codsector, c.codconcepto , c.descripcion, sec.descripcion
                    ORDER BY sec.codsector
                ) 
                UNION 
                ( 
                    SELECT 
                    sec.codsector as codsector,
                    c.codconcepto as codconcepto, 
                    c.descripcion as concepto, 
                    sec.descripcion as sector,
                    count(c.codconcepto) as cantidad,
                    2 as item 
                    FROM reclamos.solicitud r 
                    INNER JOIN reclamos.conceptosreclamos c ON (c.codconcepto = r.codconcepto) 
                    LEFT JOIN public.sectores sec ON (r.codemp = sec.codemp AND r.codsuc = sec.codsuc AND r.codsector = sec.codsector)
                    WHERE r.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}' 
                    AND r.codestadoreclamo <> 3 
                    GROUP BY sec.codsector, c.codconcepto , c.descripcion, sec.descripcion
                    ORDER BY sec.codsector
                ) 
                ORDER BY codsector, codconcepto";


    $consulta = $conexion->query($sql);
    $row      = $consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();  

    foreach ($row as $value):

        for ($i=1; $i <= 2 ; $i++) : 

            if ( ( $value['item'] > $i ) ):
                                
                if (!array_key_exists($value['codsector'].$value['codconcepto'], $cabecera)):
                
                    $cabecera[$value['codsector'].$value['codconcepto']] = array();
                    array_push($cabecera[$value['codsector'].$value['codconcepto']], $value['sector'], $value['concepto'],0);
                
                else:
                
                    array_push($cabecera[$value['codsector'].$value['codconcepto']],$value['cantidad']);            
                
                endif;
                                
            else:

                array_push($cabecera[$value['codsector'].$value['codconcepto']],$value['cantidad']);
            
            endif;

        endfor;

    endforeach;

	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(10);
    // $objReporte->SetAutoPageBreak(true, 5);
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $contador  = 0;


    foreach ($cabecera as $key => $value): 

        $contador++;
        $subtotal1 += $value[2];
        $subtotal2 += $value[3];

		$objReporte->contenido($contador,strtoupper($value[0]), strtoupper($value[1]),$value[2],$value[3]);
        // exit;

    endforeach;

	$objReporte->contenido('','',strtoupper('TOTAL'),$subtotal1,$subtotal2,1);

	$objReporte->Output();	
		
?>