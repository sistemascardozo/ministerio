<?php
 if(!session_start()){session_start();}
    require("../../../config.php");
    
    $nroinscripcion = $_POST["nroinscripcion"];
    $nrofacturacion=$_SESSION["nrofacturacion"] - 1;
    $codsuc = $_SESSION['IdSucursal'];
    //---Variables ultimo mes---
    $aguaultimo     = 0.00;
    $desagueultimo  = 0.00;
    $cargofijo      = 0.00;
    $otroconceptos  = 0.00;
    $interesultimo  = 0.00;
    $igvultimo      = 0.00;
    $redanteriorult = 0.00;
    $redactualult   = 0.00;
    
    //---Variables Deuda---
    $aguadeuda      = 0.00;
    $desaguedeuda   = 0.00;
    $cargofijodeuda = 0.00;
    $otrosdeuda     = 0.00;
    $interesdeuda   = 0.00;
    $igvdeuda       = 0.00;
    $redanteriordeu = 0.00;
    $redactualdeuda = 0.00;
    
    //---Rebajado---
    $aguarebajado       = 0.00;
    $desaguerebajado    = 0.00;
    $cargorebajado      = 0.00;
    $otrosrebajado      = 0.00;
    $interesrebajado    = 0.00;
    $igvrebajado        = 0.00;
    $redmesantrebajado  = 0.00;
    $redmesactrebajado  = 0.00;
    
    //---Pagos---
    $aguapagos          = 0.00;
    $desaguepagos       = 0.00;
    $cargopagos         = 0.00;
    $otrospagos         = 0.00;
    $interespagos       = 0.00;
    $igvpagos           = 0.00;
    $redantpagos        = 0.00;
    $redactpagos        = 0.00;
    
    //---Select para la facturacion del mes----
    $sqlmes     = "select * from facturacion.detfacturacion 
            where nrofacturacion=".($nrofacturacion)." and nroinscripcion=".$nroinscripcion." AND codsuc=".$codsuc;
    $consulta = $conexion->prepare($sqlmes);
    $consulta->execute();
    $items = $consulta->fetchAll();
    foreach($items as $rowmes)
    {
        if($rowmes["codconcepto"]==1)
        {
             $aguaultimo    += $rowmes["importe"];
             $aguarebajado  += $rowmes["importerebajado"];
             $aguapagos     += $rowmes["imppagado"];
        }
        if($rowmes["codconcepto"]==2)
        {
             $desagueultimo     += $rowmes["importe"];
             $desaguerebajado   += $rowmes["importerebajado"];
             $desaguepagos      += $rowmes["imppagado"];
        }
        if($rowmes["codconcepto"]==6)
        {
             $cargofijo         += $rowmes["importe"];
             $cargorebajado     += $rowmes["importerebajado"];
             $cargopagos        += $rowmes["imppagado"];
        }
        if($rowmes["codconcepto"]==3 || $rowmes["codconcepto"]==4 || 
           $rowmes["codconcepto"]==9 || $rowmes["codconcepto"]==54 || $rowmes["codconcepto"]==55)
        {
             $interesultimo     += $rowmes["importe"];
             $interesrebajado   += $rowmes["importerebajado"];             
             $interespagos      += $rowmes["imppagado"];
        }
        if($rowmes["codconcepto"]==5)
        {
             $igvultimo     += $rowmes["importe"];
             $igvrebajado   += $rowmes["importerebajado"];             
             $igvpagos      += $rowmes["imppagado"];
        }
        if($rowmes["codconcepto"]==7)
        {
             $redanteriorult    += $rowmes["importe"];
             $redmesantrebajado += $rowmes["importerebajado"];
             $redantpagos       += $rowmes["imppagado"];
        }
        if($rowmes["codconcepto"]==8)
        {
             $redactualult      += $rowmes["importe"];
             $redmesactrebajado += $rowmes["importerebajado"];
             $redactpagos       += $rowmes["imppagado"];;
        }
        $sqlconceptos = "select codtipoconcepto from facturacion.conceptos where codconcepto=".$rowmes["codconcepto"];
        $consconcepto = $conexion->query($sqlconceptos);
        $rowconcepto  = $consconcepto->fetch();

        if($rowconcepto[0]==2)
        {
            $otroconceptos      += $rowmes["importe"];
            $otrosrebajado      += $rowmes["importerebajado"];
            $otrospagos         += $rowmes["imppagado"];
        }
        
    }
    
   $sqldeuda   = "select * from facturacion.detfacturacion where categoria=1 and nroinscripcion=".$nroinscripcion." AND codsuc=".$codsuc;
    $consdeuda  = $conexion->query($sqldeuda);
    $items=  $consdeuda->fetchAll();
    foreach($items as $rowdeuda)
    {
        if($rowdeuda["codconcepto"]==1)
        {
             $aguadeuda     += $rowdeuda["importe"];
             $aguarebajado  += $rowdeuda["importerebajado"];
             $aguapagos     += $rowdeuda["imppagado"];
        }
        if($rowdeuda["codconcepto"]==2)
        {
             $desaguedeuda      += $rowdeuda["importe"];
             $desaguerebajado   += $rowdeuda["importerebajado"];
             $desaguepagos      += $rowdeuda["imppagado"];
        }
        if($rowdeuda["codconcepto"]==6)
        {
             $cargofijodeuda    += $rowdeuda["importe"];
             $cargorebajado     += $rowdeuda["importerebajado"];
             $cargopagos        += $rowdeuda["imppagado"];
        }
        if($rowdeuda["codconcepto"]==3 || $rowdeuda["codconcepto"]==4 || 
           $rowdeuda["codconcepto"]==9 || $rowdeuda["codconcepto"]==54 || $rowdeuda["codconcepto"]==55)
        {
             $interesdeuda      += $rowdeuda["importe"];
             $interesrebajado   += $rowdeuda["importerebajado"];
             $interespagos      += $rowdeuda["imppagado"];
        }
        if($rowdeuda["codconcepto"]==5)
        {
             $igvdeuda      += $rowdeuda["importe"];
             $igvrebajado   += $rowdeuda["importerebajado"];
             $igvpagos      += $rowdeuda["imppagado"];
        }
        if($rowdeuda["codconcepto"]==7)
        {
             $redanteriordeu    += $rowdeuda["importe"];
             $redmesantrebajado += $rowdeuda["importerebajado"]; 
             $redantpagos       += $rowdeuda["imppagado"];
        }
        if($rowdeuda["codconcepto"]==8)
        {
             $redactualdeuda       += $rowdeuda["importe"];
             $redmesactrebajado    += $rowdeuda["importerebajado"];
             $redactpagos          += $rowdeuda["imppagado"];;
        }
        
       $sqlconceptos = "select codtipoconcepto from facturacion.conceptos where codconcepto=".$rowdeuda["codconcepto"];
        $consconcepto = $conexion->query($sqlconceptos);
        $rowconcepto  = $consconcepto->fetch();

        if($rowconcepto[0]==2)
        {
            $otrosdeuda         += $rowdeuda["importe"];
            $otrosrebajado      += $rowdeuda["importerebajado"];
            $otrospagos         += $rowdeuda["imppagado"];
        }
        
    }
    
    $totalmes       = $aguaultimo+$desagueultimo+$cargofijo+$interesultimo+$igvultimo+$redanteriorult+$redactualult+$otroconceptos;
    $totaldeuda     = $aguadeuda+$desaguedeuda+$cargofijodeuda+$interesdeuda+$igvdeuda+$redanteriordeu+$redactualdeuda+$otrosdeuda;
    $totrebajado    = $aguarebajado+$desaguerebajado+$cargorebajado+$otrosrebajado+$interesrebajado+$igvrebajado+$redmesantrebajado+$redmesactrebajado;
    $totpagado      = $aguapagos+$desaguepagos+$cargopagos+$otrospagos+$interespagos+$igvpagos+$redantpagos+$redactpagos;
    
    //saldos---
    $saldoagua      = ($aguaultimo+$aguadeuda)-($aguarebajado+$aguapagos);
    $saldodesague   = ($desagueultimo+$desaguedeuda)-($desaguerebajado+$desaguepagos);
    $saldocargo     = ($cargofijo+$cargofijodeuda)-($cargorebajado+$cargopagos);
    $saldootros     = ($otroconceptos+$otrosdeuda)-($otrosrebajado+$otrospagos);
    $saldointeres   = ($interesultimo+$interesdeuda)-($interesrebajado+$interespagos);
    $saldoigv       = ($igvultimo+$igvdeuda)-($igvrebajado+$igvpagos);
    $saldoredant    = ($redanteriorult+$redanteriordeu)-($redmesantrebajado+$redantpagos);
    $saldoredact    = ($redactualult+$redactualdeuda)-($redmesactrebajado+$redactpagos);
    
    $saldotot   = $saldoagua+$saldodesague+$saldocargo+$saldootros+$saldointeres+$saldoigv+$saldoredant+$saldoredact;
    
?>
<script>
    //---Label Ultimo Mes-----
    $("#lblaguaultimo").html('<?=number_format($aguaultimo,2)?>')
    $("#lbldesagueultimo").html('<?=number_format($desagueultimo,2)?>')
    $("#lblcargofijo").html('<?=number_format($cargofijo,2)?>') 
    $("#lblconceptoultimo").html('<?=number_format($otroconceptos,2)?>')
    $("#lblinteresultimo").html('<?=number_format($interesultimo,2)?>')  
    $("#lbligvultimo").html('<?=number_format($igvultimo,2)?>')
    $("#lblanteriorultimo").html('<?=number_format($redanteriorult,2)?>') 
    $("#lblactual").html('<?=number_format($redactualult,2)?>')     
    $("#lbltotmes").html('<?=number_format($totalmes,2)?>')  
    
    //---Label Deuda Mes-----
    $("#lblaguadeuda").html('<?=number_format($aguadeuda,2)?>') 
    $("#lbldesaguedeuda").html('<?=number_format($desaguedeuda,2)?>')
    $("#lblcargofijodeuda").html('<?=number_format($cargofijodeuda,2)?>')
    $("#lblotrosdeuda").html('<?=number_format($otrosdeuda,2)?>')  
    $("#lblinteresdeuda").html('<?=number_format($interesdeuda,2)?>') 
    $("#lbligvdeuda").html('<?=number_format($igvdeuda,2)?>') 
    $("#lblanteriordeuda").html('<?=number_format($redanteriordeu,2)?>') 
    $("#lblactualdeuda").html('<?=number_format($redactualdeuda,2)?>') 
    $("#lbltotdeuda").html('<?=number_format($totaldeuda,2)?>')
    
    //---Label Rebajado-----
    $("#lblaguarebajado").html('<?=number_format($aguarebajado,2)?>')
    $("#lbldesaguerebajado").html('<?=number_format($desaguerebajado,2)?>')
    $("#lblcargofijorebajado").html('<?=number_format($cargorebajado,2)?>')
    $("#lblotrosrebajado").html('<?=number_format($otrosrebajado,2)?>')
    $("#lblinteresrebajado").html('<?=number_format($interesrebajado,2)?>')
    $("#lbligvrebajado").html('<?=number_format($igvrebajado,2)?>')
    $("#lblanteriorrebajado").html('<?=number_format($redmesantrebajado,2)?>')
    $("#lblactualrebajado").html('<?=number_format($redmesactrebajado,2)?>')
    $("#lbltotrebajado").html('<?=number_format($totrebajado,2)?>')
    
    //---Label Pagados-----
    $("#lblaguapagos").html('<?=number_format($aguapagos,2)?>')
    $("#lbldesaguepagos").html('<?=number_format($desaguepagos,2)?>')    
    $("#lblcargopagos").html('<?=number_format($cargopagos,2)?>')
    $("#lblotrospagos").html('<?=number_format($otrospagos,2)?>')
    $("#lblinterespagos").html('<?=number_format($interespagos,2)?>')
    $("#lbligvpagos").html('<?=number_format($igvpagos,2)?>') 
    $("#lblanteriorpagado").html('<?=number_format($redantpagos,2)?>')    
    $("#lblactualpagado").html('<?=number_format($redactpagos,2)?>')
    $("#lbltotpagado").html('<?=number_format($totpagado,2)?>')
    
    
    //---Label Saldos-----
    $("#lblaguasaldo").html('<?=number_format($saldoagua,2)?>')
    $("#lbldesaguesaldo").html('<?=number_format($saldodesague,2)?>')
    $("#lblcargosaldo").html('<?=number_format($saldocargo,2)?>')
    $("#lblotrossaldo").html('<?=number_format($saldootros,2)?>')
    $("#lblinteressaldo").html('<?=number_format($saldointeres,2)?>')  
    $("#lbligvsaldo").html('<?=number_format($saldoigv,2)?>')
    $("#lblanteriorsaldo").html('<?=number_format($saldoredant,2)?>')
    $("#lblactualsaldo").html('<?=number_format($saldoredact,2)?>')
    $("#lbltotsaldo").html('<?=number_format($saldotot,2)?>')
    
</script>
<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetpagos" rules="all" >
<tr class="ui-widget-header">
        <td>RUBROS</td>
        <td>ULTIMO MES</td>
        <td>TOTAL DEUDA</td>
        <td>REFINANCIADO</td>
        <td>REBAJADO</td>
        <td>PAGOS</td>
        <td>SALDO</td>
    </tr>
    <tr style="padding: 2px">
        <td>AGUA</td>
        <td align="right">
            <label id="lblaguaultimo"></label>
        </td>
        <td align="right">
            <label id="lblaguadeuda"></label>
        </td>
        <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lblaguarebajado"></label>
        </td>
        <td align="right">
            <label id="lblaguapagos"></label>
        </td>
        <td align="right">
            <label id="lblaguasaldo"></label>
        </td>
    </tr>
    <tr style="padding: 2px">
        <td>DESAGUE</td>
        <td align="right">
            <label id="lbldesagueultimo"></label>
        </td>
        <td align="right">
            <label id="lbldesaguedeuda"></label>
        </td>
        <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lbldesaguerebajado"></label>
        </td>
         <td align="right">
            <label id="lbldesaguepagos"></label>
        </td>
         <td align="right">
            <label id="lbldesaguesaldo"></label>
        </td>
    </tr>
    <tr style="padding: 2px">
        <td>CARGO FIJO</td>
        <td align="right">
            <label id="lblcargofijo"></label>
        </td>
        <td align="right">
            <label id="lblcargofijodeuda"></label>
        </td>
        <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lblcargofijorebajado"></label>
        </td>
        <td align="right">
            <label id="lblcargopagos"></label>
        </td>
        <td align="right">
            <label id="lblcargosaldo"></label>
        </td>
    </tr>
    <tr style="padding: 2px">
        <td>OTROS CONCEPTOS</td>
        <td align="right">
            <label id="lblconceptoultimo"></label>
        </td>
        <td align="right">
            <label id="lblotrosdeuda"></label>
        </td>
        <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lblotrosrebajado"></label>
        </td>
        <td align="right">
            <label id="lblotrospagos"></label>
        </td>
         <td align="right">
            <label id="lblotrossaldo"></label>
        </td>
    </tr>
    <tr style="padding: 2px">
        <td>INTERESES Y MORA</td>
        <td align="right">
            <label id="lblinteresultimo"></label>
        </td>
        <td align="right">
            <label id="lblinteresdeuda"></label>
        </td>
         <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lblinteresrebajado"></label>
        </td>
        <td align="right">
            <label id="lblinterespagos"></label>
        </td>
        <td align="right">
            <label id="lblinteressaldo"></label>
        </td>
    </tr>
    <tr style="padding: 2px">
        <td>IGV</td>
        <td align="right">
            <label id="lbligvultimo"></label>
        </td>
        <td align="right">
            <label id="lbligvdeuda"></label>
        </td>
         <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lbligvrebajado"></label>
        </td>
        <td align="right">
            <label id="lbligvpagos"></label>
        </td>
        <td align="right">
            <label id="lbligvsaldo"></label>
        </td>
    </tr>
    <tr style="padding: 2px">
        <td>REDONDEO MES ANTERIOR</td>
        <td align="right">
            <label id="lblanteriorultimo"></label>
        </td>
        <td align="right">
            <label id="lblanteriordeuda"></label>
        </td>
         <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lblanteriorrebajado"></label>
        </td>
        <td align="right">
            <label id="lblanteriorpagado"></label>
        </td>
        <td align="right">
            <label id="lblanteriorsaldo"></label>
        </td>
    </tr>
    <tr style="padding: 2px">
        <td>REDONDEO MES ACTUAL</td>
        <td align="right">
            <label id="lblactual"></label>
        </td>
        <td align="right">
            <label id="lblactualdeuda"></label>
        </td>
         <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lblactualrebajado"></label>
        </td>
        <td align="right">
            <label id="lblactualpagado"></label>
        </td>
         <td align="right">
            <label id="lblactualsaldo"></label>
        </td>
    </tr>
    <tr class="ui-widget-header">
        <td>TOTAL</td>
        <td align="right">
            <label id="lbltotmes"></label>
        </td>
        <td align="right">
            <label id="lbltotdeuda"></label>
        </td>
         <td align="right">
            0.00
        </td>
        <td align="right">
            <label id="lbltotrebajado"></label>
        </td>
        <td align="right">
            <label id="lbltotpagado"></label>
        </td>
        <td align="right">
            <label id="lbltotsaldo"></label>
        </td>
    </tr>
</table>