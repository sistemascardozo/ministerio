<?php	
 	include("../../../../objetos/clsReporte.php");
			
	class PDF extends clsReporte
	{
            function cabecera()
            {
				global $empresa, $fechaserver, $cars, $caja, $fecha, $cajeros, $img;
				
				$h = 4;
				$this->SetFont('Arial', 'B', 14);
				$tit1 = "RECORD DE FACTURACION";
				$this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');	
            }
            function contenido($nroinscripcion)
            {
                global $conexion, $idsucursal;
                $this->Ln(5);

                $h=4;
                $this->SetFont('Arial','',5);
                
                $sqlcliente = "select clie.nroinscripcion,clie.propietario,tar.nomtar,estser.descripcion as estado,tipserv.descripcion as servicio,
                               (select codcatastro from catastro.view_codcatastro where nroinscripcion=clie.nroinscripcion AND codsuc=clie.codsuc ) as codcatastro,
                               tipcal.descripcioncorta || ' ' || cal.descripcion || ' # ' || clie.nrocalle as direccion
                               from catastro.clientes as clie
                               inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
                               inner join public.estadoservicio as estser on(clie.codestadoservicio=estser.codestadoservicio)
                               inner join public.tiposervicio as tipserv on(clie.codtiposervicio=tipserv.codtiposervicio)
                               inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
                               inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
                               where clie.nroinscripcion=".$nroinscripcion." AND clie.codsuc=".$idsucursal;
                 $consulta = $conexion->query($sqlcliente);
                $rowcliente  = $consulta->fetch();
                
                $this->Cell(25,$h,"NRO. INSCRIPICON",0,0,'L');
                $this->Cell(5,$h,":",0,0,'C');
                $this->Cell(50,$h,$rowcliente["nroinscripcion"],0,0,'L');
                
                $this->Cell(25,$h,"CATASTRO",0,0,'L');
                $this->Cell(5,$h,":",0,0,'C');
                $this->Cell(25,$h,$rowcliente["codcatastro"],0,1,'L');
                
                $this->Cell(25,$h,"PROPIETARIO",0,0,'L');
                $this->Cell(5,$h,":",0,0,'C');
                $this->Cell(50,$h,strtoupper($rowcliente["propietario"]),0,1,'L');
                
                $this->Cell(25,$h,"DIRECCION",0,0,'L');
                $this->Cell(5,$h,":",0,0,'C');
                $this->Cell(50,$h,strtoupper($rowcliente["direccion"]),0,1,'L');
                
                $this->Cell(25,$h,"SERVICIO",0,0,'L');
                $this->Cell(5,$h,":",0,0,'C');
                $this->Cell(50,$h,$rowcliente["servicio"],0,1,'L');
                
                $this->Cell(25,$h,"EST. SERVICIO",0,0,'L');
                $this->Cell(5,$h,":",0,0,'C');
                $this->Cell(50,$h,$rowcliente["estado"],0,0,'L'); 
                
                $this->Cell(25,$h,"CATEGORIA",0,0,'L');
                $this->Cell(5,$h,":",0,0,'C');
                $this->Cell(50,$h,$rowcliente["nomtar"],0,1,'L'); 
                
                $this->Ln(3);
                $this->Cell(10,$h,"FACT.",1,0,'C');
                $this->Cell(10,$h,utf8_decode("AÑO"),1,0,'C');
                $this->Cell(10,$h,"MES",1,0,'C');
                $this->Cell(10,$h,"DOC.",1,0,'C');
                $this->Cell(10,$h,"SERIE",1,0,'C');
                $this->Cell(15,$h,"NRO. DOC.",1,0,'C');
                $this->Cell(15,$h,"DIA PAGO",1,0,'C');
                $this->Cell(15,$h,"AGUA",1,0,'C');
                $this->Cell(15,$h,"DESAGUE",1,0,'C');
                $this->Cell(15,$h,"CARGO FIJO",1,0,'C');
                $this->Cell(15,$h,"OTROS",1,0,'C');
                $this->Cell(15,$h,"INTERES",1,0,'C');
                $this->Cell(15,$h,"REDONDEO",1,0,'C');
                $this->Cell(15,$h,"TOTAL",1,1,'C');
                
                $totagua        = 0.00;
                $totdesague     = 0.00;
                $tototros       = 0.00;
                $totinteres     = 0.00;
                $totgeneral     = 0.00;
                $totcargo       = 0.00;
                $totredondeo    = 0.00;
                
                $sqlfacturacion="select nrofacturacion,anio,mes,coddocumento,serie,nrodocumento 
                                 from facturacion.cabfacturacion where nroinscripcion=".$nroinscripcion." and codsuc=".$idsucursal."
                                 order by anio,mes asc";
               $consfacturacion  = $conexion->query($sqlfacturacion);
                $consfacturacion=  $consfacturacion->fetchAll();
                foreach($consfacturacion as $rowfacturacion)
                {
                    $impagua        = 0.00;
                    $impdesague     = 0.00;
                    $otros          = 0.00;
                    $interes        = 0.00;
                    $impcargo       = 0.00;
                    $impredant      = 0.00;
                    $impredact      = 0.00;
                    
                    $sqldetfacturacion="select df.codconcepto,df.importe,
                                        co.codtipoconcepto 
                                        from facturacion.detfacturacion df
                                        inner join facturacion.conceptos co on df.codconcepto=co.codconcepto AND df.codsuc=co.codsuc
                                        where df.nrofacturacion=".$rowfacturacion["nrofacturacion"]." and df.nroinscripcion=".$nroinscripcion." and df.codsuc=".$idsucursal;
                    $consdetalle  = $conexion->query($sqldetfacturacion);
                    $consdetalle=  $consdetalle->fetchAll();
                    foreach($consdetalle as $rowdetalle)
                    {
                        if($rowdetalle["codconcepto"]==1){$impagua+=$rowdetalle["importe"];}
                        if($rowdetalle["codconcepto"]==2){$impdesague+=$rowdetalle["importe"];}
                        if($rowdetalle["codtipoconcepto"]==2){$otros+=$rowdetalle["importe"];}
                        if($rowdetalle["codconcepto"]==3  || $rowdetalle["codconcepto"]==4 || 
                           $rowdetalle["codconcepto"]==9 || $rowdetalle["codconcepto"]==54 || $rowdetalle["codconcepto"]==55)
                           {$interes+=$rowdetalle["importe"];}
                        if($rowdetalle["codconcepto"]==6){$impcargo+=$rowdetalle["importe"];}
                        if($rowdetalle["codconcepto"]==7){$impredant+=$rowdetalle["importe"];}
                        if($rowdetalle["codconcepto"]==8){$impredact+=$rowdetalle["importe"];}
                    }
                    
                    $sqlpagos = "select fechareg from cobranza.cabpagos
                                 inner join cobranza.detpagos on cabpagos.codemp=detpagos.codemp and cabpagos.codsuc=detpagos.codsuc and
                                 cabpagos.nroinscripcion=detpagos.nroinscripcion and cabpagos.nropago=detpagos.nropago
                                 where detpagos.nrofacturacion=".$rowfacturacion["nrofacturacion"]." and cabpagos.nroinscripcion=".$nroinscripcion;
                    
                    $conspagos  = $conexion->query($sqlpagos);
                    $rowpagos=  $conspagos->fetch();

                    $redondeo=$impredant+$impredact;
                    
                    $total=$impagua+$impdesague+$otros+$interes+$impcargo+$redondeo;
                    $totgeneral += $total;
                    
                    $this->Cell(10,$h,$rowfacturacion["nrofacturacion"],1,0,'C');
                    $this->Cell(10,$h,$rowfacturacion["anio"],1,0,'C');
                    $this->Cell(10,$h,$rowfacturacion["mes"],1,0,'C');
                    $this->Cell(10,$h,$rowfacturacion["coddocumento"],1,0,'C');
                    $this->Cell(10,$h,$rowfacturacion["serie"],1,0,'C');
                    $this->Cell(15,$h,$rowfacturacion["nrodocumento"],1,0,'C');
                    $this->Cell(15,$h,$this->DecFecha($rowpagos["fechareg"]),1,0,'C');
                    $this->Cell(15,$h,number_format($impagua,2),1,0,'R');
                    $this->Cell(15,$h,number_format($impdesague,2),1,0,'R');
                    $this->Cell(15,$h,number_format($impcargo,2),1,0,'R');
                    $this->Cell(15,$h,number_format($otros,2),1,0,'R');
                    $this->Cell(15,$h,number_format($interes,2),1,0,'R');
                    $this->Cell(15,$h,number_format($redondeo,2),1,0,'R');
                    $this->Cell(15,$h,number_format($total,2),1,1,'R');
                    
                    $totagua    += $impagua;
                    $totdesague += $impdesague;
                    $tototros   += $otros;
                    $totinteres += $interes;
                    $totcargo   += $impcargo;
                    $totredondeo+= $redondeo;
                    
                }
                
                $this->Cell(10,$h,"",0,0,'C');
                $this->Cell(10,$h,"",0,0,'C');
                $this->Cell(10,$h,"",0,0,'C');
                $this->Cell(10,$h,"",0,0,'C');
                $this->Cell(10,$h,"",0,0,'C');
                $this->Cell(15,$h,"",0,0,'C');
                $this->Cell(15,$h,"Totales=>",0,0,'R');
                $this->Cell(15,$h,number_format($totagua,2),1,0,'R');
                $this->Cell(15,$h,number_format($totdesague,2),1,0,'R');
                $this->Cell(15,$h,number_format($totcargo,2),1,0,'R');
                $this->Cell(15,$h,number_format($tototros,2),1,0,'R');
                $this->Cell(15,$h,number_format($totinteres,2),1,0,'R');
                $this->Cell(15,$h,number_format($totredondeo,2),1,0,'R');
                $this->Cell(15,$h,number_format($totgeneral,2),1,1,'R');
            }
            
        }
	
	$fechac = date("d/m/Y");
	
	$nroinscripcion = $_GET["nroinscripcion"];
	
	$idsucursal = $_SESSION['IdSucursal'];
	$codsuc		= $_SESSION['IdSucursal'];
	
	$fechaserver	= date('d/m/Y');

        
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->contenido($nroinscripcion);
	$pdf->Output();	
	
?>