<?php
     if(!session_start()){session_start();}
    include("../../../objetos/clsMantenimiento.php");

  
  $objMantenimiento   = new clsMantenimiento();
    
    $nroinscripcion = $_POST["nroinscripcion"];
    $nrofacturacion=$_SESSION["nrofacturacion"] - 1;
    $codsuc = $_SESSION['IdSucursal'];
    
   $sql        = "select clie.nroinscripcion,upper(clie.propietario) as propietario,
                   upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle) as direccion,upper(tar.nomtar) as categoria,
                   clie.catetar,upper(estser.descripcion) as estado,upper(tipser.descripcion) as tiposervicio,upper(sect.descripcion) as sector,
                   upper(tipusu.descripcion) as tipousuario,clie.nromed,clie.lecturaanterior,clie.fechalecturaanterior,clie.lecturaultima,
                   clie.fechalecturaultima,clie.codemp,clie.codsuc,
                   clie.consumo,clie.codrutlecturas,clie.codrutdistribucion,clie.lecturapromedio,clie.tipofacturacion,
                   case when clie.loteantiguo is null then clie.lote else clie.loteantiguo end as nrolote,clie.domestico,clie.comercial,
                   clie.industrial,clie.social,clie.estatal,clie.codsector,clie.codmanzanas,".$objMantenimiento->getCodCatastral("clie.").",clie.codciclo
                   from catastro.clientes as clie 
                   inner join public.calles as cal on (clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
                   inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
                   inner join facturacion.tarifas as tar on(clie.catetar=tar.catetar)  AND (clie.codemp=tar.codemp)  AND (clie.codsuc=tar.codsuc) 
                   inner join public.estadoservicio as estser on(clie.codestadoservicio=estser.codestadoservicio)
                   inner join public.tiposervicio as tipser on(clie.codtiposervicio=tipser.codtiposervicio)
                   inner join public.sectores as sect on(clie.codsector=sect.codsector) AND (clie.codemp=sect.codemp)  AND (clie.codsuc=sect.codsuc) 
                   inner join public.tipousuario as tipusu on(clie.codtipousuario=tipusu.codtipousuario)
                   where clie.nroinscripcion=".$nroinscripcion." AND clie.codsuc=".$codsuc;
   $consulta = $conexion->query($sql);
   $row  = $consulta->fetch();
    
    $sqlconsumo     = "select consumofact from facturacion.cabfacturacion where nroinscripcion=".$nroinscripcion." and nrofacturacion=".($nrofacturacion)." AND codsuc=".$codsuc;
    $consconsumo = $conexion->query($sqlconsumo);
   $rowconsumo  = $consconsumo->fetch();

    
    $t  = $row["nroinscripcion"]."|".$row["propietario"]."|".utf8_decode($row["direccion"])."|".$row["categoria"]."|".$row["catetar"]."|".$row["estado"];
    $t .= "|".$row["tiposervicio"]."|".$row["sector"]."|".$row["tipousuario"]."|".$row["codcatastro"]."|".$row["nromed"]."|".$row["lecturaanterior"];
    $t .= "|".$objMantenimiento->DecFecha($row["fechalecturaanterior"])."|".$row["lecturaultima"]."|".$objMantenimiento->DecFecha($row["fechalecturaultima"])."|".$row["consumo"];
    $t .= "|".$row["codrutlecturas"]."|".$rowconsumo[0]."|".$row["codrutdistribucion"]."|".$row["lecturapromedio"]."|";
    $t .= $tfacturacion[$row["tipofacturacion"]]."|".$row["codemp"]."|".$row["codsuc"]."|".$row["nrolote"]."|".$row["domestico"]."|".$row["comercial"];
    $t .= "|".$row["industrial"]."|".$row["social"]."|".$row["estatal"]."|".$row["codsector"]."|".$row["codmanzanas"]."|".$row["codcatastro"]."|".$row["codciclo"];
   
    echo $t;

?>
