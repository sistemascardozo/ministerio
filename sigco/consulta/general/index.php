<?php
  include("../../../include/main.php");
  include("../../../include/claseindex.php");

  $TituloVentana = "CONSULTA GENERAL";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $codsuc   = $_SESSION['IdSucursal'];
  $nrofacturacion=$_SESSION["nrofacturacion"] - 1;

?>

<style>
	.text1{
		font-size:11px;
		font-weight:bold;
		color:#000;
	}
	.text2{
		font-size:11px;
		font-weight:bold;
		color:#800000;
	}
  .fondo_td{
          background-color:cadetblue; 
          color:#FFFFFF; 
          font-weight: bold;
  }
  .fondo_div{
          border:1px #000 dashed; 
          background-color: #99FF33;
  }
</style>
<script>
        var catastro="";
        var ruta="";
//    	$(document).ready(function(){
//		function formatResult(row) {
//			return row[0];
//		}
//		function formatItem2(row) {
//		    r = "<table width='400'>";
//			r = r + "<td width='20'>" + row[0] + "</td>";
//			r = r + "<td width='250'>" + row[1] + "</td>";
//			r = r + "<td width='220'>" + row[2] + "</td>";
//			r = r + "<td width='20'>" + row[3] + "</td>";
//			r = r + "</tr>"
//			r = r + "</table>";
//			return r;
//		}
//		
//		$('#nroinscripcion').autocomplete('../../admin/autocomplet/clientes.php', {
//			autoFill: true,
//			width: 500,
//			selectFirst: true,
//			extraParams: {Campo: 'codcliente',Op:0},
//			formatItem: formatItem2, 
//			formatResult: formatResult,
//			mustMatch : false
//		}).result(function(event, item) {
//			  cagar_datos(item[0])
//                          cargar_detalle(item[0])
//		});	
//			 
//	}
//	);

  function cargar_datos_periodo_facturacion(codciclo)
  {
    $.ajax({
       url:'<?php echo $_SESSION['urldir'];?>ajax/periodo_facturacion.php',
       type:'POST',
       async:true,
       data:'codsuc=<?=$codsuc?>&codciclo='+codciclo,
       success:function(datos){
       
       }
      })  
  }

  function BuscarUsuarios()
  {
    AbrirPopupBusqueda('../../catastro/operaciones/actualizacion/?Op=5',1100,600)
  }

  function Recibir(id)
	{
           cagar_datos(id)
           cargar_detalle(id)
	}
        
  function cagar_datos(id)
  {
    $.ajax({
      url:'ajax_usuarios.php',
      type:'POST',
      async:true,
      data:'nroinscripcion='+id,
      success:function(datos){
        var r=datos.split("|")
                    
          cargar_datos_periodo_facturacion(r[32])
            $("#nroinscripcion").val(id)
            $("#lblpropietario").html(r[1])
            $("#lbldireccion").html(r[2])
            $("#lblcategoria").html(r[4]+' - '+r[3])
            $("#lblestado").html(r[5])
            $("#lbltiposervicio").html(r[6])
            $("#lblsector").html(r[7])
            $("#lbltipousuario").html(r[8])
            $("#lblnromed").html(r[10])
            $("#lbllecturaanterior").html(r[11])
            $("#lblfechanterior").html(r[12])
            $("#lbllecturaultima").html(r[13])
            $("#lblfechultima").html(r[14])
            $("#lblconsumo").html(r[15])
            $("#lblrutlecturas").html(r[16])
            $("#lblconsumofact").html(r[17])
            $("#lblrutreparto").html(r[18])
            $("#lblpromedio").html(r[19])
            $("#lbltipofacturacion").html(r[20])
            $("#lblcodsector").html(1)
            $("#lblmanzana").html(r[16])
            //$("#lblempresa").html(r[21])
            $("#lblsucursal").html(1)
            $("#lblrutalectura2").html(r[16])
            $("#lbllote").html(r[23])
            $("#lbldomestico").html(r[24])
            $("#lblcomercial").html(r[25])
            $("#lblindustrial").html(r[26])
            $("#lblsocial").html(r[27])
            $("#lblestatal").html(r[28])
            
            catastro=r[31]
            $("#Codcatastral").html(r[31])
            $("#codciclo").val(r[32])
            
            ruta=r[16]
		  }
    }) 
  }

  function cargar_detalle(id)
  {
    $.ajax({
  		url:'ajax_detalle_consulta.php',
  		type:'POST',
  		async:true,
  		data:'nroinscripcion='+id,
  		success:function(datos){
        $("#div_detalle").html(datos)
      }
    })
  }

	function Cancelar()
	{
    location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}

  function imprimirduplicado()
	{
    if(catastro=="")
    {
        alert("Seleccione Usuario a Imprimir su Recibo Duplicado")
        return
    }
    
    AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/cobranza/operaciones/cancelacion/impresion/imprimir_duplicado.php?nroinscripcion="+$("#nroinscripcion").val()+"&codsuc=<?= $codsuc?>&codciclo="+$("#codciclo").val(),800,600)
  }

	function imprimirsustitutorio()
	{
    if(catastro=="")
    {
      alert("Seleccione Usuario a Imprimir su Recibo Duplicado")
      return
    }
    
    AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/cobranza/operaciones/cancelacion/consulta/imprimir_sustitutorio.php?nroinscripcion="+$("#nroinscripcion").val()+"&codsuc=<?= $codsuc?>&codciclo="+$("#codciclo").val(),800,600)
  }

  function imprimirrecordfacturacion()
  {
      nroinscripcion=$("#nroinscripcion").val()
      if(nroinscripcion=="")
      {
          alert("Seleccione un Usuario para poder Consulta su Record de Facturacion")
          return
      }
      
      AbrirPopupImpresion("impresiones/recordfacturacion.php?nroinscripcion="+nroinscripcion,800,600)
  }

  function historialpagos()
  {
      nroinscripcion=$("#nroinscripcion").val()
      if(nroinscripcion=="")
      {
          alert("Seleccione un Usuario para poder Consultar su Historial de Pagos")
          return
      }
      
      AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/Pdf.php?NroInscripcion="+nroinscripcion,800,600)
  }
  
  function tarjetalectura()
  {
      nroinscripcion=$("#nroinscripcion").val()
      if(nroinscripcion=="")
      {
          alert("Seleccione un Usuario para poder Consultar su Tarjeta de Lecturas")
          return
      }
      
     AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/medicion/listado/historico/Pdf.php?NroInscripcion="+$("#nroinscripcion").val(),800,600)
  }

  function VerOcurrencias()
  {
    //alert();
    nroinscripcion=$("#nroinscripcion").val()
    if(nroinscripcion=="")
    {
      alert("Seleccione un Usuario para poder Consultar sus Ocurrencias")
      return
    }
    
    AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/facturacion/listado/ocurrencias/Pdf.php?NroInscripcion="+nroinscripcion,800,600)
  }
</script>
<div align="center">
<form id="form1" name="form1" method="post">
 <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
    <tr>
      <td colspan="2">
          <fieldset style="padding:4px">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr style="padding:2px" class="text1">
                      <td width="10%">N. Inscripcion <input type="hidden" id="codciclo"></td>
                      <td width="2%" align="center">:</td>
                      <td width="14%">
                          &nbsp;
                          <input type="text" name="nroinscripcion" readonly="readonly" id="nroinscripcion" class="inputtext" onkeypress="return permite(event,'num');" size="8" />
                          <span class="MljSoft-icon-buscar" alt="Buscar Usuario" onclick="BuscarUsuarios();"></span>
                      </td>
                      <td width="6%" align="right" colspan="5">Cod. Catastral</td>
                      <td width="2%" align="center">:</td>
                      <td width="5%" align="left" colspan="4">
                         <label id="Codcatastral"></label>
                      
                      </td>
                      <td width="10%" colspan="2" align="right">Lote</td> 
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="8%" align="center">
                          
                              <label id="lbllote"></label>
                          
                      </td>
                      <td width="2%">&nbsp;</td>
                      <td width="2%">&nbsp;</td>
                      <td width="5%" colspan="3">&nbsp;</td>
                  </tr>
                  <tr style="padding:2px" class="text1">
                      <td width="10%">Usuario</td>
                      <td width="2%" align="center">:</td>
                      <td width="32%" colspan="4">
                          &nbsp;
                          <label id="lblpropietario"></label>
                      </td>
                      <td width="2%">&nbsp;</td>
                      <td width="2%">&nbsp;</td>
                      <td width="2%">&nbsp;</td>
                      <td width="2%">&nbsp;</td>
                      <td width="5%">&nbsp;</td>
                      <td width="5%">&nbsp;</td>
                      <td width="5%">&nbsp;</td>
                      <td width="10%" colspan="2" align="right">Nro. Med.</td>
                      <td width="2%" align="center">:</td>
                      <td width="5%" align="center">
                          <div class="fondo_td">
                              &nbsp;
                              <label id="lblnromed"></label>
                          </div>
                      </td>
                      <td width="5%">&nbsp;</td>
                      <td width="5%">&nbsp;</td>
                      <td width="5%" colspan="3">&nbsp;</td>
                  </tr>
                  <tr style="padding:2px" class="text1">
                      <td width="10%">Direccion</td>
                      <td width="2%" align="center">:</td>
                      <td width="32%" colspan="4">
                          &nbsp;
                          <label id="lbldireccion"></label>
                      </td> 
                      <td width="10%" colspan="2" align="right">Tipo Fact.</td>
                      <td width="2%" align="center">:</td>
                      <td width="17%" colspan="4">
                          &nbsp;
                          <label id="lbltipofacturacion"></label>
                      </td>
                      <td width="10%" colspan="2" align="right">Lect. Ant.</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="5%" align="center">
                          &nbsp;
                          <label id="lbllecturaanterior"></label>
                      </td>
                      <td width="8%" colspan="2" align="right">Fec. Ant.</td>
                      <td width="2%" align="center">:</td>
                      <td width="20%" colspan="2">
                          <label id="lblfechanterior">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                      </td>
                  </tr>
                  <tr style="padding:4px">
                      <td width="10%" class="text1">Categoria</td>
                      <td width="2%" class="text1" align="center">:</td>
                      <td width="32%" colspan="4" class="text2">
                          &nbsp;
                          <label id="lblcategoria"></label>
                      </td>
                      <td width="10%" colspan="2" align="right" class="text1">Servicio</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="17%" colspan="4" class="text1">
                          &nbsp;
                          <label id="lbltiposervicio"></label>
                      </td>
                      <td width="10%" colspan="2" align="right" class="text1">Lect. Ult.</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="5%" align="center" class="text1">
                          &nbsp;
                          <label id="lbllecturaultima"></label>
                      </td>
                      <td width="10%" colspan="2" align="right" class="text1">Fec. Ult.</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="5%" class="text1">
                          <label id="lblfechultima"></label>
                          &nbsp;
                      </td>
                  </tr>
                  <tr style="padding:4px">
                      <td width="10%" class="text1">Est. Servicio</td>
                      <td width="2%" class="text1" align="center">:</td>
                      <td width="32%" colspan="4" class="text2">
                          &nbsp;
                          <label id="lblestado"></label>
                      </td>
                      <td width="10%" colspan="2" align="right" class="text1">Sector</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="17%" colspan="4" class="text1">
                          &nbsp;
                          <label id="lblsector"></label>
                      </td>
                      <td width="10%" colspan="2" class="text1" align="right">Consumo</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="5%" class="text1" align="center">
                           &nbsp;
                          <label id="lblconsumo"></label>
                      </td>
                      <td width="10%" colspan="2" class="text1" align="right">R. Lect.</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="5%" align="center" class="text1">
                          &nbsp;
                          <label id="lblrutlecturas"></label>
                      </td>
                  </tr>
                  <tr style="padding:2px" class="text1">
                      <td width="44%" colspan="6">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:1px #000 dashed">
                              <tr style="padding:2px">
                                  <td width="10%" class="fondo_td">DOM</td>
                                  <td width="10%" align="center">
                                      &nbsp;
                                      <label id="lbldomestico"></label>
                                  </td>
                                  <td width="10%" class="fondo_td">COM</td>
                                  <td width="10%" align="center">
                                      &nbsp;
                                      <label id="lblcomercial"></label>
                                  </td>
                                  <td width="10%" class="fondo_td">IND</td>
                                  <td width="10%" align="center">
                                      &nbsp;
                                      <label id="lblindustrial"></label>
                                  </td>
                                  <td width="10%" class="fondo_td">SOC</td>
                                  <td width="10%" align="center">
                                      &nbsp;
                                      <label id="lblsocial"></label>
                                  </td>
                                  <td width="10%" class="fondo_td">EST</td>
                                  <td width="10%" align="center">
                                      &nbsp;
                                      <label id="lblestatal"></label>
                                  </td>
                              </tr>
                          </table>
                      </td>
                      <td width="10%" colspan="2" align="right">Tipo Usuario</td>
                      <td width="2%" align="center">:</td>
                      <td width="17%" colspan="4">
                          &nbsp;
                          <label id="lbltipousuario"></label>
                      </td>
                      <td width="10%" colspan="2" align="right">Cons. Fact.</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="5%" align="center">
                          &nbsp;
                          <label id="lblconsumofact"></label>
                      </td>
                      <td width="10%" colspan="2" align="right">R. Rep.</td>
                      <td width="2%" align="center" class="text1">:</td>
                      <td width="5%" align="center">
                          &nbsp;
                          <label id="lblrutreparto"></label>
                      </td>
                  </tr>
              </table>
          </fieldset>
      </td>
    </tr>
   
    <tr>
        <td colspan="2">
            <div id="div_detalle"></div>
        </td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">
          <fieldset style="padding:4px">
              <table border="0" width="100%">
                  <tr style="padding:4px" align="center">
                      <td>
                          <input type="button" name="duplicado" id="duplicado" value="Recibo Duplicado" class="button" onclick="imprimirduplicado();" />
                      </td>
                      <td>
                          <input type="button" name="sustitutorio" id="sustitutorio" value="Recibo Sustitutorio" class="button" onclick="imprimirsustitutorio();" style="display:none" />
                          <input type="button" name="lectura" id="lectura" value="Tarjeta de Lectura" class="button" onclick="tarjetalectura();" />
                      </td>
                      <td>
                          <input type="button" name="recorfacturacion" id="recorfacturacion" value="Record de Facturacion" class="button" onclick="imprimirrecordfacturacion();" />
                      </td>
                      <td>
                          <input type="button" name="pagos" id="pagos" value="Cuenta Corriente" class="button" onclick="historialpagos();" />
                      </td>
                      <td>
                          <input type="button" name="ocurrencias" id="ocurrencias" value="Ocurrencias" class="button" onclick="VerOcurrencias();" />
                      </td>
                  </tr>
                  
              </table>
          </fieldset>
      </td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
   </tbody>
   
 </table>
</form>
</div>
<script>
    cargar_detalle(0);
</script>
<?php   CuerpoInferior();?>