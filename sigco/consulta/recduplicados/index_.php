<?php 
   if(!session_start()){session_start();}
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "CUENTA CORRIENTE";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>


<script type="text/javascript" src="<?=$urldir?>js/funciones.js" language="JavaScript"></script>
<script>

jQuery(function($)
	{ 
		$( "#DivTipos" ).buttonset();

	});
	var urldir 	= "<?=$urldir?>" 
	var codsuc 	= <?=$codsuc?>

function buscar_usuarios()
	{
		
		object="usuario" 
		AbrirPopupBusqueda('../../../catastro/operaciones/actualizacion/?Op=5',800,600)
	}
function Recibir(id)
{
	if(object=="usuario")
	{
		$("#nroinscripcion").val(id)
		cargar_datos_usuario(id)
	}
	
}
function cargar_datos_usuario(nroinscripcion)
{
	$.ajax({
		 url:'../../../../ajax/clientes.php',
		 type:'POST',
		 async:true,
		 data:'codsuc=1&nroinscripcion='+nroinscripcion,
		  dataType: "json",
		 success:function(datos){
			
			
			$("#cliente").val(datos.propietario)
			Consultar()
		 }
    }) 
}
function Cancelar()
	{
		location.href='<?=$urldir?>admin/index.php'
	}
function Validar()
{
		
	
	var NroInscripcion = $("#nroinscripcion").val()
	var Data=''
	if(NroInscripcion!="")
		Data +='NroInscripcion='+NroInscripcion
	else
	{
		Msj('#nroinscripcion','Seleccione Usuario',1000,'above','',false)
		return false;
	}
	return Data 
}
function Consultar()
	{
		var Data=Validar()
		if(Data!=false)
		{
			$('#DivConsulta').fadeOut(500) 
			$('#ImgLoad').fadeIn();
			
			$("#DivResultado").empty();
			$.ajax({
			 url:'Consulta.php',
			 type:'POST',
			 async:true,
			 data:Data,
			 success:function(datos)
			 {
			 	$('#ImgLoad').fadeOut(500,function(){
			       $("#DivResultado").empty().append(datos);             
					$('#DivConsulta').fadeIn(500,function(){var theTable = $('#TbIndex')
			  		$("#Filtro").keyup(function() {$.uiTableFilter( theTable, this.value)})}) 
				})
			 }
			})
		}
	}
function Pdf()
	{
		var Data=Validar()
		if(Data!=false)
		{
			var ventana=window.open('Pdf.php?'+Data, 'CuentaCorriente', 'resizable=yes, scrollbars=yes');
			ventana.focus();
		}
	}

</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="1000" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>

    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="11%">Sucursal</td>
				    <td width="2%" align="center">:</td>
				    <td width="23%">
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </td>
				    <td width="13%" align="right">&nbsp;</td>
				    <td width="3%" align="center">&nbsp;</td>
				    <td width="48%">
                    	&nbsp;</td>
				  </tr>
				  <tr>
			        <td>Usuario</td>
			        <td align="center">:</td>
			        <td>
			          <input type="text" name="nroinscripcion" id="nroinscripcion" class="inputtext" size="15" maxlength="15" value="" onkeypress="return permite(event,'num');" />
			        	<img src="<?=$urldir?>images/buscar.png" width="16" height="16" style="cursor:pointer" title="Buscar Usuario" onclick="buscar_usuarios();" />
			        	
			    		</td>
			    		<td colspan='2'>
			        <input type="text" name="cliente" id="cliente" class="inputtext" size="45" maxlength="45" value="" readonly="readonly" />
			        </td>
			         </tr>
			      <tr>
				       <td colspan="2">&nbsp;</td>
					</tr>
				<tr>
                <td colspan="6" height="40" align="center">
                
                <table width="500" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                    	<input type="button" value="Guardar" id="BtnAceptar" onclick="Consultar();" />
                    </td>
                     <td align="center" style="display:none"><input id="BtnExcel" type="button" value="Excel" onclick="Excel();" class="BtnIndex"/></td>
                     <td align="center">
                     	<input id="BtnPdf" type="button" value="Pdf" onclick="Pdf();" class="BtnIndex"/>
                    </td>
                    <td align="center">
                    	<input id="BtnCancelar" type="button" value="Cancelar" onclick="Cancelar();" />
                    </td>
                  </tr>
                </table></td>
            </tr>
				</table>
			</fieldset>
		</td>
	</tr>

    <tr>
 	<td colspan="6" >
 		<div id="ImgLoad" style="text-align:center;display:none">
	        <span class="icono-icon-loading"></span>
	        Cargando ...
	    </div>
	    <div id="DivConsulta" style="height:auto; overflow:auto;display:none ">
       	<fieldset>
        <legend class="ui-state-default ui-corner-all" >Resultados de la Consulta</legend>
			<div style="height:auto; overflow:auto;" align="center" id="DivResultado">
                <table class="ui-widget" border="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
                                <thead class="ui-widget-header" style="font-size:10px">
                              	<tr>
							        <th colspan="8">Desde el <?=($Fecha)?> Hasta el <?=($Fecha)?>  </th>
							    </tr>
                                <tr>
                                	<th width="50" scope="col">N&deg;</th>
							        <th width="50" scope="col">N&deg; Prestamo</th>
							        <th width="100" scope="col">Fecha</th>
							        <th width="500" scope="col">Beneficiario</th>
							        <th width="100" scope="col">Fecha Cosecha</th>
							        <th width="100" scope="col">Monto</th>
							        <th width="100" scope="col">Estado</th>
							        <th width="100" scope="col">&nbsp;</th>
 				    				
                                </tr>
                                </thead>
                                <tbody style="font-size:12px">
                                </tbody>
                                <tfoot class="ui-widget-header" style="font-size:10px">
                                	<tr>
				                	<td colspan="8" align="right" >&nbsp;</td>
                    				</tr>
                                </tfoot>
                            </table>
                             </div>
                                
                                  </fieldset>
                                
       
                            </td>
                            </tr>
        </div>
	 </tbody>
	
    </table>
 </form>
</div>
<script>
//$('#ImgLoad').fadeOut(500,function(){$('#DivConsulta').fadeIn(500)});
$("#BtnAceptar").attr('value','Consultar')
$("#BtnAceptar").css('background-image','url(<?=$urldir?>css/images/ver.png)')
</script>
<?php CuerpoInferior(); ?>