<?php 
	if(!session_start()){session_start();} 
	
	include("../../../objetos/clsFunciones.php");
	
	
	$Op  	= $_GET["op"];
	$objFunciones = new clsFunciones();
	
	$codemp			= 1;
	$codsuc			= $_SESSION['IdSucursal'];
	$nroinscripcion	= $_POST["nroinscripcion"];

	//OBTENER DADOS DEL USUARIO
	 $sql = "SELECT c.codcliente,c.propietario,c.codtipodocumento,c.nrodocumento,c.codciclo,tc.descripcioncorta,
            cl.descripcion as calle,c.nrocalle,c.codciclo,td.abreviado,c.codsector,
            c.codcalle,c.codantiguo
            FROM catastro.clientes as c
            INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
            inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
            where c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

	$result = $conexion->prepare($sql);
	$result->execute(array($codsuc, $nroinscripcion));
	$item = $result->fetch();
	$propietario = strtoupper($item["propietario"]);
	$direccion = strtoupper($item["descripcioncorta"] . " " . $item["calle"] . " " . $item["nrocalle"]);
	$docidentidad = strtoupper($item["nrodocumento"]);
	$codciclo    = $item['codciclo'];
	$codantiguo  = $item["codantiguo"];


	//OBTENER DATOS DEL PERIODO DE FACTURACION
	$sql = "SELECT nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
			from facturacion.periodofacturacion
			where codemp=1 and codsuc=? and codciclo=? and facturacion=0";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$codciclo));
	$row = $consulta->fetch();
	$anio=$row['anio'];
	$mes=$row['mes'];
	$nrofacturacionactual=$row['nrofacturacion'];
    /*$mes++;
    if ($mes > 12) 
    {
        $mes = 1;
        $anio++;
    }*/
    $fechavencimiento = FechaVecimiento($mes, $anio);
    //$nrofacturacionactual++;
	//OBTENER DATOS DEL CONCEPTO
	$codconcepto	= 824;//DUPLICADO DE RECIBOS
	$consulta   = $conexion->prepare("SELECT * from facturacion.conceptos where codconcepto=? and codsuc=?");
	$consulta->execute(array($codconcepto,$codsuc));
	$row 		= $consulta->fetch();


	$imptotal		= $row["importe"];
	$igv			= 0;
	$redondeo		= 0;
	$subtotal		= $row["importe"];

	$nropresupuesto	= 0;
	$observacion	= 'RECIBO DUPLICADO EMITIDO EL '.date('d-m-Y');
	$codusu			= $_SESSION['id_user'];
	$femision		= date('Y-m-d'); 
	$count			= 1;
	$nroprepago			= 0;

	

	$nrocredito = $objFunciones->setCorrelativosVarios(8,$codsuc,"SELECT",0);
	$conexion->beginTransaction();
	$sqlC 	= "insert into facturacion.cabcreditos(codemp,codsuc,nrocredito,nroinscripcion,imptotal,igv,redondeo,subtotal,codconcepto,
					  nropresupuesto,observacion,creador,fechareg,nroprepago,codantiguo,
					  propietario,direccion) values(:codemp,:codsuc,:nrocredito,:nroinscripcion,:imptotal,:igv,:redondeo,
					  :subtotal,:codconcepto,:nropresupuesto,:observacion,:creador,:fechareg,:nroprepago,:codantiguo,:propietario,:direccion)";
	
	$resultC = $conexion->prepare($sqlC);
	$resultC->execute(array(":codemp"=>$codemp,
							":codsuc"=>$codsuc,
							":nrocredito"=>$nrocredito,
							":nroinscripcion"=>$nroinscripcion,
							":imptotal"=>str_replace(",","",$imptotal),
							":igv"=>str_replace(",","",$igv),
							":redondeo"=>str_replace(",","",$redondeo),
							":subtotal"=>str_replace(",","",$subtotal),
							":codconcepto"=>$codconcepto,
							":nropresupuesto"=>$nropresupuesto,
							":observacion"=>$observacion,
							":creador"=>$codusu,
							":fechareg"=>$femision,
							":nroprepago"=>$nroprepago,
							":codantiguo"=>$codantiguo,
							":propietario"=>$propietario,
							":direccion"=>$direccion));
	if ($resultC->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        $data['res']    =2;
        die(json_encode($data));
    }
	
	$sqlD = "insert into facturacion.detcreditos(codemp,codsuc,nrocredito,nrocuota,totalcuotas,fechavencimiento,subtotal,igv,
			redondeo,imptotal,nrofacturacion,tipocuota,anio,mes,interes,estadocuota) values(:codemp,:codsuc,:nrocredito,:nrocuota,:totalcuotas,:fechavencimiento,:subtotal,
			:igv,:redondeo,:imptotal,:nrofacturacion,:tipocuota,:anio,:mes,:interes,:estadocuota)";
	
	$resultD = $conexion->prepare($sqlD);
	$resultD->execute(array(":codemp"=>$codemp,
							":codsuc"=>$codsuc,
							":nrocredito"=>$nrocredito,
							":nrocuota"=>1,
							":totalcuotas"=>1,
							":fechavencimiento"=>$objFunciones->CodFecha($fechavencimiento),
							":subtotal"=>str_replace(",","",$subtotal),
							":igv"=>str_replace(",","",$igv),
							":redondeo"=>str_replace(",","",$redondeo),
							":interes"=>0,
							":imptotal"=>str_replace(",","",$imptotal),
							":nrofacturacion"=>$nrofacturacionactual,
							":tipocuota"=>0,
							":estadocuota"=>0,
							":anio"=>$anio,
							":mes"=>$mes));

	if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } 
      $sqlD = "INSERT INTO facturacion.detcabcreditos 
            (  codemp,  codsuc,  nrocredito,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal) 
            VALUES (  :codemp,  :codsuc,  :nrocredito,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";
            
            $resultD = $conexion->prepare($sqlD);
            $resultD->execute(array(":codemp"=>$codemp,
                                    ":codsuc"=>$codsuc,
                                    ":nrocredito"=>$nrocredito,
                                    ":codconcepto"=>$codconcepto,
                                    ":item"=>1,
                                    ":subtotal"=>str_replace(",","",$subtotal),
                                    ":igv"=>str_replace(",","",$igv),
                                    ":redondeo"=>str_replace(",","",$redondeo),
                                    ":imptotal"=>str_replace(",","",$imptotal),
                                    ));
            if ($resultD->errorCode() != '00000') 
            {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                $data['res']    =2;
                die(json_encode($data)) ;
            }

    else {
        $conexion->commit();
        $objFunciones->setCorrelativosVarios(8,$codsuc,"UPDATE",$nrocredito);
        $mensaje = "El Registro se ha Grabado Correctamente";
       echo $res = 1;
    }
	
	
function FechaVecimiento($mes, $anio) {
        $dia = UltimoDia($mes);

        if (strlen($mes) == 1) {
            $mes = "0" . $mes;
        }
        return $dia . "/" . $mes . "/" . $anio;
    }

    function UltimoDia($Mes) {
        $NroDias = 0;

        switch ($Mes) {
            case 1:$NroDias = 31;
                break;
            case 2:$NroDias = 28;
                break;
            case 3:$NroDias = 31;
                break;
            case 4:$NroDias = 30;
                break;
            case 5:$NroDias = 31;
                break;
            case 6:$NroDias = 30;
                break;
            case 7:$NroDias = 31;
                break;
            case 8:$NroDias = 31;
                break;
            case 9:$NroDias = 30;
                break;
            case 10:$NroDias = 31;
                break;
            case 11:$NroDias = 30;
                break;
            case 12:$NroDias = 31;
                break;
        }

        return $NroDias;
    }
?>
