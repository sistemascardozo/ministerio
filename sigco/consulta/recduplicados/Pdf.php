<?php 
if(!session_start()){session_start();	}
	include("../../../../objetos/clsReporte.php");

	// ini_set("display_errors", 1);
	// error_reporting(E_ALL);
	
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $NroInscripcion,$codsuc,$objreporte,$conexion,$Dim;
			
			$h=4;
			$this->SetFont('Arial','B',12);
			$this->SetY(8);
			$this->Cell(0, $h+1,"ESTADO DE CUENTA CORRIENTE",0,1,'C');
			$this->Ln(5);
			$Sql ="select clie.nroinscripcion,".$objreporte->getCodCatastral("clie.").", 
			clie.propietario,td.abreviado,clie.nrodocumento,
			tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
			es.descripcion,tar.nomtar,clie.codantiguo,ts.descripcion as tiposervicio,tipofacturacion,nromed,consumo
			from catastro.clientes as clie 
			inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
			inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
			inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
			inner join public.tiposervicio as ts on(ts.codtiposervicio=clie.codtiposervicio)
			inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
			WHERE clie.codemp=1 AND clie.codsuc=".$codsuc." AND clie.nroinscripcion=".$NroInscripcion."";
			$Consulta = $conexion->query($Sql);
			$row = $Consulta->fetch();
			define('Cubico', chr(179));
			switch ($row['tipofacturacion']) 
			{
				case 0: $TipoFacturacion ='CONSUMO LEIDO ( Lect. Ultima '.$row['consumo'].'m'.Cubico.')';break;
				case 1: $TipoFacturacion ='CONSUMO PROMEDIADO ('.$row['consumo'].'m'.Cubico.')';break;
				case 2: $TipoFacturacion ='CONSUMO ASIGNADO ('.$row['consumo'].'m'.Cubico.')';break;
			}
			$Medidor='No';
			if(trim($row['nromed'])!="")
				$Medidor='Si ( N° '.$row['nromed'].')';
			$this->SetLeftMargin(10);
			$this->Ln(5);
			$h=5;
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("N° de Inscripcion"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,$NroInscripcion,0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Código Catastral"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,$row[1],0,1,'L');
			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Código Antiguo"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,$row['codantiguo'],0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode($row['abreviado']),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,$row['nrodocumento'],0,1,'L');
			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Cliente"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,$row['propietario'],0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,'Tipo Servicio',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,$row['tiposervicio'],0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Dirección"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,strtoupper($row['direccion']),0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Estado"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,$row['descripcion'],0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,'Categoria',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,$row['nomtar'],0,1,'L');
			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Tipo Facturación"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,$TipoFacturacion,0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,'Medidor',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,utf8_decode($Medidor),0,1,'L');
			$h=4;
			$this->Ln(5);
			$this->SetX(10);
			$this->SetFont('Arial','B',8);
	 	 	$this->Cell($Dim[1],5,utf8_decode('N°'),1,0,'C',false);
	 	 	$this->Cell($Dim[2],5,utf8_decode('Fecha'),1,0,'C',false);
	 	 	$this->Cell($Dim[3],5,utf8_decode('Hora'),1,0,'C',false);
			$this->Cell($Dim[4],5,utf8_decode('Mes Facturación'),1,0,'C',false);
			$this->Cell($Dim[5],5,utf8_decode('Serie'),1,0,'C',false);
			$this->Cell($Dim[6],5,utf8_decode('Número'),1,0,'C',false);
			$this->Cell($Dim[7],5,utf8_decode('Operación'),1,0,'C',false);
			$this->Cell($Dim[8],5,utf8_decode('Cat'),1,0,'C',false);
			$this->Cell($Dim[9],5,utf8_decode('Co'),1,0,'C',false);
			$this->Cell($Dim[10],5,utf8_decode('Lectura'),1,0,'C',false);
			$this->Cell($Dim[11],5,utf8_decode('Consu'),1,0,'C',false);
			$this->Cell($Dim[12],5,utf8_decode('Cargo'),1,0,'C',false);
			$this->Cell($Dim[13],5,utf8_decode('Abono'),1,0,'C',false);
			$this->Cell($Dim[14],5,utf8_decode('Saldo'),1,1,'C',false);			
			
		}
		function Detalle()
		{
			global $NroInscripcion,$codsuc,$objreporte,$conexion,$Dim,$meses;

			$Condicion1 = " WHERE f.nroinscripcion =" . $NroInscripcion. "";
	        $Orden1     = " ORDER BY f.nroinscripcion ASC ";
	        $Condicion2 = " WHERE p.nroinscripcion =". $NroInscripcion . "  AND d.nrofacturacion <> 0 AND p.anulado = 0 ";
	        $Orden2     = " ORDER BY p.nroinscripcion ASC ";
	        $Condicion3 = " WHERE p.nroinscripcion =" . $NroInscripcion . "  AND d.nrofacturacion = 0 AND p.anulado = 0 ";
	        $Orden3     = " ORDER BY p.nroinscripcion ASC ";   
	        $Condicion4 = " WHERE c.nroinscripcion =" . $NroInscripcion. "";
	        $Orden4     = " ORDER BY c.nroinscripcion ASC ";  
	        $Condicion5 = " WHERE c.nroinscripcion =" . $NroInscripcion. "";
	        $Orden5     = " ORDER BY c.nroinscripcion ASC "; 
	        $Condicion6 = " WHERE rb.nroinscripcion =" . $NroInscripcion. "";
	        $Orden6     = " ORDER BY rb.nroinscripcion ASC ";   					

			$Sql = "(
	            SELECT 
	            f.fechareg, 
	            tar.nomtar as categoria, 
	            f.serie,
	            f.nrodocumento,
	            '00:00:00' as hora,
	            ".$this->Convert('f.anio','INTEGER')." as anio, 
	            ".$this->Convert('f.mes','INTEGER')." AS mes, 
	            sum(df.importe - (df.importerebajado)) as cargo,
	            '0.00' as abono,
	            '0' as movimiento, 
	            f.enreclamo as reclamo, 
	            f.lecturaultima as lectura, 
	            f.consumo as consumo,
	            df.nrofacturacion as facturacion,
	            df.categoria as cat,
	            df.codtipodeuda as codtipodeuda,
	            '0' as nropago 
	            FROM facturacion.cabfacturacion f 
	            INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) 
	            AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
	            INNER JOIN facturacion.tarifas as tar ON (f.codemp=tar.codemp and f.codsuc=tar.codsuc and f.catetar=tar.catetar)
	            ".$Condicion1." 
	            GROUP BY df.categoria, df.codtipodeuda, df.nrofacturacion, f.enreclamo, tar.nomtar,f.lecturaultima, f.consumo, f.fechareg,f.serie,f.nrodocumento,f.anio, f.mes, f.nrofacturacion 
	            )
	            UNION
	            (
	            SELECT 
	            p.fechareg, 
	            ' ' as categoria,
	            d.serie, 
	            d.nrodocumento,
	            p.hora as hora,
	            ".$this->Convert('d.anio','INTEGER')." as anio,
	            ".$this->Convert('d.mes','INTEGER')." AS mes, 
	            '0.00' as cargo,
	            sum(d.importe) as abono,
	            '1' as movimiento,
	            '0' as reclamo,
	            '0' as lectura,
	            '0' as consumo,
	            d.nrofacturacion as facturacion,
	            '0' as cat,
	            '0' as codtipodeuda,
	            d.nropago as nropago
	            FROM
	            cobranza.cabpagos p
	            INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp)
	            AND (p.codsuc = d.codsuc)
	            AND (p.nroinscripcion = d.nroinscripcion)
	            AND (p.nropago = d.nropago) ".$Condicion2." 
	            GROUP BY  d.nropago, d.nrofacturacion, p.hora, p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes
	            ORDER BY p.fechareg ASC
	            )
	            UNION
	            (
	            SELECT 
	            p.fechareg, 
	            co.descripcion as categoria,
	            d.serie, 
	            d.nrodocumento,
	            p.hora as hora,
	            ".$this->Convert('d.anio','INTEGER')." as anio,
	            ".$this->Convert('d.mes','INTEGER')." AS mes, 
	            '0.00' as cargo,
	            sum(d.importe) as abono,
	            '2' as movimiento,
	            '0' as reclamo,
	            '0' as lectura,
	            '0' as consumo,
	            '0' as facturacion,
	            '0' as cat,
	            '0' as codtipodeuda,
	            d.nropago as nropago
	            FROM
	            cobranza.cabpagos p
	            INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp)
	            AND (p.codsuc = d.codsuc)
	            AND (p.nroinscripcion = d.nroinscripcion)
	            AND (p.nropago = d.nropago) 
	            INNER JOIN facturacion.conceptos co ON (co.codemp = d.codemp AND co.codsuc = d.codsuc AND co.codconcepto = d.codconcepto)
	            ".$Condicion3." 
	            GROUP BY  d.nropago, p.hora, p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes, co.descripcion
	            ORDER BY p.fechareg ASC
	            )
	            UNION
	            (
	            SELECT 
	            c.fechareg, 
	            ' ' as categoria,
	            ' '  as serie, 
	            '0' as nrodocumento,
	            '00:00:00' as hora,
	            ".$this->Convert('extract(year from c.fechareg)','INTEGER')." as anio,
	            ".$this->Convert('extract(month from c.fechareg)','INTEGER')." AS mes, 
	            c.subtotal as cargo,
	            '0.00' as abono,
	            '3' as movimiento,
	            '0' as reclamo,
	            '0' as lectura,
	            '0' as consumo,
	            '0' as facturacion,
	            '0' as cat,
	            '0' as codtipodeuda,
	            '0' as nropago
	            FROM
	            facturacion.cabcreditos c
	            ".$Condicion4." 
	            GROUP BY c.fechareg,c.subtotal
	            ORDER BY c.fechareg ASC
	            )
	            UNION
	            (
	            SELECT 
	            c.fechaemision, 
	            ' ' as categoria,
	            ' ' as serie, 
	            '0' as nrodocumento,
	            '00:00:00' as hora,
	            ".$this->Convert('extract(year from c.fechaemision)','INTEGER')." as anio,
	            ".$this->Convert('extract(month from c.fechaemision)','INTEGER')." AS mes, 
	            '0.00' as cargo,
	            c.totalrefinanciado as abono,
	            '4' as movimiento,
	            '0' as reclamo,
	            '0' as lectura,
	            '0' as consumo,
	            '0' as facturacion,
	            '0' as cat,
	            '0' as codtipodeuda,
	            '0' as nropago
	            FROM
	            facturacion.cabrefinanciamiento c
	            ".$Condicion5." 
	            GROUP BY c.fechaemision,c.totalrefinanciado
	            ORDER BY c.fechaemision ASC
	            )
	            UNION
	            (
	            SELECT 
	            rb.fechareg, 
	            ' ' as categoria,
	            d.seriedocumento as serie, 
	            d.nrodocumentoabono as nrodocumento,
	            '00:00:00' as hora,
	            ".$this->Convert('d.anio','INTEGER')." as anio,
	            ".$this->Convert('d.mes','INTEGER')." AS mes, 
	            '0.00' as cargo,
	            SUM(d.imprebajado) as abono,
	            '5' as movimiento,
	            '0' as reclamo,
	            '0' as lectura,
	            '0' as consumo,
	            '0' as facturacion,
	            '0' as cat,
	            '0' as codtipodeuda,
	            '0' as nropago
	            FROM
	            facturacion.cabrebajas rb 
	            INNER JOIN facturacion.detrebajas d ON ( rb.nrorebaja =  d.nrorebaja AND rb.codemp = d.codemp AND rb.codsuc = d.codsuc)
	            ".$Condicion6." 
	            GROUP BY rb.fechareg, d.seriedocumento, d.nrodocumentoabono, d.anio, d.mes
	            ORDER BY rb.fechareg ASC
	            )
	            ORDER by fechareg,anio, mes ASC ";

		  	$Consulta =$conexion->query($Sql);
			$c=0;
			$Deuda=0;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);
			$this->SetLeftMargin(10);
			$this->SetFont('Arial','',8);
			foreach($Consulta->fetchAll() as $row)
			{	
				$c++;
				
				$Operacion='';
				switch ($row['movimiento']) {
                    case 0:
                        $Operacion = "FACTURACIÓN DE PENSIONES";
                        $Anio      = $row['anio'];
                        $Mes       = $row['mes'];
                        $AnioT     = $row['anio'];
                        $MesT      = $row['mes'];
                        $Deuda     +=  $row['cargo'];
                        $style  = "style=color:red;"; 
                        $color = $this->SetTextColor(212,9,9);
                        // $this->SetTextColor(255,0,0);

                        break;
                    case 1:
                        if($row['anio'] == $AnioT && $row['mes'] == $MesT)
                            $Operacion = "PAGO RECIBO DE PENSIONES";
                        else
                            $Operacion = "PAGO DEUDA ACUMULADA";

                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $Deuda -=  $row['abono'];
                        $color = $this->SetTextColor(6,115,20);

                    break;
                    case 2:
                        $Operacion        = $row['categoria'];
                        $row['categoria'] = '';
                        $Anio             = $row['anio'];
                        $Mes              = $row['mes'];
                        $color = $this->SetTextColor(255,99,71);


                    break;
                    case 3:
                        $Operacion = "FACTURACION DE CREDITOS";

                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $color = $this->SetTextColor(0,0,139);

                    break;
                    case 4:
                        $Operacion = "PAGO POR REFINANCIAMIENTO";
                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $color = $this->SetTextColor(32,178,170); 

                    break;
                    case 5:
                        $Operacion = "REBAJAS";
                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $color = $this->SetTextColor(226,132,9);

                    break;    
                    default: ;break;
                }
				$this->SetX(10);
				$color;
				$this->Cell($Dim[1],5,$c,1,0,'C',true);
		 	 	$this->Cell($Dim[2],5,$objreporte->DecFecha($row['fechareg']),1,0,'C',true);
		 	 	$this->Cell($Dim[3],5,substr($row['hora'],0,5),1,0,'C',true);
				$this->Cell($Dim[4],5,$meses[$row['mes']]." - ".$row['anio'],1,0,'C',true);
				$this->Cell($Dim[5],5,$row['serie'],1,0,'C',true);
				$this->Cell($Dim[6],5,$row['nrodocumento'],1,0,'C',true);
				$this->Cell($Dim[7],5,utf8_decode($Operacion),1,0,'C',true);
				$this->Cell($Dim[8],5,substr($row['categoria'],0,3),1,0,'L',true);
				$this->Cell($Dim[9],5," ",1,0,'R',true);
				$this->Cell($Dim[10],5,substr($row['lectura'],0,-3),1,0,'R',true);
				$this->Cell($Dim[11],5,substr($row['consumo'],0,-3),1,0,'R',true);
				$this->Cell($Dim[12],5,$row['cargo'] = ($row['cargo']==0) ? ' ' : number_format($row['cargo'], 2),1,0,'R',true);
				$this->Cell($Dim[13],5,$row['abono'] = ($row['abono']==0) ? ' ' : number_format($row['abono'], 2),1,0,'R',true);
				$this->Cell($Dim[14],5,number_format($Deuda,2),1,1,'R',true);

			}
			$this->Ln(2);
			$this->SetFont('Arial','B',10);
			$this->SetTextColor(0,0,0);
			$this->Cell(array_sum($Dim)-$Dim[9],5,'Deuda Total:',0,0,'R',true);
			$this->Cell($Dim[9],5,number_format($Deuda,2),0,1,'R',true);
			
		}
    
    }
	define('Cubico', chr(179));
	
	$codsuc	= $_SESSION["IdSucursal"];
	$NroInscripcion = $_GET["NroInscripcion"];
	$Dim = array('1'=>10,'2'=>15,'3'=>15,'4'=>30,'5'=>15,'6'=>15,'7'=>50,'8'=>15,'9'=>15,'10'=>15,'11'=>15,'12'=>20,'13'=>20,'14'=>20);
    $x = 20;
    $h=4;
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("H");
	$objreporte->Detalle("H");	
	$objreporte->Output();	
	
?>