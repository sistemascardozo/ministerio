<?php 
	include("../../../../../config.php");
	
	$codsuc			= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$codpagos		= $_POST["codcolateral"];
	
	$sql = "select d.codconcepto as idconcepto, 
			c.ordenrecibo, 
			c.descripcion as descripcion, 
			sum(d.importe) as imptotal
			from cobranza.detpagos as d
			INNER JOIN facturacion.conceptos as c ON (d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto)
			where d.codsuc=? AND d.nroinscripcion=? AND d.nropago=?
			group by d.codconcepto, c.descripcion, c.ordenrecibo order by c.ordenrecibo";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroinscripcion,$codpagos));
	$items = $consulta->fetchAll();

	$timptotal = 0;
	$timporteacta = 0;
	$timporterebajado = 0;
	$tsaldo = 0;
?>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="ui-widget-header" style="font-size:12px">
      <tr align="center">
        <th width="85%" >Concepto</th>
        <th width="15%" >Importe</th>
        <th width="15%" >Saldo</th>
     </tr>
 </thead>
 <tbody>
     <?php foreach($items as $row): ?>
		<?php $timptotal += $row["imptotal"];?>
		<?php if ($timptotal>0): ?>
		<tr>
        	<td align="left" ><?=strtoupper($row["descripcion"])?></td>
    		<td align="right" ><?=number_format($row["imptotal"],2)?></td>
    		<td align="right" ><?=number_format($row["imptotal"],2)?></td>
 		</tr>
		<?php endif; ?>
		<?php endforeach; ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td colspan="2" align="right" >Totales ==></td>
        <td align="right" ><?=number_format($timptotal,2)?></td>
     </tr>
 </tfoot>
 
  
</table>