<?php
    if(!session_start()){session_start();}
	
	$Sede = $_SESSION['Sede'];
	
    include("../../../objetos/clsDrop.php");
    $objMantenimiento = new clsDrop();
    $Presicion = $_SESSION["Presicion"];
    $codsuc = $_SESSION["IdSucursal"];
    $NroInscripcion = $_POST["NroInscripcion"];
    $Sql = "select clie.nroinscripcion,".$objMantenimiento->getCodCatastral("clie.").", 
            clie.propietario,td.abreviado,clie.nrodocumento,
            tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
            es.descripcion,tar.nomtar,clie.codantiguo,ts.descripcion as tiposervicio,tipofacturacion,nromed,consumo
            from catastro.clientes as clie 
            inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
            inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
            inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
            inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
            inner join public.tiposervicio as ts on(ts.codtiposervicio=clie.codtiposervicio)
            inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
            WHERE clie.codemp=1 AND clie.codsuc=".$codsuc." AND clie.nroinscripcion=".$NroInscripcion."";
    $Consulta = $conexion->query($Sql);
    $row = $Consulta->fetch();


    switch ($row['tipofacturacion'])
    {
    case 0: $TipoFacturacion = 'CONSUMO LEIDO ( Lect. Ultima '.$row['consumo'].'m&#179;)';
    break;
    case 1: $TipoFacturacion = 'CONSUMO PROMEDIADO ('.$row['consumo'].'m&#179;)';
    break;
    case 2: $TipoFacturacion = 'CONSUMO ASIGNADO ('.$row['consumo'].'m&#179;)';
    break;
    }
    $Medidor = 'No';
    if(trim($row['nromed'])!="")
    $Medidor = 'Si ( N° '.$row['nromed'].')';
?>
<script type="text/javascript">
    function abrir_detalle_facturacion(nrofacturacion,categoria,codtipodeuda){
        cargar_detalle_facturacion(nrofacturacion,categoria,codtipodeuda);
        $( "#dialog-form-detalle-facturacion" ).dialog( "open" );
    }

    function cargar_detalle_facturacion(nrofacturacion,categoria,codtipodeuda)
    {
        $.ajax({
            url:urldir+'sigco/cobranza/operaciones/cancelacion/include/from_detalle_facturacion.php',
            type:'POST',
            async:true,
            data:'codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val()+
              '&nrofacturacion='+nrofacturacion+'&categoria='+categoria+'&codtipodeuda='+codtipodeuda,
            success:function(datos){
                $("#detalle-facturacion").html(datos)
            }
        })  
    }

    function abrir_detalle_pagos(codpagos){
        cargar_detalle_pagos(codpagos);
        $( "#dialog-form-detalle-pagos" ).dialog( "open" );
    }

    function cargar_detalle_pagos(codpagos)
    {
        $.ajax({
            url:urldir+'sigco/catastro/listados/cuentacorriente/include/from_detalle_pagos.php',
            type:'POST',
            async:true,
            data:'codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val()+
              '&codpagos='+codpagos,
            success:function(datos){
                $("#detalle-pagos").html(datos)
            }
        })  
    }

    function abrir_detalle_colaterales(codcolateral)
    {
        cargar_detalle_colaterales(codcolateral);
        $( "#dialog-form-detalle-colaterales" ).dialog( "open" );
    }

    function cargar_detalle_colaterales(codcolateral)
    {

        $.ajax({
            url:urldir+'sigco/catastro/listados/cuentacorriente/include/from_detalle_colaterales.php',
            type:'POST',
            async:true,
            data:'codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val()+
              '&codcolateral='+codcolateral,
            success:function(datos){
                $("#detalle-colaterales").html(datos)
            }
        })  
    }

    function abrir_detalle_creditos(nrocredito){
        cargar_detalle_creditos(nrocredito);
        $( "#dialog-form-detalle-creditos" ).dialog( "open" );
    }

    function cargar_detalle_creditos(nrocredito){

        $.ajax({
            url:urldir+'sigco/catastro/listados/cuentacorriente/include/from_detalle_creditos.php',
            type:'POST',
            async:true,
            data:'codsuc='+codsuc+'&nrocredito='+nrocredito,
            success:function(datos){
                $("#detalle-creditos").html(datos)
            }
        })  
    }

    $(function(){
        $( "#dialog-form-detalle-facturacion" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $( "#dialog-form-detalle-facturacion" ).dialog( "close" );
                }
            },
            close: function() {

            }
        });

        $( "#dialog-form-detalle-pagos" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $( "#dialog-form-detalle-pagos" ).dialog( "close" );
                }
            },
            close: function() {

            }
        });

        $( "#dialog-form-detalle-colaterales" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $( "#dialog-form-detalle-colaterales" ).dialog( "close" );
                }
            },
            close: function() {

            }
        });

        $( "#dialog-form-detalle-creditos" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $( "#dialog-form-detalle-creditos" ).dialog( "close" );
                }
            },
            close: function() {

            }
        });

    });
</script>
<table class="ui-widget" border="0" cellspacing="0" width="100%" rules="rows" align="center">
    <thead style="font-size:12px;"> 
        <tr>
            <td><span class="Negrita">N&deg; de Inscripcion:</span><?= $NroInscripcion ?></td>
            <td><span class="Negrita">C&oacute;digo Catastral:</span> <?= $row[1] ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">C&oacute;digo Antiguo:</span> <?= $row['codantiguo'] ?></td>
            <td><span class="Negrita"><?= $row['abreviado'] ?>:</span> <?= $row['nrodocumento'] ?></td>
        </tr>
        <tr>
            <td align="left"><span class="Negrita">Cliente:</span> <?= $row['propietario'] ?></td>
            <td><span class="Negrita">Tipo Servicio:</span> <?= $row['tiposervicio'] ?></td>

        </tr>
        <tr>
            <td colspan="2" align="left"><span class="Negrita">Direcci&oacute;n :</span><?= strtoupper($row['direccion']) ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">Estado:</span> <?= $row['descripcion'] ?></td>
            <td><span class="Negrita">Categoria:</span> <?= $row['nomtar'] ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">Tipo Facturaci&oacute;n:</span> <?= $TipoFacturacion ?></td>
            <td><span class="Negrita">Medidor:</span><?= $Medidor ?></td>
        </tr>
    </thead>
</table>
<table class="ui-widget" bORDER="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
        <tr>
            <th width="100" scope="col">Item</th>
            <th width="50" scope="col">Fecha</th>
            <th width="50" scope="col">Hora</th>
            <th width="200" scope="col">Mes Facturaci&oacute;n</th>
            <th width="50" scope="col">Serie</th>
            <th width="50" scope="col">N&uacute;mero</th>
            <th width="300" scope="col">Operaci&oacute;n</th>
            <th width="100" scope="col">Cat</th>
            <th width="100" scope="col" data="estado_lectura">Co</th>
            <th width="100" scope="col">Lectura</th>
            <th width="100" scope="col">Consu</th>
            <th width="100" scope="col">Cargo</th>
            <th width="100" scope="col">Abono</th>
            <th width="100" scope="col">Saldo</th>
            <th width="100" scope="col">Estado Recibo</th>
            <th scope="col" width="10" align="center">Acciones</th>

        </tr>
    </thead>
    <tbody style="font-size:12px">

        <?php

        $Condicion1 = " WHERE f.nroinscripcion =" . $NroInscripcion. "";
        $Orden1     = " ORDER BY f.nroinscripcion ASC ";
        $Condicion2 = " WHERE p.nroinscripcion =". $NroInscripcion . "  AND d.nrofacturacion <> 0 AND p.anulado = 0 ";
        $Orden2     = " ORDER BY p.nroinscripcion ASC ";
        $Condicion3 = " WHERE p.nroinscripcion =" . $NroInscripcion . "  AND d.nrofacturacion = 0 AND p.anulado = 0 ";
        $Orden3     = " ORDER BY p.nroinscripcion ASC ";   
        $Condicion4 = " WHERE c.nroinscripcion =" . $NroInscripcion. "";
        $Orden4     = " ORDER BY c.nroinscripcion ASC ";  
        $Condicion5 = " WHERE c.nroinscripcion =" . $NroInscripcion. "";
        $Orden5     = " ORDER BY c.nroinscripcion ASC "; 
        $Condicion6 = " WHERE rb.nroinscripcion =" . $NroInscripcion. "";
        $Orden6     = " ORDER BY rb.nroinscripcion ASC ";                   

        $Sql = "(
            SELECT 
            f.fechareg, 
            tar.nomtar as categoria, 
            f.serie,
            f.nrodocumento,
            '00:00:00' as hora,
            ".$objMantenimiento->Convert('f.anio','INTEGER')." as anio, 
            ".$objMantenimiento->Convert('f.mes','INTEGER')." AS mes, 
            sum(df.importe - (df.importerebajado)) as cargo,
            '0.00' as abono,
            '0' as movimiento, 
            f.enreclamo as reclamo, 
            f.lecturaultima as lectura, 
            f.consumofact as consumo,
            df.nrofacturacion as facturacion,
            df.categoria as cat,
            df.codtipodeuda as codtipodeuda,
            '0' as nropago,
            f.estadofacturacion as estado,
            0 as nrocredito
            FROM facturacion.cabfacturacion f 
            INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) 
            AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
            INNER JOIN facturacion.tarifas as tar ON (f.codemp=tar.codemp and f.codsuc=tar.codsuc and f.catetar=tar.catetar)
            ".$Condicion1." 
            GROUP BY f.estadofacturacion, df.categoria, df.codtipodeuda, df.nrofacturacion, f.enreclamo, tar.nomtar,f.lecturaultima, f.consumofact, f.fechareg,f.serie,f.nrodocumento,f.anio, f.mes, f.nrofacturacion 
            )
            UNION
            (
            SELECT 
            p.fechareg, 
            ' ' as categoria,
            d.serie, 
            d.nrodocumento,
            p.hora as hora,
            ".$objMantenimiento->Convert('d.anio','INTEGER')." as anio,
            ".$objMantenimiento->Convert('d.mes','INTEGER')." AS mes, 
            '0.00' as cargo,
            sum(d.importe) as abono,
            '1' as movimiento,
            '0' as reclamo,
            '0' as lectura,
            '0' as consumo,
            d.nrofacturacion as facturacion,
            '0' as cat,
            '0' as codtipodeuda,
            d.nropago as nropago,
            '0' as estado,
            0 as nrocredito
            FROM
            cobranza.cabpagos p
            INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp)
            AND (p.codsuc = d.codsuc)
            AND (p.nroinscripcion = d.nroinscripcion)
            AND (p.nropago = d.nropago) ".$Condicion2." 
            GROUP BY  d.nropago, d.nrofacturacion, p.hora, p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes
            ORDER BY p.fechareg ASC
            )
            UNION
            (
            SELECT 
            p.fechareg, 
            co.descripcion as categoria,
            d.serie, 
            d.nrodocumento,
            p.hora as hora,
            ".$objMantenimiento->Convert('d.anio','INTEGER')." as anio,
            ".$objMantenimiento->Convert('d.mes','INTEGER')." AS mes, 
            '0.00' as cargo,
            sum(d.importe) as abono,
            '2' as movimiento,
            '0' as reclamo,
            '0' as lectura,
            '0' as consumo,
            '0' as facturacion,
            '0' as cat,
            '0' as codtipodeuda,
            d.nropago as nropago,
            '0' as estado,
            0 as nrocredito
            FROM
            cobranza.cabpagos p
            INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp)
            AND (p.codsuc = d.codsuc)
            AND (p.nroinscripcion = d.nroinscripcion)
            AND (p.nropago = d.nropago) 
            INNER JOIN facturacion.conceptos co ON (co.codemp = d.codemp AND co.codsuc = d.codsuc AND co.codconcepto = d.codconcepto)
            ".$Condicion3." 
            GROUP BY  d.nropago, p.hora, p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes, co.descripcion
            ORDER BY p.fechareg ASC
            )
            UNION
            (
            SELECT 
            c.fechareg, 
            ' ' as categoria,
            ' '  as serie, 
            '0' as nrodocumento,
            '00:00:00' as hora,
            ".$objMantenimiento->Convert('extract(year from c.fechareg)','INTEGER')." as anio,
            ".$objMantenimiento->Convert('extract(month from c.fechareg)','INTEGER')." AS mes, 
            c.subtotal as cargo,
            '0.00' as abono,
            '3' as movimiento,
            '0' as reclamo,
            '0' as lectura,
            '0' as consumo,
            '0' as facturacion,
            '0' as cat,
            '0' as codtipodeuda,
            '0' as nropago,
            c.estareg as estado,
            c.nrocredito as nrocredito
            FROM
            facturacion.cabcreditos c
            ".$Condicion4." 
            GROUP BY c.fechareg,c.subtotal,c.estareg,c.nrocredito
            ORDER BY c.fechareg ASC
            )
            UNION
            (
            SELECT 
            c.fechaemision, 
            ' ' as categoria,
            ' ' as serie, 
            '0' as nrodocumento,
            '00:00:00' as hora,
            ".$objMantenimiento->Convert('extract(year from c.fechaemision)','INTEGER')." as anio,
            ".$objMantenimiento->Convert('extract(month from c.fechaemision)','INTEGER')." AS mes, 
            '0.00' as cargo,
            c.totalrefinanciado as abono,
            '4' as movimiento,
            '0' as reclamo,
            '0' as lectura,
            '0' as consumo,
            '0' as facturacion,
            '0' as cat,
            '0' as codtipodeuda,
            '0' as nropago,
            '0' as estado,
            0 as nrocredito
            FROM
            facturacion.cabrefinanciamiento c
            ".$Condicion5." 
            GROUP BY c.fechaemision,c.totalrefinanciado
            ORDER BY c.fechaemision ASC
            )
            UNION
            (
            SELECT 
            rb.fechareg, 
            ' ' as categoria,
            d.seriedocumento as serie, 
            d.nrodocumentoabono as nrodocumento,
            '00:00:00' as hora,
            ".$objMantenimiento->Convert('d.anio','INTEGER')." as anio,
            ".$objMantenimiento->Convert('d.mes','INTEGER')." AS mes, 
            '0.00' as cargo,
            SUM(d.imprebajado) as abono,
            '5' as movimiento,
            '0' as reclamo,
            '0' as lectura,
            '0' as consumo,
            '0' as facturacion,
            '0' as cat,
            '0' as codtipodeuda,
            '0' as nropago,
            '0' as estado,
            0 as nrocredito
            FROM
            facturacion.cabrebajas rb 
            INNER JOIN facturacion.detrebajas d ON ( rb.nrorebaja =  d.nrorebaja AND rb.codemp = d.codemp AND rb.codsuc = d.codsuc)
            ".$Condicion6." 
            GROUP BY rb.fechareg, d.seriedocumento, d.nrodocumentoabono, d.anio, d.mes
            ORDER BY rb.fechareg ASC
            )
            ORDER by fechareg,anio, mes ASC "; 

        // var_dump($Sql);exit;

        $Consulta = $conexion->query($Sql);
        $datos = $Consulta->fetchAll();
        $c=0;
        $Deuda=0;
        // var_dump($datos);exit;
        ?>

        <?php if (count($datos) > 0): ?>

        <?php foreach($datos as $row): ?>
        <?php
                $c++;
                $Operacion='';
                switch ($row['movimiento']) {
                    case 0:
                        $Operacion = "FACTURACIÓN DE PENSIONES";
                        $Anio      = $row['anio'];
                        $Mes       = $row['mes'];
                        $AnioT     = $row['anio'];
                        $MesT      = $row['mes'];
                        $Deuda     +=  $row['cargo'];
                        $style  = "style=color:red;"; 
                        $onClickDetalle = "abrir_detalle_facturacion(".$row["facturacion"].",".$row["cat"].",".$row["codtipodeuda"].")";
                        $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Facturacion' ></span>";

                        switch($row['estado']){
                            case 1:$row['estado'] = 'PEN';break;
                            case 2:$row['estado'] = 'PAG';break;
                            case 3:$row['estado'] = 'AMO';break;
                            case 4:$row['estado'] = 'ANU';break;
                            default:$row['estado'] = 'PEN';break;
                        };

                        break;
                    case 1:
                        if($row['anio'] == $AnioT && $row['mes'] == $MesT)
                            $Operacion = "PAGO RECIBO DE PENSIONES";
                        else
                            $Operacion = "PAGO DEUDA ACUMULADA";

                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $Deuda -=  $row['abono'];
                        $style = "style=color:green;"; 
                        $onClickDetalle = "abrir_detalle_pagos(".$row["nropago"].")";
                        $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Pagos' ></span>";
                        $row['estado'] = '';
                    break;
                    case 2:
                        $Operacion        = $row['categoria'];
                        $row['categoria'] = '';
                        $Anio             = $row['anio'];
                        $Mes              = $row['mes'];
                        $style            = "style=color:#FF6347;"; 
                        $onclick = '';
                        $onClickDetalle = "abrir_detalle_colaterales(".$row["nropago"].")";
                        $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Colaterales' ></span>";
                        $row['estado'] = '';
                        // $Deuda -=  $row['abono'];
                    break;
                    case 3:
                        $Operacion = "FACTURACION DE CREDITOS";

                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $style = "style=color:#00008B;"; 
                        $onClickDetalle = "abrir_detalle_creditos(".$row["nrocredito"].")";
                        $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Creditos'></span>";
                        switch($row['estado']){
                            case 1:$row['estado'] = 'ING';break;
                            case 0:$row['estado'] = 'ANU';break;
                            default:$row['estado'] = 'ING';break;
                        };
                        // $Deuda -=  $row['abono'];
                    break;
                    case 4:
                        $Operacion = "PAGO POR REFINANCIAMIENTO";
                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $style = "style=color:#20B2AA;"; 
                        $onclick = '';
                        $row['estado'] = '';
                        // $Deuda -=  $row['abono'];
                    break;
                    case 5:
                        $Operacion = "REBAJAS";
                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $style = "style=color:#E28409;"; 
                        $onclick = '';
                        $row['estado'] = '';
                        // $Deuda -=  $row['abono'];
                    break;    
                    default: ;break;
                }
        ?>

            <tr <?= $title ?> <?= $class ?> onclick="SeleccionaId(this);" id="<?= $c ?>" <?=$style?> >
                <td align="center"><?= $c ?></td>
                <td align="center" valign="middle"><?= $objMantenimiento->DecFecha($row['fechareg']) ?></td>
                <td align="center" valign="middle"><?= substr($row['hora'],0,5)?></td>
                <td align="left" valign="middle"><?= substr($meses[$row['mes']],0,3)." - ".$row['anio'] ?></td>
                <td align="center" valign="middle"><?= $row['serie'] ?></td>
                <td align="center" valign="middle"><?= $row['nrodocumento'] = ($row['nrodocumento']==0) ? '&nbsp;' : $row['nrodocumento'];?></td>
                <td align="left" valign="middle"><?= $Operacion ?></td>
                <td align="left" valign="middle"><?= substr($row['categoria'],0,3)?></td>
                <td align="left" valign="middle">&nbsp;</td>
                <td align="left" valign="middle"><?= substr($row['lectura'],0,-3) ?></td>
                <td align="left" valign="middle"><?= substr($row['consumo'],0,-3) ?></td>
                <td align="right" valign="middle"><?= $row['cargo'] = ($row['cargo']==0) ? '&nbsp;' : number_format($row['cargo'], 2);?></td>
                <td align="right" valign="middle"><?= $row['abono'] = ($row['abono']==0) ? '&nbsp;' : number_format($row['abono'], 2);?></td>
                <td align="right" valign="middle"><?= number_format($Deuda, 2) ?></td>
                
                <td><?= $row['estado'] ;?></td>
                <td>
                    <?= $onclick;?>
                </td>
            </tr>
        <?php endforeach;?>
        <?php else: ?>
            <tr>
                <td colspan="16" align="center" style="color:red;">NO EXISTE NINGUN MOVIMIENTO PARA ESTE CLIENTE</td>
            </tr>
        <?php endif; ?>
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td colspan="13" align="right" >Deuda Total:</td>
            <td align="right" ><?= number_format($Deuda, 2) ?></td>
            <td scope="col" colspan="2" align="center">&nbsp;</td>
        </tr>
    </tfoot>
</table>
<div id="dialog-form-detalle-facturacion" title="Ver Detalle de Facturacion"  >
    <div id="detalle-facturacion">
    </div>
</div>
<div id="dialog-form-detalle-pagos" title="Ver Detalle de Pagos"  >
    <div id="detalle-pagos">
    </div>
</div>

<div id="dialog-form-detalle-colaterales" title="Ver Detalle de Colaterales"  >
    <div id="detalle-colaterales">
    </div>
</div>
<div id="dialog-form-detalle-creditos" title="Ver Detalle de Creditos"  >
    <div id="detalle-creditos">
    </div>
</div>