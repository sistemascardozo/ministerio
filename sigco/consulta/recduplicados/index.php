<?php
	include("../../../include/main.php");
	include("../../../include/claseindex.php");
	
	$TituloVentana = "IMPRESION DE DUPLICADOS";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc = $_SESSION['IdSucursal'];
	
	$objMantenimiento = new clsDrop();
	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);
	$Fecha = date('d/m/Y');
	
	$nrofacturacion = $_SESSION["nrofacturacion"] - 1;
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>

    jQuery(function ($)
    {
        $("#DivTipos").buttonset();

    });
    var urldir = "<?php echo $_SESSION['urldir'];?>"
    var codsuc = <?=$codsuc ?>

    function buscar_usuarios()
    {
        object = "usuario"
        AbrirPopupBusqueda('../../catastro/operaciones/actualizacion/?Op=5', 1150, 440)
    }
    function Recibir(id)
    {
        if (object == "usuario")
        {
            $("#nroinscripcion").val(id)
            cargar_datos_usuario(id)
        }

    }
    function cargar_datos_usuario(nroinscripcion)
    {
        $.ajax({
            url: '../../../ajax/clientes.php',
            type: 'POST',
            async: true,
            data: 'codsuc=1&nroinscripcion=' + nroinscripcion,
            dataType: "json",
            success: function (datos) {

                $("#codantiguo").val(datos.codantiguo)
                $("#cliente").val(datos.propietario)
                //Consultar()
            }
        })
    }
    function MostrarDatosUsuario(datos)
    {

        $("#nroinscripcion").val(datos.nroinscripcion)
        $("#cliente").val(datos.propietario)
        Consultar()

    }
    function Cancelar()
    {
        location.href = '<?php echo $_SESSION['urldir'];?>admin/index.php'
    }
    
    function Validar()
    {
        var NroInscripcion = $("#nroinscripcion").val()
        var Data = ''
        if (NroInscripcion != "")
            Data += 'NroInscripcion=' + NroInscripcion
        else
        {
            Msj('#nroinscripcion', 'Seleccione Usuario', 1000, 'above', '', false)
            return false;
        }
        return Data
    }
    
    function Consultar()
    {
        var Data = Validar()
        if (Data != false)
        {
            $('#DivConsulta').fadeOut(500)
            $('#ImgLoad').fadeIn();

            $("#DivResultado").empty();
            $.ajax({
                url: 'Consulta.php',
                type: 'POST',
                async: true,
                data: Data,
                success: function (datos)
                {
                    $('#ImgLoad').fadeOut(500, function () {
                        $("#DivResultado").empty().append(datos);
                        $('#DivConsulta').fadeIn(500, function () {
                            var theTable = $('#TbIndex')
                            $("#Filtro").keyup(function () {
                                $.uiTableFilter(theTable, this.value)
                            })
                        })
                    })
                }
            })
        }
    }
    
    function imprimirduplicado()
    { 
        var Id = $("#nroinscripcion").val()       
        
        if (Id != "")
            var NroFact = $("#nrofacturacion").val()
        else
        {
            Msj('#codantiguo', 'Seleccione Usuario', 1000, 'above', '', false)
            return false;
        }
            if(confirm("Va a Proceder a Realizar un Duplicado de Recibo. Desea Cargar al proximo recibo de facturacion el importe?"))
            {
                $.ajax({
                    url: 'Guardar.php',
                    type: 'POST',
                    async: true,
                    data: 'nroinscripcion='+Id+'&codsuc=<?=$codsuc ?>',
                    success: function(data) {
                      OperMensaje(data)
                        $("#Mensajes").html(data);    
                        Buscar(0)
                    }
                }) 
            }
			
			//if (<?=$nrofacturacion?> == NroFact)
			//{
            	AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/cobranza/operaciones/cancelacion/impresion/imp_duplicado.php?nroinscripcion="+Id+"&codsuc=<?=$codsuc?>&codciclo=1&nrofacturacion="+NroFact,800,600);
			//}
//			else
//			{
				//AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/cobranza/operaciones/cancelacion/consultas/imprimir_recibo.php?nroinscripcion="+Id+"&codsuc=<?=$codsuc?>&codciclo=1&nrofacturacion="+NroFact,800,600);
//			}
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="1000" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="2">
                        <fieldset style="padding:4px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="11%">Sucursal</td>
                                    <td width="2%" align="center">:</td>
                                    <td width="23%">
                                        <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                                    </td>
                                    <td width="13%" align="right">&nbsp;</td>
                                    <td width="3%" align="center">&nbsp;</td>
                                    <td width="48%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="11%">Mes Facturado</td>
                                    <td width="2%" align="center">:</td>
                                    <td width="23%">
                                        <select name="nrofacturacion" id="nrofacturacion" style="width:160px">
                                            <?php
                                            $Sql = "SELECT DISTINCT cf.nrofacturacion,
                                                cf.mes||' - '||cf.anio AS fecha
                                                FROM facturacion.cabfacturacion as cf
                                                WHERE cf.codsuc=".$codsuc."  
                                                ORDER BY cf.nrofacturacion DESC";
                                            $Consulta = $conexion->query($Sql);
                                            foreach ($Consulta->fetchAll() as $rows) {
                                                $Sel = "";
                                                if ($items["nrofacturacion"] == $rows["nrofacturacion"])
                                                    $Sel = 'selected="selected"';
                                                ?>
                                                <option value="<?php echo $rows["nrofacturacion"]; ?>" <?=$Sel ?>  ><?php echo strtoupper(utf8_decode($rows["fecha"])); ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td width="13%" align="right">&nbsp;</td>
                                    <td width="3%" align="center">&nbsp;</td>
                                    <td width="48%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Usuario</td>
                                    <td align="center">:</td>
                                    <td>
                                        <input type="hidden" name="nroinscripcion" id="nroinscripcion" value="<?php echo $row["nroinscripcion"] ?>" />
                                        <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" size="15" maxlength="15" value="<?php echo $row["codantiguo"] ?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)"/>
                                        <span class="MljSoft-icon-buscar" title="Buscar Usuario" onclick="buscar_usuarios();"></span>

                                    </td>
                                    <td colspan='2'>
                                        <input type="text" name="cliente" id="cliente" class="inputtext" size="45" maxlength="45" value="" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6" height="40" align="center">

                                        <table width="500" border="0" cellspacing="0" cellpadding="0">
                                            <tr>                                                                                             
                                                <td align="center">
                                                    <input id="BtnPdf" type="button" value="Imprimir Recibo" onclick="imprimirduplicado();" class="BtnIndex" style="width: 140px !important" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <td colspan="6" >
                        <div id="ImgLoad" style="text-align:center;display:none">
                            <span class="icono-icon-loading"></span>
                            Cargando ...
                        </div>
                        <div id="DivConsulta" style="height:auto; overflow:auto;display:none ">
                            <fieldset>
                                <legend class="ui-state-default ui-corner-all" >Resultados de la Consulta</legend>
                                <div style="height:auto; overflow:auto;" align="center" id="DivResultado">
                                    <table class="ui-widget" border="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
                                        <thead class="ui-widget-header" style="font-size:10px">
                                            <tr>
                                                <th colspan="8">Desde el <?=($Fecha) ?> Hasta el <?=($Fecha) ?>  </th>
                                            </tr>
                                            <tr>
                                                <th width="50" scope="col">N&deg;</th>
                                                <th width="50" scope="col">N&deg; Prestamo</th>
                                                <th width="100" scope="col">Fecha</th>
                                                <th width="500" scope="col">Beneficiario</th>
                                                <th width="100" scope="col">Fecha Cosecha</th>
                                                <th width="100" scope="col">Monto</th>
                                                <th width="100" scope="col">Estado</th>
                                                <th width="100" scope="col">&nbsp;</th>

                                            </tr>
                                        </thead>
                                        <tbody style="font-size:12px">
                                        </tbody>
                                        <tfoot class="ui-widget-header" style="font-size:10px">
                                            <tr>
                                                <td colspan="8" align="right" >&nbsp;</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </fieldset>

                        </div>
                    </td>
                </tr>
                
            </tbody>

        </table>
    </form>
</div>
<script>
//$('#ImgLoad').fadeOut(500,function(){$('#DivConsulta').fadeIn(500)});
    $("#BtnAceptar").attr('value', 'Consultar')
    $("#BtnAceptar").css('background-image', 'url(<?=$urldir ?>css/images/ver.png)')
</script>
<?php CuerpoInferior(); ?>