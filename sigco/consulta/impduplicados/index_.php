<?php
  include("../../../include/main.php");
  include("../../../include/claseindex.php");

  $TituloVentana = "IMPRESION DE RECIBOS DUPLICADOS";
  $Criterio='Usuarios';
  $Activo=1;
  $objclsfunciones = new clsFunciones();


  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  if($Op!=5) CuerpoSuperior($TituloVentana);
  $codsuc                = $_SESSION['IdSucursal'];
  $facturacion = $objclsfunciones->datosfacturacion($codsuc, 1);
  $nrofacturacion_actual = ($facturacion["nrofacturacion"] - 1);

  //$codsuc =2;
  $FormatoGrilla = array ();

$Sql = "SELECT c.nroinscripcion,".$objFunciones->getCodCatastral("c.").", c.codcliente,
            c.propietario,tc.descripcioncorta || ' ' ||cl.descripcion || '#' || c.nrocalle,
            s.descripcion as sector,e.descripcion, 1, ca.estadofacturacion,
            CASE
              WHEN SUM(det.importerebajado) > 0 AND ca.estadofacturacion = 1 THEN 1
              ELSE 0
            END AS importe,
            CASE
              WHEN SUM(det.importerebajado) > 0 THEN 1
              ELSE 0
            END AS rebaja,
            dpg.nropago,
            cpg.fechareg AS fechapago
          FROM catastro.clientes AS c
            JOIN public.calles AS cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            JOIN public.tiposcalle AS tc on(cl.codtipocalle=tc.codtipocalle)
            JOIN public.sectores AS s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codzona=s.codzona and c.codsector=s.codsector)
            JOIN public.estadoservicio AS e on(c.codestadoservicio=e.codestadoservicio)
            JOIN facturacion.cabfacturacion AS ca ON(ca.codemp=c.codemp AND ca.codsuc=c.codsuc AND ca.nroinscripcion=c.nroinscripcion)
            JOIN facturacion.detfacturacion AS det ON(ca.codemp=det.codemp AND ca.codsuc=det.codsuc AND ca.nrofacturacion=det.nrofacturacion AND ca.nroinscripcion=det.nroinscripcion AND ca.codciclo=det.codciclo)
            LEFT JOIN cobranza.detpagos AS dpg ON(dpg.codemp=det.codemp AND dpg.codsuc=det.codsuc AND dpg.nrofacturacion=det.nrofacturacion AND dpg.nroinscripcion=det.nroinscripcion)
            LEFT JOIN cobranza.cabpagos AS cpg ON(dpg.codemp = cpg.codemp AND dpg.codsuc = cpg.codsuc AND dpg.nroinscripcion = cpg.nroinscripcion AND dpg.nropago = cpg.nropago) ";

$SqlPag = "   SELECT  COUNT(*)
              FROM catastro.clientes as c
                JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
                JOIN public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
                JOIN public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codzona=s.codzona and c.codsector=s.codsector)
                JOIN public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
                JOIN facturacion.cabfacturacion AS ca ON(ca.codemp=c.codemp AND ca.codsuc=c.codsuc AND ca.nroinscripcion=c.nroinscripcion)
                JOIN facturacion.detfacturacion AS det ON(ca.codemp=det.codemp AND ca.codsuc=det.codsuc AND ca.nrofacturacion=det.nrofacturacion AND ca.nroinscripcion=det.nroinscripcion AND ca.codciclo=det.codciclo) ";

	$SqlPag = eregi_replace("[\n\r\n\r]", ' ', $SqlPag);

  //$Sql= "SELECT * FROM catastro.view_index_catastroclientes AS w  ";

  $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  //$FormatoGrilla[1] = array('1'=>'c.nroinscripcion', '2'=>'c.codcliente','3'=>'c.propietario', '4'=>'c.codantiguo','5'=>'cl.descripcion','6'=>'tc.descripcioncorta','7'=>'c.nrocalle');
  $FormatoGrilla[1] = array('1'=>'c.nroinscripcion', '2'=>'c.codcliente','3'=>'c.propietario','4'=>'c.nrodocumento','5'=>'c.codantiguo','6'=>"tc.descripcioncorta || ' ' ||cl.descripcion || '#' || c.nrocalle");          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Nro. Inscripcion','T2'=>'Cod Catastral','T3'=>'Cod. Cliente','T4'=>'Usuario','T5'=>'Direcci&oacute;n','T6'=>'Sector','T7'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'80', 'W2'=>'140', 'W3'=>'95');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " AND ca.nrofacturacion = '" . $nrofacturacion_actual . "' AND  (c.codemp=1 AND c.codsuc=".$codsuc." ) ";
  $FormatoGrilla[8] .= " GROUP BY dpg.nropago, cpg.fechareg, c.nroinscripcion, to_char(c.codsuc, '00') || '-' || TRIM(to_char(c.codsector, '00')) || '-' || TRIM(to_char(c.codurbanizacion, '00')) || '-' || TRIM(to_char(CAST(c.codmanzanas AS INTEGER), '000')) || '-' || TRIM(to_char(CAST(c.lote AS INTEGER), '0000')) || '-' || TRIM(c.sublote), c.codcliente, c.propietario,tc.descripcioncorta || ' ' ||cl.descripcion || '#' || c.nrocalle, s.descripcion,e.descripcion, ca.estadofacturacion ";
  $FormatoGrilla[8] .= " ORDER BY c.nroinscripcion DESC ";
  //$FormatoGrilla[8] = "  AND  (w.codsuc=".$codsuc." ) ORDER BY w.nroinscripcion desc ";                                //Orden de la Consulta
  if($Op!=5)
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'imprimir.png',   //Imagen a mostrar
              'Btn1'=>'Duplicado Original',       //Titulo del Botón
              'BtnF1'=>'onclick="ImprimirDuplicadoOriginal(this.id, this, 1);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnModificar',   //Nombre del Boton
              'BtnI2'=>'imprimir.png',   //Imagen a mostrar
              'Btn2'=>'Duplicado Rebajado',       //Titulo del Botón
              'BtnF2'=>'onclick="ImprimirDuplicadoOriginal(this.id, this, 2);"',  //Eventos del Botón
              'BtnCI2'=>'11',  //Item a Comparar
              'BtnCV2'=>'1',    //Valor de comparación
              'BtnId3'=>'BtnModificar',   //Nombre del Boton
              'BtnI3'=>'imprimir.png',   //Imagen a mostrar
              'Btn3'=>'Duplicado Cancelado',       //Titulo del Botón
              'BtnF3'=>'onclick="ImprimirDuplicadoOriginal(this.id, this, 3);"',  //Eventos del Botón
              'BtnCI3'=>'9',  //Item a Comparar
              'BtnCV3'=>'2'    //Valor de comparación
              );
  else
    {
      $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'Ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
      $FormatoGrilla[5] = array('W1'=>'60','W6'=>'80','W11'=>'20','W2'=>'70','W3'=>'70','W10'=>'90');
   }
  $FormatoGrilla[10] = array(array('Name' =>'id','Col'=>1), array('Name' =>'nropago','Col'=>12), array('Name' =>'fechapago','Col'=>13));//DATOS ADICIONALES
  $FormatoGrilla[11]=7;//FILAS VISIBLES
  // $FormatoGrilla[100] = $SqlPag;

  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],1000,600);
  Pie();


?>
<script type="text/javascript">
function ValidarEnter(e, Op)
        {
        switch(e.keyCode)
            {
                case $.ui.keyCode.UP:
                    for (var i = 10; i >= 1; i--)
                        {
                            if ($('tr[tabindex=\'' + i  + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' +i  + '\']')
                                if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                                {
                                  $('tr[tabindex=\'' + i  + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }

                             }
                        };
                        $('#Valor').focus().select();
                break;
                case $.ui.keyCode.DOWN:

                       for (var i = 1; i <= 10; i++)
                        {
                            if ($('tr[tabindex=\'' + i  + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' +i  + '\']')
                                if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                                {
                                  $('tr[tabindex=\'' + i  + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }

                             }
                        };

                        $('#Valor').focus().select();
                break;
                default:  if (VeriEnter(e)) Buscar(Op);


            }


            //ValidarEnterG(evt, Op)
        }
$("#BtnNuevoB").remove();
function Enviar(obj)
{
    var Id = $(obj).parent().parent().data('id')
    opener.Recibir(Id);
    window.close();
}

function ImprimirDuplicadoOriginal(id, obj, tipo)
{
	Id2 = id.substr(id.indexOf('-') + 1);
  var nropago = $(obj).parent().parent().data('nropago')
  var fechapago = $(obj).parent().parent().data('fechapago')

  switch (tipo) {
    case 3:
      AbrirPopupImpresion("<?=$urldir?>sigco/cobranza/operaciones/cancelacion/consulta/imprimir_recibo.php?nroinscripcion=" + Id2 + "&codsuc=<?= $codsuc?>&codciclo=1&"+"&tipo="+tipo+"&codpago="+nropago+"&fechapago="+fechapago,800,600)
      break;
    default:
      AbrirPopupImpresion("<?=$urldir?>sigco/cobranza/operaciones/cancelacion/impresion/imprimir_duplicado_original.php?nroinscripcion=" + Id2 + "&codsuc=<?= $codsuc?>&codciclo=1&"+"&tipo="+tipo,800,600)
    break;
  }
}

</script>
<?php if($Op!=5) CuerpoInferior(); ?>
