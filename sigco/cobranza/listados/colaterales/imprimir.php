<?php
	set_time_limit(0);

	include("../../../../objetos/clsReporte.php");

	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $fhasta, $fdesde,$ciclotext,$Dim;

			$this->SetFont('Arial','B',14);
			$this->SetY(15);
			$tit1 = "REPORTE DE VENTAS DE COLATERALES";
			$this->Cell(277,$h+2,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);

			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(277,$h,$ciclotext,0,1,'C');

			$this->SetX(113.5);
			$this->Cell(10, $h,"Desde",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$fdesde,0,0,'L');
			$this->Cell(10, $h,"Hasta",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$fhasta,0,1,'L');

			$this->Ln(2);
			$this->Ln(1);
			$this->SetFont('Arial','',6);

			//$this->Cell($Dim[0],$h,"N°",'1',0,'C');
			$this->Cell($Dim[1],$h,"FECHA",'1',0,'C');
			$this->Cell($Dim[2],$h,"NRO. DOCUMENTO",'1',0,'C');

			$this->Cell($Dim[3],$h,"INSCRIPCION",'1',0,'C');
			$this->Cell($Dim[4],$h,"RUC",'1',0,'C');
			$this->Cell($Dim[5]+$Dim[6],$h,"NOMBRE",'1',0,'C');
			//$this->Cell($Dim[6],$h,"",'1',0,'C');
			$this->Cell($Dim[7],$h,"VALOR DE VENTA",'1',0,'C');
			$this->Cell($Dim[8],$h,"I.G.V.",'1',0,'C');
			$this->Cell($Dim[9],$h,"REDONDEO",'1',0,'C');
			$this->Cell($Dim[10],$h,"IMPORTE",'1',0,'C');
			$this->Cell($Dim[11],$h,"ESTADO",'1',1,'C');

		}

		function agregar_detalle($codsuc, $codciclo, $fdesde, $fhasta, $Csec)
		{
			global $conexion, $Dim;
			if(intval($mes) < 10){$mes = "0".$mes;}

			$h = 4;
			$primerdia = $fdesde;
			$ultimodia = $fhasta;
			$count = 0;

			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;

			$this->SetTextColor(0, 0, 0);

			$Sql = "SELECT c.fechareg, d.serie, d.nrodocumento, c.nroinscripcion, c.propietario, ";
			$Sql .= " cl.codantiguo AS codantiguo, ";
			$Sql .= " c.documento, c.estado, d.coddocumento, c.origen, ";
			$Sql .= " (CASE WHEN SUM(CASE WHEN d.codconcepto = 5 THEN d.importe ELSE 0 END) <> 0 ";
			$Sql .= " THEN SUM(CASE WHEN d.codconcepto = 5 THEN d.importe ELSE 0 END) ELSE c.igv END) AS igv, ";
			$Sql .= " SUM(CASE WHEN d.codconcepto IN (7, 8) THEN d.importe ELSE 0 END) AS redondeo, ";
			$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (5, 7, 8) THEN d.importe ELSE 0 END) AS impmes, ";
			$Sql .= " SUM(d.importe) as total, ca.anulado ";
			$Sql .= "FROM cobranza.cabprepagos c ";
			$Sql .= " JOIN cobranza.detprepagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nroprepago = d.nroprepago) AND (c.nroinscripcion = d.nroinscripcion) ";
			$Sql .= " JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
			$Sql .= " LEFT JOIN catastro.clientes cl ON(c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
			$Sql .= " LEFT JOIN cobranza.cabpagos ca ON(c.codemp = ca.codemp AND c.codsuc = ca.codsuc AND c.nroinscripcion = ca.nroinscripcion AND c.nroprepago = ca.nroprepago) ";
			$Sql .= "WHERE c.codsuc = ".$codsuc." ";
			//$Sql .= " AND c.estado IN (2) ";
			$Sql .= " /*AND c.estado IN (1, 2)*/ ";
			$Sql .= " AND c.fechareg BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
			$Sql .= " AND d.coddocumento IN (13, 14) ";
			$Sql .= " AND co.codconcepto <> 10005 ";
			$Sql .= "GROUP BY c.fechareg, d.serie, d.nrodocumento, c.nroinscripcion, c.propietario, ";
			$Sql .= " c.codsuc, c.documento, c.estado, d.coddocumento, c.origen, c.igv, cl.codantiguo, ca.anulado ";
			$Sql .= "ORDER BY c.fechareg, d.nrodocumento";

			// echo $Sql; exit;
			//
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			$borde=0;
			$tvVenta=0;
			$tnImponible=0;
			$tigv=0;
			$tredondeo=0;
			//var_dump($consulta->errorInfo());
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);
			$cAnuldados=0;
			$tPagados=0;
			$cPagados=0;
			$cBol=0;
			$cFac=0;
			$cfinan=0;

			$iBol=0;
			$iFac=0;
			$ifinan=0;
			$iAnuldados=0;
			$count=0;
			foreach($items as $row)
			{
				if(trim($row["serie"])=='203')
					$row["documento"] = trim(str_replace(array('RUC','DNI','-'), '', $row["documento"]));
				else $row["documento"]='';
				$count++;
				$x=false;

				if($row['estado'] == 0)
				{
					$row['noimp']    = 0;
					$row['impigv']   = 0;
					$row['redondeo'] = 0;
					$row['impmes']   = 0;

					$row["propietario"] = "(A) ".$row["propietario"];
					$cAnuldados++;
					$iAnuldados+=$row['total'];
					$row['total']=0;
					$x=true;

					if($row['coddocumento']==13)
						{
							$row["serie"]='B '.$row["serie"];
						}
						if($row['coddocumento']==14)
						{
							$row["serie"]='F '.$row["serie"];
						}
				}

				if($row['coddocumento']==13 && $row['origen']==0 && $row['estado']!=0)
				{
					$cBol++;
					$iBol+=$row['total'];
					$row["serie"]='B '.$row["serie"];
					$x=true;
				}
				if($row['coddocumento']==14 && $row['origen']==0 && $row['estado']!=0)
				{
					$cFac++;
					$iFac+=$row['total'];
					$row["serie"]='F '.$row["serie"];
				}
				// Suma para todos los creditos y refinanciamientos
				if(($row['origen']==1 || $row['origen']==2) && $row['estado'] != 0)
				{
					$cfinan++;
					$row["propietario"] = "(C) ".$row["propietario"];
					$ifinan+=$row['total'];

					if($row['coddocumento']==13)
						{
							$row["serie"]='B '.$row["serie"];
						}
						if($row['coddocumento']==14)
						{
							$row["serie"]='F '.$row["serie"];
						}
					$x=true;
				}
				//	if(!$x) die($row["nrodocumento"]);
				//$this->Cell($Dim[0],$h,$count,$borde,0,'C',true);
				$this->Cell($Dim[1],$h,$this->DecFecha($row["fechareg"]),$borde,0,'C',true);
				$this->Cell($Dim[2],$h,$row["serie"].' '.$row["nrodocumento"],$borde,0,'C',true);

				if(empty($row["codantiguo"])):
					$row["codantiguo"] = str_pad($row["nroinscripcion"],8,'0600000',STR_PAD_LEFT);
				endif;


				$this->Cell($Dim[3],$h,strtoupper($row["codantiguo"]),$borde,0,'C',true);

				$this->Cell($Dim[4],$h,strtoupper($row["documento"]),$borde,0,'C',true);
				$this->Cell($Dim[5]+$Dim[6],$h,strtoupper(utf8_decode($row["propietario"])),$borde,0,'L',true);
				//$this->Cell($Dim[6],$h,strtoupper($row["detalle"]),$borde,0,'L',true);

				if($row['estado'] == 0):
					$row['impmes']   = 0.00;
					$row['igv']      = 0.00;
					$row['redondeo'] = 0.00;
					$row['total']    = 0.00;
				endif;

				if($row['estado'] == 3):
					$this->Cell($Dim[7],$h,number_format(($row['impmes']-$row['igv']),2),$borde,0,'R',true);
				else:
					$this->Cell($Dim[7],$h,number_format($row['impmes'],2),$borde,0,'R',true);
				endif;


				$this->Cell($Dim[8],$h,number_format($row['igv'],2),$borde,0,'R',true);
				$this->Cell($Dim[9],$h,number_format($row['redondeo'],2),$borde,0,'R',true);
				$this->Cell($Dim[10],$h,number_format($row['total'],2),$borde,0,'R',true);

				/*
				if($row['anulado'] == 1):
					$this->Cell($Dim[11],$h,"X",$borde,0,'R',true);
					$iAnuldados += $row['total'];
					$cAnuldados++;
				else:
					$this->Cell($Dim[11],$h,"",$borde,0,'R',true);
				endif;
				*/
				switch($row['estado']) :
					case 0: $est = 'ANU';break;
					case 1: $est = 'EMI';break;
					case 2: $est = 'CAN';break;
					case 3: $est = 'CRE';break;
					default:$est = 'EMI';break;
				endswitch;

				$this->Cell($Dim[11],$h,$est,$borde,0,'C',true);


				//$tvVenta+=0;
				if($row['estado'] == 3): $tvVenta	+= ($row["impmes"] - $row['igv']);
					else: $tvVenta	+= $row["impmes"];
				endif;


				$tigv		+= $row["igv"];
				$tredondeo	+= $row["redondeo"];

				if($row['estado'] == 3):
					$row['impmes'] = ($row['impmes']-$row['igv']);
				endif;

				$imptotal 	+= $row['impmes'] + $row['igv'] + $row['redondeo'];
				$tPagados	+= $row['total'];

				$this->Ln($h);

			}

			$this->SetFont('Arial','B',8);


			$this->Cell($Dim[1]+$Dim[2]+$Dim[3]+$Dim[4]+$Dim[5]+$Dim[6], $h,"Total General ==>",0,0,'R');
			$this->Cell($Dim[7], $h,number_format($tvVenta,2),'T',0,'R');
			$this->Cell($Dim[8], $h,number_format($tigv,2),'T',0,'R');
			$this->Cell($Dim[9], $h,number_format($tredondeo,2),'T',0,'R');
			$this->Cell($Dim[10], $h,number_format($imptotal,2),'T',1,'R');

			$this->Cell(0, $h,"LEYENDA:",0,1,'L');
			$this->Cell(40, $h,"(ANU) Anulado por Vencimiento",0,1,'L');
			$this->Cell(40, $h,"(EMI) Emitido",0,1,'L');
			$this->Cell(40, $h,"(CAN) Cancelado",0,1,'L');
			$this->Cell(40, $h,"(CRE) En Convenio",0,1,'L');


			$this->AddPage("L");
			$this->Ln(3);

			$this->SetFont('Arial','B',10);
			$this->Cell(0 ,$h,"RESUMEN DE VENTAS DE COLATERALES",0,1,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',8);
			$this->SetX(130);
			$this->Cell(30,$h,"IMP. VALOR DE VENTA",'B',0,'C');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,"CANTIDAD",'B',1,'C');
			//TOTALE
			$this->Ln(3);
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"TOTAL DE BOLETAS",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($iBol,2),0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$cBol,0,1,'R');
			$this->Ln(2);
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"TOTAL DE FACTURAS",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($iFac,2),0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$cFac,0,1,'R');
			$this->Ln(2);

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"TOTAL DE DOCUMENTO FINANCIADOS",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($ifinan,2),0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$cfinan,0,1,'R');
			$this->Ln(2);

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"TOTAL DE DOCUMENTO ANULADOS",0,0,'L');
			$this->SetFont('Arial','',8);
			if(!empty($iAnuldados)):
				$iAnuldados_descripcion = " (-) " . number_format($iAnuldados,2);
				$iAnuldados = number_format($iAnuldados,2);
			else:
				$iAnuldados_descripcion = "";
				$iAnuldados = "0.00";
			endif;


			$this->Cell(30,$h,$iAnuldados_descripcion,0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$cAnuldados,0,1,'R');
			$this->Ln(2);
			

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"TOTAL GENERAL",0,0,'L');
			$this->SetFont('Arial','',8);

			$iAnuldados = str_replace(",", "", $iAnuldados);

			$this->Cell(30,$h,number_format(($tPagados-$iAnuldados),2),'T',0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$count,'T',1,'R');

		}

		function FechaDoc($cod,$serie,$numero)
		{
			global $conexion,$codsuc;
			$Sql ="SELECT fechareg FROM facturacion.cabfacturacion
					WHERE codsuc=".$codsuc." AND coddocumento=".$cod."
					AND serie='".$serie."' AND nrodocumento='".$numero."'";
			$c = $conexion->query($Sql)->fetch();
			return $this->DecFecha($c[0]);
		}
		function EquivalenteDoc($cod,$tipo)
		{
			if($tipo==1)
			{
				switch ($cod)
				{
					case 4: return "14";break;
					case 13: return "03";break;
					case 14: return "01";break;
					case 17: return "07";break;
				}
			}
			else
			{
				switch ($cod)
				{
					case 1: return "1";break;
					case 3: return "6";break;
					default: return "0";break;
				}
			}
			/*
			COMPROBANTES
			-----------------------------
			14  Recibos de Pensiones
			01  Facturas
			03  Boletas de Ventas
			07  Notas de Abono/Credito
			08  Notas de cargo/Debito


			DOCUMENTOS
			-----------------------------
			1  DNI
			6  RUC
			0  Otros*/
		}

    }

	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc		= $_GET["codsuc"];
	// $mes		= $_GET["mes"];
	$fhasta       = $_GET["fhasta"];
	$fdesde       = $_GET["fdesde"];
	$ciclotext	= $_GET["ciclotext"];
	if($_GET["sector"]!="%"){$Csec=" AND cl.codsector=".$_GET["sector"];}else{$Csec="";}
	// $m = $meses[$mes];
	$Dim = array('0'=>5,'1'=>20,'2'=>30,'3'=>20,'4'=>20,'5'=>50,'6'=>50,'7'=>20,'8'=>20,'9'=>20,'10'=>20,'11'=>12);
	$objReporte	=	new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage("L");

	$objReporte->agregar_detalle($codsuc,$ciclo,$fdesde,$fhasta,$Csec);


	$objReporte->Output();

?>
