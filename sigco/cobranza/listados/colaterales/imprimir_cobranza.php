<?php
	set_time_limit(0);


	include("../../../../objetos/clsReporte.php");

	$objfunciones	=	new clsFunciones();


	// ini_set("display_errors",1);
	// error_reporting(E_ALL);

	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $fdesde,$fhasta,$ciclotext,$Dim;

			$this->SetFont('Arial','B',14);
			$this->SetY(15);
			$tit1 = "REPORTE DE COBRANZA POR CONCEPTOS";
			$this->Cell(190,$h + 5,utf8_decode($tit1),0,1,'C');
      $this->Ln(1);

			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(190,$h,$ciclotext,0,1,'C');

			$this->SetX(70.5);
			$this->Cell(10, $h,"Desde",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$fdesde,0,0,'L');
			$this->Cell(10, $h,"Hasta",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$fhasta,0,1,'L');
			$this->Ln(3);

			$this->SetFillColor(255, 255, 255);
			$this->RoundedRect(10, 30, 195, 8, 0.5, 'DF');


			$this->SetY($this->GetY()-2);
			$this->SetFont('Arial','',6);
			$h = 8;
			$h1 = 4;
			$this->Cell($Dim[0],$h,"CONCEPTO",1,0,'C');
			$this->Cell($Dim[1],$h,"DENOMINACION",1,0,'C');
			$this->Cell($Dim[4],$h1,"IMPORTE",1,0,'C');
			$this->Cell($Dim[7],$h,"TOTAL",1,1,'C');

			$this->SetXY($this->GetX()+145,$this->GetY()-4);
			$this->Cell($Dim[5],$h1,"DEL MES",1,0,'C');
			$this->Cell($Dim[6],$h1,"MES DEL ANTERIOR",1,1,'C');

			//$this->Cell($Dim[2],$h,"IMPORTE ANTERIOR MES",1,1,'R');

			$this->Ln(2);

		}

		function agregar_detalle($codsuc,$codciclo,$fdesde,$fhasta,$Csec,$fdesde_anterior,$fhasta_anterior)
		{
				global $conexion,$Dim;
				// if(intval($mes)<10) $mes="0".$mes;

				$h=4;
				$primerdia = $fdesde;
				$ultimodia = $fhasta;
				$count = 0;

				$impmes 	= 0;
				$impigv		= 0;
				$imptotal	= 0;

				$array_general = array();

				// REBAJAS PARA EL MES
				$Sql = " SELECT DISTINCT
										d.codconcepto as codconcepto,
										co.descripcion as concepto,
				    				SUM(d.importe) as total
									FROM cobranza.cabpagos c
										JOIN cobranza.detpagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion)
										JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
									WHERE c.codsuc = {$codsuc} AND c.anulado = 0 AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
									GROUP BY d.codconcepto, co.descripcion
									ORDER BY d.codconcepto ";

				$consulta    = $conexion->query($Sql);
				$items       = $consulta->fetchAll();

				foreach($items as $item):

					if(!array_key_exists($item['codconcepto'],$array_general)):
            $array_general[$item['codconcepto']] = array();
            $array_general[$item['codconcepto']]['nombre'] = strtoupper(utf8_decode($item['concepto']));
            $array_general[$item['codconcepto']]['datos'] = array();
          endif;

					$array_general[$item['codconcepto']]['datos']['mes'] = $item['total'];

				endforeach;

				// REBAJAS PARA EL MES ANTERIOR
				$Sql = " SELECT DISTINCT
										d.codconcepto as codconcepto,
										co.descripcion as concepto,
				    				SUM(d.importe) as total
									FROM cobranza.cabpagos c
										JOIN cobranza.detpagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion)
										JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
									WHERE c.codsuc = {$codsuc} AND c.anulado = 0 AND c.fechareg BETWEEN '{$fdesde_anterior}' AND '{$fhasta_anterior}'
									GROUP BY d.codconcepto, co.descripcion
									ORDER BY d.codconcepto ";

				$consulta    = $conexion->query($Sql);
				$items       = $consulta->fetchAll();

				foreach($items as $item):

					if(!array_key_exists($item['codconcepto'],$array_general)):
            $array_general[$item['codconcepto']] = array();
            $array_general[$item['codconcepto']]['nombre'] = $item['descripcion'];
            $array_general[$item['codconcepto']]['datos'] = array();
          endif;

					$array_general[$item['codconcepto']]['datos']['antmes'] = $item['total'];

				endforeach;


				$subtotal_mes = $subtotal_mes_ant = $subtotal_total = 0;

				$this->SetFont('Arial','',9);
				foreach($array_general as $indice => $row) :

					$this->Cell($Dim[0],$h,$indice,1,0,'C');
					$this->Cell($Dim[1],$h,utf8_decode(strtoupper($row['nombre'])),1,0,'L');

					$importe_mes          = (isset($row['datos']['mes'])) ? $row['datos']['mes']:0.00;
					$importe_mes_anterior = (isset($row['datos']['antmes'])) ? $row['datos']['antmes']:0.00;
					$importe_total        = $importe_mes + $importe_mes_anterior;

					$this->Cell($Dim[5],$h,number_format($importe_mes,2),1,0,'R');
					$this->Cell($Dim[6],$h,number_format($importe_mes_anterior,2),1,0,'R');
					$this->Cell($Dim[7],$h,number_format($importe_total,2),1,1,'R');

					$subtotal_mes     += $importe_mes;
					$subtotal_mes_ant += $importe_mes_anterior;
					$subtotal_total   += $importe_total;

				endforeach;

				$this->Ln(3);
				$this->Line(10,$this->GetY(),240,$this->GetY(),3);
				$this->Ln(1);
				$this->SetFont('Arial','B',9);
				$this->Cell($Dim[0]+$Dim[1], $h,"Total General ==>",1,0,'R');
				$this->Cell($Dim[5], $h,number_format($subtotal_mes,2),1,0,'R');
				$this->Cell($Dim[6], $h,number_format($subtotal_mes_ant,2),1,0,'R');
				$this->Cell($Dim[7], $h,number_format($subtotal_total,2),1,1,'R');

			}

		//  Para los cuadros con bordes redondeados o sin ellos
		function RoundedRect($x, $y, $w, $h, $r, $style = '') {
				$k = $this->k;
				$hp = $this->h;
				if($style=='F')
					$op='f';
				elseif($style=='FD' || $style=='DF')
					$op='B';
				else
					$op='S';
				$MyArc = 4/3 * (sqrt(2) - 1);
				$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
				$xc = $x+$w-$r ;
				$yc = $y+$r;
				$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

				$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
				$xc = $x+$w-$r ;
				$yc = $y+$h-$r;
				$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
				$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
				$xc = $x+$r ;
				$yc = $y+$h-$r;
				$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
				$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
				$xc = $x+$r ;
				$yc = $y+$r;
				$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
				$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
				$this->_out($op);
			}

		function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
			 $h = $this->h;
			 $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
			 $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
			}

  }

	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc = $_GET["codsuc"];
	$fhasta = $_GET["fhasta"];
	$fdesde = $_GET["fdesde"];

	$fhasta_anterior = $objfunciones->obtener_cambio_x_mes($objfunciones->CodFecha($fhasta), '-' , 1);
	$fdesde_anterior = $objfunciones->obtener_cambio_x_mes($objfunciones->CodFecha($fdesde), '-' , 1);


	$ciclotext = $_GET["ciclotext"];
	if($_GET["sector"]!="%"){$Csec=" AND cl.codsector=".$_GET["sector"];}else{$Csec="";}
	$m = $meses[$mes];
	$Dim = array('0'=>20,'1'=>125,'2'=>40,'3'=>20,'4'=>50,'5'=>25,'6'=>25,'7'=>35);
	$objReporte	=	new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage('H');
	$objReporte->agregar_detalle($codsuc,$ciclo,$fdesde,$fhasta,$Csec,$fdesde_anterior,$fhasta_anterior);
	$objReporte->Output();

?>
