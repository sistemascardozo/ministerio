<?php
	  include("../../../../objetos/clsReporte.php");
	
	class clsCuadre extends clsReporte
	{
		function cabecera()
		{
			global $conexion,$cars,$fecha,$codsuc,$fdesde,$fhasta,$Ccar;

			$Sql="SELECT DISTINCT(nropec) 
			FROM public.depositosbanco 
			WHERE codemp=1 AND codsuc=".$codsuc." 
			".$Ccar." AND 
			(fechaini between '".$fdesde."' AND '".$fhasta."' 
			OR fechaafin between '".$fdesde."' AND '".$fhasta."') ";
			$Consulta=$conexion->query($Sql);
			$NroPec='';
			foreach($Consulta->fetchAll() as $row)
			{
				$NroPec .=$row[0].", ";
			}
			$h=4;
			
			$this->Ln(10);
			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN DE PLANILLA ENTRADA CAJA";
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(40, $h,$fecha,0,1,'L');
			
			$this->Cell(10, $h,"Nro. Pec",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->MultiCell(0, $h,$NroPec,0,'L');

			$this->Ln(1);
			$this->Cell(75, $h,"CONCEPTO",1,0,'C');
			$this->Cell(20, $h,"NRO. ITEMS",1,0,'C');
			$this->Cell(20, $h,"CODCTA.",1,0,'C');
			$this->Cell(25, $h,"CANCELADO",1,0,'C');
			$this->Cell(25, $h,"A CUENTA",1,0,'C');
			$this->Cell(25, $h,"TOTAL",1,1,'C');
						
		}
		function ContenidoCab($concepto,$cancelado,$acuenta)
		{
			$h=5;
			
			$this->SetFont('Arial','B',10);
			$this->SetTextColor(230,0,0);
			
			$this->Cell(75, $h,strtoupper($concepto),0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoDet($concepto,$cancelado,$acuenta,$ctacontable)
		{
			$h=5;
			
			$this->SetFont('Arial','',10);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(70, $h,$concepto,0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,$ctacontable,0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoFoot($cancelado,$acuenta)
		{
			$h=5;
			
			$this->SetFont('Arial','B',11);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(70, $h,"",0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"TOTALES",0,0,'R');
			$this->Cell(25, $h,number_format($cancelado,2),'T',0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),'T',0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),'T',1,'R');
		}
		function tUsuarios()
		{
			global $conexion,$codsuc,$Ccar,$fdesde,$fhasta,$cajero;
			$Sql ="select COUNT(DISTINCT(nroinscripcion))
			from cobranza.cabpagos c
			where c.codsuc=".$codsuc." and c.nropec<>0 and ".$Ccar."
			c.fechareg between '".$fdesde."' and '".$fhasta."' AND c.anulado=0";
			$row = $conexion->query($Sql)->fetch();
			//die($Sql.'-->'.$conexiones);
			$this->SetFont('Arial','',11);
			$this->Ln(5);
			$this->Cell(40,4,'TOTAL CONEXIONES',0,0,'C',false);
			$this->Cell(5, 4,':',0,0,'C');
			$this->Cell(15, 4,$row[0],0,1,'R');
		}
    }
	
	$fhasta		= $_GET["fhasta"];
	$fdesde		= $_GET["fdesde"];
	$car		= $_GET["car"];
	$codsuc		= $_GET["codsuc"];
	$cars		= $_GET["cars"];

	//if($car!="%"){$cars = $_GET["cars"];}else{$cars = "TODOS";}
	if($car!="%"){$cars = $_GET["cars"];$Ccar=" AND car=".$car;}else{$cars = "TODOS"; $Ccar="";}
	$fecha = $fdesde ." HASTA ".$fhasta;
	
	$objReporte	= new clsCuadre();
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
	$impcancelado	= 0;
	$impamortizado 	= 0;
	
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	
	$sql = "select codcategoria,categoria_descripcion,SUM(impcancelado) AS impcancelado ,SUM(impamortizado) AS impamortizado
			from cobranza.f_cuadre_cab_cierre
			WHERE codsuc=:codsuc AND  fechareg between :fechainicio AND :fechafinal AND
			nropec<>0 AND anulado=0 ".$Ccar."
			GROUP BY codcategoria,categoria_descripcion  ORDER BY codcategoria";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":codsuc"=>$codsuc,
							 ":fechainicio"=>$fdesde,
							 ":fechafinal"=>$fhasta));

	
	$items = $consulta->fetchAll();
	//var_dump($consulta->errorInfo());
	foreach($items as $row)
	{
		$objReporte->ContenidoCab($row["categoria_descripcion"],$row["impcancelado"],$row["impamortizado"]);

		$sqlD = "select concepto,ctadebe,ordenrecibo,SUM(impcancelado) AS impcancelado ,SUM(impamortizado) AS impamortizado
				from cobranza.f_cuadre_det_cierre
				WHERE codsuc=:codsuc AND fechareg between :fechainicio AND :fechafinal
				AND nropec<>0 AND anulado=0 ".$Ccar." AND codcategoria=:codcategoria
				GROUP BY concepto,ctadebe,ordenrecibo
				ORDER BY ordenrecibo";
		
		$impcancelado 	+= $row["impcancelado"];
		$impamortizado 	+= $row["impamortizado"];
		
		$consultaD = $conexion->prepare($sqlD);
		$consultaD->execute(array(":codsuc"=>$codsuc,
									  ":fechainicio"=>$fdesde,
									  ":fechafinal"=>$fhasta,
									  ":codcategoria"=>$row["codcategoria"]));
		$itemsD = $consultaD->fetchAll();
		
		foreach($itemsD as $rowD)
		{
			$objReporte->ContenidoDet(strtoupper($rowD["concepto"]),$rowD["impcancelado"],$rowD["impamortizado"],$rowD["ctadebe"]);
		}
	}
	$objReporte->ContenidoFoot($impcancelado,$impamortizado);
	$objReporte->tUsuarios();
	$objReporte->AddPage();

	//
		
	$sqlD = "select codconcepto,concepto,ctadebe,ordenrecibo,SUM(impcancelado) AS impcancelado ,SUM(impamortizado) AS impamortizado
				from cobranza.f_cuadre_det_cierre
				WHERE codsuc=:codsuc AND fechareg between :fechainicio AND :fechafinal
				AND nropec<>0 AND anulado=0 ".$Ccar."
				GROUP BY codconcepto,concepto,ctadebe,ordenrecibo
				ORDER BY ordenrecibo";

		$consultaD = $conexion->prepare($sqlD);
		$consultaD->execute(array(":codsuc"=>$codsuc,
								  ":fechainicio"=>$fdesde,
								  ":fechafinal"=>$fhasta));
		$itemsD = $consultaD->fetchAll();
		$Conceptos = array();
		foreach($itemsD as $rowD)
		{
			

			switch ($rowD["codconcepto"]) 
						{
						    case 1:case 6:case 8:
						      $IdConcepto=1;
						    break;
						    case 2:
						      $IdConcepto=2;
						    break;
						    case 3:case 4:case 72:
						      $IdConcepto=3;
						    break;
						}

			if (!array_key_exists($IdConcepto, $Conceptos))
			{
				$Conceptos[$IdConcepto] = array("Id"=>$IdConcepto, "Descripcion"=>$rowD["concepto"],"Cancelado"=>$rowD["impcancelado"],"Amortizado"=>$rowD["impamortizado"],"CtaDebe"=>$rowD["ctadebe"]);
			}
			else
			{
				$DescripcionT =  $Conceptos[$IdConcepto]["Descripcion"];
				if($IdConcepto==3)
					$DescripcionT = "Interes";
				$Conceptos[$IdConcepto] = array("Id"=>$IdConcepto, "Descripcion"=>$DescripcionT,"CtaDebe"=>$rowD["ctadebe"],"Cancelado"=>$Conceptos[$IdConcepto]["Cancelado"]+$rowD["impcancelado"],"Amortizado"=>$Conceptos[$IdConcepto]["Amortizado"]+$rowD["impamortizado"]);
			}


			//$objReporte->ContenidoDet(strtoupper($rowD["concepto"]),$rowD["impcancelado"],$rowD["impamortizado"],$rowD["ctadebe"]);
		}
	//
		sort($Conceptos);
		for($i=0;$i<count($Conceptos);$i++)
		{
			$objReporte->ContenidoDet(strtoupper($Conceptos[$i]["Descripcion"]),$Conceptos[$i]["Cancelado"],$Conceptos[$i]["Amortizado"],$Conceptos[$i]["CtaDebe"]);
		}
		$objReporte->ContenidoFoot($impcancelado,$impamortizado);
			$objReporte->tUsuarios();
	$objReporte->Output();	
	 