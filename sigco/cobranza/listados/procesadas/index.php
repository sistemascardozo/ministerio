<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "PLANILLAS PROCESADAS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_procesados.js"></script>
<script>	
	jQuery(function($)
	{ 
	
		$( "#DivTipos" ).buttonset();
	});	
	function ValidarForm(Op)
	{
		var cars="";
		if(document.getElementById("raeconsolidado").checked==false)
		{
			if($("#todoscars").val()==1)
			{
			  cars="%"
			}else{
			  cars=$("#car").val();
			  if(cars==0)
			  {
				alert("Seleccione el Car")
				return false
			  }
			}
		}
		
		if(document.getElementById("rabresumen").checked==true)
		{
			url="imprimir.php";
		}
		if(document.getElementById("rabdetallado").checked==true)
		{
			url="imprimir_detallado.php";
		}		
		if(document.getElementById("rabtalones").checked==true)
		{
			url="imprimir_talones.php";
		}	
		if(document.getElementById("rabtipopago").checked==true)
		{
			url="imprimir_formapago.php";
		}
		if(document.getElementById("raeconsolidado").checked==true)
		{
			url="imprimir_consolidado.php";
		}
		url = url+"?fhasta="+$("#fhasta").val()+"&fdesde="+$("#fdesde").val()+"&car="+cars+"&codsuc=<?=$codsuc?>&cars="+$("#car option:selected").text();
		AbrirPopupImpresion(url,800,600)
		return false
	}	
	function Cancelar()
	{
		location.href='<?=$urldir?>/admin/indexB.php'
	}
	function CambiarEstadoB(Check,Obj)
	{
		if(Check.checked==true)
		{
			document.getElementById("car").disabled		= true
			$("#"+Obj).val(1)
		}else{
			document.getElementById("car").disabled		= false
			$("#"+Obj).val(0)
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td colspan="3"></td></tr>
				  <tr>
				    <td width="15%">Car</td>
				    <td width="4%" align="center">:</td>
				    <td colspan="4">
			          	<? $objDrop->drop_car($codsuc); ?>
                        <input type="checkbox" name="checkbox" id="checkbox" checked="checked" onclick="CambiarEstado(this,'todoscars');quitar_disabled(this,'car');" />
		            	Todos los Cars
                    </td>
			      </tr>
				  <tr>
				    <td>Fecha Desde</td>
				    <td align="center">:</td>
				    <td colspan="4"><label>
				      <input type="text" name="fdesde" id="fdesde" class="inputtext" size="13" maxlength="20" value="<?=$objDrop->FechaServer()?>" />
				    </label></td>
			      </tr>
				  <tr>
				    <td>Fecha Hasta</td>
				    <td align="center">:</td>
				    <td colspan="4"><input type="text" name="fhasta" id="fhasta" class="inputtext" size="13" maxlength="20" value="<?=$objDrop->FechaServer()?>" /></td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td width="13%"><input type="hidden" name="todoscars" id="todoscars" value="1" /></td>
				    <td width="30%" align="center">&nbsp;</td>
				    <td width="3%">&nbsp;</td>
				    <td width="35%">&nbsp;</td>
			      </tr>
				  <tr>
				    <td colspan="6" align="center">
            				<div id="DivTipos" style="display:inline">
		                           <input type="radio" name="rabresumen" id="raeconsolidado" value="radio5" checked="checked"/>
				                        <label for="raeconsolidado">Consolidado</label>
			                           	<input type="radio" name="rabresumen" id="rabresumen" value="radio"  />
				                       	<label for="rabresumen">Resumen</label>
				                        <input type="radio" name="rabresumen" id="rabtipopago" value="radio4" />
				                        <label for="rabtipopago">Forma de Pago</label>    
				                        <input type="radio" name="rabresumen" id="rabtalones" value="radio3" />
				                        <label for="rabtalones">Talones</label>
				                        <input type="radio" name="rabresumen" id="rabdetallado" value="radio2" />
				                        <label for="rabdetallado">Detallado</label>
							</div>	
            			</td>
            		</tr>
                      <tr><td colspan="6"></td></tr>
                    		<tr><td colspan='6' align="center"> <input type="button" onclick="return ValidarForm();" value="Exportar" id=""></td></tr>
                        	<tr><td colspan="6"></td></tr>
                  
			      </tr>
				</table>
			</fieldset>
		</td>
	</tr>
 
	 </tbody>
    </table>
 </form>
</div>
<script>
	$("#car").attr("disabled",true)
</script>
<?php   CuerpoInferior();?>