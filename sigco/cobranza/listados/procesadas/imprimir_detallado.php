<?php
	include("../../../../objetos/clsReporte.php"); 
	
	class clsDetallado extends clsReporte
	{
		function cabecera()
		{
			global $conexion,$cars,$fecha,$codsuc,$fdesde,$fhasta,$car,$$Ccar;
			$this->SetTextColor(0,0,0);
			$Sql="SELECT DISTINCT(nropec) 
			FROM public.depositosbanco 
			WHERE codemp=1 AND codsuc=".$codsuc." 
			 ".$$Ccar." AND 
			(fechaini between '".$fdesde."' AND '".$fhasta."' 
			OR fechaafin between '".$fdesde."' AND '".$fhasta."') ";
			$Consulta=$conexion->query($Sql);
			$NroPec='';
			foreach($Consulta->fetchAll() as $row)
			{
				$NroPec .=$row[0].", ";
			}

			$this->SetFont('Arial','B',14);
			$tit1 = "DETALLE DE PLANILLA ENTRADA CAJA";
			$this->Cell(277,$h+2,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(40, $h,$fecha,0,1,'L');
			
			$this->Cell(10, $h,"Nro. Pec",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->MultiCell(0, $h,$NroPec,0,'L');
			$this->Ln(1);
			$this->SetFont('Arial','',6);
			$this->Cell(12, $h,"Nro. Pago",1,0,'C');
			$this->Cell(15, $h,"Codigo",1,0,'C');
			$this->Cell(54, $h,"Cliente",1,0,'C');
			$this->Cell(48, $h,"Direccion",1,0,'C');
			$this->Cell(10, $h,"Doc",1,0,'C');
			$this->Cell(10, $h,"Serie",1,0,'C');
			$this->Cell(12, $h,"Nro. Doc.",1,0,'C');
			$this->Cell(10, $h,utf8_decode("Año"),1,0,'C');
			$this->Cell(10, $h,"Mes",1,0,'C');
			$this->Cell(12, $h,"Agua",1,0,'C');
			$this->Cell(12, $h,"Desague",1,0,'C');
			$this->Cell(12, $h,"Cargo Fijo",1,0,'C');
			$this->Cell(12, $h,"Intereses",1,0,'C');
			$this->Cell(12, $h,"Redondeo",1,0,'C');
			$this->Cell(12, $h,"Otros",1,0,'C');
			$this->Cell(12, $h,"Igv",1,0,'C');
			$this->Cell(12, $h,"Total",1,1,'C');
			//$this->Ln(3);
						
		}
		function ContenidoCab($categoria)
		{
			$h=4;
			
			$this->SetFont('Arial','B',7);
			$this->SetTextColor(230,0,0);
			
			$this->Cell(277, $h,"(1)  ".$categoria,0,1,'L');
		}
		function ContenidoCab_1($tipopago)
		{
			$h=4;
			
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(170,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(272, $h,"(2) ".$tipopago,0,1,'L');
		}
		function ContenidoCab_2($tipodeuda)
		{
			$h=4;
			
			$this->SetFont('Arial','B',7);
			$this->SetTextColor(200,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(267, $h,"(3) ".$tipodeuda,0,1,'L');
		}
		function ContenidoDetalle($nropago,$nroinscripcion,$propietario,$direccion,$documento,
								 $serie,$nrodocumento,$anio,$mes,$agua,$desague,$cargofijo,$intereses,$redondeo,
								 $otros,$igv,$total)
		{
			global $codsuc;
			$nroinscripcion =  $this->CodUsuario($codsuc,$nroinscripcion);
			$h=4;
			
			$this->SetFont('Arial','',6);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(12, $h,$nropago,0,0,'C');
			$this->Cell(15, $h,$nroinscripcion,0,0,'C');
			$this->Cell(54, $h,utf8_decode($propietario),0,0,'L');
			$this->Cell(48, $h,$direccion,0,0,'L');
			$this->Cell(10, $h,$documento,0,0,'C');
			$this->Cell(10, $h,$serie,0,0,'C');
			$this->Cell(12, $h,$nrodocumento,0,0,'C');
			$this->Cell(10, $h,$anio,0,0,'C');
			$this->Cell(10, $h,$mes,0,0,'C');
			$this->Cell(12, $h,$agua,0,0,'R');
			$this->Cell(12, $h,$desague,0,0,'R');
			$this->Cell(12, $h,$cargofijo,0,0,'R');
			$this->Cell(12, $h,$intereses,0,0,'R');
			$this->Cell(12, $h,$redondeo,0,0,'R');
			$this->Cell(12, $h,$otros,0,0,'R');
			$this->Cell(12, $h,$igv,0,0,'R');
			$this->Cell(12, $h,$total,0,1,'R');
		}
		function ContenidoDetallex($nropago,$nroinscripcion,$propietario,$direccion,$documento,
								 $serie,$nrodocumento,$anio,$mes,$concepto,$total)
		{
			$h=4;
			
			$this->SetFont('Arial','',6);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(12, $h,$nropago,0,0,'C');
			$this->Cell(15, $h,$nroinscripcion,0,0,'C');
			$this->Cell(54, $h,$propietario,0,0,'L');
			$this->Cell(48, $h,$direccion,0,0,'L');
			$this->Cell(10, $h,$documento,0,0,'C');
			$this->Cell(10, $h,$serie,0,0,'C');
			$this->Cell(12, $h,$nrodocumento,0,0,'C');
			$this->Cell(10, $h,$anio,0,0,'C');
			$this->Cell(10, $h,$mes,0,0,'C');
			$this->Cell(84, $h,$concepto,0,0,'L');
			$this->Cell(12, $h,$total,0,1,'R');
		}
		function ContenidoFoot($agua,$desague,$cargofijo,$intereses,$redondeo,$otros,$igv,$total)
		{
			$h=4;
			
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(255,0,0);
			
			$this->Cell(12, $h,"",0,0,'C');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(54, $h,"",0,0,'L');
			$this->Cell(48, $h,"",0,0,'L');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(32, $h,"(3) Total Deuda -->",0,0,'R');
			$this->Cell(12, $h,$agua,'T',0,'R');
			$this->Cell(12, $h,$desague,'T',0,'R');
			$this->Cell(12, $h,$cargofijo,'T',0,'R');
			$this->Cell(12, $h,$intereses,'T',0,'R');
			$this->Cell(12, $h,$redondeo,'T',0,'R');
			$this->Cell(12, $h,$otros,'T',0,'R');
			$this->Cell(12, $h,$igv,'T',0,'R');
			$this->Cell(12, $h,$total,'T',1,'R');
		}
		function ContenidoFootx($agua,$desague,$cargofijo,$intereses,$redondeo,$otros,$igv,$total)
		{
			$h=4;
			
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(255,0,0);
			
			$this->Cell(12, $h,"",0,0,'C');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(54, $h,"",0,0,'L');
			$this->Cell(48, $h,"",0,0,'L');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(32, $h,"(2) Total Tipo Pago -->",0,0,'R');
			$this->Cell(12, $h,$agua,'T',0,'R');
			$this->Cell(12, $h,$desague,'T',0,'R');
			$this->Cell(12, $h,$cargofijo,'T',0,'R');
			$this->Cell(12, $h,$intereses,'T',0,'R');
			$this->Cell(12, $h,$redondeo,'T',0,'R');
			$this->Cell(12, $h,$otros,'T',0,'R');
			$this->Cell(12, $h,$igv,'T',0,'R');
			$this->Cell(12, $h,$total,'T',1,'R');
		}
		function ContenidoFooty($agua,$desague,$cargofijo,$intereses,$redondeo,$otros,$igv,$total)
		{
			$h=4;
			
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(255,0,0);
			
			$this->Cell(12, $h,"",0,0,'C');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(54, $h,"",0,0,'L');
			$this->Cell(48, $h,"",0,0,'L');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(32, $h,"(1) Total Categoria-->",0,0,'R');
			$this->Cell(12, $h,$agua,'T',0,'R');
			$this->Cell(12, $h,$desague,'T',0,'R');
			$this->Cell(12, $h,$cargofijo,'T',0,'R');
			$this->Cell(12, $h,$intereses,'T',0,'R');
			$this->Cell(12, $h,$redondeo,'T',0,'R');
			$this->Cell(12, $h,$otros,'T',0,'R');
			$this->Cell(12, $h,$igv,'T',0,'R');
			$this->Cell(12, $h,$total,'T',1,'R');
		}
		function ContenidoFootZ($agua,$desague,$cargofijo,$intereses,$redondeo,$otros,$igv,$total)
		{
			$h=4;
			
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(255,0,0);
			
			$this->Cell(12, $h,"",0,0,'C');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(54, $h,"",0,0,'L');
			$this->Cell(48, $h,"",0,0,'L');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(32, $h,"Total -->",0,0,'R');
			$this->Cell(12, $h,$agua,'T',0,'R');
			$this->Cell(12, $h,$desague,'T',0,'R');
			$this->Cell(12, $h,$cargofijo,'T',0,'R');
			$this->Cell(12, $h,$intereses,'T',0,'R');
			$this->Cell(12, $h,$redondeo,'T',0,'R');
			$this->Cell(12, $h,$otros,'T',0,'R');
			$this->Cell(12, $h,$igv,'T',0,'R');
			$this->Cell(12, $h,$total,'T',1,'R');
		}
    }
	
	$fdesde		= $_GET["fdesde"];
	$fhasta		= $_GET["fhasta"];
	$car		= $_GET["car"];
	$codsuc		= $_GET["codsuc"];

	if($car!="%"){$cars = $_GET["cars"];}else{$cars = "TODOS";}
	$fecha = $fdesde ." HASTA ".$fhasta;
	if($car!="%"){$Ccar=" AND c.car=".$car;}else{$Ccar="";}
	//$car 		= "%".$car."%";
	
	$totagua 		= 0;
	$totdesague		= 0;
	$totcargofijo	= 0;
	$totinteres		= 0;
	$totredondeo	= 0;
	$tototros		= 0;
	$totigv			= 0;
	$totalT			= 0;
			
	$objReporte	=	new clsDetallado();
	
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
	$objReporte->AliasNbPages();
	$objReporte->AddPage("L");
	$cat 	= -1;
	$tpago	= "100";
	$tdeuda	= 0;
	
	$sqlC = "select d.categoria,d.tipopago,t.descripcion as tipod,c.nropago,c.propietario,c.direccion,d.coddocumento,d.serie,d.nrodocumento,
			 d.anio,d.mes,t.codtipodeuda,c.nroinscripcion from cobranza.detpagos as d
			 inner join cobranza.cabpagos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.nroinscripcion=c.nroinscripcion and d.nropago=c.nropago)
			 inner join public.tipodeuda as t on(d.codtipodeuda=t.codtipodeuda)
			 where CONVERT(CHAR,c.car) LIKE :car and c.codsuc=:codsuc and c.fechareg between :finicio and :ffinal and c.nropec<>0 and c.anulado=0 
			 group by d.categoria,d.tipopago,t.descripcion,c.nropago,c.propietario,c.direccion,d.coddocumento,d.serie,d.nrodocumento,
			 d.anio,d.mes,t.codtipodeuda,c.nroinscripcion order by d.categoria,d.tipopago,t.codtipodeuda,c.nropago;";
	
	$sqlC = "select d.categoria from cobranza.detpagos as d
			 inner join cobranza.cabpagos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.nroinscripcion=c.nroinscripcion and d.nropago=c.nropago)
			 inner join public.tipodeuda as t on(d.codtipodeuda=t.codtipodeuda)
			 where c.codsuc=:codsuc ".$Ccar." and c.fechareg between :finicio and :ffinal and c.nropec<>0 and c.anulado=0 
			 group by d.categoria order by d.categoria;";

	$consultaCat = $conexion->prepare($sqlC); 
	$consultaCat->execute(array(
							  ":codsuc"=>$codsuc,
							  ":finicio"=>$fdesde,
							  ":ffinal"=>$fhasta));
	
	$itemsCat = $consultaCat->fetchAll();
	
	$opcDeuda 		= 0;
	$opcTipoPago 	= 0;
	$opcCategoria	= 0;
	
	foreach($itemsCat as $rowCat)
	{
		$totCagua 			= 0;
		$totCdesague		= 0;
		$totCcargofijo		= 0;
		$totCinteres		= 0;
		$totCredondeo		= 0;
		$totCotros			= 0;
		$totCigv			= 0;
		$totalcategoria		= 0;

		$objReporte->ContenidoCab($categoria[$rowCat["categoria"]]);
		$sqlC = "select d.tipopago
				from cobranza.detpagos as d
			 inner join cobranza.cabpagos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.nroinscripcion=c.nroinscripcion and d.nropago=c.nropago)
			 inner join public.tipodeuda as t on(d.codtipodeuda=t.codtipodeuda)
			 where c.codsuc=:codsuc ".$Ccar."
			 and c.fechareg between :finicio and :ffinal and c.nropec<>0 
			 and c.anulado=0 AND d.categoria=:categoria
			 group by d.tipopago
			 order by d.tipopago;";
		$consultaFP = $conexion->prepare($sqlC); 
		$consultaFP->execute(array(
								  ":codsuc"=>$codsuc,
								  ":finicio"=>$fdesde,
								  ":ffinal"=>$fhasta,
								  ":categoria"=>$rowCat["categoria"]
								  ));
		
		$itemsFP = $consultaFP->fetchAll();
		foreach($itemsFP as $rowFP)
		{
			$totPagua 			= 0;
			$totPdesague		= 0;
			$totPcargofijo		= 0;
			$totPinteres		= 0;
			$totPredondeo		= 0;
			$totPotros			= 0;
			$totPigv			= 0;
			$totalpago			= 0;
			$objReporte->ContenidoCab_1($tippago[$rowFP["tipopago"]]);
			$sqlC = "select t.codtipodeuda,t.descripcion as tipod
			from cobranza.detpagos as d
			 inner join cobranza.cabpagos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.nroinscripcion=c.nroinscripcion and d.nropago=c.nropago)
			 inner join public.tipodeuda as t on(d.codtipodeuda=t.codtipodeuda)
			 where c.codsuc=:codsuc ".$Ccar."
			 and c.fechareg between :finicio and :ffinal and c.nropec<>0 
			 and c.anulado=0 AND d.categoria=:categoria AND d.tipopago=:tipopago
			 group by t.codtipodeuda,t.descripcion order by t.codtipodeuda;";
			$consultaTD = $conexion->prepare($sqlC); 
			$consultaTD->execute(array(
									  ":codsuc"=>$codsuc,
									  ":finicio"=>$fdesde,
									  ":ffinal"=>$fhasta,
									  ":categoria"=>$rowCat["categoria"],
									  ":tipopago"=>$rowFP["tipopago"]
									  ));
			$itemsTD = $consultaTD->fetchAll();
			foreach($itemsTD as $rowTD)
			{
				$totDagua 			= 0;
				$totDdesague		= 0;
				$totDcargofijo		= 0;
				$totDinteres		= 0;
				$totDredondeo		= 0;
				$totDotros			= 0;
				$totDigv			= 0;
				$totaldeuda			= 0;
				$objReporte->ContenidoCab_2($rowTD["tipod"]);
				$sqlC = "select d.categoria,d.tipopago,t.descripcion as tipod,c.nropago,c.propietario,c.direccion,d.coddocumento,d.serie,d.nrodocumento,
				 d.anio,d.mes,t.codtipodeuda,c.nroinscripcion from cobranza.detpagos as d
				 inner join cobranza.cabpagos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.nroinscripcion=c.nroinscripcion and d.nropago=c.nropago)
				 inner join public.tipodeuda as t on(d.codtipodeuda=t.codtipodeuda)
				 where c.codsuc=:codsuc ".$Ccar."
				 and c.fechareg between :finicio and :ffinal and c.nropec<>0 
				 and c.anulado=0 AND d.categoria=:categoria AND d.tipopago=:tipopago
				 AND t.codtipodeuda=:codtipodeuda
				 group by d.categoria,d.tipopago,t.descripcion,c.nropago,c.propietario,c.direccion,d.coddocumento,d.serie,d.nrodocumento,
				 d.anio,d.mes,t.codtipodeuda,c.nroinscripcion order by d.categoria,d.tipopago,t.codtipodeuda,c.nropago;";
				$consultaC = $conexion->prepare($sqlC); 
				$consultaC->execute(array(
										  ":codsuc"=>$codsuc,
										  ":finicio"=>$fdesde,
										  ":ffinal"=>$fhasta,
										  ":categoria"=>$rowCat["categoria"],
										  ":tipopago"=>$rowFP["tipopago"],
										  ":codtipodeuda"=>$rowTD["codtipodeuda"],
										  ));
				$itemsC = $consultaC->fetchAll();
				foreach($itemsC as $rowC)
				{
					$impAgua 		= 0;
					$impDesague 	= 0;
					$impInteres		= 0;
					$impIgv			= 0;
					$impCargoFijio	= 0;
					$impColateral	= 0;
					$impRedondeo	= 0;
					$total			= 0;
					$sqlD = "select d.importe,co.categoria,co.codconcepto,co.descripcion
				 from cobranza.detpagos as d
				 inner join cobranza.cabpagos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				 and d.nropago=c.nropago and d.nroinscripcion=c.nroinscripcion)
				 inner join facturacion.conceptos as co on(d.codemp=co.codemp and d.codsuc=co.codsuc and d.codconcepto=co.codconcepto)
				 where d.codsuc=? and d.nropago=? and c.nropec<>0 and c.anulado=0 
				 AND d.categoria=? AND d.tipopago=? and d.codtipodeuda=? AND d.nrodocumento=?
				 group by co.categoria,co.codconcepto,d.importe,co.descripcion";
		
					$consultaD = $conexion->prepare($sqlD);
					$consultaD->execute(array(
						$codsuc,$rowC["nropago"],$rowCat["categoria"],$rowFP["tipopago"],$rowTD["codtipodeuda"],$rowC["nrodocumento"]));
					$itemsD = $consultaD->fetchAll();

					foreach($itemsD as $rowD)
					{
						/*if($rowD["categoria"]==1){$impAgua+=$rowD["importe"];}
						if($rowD["categoria"]==2){$impDesague+=$rowD["importe"];}
						if($rowD["categoria"]==3){$impInteres+=$rowD["importe"];}
						if($rowD["categoria"]==4){$impIgv+=$rowD["importe"];}
						if($rowD["categoria"]==5){$impCargoFijio+=$rowD["importe"];}
						if($rowD["categoria"]==6){$impColateral+=$rowD["importe"];}
						if($rowD["categoria"]==7){$impRedondeo+=$rowD["importe"];}*/
						switch ($rowD["categoria"]) 
						{
						    case 1:
						       $impAgua+=$rowD["importe"];
						    break;
						    case 2:
						       $impDesague+=$rowD["importe"];
						    break;
						    case 3:
						        $impInteres+=$rowD["importe"];
						    break;
						    case 4:
						        $impIgv+=$rowD["importe"];
						    break;
						    case 5:
						      $impCargoFijio+=$rowD["importe"];
						    break;
						    case 6:
						      $impColateral+=$rowD["importe"];
						    break;
						    case 7:
						      $impRedondeo+=$rowD["importe"];
						    break;
						    default:
						       $impotros += $rowF[1];
						}
						
						if($rowC["categoria"]==3 && $rowC["codtipodeuda"]==5){
								
					
								$objReporte->ContenidoDetallex($rowC["nropago"],
															   $rowC["nroinscripcion"],
															   strtoupper($rowC["propietario"]),
															   strtoupper($rowC["direccion"]),
															   $rowC["coddocumento"],
															   $rowC["serie"],
															   $rowC["nrodocumento"],
															   $rowC["anio"],
															   $rowC["mes"],
															   strtoupper($rowD["descripcion"]),
															   number_format($rowD["importe"],2));	
						}
					}
					//--Variables para el Tipo de Deuda---
					$totDagua 			+= $impAgua;
					$totDdesague		+= $impDesague;
					$totDcargofijo		+= $impCargoFijio;
					$totDinteres		+= $impInteres;
					$totDredondeo		+= $impRedondeo;
					$totDotros			+= $impColateral;
					$totDigv			+= $impIgv;
					
					$total = $impAgua + $impDesague + $impInteres + $impIgv + $impCargoFijio + $impColateral + $impRedondeo;
					
					$totaldeuda += $total;
					
					//--Variables para el Tipo de Pago---
					$totPagua 			+= $impAgua;
					$totPdesague		+= $impDesague;
					$totPcargofijo		+= $impCargoFijio;
					$totPinteres		+= $impInteres;
					$totPredondeo		+= $impRedondeo;
					$totPotros			+= $impColateral;
					$totPigv			+= $impIgv;

					$totalpago += $total;
					
					//--Variables para la Categoria---
					$totCagua 			+= $impAgua;
					$totCdesague		+= $impDesague;
					$totCcargofijo		+= $impCargoFijio;
					$totCinteres		+= $impInteres;
					$totCredondeo		+= $impRedondeo;
					$totCotros			+= $impColateral;
					$totCigv			+= $impIgv;
				
					$totalcategoria += $total;
					
					//--Variables para la Sumatoria Total---
					$totagua 		+= $impAgua;
					$totdesague		+= $impDesague;
					$totcargofijo	+= $impCargoFijio;
					$totinteres		+= $impInteres;
					$totredondeo	+= $impRedondeo;
					$tototros		+= $impColateral;
					$totigv			+= $impIgv;
				
					$totalT 		+= $total;
					if($rowC["categoria"]!=3 && $rowC["codtipodeuda"]!=5)
					{
						
					
						$objReporte->ContenidoDetalle($rowC["nropago"],
													   $rowC["nroinscripcion"],
													   strtoupper(utf8_decode($rowC["propietario"])),
													   strtoupper($rowC["direccion"]),
													   $rowC["coddocumento"],
													   $rowC["serie"],
													   $rowC["nrodocumento"],
													   $rowC["anio"],
													   $rowC["mes"],
													   number_format($impAgua,2),
													   number_format($impDesague,2),
													   number_format($impCargoFijio,2),
													   number_format($impInteres,2),
													   number_format($impRedondeo,2),
													   number_format($impColateral,2),
													   number_format($impIgv,2),
													   number_format($total,2));
					}

				}
				//if($rowC["categoria"]==3 && $rowC["codtipodeuda"]==5){$totDotros=0;}
			$objReporte->ContenidoFoot(number_format($totDagua,2),
									   number_format($totDdesague,2),
									   number_format($totDcargofijo,2),
									   number_format($totDinteres,2),
									   number_format($totDredondeo,2),
									   number_format($totDotros,2),
									   number_format($totDigv,2),
									   number_format($totaldeuda,2));


			}
			$objReporte->ContenidoFootx(number_format($totPagua,2),
										number_format($totPdesague,2),
										number_format($totPcargofijo,2),
										number_format($totPinteres,2),
										number_format($totPredondeo,2),
										number_format($totPotros,2),
										number_format($totPigv,2),
										number_format($totalpago,2));

		}	 	
		$objReporte->ContenidoFooty(number_format($totCagua,2),
										number_format($totCdesague,2),
										number_format($totCcargofijo,2),
										number_format($totCinteres,2),
										number_format($totCredondeo,2),
										number_format($totCotros,2),
										number_format($totCigv,2),
										number_format($totalcategoria,2));	


	}
	
	$objReporte->ContenidoFootZ(number_format($totagua,2),
							    number_format($totdesague,2),
								number_format($totcargofijo,2),
								number_format($totinteres,2),
								number_format($totredondeo,2),
							    number_format($tototros,2),
								number_format($totigv,2),
								number_format($totalT,2));
	$objReporte->Output();	
	
?>