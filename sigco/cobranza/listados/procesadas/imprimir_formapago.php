<?php
	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$cars,$caja,$conexion,$codsuc,$Ccar,$fdesde,$fhasta;

			$Sql="SELECT DISTINCT(nropec) 
			FROM public.depositosbanco 
			WHERE codemp=1 AND codsuc=".$codsuc." 
			".$Ccar." AND 
			(fechaini between '".$fdesde."' AND '".$fhasta."' 
			OR fechaafin between '".$fdesde."' AND '".$fhasta."') ";
			$Consulta=$conexion->query($Sql);
			$NroPec='';
			foreach($Consulta->fetchAll() as $row)
			{
				$NroPec .=$row[0].", ";
			}

			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN DE PLANILLA ENTRADA CAJA POR FORMA DE PAGO";
			$this->Cell(190,$h+3,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			$this->Cell(190,0.01,'',1,1,'L');
			$this->Ln(2);
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(60, $h,$cars,0,0,'L');
			
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(40, $h,$fecha,0,1,'L');

			$this->Cell(10, $h,"Nro. Pec",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->MultiCell(0, $h,$NroPec,0,'L');	
			
						
		}
		function resumen($codsuc,$Ccar,$fecha,$cajero)
		{
			global $conexion,$fdesde,$fhasta;
			$Sql ="select fp.descripcion,SUM(c.imptotal)
			from cobranza.cabpagos c
			INNER JOIN public.formapago fp ON ( fp.codformapago=c.codformapago)
			where c.codsuc=".$codsuc." and c.nropec<>0  ".$Ccar." and 
			c.fechareg between '".$fdesde."' AND '".$fhasta."' AND c.anulado=0
			GROUP BY fp.descripcion";
			$this->Ln(5);
			$this->SetFont('Arial','B',12);
			$tit1 = "RESUMEN";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'C');	
			$this->Ln(2);
			$Consulta =$conexion->query($Sql);
			$Total=0;
			$this->SetFont('Arial','',9);
			foreach($Consulta->fetchAll() as $row)
			{
				$this->SetX(70);
				$this->Cell(30,4,strtoupper($row[0]),0,0,'C',false);
				$this->Cell(5,4,':',0,0,'C',false);
				$this->Cell(20,4,number_format($row[1],2),0,1,'R',false);
				$Total+=$row[1];
			}	
			$this->Ln(2);
			$this->SetX(105);
			$this->Cell(20,0.01,'',1,1,'R',false);
			$this->Ln(2);
			$this->SetX(70);
			$this->Cell(30,4,'TOTAL',0,0,'C',false);
			$this->Cell(5,4,':',0,0,'C',false);
			$this->Cell(20,4,number_format($Total,2),0,1,'R',false);
			$this->Ln(5);

		}
		function cabdetellado()
		{
			$this->Ln(5);
			$this->SetFont('Arial','B',12);
			$tit1 = "DETALLADO";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'C');
			$h=4;
			$this->Ln(1);
			$this->SetFont('Arial','',7);
			$this->Cell(15, $h,"Nro. Pago",1,0,'C');
			$this->Cell(15, $h,"Codigo",1,0,'C');
			$this->Cell(65, $h,"Cliente",1,0,'C');
			$this->Cell(15, $h,"Doc",1,0,'C');
			$this->Cell(15, $h,"Serie",1,0,'C');
			$this->Cell(20, $h,"Nro. Doc.",1,0,'C');
			$this->Cell(20, $h,"Total",1,0,'C');
			$this->Cell(20, $h,"Efectivo",1,0,'C');
			$this->Cell(5, $h,"A",1,1,'C');
		}
		function contenido($nropago,$codigo,$usuario,$doc,$serie,$nrodoc,$total,$efectivo,$anulado)
		{
			$h=4;
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(15, $h,$nropago,0,0,'C');
			$this->Cell(15, $h,$codigo,0,0,'C');
			$this->Cell(65, $h,utf8_decode($usuario),0,0,'L');
			$this->Cell(15, $h,$doc,0,0,'C');
			$this->Cell(15, $h,$serie,0,0,'C');
			$this->Cell(20, $h,$nrodoc,0,0,'C');
			$this->Cell(20, $h,$total,0,0,'R');
			$this->Cell(20, $h,$efectivo,0,0,'R');
			$this->Cell(5, $h,$anulado,0,1,'C');
			
		}
		function contenidofoot($cont_normal,$cont_anulado,$tot_procesado,$total_anulado)
		{
			$h=4;
			
			$this->Ln(5);
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Cobrados",0,0,'R');
			$this->Cell(15, $h,$cont_normal,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Procesado",0,0,'R');
			$this->Cell(20, $h,$tot_procesado,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Anulado",0,0,'R');
			$this->Cell(15, $h,$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Menos Total Anulado",0,0,'R');
			$this->Cell(20, $h,$total_anulado,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Total de Talones",0,0,'R');
			$this->Cell(15, $h,$cont_normal+$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Cobrado",0,0,'R');
			$this->Cell(20, $h,$tot_procesado,0,1,'R');
		}
    }
	
	/*$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];
	
	$cajeros 	= $_SESSION['user'];
	$cars		= $_GET["cars"];
	$caja		= $_GET["caja"];*/

	$fhasta		= $_GET["fhasta"];
	$fdesde		= $_GET["fdesde"];
	$car		= $_GET["car"];
	$codsuc		= $_GET["codsuc"];
	
	if($car!="%"){$cars = $_GET["cars"];}else{$cars = "TODOS";}
	$fecha = $fdesde ." HASTA ".$fhasta;
	//$car		= "%".$_GET["car"]."%";
	if($car!="%"){$Ccar=" AND c.car=".$car;}else{$Ccar="";}
        
	$objReporte	=	new clsTalones();
	
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->resumen($codsuc,$Ccar,$fecha,$cajero);
	$objReporte->cabdetellado();	
	$Sql ="select fp.codformapago, fp.descripcion
			from cobranza.cabpagos c
			INNER JOIN public.formapago fp ON ( fp.codformapago=c.codformapago)
			where c.codsuc=".$codsuc." and c.nropec<>0  ".$Ccar." and 
			c.fechareg between '".$fdesde."' and '".$fhasta."' and c.anulado=0
			GROUP BY fp.codformapago, fp.descripcion";
	$Consulta =$conexion->query($Sql);
	$cont_normal=0;
		$cont_anulado=0;
		
		$tot_procesados=0;
		$total_anulado=0;
	foreach($Consulta->fetchAll() as $row)
	{	$h=4;
		$objReporte->Ln(2);
		$objReporte->SetFont('Arial','B',9);
		$objReporte->SetTextColor(0,0,0);
		$objReporte->Cell(15, $h,strtoupper($row[1]),0,1,'L');
		$objReporte->Cell(15,0.01,'',1,1,'L');
		$objReporte->Ln(2);
		$sqlC = "select nropago,nroinscripcion,propietario,imppagado,imptotal,anulado from cobranza.cabpagos 
				 where codsuc=? and nropec<>0 and CONVERT(CHAR,car) LIKE '".$car."'
				 and fechareg between '".$fdesde."' and '".$fhasta."'
				 AND codformapago=?";
		
		$consultaC = $conexion->prepare($sqlC);
		$consultaC->execute(array($codsuc,$row[0]));
		$itemsC = $consultaC->fetchAll();
		
		
		
		foreach($itemsC as $rowC)
		{
			$sqlD = "select nrodocumento from cobranza.detpagos where codemp=1 and codsuc=? and nroinscripcion=? and 
							nropago=? and nrodocumento<>0 group by nrodocumento";
			
			$consultaD = $conexion->prepare($sqlD);
			$consultaD->execute(array($codsuc,$rowC["nroinscripcion"],$rowC["nropago"]));
			$itemsD = $consultaD->fetch();
			
			if($rowC["anulado"]==1)
			{
				$x				= "x";
				$cont_anulado++;
				$total_anulado += $rowC["imptotal"];
			}else{
				$x				= "";
				$cont_normal++;
				$tot_procesados += $rowC["imptotal"];
			}

			$objReporte->contenido($rowC["nropago"],$rowC["nroinscripcion"],utf8_decode($rowC["propietario"]),4,'0051',$itemsD["nrodocumento"],
							number_format($rowC["imptotal"],2),number_format($rowC["imppagado"],2),$x);
			
		}
	}
	$objReporte->contenidofoot($cont_normal,$cont_anulado,number_format($tot_procesados,2),number_format($total_anulado,2));

	$objReporte->Output();	
	
?>