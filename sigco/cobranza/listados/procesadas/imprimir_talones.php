<?php
	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $conexion,$cars,$fecha,$codsuc,$fdesde,$fhasta,$Ccar;

			$Sql="SELECT DISTINCT(nropec) 
			FROM public.depositosbanco 
			WHERE codemp=1 AND codsuc=".$codsuc." 
			".$Ccar." AND 
			(fechaini between '".$fdesde."' AND '".$fhasta."' 
			OR fechaafin between '".$fdesde."' AND '".$fhasta."') ";
			$Consulta=$conexion->query($Sql);
			$NroPec='';
			foreach($Consulta->fetchAll() as $row)
			{
				$NroPec .=$row[0].", ";
			}

			$this->SetFont('Arial','B',14);
			$tit1 = "VERIFICACION PLANILLA ENTRADA A CAJA POR TALONES";
			$this->Cell(190,$h+2,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(40, $h,$fecha,0,1,'L');

			$this->Cell(10, $h,"Nro. Pec",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->MultiCell(0, $h,$NroPec,0,'L');
				
			$this->Ln(1);
			$this->SetFont('Arial','',7);
			$this->Cell(15, $h,"Nro. Pago",1,0,'C');
			$this->Cell(15, $h,"Codigo",1,0,'C');
			$this->Cell(65, $h,"Cliente",1,0,'C');
			$this->Cell(15, $h,"Doc",1,0,'C');
			$this->Cell(15, $h,"Serie",1,0,'C');
			$this->Cell(20, $h,"Nro. Doc.",1,0,'C');
			$this->Cell(20, $h,"Total",1,0,'C');
			$this->Cell(20, $h,"Efectivo",1,0,'C');
			$this->Cell(5, $h,"A",1,1,'C');
						
		}
		function contenido($nropago,$codigo,$usuario,$doc,$serie,$nrodoc,$total,$efectivo,$anulado)
		{
			global $codsuc;
			$codigo =  $this->CodUsuario($codsuc,$codigo);
			$h=4;
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(15, $h,$nropago,0,0,'C');
			$this->Cell(15, $h,$codigo,0,0,'C');
			$this->Cell(65, $h,utf8_decode($usuario),0,0,'L');
			$this->Cell(15, $h,$doc,0,0,'C');
			$this->Cell(15, $h,$serie,0,0,'C');
			$this->Cell(20, $h,$nrodoc,0,0,'C');
			$this->Cell(20, $h,$total,0,0,'R');
			$this->Cell(20, $h,$efectivo,0,0,'R');
			$this->Cell(5, $h,$anulado,0,1,'C');
			
		}
		function contenidofoot($cont_normal,$cont_anulado,$tot_procesado,$total_anulado,$cont_rpspublic,$cont_boleta,$cont_factura,$tot_rpspublic,$tot_boleta,$tot_factura)
		{
			$h=4;
			
			$this->Ln(5);
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Nro de Resibos por Serv. P.",0,0,'R');
			$this->Cell(15, $h,$cont_rpspublic,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Resibos por Serv. P.",0,0,'R');
			$this->Cell(20, $h,$tot_rpspublic,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Nro de Boletas.",0,0,'R');
			$this->Cell(15, $h,$cont_boleta,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Boletas",0,0,'R');
			$this->Cell(20, $h,$tot_boleta,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Nro de Facturas.",0,0,'R');
			$this->Cell(15, $h,$cont_factura,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Facturas",0,0,'R');
			$this->Cell(20, $h,$tot_factura,0,1,'R');
			
			$this->Cell(190,0.1,"",1,1,'C');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Cobrados",0,0,'R');
			$this->Cell(15, $h,$cont_normal,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Procesado",0,0,'R');
			$this->Cell(20, $h,$tot_procesado,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Anulado",0,0,'R');
			$this->Cell(15, $h,$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Menos Total Anulado",0,0,'R');
			$this->Cell(20, $h,$total_anulado,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Total de Talones",0,0,'R');
			$this->Cell(15, $h,$cont_normal+$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Cobrado",0,0,'R');
			$this->Cell(20, $h,$tot_procesado,0,1,'R');
		}
    }
	
	$fhasta		= $_GET["fhasta"];
	$fdesde		= $_GET["fdesde"];
	$car		= $_GET["car"];
	$codsuc		= $_GET["codsuc"];
	
	if($car!="%"){$cars = $_GET["cars"];}else{$cars = "TODOS";}
	$fecha = $fdesde ." HASTA ".$fhasta;
	//$car		= "%".$_GET["car"]."%";
	if($car!="%"){$Ccar=" AND c.car=".$car;}else{$Ccar="";}
	$objReporte	=	new clsTalones();
	
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$Sql ="SELECT DISTINCT( c.nropago),d.coddocumento,doc.descripcion
			FROM cobranza.cabpagos c
			  INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
			  AND (c.codsuc = d.codsuc)
			  AND (c.nroinscripcion = d.nroinscripcion)
			  AND (c.nropago = d.nropago)
			  INNER JOIN reglasnegocio.documentos doc ON (d.coddocumento = doc.coddocumento) AND (d.codsuc = doc.codsuc)
     		   where c.codsuc=? and c.nropec<>0  ".$Ccar." and c.fechareg between ? and ?
			  ORDER BY d.coddocumento,c.nropago";
	$consultaG = $conexion->prepare($Sql);
	$consultaG->execute(array($codsuc,$fdesde,$fhasta));
	$itemsG = $consultaG->fetchAll();
	$cont_normal=0;
	$cont_anulado=0;
	$cont_rpspublic=0;
	$cont_boleta=0;
	$cont_factura=0;
	
	$tot_procesados=0;
	$total_anulado=0;
	$total_rpspublic=0;
	$total_boleta=0;
	$total_factura=0;
	$Temp='';
	$tot_procesados=0;
	$total_anulado=0;
	$total_rpspublic=0;
	$total_boleta=0;
	$total_factura=0;
	foreach($itemsG as $rowG)
	{		
		if($Temp!=$rowG[1])
		{
			$objReporte->SetFont('Arial','B',7);
			$objReporte->Cell(0,5,strtoupper($rowG[2]),0,1,'L');
			$Temp=$rowG[1];
		}

	$sqlC = "select nropago,nroinscripcion,propietario,imppagado,imptotal,anulado from cobranza.cabpagos 
			 where codsuc=? and nropec<>0 ".$Ccar." and fechareg between ? and ? AND nropago=?";
	
	$consultaC = $conexion->prepare($sqlC);
	$consultaC->execute(array($codsuc,$fdesde,$fhasta,$rowG[0]));
	$itemsC = $consultaC->fetchAll();
	
	$cont_normal=0;
	$cont_anulado=0;
	$cont_rpspublic=0;
	$cont_boleta=0;
	$cont_factura=0;
	
	
		
	foreach($itemsC as $rowC)
	{
		$sqlD = "	select nrodocumento ,coddocumento,case when coddocumento=4 then 'RSP' when coddocumento=13 then'BOL.' when coddocumento=14 then 'FAC.' end as documento,serie,coddocumento as coddocumentocod
					from cobranza.detpagos 
					where codemp=1 and codsuc=? and nroinscripcion=? and 
						nropago=? and nrodocumento<>0 group by nrodocumento,coddocumento,serie";
		
		$consultaD = $conexion->prepare($sqlD);
		$consultaD->execute(array($codsuc,$rowC["nroinscripcion"],$rowC["nropago"]));
		$itemsD = $consultaD->fetch();
		
		if($rowC["anulado"]==1)
		{
			$x				= "x";
			$cont_anulado++;
			$total_anulado += $rowC["imptotal"];
		}else{
			$x				= "";
			$cont_normal++;
			$tot_procesados += $rowC["imptotal"];
		}
		
		if($itemsD["coddocumentocod"]==4)
		{
			$cont_rpspublic++;
			$tot_rpspublic += $rowC["imptotal"];
			
		}elseif($itemsD["coddocumentocod"]==13){
			$cont_boleta++;
			$tot_boleta += $rowC["imptotal"];
			
		}elseif($itemsD["coddocumentocod"]==14){
			$cont_factura++;
			$tot_factura += $rowC["imptotal"];
		}
		
		$objReporte->contenido($rowC["nropago"],$rowC["nroinscripcion"],$rowC["propietario"],$itemsD["documento"],$itemsD["serie"],$itemsD["nrodocumento"],
						number_format($rowC["imptotal"],2),number_format($rowC["imppagado"],2),$x);
		
	}
	}
	$objReporte->contenidofoot($cont_normal,$cont_anulado,number_format($tot_procesados,2),number_format($total_anulado,2) ,$cont_rpspublic,$cont_boleta, $cont_factura,number_format($tot_rpspublic,2),number_format($tot_boleta,2),number_format($tot_factura,2));

	$objReporte->Output();	
	
?>