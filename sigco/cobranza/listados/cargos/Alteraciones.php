<?php
	set_time_limit(0);
	
	include("../../../../objetos/clsReporte.php"); 
	
	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $m,$anio,$ciclotext,$Dim;

			$this->SetFont('Arial','B',14);
			$this->SetY(15);
			$tit1 = "REPORTE DE ALTERACIONES POR CARGOS";
			$this->Cell(277,$h+2,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(277,$h,$ciclotext,0,1,'C');
			
			$this->SetX(113.5);
			$this->Cell(10, $h,"MES",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$m,0,0,'L');
			$this->Cell(10, $h,utf8_decode("AÑO"),0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$anio,0,1,'L');
			
			$this->Ln(2);
			$this->Ln(1);
			$this->SetFont('Arial','',6);
			

			$this->Cell($Dim[1],$h,"NRO. DCTO.",'1',0,'C');
			$this->Cell($Dim[2],$h,"FECHA",'1',0,'C');
			$this->Cell($Dim[3],$h,"INSCRIPCION",'1',0,'C');
			$this->Cell($Dim[4],$h,"NOMBRE",'1',0,'C');
			$this->Cell($Dim[5],$h,"VALOR DE VENTA",'1',0,'C');
			$this->Cell($Dim[6],$h,"NO IMPONIBLE",'1',0,'C');
			$this->Cell($Dim[7],$h,"I.G.V.",'1',0,'C');
			$this->Cell($Dim[8],$h,"REDONDEO",'1',0,'C');
			$this->Cell($Dim[9],$h,"IMPORTE",'1',1,'C');

			
			
						
		}
		function agregar_detalle($codsuc,$codciclo,$anio,$mes,$Csec)
		{
			global $conexion,$Dim; 
			if(intval($mes)<10) $mes="0".$mes;
			
			$h=4;
			$ultimodia = $this->obtener_ultimo_dia_mes($mes,$anio);
			$primerdia = $this->obtener_primer_dia_mes($mes,$anio);
			$count = 0;
			
			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;
			
			$this->SetTextColor(0,0,0);
			$sql = "select nrofacturacion,fechaemision
			from facturacion.periodofacturacion
			where codemp=1 and codsuc=? and codciclo=? AND anio=? AND mes=?";
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$codciclo,$anio,$mes));
			$row = $consulta->fetch();
			$nrofacturacion=$row['nrofacturacion'];
			

			
				//REBAJAS
				$Sql="SELECT  c.fechaemision as fechareg,
							d.coddocumentoabono as coddocumento,d.seriedocumentoabono as serie,
							d.nrodocumentoabono as nrodocumento,
					cl.codtipodocumento,cl.nrodocumento as nrodoc,
					c.nroinscripcion,cl.propietario,SUM(d.imprebajado) as impmes,
					SUM(CASE WHEN d.codconcepto=5 THEN d.imprebajado  ELSE 0 END) as  impigv,
					d.coddocumento as coddocumento2,d.nrodocumento as nrodocumento2,
					d.seriedocumento as seriedocumento2,c.glosa,c.estareg,
					 SUM(CASE WHEN d.codconcepto<>7 AND d.codconcepto<>8 THEN d.imprebajado  ELSE 0 END) as noimp,
					 SUM(CASE WHEN d.codconcepto=7 or d.codconcepto=8 THEN d.imprebajado  ELSE 0 END) as redondeo
					FROM   facturacion.cabrebajas c
					INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nrorebaja = d.nrorebaja)
					INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
					AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
				  	inner join catastro.clientes as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and
            				c.nroinscripcion=cl.nroinscripcion)
						  WHERE c.codsuc=".$codsuc."22 /*AND c.estareg=11111 */
						  AND c.anio='".$anio."' 
						  AND c.mes='".intval($mes)."'
						  /*AND c.fechaemision BETWEEN '{$primerdia}' AND '{$ultimodia}'*/
						  and d.imprebajado>0
						  GROUP BY c.fechaemision,d.coddocumentoabono,d.seriedocumentoabono,
							d.nrodocumentoabono,cl.codtipodocumento,cl.nrodocumento,
							c.nroinscripcion,cl.propietario,
							d.coddocumento ,d.nrodocumento ,
					d.seriedocumento ,c.glosa,c.estareg
					ORDER BY c.fechaemision ";
			
			$consulta = $conexion->prepare($Sql);
			$consulta->execute(array($codsuc,$codciclo));
			$items = $consulta->fetchAll();
			$borde=0;
			$tvVenta=0;
			$tnImponible=0;
			$tigv=0;
			$tredondeo=0;
			//var_dump($consulta->errorInfo());
			$this->SetFillColor(255,255,255); //Color de Fondo
		    $this->SetTextColor(0);
		    $tfagua=0;
		    $tfdes=0;
		    $tfotros=0;
		    $tfigv=0;
		    $tfredondeo=0;

			foreach($items as $row)
			{
				$count++;
				$datos=$this->FechaDoc($row["coddocumento2"],$row["seriedocumento2"],$row["nrodocumento2"]);
				if($row['estareg']==0)
				{
					$row['noimp']=0;
					$row['impigv']=0;
					$row['redondeo']=0;
					$row['impmes']=0;
					$row["glosa"] = $row["glosa"]." ANULADO";
			
				}
				$this->Cell($Dim[1],$h,'F '.$row["seriedocumento2"].' '.$row["nrodocumento2"],$borde,0,'L',true);
				$this->Cell($Dim[2],$h,$datos[0],$borde,0,'C',true);
				$this->Cell($Dim[3],$h,$this->CodUsuario($codsuc,strtoupper($row["nroinscripcion"])),$borde,0,'L',true);
				$this->Cell($Dim[4],$h,strtoupper($row["propietario"]),$borde,0,'L',true);
				$this->Cell($Dim[5],$h,'0.00',$borde,0,'R',true);
				$this->Cell($Dim[6],$h,number_format($datos[1],2),$borde,0,'R',true);
				$this->Cell($Dim[7],$h,number_format($datos[2],2),$borde1,0,'R',true);
				$this->Cell($Dim[8],$h,number_format($datos[3],2),$borde,0,'R',true);
				$this->Cell($Dim[9],$h,number_format($datos[4],2),$borde,0,'R',true);

				$tfigv+=$datos[2];
				$tfagua+=$datos[5];
				$tfdes+=$datos[6];
				$tfotros+=$datos[7];
				$tfredondeo+=$datos[3];

				$this->Ln($h);

				$this->Cell($Dim[1],$h,'N '.$row["serie"].' '.$row["nrodocumento"],$borde,0,'L',true);
				$this->Cell($Dim[2],$h,$this->DecFecha($row["fechareg"]),$borde,0,'C',true);
				$this->Cell($Dim[3]+$Dim[4],$h,strtoupper($row["glosa"]),$borde,0,'L',true);
				$this->Cell($Dim[5],$h,'0.00',$borde,0,'R',true);
				$this->Cell($Dim[6],$h,number_format($row['noimp'],2),$borde,0,'R',true);
				$this->Cell($Dim[7],$h,number_format($row['impigv'],2),$borde1,0,'R',true);
				$this->Cell($Dim[8],$h,number_format($row['redondeo'],2),$borde,0,'R',true);
				$this->Cell($Dim[9],$h,number_format($row['impmes'],2),$borde,0,'R',true);
				$this->Ln($h);

				$this->Cell($Dim[1],$h,'',$borde,0,'L',true);
				$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
				$this->Cell($Dim[3],$h,'',$borde,0,'L',true);
				$this->SetFont('Arial','B',6);
				$this->Cell($Dim[4],$h,'Nuevo Importe',$borde,0,'L',true);
				$this->SetFont('Arial','',6);
				$this->Cell($Dim[5],$h,'0.00',$borde,0,'R',true);
				$this->Cell($Dim[6],$h,number_format($datos[1]-$row['noimp'],2),$borde,0,'R',true);
				$this->Cell($Dim[7],$h,number_format($datos[2]-$row['impigv'],2),$borde1,0,'R',true);
				$this->Cell($Dim[8],$h,number_format($datos[3]-$row['redondeo'],2),$borde,0,'R',true);
				$this->Cell($Dim[9],$h,number_format($datos[4]-$row['impmes'],2),$borde,0,'R',true);
				$this->Ln($h);



				$tvVenta+=0;
				$tnImponible+=$row["noimp"];
				$tigv+=$row["impigv"];
				$tredondeo+=$row["redondeo"];
				$imptotal+=$row["impmes"];
				

			}
			$this->Cell(0 ,0.01,"",1,1,'C');
			//$imptotal += ($impmes+$impigv);
			$this->Ln(3);
			
			$this->SetFont('Arial','B',10);
			$this->Cell(0 ,$h,"RESUMEN DE ALTERACIONES POR CARGOS",0,1,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',8);
			$this->SetX(130);
			$this->Cell(30,$h,"MONTO INICIAL",'B',0,'C');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,"IMPORTE CARGOS",'B',0,'C');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,"NUEVO IMPORTE",'B',1,'C');
			//TOTALE
			$Sql="SELECT  
					SUM(CASE WHEN d.codconcepto=1 THEN d.imprebajado  ELSE 0 END) as agua,
					SUM(CASE WHEN d.codconcepto=2 THEN d.imprebajado  ELSE 0 END) as desague,
					SUM(CASE WHEN d.codconcepto=5 THEN d.imprebajado  ELSE 0 END) as igv,
    				SUM(CASE WHEN d.codconcepto=7 or d.codconcepto=8 THEN d.imprebajado  ELSE 0 END) as redondeo,
    				SUM(CASE WHEN d.codconcepto<>1 AND d.codconcepto<>2  AND d.codconcepto<>5  AND d.codconcepto<>7  AND d.codconcepto<>8 THEN d.imprebajado  ELSE 0 END) as otros
					FROM   facturacion.cabrebajas c
					INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nrorebaja = d.nrorebaja)
					INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
					AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
				  	inner join catastro.clientes as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and
            				c.nroinscripcion=cl.nroinscripcion)
						  WHERE c.codsuc=".$codsuc."11 AND c.estareg=1 
						  AND c.anio='".$anio."' 
						  AND c.mes='".intval($mes)."'
						  /*AND c.fechaemision BETWEEN '{$primerdia}' AND '{$ultimodia}'*/
						   ";
			$c = $conexion->query($Sql)->fetch();
			$this->Ln(3);
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"1. Agua",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tfagua,2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($c['agua'],2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tfagua-$c['agua'],2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"2. Desague",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tfdes,2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($c['desague'],2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tfdes-$c['desague'],2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"3. Otros Conceptos",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tfotros,2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($c['otros'],2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tfotros-$c['otros'],2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"4. I.G.V.",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tfigv,2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($c['igv'],2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tfigv-$c['igv'],2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"",0,0,'L');
			$this->Cell(30,$h,'','B',0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,'','B',0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,'','B',1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total del Detalle ",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tfagua+$tfdes+$tfotros+$tfigv,2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($c['agua']+$c['desague']+$c['otros']+$c['igv'],2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format(($tfagua+$tfdes+$tfotros+$tfigv)-($c['agua']+$c['desague']+$c['otros']+$c['igv']),2),0,1,'R');


			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Redondeo",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tfredondeo,2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($c['redondeo'],2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tfredondeo-$c['redondeo'],2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tfagua+$tfdes+$tfotros+$tfigv+$tfredondeo,2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($c['agua']+$c['desague']+$c['otros']+$c['igv']+$c['redondeo'],2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format(($tfagua+$tfdes+$tfotros+$tfigv+$tfredondeo)-($c['agua']+$c['desague']+$c['otros']+$c['igv']+$c['redondeo']),2),0,1,'R');

		}
			
		function FechaDoc($cod,$serie,$numero)
		{
			global $conexion,$codsuc;
			$Sql ="SELECT c.fechareg,
					SUM(CASE WHEN d.codconcepto<>7 AND d.codconcepto<>8 THEN d.importe  ELSE 0 END) as noimp,
					 SUM(CASE WHEN d.codconcepto=7 or d.codconcepto=8 THEN d.importe  ELSE 0 END) as redondeo,
					 SUM(d.importe) as total,
					 SUM(CASE WHEN d.codconcepto=1 THEN d.importe  ELSE 0 END) as agua,
					SUM(CASE WHEN d.codconcepto=2 THEN d.importe  ELSE 0 END) as desague,
					SUM(CASE WHEN d.codconcepto=5 THEN d.importe  ELSE 0 END) as igv,
					SUM(CASE WHEN d.codconcepto<>1 AND d.codconcepto<>2  AND d.codconcepto<>5  AND d.codconcepto<>7  AND d.codconcepto<>8 THEN d.importe  ELSE 0 END) as otros
					FROM facturacion.cabfacturacion c
					  INNER JOIN facturacion.detfacturacion d ON (c.codemp = d.codemp)
					  AND (c.codsuc = d.codsuc)
					  AND (c.nrofacturacion = d.nrofacturacion)
					  AND (c.nroinscripcion = d.nroinscripcion)
					  AND (c.codciclo = d.codciclo)
					WHERE c.codsuc=".$codsuc." AND c.coddocumento=".$cod."
					AND c.serie='".$serie."' AND c.nrodocumento='".$numero."'
					GROUP BY c.fechareg";
			$c = $conexion->query($Sql)->fetch();
			$datos[0]=$this->DecFecha($c[0]);
			$datos[1]=$c['noimp'];
			$datos[2]=$c['igv'];
			$datos[3]=$c['redondeo'];
			$datos[4]=$c['total'];
			$datos[5]=$c['agua'];
			$datos[6]=$c['desague'];
			$datos[7]=$c['otros'];
			return $datos;
		}
		function EquivalenteDoc($cod,$tipo)
		{
			if($tipo==1)
			{
				switch ($cod) 
				{
					case 4: return "14";break;
					case 13: return "03";break;
					case 14: return "01";break;
					case 17: return "07";break;
				}
			}
			else
			{
				switch ($cod) 
				{
					case 1: return "1";break;
					case 3: return "6";break;
					default: return "0";break;
				}
			}
			/*
			COMPROBANTES
			-----------------------------
			14  Recibos de Pensiones
			01  Facturas
			03  Boletas de Ventas
			07  Notas de Abono/Credito
			08  Notas de cargo/Debito


			DOCUMENTOS
			-----------------------------
			1  DNI
			6  RUC
			0  Otros*/
		}

    }
	
	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc		= $_GET["codsuc"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclotext	= $_GET["ciclotext"];
	if($_GET["sector"]!="%"){$Csec=" AND cl.codsector=".$_GET["sector"];}else{$Csec="";}
	$m = $meses[$mes];
	$Dim = array('1'=>40,'2'=>25,'3'=>50,'4'=>70,'5'=>20,'6'=>20,'7'=>10,'8'=>20,'9'=>20);	
/*	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$caja		= $_GET["caja"];
	$cars		= $_GET["cars"];
	$cajeros 	= $_SESSION['nombre'];
	$nrocaja	= $_GET["nrocaja"];	
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$totagua 		= 0;
	$totdesague		= 0;
	$totcargofijo	= 0;
	$totinteres		= 0;
	$totredondeo	= 0;
	$tototros		= 0;
	$totigv			= 0;
	$totalT			= 0;*/
			
	$objReporte	=	new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage("L");
	
	$objReporte->agregar_detalle($codsuc,$ciclo,$anio,$mes,$Csec);

	
	$objReporte->Output();	
	
?>