<?php
	set_time_limit(0);
	
	include("../../../../objetos/clsReporte.php"); 
	
	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $m,$anio,$ciclotext,$Dim;

			$this->SetFont('Arial','B',14);
			$this->SetY(15);
			$tit1 = "REPORTE DE CARGOS";
			$this->Cell(277,$h+2,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(277,$h,$ciclotext,0,1,'C');
			
			$this->SetX(113.5);
			$this->Cell(10, $h,"MES",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$m,0,0,'L');
			$this->Cell(10, $h,utf8_decode("AÑO"),0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$anio,0,1,'L');
			
			$this->Ln(2);
			$this->Ln(1);
			$this->SetFont('Arial','',6);
			

			$this->Cell($Dim[1],$h,"NRO. DCTO.",'1',0,'C');
			$this->Cell($Dim[2],$h,"FECHA",'1',0,'C');
			$this->Cell($Dim[3],$h,"CODIGO",'1',0,'C');
			$this->Cell($Dim[4],$h,"NOMBRE",'1',0,'C');
			$this->Cell($Dim[5],$h,"VALOR DE VENTA",'1',0,'C');
			$this->Cell($Dim[6],$h,"NO IMPONIBLE",'1',0,'C');
			$this->Cell($Dim[7],$h,"I.G.V.",'1',0,'C');
			$this->Cell($Dim[8],$h,"REDONDEO",'1',0,'C');
			$this->Cell($Dim[9],$h,"IMPORTE",'1',1,'C');

			
			
						
		}
		function agregar_detalle($codsuc,$codciclo,$anio,$mes,$Csec)
		{
			global $conexion,$Dim; 
			if(intval($mes)<10) $mes="0".$mes;
			
			$h=4;
			$ultimodia = $this->obtener_ultimo_dia_mes($mes,$anio);
			$primerdia = $this->obtener_primer_dia_mes($mes,$anio);
			$count = 0;
			
			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;
			
			$this->SetTextColor(0,0,0);
			$sql = "select nrofacturacion,fechaemision
			from facturacion.periodofacturacion
			where codemp=1 and codsuc=? and codciclo=? AND anio=? AND mes=?";
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$codciclo,$anio,$mes));
			$row = $consulta->fetch();
			$nrofacturacion=$row['nrofacturacion'];
			$this->SetFont('Arial','',8);
			$Sql="	(
					SELECT dp.serie,dp.nrodocumento,c.fechareg,to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')) as codantiguo, SUM(d.interes) as noimp,c.estareg, 1 AS tipo
					FROM facturacion.cabcreditos c 
					INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrocredito = d.nrocredito) 
					INNER JOIN cobranza.detprepagos dp ON (c.codemp = dp.codemp) AND (c.codsuc = dp.codsuc) AND (c.nroprepagoinicial = dp.nroprepago) AND (c.nroinscripcion = dp.nroinscripcion)
					WHERE c.codsuc=".$codsuc." AND c.estareg=1 AND extract(year from c.fechareg) ='".$anio."'  AND extract(month from c.fechareg) ='".intval($mes)."' and c.interes>0 AND c.nroprepago>0 AND c.sininteres=0 AND c.nroprepagoinicial>0
					GROUP BY dp.serie,dp.nrodocumento,c.fechareg,to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')),c.estareg
					ORDER BY c.fechareg,dp.nrodocumento
					)
					UNION
					(
					SELECT dp.serie,dp.nrodocumento,c.fechaemision as fechareg,to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')) as codantiguo,
					CAST(SUM(d.importe)-c.totalrefinanciado AS NUMERIC (18,2)) as noimp,c.estareg,2 AS tipo
					FROM facturacion.cabrefinanciamiento c 
					INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorefinanciamiento = d.nrorefinanciamiento) 
					INNER JOIN cobranza.detprepagos dp ON (c.codemp = dp.codemp) AND (c.codsuc = dp.codsuc) AND (c.nroprepagoinicial = dp.nroprepago) AND (c.nroinscripcion = dp.nroinscripcion)
					where d.codemp=1 and d.codsuc=".$codsuc." AND c.estareg=1 AND  c.codestadorefinanciamiento<>5 AND extract(year from c.fechaemision) ='".$anio."'  AND extract(month from c.fechaemision) ='".intval($mes)."'  and c.interes>0 AND c.nroprepagoinicial>0 AND c.sininteres=0
					GROUP BY c.totalrefinanciado,dp.serie,dp.nrodocumento,c.fechaemision,to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')),c.estareg
					ORDER BY c.fechaemision,dp.nrodocumento
					)
					ORDER BY fechareg,nrodocumento";
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			$borde=0;
			$tvVenta=0;
			$tnImponible=0;
			$tigv=0;
			$tredondeo=0;
			//var_dump($consulta->errorInfo());
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);
			$TotalC=0;
			$TotalF=0;
			foreach($items as $row)
			{
				$count++;
				$row["glosa"]='INICIAL CONVENIO';
				if($row['estareg']==0)
				{
					$row['noimp']=0;
					$row['impigv']=0;
					$row['redondeo']=0;
					$row['impmes']=0;
					$row["glosa"] = $row["glosa"]." ANULADA";
				}
				if($row['tipo']==1 && $row['estareg']=1 ) $TotalC+=$row['noimp'];
				if($row['tipo']==2 && $row['estareg']=1 ) $TotalF+=$row['noimp'];
				$this->Cell($Dim[1],$h,'N '.$row["serie"].' '.$row["nrodocumento"],$borde,0,'C',true);
				$this->Cell($Dim[2],$h,$this->DecFecha($row["fechareg"]),$borde,0,'C',true);
				$this->Cell($Dim[3],$h,strtoupper($row["codantiguo"]),$borde,0,'L',true);
				$this->Cell($Dim[4],$h,strtoupper($row["glosa"]),$borde,0,'L',true);
				$this->Cell($Dim[5],$h,'0.00',$borde,0,'R',true);
				$this->Cell($Dim[6],$h,number_format($row['noimp'],2),$borde,0,'R',true);
				$this->Cell($Dim[7],$h,number_format($row['impigv'],2),$borde1,0,'R',true);
				$this->Cell($Dim[8],$h,number_format($row['redondeo'],2),$borde,0,'R',true);
				$this->Cell($Dim[9],$h,number_format($row['noimp'],2),$borde,0,'R',true);
				
				$tvVenta+=0;
				$tnImponible+=$row["noimp"];
				$tigv+=$row["impigv"];
				$tredondeo+=$row["redondeo"];
				$imptotal+=$row["noimp"];
				$this->Ln($h);

			}
			
			//$imptotal += ($impmes+$impigv);
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(230,0,0);
			

			$this->Cell($Dim[1]+$Dim[2]+$Dim[3]+$Dim[4], $h,"Total General ==>",0,0,'R');
			$this->Cell($Dim[5], $h,number_format($tvVenta,2),'T',0,'R');
			$this->Cell($Dim[6], $h,number_format($tnImponible,2),'T',0,'R');
			$this->Cell($Dim[7], $h,number_format($tigv,2),'T',0,'R');
			$this->Cell($Dim[8], $h,number_format($tredondeo,2),'T',0,'R');
			$this->Cell($Dim[9], $h,number_format($imptotal,2),'T',0,'R');
			
			$this->SetTextColor(0,0,0);
			$TotalF= $imptotal;


			$this->Cell(0 ,0.01,"",1,1,'C');
			//$imptotal += ($impmes+$impigv);
			$this->AddPage("L");
			$this->Ln(3);
			
			$this->SetFont('Arial','B',10);
			$this->Cell(0 ,$h,"RESUMEN DE CARGOS",0,1,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',8);
			$this->SetX(130);
			$this->Cell(30,$h,"IMPORTE CARGOS",'B',0,'C');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,"CANTIDAD",'B',1,'C');
			//TOTALE
			$Sql="SELECT 
					SUM(CASE WHEN d.codconcepto=1 THEN d.importe  ELSE 0 END) as agua,
					SUM(CASE WHEN d.codconcepto=2 THEN d.importe  ELSE 0 END) as desague,
					SUM(CASE WHEN d.codconcepto=5 THEN d.importe  ELSE 0 END) as igv,
					SUM(CASE WHEN d.codconcepto=10 THEN d.importe  ELSE 0 END) as inicial,
					SUM(CASE WHEN d.codconcepto<>1 AND d.codconcepto<>2 AND d.codconcepto<>5
							  AND d.codconcepto<>10 THEN d.importe  ELSE 0 END) as otros
					FROM facturacion.cabrefinanciamiento c 
					INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorefinanciamiento = d.nrorefinanciamiento) 
					INNER JOIN cobranza.detprepagos dp ON (c.codemp = dp.codemp) AND (c.codsuc = dp.codsuc) AND (c.nroprepagoinicial = dp.nroprepago) AND (c.nroinscripcion = dp.nroinscripcion)
					where d.codemp=1 and d.codsuc=".$codsuc." AND c.estareg=1 AND extract(year from c.fechaemision) ='".$anio."'  AND extract(month from c.fechaemision) ='".intval($mes)."'  and c.interes>0 AND c.nroprepagoinicial>0 AND c.sininteres=0";
			$cr = $conexion->query($Sql)->fetch();
			$Sql="SELECT 
					SUM(CASE WHEN df.codconcepto=1 THEN df.importe  ELSE 0 END) as agua,
					SUM(CASE WHEN df.codconcepto=2 THEN df.importe  ELSE 0 END) as desague,
					SUM(CASE WHEN df.codconcepto=5 THEN df.importe  ELSE 0 END) as igv,
					SUM(CASE WHEN df.codconcepto<>1 AND df.codconcepto<>2 AND df.codconcepto<>5
							  AND df.codconcepto<>10 THEN df.importe  ELSE 0 END) as otros
					FROM facturacion.cabrefinanciamiento c 
					INNER JOIN cobranza.detprepagos dp ON (c.codemp = dp.codemp) AND (c.codsuc = dp.codsuc) AND (c.nroprepagoinicial = dp.nroprepago) AND (c.nroinscripcion = dp.nroinscripcion)
					INNER JOIN facturacion.origen_refinanciamiento ori ON (c.codemp = ori.codemp) AND (c.codsuc = ori.codsuc)  AND (c.nrorefinanciamiento = ori.nrorefinanciamiento)
					INNER JOIN facturacion.cabfacturacion cf ON (ori.codemp = cf.codemp)  AND (ori.codsuc = cf.codsuc)  AND (ori.codciclo = cf.codciclo)  AND (ori.nrofacturacion = cf.nrofacturacion)  AND (ori.nroinscripcion = cf.nroinscripcion)
					INNER JOIN facturacion.detfacturacion df ON (cf.codemp = df.codemp)  AND (cf.codsuc = df.codsuc)  AND (cf.nrofacturacion = df.nrofacturacion)  AND (cf.nroinscripcion = df.nroinscripcion)
					where c.codemp=1 and c.codsuc=".$codsuc." AND c.estareg=1 AND extract(year from c.fechaemision) ='".$anio."'  AND extract(month from c.fechaemision) ='".intval($mes)."'  and c.interes>0 AND c.nroprepagoinicial>0 AND c.sininteres=0";
			$cf = $conexion->query($Sql)->fetch();
			$Tagua= 0;//(90/100)*$cr['inicial']+$cr['agua']-$cf['agua'];
			$TDesa=  0;//(10/100)*$cr['inicial']+$cr['desague']-$cf['desague'];
			$TOtro= $imptotal-$Tagua -$TDesa;//+($cr['otros']-$cf['otros'];

			$cTotal = $Tagua+$TDesa+$TOtro;
			$this->Ln(3);
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"1. Agua",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($Tagua,2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"2. Desague",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($TDesa,2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"3. Otros Conceptos",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($TOtro,2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"4. I.G.V.",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tigv,2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');


			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"",0,0,'L');
			$this->Cell(30,$h,'','B',0,'R');$this->Cell(5,$h,"",0,0,'C');
		
			$this->Cell(30,$h,'','B',1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total del Detalle ",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($cTotal,2),0,1,'R');
			


			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Redondeo",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($c['redondeo'],2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');
			

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($cTotal,2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$count,0,1,'R');

			
				
		}
			
		function FechaDoc($cod,$serie,$numero)
		{
			global $conexion,$codsuc;
			$Sql ="SELECT fechareg FROM facturacion.cabfacturacion
					WHERE codsuc=".$codsuc." AND coddocumento=".$cod."
					AND serie='".$serie."' AND nrodocumento='".$numero."'";
			$c = $conexion->query($Sql)->fetch();
			return $this->DecFecha($c[0]);
		}
		function EquivalenteDoc($cod,$tipo)
		{
			if($tipo==1)
			{
				switch ($cod) 
				{
					case 4: return "14";break;
					case 13: return "03";break;
					case 14: return "01";break;
					case 17: return "07";break;
				}
			}
			else
			{
				switch ($cod) 
				{
					case 1: return "1";break;
					case 3: return "6";break;
					default: return "0";break;
				}
			}
			/*
			COMPROBANTES
			-----------------------------
			14  Recibos de Pensiones
			01  Facturas
			03  Boletas de Ventas
			07  Notas de Abono/Credito
			08  Notas de cargo/Debito


			DOCUMENTOS
			-----------------------------
			1  DNI
			6  RUC
			0  Otros*/
		}

    }
	
	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc		= $_GET["codsuc"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclotext	= $_GET["ciclotext"];
	if($_GET["sector"]!="%"){$Csec=" AND cl.codsector=".$_GET["sector"];}else{$Csec="";}
	$m = $meses[$mes];
	$Dim = array('1'=>40,'2'=>25,'3'=>20,'4'=>100,'5'=>20,'6'=>20,'7'=>10,'8'=>20,'9'=>20);	
/*	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$caja		= $_GET["caja"];
	$cars		= $_GET["cars"];
	$cajeros 	= $_SESSION['nombre'];
	$nrocaja	= $_GET["nrocaja"];	
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$totagua 		= 0;
	$totdesague		= 0;
	$totcargofijo	= 0;
	$totinteres		= 0;
	$totredondeo	= 0;
	$tototros		= 0;
	$totigv			= 0;
	$totalT			= 0;*/
			
	$objReporte	=	new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage("L");
	
	$objReporte->agregar_detalle($codsuc,$ciclo,$anio,$mes,$Csec);

	
	$objReporte->Output();	
	
?>