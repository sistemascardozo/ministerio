<?php
session_name("pnsu");
if(!session_start()){session_start();}

	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$cars,$caja;
			$this->SetY(20);
			$this->SetFont('Arial','B',8);
			$tit1 = "";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'R');
			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN TOTAL DEL ".$fecha;
			$this->Cell(190,$h+3,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);
			$this->Cell(190,0.01,'',1,1,'L');
			  $this->Ln(2);
			$h=4;

		}
		function tCar($IdCar,$Car)
		{
			global $conexion,$codsuc,$fdesde,$fhasta,$cajero,$caj,$Ccaj;
			$Sql ="select SUM(c.imptotal)
			from cobranza.cabpagos c
			where c.codsuc=".$codsuc." and c.nropec=0 and c.car=".$IdCar." and
			c.fechareg between '".$fdesde."' AND '".$fhasta."'
			 ".$Ccaj." ".$cajero." AND c.anulado=0	";

			$Consulta 	= $conexion->query($Sql);
			$row		= $Consulta->fetch();
			$Total = $row[0];
			$h=4;
			$this->Ln(2);
			$this->SetFont('Arial','',9);
			$this->SetTextColor(0,0,0);
			$this->Cell(90, $h,strtoupper($Car),0,0,'L');
			$this->Cell(5, $h,':',0,0,'C');
			$this->Cell(20, $h,number_format($Total,2),0,1,'R');
			$this->Ln(2);
		}
		function tCars()
		{
			global $conexion,$codsuc,$fdesde,$fhasta,$cajero,$caj,$Ccar,$Ccaj;
			$Sql ="select SUM(c.imptotal)
			from cobranza.cabpagos c
			where c.codsuc=".$codsuc." and c.nropec=0 and
			c.fechareg between '".$fdesde."' AND '".$fhasta."' ".$Ccaj."
			".$cajero." AND c.anulado=0 ".$Ccar."";
			$this->Ln(2);
			$Consulta 	= $conexion->query($Sql);
			$row		= $Consulta->fetch();
			$Total = $row[0];
			$h=4;
			$this->Ln(2);
			$this->SetFont('Arial','',9);
			$this->SetTextColor(0,0,0);
			$this->Cell(90, $h,strtoupper(utf8_decode('TOTAL RECAUDACIÓN')),0,0,'L');
			$this->Cell(5, $h,':',0,0,'C');
			$this->Cell(20, $h,number_format($Total,2),0,1,'R');
			$this->Ln(2);
		}
		function rFormaPago()
		{
			global $conexion,$codsuc,$car,$fdesde,$fhasta,$cajero,$caj,$Ccar,$Ccaj;
			$Sql ="select fp.descripcion,SUM(c.imptotal)
			from cobranza.cabpagos c
			INNER JOIN public.formapago fp ON ( fp.codformapago=c.codformapago)
			where c.codsuc=".$codsuc." and c.nropec=0 and
			c.fechareg between '".$fdesde."' AND '".$fhasta."' ".$Ccaj."
			".$cajero." AND c.anulado=0 ".$Ccar."
			GROUP BY fp.descripcion";
			$this->Ln(5);
			$this->SetFont('Arial','B',12);
			$Consulta =$conexion->query($Sql);
			$Total=0;
			$this->SetFont('Arial','',9);
			foreach($Consulta->fetchAll() as $row)
			{

				$this->Cell(45,4,strtoupper($row[0]),0,0,'C',false);
				$this->Cell(5,4,':',0,0,'C',false);
				$this->Cell(20,4,number_format($row[1],2),0,1,'R',false);
				$Total+=$row[1];
			}
			$this->Ln(2);
			$this->SetX(60);
			$this->Cell(20,0.01,'',1,1,'R',false);
			$this->Ln(2);
			$this->Cell(45,4,'TOTAL',0,0,'C',false);
			$this->Cell(5,4,':',0,0,'C',false);
			$this->Cell(20,4,number_format($Total,2),0,1,'R',false);
			$this->Ln(5);

		}


    }

	$fhasta		= $_GET["fhasta"];
	$fdesde		= $_GET["fdesde"];
	$car		= $_GET["car"];
	$caj		= $_GET["caja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$fecha = $fdesde ." HASTA ".$fhasta;

	if($car!="%"){$cars = $_GET["cars"];$Ccar=" AND c.car=".$car;}else{$cars = "TODOS"; $Ccar="";}
	if($caj!="%"){$caja = $_GET["cajas"];$Ccaj=" and c.nrocaja=".$caj;}else{$caja = "TODOS";$Ccaj='';}

	//if($cajero=="%"){$cajeroD="and CONVERT(CHAR,cab.creador) LIKE '".$cajero."'";}else{$cajeroD="and cab.creador in(".$cajero.")";}
	if($cajero=="%"){$cajero="";}else{$cajero=" and creador in(".$cajero.")";}


	$objReporte	=	new clsTalones();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
	$Sql ="select ca.car, ca.descripcion
			from cobranza.cabpagos c
			INNER JOIN cobranza.car ca ON ( ca.car=c.car AND ca.codemp = c.codemp AND ca.codsuc = c.codsuc )

			where c.codsuc=".$codsuc." and c.nropec=0 ".$Ccar."
			 and  c.fechareg between '".$fdesde."' AND '".$fhasta."' ".$Ccaj."
			 ".$cajero." AND c.anulado=0
			GROUP BY ca.car, ca.descripcion ORDER BY ca.car";

	$ConsultaCar =$conexion->query($Sql);
	$objReporte->SetFont('Arial','B',12);
	$objReporte->Cell(190,4,utf8_decode('CENTROS DE RECAUDACIÓN'),0,1,'L');
	$objReporte->Ln(5);

	foreach($ConsultaCar->fetchAll() as $rowcar)
	{
		$car=$rowcar['car']	;
		$objReporte->tCar($rowcar[0],$rowcar[1]);

	}
	$objReporte->SetX(105);
	$objReporte->Cell(20,0.01,'',1,1,'R',false);
	$objReporte->tCars();
	$objReporte->SetFont('Arial','',9);
	$objReporte->Cell(190,0.01,'',1,1,'L');
	$objReporte->Ln(5);
	$objReporte->SetFont('Arial','B',12);
	$objReporte->Cell(190,4,'FORMAS DE PAGO',0,1,'L');
	$objReporte->rFormaPago();
	$objReporte->Cell(190,0.001,'',1,1,'L');
	$objReporte->Output();

?>
