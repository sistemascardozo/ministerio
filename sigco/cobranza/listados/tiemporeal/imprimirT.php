<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	include("../../../../objetos/clsReporte.php");

	class clsTiempoReal extends clsReporte
	{
		function cabecera()
		{
			global $cars, $caja, $cajeros, $fecha;

			$h=4;
			$this->SetFont('Arial','B',8);
			$tit1 = "";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'R');
			$this->Ln(2);
			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN DE PLANILLA ENTRADA CAJA";
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);

			$h=4;
			$this->SetFont('Arial','B',7);
			$this->Cell(5, $h,"CAR",0,0,'L');
			$this->Cell(4, $h,":",0,0,'C');
			$this->SetFont('Arial','',7);
			$this->Cell(30, $h,$cars,0,0,'L');
			$this->SetFont('Arial','B',7);
			$this->Cell(7, $h,"CAJA",0,0,'R');
			$this->Cell(4, $h,":",0,0,'C');
			$this->SetFont('Arial','',7);
			$this->Cell(30, $h,strtoupper($caja),0,0,'L');
			$this->SetFont('Arial','B',7);
			$this->Cell(10, $h,"CAJERO",0,0,'R');
			$this->Cell(4, $h,":",0,0,'C');
			$this->SetFont('Arial','',7);
			$this->Cell(44, $h, utf8_decode(strtoupper($cajeros)),0,0,'L');
			$this->SetFont('Arial','B',7);
			$this->Cell(8, $h,"FECHA",0,0,'R');
			$this->Cell(4, $h,":",0,0,'C');
			$this->SetFont('Arial','',7);
			$this->Cell(40, $h,$fecha,0,1,'L');

			$this->Ln(1);
			$this->Cell(20, $h,"CODIGO",1,0,'C');
			$this->Cell(65, $h,"CONCEPTO",1,0,'C');
			$this->Cell(20, $h,"NRO. ITEMS",1,0,'C');
			$this->Cell(20, $h,"CODCTA.",1,0,'C');
			$this->Cell(25, $h,"CANCELADO",1,0,'C');
			$this->Cell(25, $h,"A CUENTA",1,0,'C');
			$this->Cell(20, $h,"TOTAL",1,1,'C');
			//$this->Ln(3);

		}
		function ContenidoCab($concepto,$cancelado,$acuenta)
		{
			$h=4;

			$this->SetFont('Arial','B',7);
			$this->SetTextColor(230,0,0);

			$this->Cell(20, $h,$codigo,0,0,'L');
			$this->Cell(65, $h,$concepto,0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(20, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoDet($codigo, $concepto,$cancelado,$acuenta,$ctacontable)
		{
			$h=4;

			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);

			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(10, $h,$codigo,0,0,'C');
			$this->Cell(70, $h,$concepto,0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,$ctacontable,0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(20, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoFoot($cancelado,$acuenta)
		{
			$h=4;

			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);

			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(70, $h,"",0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"TOTALES",0,0,'R');
			$this->Cell(35, $h,number_format($cancelado,2),'T',0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),'T',0,'R');
			$this->Cell(20, $h,number_format($cancelado+$acuenta,2),'T',1,'R');
		}
    }

	$fhasta		= $_GET["fhasta"];
	$fdesde		= $_GET["fdesde"];
	$car		= $_GET["car"];
	$caj		= $_GET["caja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$fecha = $fdesde ." HASTA ".$fhasta;

	if($car!="%")
	{
		$cars = $_GET["cars"];
		$Ccar2 =" AND c.car = ".$car;
		$Ccar = " AND car = ".$car;
	}
	else
	{
		$cars = "TODOS";
		$Ccar="";
	}

	if($caj!="%"){$caja = $_GET["cajas"];$Ccaj=" and nrocaja=".$caj;}else{$caja = "TODOS";$Ccaj='';}

	if($cajero=="%")
	{
		$cajero="";
	}
	else
	{
		$cajero2 = " AND c.codusu IN(".$cajero.")";
		$cajero = " AND creador IN(".$cajero.")";

	}

	$cajeros = "TODOS";

	$objReporte	=	new clsTiempoReal();
	$objReporte->AliasNbPages();

	$sql = "SELECT 0, 0 ";

	if ($cajero2 != '')
	{
		$sql = "SELECT DISTINCT c.codusu, c.nombres ";
		$sql .= "FROM seguridad.usuarios c ";
		$sql .= " INNER JOIN cobranza.cabpagos cp ON (c.codusu = cp.creador) AND (c.codemp = cp.codemp) AND (c.codsuc = cp.codsuc) ";
		$sql .= "WHERE c.codsuc = ".$codsuc.$Ccar2.$cajero2." ";
		$sql .= " AND cp.fechareg BETWEEN '".$fdesde."' AND '".$fhasta."'";
	}

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array());

	$itemsC = $consulta->fetchAll();

	foreach($itemsC as $rowC)
	{
		$cajero = ' AND creador = '.$rowC[0];
		$cajeros = $rowC[1];

		if ($_GET["cajero"] == '%')
		{
			$cajero = '';
			$cajeros = "TODOS";
		}

		$objReporte->AddPage();

		$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
		$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
		$impcancelado	= 0;
		$impamortizado 	= 0;


		$sql = "SELECT codconcepto, concepto, ctadebe, ordenrecibo, SUM(impcancelado) AS impcancelado, SUM(impamortizado) AS impamortizado ";
		$sql .= "FROM cobranza.f_cuadre_det_cierre ";
		$sql .= "WHERE codsuc = ".$codsuc." AND fechareg BETWEEN '".$fdesde."' AND '".$fhasta."' AND nropec = 0 AND anulado=0 ";
		$sql .= " ".$Ccar.$Ccaj.$cajero." ";
		$sql .= "GROUP BY codconcepto, concepto, ctadebe, ordenrecibo ";
		//$sql .= "ORDER BY ordenrecibo";
		$sql .= "ORDER BY codconcepto";

		//$sql = "SELECT codcategoria, categoria_descripcion, SUM(impcancelado) AS impcancelado, SUM(impamortizado) AS impamortizado ";
//		$sql .= "FROM cobranza.f_cuadre_cab_cierre_pro(".$codsuc.", '".$fdesde."', '".$fhasta."', '".$car."', '') ";
		//die($sql);
		//echo $sql.'<br>';

		$consulta = $conexion->prepare($sql);
		$consulta->execute(array());

		$items = $consulta->fetchAll();

		foreach($items as $row)
		{
			//$objReporte->ContenidoCab(strtoupper($row["categoria_descripcion"]), $row["impcancelado"], $row["impamortizado"]);

			$impcancelado 	+= $row["impcancelado"];
			$impamortizado 	+= $row["impamortizado"];

			//$sqlD = "SELECT concepto, ctadebe, ordenrecibo, SUM(impcancelado) AS impcancelado, SUM(impamortizado) AS impamortizado
//					FROM cobranza.f_cuadre_det_cierre
//					WHERE codsuc = :codsuc AND fechareg BETWEEN :fechainicio AND :fechafinal
//					AND nropec = 0 AND anulado = 0 ".$Ccar.$Ccaj.$cajero." AND codcategoria = :codcategoria
//					GROUP BY concepto, ctadebe, ordenrecibo
//					ORDER BY ordenrecibo";
//			//die($sqlD);
//
//
//			$consultaD = $conexion->prepare($sqlD);
//			$consultaD->execute(array(":codsuc"=>$codsuc,
//									  ":fechainicio"=>$fdesde,
//									  ":fechafinal"=>$fhasta,
//									  ":codcategoria"=>$row["codcategoria"]
//									  ));
			//foreach($consultaD->fetchAll() as $rowD)
			//{
				$objReporte->ContenidoDet($row["codconcepto"], utf8_decode(strtoupper($row["concepto"])),$row["impcancelado"],$row["impamortizado"],$row["ctadebe"]);
			//}
		}
		$objReporte->ContenidoFoot($impcancelado,$impamortizado);
	}
	$objReporte->Output();

?>
