<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$cars,$caja;
			$this->SetY(20);
			$this->SetFont('Arial','B',8);
			$tit1 = "";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'R');
			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN TOTAL DEL ".$fecha;
			$this->Cell(190,$h+3,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);
			$this->Cell(190,0.01,'',1,1,'L');
			  $this->Ln(2);
			$h=4;
		}

		function contenido($nombre, $altura, $inicio)
		{
			$this->SetXY(19,$inicio);
			$this->SetFont('Arial','',8);
			$this->SetTextColor(0,0,0);
			$this->Cell(70, $altura,strtoupper($nombre),0,1,'L');
		}

		function detalle_contenido($nombre,$cantidad)
		{

			$this->SetX(80);
			$h = 5;
			$this->SetFont('Arial','',8);
			$this->SetTextColor(0,0,0);
			$this->Cell(90, $h,strtoupper($nombre), 0,0,'L');
			$this->Cell(20, $h,number_format($cantidad,2),0,1,'R');
		}

  }

	$fhasta = $_GET["fhasta"];
	$fdesde = $_GET["fdesde"];
	$car    = $_GET["car"];
	$caj    = $_GET["caja"];
	$cajero = $_GET["cajero"];
	$codsuc = $_GET["codsuc"];

	$fecha  = $fdesde ." HASTA ".$fhasta;

	if($car!="%"){$cars = $_GET["cars"];$Ccar=" AND c.car=".$car;}else{$cars = "TODOS"; $Ccar="";}
	if($caj!="%"){$caja = $_GET["cajas"];$Ccaj=" and c.nrocaja=".$caj;}else{$caja = "TODOS";$Ccaj='';}

	if($cajero=="%"){$cajero="";}else{$cajero=" and creador in(".$cajero.")";}


	$objReporte	=	new clsTalones();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
	$Sql  = " SELECT ca.car, ca.descripcion, c.creador,u.nombres, sum(c.imptotal) ";
	$Sql .= " FROM cobranza.cabpagos c ";
	$Sql .= " INNER JOIN cobranza.car ca ON ( ca.car=c.car AND ca.codemp = c.codemp AND ca.codsuc = c.codsuc ) ";
	$Sql .= " INNER JOIN seguridad.usuarios u ON ( c.creador = u.codusu ) ";
	$Sql .= " WHERE c.codsuc = " . $codsuc . " " . $Ccaj . " " . $cajero . "";
	$Sql .= " AND c.fechareg BETWEEN '".$fdesde."' AND '".$fhasta."' AND c.anulado = 0" ;
	$Sql .= " GROUP BY ca.car, ca.descripcion, c.creador,u.nombres";
	$Sql .= " ORDER BY ca.car";
	$ConsultaCar =$conexion->query($Sql);

	$array_prinicipal = array();
	foreach ($ConsultaCar->fetchAll() as $rowcar) :

		if(!array_key_exists($rowcar['car'], $array_prinicipal)):
			$array_prinicipal[$rowcar['car']] = array();
			$array_prinicipal[$rowcar['car']]['nombre'] = $rowcar['descripcion'];
		endif;

			$array_prinicipal[$rowcar['car']][] = array(
				'idusuario' => $rowcar['creador'],
				'nombres_usuario' => $rowcar['nombres'],
				'cantidad' => $rowcar['sum']
			);

	endforeach;

	$objReporte->SetFont('Arial','B',12);
	$objReporte->Cell(190,4,utf8_decode('RESUMEN DE CIERRE POR CAJERO'),0,1,'C');
	$objReporte->Ln(5);

	$objReporte->SetX(19);
	$objReporte->SetFont('Arial','B',9);
	$objReporte->Cell(61,4,utf8_decode('NOMBRE CAR'), 0,0,'L');
	$objReporte->Cell(95,4,utf8_decode('NOMBRE OPERADOR'), 0,0,'L');
	$objReporte->Cell(15,4,utf8_decode('MONTO'), 0,1,'R');
	$objReporte->line(20, 48, 195, 48, 0.5);
	$objReporte->ln(2);

	$cantida_total = 0;
	foreach($array_prinicipal as $indice_idcar => $datos) :

		$nombre_car = $datos['nombre'];
		unset($datos['nombre']);
		$contador = 0;
		$contador_altura = 0;
		$cantida_subtotal = 0;
		$punto_inicial_car = $objReporte->GetY();

		foreach($datos as $indice_correlativo => $usuarios_datos):

			$contador++;
			$contador_altura += 5;
			$objReporte->detalle_contenido($usuarios_datos['nombres_usuario'], $usuarios_datos['cantidad']);
			$cantida_subtotal +=$usuarios_datos['cantidad'];
		endforeach;

		$cantida_total += $cantida_subtotal;
		$objReporte->contenido($nombre_car, $contador_altura, $punto_inicial_car);
		$objReporte->SetFont('Arial','B',8);
		$objReporte->line(20, $objReporte->GetY(), 195, $objReporte->GetY(), 1);
		$objReporte->Cell(155,5,'Subtotal', 0, 0,'R');
		$objReporte->Cell(5,5,':',0,0,'R');
		$objReporte->Cell(20,5, number_format($cantida_subtotal,2),0,0,'R');
		$objReporte->Ln(5);

	endforeach;

	$objReporte->SetX(10);
	$objReporte->Cell(190,0.01,'',1,1,'L');
	$objReporte->SetFont('Arial','B',8);
	$objReporte->Cell(155,5,'MONTO TOTAL CAJEROS',0,0,'R');
	$objReporte->Cell(5,5,':',0,0,'R');
	$objReporte->Cell(20,5,number_format($cantida_total, 2),0,1,'R');
	$objReporte->Output();

?>
