<?php 
	
	$texto = $_GET['mestexto']."".date("Y-m-d_H:i:s");
	
	include("../../../../objetos/clsReporte.php");
	include("../../../../objetos/clsReporteExcel.php");
	
	header("Content-type: application/vnd.ms-excel; name='excel'");  
   	header("Content-Disposition: filename=FACTURACION_DE_{$texto}.xls"); 
   	header("Pragma: no-cache");  
   	header("Expires: 0");
  	
	$clsFunciones = new clsFunciones();
	$objReporte   = new clsReporte();
	
	$fhasta   = $_GET["fhasta"];
	$fdesde   = $_GET["fdesde"];
	$car      = $_GET["car"];
	$caj      = $_GET["caja"];
	$cajero   = $_GET["cajero"];
	$codsuc   = $_GET["codsuc"];
	$concepto = $_GET["concepto"];
	$condes   = $_GET["conceptodescripcion"];
	
	$fecha = str_replace("/","-",$fdesde) ." HASTA ". str_replace("/","-",$fhasta);
	
	if($car!="%"){$cars = $_GET["cars"];$Ccar=" AND cpg.car = ".$car;}else{$cars = "TODOS"; $Ccar="";}
	if($caj!="%"){$caja = $_GET["cajas"];$Ccaj=" and cpg.nrocaja = ".$caj;}else{$caja = "TODOS";$Ccaj='';}
	
	if($cajero=="%"){$cajero="";}else{$cajero=" and cpg.creador in(".$cajero.")";}
	if($concepto=="%"){$concepto=""; $condes="TODOS";}else{$concepto=" dpg.codconcepto = " . $_GET["concepto"] . " AND ";}
	$cajeros = "TODOS";
	$fhasta     = $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde     = $objReporte->CodFecha($_GET["fdesde"]);
	$cat        = 100;
	$tpago      = 100;
	$tdeuda     = 0;

	

	CabeceraExcel(2,8);
	// Para obtener el periodo de faturacion
	$sql =	"SELECT DISTINCT clie.nroinscripcion as inscripcion,
			UPPER(clie.propietario) as propietario,
			UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle) as direccion,
			(dpg.serie || '-' || dpg.nrodocumento) as comprobante,
			dfa.nrocuota as letra,
			dpg.importe as cobrado,
			cpg.fechareg as fechapago,
			dpg.anio as anio,
			".$clsFunciones->getCodCatastral("clie.").",
			dpg.mes as mes
			FROM cobranza.detpagos as dpg
			INNER JOIN cobranza.cabpagos AS cpg ON (dpg.codemp = cpg.codemp AND dpg.codsuc = cpg.codsuc AND dpg.nroinscripcion = cpg.nroinscripcion AND dpg.nropago = cpg.nropago)
			INNER JOIN facturacion.detfacturacion AS dfa ON (dpg.codemp = dfa.codemp AND dpg.codsuc = dfa.codsuc AND dpg.nrofacturacion = dfa.nrofacturacion AND dpg.nroinscripcion = dfa.nroinscripcion AND dpg.codconcepto = dfa.codconcepto)
			INNER JOIN catastro.clientes AS clie ON (dpg.codemp=clie.codemp AND dpg.codsuc=clie.codsuc AND dpg.nroinscripcion=clie.nroinscripcion)
			INNER JOIN public.calles AS cal ON(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona)
			INNER JOIN public.tiposcalle AS tipcal ON (cal.codtipocalle=tipcal.codtipocalle)
			WHERE " . $concepto . "cpg.fechareg BETWEEN '".$fdesde."' AND '".$fhasta."' 
			".$Ccar.$Ccaj.$cajero." AND cpg.anulado = 0
			ORDER BY clie.nroinscripcion ASC";

		
		//$objReporte->ContenidoTotal($TotalCuota,$TotalComi,$TotalCobra,$cantidad);

?>
<table class="ui-widget" border="0" cellspacing="0" id="TbTitulo" width="100%" rules="rows">
	<thead class="ui-widget-header" style="font-size:14px">
		<tr title="Cabecera">
			<th scope="col" colspan="10" align="center" >RECAUDACION DE CUOTAS DE <?=$condes?> - DEL <?=$fecha?></th>
		</tr>
	</thead>
</table>
<table>
	<thead>
		<tr>
			<th width="50" scope="col" class="ui-widget-header">PROYEC</th>
	        <th width="50" scope="col" class="ui-widget-header">SUMINISTRO</th>
	        <th width="300" scope="col" class="ui-widget-header">NOMBRE DEL USUARIO</th>
	        <th width="300" scope="col" class="ui-widget-header">DIRECCION</th>
	        <th width="50" scope="col" class="ui-widget-header">FACTURA</th>
	    	<th width="50" scope="col" class="ui-widget-header">LETRA</th>
	    	<th width="50" scope="col" class="ui-widget-header">CUOTA</th>
	    	<th width="50" scope="col" class="ui-widget-header">COMIS</th>
	    	<th width="50" scope="col" class="ui-widget-header">COBRADO</th>
	    	<th width="50" scope="col" class="ui-widget-header">FECHA DE PAGO</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$consultaC = $conexion->prepare($sql); 
		$consultaC->execute();
		$itemsC    = $consultaC->fetchAll();

		$TotalCuota = 0;
		$TotalComi  = 0;
		$TotalCobra = 0;
		$cantidad   = 0;

		foreach($itemsC as $rowC)
		{
			$cantidad++;
			$comision    =  0.50;
			$cuota       = $rowC["cobrado"] - $comision;
			$fact        = substr($clsFunciones->obtener_primer_dia_mes($rowC["mes"],$rowC["anio"]),0,-3);
			$codcatastro = str_replace('-','',substr($rowC['codcatastro'],3));
			//$objReporte->ContenidoDetalle($codcatastro, $rowC["propietario"], $rowC["direccion"], $fact, $rowC["letra"], $cuota, $comision, $rowC["cobrado"], $rowC["fechapago"]);	
			
			
			?>
			<tr>
					<td width="50" scope="col" >940111</td>
			        <td width="200" scope="col" style="mso-number-format:'\@'"><?=$codcatastro?></td>
			        <td width="30" scope="col" style="mso-number-format:'\@'"><?=trim((strtoupper($rowC["propietario"])))?></td>
			        <td width="50" scope="col" style="mso-number-format:'\@'"><?=(strtoupper($rowC["direccion"]));?></td>
			        <td width="50" scope="col" style="mso-number-format:'\@'"><?=(strtoupper($fact));?></td>
			        <td width="50" scope="col" align="center" ><?=$rowC["letra"];?></td>
			 		<td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($cuota,2)?></td>			 			
			 		<td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($comision,2)?></td>			 			
			 		<td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($rowC["cobrado"],2)?></td>			 			
			 		<td width="50" scope="col" align="center" ><?=$clsFunciones->CodFecha($rowC["fechapago"]);?></td>
				</tr>
			 	<?php
			 	$TotalCuota += $cuota; 
			$TotalComi  += $comision; 
			$TotalCobra += $rowC["cobrado"]; 

		}		
		
		?>
	</tbody>
	<tfoot class="ui-widget-header" style="font-size:14px">
        <tr>
        <td colspan="4" align="center" class="ui-widget-header">TOTAL : <?=$cantidad?> REGISTROS</td>
        <td align="center" colspan="2" class="ui-widget-header">S/.</td>
    	<td width="50" scope="col" align="right" class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($TotalCuota,2)?></td>			 			
    	<td width="50" scope="col" align="right" class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($TotalComi,2)?></td>		
    	<td width="50" scope="col" align="right" class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($TotalCobra,2)?></td>		
    	<td width="50" scope="col" align="right" class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"></td>		
        </tr>
    </tfoot>
</table>
	