<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "PLANILLAS EN TIEMPO REAL";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	
	$objDrop 	= new clsDrop();

?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_tiempo_real.js"></script>

<script>
jQuery(function($)
	{ 
		$("#fecha").mask("99/99/9999");
		$( "#DivTipos" ).buttonset();
	});
	var codsuc = <?=$codsuc?>
	
	function ValidarForm(Op)
	{
		var car    = "";
		var caj    = "";
		var cajero = "";
		
		if($("#todoscars").val()==1)
		{
			car="%";
		}else{
			car=$("#car").val();
			if(car==0)
			{
				alert("Seleccione el Car")
				return false
			}
		}

		if($("#todoscaja").val()==1)
		{
			caj="%";
		}else{
			caj=$("#caja").val();
			if(caj==0)
			{
				alert("Seleccione la Caja")
				return false
			}
		}
		
		var cad="";

		for(i=1;i<=$("#count").val();i++)
		{
			if($("#seleccion"+i).val()==1)
			{
				cad=cad + "," + $("#codusu"+i).val();
			}
		}

		if($("#idconceptos").val() == 1)
		{
			concep   = "%";
			concepto = "";
		}else{
			concep = $("#codconcepto").val();
			if(concep == 0)
			{
				alert("Seleccione el concepto")
				return false
			}else{
				concepto = $("#codconcepto option:selected").text();
			}
		}

		cajero=cad.substring(1,cad.length);

		if(cad==""){cajero="%";}
		

		var Data = "?fhasta="+$("#fhasta").val()+"&fdesde="+$("#fdesde").val()+"&car="+car
									+"&caja="+caj+"&cajero="+cajero+"&codsuc="+codsuc+"&cars="+$("#car option:selected").text()
									+"&cajas="+$("#caja option:selected").text()+"&concepto="+concep+"&conceptodescripcion="+concepto;

		if(document.getElementById("rabdetallado").checked==true)
		{
			url="imprimir_detallado.php";
		}		
		AbrirPopupImpresion(url+Data,800,600)
		
		return false
	}	
	function ValidarForm2(Op)
	{
		var car    = "";
		var caj    = "";
		var cajero = "";
		
		if($("#todoscars").val()==1)
		{
			car="%";
		}else{
			car=$("#car").val();
			if(car==0)
			{
				alert("Seleccione el Car")
				return false
			}
		}

		if($("#todoscaja").val()==1)
		{
			caj="%";
		}else{
			caj=$("#caja").val();
			if(caj==0)
			{
				alert("Seleccione la Caja")
				return false
			}
		}
		
		var cad="";

		for(i=1;i<=$("#count").val();i++)
		{
			if($("#seleccion"+i).val()==1)
			{
				cad=cad + "," + $("#codusu"+i).val();
			}
		}

		if($("#idconceptos").val() == 1)
		{
			concep   = "%";
			concepto = "";
		}else{
			concep = $("#codconcepto").val();
			if(concep == 0)
			{
				alert("Seleccione el concepto")
				return false
			}else{
				concepto = $("#codconcepto option:selected").text();
			}
		}

		cajero=cad.substring(1,cad.length);

		if(cad==""){cajero="%";}
		

		var Data = "?fhasta="+$("#fhasta").val()+"&fdesde="+$("#fdesde").val()+"&car="+car
									+"&caja="+caj+"&cajero="+cajero+"&codsuc="+codsuc+"&cars="+$("#car option:selected").text()
									+"&cajas="+$("#caja option:selected").text()+"&concepto="+concep+"&conceptodescripcion="+concepto;

		if(document.getElementById("rabdetallado").checked==true)
		{
			url="Excel.php";
		}		
		AbrirPopupImpresion(url+Data,800,600)
		
		return false
	}	
	function validar_concepto(obj,idx)
	{
		if(obj.checked)
		{
			$("#"+idx).val(1)
			$("#codconcepto").attr("disabled",true)
		}else{
			$("#"+idx).val(0)
			$("#codconcepto").attr("disabled",false)
		}
	}
	function Cancelar()
	{
		location.href='<?=$urldir?>/admin/indexB.php'
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td colspan="3"></td></tr>
				  <tr>
				    <td width="14%">Car</td>
				    <td width="3%" align="center">:</td>
				    <td colspan="4">
			          <? $objDrop->drop_car($codsuc); ?>
                      <input type="checkbox" name="chkcar" id="chkcar" checked="checked" onclick="CambiarEstado(this,'todoscars');quitar_disabled(this,'car')" />
		              Todos los Cars
                    </td>
			      </tr>
				  <tr>
				    <td>Caja</td>
				    <td align="center">:</td>
				    <td colspan="4">
						<? $objDrop->drop_cajas($codsuc,"onchange='ver_cajas_usuario(this.value);'"); ?>
				      	<input type="checkbox" name="chkcaja" id="chkcaja" checked="checked" onclick="CambiarEstado(this,'todoscaja');quitar_disabled(this,'caja');ver_cajas_usuario('%');" />
			        	Todas las Cajas 
                    </td>
			      </tr>
				  <tr style="padding-top:4px; padding-bottom:4px">
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td colspan="4">
						<div id="cajasxusu">
						</div>
					</td>
			      </tr>
				  <tr>
				  	<td>Concepto</td>
					<td align="center">:</td>
					<td colspan="4">
				    	<? $objDrop->drop_conceptos($codsuc,'','',1000); ?>
					    <input type="hidden" name="idconceptos" id="idconceptos" value="1">
						<input type="checkbox" checked="checked" name="chkconceptos" id="chkconceptos" value="checkbox" onclick="CambiarEstado(this,'idconceptos');validar_concepto(this,'idconceptos')" />
						<span>Todas los Conceptos</span>
					</td>

			      </tr>
				  <tr>
				    <td>Fecha Desde</td>
				    <td align="center">:</td>
				    <td colspan="4"><label>
				      <input type="text" name="fdesde" id="fdesde" class="inputtext" maxlength="20" value="<?=$objDrop->FechaServer()?>" style="width:80px;" />
				    </label></td>
			      </tr>
				  <tr>
				    <td>Fecha Hasta</td>
				    <td align="center">:</td>
				    <td colspan="4"><input type="text" name="fhasta" id="fhasta" class="inputtext" maxlength="20" value="<?=$objDrop->FechaServer()?>" style="width:80px;" /></td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td width="15%">
						<input type="hidden" name="todoscars" id="todoscars" value="1" />
						<input type="hidden" name="todoscaja" id="todoscaja" value="1" />
					</td>
				    <td width="30%" align="center">&nbsp;</td>
				    <td width="3%">&nbsp;</td>
				    <td width="35%">&nbsp;</td>
			      </tr>
			      <tr>
   					<td colspan="6" style="padding:4px;"  align="center">
					<div id="DivTipos" style="display:inline">
                        <input type="radio" name="rabresumen" id="rabdetallado" value="radio2" checked="checked" />
                        <label for="rabdetallado">Detallado</label>
					</div>	
					</td>
  					</tr>
					<tr><td colspan="6"></td></tr>
                    		<tr><td colspan='6' align="center"> 
                    			<input type="button" onclick="return ValidarForm();" value="Exportar" id="">
                    			<input type="button" onclick="return ValidarForm2();" value="Excel" id="">
                			</td></tr>
                        	<tr><td colspan="6"></td></tr>
				</table>
			</fieldset>
		</td>
	</tr>
	

	 </tbody>

    </table>
 </form>
</div>
<script>
	ver_cajas_usuario("%")
	
	$("#car").attr("disabled",true)
	$("#caja").attr("disabled",true)
	$("#codconcepto").attr("disabled",true)
</script>
<?php   CuerpoInferior();?>