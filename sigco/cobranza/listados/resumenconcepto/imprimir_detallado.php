<?php
	include("../../../../objetos/clsReporte.php");

    $clfunciones = new clsFunciones();

	class clsDetallado extends clsReporte
	{

		function Header() {

            global $codsuc,$meses;
			global $Dim,$fdesde;

			$periodo = $this->DecFechaLiteral($fdesde);
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            // $tit1 = "CRITICA DE LECTURAS";
            // $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera()
		{
			global $caja,$cars,$fecha,$cajeros,$condes,$Dim;

            $this->Ln(3);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','B',14);
			$tit1 = "RECAUDACION DE CUOTAS DE " . $condes . " - DEL " . $fecha;
			$this->Cell(277,$h+2,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);

			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(40, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"CAJA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,strtoupper($caja),0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(60, $h,$fecha,0,0,'L');
			$this->Cell(15, $h,"CAJERO",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(50, $h,$cajeros,0,1,'L');

			$this->Ln(1);
			$this->SetFont('Arial','',7);
			$this->Cell($Dim[0], $h,"PROYEC",1,0,'C',false);
			$this->Cell($Dim[1], $h,"SUMINISTRO",1,0,'C',false);
			$this->Cell($Dim[2], $h,"NOMBRE DEL USUARIO",1,0,'C',false);
			$this->Cell($Dim[3], $h,"DIRECCION",1,0,'C',false);
			$this->Cell($Dim[4], $h,"FACTURA",1,0,'C',false);
			$this->Cell($Dim[5], $h,"LETRA",1,0,'C',false);
			$this->Cell($Dim[6], $h,"CUOTA",1,0,'C',false);
			$this->Cell($Dim[7], $h,"COMIS",1,0,'C',false);
			$this->Cell($Dim[8], $h,"COBRADO",1,0,'C',false);
			$this->Cell($Dim[9], $h,"FECHA DE PAGO",1,1,'C',false);
		}

		function ContenidoDetalle($numeroinscripcion, $propietario, $direccion, $comprobante, $letra, $cuota, $comision, $cobrado, $fechapago)
		{
			global $Dim;
			$h = 7;

			$this->SetFont('Arial','',8);
			$this->SetTextColor(0,0,0);

			$this->Cell($Dim[0], $h,"940111",0,0,'C',false);
			$this->Cell($Dim[1], $h,$numeroinscripcion,0,0,'C',false);
			$this->Cell($Dim[2], $h,utf8_decode($propietario),0,0,'L',false);
			$this->Cell($Dim[3], $h,utf8_decode($direccion),0,0,'L',false);
			$this->Cell($Dim[4], $h,$comprobante,0,0,'C',false);
			$this->Cell($Dim[5], $h,$letra,0,0,'C',false);
			$this->Cell($Dim[6], $h,number_format($cuota,2),0,0,'R',false);
			$this->Cell($Dim[7], $h,number_format($comision,2),0,0,'R',false);
			$this->Cell($Dim[8], $h, number_format($cobrado,2),0,0,'R',false);
			$this->Cell($Dim[9], $h,$fechapago,0,1,'C',false);
		}

		function ContenidoTotal($totcuota, $totcomi, $totcobrar,$registros)
		{
			global $Dim;

			$h = 3;
			$this->SetFont('Arial','',8);
			$this->Cell( array_sum($Dim) ,$h,"__________________________________________________________________________________________________________________________________________________________________________________",0,1,'L',false);
			$h = 5;
			$this->SetFont('Arial','B',9);
			$this->Cell( (array_sum($Dim) - ($Dim[5] + $Dim[6] + $Dim[7] + $Dim[8] + $Dim[9]) ), $h,"TOTAL: " . $registros . " REGISTROS ",0,0,'R',false);
			$this->Cell($Dim[5], $h," S/. ",0,0,'C',false);
			$this->Cell($Dim[6], $h,number_format($totcuota,2),0,0,'R',false);
			$this->Cell($Dim[7], $h,number_format($totcomi,2),0,0,'R',false);
			$this->Cell($Dim[8], $h, number_format($totcobrar,2),0,1,'R',false);
		}

    }


    $Dim = array('0'=>20,'1'=>20,'2'=>55,'3'=>55,'4'=>25,'5'=>15,'6'=>22,'7'=>22,'8'=>22,'9'=>25);

	$fhasta   = $_GET["fhasta"];
	$fdesde   = $_GET["fdesde"];
	$car      = $_GET["car"];
	$caj      = $_GET["caja"];
	$cajero   = $_GET["cajero"];
	$codsuc   = $_GET["codsuc"];
	$concepto = $_GET["concepto"];
	$condes   = $_GET["conceptodescripcion"];

	$fecha = str_replace("/","-",$fdesde) ." HASTA ". str_replace("/","-",$fhasta);

	if($car!="%"){$cars = $_GET["cars"];$Ccar=" AND cpg.car = ".$car;}else{$cars = "TODOS"; $Ccar="";}
	if($caj!="%"){$caja = $_GET["cajas"];$Ccaj=" and cpg.nrocaja = ".$caj;}else{$caja = "TODOS";$Ccaj='';}

	if($cajero=="%"){$cajero="";}else{$cajero=" and cpg.creador in(".$cajero.")";}
	if($concepto=="%"){$concepto=""; $condes="TODOS";}else{$concepto=" dpg.codconcepto = " . $_GET["concepto"] . " AND ";}
	$cajeros = "TODOS";

	$objReporte =	new clsDetallado();
	$objReporte->AliasNbPages();
	$objReporte->AddPage("L");
	$fhasta     = $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde     = $objReporte->CodFecha($_GET["fdesde"]);
	$cat        = 100;
	$tpago      = 100;
	$tdeuda     = 0;

	$sql =	"SELECT DISTINCT clie.nroinscripcion as inscripcion,
			UPPER(clie.propietario) as propietario,
			UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
			(dpg.serie || '-' || dpg.nrodocumento) as comprobante,
			dfa.nrocuota as letra,
			dpg.importe as cobrado,
			cpg.fechareg as fechapago,
			dpg.anio as anio,
			".$clfunciones->getCodCatastral("clie.").",
			dpg.mes as mes
			FROM cobranza.detpagos as dpg
			JOIN cobranza.cabpagos AS cpg ON (dpg.codemp = cpg.codemp AND dpg.codsuc = cpg.codsuc AND dpg.nroinscripcion = cpg.nroinscripcion AND dpg.nropago = cpg.nropago)
			LEFT JOIN facturacion.detfacturacion AS dfa ON (dpg.codemp = dfa.codemp AND dpg.codsuc = dfa.codsuc AND dpg.nrofacturacion = dfa.nrofacturacion AND dpg.nroinscripcion = dfa.nroinscripcion AND dpg.codconcepto = dfa.codconcepto)
			JOIN catastro.clientes AS clie ON (dpg.codemp=clie.codemp AND dpg.codsuc=clie.codsuc AND dpg.nroinscripcion=clie.nroinscripcion)
			JOIN public.calles AS cal ON(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona)
			JOIN public.tiposcalle AS tipcal ON (cal.codtipocalle=tipcal.codtipocalle)
			WHERE " . $concepto . "cpg.fechareg BETWEEN '".$fdesde."' AND '".$fhasta."'
			".$Ccar.$Ccaj.$cajero." AND cpg.anulado = 0
			ORDER BY clie.nroinscripcion ASC";

	$consultaC = $conexion->prepare($sql);
	$consultaC->execute();
	$itemsC    = $consultaC->fetchAll();

	$TotalCuota = 0;
	$TotalComi  = 0;
	$TotalCobra = 0;
	$cantidad   = 0;

	foreach($itemsC as $rowC)
	{

		$rowC["letra"] = (empty($rowC["letra"])) ? 0 : $rowC["letra"];
		$cantidad++;
		$comision    =  0.50;
		$cuota       = $rowC["cobrado"] - $comision;
		$fact        = substr($clfunciones->obtener_primer_dia_mes($rowC["mes"],$rowC["anio"]),0,-3);
		$codcatastro = str_replace('-','',substr($rowC['codcatastro'],3));
		$objReporte->ContenidoDetalle($codcatastro, $rowC["propietario"], $rowC["direccion"], $fact, $rowC["letra"], $cuota, $comision, $rowC["cobrado"], $rowC["fechapago"]);

		$TotalCuota += $cuota;
		$TotalComi  += $comision;
		$TotalCobra += $rowC["cobrado"];

	}
	$objReporte->ContenidoTotal($TotalCuota,$TotalComi,$TotalCobra,$cantidad);
	$objReporte->Output();

?>
