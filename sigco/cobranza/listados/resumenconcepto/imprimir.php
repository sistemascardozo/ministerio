<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsTiempoReal extends clsReporte
	{
		function cabecera()
		{
			global $cars,$caja,$cajeros,$fecha;
                        
			$h=4;
			
			$this->Ln(10);
			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN DE PLANILLA ENTRADA CAJA";
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(50, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"CAJA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,strtoupper($caja),0,0,'L');
			$this->Cell(10, $h,"CAJERO",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,strtoupper($cajeros),0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(40, $h,$fecha,0,1,'L');
				
			$this->Ln(1);
			$this->Cell(75, $h,"CONCEPTO",1,0,'C');
			$this->Cell(20, $h,"NRO. ITEMS",1,0,'C');
			$this->Cell(20, $h,"CODCTA.",1,0,'C');
			$this->Cell(25, $h,"CANCELADO",1,0,'C');
			$this->Cell(25, $h,"A CUENTA",1,0,'C');
			$this->Cell(25, $h,"TOTAL",1,1,'C');
			//$this->Ln(3);
						
		}
		function ContenidoCab($concepto,$cancelado,$acuenta)
		{
			$h=4;
			
			$this->SetFont('Arial','B',7);
			$this->SetTextColor(230,0,0);
			
			$this->Cell(75, $h,$concepto,0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoDet($concepto,$cancelado,$acuenta,$ctacontable)
		{
			$h=4;
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(70, $h,$concepto,0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,$ctacontable,0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoFoot($cancelado,$acuenta)
		{
			$h=4;
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(70, $h,"",0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"TOTALES",0,0,'R');
			$this->Cell(25, $h,number_format($cancelado,2),'T',0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),'T',0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),'T',1,'R');
		}
    }

	$fhasta		= $_GET["fhasta"];
	$fdesde		= $_GET["fdesde"];
	$car		= $_GET["car"];
	$caj		= $_GET["caja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];
	
	$fecha = $fdesde ." HASTA ".$fhasta;
	
	if($car!="%"){$cars = $_GET["cars"];$Ccar=" AND car=".$car;}else{$cars = "TODOS"; $Ccar="";}
	if($caj!="%"){$caja = $_GET["cajas"];$Ccaj=" and nrocaja=".$caj;}else{$caja = "TODOS";$Ccaj='';}
	
	if($cajero=="%"){$cajero="";}else{$cajero=" and creador in(".$cajero.")";}	
	
	$cajeros = "TODOS";
        
	$objReporte	=	new clsTiempoReal();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
	$impcancelado	= 0;
	$impamortizado 	= 0;
	

	$sql = "select codcategoria,categoria_descripcion,SUM(impcancelado) AS impcancelado ,SUM(impamortizado) AS impamortizado
			from cobranza.f_cuadre_cab_cierre
			WHERE codsuc=:codsuc AND  fechareg between :fechainicio AND :fechafinal AND
			nropec=0 AND anulado=0 ".$Ccar.$Ccaj.$cajero."
			GROUP BY codcategoria,categoria_descripcion  ORDER BY codcategoria";
	//die($sql);
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":codsuc"=>$codsuc,
							 ":fechainicio"=>$fdesde,
							 ":fechafinal"=>$fhasta,
							 ));

	$items = $consulta->fetchAll();

	foreach($items as $row)
	{
		$objReporte->ContenidoCab(strtoupper($row["categoria_descripcion"]),$row["impcancelado"],$row["impamortizado"]);
		


		$sqlD = "select concepto,ctadebe,ordenrecibo,SUM(impcancelado) AS impcancelado ,SUM(impamortizado) AS impamortizado
				from cobranza.f_cuadre_det_cierre
				WHERE codsuc=:codsuc AND fechareg between :fechainicio AND :fechafinal
				AND nropec=0 AND anulado=0 ".$Ccar.$Ccaj.$cajero." AND codcategoria=:codcategoria
				GROUP BY concepto,ctadebe,ordenrecibo
				ORDER BY ordenrecibo";
		//die($sqlD);
		$impcancelado 	+= $row["impcancelado"];
		$impamortizado 	+= $row["impamortizado"];
		
		$consultaD = $conexion->prepare($sqlD);
		$consultaD->execute(array(":codsuc"=>$codsuc,
								  ":fechainicio"=>$fdesde,
								  ":fechafinal"=>$fhasta,
								  ":codcategoria"=>$row["codcategoria"]
								  ));
		$itemsD = $consultaD->fetchAll();
		
		foreach($itemsD as $rowD)
		{
			$objReporte->ContenidoDet(strtoupper($rowD["concepto"]),$rowD["impcancelado"],$rowD["impamortizado"],$rowD["ctadebe"]);
		}
	}
	$objReporte->ContenidoFoot($impcancelado,$impamortizado);
	$objReporte->Output();	
	
?>