<?php
	set_time_limit(0);
	
	include("../../../../objetos/clsReporte.php"); 
	
	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $m,$anio,$ciclotext,$Dim;

			$this->SetFont('Arial','B',14);
			$this->SetY(15);
			$tit1 = "REPORTE DE NOTAS DE PAGO";
			$this->Cell(277,$h+2,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(277,$h,$ciclotext,0,1,'C');
			
			$this->SetX(113.5);
			$this->Cell(10, $h,"MES",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$m,0,0,'L');
			$this->Cell(10, $h,utf8_decode("AÑO"),0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$anio,0,1,'L');
			
			$this->Ln(2);
			$this->Ln(1);
			$this->SetFont('Arial','',6);
			

			$this->Cell($Dim[1],$h,"NRO. DCTO.",'1',0,'C');
			$this->Cell($Dim[2],$h,"FECHA",'1',0,'C');
			$this->Cell($Dim[3],$h,"INSCRIPCION",'1',0,'C');
			$this->Cell($Dim[4],$h,"NOMBRE",'1',0,'C');
			$this->Cell($Dim[5],$h,"IMPORTE",'1',1,'C');

			
			
						
		}
		function agregar_detalle($codsuc,$codciclo,$anio,$mes,$Csec)
		{
			global $conexion,$Dim; 
			if(intval($mes)<10) $mes="0".$mes;
			
			$h=4;
			$ultimodia = $this->obtener_ultimo_dia_mes($mes,$anio);
			$primerdia = $this->obtener_primer_dia_mes($mes,$anio);
			$count = 0;
			
			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;
			
			$this->SetTextColor(0,0,0);
			$sql = "select nrofacturacion,fechaemision
			from facturacion.periodofacturacion
			where codemp=1 and codsuc=? and codciclo=? AND anio=? AND mes=?";
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$codciclo,$anio,$mes));
			$row = $consulta->fetch();
			$nrofacturacion=$row['nrofacturacion'];
			

			
				//REBAJAS
				$Sql="SELECT 
					c.nroinscripcion,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.propietario  END as propietario
					/*c.propietario as propietario*/,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.direccion  END as direccion
					/*c.direccion as direccion*/,
					'' as codcatastro,
					to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')) as codantiguo,c.nroprepago,
					SUM(d.importe) AS impmes,d.serie,d.nrodocumento,c.fechareg,d.detalle,c.estado
					FROM  cobranza.cabprepagos c
					INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
					AND (c.nroinscripcion = d.nroinscripcion)
					INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
					AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
					WHERE c.codsuc = {$codsuc} AND c.estado<>3 
				  	/*AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'*/
				  	 AND d.anio='".$anio."' 	  AND d.mes='".$mes."'
					AND d.coddocumento IN (16) 	AND co.codconcepto <> 10005
					GROUP BY c.nroinscripcion, c.propietario,c.direccion,c.codsuc,
					c.nroprepago,d.serie,d.nrodocumento,c.fechareg,d.detalle,c.estado
					ORDER BY c.fechareg";
			
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			$borde=0;
			$tvVenta=0;
			$tnImponible=0;
			$tigv=0;
			$tredondeo=0;
			//var_dump($consulta->errorInfo());
			$this->SetFillColor(255,255,255); //Color de Fondo
		$this->SetTextColor(0);
		$cAnuldados=0;
		$tPagados=0;
		$tPendientes=0;
		$tAnulados=0;
		$cPagados=0;
		$cPendientes=0;
			foreach($items as $row)
			{
				$count++;
				if($row['estado']==0)
				{
					$tAnulados+=$row['impmes'];
					$row['noimp']=0;
					$row['impigv']=0;
					$row['redondeo']=0;
					$row['impmes']=0;
					$row["detalle"] = $row["detalle"]." ANULADO";
					$cAnuldados++;
				}
				if($row['estado']==2)
				{
					$tPagados+=$row['impmes'];
					$cPagados++;
				}
				if($row['estado']==1)
				{
					$tPendientes+=$row['impmes'];
					$cPendientes++;
				}
				$this->Cell($Dim[1],$h,'N '.$row["serie"].' '.$row["nrodocumento"],$borde,0,'C',true);
				$this->Cell($Dim[2],$h,$this->DecFecha($row["fechareg"]),$borde,0,'C',true);
				$this->Cell($Dim[3],$h,strtoupper($row["codantiguo"]),$borde,0,'C',true);
				$this->Cell($Dim[4],$h,strtoupper($row["propietario"]).'==>('.strtoupper($row["detalle"]).')',$borde,0,'L',true);
				$this->Cell($Dim[5],$h,number_format($row['impmes'],2),$borde,0,'R',true);
				
				$tvVenta+=0;
				$tnImponible+=$row["noimp"];
				$tigv+=$row["impigv"];
				$tredondeo+=$row["redondeo"];
				$imptotal+=$row["impmes"];
				$this->Ln($h);

			}
			$this->AddPage("L");
			$this->Ln(3);
			$h=5;
			$this->SetFont('Arial','B',10);
			$this->Cell(0 ,$h,"RESUMEN DE NOTAS DE PAGO",0,1,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',8);
			$this->SetX(130);
			$this->Cell(30,$h,"IMPORTE",'B',0,'C');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,"CANTIDAD",'B',1,'C');
			//TOTALE
			$this->Ln(3);
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"ANULADAS",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tAnulados,2),0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$cAnuldados,0,1,'R');
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"PENDIENTES",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tPendientes,2),0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$cPendientes,0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"PAGADAS",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tPagados,2),0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$cPagados,0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"EMITIDAS",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($tPagados+$tPendientes+$tAnulados,2),'T',0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$count,'T',1,'R');


			
			
				
		}
			
		function FechaDoc($cod,$serie,$numero)
		{
			global $conexion,$codsuc;
			$Sql ="SELECT fechareg FROM facturacion.cabfacturacion
					WHERE codsuc=".$codsuc." AND coddocumento=".$cod."
					AND serie='".$serie."' AND nrodocumento='".$numero."'";
			$c = $conexion->query($Sql)->fetch();
			return $this->DecFecha($c[0]);
		}
		function EquivalenteDoc($cod,$tipo)
		{
			if($tipo==1)
			{
				switch ($cod) 
				{
					case 4: return "14";break;
					case 13: return "03";break;
					case 14: return "01";break;
					case 17: return "07";break;
				}
			}
			else
			{
				switch ($cod) 
				{
					case 1: return "1";break;
					case 3: return "6";break;
					default: return "0";break;
				}
			}
			/*
			COMPROBANTES
			-----------------------------
			14  Recibos de Pensiones
			01  Facturas
			03  Boletas de Ventas
			07  Notas de Abono/Credito
			08  Notas de cargo/Debito


			DOCUMENTOS
			-----------------------------
			1  DNI
			6  RUC
			0  Otros*/
		}

    }
	
	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc		= $_GET["codsuc"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclotext	= $_GET["ciclotext"];
	if($_GET["sector"]!="%"){$Csec=" AND cl.codsector=".$_GET["sector"];}else{$Csec="";}
	$m = $meses[$mes];
	$Dim = array('1'=>40,'2'=>25,'3'=>50,'4'=>100,'5'=>25,'6'=>20,'7'=>10,'8'=>20,'9'=>20);	
/*	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$caja		= $_GET["caja"];
	$cars		= $_GET["cars"];
	$cajeros 	= $_SESSION['nombre'];
	$nrocaja	= $_GET["nrocaja"];	
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$totagua 		= 0;
	$totdesague		= 0;
	$totcargofijo	= 0;
	$totinteres		= 0;
	$totredondeo	= 0;
	$tototros		= 0;
	$totigv			= 0;
	$totalT			= 0;*/
			
	$objReporte	=	new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage("L");
	
	$objReporte->agregar_detalle($codsuc,$ciclo,$anio,$mes,$Csec);

	
	$objReporte->Output();	
	
?>