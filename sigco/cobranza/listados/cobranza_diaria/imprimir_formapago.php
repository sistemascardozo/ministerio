<?php
session_name("pnsu");
if(!session_start()){session_start();}

	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$cars,$caja;

			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN DE PLANILLA ENTRADA CAJA POR FORMA DE PAGO";
			$this->Cell(190,$h+3,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);
			$this->Cell(190,0.01,'',1,1,'L');
			  $this->Ln(2);
			$h=4;
			$this->SetFont('Arial', 'B', 7);
			$this->Cell(7, $h, "CAR", 0, 0, 'L');
			$this->Cell(4, $h, ":", 0, 0, 'C');
			$this->SetFont('Arial', '', 7);
			$this->Cell(30, $h, $cars, 0, 0, 'L');
			$this->SetFont('Arial', 'B', 7);
			$this->Cell(8, $h, "CAJA", 0, 0, 'L');
			$this->Cell(4, $h, ":", 0, 0, 'C');
			$this->SetFont('Arial', '', 7);
			$this->Cell(30, $h, strtoupper($caja), 0, 0, 'L');
			$this->SetFont('Arial', 'B', 7);
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->SetFont('Arial', '', 7);
			$this->Cell(40, $h,$fecha,0,0,'L');
			$this->SetFont('Arial', 'B', 7);
			$this->Cell(15, $h,"CAJERO",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->SetFont('Arial', '', 7);
			$this->Cell(50, $h,$cajeros,0,1,'L');
			$this->Cell(190,0.01,'',1,1,'L');


		}
		function resumen($codsuc,$Ccar,$fdesde,$fhasta,$cajero,$Ccaj)
		{
			global $conexion;
			$Sql ="select fp.descripcion,SUM(c.imptotal)
			from cobranza.cabpagos c
			INNER JOIN public.formapago fp ON ( fp.codformapago=c.codformapago)
			where c.codsuc=".$codsuc." and c.nropec=0  ".$Ccar."
			 and  c.fechareg between '".$fdesde."' AND '".$fhasta."'
			 ".$Ccaj." ".$cajero." AND c.anulado=0
			GROUP BY fp.descripcion";
			$this->Ln(5);
			$this->SetFont('Arial','B',12);
			$tit1 = "RESUMEN";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'C');
			$this->Ln(2);
			$Consulta =$conexion->query($Sql);
			$Total=0;
			$this->SetFont('Arial','',9);
			foreach($Consulta->fetchAll() as $row)
			{
				$this->SetX(70);
				$this->Cell(30,4,strtoupper($row[0]),0,0,'C',false);
				$this->Cell(5,4,':',0,0,'C',false);
				$this->Cell(20,4,number_format($row[1],2),0,1,'R',false);
				$Total+=$row[1];
			}
			$this->Ln(2);
			$this->SetX(105);
			$this->Cell(20,0.01,'',1,1,'R',false);
			$this->Ln(2);
			$this->SetX(70);
			$this->Cell(30,4,'TOTAL',0,0,'C',false);
			$this->Cell(5,4,':',0,0,'C',false);
			$this->Cell(20,4,number_format($Total,2),0,1,'R',false);
			$this->Ln(5);

		}
		function cabdetellado()
		{
			$this->Ln(5);
			$this->SetFont('Arial','B',12);
			$tit1 = "DETALLADO";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'C');
			$h=4;
			$this->Ln(1);
			$this->SetFont('Arial','',7);
			$this->Cell(15, $h,"Nro. Pago",1,0,'C');
			$this->Cell(15, $h,"Codigo",1,0,'C');
			$this->Cell(65, $h,"Cliente",1,0,'C');
			$this->Cell(15, $h,"Doc",1,0,'C');
			$this->Cell(15, $h,"Serie",1,0,'C');
			$this->Cell(28, $h,"Nro. Doc.",1,0,'C');
			$this->Cell(12, $h,"Total",1,0,'C');
			$this->Cell(20, $h,"Efectivo",1,0,'C');
			$this->Cell(5, $h,"A",1,1,'C');
		}
		function contenido($nropago,$codigo,$usuario,$doc,$serie,$nrodoc,$total,$efectivo,$anulado)
		{
			global $codsuc;
			$codigo = $this->CodUsuario($codsuc,$codigo);
			$h=4;

			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);

			$this->Cell(15, $h,$nropago,0,0,'C');
			$this->Cell(15, $h,$codigo,0,0,'C');
			$this->Cell(65, $h,utf8_decode($usuario),0,0,'L');
			$this->Cell(15, $h,$doc,0,0,'C');
			$this->Cell(15, $h,$serie,0,0,'C');
			$this->Cell(28, $h,$nrodoc,0,0,'C');
			$this->Cell(12, $h,$total,0,0,'R');
			$this->Cell(20, $h,$efectivo,0,0,'R');
			$this->Cell(5, $h,$anulado,0,1,'C');

		}
		// function contenidofoot($cont_normal,$cont_anulado,$tot_procesado,$total_anulado)
		function contenidofoot($cont_normal,$cont_anulado,$cont_tick,$tot_procesado,$total_anulado,$total_ticket_pro)
		{
			$h=4;

			$this->Ln(5);

			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Cobrados",0,0,'R');
			$this->Cell(15, $h,$cont_normal,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Procesado",0,0,'R');
			$this->Cell(20, $h,$tot_procesado,0,1,'R');

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Total de Ticket",0,0,'R');
			$this->Cell(15, $h,$cont_tick,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Procesado Ticket",0,0,'R');
			$this->Cell(20, $h,$total_ticket_pro,0,1,'R');

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Anulado",0,0,'R');
			$this->Cell(15, $h,$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Menos Total Anulado",0,0,'R');
			$this->Cell(20, $h,$total_anulado,0,1,'R');

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Total de Talones",0,0,'R');
			$this->Cell(15, $h,$cont_normal+$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Cobrado",0,0,'R');
			$tot_procesado= str_replace(',', '', $tot_procesado);
        	$total_ticket_pro= str_replace(',', '', $total_ticket_pro);
			$this->Cell(20, $h,number_format($tot_procesado+$total_ticket_pro,2),0,1,'R');
			}
    }

	$fhasta		= $_GET["fhasta"];
	$fdesde		= $_GET["fdesde"];
	$car		= $_GET["car"];
	$caj		= $_GET["caja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$fecha = $fdesde ." HASTA ".$fhasta;

	if($car!="%"){$cars = $_GET["cars"];$Ccar=" AND car=".$car;}else{$cars = "TODOS"; $Ccar="";}
	if($caj!="%"){$caja = $_GET["cajas"];$Ccaj=" and nrocaja=".$caj;}else{$caja = "TODOS";$Ccaj='';}

	if($cajero=="%"){$cajero="";}else{$cajero=" and creador in(".$cajero.")";}

	$cajeros = "TODOS";
	$objReporte	=	new clsTalones();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);
	$objReporte->resumen($codsuc,$Ccar,$fdesde,$fhasta,$cajero,$Ccaj);
	$objReporte->cabdetellado();
	$Sql ="select fp.codformapago, fp.descripcion
			from cobranza.cabpagos c
			INNER JOIN public.formapago fp ON ( fp.codformapago=c.codformapago)
			where c.codsuc=".$codsuc." and c.nropec=0
			 and  c.fechareg between '".$fdesde."' AND '".$fhasta."'
			".$Ccar.$Ccaj.$cajero." AND c.anulado=0
			GROUP BY fp.codformapago, fp.descripcion";
	$Consulta =$conexion->query($Sql);
	$tot_procesados=0;
		$total_anulado=0;
	foreach($Consulta->fetchAll() as $row)
	{	$h=4;
		$objReporte->Ln(2);
		$objReporte->SetFont('Arial','B',9);
		$objReporte->SetTextColor(0,0,0);
		$objReporte->Cell(15, $h,strtoupper($row[1]),0,1,'L');
		$objReporte->Cell(15,0.01,'',1,1,'L');
		$objReporte->Ln(2);
		$sqlC = " select nropago,nroinscripcion,propietario,imppagado,imptotal,anulado, ticket_serie, ticket_numero ";
		$sqlC .= " from cobranza.cabpagos
				 where codsuc=".$codsuc." and nropec=0
				 and fechareg between '".$fdesde."' AND '".$fhasta."' ".$Ccar.$Ccaj.$cajero." AND codformapago=?";

		$consultaC = $conexion->prepare($sqlC);
		$consultaC->execute(array($row[0]));
		$itemsC = $consultaC->fetchAll();

		$cont_normal=0;
		$cont_anulado=0;
		$cont_ticket=0;
		$total_ticket_procesados=0;

		foreach($itemsC as $rowC)
		{
			$sqlD = "select nrodocumento, coddocumento from cobranza.detpagos where codemp=1 and codsuc=? and nroinscripcion=? and
							nropago=? and nrodocumento<>0 group by nrodocumento, coddocumento";

			$consultaD = $conexion->prepare($sqlD);
			$consultaD->execute(array($codsuc,$rowC["nroinscripcion"],$rowC["nropago"]));
			$itemsD = $consultaD->fetch();

			if($rowC["anulado"]==1)
			{
				$x				= "x";
				$cont_anulado++;
				$total_anulado += $rowC["imptotal"];
			}else{
				$x				= "";

				if($rowC['ticket_numero'] != 0):
					$cont_ticket++;
					$total_ticket_procesados += $rowC["imptotal"];
				else:
					$cont_normal++;
					$tot_procesados += $rowC["imptotal"];
				endif;

			}

			// Para las ticketeras
			if($rowC['ticket_numero'] != 0):
				$itemsD["nrodocumento"] = $itemsD["nrodocumento"] . " - TIK - " . $rowC['ticket_numero'];
			endif;

			$objReporte->contenido($rowC["nropago"],$rowC["nroinscripcion"],$rowC["propietario"],4,'0001',$itemsD["nrodocumento"],
							number_format($rowC["imptotal"],2),number_format($rowC["imppagado"],2),$x);

		}
	}
	// $objReporte->contenidofoot($cont_normal,$cont_anulado,number_format($tot_procesados,2),number_format($total_anulado,2));
	$objReporte->contenidofoot($cont_normal,$cont_anulado,$cont_ticket,number_format($tot_procesados,2),number_format($total_anulado,2),number_format($total_ticket_procesados,2));

	$objReporte->Output();

?>
