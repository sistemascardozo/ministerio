<?php
	set_time_limit(0);

	include("../../../../objetos/clsReporte.php");

	// ini_set("display_errors",1);
	// error_reporting(E_ALL);

	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $fdesde,$fhasta,$ciclotext,$Dim;

			$this->SetFont('Arial','B',14);
			$this->SetY(15);
			$tit1 = "REPORTE DE COBRANZA POR CONCEPTOS";
			$this->Cell(190,$h + 5,utf8_decode($tit1),0,1,'C');
      $this->Ln(1);

			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(190,$h,$ciclotext,0,1,'C');

			$this->SetX(70.5);
			$this->Cell(10, $h,"Desde",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$fdesde,0,0,'L');
			$this->Cell(10, $h,"Hasta",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$fhasta,0,1,'L');
			$this->Ln(3);

			$this->SetFillColor(255, 255, 255);
			$this->RoundedRect(10, 30, 180, 8, 0.5, 'DF');

			$this->SetFont('Arial','',6);
			$this->Cell($Dim[0],$h,"CONCEPTO",0,0,'C');
			$this->Cell($Dim[1],$h,"DENOMINACION",0,0,'C');
			$this->Cell($Dim[2],$h,"IMPORTE",0,1,'R');

			$this->Ln(3);

		}

		function agregar_detalle($codsuc,$codciclo,$fdesde,$fhasta,$Csec)
		{
				global $conexion,$Dim;
				// if(intval($mes)<10) $mes="0".$mes;

				$h=4;
				$primerdia = $fdesde;
				$ultimodia = $fhasta;
				$count = 0;

				$impmes 	= 0;
				$impigv		= 0;
				$imptotal	= 0;

				// REBAJAS
				$Sql = " SELECT DISTINCT
										d.codconcepto as codconcepto,
										co.descripcion as concepto,
				    				SUM(d.importe) as total
									FROM cobranza.cabpagos c
										JOIN cobranza.detpagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion)
										JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
									WHERE c.codsuc = {$codsuc} AND c.anulado = 0 AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
									GROUP BY d.codconcepto, co.descripcion
									ORDER BY d.codconcepto ";

				$consulta    = $conexion->prepare($Sql);
				$consulta->execute();
				$items       = $consulta->fetchAll();
				$subtotal       = 0;

				$this->SetFont('Arial','',9);
				foreach($items as $row) :

					$this->Cell($Dim[0],$h,$row["codconcepto"],0,0,'C');
					$this->Cell($Dim[1],$h,utf8_decode(strtoupper($row["concepto"])),0,0,'L');
					$this->Cell($Dim[2],$h,number_format($row['total'],2),0,1,'R');
					$subtotal += $row['total'];

				endforeach;

				$this->Ln(3);
				$this->Line(10,$this->GetY(),190,$this->GetY(),3);
				$this->Ln(1);
				$this->SetFont('Arial','B',9);
				$this->Cell($Dim[0]+$Dim[1], $h,"Total General ==>",0,0,'R');
				$this->Cell($Dim[2], $h,number_format($subtotal,2),0,1,'R');

			}

		//  Para los cuadros con bordes redondeados o sin ellos
		function RoundedRect($x, $y, $w, $h, $r, $style = '') {
				$k = $this->k;
				$hp = $this->h;
				if($style=='F')
					$op='f';
				elseif($style=='FD' || $style=='DF')
					$op='B';
				else
					$op='S';
				$MyArc = 4/3 * (sqrt(2) - 1);
				$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
				$xc = $x+$w-$r ;
				$yc = $y+$r;
				$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

				$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
				$xc = $x+$w-$r ;
				$yc = $y+$h-$r;
				$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
				$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
				$xc = $x+$r ;
				$yc = $y+$h-$r;
				$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
				$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
				$xc = $x+$r ;
				$yc = $y+$r;
				$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
				$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
				$this->_out($op);
			}

		function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
			 $h = $this->h;
			 $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
			 $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
			}

  }

	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc    = $_GET["codsuc"];
	$fhasta       = $_GET["fhasta"];
	$fdesde       = $_GET["fdesde"];
	$ciclotext = $_GET["ciclotext"];
	if($_GET["sector"]!="%"){$Csec=" AND cl.codsector=".$_GET["sector"];}else{$Csec="";}
	$m = $meses[$mes];
	$Dim = array('0'=>20,'1'=>100,'2'=>40,'3'=>20);
	$objReporte	=	new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->agregar_detalle($codsuc,$ciclo,$fdesde,$fhasta,$Csec);
	$objReporte->Output();

?>
