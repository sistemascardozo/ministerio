<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "RESUMEN DE COBRANZA DIARIA";

	$Activo = 1;

	CuerpoSuperior($TituloVentana);

	$codsuc 	= $_SESSION['IdSucursal'];
	$codusu		= $_SESSION['id_user'];

	$objDrop 	= new clsDrop();

	$sql = "SELECT c.nrocaja, cj.descripcion
			FROM cobranza.cajasxusuario as c
			INNER JOIN cobranza.cajas cj ON(c.nrocaja = cj.nrocaja)
			WHERE c.codsuc = ".$codsuc." AND c.codusu = ".$codusu." ";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array());

	$items = $consulta->fetch();

	$Caja = $items[0];

	$car = $_SESSION["car"];
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_tiempo_real.js"></script>
<script>
	jQuery(function($){
		$("#fecha").mask("99/99/9999");
		$( "#DivTipos" ).buttonset();

		$('#car').val(<?php echo $car;?>);

		if (<?php echo $car;?> != 9)
		{
			$("#todoscars").val(0);
			$('#chkcar').attr("checked", false)
			$('#chkcaja').attr("checked", false)

			$("#todoscaja").val(0);
			//$('#chkcaja').attr("checked", false)

			$('#car').attr("disabled", true);
			$('#chkcar').attr("disabled", true);
			$('#chkcaja').attr("disabled", true);

			$('#caja').val(<?php echo $Caja;?>);
			ver_cajas_usuario();
		}
	});

	var codsuc = <?=$codsuc?>

	function ValidarForm(Op)
	{
		var car="";
		var caj="";
		var cajero="";

		if($("#todoscars").val()==1)
		{
			car="%";
		}else{
			car=$("#car").val();
			if(car==0)
			{
				alert("Seleccione el Car")
				return false
			}
		}
		if($("#todoscaja").val()==1)
		{
			caj="%";
		}else{
			caj=$("#caja").val();
			if(caj==0)
			{
				alert("Seleccione la Caja")
				return false
			}
		}

		var cad="";

		for(i=1;i<=$("#count").val();i++)
		{
			if($("#seleccion"+i).val()==1)
			{
				cad=cad + "," + $("#codusu"+i).val();
			}
		}
		cajero=cad.substring(1,cad.length);

		if(cad==""){cajero="%";}


		var Data = "?fhasta=" + $("#fhasta").val() + "&fdesde=" + $("#fdesde").val() + "&car=" + car
									+ "&caja=" + caj + "&cajero=" + cajero + "&codsuc=" + codsuc + "&cars=" + $("#car option:selected").text()
									+ "&cajas=" + $("#caja option:selected").text();

		if($("#raeconsolidado").is(':checked')==true)
		{
			url="imprimir_consolidado.php";
		}
		if($("#rabresumen").is(':checked')==true)
		{
			url="imprimir.php";
		}
		AbrirPopupImpresion(url+Data,800,600)

		return false
	}
	function Cancelar()
	{
		location.href='<?=$urldir?>/admin/indexB.php'
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td colspan="3"></td></tr>
				  <tr>
				    <td width="14%">Car</td>
				    <td width="3%" align="center">:</td>
				    <td colspan="4">
			          <? $objDrop->drop_car($codsuc); ?>
                      <input type="checkbox" name="chkcar" id="chkcar" onclick="CambiarEstado(this, 'todoscars'); quitar_disabled(this, 'car')" />
		              Todos los Cars
                    </td>
			      </tr>
				  <tr>
				    <td>Caja</td>
				    <td align="center">:</td>
				    <td colspan="4">
						<? $objDrop->drop_cajas($codsuc, "onchange='ver_cajas_usuario(this.value);'"); ?>
				      	<input type="checkbox" name="chkcaja" id="chkcaja" checked="checked" onclick="CambiarEstado(this, 'todoscaja'); quitar_disabled(this, 'caja'); ver_cajas_usuario();" />
			        	Todas las Cajas
                    </td>
			      </tr>
				  <tr style="padding-top:4px; padding-bottom:4px">
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td colspan="4">
						<div id="cajasxusu">
						</div>
					</td>
			      </tr>
				  <tr>
				    <td>Fecha Desde</td>
				    <td align="center">:</td>
				    <td colspan="4"><label>
				      <input type="text" name="fdesde" id="fdesde" class="inputtext" maxlength="20" value="<?=$objDrop->FechaServer()?>" style="width:80px;" />
				    </label></td>
			      </tr>
				  <tr>
				    <td>Fecha Hasta</td>
				    <td align="center">:</td>
				    <td colspan="4"><input type="text" name="fhasta" id="fhasta" class="inputtext" maxlength="20" value="<?=$objDrop->FechaServer()?>" style="width:80px;" /></td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td width="15%">
						<input type="hidden" name="todoscars" id="todoscars" value="0" />
						<input type="hidden" name="todoscaja" id="todoscaja" value="1" />
					</td>
				    <td width="30%" align="center">&nbsp;</td>
				    <td width="3%">&nbsp;</td>
				    <td width="35%">&nbsp;</td>
			      </tr>
			      <tr>
   					<td colspan="6" style="padding:4px;"  align="center">
					<div id="DivTipos" style="display:inline">
						<input type="radio" name="rabresumen" id="raeconsolidado" value="radio5" checked="checked" />
						<label for="raeconsolidado">Consolidado</label>
						<input type="radio" name="rabresumen" id="rabresumen" value="radio" />
						<label for="rabresumen">Resumen</label>
					</div>
					</td>
  					</tr>
					<tr><td colspan="6"></td></tr>
                    		<tr><td colspan='6' align="center"> <input type="button" onclick="return ValidarForm();" value="Exportar" id=""></td></tr>
                        	<tr><td colspan="6"></td></tr>
				</table>
			</fieldset>
		</td>
	</tr>


	 </tbody>

    </table>
 </form>
</div>
<script>
	ver_cajas_usuario("%")

	//$("#car").attr("disabled",true)
	$("#caja").attr("disabled",true)
</script>
<?php   CuerpoInferior();?>
