<?php
session_name("pnsu");
if(!session_start()){session_start();}

	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$dim;
			$this->SetY(10);
			$this->SetFont('Arial','B',8);
			$tit1 = "";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'R');
			$this->SetFont('Arial','B',9);
			$tit1 = "RESUMEN COBRANZA ".$fecha;
			$this->Cell(65,$h+3,utf8_decode(""),0,0,'C');
			$this->Cell(100,$h+3,utf8_decode($tit1),0,0,'L');
			$this->Cell(25,$h+3,utf8_decode(""),0,1,'C');
      $this->Ln(5);

			$h = 6;
			$this->Cell($dim[1],$h,utf8_decode("FECHA"),1,0,'C');
			$this->Cell($dim[2],$h,utf8_decode("CAR"),1,0,'C');
			$this->Cell($dim[3],$h,utf8_decode("RECIBOS"),1,0,'C');
			$this->Cell($dim[4],$h,utf8_decode("BOL Ó FACT"),1,0,'C');
			$this->Cell($dim[5],$h,utf8_decode("NOTA"),1,0,'C');
			$this->Cell($dim[6],$h,utf8_decode("TOTAL DIARIA"),1,1,'C');
			$h=4;

		}

		function ContenidoCabecera($fecha, $cantidad)
		{

			global $dim;

			$h=(4*$cantidad+1);
			$this->SetFont('Arial','',8);
			$this->SetTextColor(0,0,0);
			$this->Cell($dim[1], $h,$fecha,0,0,'C');
		}


		function Contenido($cars, $recibo, $factura_o_boleta, $nota, $total, $contador)
		{

			global $dim;

			$h=4;
			$this->SetFont('Arial','',8);
			$this->SetTextColor(0,0,0);
			if($contador == 1):
				$this->SetXY($this->GetX(), $this->GetY());
			else:
				$this->SetXY($this->GetX()+30, $this->GetY());
			endif;
			$this->Cell($dim[2], $h,$cars, 0,0,'L');
			$this->Cell($dim[3], $h,$recibo, 0,0,'C');
			$this->Cell($dim[4], $h,$factura_o_boleta, 0,0,'C');
			$this->Cell($dim[5], $h,$nota, 0,0,'C');
			$this->Cell($dim[6], $h,number_format($total,2), 0,0,'R');
		}

		function pie($trecibo, $tfactura_o_boleta, $tnota, $ttotal)
		{

			global $dim;

			$h=4;
			$this->Ln(2);
			$this->Cell(0, 0.2,"",1,1,'R');
			$this->SetFont('Arial','B',9);
			$this->SetTextColor(0,0,0);
			$this->Cell(($dim[1]+$dim[2]), $h,"TOTAL ====>",0,0,'C');
			$this->Cell($dim[3], $h,$trecibo,0,0,'C');
			$this->Cell($dim[4], $h,$tfactura_o_boleta,0,0,'C');
			$this->Cell($dim[5], $h,$tnota,0,0,'C');
			$this->Cell($dim[6], $h,number_format($ttotal,2),0,1,'R');
			$this->Ln(2);
		}


  }

	$dim = array(1=>30, 2=>60, 3=>20, 4=>25, 5=>25, 6=>30);

	$fhasta = $_GET["fhasta"];
	$fdesde = $_GET["fdesde"];
	$car    = $_GET["car"];
	$caj    = $_GET["caja"];
	$cajero = $_GET["cajero"];
	$codsuc = $_GET["codsuc"];
	$fecha  = $fdesde ." HASTA ".$fhasta;


	$objReporte	=	new clsTalones();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);

	$array_general = array();
	$array_car = array();


$Sql =" SELECT
						c.fechareg,
						c.nropago,
						d.coddocumento,
						c.car,
						ca.descripcion AS cars,
						SUM(d.importe) AS impmes
					FROM cobranza.cabpagos c
						JOIN cobranza.detpagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion)
						JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
						JOIN cobranza.car ca ON (ca.codemp = c.codemp) AND (ca.codsuc = c.codsuc) AND (ca.car = c.car)
					WHERE c.codsuc = " . $codsuc . " AND c.anulado = 0
						AND c.fechareg BETWEEN '" . $fdesde . "' AND '" . $fhasta . "'
						AND d.coddocumento IN (4, 13, 14) AND co.codconcepto <> 10005
					GROUP BY c.fechareg, c.nropago, d.coddocumento, c.car, ca.descripcion
					ORDER BY fechareg, nropago";

	$Consulta = $conexion->query($Sql);
	$data = $Consulta->fetchAll();

	foreach ($data as $item) :

		if(!array_key_exists($item['fechareg'],$array_general)):
			$array_general[$item['fechareg']] = array();
		endif;

		if(!array_key_exists($item['car'],$array_general[$item['fechareg']])):

			$array_general[$item['fechareg']][$item['car']]             = array();
			$array_general[$item['fechareg']][$item['car']]['recibos']  = 0;
			$array_general[$item['fechareg']][$item['car']]['cantidad'] = 0;
			$array_general[$item['fechareg']][$item['car']]['abono']    = 0;
			$array_general[$item['fechareg']][$item['car']]['total']    = 0;

		endif;

		if($item['coddocumento'] == 13 OR $item['coddocumento'] == 14):
			$array_general[$item['fechareg']][$item['car']]['cantidad']++;
		endif;

		if($item['coddocumento'] == 4):
			$array_general[$item['fechareg']][$item['car']]['recibos']++;
		endif;

		$array_general[$item['fechareg']][$item['car']]['total'] += $item['impmes'];

		if(!array_key_exists($item['cars'],$array_car)):
			$array_car[$item['car']] = $item['cars'];
		endif;

	endforeach;

// echo "<pre>"; var_dump($array_general); echo "</pre>"; exit;

	//Para las Notas
$Sql = "SELECT ca.fechareg, d.serie, d.nrodocumento, SUM(d.importe) AS impmes,
					ca.car, car.descripcion AS cars
		FROM  cobranza.cabprepagos c
			JOIN cobranza.detprepagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago) AND (c.nroinscripcion = d.nroinscripcion)
			JOIN cobranza.cabpagos ca ON (c.codemp = ca.codemp) AND (c.codsuc = ca.codsuc) AND (c.nroprepago = ca.nroprepago) AND (c.nroinscripcion = ca.nroinscripcion)
			JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
			JOIN cobranza.car car ON (car.codemp = ca.codemp) AND (car.codsuc = ca.codsuc) AND (car.car = ca.car)
		WHERE c.codsuc = {$codsuc} AND c.estado=2
			AND ca.fechareg BETWEEN '" . $fdesde . "' AND '" . $fhasta . "'
			AND d.coddocumento IN (16) AND co.codconcepto <> 10005
		GROUP BY ca.fechareg, d.serie, d.nrodocumento, d.detalle, ca.car, car.descripcion
		ORDER BY ca.fechareg";

		$Consulta = $conexion->query($Sql);
		$data = $Consulta->fetchAll();

		foreach ($data as $item) :

			if(!array_key_exists($item['fechareg'],$array_general)):
				$array_general[$item['fechareg']] = array();
			endif;

			if(!array_key_exists($item['car'],$array_general[$item['fechareg']])):

				$array_general[$item['fechareg']][$item['car']]             = array();
				$array_general[$item['fechareg']][$item['car']]['recibos']  = 0;
				$array_general[$item['fechareg']][$item['car']]['cantidad'] = 0;
				$array_general[$item['fechareg']][$item['car']]['abono']    = 0;
				$array_general[$item['fechareg']][$item['car']]['total']    = 0;

			endif;

			$array_general[$item['fechareg']][$item['car']]['abono']++;
			$array_general[$item['fechareg']][$item['car']]['total'] += $item['impmes'];

			if(!array_key_exists($item['cars'],$array_car)):
				$array_car[$item['car']] = $item['cars'];
			endif;


		endforeach;

		ksort($array_general);
		ksort($array_car);
		$subrecibos = $subbolfact = $subnota = $subtotal = 0;

		foreach($array_general as $ind_fecha => $datos):

			$cantidad_celdas = count($datos);

			$objReporte->ContenidoCabecera($ind_fecha, $cantidad_celdas);
			$contador = 1;
			foreach($datos as $ind_car => $data):
				$nombre_car = $array_car[$ind_car];
				$objReporte->Contenido($nombre_car, $data['recibos'],  $data['cantidad'], $data['abono'], $data['total'], $contador);
				$objReporte->Ln();
				$contador++;

				$subrecibos += $data['recibos'];
				$subbolfact += $data['cantidad'];
				$subnota    += $data['abono'];
				$subtotal   += $data['total'];

			endforeach;

			$objReporte->Ln();


		endforeach;

		$objReporte->pie($subrecibos, $subbolfact, $subnota, $subtotal);

	$objReporte->Output();

?>
