<?php
session_name("pnsu");
if(!session_start()){session_start();}

	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$cars,$caja;

			$this->SetFont('Arial','B',14);
			$tit1 = "VERIFICACION PLANILLA ENTRADA A CAJA POR TALONES";
			$this->Cell(190,$h+2,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);

			$h=4;
			$this->SetFont('Arial', 'B', 7);
			$this->Cell(7, $h, "CAR", 0, 0, 'L');
			$this->Cell(4, $h, ":", 0, 0, 'C');
			$this->SetFont('Arial', '', 7);
			$this->Cell(30, $h, $cars, 0, 0, 'L');
			$this->SetFont('Arial', 'B', 7);
			$this->Cell(8, $h, "CAJA", 0, 0, 'L');
			$this->Cell(4, $h, ":", 0, 0, 'C');
			$this->SetFont('Arial', '', 7);
			$this->Cell(30, $h, strtoupper($caja), 0, 0, 'L');
			$this->SetFont('Arial', 'B', 7);
			$this->Cell(10, $h, "FECHA", 0, 0, 'L');
			$this->Cell(4, $h, ":", 0, 0, 'C');
			$this->SetFont('Arial', '', 7);
			$this->Cell(37, $h, $fecha, 0, 0, 'L');
			$this->SetFont('Arial', 'B', 7);
			$this->Cell(12, $h, "CAJERO", 0, 0, 'L');
			$this->Cell(4, $h, ":", 0, 0, 'C');
			$this->SetFont('Arial', '', 7);
			$this->Cell(40, $h, $cajeros, 0, 1, 'L');

			$this->Ln(1);
			$this->SetFont('Arial','',7);
			$this->Cell(15, $h,"Nro. Pago",1,0,'C');
			$this->Cell(15, $h,"Codigo",1,0,'C');
			$this->Cell(65, $h,"Cliente",1,0,'C');
			$this->Cell(15, $h,"Doc",1,0,'C');
			$this->Cell(15, $h,"Serie",1,0,'C');
			$this->Cell(28, $h,"Nro. Doc.",1,0,'C');
			$this->Cell(12, $h,"Total",1,0,'C'); //20
			$this->Cell(20, $h,"Efectivo",1,0,'C');
			$this->Cell(5, $h,"A",1,1,'C');

		}
		function contenido($nropago,$codigo,$usuario,$doc,$serie,$nrodoc,$total,$efectivo,$anulado,$i)
		{
			global $codsuc;
			$codigo = $this->CodUsuario($codsuc,$codigo);
			$h=4;

			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);

			$this->Cell(15, $h,$i,0,0,'C');
			$this->Cell(15, $h,$codigo,0,0,'C');
			$this->Cell(65, $h,utf8_decode($usuario),0,0,'L');
			$this->Cell(15, $h,$doc,0,0,'C');
			$this->Cell(15, $h,$serie,0,0,'C');
			$this->Cell(28, $h,$nrodoc,0,0,'C');
			$this->Cell(12, $h,$total,0,0,'R');
			$this->Cell(20, $h,$efectivo,0,0,'R');
			$this->Cell(5, $h,$anulado,0,1,'C');

		}
		function contenidofoot($cont_normal,$cont_anulado,$cont_tick,$tot_procesado,$total_anulado,$total_ticket_pro)
		{
			$h=4;

			$this->Ln(5);

			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Cobrados",0,0,'R');
			$this->Cell(15, $h,$cont_normal,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Procesado",0,0,'R');
			$this->Cell(20, $h,$tot_procesado,0,1,'R');

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Total de Ticket",0,0,'R');
			$this->Cell(15, $h,$cont_tick,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Procesado Ticket",0,0,'R');
			$this->Cell(20, $h,$total_ticket_pro,0,1,'R');

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Anulado",0,0,'R');
			$this->Cell(15, $h,$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Menos Total Anulado",0,0,'R');
			$this->Cell(20, $h,$total_anulado,0,1,'R');

			$tot_procesado 		= str_replace(',', '', $tot_procesado);
			$total_ticket_pro 	= str_replace(',', '', $total_ticket_pro);

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Total de Talones",0,0,'R');
			$this->Cell(15, $h,$cont_normal+$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Cobrado",0,0,'R');
			$this->Cell(20, $h,number_format($tot_procesado+$total_ticket_pro,2),0,1,'R');
		}
    }

	/*$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$cajeros 	= $_SESSION['user'];
	$cars		= $_GET["cars"];
	$caja		= $_GET["caja"];
      */

    $fhasta		= $_GET["fhasta"];
	$fdesde		= $_GET["fdesde"];
	$car		= $_GET["car"];
	$caj		= $_GET["caja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$fecha = $fdesde ." HASTA ".$fhasta;

	if($car!="%"){$cars = $_GET["cars"];$Ccar=" AND car=".$car;}else{$cars = "TODOS"; $Ccar="";}
	if($caj!="%"){$caja = $_GET["cajas"];$Ccaj=" and nrocaja=".$caj;}else{$caja = "TODOS";$Ccaj='';}
	$cajeros = "TODOS";
	if($cajero=="%"){$cajero="";}else
	{
		$cajeros='';
		$Sql="SELECT  login FROM seguridad.usuarios WHERE codusu IN (".$cajero.") ;";
		$Consulta = $conexion->query($Sql);
		foreach($Consulta->fetchAll() as $row)
		{
			$cajeros .=$row[0].",";
		}
		$cajeros=substr($cajeros,0,strlen($cajeros)-1);
		$cajero=" and creador in(".$cajero.")";


	}


	$objReporte	=	new clsTalones();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fhasta		= $objReporte->CodFecha($_GET["fhasta"]);
	$fdesde		= $objReporte->CodFecha($_GET["fdesde"]);

	$sqlC = "SELECT nropago, nroinscripcion, propietario, imppagado, imptotal, anulado, ticket_serie, ticket_numero ";
	$sqlC .= "FROM cobranza.cabpagos ";
	$sqlC .= "WHERE codsuc = ".$codsuc." AND nropec = 0 ";
	$sqlC .= " AND fechareg BETWEEN '".$fdesde."' AND '".$fhasta."' ";
	$sqlC .= " ".$Ccar.$Ccaj.$cajero." ";
	$sqlC .= "ORDER BY nropago";


	$consultaC = $conexion->prepare($sqlC);
	$consultaC->execute(/*array($codsuc,$car,$fecha,$cajero)*/);
	$itemsC = $consultaC->fetchAll();

	$cont_normal    = 0;
	$cont_anulado   = 0;
	$cont_ticket   = 0;

	$tot_procesados = 0;
	$total_anulado  = 0;
	$total_ticket_procesados  = 0;

	$i=0;

	foreach($itemsC as $rowC)
	{
		$sqlD = "select nrodocumento, serie, coddocumento from cobranza.detpagos where codemp=1 and codsuc=? and nroinscripcion=? and
						nropago=? and nrodocumento<>0 group by nrodocumento, serie, coddocumento	";

		$consultaD = $conexion->prepare($sqlD);
		$consultaD->execute(array($codsuc,$rowC["nroinscripcion"],$rowC["nropago"]));
		$itemsD = $consultaD->fetch();

		$i++;
		if($rowC["anulado"] == 1)
		{
			$x				= "x";
			$cont_anulado++;
			$total_anulado += $rowC["imptotal"];
		}else{
			$x				= "";

			if($rowC['ticket_numero'] != 0):
				$cont_ticket++;
				$total_ticket_procesados += $rowC["imptotal"];
			else:
				$cont_normal++;
				$tot_procesados += $rowC["imptotal"];
			endif;
		}

		// Para las ticketeras
		if($rowC['ticket_numero'] != 0):
			$itemsD["nrodocumento"] = $itemsD["nrodocumento"] . " - TIK - " . $rowC['ticket_numero'];
		endif;

		$objReporte->contenido($rowC["nropago"],$rowC["nroinscripcion"],$rowC["propietario"],4,$itemsD["serie"],$itemsD["nrodocumento"],
						number_format($rowC["imptotal"],2),number_format($rowC["imppagado"],2),$x,$i);

	}

	$objReporte->contenidofoot($cont_normal,$cont_anulado,$cont_ticket,number_format($tot_procesados,2),number_format($total_anulado,2),number_format($total_ticket_procesados,2));

	$objReporte->Output();

?>
