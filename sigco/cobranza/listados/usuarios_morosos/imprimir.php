<?php

set_time_limit(0);

include("../../../../objetos/clsReporte.php");

class clsRegistroVentas extends clsReporte {

    function cabecera() {
        global $mes, $anio, $meses, $Dim, $Resumen;

        $this->SetFont('Arial', 'B', 11);
        $this->SetY(15);
        $tit1 = "LISTADO DE USUARIOS MOROSOS POR SERVICIO";
        $this->Cell(0, $h + 2, utf8_decode($tit1), 0, 1, 'C');

        $this->Ln(5);

        $h = 5;

        $this->SetFont('Arial', '', 7);
        $this->Cell(277, $h, $ciclotext, 0, 1, 'C');

        if ($Resumen == 0) {
            $this->SetX(113.5);
            $this->Cell(10, $h, "PERIODO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, $meses[$mes]."-".$anio, 0, 1, 'L');

            $this->Ln(2);
            $this->Ln(1);
            $this->SetFont('Arial', '', 6);

            $this->Cell($Dim[0], $h, utf8_decode("N°"), '1', 0, 'C');
            $this->Cell($Dim[1], $h, "S", 'LTB', 0, 'C');
            $this->Cell($Dim[2], $h, "R", 'TB', 0, 'C');
            $this->Cell($Dim[3], $h, "Sec", 'TBR', 0, 'C');
            $this->Cell($Dim[4], $h, utf8_decode("Inscripción"), '1', 0, 'C');
            $this->Cell($Dim[5], $h, "Cod. Catastral", '1', 0, 'C');
            $this->Cell($Dim[6], $h, "Apellidos y Nombres", '1', 0, 'C');
            $this->Cell($Dim[7], $h, utf8_decode("Dirección"), '1', 0, 'C');
            $this->Cell($Dim[8], $h, "T. Serv.", '1', 0, 'C');
            $this->Cell($Dim[9], $h, "Categ.", '1', 0, 'C');
            $this->Cell($Dim[10], $h, utf8_decode("N° Medidor"), '1', 0, 'C');
            $this->Cell($Dim[11], $h, utf8_decode("N° Meses"), '1', 0, 'C');
            $this->Cell($Dim[12], $h, "Importe", '1', 0, 'C');
            $this->Cell($Dim[13], $h, "Ult. Factu.", '1', 0, 'C');
            $this->Cell($Dim[14], $h, "Observaciones", '1', 1, 'C');
        } else {
            $this->SetX(67);
            $this->Cell(10, $h, "PERIODO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, $meses[$mes]."-".$anio, 0, 1, 'L');

            $this->Ln(2);
        }
    }

    function agregar_detalle($codsuc, $codciclo) {
        global $conexion, $Dim, $anio, $mes, $sector, $ruta, $desde, $hasta, $periodo, $objReporte, $Resumen;

        $h = 6;

        if ($sector <> '%') {
            $sector = " AND cli.codsector = ".$sector." ";
        } else {
            $sector = "";
        }
        if ($ruta <> '%') {
            $ruta = " AND cli.codrutlecturas = ".$ruta." ";
        } else {
            $ruta = "";
        }

        $this->SetTextColor(0, 0, 0);

        $Sql = "SELECT cli.codsector, cli.codrutlecturas, con.orden_lect AS Secuencia, ";
        $Sql .= " cli.codantiguo, ".$objReporte->getCodCatastral('cli.').", cli.propietario, ";
        $Sql .= " tca.descripcioncorta || ' ' || ca.descripcion || ' #' || cli.nrocalle AS Direccion, ";
        $Sql .= " CASE WHEN cli.codtiposervicio = 1 THEN 'A/D' ";
        $Sql .= "  WHEN cli.codtiposervicio = 2 THEN 'A' ";
        $Sql .= "  WHEN cli.codtiposervicio = 3 THEN 'D' END AS TipoServicio, ";
        $Sql .= " tar.nomtar, ";
        $Sql .= " cli.nromed, ";
        $Sql .= " (ccta.nromeses) + 1 AS NroMeses, ";
        $Sql .= " (ccta.impdeuda + ccta.impmes + ccta.impigv) AS Importe, ";
        $Sql .= " ccta.nroinscripcion ";

        $Sql .= "FROM catastro.clientes cli ";
        $Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
        $Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
        $Sql .= " INNER JOIN public.calles ca ON (cli.codemp = ca.codemp) AND (cli.codsuc = ca.codsuc) AND (cli.codzona = ca.codzona) AND (cli.codcalle = ca.codcalle) ";
        $Sql .= " INNER JOIN public.tiposcalle tca ON (ca.codtipocalle = tca.codtipocalle) ";
        $Sql .= " INNER JOIN facturacion.tarifas tar ON (ccta.codemp = tar.codemp) AND (ccta.codsuc = tar.codsuc) AND (ccta.catetar = tar.catetar) AND (tar.estado = 1) ";
        $Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
        $Sql .= " AND ccta.codciclo = ".$codciclo." ";
        $Sql .= $sector;
        $Sql .= $ruta;
        $Sql .= " AND ccta.periodo = '".$periodo."' ";
        $Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
        $Sql .= " AND ccta.tipo = 0 ";
        $Sql .= "ORDER BY cli.codsector, cli.codrutlecturas, con.orden_lect ";
        //die($Sql); exit;
        $consulta = $conexion->prepare($Sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        $borde = 0;

        $tDeuda = 0;

        $this->SetFillColor(255, 255, 255); //Color de Fondo
        $this->SetTextColor(0);

        $count = 0;

        $aSector = array();

        foreach ($items as $row) {
            $count++;

            $this->Cell($Dim[0], $h, $count, $borde, 0, 'C', true);
            $this->Cell($Dim[1], $h, $row['codsector'], $borde, 0, 'C', true);
            $this->Cell($Dim[2], $h, $row['codrutlecturas'], $borde, 0, 'C', true);
            $this->Cell($Dim[3], $h, $row['secuencia'], $borde, 0, 'C', true);
            $this->Cell($Dim[4], $h, $row['codantiguo'], $borde, 0, 'C', true);
            $this->Cell($Dim[5], $h, $row[4], $borde, 0, 'C', true);
            $this->Cell($Dim[6], $h, strtoupper(utf8_decode($row[5])), $borde, 0, 'L', true);
            $this->Cell($Dim[7], $h, strtoupper(utf8_decode($row[6])), $borde, 0, 'L', true);
            $this->Cell($Dim[8], $h, $row[7], $borde, 0, 'C', true);
            $this->Cell($Dim[9], $h, $row[8], $borde, 0, 'C', true);
            $this->Cell($Dim[10], $h, $row[9], $borde, 0, 'C', true);
            $this->Cell($Dim[11], $h, $row[10], $borde, 0, 'C', true);

            $this->Cell($Dim[12], $h, number_format($row[11], 2), $borde, 0, 'R', true);
            $tDeuda += $row[11];

            //Ultima Facturación
            $SqlF = "SELECT cf.nrofacturacion ";
            $SqlF .= "FROM facturacion.cabfacturacion cf ";
            $SqlF .= " INNER JOIN facturacion.detfacturacion df ON(cf.codsuc = df.codsuc) AND (cf.nrofacturacion = df.nrofacturacion) AND (cf.nroinscripcion = df.nroinscripcion) ";
            $SqlF .= "WHERE cf.codemp = 1 ";
            $SqlF .= " AND cf.codsuc = ".$codsuc." ";
            $SqlF .= " AND cf.codciclo = ".$codciclo." ";
            $SqlF .= " AND cf.nrodocumento > 0 ";
            $SqlF .= " AND cf.nroinscripcion = ".$row[12]." ";
            $SqlF .= " AND df.codconcepto IN (1, 2) ";
            $SqlF .= "ORDER BY cf.nrofacturacion DESC ";
            $SqlF .= "LIMIT 1 ";
            //die($SqlF);
            $consultaF = $conexion->prepare($SqlF);
            $consultaF->execute(array());

            $consultaF = $consultaF->fetch();

            $NroFacturacion = $consultaF[0];

            $SqlF = "SELECT anio, mes ";
            $SqlF .= "FROM facturacion.periodofacturacion ";
            $SqlF .= "WHERE codemp = 1 ";
            $SqlF .= " AND codsuc = ".$codsuc." ";
            $SqlF .= " AND codciclo = ".$codciclo." ";
            $SqlF .= " AND nrofacturacion = ".$NroFacturacion;

            $consultaF = $conexion->prepare($SqlF);
            $consultaF->execute(array());

            $UltimaF = $consultaF->fetch();

            $this->Cell($Dim[13], $h, $objReporte->obtener_nombre_mes($UltimaF[1])." - ".$UltimaF[0], $borde, 0, 'C', true);
            $this->Cell($Dim[14], $h, strtoupper(utf8_decode($row[13])), 'B', 1, 'L', true);


            if (!array_key_exists($row['codsector'].'-'.$row['codrutlecturas'], $aSector)) {
                $aSector[$row['codsector'].'-'.$row['codrutlecturas']] = array("IdSector" => $row['codsector'], "
																					IdRuta" => $row['codrutlecturas'],
                    "Conexiones" => 1,
                    "Deuda" => $row[11]);
            } else {
                $aSector[$row['codsector'].'-'.$row['codrutlecturas']] = array("IdSector" => $row['codsector'],
                    "IdRuta" => $row['codrutlecturas'],
                    "Conexiones" => $aSector[$row['codsector'].'-'.$row['codrutlecturas']]["Conexiones"] + 1,
                    "Deuda" => $aSector[$row['codsector'].'-'.$row['codrutlecturas']]["Deuda"] + $row[11]);
            }
        }

        $this->SetFont('Arial', 'B', 6);

        $this->Cell(202, $h, "Total General ==>", "T", 0, 'R');
        $this->Cell($Dim[12], $h, number_format($tDeuda, 2), 'T', 0, 'R');
        $this->Cell($Dim[13], $h, '', 'T', 0, 'R');
        $this->Cell($Dim[14], $h, '', 'T', 1, 'R');

        $Resumen = 1;

        $this->AddPage("P");
        $this->Ln(3);

        $this->SetFont('Arial', 'B', 10);
        $this->SetX(30);
        $this->Cell(70, $h, "RESUMEN", 0, 1, 'L');
        $this->Ln(3);
        $this->SetFont('Arial', 'B', 8);
        $this->SetX(30);
        $this->Cell(15, $h, "CODIGO", 0, 0, 'C');
        $this->Cell(80, $h, "DESCRIPCION", 0, 0, 'C');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, "IMPORTE", 'B', 1, 'C');
        //TOTALE
        $this->Ln(3);

        $Countt = 0;

        $SqlA = "SELECT CodCon, Descrip, Categ, ";
        $SqlA .= " SUM(Agua) AS Agua, ";
        $SqlA .= " SUM(Desague) AS Desague, ";
        $SqlA .= " SUM(Interes) AS Interes, ";
        $SqlA .= " SUM(Fijo) AS Fijo, ";
        $SqlA .= " SUM(Colateral) AS Colateral, ";
        $SqlA .= " SUM(ColateralI) AS ColateralI ";
        $SqlA .= "FROM (";

        foreach ($items as $row) {
            $Countt++;

            if ($Countt > 1) {
                $SqlA .= " UNION ALL ";
            }

            $SqlA .= "(SELECT df.codconcepto AS CodCon, c.descripcion AS Descrip, c.categoria AS Categ, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 1 THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Agua, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 2 THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Desague, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto IN(3, 4, 9, 71, 72, 10013) THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Interes, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 6 THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Fijo, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto NOT IN(1, 2, 3, 4, 6, 9, 71, 72, 10013) THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Colateral, ";
            $SqlA .= " 0 AS ColateralI ";
            $SqlA .= "FROM facturacion.cabctacorriente cf ";
            $SqlA .= " INNER JOIN facturacion.detctacorriente df ON (cf.codemp = df.codemp) AND (cf.codsuc = df.codsuc) ";
            $SqlA .= "  AND (cf.nrofacturacion = df.nrofacturacion) AND (cf.nroinscripcion = df.nroinscripcion) ";
            $SqlA .= "  AND (cf.tipo = df.tipo) AND (cf.tipoestructura = df.tipoestructura) AND (cf.anio = df.anio) AND (cf.mes = df.mes) ";
            $SqlA .= " INNER JOIN facturacion.conceptos c ON (df.codemp = c.codemp) AND (df.codsuc = c.codsuc) AND (df.codconcepto = c.codconcepto) ";
            $SqlA .= "WHERE cf.codsuc = ".$codsuc." ";
            $SqlA .= " AND cf.codciclo = ".$codciclo." ";
            $SqlA .= " AND df.estadofacturacion = 1 ";
            //$SqlA .= " AND cf.tipo = 0 ";
            //$SqlA .= " AND df.tipo = 0 ";
            $SqlA .= " AND df.periodo = '".$periodo."' ";
            //$SqlA .= " AND df.codconcepto = 806 ";
            $SqlA .= " AND cf.nroinscripcion = ".$row[12]." ";
            $SqlA .= "GROUP BY df.codconcepto, c.descripcion, c.categoria) ";
            //die($SqlA);
        }
        $SqlA .= ") Deudores ";
        $SqlA .= "GROUP BY CodCon, Descrip, Categ ";
        $SqlA .= "ORDER BY Categ, CodCon ";
        //die($SqlA);

        $consulta = $conexion->prepare($SqlA);
        $consulta->execute();
        $items = $consulta->fetchAll();

        $Total = 0;
        $TotalI = 0;
        $TotalRP = 0;
        $TotalRN = 0;

        $this->SetFont('Arial', '', 8);

        foreach ($items as $row) {
            $Imp = 0;

            switch ($row[0]) {
                case 1:
                    $Imp = $row['agua'];

                    break;

                case 2:
                    $Imp = $row['desague'];

                    break;

                case 6:
                    $Imp = $row['fijo'];

                    break;

                case 7 or 8:
                    $Imp = $row['colateral'];

                    break;

                default:
                    $Imp = $row['colateral'];

                    break;
            }

            if ($row[2] == 3) {
                $Imp = $row['interes'];
            }

            if ($row[2] <> 7 and $row[2] <> 4) {
                $this->SetX(30);
                $this->Cell(15, $h, $row[0], 0, 0, 'C');
                $this->Cell(80, $h, $row[1], 0, 0, 'L');
                $this->Cell(5, $h, "", 0, 0, 'C');
                $this->Cell(30, $h, number_format($Imp, 2), 0, 1, 'R');

                $this->Ln(2);

                $Total += $Imp;

                if ($row['colaterali'] > 0) {
                    $TotalI += $row['colaterali'];
                }
            } else {
                if ($row[0] == 5) {
                    $TotalI += $Imp;
                }
                if ($row[0] == 7) {
                    $TotalRN += $Imp;
                }
                if ($row[0] == 8) {
                    $TotalRP += $Imp;
                }
            }
        }

        $this->SetX(30);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(95, $h, "SUB TOTAL :", 0, 0, 'R');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($Total, 2), 'T', 1, 'R');

        $this->Ln(2);

        $this->SetFont('Arial', '', 8);

        $this->SetX(30);
        $this->Cell(15, $h, 5, 0, 0, 'C');
        $this->Cell(80, $h, 'I.G.V.', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($TotalI, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->Cell(15, $h, 7, 0, 0, 'C');
        $this->Cell(80, $h, 'REDONDEO NEGATIVO', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($TotalRN, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->Cell(15, $h, 8, 0, 0, 'C');
        $this->Cell(80, $h, 'REDONDEO POSITIVO', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($TotalRP, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(95, $h, "TOTAL :", 0, 0, 'R');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($Total + $TotalI + $TotalRN + $TotalRP, 2), 'T', 1, 'R');

        $Resumen = 1;

        $this->AddPage("P");
        $this->Ln(3);

        $this->SetFont('Arial', 'B', 10);
        $this->SetX(20);
        $this->Cell(55, $h, "RESUMEN POR RUTAS", 0, 1, 'C');

        $this->Ln(3);

        $this->SetFont('Arial', 'B', 8);
        $this->SetX(20);
        $this->Cell(10, $h, "Sec.", 1, 0, 'C');
        $this->Cell(10, $h, "Ruta", 1, 0, 'C');
        $this->Cell(20, $h, "Conexiones", 1, 0, 'C');
        $this->Cell(20, $h, "Importe", 1, 1, 'C');
        //TOTALE
        $this->Ln(2);

        sort($aSector);
        $TotalC = 0;
        $Total = 0;

        $this->SetFont('Arial', '', 8);

        $SecT = 0;

        for ($i = 0; $i < count($aSector); $i++) {
            if ($SecT <> $aSector[$i]['IdSector']) {
                $Sect = $aSector[$i]['IdSector'];
            } else {
                $Sect = '';
            }

            $SecT = $aSector[$i]['IdSector'];

            $this->SetX(20);
            $this->Cell(10, $h, $Sect, 0, 0, 'C');
            $this->Cell(10, $h, $aSector[$i]['IdRuta'], 0, 0, 'C');
            $this->Cell(20, $h, utf8_decode($aSector[$i]['Conexiones']), 0, 0, 'C');
            $this->Cell(20, $h, number_format($aSector[$i]['Deuda'], 2), 0, 1, 'R');

            $this->Ln(1);

            $TotalC += $aSector[$i]['Conexiones'];
            $Total += $aSector[$i]['Deuda'];
        }

        $this->SetX(20);

        $this->SetFont('Arial', 'B', 8);

        $this->Cell(20, $h, "TOTALES : ", 'T', 0, 'R');
        $this->Cell(20, $h, $TotalC, 'T', 0, 'C');
        $this->Cell(20, $h, number_format($Total, 2), 'T', 1, 'R');

        $this->Ln(2);
    }

}

$objReporte = new clsRegistroVentas();

$codsuc = $_GET["codsuc"];
$ciclo  = $_GET["ciclo"];
$sector = $_GET["sector"];
$ruta = $_GET["ruta"];
$anio = $_GET["anio"];
$mes  = $_GET["mes"];
$desde = $_GET["desde"];
$hasta = $_GET["hasta"];

$periodo = $anio.$mes;

$Resumen = 0;

$Dim = array('0' => 8, '1' => 5, '2' => 5, '3' => 8, '4' => 15, '5' => 25, '6' => 45, '7' => 45, '8' => 10, '9' => 10, '10' => 15, '11' => 11,
    '12' => 15, '13' => 20, '14' => 40);

$objReporte->AliasNbPages();
$objReporte->AddPage("L");

$objReporte->agregar_detalle($codsuc, $ciclo);

$objReporte->Output();
?>
