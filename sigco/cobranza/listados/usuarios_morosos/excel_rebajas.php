<?php
include("../../../../objetos/clsReporte.php");
include("../../../../objetos/clsReporteExcel.php");
/*
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=ListadoUsuariosRebajados.xls");
header("Pragma: no-cache");
header("Expires: 0");
 * 
 */
$clsFunciones = new clsFunciones();
$objReporte = new clsReporte();

$codsuc = $_GET["codsuc"];
$codciclo = $_GET["ciclo"];
$sector = $_GET["sector"];
$ruta = $_GET["ruta"];
$anio = $_GET["anio"];
$mes = $_GET["mes"];
$desde = $_GET["desde"];
$hasta = $_GET["hasta"];

$periodo = $anio.$mes;

$Resumen = 0;

CabeceraExcel(2, 13);
?>
<table class="ui-widget" border="0" cellspacing="0" id="TbTitulo" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:14px">
        <tr title="Cabecera">
            <th scope="col" colspan="11" align="center" >LISTADO DE USUARIOS MOROSOS QUE SE REBAJARON</th>
        </tr>
        <tr>
            <th colspan="11">CICLO : <?=$ciclotext?>&nbsp;&nbsp;&nbsp;&nbsp; PERIODO:&nbsp;<?=$meses[$mes]."-".$anio;?></th>
        </tr>    
    </thead>
</table>
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:13px">
        <tr>
            <th width="30" scope="col" class="ui-widget-header">N&deg;</th>
            <th width="30" scope="col" class="ui-widget-header">S</th>
            <th width="30" scope="col" class="ui-widget-header">R</th>
            <th width="30" scope="col" class="ui-widget-header">Sec</th>
            <th width="80" scope="col" class="ui-widget-header">Inscripcion</th>
            <th width="200" scope="col" class="ui-widget-header">Apellidos y Nombres</th>
            <th width="200" scope="col" class="ui-widget-header">Direccion</th>
            <th width="80" scope="col" class="ui-widget-header">Imp. Rebajado</th>
            <th width="50" scope="col" class="ui-widget-header">Fecha</th>
            <th width="80" scope="col" class="ui-widget-header">Deuda Total</th>
            <th width="150" scope="col" class="ui-widget-header">Observaciones</th>
        </tr>
    </thead>
    <tbody style="font-size:12px">
        <?php
        $h = 4;
        $s = 2;

        if ($sector <> '%') {
            $sector = " AND cli.codsector = ".$sector." ";
        } else {
            $sector = "";
        }
        if ($ruta <> '%') {
            $ruta = " AND cli.codrutlecturas = ".$ruta." ";
        } else {
            $ruta = "";
        }
        
        $Sql = "SELECT cli.codsector, cli.codrutlecturas, con.orden_lect AS Secuencia, ";
        $Sql .= " cli.codantiguo, cli.nroinscripcion, cli.propietario, ";
        $Sql .= " tca.descripcioncorta || ' ' || ca.descripcion || ' #' || cli.nrocalle AS Direccion, ";
        $Sql .= " to_char(reb.fechaemision, 'dd/mm/YYYY') fechaemision, ";
        $Sql .= " SUM(dre.imporiginal) AS deuda, ";
        $Sql .= " SUM(dre.imprebajado) AS imprebajado ";
        $Sql .= " FROM catastro.clientes cli ";
        $Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
        $Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
        $Sql .= " INNER JOIN public.calles ca ON (cli.codemp = ca.codemp) AND (cli.codsuc = ca.codsuc) AND (cli.codzona = ca.codzona) AND (cli.codcalle = ca.codcalle) ";
        $Sql .= " INNER JOIN public.tiposcalle tca ON (ca.codtipocalle = tca.codtipocalle) ";
        $Sql .= " INNER JOIN facturacion.cabrebajas AS reb ON cli.nroinscripcion = reb.nroinscripcion AND cli.codemp = reb.codemp AND cli.codsuc = reb.codsuc
                INNER JOIN facturacion.detrebajas AS dre ON dre.codemp = reb.codemp AND dre.codsuc = reb.codsuc AND dre.nrorebaja = reb.nrorebaja ";
        $Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
        $Sql .= " AND ccta.codciclo = ".$codciclo." ";
        $Sql .= $sector;
        $Sql .= $ruta;
        $Sql .= " AND ccta.periodo = '".$periodo."' ";
        $Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
        $Sql .= " AND ccta.tipo = 0 AND reb.codestrebaja<>0 ";
        $Sql .= " AND reb.anio='$anio' AND reb.mes='$mes' ";
        $Sql .= " GROUP BY cli.codsector, cli.codrutlecturas,
                con.orden_lect, cli.codantiguo,
                cli.nroinscripcion, cli.propietario,
                tca.descripcioncorta, ca.descripcion,
                cli.nrocalle, reb.fechaemision ";
        $Sql .= " ORDER BY cli.codsector, cli.codrutlecturas, con.orden_lect ";

        $consulta = $conexion->prepare($Sql);
        $consulta->execute();
        $items = $consulta->fetchAll();
        $count = 0;
        foreach ($items as $row) {
            $count++;
            $tDeuda += $row[8];
            
            if (!array_key_exists($row['codsector'].'-'.$row['codrutlecturas'], $aSector)) {
                $aSector[$row['codsector'].'-'.$row['codrutlecturas']] = array(
                    "IdSector" => $row['codsector'], 
                    "IdRuta" => $row['codrutlecturas'],
                    "Conexiones" => 1,
                    "Deuda" => $row[8]);
            } else {
                $aSector[$row['codsector'].'-'.$row['codrutlecturas']] = array(
                    "IdSector" => $row['codsector'],
                    "IdRuta" => $row['codrutlecturas'],
                    "Conexiones" => $aSector[$row['codsector'].'-'.$row['codrutlecturas']]["Conexiones"] + 1,
                    "Deuda" => $aSector[$row['codsector'].'-'.$row['codrutlecturas']]["Deuda"] + $row[8]);
            }
            
            ?>

            <tr <?=$title ?> <?=$class ?> onclick="SeleccionaId(this);" id="<?=$count ?>" >
                <td align="center" valign="middle"><?php echo $count; ?></td>
                <td align="center" valign="middle"><?php echo $row['codsector']; ?></td>
                <td align="center" valign="middle"><?php echo $row['codrutlecturas']; ?></td>
                <td align="center" valign="middle"><?php echo $row['secuencia']; ?></td>
                <td align="center" valign="middle"><?php echo $row['codantiguo']; ?></td>
                <td align="left" valign="middle"><?php echo strtoupper(utf8_decode($row[5])); ?></td>
                <td align="left" valign="middle"><?php echo strtoupper(utf8_decode($row[6])); ?></td>
                <td align="right" valign="middle"><?php echo number_format($row[9], 2); ?></td>
                <td align="center" valign="middle"><?php echo $row['fechaemision']; ?></td>
                <td align="right" valign="middle"><?php echo number_format($row[8], 2); ?></td>
                <td align="left" valign="middle"><?php echo strtoupper(utf8_decode($row[13])); ?></td>
                <?php
            }
            ?>
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td colspan="9" align="right" class="ui-widget-header">Total de Deuda : </td>
            <td class="ui-widget-header" align="right"><?=number_format($tDeuda,2) ?></td>
            <td class="ui-widget-header">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="11" align="center" class="ui-widget-header">Usuario Registrados <?=$count ?></td>
        </tr>
    </tfoot>
</table>
<br />
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="40%" rules="rows" align="center">
    <thead class="ui-widget-header" style="font-size:13px">
        <tr>
            <td colspan="4">RESUMEN</td>
        </tr>
        <tr>
            <th width="30" scope="col" class="ui-widget-header">CODIGO</th>
            <th width="120" scope="col" class="ui-widget-header">DESCRIPCION</th>
            <th width="30" scope="col" class="ui-widget-header">&nbsp;</th>
            <th width="40" scope="col" class="ui-widget-header">IMPORTE</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $SqlA = "SELECT CodCon, Descrip, Categ, ";
            $SqlA .= " SUM(Agua) AS Agua, ";
            $SqlA .= " SUM(Desague) AS Desague, ";
            $SqlA .= " SUM(Interes) AS Interes, ";
            $SqlA .= " SUM(Fijo) AS Fijo, ";
            $SqlA .= " SUM(Colateral) AS Colateral, ";
            $SqlA .= " SUM(ColateralI) AS ColateralI ";
            $SqlA .= "FROM (";

            foreach ($items as $row) {

                $Countt++;

                if ($Countt > 1) {
                    $SqlA .= " UNION ALL ";
                }

                $SqlA .= "(SELECT df.codconcepto AS CodCon, c.descripcion AS Descrip, c.categoria AS Categ, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto = 1 THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Agua, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto = 2 THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Desague, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto IN(3, 4, 9, 71, 72, 10013) THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Interes, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto = 6 THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Fijo, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto NOT IN(1, 2, 3, 4, 6, 9, 71, 72, 10013) THEN df.importe - df.importerebajado + df.importeaumentado ELSE 0 END) AS Colateral, ";
                $SqlA .= " 0 AS ColateralI ";
                $SqlA .= "FROM facturacion.cabfacturacion cf ";
                $SqlA .= " INNER JOIN facturacion.detfacturacion df ON (cf.codemp = df.codemp) AND (cf.codsuc = df.codsuc) AND (cf.nrofacturacion = df.nrofacturacion) AND (cf.nroinscripcion = df.nroinscripcion) ";
                $SqlA .= " INNER JOIN facturacion.conceptos c ON (df.codemp = c.codemp) AND (df.codsuc = c.codsuc) AND (df.codconcepto = c.codconcepto) ";
                $SqlA .= "WHERE cf.codsuc = ".$codsuc." ";
                $SqlA .= " AND cf.codciclo = ".$codciclo." ";
                $SqlA .= " AND df.estadofacturacion = 1 ";
                $SqlA .= " AND cf.enreclamo = 0 ";
                $SqlA .= " AND df.categoria <> 2 ";
                //$SqlA .= " AND df.codconcepto = 806 ";
                $SqlA .= " AND cf.nroinscripcion = ".$row['nroinscripcion']." ";
                $SqlA .= "GROUP BY df.codconcepto, c.descripcion, c.categoria) ";

            }

            $SqlA .= ") Deudores ";
            $SqlA .= "GROUP BY CodCon, Descrip, Categ ";
            $SqlA .= "ORDER BY Categ, CodCon ";

            $consulta = $conexion->prepare($SqlA);
            $consulta->execute();
            $items = $consulta->fetchAll();

            $Total = 0;
            $TotalI = 0;
            $TotalRP = 0;
            $TotalRN = 0;

            foreach ($items as $row) {
                $Imp = 0;

                switch ($row[0]) {
                    case 1:
                        $Imp = $row['agua'];

                        break;

                    case 2:
                        $Imp = $row['desague'];

                        break;

                    case 6:
                        $Imp = $row['fijo'];

                        break;

                    case 7 or 8:
                        $Imp = $row['colateral'];

                        break;

                    default:
                        $Imp = $row['colateral'];

                        break;
                }

                if ($row[2] == 3) {
                    $Imp = $row['interes'];
                }
                
                if ($row[2] <> 7 and $row[2] <> 4) {
                    
                    $Total += $Imp;

                    if ($row['colaterali'] > 0) {
                        $TotalI += $row['colaterali'];
                    }
                

        ?>
            <tr style="font-size:12px">
                <td align="center"><?=$row[0];?></td>
                <td><?=$row[1];?></td>
                <td></td>
                <td align="right"><?=number_format($Imp, 2);?></td>
            </tr>
        <?php
                }
                else
                {
                    if ($row[0] == 5) {
                    $TotalI += $Imp;
                    }
                    if ($row[0] == 7) {
                        $TotalRN += $Imp;
                    }
                    if ($row[0] == 8) {
                        $TotalRP += $Imp;
                    }
                }            
            }
            ?>
            <tr style="font-size:12px; font-weight: bold;">
                <td colspan="3" align="right">SUB TOTAL :</td>
                <td align="right"><?=number_format($Total, 2);?></td>
            </tr>
            <tr style="font-size:12px">
                <td align="center">5</td>
                <td>IGV :</td>
                <td></td>
                <td align="right"><?=number_format($TotalI, 2);?></td>
            </tr>
            <tr style="font-size:12px">
                <td align="center">7</td>
                <td>REDONDEO NEGATIVO :</td>
                <td></td>
                <td align="right"><?=number_format($TotalRN, 2);?></td>
            </tr>
            <tr style="font-size:12px">
                <td align="center">8</td>
                <td>REDONDEO POSITIVO :</td>
                <td></td>
                <td align="right"><?=number_format($TotalRP, 2);?></td>
            </tr>
            <tr style="font-size:12px; font-weight: bold;">
                <td colspan="3" align="right">TOTAL :</td>
                <td align="right"><?=number_format($Total + $TotalI + $TotalRN + $TotalRP, 2);?></td>
            </tr>
            <?php
        ?>
    </tbody>
</table>
<br />
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="40%" rules="rows" align="center">
    <thead class="ui-widget-header" style="font-size:13px">
        <tr>
            <td colspan="4">RESUMEN POR RUTAS</td>
        </tr>
        <tr>
            <th width="30" scope="col" class="ui-widget-header">Sec.</th>
            <th width="120" scope="col" class="ui-widget-header">Ruta</th>
            <th width="30" scope="col" class="ui-widget-header">Conexiones</th>
            <th width="40" scope="col" class="ui-widget-header">Importe</th>
        </tr>
    </thead>
    <tbody>
    <?php
        sort($aSector);
        $TotalC = 0;
        $Total = 0;
        $SecT = 0;
        for ($i = 0; $i < count($aSector); $i++) 
        {
            if ($SecT <> $aSector[$i]['IdSector']) {
                $Sect = $aSector[$i]['IdSector'];
            } else {
                $Sect = '';
            }

            $SecT = $aSector[$i]['IdSector'];
            ?>
            <tr style="font-size:12px">
                <td align="center"><?=$Sect;?></td>
                <td align="center"><?=$aSector[$i]['IdRuta'];?></td>
                <td align="center"><?=utf8_decode($aSector[$i]['Conexiones']);?></td>
                <td align="right"><?=number_format($aSector[$i]['Deuda'], 2);?></td>
            </tr>
        
            <?php
            $TotalC += $aSector[$i]['Conexiones'];
            $Total += $aSector[$i]['Deuda'];
        }
    ?> 
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:12px">
        <tr>
            <td colspan="2" align="right" class="ui-widget-header">TOTALES :</td>
            <td class="ui-widget-header" align="center"><?=number_format($TotalC,0) ?></td>
            <td class="ui-widget-header" align="right"><?=number_format($Total, 2)?></td>
        </tr>        
    </tfoot>
</table>
