<?php

set_time_limit(0);

include("../../../../objetos/clsReporte.php");

class clsRegistroVentas extends clsReporte {

    function cabecera() {
        global $mes, $anio, $meses, $Dim, $Resumen;

        $this->SetFont('Arial', 'B', 9);
        $this->SetY(15);
		
        $tit1 = "LISTADO DE USUARIOS MOROSOS QUE SE REBAJARON";
        
		$this->Cell(0, $h + 2, utf8_decode($tit1), 0, 1, 'C');

        $this->Ln(5);

        $h = 4;
        $this->SetFont('Arial', '', 7);
        $this->Cell(277, $h, $ciclotext, 0, 1, 'C');

        if ($Resumen == 0) {
            $this->SetX(113.5);
            $this->Cell(10, $h, "PERIODO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, $meses[$mes] . "-" . $anio, 0, 1, 'L');

            $this->Ln(2);
            $this->Ln(1);
            $this->SetFont('Arial', '', 6);

            $this->Cell($Dim[0], $h, utf8_decode("N°"), '1', 0, 'C');
            $this->Cell($Dim[1], $h, "S", 'LTB', 0, 'C');
            $this->Cell($Dim[2], $h, "R", 'TB', 0, 'C');
            $this->Cell($Dim[3], $h, "Sec", 'TBR', 0, 'C');
            $this->Cell($Dim[4], $h, utf8_decode("Inscripción"), '1', 0, 'C');
            $this->Cell($Dim[5], $h, "Apellidos y Nombres", '1', 0, 'C');
            $this->Cell($Dim[6], $h, utf8_decode("Dirección"), '1', 0, 'C');
            $this->Cell($Dim[7], $h, utf8_decode("Imp. Original"), '1', 0, 'C');
            $this->Cell($Dim[8], $h, utf8_decode("Fecha"), '1', 0, 'C');
            $this->Cell($Dim[9], $h, "Imp. Rebajado", '1', 0, 'C');
            $this->Cell($Dim[10], $h, "Observaciones", '1', 1, 'C');
        } else {
            $this->SetX(67);
            $this->Cell(10, $h, "PERIODO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, $meses[$mes] . "-" . $anio, 0, 1, 'L');

            $this->Ln(2);
        }
    }

	function agregar_detalle($codsuc, $codciclo) {
        global $conexion, $Dim, $anio, $mes, $sector, $ruta, $desde, $hasta, $periodo, $objReporte, $Resumen;

        $h = 4;

        if ($sector <> '%') {
            $sector = " AND cli.codsector = " . $sector . " ";
        } else {
            $sector = "";
        }
        if ($ruta <> '%') {
            $ruta = " AND cli.codrutlecturas = " . $ruta . " ";
        } else {
            $ruta = "";
        }

        $this->SetTextColor(0, 0, 0);

        $Sql = "SELECT cli.codsector, cli.codrutlecturas, con.orden_lect AS Secuencia, ";
        $Sql .= " cli.codantiguo, cli.nroinscripcion, cli.propietario, ";
        $Sql .= " tca.descripcioncorta || ' ' || ca.descripcion || ' #' || cli.nrocalle AS Direccion, ";
        $Sql .= " reb.fechaemision, ";
        $Sql .= " SUM(dre.imporiginal) AS deuda, ";
        $Sql .= " SUM(dre.imprebajado) AS montopagado, ";
        $Sql .= " reb.glosa ";
        $Sql .= "FROM catastro.clientes cli ";
        $Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
        $Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
        $Sql .= " INNER JOIN public.calles ca ON (cli.codemp = ca.codemp) AND (cli.codsuc = ca.codsuc) AND (cli.codzona = ca.codzona) AND (cli.codcalle = ca.codcalle) ";
        $Sql .= " INNER JOIN public.tiposcalle tca ON (ca.codtipocalle = tca.codtipocalle) ";
        $Sql .= " INNER JOIN facturacion.cabrebajas AS reb ON (cli.nroinscripcion = reb.nroinscripcion) AND (cli.codemp = reb.codemp) AND (cli.codsuc = reb.codsuc) ";
		$Sql .= " INNER JOIN facturacion.detrebajas AS dre ON (dre.codemp = reb.codemp) AND (dre.codsuc = reb.codsuc) AND (dre.nrorebaja = reb.nrorebaja) ";
            
        $Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
        $Sql .= " AND ccta.codciclo = ".$codciclo." ";
        $Sql .= $sector;
        $Sql .= $ruta;
        $Sql .= " AND ccta.periodo = '".$periodo."' ";
        $Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
        $Sql .= " AND ccta.tipo = 0 ";
		$Sql .= " AND reb.codestrebaja <> 0 ";
        //$Sql .= " AND reb.anio = '".$anio."' AND reb.mes = '".$mes."' ";
		$Sql .= " AND EXTRACT(YEAR FROM reb.fechareg) = '".$anio."' ";
		$Sql .= " AND EXTRACT(MONTH FROM reb.fechareg) = '".intval($mes)."'";
		//$Sql .= " AND ccta.nroinscripcion = 124396 ";

        $Sql .= "GROUP BY cli.codsector, cli.codrutlecturas, con.orden_lect, cli.codantiguo, cli.nroinscripcion, ";
		$Sql .= " cli.propietario, tca.descripcioncorta, ca.descripcion, cli.nrocalle, reb.fechaemision, reb.glosa ";
        $Sql .= "ORDER BY cli.codsector, cli.codrutlecturas, con.orden_lect ";

        $consulta = $conexion->prepare($Sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        $borde = 0;

        $tRebaja = 0;
        $tTotal = 0;

        $this->SetFillColor(255, 255, 255); //Color de Fondo
        $this->SetTextColor(0);

        $count = 0;

        $aSector = array();

        foreach ($items as $row) {
            $count++;

            $this->Cell($Dim[0], $h, $count, $borde, 0, 'C', true);
            $this->Cell($Dim[1], $h, $row['codsector'], $borde, 0, 'C', true);
            $this->Cell($Dim[2], $h, $row['codrutlecturas'], $borde, 0, 'C', true);
            $this->Cell($Dim[3], $h, $row['secuencia'], $borde, 0, 'C', true);
            $this->Cell($Dim[4], $h, $row['codantiguo'], $borde, 0, 'C', true);
            $this->Cell($Dim[5], $h, strtoupper(utf8_decode($row[5])), $borde, 0, 'L', true);
            $this->Cell($Dim[6], $h, strtoupper(utf8_decode($row[6])), $borde, 0, 'L', true);
            $this->Cell($Dim[7], $h, number_format($row[8], 2), $borde, 0, 'R', true);
			$tTotal += $row[8];
			
            $this->Cell($Dim[8], $h, $this->DecFecha($row['fechaemision']), $borde, 0, 'C', true);
            $this->Cell($Dim[9], $h, number_format($row[9], 2), $borde, 0, 'R', true);
            $tRebaja += $row[9];

            $this->Cell($Dim[10], $h, strtoupper(utf8_decode($row[10])), $borde, 1, 'L', true);
			
			if (!array_key_exists($row['codsector'].'-'.$row['codrutlecturas'], $aSector))
			{
                $aSector[$row['codsector'].'-'.$row['codrutlecturas']] = array("IdSector" => $row['codsector'], 
																				"IdRuta" => $row['codrutlecturas'],
                    															"Conexiones" => 1,
                    															"Deuda" => $row[8],
                    															"DeudaR" => $row[9]);
            }
			else
			{
                $aSector[$row['codsector'].'-'.$row['codrutlecturas']] = array("IdSector" => $row['codsector'], 
																				"IdRuta" => $row['codrutlecturas'],
                    															"Conexiones" => $aSector[$row['codsector'].'-'.$row['codrutlecturas']]["Conexiones"] + 1,
                    															"Deuda" => $aSector[$row['codsector'].'-'.$row['codrutlecturas']]["Deuda"] + $row[8],
                    															"DeudaR" => $aSector[$row['codsector'].'-'.$row['codrutlecturas']]["DeudaR"] + $row[9]); 
            }
        }

        $this->SetFont('Arial', 'B', 6);

        $this->Cell(161, $h, "Total General ==>", "T", 0, 'R');
        $this->Cell($Dim[7], $h, number_format($tTotal, 2), 'T', 0, 'R');
        $this->Cell($Dim[8], $h, '', 'T', 0, 'R');
        $this->Cell($Dim[9], $h, number_format($tRebaja, 2), 'T', 0, 'R');
        $this->Cell($Dim[10], $h, '', 'T', 0, 'R');

        $Resumen = 1;

        $this->AddPage("P");
        $this->Ln(3);

        $this->SetFont('Arial', 'B', 10);
        $this->SetX(30);
        $this->Cell(70, $h, "RESUMEN", 0, 1, 'L');
        $this->Ln(3);
        $this->SetFont('Arial', 'B', 8);
        $this->SetX(30);
        $this->Cell(15, $h, "CODIGO", 0, 0, 'C');
        $this->Cell(80, $h, "DESCRIPCION", 0, 0, 'C');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, "IMP. ORIGINAL", 'B', 0, 'C');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, "IMP. REBAJADO", 'B', 1, 'C');
        //TOTALE
        $this->Ln(3);

        $Countt = 0;

        $SqlA = "SELECT CodCon, Descrip, Categ, ";
        $SqlA .= " SUM(Agua) AS Agua, ";
        $SqlA .= " SUM(AguaR) AS AguaR, ";
        $SqlA .= " SUM(Desague) AS Desague, ";
        $SqlA .= " SUM(DesagueR) AS DesagueR, ";
        $SqlA .= " SUM(Interes) AS Interes, ";
        $SqlA .= " SUM(InteresR) AS InteresR, ";
        $SqlA .= " SUM(Fijo) AS Fijo, ";
        $SqlA .= " SUM(FijoR) AS FijoR, ";
        $SqlA .= " SUM(Colateral) AS Colateral, ";
        $SqlA .= " SUM(ColateralR) AS ColateralR, ";
        $SqlA .= " SUM(ColateralI) AS ColateralI ";
        $SqlA .= "FROM (";

        foreach ($items as $row) {
            $Countt++;

            if ($Countt > 1) {
                $SqlA .= " UNION ALL ";
            }

            $SqlA .= "(SELECT df.codconcepto AS CodCon, c.descripcion AS Descrip, c.categoria AS Categ, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 1 THEN df.imporiginal ELSE 0 END) AS Agua, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 1 THEN df.imprebajado ELSE 0 END) AS AguaR, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 2 THEN df.imporiginal ELSE 0 END) AS Desague, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 2 THEN df.imprebajado ELSE 0 END) AS DesagueR, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto IN(3, 4, 9, 71, 72, 10013) THEN df.imporiginal ELSE 0 END) AS Interes, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto IN(3, 4, 9, 71, 72, 10013) THEN df.imprebajado ELSE 0 END) AS InteresR, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 6 THEN df.imporiginal ELSE 0 END) AS Fijo, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto = 6 THEN df.imprebajado ELSE 0 END) AS FijoR, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto NOT IN(1, 2, 3, 4, 6, 9, 71, 72, 10013) THEN df.imporiginal ELSE 0 END) AS Colateral, ";
            $SqlA .= " SUM(CASE WHEN df.codconcepto NOT IN(1, 2, 3, 4, 6, 9, 71, 72, 10013) THEN df.imprebajado ELSE 0 END) AS ColateralR, ";
            $SqlA .= " 0 AS ColateralI ";
            $SqlA .= "FROM facturacion.cabrebajas cf ";
            $SqlA .= " INNER JOIN facturacion.detrebajas df ON (cf.codemp = df.codemp) AND (cf.codsuc = df.codsuc) AND (cf.nrorebaja = df.nrorebaja) ";
            $SqlA .= " INNER JOIN facturacion.conceptos c ON (df.codemp = c.codemp) AND (df.codsuc = c.codsuc) AND (df.codconcepto = c.codconcepto) ";
            $SqlA .= "WHERE cf.codsuc = ".$codsuc." ";
            $SqlA .= " AND EXTRACT(YEAR FROM cf.fechareg) = '".$anio."' ";
			$SqlA .= " AND EXTRACT(MONTH FROM cf.fechareg) = '".intval($mes)."'";
            //$SqlA .= " AND df.categoria <> 2 ";
            //$SqlA .= " AND df.codconcepto = 806 ";
            $SqlA .= " AND cf.nroinscripcion = ".$row['nroinscripcion']." ";
            $SqlA .= "GROUP BY df.codconcepto, c.descripcion, c.categoria) ";
            //die($SqlA);
        }
        $SqlA .= ") Deudores ";
        $SqlA .= "GROUP BY CodCon, Descrip, Categ ";
        $SqlA .= "ORDER BY Categ, CodCon ";
        //die($SqlA);

        $consulta = $conexion->prepare($SqlA);
        $consulta->execute();
        $items = $consulta->fetchAll();

        $Total = 0;
        $TotalR = 0;
		
        $TotalI = 0;
        $TotalIR = 0;
        $TotalRP = 0;
        $TotalRPR = 0;
        $TotalRN = 0;;
        $TotalRNR = 0;

        $this->SetFont('Arial', '', 8);

        foreach ($items as $row) {
            $Imp = 0;
            $ImpR = 0;

            switch ($row[0]) {
                case 1:
                    $Imp = $row['agua'];
                    $ImpR = $row['aguar'];

                    break;

                case 2:
                    $Imp = $row['desague'];
                    $ImpR = $row['desaguer'];

                    break;

                case 6:
                    $Imp = $row['fijo'];
                    $ImpR = $row['fijor'];

                    break;

                case 7 or 8:
                    $Imp = $row['colateral'];
                    $ImpR = $row['colateralr'];

                    break;

                default:
                    $Imp = $row['colateral'];
                    $ImpR = $row['colateralr'];

                    break;
            }

            if ($row[2] == 3) {
                $Imp = $row['interes'];
                $ImpR = $row['interesr'];
            }

            if ($row[2] <> 7 and $row[2] <> 4)
			{
                $this->SetX(30);
                $this->Cell(15, $h, $row[0], 0, 0, 'C');
                $this->Cell(80, $h, $row[1], 0, 0, 'L');
                $this->Cell(5, $h, "", 0, 0, 'C');
                $this->Cell(20, $h, number_format($Imp, 2), 0, 0, 'R');
                $this->Cell(5, $h, "", 0, 0, 'C');
                $this->Cell(20, $h, number_format($ImpR, 2), 0, 1, 'R');

                $this->Ln(2);

                $Total += $Imp;
                $TotalR += $ImpR;

                if ($row['colaterali'] > 0) {
                    $TotalI += $row['colaterali'];
                    $TotalIR += $row['colaterali'];
                }
            } else {
                if ($row[0] == 5) {
                    $TotalI += $Imp;
                    $TotalIR += $ImpR;
                }
                if ($row[0] == 7) {
                    $TotalRN += $Imp;
                    $TotalRNR += $ImpR;
                }
                if ($row[0] == 8) {
                    $TotalRP += $Imp;
                    $TotalRPR += $ImpR;
                }
            }
        }

        $this->SetX(30);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(95, $h, "SUB TOTAL :", 0, 0, 'R');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($Total, 2), 'T', 0, 'R');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($TotalR, 2), 'T', 1, 'R');

        $this->Ln(2);

        $this->SetFont('Arial', '', 8);

        $this->SetX(30);
        $this->Cell(15, $h, 5, 0, 0, 'C');
        $this->Cell(80, $h, 'I.G.V.', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($TotalI, 2), 0, 0, 'R');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($TotalIR, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->Cell(15, $h, 7, 0, 0, 'C');
        $this->Cell(80, $h, 'REDONDEO NEGATIVO', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($TotalRN, 2), 0, 0, 'R');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($TotalRNR, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->Cell(15, $h, 8, 0, 0, 'C');
        $this->Cell(80, $h, 'REDONDEO POSITIVO', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($TotalRP, 2), 0, 0, 'R');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($TotalRPR, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(95, $h, "TOTAL :", 0, 0, 'R');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($Total + $TotalI + $TotalRN + $TotalRP, 2), 'T', 0, 'R');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, number_format($TotalR + $TotalIR + $TotalRNR + $TotalRPR, 2), 'T', 1, 'R');

        //$Resumen = 1;
//
//        $this->AddPage("P");
//        $this->Ln(3);
//
//        $this->SetFont('Arial', 'B', 10);
//        $this->SetX(20);
//        $this->Cell(55, $h, "RESUMEN POR RUTAS", 0, 1, 'C');
//
//        $this->Ln(3);
//
//        $this->SetFont('Arial', 'B', 8);
//        $this->SetX(20);
//        $this->Cell(10, $h, "Sec.", 1, 0, 'C');
//        $this->Cell(10, $h, "Ruta", 1, 0, 'C');
//        $this->Cell(20, $h, "Conexiones", 1, 0, 'C');
//        $this->Cell(20, $h, "Imp. Original", 1, 0, 'C');
//        $this->Cell(20, $h, "Imp. Rebajado", 1, 1, 'C');
//        //TOTALE
//        $this->Ln(2);
//
//        sort($aSector);
//        $TotalC = 0;
//        $Total = 0;
//
//        $this->SetFont('Arial', '', 8);
//
//        $SecT = 0;
//
//        for ($i = 0; $i < count($aSector); $i++)
//		{
//            if ($SecT <> $aSector[$i]['IdSector'])
//			{
//                $Sect = $aSector[$i]['IdSector'];
//            }
//			else
//			{
//                $Sect = '';
//            }
//
//            $SecT = $aSector[$i]['IdSector'];
//
//            $this->SetX(20);
//            $this->Cell(10, $h, $Sect, 0, 0, 'C');
//            $this->Cell(10, $h, $aSector[$i]['IdRuta'], 0, 0, 'C');
//            $this->Cell(20, $h, utf8_decode($aSector[$i]['Conexiones']), 0, 0, 'C');
//            $this->Cell(20, $h, number_format($aSector[$i]['Deuda'], 2), 0, 0, 'R');
//            $this->Cell(20, $h, number_format($aSector[$i]['DeudaR'], 2), 0, 1, 'R');
//
//            $this->Ln(1);
//
//            $TotalC += $aSector[$i]['Conexiones'];
//            $Total += $aSector[$i]['Deuda'];
//            $TotalR += $aSector[$i]['DeudaR'];
//        }
//
//        $this->SetX(20);
//
//        $this->SetFont('Arial', 'B', 8);
//
//        $this->Cell(20, $h, "TOTALES : ", 'T', 0, 'R');
//        $this->Cell(20, $h, $TotalC, 'T', 0, 'C');
//        $this->Cell(20, $h, number_format($Total, 2), 'T', 0, 'R');
//        $this->Cell(20, $h, number_format($TotalR, 2), 'T', 1, 'R');
//
//        $this->Ln(2);
    }

}

$objReporte = new clsRegistroVentas();

$codsuc = $_GET["codsuc"];
$ciclo = $_GET["ciclo"];
$sector = $_GET["sector"];
$ruta = $_GET["ruta"];
$anio = $_GET["anio"];
$mes = $_GET["mes"];
$desde = $_GET["desde"];
$hasta = $_GET["hasta"];

$periodo = $anio . $mes;

$Resumen = 0;

$Dim = array('0' => 8, '1' => 5, '2' => 5, '3' => 8, '4' => 15, '5' => 60, '6' => 60, '7' => 18, '8' => 15, '9' => 18, '10' => 60);

$objReporte->AliasNbPages();
$objReporte->AddPage("L");

$objReporte->agregar_detalle($codsuc, $ciclo);

$objReporte->Output();
?>
