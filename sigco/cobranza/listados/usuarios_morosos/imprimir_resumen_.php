<?php
	set_time_limit(0);

	include("../../../../objetos/clsReporte.php");

	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $mes, $anio, $meses, $Dim, $Resumen;

			$this->SetFont('Arial', 'B', 11);
			$this->SetY(15);
			$tit1 = "RESUMEN FINAL DE GESTION DE COBRANZA";
			$this->Cell(0, $h + 2, utf8_decode($tit1), 0, 1, 'C');

            $this->Ln(5);

			$h = 8;
			
			$this->SetFont('Arial', '', 8);
			
			$this->Cell(277, $h, $ciclotext, 0, 1, 'C');
			
			if ($Resumen == 0)
			{
				$this->SetFont('Arial', '', 7);
				
				$this->SetX(67);
				$this->Cell(10, $h, "PERIODO", 0, 0, 'R');
				$this->Cell(5, $h, ":", 0, 0, 'C');
				$this->Cell(30, $h, $meses[$mes]."-".$anio, 0, 1, 'L');
	
				$this->Ln(3);
				
				$this->SetFont('Arial', '', 10);
				
				$this->Cell($Dim[0], $h + 1, "Item", '1', 0, 'C');
				$this->Cell($Dim[1], $h + 1, utf8_decode("Descripción de Variables"), 1, 0, 'C');
				$this->Cell($Dim[2], $h + 1, utf8_decode("N° Usuarios"), 1, 0, 'C');
				$this->Cell($Dim[3], $h + 1, "Monto S/.", 1, 1, 'C');
			}
			else
			{
				$this->SetX(67);
				$this->Cell(10, $h, "PERIODO", 0, 0, 'R');
				$this->Cell(5, $h, ":", 0, 0, 'C');
				$this->Cell(30, $h, $meses[$mes]."-".$anio, 0, 1, 'L');
	
				$this->Ln(2);
			}

		}

		function agregar_detalle($codsuc, $codciclo)
		{
			global $conexion, $Dim, $anio, $mes, $sector, $ruta, $desde, $hasta, $periodo, $objReporte, $Resumen;

			$h = 8;
			
			if ($sector <> '%')
			{
				$sector = " AND cli.codsector = ".$sector." ";
			}
			else
			{
				$sector = "";
			}
			if ($ruta <> '%')
			{
				$ruta = " AND cli.codrutlecturas = ".$ruta." ";
			}
			else
			{
				$ruta = "";
			}
			
			$this->SetTextColor(0, 0, 0);
			
		//Total Usuarios
			$Sql = "SELECT COUNT(ccta.nroinscripcion) AS Total, ";
			$Sql .= " SUM(ccta.impdeuda + ccta.impmes + ccta.impigv) AS Importe ";
			
			$Sql .= "FROM catastro.clientes cli ";
			$Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
			$Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
			$Sql .= " INNER JOIN public.calles ca ON (cli.codemp = ca.codemp) AND (cli.codsuc = ca.codsuc) AND (cli.codzona = ca.codzona) AND (cli.codcalle = ca.codcalle) ";
			$Sql .= " INNER JOIN public.tiposcalle tca ON (ca.codtipocalle = tca.codtipocalle) ";
			$Sql .= " INNER JOIN facturacion.tarifas tar ON (ccta.codemp = tar.codemp) AND (ccta.codsuc = tar.codsuc) AND (ccta.catetar = tar.catetar) AND (tar.estado = 1) ";
			
			$Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
			$Sql .= " AND ccta.codciclo = ".$codciclo." ";
			$Sql .= $sector;
			$Sql .= $ruta;
			$Sql .= " AND ccta.periodo = '".$periodo."' ";
			$Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
			$Sql .= " AND ccta.tipo = 0 ";

			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$item = $consulta->fetch();
			
			$borde = 0;
						
			$this->SetFillColor(255, 255, 255); //Color de Fondo
			$this->SetTextColor(0);

			$this->Cell($Dim[0], $h, "1", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Total Usuarios Morosos", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, $item['total'], $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, number_format($item['importe'], 2), $borde, 1, 'R', true);
			
			$MontoRecuperar = $item['importe'];
			
		//Nuevos Morosos
			$Sql = "SELECT COUNT(ccta.nroinscripcion) AS Total, ";
			$Sql .= " SUM(ccta.impdeuda + ccta.impmes + ccta.impigv) AS Importe ";
			
			$Sql .= "FROM catastro.clientes cli ";
			$Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
			$Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
			$Sql .= " INNER JOIN public.calles ca ON (cli.codemp = ca.codemp) AND (cli.codsuc = ca.codsuc) AND (cli.codzona = ca.codzona) AND (cli.codcalle = ca.codcalle) ";
			$Sql .= " INNER JOIN public.tiposcalle tca ON (ca.codtipocalle = tca.codtipocalle) ";
			$Sql .= " INNER JOIN facturacion.tarifas tar ON (ccta.codemp = tar.codemp) AND (ccta.codsuc = tar.codsuc) AND (ccta.catetar = tar.catetar) AND (tar.estado = 1) ";
			
			$Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
			$Sql .= " AND ccta.codciclo = ".$codciclo." ";
			$Sql .= $sector;
			$Sql .= $ruta;
			$Sql .= " AND ccta.periodo = '".$periodo."' ";
			$Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
			$Sql .= " AND ccta.tipo = 0 ";
			$Sql .= " AND ccta.nroinscripcion NOT IN(";
			
				if ($mes == 1)
				{
					$anioA = $anio - 1;
					$mesA = 12;
				}
				else
				{
					$anioA = $anio;
					$mesA = $mes - 1;
				}
				
				$periodoA = $anioA.$mesA;
				
				$Sql .= "SELECT ccta.nroinscripcion ";
				
				$Sql .= "FROM catastro.clientes cli ";
				$Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
				$Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
				$Sql .= " INNER JOIN public.calles ca ON (cli.codemp = ca.codemp) AND (cli.codsuc = ca.codsuc) AND (cli.codzona = ca.codzona) AND (cli.codcalle = ca.codcalle) ";
				$Sql .= " INNER JOIN public.tiposcalle tca ON (ca.codtipocalle = tca.codtipocalle) ";
				$Sql .= " INNER JOIN facturacion.tarifas tar ON (ccta.codemp = tar.codemp) AND (ccta.codsuc = tar.codsuc) AND (ccta.catetar = tar.catetar) AND (tar.estado = 1) ";
				
				$Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
				$Sql .= " AND ccta.codciclo = ".$codciclo." ";
				$Sql .= $sector;
				$Sql .= $ruta;
				$Sql .= " AND ccta.periodo = '".$periodoA."' ";
				$Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
				$Sql .= " AND ccta.tipo = 0 ";
			
			$Sql .= ") ";
			

			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$item = $consulta->fetch();

			$this->Cell($Dim[0], $h, "2", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Nuevos Morosos del Mes", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, $item['total'], $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, number_format($item['importe'], 2), $borde, 1, 'R', true);
			
		//Cartas Preparadas
			$this->Cell($Dim[0], $h, "3", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Cartas Preparadas", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, 0, $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, "", $borde, 1, 'R', true);
			
		//Cartas Entregadas
			$this->Cell($Dim[0], $h, "4", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Cartas Entregadas", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, 0, $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, "", $borde, 1, 'R', true);
			
		//Cartas No Entregadas
			$this->Cell($Dim[0], $h, "5", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Cartas No Entregadas", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, 0, $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, "", $borde, 1, 'R', true);
			
		//Monto por Recuperar
			$this->Cell($Dim[0], $h, "6", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Monto Total x Recuperar", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, "", $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, number_format($MontoRecuperar, 2), $borde, 1, 'R', true);
			
		//Monto Cobrado
			$Sql = "SELECT COUNT(nroinscripcion) AS Total, SUM(Importe) AS Importe FROM(";
			$Sql .= "SELECT ccta.nroinscripcion, SUM(dp.importe) AS Importe ";
			
			$Sql .= "FROM catastro.clientes cli ";
			$Sql .= " INNER JOIN cobranza.cabpagos cp ON (cli.codemp = cp.codemp) AND (cli.codsuc = cp.codsuc) AND (cli.nroinscripcion = cp.nroinscripcion) ";
			$Sql .= " INNER JOIN cobranza.detpagos dp ON (cp.codemp = dp.codemp) AND (cp.codsuc = dp.codsuc) AND (cp.nroinscripcion = dp.nroinscripcion) AND (cp.nropago = dp.nropago) ";
			$Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
			
			$Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
			$Sql .= " AND ccta.codciclo = ".$codciclo." ";
			$Sql .= $sector;
			$Sql .= $ruta;
			$Sql .= " AND ccta.periodo = '".$periodo."' ";
			$Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
			$Sql .= " AND ccta.tipo = 0 ";
			$Sql .= " AND cp.anio = '".$anio."' ";
			$Sql .= " AND cp.mes = '".str_pad('0', 2, $mes,  STR_PAD_RIGHT)."' ";
			$Sql .= " AND cp.anulado = 0 ";
			$Sql .= " AND dp.codconcepto NOT IN(10) ";
			$Sql .= "GROUP BY ccta.nroinscripcion) pago";
			
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$item = $consulta->fetch();

			$this->Cell($Dim[0], $h, "7", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Monto Cobrado x Recibos", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, $item['total'], $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, number_format($item['importe'], 2), $borde, 1, 'R', true);
			
			$ImporteRec = $item['importe'];
			
		//Monto Cobrado x Cuota Inicial
			$Sql = "SELECT COUNT(nroinscripcion) AS Total, SUM(Importe) AS Importe FROM(";
			$Sql .= "SELECT ccta.nroinscripcion, SUM(dp.importe) AS Importe ";
			
			$Sql .= "FROM catastro.clientes cli ";
			$Sql .= " INNER JOIN cobranza.cabpagos cp ON (cli.codemp = cp.codemp) AND (cli.codsuc = cp.codsuc) AND (cli.nroinscripcion = cp.nroinscripcion) ";
			$Sql .= " INNER JOIN cobranza.detpagos dp ON (cp.codemp = dp.codemp) AND (cp.codsuc = dp.codsuc) AND (cp.nroinscripcion = dp.nroinscripcion) AND (cp.nropago = dp.nropago) ";
			$Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
			
			$Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
			$Sql .= " AND ccta.codciclo = ".$codciclo." ";
			$Sql .= $sector;
			$Sql .= $ruta;
			$Sql .= " AND ccta.periodo = '".$periodo."' ";
			$Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
			$Sql .= " AND ccta.tipo = 0 ";
			$Sql .= " AND cp.anio = '".$anio."' ";
			$Sql .= " AND cp.mes = '".str_pad('0', 2, $mes,  STR_PAD_RIGHT)."' ";
			$Sql .= " AND cp.anulado = 0 ";
			$Sql .= " AND dp.codconcepto IN(10) ";
			$Sql .= "GROUP BY ccta.nroinscripcion) pago";
			
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$item = $consulta->fetch();

			$this->Cell($Dim[0], $h, "8", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Monto Cobrado x Cuota Inicial", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, $item['total'], $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, number_format($item['importe'], 2), $borde, 1, 'R', true);
			
			$ImporteRec += $item['importe'];
			
		//Monto Cobrado x Cuota Inicial
			$this->Cell($Dim[0], $h, "9", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Monto Total Recuperado en S/.", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, "", $borde, 0, 'C', true);
			
			$this->SetFont('Arial', 'B', 10);
			
			$this->Cell($Dim[3], $h, number_format($ImporteRec, 2), $borde, 1, 'R', true);
			
			$this->SetFont('Arial', '', 10);
			
		//Monto Rebajado x Reclamo
			$Sql = "SELECT COUNT(codantiguo) AS Total, SUM(Importe) AS Importe FROM(";
			$Sql .= "SELECT cli.codantiguo, ";
			$Sql .= " SUM(dre.imprebajado) AS Importe ";
			$Sql .= "FROM catastro.clientes cli ";
			$Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
			$Sql .= " INNER JOIN facturacion.cabrebajas AS reb ON (cli.nroinscripcion = reb.nroinscripcion) AND (cli.codemp = reb.codemp) AND (cli.codsuc = reb.codsuc) ";
			$Sql .= " INNER JOIN facturacion.detrebajas AS dre ON (dre.codemp = reb.codemp) AND (dre.codsuc = reb.codsuc) AND (dre.nrorebaja = reb.nrorebaja) ";
				
			$Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
			$Sql .= " AND ccta.codciclo = ".$codciclo." ";
			$Sql .= $sector;
			$Sql .= $ruta;
			$Sql .= " AND ccta.periodo = '".$periodo."' ";
			$Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
			$Sql .= " AND ccta.tipo = 0 ";
			$Sql .= " AND reb.codestrebaja <> 0 ";
			$Sql .= " AND EXTRACT(YEAR FROM reb.fechareg) = '".$anio."' ";
			$Sql .= " AND EXTRACT(MONTH FROM reb.fechareg) = '".intval($mes)."'";
			$Sql .= "GROUP BY cli.codantiguo) Rebaja";
			
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$item = $consulta->fetch();

			$this->Cell($Dim[0], $h, "10", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Monto Rebajado x Reclamo", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, $item['total'], $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, number_format($item['importe'], 2), $borde, 1, 'R', true);
			
			$ImporteReb += $item['importe'];
			
		//Monto Por Cobrar en Cuotas de Convenio
			$Sql = "SELECT COUNT(codantiguo) AS Total, SUM(Importe) AS Importe FROM(";
			$Sql .= "SELECT cli.codantiguo, ";
			$Sql .= " SUM(dre.importe) AS Importe ";
			$Sql .= "FROM catastro.clientes cli ";
			$Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
			$Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
			$Sql .= " INNER JOIN public.calles ca ON (cli.codemp = ca.codemp) AND (cli.codsuc = ca.codsuc) AND (cli.codzona = ca.codzona) AND (cli.codcalle = ca.codcalle) ";
			$Sql .= " INNER JOIN public.tiposcalle tca ON (ca.codtipocalle = tca.codtipocalle) ";
			$Sql .= " INNER JOIN facturacion.cabrefinanciamiento AS reb ON (cli.nroinscripcion = reb.nroinscripcion) AND (cli.codemp = reb.codemp) AND (cli.codsuc = reb.codsuc) ";
			$Sql .= " INNER JOIN facturacion.detrefinanciamiento AS dre ON (dre.codemp = reb.codemp) AND (dre.codsuc = reb.codsuc) AND (dre.nrorefinanciamiento = reb.nrorefinanciamiento) ";
				
			$Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
			$Sql .= " AND ccta.codciclo = ".$codciclo." ";
			$Sql .= $sector;
			$Sql .= $ruta;
			$Sql .= " AND ccta.periodo = '".$periodo."' ";
			$Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
			$Sql .= " AND ccta.tipo = 0 ";
			$Sql .= " AND reb.estareg <> 0 ";
			//$Sql .= " AND reb.fechaemision BETWEEN '".$anio."-".$mes."-01' AND '".$this->obtener_ultimo_dia_mes($mes, $anio)."' ";
			$Sql .= " AND EXTRACT(YEAR FROM reb.fechaemision) = '".$anio."' ";
			$Sql .= " AND EXTRACT(MONTH FROM reb.fechaemision) = '".intval($mes)."'";
			$Sql .= "GROUP BY cli.codantiguo) Rebaja";
			
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$item = $consulta->fetch();

			$this->Cell($Dim[0], $h, "11", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Monto Por Cobrar en Cuotas de Convenio", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, $item['total'], $borde, 0, 'C', true);
			$this->Cell($Dim[3], $h, number_format($item['importe'], 2), $borde, 1, 'R', true);
			
			$ImporteRef += $item['importe'];
			
		//Nuevo Soldo x Cobrar
			$this->Cell($Dim[0], $h, "12", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Nuevo Soldo x Cobrar", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, "", $borde, 0, 'C', true);
			
			$this->SetFont('Arial', 'B', 10);
			
			$this->Cell($Dim[3], $h, number_format($MontoRecuperar - $ImporteRec - $ImporteReb - $ImporteRef, 2), $borde, 1, 'R', true);
			
			$this->SetFont('Arial', '', 10);
			
		//Conexiones Cortadas AGUA
			$this->Cell($Dim[0], $h, "13", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Conexiones Cortadas AGUA", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, "0", $borde, 0, 'C', true);			
			$this->Cell($Dim[3], $h, "", $borde, 1, 'R', true);
			
		//Conexiones Levantadas AGUA
			$this->Cell($Dim[0], $h, "14", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Conexiones Levantadas AGUA", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, "0", $borde, 0, 'C', true);			
			$this->Cell($Dim[3], $h, "", $borde, 1, 'R', true);
			
		//Conexiones Cortadas DESAGUE
			$this->Cell($Dim[0], $h, "15", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Conexiones Cortadas DESAGUE", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, "0", $borde, 0, 'C', true);			
			$this->Cell($Dim[3], $h, "", $borde, 1, 'R', true);
			
		//Conexiones Levantadas DESAGUE
			$this->Cell($Dim[0], $h, "16", $borde, 0, 'C', true);
			$this->Cell($Dim[1], $h, "Conexiones Levantadas DESAGUE", $borde, 0, 'L', true);
			$this->Cell($Dim[2], $h, "0", $borde, 0, 'C', true);			
			$this->Cell($Dim[3], $h, "", $borde, 1, 'R', true);
		}
    }
	
	$objReporte	= new clsRegistroVentas();
	
	$codsuc		= $_GET["codsuc"];
	$ciclo		= $_GET["ciclo"];
	$sector		= $_GET["sector"];
	$ruta		= $_GET["ruta"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$desde		= $_GET["desde"];
	$hasta		= $_GET["hasta"];
	
	$periodo = $anio.$mes;
	
	$Resumen = 0;
	
	$Dim = array('0'=>20, '1'=>110, '2'=>30, '3'=>30);
	
	$objReporte->AliasNbPages();
	$objReporte->AddPage("P");

	$objReporte->agregar_detalle($codsuc, $ciclo);

	$objReporte->Output();

?>
