<?php
include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$TituloVentana = "Usuarios Morosos por Servicio";
$Activo = 1;

CuerpoSuperior($TituloVentana);

$codsuc = $_SESSION['IdSucursal'];
$objDrop = new clsDrop();
$sucursal = $objDrop->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_listadofacturacion.js" language="JavaScript"></script>
<script>
    jQuery(function($)
    {
        $("#DivTipos").buttonset();
        $("#desde").val(3);
        $("#hasta").val(9999);
    });

    var urldir = "<?php echo $_SESSION['urldir']; ?>"
    var codsuc = <?= $codsuc ?>;

    function pdf(Op)
    {
        var rutas = ""
        var sectores = ""

        if ($("#ciclo").val() == 0)
        {
            alert('Seleccione el Ciclo');

            $("#ciclo").focus();

            return false;
        }
        if ($("#anio").val() == 0)
        {
            alert('Seleccione el Anio a Consultar');

            $("#anio").focus();

            return false;
        }
        if ($("#mes").val() == 0)
        {
            alert('Seleccione el Mes a Consultar');

            $("#mes").focus();

            return false;
        }
        if ($("#todosectores").val() == 0)
        {
            sectores = $("#codsector").val();

            if (sectores == 0)
            {
                alert("Seleccione el Sector");

                $("#codsector").focus();

                return false;
            }
        }
        else
        {
            sectores = "%";
        }
        if ($("#todosrutas").val() == 0)
        {
            rutas = $("#rutaslecturas").val();

            if (rutas == 0)
            {
                alert("Seleccione la Ruta de Lectura");

                $("#rutaslecturas").focus();

                return false;
            }
        }
        else
        {
            rutas = "%";
        }

        if (parseInt($("#desde").val()) == 0 || $("#desde").val() == '')
        {
            alert('Ingrese el rango Inicial de meses de deuda!');

            $("#desde").focus();

            return false;
        }
        if (parseInt($("#hasta").val()) == 0 || $("#hasta").val() == '')
        {
            alert('Ingrese el rango Final de meses de deuda!');

            $("#hasta").focus();

            return false;
        }

        if (parseInt($("#desde").val()) > parseInt($("#hasta").val()))
        {
            alert('El rango Inicial de meses de deuda, no puede superar al Final!');

            $("#desde").focus();

            return false;
        }

        if (document.getElementById("optListadoG").checked == true)
        {
            url = "imprimir.php";
        }
        if (document.getElementById("optPagos").checked == true)
        {
            url = "imprimir_pagos.php";
        }
        if (document.getElementById("optRebajas").checked == true)
        {
            url = "imprimir_rebajas.php";
        }
        if (document.getElementById("oprRefinanciamientos").checked == true)
        {
            url = "imprimir_refinanciamientos.php";
        }
        if (document.getElementById("optResumenT").checked == true)
        {
            url = "imprimir_optListadoG.php";
        }

        url += "?codsuc=<?= $codsuc ?>&ciclo=" + $("#ciclo").val() + "&sector=" + sectores + "&ruta=" + rutas + "&anio=" + $("#anio").val() + "&mes=" + $("#mes").val() + "&desde=" + $("#desde").val() + "&hasta=" + $("#hasta").val();

        AbrirPopupImpresion(url, 800, 600)

        return false
    }

    function Cancelar()
    {
        location.href = '<?php echo $_SESSION['urldir']; ?>/admin/indexB.php'
    }

    function cargar_rutas_lecturas(obj, cond)
    {
        $.ajax({
            url: '../../../../ajax/rutas_lecturas_drop.php',
            type: 'POST',
            async: true,
            data: 'codsector=' + obj + '&codsuc=<?= $codsuc ?>&condicion=' + cond,
            success: function(datos) {
                $("#div_rutaslecturas").html(datos)
                $("#chkrutas").attr("checked", true)
                $("#todosrutas").val(1)
            }
        })
    }

    function validar_sectores(obj, idx)
    {
        $("#rutaslecturas").attr("disabled", true)
        $("#chkrutas").attr("checked", true)
        $("#todosrutas").val(1)

        if (obj.checked)
        {
            $("#" + idx).val(1)
            $("#codsector").attr("disabled", true)
        } else {
            $("#" + idx).val(0)
            $("#codsector").attr("disabled", false)
        }
    }
    function validar_rutas(obj, idx)
    {
        if (obj.checked)
        {
            $("#" + idx).val(1)
            $("#rutaslecturas").attr("disabled", true)
        } else {
            $("#" + idx).val(0)
            $("#rutaslecturas").attr("disabled", false)
        }
    }
</script>
<div>
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar; ?>" enctype="multipart/form-data">
        <fieldset>
            <table width="700" border="0" cellspacing="2" cellpadding="2">
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td width="130" align="right">Sucursal</td>
                    <td width="20" align="center">:</td>
                    <td>
                        <input type="text" name="sucursal" id="sucursal" value="<?= $sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                        <input type="hidden" name="todosrutas" id="todosrutas" value="1" />
                        <input type="hidden" name="todosectores" id="todosectores" value="1" />
                        <input type="hidden" name="orden" id="orden" value="1" />
                    </td>
                    <td width="100" align="right">Ciclo</td>
                    <td width="20" align="center">:</td>
                    <td>
                        <?php $objDrop->drop_ciclos($codsuc, 0, "onchange='datos_facturacion(this.value); cargar_anio(this.value,1);'"); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right">Sector</td>
                    <td align="center">:</td>
                    <td>
                        <?php echo $objDrop->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_lecturas(this.value,1);'"); ?></td>
                    <td  align="right">Todos los Sectores </td>
                    <td align="center">
                        <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosectores');
                                validar_sectores(this, 'todosectores');" />
                    </td>                    
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">Ruta</td>
                    <td align="center">:</td>
                    <td>
                        <div id="div_rutaslecturas">
                            <select name="rutaslecturas" id="rutaslecturas" style="width:220px" class="select" disabled="disabled">
                                <option value="0">--Seleccione la Ruta de Lectura--</option>
                            </select>
                        </div>
                    </td>
                    <td align="right">Todas las Rutas </td>                    
                    <td align="center">
                        <input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosrutas');
                                validar_rutas(this, 'todosrutas');" />
                    </td>
                    <td>&nbsp;</td>
                </tr>               
                <tr>
                    <td align="right">A&ntilde;o</td>
                    <td align="center">:</td>
                    <td colspan="2">
                        <div id="div_anio">
                            <?php $objDrop->drop_anio($codsuc, 0); ?>
                        </div>
                    </td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">Mes</td>
                    <td align="center">:</td>
                    <td colspan="2">
                        <div id="div_meses">
                            <?php $objDrop->drop_mes($codsuc, 0, 0); ?>
                        </div>
                    </td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">Meses de Deuda</td>
                    <td align="center">:</td>
                    <td colspan="2"><table width="230" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50" align="right">Desde</td>
                                <td width="20" align="center">:</td>
                                <td><input type="text" name="desde" id="desde" value="" class="inputtext numeric" style="width:50px; text-align:center;" /></td>
                                <td width="50" align="right">Hasta</td>
                                <td width="20" align="center">:</td>
                                <td><input type="text" name="hasta" id="hasta" value="" class="inputtext numeric" style="width:50px; text-align:center;" /></td>
                            </tr>
                        </table></td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>      
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <div id="DivTipos" style="display:inline">

                            <input type="radio" name="optReporte" id="optListadoG" value="radio" checked="checked" />
                            <label for="optListadoG">Listado General</label>&nbsp;
                            <input type="radio" name="optReporte" id="optPagos" value="radio5" />
                            <label for="optPagos">Usuarios que Pagaron</label>&nbsp;
                            <input type="radio" name="optReporte" id="optRebajas" value="radio2" />
                            <label for="optRebajas">Usuarios que Rebajaron</label>&nbsp;
                            <input type="radio" name="optReporte" id="optRefinanciamientos" value="radio3" />
                            <label for="optRefinanciamientos">Usuarios que Refianaciaron</label>&nbsp;
                            <input type="radio" name="optReporte" id="optResumenT" value="radio4" />
                            <label for="optResumenT">Resumen Final</label>

                        </div>	
                    </td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <div id="DivTipos" style="display:inline">
                            <input type="button" name="rabresumen" id="rptpdf" value="GENERAR EN PDF" onclick="pdf()"/>
                        </div>	
                      <!--<input type="button" onclick="return ValidarForm();" value="Generar" id="">-->                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
<div id="mostrar_datos"></div>
<script>
    $("#codsector").attr("disabled", true)
</script>
<?php CuerpoInferior(); ?>