<?php

set_time_limit(0);

include("../../../../objetos/clsReporte.php");

class clsRegistroVentas extends clsReporte {

    function cabecera() {
        global $mes, $anio, $meses, $Dim, $Resumen;

        $this->SetFont('Arial', 'B', 11);
        $this->SetY(15);
        $tit1 = "LISTADO GENERAL DE USUARIOS QUE PAGARON";
        $this->Cell(0, $h + 2, utf8_decode($tit1), 0, 1, 'C');

        $this->Ln(5);

        $h = 4;
        $this->SetFont('Arial', '', 7);
        $this->Cell(277, $h, $ciclotext, 0, 1, 'C');

        if ($Resumen == 0) {
            $this->SetX(113.5);
            $this->Cell(10, $h, "PERIODO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, $meses[$mes]."-".$anio, 0, 1, 'L');

            $this->Ln(2);
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);

            $this->Cell($Dim[0], $h, utf8_decode("N°"), '1', 0, 'C');
            $this->Cell($Dim[1], $h, "S", 'LTB', 0, 'C');
            $this->Cell($Dim[2], $h, "R", 'TB', 0, 'C');
            $this->Cell($Dim[3], $h, "Sec", 'TBR', 0, 'C');
            $this->Cell($Dim[4], $h, utf8_decode("Inscripción"), '1', 0, 'C');
            $this->Cell($Dim[5], $h, "Apellidos y Nombres", '1', 0, 'C');
            $this->Cell($Dim[6], $h, utf8_decode("Dirección"), '1', 0, 'C');
            $this->Cell($Dim[7], $h, "Imp. Pagado", '1', 0, 'C');
            $this->Cell($Dim[8], $h, "Fecha", '1', 0, 'C');
            $this->Cell($Dim[9], $h, "E/S", '1', 0, 'C');
            $this->Cell($Dim[10], $h, "Observaciones", '1', 1, 'C');
        } else {
            $this->SetX(67);
            $this->Cell(10, $h, "PERIODO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, $meses[$mes]."-".$anio, 0, 1, 'L');

            $this->Ln(2);
        }
    }

    function agregar_detalle($codsuc, $codciclo) {
        global $conexion, $Dim, $anio, $mes, $sector, $ruta, $desde, $hasta, $periodo, $objReporte, $Resumen;

        $h = 4;

        if ($sector <> '%') {
            $sector = " AND cli.codsector = ".$sector." ";
        } else {
            $sector = "";
        }
        if ($ruta <> '%') {
            $ruta = " AND cli.codrutlecturas = ".$ruta." ";
        } else {
            $ruta = "";
        }
        
        $mesp= substr("00", 0, 2 - strlen($mes)).$mes;
        
        $Sql = "SELECT cli.codsector, cli.codrutlecturas, con.orden_lect AS Secuencia, ";
        $Sql .= " cli.codantiguo, cli.propietario, ";
        $Sql .= " tca.descripcioncorta || ' ' || ca.descripcion || ' #' || cli.nrocalle AS Direccion, ";
        $Sql .= " cp.imptotal AS Importe, ";
        $Sql .= " cp.fechareg AS Fecha, ";
        $Sql .= " cp.glosa, ";
        $Sql .= " cp.nroinscripcion, ";
        $Sql .= " cli.codestadoservicio ";

        $Sql .= "FROM catastro.clientes cli ";
        $Sql .= " INNER JOIN cobranza.cabpagos cp ON (cli.codemp = cp.codemp) AND (cli.codsuc = cp.codsuc) AND (cli.nroinscripcion = cp.nroinscripcion) ";
        $Sql .= " INNER JOIN facturacion.cabctacorriente ccta ON (cli.codemp = ccta.codemp) AND (cli.codsuc = ccta.codsuc) AND (cli.codciclo = ccta.codciclo) AND (cli.nroinscripcion = ccta.nroinscripcion) ";
        $Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
        $Sql .= " INNER JOIN public.calles ca ON (cli.codemp = ca.codemp) AND (cli.codsuc = ca.codsuc) AND (cli.codzona = ca.codzona) AND (cli.codcalle = ca.codcalle) ";
        $Sql .= " INNER JOIN public.tiposcalle tca ON (ca.codtipocalle = tca.codtipocalle) ";

        $Sql .= "WHERE ccta.codsuc = ".$codsuc." ";
        $Sql .= " AND ccta.codciclo = ".$codciclo." ";
        $Sql .= $sector;
        $Sql .= $ruta;
        $Sql .= " AND ccta.periodo = '".$periodo."' ";
        $Sql .= " AND ccta.nromeses + 1 BETWEEN ".$desde." AND ".$hasta." ";
        $Sql .= " AND ccta.tipo = 0 ";
        $Sql .= " AND cp.anio = '".$anio."' ";
        $Sql .= " AND cp.mes = '" .$mesp. "' ";
        $Sql .= " AND cp.anulado = 0 ";
        //$Sql .= " AND ccta.nroinscripcion = 20739 ";

        $Sql .= "ORDER BY cli.codsector, cli.codrutlecturas, con.orden_lect ";
        //die($Sql); exit;

        $consulta = $conexion->prepare($Sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        $borde = 0;

        $tPago = 0;

        $this->SetFillColor(255, 255, 255); //Color de Fondo
        $this->SetTextColor(0);

        $count = 0;

        foreach ($items as $row) {
            $count++;

            $this->Cell($Dim[0], $h, $count, $borde, 0, 'C', true);
            $this->Cell($Dim[1], $h, $row['codsector'], $borde, 0, 'C', true);
            $this->Cell($Dim[2], $h, $row['codrutlecturas'], $borde, 0, 'C', true);
            $this->Cell($Dim[3], $h, $row['secuencia'], $borde, 0, 'C', true);
            $this->Cell($Dim[4], $h, $row['codantiguo'], $borde, 0, 'C', true);
            $this->Cell($Dim[5], $h, strtoupper(utf8_decode($row[4])), $borde, 0, 'L', true);
            $this->Cell($Dim[6], $h, strtoupper(utf8_decode($row[5])), $borde, 0, 'L', true);
            $this->Cell($Dim[7], $h, number_format($row[6], 2), $borde, 0, 'R', true);
            $this->Cell($Dim[8], $h, $this->DecFecha($row[7]), $borde, 0, 'C', true);

            if ($row['codestadoservicio'] == 1) {
                $EstadoServicio = 'ACT';
            }
            if ($row['codestadoservicio'] == 2) {
                $EstadoServicio = 'CORT';
            }

            $this->Cell($Dim[9], $h, $EstadoServicio, $borde, 0, 'C', true);
            $tPago += $row[6];

            $this->Cell($Dim[10], $h, $row[8], $borde, 1, 'L', true);
        }

        $this->SetFont('Arial', 'B', 6);

        $this->Cell(163, $h, "Total General ==>", "T", 0, 'R');
        $this->Cell($Dim[7], $h, number_format($tPago, 2), 'T', 0, 'R');
        $this->Cell($Dim[8], $h, '', 'T', 0, 'R');
        $this->Cell($Dim[9], $h, '', 'T', 0, 'R');
        $this->Cell($Dim[10], $h, '', 'T', 1, 'R');

        $Resumen = 1;

        $this->AddPage("P");
        $this->Ln(3);

        $this->SetFont('Arial', 'B', 10);
        $this->SetX(30);
        $this->Cell(70, $h, "RESUMEN", 0, 1, 'L');
        $this->Ln(3);
        $this->SetFont('Arial', 'B', 8);
        $this->SetX(30);
        $this->Cell(15, $h, "CODIGO", 0, 0, 'C');
        $this->Cell(80, $h, "DESCRIPCION", 0, 0, 'C');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, "IMPORTE", 'B', 1, 'C');
        //TOTALE
        $this->Ln(3);

        $Countt = 0;

        $SqlA = "SELECT CodCon, Descrip, Categ, ";
        $SqlA .= " SUM(Agua) AS Agua, ";
        $SqlA .= " SUM(Desague) AS Desague, ";
        $SqlA .= " SUM(Interes) AS Interes, ";
        $SqlA .= " SUM(Fijo) AS Fijo, ";
        $SqlA .= " SUM(Colateral) AS Colateral, ";
        $SqlA .= " SUM(ColateralI) AS ColateralI ";
        $SqlA .= "FROM (";

        $aUsuario = array();

        foreach ($items as $row) {
            $Countt++;

            if (!array_key_exists($row[9], $aUsuario)) {
                $aUsuario[$row[9]] = array("Id" => $row[9]);

                if ($Countt > 1) {
                    $SqlA .= " UNION ALL ";
                }

                $SqlA .= "(SELECT df.codconcepto AS CodCon, c.descripcion AS Descrip, c.categoria AS Categ, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto = 1 THEN df.importe ELSE 0 END) AS Agua, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto = 2 THEN df.importe ELSE 0 END) AS Desague, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto IN(3, 4, 9, 71, 72, 10013) THEN df.importe ELSE 0 END) AS Interes, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto = 6 THEN df.importe ELSE 0 END) AS Fijo, ";
                $SqlA .= " SUM(CASE WHEN df.codconcepto NOT IN(1, 2, 3, 4, 6, 9, 71, 72, 10013) THEN df.importe ELSE 0 END) AS Colateral, ";
                $SqlA .= " 0 AS ColateralI ";
                $SqlA .= "FROM cobranza.cabpagos cf ";
                $SqlA .= " INNER JOIN cobranza.detpagos df ON (cf.codemp = df.codemp) AND (cf.codsuc = df.codsuc) AND (cf.nropago = df.nropago) AND (cf.nroinscripcion = df.nroinscripcion) ";
                $SqlA .= " INNER JOIN facturacion.conceptos c ON (df.codemp = c.codemp) AND (df.codsuc = c.codsuc) AND (df.codconcepto = c.codconcepto) ";
                $SqlA .= "WHERE cf.codsuc = ".$codsuc." ";
                $SqlA .= " AND cf.codciclo = ".$codciclo." ";
                $SqlA .= " AND cf.anio = '".$anio."' ";
                $SqlA .= " AND cf.mes = '" .$mes."' ";
                $SqlA .= " AND cf.nroinscripcion = ".$row[9]." ";
                $SqlA .= " AND cf.anulado = 0 ";
                $SqlA .= "GROUP BY df.codconcepto, c.descripcion, c.categoria) ";
                //die($SqlA);
            }
        }
        $SqlA .= ") Deudores ";
        $SqlA .= "GROUP BY CodCon, Descrip, Categ ";
        $SqlA .= "ORDER BY Categ, CodCon ";
        //die($SqlA);

        $consulta = $conexion->prepare($SqlA);
        $consulta->execute();
        $items = $consulta->fetchAll();

        $Total = 0;
        $TotalI = 0;
        $TotalRP = 0;
        $TotalRN = 0;

        $this->SetFont('Arial', '', 8);

        foreach ($items as $row) {
            $Imp = 0;

            switch ($row[0]) {
                case 1:
                    $Imp = $row['agua'];

                    break;

                case 2:
                    $Imp = $row['desague'];

                    break;

                case 6:
                    $Imp = $row['fijo'];

                    break;

                case 7 or 8:
                    $Imp = $row['colateral'];

                    break;

                default:
                    $Imp = $row['colateral'];

                    break;
            }

            if ($row[2] == 3) {
                $Imp = $row['interes'];
            }

            if ($row[2] <> 7 and $row[2] <> 4) {
                $this->SetX(30);
                $this->Cell(15, $h, $row[0], 0, 0, 'C');
                $this->Cell(80, $h, $row[1], 0, 0, 'L');
                $this->Cell(5, $h, "", 0, 0, 'C');
                $this->Cell(30, $h, number_format($Imp, 2), 0, 1, 'R');

                $this->Ln(2);

                $Total += $Imp;

                if ($row['colaterali'] > 0) {
                    $TotalI += $row['colaterali'];
                }
            } else {
                if ($row[0] == 5) {
                    $TotalI += $Imp;
                }
                if ($row[0] == 7) {
                    $TotalRN += $Imp;
                }
                if ($row[0] == 8) {
                    $TotalRP += $Imp;
                }
            }
        }

        $this->SetX(30);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(95, $h, "SUB TOTAL :", 0, 0, 'R');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($Total, 2), 'T', 1, 'R');

        $this->Ln(2);

        $this->SetFont('Arial', '', 8);

        $this->SetX(30);
        $this->Cell(15, $h, 5, 0, 0, 'C');
        $this->Cell(80, $h, 'I.G.V.', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($TotalI, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->Cell(15, $h, 7, 0, 0, 'C');
        $this->Cell(80, $h, 'REDONDEO NEGATIVO', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($TotalRN, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->Cell(15, $h, 8, 0, 0, 'C');
        $this->Cell(80, $h, 'REDONDEO POSITIVO', 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($TotalRP, 2), 0, 1, 'R');

        $this->Ln(2);

        $this->SetX(30);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(95, $h, "TOTAL :", 0, 0, 'R');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(30, $h, number_format($Total + $TotalI + $TotalRN + $TotalRP, 2), 'T', 1, 'R');
    }

}

$objReporte = new clsRegistroVentas();

$codsuc = $_GET["codsuc"];
$ciclo = $_GET["ciclo"];
$sector = $_GET["sector"];
$ruta = $_GET["ruta"];
$anio = $_GET["anio"];
$mes = $_GET["mes"];
$desde = $_GET["desde"];
$hasta = $_GET["hasta"];

$periodo = $anio.$mes;

$Resumen = 0;

$Dim = array('0' => 8, '1' => 5, '2' => 5, '3' => 10, '4' => 15, '5' => 65, '6' => 55, '7' => 17, '8' => 15, '9' => 7, '10' => 75);

$objReporte->AliasNbPages();
$objReporte->AddPage("L");

$objReporte->agregar_detalle($codsuc, $ciclo);

$objReporte->Output();

?>