<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "PAGOS";
	$Activo = 1;

	CuerpoSuperior($TituloVentana);

	$Op = isset($_GET['Op'])?$_GET['Op']:0;

	$codsuc = $_SESSION['IdSucursal'];
	$FormatoGrilla = array ();
	$Fecha = isset($_GET["Fecha"])?$_GET["Fecha"]:date('d/m/Y');
  // $and =" AND cab.fechareg='".$objFunciones->CodFecha($Fecha)."' AND cab.creador=".$_SESSION['id_user'];
  // $and = " AND cab.fechareg='".$objFunciones->CodFecha($Fecha)."'";

	$Sql = "SELECT
				cab.nropago,
				clie.codantiguo,
				upper(car.descripcion),
				upper(cab.propietario),
				upper(caj.descripcion) AS caja,
				upper(usu.login) AS cajera,
				".$objFunciones->FormatFecha('cab.fechareg').",
				".$objFunciones->SubString('cab.hora',1,7)." ,
				".$objFunciones->Convert("cab.imptotal","NUMERIC (18,2)").",
				upper(forms.descripcion),
				CASE WHEN cab.condpago=0 THEN 'CONTADO' ELSE 'CREDITO' END,
				cab.nropec,
				cab.nroprepago,
				cab.anulado,
				1,
				cab.nroinscripcion,
				cab.car,
				cab.nrocaja,
				cab.creador,
				cab.codformapago,
				cab.fechareg,
				CASE WHEN cab.nroinspeccion = 0 THEN 0 ELSE 1 END,
				cab.nroinspeccion
			FROM cobranza.cabpagos AS cab
				INNER JOIN cobranza.car car ON (cab.car=car.car)
				INNER JOIN cobranza.cajas caj ON (cab.nrocaja=caj.nrocaja)
				INNER JOIN public.formapago AS forms ON (cab.codformapago=forms.codformapago)
				INNER JOIN seguridad.usuarios AS usu ON (usu.codusu=cab.creador AND usu.codemp=cab.codemp AND usu.codsuc=cab.codsuc)
				LEFT JOIN catastro.clientes clie ON (cab.nroinscripcion = clie.nroinscripcion) ";

	$FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
	$FormatoGrilla[1] = array('1'=>'cab.nropago', '2'=>'car.descripcion', '3'=>'cab.propietario',
                      '4'=>'forms.descripcion', '5'=>'cab.nropec', '6'=>'cab.nroprepago',
					  '7'=>'cab.nroinscripcion', '8'=>' cab.nroinscripcion', '9'=>' clie.codantiguo');          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1'=>'Pago', 'T2'=>'Nro Inscripción', 'T3'=>'Car', 'T4'=>'Propietario', 'T5'=>'Caja', 'T6'=>'Login',
                        'T7'=>'Fecha de Pago', 'T8'=>'Hora', 'T9'=>'Importe', 'T10'=>'Forma Pago',
                          'T11'=>'Cond. Pago', 'T12'=>'Pec', 'T13'=>'Pre-Pago');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'left', 'A4'=>'left', 'A5'=>'center', 'A6'=>'left',
						'A7'=>'center', 'A8'=>'center', 'A9'=>'right', 'A10'=>'center', 'A11'=>'center', 'A12'=>'center', 'A13'=>'center');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1'=>'30', 'W2'=>'60', 'W3'=>'120', 'W4'=>'180', 'W5'=>'50', 'W6'=>'10', 'W7'=>'50', 'W8'=>'60',
						'W9'=>'60', 'W10'=>'15', 'W11'=>'50', 'W12'=>'20', 'W13'=>'20', 'W14'=>'20');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 1200;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = "  AND (cab.codemp=1 AND cab.codsuc=".$codsuc.")". $and ." ORDER BY cab.nropago DESC ";                                   //Orden de la Consulta
	$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'4',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'imprimir.png',   //Imagen a mostrar
              'Btn1'=>'Imprimir Recibo de Pago',       //Titulo del Botón
              'BtnF1'=>'onclick="imprimirrecibo(this);"',  //Eventos del Botón
              'BtnCI1'=>'15',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
							/*
              'BtnId2'=>'BtnModificar',   //Nombre del Boton
              'BtnI2'=>'imprimir.png',   //Imagen a mostrar
              'Btn2'=>'Imprimir Sustitutorio de Pago',       //Titulo del Botón
              'BtnF2'=>'onclick="imprimirsustitutorio(this);"',  //Eventos del Botón
              'BtnCI2'=>'15',  //Item a Comparar
              'BtnCV2'=>'1',    //Valor de comparación
							*/
              'BtnId2'=>'BtnModificar',   //Nombre del Boton
              'BtnI2'=>'imprimir.png',   //Imagen a mostrar
              'Btn2'=>'Imprimir Comprobante',       //Titulo del Botón
              'BtnF2'=>'onclick="imprimirticket(this);"',  //Eventos del Botón
              'BtnCI2'=>'15',  //Item a Comparar
              'BtnCV2'=>'1',    //Valor de comparación
              'BtnId3'=>'BtnModificar',   //Nombre del Boton
              'BtnI3'=>'imprimir.png',   //Imagen a mostrar
              'Btn3'=>'Imprimir Solicitud',       //Titulo del Botón
              'BtnF3'=>'onclick="imprimirSolicitud(this);"',  //Eventos del Botón
              'BtnCI3'=>'22',  //Item a Comparar
              'BtnCV3'=>'1',    //Valor de comparación
			  			'BtnId4'=>'BtnModificar',   //Nombre del Boton
              'BtnI4'=>'imprimir.png',   //Imagen a mostrar
              'Btn4'=>'Imprimir Ticket',       //Titulo del Botón
              'BtnF4'=>'onclick="imprimirticket2(this);"',  //Eventos del Botón
              'BtnCI4'=>'15',  //Item a Comparar
              'BtnCV4'=>'1',    //Valor de comparación
              );
	$FormatoGrilla[10] = array(array('Name' =>'nroinspeccion', 'Col'=>23),
								array('Name' =>'nroinscripcion', 'Col'=>16),
								array('Name' =>'fechapago','Col'=>21),
								array('Name' =>'codpago','Col'=>1));//DATOS ADICIONALES

	$FormatoGrilla[11] = 11;//FILAS VISIBLES
	$FormatoGrilla[12] = array(array('Col' => '14', 'Estado' => 1, 'Color' => '#FF4500'));//ESTADO Registro

	$_SESSION['Formato'] = $FormatoGrilla;

	$Previo=' <table width="100%" border="0" >';
	$Previo.='<tr><td valign="middle" align="left" width="65">Fecha :</td><td valign="middle" align="left">';
	$Previo.= '<input type="text" class="inputtext ui-corner-all text" name="3form1_fecha" id="Fecha" value="'.$Fecha.'" readonly="readonly" style="width:100px" onchange="BuscarB('.$Op.')"/>';
	$Previo.='</td></tr></table>';

	Cabecera("", $FormatoGrilla[7], 1000, 600);

	Pie();
	CuerpoInferior();
?>
<script>
	function ValidarEnter(e, Op)
	{
        switch(e.keyCode)
		{
			case $.ui.keyCode.UP:
				for (var i = 10; i >= 1; i--)
				{
					if ($('tr[tabindex=\'' + i  + '\']') != null )
					{
						var obj = $('tr[tabindex=\'' +i  + '\']');
						if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
						{
							$('tr[tabindex=\'' + i  + '\']').focus();
							e.preventDefault();
							return false;
							break;
						}

					}
				};
				$('#Valor').focus().select();
                break;

			case $.ui.keyCode.DOWN:
				for (var i = 1; i <= 10; i++)
				{
					if ($('tr[tabindex=\'' + i  + '\']') != null )
					{
						var obj = $('tr[tabindex=\'' +i  + '\']');
						if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
						{
							$('tr[tabindex=\'' + i  + '\']').focus();
							e.preventDefault();
							return false;
 							break;
						}
					}
				};
				$('#Valor').focus().select();
				break;

			default:  if (VeriEnter(e)) Buscar(Op);
		}
		//ValidarEnterG(evt, Op)
	}

	function BuscarB(Op)
	{
		var Valor = document.getElementById('Valor').value;
		var Fecha = document.getElementById('Fecha').value;
		var Op2 = '';
		if (Op != 0)
		{
		  Op2 = '&Op=' + Op;
		}
		location.href = 'index.php?Valor=' + Valor + '&pagina=' + Pagina + Op2 + '&Fecha=' + Fecha;
	}

	$(document).ready(function(){
// $("#BtnRegresar").show();
  		$("#Fecha").datepicker(
		{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true,
			beforeShow: function(input, inst)
			{
				inst.dpDiv.css({marginTop: (input.offsetHeight ) - 20  + 'px', marginLeft: (input.offsetWidth) - 90 + 'px' });
			}
		});

		$("#BtnNuevoB").hide();
		$("#DivUpdate").dialog({
			autoOpen: false,
			modal: true,
			width: 400,
			height:300,
			resizable: true,
			show: "scale",
			hide: "scale",
			close: function() {

		    },
			buttons: {
				"Actualizar": function() { Actualizar(); } ,
				"Cancelar": function() { $(this).dialog("close"); }
			}
		   //
		 });
	});

	function SeleccionaIdB(obj, nropec, estado)
	{
		Id 		= 'Id=' + obj.id;
		IdAnt	= Id2;
		Id2 	= obj.id;
		pec		= nropec;
		est		= estado;
		if (IdAnt != '' && estant == 0)
		{
			Limpiar(IdAnt);
		}
		if(est==0)
		{
			$("#" + obj.id).addClass("ui-state-active");
		}
		estant = est
	}

	function imprimirrecibo(obj)
	{
		var nroinscripcion = $(obj).parent().parent().data('nroinscripcion');
		var codciclo = 1;
		var fechapago = $(obj).parent().parent().data('fechapago');
		var codpago = $(obj).parent().parent().data('codpago');

		AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/cobranza/operaciones/cancelacion/consulta/imprimir_recibo.php?nroinscripcion=" + nroinscripcion + '&codsuc=<?=$codsuc?>&codciclo=' + codciclo + '&fechapago=' + fechapago + '&codpago=' + codpago, 800, 600);
	}

	function imprimirsustitutorio(obj)
	{
		var nroinscripcion = $(obj).parent().parent().data('nroinscripcion');
		var codciclo = 1;
		var fechapago = $(obj).parent().parent().data('fechapago');
		var codpago = $(obj).parent().parent().data('codpago');

		AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/cobranza/operaciones/cancelacion/consulta/imprimir_sustitutorio.php?nroinscripcion=" + nroinscripcion + '&codsuc=<?=$codsuc?>&codciclo=' + codciclo + '&fechapago=' + fechapago + '&codpago=' + codpago, 800, 600);
	}

	function imprimirticket(obj)
	{
		var nroinscripcion = $(obj).parent().parent().data('nroinscripcion');
		var codciclo = 1;
		var fechapago = $(obj).parent().parent().data('fechapago');
		var codpago = $(obj).parent().parent().data('codpago');

		AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_pago.php?nroinscripcion=" + nroinscripcion + '&codsuc=<?=$codsuc?>&codciclo=' + codciclo + '&fechapago=' + fechapago + '&codpago=' + codpago, 800, 600);
	}

	function imprimirSolicitud(obj)
	{
		var nroinspeccion = $(obj).parent().parent().data('nroinspeccion');
		var codciclo = 1;
		var fechapago = $(obj).parent().parent().data('fechapago');
		var codpago = $(obj).parent().parent().data('codpago');

		AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/catastro/operaciones/solicitud/imprimir.php?nrosolicitud=" + nroinspeccion + '&codsuc=<?=$codsuc?>', 800, 600);
	}

	function imprimirticket2(obj)
	{
		var nroinscripcion = $(obj).parent().parent().data('nroinscripcion');
		var codciclo = 1;
		var fechapago = $(obj).parent().parent().data('fechapago');
		var codpago = $(obj).parent().parent().data('codpago');

		AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?nroinscripcion=" + nroinscripcion + '&codsuc=<?=$codsuc?>&codciclo=' + codciclo + '&fechapago=' + fechapago + '&codpago=' + codpago, 800, 600);
	}

	function Operacion(Op)
	{
		if(Op!=4)
		{
			var url = 'mantenimiento.php?Op=' + Op;
			if(Op!=1)
			{
				if(Id=="")
				{
					alert('Seleccione un Elemento del Item');
					return;
				}
				url = url + '&' + Id;
			}
			location.href = url;
		}
		else
		{
			location.href = '../index.php';
		}
	}

	var Crador = '';
	function ActualizarPago(nropago, importe, propietario, car, nrocaja, creador, nroinscripcion, codformapago)
	{
		$("#NroPago").val(nropago);
		$("#Importe").val(importe);
		$("#Propietario").val(propietario);
		$("#car").val(car);
		$("#caja").val(nrocaja);
		$("#nroinscripcion").val(nroinscripcion);
		$("#formapago").val(codformapago);
		Crador = creador;
		cUsuarios();
		$("#DivUpdate").dialog('open');
	}

	function cUsuarios()
	{
	  	//return false;
		var IdCaja = $("#caja").val()
		$.ajax({
		  url:'cCajeros.php',
		  type:'POST',
		  async:true,
		  data:'IdCaja=' + IdCaja+'&Crador='+Crador,
		  success:function(datos)
		  {
			  $("#Cajero").html(datos);

		  }
		})
	}
function Actualizar()
{
var obj = $("#Propietario");
  if(obj.val()=="")
  {
    Msj(obj,'Ingrese Propietario',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
  obj = $("#car");
  if(obj.val()=="0")
  {
    Msj(obj,'Seleccione Car',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
   obj = $("#caja");
  if(obj.val()=="0")
  {
    Msj(obj,'Seleccione Caja',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
obj = $("#Cajero");
  if(obj.val()=="")
  {
    Msj(obj,'Seleccione Cajero',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
  obj = $("#formapago");
  if(obj.val()=="0")
  {
    Msj(obj,'Seleccione Forma Pago',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
	var Data = $("#form1").serialize();
    $.ajax({
      url:'Actualizar.php',
      type:'POST',
      async:true,
      data:Data,
      success:function(datos)
      {
          if(datos==0)
          {
          	window.parent.OperMensaje('Pago Actualizado Correctamente',1)
          	window.location.assign('index.php')
          }
     	else{
     		window.parent.OperMensaje('Pago Actualizado Correctamente',1)
     	}
      }
    })
}
</script>
<div id="frmpop2" style="display:none; width:700px">
  <div style="width:100%;" class="barra" >
  		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr style="padding:4px">
            <td  style="color:#FFF; font-size:12px; font-weight:bold;">Ingresos en Caja</td>
            <td align="right">
            	<img onClick="close_frmpop();" src="<?php echo $_SESSION['urldir'];?>images/iconos/Close.gif" height="15" width="15" style="cursor:pointer" />
            </td>
          </tr>
        </table>
  </div>
  <table width="100%" bgcolor="#EEEEEE">
       <tr>
		<td width="100%" align="center" valign="middle" class="tdconfir" id="btn_conf">
			<div id="div_detalle"></div>
		</td>
    </tr>
    </table>
</div>
<div id="DivUpdate" title="Actualizar Datos del Pago" style="display: none; font-size:14; font-weight:bold">
	<form id="form1" name="form1">
  <table border="0" cellspacing="0" cellpadding="0" width="100%" >
    <tr>
        <td class="TitDetalle">Nro Pago :</td>
        <td class="CampoDetalle">
          <input readonly="readonly" type="text" class="text ui-corner-all" title="NroPago"  id="NroPago" name="NroPago" />
          <input readonly="readonly" type="hidden"  id="nroinscripcion" name="nroinscripcion" />
        </td>
      </tr>
      <tr>
        <td class="TitDetalle">Importe:</td>
        <td class="CampoDetalle">
          <input readonly="readonly" type="text" class="numeric text ui-corner-all" title="Importe"  id="Importe"/>
        </td>
      </tr>
      <tr>
        <td class="TitDetalle">Propietario :</td>
        <td class="CampoDetalle">
          <input  type="text" class="text2 ui-corner-all" title="Propietario"  id="Propietario" name="Propietario" readonly="readonly"/>
        </td>
      </tr>
      <tr>
        <td class="TitDetalle">Car:</td>
        <td class="CampoDetalle">
        <? echo $objMantenimiento->drop_car($codsuc,1); ?>

        </td>
      </tr>
     <tr>
        <td class="TitDetalle">Caja:</td>
        <td class="CampoDetalle">
        <? echo $objMantenimiento->drop_cajas($codsuc,'onchange="cUsuarios()"'); ?>

        </td>
      </tr>
       <tr >
        <td class="TitDetalle">Cajero:</td>
        <td class="CampoDetalle">
        <select id="Cajero"  class="requiere ui-corner-all text2" title="Tipo Documento" data-actual="<?=$IdTipoDocumento?>" name="Cajero">
        </select>
       	</td>
      </tr>
      <tr>
        <td class="TitDetalle">Forma:</td>
        <td class="CampoDetalle">
         <? $objMantenimiento->drop_forma_pago($formapago,""," where estareg=1 and tipodocumento=0"); ?>
       	</td>
      </tr>
  </table>
</div>
</form>
<?php CuerpoInferior();?>
