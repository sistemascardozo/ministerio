<?
	include('../../../../config.php');
	
	$conexion->beginTransaction();
	
	$NroFacturacion	= 199;
	$CodConcepto	= 806;	//15, 824
	
	//Actualizar Importe de Cortes y Reaperturas
	$SqlUCR = "UPDATE facturacion.detctacorriente ";
	$SqlUCR .= " SET importe = CAST((importe / 1.18) AS NUMERIC (18, 2)) ";
	$SqlUCR .= "WHERE nrofacturacion = ".$NroFacturacion." ";
	$SqlUCR .= " AND codconcepto = ".$CodConcepto;
	
	$result = $conexion->prepare($SqlUCR);
    $result->execute(array());
	
	$SqlUCR = "UPDATE facturacion.detfacturacion ";
	$SqlUCR .= " SET importe = CAST((importe / 1.18) AS NUMERIC (18, 2)) ";
	$SqlUCR .= "WHERE nrofacturacion = ".$NroFacturacion." ";
	$SqlUCR .= " AND codconcepto = ".$CodConcepto;
	
	$result = $conexion->prepare($SqlUCR);
    $result->execute(array());
	
	//Obtenemos los registros para actualizar importes
	$Sql = "SELECT DISTINCT nroinscripcion ";
	$Sql .= "FROM facturacion.detctacorriente ";
	$Sql .= "WHERE nrofacturacion = ".$NroFacturacion." ";
	$Sql .= " AND codconcepto = ".$CodConcepto;
	//$Sql .= " AND nroinscripcion <> 113699 ";
	
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array());
	$items = $consulta->fetchAll();
	
	foreach($items as $row)
	{
		$SqlD = "SELECT c.impmes + c.impigv, ";
		$SqlD .= " SUM(d.importe) AS IMPONIBLE, ";
		$SqlD .= " CAST((SUM(d.importe)*0.18) AS NUMERIC (18, 2)) AS IGV ";
		$SqlD .= "FROM facturacion.detctacorriente d ";
		$SqlD .= " INNER JOIN facturacion.cabctacorriente c ON(d.codsuc = c.codsuc) AND (d.nrofacturacion = c.nrofacturacion) AND (d.nroinscripcion = c.nroinscripcion)";
		$SqlD .= "WHERE d.codconcepto NOT IN(3, 4, 5, 7, 8, 9, 71, 72, 10013) ";
		$SqlD .= " AND d.nrofacturacion = ".$NroFacturacion." ";
		$SqlD .= " AND d.nroinscripcion = ".$row[0]." ";
		$SqlD .= "GROUP BY c.impmes, c.impigv ";
		
		$consultaD = $conexion->prepare($SqlD);
		$consultaD->execute(array());
		$rowD = $consultaD->fetch();
		
		$SqlD2 = "SELECT SUM(d.importe) AS NOIMPONIBLE ";
		$SqlD2 .= "FROM facturacion.detctacorriente d ";
		$SqlD2 .= "WHERE d.codconcepto IN(3, 4, 7, 8, 9, 71, 72, 10013) ";
		$SqlD2 .= " AND d.nrofacturacion = ".$NroFacturacion." ";
		$SqlD2 .= " AND d.nroinscripcion = ".$row[0]." ";
		
		$consultaD2 = $conexion->prepare($SqlD2);
		$consultaD2->execute(array());
		$rowD2 = $consultaD2->fetch();
		
		$Importe	= number_format($rowD[0], 2);
		$Imponible	= number_format($rowD[1], 2);
		$IGV		= number_format($rowD[2], 2);
		$NoImponible = number_format($rowD2[0], 2);
		
		$Redondeo	= number_format($Importe - ($Imponible + $IGV + $NoImponible), 2);
		
		echo str_pad($row[0], 6, "0", STR_PAD_LEFT)."|".str_pad($Imponible, 6, "0", STR_PAD_LEFT)."|".str_pad($NoImponible, 6, "0", STR_PAD_LEFT)."|".str_pad($IGV, 6, "0", STR_PAD_LEFT)."|".str_pad($Redondeo, 6, "0", STR_PAD_LEFT)."|".str_pad($Importe, 6, "0", STR_PAD_LEFT)."<br>";
		
		//Actualizar IGV CTA Corriente
		$SqlU = "UPDATE facturacion.detctacorriente ";
		$SqlU .= " SET importe = '".$IGV."' ";
		$SqlU .= "WHERE nrofacturacion = ".$NroFacturacion." ";
		$SqlU .= " AND nroinscripcion = ".$row[0]." ";
		$SqlU .= " AND codconcepto = 5 ";
		
		$result = $conexion->prepare($SqlU);
        $result->execute(array());
		
		if ($result->errorCode() != '00000') {
			$conexion->rollBack();
			$mensaje = "Error UPDATE detctacorriente<br>".$SqlU;
			die($mensaje);
		}
		
		$SqlU = "UPDATE facturacion.cabctacorriente ";
		$SqlU .= " SET impigv = '".$IGV."' ";
		$SqlU .= "WHERE nrofacturacion = ".$NroFacturacion." ";
		$SqlU .= " AND nroinscripcion = ".$row[0]." ";
		
		$result = $conexion->prepare($SqlU);
        $result->execute(array());
		
		if ($result->errorCode() != '00000') {
			$conexion->rollBack();
			$mensaje = "Error UPDATE cabctacorriente<br>".$SqlU;
			die($mensaje);
		}
		
		//Actualizar IGV Factiracion
		$SqlU = "UPDATE facturacion.detfacturacion ";
		$SqlU .= " SET importe = '".$IGV."' ";
		$SqlU .= "WHERE nrofacturacion = ".$NroFacturacion." ";
		$SqlU .= " AND nroinscripcion = ".$row[0]." ";
		$SqlU .= " AND codconcepto = 5 ";
		
		$result = $conexion->prepare($SqlU);
        $result->execute(array());
		
		if ($result->errorCode() != '00000') {
			$conexion->rollBack();
			$mensaje = "Error UPDATE detfacturacion<br>".$SqlU;
			die($mensaje);
		}
		
		$SqlU = "UPDATE facturacion.cabfacturacion ";
		$SqlU .= " SET impigv = '".$IGV."' ";
		$SqlU .= "WHERE nrofacturacion = ".$NroFacturacion." ";
		$SqlU .= " AND nroinscripcion = ".$row[0]." ";
		
		$result = $conexion->prepare($SqlU);
        $result->execute(array());
		
		if ($result->errorCode() != '00000') {
			$conexion->rollBack();
			$mensaje = "Error UPDATE cabfacturacion<br>".$SqlU;
			die($mensaje);
		}
	}
?>