<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	set_time_limit(0);

	include("../../../../objetos/clsReporte.php");

	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $m, $anio, $ciclotext, $Sede;

			$this->SetFont('Arial','B',14);

			$tit1 = "REGISTRO DE VENTAS";
			$this->Cell(277, $h + 2, utf8_decode($tit1), 0, 1, 'C');

            $this->Ln(5);

			$h=4;
			$this->SetFont('Arial', '', 7);
			$this->Cell(277, $h, $ciclotext, 0, 1, 'C');

			$this->SetX(113.5);
			$this->Cell(10, $h, "MES", 0, 0, 'R');
			$this->Cell(5, $h, ":", 0, 0, 'C');
			$this->Cell(30, $h,$m,0,0,'L');
			$this->Cell(10, $h,utf8_decode("AÑO"),0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$anio,0,1,'L');

			$this->Ln(2);
/*			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$fecha,0,0,'L');
			$this->Cell(15, $h,"CAJERO",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(50, $h,$cajeros,0,1,'L');*/

			$this->Ln(1);
			$this->SetFont('Arial','',6);

			$this->Cell(15, $h, "Numero", 'RLT', 0, 'C');
			$this->Cell(15, $h, "Fecha", 'RLT', 0, 'C');
			$this->Cell(15, $h, "Fecha de ", 'RLT', 0, 'C');
			$this->Cell(30, $h, "Comprobante de Pago", 'RLT', 0, 'C');
			$this->Cell(80, $h, "Informacion del Cliente", 'RLT', 0, 'C');
			$this->Cell(15, $h, "Base", 'RLT', 0, 'C');
			$this->Cell(24, $h, "Importe Total de la", 'RLT', 0, 'C');
			$this->Cell(12, $h, "", 'RLT', 0, 'C');
			$this->Cell(17, $h, "Importe Total del", 'RLT', 0, 'C');
			$this->Cell(56, $h, "Referencia del Comprobante de Pago o ", 'RLT', 1, 'C');

			$this->Cell(15, $h, "Correlativo", 'RL', 0, 'C');
			$this->Cell(15, $h, "Emision", 'RL', 0, 'C');
			$this->Cell(15, $h, "Venc. y/o", 'RL', 0, 'C');
			$this->Cell(30, $h, "o Documento", 'RLB', 0, 'C');
			$this->Cell(80, $h, "", 'RBL', 0, 'C');
			$this->Cell(15, $h, "Imponible de ", 'RL', 0, 'C');
			$this->Cell(24, $h, "Ope. Exo. o Inaf.", 'RLB', 0, 'C');
			$this->Cell(12, $h, "Igv", 'RL', 0, 'C');
			$this->Cell(17, $h, "Comprobante de ", 'RL', 0, 'C');
			$this->Cell(56, $h, "Documento Original que se Modifica", 'RLB', 1, 'C');

			$this->Cell(15, $h, "del Registro", 'RL', 0, 'C');
			$this->Cell(15, $h, "Comprob.", 'RL', 0, 'C');
			$this->Cell(15, $h, "Pago", 'RL', 0, 'C');
			$this->Cell(9, $h, "Tipo", 'RL', 0, 'C');
			$this->Cell(10, $h, "Serie", 'RL', 0, 'C');
			$this->Cell(11, $h, "Numero", 'RL', 0, 'C');
			$this->Cell(20, $h, "Doc. de Identidad", 'RLB', 0, 'C');
			$this->Cell(15, $h, "Numero de ", 'RL', 0, 'C');
			$this->Cell(45, $h, "Apellidos y Nombres, ", 'RL', 0, 'C');
			$this->Cell(15, $h, "la operacion", 'RL', 0, 'C');
			$this->Cell(12, $h, "Exo.", 'RL', 0, 'C');
			$this->Cell(12, $h, "Inaf.", 'RL', 0, 'C');
			$this->Cell(12, $h, "", 'RL', 0, 'C');
			$this->Cell(17, $h, "Pago", 'RL', 0, 'C');
			$this->Cell(15, $h, "Fecha",'RL',0,'C');
			$this->Cell(5, $h, "Tipo", 'RL', 0, 'C');
			$this->Cell(10, $h, "Serie", 'RL', 0, 'C');
			$this->Cell(26, $h, "Nro. del Comprobante de", 'RL', 1, 'C');

			$this->Cell(15, $h, "o Cod. Unico", 'RL', 0, 'C');
			$this->Cell(15, $h, "de Pago o", 'RL', 0, 'C');
			$this->Cell(15, $h, "", 'RL', 0, 'C');
			$this->Cell(9, $h, "", 'RL', 0, 'C');
			$this->Cell(10, $h, "", 'RL', 0, 'C');
			$this->Cell(11, $h, "", 'RL', 0, 'C');
			$this->Cell(5, $h, "Tip.", 'RL', 0, 'C');
			$this->Cell(15, $h, "Numero", 'RL', 0, 'C');
			$this->Cell(15, $h, "Inscripcion", 'RL', 0, 'C');
			$this->Cell(45, $h, "Demoninacion o Razon Social", 'RL', 0, 'C');
			$this->Cell(15, $h, "Gravada", 'RL', 0, 'C');
			$this->Cell(12, $h, "", 'RL', 0, 'C');
			$this->Cell(12, $h, "", 'RL', 0, 'C');
			$this->Cell(12, $h, "", 'RL', 0, 'C');
			$this->Cell(17, $h, "", 'RL', 0, 'C');
			$this->Cell(15, $h, "", 'RL', 0, 'C');
			$this->Cell(5, $h, "", 'RL', 0, 'C');
			$this->Cell(10, $h, "", 'RL', 0, 'C');
			$this->Cell(26, $h, "Pago o Documento", 'RL', 1, 'C');

			$this->Cell(15, $h, "de la Oper,", 'RLB', 0, 'C');
			$this->Cell(15, $h, "Documento",'RLB',0,'C');
			$this->Cell(15, $h, "", 'RLB', 0, 'C');
			$this->Cell(9, $h, "", 'RLB', 0, 'C');
			$this->Cell(10, $h, "", 'RLB', 0, 'C');
			$this->Cell(11, $h, "", 'RLB', 0, 'C');
			$this->Cell(5, $h, "", 'RLB', 0, 'C');
			$this->Cell(15, $h, "", 'RLB', 0, 'C');
			$this->Cell(15, $h, "", 'RLB', 0, 'C');
			$this->Cell(45, $h, "", 'RLB', 0, 'C');
			$this->Cell(15, $h, "", 'RLB', 0, 'C');
			$this->Cell(12, $h, "", 'RLB', 0, 'C');
			$this->Cell(12, $h, "", 'RLB', 0, 'C');
			$this->Cell(12, $h, "", 'RLB', 0, 'C');
			$this->Cell(17, $h, "", 'RLB', 0, 'C');
			$this->Cell(15, $h, "", 'RLB', 0, 'C');
			$this->Cell(5, $h, "", 'RLB', 0, 'C');
			$this->Cell(10, $h, "", 'RLB', 0, 'C');
			$this->Cell(26, $h, "", 'RLB', 1, 'C');

/*			$this->Cell(12, $h,"Nro. Pago",1,0,'C');
			$this->Cell(15, $h,"Codigo",1,0,'C');
			$this->Cell(54, $h,"Cliente",1,0,'C');
			$this->Cell(48, $h,"Direccion",1,0,'C');
			$this->Cell(10, $h,"Doc",1,0,'C');
			$this->Cell(10, $h,"Serie",1,0,'C');
			$this->Cell(12, $h,"Nro. Doc.",1,0,'C');
			$this->Cell(10, $h,utf8_decode("Año"),1,0,'C');
			$this->Cell(10, $h,"Mes",1,0,'C');
			$this->Cell(12, $h,"Agua",1,0,'C');
			$this->Cell(12, $h,"Desague",1,0,'C');
			$this->Cell(12, $h,"Cargo Fijo",1,0,'C');
			$this->Cell(12, $h,"Intereses",1,0,'C');
			$this->Cell(12, $h,"Redondeo",1,0,'C');
			$this->Cell(12, $h,"Otros",1,0,'C');
			$this->Cell(12, $h,"Igv",1,0,'C');
			$this->Cell(12, $h,"Total",1,1,'C');*/

		}

		function agregar_detalle($codsuc, $codciclo, $anio, $mes, $Csec, $Archivo)
		{
			global $conexion;

			$empresa = $this->datos_empresa($codsuc);
			$RUC = $empresa["ruc"];

			if(intval($mes) < 10)
			{
				$mes = "0".$mes;
			}

			$aniomin = substr($anio, 2, 2);

			if ($Archivo == 1)
			{
				if($codsuc == 1)
				{
					$RutaDisco = 'D:\SIINCO\AVALON\\';
				}
				if($codsuc == 2)
				{
					$RutaDisco = 'D:\SIINCO\AVALON\\';
				}
				if($codsuc == 3)
				{
					$RutaDisco = 'G:\SIINCO\AVALON\\';
				}


				$NombreTxt = 'LE'.$RUC.$anio.$mes.'00140100001111';
				//unlink($RutaDisco."progreso.txt");
				$file = fopen($RutaDisco.$NombreTxt.".txt", "w");
			}

			$h = 4;
			$ultimodia = $this->obtener_ultimo_dia_mes($mes, $anio);
			$primerdia = $this->obtener_primer_dia_mes($mes, $anio);

			$count = 0;

			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;
			$ImportT	= 0;

			$imponib 	= 0;
			$noimponib 	= 0;

			$mes = intval($mes);

			$this->SetTextColor(0, 0, 0);

			$sql = "SELECT nrofacturacion, fechaemision ";
			$sql .= "FROM facturacion.periodofacturacion ";
			$sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";

			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc, $codciclo, $anio, $mes));
			$row = $consulta->fetch();

			$nrofacturacion = $row['nrofacturacion'];


				$sql = "SELECT c.fechareg, c.coddocumento, c.serie, c.nrodocumento, cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
				$sql .= " c.nroinscripcion, cl.propietario, ";
				$sql .= " CAST(sum(d.importe) AS NUMERIC (18, 2)) AS impmes, ";
				//$sql .= " c.impigv, ";
				$sql .= " c.fechavencimiento, cl.codantiguo, ";
				//$sql .= " SUM(CASE WHEN d.codconcepto IN(1, 2, 6) THEN d.importe END) AS Imponible, ";
				//$sql .= " SUM(CASE WHEN d.codconcepto NOT IN(1, 2, 5, 6) THEN d.importe END) AS NoImponible ";
				$sql .= " SUM(CASE WHEN d.codconcepto IN(5) THEN d.importe END) AS impigv, ";
				$sql .= " SUM(CASE WHEN d.codconcepto NOT IN(5, 3, 4, 7, 8, 9, 11, 71, 72, 992, 10013) THEN d.importe END) AS Imponible, ";
				$sql .= " SUM(CASE WHEN d.codconcepto IN(3, 4, 7, 8, 9, 11, 71, 72, 992, 10013) THEN d.importe END) AS NoImponible ";
				$sql .= "FROM facturacion.cabctacorriente c ";
				$sql .= " INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) ";
				$sql .= "  AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) ";
				$sql .= "  AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) ";
				$sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
				$sql .= " INNER JOIN catastro.clientes AS cl ON(c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.nroinscripcion = cl.nroinscripcion) ";
				$sql .= "WHERE c.codemp = 1 ";
				$sql .= " AND d.codsuc = ".$codsuc." ";
				$sql .= " AND cl.codciclo = ".$codciclo." ".$Csec." ";
				$sql .= " AND ((d.nrocredito = 0 AND d.nrorefinanciamiento = 0) OR d.codconcepto = 9 OR d.codtipodeuda IN (3, 4, 7, 9)) ";
				$sql .= " AND d.tipo = 0 ";
				$sql .= " AND d.tipoestructura = 0 ";
				$sql .= " AND d.periodo = '".$anio.$mes."' ";
				$sql .= " AND d.codconcepto <> 10012 ";
				$sql .= " AND d.codtipodeuda NOT IN (3, 4, 7) ";
				//$sql .= " AND c.nroinscripcion = 113699 ";
				$sql .= "GROUP BY c.fechareg, c.coddocumento, c.serie, c.nrodocumento, cl.codtipodocumento, cl.nrodocumento, ";
				$sql .= " c.nroinscripcion, cl.propietario, c.impigv, c.fechavencimiento, cl.codantiguo ";
				$sql .= "ORDER BY c.nroinscripcion";
			//die($sql);

			$Dato = '';
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array());
			$items = $consulta->fetchAll();

			$xt = 0;
			//var_dump($consulta->errorInfo());

			foreach($items as $row)
			{
				$count++;

				$row["impmes"] = $row["impmes"];
				
				$Impo = $row["imponible"];
				
				
				if(intval($mes) == 2 AND intval($anio) == 2016)
				{
					$Impo = $row["impmes"] - $row["noimponible"] - $row["impigv"];
				}

				$this->SetWidths(array(15, 15, 15, 9, 10, 11, 5, 15, 15, 45, 15, 12, 12, 12, 17, 15, 5, 10, 26));
				$this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "L", "R", "R", "R", "R", "R", "R", "R", "R", "R"));
				$this->Row(array($count,
								 $this->DecFecha($row["fechareg"]),
								 $this->DecFecha($row["fechavencimiento"]),
								 $this->EquivalenteDoc($row["coddocumento"], 1),
								 $row["serie"],
								 $row["nrodocumento"],
								 $this->EquivalenteDoc($row["codtipodocumento"], 2),
								 $row["nrodoc"],
								 $row["codantiguo"],
								 utf8_decode(strtoupper($row["propietario"])),
								 number_format($Impo, 2),
								 "",
								 number_format($row["noimponible"], 2),
								 number_format($row["impigv"], 2),
								 number_format($row["impmes"], 2),
								 "",
								 "",
								 "",
								 ""));

				//$imponib 	+= $row["imponible"];
				$noimponib 	+= $row["noimponible"];

				$impmes += $Impo; //$row["imponible"];
				$impigv += $row["impigv"];
				
				$ImportT += $row["impmes"];
			}
			
			$impmes		= round($impmes, 2);
			$noimponib	= round($noimponib, 2);
			//$impigv		= round($impmes * 0.18, 2); //round($impigv, 2);
			$ImportT	= $impmes + $noimponib + $impigv; //round($ImportT, 2);
			
			//$imptotal += $ImportT; //($impmes + $noimponib + $impigv);// - ($TotalRestar - $xt);
			
			$cRecibos = $count;
			$tRecibos = $ImportT;

			$this->Ln(3);
			$this->SetFont('Arial', 'B', 6);
			//$this->SetTextColor(230, 0, 0);

			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", '0', 0, 'C');
			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(9, $h, "", 0, 0, 'C');
			$this->Cell(10, $h, "", 0 ,0, 'C');
			$this->Cell(11, $h, "", 0, 0, 'C');
			$this->Cell(5, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(45, $h, "Total Facturacion ==>", 0, 0, 'R');
			$this->Cell(16, $h, number_format($impmes, 2), 'T', 0, 'R');
			$this->Cell(12, $h, number_format($imponib, 2), 'T', 0, 'C');
			$this->Cell(12, $h, number_format($noimponib, 2), 'T', 0, 'R');
			$this->Cell(12, $h, number_format($impigv, 2), 'T', 0, 'R');
			$this->Cell(17, $h, number_format($ImportT, 2), 'T', 0, 'R');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(26, $h, "", 'T', 1, 'C');

			$count = 0;

			$this->AddPage("L");
			//$this->SetTextColor(0, 0, 0);
			$this->SetFont('Arial', '', 6);

			$TotalF = $imptotal;
			$impmes = 0;
			$impigv = 0;
			$imptotal = 0;
			$cBoletas = 0;
			$tBoletas = 0;
			$cFacturas = 0;
			$tFacturas = 0;

//BOLETAS Y FACTURAS

			$Sql = "SELECT c.nroinscripcion, c.propietario AS propietario, ";
			$Sql .= " c.direccion AS direccion, '' AS codcatastro, ";
			$Sql .= " c.fechareg, d.serie, d.nrodocumento,  c.propietario, ";
			$Sql .= " cl.codantiguo AS codantiguo, c.estado, ";
			$Sql .= " c.nroprepago, ";
			$Sql .= " SUM(d.importe) AS impmes,";
			$Sql .= " c.igv AS impigv, ";
			$Sql .= " c.fechareg, d.coddocumento, d.serie, d.nrodocumento, ";
			$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (5, 7, 8) THEN d.importe ELSE 0 END) AS Imponible, ";
			$Sql .= " SUM(CASE WHEN d.codconcepto IN (7, 8) THEN d.importe ELSE 0 END) AS NoImponible ";
			$Sql .= "FROM cobranza.cabprepagos c ";
			$Sql .= " JOIN cobranza.detprepagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nroprepago = d.nroprepago) AND (c.nroinscripcion = d.nroinscripcion) ";
			$Sql .= " JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
			$Sql .= " LEFT JOIN catastro.clientes cl ON(c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
			$Sql .= " LEFT JOIN cobranza.cabpagos ca ON(c.codemp = ca.codemp AND c.codsuc = ca.codsuc AND c.nroinscripcion = ca.nroinscripcion AND c.nroprepago = ca.nroprepago) ";
			$Sql .= "WHERE c.codsuc = ".$codsuc." ";
			$Sql .= " AND c.estado NOT IN (0) ";
			//$Sql .= " AND c.estado IN (1, 2) ";
			$Sql .= " AND c.fechareg BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
			$Sql .= " AND d.coddocumento IN (13, 14) ";
			$Sql .= " AND co.codconcepto <> 10005 ";
			$Sql .= "GROUP BY c.nroinscripcion, c.propietario, c.direccion, c.codsuc, c.nroprepago, ";
			$Sql .= "c.fechareg, d.coddocumento, d.serie, d.nrodocumento, cl.codantiguo, c.igv, c.estado ";
			$Sql .= "ORDER BY c.fechareg, d.nrodocumento";

			$items = $conexion->query($Sql)->fetchAll();
			//var_dump($consulta->errorInfo());
			//

			$noimponib 	= 0;
			$ImportT	= 0;
			
			foreach($items as $row)
			{
				$count++;

				if($row["estado"] == 3):
					//$row["imponible"] = ($row["imponible"]-$row["impigv"]);
				else:
					$row["imponible"] = $row["imponible"];
				endif;

				$this->SetWidths(array(15, 15, 15, 9, 10, 11, 5, 15, 15, 45, 15, 12, 12, 12, 17, 15, 5, 10, 26));
				$this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "L", "R", "R", "R", "R", "R", "R", "R", "R", "R"));
				$this->Row(array($count,
								 $this->DecFecha($row["fechareg"]),
								 $this->DecFecha($row["fechareg"]),
								 $this->EquivalenteDoc($row["coddocumento"], 1),
								 $row["serie"],
								 $row["nrodocumento"],
								 $this->EquivalenteDoc($row["codtipodocumento"], 2),
								 $row["nrodoc"],
								 $this->CodUsuario($codsuc, $row["nroinscripcion"]) ,
								 utf8_decode(strtoupper($row["propietario"])),
								 //number_format($row["imponible"], 2),
								 number_format($row["impmes"] - $row["noimponible"] - $row["impigv"], 2),
								 "",
								 number_format($row["noimponible"], 2),
								 number_format($row["impigv"], 2),
								 number_format($row["impmes"], 2),
								 "",
								 "",
								 "",
								 ""));
								 
				$impmes		+= $row["impmes"] - $row["noimponible"] - $row["impigv"]; //$row["imponible"];//
				$noimponib	+= $row["noimponible"];
				$impigv		+= $row["impigv"];
				
				$ImportT	+= $row["impmes"];
				
				if($row['coddocumento'] == 13)
				{
					$cBoletas++;
					$tBoletas += $row["impmes"];//$impmes + $noimponib + $impigv;
				}
				if($row['coddocumento'] == 14)
				{
					$cFacturas++;
					$tFacturas += $row["impmes"];//$impmes + $noimponib + $impigv;
				}
			}
			
			$impmes		= round($impmes, 2);
			$noimponib	= round($noimponib, 2);
			$impigv		= round($impmes * 0.18, 2); //round($impigv, 2);
			$ImportT	= $impmes + $noimponib + $impigv; //round($ImportT, 2);
			
			$ImportT1	= round($ImportT, 2);
			$ImportT2	= round($ImportT, 1);
			
			$noimponib	= round($noimponib - ($ImportT1 - $ImportT2), 2);
			$ImportT	= $impmes + $noimponib + $impigv;
			
			$this->Ln(3);
			$count = 0;
			$this->SetFont('Arial', 'B', 6);

			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", '0', 0, 'C');
			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(9, $h, "", 0, 0, 'C');
			$this->Cell(10, $h, "", 0, 0, 'C');
			$this->Cell(11, $h, "", 0, 0, 'C');
			$this->Cell(5, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(45, $h, "Total Recibos ==>", 0, 0, 'R');
			$this->Cell(15, $h, number_format($impmes, 2), 'T', 0, 'R');
			$this->Cell(12, $h, "", 'T', 0, 'C');
			$this->Cell(12, $h, number_format($noimponib, 2), 'T', 0, 'R');

			$this->Cell(12, $h, number_format($impigv, 2), 'T', 0, 'R');
			$this->Cell(17, $h, number_format($ImportT, 2), 'T', 0, 'R');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(26, $h, "", 'T', 1, 'C');

			//echo $TotalF+$TotalR;
			$this->AddPage("L");

			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;

			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','',6);

//REBAJAS
			if ($anio == 2016 && intval($mes) <= 2)
			{
				$Sql = "SELECT c.fechaemision AS fechareg, ";
				$Sql .= " d.coddocumentoabono AS coddocumento, d.seriedocumentoabono AS serie, d.nrodocumentoabono AS nrodocumento, ";
				$Sql .= " cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
				$Sql .= " c.nroinscripcion, cl.propietario, ";
				$Sql .= " SUM(d.imprebajado) AS impmes, ";
				$Sql .= " SUM(d.imprebajado) - (SUM(d.imprebajado) / 1.18) AS impigv, ";
				$Sql .= " d.coddocumento AS coddocumento2, d.nrodocumento AS nrodocumento2, ";
				$Sql .= " d.seriedocumento AS seriedocumento2, c.glosa, c.estareg, ";
				$Sql .= " 0 AS noimp, ";
				$Sql .= " 0 AS redondeo, ";
				$Sql .= " SUM(d.imprebajado) / 1.18 AS imptot ";
				$Sql .= "FROM facturacion.cabrebajas c ";
				$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
				$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
				$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
				$Sql .= "WHERE c.codsuc = ".$codsuc." ";
				//$Sql .= " AND c.estareg = 1 ";
				$Sql .= " AND c.codestrebaja = 1 ";
				//$Sql .= " AND c.anio = '".$anio."' ";
				//$Sql .= " AND c.mes = '".intval($mes)."' ";
				$Sql .= " AND c.fechaemision BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
				$Sql .= "GROUP BY c.fechaemision, d.coddocumentoabono, d.seriedocumentoabono, d.nrodocumentoabono, cl.codtipodocumento, cl.nrodocumento, ";
				$Sql .= " c.nroinscripcion, cl.propietario, d.coddocumento, d.nrodocumento, d.seriedocumento, c.glosa, c.estareg ";
				$Sql .= "ORDER BY c.fechaemision ";
			}
			else
			{
				$Sql = "SELECT c.fechaemision AS fechareg, ";
				$Sql .= " d.coddocumentoabono AS coddocumento, d.seriedocumentoabono AS serie, d.nrodocumentoabono AS nrodocumento, ";
				$Sql .= " cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
				$Sql .= " c.nroinscripcion, cl.propietario, ";
				$Sql .= " SUM(d.imprebajado) AS impmes, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto = 5 THEN d.imprebajado ELSE 0 END) AS impigv, ";
				$Sql .= " d.coddocumento AS coddocumento2, d.nrodocumento AS nrodocumento2, ";
				$Sql .= " d.seriedocumento AS seriedocumento2, c.glosa, c.estareg, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (5, 3, 4, 7, 8, 9, 11, 71, 72, 992, 10013) THEN d.imprebajado ELSE 0 END) AS imp, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto IN (3, 4, 7, 8, 9, 11, 71, 72, 992, 10013) THEN d.imprebajado ELSE 0 END) AS noimp, ";
				$Sql .= " c.nrorebaja ";
				$Sql .= "FROM facturacion.cabrebajas c ";
				$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
				$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
				$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
				$Sql .= "WHERE c.codsuc = ".$codsuc." ";
				//$Sql .= " AND c.estareg = 1 ";
				$Sql .= " AND c.codestrebaja = 1 ";
				//$Sql .= " AND c.anio = '".$anio."' ";
				//$Sql .= " AND c.mes = '".intval($mes)."' ";
				$Sql .= " AND c.fechaemision BETWEEN '{$primerdia}' AND '{$ultimodia}' ";
				$Sql .= "GROUP BY c.fechaemision, d.coddocumentoabono, d.seriedocumentoabono, d.nrodocumentoabono, cl.codtipodocumento, cl.nrodocumento, ";
				$Sql .= " c.nroinscripcion, cl.propietario, d.coddocumento, d.nrodocumento, d.seriedocumento, c.glosa, c.estareg, c.nrorebaja ";
				$Sql .= "ORDER BY c.fechaemision ";
			}

			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			//var_dump($consulta->errorInfo());

			$tvVenta = 0;
			$tigv = 0;
			$tredondeo = 0;

			$noimponib = 0;

			foreach($items as $row)
			{
				$count++;
				$fecha_documento = '';
				if(!empty($row["nrodocumento2"])):
					$fecha_documento = $this->FechaDoc($row["coddocumento2"], $row["seriedocumento2"], $row["nrodocumento2"]);
				endif;

				$this->SetWidths(array(15, 15, 15, 9, 10, 11, 5, 15, 15, 45, 15, 12, 12, 12, 17, 15, 5, 10, 26));
				$this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "L", "R", "R", "R", "R", "R", "R", "R", "R", "R"));
				$this->Row(array($count,
								 $this->DecFecha($row["fechareg"]),
								 $this->DecFecha($row["fechareg"]),
								 $this->EquivalenteDoc($row["coddocumento"], 1),
								 $row["serie"],
								 $row["nrodocumento"],
								 $row["codtipodocumento"],
								 $row["nrorebaja"],
								 $this->CodUsuario($codsuc, $row["nroinscripcion"]) ,
								 utf8_decode(strtoupper($row["propietario"])),
								 number_format($row["imp"], 2),
								 "",
								 number_format($row["noimp"], 2),
								 number_format($row["impigv"], 2),
								 number_format($row["impmes"], 2),
								 $fecha_documento,
								 $this->EquivalenteDoc($row["coddocumento2"], 1),
								 $row["seriedocumento2"],
								 $row["nrodocumento2"]));

				$tvVenta	+= $row['imp'];
				
				$noimponib 	+= $row["noimp"];
				$impigv += $row["impigv"];
				$impmes += $row["impmes"];
				
			}
			
			$imptotal = $impmes;
			
			$tAbonos = $impmes;

			$cAbonos = $count;

			$this->Ln(3);

			$this->SetFont('Arial', 'B', 6);
			//$this->SetTextColor(230, 0, 0);

			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", '0', 0, 'C');
			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(9, $h, "", 0, 0, 'C');
			$this->Cell(10, $h, "", 0, 0, 'C');
			$this->Cell(11, $h, "", 0, 0, 'C');
			$this->Cell(5, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", 0, 0, 'C');
			$this->Cell(15, $h, "", 0,0, 'C');
			$this->Cell(45, $h, "Total Rebajas ==>", 0, 0, 'R');
			$this->Cell(15, $h, number_format($tvVenta, 2), 'T', 0, 'R');
			$this->Cell(12, $h, "", 'T', 0, 'C');
			$this->Cell(12, $h, number_format($noimponib, 2), 'T', 0, 'R');
			$this->Cell(12, $h, number_format($impigv, 2), 'T', 0, 'R');
			$this->Cell(17, $h, number_format($impmes, 2), 'T', 0, 'R');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(10, $h, "", 'T', 0, 'C');
			$this->Cell(26, $h, "", 'T', 1, 'C');
			$this->SetTextColor(0, 0, 0);

			$count = 0;

//CARGOS
			$count = 0;
			$this->AddPage("L");
			$this->SetTextColor(0, 0, 0);
			$this->SetFont('Arial', '', 6);
			
			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;

			$Sql = "(";
			$Sql .= "SELECT c.fechareg, to_char(c.codsuc, '00') || '' || TRIM(to_char(c.nroinscripcion, '000000')) AS codantiguo, ";
			$Sql .= " c.propietario, ";
			$Sql .= " SUM(d.interes) AS impmes, ";
			$Sql .= " 0 AS impigv, ";
			$Sql .= " c.estareg, '' AS coddocumento2, '' AS seriedocumento2, '' AS nrodocumento2, ";
			$Sql .= " '' AS coddocumento, '' AS nrodocumento, '' AS serie, ";
			$Sql .= " c.nrocredito AS id, c.nroprepago, c.nroinscripcion, c.nroprepagoinicial, ";
			$Sql .= " 1 AS tipo, ";
			$Sql .= " SUM(d.interes) AS imptot ";
			$Sql .= "FROM facturacion.cabcreditos c ";
			$Sql .= " INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrocredito = d.nrocredito) ";
			$Sql .= "WHERE c.codsuc = ".$codsuc." ";
			$Sql .= " AND c.estareg = 1 ";
			$Sql .= " AND EXTRACT(YEAR FROM c.fechareg) = '".$anio."' ";
			$Sql .= " AND EXTRACT(MONTH FROM c.fechareg) = '".intval($mes)."' ";
			$Sql .= " AND c.interes > 0 ";
			$Sql .= " AND c.nroprepago > 0 ";
			$Sql .= " AND c.sininteres = 0 ";
			$Sql .= " AND c.nroprepagoinicial > 0 ";
			$Sql .= "GROUP BY c.fechareg, to_char(c.codsuc, '00') || '' || TRIM(to_char(c.nroinscripcion, '000000')), c.estareg, ";
			$Sql .= " c.propietario, c.nrocredito, c.nroprepago, c.nroinscripcion, c.nroprepagoinicial, c.igv ";
			$Sql .= "ORDER BY fechareg, to_char(c.codsuc, '00') || '' || TRIM(to_char(c.nroinscripcion, '000000')) ";
			$Sql .= ") ";
			$Sql .= "UNION";
			$Sql .= "(";
			$Sql .= "SELECT c.fechaemision AS fechareg, clie.codantiguo AS codantiguo, ";
			$Sql .= " clie.propietario, ";
			$Sql .= " CAST(SUM(d.importe) - c.totalrefinanciado AS NUMERIC (18,2)) AS impmes, ";
			$Sql .= " 0 AS impigv, ";
			$Sql .= " c.estareg, '' AS coddocumento2, '' AS seriedocumento2, '' AS nrodocumento2, ";
			$Sql .= " '' AS coddocumento, '' AS nrodocumento, '' AS serie, ";
			$Sql .= " c.nrorefinanciamiento AS id, c.nroprepago, c.nroinscripcion, c.nroprepagoinicial, ";
			$Sql .= " 2 AS tipo, ";
			$Sql .= " CAST(SUM(d.importe) - c.totalrefinanciado AS NUMERIC (18,2)) AS imptot ";
			$Sql .= "FROM facturacion.cabrefinanciamiento c ";
			$Sql .= " INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorefinanciamiento = d.nrorefinanciamiento) ";
			$Sql .= " INNER JOIN catastro.clientes clie ON (c.codemp = clie.codemp) AND (c.codsuc = clie.codsuc) AND (c.nroinscripcion = clie.nroinscripcion) ";
			$Sql .= "WHERE d.codemp = 1 ";
			$Sql .= " AND d.codsuc = ".$codsuc." ";
			$Sql .= " AND c.estareg = 1 ";
			$Sql .= " AND EXTRACT(YEAR FROM c.fechaemision) = '".$anio."' ";
			$Sql .= " AND EXTRACT(MONTH FROM c.fechaemision) = '".intval($mes)."' ";
			$Sql .= " AND c.interes > 0 ";
			$Sql .= " AND c.nroprepagoinicial > 0 ";
			$Sql .= " AND c.sininteres = 0 ";
			$Sql .= " AND c.codestadorefinanciamiento <> 5 ";
			$Sql .= "GROUP BY c.totalrefinanciado, c.fechaemision, clie.codantiguo, c.estareg, clie.propietario, c.nrorefinanciamiento, ";
			$Sql .= " c.nroprepago, c.nroinscripcion, c.nroprepagoinicial ";
			$Sql .= "ORDER BY c.nroinscripcion ";
			$Sql .= ")--ORDER BY fechareg, nrodocumento ";
			//die($Sql);

			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			//var_dump($consulta->errorInfo());
			foreach($items as $row)
			{
				$count++;
				if($row['tipo']==1)
				{
					$fechadocu=$this->DecFecha($row["fechareg"]);
					$datos = $this->DatosPrepago($row["nroinscripcion"], $row["nroprepago"]);
					$row["coddocumento2"] = $datos['coddocumento'];
					$row["seriedocumento2"] = $datos['serie'];
					$row["nrodocumento2"] = $datos['nrodocumento'];
					$datos= $this->DatosPrepago($row["nroinscripcion"], $row["nroprepagoinicial"]);
					$row["coddocumento"]=$datos['coddocumento'];
					$row["serie"]=$datos['serie'];
					$row["nrodocumento"]=$datos['nrodocumento'];
				}
				else
					{
						$datos= $this->DatosRef($row['id']);
						 $row["coddocumento2"]=$datos['coddocumento'];
						 $row["seriedocumento2"]=$datos['serie'];
						 $row["nrodocumento2"]=$datos['nrodocumento'];
						 $fechadocu=$this->DecFecha($datos['fechareg']);
						 $datos= $this->DatosPrepago($row["nroinscripcion"],$row["nroprepagoinicial"]);
						 $row["coddocumento"]=$datos['coddocumento'];
						$row["serie"]=$datos['serie'];
						$row["nrodocumento"]=$datos['nrodocumento'];
					}
				$this->SetWidths(array(15,15,15,9,10,11,5,15,15,45,15,12,12,12,17,15,5,10,26));
				$this->SetAligns(array("C","C","C","C","C","C","C","C","C","L","R","R","R","R","R","R","C","C","C"));
				$this->Row(array($count,
								 $this->DecFecha($row["fechareg"]),
								  $this->DecFecha($row["fechareg"]),
								  $this->EquivalenteDoc($row["coddocumento"],1),
								 $row["serie"],
								 $row["nrodocumento"],
								 $row["codtipodocumento"],
								 $row["nrodoc"],
								 $this->CodUsuario($codsuc,$row["nroinscripcion"]) ,
								 utf8_decode(strtoupper($row["propietario"])),
								 number_format($row["impmes"], 2),
								 "",
								 "",
								 number_format($row["impigv"], 2),
								 number_format($row["imptot"], 2),
								 $fechadocu,
								  $this->EquivalenteDoc($row["coddocumento2"],1),
								 $row["seriedocumento2"],
								 $row["nrodocumento2"]));

				$impmes += $row["impmes"];
				$impigv += $row["impigv"];
			}

			$imptotal = $impmes + $impigv;
			$tCargos = $impmes + $impigv;
			
			$cCargos = $count;
			
			$this->Ln(3);

			$this->SetFont('Arial', 'B', 6);
			//$this->SetTextColor(230,0,0);

			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(15, $h,"",'0',0,'C');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(9, $h,"",0,0,'C');
			$this->Cell(10, $h,"",0,0,'C');
			$this->Cell(11, $h,"",0,0,'C');
			$this->Cell(5, $h,"",0,0,'C');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(45, $h,"Total Cargos ==>",0,0,'R');
			$this->Cell(15, $h,number_format($imptotal,2),'T',0,'R');
			$this->Cell(12, $h,"",'T',0,'C');
			$this->Cell(12, $h,"",'T',0,'R');
			$this->Cell(12, $h,"",'T',0,'R');
			$this->Cell(17, $h,number_format($imptotal,2),'T',0,'R');
			$this->Cell(10, $h,"",'T',0,'C');
			$this->Cell(10, $h,"",'T',0,'C');
			$this->Cell(10, $h,"",'T',0,'C');
			$this->Cell(26, $h,"",'T',1,'C');
			$this->SetTextColor(0,0,0);
			$TotalF = $imptotal;
			$count=0;



			$this->SetTextColor(0,0,0);
			$this->AddPage("L");
			$this->Ln(3);
			$h = 5;
			$this->SetFont('Arial','B',14);
			$this->Cell(0 ,$h,"RESUMEN",0,1,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',8);
			$this->SetX(130);
			$this->Cell(30,$h,"CANTIDAD",'B',0,'C');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,"IMPORTE",'B',1,'C');
			//TOTALE
			$this->Ln(3);
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total de Recibos 'F'",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$cFacturas,0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tFacturas, 2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total de Recibos 'B'",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$cBoletas,0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tBoletas, 2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total Cargos (+)",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$cCargos,0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tCargos, 2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total Abonos (-)",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$cAbonos,0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tAbonos, 2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total Recibos",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$cRecibos,0,0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format($tRecibos, 2),0,1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total General",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h, $cFacturas + $cBoletas + $cCargos + $cAbonos + $cRecibos,'T',0,'R');
			$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,number_format(($tFacturas + $tBoletas + $tCargos + $tRecibos) - $tAbonos, 2),'T',1,'R');

			//fclose($file);

		}

		function FechaDoc($cod, $serie, $numero)
		{
			global $conexion, $codsuc;

			$Sql = "SELECT fechareg FROM facturacion.cabfacturacion
					WHERE codsuc=".$codsuc." AND coddocumento=".$cod."
					AND serie='".$serie."' AND nrodocumento='".$numero."'";

			$consulta = $conexion->prepare($Sql);
			$consulta->execute();

			if ($consulta->errorCode() != '00000')
			{
				return '31/12/1900';
			}

			$c = $consulta->fetch();

			return $this->DecFecha($c[0]);
		}
		function DatosRef($nrorefinanciamiento)
		{
			global $codsuc, $conexion;

			$Sql = "SELECT f.serie, f.nrodocumento, f.nrofacturacion, f.fechareg, f.coddocumento
				FROM facturacion.cabfacturacion f
				  INNER JOIN facturacion.origen_refinanciamiento o ON (f.codemp = o.codemp)
				  AND (f.codsuc = o.codsuc)
				  AND (f.codciclo = o.codciclo)
				  AND (f.nrofacturacion = o.nrofacturacion)
				  AND (f.nroinscripcion = o.nroinscripcion)
				  where o.codemp=1 and o.codsuc=? and o.nrorefinanciamiento=?
				  GROUP BY f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg,f.coddocumento
				  ORDER BY f.nrofacturacion DESC ";
			$consulta2 = $conexion->prepare($Sql);
			$consulta2->execute(array($codsuc,$nrorefinanciamiento));
			$itemD = $consulta2->fetchAll();

			return $itemD[0];
		}
		function EquivalenteDoc($cod,$tipo)
		{
			if($tipo==1)
			{
				switch ($cod)
				{
					case 4: return "14";break;
					case 13: return "03";break;
					case 14: return "01";break;
					case 17: return "07";break;
					case 16: return "08";break;
				}
			}
			else
			{
				switch ($cod)
				{
					case 1: return "1";break;
					case 3: return "6";break;
					default: return "0";break;
				}
			}
			/*
			COMPROBANTES
			-----------------------------
			14  Recibos de Pensiones
			01  Facturas
			03  Boletas de Ventas
			07  Notas de Abono/Credito
			08  Notas de cargo/Debito


			DOCUMENTOS
			-----------------------------
			1  DNI
			6  RUC
			0  Otros*/
		}
		function DatosPrepago($nroinscripcion,$nroprepago)
		{
			global $codsuc,$conexion;
			$Sql="SELECT  d.coddocumento, d.serie, d.nrodocumento
					FROM  cobranza.cabprepagos c
					INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
					 AND (c.nroinscripcion = d.nroinscripcion)
				  WHERE c.codsuc={$codsuc} AND c.nroinscripcion={$nroinscripcion}
				  		AND d.nroprepago={$nroprepago}
				GROUP BY d.coddocumento, d.serie, d.nrodocumento ";
			$consulta2 = $conexion->prepare($Sql);
			$consulta2->execute();
			$itemD = $consulta2->fetchAll();
			return $itemD[0];
		}
    }

	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc		= $_GET["codsuc"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclotext	= $_GET["ciclotext"];

	$Archivo	= $_GET["Archivo"];

	if($_GET["sector"] != "%")
	{
		$Csec = " AND cl.codsector = ".$_GET["sector"];
	}
	else
	{
		$Csec = "";
	}

	$m = $meses[$mes];

/*	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$caja		= $_GET["caja"];
	$cars		= $_GET["cars"];
	$cajeros 	= $_SESSION['nombre'];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$totagua 		= 0;
	$totdesague		= 0;
	$totcargofijo	= 0;
	$totinteres		= 0;
	$totredondeo	= 0;
	$tototros		= 0;
	$totigv			= 0;
	$totalT			= 0;*/

	$objReporte	= new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage("L");

	$objReporte->agregar_detalle($codsuc, $ciclo, $anio, $mes, $Csec, $Archivo);

	$objReporte->Output();

?>
