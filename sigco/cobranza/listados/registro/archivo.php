<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	set_time_limit(0);

	include("../../../../objetos/clsReporte.php");

	class clsRegistroVentas extends clsReporte
	{

		function agregar_detalle($codsuc, $codciclo, $anio, $mes, $Csec, $Archivo)
		{
			global $conexion;

			$empresa = $this->datos_empresa($codsuc);
			$RUC = $empresa["ruc"];

			if(intval($mes) < 10)
			{
				$mes = "0".$mes;
			}

			$aniomin = substr($anio, 2, 2);

			if ($Archivo == 1)
			{
				$RutaDisco = 'D:\E-SIINCOWEB\AVALON\\';
			
				$NombreTxt = 'LE'.$RUC.$anio.$mes.'00140100001111';
				//unlink($RutaDisco."progreso.txt");
				$file = fopen($RutaDisco.$NombreTxt.".txt", "w");
			}

			$h = 4;
			$ultimodia = $this->obtener_ultimo_dia_mes($mes, $anio);
			$primerdia = $this->obtener_primer_dia_mes($mes, $anio);

			$count = 0;

			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;
			$mes = intval($mes);

			$this->SetTextColor(0, 0, 0);

			$sql = "SELECT nrofacturacion, fechaemision ";
			$sql .= "FROM facturacion.periodofacturacion ";
			$sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";

			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc, $codciclo, $anio, $mes));
			$row = $consulta->fetch();

			$nrofacturacion = $row['nrofacturacion'];
			//QUITAR
			$TotalRestar = 0;

			//if(intval($mes) == 10 AND intval($anio) == 2014)
//			{
//				if($codsuc == 1)
//				{
//			    	$factoct = '240';
//				}
//			    if($codsuc == 2)
//				{
//			    	$factoct = '186';
//				}
//			    if($codsuc == 3)
//				{
//			    	$factoct = '131';
//				}
//
//				$Sql = "SELECT SUM(importe) ";
//				$Sql .= "FROM facturacion.detfacturacion ";
//				$Sql .= "WHERE codsuc = ".$codsuc." AND nrorefinanciamiento <> 0 AND codconcepto IN(1, 2, 6) ";
//				$Sql .= " AND nrofacturacion = ".$factoct." ";
//
//				$row = $conexion->query($Sql)->fetch();
//
//				$TotalRestar = $row[0];
//
//				//DETALLE
//				$Conceptos2 = array();
//
//				$Sql2 = "SELECT CAST(SUM(d.importe) AS NUMERIC(18, 2)), d.nroinscripcion ";
//				$Sql2 .= "FROM facturacion.detfacturacion d ";
//				$Sql2 .= "WHERE d.codsuc = ".$codsuc." AND nrorefinanciamiento <> 0 AND d.codconcepto IN(1, 2, 6) ";
//				$Sql2 .= " AND d.nrofacturacion = ".$factoct." ";
//				$Sql2 .= "GROUP BY d.nroinscripcion";
//
//				$Consulta2 = $conexion->query($Sql2);
//
//				foreach($Consulta2->fetchAll() as $row2)
//				{
//					if (!array_key_exists($row2['nroinscripcion'], $Conceptos2))
//					{
//						$Conceptos2[$row2['nroinscripcion']] = array(
//																"Id"=>$row2['nroinscripcion'],
//																"Importe"=>$row2[0]
//																);
//					}
//					else
//					{
//						$Conceptos2[$row2['nroinscripcion']] = array(
//																"Id"=>$row2['nroinscripcion'],
//																"Importe"=>$Conceptos2[$row2['nroinscripcion']]["Importe"] + $row2[0]);
//					}
//				}
//				//DETALLE
//			}
//			//QUITAR

				$sql = "SELECT c.fechareg, c.coddocumento, c.serie, c.nrodocumento, cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
				$sql .= " c.nroinscripcion, cl.propietario, ";
				$sql .= " CAST(sum(d.importe) AS NUMERIC (18, 2)) AS impmes,";
				$sql .= " c.impigv, c.fechavencimiento ";
				$sql .= "FROM facturacion.cabctacorriente c ";
				$sql .= " INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) ";
				$sql .= "  AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) ";
				$sql .= "  AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) ";
				$sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
				$sql .= " INNER JOIN catastro.clientes AS cl ON(c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.nroinscripcion = cl.nroinscripcion) ";
				$sql .= "WHERE c.codemp = 1 AND d.codsuc = ? AND cl.codciclo = ? ".$Csec." ";
				$sql .= " AND ((d.nrocredito = 0 AND d.nrorefinanciamiento = 0) OR d.codconcepto = 9 OR d.codtipodeuda in (3, 4, 7, 9)) ";
				$sql .= " AND d.tipo = 0 AND d.tipoestructura = 0 AND d.periodo = '".$anio.$mes."' ";
				$sql .= " AND d.codconcepto <> 10012 AND d.codtipodeuda NOT IN (3, 4, 7) ";
				$sql .= "GROUP BY c.fechareg, c.coddocumento, c.serie, c.nrodocumento, cl.codtipodocumento, cl.nrodocumento, ";
				$sql .= " c.nroinscripcion, cl.propietario, c.impigv, c.fechavencimiento ";
				$sql .= "ORDER BY c.nroinscripcion";

			//die($sql);

			$Dato = '';
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc, $codciclo));
			$items = $consulta->fetchAll();

			$xt = 0;
			//var_dump($consulta->errorInfo());

			foreach($items as $row)
			{
				$count++;

				$restar = isset($Conceptos2[$row['nroinscripcion']]["Importe"])?$Conceptos2[$row['nroinscripcion']]["Importe"]:0;
				$row["impmes"] = $row["impmes"] - $restar;

				$xt += $restar;

				$flagD = false;

				$NroDocumento = $row["nrodocumento"]?$row["nrodocumento"]:0;

				$impmes += $row["impmes"];
				$impigv += $row["impigv"];

				if ($NroDocumento == 0)
				{
					$MesImporte = $MesImporte + $row["impmes"];
				}
				else
				{
					$row["impmes"] = $row["impmes"] + $MesImporte;

					$MesImporte = 0;

					$flagD = true;
				}

				$Propietario = str_replace("º", " ", trim($row["propietario"]));
				$Propietario = str_replace("Ñ", "N", trim($Propietario));
				$Propietario = str_replace("/", " ", trim($Propietario));
				$Propietario = str_replace("|", "", trim($Propietario));
				$Propietario = str_replace("'", "", trim($Propietario));

				$ImporteMes = str_replace(",", "", number_format($row["impmes"], 2));
				$ImporteIgv = str_replace(",", "", number_format($row["impigv"], 2));
				$ImporteMesIgv = str_replace(",", "", number_format($row["impmes"] + $row["impigv"], 2));

				if ($flagD)
				{
					$Dato = $anio.str_pad($mes, 2, "0", STR_PAD_LEFT).'00'.'|';		//01 - Periodo
					$Dato .= $codsuc.'1|';											//02 - CUO
					$Dato .= 'M'.str_pad($count, 5, "0", STR_PAD_LEFT).'|';			//03 - Correlativo
					$Dato .= $this->DecFecha($row["fechareg"]).'|';					//04 - Fecha de Emisión del Comprobante de Pago
					$Dato .= $this->DecFecha($row["fechavencimiento"]).'|';			//05 - Fecha de Vencimiento del Comprobante de Pago
					$Dato .= $this->EquivalenteDoc($row["coddocumento"], 1).'|';	//06 - Tipo de Comprobante de Pago
					$Dato .= str_pad($row["serie"], 4, "0", STR_PAD_LEFT).'|';		//07 - Serie de Comprobante de Pago
					$Dato .= str_pad(($row["nrodocumento"]?$row["nrodocumento"]:0), 8, "0", STR_PAD_LEFT).'|';	//08 - Nro de Comprobante de Pago
					$Dato .= '|';													//09 - Tickets
					$Dato .= '0|';													//10 - Tipo Documento Ident.
					$Dato .= str_pad('', 11, "0", STR_PAD_RIGHT).'|';				//11 - Nro Doc. Identidad
					$Dato .= str_pad($Propietario, 100, " ", STR_PAD_RIGHT).'|';	//12 - Razón social del Cliente
					$Dato .= '0.00|';												//13 - Exportaciones
					$Dato .= '0.00|';												//14 - Exportaciones
					$Dato .= '0.00|';												//15 - Exportaciones
					$Dato .= str_pad($ImporteIgv, 10, " ", STR_PAD_LEFT).'|'; 		//16 - Importe IGV
					$Dato .= '0.00|'; 												//17 - Importe IGV - Descuento
					$Dato .= '0.00|';												//18 - Importe Total Exoneracion
					$Dato .= str_pad($ImporteMes, 10, " ", STR_PAD_LEFT).'|';		//19 - Importe Total Inafecto
					$Dato .= '0.00|';												//20 - Imp. Selectivo al Consumo
					$Dato .= '0.00|';												//21 - Base Imponible - Arroz Pilado
					$Dato .= '0.00|';												//22 - Impuesto - Arroz Pilado
					$Dato .= '0.00|';												//23 - Otros Conceptos
					$Dato .= str_pad($ImporteMesIgv, 10, " ", STR_PAD_LEFT).'|'; 	//24 - Importe Total Comprobante
					$Dato .= 'PEN|';												//25 - Tipo Moneda
					$Dato .= '1.000|';												//26 - Tipo de Cambio
					$Dato .= '01/01/0001|';											//27 - Fec. Emisión Comprobante a afectar
					$Dato .= '00|';													//28 - Tipo de Comprobante a afectar
					$Dato .= '-|';													//29 - Serie Comprobante a afectar
					$Dato .= '-|';													//30 - Nro Comprobante a afectar
					$Dato .= str_pad('', 12, " ", STR_PAD_LEFT).'|'; 				//31 - Nro Contrato Consorcio
					$Dato .= '1|';													//32 - Inconcistencia en Tipo de Cambio
					$Dato .= '1|';													//33 - Medio de pago
					$Dato .= '1|';													//34 - Estado de Oportunidad
					$Dato .= '\n';

					fwrite($file, trim($Dato).PHP_EOL);
				}

				//$impmes += $row["impmes"];
//				$impigv += $row["impigv"];
			}

			$imptotal += ($impmes + $impigv) - ($TotalRestar - $xt);
			$cRecibos = $count;
			$tRecibos = $imptotal;

			$count = 0;

			$TotalF = $imptotal;
			$impmes = 0;
			$impigv = 0;
			$imptotal = 0;
			$cBoletas = 0;
			$tBoletas = 0;
			$cFacturas = 0;
			$tFacturas = 0;

//BOLETAS Y FACTURAS
			$Sql = "(SELECT
						c.nroinscripcion,
						c.propietario AS propietario,
						c.direccion AS direccion,
						'' AS codcatastro,
						to_char(c.codsuc, '00') || '' || TRIM(to_char(c.nroinscripcion, '000000')) AS codantiguo,
						c.nroprepago,
						SUM(d.importe) AS impmes, c.fechareg, d.coddocumento, d.serie, d.nrodocumento, c.documento AS nrodoc
					FROM cobranza.cabprepagos c
						INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
							AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
							AND (c.nroinscripcion = d.nroinscripcion)
						INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
							AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
					WHERE c.codsuc = {$codsuc} AND c.estado <> 0 AND c.estado IN (1, 2)
						AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
						AND d.coddocumento IN (13, 14) AND co.codconcepto <> 10005
					GROUP BY c.nroinscripcion, c.propietario, c.direccion, c.codsuc, c.nroprepago, c.fechareg, d.coddocumento, d.serie, d.nrodocumento, c.documento
				)
				UNION
				(
					SELECT
						c.nroinscripcion,
						c.propietario AS propietario,
						c.direccion AS direccion,
						'' AS codcatastro,
						to_char(c.codsuc, '00') || '' || TRIM(to_char(c.nroinscripcion, '000000')) AS codantiguo,
						c.nropago,
						SUM(d.importe) AS impmes, c.fechareg, d.coddocumento, d.serie, d.nrodocumento, c.nrodocumento AS nrodoc
					FROM cobranza.cabpagos c
						INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
							AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago)
							AND (c.nroinscripcion = d.nroinscripcion)
						INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
							AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
						WHERE c.codsuc = {$codsuc} AND c.anulado = 0 AND c.car <> 9
							AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
							AND d.coddocumento IN (13, 14) AND co.codconcepto <> 10005
						GROUP BY c.nroinscripcion, c.propietario, c.direccion, c.codsuc, c.nropago, c.fechareg, d.coddocumento, d.serie, d.nrodocumento, c.nrodocumento
					)
					ORDER BY fechareg, nrodocumento ";

			$items = $conexion->query($Sql)->fetchAll();
			//var_dump($consulta->errorInfo());
			//
			foreach($items as $row)
			{
				$count++;

				$Cuo = '';

				if($row['coddocumento'] == 13)
				{
					$cBoletas++;
					$tBoletas += $row["impmes"] + $row["impigv"];

					$Cuo = '2';
				}
				if($row['coddocumento'] == 14)
				{
					$cFacturas++;
					$tFacturas += $row["impmes"] + $row["impigv"];

					$Cuo = '3';
				}

				$impmes += $row["impmes"];
				$impigv += $row["impigv"];

				$Propietario = str_replace("º", " ", trim($row["propietario"]));
				$Propietario = str_replace("Ñ", "N", trim($Propietario));
				$Propietario = str_replace("/", " ", trim($Propietario));
				$Propietario = str_replace("|", "", trim($Propietario));
				$Propietario = str_replace("'", "", trim($Propietario));

				$ImporteMes = str_replace(",", "", number_format($row["impmes"], 2));
				$ImporteIgv = str_replace(",", "", number_format($row["impigv"], 2));
				$ImporteMesIgv = str_replace(",", "", number_format($row["impmes"] + $row["impigv"], 2));

				if ($Archivo == 1)
				{
					$Dato = $anio.str_pad($mes, 2, "0", STR_PAD_LEFT).'00'.'|';		//01 - Periodo
					$Dato .= $codsuc.$Cuo.'|';										//02 - CUO
					$Dato .= 'M'.str_pad($count, 5, "0", STR_PAD_LEFT).'|';			//03 - Correlativo
					$Dato .= $this->DecFecha($row["fechareg"]).'|';					//04 - Fecha de Emisión del Comprobante de Pago
					$Dato .= $this->DecFecha($row["fechareg"]).'|';					//05 - Fecha de Vencimiento del Comprobante de Pago
					$Dato .= $this->EquivalenteDoc($row["coddocumento"], 1).'|';	//06 - Tipo de Comprobante de Pago
					$Dato .= str_pad($row["serie"], 4, "0", STR_PAD_LEFT).'|';		//07 - Serie de Comprobante de Pago
					$Dato .= str_pad(($row["nrodocumento"]?$row["nrodocumento"]:0), 8, "0", STR_PAD_LEFT).'|';	//08 - Nro de Comprobante de Pago
					
					$Doc = 0;
					$Num = '00000000000';

					if ($this->EquivalenteDoc($row["coddocumento"], 1) == '01')
					{
						$Doc = 6;

						$Num = trim($row["nrodoc"]);
						$Num = trim(str_replace("-", "", $Num));
						$Num = trim(str_replace("DNI", "", $Num));
						$Num = trim(str_replace("RUC", "", $Num));
					}
					
					$Dato .= '|';													//09 - Tickets
					$Dato .= $Doc.'|';												//10 - Tipo Documento Ident.
					$Dato .= $Num.'|';												//11 - Nro Doc. Identidad
					$Dato .= str_pad($Propietario, 100, " ", STR_PAD_RIGHT).'|';	//12 - Razón social del Cliente
					$Dato .= '0.00|';												//13 - Exportaciones
					$Dato .= '0.00|';												//14 - Exportaciones
					$Dato .= '0.00|';												//15 - Exportaciones
					$Dato .= str_pad($ImporteIgv, 10, " ", STR_PAD_LEFT).'|'; 		//16 - Importe IGV
					$Dato .= '0.00|'; 												//17 - Importe IGV - Descuento
					$Dato .= '0.00|';												//18 - Importe Total Exoneracion
					$Dato .= str_pad($ImporteMes, 10, " ", STR_PAD_LEFT).'|';		//19 - Importe Total Inafecto
					$Dato .= '0.00|';												//20 - Imp. Selectivo al Consumo
					$Dato .= '0.00|';												//21 - Base Imponible - Arroz Pilado
					$Dato .= '0.00|';												//22 - Impuesto - Arroz Pilado
					$Dato .= '0.00|';												//23 - Otros Conceptos
					$Dato .= str_pad($ImporteMesIgv, 10, " ", STR_PAD_LEFT).'|'; 	//24 - Importe Total Comprobante
					$Dato .= 'PEN|';												//25 - Tipo Moneda
					$Dato .= '1.000|';												//26 - Tipo de Cambio
					$Dato .= '01/01/0001|';											//27 - Fec. Emisión Comprobante a afectar
					$Dato .= '00|';													//28 - Tipo de Comprobante a afectar
					$Dato .= '-|';													//29 - Serie Comprobante a afectar
					$Dato .= '-|';													//30 - Nro Comprobante a afectar
					$Dato .= str_pad('', 12, " ", STR_PAD_LEFT).'|'; 				//31 - Nro Contrato Consorcio
					$Dato .= '1|';													//32 - Inconcistencia en Tipo de Cambio
					$Dato .= '1|';													//33 - Medio de pago
					$Dato .= '1|';													//34 - Estado de Oportunidad
					$Dato .= '\n';
					
					fwrite($file, trim($Dato).PHP_EOL);

					//if ($Num != '-')
//					{
//						return;
//					}
				}
			}

			$imptotal += ($impmes + $impigv);

			$TotalR = $imptotal;
			//echo $TotalF+$TotalR;

			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;

//REBAJAS
			$Sql = "SELECT c.fechaemision AS fechareg,
						d.coddocumentoabono AS coddocumento,
						d.seriedocumentoabono AS serie,
						d.nrodocumentoabono AS nrodocumento,
						cl.codtipodocumento, cl.nrodocumento AS nrodoc,
						c.nroinscripcion, cl.propietario, SUM(d.imprebajado) * -1 AS impmes, 0 AS impigv,
						d.coddocumento AS coddocumento2, 
						d.nrodocumento AS nrodocumento2,
						d.seriedocumento AS seriedocumento2
					FROM facturacion.cabrebajas c
						INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp)
							AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja)
						INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
				  			AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
				  		INNER JOIN catastro.clientes AS cl on(c.codemp = cl.codemp AND c.codsuc = cl.codsuc
							AND c.nroinscripcion = cl.nroinscripcion)
					WHERE c.codsuc = ".$codsuc." AND c.estareg = 1 AND c.anio = '".$anio."' AND c.mes = '".intval($mes)."'
					GROUP BY c.fechaemision, d.coddocumentoabono, d.seriedocumentoabono, d.nrodocumentoabono, cl.codtipodocumento, cl.nrodocumento,
						c.nroinscripcion, cl.propietario, d.coddocumento, d.nrodocumento, d.seriedocumento
					ORDER BY c.fechaemision ";

			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			//var_dump($consulta->errorInfo());

			foreach($items as $row)
			{
				$count++;

				$SaltoLinea = "\n";

				if (count($row) == $count)
				{
					$SaltoLinea = "";
				}

				$flagD = false;

				$NroDocumento = $row["nrodocumento"]?$row["nrodocumento"]:0;

				$impmes += $row["impmes"];
				$impigv += $row["impigv"];

				if ($NroDocumento == 0)
				{
					$MesImporte = $MesImporte + $row["impmes"];
				}
				else
				{
					$row["impmes"] = $row["impmes"] + $MesImporte;

					$MesImporte = 0;

					$flagD = true;
				}

				$Propietario = str_replace("º", " ", trim($row["propietario"]));
				$Propietario = str_replace("Ñ", "N", trim($Propietario));
				$Propietario = str_replace("/", " ", trim($Propietario));
				$Propietario = str_replace("|", "", trim($Propietario));
				$Propietario = str_replace("'", "", trim($Propietario));

				$ImporteMes = str_replace(",", "", number_format($row["impmes"], 2));
				$ImporteMesIgv = str_replace(",", "", number_format($row["impmes"] + $row["impigv"], 2));

				if ($flagD)
				{
					$Dato = $anio.str_pad($mes, 2, "0", STR_PAD_LEFT).'00'.'|';		//01 - Periodo
					$Dato .= $codsuc.'5|';											//02 - CUO
					$Dato .= 'M'.str_pad($count, 5, "0", STR_PAD_LEFT).'|';			//03 - Correlativo
					$Dato .= $this->DecFecha($row["fechareg"]).'|';					//04 - Fecha de Emisión del Comprobante de Pago
					$Dato .= $this->DecFecha($row["fechareg"]).'|';					//05 - Fecha de Vencimiento del Comprobante de Pago
					$Dato .= $this->EquivalenteDoc($row["coddocumento"], 1).'|';	//06 - Tipo de Comprobante de Pago
					$Dato .= str_pad($row["serie"], 4, "0", STR_PAD_LEFT).'|';		//07 - Serie de Comprobante de Pago
					$Dato .= str_pad(($row["nrodocumento"]?$row["nrodocumento"]:0), 8, "0", STR_PAD_LEFT).'|';	//08 - Nro de Comprobante de Pago
					$Dato .= '|';													//09 - Tickets
					$Dato .= '0|';													//10 - Tipo Documento Ident.
					$Dato .= str_pad('', 11, "0", STR_PAD_RIGHT).'|';				//11 - Nro Doc. Identidad
					$Dato .= str_pad($Propietario, 100, " ", STR_PAD_RIGHT).'|';	//12 - Razón social del Cliente
					$Dato .= '0.00|';												//13 - Exportaciones
					$Dato .= '0.00|';												//14 - Exportaciones
					$Dato .= '0.00|';												//15 - Exportaciones
					$Dato .= str_pad($ImporteIgv, 10, " ", STR_PAD_LEFT).'|'; 		//16 - Importe IGV
					$Dato .= '0.00|'; 												//17 - Importe IGV - Descuento
					$Dato .= '0.00|';												//18 - Importe Total Exoneracion
					$Dato .= str_pad($ImporteMes, 10, " ", STR_PAD_LEFT).'|';		//19 - Importe Total Inafecto
					$Dato .= '0.00|';												//20 - Imp. Selectivo al Consumo
					$Dato .= '0.00|';												//21 - Base Imponible - Arroz Pilado
					$Dato .= '0.00|';												//22 - Impuesto - Arroz Pilado
					$Dato .= '0.00|';												//23 - Otros Conceptos
					$Dato .= str_pad($ImporteMesIgv, 10, " ", STR_PAD_LEFT).'|'; 	//24 - Importe Total Comprobante
					$Dato .= 'PEN|';												//25 - Tipo Moneda
					$Dato .= '1.000|';												//26 - Tipo de Cambio
					$Dato .= $this->FechaDoc($row["coddocumento2"], $row["seriedocumento2"], $row["nrodocumento2"]).'|';	//27 - Fec. Emisión Comprobante a afectar
					$Dato .= $this->EquivalenteDoc($row["coddocumento2"], 1).'|';	//28 - Tipo de Comprobante a afectar
					$Dato .= str_pad($row["seriedocumento2"], 4, "0", STR_PAD_LEFT).'|';	//29 - Serie Comprobante a afectar
					$Dato .= str_pad(($row["nrodocumento2"]?$row["nrodocumento2"]:0), 8, "0", STR_PAD_LEFT).'|';	//30 - Nro Comprobante a afectar
					$Dato .= str_pad('', 12, " ", STR_PAD_LEFT).'|'; 				//31 - Nro Contrato Consorcio
					$Dato .= '1|';													//32 - Inconcistencia en Tipo de Cambio
					$Dato .= '1|';													//33 - Medio de pago
					$Dato .= '1|';													//34 - Estado de Oportunidad
					$Dato .= '\n';
					
					fwrite($file, trim($Dato).PHP_EOL);
				}
			}

			$imptotal += ($impmes + $impigv);
			$tAbonos = ($impmes + $impigv);
			$cAbonos = $count;

			$TotalF = $imptotal;
			$count = 0;

//CARGOS
			$count = 0;

			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;

			$Sql = "(
					SELECT c.fechareg,
						to_char(c.codsuc, '00') || '' || TRIM(to_char(c.nroinscripcion, '000000')) AS codantiguo,
						c.propietario,
					 	SUM(d.interes) AS impmes, c.estareg,
					 	'' AS coddocumento2, '' AS seriedocumento2, '' AS nrodocumento2,
					 	'' AS coddocumento, '' AS nrodocumento, '' AS serie,
					 	c.nrocredito AS id, c.nroprepago, c.nroinscripcion, c.nroprepagoinicial,
					  	1 AS tipo
					FROM facturacion.cabcreditos c
						INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrocredito = d.nrocredito)
					WHERE c.codsuc = ".$codsuc." AND c.estareg = 1
						AND extract(year from c.fechareg) = '".$anio."'
						AND extract(month from c.fechareg) = '".intval($mes)."'
						AND c.interes > 0 AND c.nroprepago > 0 AND c.sininteres = 0 AND c.nroprepagoinicial > 0
					GROUP BY c.fechareg,
						to_char(c.codsuc, '00') || '' || TRIM(to_char(c.nroinscripcion, '000000')), c.estareg,
						c.propietario,
						c.nrocredito, c.nroprepago, c.nroinscripcion, c.nroprepagoinicial
					ORDER BY fechareg, to_char(c.codsuc, '00') || '' || TRIM(to_char(c.nroinscripcion, '000000'))
					)
					UNION
					(
					SELECT c.fechaemision AS fechareg,
					to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')) as codantiguo,
					clie.propietario AS propietario,
					CAST(SUM(d.importe) - c.totalrefinanciado AS NUMERIC (18, 2)) as impmes,c.estareg,
					'' as coddocumento2, '' as seriedocumento2, '' as nrodocumento2,
					'' as coddocumento, '' as nrodocumento, '' as serie,
					c.nrorefinanciamiento as id,c.nroprepago,c.nroinscripcion,c.nroprepagoinicial,
					2 AS tipo
					FROM facturacion.cabrefinanciamiento c
					INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorefinanciamiento = d.nrorefinanciamiento)
					INNER JOIN catastro.clientes as clie on(c.codemp=clie.codemp and c.codsuc=clie.codsuc and c.nroinscripcion=clie.nroinscripcion)
					where d.codemp=1 and d.codsuc=".$codsuc." AND c.estareg=1
					AND extract(year from c.fechaemision) ='".$anio."'
					AND extract(month from c.fechaemision) ='".intval($mes)."'
					and c.interes>0 AND c.nroprepagoinicial>0 AND c.sininteres=0  AND c.codestadorefinanciamiento <> 5
					GROUP BY c.totalrefinanciado,c.fechaemision,
					to_char(c.codsuc, '00')||''||TRIM(to_char(c.nroinscripcion,'000000')),c.estareg,
					clie.propietario,
					c.nrorefinanciamiento,
					c.nroprepago,c.nroinscripcion,c.nroprepagoinicial

					ORDER BY c.nroinscripcion
					)
					ORDER BY tipo, fechareg, nrodocumento";
			//die($Sql);
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			//var_dump($consulta->errorInfo());
			foreach($items as $row)
			{
				$count++;

				if($row['tipo'] == 1)
				{
					$fechadocu = $this->DecFecha($row["fechareg"]);
					$datos = $this->DatosPrepago($row["nroinscripcion"], $row["nroprepago"]);

					$row["coddocumento2"] = $datos['coddocumento'];
					$row["seriedocumento2"] = $datos['serie'];
					$row["nrodocumento2"] = $datos['nrodocumento'];

					$datos = $this->DatosPrepago($row["nroinscripcion"], $row["nroprepagoinicial"]);

					$row["coddocumento"] = $datos['coddocumento'];
					$row["serie"] = $datos['serie'];
					$row["nrodocumento"] = $datos['nrodocumento'];

					$Cuo = '4';
				}
				else
				{
					$datos = $this->DatosRef($row['id']);

					$row["coddocumento2"] = $datos['coddocumento'];
					$row["seriedocumento2"] = $datos['serie'];
					$row["nrodocumento2"] = $datos['nrodocumento'];

					$fechadocu = $this->DecFecha($datos['fechareg']);
					$datos = $this->DatosPrepago($row["nroinscripcion"], $row["nroprepagoinicial"]);

					$row["coddocumento"] = $datos['coddocumento'];
					$row["serie"] = $datos['serie'];
					$row["nrodocumento"] = $datos['nrodocumento'];

					$Cuo = '6';
				}

				$impmes += $row["impmes"];
				$impigv += $row["impigv"];

				$Propietario = str_replace("º", " ", trim($row["propietario"]));
				$Propietario = str_replace("Ñ", "N", trim($Propietario));
				$Propietario = str_replace("/", " ", trim($Propietario));
				$Propietario = str_replace("|", "", trim($Propietario));
				$Propietario = str_replace("'", "", trim($Propietario));

				$ImporteMes = str_replace(",", "", number_format($row["impmes"], 2));
				$ImporteMesIgv = str_replace(",", "", number_format($row["impmes"] + $row["impigv"], 2));

				if ($Archivo == 1)
				{
					$Dato = $anio.str_pad($mes, 2, "0", STR_PAD_LEFT).'00'.'|';		//01 - Periodo
					$Dato .= $codsuc.$Cuo.'|';										//02 - CUO
					$Dato .= 'M'.str_pad($count, 5, "0", STR_PAD_LEFT).'|';			//03 - Correlativo
					$Dato .= $this->DecFecha($row["fechareg"]).'|';					//04 - Fecha de Emisión del Comprobante de Pago
					$Dato .= $this->DecFecha($row["fechareg"]).'|';					//05 - Fecha de Vencimiento del Comprobante de Pago
					$Dato .= $this->EquivalenteDoc($row["coddocumento"], 1).'|';	//06 - Tipo de Comprobante de Pago
					$Dato .= str_pad($row["serie"], 4, "0", STR_PAD_LEFT).'|';		//07 - Serie de Comprobante de Pago
					$Dato .= str_pad(($row["nrodocumento"]?$row["nrodocumento"]:0), 8, "0", STR_PAD_LEFT).'|';	//08 - Nro de Comprobante de Pago
					$Dato .= '|';													//09 - Tickets
					$Dato .= '0|';													//10 - Tipo Documento Ident.
					$Dato .= str_pad('', 11, "0", STR_PAD_RIGHT).'|';				//11 - Nro Doc. Identidad
					$Dato .= str_pad($Propietario, 100, " ", STR_PAD_RIGHT).'|';	//12 - Razón social del Cliente
					$Dato .= '0.00|';												//13 - Exportaciones
					$Dato .= '0.00|';												//14 - Exportaciones
					$Dato .= '0.00|';												//15 - Exportaciones
					$Dato .= str_pad($ImporteIgv, 10, " ", STR_PAD_LEFT).'|'; 		//16 - Importe IGV
					$Dato .= '0.00|'; 												//17 - Importe IGV - Descuento
					$Dato .= '0.00|';												//18 - Importe Total Exoneracion
					$Dato .= str_pad($ImporteMes, 10, " ", STR_PAD_LEFT).'|';		//19 - Importe Total Inafecto
					$Dato .= '0.00|';												//20 - Imp. Selectivo al Consumo
					$Dato .= '0.00|';												//21 - Base Imponible - Arroz Pilado
					$Dato .= '0.00|';												//22 - Impuesto - Arroz Pilado
					$Dato .= '0.00|';												//23 - Otros Conceptos
					$Dato .= str_pad($ImporteMesIgv, 10, " ", STR_PAD_LEFT).'|'; 	//24 - Importe Total Comprobante
					$Dato .= 'PEN|';												//25 - Tipo Moneda
					$Dato .= '1.000|';												//26 - Tipo de Cambio
					$Dato .= $fechadocu.'|';										//27 - Fec. Emisión Comprobante a afectar
					$Dato .= $this->EquivalenteDoc($row["coddocumento2"], 1).'|';	//28 - Tipo de Comprobante a afectar
					$Dato .= str_pad($row["seriedocumento2"], 4, "0", STR_PAD_LEFT).'|';	//29 - Serie Comprobante a afectar
					$Dato .= str_pad(($row["nrodocumento2"]?$row["nrodocumento2"]:0), 8, "0", STR_PAD_LEFT).'|';	//30 - Nro Comprobante a afectar
					$Dato .= str_pad('', 12, " ", STR_PAD_LEFT).'|'; 				//31 - Nro Contrato Consorcio
					$Dato .= '1|';													//32 - Inconcistencia en Tipo de Cambio
					$Dato .= '1|';													//33 - Medio de pago
					$Dato .= '1|';													//34 - Estado de Oportunidad
					$Dato .= '\n';
					
					fwrite($file, trim($Dato).PHP_EOL);
				}
			}

			$imptotal += ($impmes + $impigv);
			$tCargos = ($impmes + $impigv);
			$cCargos = $count;

			$TotalF = $imptotal;
			$count = 0;

//Total
//			$Dato = number_format($tFacturas + $tBoletas + $tCargos - $tAbonos + $tRecibos, 2)."\n";
//
//			fwrite($file, $Dato.PHP_EOL);
//
//			fclose($file);

		}

		function FechaDoc($cod, $serie, $numero)
		{
			global $conexion, $codsuc;

			$Sql = "SELECT fechareg FROM facturacion.cabfacturacion
					WHERE codsuc=".$codsuc." AND coddocumento=".$cod."
					AND serie='".$serie."' AND nrodocumento='".$numero."'";

			$consulta = $conexion->prepare($Sql);
			$consulta->execute();

			if ($consulta->errorCode() != '00000')
			{
				return '31/12/1900';
			}

			$c = $consulta->fetch();

			return $this->DecFecha($c[0]);
		}
		function DatosRef($nrorefinanciamiento)
		{
			global $codsuc, $conexion;

			$Sql = "SELECT f.serie, f.nrodocumento, f.nrofacturacion, f.fechareg, f.coddocumento
				FROM facturacion.cabfacturacion f
				  INNER JOIN facturacion.origen_refinanciamiento o ON (f.codemp = o.codemp)
				  AND (f.codsuc = o.codsuc)
				  AND (f.codciclo = o.codciclo)
				  AND (f.nrofacturacion = o.nrofacturacion)
				  AND (f.nroinscripcion = o.nroinscripcion)
				  where o.codemp=1 and o.codsuc=? and o.nrorefinanciamiento=?
				  GROUP BY f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg,f.coddocumento
				  ORDER BY f.nrofacturacion DESC ";
			$consulta2 = $conexion->prepare($Sql);
			$consulta2->execute(array($codsuc,$nrorefinanciamiento));
			$itemD = $consulta2->fetchAll();

			return $itemD[0];
		}
		function EquivalenteDoc($cod, $tipo)
		{
			if($tipo == 1)
			{
				switch ($cod)
				{
					case 4: return "14";break;
					case 13: return "03";break;
					case 14: return "01";break;
					case 17: return "07";break;
					case 16: return "08";break;
				}
			}
			else
			{
				switch ($cod)
				{
					case 1: return "1";break;
					case 3: return "6";break;
					default: return "0";break;
				}
			}
			/*
			COMPROBANTES
			-----------------------------
			14  Recibos de Pensiones
			01  Facturas
			03  Boletas de Ventas
			07  Notas de Abono/Credito
			08  Notas de cargo/Debito


			DOCUMENTOS
			-----------------------------
			1  DNI
			6  RUC
			0  Otros*/
		}
		function DatosPrepago($nroinscripcion,$nroprepago)
		{
			global $codsuc,$conexion;
			$Sql="SELECT  d.coddocumento, d.serie, d.nrodocumento
					FROM  cobranza.cabprepagos c
					INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
					 AND (c.nroinscripcion = d.nroinscripcion)
				  WHERE c.codsuc={$codsuc} AND c.nroinscripcion={$nroinscripcion}
				  		AND d.nroprepago={$nroprepago}
				GROUP BY d.coddocumento, d.serie, d.nrodocumento ";
			$consulta2 = $conexion->prepare($Sql);
			$consulta2->execute();
			$itemD = $consulta2->fetchAll();
			return $itemD[0];
		}
    }

	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc		= $_GET["codsuc"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclotext	= $_GET["ciclotext"];

	$Archivo	= 1;

	if($_GET["sector"] != "%")
	{
		$Csec = " AND cl.codsector = ".$_GET["sector"];
	}
	else
	{
		$Csec = "";
	}

	$m = $meses[$mes];

	$objReporte	= new clsRegistroVentas();

	$objReporte->agregar_detalle($codsuc, $ciclo, $anio, $mes, $Csec, $Archivo);
?>