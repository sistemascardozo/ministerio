<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();} 
	
	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$cars,$caja;

			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN TOTAL DEL ".$fecha." (TODOS LOS USUARIOS)";
			$this->Cell(190,$h+3,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			$this->Cell(190,0.01,'',1,1,'L');
			  $this->Ln(2);
			$h=4;
			/*$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(60, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"CAJA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,strtoupper($caja),0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$fecha,0,0,'L');
			$this->Cell(15, $h,"CAJERO",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(50, $h,$cajeros,0,1,'L');
			$this->Cell(190,0.01,'',1,1,'L');	
			*/
						
		}
		function tCar($IdCar,$Car)
		{
			global $conexion,$codsuc,$fecha,$cajero;
			$Sql ="select SUM(c.imptotal)
			from cobranza.cabpagos c
			where c.codsuc=".$codsuc." and c.nropec=0 and c.car=".$IdCar." and 
			c.fechareg='".$fecha."'  AND c.anulado=0
			";
			$Consulta 	= $conexion->query($Sql);
			$row		= $Consulta->fetch();
			$Total = $row[0];
			$h=4;
			$this->Ln(2);
			$this->SetFont('Arial','',9);
			$this->SetTextColor(0,0,0);
			$this->Cell(45, $h,strtoupper($Car),0,0,'L');
			$this->Cell(5, $h,':',0,0,'C');
			$this->Cell(20, $h,number_format($Total,2),0,1,'R');
			$this->Ln(2);
		}
		function tCars()
		{
			global $conexion,$codsuc,$fecha,$cajero;
			$Sql ="select SUM(c.imptotal)
			from cobranza.cabpagos c
			where c.codsuc=".$codsuc." and c.nropec=0 and 
			c.fechareg='".$fecha."'  AND c.anulado=0
			";
			$this->Ln(2);
			$Consulta 	= $conexion->query($Sql);
			$row		= $Consulta->fetch();
			$Total = $row[0];
			$h=4;
			$this->Ln(2);
			$this->SetFont('Arial','',9);
			$this->SetTextColor(0,0,0);
			$this->Cell(45, $h,strtoupper(utf8_decode('TOTAL RECAUDACIÓN')),0,0,'L');
			$this->Cell(5, $h,':',0,0,'C');
			$this->Cell(20, $h,number_format($Total,2),0,1,'R');
			$this->Ln(2);
		}
		function rFormaPago()
		{
			global $conexion,$codsuc,$car,$fecha,$cajero;
			$Sql ="select fp.descripcion,SUM(c.imptotal)
			from cobranza.cabpagos c
			INNER JOIN public.formapago fp ON ( fp.codformapago=c.codformapago)
			where c.codsuc=".$codsuc." and c.nropec=0 and 
			c.fechareg='".$fecha."'  AND c.anulado=0
			GROUP BY fp.descripcion";
			$this->Ln(5);
			$this->SetFont('Arial','B',12);
			$Consulta =$conexion->query($Sql);
			$Total=0;
			$this->SetFont('Arial','',9);
			foreach($Consulta->fetchAll() as $row)
			{
			
				$this->Cell(45,4,strtoupper($row[0]),0,0,'C',false);
				$this->Cell(5,4,':',0,0,'C',false);
				$this->Cell(20,4,number_format($row[1],2),0,1,'R',false);
				$Total+=$row[1];
			}	
			$this->Ln(2);
			$this->SetX(60);
			$this->Cell(20,0.01,'',1,1,'R',false);
			$this->Ln(2);
			$this->Cell(45,4,'TOTAL',0,0,'C',false);
			$this->Cell(5,4,':',0,0,'C',false);
			$this->Cell(20,4,number_format($Total,2),0,1,'R',false);
			$this->Ln(5);

		}
		
		
    }
	
	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];
	
	$cajeros 	= $_SESSION['user'];
	$cars		= $_GET["cars"];
	$caja		= $_GET["caja"];
        
	$objReporte	=	new clsTalones();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fecha		= $objReporte->CodFecha($_GET["fecha"]);
	$Sql ="select ca.car, ca.descripcion 
			from cobranza.cabpagos c
			INNER JOIN cobranza.car ca ON ( ca.car=c.car AND ca.codemp = c.codemp AND ca.codsuc = c.codsuc ) 
			where c.codsuc=".$codsuc." and c.nropec=0 and 
			c.fechareg='".$fecha."' AND c.anulado=0
			GROUP BY ca.car, ca.descripcion ";
	$ConsultaCar =$conexion->query($Sql);
	$objReporte->SetFont('Arial','B',12);
	$objReporte->Cell(190,4,utf8_decode('CENTROS DE RECAUDACIÓN'),0,1,'L');
	$objReporte->Ln(5);
	foreach($ConsultaCar->fetchAll() as $rowcar)
	{	
		$car=$rowcar['car']	;
		$objReporte->tCar($rowcar[0],$rowcar[1]);
	
	}
	$objReporte->SetX(60);
	$objReporte->Cell(20,0.01,'',1,1,'R',false);
	$objReporte->tCars();
	$objReporte->SetFont('Arial','',9);
	$objReporte->Cell(190,0.01,'',1,1,'L');
	$objReporte->Ln(5);
	$objReporte->SetFont('Arial','B',12);
	$objReporte->Cell(190,4,'FORMAS DE PAGO',0,1,'L');
	$objReporte->rFormaPago();
	$objReporte->Cell(190,0.001,'',1,1,'L');
	$objReporte->Output();	
	
?>