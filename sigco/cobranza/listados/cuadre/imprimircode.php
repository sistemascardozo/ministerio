<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsReporte.php");
	//require_once  'Image/Barcode.php';
	class clsCuadre extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$cars,$caja;
                        
			$h=4;
			
			$this->Ln(10);
			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN DE PLANILLA ENTRADA CAJA";
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
				
			$grafico = new Graph();
			$grafico=Image_Barcode::draw('123', 'code128', 'jpg', false);
			imagejpeg($grafico,'barra.jpg');
			$this->Image('barra.jpg',6,1,17,16);

			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(60, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"CAJA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,strtoupper($caja),0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$fecha,0,0,'L');
			$this->Cell(15, $h,"CAJERO",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(50, $h,$cajeros,0,1,'L');
				
			$this->Ln(1);
			$this->Cell(75, $h,"CONCEPTO",1,0,'C');
			$this->Cell(20, $h,"NRO. ITEMS",1,0,'C');
			$this->Cell(20, $h,"CODCTA.",1,0,'C');
			$this->Cell(25, $h,"CANCELADO",1,0,'C');
			$this->Cell(25, $h,"A CUENTA",1,0,'C');
			$this->Cell(25, $h,"TOTAL",1,1,'C');
						
		}
		function ContenidoCab($concepto,$cancelado,$acuenta)
		{
			$h=4;
			
			$this->SetFont('Arial','B',7);
			$this->SetTextColor(230,0,0);
			
			$this->Cell(75, $h,strtoupper($concepto),0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoDet($concepto,$cancelado,$acuenta,$ctacontable)
		{
			$h=4;
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(70, $h,$concepto,0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,$ctacontable,0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoFoot($cancelado,$acuenta)
		{
			$h=4;
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(70, $h,"",0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"TOTALES",0,0,'R');
			$this->Cell(25, $h,number_format($cancelado,2),'T',0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),'T',0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),'T',1,'R');
		}
    }
	
	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];
	
	$cajeros 	= $_SESSION['nombre'];
	$cars		= $_GET["cars"];
	$caja		= $_GET["caja"];

	$objReporte	= new clsCuadre();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	
	$impcancelado	= 0;
	$impamortizado 	= 0;
	
	$sql = "select * from cobranza.f_cuadre_cab_cierre(:codsuc,:fechainicio,:fechafinal,:car,:parametro) as 
			(codcategoria public.parametro,codtipodeuda integer,categoria integer,categoria_descripcion varchar,
			impcancelado numeric,impamortizado numeric);";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":codsuc"=>$codsuc,
							 ":car"=>$car,
							 ":fechainicio"=>$fecha,
							 ":fechafinal"=>$fecha,
							 ":parametro"=>" and c.nropec=0 and c.creador=".$cajero));
	$items = $consulta->fetchAll();
	//var_dump($consulta->errorInfo());
	foreach($items as $row)
	{
		$objReporte->ContenidoCab($row["categoria_descripcion"],$row["impcancelado"],$row["impamortizado"]);
		//echo $row["impcancelado"]."<br>";
		$sqlD = "select * from cobranza.f_cuadre_det_cierre(:codsuc,:car,:fechaini,:fechafin,:codcategoria,:parametro)
				 as (concepto varchar,codcategoria public.parametro,codconcepto public.parametro,car public.parametro,
				 fechareg public.fechareg,creador public.codusu,nrocaja public.parametro,nropec integer,ctadebe public.noroctacontable,
				 impcancelado numeric,impamortizado numeric)";
		
		$impcancelado 	+= $row["impcancelado"];
		$impamortizado 	+= $row["impamortizado"];
		//echo "<br>".$codsuc."-".$car."-".$fecha."-".$fecha."-".$row["codcategoria"]."-".$cajero;
		$consultaD = $conexion->prepare($sqlD);
		$consultaD->execute(array(":codsuc"=>$codsuc,
									  ":car"=>$car,
									  ":fechaini"=>$fecha,
									  ":fechafin"=>$fecha,
									  ":codcategoria"=>$row["codcategoria"],
									  ":parametro"=>" and nropec=0 and cab.creador=".$cajero));
		$itemsD = $consultaD->fetchAll();

		if(!$itemsD)
		{
			$conexion->rollBack();
			var_dump($result->errorInfo());

			die();
		}
		foreach($itemsD as $rowD)
		{
			$t=$rowD["impcancelado"]+$rowD["impamortizado"];
			if($t!=0)
			{
				$objReporte->ContenidoDet(strtoupper($rowD["concepto"]),$rowD["impcancelado"],$rowD["impamortizado"],$rowD["ctadebe"]);
			}
		}
	}
	$objReporte->ContenidoFoot($impcancelado,$impamortizado);

	$objReporte->Output();	
	
?>