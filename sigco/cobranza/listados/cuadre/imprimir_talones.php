<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();} 
	
	include("../../../../objetos/clsReporte.php");

	class clsTalones extends clsReporte
	{
		function cabecera()
		{
			global $cajeros,$fecha,$cars,$caja;
			$this->SetY($this->GetY()-5);
			$this->SetFont('Arial','B',8);
			$tit1 = "";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'R');

			$this->SetFont('Arial','B',14);
			$tit1 = "VERIFICACION PLANILLA ENTRADA A CAJA POR TALONES";
			$this->Cell(190,$h+2,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(60, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"CAJA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,strtoupper($caja),0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$fecha,0,0,'L');
			$this->Cell(15, $h,"CAJERO",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(50, $h,$cajeros,0,1,'L');
				
			$this->Ln(1);
			$this->SetFont('Arial','',7);
			$this->Cell(15, $h,"Nro. Pago",1,0,'C');
			$this->Cell(15, $h,"Codigo",1,0,'C');
			$this->Cell(65, $h,"Cliente",1,0,'C');
			$this->Cell(15, $h,"Doc",1,0,'C');
			$this->Cell(15, $h,"Serie",1,0,'C');
			$this->Cell(20, $h,"Nro. Doc.",1,0,'C');
			$this->Cell(20, $h,"Total",1,0,'C');
			$this->Cell(20, $h,"Efectivo",1,0,'C');
			$this->Cell(5, $h,"A",1,1,'C');
						
		}
		function contenido($nropago,$codigo,$usuario,$doc,$serie,$nrodoc,$total,$efectivo,$anulado,$i)
		{
			$h=4;
			global $codsuc;
			$codigo = $this->CodUsuario($codsuc,$codigo);
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			$this->Cell(15, $h,$i,0,0,'C');
			$this->Cell(15, $h,$codigo,0,0,'C');
			$this->Cell(65, $h,utf8_decode($usuario),0,0,'L');
			$this->Cell(15, $h,$doc,0,0,'C');
			$this->Cell(15, $h,$serie,0,0,'C');
			$this->Cell(20, $h,$nrodoc,0,0,'C');
			$this->Cell(20, $h,$total,0,0,'R');
			$this->Cell(20, $h,$efectivo,0,0,'R');
			$this->Cell(5, $h,$anulado,0,1,'C');
			
		}
		function contenidofoot($cont_normal,$cont_anulado,$tot_procesado,$total_anulado,$cont_rpspublic,$cont_boleta,$cont_factura,$tot_rpspublic,$tot_boleta,$tot_factura)
		{
			$h=4;
			
			$this->Ln(5);
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Nro de Resibos por Serv. P.",0,0,'R');
			$this->Cell(15, $h,$cont_rpspublic,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Resibos por Serv. P.",0,0,'R');
			$this->Cell(20, $h,$tot_rpspublic,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Nro de Boletas.",0,0,'R');
			$this->Cell(15, $h,$cont_boleta,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Boletas",0,0,'R');
			$this->Cell(20, $h,$tot_boleta,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Nro de Facturas.",0,0,'R');
			$this->Cell(15, $h,$cont_factura,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Facturas",0,0,'R');
			$this->Cell(20, $h,$tot_factura,0,1,'R');
			$this->Cell(190,0.1,"",1,1,'C');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Cobrados",0,0,'R');
			$this->Cell(15, $h,$cont_normal,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Procesado",0,0,'R');
			$this->Cell(20, $h,$tot_procesado,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Totales de Talones Anulado",0,0,'R');
			$this->Cell(15, $h,$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Menos Total Anulado",0,0,'R');
			$this->Cell(20, $h,$total_anulado,0,1,'R');
			
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(50, $h,"Total de Talones",0,0,'R');
			$this->Cell(15, $h,$cont_normal+$cont_anulado,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(40, $h,"Total Cobrado",0,0,'R');
			$this->Cell(20, $h,$tot_procesado,0,1,'R');
		}
    }
	
	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];
	$cajeros 	= $_SESSION['user'];
	$cars		= $_GET["cars"];
	$caja		= $_GET["caja"];
        
	$objReporte	=	new clsTalones();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fecha		= $objReporte->CodFecha($_GET["fecha"]);
	
	$Sql = "SELECT DISTINCT(c.nropago), d.coddocumento, doc.descripcion ";
	$Sql .= "FROM cobranza.cabpagos c ";
	$Sql .= " INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nroinscripcion = d.nroinscripcion) AND (c.nropago = d.nropago) ";
	$Sql .= " INNER JOIN reglasnegocio.documentos doc ON (d.coddocumento = doc.coddocumento) AND (d.codsuc = doc.codsuc) ";
	$Sql .= "WHERE c.codsuc = ".$codsuc." ";
	$Sql .= " AND c.nropec = 0 ";
	$Sql .= " AND c.car = ".$car." ";
	$Sql .= " AND c.fechareg = '".$fecha."' ";
	$Sql .= " AND c.creador = ".$cajero." ";
	$Sql .= "ORDER BY d.coddocumento, c.nropago";
	
	$consultaG = $conexion->prepare($Sql);
	$consultaG->execute(array());
	
	$itemsG = $consultaG->fetchAll();
	
	$cont_normal=0;
	$cont_anulado=0;
	$cont_rpspublic=0;
	$cont_boleta=0;
	$cont_factura=0;
	
	$tot_procesados=0;
	$total_anulado=0;
	$total_rpspublic=0;
	$total_boleta=0;
	$total_factura=0;
	$Temp='';
	$TotalDoc=0;
	$NonDoc='';
	
	$h = 5;
	$i = 0;
	
	foreach($itemsG as $rowG)
	{		
		if($Temp != $rowG[1])
		{
			if($NonDoc != "")
			{
				$objReporte->SetFont('Arial','B',7);
				$objReporte->SetTextColor(0,0,0);
				$objReporte->Cell(145, $h,strtoupper('SUB TOTAL '.$NonDoc.""),0,0,'R');
				$objReporte->Cell(20, $h,number_format($TotalDoc, 2),0,0,'R');
				$objReporte->Cell(5, $h,'',0,1,'C');
				$TotalDoc=0;
				
			}
			
			$NonDoc = $rowG[2];

			$objReporte->SetFont('Arial','B',7);
			$objReporte->Cell(0,5,strtoupper($rowG[2]),0,1,'L');
			
			$Temp = $rowG[1];
		}
		
		$sqlC = "SELECT c.nropago, c.nroinscripcion, c.propietario, c.imppagado, c.imptotal, c.anulado ";
		$sqlC .= "FROM cobranza.cabpagos c ";
		$sqlC .= "WHERE c.codsuc = ".$codsuc." ";
		$sqlC .= " AND c.nropec = 0 ";
		$sqlC .= " AND c.car = ".$car." ";
		$sqlC .= " AND c.fechareg = '".$fecha."' ";
		$sqlC .= " AND c.creador = ".$cajero." ";
		$sqlC .= " AND c.nropago = ".$rowG[0]." ";
		
		$consultaC = $conexion->prepare($sqlC);
		$consultaC->execute(array());
		
		$itemsC = $consultaC->fetchAll();
		
		foreach($itemsC as $rowC)
		{
			$sqlD = "SELECT MAX(nrodocumento) AS nrodocumento, ";
			$sqlD .= " CASE WHEN coddocumento = 4 THEN 'RSP' WHEN coddocumento = 13 THEN 'BOL.' WHEN coddocumento = 14 THEN 'FAC.' END AS coddocumento, ";
			$sqlD .= " serie, coddocumento AS coddocumentocod, SUM(importe) AS importe ";
			$sqlD .= "FROM cobranza.detpagos ";
			$sqlD .= "WHERE codemp = 1 ";
			$sqlD .= " AND codsuc = ".$codsuc." ";
			$sqlD .= " AND nroinscripcion = ".$rowC["nroinscripcion"]." ";
			$sqlD .= " AND nropago = ".$rowC["nropago"]." ";
			$sqlD .= " AND nrodocumento<>0 ";
			$sqlD .= " AND coddocumento = ".$rowG['coddocumento']." ";
			$sqlD .= "GROUP BY coddocumento, serie";
			//die($sqlD); exit;
			$consultaD = $conexion->prepare($sqlD);
			$consultaD->execute(array());
			
			$itemsD = $consultaD->fetch();
			
			$i++;
			
			if($rowC["anulado"] == 1)
			{
				$x = "x";
				$cont_anulado++;
				$total_anulado += $itemsD["imptotal"];
			}
			else
			{
				$x = "";
				$cont_normal++;
				$tot_procesados += $rowC["imptotal"];
			}

			if($itemsD["coddocumentocod"] == 4)
			{
				$cont_rpspublic++;
				$tot_rpspublic += $rowC["imptotal"];
				
			}
			elseif($itemsD["coddocumentocod"] == 13)
			{
				$cont_boleta++;
				$tot_boleta += $rowC["importe"];
				
			}
			elseif ($itemsD["coddocumentocod"] == 14)
			{
				$cont_factura++;
				$tot_factura += $rowC["imptotal"];
			}
			
			$TotalDoc += $rowC["imptotal"];
			$objReporte->contenido($rowC["nropago"], $rowC["nroinscripcion"], $rowC["propietario"], $itemsD["coddocumento"], 
				$itemsD["serie"], $itemsD["nrodocumento"], number_format($rowC["imptotal"], 2), number_format($rowC["imppagado"], 2), $x, $i
				);
			
		}

	}
	
	if($NonDoc != "")
	{
		$objReporte->SetFont('Arial','B',7);
		$objReporte->SetTextColor(0,0,0);
		$objReporte->Cell(145, $h,strtoupper('SUB TOTAL '.$NonDoc.""),0,0,'R');
		$objReporte->Cell(20, $h,number_format($TotalDoc, 2),0,0,'R');
		$objReporte->Cell(5, $h,'',0,1,'C');
		$TotalDoc = 0;
		
	}
	
	$objReporte->contenidofoot($cont_normal,$cont_anulado,number_format($tot_procesados,2),number_format($total_anulado,2),$cont_rpspublic,$cont_boleta, $cont_factura,number_format($tot_rpspublic,2),number_format($tot_boleta,2),number_format($tot_factura,2));

	$objReporte->Output();	
	
?>