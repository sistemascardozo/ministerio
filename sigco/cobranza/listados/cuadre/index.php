<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$objDrop 	= new clsDrop();
	
	$TituloVentana = "CUADRE CAJA";
	
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc 	= $_SESSION['IdSucursal'];

	
	$codsuc 	= $_SESSION['IdSucursal'];
	$codusu		= $_SESSION['id_user'];
	$cajero		= $_SESSION['nombre'];
	
	$sql = "SELECT c.nrocaja, cj.descripcion
			FROM cobranza.cajasxusuario as c
			INNER JOIN cobranza.cajas cj ON(c.nrocaja = cj.nrocaja)
			WHERE c.codsuc = ? AND c.codusu = ?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc, $codusu));
	
	$items = $consulta->fetch();
	
	$car = $_SESSION["car"];
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	jQuery(function($)
	{ 
		$("#fecha").mask("99/99/9999");
		$("#DivTipos").buttonset();
		
		$('#car').val(<?=$car?>).attr('selected', 'selected');
		
		//if (<?=$car?> != 9)
		//{
			$('#car').attr("disabled", true);
		//}
	});
	$(function() {
		$( "#fecha" ).datepicker(
		{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true
		});
	});
	
	function ValidarForm(Op)
	{
		car = $("#car").val();
		
		if(car == 0)
		{
			Msj($("#car"), "Seleccione Car")
			return false
		}
		
		url = ""
		
		if($("#raeconsolidado").is(':checked')==false)
		{
			if(car==0)
			{
				alert("Seleccione el Car")
				return false		
			}
		}
		if($("#rabresumen").is(':checked')==true)
		{
			url="imprimir.php";
		}
		if($("#rabdetallado").is(':checked')==true)
		{
			url="imprimir_detallado.php";
		}
		if($("#rabtalones").is(':checked')==true)
		{
			url="imprimir_talones.php";
		}
		if($("#rabtipopago").is(':checked')==true)
		{
			url="imprimir_formapago.php";
		}
		if($("#raeconsolidado").is(':checked')==true)
		{
			url="imprimir_consolidado.php";
		}
		//if(document.getElementById("rabresumentot").checked==true)
		//{
		//	url="imprimir_consolidado_tot.php";
		//}

		url += "?fecha="+$("#fecha").val()+"&car="+car+"&caja=<?=$items["descripcion"]?>"+
				"&nrocaja=<?=$items["nrocaja"]?>&cajero=<?=$codusu?>&codsuc=<?=$codsuc?>&cars="+$("#car option:selected").text();
		AbrirPopupImpresion(url,800,600)
		
		return false
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td colspan="3"></td></tr>
				  <tr>
				    <td width="9%">Car</td>
				    <td width="3%" align="center">:</td>
				    <td colspan="3"><? $objDrop->drop_car($codsuc); ?></td>
			      </tr>
				  <tr>
				    <td>Fecha</td>
				    <td align="center">:</td>
				    <td><label>
				      <input type="text" name="fecha" id="fecha" class="inputtext" maxlength="20" value="<?=$objDrop->FechaServer()?>" style="width:80px;" />
				    </label></td>
				    <td width="3%" align="center">&nbsp;</td>
				    <td width="35%">&nbsp;</td>
			      </tr>
				  <tr>
				    <td>Caja</td>
				    <td align="center">:</td>
				    <td><input type="text" name="caja" id="caja" class="inputtext" size="20" maxlength="20" readonly="readonly" value="<?=$items["descripcion"]?>" /></td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
				    <td>Cajero</td>
				    <td align="center">:</td>
				    <td><input type="text" name="cajero" id="cajero" class="inputtext" size="50" maxlength="50" readonly="readonly" value="<?=$cajero?>" /></td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr style="padding:4px">
				    <td colspan="5" align="center">
                         				<div id="DivTipos" style="display:inline">
                    					<input type="radio" name="rabresumen" id="raeconsolidado" value="radio5" checked="checked"/>
				                        <label for="raeconsolidado">Consolidado</label>
			                           	<input type="radio" name="rabresumen" id="rabresumen" value="radio"  />
				                       	<label for="rabresumen">Resumen</label>
				                        <input type="radio" name="rabresumen" id="rabtipopago" value="radio4" />
				                        <label for="rabtipopago">Forma de Pago</label>    
				                        <input type="radio" name="rabresumen" id="rabtalones" value="radio3" />
				                        <label for="rabtalones">Talones</label>
				                        <!--<input type="radio" name="rabresumen" id="rabdetallado" value="radio2" />
				                        <label for="rabdetallado">Detallado</label>-->
				                        <!--<input type="radio" name="rabresumen" id="rabresumentot" value="radio5" />
				                        <label for="rabresumentot">Consolidado Todos</label>-->
				                            
									</div>	
                    			</td>
                    		</tr>
                    		<tr><td colspan="5"></td></tr>
                    		<tr><td colspan='5' align="center"> <input type="button" onclick="return ValidarForm();" value="Exportar" id=""></td></tr>
                        	<tr><td colspan="5"></td></tr>
				  
				</table>
			</fieldset>
		</td>
	</tr>

    </table>
 </form>
</div>
<?php   CuerpoInferior();?>