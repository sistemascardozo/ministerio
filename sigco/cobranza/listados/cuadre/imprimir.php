<?php
	include("../../../../objetos/clsReporte.php");
	
	class clsCuadre extends clsReporte
	{
		function cabecera()
		{
			global $cajeros, $fecha, $cars, $caja, $_GET;
			
            $this->SetY($this->GetY()-5);
			$this->SetFont('Arial','B',8);
			$tit1 = "";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'R');            
			$h=4;
			
			$this->Ln(1);
			$this->SetFont('Arial','B',14);
			$tit1 = "RESUMEN DE PLANILLA ENTRADA CAJA";
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			
            $this->Ln(5);
			
			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"CAR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(60, $h,$cars,0,0,'L');
			$this->Cell(10, $h,"CAJA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,strtoupper($caja),0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$_GET["fecha"],0,0,'L');
			$this->Cell(15, $h,"CAJERO",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(50, $h,$cajeros,0,1,'L');
				
			$this->Ln(1);
			$this->Cell(75, $h,"CONCEPTO",1,0,'C');
			$this->Cell(20, $h,"NRO. ITEMS",1,0,'C');
			$this->Cell(20, $h,"CODCTA.",1,0,'C');
			$this->Cell(25, $h,"CANCELADO",1,0,'C');
			$this->Cell(25, $h,"A CUENTA",1,0,'C');
			$this->Cell(25, $h,"TOTAL",1,1,'C');
						
		}
		function ContenidoCab($concepto,$cancelado,$acuenta)
		{
			$h=4;
			
			$this->SetFont('Arial','B',7);
			$this->SetTextColor(230,0,0);
			
			$this->Cell(75, $h,strtoupper($concepto),0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(25, $h,number_format($cancelado,2),0,0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),0,0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),0,1,'R');
		}
		function ContenidoDet($concepto, $cancelado, $acuenta, $ctacontable)
		{
			$h=4;
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(5, $h, "", 0, 0, 'L');
			$this->Cell(70, $h, $concepto, 0, 0, 'L');
			$this->Cell(20, $h, "", 0, 0, 'C');
			$this->Cell(20, $h, $ctacontable, 0, 0, 'C');
			$this->Cell(25, $h, number_format($cancelado, 2), 0, 0, 'R');
			$this->Cell(25, $h, number_format($acuenta, 2), 0, 0, 'R');
			$this->Cell(25, $h, number_format($cancelado + $acuenta, 2), 0, 1, 'R');
		}
		function ContenidoFoot($cancelado, $acuenta)
		{
			$h=4;
			
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(5, $h,"",0,0,'L');
			$this->Cell(70, $h,"",0,0,'L');
			$this->Cell(20, $h,"",0,0,'C');
			$this->Cell(20, $h,"TOTALES",0,0,'R');
			$this->Cell(25, $h,number_format($cancelado,2),'T',0,'R');
			$this->Cell(25, $h,number_format($acuenta,2),'T',0,'R');
			$this->Cell(25, $h,number_format($cancelado+$acuenta,2),'T',1,'R');
		}
    }
	

	$car		= $_GET["car"];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];
	
	$cajeros 	= $_SESSION['user'];
	$cars		= $_GET["cars"];
	$caja		= $_GET["caja"];

	$objReporte	= new clsCuadre();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$fecha		= $objReporte->CodFecha($_GET["fecha"]);
	$impcancelado	= 0;
	$impamortizado 	= 0;
	
	$sql = "SELECT codcategoria, categoria_descripcion, SUM(impcancelado) AS impcancelado, SUM(impamortizado) AS impamortizado ";
	$sql .= "FROM cobranza.f_cuadre_cab_cierre ";
	$sql .= "WHERE codsuc = ".$codsuc." AND fechareg = '".$fecha."' AND car = ".$car." ";
	$sql .= " AND nropec = 0 AND anulado = 0 AND creador = ".$cajero." ";
	$sql .= "GROUP BY codcategoria, categoria_descripcion ";
	$sql .= "ORDER BY codcategoria";
	
	$consulta = $conexion->query($sql);

	$items = $consulta->fetchAll();
	//var_dump($consulta->errorInfo());
	foreach($items as $row)
	{
		$codcategoria = $row["codcategoria"];
		
		$objReporte->ContenidoCab($row["categoria_descripcion"], $row["impcancelado"], $row["impamortizado"]);
		//echo $row["impcancelado"]."<br>";
		$sqlD = "SELECT concepto, impcancelado, impamortizado, ctadebe, ordenrecibo ";
		$sqlD .= "FROM cobranza.f_cuadre_det_cierre ";
		$sqlD .= "WHERE codsuc = ".$codsuc." AND fechareg = '".$fecha."' AND car = ".$car." ";
		$sqlD .= " AND nropec = 0 AND anulado = 0 AND creador = ".$cajero." AND codcategoria = ".$codcategoria." ";
		$sqlD .= "GROUP BY concepto, impcancelado, impamortizado, ctadebe, ordenrecibo ";
		$sqlD .= "ORDER BY ordenrecibo";
		
		//$impcancelado 	+= $row["impcancelado"];
		//$impamortizado 	+= $row["impamortizado"];
		
		$consultaD = $conexion->query($sqlD);

		$itemsD = $consultaD->fetchAll();

		foreach($itemsD as $rowD)
		{
			$t = $rowD["impcancelado"] + $rowD["impamortizado"];
			
			$impcancelado 	+= $rowD["impcancelado"];
			$impamortizado 	+= $rowD["impamortizado"];
		
			if($t != 0)
			{
				$objReporte->ContenidoDet(strtoupper($rowD["concepto"]), $rowD["impcancelado"], $rowD["impamortizado"], $rowD["ctadebe"]);
			}
		}
	}
	$objReporte->ContenidoFoot($impcancelado, $impamortizado);

	$objReporte->Output();	
?>