<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
  
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	$TituloVentana = "ANULACION DE PAGOS";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	$id_user = $_SESSION['id_user'];
	
	$FormatoGrilla = array ();
	
	$Criterio = 'Anular';
	
	if($id_user != 1)
	{
		$cond = " AND cab.creador = ".$id_user;
	}
	
	$Sql = "SELECT cab.nropago, clie.codantiguo, upper(car.descripcion), upper(cab.propietario),
              ".$objFunciones->FormatFecha('cab.fechareg').", SUBSTRING(CAST(cab.hora AS TEXT), 1, 5),
             ".$objFunciones->Convert("cab.imptotal", "NUMERIC (18, 2)").",
             upper(forms.descripcion), CASE WHEN cab.condpago = 0 THEN 'CONTADO' ELSE 'CREDITO' END,
             cab.nropec, cab.nroprepago, usu.login,
             cab.anulado, cab.nroinscripcion, cab.car,
             cab.nrocaja, cab.creador, cab.codformapago
             FROM cobranza.cabpagos AS cab
             INNER JOIN cobranza.car car ON(cab.car = car.car)
             INNER JOIN public.formapago as forms ON(cab.codformapago = forms.codformapago)
             INNER JOIN seguridad.usuarios as usu ON(cab.creador = usu.codusu)
			 LEFT JOIN catastro.clientes clie ON (cab.nroinscripcion = clie.nroinscripcion) ";

  $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'cab.nropago', '2'=>'car.descripcion', '3'=>'cab.propietario',
                      '4'=>'forms.descripcion', '5'=>'cab.nropec', '6'=>'cab.nroprepago', '7'=>'clie.codantiguo');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Pago', 'T2'=>'Nro Inscripción', 'T3'=>'Car', 'T4'=>'Propietario',
                        'T5'=>'Fecha de Pago', 'T6'=>'Hora', 'T7'=>'Importe', 'T8'=>'Forma de Pago',
                          'T9'=>'Cond. Pago', 'T10'=>'Pec', 'T11'=>'Pre-Pago');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'left', 'A4'=>'left', 'A5'=>'center', 'A6'=>'center', 'A7'=>'right', 'A10'=>'center', 'A11'=>'center');   
  $FormatoGrilla[5] = array('W1'=>'60', 'W2'=>'60', 'W3'=>'120', 'W4'=>'180', 'W5'=>'90', 'W6'=>'50', 'W7'=>'80', 'W8'=>'80', 'W9'=>'80', 'W10'=>'50', 'W11'=>'60', 'W12'=>'20');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  AND (cab.codemp = 1 AND cab.codsuc=".$codsuc.$cond." /*and cab.anulado=0*/) ORDER BY cab.nropago DESC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'2',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Anular(this);"',  //Eventos del Botón
              'BtnCI1'=>'13',  //Item a Comparar
              'BtnCV1'=>'0' ,   //Valor de comparación
              'BtnId2'=>'BtnModificar',   //Nombre del Boton
              'BtnI2'=>'imprimir.png',   //Imagen a mostrar
              'Btn2'=>'Imprimir Recibo de Pago',       //Titulo del Botón
              'BtnF2'=>'onclick="imprimirrecibo(this);"',  //Eventos del Botón
              'BtnCI2'=>'13',  //Item a Comparar
              'BtnCV2'=>'0',    //Valor de comparación 
              );
	$FormatoGrilla[10] = array(array('Name' =>'id', 'Col'=>1),
  								array('Name' =>'estado', 'Col'=>12),
  								array('Name' =>'nropec', 'Col'=>10),
								array('Name' =>'nroinscripcion', 'Col'=>14), 
								array('Name' =>'fechapago', 'Col'=>5));//DATOS ADICIONALES                
  $FormatoGrilla[11] = 12;//FILAS VISIBLES   
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  
  Cabecera('', $FormatoGrilla[7], 1000, 600);
  
  Pie();
?>
<script>
	var Id=''
	$("#BtnNuevoB").hide();
	function ValidarEnter(e, Op)
        {
        switch(e.keyCode)
            {   
                case $.ui.keyCode.UP: 
                    for (var i = 10; i >= 1; i--) 
                        {
                            if ($('tr[tabindex=\'' + i  + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' +i  + '\']')
                                if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                                {
                                  $('tr[tabindex=\'' + i  + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }
                               
                             }
                        };
                        $('#Valor').focus().select();
                break;
                case $.ui.keyCode.DOWN:

                       for (var i = 1; i <= 10; i++) 
                        {
                            if ($('tr[tabindex=\'' + i  + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' +i  + '\']')
                                if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                                {
                                  $('tr[tabindex=\'' + i  + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }
                               
                             }
                        };

                        $('#Valor').focus().select();
                break;
                default:  if (VeriEnter(e)) Buscar(Op);
                
                
            }
             
        
            //ValidarEnterG(evt, Op)
        }
	function imprimirrecibo(obj)
	{
    var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
    var codciclo =1
    var fechapago = $(obj).parent().parent().data('fechapago')
    var codpago = $(obj).parent().parent().data('id')
		AbrirPopupImpresion("../cancelacion/consulta/imprimir_recibo.php?nroinscripcion="+nroinscripcion+'&codsuc=<?=$codsuc?>&codciclo='+codciclo+'&fechapago='+fechapago+'&codpago='+codpago,800,600)
	}
	function Anular(obj)
	{
		$("#form1").remove();
		var pec = $(obj).parent().parent().data('nropec')
		var est = $(obj).parent().parent().data('estado')
		if(pec!=0)
		{
			alert('El Documento no se puede Anular por que Ya Tiene Asignado un Nro. Pec')
			return
		}
		if(est==1)
		{
			alert('El Documento se Encuentra Anulado...Ya no se puede volver a ANULAR')
			return
		}
		 var Id = $(obj).parent().parent().data('id')
     var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
	    //location.href='atender?Id='+Id+'&codsuc=<?=$codsuc?>';
	    $("#Modificar").dialog({title:'Anular Cobranza'});
	    $("#Modificar").dialog("open");
	    $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
	    //$("#button-maceptar").unbind( "click" );
	    //$("#button-maceptar").click(function(){GuardarOk();});
	    $("#button-maceptar").children('span.ui-button-text').text('Anular Cobranza')
	    $.ajax({
	       url:'mantenimiento.php?Op=1',
	       type:'POST',
	       async:true,
	       data:'Id=' + Id+ '&codsuc=<?=$codsuc?>&nroinscripcion='+nroinscripcion,
	       success:function(data){
	        $("#DivModificar").html(data);
	       }
	    })
		
	}
	
	function SeleccionaIdB(obj,nropec,estado)
	{
		Id 		= 'Id=' + obj.id;
		IdAnt	= Id2;
		Id2 	= obj.id;
		pec		= nropec
		est		= estado
		if (IdAnt!='' && estant==0)
		{
			Limpiar(IdAnt);
		}
		if(est==0)
		{	
			obj.style.backgroundColor="#999999";
		}
		estant=est
	}
	function color_overB(Fila,estado)
	{
		var color="rgb(153, 153, 153)";
		if(document.all) {color="#999999";}
		if (Fila.style.backgroundColor!=color && estado==0)
		{
   			Fila.style.backgroundColor="#cccccc";
		}
	}
	
	function color_outB(Fila,estado)
	{
		var color="rgb(153, 153, 153)";
		if(document.all) color="#999999";
		if (Fila.style.backgroundColor!=color && estado==0)
		{
   			Fila.style.backgroundColor="#ffffff";
		}
	}
</script>
<?php CuerpoInferior();?>