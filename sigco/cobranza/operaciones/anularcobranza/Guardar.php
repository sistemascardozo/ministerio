<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	include("../../../../objetos/clsseguridad.php");
	
	$conexion->beginTransaction();
	
	$objFunciones = new clsFunciones();
	$objSeguridad = new clsSeguridad();
	
	$contador 		= $_POST["contador"];
	$motivo			= strtoupper($_POST["obs"]);
	$nropago		= $_POST["nropago"];
	$codsuc 		= $_SESSION["IdSucursal"];
	$idusuario      = $_SESSION['id_user'];
	$nroprepago		= $_POST["nroprepago"];
	$nroinscripcion	= $_POST["nroinscripcion"];
	
	$upd = "UPDATE cobranza.cabpagos ";
	$upd .= "SET anulado = 1, motivoanulacion = ?, usuarioanu = ?, fechaanu = ? ";
	$upd .= "WHERE codemp = 1 and codsuc = ? ";
	$upd .= " AND nropago = ? AND nroinscripcion = ?";
	
	$result = $conexion->prepare($upd);
	$result->execute(array($motivo,$idusuario,date('Y-m-d H:i:s'), $codsuc, $nropago, $nroinscripcion));
	
	if($result->errorCode() != '00000')
	{
		$conexion->rollBack();
		
		$mensaje = "Error update cabpagos";
		
		die(2);
	}


	$sqlpagos = "SELECT codconcepto, importe, tipopago, nroinscripcion, nrofacturacion, nropresupuesto, item ";
	$sqlpagos .= "FROM cobranza.detpagos ";
	$sqlpagos .= "WHERE codemp = 1 AND codsuc = ? ";
	$sqlpagos .= " AND nropago = ? AND nroinscripcion = ?";
	
	$result = $conexion->prepare($sqlpagos);
	$result->execute(array($codsuc, $nropago, $nroinscripcion));
	
	if($result->errorCode() != '00000')
	{
		$conexion->rollBack();
		
		$mensaje = "Error select detpagos";
		
		die(2);
	}

	$itemsP = $result->fetchAll();
	
	foreach($itemsP as $rowP)
	{
		if($rowP["tipopago"] == 0)
		{
			$updfact = "UPDATE facturacion.detfacturacion ";
			$updfact .= "SET imppagado = imppagado - :imppagado, estadofacturacion = 1 ";
			$updfact .= "WHERE codemp = 1 AND codsuc = :codsuc AND nrofacturacion = :nrofacturacion ";
			$updfact .= " AND nroinscripcion = :nroinscripcion AND codconcepto = :codconcepto AND item = :item";
		}
		else
		{
			$updfact  = "UPDATE facturacion.detfacturacion ";
			$updfact .= "SET imppagado = imppagado - :imppagado, estadofacturacion = 1, importeacta = importeacta - :imppagado ";
			$updfact .= "WHERE codemp = 1 AND codsuc = :codsuc AND nrofacturacion = :nrofacturacion ";
			$updfact .= " AND nroinscripcion = :nroinscripcion AND codconcepto = :codconcepto AND item = :item";
		}
		
		$result = $conexion->prepare($updfact);
		$result->execute(array(":imppagado"=>$rowP["importe"],
								  ":codsuc"=>$codsuc,
								  ":nrofacturacion"=>$rowP["nrofacturacion"],
								  ":nroinscripcion"=>$rowP["nroinscripcion"],
								  ":codconcepto"=>$rowP["codconcepto"],
								  ":item"=>$rowP["item"],
								  ));
								  
		if($result->errorCode() != '00000')
		{
			$conexion->rollBack();
			
			$mensaje = "Error update detfacturacion";
			
			die(2);
		}
		
		$updCP = "UPDATE solicitudes.cabpresupuesto ";
		$updCP .= "SET estadopresupuesto = 2 ";
		$updCP .= "WHERE codsuc = ? AND nropresupuesto = ?";
		
		$result = $conexion->prepare($updCP);
		$result->execute(array($codsuc, $rowP["nropresupuesto"]));
		
		if($result->errorCode() != '00000')
		{
			$conexion->rollBack();
			
			$mensaje = "Error update cabpresupuesto";
			
			die(2);
		}
	}
	
	$updPP = "UPDATE cobranza.cabprepagos ";
	$updPP .= "SET estado = 1 ";
	$updPP .= "WHERE codemp = 1 AND codsuc = ? AND nroprepago = ?";
	
	$result = $conexion->prepare($updPP);
	$result->execute(array($codsuc, $nroprepago));
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		
		$mensaje = "Error update cabprepagos";
		
		die(2);
	}
		
		
	//ANULAR CREDITO
	$nrocredito = $_POST['nrocredito'];
	
	if($nrocredito != 0)
	{
		$fecha 		= $objFunciones->FechaServer();
		$hora		= $objFunciones->HoraServidor();
		$idusuario	= $_SESSION["id_user"];

		$upd = "UPDATE facturacion.cabcreditos ";
		$upd .= "SET estareg = 0, horaanulacion = ?, fechaanulacion = ?, creadoranulacion = ? ";
		$upd .= "WHERE codemp = 1 AND codsuc = ? AND nrocredito = ?";
		
		$resultC = $conexion->prepare($upd);
		$resultC->execute(array($hora, $fecha, $idusuario, $codsuc, $nrocredito));
		
		if ($resultC->errorCode() != '00000') 
		{
			$conexion->rollBack();
			
			$mensaje = "Error reclamos";
			
			die(2);
		}
	
	
		$objSeguridad->seguimiento(1, $codsuc, 5, $idusuario, "SE HA ANULADO EL CREDITO CON NUMERO ".$nrocredito, 145);
	}
	//ANULAR CREDITO




	if(!$result)
	{
		$conexion->rollBack();
		var_dump($resultC->errorInfo());
		die();
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		$res=1;
	}
	
?>
<script>
	 location.href = 'index.php';
</script>
