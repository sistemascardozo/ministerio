<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "EXPORTAR ASIENTOS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	
	$objFunciones 	= new clsFunciones();
	$codsuc 		= $_SESSION['IdSucursal'];
	
	$fecha			= $objFunciones->FechaServer();

?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var c = 0
	$(function() {
	var dates = $("#FechaDesde, #FechaHasta").datepicker({
			buttonText: 'Selecione Fecha de Busqueda',
			showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true,
			numberOfMonths: 2,
			onSelect: function( selectedDate ) {
				var option = this.id == "FechaDesde" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	});

	/*function Generar()
	{
		var Data=Validar()
		if(Data!=false)
		{
			
			$("#DivResultado").empty();
			$.ajax({
			 url:'Generar.php',
			 type:'POST',
			 async:true,
			 data:Data,
			 success:function(datos)
			 {
			 	if(datos==0)
			 		window.parent.OperMensaje('Asientos Generados Correctamente',1)
			 	else
			 		window.parent.OperMensaje('Error al Generar Asientos',0)
			 }
			})
		}
	}*/
	function Generar()
	{
		var Data=Validar()
		if(Data!=false)
		{
			$('#DivConsulta').fadeOut(500) 
			$('#ImgLoad').fadeIn();
			
			$("#DivResultado").empty();
			$.ajax({
			 url:'exportar.php',
			 type:'POST',
			 async:true,
			 data:Data,
			 success:function(datos)
			 {
			 	
			    $('#ImgLoad').fadeOut(500,function(){
             	$('#DivConsulta').fadeIn(500,function(){
						$("#DivResultado").empty().append(datos);  
			  		}) 
				})
			 	
			 }
			})
		}
	}		
	function Validar()
	{
		var Data=''
		var Desde = $("#FechaDesde").val()
		var Hasta = $("#FechaHasta").val()
		return Data +'&codsuc=<?=$codsuc?>'+'&Desde='+Desde+'&Hasta='+Hasta
	}
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function Consultar()
	{
		var Data=Validar()
		if(Data!=false)
		{
			$('#DivConsulta').fadeOut(500) 
			$('#ImgLoad').fadeIn();
			
			$("#DivResultado").empty();
			$.ajax({
			 url:'Consulta.php',
			 type:'POST',
			 async:true,
			 data:Data,
			 success:function(datos)
			 {
			 	$('#ImgLoad').fadeOut(500,function(){
			                  
					$('#DivConsulta').fadeIn(500,function(){ $("#DivResultado").empty().append(datos);  
			  		}) 
				})
			 }
			})
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
	<table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<fieldset style="padding:4px; width:95%;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="19%">Periodo Inicial :</td>
								<td>
									<input type="text" class="inputtext ui-corner-all" id="FechaDesde" size="9" maxlength="10" value="<?=$fecha?>" />
								</td>
							</tr>
							<tr>
								<td width="19%">Periodo Final :</td>
								<td>
									<input type="text" class="inputtext ui-corner-all" id="FechaHasta" size="9" maxlength="10" value="<?=$fecha?>" />
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="button" value="Consultar" id="BtnAceptar" onclick="Consultar();" />
									&nbsp;
									<input type="button" value="Generar" id="BtnAceptar" onclick="Generar();" />
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<div id="ImgLoad" style="text-align:center; display:none;">
						<span class="icono-icon-loading"></span>
						Cargando ...
					</div>
					<div id="DivConsulta" style="height:auto; overflow:auto; display:none;">
						<fieldset style="padding:4px; width:95%;">
							<legend class="ui-state-default ui-corner-all" align="left" >Resultados de la Consulta</legend>
							<div style="height:auto; overflow:auto;" align="center" id="DivResultado"></div>
						</fieldset>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="CampoDetalle">&nbsp;</td>
			</tr>
		</tbody>
	</table>
</form>
</div>
<script>
//$('#ImgLoad').fadeOut(500,function(){$('#DivConsulta').fadeIn(500)});
$("#BtnAceptar").attr('value','Consultar')
$("#BtnAceptar").css('background-image','url(<?php echo $_SESSION['urldir'];?>css/images/ver.png)')
</script>
<?php   CuerpoInferior();?>