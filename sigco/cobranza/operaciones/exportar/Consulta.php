<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	ini_set("display_errors", 1);
	
	include("../../../../objetos/clsDrop.php");
	
	$objMantenimiento = new clsDrop();
	
	$codsuc	= $_SESSION["IdSucursal"];
	
	$Desde = $objMantenimiento->CodFecha($_POST["Desde"]);
	$Hasta = $objMantenimiento->CodFecha($_POST["Hasta"]);
	
	$Condicion = " AND cab.fechareg BETWEEN CAST('".$Desde."' AS DATE) AND CAST('".$Hasta."' AS DATE) ";
	//$Condicion .= " AND cab.creador = 2";
?>
<div align="center">
	<table class="ui-widget" border="0" cellspacing="0" width="95%" rules="rows" align="center">
        <tr>
            <th align="center">
                <table border="0" cellspacing="0" width="100%" rules="rows" align="left">
                    <tbody>
                        <tr>
                            <td colspan="2" width="100%" valign="top">
                                <table class="ui-widget" border="1" cellspacing="0" width="100%" rules="rows" align="left">
                                    <thead class="ui-widget-header" style="font-size:10px">
                                        <tr>
                                            <th colspan="5">Detalle de Ingresos</th>
                                        </tr>
                                        <tr>
                                            <th width="70" scope="col">C&oacute;digo</th>
                                            <th scope="col">Concepto</th>
                                            <th width="90" scope="col">N&deg; Cuenta</th>
                                            <th width="100" scope="col">Debe</th>
                                            <th width="100" scope="col">Haber</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
	$TotalPec = 0;//02039634,
	$TotalD = 0;
	$TotalH = 0;
	
	$Sql = "SELECT con.ctadebe, SUM(CASE WHEN det.importe <= 0 AND con.signo = '-' THEN det.importe * (-1) ELSE det.importe END) AS Totales, ";
	$Sql .= " con.descripcion, con.ordenrecibo, con.signo, con.codantiguo, ";
	$Sql .= " con.ctahaber, con.ctadebeprov, con.ctahaberprov, con.ctahaberquiebres, con.ctadebequiebres, det.codtipodeuda, con.codconcepto ";
	$Sql .= "FROM cobranza.cabpagos AS cab ";
	$Sql .= " INNER JOIN cobranza.detpagos AS det ON(det.codemp = cab.codemp) AND (det.codsuc = cab.codsuc) AND (det.nroinscripcion = cab.nroinscripcion) AND (det.nropago = cab.nropago) ";
	$Sql .= " INNER JOIN facturacion.conceptos AS con ON(det.codemp = con.codemp) AND (det.codsuc = con.codsuc) AND (det.codconcepto = con.codconcepto) ";
	$Sql .= "WHERE cab.codemp = 1 AND cab.codsuc = ".$codsuc;
	$Sql .= $Condicion." AND cab.anulado = 0 ";
	$Sql .= "GROUP BY con.ctadebe, con.descripcion, con.ordenrecibo, con.signo, codantiguo, ";
	$Sql .= " con.ctahaber, con.ctadebeprov, con.ctahaberprov, con.ctadebequiebres, con.ctahaberquiebres, det.codtipodeuda, con.codconcepto ";
	//$Sql .= "ORDER BY con.codantiguo ASC ";
	$Sql .= "ORDER BY con.codconcepto ASC ";
	
	//die($Sql);

	$ConsultaT = $conexion->query($Sql);
        
	foreach($ConsultaT->fetchAll() as $rowT)
	{
		$TotalPec += $rowT[1];
		$TotalCuenta = $rowT[1];
		$D = $TotalCuenta;
		$H = $TotalCuenta;
		$NroCuentaConcepto = $rowT['ctadebe'];
																		
		if($rowT['codtipodeuda'] == 10)//SI TIPO DEUDA 10  PROVISION
		{
			$NroCuentaConcepto = $rowT['ctadebeprov'];
		}
		if($rowT['codtipodeuda'] == 11)//SI TIPO DEUDA 11  QUIBRE
		{
			$NroCuentaConcepto = $rowT['ctadebequiebres'];
		}
		
		if($rowT['signo'] == '-')
		{
			$flag = 1;
			$tipocta = 'D';
			$TotalD += $TotalCuenta;
			$H = 0;
			$NroCuentaConcepto = $rowT['ctahaber'];
			
			if($rowT['codtipodeuda'] == 10)//SI TIPO DEUDA 10  PROVISION
			{
				$NroCuentaConcepto = $rowT['ctahaberprov'];
			}
			if($rowT['codtipodeuda'] == 11)//SI TIPO DEUDA 11  INCOBRABLE
			{
				$NroCuentaConcepto = $rowT['ctahaberquiebres'];
			}
		}
		else
		{
			$D = 0;
			$TotalH += $TotalCuenta;
		}
?>
                                        <tr>
                                            <!-- <td align="center"><?=$rowT['codantiguo']?></td> -->

                                            <td align="center"><?=$rowT['codconcepto']?></td>
                                            <td><?=$rowT[2]?></td>
                                            <td align="center"><?=$NroCuentaConcepto?></td>
                                            <td align="right" style="padding-right:5px;"><?=number_format($D, 2)?></td>
                                            <td align="right" style="padding-right:5px;"><?=number_format($H, 2)?></td>
                                        </tr>
<?php
	}
        
//TOTAL A CAJA
	$Sql = "SELECT SUM(det.importe) AS Totales ";
	$Sql .= "FROM cobranza.cabpagos AS cab ";
	$Sql .= " INNER JOIN cobranza.detpagos AS det ON(det.codemp = cab.codemp) AND (det.codsuc = cab.codsuc) AND (det.nroinscripcion = cab.nroinscripcion) AND (det.nropago = cab.nropago) ";
	$Sql .= " INNER JOIN facturacion.conceptos AS con ON(det.codemp = con.codemp) AND (det.codsuc = con.codsuc) AND (det.codconcepto = con.codconcepto) ";
	$Sql .= "WHERE cab.codsuc = ".$codsuc;
	$Sql .= $Condicion." AND  cab.anulado = 0 ";
	
	$ConsultaT = $conexion->query($Sql);
	
	$cont = 0;
	foreach($ConsultaT->fetchAll() as $rowT)
	{
		$cont++;
		
		//$TotalPec+=$rowT[1];
		$NroCuentaConcepto = '101111101';//$rowT[0];
		$TotalCuenta = $rowT[0];
		$concepto = 'CAJA';
		$codconcepto = '';
		$TotalD += $TotalCuenta;
?>
                                        <tr>
                                            <td width="20">&nbsp;</td>
                                            <td width="20"><?=$concepto?></td>
                                            <td width="20" align="center"><?=$NroCuentaConcepto?></td>
                                            <td width="20" align="right" style="padding-right:5px;"><?=number_format($TotalCuenta, 2)?></td>
                                            <td width="20" align="right" style="padding-right:5px;"><?=number_format(0, 2)?></td>
                                        </tr>
<?php																	
	}
?>
                                    </tbody>
                                    <tfoot class="ui-widget-header" style="font-size:10px">
                                        <tr>
                                            <td align="right" colspan="3">Total:</td>
                                            <td align="right" style="padding-right:5px;"><?=number_format($TotalD, 2)?></td>
                                            <td align="right" style="padding-right:5px;"><?=number_format($TotalH, 2)?></td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="4">DIF:</td>
                                            <td align="right" style="padding-right:5px;"><?=number_format($TotalD - $TotalH, 2)?></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </th>
        </tr>
	</table>
</div>
<?php
	//}
?>
	