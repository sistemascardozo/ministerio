<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");
	include("../../../../../esigad/conexion.php");
	
	$objMantenimiento = new clsDrop();
	
	$codsuc	= $_SESSION["IdSucursal"];
	$fecha = $_POST["fecha"];
	$idusuario		= $_SESSION['dni_user'];
	$fechareg = date('d-m-Y');
	$idtipocomprobante = 1;//CAJA BANCO INGRESOS
	$idorigen=1;//TESORERIA
	$idsistema=19;//TESOR
	$idcentrocostos='';
	$idperiodo =substr($fecha,3,2 );
	$anio = substr($fecha,6,4 );
	$IdCategoria=3;
	$ASTCAJA = $objMantenimiento->getParamae("ASTCJA", $codsuc);//'101111101';
	$ASTCAJA = $ASTCAJA["valor"];
	
	pg_query($conectar, "BEGIN");
	//DELETE
	$Sql="DELETE FROM public.tesor_ingresosasientos WHERE idsucursal= ".$codsuc." AND fecha='".$fecha."';";
	$result = pg_query($conectar, $Sql);
	$Sql="DELETE FROM public.tesor_ingresosdetalle WHERE idsucursal= ".$codsuc." AND fecha='".$fecha."';";
	$result = pg_query($conectar, $Sql);
	$Sql="DELETE FROM public.tesor_ingresos WHERE idsucursal= ".$codsuc." AND fecha='".$fecha."';";
	$result = pg_query($conectar, $Sql);

	$Sql="DELETE FROM public.conta_iteingresos WHERE idsucursal= ".$codsuc." AND fecha='".$fecha."' 
	AND idtipocomprobante='".$idtipocomprobante."';";
	$result = pg_query($conectar, $Sql);
	$Sql="DELETE FROM public.conta_cabingresos WHERE idsucursal= ".$codsuc." AND fecha='".$fecha."' 
	AND idtipocomprobante='".$idtipocomprobante."';";
	$result = pg_query($conectar, $Sql);
	//DELETE

	$Sql ="SELECT cab.car,car.descripcion
			FROM cobranza.cabpagos AS cab
			INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc 
			AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago
			INNER JOIN cobranza.car ON cab.codemp = cobranza.car.codemp AND cab.codsuc = cobranza.car.codsuc 
			AND cab.car = cobranza.car.car
			WHERE cab.codemp = '1' AND cab.codsuc = ".$codsuc." AND cab.fechareg = '".$fecha."' AND cab.nropec <> 0
			GROUP BY cab.car,car.descripcion
			ORDER BY cab.car,car.descripcion";
	$ConsultaC = $conexion->query($Sql);

	foreach($ConsultaC->fetchAll() as $rowC)
	{
		$IdCar = $rowC[0];

					$Sql ="SELECT cab.nropec,SUM(det.importe) AS Totales
						FROM cobranza.cabpagos AS cab
						INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc 
						AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago
						WHERE cab.codemp = '01' AND cab.codsuc = ".$codsuc." AND cab.fechareg = '".$fecha."' AND
						cab.nropec <> 0 AND cab.car='".$rowC[0]."'
						GROUP BY cab.fechareg,cab.nropec
						ORDER BY  cab.nropec ASC ";
					$ConsultaP = $conexion->query($Sql);
					foreach($ConsultaP->fetchAll() as $rowP)
					{
						$NroPec=$rowP[0];
						$TotalPec=$rowP[1];
						$nrocomprobante = "RI".substr("00000000",0,8-strlen($NroPec)).$NroPec;
						$glosa ='LIQUIDACION DE COBRANZA PEC '.$NroPec;
						
						///////////////CABECERAS//////////7
						$Sql ="INSERT INTO public.tesor_ingresos
								(idempresa,idsucursal,nrocomprobante,recibi,fecha,glosa,estado,idperiodo,idusuario,fechareg,anio) 
								VALUES (1,".$codsuc.",'".$nrocomprobante."','QUILLABAMBA','".$fecha."','".$glosa."',
										1,'".$idperiodo."','".$idusuario."','".$fechareg."','".$anio."') RETURNING idingreso ";
						$result = pg_query($conectar, $Sql);
						$rowI = pg_fetch_array($result);
						$idingreso = $rowI[0];
						//DETALLE
							$idconcepto=3200;//ASIENTO MANUAL TABLA TESOR_CONCCEPTOS
							$idtipodocumento = '01';//PLANILLA ENTRADA A CAJA  TESOR_TIPODOCUMENTO
							$Sql="INSERT INTO public.tesor_ingresosdetalle 
							(idempresa,idsucursal,idingreso,nrocomprobante,idconcepto,descripcion,
							idtipodocumento,nrodocumento, importe,  fecha)
							VALUES (1,".$codsuc.",'".$idingreso."','".$nrocomprobante."','".$idconcepto."',
									'','".$idtipodocumento."','".$NroPec."','".$TotalPec."','".$fecha."')  RETURNING idingresodetalle ;";
							$result = pg_query($conectar, $Sql);
							$rowI = pg_fetch_array($result);
							$idingresodetalle = $rowI[0];
							//DETALLE
						$idtipodocumentos=23;//PLLA.ENTRADA A CAJA
						$Sql ="INSERT INTO public.conta_cabingresos (idempresa,idsucursal,idtipocomprobante,nrocomprobante,idorigen,
								idtipodocumentos,nrodocumento,idperiodo,fecha,glosa,anulado,idusuario,fechareg,idsistema,anio) 
								VALUES (1,".$codsuc.",'".$idtipocomprobante."','".$NroPec."','".$idorigen."',
									'".$idtipodocumentos."','".$NroPec."','".$idperiodo."','".$fecha."','".$glosa."',0,
									'".$idusuario."','".$fechareg."','".$idsistema."','".$anio."');";
						$result = pg_query($conectar, $Sql);


                    	$TotalPec=0;
                		$Sql ="SELECT con.ctahaber,SUM(det.importe) AS Totales 
								FROM cobranza.cabpagos AS cab 
								INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago 
								INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto 
								WHERE cab.codemp = '01' AND cab.codsuc = ".$codsuc." AND cab.fechareg = '".$fecha."' AND
										cab.car='".$rowC[0]."' AND cab.nropec ='".$rowP[0]."'
								GROUP BY con.ctahaber 
								ORDER BY con.ctahaber  ASC ";
							$ConsultaT = $conexion->query($Sql);
							foreach($ConsultaT->fetchAll() as $rowT)
							{
								$TotalPec+=$rowT[1];
								$NroCuentaConcepto=$rowT[0];
								$TotalCuenta=$rowT[1];
								$Sql="INSERT INTO public.tesor_ingresosasientos 
										(idingresodetalle,idempresa,idsucursal,idingreso,nrocomprobante,nroctacontable,
											descripcion,idcentrocostos,tipo,importe,fecha) 
										VALUES ('".$idingresodetalle."',1,".$codsuc.",'".$idingreso."','".$nrocomprobante."',
											  '".$NroCuentaConcepto."','','".$idcentrocostos."','H','".$TotalCuenta."','".$fecha."');";
								$result = pg_query($conectar, $Sql);

								$Sql="INSERT INTO public.conta_iteingresos
									(idempresa,idsucursal,idtipocomprobante,idperiodo,nrocomprobante,nroctacontable,idcategoria,
										tipocta,nroctacostos,idproveedor,monto,anio,fecha,anulado,fix_bg,change_sign,idtipodocumentos,nrodocumento) 
									VALUES (1,".$codsuc.",'".$idtipocomprobante."','".$idperiodo."','".$NroPec."','".$NroCuentaConcepto."',
								  		'".$IdCategoria."',1,'".$idcentrocostos."','','".$TotalCuenta."','".$anio."','".$fecha."',
								  		0,0,0,'".$idtipodocumentos."','".$NroPec."');";
								$result = pg_query($conectar, $Sql);
								
							
							}

							$Sql ="SELECT con.ctadebe,SUM(det.importe) AS Totales 
								FROM cobranza.cabpagos AS cab 
								INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago 
								INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto 
								WHERE cab.codemp = '01' AND cab.codsuc = ".$codsuc." AND cab.fechareg = '".$fecha."' AND
										cab.car='".$rowC[0]."' AND cab.nropec ='".$rowP[0]."'
								GROUP BY con.ctadebe 
								ORDER BY con.ctadebe  ASC ";
							$ConsultaT = $conexion->query($Sql);
							foreach($ConsultaT->fetchAll() as $rowT)
							{
								//$TotalPec+=$rowT[1];
								$NroCuentaConcepto=$rowT[0];
								$TotalCuenta=$rowT[1];
								$Sql="INSERT INTO public.tesor_ingresosasientos 
										(idingresodetalle,idempresa,idsucursal,idingreso,nrocomprobante,nroctacontable,
											descripcion,idcentrocostos,tipo,importe,fecha) 
										VALUES ('".$idingresodetalle."',1,".$codsuc.",'".$idingreso."','".$nrocomprobante."',
											  '".$NroCuentaConcepto."','','".$idcentrocostos."','D','".$TotalCuenta."','".$fecha."');";
								$result = pg_query($conectar, $Sql);

								$Sql="INSERT INTO public.conta_iteingresos
										(idempresa,idsucursal,idtipocomprobante,idperiodo,nrocomprobante,nroctacontable,idcategoria,
											tipocta,nroctacostos,idproveedor,monto,anio,fecha,anulado,fix_bg,change_sign,idtipodocumentos,nrodocumento) 
										VALUES (1,".$codsuc.",'".$idtipocomprobante."','".$idperiodo."','".$NroPec."','".$NroCuentaConcepto."',
									  		'".$IdCategoria."',0,'".$idcentrocostos."','','".$TotalCuenta."','".$anio."','".$fecha."',
									  		0,0,0,'".$idtipodocumentos."','".$NroPec."');";
								$result = pg_query($conectar, $Sql);

								//////HABER
								$Sql="INSERT INTO public.tesor_ingresosasientos 
										(idingresodetalle,idempresa,idsucursal,idingreso,nrocomprobante,nroctacontable,
											descripcion,idcentrocostos,tipo,importe,fecha) 
										VALUES ('".$idingresodetalle."',1,".$codsuc.",'".$idingreso."','".$nrocomprobante."',
											  '".$NroCuentaConcepto."','','".$idcentrocostos."','H','".$TotalCuenta."','".$fecha."');";
								$result = pg_query($conectar, $Sql);

								$Sql="INSERT INTO public.conta_iteingresos
										(idempresa,idsucursal,idtipocomprobante,idperiodo,nrocomprobante,nroctacontable,idcategoria,
											tipocta,nroctacostos,idproveedor,monto,anio,fecha,anulado,fix_bg,change_sign,idtipodocumentos,nrodocumento) 
										VALUES (1,".$codsuc.",'".$idtipocomprobante."','".$idperiodo."','".$NroPec."','".$NroCuentaConcepto."',
									  		'".$IdCategoria."',1,'".$idcentrocostos."','','".$TotalCuenta."','".$anio."','".$fecha."',
									  		0,0,0,'".$idtipodocumentos."','".$NroPec."');";
								$result = pg_query($conectar, $Sql);
								//ESPECIFICA
								$Actividad='014787';
								$TipoC='Ting';
								$Especifica='1.3.3.9.2.21';
								
								$Sql="INSERT INTO 
									    public.tesor_clasgei
									  (
									    idcomprobante,
									    id_actividad,
									    id_clasificador,
									    monto,
									    tipo_comprobante,
									    nroctacontable
									  ) 
									  VALUES (
									    '".$idingreso."',
									    '".$Actividad."',
									    '".$Especifica."',
									    '".$TotalCuenta."',
									    '".$TipoC."',
									    '".$NroCuentaConcepto."'
									  );";
								$result = pg_query($conectar, $Sql);
							
							}

							//////////7
							/*$Sql="INSERT INTO public.tesor_ingresosasientos 
										(idingresodetalle,idempresa,idsucursal,idingreso,nrocomprobante,nroctacontable,
											descripcion,idcentrocostos,tipo,importe,fecha) 
										VALUES ('".$idingresodetalle."',1,".$codsuc.",'".$idingreso."','".$nrocomprobante."',
											  '".$ASTCAJA."','','".$idcentrocostos."','D','".$TotalPec."','".$fecha."');";
							$result = pg_query($conectar, $Sql);

							$Sql="INSERT INTO public.conta_iteingresos
									(idempresa,idsucursal,idtipocomprobante,idperiodo,nrocomprobante,nroctacontable,idcategoria,
										tipocta,nroctacostos,idproveedor,monto,anio,fecha,anulado,fix_bg,change_sign,idtipodocumentos,nrodocumento) 
									VALUES (1,".$codsuc.",'".$idtipocomprobante."','".$idperiodo."','".$NroPec."','".$ASTCAJA."',
								  		'".$IdCategoria."',0,'".$idcentrocostos."','','".$TotalPec."','".$anio."','".$fecha."',
								  		0,0,0,'".$idtipodocumentos."','".$NroPec."');";
							$result = pg_query($conectar, $Sql);*/
							/////////////


                        	$TotalPecDeposito=0;
                    		$Sql ="SELECT cta.nroctacontable,SUM(dep.impdeposito) AS Totales
									FROM public.depositosbanco  as dep
									inner join public.ctasxbanco as cta ON dep.codemp = cta.codemp AND dep.codsuc = cta.codsuc AND dep.codctabanco = cta.codctabanco  
									inner join public.bancos as ban ON ban.codbanco = cta.codbanco  
									WHERE dep.codemp = '01' AND dep.codsuc = ".$codsuc." AND dep.fechaini = '".$fecha."' AND
										dep.car='".$rowC[0]."' AND dep.nropec ='".$rowP[0]."'
									GROUP BY   cta.nroctacontable ";
							$ConsultaT = $conexion->query($Sql);
							foreach($ConsultaT->fetchAll() as $rowT)
								{
									$TotalPecDeposito+=$rowT[1];
									$NroCuentaBanco=$rowT[0];
									$TotalDeposito=$rowT[1];
									$Sql="INSERT INTO public.tesor_ingresosasientos 
										(idingresodetalle,idempresa,idsucursal,idingreso,nrocomprobante,nroctacontable,
											descripcion,idcentrocostos,tipo,importe,fecha) 
										VALUES ('".$idingresodetalle."',1,".$codsuc.",'".$idingreso."','".$nrocomprobante."',
											  '".$NroCuentaBanco."','','".$idcentrocostos."','D','".$TotalDeposito."','".$fecha."');";
									$result = pg_query($conectar, $Sql);

									$Sql="INSERT INTO public.conta_iteingresos
										(idempresa,idsucursal,idtipocomprobante,idperiodo,nrocomprobante,nroctacontable,idcategoria,
											tipocta,nroctacostos,idproveedor,monto,anio,fecha,anulado,fix_bg,change_sign,idtipodocumentos,nrodocumento) 
										VALUES (1,".$codsuc.",'".$idtipocomprobante."','".$idperiodo."','".$NroPec."','".$NroCuentaBanco."',
									  		'".$IdCategoria."',0,'".$idcentrocostos."','','".$TotalDeposito."','".$anio."','".$fecha."',
									  		0,0,0,'".$idtipodocumentos."','".$NroPec."');";
									$result = pg_query($conectar, $Sql);
							
								}
								//////////7
								/*$Sql="INSERT INTO public.tesor_ingresosasientos 
											(idingresodetalle,idempresa,idsucursal,idingreso,nrocomprobante,nroctacontable,
												descripcion,idcentrocostos,tipo,importe,fecha) 
											VALUES ('".$idingresodetalle."',1,".$codsuc.",'".$idingreso."','".$nrocomprobante."',
											  '".$ASTCAJA."','','".$idcentrocostos."','H','".$TotalPecDeposito."','".$fecha."');";
								$result = pg_query($conectar, $Sql);

								$Sql="INSERT INTO public.conta_iteingresos
										(idempresa,idsucursal,idtipocomprobante,idperiodo,nrocomprobante,nroctacontable,idcategoria,
											tipocta,nroctacostos,idproveedor,monto,anio,fecha,anulado,fix_bg,change_sign,idtipodocumentos,nrodocumento) 
										VALUES (1,".$codsuc.",'".$idtipocomprobante."','".$idperiodo."','".$NroPec."','".$ASTCAJA."',
									  		'".$IdCategoria."',1,'".$idcentrocostos."','','".$TotalPecDeposito."','".$anio."','".$fecha."',
									  		0,0,0,'".$idtipodocumentos."','".$NroPec."');";
								$result = pg_query($conectar, $Sql);*/
								/////////////
							                                	
						
					}
		
	}
	if (!$result)
	{
		pg_query($conectar, "ROLLBACK");
		$res=0;$mensaje = "Ocurrio un Error";
		
	}
	else
	{
		pg_query($conectar, "COMMIT");
	}
	
	?>
	
