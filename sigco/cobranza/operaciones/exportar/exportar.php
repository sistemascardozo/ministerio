<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");
	
	set_time_limit(0);
	ini_set("display_errors", 1);
	
	$objMantenimiento = new clsDrop();
	
	$Desde = $objMantenimiento->CodFecha($_POST["Desde"]);
	$Hasta = $objMantenimiento->CodFecha($_POST["Hasta"]);
	
	$Condicion 	= " AND cab.fechareg BETWEEN CAST('".$Desde."' AS DATE) AND CAST('".$Hasta."' AS DATE) ";
	$codsuc		= $_POST["codsuc"];
		
	$conexion2 = odbc_connect("ODBC_AVALON_CAJA", "", "") or die ("No se pudo conectar con DBF");
	
	ereg("([0-9]{2,4})/([0-9]{1,4})/([0-9]{1,4})", $_POST["Desde"], $mifecha);
    $Fecha = $mifecha[2].substr($mifecha[3],2,2);

    if($codsuc == 1)
	{
    	$RutaDisco='D:\E-SIINCOWEB\AVALON\CAJA\ ';
	}
	
    $tec = trim($RutaDisco)."TEC".$Fecha.".dbf";
	
	if(!file_exists($tec))
	{	
		$archivo = "TEC.dbf";
		
	    if (!copy($archivo, trim($RutaDisco)."TEC".$Fecha.".dbf")) 
	    {
	   		echo "Error al copiar $archivo...\n";
		} 
	}
	
	$tel = trim($RutaDisco)."TEL".$Fecha.".dbf";
	
	if(!file_exists($tel))
	{	
		$archivo = "TEL.dbf";
		
	    if (!copy($archivo, trim($RutaDisco)."TEL".$Fecha.".dbf")) 
	    {
	   		echo "Error al copiar $archivo...\n";
		}
	}
	
	$lcl = trim($RutaDisco)."LCL".$Fecha.".dbf";
	
	if(!file_exists($lcl))
	{	
		$archivo = "LCL.dbf";
	    if (!copy($archivo, trim($RutaDisco)."LCL".$Fecha.".dbf")) 
	    {
	   		echo "Error al copiar $archivo...\n";
		}
     
	}
	 
	
    //ORDEN LCL, 1 REG, TEC,1REG, TEL, DETALLE 2103280
	//---Creamos los Archivos tec, lcl, tel------

	$count = 0;
	//FOR DE TODAS LAS FECHA DE COBRO EN EL RAGO
	$Sql = "SELECT cab.fechareg ";
	$Sql .= "FROM cobranza.cabpagos AS cab ";
	$Sql .= " INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago ";
	$Sql .= "WHERE cab.codemp = 1 AND cab.codsuc = ".$codsuc;
	$Sql .= $Condicion." AND cab.anulado = 0 ";
	$Sql .= "GROUP BY cab.fechareg ORDER BY cab.fechareg";
		
	$ConsultaF = $conexion->query($Sql);
	
	foreach($ConsultaF->fetchAll() as $rowF)	
	{
		$fecha = $rowF[0];
		//ELIMAR DATA ANTERIOR DEL DBF
		$Dia 	= substr($objMantenimiento->DecFecha($rowF['fechareg']), 0, 2);
	    $Mes 	= substr($objMantenimiento->DecFecha($rowF['fechareg']), 3, 2);
	    $Anio 	= substr($objMantenimiento->DecFecha($rowF['fechareg']), 6, 4);
	    $Anio 	= substr($Anio, 2, 2);

	    $Sql = "DELETE FROM LCL".$Fecha." WHERE LDOC1 IN('LC".$Anio.$Mes.$Dia."01')";
		
		odbc_exec($conexion2, $Sql) or die("<p>".odbc_errormsg());
	    /*$Sql="SELECT * FROM  LCL".$Fecha." WHERE LDOC1='LC".$Anio.$Mes.$Dia."01'";
		$restipo = odbc_exec($conexion2,$Sql);
		while(odbc_fetch_row($restipo))
		{
			$LORIG = odbc_result($restipo, "LDOC1");
			echo($LORIG);
		}
			
		die('NO');*/

		$Sql = "DELETE FROM TEC".$Fecha." WHERE DOC1 = 'LC".$Anio.$Mes.$Dia."01'";
		
		odbc_exec($conexion2, $Sql) or die("<p>".odbc_errormsg());
		
		$Sql = "DELETE FROM TEL".$Fecha." WHERE LDOC1 = 'LC".$Anio.$Mes.$Dia."01'";
		
		odbc_exec($conexion2, $Sql) or die("<p>".odbc_errormsg());
		
		///////TOTAL CON SIGNO (-)
		$Sql = "SELECT SUM(CASE WHEN det.importe <= 0 THEN det.importe * (-1) ELSE det.importe END) AS impcancelado ";
		$Sql .= "FROM cobranza.cabpagos AS cab ";
		$Sql .= " INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago ";
		$Sql .= " INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto ";
		$Sql .= "WHERE cab.codemp = 1 AND cab.codsuc = ".$codsuc." ";
		$Sql .= " AND cab.fechareg = '".$fecha."' AND cab.anulado = 0 AND con.signo = '-'";
		
		$row = $conexion->query($Sql)->fetch();
		
		$TotalD = $row[0];
		//---Generamos la sentencia para recuperar los archivos------
		//---lcl
		
		$Sql = "SELECT c.nropec, c.fechareg, CASE WHEN SUM(d.importe) IS NULL THEN 0 ELSE SUM(d.importe) END AS importe ";
		$Sql .= "FROM cobranza.cabpagos AS c ";
		$Sql .= " INNER JOIN cobranza.detpagos AS d ON(c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nropago = d.nropago AND c.nroinscripcion = d.nroinscripcion) ";
		$Sql .= "WHERE c.codsuc = ".$codsuc." ";
		$Sql .= " AND c.fechareg = '".$fecha."' AND c.nropec = 0 AND c.anulado = 0 ";
		$Sql .= "GROUP BY c.nropec, c.fechareg";
				
		$ConsultaT = $conexion->query($Sql);
		
		foreach($ConsultaT->fetchAll() as $row)
		{			
			$Dia	= substr($objMantenimiento->DecFecha($row['fechareg']), 0, 2);
			$Mes	= substr($objMantenimiento->DecFecha($row['fechareg']), 3, 2);
			$Anio	= substr($objMantenimiento->DecFecha($row['fechareg']), 6, 4);
			$Anio	= substr($Anio, 2, 2);

			$Sql = "INSERT INTO LCL".$Fecha." ";
			$Sql .= "(LORIG, LDOC1, LLINE, LFECH, LCCAR, ";
			$Sql .= " LTIPO, GLOSA, LIMP_R, ";
			$Sql .= " LUBIC, LTRAB, LCOMP, LCCTA, LIMP_F, LIMP_C, LFLADH, COD_CPTO) ";
			$Sql .= "VALUES(1, 'LC".$Anio.$Mes.$Dia."01', '001', {".$Mes."/".$Dia."/".$Anio."}, '11', ";
			$Sql .= " '03', 'EFECTIVO', ".($row["importe"] + $TotalD).", ";
			$Sql .= " ' ', 0, 0, ' ', 0, 0, ' ', ' ')";
			
			odbc_exec($conexion2, $Sql) or die("<p>".odbc_errormsg());
		}
		
		$Sql = "SELECT c.nropec, c.fechareg, CASE WHEN SUM(d.importe) IS NULL THEN 0 ELSE SUM(d.importe) END AS importe ";
		$Sql .= "FROM cobranza.cabpagos AS c ";
		$Sql .= " INNER JOIN cobranza.detpagos AS d ON(c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nropago = d.nropago AND c.nroinscripcion = d.nroinscripcion) ";
		$Sql .= "WHERE c.fechareg = '".$fecha."' AND c.nropec = 0 AND c.anulado = 0 ";
		$Sql .= "GROUP BY c.nropec, c.fechareg";

		$ConsultaT = $conexion->query($Sql);
		
		foreach($ConsultaT->fetchAll() as $row)
		{			
			$Dia	= substr($objMantenimiento->DecFecha($row['fechareg']), 0, 2);
			$Mes	= substr($objMantenimiento->DecFecha($row['fechareg']), 3, 2);
			$Anio	= substr($objMantenimiento->DecFecha($row['fechareg']), 6, 4);
			$Anio	= substr($Anio, 2, 2);
			
			$Sql = "INSERT INTO TEC".$Fecha." ";
			$Sql .= "(DOC1, FECT, ORIG, GLOS, ";
			$Sql .= " CTOT, ENOM, TIPOPE, ST, MON_TIP, MON_SOL, ";
			$Sql .= " N_REN, ASIE, NROA, ECHQ, BANC, CARCOD, MESANO, PLANI, RECIB, COMIS, FACTU, TRECI, TFACT) ";
			$Sql .= "VALUES('LC".$Anio.$Mes.$Dia."01', '".$Mes."".$Dia."".$Anio."', 1, 'LIQUIDACION DE COBRANZA', ";
			$Sql .= " ".($row["importe"]+$TotalD).", 'CLIENTES', 1, 1, '01', 1, ";
			$Sql .= " ' ', 0, ' ', ' ', ' ', ' ', ' ', ' ', 0, 0, 0, 0, 0)";
			
			odbc_exec($conexion2, $Sql) or die("<p>".odbc_errormsg());
		}
		
		//////DETALLE 
		$Sql = "SELECT con.ctadebe, SUM(CASE WHEN det.importe <= 0 AND con.signo = '-' THEN det.importe * (-1) ELSE det.importe END) AS Totales, ";
		$Sql .= " con.descripcion, con.codantiguo, cab.fechareg, con.ordenrecibo, con.signo, ";
		$Sql .= " con.ctahaber, con.ctadebeprov, con.ctahaberprov, det.codtipodeuda ";
		$Sql .= "FROM cobranza.cabpagos AS cab ";
		$Sql .= " INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago ";
		$Sql .= " INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto ";
		$Sql .= "WHERE cab.codsuc = ".$codsuc." ";
		$Sql .= " AND cab.fechareg = '".$fecha."' AND cab.anulado = 0 ";
		$Sql .= "GROUP BY con.ctadebe, con.descripcion, con.codantiguo, cab.fechareg, con.ordenrecibo, con.signo, ";
		$Sql .= " con.ctahaber, con.ctadebeprov, con.ctahaberprov, det.codtipodeuda ";
		$Sql .= "ORDER BY con.codantiguo ASC ";
		
		$ConsultaT = $conexion->query($Sql);
		
		$cont = 0;
		$TotalDebe = 0;
		
		$TotalPec = 0;
		
		foreach($ConsultaT->fetchAll() as $rowT)
		{ //grandez tuana celi flor 33
			$cont++;
			$TotalPec += $rowT[1];
			
			$NroCuentaConcepto = $rowT['ctadebe'];
			$TotalCuenta = $rowT[1];
			$concepto = $rowT[2];
			$codconcepto = trim($rowT[3]);
			$Dia = substr($objMantenimiento->DecFecha($rowT['fechareg']), 0, 2);
			$Mes = substr($objMantenimiento->DecFecha($rowT['fechareg']), 3, 2);
			$Anio2 = substr($objMantenimiento->DecFecha($rowT['fechareg']), 6, 4);
			$Anio = substr($Anio2, 2, 2);
			$flag = 2;
			$tipocta = 'H';
			
			if($rowT['codtipodeuda'] == 10)//SI TIPO DEUDA 10  PROVISION
			{
				$NroCuentaConcepto = $rowT['ctadebeprov'];
			}
	            /*if($rowT['codtipodeuda']==11)//SI TIPO DEUDA 11  QUIBRE
	            {
	            	$NroCuentaConcepto=$rowT['ctadebequiebres'];
	            }*/
			if($rowT['signo'] == '-')
			{
				$flag = 1;
				$tipocta = 'D';
				$TotalDebe += $TotalCuenta;
				$NroCuentaConcepto = $rowT['ctahaber'];
				
				if($rowT['codtipodeuda'] == 10)//SI TIPO DEUDA 10  PROVISION
				{
					$NroCuentaConcepto = $rowT['ctahaberprov'];
				}
		            /*if($rowT['codtipodeuda']==11)//SI TIPO DEUDA 11  INCOBRABLE
		            {
		            	$NroCuentaConcepto=$rowT['ctahaberquiebres'];
		            }*/
			}
			
			//$Sql = "INSERT INTO public.tesor_ingresosasientos ";
//			$Sql .= "(idingresodetalle, idempresa, idsucursal, idingreso, nrocomprobante, nroctacontable, ";
//			$Sql .= " descripcion, idcentrocostos, tipo, importe, fecha) ";
//			$Sql .= "VALUES('".$idingresodetalle."', 1, ".$codsuc.", '".$idingreso."', '".$nrocomprobante."', '".$NroCuentaConcepto."', ";
//			$Sql .= " '', '".$idcentrocostos."', 'H', '".$TotalCuenta."', '".$fecha."');";
				// $conexion->query($Sql);
				
			$Sql = "INSERT INTO TEL".$Fecha." ";
			$Sql .= "(LORIG, FECT, LDOC1, LLINE, ";
			$Sql .= " N_REN, ILINE, LFECH, LCCAR, ";
			$Sql .= " LCCTA, LTOPE, LCHEQ, GLOSA, LCAJA, ";
			$Sql .= " LIMPOS, FLAGDH, LIDEN, COD_CPTO, ";
			$Sql .= " PRO_RUC, LASIE, LCCOS, NCCOS, LUBIC, LCTAC, LANAL, LCOMP, LTRAB, LPPTO, LIMPOD, FLADDH, RCAJA) ";
			$Sql .= "VALUES(1, {".$Mes."/".$Dia."/".$Anio."}, 'LC".$Anio.$Mes.$Dia."01', '".str_pad($cont, 4, "0", STR_PAD_LEFT)."', ";
			$Sql .= " '".$codconcepto."', '0001', {".$Mes."/".$Dia."/".$Anio."}, '1 1', ";
			$Sql .= " '".$NroCuentaConcepto."', '03', 'EFECTIVO', '".$concepto."', '".$flag."', ";
			$Sql .= " ".$TotalCuenta.", '".$tipocta."', 'P', '".$codconcepto."', ";
			$Sql .= " ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0, ' ', ' ')";
			
			odbc_exec($conexion2, $Sql) or die("<p>".odbc_errormsg());


				/* dbase_add_record($tel_dbf, array('52',
				 	  $Mes."/".$Dia."/".$Anio,
				 	'LC'.$Anio.$Mes.$Dia.'01',
												  str_pad($cont,4,"0",STR_PAD_LEFT),
												   $codconcepto,
												   '',
												   '0001',
												   '',
												   '',
												   '',
												    $Mes."/".$Dia."/".$Anio,
												    '1 1',
												    '',
												    $NroCuentaConcepto,
												    '',
												    '03',
												    'EFECTIVO',
												    '',
												    '',
												    '',
												    '',
												     $concepto,
												    $flag,
												    $TotalCuenta,
												    $tipocta,
												    '','',
												    'P','',	$codconcepto,

												)
								  );  */
		}
			
	/*
	--SELECT * FROM facturacion.detfacturacion where codsuc=2 and codconcepto=10005
	--update facturacion.detfacturacion set codconcepto=10012 where codsuc=2 and codconcepto=10005
	SELECT * from cobranza.detpagos where codsuc=2 and codconcepto=10005 and importe<0
	update cobranza.detpagos set codconcepto=10012 where codsuc=2 and codconcepto=10005 and importe<0
	*/
	/*		$Sql ="SELECT con.ctadebe,SUM(det.importe) AS Totales,con.descripcion,con.codconcepto,cab.fechareg 
				FROM cobranza.cabpagos AS cab 
				INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago 
				INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto 
				WHERE cab.codsuc = ".$codsuc." AND cab.fechareg = '".$fecha."' 
				GROUP BY con.ctadebe ,con.descripcion,con.codconcepto,cab.fechareg
				ORDER BY con.ctadebe  ASC ";
			$ConsultaT = $conexion->query($Sql);
			foreach($ConsultaT->fetchAll() as $rowT)
			{
				//$TotalPec+=$rowT[1];
				$NroCuentaConcepto=$rowT[0];
				$TotalCuenta=$rowT[1];
				$cont++;
				$concepto= $rowT[2];
				$codconcepto= $rowT[3];

				$Sql="INSERT INTO public.tesor_ingresosasientos 
						(idingresodetalle,idempresa,idsucursal,idingreso,nrocomprobante,nroctacontable,
							descripcion,idcentrocostos,tipo,importe,fecha) 
						VALUES ('".$idingresodetalle."',1,".$codsuc.",'".$idingreso."','".$nrocomprobante."',
							  '".$NroCuentaConcepto."','','".$idcentrocostos."','D','".$TotalCuenta."','".$fecha."');";
				 //$conexion->query($Sql);


				//////HABER
				$Sql="INSERT INTO public.tesor_ingresosasientos 
						(idingresodetalle,idempresa,idsucursal,idingreso,nrocomprobante,nroctacontable,
							descripcion,idcentrocostos,tipo,importe,fecha) 
						VALUES ('".$idingresodetalle."',1,".$codsuc.",'".$idingreso."','".$nrocomprobante."',
							  '".$NroCuentaConcepto."','','".$idcentrocostos."','H','".$TotalCuenta."','".$fecha."');";
				// $conexion->query($Sql);

				$Sql="INSERT INTO public.conta_iteingresos
						(idempresa,idsucursal,idtipocomprobante,idperiodo,nrocomprobante,nroctacontable,idcategoria,
							tipocta,nroctacostos,idproveedor,monto,anio,fecha,anulado,fix_bg,change_sign,idtipodocumentos,nrodocumento) 
						VALUES (1,".$codsuc.",'".$idtipocomprobante."','".$idperiodo."','".$NroPec."','".$NroCuentaConcepto."',
					  		'".$IdCategoria."',1,'".$idcentrocostos."','','".$TotalCuenta."','".$anio."','".$fecha."',
					  		0,0,0,'".$idtipodocumentos."','".$NroPec."');";
				 //$conexion->query($Sql);
				
				 dbase_add_record($tel_dbf, array('52',$Dia."/".$Mes."/".$Anio2,"LC".$Anio.$Mes.$Dia.'01',
												  str_pad($cont,4,"0",STR_PAD_LEFT),
												   '101',
												   '',
												   '0001',
												   '',
												   '',
												   '',
												    $fecha,
												    '1 1',
												    '',
												    $NroCuentaConcepto,
												    '',
												    '03',
												    'EFECTIVO',
												    '',
												    '',
												    '',
												    '',
												     $concepto,
												    "2",
												    $TotalCuenta,
												    'H',
												    '','',
												    'P','',	$codconcepto,

												)
								  );  

			
			}*/
			
		//TOTAL A CAJA
		$Sql = "SELECT SUM(det.importe) AS Totales ";
		$Sql .= "FROM cobranza.cabpagos AS cab ";
		$Sql .= " INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago ";
		$Sql .= " INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto ";
		$Sql .= "WHERE cab.codsuc = ".$codsuc." ";
		$Sql .= " AND cab.fechareg = '".$fecha."' AND cab.anulado = 0";
		
		$ConsultaT = $conexion->query($Sql);
		
		foreach($ConsultaT->fetchAll() as $rowT)
		{
			$cont++;
			
			//$TotalPec+=$rowT[1];
			$NroCuentaConcepto = '101111101';//$rowT[0];
			$TotalCuenta = $rowT[0];
			$concepto = 'CAJA';
			$codconcepto = '999';
			$tipocta = 'D';
			
			$Sql = "INSERT INTO TEL".$Fecha." ";
			$Sql .= "(LORIG, FECT, LDOC1, LLINE, ";
			$Sql .= " N_REN, ILINE, LFECH, LCCAR, ";
			$Sql .= " LCCTA, LTOPE, LCHEQ, GLOSA, LCAJA, ";
			$Sql .= " LIMPOS, FLAGDH, LIDEN, COD_CPTO, ";
			$Sql .= " PRO_RUC, LASIE, LCCOS, NCCOS, LUBIC, LCTAC, LANAL, LCOMP, LTRAB, LPPTO, LIMPOD, FLADDH, RCAJA) ";
			$Sql .= " VALUES(1, {".$Mes."/".$Dia."/".$Anio."}, 'LC".$Anio.$Mes.$Dia."01', '".str_pad($cont, 4, "0", STR_PAD_LEFT)."', ";
			$Sql .= " '".$codconcepto."', '0001', {".$Mes."/".$Dia."/".$Anio."}, '1 1', ";
			$Sql .= " '".$NroCuentaConcepto."', '03', 'EFECTIVO', '".$concepto."', '".$flag."', ";
			$Sql .= " ".$TotalCuenta.", '".$tipocta."', 'P', ' ', ";
			$Sql .= " ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0, ' ', ' ')";
			
			odbc_exec($conexion2, $Sql) or die("<p>".odbc_errormsg());
		}
	}
	
		//mover archivos a carpeta avalon

		//$rutaavalo = '\\\\10.2.0.201\ADMINISTRA\SIINCO\MCAJA\ ';

		/*$tec = $rutaavalo."TEC".$Fecha.".dbf";
		if(file_exists($tec))
		{	
			unlink($tec);  
		}
		$tel = $rutaavalo."TEL".$Fecha.".dbf";
		if(file_exists($tel))
		{	
			unlink($tel);  
		}
		$lcl = $rutaavalo."LCL".$Fecha.".dbf";
		if(file_exists($lcl))
		{	
			unlink($lcl);  
		}*/

		/*$archivo = "tablas/TEC".$Fecha.".dbf";
	    if (!copy($archivo, trim($rutaavalo)."TEC".$Fecha.".dbf")) 
	    {
	   		echo "Error al copiar $archivo...\n";
		}*/
		
	$tec = "tablas/TEC".$Fecha.".dbf";
	if(file_exists($tec))
	{	
		unlink($tec);  
	}
	
	$tel = "tablas/TEL".$Fecha.".dbf";
	if(file_exists($tel))
	{	
		unlink($tel);  
	}
	
	$lcl = "tablas/LCL".$Fecha.".dbf";
	if(file_exists($lcl))
	{	
		unlink($lcl);  
	}
	
//
	$archivo = trim($RutaDisco)."TEC".$Fecha.".dbf";
	if (!copy($archivo, "tablas/TEC".$Fecha.".dbf")) 
	{
		echo "Error al copiar $archivo...\n";
	}
	
	$archivo = trim($RutaDisco)."TEL".$Fecha.".dbf";
	if (!copy($archivo, "tablas/TEL".$Fecha.".dbf")) 
	{
		echo "Error al copiar $archivo...\n";
	}
	//echo $RutaDisco;
	$archivo = trim($RutaDisco)."LCL".$Fecha.".dbf";
	if (!copy($archivo, "tablas/LCL".$Fecha.".dbf")) 
	{
		echo "Error al copiar $archivo...\n";
	}
		 
	echo "Los Asientos Contables de Cobranza<br>";
	echo "<br>";
	echo "<a href='tablas/LCL".$Fecha.".dbf' target='_blank'>LCl".$Fecha."</a><br>";
	echo "<br>";
	echo "<a href='tablas/TEC".$Fecha.".dbf' target='_blank'>TEC".$Fecha."</a><br>";
	echo "<br>";
	echo "<a href='tablas/TEL".$Fecha.".dbf' target='_blank'>TEL".$Fecha."</a><br>";
	echo "<br>";
	echo "Se han Generado Correctamente";
		
		
/*	$sql = "select co.ctadebe,c.car,c.fechareg,c.nropec,
			case when sum(d.importe) is null then 0 else sum(d.importe) end as importe
			from cabpagos as c
			inner join detpagos as d on(c.codemp=d.codemp and c.codsuc=d.codsuc 
			and c.nropago=d.nropago and c.nroinscripcion=d.nroinscripcion)
			inner join conceptos as co on(d.codconcepto=co.codconcepto)
			where c.fechareg='".$fecha."' and c.nropec<>0
			group by co.ctadebe,d.codconcepto,c.car,c.fechareg,c.nropec
			order by d.codconcepto";
	$consulta=pg_query($sql);
	while($row=pg_fetch_array($consulta))
	{
		

	}*/
	
?>
