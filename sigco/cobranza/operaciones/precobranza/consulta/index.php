<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");
	
	$TituloVentana = "PRE PAGOS";
	$Activo=1;
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	$FormatoGrilla = array ();
	
	$Fecha   = isset($_GET["Fecha"])?$_GET["Fecha"]:date('d/m/Y');
	
	$and =" AND cab.fechareg='".$objFunciones->CodFecha($Fecha)."' AND cab.creador=".$_SESSION['id_user'];
	
	$Sql = "select cab.nroprepago,upper(cab.propietario),
              ".$objFunciones->FormatFecha('cab.fechareg').", cab.hora,
              ".$objFunciones->Convert("cab.imptotal","NUMERIC (18,2)").",
              CASE WHEN cab.estado=0 THEN 'ANULADO' WHEN cab.estado=1 THEN 'PENDIENTE' ELSE 'CANCELADO' END,
              cab.nroprepago,1,cab.nroinscripcion,cab.creador,cab.codformapago,cab.fechareg
             from cobranza.cabprepagos as cab
              ";

  $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'cab.nroprepago','3'=>'cab.propietario',
                      '2'=>'cab.nroprepago');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Pre Pago','T2'=>'Propietario',
                        'T3'=>'Fecha','T4'=>'Hora','T5'=>'Importe','T6'=>'Estado'  );   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center','A4'=>'center','A6'=>'center','A9'=>'center','A5'=>'right');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60','W2'=>'180','W3'=>'80','W4'=>'90','W5'=>'70','W6'=>'80','W7'=>'20','W8'=>'80','W9'=>'60','W10'=>'60','W11'=>'60');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  and (cab.codemp=1 and cab.codsuc=".$codsuc.") ". $and ." ORDER BY cab.nroprepago DESC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'imprimir.png',   //Imagen a mostrar
              'Btn1'=>'Imprimir Boleta',       //Titulo del Botón
              'BtnF1'=>'onclick="imprimirticket(this);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación 
              );
			  
  $FormatoGrilla[10] = array(array('Name' =>'nroinscripcion','Col'=>9),array('Name' =>'fechapago','Col'=>12),array('Name' =>'codpago','Col'=>1));//DATOS ADICIONALES           
  $FormatoGrilla[11]=6;//FILAS VISIBLES            
  $_SESSION['Formato'] = $FormatoGrilla;
  
  $Previo=' <table width="100%" border="0" >';
  $Previo.='<tr><td valign="middle" align="left" width="65">Fecha :</td><td valign="middle" align="left">';
  $Previo.= '<input type="text" class="inputtext ui-corner-all text" name="3form1_fecha" id="Fecha" value="'.$Fecha.'" readonly="readonly" style="width:100px" onchange="BuscarB('.$Op.')"/>';
  $Previo.='</td></tr></table>';
  
  Cabecera($Previo, $FormatoGrilla[7],1000,600);
  Pie();
  CuerpoInferior();


?>
<script>
function BuscarB(Op)
  {
    var Valor     = document.getElementById('Valor').value
    var Fecha   = document.getElementById('Fecha').value
    var Op2 = ''
    if (Op!=0)
    {
      Op2 = '&Op=' + Op;
    }
    location.href='index.php?Valor=' + Valor + '&pagina=' + Pagina + Op2 + '&Fecha='+Fecha;
  }
$(document).ready(function(){
  $("#BtnRegresar").show();
  $( "#Fecha" ).datepicker(
    {
      showOn: 'button',
      direction: 'up',
      buttonImage: '../../../../../images/iconos/calendar.png',
      buttonImageOnly: true,
      showOn: 'both',
      showButtonPanel: true,

      beforeShow: function(input, inst) 
      { 
        inst.dpDiv.css({marginTop: (input.offsetHeight ) - 20  + 'px', marginLeft: (input.offsetWidth) - 90 + 'px' }); 
      }
    });

  $("#BtnNuevoB").remove();
	 $("#DivUpdate").dialog({
		  autoOpen: false,
		  modal: true,
		  width: 400,
		  height:300,
		  resizable: true,
		  show: "scale",
		  hide: "scale",
		  close: function() {
		    
		    },
		  buttons: {
		  "Actualizar": function() { Actualizar(); } ,
		  "Cancelar": function() { $( this ).dialog( "close" ); } 
		}
		   //
		 });
	  });

	function SeleccionaIdB(obj,nropec,estado)
	{
		Id 		= 'Id=' + obj.id;
		IdAnt	= Id2;
		Id2 	= obj.id;
		pec		= nropec
		est		= estado
		if (IdAnt!='' && estant==0)
		{
			Limpiar(IdAnt);
		}
		if(est==0)
		{	
			$("#"+obj.id).addClass( "ui-state-active" );
		}
		estant=est
	}
	function imprimirrecibo(obj)
	{
    var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
    var codciclo =1
    var fechapago = $(obj).parent().parent().data('fechapago')
    var codpago = $(obj).parent().parent().data('codpago')
		AbrirPopupImpresion("imprimir_recibo.php?nroinscripcion=" + nroinscripcion + '&codsuc=<?=$codsuc?>&codciclo=' + codciclo + '&fechapago=' + fechapago+'&codpago=' + codpago, 800, 600);
	}
	function imprimirsustitutorio(obj)
	{
    var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
    var codciclo =1
    var fechapago = $(obj).parent().parent().data('fechapago')
    var codpago = $(obj).parent().parent().data('codpago')
		AbrirPopupImpresion("imprimir_sustitutorio.php?nroinscripcion=" + nroinscripcion + '&codsuc=<?=$codsuc?>&codciclo=' + codciclo + '&fechapago=' + fechapago + '&codpago=' + codpago, 800, 600);
	}
	function imprimirticket(obj)
	{
    var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
    var codciclo =1
    var fechapago = $(obj).parent().parent().data('fechapago')
    var codpago = $(obj).parent().parent().data('codpago')
		AbrirPopupImpresion("../../cancelacion/consulta/imprimir_boleta_prepago.php?nroinscripcion=" + nroinscripcion + '&codsuc=<?=$codsuc?>&codciclo=' + codciclo + '&fechapago=' + fechapago + '&codpago=' + codpago, 800, 600);
	}

	function Operacion(Op)
	{
		if(Op!=4)
		{
			var url = 'mantenimiento.php?Op='+Op;
			if(Op!=1)
			{
				if(Id=="")
				{
					alert('Seleccione un Elemento del Item')
					return
				}
				url=url+'&'+Id;
			}
			location.href=url;
		}else{
			location.href='../index.php'
		}
	}
var Crador=''
function ActualizarPago(nropago,importe,propietario,car,nrocaja,creador,nroinscripcion,codformapago)
{
	$("#NroPago").val(nropago);
	$("#Importe").val(importe);
	$("#Propietario").val(propietario);
	$("#car").val(car);
	$("#caja").val(nrocaja);
	$("#nroinscripcion").val(nroinscripcion);
	$("#formapago").val(codformapago);
	Crador=creador
	cUsuarios()
	$("#DivUpdate").dialog('open');
}
function cUsuarios()
{
  //return false;
	var IdCaja = $("#caja").val()
    $.ajax({
      url:'cCajeros.php',
      type:'POST',
      async:true,
      data:'IdCaja=' + IdCaja+'&Crador='+Crador,
      success:function(datos)
      {
          $("#Cajero").html(datos);
     
      }
    })
}
function Actualizar()
{
var obj = $("#Propietario");
  if(obj.val()=="")
  {
    Msj(obj,'Ingrese Propietario',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
  obj = $("#car");
  if(obj.val()=="0")
  {
    Msj(obj,'Seleccione Car',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
   obj = $("#caja");
  if(obj.val()=="0")
  {
    Msj(obj,'Seleccione Caja',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
obj = $("#Cajero");
  if(obj.val()=="")
  {
    Msj(obj,'Seleccione Cajero',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
  obj = $("#formapago");
  if(obj.val()=="0")
  {
    Msj(obj,'Seleccione Forma Pago',1000,'above','error',true);
    return false;
  }
  obj.removeClass( "ui-state-error" );
	var Data = $("#form1").serialize();
    $.ajax({
      url:'Actualizar.php',
      type:'POST',
      async:true,
      data:Data,
      success:function(datos)
      {
          if(datos==0)
          {
          	window.parent.OperMensaje('Pago Actualizado Correctamente',1)
          	window.location.assign('index.php')
          }
     	else{
     		window.parent.OperMensaje('Pago Actualizado Correctamente',1)
     	}
      }
    })
}
</script>
<div id="frmpop2" style="display:none; width:700px">
  <div style="width:100%;" class="barra" >
  		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr style="padding:4px">
            <td  style="color:#FFF; font-size:12px; font-weight:bold;">Ingresos en Caja</td>
            <td align="right">
            	<img onClick="close_frmpop();" src="<?php echo $_SESSION['urldir'];?>images/iconos/Close.gif" height="15" width="15" style="cursor:pointer" />
            </td>
          </tr>
        </table>
  </div>
  <table width="100%" bgcolor="#EEEEEE">
       <tr>
		<td width="100%" align="center" valign="middle" class="tdconfir" id="btn_conf">
			<div id="div_detalle"></div>
		</td>
    </tr>
    </table>    	
</div>
<div id="DivUpdate" title="Actualizar Datos del Pago" style="display: none; font-size:14; font-weight:bold">
	<form id="form1" name="form1">
  <table border="0" cellspacing="0" cellpadding="0" width="100%" >
    <tr>
        <td class="TitDetalle">Nro Pago :</td>
        <td class="CampoDetalle">
          <input readonly="readonly" type="text" class="text ui-corner-all" title="NroPago"  id="NroPago" name="NroPago" />
          <input readonly="readonly" type="hidden"  id="nroinscripcion" name="nroinscripcion" />
        </td>
      </tr>
      <tr>
        <td class="TitDetalle">Importe:</td>
        <td class="CampoDetalle">
          <input readonly="readonly" type="text" class="numeric text ui-corner-all" title="Importe"  id="Importe"/>
        </td>
      </tr>
      <tr>
        <td class="TitDetalle">Propietario :</td>
        <td class="CampoDetalle">
          <input  type="text" class="text2 ui-corner-all" title="Propietario"  id="Propietario" name="Propietario" readonly="readonly"/>
        </td>
      </tr>
      <tr>
        <td class="TitDetalle">Car:</td>
        <td class="CampoDetalle">
        <? echo $objMantenimiento->drop_car($codsuc, 1); ?>
          
        </td>
      </tr>
     <tr>
        <td class="TitDetalle">Caja:</td>
        <td class="CampoDetalle">
        <? echo $objMantenimiento->drop_cajas($codsuc, 'onchange="cUsuarios()"'); ?>
          
        </td>
      </tr>
       <tr >
        <td class="TitDetalle">Cajero:</td>
        <td class="CampoDetalle">
        <select id="Cajero"  class="requiere ui-corner-all text2" title="Tipo Documento" data-actual="<?=$IdTipoDocumento?>" name="Cajero">
        </select>
       	</td>
      </tr>
      <tr>
        <td class="TitDetalle">Forma:</td>
        <td class="CampoDetalle">
         <? $objMantenimiento->drop_forma_pago($formapago, "", " WHERE estareg = 1 AND tipodocumento = 0"); ?>
       	</td>
      </tr>
  </table>
</div>
</form>
<?php CuerpoInferior();?>