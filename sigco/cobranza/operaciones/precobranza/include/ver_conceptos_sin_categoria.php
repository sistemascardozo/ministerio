<?php 
	include("../../../../../config.php");
	
	$codsuc = $_GET["codsuc"];
	
	$Sql = "SELECT c.codconcepto, c.descripcion, t.descripcion AS tipo ";
	$Sql .= "FROM facturacion.conceptos c ";
	$Sql .= " INNER JOIN public.tipoconceptos t ON(c.codtipoconcepto = t.codtipoconcepto) ";
	$Sql .= "WHERE c.estareg = 1 AND c.codsuc = ".$codsuc." ";
	$Sql .= "ORDER BY c.codconcepto";
	
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array());
	$items = $consulta->fetchAll();

?>
<link href="../../../../../css/css_base.css" rel="stylesheet" type="text/css">
<div style="height:10px"></div>
<table width="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" style="cursor:pointer">
  <tr align="center" class="CabIndex">
    <td width="11%">Codigo</td>
    <td width="66%">Descripcion</td>
    <td width="23%">Tipo de Concepto</td>
  </tr>
  <?php
  	foreach($items as $row)
	{
		$consultaV = $conexion->prepare("SELECT COUNT(*) FROM cobranza.catcobranza_conceptos WHERE codsuc = ? AND codconcepto = ?");
		$consultaV->execute(array($codsuc, $row["codconcepto"])); 
		$itemsV = $consultaV->fetch();
		
		if($itemsV[0] == 0)
		{
  ?>
  	<tr>
        <td width="11%" align="center"><?=$row["codconcepto"]?></td>
        <td width="66%"><?=utf8_decode(strtoupper($row["descripcion"]))?></td>
        <td width="23%"><?=strtoupper($row["tipo"])?></td>
    </tr>
  <?php } } ?>
 </table>