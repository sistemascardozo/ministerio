<?php 
	include("../../../../../config.php");
	
	$codsuc			= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$nrofacturacion = $_POST["nrofacturacion"];
	$categoria		= $_POST["categoria"];
	$codtipodeuda	= $_POST["codtipodeuda"];
	
	$sql = "SELECT d.codconcepto, d.concepto, SUM(d.importe) AS imptotal, SUM(d.importeacta) AS importeacta, ";
	$sql .= " SUM(d.importerebajado) AS importerebajado, ";
	$sql .= " SUM(d.importe) - SUM(d.importeacta) - SUM(d.importerebajado) AS saldo ";
	$sql .= "FROM facturacion.detfacturacion AS d ";
	$sql .= " INNER JOIN facturacion.conceptos AS c ON(d.codemp = c.codemp AND d.codsuc = c.codsuc AND d.codconcepto = c.codconcepto) ";
	$sql .= "WHERE d.codsuc = ? AND d.nroinscripcion = ? AND d.nrofacturacion = ? AND d.categoria = ? AND d.codtipodeuda = ? AND d.estadofacturacion <> 4 ";
	$sql .= "GROUP BY d.codconcepto, d.concepto, c.ordenrecibo, d.item ";
	$sql .= "ORDER BY c.ordenrecibo";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc, $nroinscripcion, $nrofacturacion, $categoria, $codtipodeuda));
	$items = $consulta->fetchAll();
	
	$timptotal = 0;
	$timporteacta = 0;
	$timporterebajado = 0;
	$tsaldo = 0;
?>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="thead-header" style="font-size:12px">
      <tr align="center">
        <th width="85%" >Concepto</th>
        <th width="15%" >Facturado</th>
        <th width="15%" >Amortizado</th>
        <th width="15%" >Rebajado</th>
        <th width="15%" >Saldo</th>
     </tr>
 </thead>
 <tbody>
     <?php
	 	foreach($items as $row)
		{
			$timptotal += $row["imptotal"];
			$timporteacta += $row["importeacta"];
			$timporterebajado += $row["importerebajado"];
			$tsaldo += $row["saldo"];
			//if($timptotal>0)
			//{
	 ?>
     <tr>
        <td align="left" ><?=strtoupper($row["concepto"])?></td>
        <td align="right" ><?=number_format($row["imptotal"],2)?></td>
        <td align="right" ><?=number_format($row["importeacta"],2)?></td>
        <td align="right" ><?=number_format($row["importerebajado"],2)?></td>
        <td align="right" ><?=number_format($row["saldo"],2)?></td>

     </tr>
     <?php } //} ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td align="right" >Totales ==></td>
        <td align="right" ><?=number_format($timptotal,2)?></td>
        <td align="right" ><?=number_format($timporteacta,2)?></td>
        <td align="right" ><?=number_format($timporterebajado,2)?></td>
        <td align="right" ><?=number_format($tsaldo,2)?></td>
     </tr>
 </tfoot>
 
  
</table>