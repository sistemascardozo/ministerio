<style type="text/css">
    .ui-tabs .ui-tabs-panel
    {
        padding: 0em 0em;
    }
    .ui-tabs .ui-tabs-nav
    {
        padding: 0em 0em 0
    }
    /*#siteContenido
    {
      top: 0;
      padding:0;
    }*//**/
    #tbCampos td {
        /*height: 20px;*/
    }
</style>
<fieldset style="padding:4px">
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
            <td width="90" valign="top"><label id="lblTipocobro">N&deg; Inscripci&oacute;n</label></td>
            <td width="20" align="center" valign="top">:</td>
            <td valign="top">
                <input type="text" name="codantiguo" id="codantiguo" class="inputtext" maxlength="15" value="" onkeypress="ValidarCodAntiguo(event);" style="width:70px;"/>
                <input type="hidden" name="nroinscripcion" id="nroinscripcion" class="inputtext" size="15" maxlength="15" value="" onkeypress="ValidarNroInscripcion(event)"/>
                <input type="text" name="nrorecibo" id="nrorecibo" class="inputtext" size="15" maxlength="15" value="<?=$nrorecibo?>" onkeypress="ValidarRecibo(event)" />

                <span class="MljSoft-icon-buscar" title="Buscar Cliente" onclick="buscar_usuarios();"></span>&nbsp;
                <input type="hidden" name="eventualtext" id="eventualtext" value="0" />
                <input type="checkbox" name="eventual" id="eventual" onclick="habilitar_eventual(this);" />
                <label id="lbleventual">Eventual</label>
                &nbsp;                
                <input type="checkbox" id="solocolateral" />
                <label id="lbleventual">Colateral</label>
            </td>
            <td width="100" align="right" valign="top">
                <label id="lbl_nroprepago">Nro.Pre-Pago</label>
            </td>
            <td width="20" align="center" valign="top">
                <label id="lbl_puntos">:</label>
                <label class="verprepago">:</label>
            </td>
            <td width="" valign="top">
                <input type="text" name="nroprepago" id="nroprepago" class="inputtext" maxlength="15" value="" readonly="readonly" style="width:80px;" />
                <span class="MljSoft-icon-buscar" title="Buscar Pre-Pagos" onclick="buscar_prepagos();" id="b_prepago"></span>
                <input type="hidden" name="anio" id="anio" value="0" />
                <input type="hidden" name="mes" id="mes" value="0" />
                <input type="hidden" name="codciclo" id="codciclo" value="0" />
                <input type="hidden" name="nrofacturacionactual" id="nrofacturacionactual" />
                <input type="hidden" name="glosa" id="Glosa" value="" />
                <input type="hidden" name="nuevocliente" id="nuevocliente" value="" />
                <input type="hidden" name="codexpediente" id="codexpediente" value="0" />
               
            </td>
            <td width="100" align="right" valign="top"><label class="verprepago2">Nro. Servicio</label> </td>
            <td width="20" align="center" valign="top">:</td>
            <td width="" valign="top">
                <input type="text" name="nroinspeccion" id="nroinspeccion" class="inputtext verprepago2" maxlength="15" value="" readonly="readonly" style="width:80px;" />
                <span class="MljSoft-icon-buscar verprepago2" title="Buscar Servicio" onclick="buscar_servicio();" ></span>
                
                <input type="hidden" name="cont_facturacion" id="cont_facturacion" value="0" />
                <input type="hidden" name="cont_cobranza" id="cont_cobranza" value="0" />

                <input type="hidden" name="subtotaltotal" id="subtotaltotal" value="0" />
                <input type="hidden" name="igvtotal" id="igvtotal" value="0" />
                <input type="hidden" name="redondeototal" id="redondeototal" value="0" />
                <input type="hidden" name="totalpago" id="totalpago" value="0" />
            </td>
            <td width="120" align="right">
                <input type="button" onclick="imprimir_duplicado()" value="Duplicado" id="BtnImprimir" title="Imprimir Recibo Duplicado">
            </td>
        </tr>
        <tr>
            <td valign="top">Cliente</td>
            <td align="center" valign="top">:</td>
            <td valign="top">
                <input type="text" name="cliente" id="cliente" class="inputtext" maxlength="45" value="" readonly="readonly" style="width:250px;"  />
            </td>
            <td align="right" valign="top"><label id="lbl_condpago">Cond. Pago</label></td>
            <td align="center" valign="top"><label id="lbl_puntos_prepago">:</label></td>
            <td valign="top">
                <select name="condpago" id="condpago" style="width:100px" class="select">
                    <option value="0">CONTADO</option>
                    <option value="1">CREDITO</option>
                </select>
                Cargo&nbsp;:&nbsp;
                <input type="text" name="nrocargo" id="nrocargo" class="inputtext" maxlength="5" value="" readonly="readonly" style="width:80px;" />
                <span class="MljSoft-icon-buscar" title="Buscar Cargos" onclick="buscar_cargos();" ></span>
                
            </td>
            <td align="right" valign="top">Fecha </td>
            <td align="center" valign="top">:</td>
            <td valign="top">
                <input type="text" name="fechareg" id="fechareg" class="inputtext" readonly="readonly" value="<?=$fechaserver?>" style="width:80px;" />
                
            </td>
            <td align="right">
                <input type="button" onclick="javascrip:location.href = 'consulta/'" value="Pagos" id="BtnBuscar" title="Consultar Pagos">
            </td>
        </tr>
        <tr>
            <td valign="top">Direccion</td>
            <td align="center" valign="top">:</td>
            <td valign="top">
                <input type="text" name="direccion" id="direccion" class="inputtext" maxlength="45" value="" readonly="readonly" style="width:250px;" />
            </td>

            <td align="right" valign="top">Forma</td>
            <td align="center" valign="top">:</td>
            <td valign="top">
                <?php $objMantenimiento->drop_forma_pago($formapago, "", " WHERE estareg = 1 AND tipodocumento = 0"); ?>
            </td>
            <td align="right" valign="top">Car</td>
            <td align="center" valign="top">:</td>
            <td valign="top">
                <input type="text" name="carx" id="carx" value="<?= $cars ?>" class="inputtext" readonly="readonly" size="20" maxlength="35"/>
                
            </td>
            <td align="right">
                <input type="button" onclick="Cerrar();" value="Cancelar" id="BtnCancelar" title="Salir del Modulo" />
            </td>
        </tr>
        <tr>
            <td valign="top">Doc. Ident</td>
            <td align="center" valign="top">:</td>
            <td valign="top">
                <input type="text" name="docidentidad" id="docidentidad" class="inputtext" size="15" maxlength="45"  value=""/>
                &nbsp;&nbsp;Glosa&nbsp;&nbsp;:&nbsp;&nbsp;<span class="icono-icon-detalle" title="Glosa" onclick="VerGlosa()"></span>
          </td>
            <td align="right" valign="top">Documento</td>
            <td align="center" valign="top">:</td>
            <td valign="top">
                <?php $objMantenimiento->drop_documentos(13, 'onChange="cSerieNun(this)"', " WHERE estareg = 1 AND tipodocumento = 0 AND codsuc = ".$codsuc); ?>

            </td>
            <td align="right" valign="top">Serie</td>
            <td align="center" valign="top">:</td>
            <td valign="top">
              <input type="text" name="seriedoc" id="seriedoc" class="inputtext" maxlength="45"  readonly="readonly" value="" style="width:40px;" />
                - <input type="text" name="numerodoc" id="numerodoc" class="inputtext" maxlength="45" readonly="readonly" value="" style="width:70px;" />
                <br />
                <div id="div_estadoservicio" style="color:#FF0000; font-size:13px; font-weight:bold; padding:1px; display:inline"></div>
          </td>
            <td align="right" valign="top">
                <input type="button" onclick="imprimir_historial()" value="Cta. Cte." id="BtnCtaCte" title="Imprimir Cuenta Corriente" />
          </td>
      </tr>
    </table>
</fieldset>