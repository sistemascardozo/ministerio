<div style="overflow:auto; height:120px">
	<table border="0" align="center" cellspacing="2" cellpadding="2" class="ui-widget"  width="100%" id="tbconceptos" rules="all">
		<thead class="thead-header" >
			<tr align="center">
				<th>Codigo</th>
				<th>Categoria</th>
				<th>Concepto</th>
				<th>Cta. Debe</th>
				<th width="120">Importe</th>
				<th width="120">Presupuesto</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
        <tbody>
<?php
	$contconcepto	= 0;
	$contvalidar	= 0;
	$mensaje		= "";
		
   	$Sql = "SELECT c.codconcepto, t.descripcion AS tipo, c.descripcion AS concepto, c.ctadebe, c.importe, c.presupuesto, c.tipopresupuesto, c.afecto_igv ";
	$Sql .= "FROM facturacion.conceptos AS c ";
	$Sql .= " INNER JOIN public.tipoconceptos AS t ON(c.codtipoconcepto = t.codtipoconcepto) ";
	$Sql .= "WHERE c.codtipoconcepto = 2 AND c.estareg = 1 AND c.codsuc = ".$codsuc." ";
	$Sql .= "ORDER BY c.codconcepto";
	
	$consultaC = $conexion->prepare($Sql);
	$consultaC->execute(array());
	$itemsC = $consultaC->fetchAll();
		
	foreach($itemsC as $rowC)
	{
		$contconcepto++;
			
		$consultaV = $conexion->prepare("SELECT COUNT(*) FROM cobranza.catcobranza_conceptos WHERE codsuc = ? AND codconcepto = ?");
		$consultaV->execute(array($codsuc, $rowC["codconcepto"])); 
		$itemsV = $consultaV->fetch();
			
		$color = "";
		
		if($itemsV[0] == 0)
		{
			$color = "color:#F00;";
			$contvalidar++;
		}
?>
			<tr style="background-color:#FFF; <?=$color?>" data-orden="<?=$contconcepto?>" id="TbC_<?=$rowC["codconcepto"]?>">
				<td align="center">
					<input type="hidden" name="codconcepto<?=$contconcepto?>" id="codconcepto<?=$contconcepto?>" value="<?=$rowC["codconcepto"]?>" />
					<?=$rowC["codconcepto"]?>
				</td>
				<td align="center"><?=strtoupper($rowC["tipo"])?></td>
				<td style="padding-left:5px;">
					<input type="hidden" name="concepto<?=$contconcepto?>" id="concepto<?=$contconcepto?>" value="<?=strtoupper($rowC["concepto"])?>" />
					<?=strtoupper($rowC["concepto"])?>
				</td>
				<td align="center"><?=strtoupper($rowC["ctadebe"])?></td>
				<td align="center">
					<input type="text" name="importe<?=$contconcepto?>" id="importe<?=$contconcepto?>" maxlength="15" value="<?=number_format($rowC["importe"], 2)?>" class="inputtext focusselect" style="text-align:right; width:100px;" onKeyPress="return permite(event, 'num');" <?=$ventana=="precobranza"?"":""?> />
				</td>
				<td align="center">
					<input type="text" name="nropresupuesto<?=$contconcepto?>" id="nropresupuesto<?=$contconcepto?>" maxlength="15" value="" class="inputtext" style="text-align:right; width:100px;" readonly="readonly" />
					<input type="hidden" name="tippresupuesto<?=$contconcepto?>" id="tippresupuesto<?=$contconcepto?>" value="<?=$rowC["presupuesto"]?>" />
					<input type="hidden" name="afecto_igv<?=$contconcepto?>" id="afecto_igv<?=$contconcepto?>" value="<?=$rowC["afecto_igv"]?>" />
					<input type="hidden" name="afecto_igv_pre<?=$contconcepto?>" id="afecto_igv_pre<?=$contconcepto?>" value="0" />
				</td>
				<td style="padding-left:5px;">
<?php
		if($rowC["presupuesto"] == 1)
		{
?>
					<span class="MljSoft-icon-buscar" title="Buscar Presupuesto" onclick="buscar_presupuesto(<?=$contconcepto?>, <?=$rowC["tipopresupuesto"]?>);"></span>
					<span class="icono-icon-detalle con<?=$contconcepto?>" title="Detalle del Credito" onclick="detalle_credito(<?=$contconcepto?>);" style="display:none" ></span>
<?php
		}
?>
					<span class="icono-icon-add"  title="Agregar Conceptos" onclick="agregar_concepto(<?=$contconcepto?>);"></span>
				</td>
			</tr>
<?php 
	} 

	if($contvalidar > 0)
	{
		$mensaje = "HAY CONCEPTOS SIN REGISTRAR LA CATEGORIA <span onclick='cargar_conceptos_sin_categoria();' style='cursor:pointer;color:#000;'>VER AQUI CONCEPTOS FALTANTES !!!!!</span>";
	}
?>
		</tbody>
	</table>
</div>