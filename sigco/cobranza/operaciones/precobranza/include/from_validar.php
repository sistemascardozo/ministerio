<script>
	var op = 1;

	$(function() {
		$("#fdescarga").mask("99/99/9999");
		$("#car").keypress(function(e){ValidarPass(e)});
		
		$("#fdescarga").datepicker({
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true,
			beforeShow: function(input, inst) 
			{ 
				inst.dpDiv.css({marginTop: (input.offsetHeight ) - 20  + 'px', marginLeft: (input.offsetWidth) - 90 + 'px' }); 
			}
		});

		$("#dialog-form-validar:ui-dialog").dialog("destroy");
		$("#dialog-form-validar").dialog({
			autoOpen: true,
			height: 300,
			width: 400,
			modal: true,
/*			show: "blind",
			hide: "scale",*/
			buttons: {
				"Aceptar": function() {
					validar_usuario();
				},
				"Cancelar": function() { 
					location.href = '../../../../admin/index.php';
				}
			},
			close: function() {
				if(op == 1)
				{
					location.href = '../../../../admin/index.php';
				}
				else
				{
					location.href = 'index.php?Op=0&fdescarga=' + $("#fdescarga").val() + "&cars=" + $("#car option:selected").text();
				}
			}
		});
	})
   
	function validar_usuario()
	{
		var url = "";
		var user = "";
		
		if($("#contra").val() == "")
		{
			$("#mensaje").html("Digite la Contrase&ntilde;a del Usuario");
			$("#contra").focus().select();
			
			return false					
		}
		
		if($("#car").val() == 0)
		{
			$("#mensaje").html("Seleccione CAR");
			$("#car").focus();
			
			return false					
		}
		
		if(ventana == "cobranza" || ventana == "precobranza")
		{
			url = urldir + 'sigco/cobranza/operaciones/cancelacion/ajax/validar_usuario.php';
		}
		else
		{
			url = urldir + 'sigco/cobranza/operaciones/cancelacion/ajax/validar_usuario_remove.php';
			user = "&user=" + usuario;
		}

		$.ajax({
			 url:url,
			 type:'POST',
			 async:true,
			 data:'codsuc=' + codsuc + '&contra=' + $("#contra").val() + "&car=" + $("#car").val() + "&fdescarga=" + $("#fdescarga").val() + user,
			 success:function(datos){
				var r = datos.split("|")

				if(r[0] == 1)
				{
					$("#mensaje").html(r[1]);
					$("#contra").focus().select();
				}
				else
				{
					op = r[0];
					
					$("#dialog-form-validar").dialog("close");
				}
			 }
		})
	}
	
	function ValidarPass(e)
	{
		if (VeriEnter(e))
		{ 
	 		//validar_usuario_remove($("#user").val(),$("#contra_remove").val())
	 		validar_usuario();
			
			e.returnValue = false;
	    }
	}
</script>
<div id="dialog-form-validar" title="Validacion de Usuario"  >
<table width="100%" border="0" >
  <tr style="height:5px">
    <td colspan="3" align="right"></td>
  </tr>
  <tr>
    <td width="34%" align="right">Confirmar Contrase&ntilde;a</td>
    <td width="3%" align="center">:</td>
    <td width="63%"><label>
      <input type="password" name="contra" id="contra" size="15" maxlength="20" class="inputtext" onkeypress="ValidarPass(event)">
    </label></td>
  </tr>
  <tr id="TrFecha">
    <td align="right">Fecha</td>
    <td align="center">:</td>
    <td><input type="text" name="fdescarga" id="fdescarga" size="12" maxlength="30" class="inputtext" value="<?=$fechaserver?>"></td>
  </tr>
  <tr>
    <td align="right">CAR</td>
    <td align="center">:</td>
    <td >
    	<div id="DivCar">
    	<? echo $objMantenimiento->drop_car($codsuc,isset($_SESSION["car"])?$_SESSION["car"]:"0"); ?>
    	</div>
    	<div id="DivNomCar"></div>
    </td>
  </tr>
  <tr style="height:5px">
    <td colspan="3" align="center">
      <div id="mensaje" style="color:#F00; font-weight:bold; font-size:12px; padding:4px">
      </div>
      </td>
  </tr>
  </table>
</div>
<script>
	$("#contra").focus();
//$("#car").val(2)
</script>