<?php
	//if(!session_start()){session_start();}

	include("clsproceso.php");

	include("../../../../objetos/clsDrop.php");

	// ini_set("display_erros", 1);
	// error_reporting(E_ALL);

	$objDrop = new clsDrop();

	$codemp			= 1;
	$codsuc 		= $_SESSION["IdSucursal"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$car			= $_SESSION["car"];
	$formapago		= $_POST["formapago"];
	$idusuario		= $_SESSION['id_user'];
	$fechareg		= $objDrop->CodFecha($_POST["fechareg"]);
	$subtotal		= $_POST["subtotal"];
	$hora			= $objDrop->HoraServidor();
	$codciclo		= $_POST["codciclo"];
	$condpago		= $_POST["condpago"];
	$igv			= $_POST["igv"];
	$redondeo		= $_POST["redondeo"];
	//die($redondeo);
	$total			= $_POST["total"];
	$eventual		= $_POST["eventualtext"];
	$propietario	= $_POST["cliente"];
	$direccion		= $_POST["direccion"];
	$docidentidad	= $_POST["docidentidad"];
	$count			= $_SESSION["ocobranza"]->nitem;
	$contador		= $_POST["cont_cobranza"];
	$nroprepago 	= $objDrop->setCorrelativosVarios(6,$codsuc,"SELECT",0);

	$glosa = $_POST["glosa"];
	$nroinspeccion=$_POST['nroinspeccion']?$_POST['nroinspeccion']:0;

	$EsServicio=0;
	//OBTENER CORRELATIVOS ACTUALES
    $DocSeries= $objDrop->GetSeries($codsuc,$_POST['documento']);
    $seriedoc = $DocSeries["serie"];
    $numerodoc = $DocSeries["correlativo"];
    //OBTENER CORRELATIVOS ACTUALES
    $conexion->beginTransaction();

	$sqlC = "INSERT INTO cobranza.cabprepagos ";
	$sqlC .= " (codemp, codsuc, nroinscripcion, nroprepago, car, codformapago, creador, fechareg, subtotal, hora, ";
	$sqlC .= " codciclo, condpago, igv, redondeo, imptotal, eventual, propietario, direccion, documento, glosa, nroinspeccion)
			values(:codemp,:codsuc,:nroinscripcion,:nroprepago,:car,:codformapago,:creador,:fechareg,
			:subtotal,:hora,:codciclo,:condpago,:igv,:redondeo,:imptotal,:eventual,:propietario,:direccion,:documento,:glosa,:nroinspeccion)";
	$resultC = $conexion->prepare($sqlC);
	$resultC->execute(array(":codemp"=>$codemp,
							":codsuc"=>$codsuc,
							":nroprepago"=>$nroprepago,
							":nroinscripcion"=>$nroinscripcion,
							":car"=>$car,
							":codformapago"=>$formapago,
							":creador"=>$idusuario,
							":fechareg"=>$fechareg,
							":subtotal"=>$subtotal,
							":hora"=>$hora,
							":codciclo"=>$codciclo,
							":condpago"=>$condpago,
							":igv"=>$igv,
							":redondeo"=>$redondeo,
							":imptotal"=>$total,
							":eventual"=>$eventual,
							":propietario"=>$propietario,
							":direccion"=>$direccion,
							":documento"=>$docidentidad,
							":glosa"=>$glosa,
							":nroinspeccion"=>$nroinspeccion));
	if($resultC->errorCode()!='00000')
	{
		$conexion->rollBack();
		$data['mensaje'] = "Error update cabpagos 300";
		$data['res'] = 2;
        die(json_encode($data)) ;
	}

	$Item = 0;

	for($i=1;$i<=$contador;$i++)
	{
		if(isset($_POST["nrofacturacion".$i]))
		{
			$nrofacturacion = $_POST["nrofacturacion".$i];

			if($nrofacturacion != 0)
			{
				for($j=0; $j<= $count; $j++)
				{
					if($nrofacturacion == $_SESSION["ocobranza"]->nrofacturacionx[$j])
					{
						$Item += 1;

						$sqlD = "INSERT INTO cobranza.detprepagos(codemp,codsuc,nroinscripcion,nroprepago, item, codconcepto,importe,detalle,tipopago,
								nrofacturacion,coddocumento,nrodocumento,serie,codtipodeuda,anio,mes,categoria) values(:codemp,:codsuc,:nroinscripcion,
								:nroprepago, :item, :codconcepto, '".$_SESSION["ocobranza"]->importex[$j]."',:detalle,:tipopago,:nrofacturacion,:coddocumento,:nrodocumento,:serie,:codtipodeuda,
								:anio,:mes,:categoria)";

						$resultD = $conexion->prepare($sqlD);
						$resultD->execute(array(":codemp"=>$codemp,
												":codsuc"=>$codsuc,
												":nroinscripcion"=>$nroinscripcion,
												":nroprepago"=>$nroprepago,
												":item"=>$Item,
												":codconcepto"=>$_SESSION["ocobranza"]->codconceptox[$j],
												":detalle"=>$_POST["detallex".$i],
												":tipopago"=>$_SESSION["ocobranza"]->tipopagox[$j],
												":nrofacturacion"=>$nrofacturacion,
												":coddocumento"=>4,
												":nrodocumento"=>$_SESSION["ocobranza"]->nrodocumentox[$j],
												":serie"=>"0002",
												":codtipodeuda"=>$_SESSION["ocobranza"]->codtipodeudax[$j],
												":anio"=>$_SESSION["ocobranza"]->aniox[$j],
												":mes"=>$_SESSION["ocobranza"]->mesx[$j],
												":categoria"=>$_SESSION["ocobranza"]->categoriax[$j]
												 ));
						if($resultD->errorCode()!='00000')
						{
							$conexion->rollBack();
							$data['mensaje'] = "Error update cabpagos";
							$data['res']    =2;
                			die(json_encode($data)) ;
						}

						$AnioC = $_SESSION["ocobranza"]->aniox[$j];
						$MesC = $_SESSION["ocobranza"]->mesx[$j];
						$CateC = $_SESSION["ocobranza"]->categoriax[$j];

						$codtipodeuda = $_SESSION["ocobranza"]->codtipodeudax[$j];
					}
				}

			}else
			{

					$npresupuesto = $_POST["npresupuesto".$i] != "" ? $_POST["npresupuesto".$i] : 0;
					$npresupuesto_con_expediente = (isset($_POST["codexpediente".$i]))?$_POST["codexpediente".$i]:0;

					// Cuando el expediente se debe registrar en detpagos
					if(!empty($npresupuesto)){
						$npresupuesto_mod = $npresupuesto;
					}else{
						$npresupuesto_mod = $npresupuesto_con_expediente;
					}

					$EsServicio = 1;

					$Item += 1;

					$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
					$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, ";
					$sqlD .= " nrodocumento, serie, codtipodeuda, anio, mes, ";
					$sqlD .= " categoria, codcategoria, nropresupuesto) ";
					$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepago.", ".$Item.", ".$_POST["codconceptox".$i].", ";
					$sqlD .= " '".$_POST["imptotal".$i]."', '".$_POST["detalle".$i]."', 0, ".$nrofacturacion.", ".$_POST['documento'].", ";
					$sqlD .= " ".$numerodoc.", '".$seriedoc."', ".$_POST["codtipodeuda".$i].", '".$_POST["anioy".$i]."', '".$_POST["mesy".$i]."', ";
					$sqlD .= " 3, ".$_POST["codcategoria".$i].", ".$npresupuesto_mod.")";

					$resultD = $conexion->prepare($sqlD);
					$resultD->execute(array());

					if($resultD->errorCode()!='00000')
					{
						$conexion->rollBack();
						$data['mensaje'] = "Error update detprepagos 100";
						$data['res'] = 2;

						// echo $sqlD;

                		die(json_encode($data)) ;
					}

					$AnioC = $_POST["anioy".$i];
					$MesC = $_POST["mesy".$i];
					$CateC = $_POST["codcategoria".$i];

					$codtipodeuda = $_POST["codtipodeuda".$i];

			}
		}
	}

	if($nrofacturacion == 0)
	{
		//Agregar IGV Redondeo
		if ($igv <> 0)
		{
			$Item += 1;

			// Cuando el expediente se debe registrar en detpagos
			if(!empty($npresupuesto)){
			  $npresupuesto_mod = $npresupuesto;
			}else{
			  $npresupuesto_mod = $npresupuesto_con_expediente;
			}


			$sqlD = "INSERT INTO cobranza.detprepagos(codemp,codsuc,nroinscripcion,nroprepago, item, codconcepto,importe,detalle,tipopago,
						nrofacturacion,coddocumento,nrodocumento,serie,codtipodeuda,anio,mes,categoria, nropresupuesto) values(:codemp,:codsuc,:nroinscripcion,
						:nroprepago, :item, :codconcepto,:importe,:detalle,:tipopago,:nrofacturacion,:coddocumento,:nrodocumento,:serie,:codtipodeuda,
						:anio,:mes,:categoria,:nropresupuesto)";

			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array(":codemp"=>$codemp,
									":codsuc"=>$codsuc,
									":nroinscripcion"=>$nroinscripcion,
									":nroprepago"=>$nroprepago,
									":item"=>$Item,
									":codconcepto"=>5,
									":importe"=>$igv,
									":detalle"=>"I.G.V.",
									":tipopago"=>0,
									":nrofacturacion"=>$nrofacturacion,
									":coddocumento"=>$_POST['documento'],
									":nrodocumento"=>$numerodoc,
									":serie"=>$seriedoc,
									":codtipodeuda"=>$codtipodeuda,
									":anio"=>$AnioC,
									":mes"=>$MesC,
									":categoria"=>$CateC,
									":nropresupuesto"=>$npresupuesto_mod
									 ));
			if($resultD->errorCode()!='00000')
			{
				$conexion->rollBack();
				$data['mensaje'] = "Error update detprepagos IGV";
				$data['res']    =2;

				die(json_encode($data));
			}
		}
		//echo "Paso";
		if ($redondeo < 0)
		{
			$Item += 1;
			// Cuando el expediente se debe registrar en detpagos
			if(!empty($npresupuesto)){
			  $npresupuesto_mod = $npresupuesto;
			}else{
			  $npresupuesto_mod = $npresupuesto_con_expediente;
			}

			$sqlD = "insert into cobranza.detprepagos(codemp,codsuc,nroinscripcion,nroprepago, item, codconcepto,importe,detalle,tipopago,
						nrofacturacion,coddocumento,nrodocumento,serie,codtipodeuda,anio,mes,categoria,nropresupuesto) values(:codemp,:codsuc,:nroinscripcion,
						:nroprepago, :item, :codconcepto,:importe,:detalle,:tipopago,:nrofacturacion,:coddocumento,:nrodocumento,:serie,:codtipodeuda,
						:anio,:mes,:categoria,:nropresupuesto)";

			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array(":codemp"=>$codemp,
									":codsuc"=>$codsuc,
									":nroinscripcion"=>$nroinscripcion,
									":nroprepago"=>$nroprepago,
									":item"=>$Item,
									":codconcepto"=>7,
									":importe"=>$redondeo,
									":detalle"=>"DIFERENCIA REDONDEO NEGATIVO",
									":tipopago"=>0,
									":nrofacturacion"=>$nrofacturacion,
									":coddocumento"=>$_POST['documento'],
									":nrodocumento"=>$numerodoc,
									":serie"=>$seriedoc,
									":codtipodeuda"=>$codtipodeuda,
									":anio"=>$AnioC,
									":mes"=>$MesC,
									":categoria"=>$CateC,
									":nropresupuesto"=>$npresupuesto_mod
									 ));
			if($resultD->errorCode()!='00000')
			{
				$conexion->rollBack();
				$data['mensaje'] = "Error update cabpagos 500";
				$data['res'] = 2;
				die(json_encode($data)) ;
			}
		}
		if ($redondeo > 0)
		{

			// Cuando el expediente se debe registrar en detpagos
			if(!empty($npresupuesto)){
			  $npresupuesto_mod = $npresupuesto;
			}else{
			  $npresupuesto_mod = $npresupuesto_con_expediente;
			}

			$Item += 1;

			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, importe, ";
			$sqlD .= " detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, codtipodeuda, ";
			$sqlD .= " anio, mes, categoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepago.", ".$Item.", 8, '".$redondeo."', ";
			$sqlD .= " 'DIFERENCIA REDONDEO POSITIVO', 0, ".$nrofacturacion.", ".$_POST['documento'].", ".$numerodoc.", '".$seriedoc."', ".$codtipodeuda.", ";
			$sqlD .= " '".$AnioC."', '".$MesC."', ".$CateC.", $npresupuesto_mod)";

			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());

			if($resultD->errorCode()!='00000')
			{
				$conexion->rollBack();
				$data['mensaje'] = "Error update detprepagos redondeo<br>".$sqlD;
				$data['res'] = 2;

				die(json_encode($data)) ;
			}
		}
	}
//					die($sqlD);


	//ACTUALIZA CORRELATIVOS//
	if ($EsServicio == 1)
	{
	    $sql = "UPDATE  reglasnegocio.correlativos
		SET  correlativo = correlativo+1
		WHERE codemp=1 AND  codsuc=" . $codsuc . " AND nrocorrelativo=".$_POST['documento'];
	    $result = $conexion->query($sql);
	    if (!$result) {
	        $conexion->rollBack();
	        $mensaje = "619 Error UPDATE correlativos";
	        $data['res']    =2;
	        $data['mensaje']    =$mensaje;
	        die(json_encode($data));
	    }
	}

	$conexion->commit();
	$objDrop->setCorrelativosVarios(6,$codsuc,"UPDATE",$nroprepago);
	$data['res']        =  1;
	$data['esservicio'] =  $EsServicio;
	$data['pago']       =  $nroprepago;
	$data['tipo']       =  1;//PREPAGO
	echo json_encode($data);

?>
