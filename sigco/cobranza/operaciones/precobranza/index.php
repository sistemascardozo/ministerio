<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "PRE-COBRANZA";
	$Activo=0;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objMantenimiento 	= new clsDrop();
	

	unset($_SESSION["ocobranza"]);
	
	$op				= $_GET["Op"];
	$codsuc 		= $_SESSION["IdSucursal"];
	$fechaserver 	= isset($_GET["fdescarga"])?$_GET["fdescarga"]:$objMantenimiento->FechaServer();
	$cars			= isset($_GET["cars"])?$_GET["cars"]:"";
	$formapago			= isset($_GET["formapago"])?$_GET["formapago"]:"1";
	$paramae		= $objMantenimiento->getParamae("IMPIGV",$codsuc);
	$nrorecibo	= isset($_GET["nrorecibo"])?$_GET["nrorecibo"]:'';
	
	$ventana 				= "precobranza";
	$_SESSION["ventana"] 	= "precobranza";
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="../cancelacion/js_cobranza.js"></script>
<script>
$(document).ready(function() { 
	$("#BtnBuscar").val('Pre Pagos');
	$("#BtnBuscar").attr('title', 'Consultar Pagos');
	
	$(".verprepago, #nrorecibo").hide();
	
	$("#dialog-form-amortizar:ui-dialog").dialog("destroy");
	
	$("#dialog-form-amortizar").dialog({
		autoOpen: false,
		height: 300,
		width: 480,
		modal: true,
		buttons: {
			"Aceptar": function() {
				if(GuardarAmortizacion())
				{
					agregar_facturacion_precobranza();
					
					$(this).dialog("close");
				}
			},
			"Cancelar": function() {
				$(this).dialog("close");
			}
		},
		close: function() {

		}
	});
		
	$('form').keypress(function(e){   
		if(e == 13){
			return false;
		}
	  });

	$('input').keypress(function(e){
		if(e.which == 13){
			return false;
		}
	});

});
	var codsuc			= <?=$codsuc?>;
	var urldir			= "<?php echo $_SESSION['urldir'];?>";
	var usuario			= "<?=strtoupper($_SESSION['user']);?>";
	var ventana			= "precobranza" ;
	
	var cont_fact 		= 0;
	var cont_cob		= 0;
	var contador		= 0;
	
	var subt			= 0;
	var red				= 0;
	var impigv			= <?=$paramae["valor"]?>;
	var tot				= 0;
	
	var idx				= 0;
	var index		 	= 0;
	var object			= "";

	function agregar_facturacion_precobranza()
	{
		var nrofacturacion  = $("#nrofact" + idx).val();
		var tipodeuda		= $("#codtipodeuda" + idx).val();
		var categoria		= $("#catg" + idx).val()
		var importe			= ($("#automatico").val()).replace(',', '');
		//alert(importe);
		var red				= $("#red" + idx).val()
		var igv				= $("#igv" + idx).val()
		var int				= $("#int" + idx).val()
		
		if (int == '')
		{
			int = 0;	
		}
		if (igv == '')
		{
			igv = 0;	
		}
		if (red == '')
		{
			red = 0;	
		}
		
		for (i = 1; i<= $("#cont_cobranza").val(); i++) 
		{
			try
			{
				if($("#nrofacturacion" + i).val() == nrofacturacion && $("#codtd" + i).val() == tipodeuda && $("#codctg" + i).val() == categoria)
				{
					alert('El Nro. Facturacion ya Fue Agregado en la Lista')
					return
				}
			}
			catch(exp)
			{
				
			}
		}
		
		cont_cob++
		contador++
		
		$("#tbdetpagos tbody").append("<tr style='background-color:#FFF' id='tr_" + cont_cob + "'>"+
										 "<td align='center'><input type='hidden' name='nrofacturacion"+cont_cob+"' id='nrofacturacion"+cont_cob+"' value='"+nrofacturacion+"' />"+nrofacturacion+"</td>"+
										 "<td align='center'><input type='hidden' name='anioy"+cont_cob+"' id='anioy"+cont_cob+"' value='"+ $("#anioz"+idx).val()+"' />"+$("#doc"+idx).val()+"</td>"+
										 "<td align='center'><input type='hidden' name='mesy"+cont_cob+"' id='mesy"+cont_cob+"' value='"+ $("#mesz"+idx).val()+"' />"+$("#nrodoc"+idx).val()+"</td>"+
										 "<td align='center'><input type='hidden' name='codtd"+cont_cob+"' id='codtd"+cont_cob+"' value='"+tipodeuda+"' />"+$("#anio"+idx).val()+"</td>"+
										 "<td align='center'><input type='hidden' name='codctg"+cont_cob+"' id='codctg"+cont_cob+"' value='"+categoria+"' />"+$("#tipd"+idx).val()+"</td>"+
										 "<td align='center'><input type='hidden' name='detallex"+cont_cob+"' id='detallex"+cont_cob+"' value='COBRANZA - "+$("#tipd"+idx).val()+"' />COBRANZA - " + $("#tipd" + idx).val() + "</td>" +
										 "<td align='center'>" + $("#cat" + idx).val() + "</td>" +
										 "<td align='right'><input type='hidden' name='imptotal" + cont_cob + "' id='imptotal" + cont_cob + "' value='" + importe + "' />" + parseFloat(importe).toFixed(2) + "</td>" +
										 "<td><img src='../../../../images/iconos/cancel.png' align='center' width='16' height='16' style='cursor:pointer' title='Borrar Registros' onclick='remover_item_precobranza(" + cont_cob + ");' /></td>" +
										 "</tr>");
		
		//----Calcula los Totales del Facturacion---
		imigv	= igv;
		//subt	= parseFloat(importe) - parseFloat(imigv);

		subt = parseFloat(importe) - parseFloat(imigv) - parseFloat(int) - parseFloat(red);
		
		//subt = parseFloat(importe) - subt;
		
		subt 	= parseFloat($("#subtotal").val()) + parseFloat(subt) + parseFloat(int);
		imigv	= parseFloat($("#igv").val()) + parseFloat(igv); //CalculaIgv(parseFloat(subt), impigv)
		red		= parseFloat($("#redondeo").val()) + parseFloat(red); //CalcularRedondeo(parseFloat(subt) + parseFloat(imigv))
		tot		= parseFloat(subt) + parseFloat(imigv) + parseFloat(red);
		
	
		$("#subtotal").val(parseFloat(subt).toFixed(2))
		$("#redondeo").val(parseFloat(red).toFixed(2))
		$("#igv").val(parseFloat(imigv).toFixed(2))
		$("#total").val(parseFloat(tot).toFixed(2))
		
		$("#cont_cobranza").val(cont_cob)
		
	}
	function remover_item_precobranza(ind)
	{	
		//----Calcula los Totales del Facturacion---
		impt = $("#imptotal" + ind).val().replace(',', '');
	
		subt 	= (parseFloat($("#subtotal").val().replace(',', '')) + parseFloat($("#igv").val().replace(',', '')) + parseFloat($("#redondeo").val())) - parseFloat(impt)
		imigv	= CalculaIgv((parseFloat(subt) + parseFloat(red)), impigv)
		red		= CalcularRedondeo(parseFloat(subt)+parseFloat(imigv))
		tot		= parseFloat(subt) + parseFloat(imigv) + parseFloat(red)
	
		//alert(subt);
		
		$("#subtotal").val(parseFloat(subt).toFixed(2))
		$("#redondeo").val(parseFloat(red).toFixed(2))
		$("#igv").val(parseFloat(imigv).toFixed(2))
		$("#total").val(parseFloat(tot).toFixed(2))
		
		seguimiento(usuario);
		contador = parseInt(contador) - 1
		$("#tr_"+ind).remove();
	}
		
	function ValidarForm(Op)
	{
		if($("#cont_validar").val()>0)
		{
			alert("Hay Conceptos sin asignar la categoria de cobranza...asignelos antes de poder continuar")
			return false		
		}
		if($("#nroinscripcion").val()=="")
		{
			alert("Digite el Nro. de Inscripcion del Usuario")
			$("#nroinscripcion").focus();
			return false
		}
		if(contador==0)
		{
			alert('No hay Ningun Item Ingresado para Facturar')
			return false
		}
		
		GuardarPre()
		
	}
	
	function Cancelar()
	{
		location.href='index.php?Op=1';
	}
	function teclas(Evnt)
	{
		var keyPressed = (Evnt.which) ? Evnt.which : event.keyCode

		if(keyPressed==113)
		{
			BuscarUsuarios();
		}
		if(keyPressed==120)
		{
			ValidarForm();
		}
	}

</script>
<body onKeyup="teclas(event)">
<div align="center">
<form id="form1" name="form1" >
  <table width="1200" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
     
      <tbody>
	<tr>
	  <td colspan="9" class="TitDetalle">
    	<? include("../cancelacion/include/cabecera.php"); ?>  
      </td>
	</tr>

	<tr>
	  <td colspan="9" class="TitDetalle">
      	<div id="tabs">
	      <ul>
	        <li style="font-size:10px"><a href="#tabs-3">Facturacion</a></li>
	        <li style="font-size:10px"><a href="#tabs-2">Otros Servicios</a></li>
	        </ul>
	      <div id="tabs-3" style="height:140px">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td>
	              <div style="overflow:auto; height:140px">
	                <? include("../cancelacion/include/facturacion.php"); ?>
	                </div>
	              </td>
	            </tr>
	          </table>
	        </div>
	      <div id="tabs-2" style="height:140px">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td colspan="2">
	              <? include("../cancelacion/include/conceptos.php"); ?>
	              </td>
	            </tr>
	          <tr>
	            <td colspan="2">&nbsp;</td>
	            </tr>
	          <tr>
	            <td width="40%">Buscar Conceptos : 
	              <input type="text" name="buscarconcepto" id="buscarconcepto" class="inputtext" size="45" maxlength="45" value=""  /></td>
	            <td width="60%"><div id="div_valir" align="center" style="color:#F00; font-weight:bold; font-size:11px; padding:4px"><?=$mensaje?></div></td>
	            </tr>
	          </table>                	
	        </div>                
	      </div> 
      </td>
	  </tr>
      <tr style="height:5px">
	  	<td colspan="9"></td>
	  </tr>
	<tr>
	  <td colspan="9" class="TitDetalle">
    	<div style="height:160px; overflow:scroll">
        	<? include("../cancelacion/include/cobranza.php"); ?>
        </div>  
      </td>
	</tr>
	<tr>
	  <td width="92" class="TitDetalle">
      	<input type="hidden" name="cont_facturacion" id="cont_facturacion" value="0" />
	    <input type="hidden" name="cont_cobranza" id="cont_cobranza" value="0" />
        <input type="hidden" name="cont_validar" id="cont_validar" value="<?=$contvalidar?>" />  
      </td>
	  <td colspan="8" class="CampoDetalle">&nbsp;</td>
	  </tr>
    </tbody>
	  <tfoot>
	<td colspan="9" style="background-image:url(<?php echo $_SESSION['urldir'];?>images/Pie_Index.png); padding:4px; height:45px">
            <table width="100%" border="0">
                <tr>
                    <td width="11%" align="left">
                        <input name="btnAceptar" class="button" type="button" id="btnAceptar" value="Aceptar" onClick="ValidarForm();"/></td>
                    <td width="79%" align="left"><table width="100%" style="border:1px #000000 dashed"cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="background-color:#CCCCCC; padding:4px; color:#000000; font-size:12px; font-weight:bold" align="right">Sub. Total : </td>
                        <td style="padding-left:4px"><label>
                          <input type="text" name="subtotal" id="subtotal" class="inputtext" readonly size="15" maxlength="15" value="0.00" />
                        </label></td>
                        <td style="background-color:#CCCCCC; padding:4px; color:#000000; font-size:12px; font-weight:bold" align="right">Igv : </td>
                        <td style="padding-left:4px"><input type="text" name="igv" id="igv" class="inputtext" readonly size="15" maxlength="15" value="0.00" /></td>
                        <td style="background-color:#CCCCCC; padding:4px; color:#000000; font-size:12px; font-weight:bold" align="right">Redondeo : </td>
                        <td style="padding-left:4px"><input type="text" name="redondeo" id="redondeo" class="inputtext" readonly size="15" maxlength="15" value="0.00" /></td>
                        <td style="background-color:#CCCCCC; padding:4px; color:#000000; font-size:12px; font-weight:bold" align="right">Total : </td>
                        <td style="padding-left:4px">
                          <input type="text" name="total" id="total" class="inputtext" readonly size="15" maxlength="15" value="0.00" />
                        </span></td>
                        <td>&nbsp;</td>
                      </tr>
                      
                    </table></td>
                    <td width="10%" align="right">
                        <input name="btnCancelar" class="button" type="button" id="btnCancelar" value="Cancelar" onClick="Cancelar();"  /></td>
				</tr>
            </table></td>
	  <td width="1"></tfoot>
    </table>
<div id="dialog-form-amortizar" title="Amortizar Registro de Facturacion"  >
    <div id="div_amortizar">
    </div>
</div>
<div id="dialog-form-detalle-facturacion" title="Ver Detalle de Facturacion"  >
	<div id="detalle-facturacion">
    </div>
</div>
<div id="DivGlosa" title="Observaciones"  >
	<table width="100%" border="0">
       <tr>
		<td> Glosa:</td>
		<td>
			<textarea class="inputtext ui-corner-all" id="GlosaTemp" title="Glosa" cols="50" rows="5" ></textarea>
		</td>
		</tr>
	</table>
	</div>
</form>
</div>
<div id="DivCuentaCorriente" title="Ver Cuenta Corriente"  >
    <div id="detalle-CuentaCorriente">
    </div>
</div>
</body>
<script>
	$("#sustitutorio").hide()
	$("#b_prepago").hide();
	$("#nroprepago").hide();
	$("#lbl_puntos").hide();
	$("#lbl_nroprepago").hide();
	/*$("#lbleventual").hide();*/
	/*$("#eventual").hide();*/
	$("#lbl_condpago").hide();
	$("#condpago").hide();	
	$("#lbl_puntos_prepago").hide();
	
	cargar_datos_periodo_facturacion(1)
</script>
<? 
	if($op==1)
	{
		include("../cancelacion/include/from_validar.php"); 
	}
?>
<?php   CuerpoInferior();?>