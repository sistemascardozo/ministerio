<?php
	include("../../../../config.php");

	$codsuc			= $_POST["codsuc"];
	$nroinscripcion	= $_POST["nroinscripcion"];
	$nrofacturacion	= $_POST["nrofacturacion"];
	$documento		= $_POST["doc"];
	$nrodocumento	= $_POST["ndoc"];
	$td				= $_POST["td"];
	$anio			= $_POST["anio"];
	$mes			= $_POST["mes"];
	$cat			= $_POST["cat"];
?>
<table id="tbconceptos" border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" rules="all">
  <tr class="ui-widget-header" >

    <td width="50" align="center" >C&oacute;digo</td>
    <td align="center" >Concepto</td>
    <td width="80" align="center" >Importe</td>
    <td width="80" align="center" >Amortizar</td>
  </tr>
  <?php
   $total			= 0;
   $cont			= 0;
   $montRedondeo 	= 0;
   $montIgv			= 0;
   
   $sql = "SELECT d.nrofacturacion, d.codconcepto, (d.importe - (d.imppagado + d.importerebajado)) AS imptotal,
    		d.codtipodeuda,cp.descripcion as concepto,td.descripcion AS tipodeuda, d.categoria,
    		c.coddocumento, c.nrodocumento, c.serie, d.codemp, d.codsuc,d.nroinscripcion
			from facturacion.detfacturacion as d
   			inner join facturacion.conceptos as cp ON(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
   			inner join public.tipodeuda as td ON (d.codtipodeuda = td.codtipodeuda)
   			inner join facturacion.cabfacturacion c ON (d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and d.nrofacturacion=c.nrofacturacion
														and d.nroinscripcion=c.nroinscripcion)
			where d.codemp=1 and d.codsuc=? and d.nroinscripcion=? 
  			and d.nrofacturacion=? and d.codtipodeuda=? and d.categoria=? order by d.codconcepto";
	
   $consulta = $conexion->prepare($sql);
   $consulta->execute(array($codsuc,$nroinscripcion,$nrofacturacion,$td,$cat));
   $items = $consulta->fetchAll();

	foreach($items as $row)
   {
			$cat	= "";
			$cont	= $cont+1;
			$total += str_replace(',', '', $row["imptotal"]);
			
			if($row["categoria"]==0){$cat = "FACTURACION";}
			if($row["categoria"]==1){$cat = "SALDO";}
			if($row["categoria"]==2){$cat = "SALDO RFINANCIADO";}
			
  ?>
      <tr style="background-color:#FFF">
        <td align="center"><?=$row["codconcepto"]?>
        <input type="hidden" name="codconceptoy<?=$cont?>" id="codconceptoy<?=$cont?>" value="<?=$row["codconcepto"]?>" />
        <input type="hidden" name="categ<?=$cont?>" id="categ<?=$cont?>" value="<?=$row["categoria"]?>" /></td>
        <td align="left" style="padding-left:5px;"><?=$row["concepto"]?>
        <input type="hidden" id="tdeudax<?=$cont?>" name="tdeudax<?=$cont?>" value="<?=$row["codtipodeuda"]?>" /></td>
        <td align="right"><?=number_format($row["imptotal"],2)?>
        <input type="hidden" name="impx<?=$cont?>" id="impx<?=$cont?>" value="<?=$row["imptotal"]?>" /></td>
        <td align="center"><label>
          <?php
		  	$imp = number_format(0, 2);
			
			if($row["codconcepto"]==7 || $row["codconcepto"]==8)
			{
				$montRedondeo += str_replace(',', '', $row["imptotal"]);
			}	
			
			if($row["codconcepto"]==5)
			{
				$montIgv += str_replace(',', '', $row["imptotal"]);
			}
			
			$imp  = number_format($row["imptotal"], 2);
			
		  ?>
          <input type="text" name="impamort<?=$cont?>" id="impamort<?=$cont?>" class="inputtext" maxlength="15" value="<?=$imp?>" style="text-align:right; width:80px;" readonly="readonly" />
        </label></td>
      </tr>
  <?php
  	} 
   ?>
   <tr style="background-color:#FFF; color:#000; font-weight:bold; font-size:12px">
    <td align="center"><input type="hidden" name="contamortizacion" id="contamortizacion" value="<?=$cont?>" /></td>
    <td align="right">Totales:</td>
    <td align="right"><?=number_format($total, 2)?><input name="imptotalh" type="hidden" id="imptotalh" value="<?=$total?>" /></td>
    <td align="center">
    	<label id="totamortizacion">0.00</label></td>
   </tr>
</table>
<div style="height:10px">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr style="padding:4px">
    <td width="13%" align="right">Autom&aacute;tico</td>
    <td width="7%"><input type="text" name="automatico" id="automatico" class="inputtext" size="15" maxlength="15" value="<?=number_format($total,2)?>" style="text-align:right" onkeypress="return permite(event,'num');" onkeyup="CalcularAmortizacion();" /></td>
    <td width="70%">
    	<span title="Calcular Monto Automatico" onclick="CalcularAmortizacion();" class="icono-icon-add"></span>
    </td>
    <td width="10%">&nbsp;</td>
  </tr>
</table>
</div>
<script>
	
	function CalcularAmortizacion()
	{
		var mTotal		= <?=$total?>;
		var mRedodento	= <?=$montRedondeo?>;
		
		var codconcepto = 0;

		var auto = parseFloat($("#automatico").val().replace(',', '')) - parseFloat(mRedodento);
		
		if(eval(auto <= 0))
		{
			Msj($("#automatico"), 'El Monto Ingesado no es Valido')
				$("#automatico").focus().select();
			return
		}
		if(parseFloat($("#automatico").val())>parseFloat($("#imptotalh").val()))
		{
			Msj($("#automatico"),'El Monto Ingresado no puede ser Igual o Mayor al Monto Total de la Facturacion');
				$("#automatico").focus().select();
			return
		}
		
		//if (mTotal != auto)
//		{
			for(i = 1; i <= <?=$cont?>; i++)	
			{
				codconcepto = $("#codconceptoy" + i).val();
				
				if(eval(codconcepto != 7 && codconcepto != 8))
				{
					Impp = $("#impx" + i).val().replace(',', '');
					
					//alert(auto);
					
					if(eval(auto >= Impp))
					{
						$("#impamort" + i).val(parseFloat(Impp).toFixed(2));
						
						auto = parseFloat(auto) - parseFloat(Impp).toFixed(2);
					}
					else
					{
						$("#impamort" + i).val(parseFloat(auto).toFixed(2));
						auto = 0;
					}
				}
				//alert(auto);
			}
		//}
//		else
//		{
//			for(i = 1; i <= <?=$cont?>; i++)	
//			{
//				$("#impamort" + i).val(parseFloat($("#impx" + i).val()).toFixed(2));
//			}
//		}
		
		$("#totamortizacion").html(parseFloat($("#automatico").val().replace(',', '')).toFixed(2))
	}
	function entercalcularamortizacion(e)
	{
		if (VeriEnter(e) )
		{
			CalcularAmortizacion()
		}
	}
	function GuardarAmortizacion()
	{
		var mRedodento	= <?=$montRedondeo?>;
		var mIgv		= <?=$montIgv?>;
		
		var codconcepto = 0;

		var auto = parseFloat($("#automatico").val().replace(',', '')) - parseFloat(mRedodento)
		
		if(eval(auto <= 0))
		{
			Msj($("#automatico"),'El Monto Ingesado no es Valido')
				$("#automatico").focus().select();
			return false
		}
		if(parseFloat($("#automatico").val().replace(',', '')) > parseFloat($("#imptotalh").val().replace(',', '')))
		{
			Msj($("#automatico"),'El Monto Ingresado no puede ser Igual o Mayor al Monto Total de la Facturacion');
				$("#automatico").focus().select();
			return false
		}
		for(i=1; i<= <?=$cont?>; i++)	
		{
			codconcepto = $("#codconceptoy" + i).val();
			
			if(eval(codconcepto!=7 && codconcepto!=8))
			{
				if(eval(auto >= $("#impx" + i).val()))
				{
					$("#impamort" + i).val(parseFloat($("#impx" + i).val().replace(',', '')).toFixed(2))
					auto = parseFloat(auto) - parseFloat($("#impx" + i).val().replace(',', ''))
				}else{
					$("#impamort" + i).val(parseFloat(auto).toFixed(2))
					auto = 0
				}
			}
		}
		
		$("#totamortizacion").html(parseFloat($("#automatico").val().replace(',', '')).toFixed(2))

		var cad = "";
		
		for(i=1;i<=$("#contamortizacion").val();i++)
		{
			cad = cad + "codconcepto" + i + "=" + $("#codconceptoy" + i).val();
			cad = cad + "&importe" + i + "=" + $("#impamort" + i).val().replace(',', '');
			cad = cad + "&detalle" + i + "=''";
			cad = cad + "&nrofacturacion" + i + "=<?=$nrofacturacion?>";
			cad = cad + "&tdeuda" + i + "=" + $("#tdeudax" + i).val();
			cad = cad + "&categ" + i + "=" + $("#categ" + i).val() + "&";
		}

		$.ajax({
			 url:'proceso.php',
			 type:'POST',
			 async:true,
			 data: cad + 'cont=' + $("#contamortizacion").val() + '&doc=<?=$documento?>&nrodocumento=<?=$nrodocumento?>&td=<?=$td?>&anio=<?=$anio?>&mes=<?=$mes?>',
			 success:function(datos){
			 	
			 }
		});
		return true
	}
</script>