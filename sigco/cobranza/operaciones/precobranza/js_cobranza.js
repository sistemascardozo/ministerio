// JavaScript Document
$(function() {
    $("#DivCuentaCorriente").dialog({
        autoOpen: false,
        height: 600,
        width: 1000,
        modal: true,
        buttons: {
            "Cerrar": function() {
                $("#DivCuentaCorriente").dialog("close");
            }
        }
    });

    $(".focusselect").click(function() {
        $(this).select()
    });

    $("#tabs").tabs();
    $("#nrorecibo").focus();
    $("#codantiguo").focus();

    objbusqueda = $("#codantiguo");
    cSerieNun();

    $(document).bind('keydown', function(e) {
        if (e.which == 113)
        {
            return ValidarForm();
        }

        if (e.which == 114)
        {
            buscar_usuarios();

            return false;
        }
    });

    $("#DivGlosa").dialog({
        autoOpen: false,
        modal: true,
        width: 450,
        height: 250,
        resizable: false,
        show: "scale",
        hide: "scale",
        close: function() {

        },
        buttons: {
            "Guardar": function() {
                $("#Glosa").val($("#GlosaTemp").val());

                $(this).dialog("close");
            },
            "Cancelar": function() {
                $("#GlosaTemp").val('');
                $("#Glosa").val('');

                $(this).dialog("close");
            }
        }
    });

    theTable = $("#tbconceptos");

    $("#buscarconcepto").keyup(function() {
        $.uiTableFilter(theTable, this.value);
    });

    function formatResult(row)
    {
        return row[0];
    }

    function formatItem2(row)
    {
        r = "<table width='400'>";
        r = r+"<td width='20'>"+row[0]+"</td>";
        r = r+"<td width='250'>"+row[1]+"</td>";
        r = r+"<td width='220'>"+row[2]+"</td>";
        r = r+"<td width='20'>"+row[3]+"</td>";
        r = r+"</tr>"
        r = r+"</table>";

        return r;
    }

    $('#nroinscripcion1').autocomplete('../../../../autocomplet/clientes_cobranza.php', {
        autoFill: true,
        width: 500,
        selectFirst: true,
        extraParams: {Op: 0, codsuc: codsuc},
        formatItem: formatItem2,
        formatResult: formatResult,
        mustMatch: false
    }).result(function(event, item) {
        $("#nroinscripcion").val(item[0]);
        $("#nroprepago").val("");

        cargar_datos_usuario(item[0]);
        cargar_datos_facturacion(item[0]);

        if (ventana == "cobranza")
        {
            cargar_datos_cobranza(item[0]);
            //cargar_totales(item[0])
        }
    });

    function formatResultrec(row) {
        return row[5];
    }

    function formatItemrec(row) {
        r = "<table width='400' style='display:none'>";
        r = r+"<td width='20'>"+row[3]+"</td>";
        r = r+"<td width='250'>"+row[1]+"</td>";
        r = r+"<td width='220'>"+row[2]+"</td>";
        r = r+"<td width='20'>"+row[0]+"</td>";
        r = r+"</tr>"
        r = r+"</table>";
        return r;
    }

    $('#nrorecibo2').autocomplete('../../../../autocomplet/nrorecibo.php', {
        //autoFill: true,
        width: 500,
        //selectFirst: true,
        extraParams: {Campo: 'nroinscripcion', Op: 0},
        formatItem: formatItemrec,
        formatResult: formatResultrec,
        mustMatch: false
    }).result(function(event, item) {
        $("#nroinscripcion").val(item[0]);
        $("#nroprepago").val("");
        $("#nrorecibo").val(item[3]);

        cargar_datos_usuario(item[0]);
        cargar_datos_facturacion(item[0]);

        if (ventana == "cobranza")
        {
            cargar_datos_cobranza(item[0]);
            //cargar_totales(item[0])
        }
    });

    $("#dialog-form-vuelto").dialog({
        autoOpen: false,
        height: 250,
        width: 400,
        modal: true,
        buttons: [
            {
                id: "button-guardarpago",
                text: "Aceptar",
                click: function() {
                    GuardarTodo();
                }
            },
            {
                id: "button-cancelarpago",
                text: "Salir",
                click: function() {
                    $("#dialog-form-vuelto").dialog("close");
                }
            }],
        close: function() {
            $("#nrorecibo").focus();
        }
    });

    $("#dialog-form-validar-remove").dialog({
        autoOpen: false,
        height: 300,
        width: 400,
        modal: true,
        buttons: {
            "Aceptar": function() {
                validar_usuario_remove($("#user").val(), $("#contra_remove").val());
            },
            "Cancelar": function() {
                $("#dialog-form-validar-remove").dialog("close");
            }
        },
        close: function() {

        }
    });

    $("#dialog-form-update-importe").dialog({
        autoOpen: false,
        height: 300,
        width: 400,
        modal: true,
        buttons: {
            "Aceptar": function() {
                ValidarUpdateImporteOk();
            },
            "Cancelar": function() {
                $("#dialog-form-update-importe").dialog("close");
            }
        },
        close: function() {

        }
    });

    $("#dialog-form-detalle-facturacion").dialog({
        autoOpen: false,
        height: 400,
        width: 500,
        modal: true,
        buttons: {
            "Cerrar": function() {
                $("#dialog-form-detalle-facturacion").dialog("close");
            }
        },
        close: function() {

        }
    });

    $("#DivDetCredito").dialog({
        autoOpen: false,
        height: 400,
        width: 630,
        modal: true,
        buttons: {
            "Cerrar": function() {
                $("#DivDetCredito").dialog("close");
            }
        },
        close: function() {

        }
    });

});
///////////////////////7
var Car = 0;

function GuardarTodo()
{
    $("#efectivo_x").val($("#efectivo").val());
    $("#vuelto_x").val($("#vuelto").val());

    if ($("#efectivo").val().length > 9)
    {
        Msj($("#efectivo"), 'Monto Invalido');

        $("#efectivo").focus().select();

        return false;
    }

    if ($("#vueltox").val() < 0)
    {
        Msj($("#efectivo"), "El Vuelto no puede ser Menor a 0");

        return false;
    }
//AGREGAR CONCEPTO DE IGV//
    var id = parseInt($("#tbdetpagos tbody tr").length);// - 1;//MENOS LA CABECERA
    var nrofac = '';
    var sumigv = 0;
    var imptotal = 0;

    for (var i = 1; i <= id; i++)
    {
        nrofac = $("#nrofacturacion"+i).val();

        if (nrofac == 0)
        {
            sumigv += parseFloat($("#igvadd"+i).val());
            imptotal += parseFloat($("#imptotal"+i).val());
        }
    }

    sumigv = Round(sumigv, 2);
    imptotal = Round(imptotal, 2);

    var cont_cob = parseInt(id)+1;

    if (sumigv > 0)
    {
        $("#tbdetpagos tbody").append("<tr style='background-color:#FFF' id='tr_"+cont_cob+"'>" +
                "<td align='center'><input type='hidden' name='nrofacturacion"+cont_cob+"' id='nrofacturacion"+cont_cob+"' value='0' /></td>" +
                "<td align='center'><input type='hidden' name='anioy"+cont_cob+"' id='anioy"+cont_cob+"' value='"+$("#anio").val()+"' /></td>" +
                "<td align='center'><input type='hidden' name='codcategoria"+cont_cob+"' id='codcategoria"+cont_cob+"' value='4' /></td>" +
                "<td align='center'><input type='hidden' name='mesy"+cont_cob+"' id='mesy"+cont_cob+"' value='"+$("#mes").val()+"' /></td>" +
                "<td align='center'><input type='hidden' name='codtipodeuda"+cont_cob+"' id='codtipodeuda"+cont_cob+"' value='5' /></td>" +
                "<td align='center'><input type='hidden' name='codconceptox"+cont_cob+"' id='codconceptox"+cont_cob+"' value='5' /></td>" +
                "<td align='center'><input type='hidden' name='detalle"+cont_cob+"' id='detalle"+cont_cob+"' value='PAGO POR IGV' /></td>" +
                "<td align='right'><input type='hidden' name='npresupuesto"+cont_cob+"' id='npresupuesto"+cont_cob+"' value='0' />" +
                "<input type='hidden' name='igvadd"+cont_cob+"' id='igvadd"+cont_cob+"' value='"+sumigv+"' />" +
                "<input type='hidden' name='imptotal"+cont_cob+"' id='imptotal"+cont_cob+"' value='"+sumigv+"' /></td>" +
                "<td></td>" +
                "</tr>");

        var redondeo = CalcularRedondeo(parseFloat(imptotal)+parseFloat(sumigv));
        redondeo = Round(redondeo, 2);

        cont_cob++;

        $("#tbdetpagos tbody").append("<tr style='background-color:#FFF' id='tr_"+cont_cob+"'>" +
                "<td align='center'><input type='hidden' name='nrofacturacion"+cont_cob+"' id='nrofacturacion"+cont_cob+"' value='0' /></td>" +
                "<td align='center'><input type='hidden' name='anioy"+cont_cob+"' id='anioy"+cont_cob+"' value='"+$("#anio").val()+"' /></td>" +
                "<td align='center'><input type='hidden' name='codcategoria"+cont_cob+"' id='codcategoria"+cont_cob+"' value='4' /></td>" +
                "<td align='center'><input type='hidden' name='mesy"+cont_cob+"' id='mesy"+cont_cob+"' value='"+$("#mes").val()+"' /></td>" +
                "<td align='center'><input type='hidden' name='codtipodeuda"+cont_cob+"' id='codtipodeuda"+cont_cob+"' value='5' /></td>" +
                "<td align='center'><input type='hidden' name='codconceptox"+cont_cob+"' id='codconceptox"+cont_cob+"' value='8' /></td>" +
                "<td align='center'><input type='hidden' name='detalle"+cont_cob+"' id='detalle"+cont_cob+"' value='PAGO POR REDONDEO' /></td>" +
                "<td align='right'><input type='hidden' name='npresupuesto"+cont_cob+"' id='npresupuesto"+cont_cob+"' value='0' />" +
                "<input type='hidden' name='igvadd"+cont_cob+"' id='igvadd"+cont_cob+"' value='"+redondeo+"' />" +
                "<input type='hidden' name='imptotal"+cont_cob+"' id='imptotal"+cont_cob+"' value='"+redondeo+"' /></td>" +
                "<td></td>" +
                "</tr>");

        $("#cont_cobranza").val(cont_cob);
    }
//AGREGAR CONCEPTO DE IGV//

// Tipo de Documento

    var tipo_documento = $("#documento").val();

    //document.form1.submit();

    $("#dialog-form-vuelto").dialog('close');
    window.parent.blokear_pantalla("Guardando :D");

    $.ajax({
        url: urldir+'sigco/cobranza/operaciones/cancelacion/Guardar.php',
        type: 'POST',
        async: true,
        data: $('#form1').serialize(),
        dataType: 'json',
        success: function(data)
        {
            document.getElementById("blokea").style.display = "none";
            document.getElementById("frm_loading").style.display = "none";

            OperMensaje(data.res);

            if (data.res == 1)
            {
                if ($("#condpago").val() == 0)
                {
                    if (data.esservicio == 1 && $("#eventualtext").val() == 0 && data.concredito != 1)
                    {
                        if ($("#nroinspeccion").val() == 0 && $("#nrocargo").val() == '')
                        {
                            //        		if(confirm('Desea Generar Solicitud?'))
                            // {
                            $.ajax({
                                url: urldir+'sigco/cobranza/operaciones/cancelacion/consulta/GenerarSolicitud.php',
                                type: 'POST',
                                async: true,
                                data: 'tipo=2&pago='+data.pago+'&codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val(),
                                dataType: 'json',
                                success: function(datos)
                                {
                                    if (datos.res == 1)
                                    {
                                        if (confirm('Desea Imprimir la Solicitud?'))
                                        {
                                            AbrirPopupImpresion(urldir+"sigco/catastro/operaciones/solicitud/imprimir.php?nrosolicitud="+datos.nroinspeccion+'&codsuc='+codsuc, 800, 600);
                                        }
                                        // else{

                                        if (confirm("Desea Imprimir el Recibo?"))
                                        {
                                            AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_pago.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                        }
                                        // }
                                    }
                                    // ()

                                    if (tipo_documento == '26') {
                                        AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                    }

                                    location.reload(true);
                                }
                            });

                            // }
                            // else
                            // {
                            // 	if(data.esservicio==1)
                            //                	{
                            //                   		if(confirm("Desea Imprimir el Recibo?"))
                            //                		   	AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_pago.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc,800,600)
                            //                	}
                            //                	location.reload(true);
                            // }

                        }
                        else
                        {
                            if ($("#nrocargo").val() == '')
                            {
                                if (confirm('Desea Imprimir la Solicitud'))
                                {
                                    AbrirPopupImpresion(urldir+"sigco/catastro/operaciones/solicitud/imprimir.php?nrosolicitud="+$("#nroinspeccion").val()+'&codsuc='+codsuc, 800, 600);
                                }
                            }
                            if (data.esservicio == 1)
                            {
                                if (confirm("Desea Imprimir el Recibo?"))
                                {
                                    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_pago.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                }
                            }

                            if (tipo_documento == '26') {
                                AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                            }


                            location.reload(true);
                        }
                    }
                    else
                    {
                        if ($("#eventualtext").val() == 0) //DESCHEKEADO
                        {
                            if (data.esservicio == 1)
                            {
                                if (confirm("Desea Imprimir el Recibo?"))
                                {
                                    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php?codpago="+data.nroprepago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                }
                            }
                            if (data.esservicio == 1)
                            {
                                if (confirm("Desea Imprimir la Nota de Pago?"))
                                {
                                    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_pago.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                }
                            }
                            if (data.concredito == 1)
                            {
                                if (confirm("Desea Imprimir el Convenio?"))
                                {
                                    AbrirPopupImpresion(urldir+"sigco/facturacion/operaciones/creditos/generar/imprimirc.php?Id="+data.nrocredito+'&codsuc='+codsuc, 800, 600);
                                }
                            }

                            if (Car == 0)
                            {

                                if (tipo_documento == '26') {
                                    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                }

                            }

                            location.reload(true);
                        }
                        else //EVENTUAL
                        {

                            //     		if(confirm('Desea Generar Solicitud?'))
                            // {
                            $.ajax({
                                url: urldir+'sigco/cobranza/operaciones/cancelacion/consulta/GenerarSolicitud.php',
                                type: 'POST',
                                async: true,
                                data: 'tipo=2&pago='+data.pago+'&codsuc='+codsuc+'&nroinscripcion=0',
                                dataType: 'json',
                                success: function(datos)
                                {
                                    if (datos.res == 1)
                                    {
                                        if (confirm('Desea Imprimir la Solicitud?'))
                                        {
                                            AbrirPopupImpresion(urldir+"sigco/catastro/operaciones/solicitud/imprimir.php?nrosolicitud="+datos.nroinspeccion+'&codsuc='+codsuc, 800, 600);
                                        }
                                        // else{

                                        if (confirm("Desea Imprimir el Recibo?"))
                                        {
                                            AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_pago.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                        }
                                        // }
                                    }

                                    if (tipo_documento == '26') {
                                        AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                    }

                                    location.reload(true);
                                }

                            });

                            // }
                            // else
                            // {
                            // 		location.reload(true);
                            // }
                        }
                    }
                }
                else
                {
                    if ($("#nroinspeccion").val() == '' || $("#nroinspeccion").val() == 0)
                    {
                        if (confirm('Desea Generar Solicitud?'))
                        {
                            $.ajax({
                                url: urldir+'sigco/cobranza/operaciones/cancelacion/consulta/GenerarSolicitud.php',
                                type: 'POST',
                                async: true,
                                data: 'tipo=2&pago=0&codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val()+'&nrocargo='+data.nrocredito,
                                dataType: 'json',
                                success: function(datos)
                                {
                                    if (datos.res == 1)
                                    {
                                        AbrirPopupImpresion(urldir+"sigco/catastro/operaciones/solicitud/imprimir.php?nrosolicitud="+datos.nroinspeccion+'&codsuc='+codsuc, 800, 600);
                                    }

                                    if ($('#carx').val() != '')
                                    {
                                        //AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                    }

                                    location.reload(true);
                                }
                            });

                        }
                        else
                        {
                            if ($('#carx').val() != '')
                            {
                                if (tipo_documento == '26') {
                                    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                }
                            }

                            location.reload(true);
                        }
                    }
                    else
                    {
                        if (tipo_documento == '26') {
                            AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                        }
                        location.reload(true);
                    }
                }
            }

            if (data.res == 2)
            {
                alert("Error en: "+data.mensaje);
            }
            if (data.res == 0)
            {
                alert("Concepto sin Asignar: "+data.Concepto);
            }
        }
    });
}


function ValidarNroInscripcion(e)
{
    if (VeriEnter(e))
    {
        if ($("#nroinscripcion").val() != "")
        {
            var Id = $("#nroinscripcion").val();

            $("#nroprepago").val("");
            cargar_datos_usuario(Id);
            cargar_datos_facturacion(Id);

            if (ventana == "cobranza")
            {
                cargar_datos_cobranza(Id);
            }
        }
    }
}

function ValidarCodAntiguo(e)
{
    if (VeriEnter(e))
    {
        if ($("#codantiguo").val() != "")
        {
            var Id = $("#codantiguo").val();

            $("#nroprepago").val("");

            cargar_datos_usuario_anti(Id);
        }
    }
}

function parpadeo()
{
    if (document.getElementById("div_estadoservicio").style.color == "#ff0000")
    {
        document.getElementById("div_estadoservicio").style.color = "#000000";
    }
    else
    {
        document.getElementById("div_estadoservicio").style.color = "#ff0000";
    }
    //setTimeout("parpadeo()", 1000);
}

setTimeout("parpadeo()", 1000);

function buscar_usuarios()
{
    if ($("#eventualtext").val() == 1)
    {
        alert('No puede Buscar Usuarios...Por que va esta trabajando con usuario eventual');

        return;
    }

    object = "usuario";

    AbrirPopupBusqueda('../../../catastro/operaciones/actualizacion/?Op=5', 1150, 450);
}

function Recibir(id)
{
    if (object == "usuario")
    {
        $("#nroinscripcion").val(id);
        $("#nrorecibo").val(id);
        $("#nroprepago").val("");

        cargar_datos_usuario(id);
        cargar_datos_facturacion(id);

        if (ventana == "cobranza")
        {
            cargar_datos_cobranza(id);
        }
    }

    if (object == "presupuesto")
    {
        //datos_presupuesto(id)
        datos_expediente(id);
    }

    if (object == "solicitudinspeccion")
    {
        datos_solicitudinspeccion(id);
    }
}

function datos_solicitudinspeccion(nroinspeccion)
{
    $.ajax({
        url: urldir+'ajax/solicitudinspeccion.php',
        type: 'POST',
        async: true,
        dataType: 'json',
        data: 'codsuc='+codsuc+'&nroinspeccion='+nroinspeccion,
        success: function(datos) {
            $("#nuevocliente").val(datos.nuevocliente)

            if (datos.nuevocliente == 0)
            {
                $("#nroinscripcion").val(datos.nroinscripcion);

                cargar_datos_usuario(datos.nroinscripcion);
            }
            else
            {
                $("#cliente").val(datos.propietario);
                $("#direccion").val(datos.direccion);
                $("#nroinscripcion").val(datos.nroinscripcion);
            }

            $("#nroinspeccion").val(datos.nroinspeccion);

            datos_detsolicitudinspeccion(datos.nroinspeccion);
        }
    })
}

function datos_detsolicitudinspeccion(nroinspeccion)
{
    $.ajax({
        url: urldir+'ajax/detsolicitudinspeccion.php',
        type: 'POST',
        async: true,
        dataType: 'json',
        data: 'codsuc='+codsuc+'&nroinspeccion='+nroinspeccion,
        success: function(datos) {
            $("#tbdetpagos tbody").html(datos.tr);

            red = CalcularRedondeo(parseFloat(datos.subtotaltotal)+parseFloat(datos.igvtotal));

            $("#subtotal").val(parseFloat(datos.subtotaltotal).toFixed(2));
            $("#redondeo").val(parseFloat(red).toFixed(2));
            $("#igv").val(parseFloat(datos.igvtotal).toFixed(2));
            $("#total").val(parseFloat(datos.importetotal).toFixed(2));

            contador = datos.cont_cobranza;
            $("#cont_cobranza").val(datos.cont_cobranza);
        }
    })
}

function cargar_datos_usuario(nroinscripcion)
{
    $.ajax({
        url: urldir+'ajax/clientes.php',
        type: 'POST',
        async: true,
        dataType: 'json',
        data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion+'&nrorecibo='+$('#nrorecibo').val(),
        success: function(datos) {
            //var r = datos.split("|");
            codestadoservicio = datos.codestadoservicio;
            $("#cliente").val(datos.propietario);
            $("#direccion").val(datos.direccion);
            $("#docidentidad").val(datos.tdabreviado+" "+datos.nrodocumento);
            $("#div_estadoservicio").html(datos.estadoservicio);
            $("#codantiguo").val(datos.codantiguo);
            cargar_datos_periodo_facturacion(datos.codciclo);
            $("#codciclo").val(datos.codciclo);
        }
    })
}

function cargar_datos_usuario_anti(codantiguo)
{
    $.ajax({
        url: urldir+'ajax/clientes_antiguo.php',
        type: 'POST',
        async: true,
        dataType: 'json',
        data: 'codsuc='+codsuc+'&codantiguo='+codantiguo+'&nrorecibo='+$('#nrorecibo').val(),
        success: function(datos) {
            //var r=datos.split("|")
            if (datos.nroinscripcion == null)
            {
                Msj($("#codantiguo"), "No existe Usuario");
                return false;
            }

            $("#nroinscripcion").val(datos.nroinscripcion);
            $("#cliente").val(datos.propietario);
            $("#direccion").val(datos.direccion);
            $("#docidentidad").val(datos.tdabreviado+" "+datos.nrodocumento);
            $("#div_estadoservicio").html(datos.estadoservicio);
            $("#codciclo").val(datos.codciclo);

            cargar_datos_periodo_facturacion(datos.codciclo);
            cargar_datos_facturacion(datos.nroinscripcion);

            if (ventana == "cobranza")
            {
                codestadoservicio = datos.codestadoservicio;
                cargar_datos_cobranza(datos.nroinscripcion);
            }
        }
    })
}

var codestadoservicio = 0;

function cargar_datos_facturacion(nroinscripcion)
{
    $.ajax({
        url: urldir+'sigco/cobranza/operaciones/cancelacion/ajax/facturacion.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion+'&nrorecibo='+$('#nrorecibo').val(),
        success: function(datos) {
            var r = datos.split("|");

            if ($('#solocolateral').attr("checked") != "checked")
            {
                QuitaRow_Facturacion();

                $("#tbfacturacion tbody").html(r[0]);
                $("#cont_facturacion").val(r[1]);
                cont_fact = r[1];

                if (r[1] == 0)
                {
                    Msj($("#cliente"), "Cliente no presente deuda");
                    $("#div_estadoservicio").html($("#div_estadoservicio").html()+' - SIN DEUDA');
                    $("#nrorecibo").focus().select();
                }
            }
        }
    })
}

var DiasInteres = 0
var ImporteInteres = 0;

function cargar_datos_cobranza(nroinscripcion)
{
    $.ajax({
        url: urldir+'sigco/cobranza/operaciones/cancelacion/ajax/cobranza.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion+'&nrorecibo='+$('#nrorecibo').val()+'&codestadoservicio='+codestadoservicio+'&fdescarga='+$("#fechareg").val()+'&TasaInteres='+TasaInteres,
        success: function(datos) {
            var r = datos.split("|");

            if ($("#condpago").val() == 0)
            {
                if ($('#solocolateral').attr("checked") != "checked")
                {
                    QuitaRow_Cobranza();

                    $("#tbdetpagos tbody").html(r[0]);
                    $("#cont_cobranza").val(r[1]);

                    DiasInteres = r[2];
                    ImporteInteres = r[3];
                    cont_cob = r[1];
                    contador = r[1];

                    mesesD = r[4];

                    cargar_totales(nroinscripcion);

                    //alert(meses+' '+DiasInteres);

                    $('#button-guardarpago').show();

                    if (Car > 0)
                    {
                        txt = $("#DivIntMor").html()+'<br><br><center>¡PAGO SOLO EN OFICINA PRINCIPAL!</center>';

                        if (mesesD > 2)
                        {
                            $('#button-guardarpago').hide();

                            $("#DivIntMor").html(txt);
                        }
                        else
                        {
                            if (mesesD == 2 && DiasInteres > 0)
                            {
                                $('#button-guardarpago').hide();

                                $("#DivIntMor").html(txt);
                            }
                            if (mesesD == 1 && DiasInteres > 28)
                            {
                                $('#button-guardarpago').hide();

                                $("#DivIntMor").html(txt);
                            }
                        }
                    }
                }
            }
        }
    })
}

function QuitaRow_Cobranza()
{
    $("#cont_cobranza").val(0);
    cont_cob = 0;
    contador = 0;
}

function QuitaRow_Facturacion()
{
    $("#cont_facturacion").val(0);
    cont_fact = 0;
}

function agregar_concepto(idx)
{
    if ($("#nroprepago").val() != "" && $("#nroprepago").val() != 0)
    {
        Msj($("#nroprepago"), "No se puede agregar, es un Prepago");

        return false;
    }

    var codconcepto = $("#codconcepto"+idx).val();
    var concepto = $("#concepto"+idx).val();

    // Para el importe
    var importe = parseFloat($("#importe"+idx).val());
    //importe          = importe+parseFloat(CalcularRedondeo($("#importe"+idx).val()));

    var tipopresupuesto = $("#tippresupuesto"+idx).val();
    var afecto_igv = $("#afecto_igv"+idx).val();
    var afecto_igv_pre = $("#afecto_igv_pre"+idx).val();

    if ($("#nroinscripcion").val() == "" && codconcepto != 15 && codconcepto != 16)
    {
        Msj(objbusqueda, "Seleccione el Usuario que hara la Cobranza");

        return false;
    }

    if (importe == 0.00 || importe == 0 || importe == "")
    {
        alert("Ingrese el Importe del Concepto");

        return;
    }

    if (tipopresupuesto == 1)
    {
        if ($("#nropresupuesto"+idx).val() == "")
        {
            alert("Ingrese el Nro. de Presupuesto");

            return;
        }
    }

    for (var i = 1; i <= $("#cont_cobranza").val(); i++)
    {
        try {
            if (codconcepto == $("#codconceptox"+i).val())
            {
                alert("El Concepto ya Fue Agregado");

                return false;
            }
        }
        catch (e) {
        }
    }

    cont_cob++

    var npresupuesto = $("#nropresupuesto"+idx).val();

    if (npresupuesto == "")
    {
        npresupuesto = 0;
    }

    var igv = 0;

    if (afecto_igv_pre == 1)//YA ESTA AGREGADO EL IGV EN EL PRESUPUESTO
    {
        igv = CalculaIgv((parseFloat(importe)), impigv);
        importe = parseFloat(importe) - igv;
    }
    else
    {
        if (afecto_igv == 1) //SI EL CONCEPTO TIENE IGV
        {
            igv = CalculaIgv((parseFloat(importe)), impigv);
        }
    }

    if (Trim($("#tbdetpagos tbody").html()) == '')
    {
        cont_cob = 1;
    }

    $("#tbdetpagos tbody").append("<tr style='background-color:#FFF' id='tr_"+cont_cob+"'>" +
            "<td align='center'><input type='hidden' name='nrofacturacion"+cont_cob+"' id='nrofacturacion"+cont_cob+"' value='0' />0</td>" +
            "<td align='center'><input type='hidden' name='anioy"+cont_cob+"' id='anioy"+cont_cob+"' value='"+$("#anio").val()+"' />RSP</td>" +
            "<td align='center'><input type='hidden' name='codcategoria"+cont_cob+"' id='codcategoria"+cont_cob+"' value='4' />0</td>" +
            "<td align='center'><input type='hidden' name='mesy"+cont_cob+"' id='mesy"+cont_cob+"' value='"+$("#mes").val()+"' />"+$("#anio").val()+" - "+meses($("#mes").val())+"</td>" +
            "<td align='center'><input type='hidden' name='codtipodeuda"+cont_cob+"' id='codtipodeuda"+cont_cob+"' value='5' />COBRANZA DIRECTAMENTE EN CAJA</td>" +
            "<td align='center'><input type='hidden' name='codconceptox"+cont_cob+"' id='codconceptox"+cont_cob+"' value='"+codconcepto+"' />PAGO POR "+concepto+"</td>" +
            "<td align='center'><input type='hidden' name='detalle"+cont_cob+"' id='detalle"+cont_cob+"' value='PAGO POR "+concepto+"' />PRESTACION DE SERVICIOS EN CAJA</td>" +
            "<td align='right'><input type='hidden' name='npresupuesto"+cont_cob+"' id='npresupuesto"+cont_cob+"' value='"+npresupuesto+"' />" +
            "<input type='hidden' name='igvadd"+cont_cob+"' id='igvadd"+cont_cob+"' value='"+igv+"' />" +
            "<input type='hidden' name='imptotal"+cont_cob+"' id='imptotal"+cont_cob+"' value='"+importe+"' />"+parseFloat(importe).toFixed(2)+"</td>" +
            "<td><span onclick='open_validar_remover_item("+cont_cob+");' title='Quitar Registro' class='icono-icon-trash'></span> </td>" +
            "</tr>"
            )

    //----Calcula los Totales del Facturacion---
    subt = parseFloat($("#subtotal").val())+parseFloat(importe);
    imigv = parseFloat($("#igv").val())+parseFloat(igv);
    red = CalcularRedondeo(parseFloat(subt)+parseFloat(imigv));
    tot = parseFloat(subt)+parseFloat(imigv)+parseFloat(red);

    $("#subtotal").val(parseFloat(subt).toFixed(2))
    $("#redondeo").val(parseFloat(red).toFixed(2))
    $("#igv").val(parseFloat(imigv).toFixed(2))
    $("#total").val(parseFloat(tot).toFixed(2))

    contador++;

    $("#cont_cobranza").val(cont_cob);
}

function agregar_concepto_edit(idx)
{
    var codconcepto = $("#codconcepto"+idx).val();
    var concepto = $("#concepto"+idx).val();
    var importe = $("#importe"+idx).val();
    var tipopresupuesto = $("#tippresupuesto"+idx).val()
    var afecto_igv = $("#afecto_igv"+idx).val()
    var afecto_igv_pre = $("#afecto_igv_pre"+idx).val()

    if ($("#nroinscripcion").val() == "" && codconcepto != 15 && codconcepto != 16)
    {
        Msj(objbusqueda, "Seleccione el Usuario que hara la Cobranza")
        return false
    }
    //importe = prompt("Confirmar Importe del Concepto: "+concepto, importe);

    /*while (importe==0.00 || importe==0 || importe=="")
     {
     importe = prompt("Confirmar Importe del Concepto: "+concepto, importe);
     }*/

    if (importe == 0.00 || importe == 0 || importe == "" || importe == null)
        return false
    /*if(importe==0.00 || importe==0 || importe=="")
     {
     alert("Ingrese el Importe del Concepto")
     return;
     }*/
    if (tipopresupuesto == 1)
    {
        if ($("#nropresupuesto"+idx).val() == "")
        {
            alert("Ingrese el Nro. de Presupuesto");
            return
        }
    }
    for (var i = 1; i <= $("#cont_cobranza").val(); i++)
    {
        try {
            if (codconcepto == $("#codconceptox"+i).val())
            {
                alert("El Concepto ya Fue Agregado")
                return false
            }
        } catch (e) {
        }
    }
    cont_cob++

    var npresupuesto = $("#nropresupuesto"+idx).val();
    if (npresupuesto == "") {
        npresupuesto = 0;
    }
    var igv = 0
    if (afecto_igv_pre == 1)//YA ESTA AGREGADO EL IGV EN EL PRESUPUESTO
    {
        //igv=0
        igv = CalculaIgv((parseFloat(importe)), impigv)
        importe = parseFloat(importe) - igv
    }
    else
    {
        if (afecto_igv == 1) //SI EL CONCEPTO TIENE IGV
        {
            igv = CalculaIgv((parseFloat(importe)), impigv)
        }
    }
    $("#tbdetpagos tbody").append("<tr style='background-color:#FFF' id='tr_"+cont_cob+"'>" +
            "<td align='center'><input type='hidden' name='nrofacturacion"+cont_cob+"' id='nrofacturacion"+cont_cob+"' value='0' />0</td>" +
            "<td align='center'><input type='hidden' name='anioy"+cont_cob+"' id='anioy"+cont_cob+"' value='"+$("#anio").val()+"' />RSP</td>" +
            "<td align='center'><input type='hidden' name='codcategoria"+cont_cob+"' id='codcategoria"+cont_cob+"' value='4' />0</td>" +
            "<td align='center'><input type='hidden' name='mesy"+cont_cob+"' id='mesy"+cont_cob+"' value='"+$("#mes").val()+"' />"+$("#anio").val()+" - "+meses($("#mes").val())+"</td>" +
            "<td align='center'><input type='hidden' name='codtipodeuda"+cont_cob+"' id='codtipodeuda"+cont_cob+"' value='5' />COBRANZA DIRECTAMENTE EN CAJA</td>" +
            "<td align='center'><input type='hidden' name='codconceptox"+cont_cob+"' id='codconceptox"+cont_cob+"' value='"+codconcepto+"' />PAGO POR "+concepto+"</td>" +
            "<td align='center'><input type='hidden' name='detalle"+cont_cob+"' id='detalle"+cont_cob+"' value='PAGO POR "+concepto+"' />PRESTACION DE SERVICIOS EN CAJA</td>" +
            "<td align='right'><input type='hidden' name='npresupuesto"+cont_cob+"' id='npresupuesto"+cont_cob+"' value='"+npresupuesto+"' />" +
            "<input type='hidden' name='igvadd"+cont_cob+"' id='igvadd"+cont_cob+"' value='"+igv+"' />" +
            "<input type='hidden' name='imptotal"+cont_cob+"' id='imptotal"+cont_cob+"' value='"+importe+"' /><label id='lblimptotal"+cont_cob+"' >"+parseFloat(importe).toFixed(2)+"<label></td>" +
            "<td><span onclick='open_validar_remover_item("+cont_cob+");' title='Quitar Registro' class='icono-icon-trash'></span> " +
            "<span onclick='open_update_importe_item("+cont_cob+");' title='Modificar Importe' class='icono-icon-dinero'></span> </td>" +
            "</tr>"
            )

    //----Calcula los Totales del Facturacion---
    subt = parseFloat($("#subtotal").val())+parseFloat(importe)
    //imigv	= CalculaIgv((parseFloat(subt)+parseFloat(red)),impigv)
    imigv = parseFloat($("#igv").val())+parseFloat(igv)
    red = CalcularRedondeo(parseFloat(subt)+parseFloat(imigv))
    tot = parseFloat(subt)+parseFloat(imigv)+parseFloat(red)

    $("#subtotal").val(parseFloat(subt).toFixed(2))
    $("#redondeo").val(parseFloat(red).toFixed(2))
    $("#igv").val(parseFloat(imigv).toFixed(2))
    $("#total").val(parseFloat(tot).toFixed(2))

    contador++;
    $("#cont_cobranza").val(cont_cob)
}

function cargar_datos_periodo_facturacion(codciclo)
{
    $.ajax({
        url: urldir+'ajax/periodo_facturacion.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&codciclo='+codciclo,
        success: function(datos) {
            var r = datos.split("|")
            $("#codciclo").val(codciclo)
            $("#anio").val(r[1])
            $("#mes").val(r[3])
            $("#nrofacturacionactual").val(r[0])
        }
    })
}

function agregar_facturacion(idx)
{
    var nrofacturacion = $("#nrofact"+idx).val();
    var importe = $("#imp"+idx).val();

    for (i = 1; i <= $("#cont_cobranza").val(); i++)
    {

        try
        {
            if ($("#nrofacturacion"+i).val() == nrofacturacion)
            {
                alert('El Nro. Facturacion ya Fue Agregado en la Lista')
                return
            }
        } catch (exp)
        {

        }
    }
    cont_cob++
    contador++
    $("#tbdetpagos tbody").append("<tr style='background-color:#FFF' id='tr_"+cont_cob+"'>" +
            "<td align='center'><input type='hidden' name='nrofacturacion"+cont_cob+"' id='nrofacturacion"+cont_cob+"' value='"+nrofacturacion+"' />"+nrofacturacion+"</td>" +
            "<td align='center'><input type='hidden' name='anioy"+cont_cob+"' id='anioy"+cont_cob+"' value='"+$("#anioz"+idx).val()+"' />"+$("#doc"+idx).val()+"</td>" +
            "<td align='center'><input type='hidden' name='mesy"+cont_cob+"' id='mesy"+cont_cob+"' value='"+$("#mesz"+idx).val()+"' />"+$("#nrodoc"+idx).val()+"</td>" +
            "<td align='center'>"+$("#anio"+idx).val()+"</td>" +
            "<td align='center'>"+$("#tipd"+idx).val()+"</td>" +
            "<td align='center'><input type='hidden' name='detallex"+cont_cob+"' id='detallex"+cont_cob+"' value='COBRANZA - "+$("#tipd"+idx).val()+"' />COBRANZA - "+$("#tipd"+idx).val()+"</td>" +
            "<td align='center'>"+$("#cat"+idx).val()+"</td>" +
            "<td align='right'><input type='hidden' name='imptotal"+cont_cob+"' id='imptotal"+cont_cob+"' value='"+importe+"' />"+parseFloat(importe).toFixed(2)+"</td>" +
            "<td><span onclick='open_validar_remover_item("+cont_cob+");' title='Quitar Registro' class='icono-icon-trash'></span></td>" +
            "</tr>");

    //----Calcula los Totales del Facturacion---
    subt = parseFloat($("#subtotal").val())+parseFloat(importe)
    imigv = CalculaIgv((parseFloat(subt)+parseFloat(red)), impigv)
    red = CalcularRedondeo(parseFloat(subt)+parseFloat(imigv))
    tot = parseFloat(subt)+parseFloat(imigv)+parseFloat(red)

    $("#subtotal").val(parseFloat(subt).toFixed(2))
    $("#redondeo").val(parseFloat(red).toFixed(2))
    $("#igv").val(parseFloat(imigv).toFixed(2))
    $("#total").val(parseFloat(tot).toFixed(2))

    $("#cont_cobranza").val(cont_cob)

}

function cargar_totales(nroinscripcion)
{
    $("#subtotal").val('0.00')
    $("#igv").val('0.00')
    $("#redondeo").val('0.00')
    $("#total").val('0.00')

    var id = parseInt($("#tbdetpagos tbody tr").length);// - 1;//MENOS LA CABECERA

    if (id == 0) {
        return false;
    }

    $.ajax({
        url: urldir+'sigco/cobranza/operaciones/cancelacion/ajax/cargar_totales.php',
        type: 'POST',
        async: false,
        data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion,
        success: function(datos) {
            var r = datos.split("|");

            $("#subtotal").val(r[0]);
            $("#igv").val(r[2]);
            $("#redondeo").val(r[1]);
            $("#total").val(r[3]);

            if ($("#condpago").val() == 0 && DiasInteres == 0)
            {
                return ValidarForm();
            }
            else
            {
                if (DiasInteres > 0)
                {
                    //var objc = $("#TbC_887");
                    //var orden = $(objc).data('orden')
                    //$("#importe"+orden).val((parseFloat(ImporteInteres)*parseInt(DiasInteres)*(parseFloat(TasaInteres)/30)).toFixed(1));
                    //agregar_concepto_edit(orden)
                    var txt = 'Interes por deuda a la fecha : S/.'+parseFloat(ImporteInteres).toFixed(2);

                    $("#DivIntMor").html(txt);
                    //alert(ImporteInteres);
                    return ValidarForm();
                }
            }
        }
    })
}

function open_validar_remover_item(idx)
{
    index = idx
    //$("#user").val(0)

    $("#dialog-form-validar-remove").dialog("open");
    $("#contra_remove").val("").focus();
}
function open_update_importe_item(idx)
{
    $("#ItemUpdateImporte").val(idx);
    $("#LblUpdateImporteColateral").html($("#detalle" + idx).val());
    $("#UpdateImporteColateralImporte").val(parseFloat($("#imptotal" + idx).val()).toFixed(2));
    $("#UpdateImporteColateralOriginal").val($("#imptotal" + idx).val());
    $("#dialog-form-update-importe").dialog("open");
    $("#UpdateImporteColateralImporte").focus().select();
	//alert($("#imptotal" + idx).val());
}

function ValidarUserD(e)
{
    if (VeriEnter(e))
    {
        validar_usuario_remove($("#user").val(), $("#contra_remove").val());

        e.returnValue = false;
    }
}

function validar_usuario_remove(user, contra)
{//alert(user)
    if (user == 0)
    {
        $("#mensaje_remover").html("Seleccione el Login del Usuario")
        return false
    }
    if (contra == "")
    {
        $("#mensaje_remover").html("Digite la Contrase&ntilde;a del Usuario")
        return false
    }

    $.ajax({
        url: urldir+'sigco/cobranza/operaciones/cancelacion/ajax/validar_usuario_remove.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&user='+user+"&contra="+contra,
        success: function(datos) {
            var r = datos.split("|");

            if (r[0] == 1)
            {
                $("#mensaje_remover").html(r[1]);
                $("#contra_remove").focus().select();
            }
            else
            {
                remover_item();
                $("#dialog-form-validar-remove").dialog("close");
            }
        }
    })
}
function str_replace(cadena, cambia_esto, por_esto)
{
    if (cadena == undefined || cadena == '')
        return ""
    return cadena.split(cambia_esto).join(por_esto);
}
var FacturacionesDel = '';
function remover_item()
{
    //----Calcula los Totales del Facturacion---
    impt = $("#imptotal"+index).val()
    //alert(impt)
    impt = str_replace(impt, ',', '')
    //subt 	= (parseFloat($("#subtotal").val())+parseFloat($("#redondeo").val())) - parseFloat(impt)
    subt = parseFloat(str_replace($("#subtotal").val(), ',', ''))+parseFloat(str_replace($("#redondeo").val(), ',', '')) - parseFloat(impt)
    subt = parseFloat(subt).toFixed(2);
    //alert(subt)
    //alert(subt+'='+$("#subtotal").val()+'+'+$("#redondeo").val()+'-'+impt)
    imigv = CalculaIgv((parseFloat(subt)+parseFloat(red)), impigv)

    red = CalcularRedondeo(parseFloat(subt)+parseFloat(imigv))
    tot = parseFloat(subt)+parseFloat(imigv)+parseFloat(red)

    $("#subtotal").val(parseFloat(subt).toFixed(2))
    $("#redondeo").val(parseFloat(red).toFixed(2))
    $("#igv").val(parseFloat(imigv).toFixed(2))
    $("#total").val(parseFloat(tot).toFixed(2))

    seguimiento($("#user").val());
    contador = parseInt(contador) - 1

    FacturacionesDel += $('#nrofacturacion'+index).val()+','

    $("#tr_"+index).remove();



    //////////////////77


    $("#subtotal").val('0.00')
    $("#igv").val('0.00')
    $("#redondeo").val('0.00')
    $("#total").val('0.00')

    var id = parseInt($("#tbdetpagos tbody tr").length);// - 1;//MENOS LA CABECERA
    if (id == 0)
        return false
    $.ajax({
        url: urldir+'sigco/cobranza/operaciones/cancelacion/ajax/cargar_totales.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val()+'&FacturacionesDel='+FacturacionesDel,
        success: function(datos) {
            var r = datos.split("|")

            $("#subtotal").val(r[0])
            $("#igv").val(r[2])
            $("#redondeo").val(r[1])
            $("#total").val(r[3])

            //return ValidarForm();
        }
    })
    //////////////////////
}
function seguimiento(codusuario)
{
    $.ajax({
        url: '../../../../admin/validaciones/seguimiento_remove.php',
        type: 'POST',
        async: true,
        data: 'idsistema=104&comentario=SE HA BORRADO UN CONCEPTO DE COBRANZA CON EL USUARIO '+codusuario,
        success: function(datos) {

        }
    })
}

function buscar_presupuesto(idx, tipo)
{
    index = idx;
    object = "presupuesto"

    AbrirPopupBusqueda('../../../solicitudes/operaciones/expedientes/?Op=6', 1150, 550);
}

function datos_presupuesto(id)
{
    $.ajax({
        url: '../../../../ajax/mostrar/presupuesto.php',
        type: 'POST',
        async: true,
        data: 'nropresupuesto='+id+"&codsuc="+codsuc,
        dataType: "json",
        success: function(datos)
        {

            $("#nropresupuesto"+index).val(id)
            $("#nroinscripcion").val(datos.nroinscripcion)
            $("#cliente").val(datos.propietario)
            $("#direccion").val(datos.calle)
            if (datos.nroinspeccion != 0)
                $("#nroinspeccion").val(datos.nroinspeccion)
            $("#docidentidad").val(datos.abreviado+" - "+datos.nrodocumento)

            if (datos.tipopago == 1)//AL CONTADO
            {
                $("#importe"+index).val(datos.totalpresupuesto)
                $(".con"+index).hide()
            } else {
                $("#importe"+index).val(datos.cuotainicial)
                $(".con"+index).show()
            }
            $("#afecto_igv_pre"+index).val(datos.conigv)
            cargar_datos_periodo_facturacion(1)
            $("#nuevocliente").val(datos.nuevocliente)

            if (datos.masdeuno == 1)
            {
                if (confirm('Agregar Todos los presupuesto del Usuario?'))
                {
                    $("#tbdetpagos tbody").html(datos.tr)
                    red = CalcularRedondeo(parseFloat(datos.subtotaltotal)+parseFloat(datos.igvtotal))
                    $("#subtotal").val(parseFloat(datos.subtotaltotal).toFixed(2))
                    $("#redondeo").val(parseFloat(red).toFixed(2))
                    $("#igv").val(parseFloat(datos.igvtotal).toFixed(2))
                    $("#total").val(parseFloat(datos.importetotal).toFixed(2))
                    contador = datos.cont_cobranza;
                    $("#cont_cobranza").val(datos.cont_cobranza)
                }
            }

        }
    })
}

function datos_expediente(id)
{
    $("#codexpediente").val(id);

    $.ajax({
        url: '../../../../ajax/mostrar/expediente.php',
        type: 'POST',
        async: true,
        data: 'codexpediente='+id+"&codsuc="+codsuc,
        dataType: "json",
        success: function(datos)
        {

            //$("#nropresupuesto"+index).val(id)
            $("#nroinscripcion").val(datos.nroinscripcion)

            $("#cliente").val(datos.propietario)
            $("#direccion").val(datos.direccion)

            if (datos.nroinspeccion != 0)
            {
                $("#nroinspeccion").val(datos.nroinspeccion)
            }

            //$("#docidentidad").val(datos.abreviado+" - "+datos.nrodocumento)

            $("#docidentidad").val(datos.documento)

            /*if(datos.tipopago==1)//AL CONTADO
             {
             $("#importe"+index).val(datos.totalpresupuesto)
             $(".con"+index).hide()
             }else{
             $("#importe"+index).val(datos.cuotainicial)
             $(".con"+index).show()
             }*/
            $("#afecto_igv_pre"+index).val(datos.conigv)
            $("#nuevocliente").val(datos.nuevocliente)

            cargar_datos_periodo_facturacion(1)

            $("#tbdetpagos tbody").html(datos.tr)

            red = CalcularRedondeo(parseFloat(datos.subtotaltotal)+parseFloat(datos.igvtotal))

            $("#subtotal").val(parseFloat(datos.subtotaltotal).toFixed(2))
            $("#redondeo").val(parseFloat(red).toFixed(2))
            $("#igv").val(parseFloat(datos.igvtotal).toFixed(2))
            $("#total").val(parseFloat(datos.importetotal).toFixed(2))

            contador = datos.cont_cobranza;

            $("#cont_cobranza").val(datos.cont_cobranza)

            cont_cob = parseInt($("#tbdetpagos tbody tr").length);


        }
    })
}
function detalle_credito(idx)
{
    $.ajax({
        url: 'ajax/precredito.php',
        type: 'POST',
        async: true,
        data: 'nropresupuesto='+$("#nropresupuesto"+idx).val()+'&codsuc='+codsuc+'&anio='+$("#anio").val()+'&mes='+$("#mes").val()+"&nrofacturacion="+$("#nrof").val(),
        success: function(datos) {
            var r = datos.split("|")

            $("#DetCredtio tbody").html(r[7])
            $("#DivDetCredito").dialog("open");

        }
    })
}

function GuardarPre()
{
    var subtotal = $("#subtotal").val();
    var igv = $("#igv").val();
    var redondeo = $("#redondeo").val();
    var total = $("#total").val();
    $("#subtotaltotal").val(subtotal);
    $("#igvtotal").val(igv);
    $("#redondeototal").val(redondeo);
    $("#totalpago").val(total);
    //+'&subtotaltotal='+subtotal+'&igvtotal='+igv+'&redondeototal='+redondeo+'&totalpago='+total
    $.ajax({
        url: 'Guardar.php',
        type: 'POST',
        async: true,
        data: $('#form1').serialize(),
        dataType: 'json',
        success: function(data)
        {
            OperMensaje(data.res);

            if (data.res == 1)
            {
                if (data.esservicio == 1 && $("#eventualtext").val() == 0 && $("#nroinspeccion").val() == '')
                {
                    if (confirm('Desea Generar Solicitud?'))
                    {
                        $.ajax({
                            url: urldir+'sigco/cobranza/operaciones/cancelacion/consulta/GenerarSolicitud.php',
                            type: 'POST',
                            async: true,
                            data: 'tipo='+data.tipo+'&pago='+data.pago+"&codsuc="+codsuc+'&nroinscripcion='+$("#nroinscripcion").val(),
                            dataType: 'json',
                            success: function(datos)
                            {
                                if (datos.res == 1)
                                {
                                    AbrirPopupImpresion(urldir+"sigco/catastro/operaciones/solicitud/imprimir.php?nrosolicitud="+datos.nroinspeccion+'&codsuc='+codsuc+'&nuevocliente=0', 800, 600);
                                }
                                if (data.esservicio == 1)
                                {
                                    if (confirm("Desea Imprimir el Recibo?"))
                                    {
                                        AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                    }
                                }
                                //location.reload(true);
                            }
                        });
                    }
                    else
                    {
                        if (data.esservicio == 1)
                        {
                            if (confirm("Desea Imprimir el Recibo?"))
                            {
                                AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                            }
                        }
                        //location.reload(true);
                    }
                }
                else
                {
                    if ($("#eventualtext").val() == 1) //Chekeado es eventual
                    {
                        // if(confirm('Desea Generar Solicitud?'))
                        // {
                        // Generando Solicitud
                        $.ajax({
                            url: urldir+'sigco/cobranza/operaciones/cancelacion/consulta/GenerarSolicitud.php',
                            type: 'POST',
                            async: true,
                            data: 'tipo=1&pago='+data.pago+'&codsuc='+codsuc+'&nroinscripcion=0',
                            dataType: 'json',
                            success: function(datos)
                            {
                                if (datos.res == 1) {
                                    if ($("#nroinspeccion").val() != '')
                                    {
                                        if (confirm('Desea Imprimir Solicitud'))
                                        {
                                            AbrirPopupImpresion(urldir+"sigco/catastro/operaciones/solicitud/imprimir.php?nrosolicitud="+$("#nroinspeccion").val()+'&codsuc='+codsuc+'&nuevocliente='+$("#nuevocliente").val(), 800, 600);
                                        }
                                    }
                                    if (data.esservicio == 1)
                                    {
                                        if (confirm("Desea Imprimir el Recibo?"))
                                        {
                                            AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
                                        }
                                    }
                                }
                            }

                        });
                        // }

                    }

                }
            }

            //AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_tiket.php?codpago="+data.pago+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#fechareg").val()+'&codsuc='+codsuc, 800, 600);
            location.reload(true);
        }
    });
}

function imprimir_duplicado()
{
    if ($("#nroinscripcion").val() == "")
    {
        Msj(objbusqueda, "Seleccione Usuario");

        return
    }

    if (confirm("Va a Proceder a Realizar un Duplicado de Recibo. Desea Cargar al proximo recibo de facturacion el importe?"))
    {
        $.ajax({
            url: urldir+'sigco/consulta/impduplicados/Guardar.php',
            type: 'POST',
            async: true,
            data: 'nroinscripcion='+$("#nroinscripcion").val()+'&codsuc='+codsuc,
            success: function(data) {
                OperMensaje(data)
                $("#Mensajes").html(data);

                location.reload(true);
            }
        })
    }

    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/impresion/imprimir_duplicado.php?nroinscripcion="+$("#nroinscripcion").val()+"&codsuc="+codsuc+"&codciclo="+$("#codciclo").val(), 800, 600);
}

function imprimir_historial()
{
    if ($("#nroinscripcion").val() == "")
    {
        Msj(objbusqueda, "Seleccione Usuario")
        return
    }
    $.ajax({
        url: urldir+'sigco/catastro/listados/cuentacorriente/Consulta.php',
        type: 'POST',
        async: true,
        data: 'NroInscripcion='+$("#nroinscripcion").val(),
        success: function(datos)
        {

            $("#detalle-CuentaCorriente").empty().append(datos);
            $("#DivCuentaCorriente").dialog('open');

        }
    })
    //AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/impresion/imprimir_historial.php?nroinscripcion="+$("#nroinscripcion").val()+"&codsuc="+codsuc+"&codciclo="+$("#codciclo").val(),800,600)
    //AbrirPopupImpresion(urldir+"sigco/catastro/listados/cuentacorriente/Pdf.php?NroInscripcion="+$("#nroinscripcion").val(),800,600)
}
function cargar_monto_amortizar(nroinscripcion, nrofacturacion, index, documento, nrodocumento, tipodeuda, anio, mes, categoria)
{
    cargar_datos_amortizar(nroinscripcion, nrofacturacion, documento, nrodocumento, tipodeuda, anio, mes, categoria)
    idx = index

}

function cargar_datos_amortizar(nroinscripcion, nrofacturacion, documento, nrodocumento, tipodeuda, anio, mes, categoria)
{
    $.ajax({
        url: 'amortizar.php',
        type: 'POST',
        async: true,
        data: 'nroinscripcion='+nroinscripcion+"&nrofacturacion="+nrofacturacion+"&codsuc="+codsuc+'&doc='+documento+'&ndoc='+nrodocumento +
                '&td='+tipodeuda+'&anio='+anio+'&mes='+mes+'&cat='+categoria,
        success: function(datos) {
            $("#div_amortizar").html(datos)
            $("#dialog-form-amortizar").dialog("open");
            $("#automatico").focus().select();

            CalcularAmortizacion();
        }
    })
}

function buscar_prepagos()
{
    AbrirPopupBusqueda(urldir+'sigco/cobranza/operaciones/cancelacion/busquedas/prepagos.php?Op=5', 1150, 450)
}

function recibir_prepagos(id)
{
    var r = id.split("|")

    $("#nroprepago").val(r[0])
    $("#nroinscripcion").val(r[1])

    //cargar_datos_usuario(r[1])
    cargar_datos_facturacion(r[1])
    cargar_datos_precobranza(r[0])
    //cargar_totales_precobranza(r[0])
}
function cargar_datos_precobranza(nroprepago)
{
    $.ajax({
        url: 'ajax/precobranza.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&nroprepago='+nroprepago+'&nroinscripcion='+$("#nroinscripcion").val(),
        success: function(datos) {
            var r = datos.split("|")

            QuitaRow_Cobranza();

            $("#tbdetpagos tbody").html(r[0])
            $("#cont_cobranza").val(r[1])
            cont_cob = r[1]
            contador = r[1]

            $("#cliente").val(r[2])
            $("#direccion").val(r[3])
            $("#docidentidad").val(r[4])

            if (r[5] == 1)
            {
                $("#eventual").attr("checked", true)
                $("#eventualtext").val(1)
            } else {
                $("#eventual").attr("checked", false)
                $("#eventualtext").val(0)
            }
            $("#nroinspeccion").val(r[6])
            $("#Glosa").val(r[7])
            cargar_totales_precobranza()
        }
    })
}
function cargar_totales_precobranza()
{
    /*$.ajax({
     url:'ajax/cargar_totales_precobranza.php',
     type:'POST',
     async:true,
     data:'codsuc='+codsuc+'&nroprepago='+nroprepago,
     success:function(datos){
     var r=datos.split("|")
     
     $("#subtotal").val(r[0])
     $("#igv").val(r[2])
     $("#redondeo").val(r[1])
     $("#total").val(r[3])
     
     }
     }) */
    var id = parseInt($("#tbdetpagos tbody tr").length);// - 1;//MENOS LA CABECERA
    var imptotal = 0
    var igvtotal = 0
    var redtotal = 0
    var imp = 0

    for (var i = 1; i <= id; i++)
    {
        imp = $("#redadd"+i).val();
        redtotal += parseFloat(imp);

        imp = $("#imptotal"+i).val()
        imptotal += parseFloat(imp);

        imp = $("#igvadd"+i).val();
        igvtotal += parseFloat(imp);
    }

    //if (parseFloat(redtotal) > 0)
//	{
//		imptotal = imptotal+redtotal;
//	}
//	else
//	{
//		imptotal = imptotal - redtotal;
//	}

    $("#subtotal").val(imptotal.toFixed(2));
    $("#igv").val(igvtotal.toFixed(2));
    $("#redondeo").val(redtotal.toFixed(2));

    var total = parseFloat(imptotal)+parseFloat(igvtotal)+parseFloat(redtotal);
    $("#total").val(total.toFixed(2));
}

function habilitar_eventual(Obj)
{
    $("#cliente").val("")
    $("#codantiguo").val("")
    $("#nrorecibo").val("")
    $("#direccion").val("")
    $("#docidentidad").val("")
    $("#nroprepago").val("")

    $("#subtotal").val("0.00")
    $("#igv").val("0.00")
    $("#redondeo").val("0.00")
    $("#total").val("0.00")
    QuitaRow_Cobranza()
    QuitaRow_Facturacion()
    $("#tbfacturacion tbody").html('')
    $("#tbdetpagos tbody").html('')
    if (Obj.checked == true)
    {
        $("#conrecibo").attr('checked', false)
        habilitar_rapida($("#conrecibo"))
        $("#eventualtext").val(1)
        $("#nroinscripcion").val(0)
        $("#codcliente").val(0)
        document.getElementById("nroinscripcion").readOnly = true

        document.getElementById("cliente").readOnly = false
        document.getElementById("direccion").readOnly = false
        document.getElementById("docidentidad").readOnly = false
    } else
    {
        $("#eventualtext").val(0)
        $("#nroinscripcion").val("")
        $("#codcliente").val("")
        document.getElementById("nroinscripcion").readOnly = false

        document.getElementById("cliente").readOnly = true
        document.getElementById("direccion").readOnly = true
        document.getElementById("docidentidad").readOnly = true
    }
}
function habilitar_rapida(Obj)
{
    $("#subtotal").val("0.00")
    $("#igv").val("0.00")
    $("#redondeo").val("0.00")
    $("#total").val("0.00")
    $("#codantiguo").val("")
    $("#nrorecibo").val("")
    $("#cliente").val("")
    $("#direccion").val("")
    $("#docidentidad").val("")
    $("#nroprepago").val("")
    QuitaRow_Cobranza()
    QuitaRow_Facturacion()
    $("#tbfacturacion tbody").html('')
    $("#tbdetpagos tbody").html('')
    if (Obj.checked == true)
    {
        $("#eventual").attr('checked', false)
        habilitar_eventual($("#eventual"))
        $("#lblTipocobro").html('N&deg; Recibo')
        $("#nroinscripcion").val(0)
        $("#codcliente").val(0)
        $("#codantiguo").hide()
        $("#nrorecibo").show()
        document.getElementById("nroinscripcion").readOnly = true

        document.getElementById("cliente").readOnly = false
        document.getElementById("direccion").readOnly = false
        document.getElementById("docidentidad").readOnly = false
    } else {
        $("#lblTipocobro").html('N&deg; Inscripci&oacute;n')
        $("#nroinscripcion").val("")
        $("#codcliente").val("")
        $("#codantiguo").show()
        $("#nrorecibo").hide()
        document.getElementById("nroinscripcion").readOnly = false

        document.getElementById("cliente").readOnly = true
        document.getElementById("direccion").readOnly = true
        document.getElementById("docidentidad").readOnly = true
    }
}

function cargar_conceptos_sin_categoria()
{
    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/include/ver_conceptos_sin_categoria.php?codsuc="+codsuc, 800, 600);
}

function abrir_detalle_facturacion(nrofacturacion, categoria, codtipodeuda)
{
    cargar_detalle_facturacion(nrofacturacion, categoria, codtipodeuda);

    $("#dialog-form-detalle-facturacion").dialog("open");
}

function cargar_detalle_facturacion(nrofacturacion, categoria, codtipodeuda)
{
    $.ajax({
        url: urldir+'sigco/cobranza/operaciones/cancelacion/include/from_detalle_facturacion.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val() +
                '&nrofacturacion='+nrofacturacion+'&categoria='+categoria+'&codtipodeuda='+codtipodeuda,
        success: function(datos) {
            $("#detalle-facturacion").html(datos);
        }
    })
}

function cSerieNun()
{
    var TipoDoc = $("#documento").val();

    $.ajax({
        url: urldir+'sigco/cobranza/operaciones/cancelacion/include/cSerieNun.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&TipoDoc='+TipoDoc,
        success: function(datos) {
            var r = datos.split("|");

            $("#seriedoc").val(r[0]);
            $("#numerodoc").val(r[1]);
        }
    })
}

function ValidarEfect(e)
{
    if (VeriEnter(e))
    {
        if (cobrapida == 0)
        {
            $("#button-guardarpago").focus();
        }
        else
        {
            GuardarTodo();
        }

        e.returnValue = false;
    }
}

function ValidarUpdateImporte(e)
{
    if (VeriEnter(e))
    {
        ValidarUpdateImporteOk();

        e.returnValue = false;
    }
}

function ValidarUpdateImporteOk()
{
    var importe = str_replace($("#UpdateImporteColateralImporte").val(), ',', '');

    if (importe == 0.00 || importe == 0 || importe == "" || isNaN(importe))
    {
        Msj($("#UpdateImporteColateralImporte"), "Ingrese Importe Valido");

        return;
    }

    var idx = $("#ItemUpdateImporte").val();

    $("#imptotal"+idx).val(parseFloat(importe).toFixed(2));
    $("#lblimptotal"+idx).html(parseFloat(importe).toFixed(2));

    var importeOriginal = str_replace($("#UpdateImporteColateralOriginal").val(), ',', '');

    impt = importe;
    subt = parseFloat(str_replace($("#subtotal").val(), ',', '')) - parseFloat(importeOriginal)+parseFloat(importe);
    red = str_replace($("#redondeo").val(), ',', '');
    imigv = CalculaIgv((parseFloat(subt)+parseFloat(red)), impigv);
    red = CalcularRedondeo(parseFloat(subt)+parseFloat(imigv));
    tot = parseFloat(subt)+parseFloat(imigv)+parseFloat(red);

    $("#subtotal").val(parseFloat(subt).toFixed(2));
    $("#redondeo").val(parseFloat(red).toFixed(2));
    $("#igv").val(parseFloat(imigv).toFixed(2));
    $("#total").val(parseFloat(tot).toFixed(2));

    $("#dialog-form-update-importe").dialog("close");
    $("#LblUpdateImporteColateral").html('');
    $("#UpdateImporteColateralImporte").val('0.00');
}

function VerGlosa()
{
    $("#DivGlosa").dialog('open');
}

var cobrapida = 0;

function ValidarRecibo(e)
{
    if (VeriEnter(e))
    {
        if ($("#nrorecibo").val() != "")
        {
            var Id = $("#nrorecibo").val()
            $.ajax({
                url: urldir+'sigco/cobranza/operaciones/cancelacion/ajax/vRecibo.php',
                type: 'POST',
                async: true,
                data: 'Id='+Id+'&codsuc='+codsuc,
                success: function(datos)
                {
                    if (datos != 0)
                    {
                        cobrapida = 1
                        var item = datos.split("|");
                        $("#nroinscripcion").val(item[0])
                        $("#nroprepago").val("")
                        cargar_datos_usuario(item[0])
                        cargar_datos_facturacion(item[0])
                        if (ventana == "cobranza")
                        {
                            cargar_datos_cobranza(item[0])
                            //cargar_totales(item[0])
                        }
                    }
                    else
                    {
                        //location.reload();
                        Msj($("#nrorecibo"), 'Recibo no existe')
                    }


                }
            })

        }
    }
}

function Cerrar()
{
    location.href = '../../../../admin/index.php';
}

function  buscar_servicio()
{
    object = "solicitudinspeccion";

    AbrirPopupBusqueda('../../../catastro/operaciones/solicitud/?Op=5', 1150, 450);
}

function cVentana()
{

    if (ventana == "precobranza")
    {
        location.href = '../cancelacion/index.php?Op=0&fdescarga='+$("#fechareg").val()+'&cars='+$("#carx").val();
    }
    else
    {
        location.href = '../precobranza/index.php?Op=0&fdescarga='+$("#fechareg").val()+'&cars='+$("#carx").val();
    }
}

function buscar_cargos()
{
    AbrirPopupBusqueda(urldir+'sigco/facturacion/operaciones/cargos/generar/index.php?Op=5', 1150, 450);
}

function RecibirCargo(id)
{
    $("#nrocargo").val(id);

    $.ajax({
        url: '../../../../ajax/mostrar/cargos.php',
        type: 'POST',
        async: true,
        data: 'cargo='+id+"&codsuc="+codsuc,
        dataType: "json",
        success: function(datos)
        {
            //$("#nropresupuesto"+index).val(id);
            $("#nroinscripcion").val(datos.nroinscripcion)
            $("#codantiguo").val(datos.codantiguo)
            $("#cliente").val(datos.propietario)
            $("#direccion").val(datos.direccion)

            if (datos.nroinspeccion != 0)
            {
                $("#nroinspeccion").val(datos.nroinspeccion);
            }

            $("#docidentidad").val(datos.abreviado+" - "+datos.nrodocumento)

            /*if(datos.tipopago==1)//AL CONTADO
             {
             $("#importe"+index).val(datos.totalpresupuesto)
             $(".con"+index).hide()
             }else{
             $("#importe"+index).val(datos.cuotainicial)
             $(".con"+index).show()
             }*/
            $("#afecto_igv_pre"+index).val(datos.conigv)
            $("#nuevocliente").val(datos.nuevocliente)
            cargar_datos_periodo_facturacion(1)



            $("#tbdetpagos tbody").html(datos.tr)
            red = CalcularRedondeo(parseFloat(datos.subtotaltotal)+parseFloat(datos.igvtotal))
            $("#subtotal").val(parseFloat(datos.subtotaltotal).toFixed(2))
            $("#redondeo").val(parseFloat(red).toFixed(2))
            $("#igv").val(parseFloat(datos.igvtotal).toFixed(2))
            $("#total").val(parseFloat(datos.importetotal).toFixed(2))
            contador = datos.cont_cobranza;
            $("#cont_cobranza").val(datos.cont_cobranza)
            cont_cob = parseInt($("#tbdetpagos tbody tr").length);


        }
    })
}
