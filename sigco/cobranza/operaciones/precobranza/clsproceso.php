<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	class cobranza
	{
		//----Variables para el Detalle de la Facturacion
		var $item;
		var $nrofacturacion;
		var $concepto;
		var $importe;
		var $tipodeuda;
		var $conceptos;
		var $deuda;
		var $categoria;
		var $coddocumento;
		var $nrodocumento;
		var $serie;
		
		//----Variables para el Detalle de la Amortizacion
		var $nitem;
		var $codconceptox;
		var $importex;
		var $detallex;
		var $tipopagox;
		var $nrofacturacionx;
		var $tipodeudax;
		var $documentox;
		var $nrodocumentox;
		var $codtipodeudax;
		var $aniox;
		var $mesx;
		var $categoriax;

		function cobranza()
		{
			//---Valores para que aparesca el detalle de la facturacion-----
			$this->item				= 0;
			$this->nrofacturacion 	= "";
			$this->concepto			= "";
			$this->importe			= "";
			$this->tipodeuda		= "";
			$this->conceptos		= "";
			$this->deuda			= "";
			$this->categoria		= "";
			$this->coddocumento		= "";
			$this->nrodocumento		= "";
			$this->serie			= "";
			
			//---Valores para el detalle de la amortizacion-----
			$this->nitem			= 0;
			$this->codconceptox		= "";
			$this->importex			= "";
			$this->detallex			= "";
			$this->tipopagox		= "";
			$this->nrofacturacionx	= "";
			$this->documentox		= "";
			$this->nrodocumentox	= "";
			$this->codtipodeudax	= "";
			$this->aniox			= "";
			$this->mesx				= "";
			$this->categoriax		= "";
		}
		function agrevarvalores($nrofact,$codconcepto,$imp,$deuda,$con,$deu,$cat,$coddoc,$nrodoc,$ser)
		{
			$this->nrofacturacion[$this->item] 	= $nrofact;
			$this->concepto[$this->item] 		= $codconcepto;
			$this->importe[$this->item] 		= $imp;
			$this->tipodeuda[$this->item] 		= $deuda;
			$this->conceptos[$this->item] 		= $con;
			$this->deuda[$this->item] 			= $deu;
			$this->categoria[$this->item] 		= $cat;
			$this->coddocumento[$this->item] 	= $coddoc;
			$this->nrodocumento[$this->item] 	= $nrodoc;
			$this->serie[$this->item] 			= $ser;
			$this->item++;
		}
		function agregarvaloresamortizacion($codconcepto,$importe,$detalle,$nrofact,$tdeuda,$doc,$nrodoc,$td,$anio,$mes,$catg)
		{
			$this->codconceptox[$this->nitem] 		= $codconcepto;
			$this->importex[$this->nitem] 			= $importe;
			$this->detallex[$this->nitem] 			= $detalle;
			$this->tipopagox[$this->nitem] 			= "1";
			$this->nrofacturacionx[$this->nitem] 	= $nrofact;
			$this->tipodeudax[$this->nitem]			= $tdeuda;
			$this->documentox[$this->nitem]			= $doc;
			$this->nrodocumentox[$this->nitem]		= $nrodoc;
			$this->codtipodeudax[$this->nitem]		= $td;
			$this->aniox[$this->nitem]				= $anio;
			$this->mesx[$this->nitem]				= $mes;
			$this->categoriax[$this->nitem]			= $catg;
			$this->nitem++;
		}
		function actualizarvaloresamortizacion($codconcepto,$importe,$detalle,$nrofact,$tdeuda,$doc,$nrodoc,$td,$idx,$anio,$mes,$catg)
		{
			$this->codconceptox[$idx] 		= $codconcepto;
			$this->importex[$idx] 			+= $importe;
			$this->detallex[$idx] 			= $detalle;
			$this->tipopagox[$idx] 			= "1";
			$this->nrofacturacionx[$idx] 	= $nrofact;
			$this->tipodeudax[$idx]			= $tdeuda;
			$this->documentox[$idx] 		= $doc;
			$this->nrodocumentox[$idx]		= $nrodoc;
			$this->codtipodeudax[$idx]		= $td;
			$this->aniox[$idx]				= $anio;
			$this->mesx[$idx]				= $mes;
			$this->categoriax[$idx]			= $catg;
		}
		function agregarvaloresamortizacionxx($codconcepto,$importe,$detalle,$nrofact,$tdeuda,$doc,$nrodoc,$td,$anio,$mes,$catg)
		{
			$entro	= 0;
			$idx	= 0;

			for($i=0;$i<$this->nitem;$i++)
			{
				if($this->codconceptox[$i] == $codconcepto && $this->nrofacturacionx[$i] == $nrofact && $this->tipodeudax[$i] == $tdeuda)
				{
					$entro = 1;
					$idx = $i;
				}
			}
			if($entro==0)
			{
				$this->agregarvaloresamortizacion($codconcepto,$importe,$detalle,$nrofact,$tdeuda,$doc,$nrodoc,$td,$anio,$mes,$catg);
			}else{
				$this->actualizarvaloresamortizacion($codconcepto, $importe, $detalle, $nrofact, $tdeuda, $doc, $nrodoc, $td, $idx, $anio, $mes, $catg);
			}

		}
	}
	
	if (!isset($_SESSION["ocobranza"])){ 
 		$_SESSION["ocobranza"] = new cobranza();
	} 
?>