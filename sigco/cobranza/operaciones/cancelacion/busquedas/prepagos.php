<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");
	
	$TituloVentana = "PRE PAGOS";
	$Activo=1;
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	$FormatoGrilla = array ();
	
	$Fecha   = isset($_GET["Fecha"])?$_GET["Fecha"]:date('d/m/Y');
	
	$and = " AND cab.fechareg='".$objFunciones->CodFecha($Fecha)."'";
	
	$Sql = "SELECT DISTINCT cab.nroprepago, d.abreviado|| ' ' || p.serie ||'-'|| p.nrodocumento, UPPER(cab.propietario),
              ".$objFunciones->FormatFecha('cab.fechareg').", cab.hora,
              ".$objFunciones->Convert("cab.imptotal", "NUMERIC (18, 2)").",
              CASE WHEN cab.estado=0 THEN 'ANULADO' WHEN cab.estado=1 THEN 'PENDIENTE' ELSE 'CANCELADO' END,
              cab.nroprepago,1,cab.nroinscripcion,cab.creador,cab.codformapago,cab.fechareg,
              d.abreviado , p.serie ,  p.nrodocumento
             from cobranza.cabprepagos as cab
              INNER JOIN cobranza.detprepagos p ON (cab.codemp = p.codemp) AND (cab.codsuc = p.codsuc) AND (cab.nroprepago = p.nroprepago) 
               INNER JOIN reglasnegocio.documentos d ON (d.codsuc = p.codsuc) AND (d.coddocumento = p.coddocumento)
              ";

  $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'cab.nroprepago','7'=>'cab.propietario',
                      '2'=>'cab.nroprepago','3'=>'d.abreviado','4'=>'p.serie','5'=>'p.nrodocumento','6'=>"to_char(cab.codsuc,'00')||''||TRIM(to_char(cab.nroinscripcion,'000000')) ");          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Pre Pago','T2'=>'Documento','T3'=>'Propietario',
                        'T4'=>'Fecha','T5'=>'Hora','T6'=>'Importe','T7'=>'Estado'  );   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A3'=>'left', 'A4'=>'center','A5'=>'center','A7'=>'center','A10'=>'center','A6'=>'right');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60','W2'=>'60','W3'=>'180','W4'=>'80','W5'=>'90','W6'=>'70','W7'=>'80','W8'=>'20','W9'=>'80','W10'=>'60','W11'=>'60','W12'=>'60');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  and (cab.codemp=1 and cab.codsuc=".$codsuc.") AND cab.estado=1 ORDER BY cab.nroprepago DESC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'9',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
  $FormatoGrilla[10] = array(array('Name' =>'nroinscripcion','Col'=>10),array('Name' =>'fechapago','Col'=>13),array('Name' =>'codpago','Col'=>1));//DATOS ADICIONALES           
  $FormatoGrilla[11]=7;//FILAS VISIBLES            
  $_SESSION['Formato'] = $FormatoGrilla;
  
  $Previo=' <table width="100%" border="0" >';
  $Previo.='<tr><td valign="middle" align="left" width="65">Fecha :</td><td valign="middle" align="left">';
  $Previo.= '<input type="text" class="inputtext ui-corner-all text" name="3form1_fecha" id="Fecha" value="'.$Fecha.'" readonly="readonly" style="width:100px" onchange="BuscarB('.$Op.')"/>';
  $Previo.='</td></tr></table>';
  Cabecera('', $FormatoGrilla[7],1000,600);
  Pie();


?>
<script>
var urldir  = "<?php echo $_SESSION['urldir'];?>";

function BuscarB(Op)
  {
    var Valor     = document.getElementById('Valor').value
    var Fecha   = document.getElementById('Fecha').value
    var Op2 = ''
    if (Op!=0)
    {
      Op2 = '&Op=' + Op;
    }
    location.href='index.php?Valor=' + Valor + '&pagina=' + Pagina + Op2 + '&Fecha='+Fecha;
  }
$(document).ready(function(){

  $("#BtnNuevoB").remove();
    });

	function Enviar(obj)
	{
		var codpago = $(obj).parent().parent().data('codpago')
    	var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
		opener.recibir_prepagos(codpago+'|'+nroinscripcion);
		window.close();
	}
</script>
</form>
