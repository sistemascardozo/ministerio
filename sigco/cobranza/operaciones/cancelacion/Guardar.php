<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsFunciones.php");

$objFunciones = new clsFunciones();

$fechas = $objFunciones->DecFechaLiteral();

$contador = $_POST["cont_cobranza"];
$codemp = 1;
$codsuc = $_SESSION["IdSucursal"];
$nroinscripcion = $_POST["nroinscripcion"];
$eventualtext = $_POST["eventualtext"];
$npresupuesto_con_expediente1 = 0; // para obtener siempre el primer numero de expediente
//
if ($nroinscripcion != "0") {
    $sql = "SELECT c.codcliente, c.propietario, c.codtipodocumento, c.nrodocumento, c.codciclo, tc.descripcioncorta, ";
    $sql .= " cl.descripcion AS calle, c.nrocalle, c.codciclo, td.abreviado, c.codsector, c.codcalle ";
    $sql .= "FROM catastro.clientes AS c ";
    $sql .= " INNER JOIN public.calles AS cl ON(c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona) ";
    $sql .= " INNER JOIN public.tiposcalle AS tc ON(cl.codtipocalle = tc.codtipocalle) ";
    $sql .= " INNER JOIN public.tipodocumento AS td ON(c.codtipodocumento = td.codtipodocumento) ";
    $sql .= "WHERE c.codemp = 1 AND c.codsuc = ? ";
    $sql .= " AND c.nroinscripcion = ?";

    $result = $conexion->prepare($sql);
    $result->execute(array($codsuc, $nroinscripcion));

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error SELECT Catastro";
        $data['res'] = 2;

        die(json_encode($data));
    }

    $item = $result->fetch();
    $propietario = strtoupper($item["propietario"]);
    $direccion = strtoupper($item["descripcioncorta"]." ".$item["calle"]." ".$item["nrocalle"]);
    $docidentidad = strtoupper($item["nrodocumento"]);

    if ($item["propietario"] == '') {
        $propietario = strtoupper($_POST["cliente"]);
        $direccion = strtoupper($_POST["direccion"]);
        $docidentidad = strtoupper($_POST["docidentidad"]);
    }
} else {
    $propietario = strtoupper($_POST["cliente"]);
    $direccion = strtoupper($_POST["direccion"]);
    $docidentidad = strtoupper($_POST["docidentidad"]);
}

//
$condpago = $_POST["condpago"];
$documento = $_POST["documento"];

$formapago = $_POST["formapago"];
$fechareg = $objFunciones->CodFecha($_POST["fechareg"]);
$car = $_SESSION["car"];
$nrocaja = $_SESSION["nocaja"];
$idusuario = $_SESSION['id_user'];

$imptotal = $_POST["total"];

$efectivo = $_POST["efectivo_x"];
$hora = $objFunciones->HoraServidor();
$igv = $_POST["igv"];
$redondeo = $_POST["redondeo"];
$subtotal = $_POST["subtotal"];
$nroprepago = $_POST["nroprepago"] ? $_POST["nroprepago"] : 0;
$anio = $fechas["anio"];
$mes = $fechas["mes_num"];
$facturacion = 0;
$numerodoc = $_POST["numerodoc"];
$seriedoc = $_POST["seriedoc"];
$glosa = $_POST["glosa"];
$Flag = true;
$UpdCorrelativo = false;
$nroinspeccion = $_POST['nroinspeccion'] ? $_POST['nroinspeccion'] : 0;
$codexpediente = $_POST['codexpediente'] ? $_POST['codexpediente'] : 0;
$codexpedienteV = $_POST['codexpediente'] ? $_POST['codexpediente'] : 0;
//$codexpediente;
$nrocargo = $_POST['nrocargo'] ? $_POST['nrocargo'] : 0;

if ($eventualtext == 1) {
    if (strlen(trim($docidentidad)) == 11) {
        $docidentidad = "RUC - ".$docidentidad;
    } else {
        $docidentidad = "DNI - ".$docidentidad;
    }
}

$nropago = $objFunciones->setCorrelativosVarios(5, $codsuc, "SELECT", 0);
$nrocredito = $objFunciones->setCorrelativosVarios(8, $codsuc, "SELECT", 0);
$nroprepagocre = $objFunciones->setCorrelativosVarios(6, $codsuc, "SELECT", 0);
$concredito = false;

$conexion->beginTransaction();

//Generación de Correlativo para Tickets
$SqlC = "SELECT * FROM reglasnegocio.correlativos ";
$SqlC .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
$SqlC .= " AND nrocorrelativo = 26";

$result = $conexion->query($SqlC);
$RowC = $result->fetch();

if (!$result) {
    $conexion->rollBack();

    $mensaje = "116 Error SELECT correlativos";

    $data['res'] = 2;
    $data['mensaje'] = $mensaje;

    die(json_encode($data));
}

//if ($documento == 26) :
//
//    $Ticket_Serie = $RowC['serie'];
//    $Ticket_Numero = $RowC['correlativo'];
//
//else:
//    $Ticket_Serie = 0;
//    $Ticket_Numero = 0;
//endif;

$Ticket_Serie = $RowC['serie'];
$Ticket_Numero = $RowC['correlativo'];
    
$SqlC = "UPDATE reglasnegocio.correlativos ";
$SqlC .= " SET correlativo = correlativo + 1 ";
$SqlC .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
$SqlC .= " AND nrocorrelativo = 26";

$result = $conexion->query($SqlC);

if (!$result) {
    $conexion->rollBack();
    $mensaje = "139 Error UPDATE correlativos";
    $data['res'] = 2;
    $data['mensaje'] = $mensaje;

    die(json_encode($data));
}

if ($condpago == 0) { //CONTADO
    $ImpTT = str_replace(",", "", $imptotal);
    $ImpPG = str_replace(",", "", $efectivo);
    $ImpST = str_replace(",", "", $subtotal);
    $igv = str_replace(",", "", $igv);

    $sqlC = "INSERT INTO cobranza.cabpagos";
    $sqlC .= "(codemp, codsuc, nroinscripcion, nropago, car, nrocaja, codformapago, creador, imptotal, imppagado, hora, ";
    $sqlC .= " condpago, igv, redondeo, subtotal, propietario, direccion, nrodocumento, eventual, ";
    $sqlC .= " nroprepago, fechareg, anio, mes, glosa, nroinspeccion, nrocredito, ticket_serie, ticket_numero) ";
    $sqlC .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nropago.", ".$car.", ".$nrocaja.", ".$formapago.", ".$idusuario.", '".$ImpTT."', '".$ImpPG."', '".$hora."', ";
    $sqlC .= " ".$condpago.", '".$igv."', '".$redondeo."', '".$ImpST."', '".$propietario."', '".$direccion."', '".$docidentidad."', ".$eventualtext.", ";
    $sqlC .= " ".$nroprepago.", '".$fechareg."', '".$anio."', '".$mes."', '".$glosa."', ".$nroinspeccion.", ".$nrocargo.", '".$Ticket_Serie."', '".$Ticket_Numero."');";

    $result = $conexion->prepare($sqlC);

    $result->execute(array());

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();

        $mensaje = "165 Error INSERT CobranzaCabPagos";
        $data['mensaje'] = $mensaje;
        $data['res'] = 2;

        die(json_encode($data));
    }
    
    if ($nroprepago == 0) //---inicio de validacion 
	{
        for ($i = 1; $i <= $contador; $i++) {
            if (isset($_POST["nrofacturacion".$i]))
			{
                $nrofacturacion = $_POST["nrofacturacion".$i];

                if ($nrofacturacion != 0)
				{
                    $sqlD = "SELECT d.codconcepto, d.categoria, d.codtipodeuda, (d.importe - (d.importerebajado + d.imppagado)) AS importe, ";
                    $sqlD .= " c.coddocumento, c.nrodocumento, c.serie, d.nrocredito, d.nrorefinanciamiento, d.nrocuota, d.item ";
                    $sqlD .= "FROM facturacion.detfacturacion AS d ";
                    $sqlD .= " INNER JOIN facturacion.cabfacturacion AS c ON(d.codemp = c.codemp AND d.codsuc = c.codsuc AND d.codciclo = c.codciclo ";
                    $sqlD .= " AND d.nrofacturacion = c.nrofacturacion AND d.nroinscripcion = c.nroinscripcion) ";
                    $sqlD .= "WHERE d.codemp = 1 AND d.codsuc = :codsuc AND d.nroinscripcion = :nroinscripcion ";
                    $sqlD .= " AND d.estadofacturacion <> 2 AND d.estadofacturacion <> 4 ";
                    $sqlD .= " AND d.codtipodeuda = :tipodeuda AND d.nrofacturacion = :nrofacturacion ";
                    $sqlD .= "ORDER BY d.codconcepto ";

                    $result = $conexion->prepare($sqlD);

                    $result->execute(array(":codsuc" => $codsuc,
                        ":nroinscripcion" => $nroinscripcion,
                        ":tipodeuda" => $_POST["codtipodeuda".$i],
                        ":nrofacturacion" => $nrofacturacion));

                    if ($result->errorCode() != '00000') {
                        $conexion->rollBack();

                        $mensaje = "210 Error SELECT detfacturacion";
                        $data['res'] = 2;
                        $data['mensaje'] = $mensaje;

                        die(json_encode($data));
                    }

                    $itemsV = $result->fetchAll();

                    foreach ($itemsV as $rowD) {
                        $sqlCat = "SELECT catcobra.codcategoria, cobra.descripcion ";
                        $sqlCat .= "FROM cobranza.catcobranza_conceptos AS catcobra ";
                        $sqlCat .= " INNER JOIN cobranza.categoriacobranza AS cobra ON(catcobra.codcategoria = cobra.codcategoria) ";
                        $sqlCat .= "WHERE catcobra.codconcepto = ".$rowD["codconcepto"]." ";
						$sqlCat .= " AND cobra.categoria = ".$rowD["categoria"]." ";
						$sqlCat .= " AND cobra.codtipodeuda = ".$rowD["codtipodeuda"]." AND codsuc = ".$codsuc." ";

                        $result = $conexion->prepare($sqlCat);

                        $result->execute(array());

                        if ($result->errorCode() != '00000') {
                            $conexion->rollBack();

                            $mensaje = "236 Error SELECT catcobranza_conceptos";
                            $data['res'] = 2;
                            $data['mensaje'] = $mensaje;

                            die(json_encode($data));
                        }

                        $itemsCat = $result->fetch();

                        if ($itemsCat["codcategoria"] == '') {
                            $data['res'] = 0;
                            $data['Mensaje'] = 'Categoria Nulo';
                            $data['Concepto'] = $rowD["codconcepto"];
                            $data['Categoria'] = $rowD["categoria"];
                            $data['Tipo Deuda'] = $rowD["codtipodeuda"];

                            die(json_encode($data));
                            //die("Categoria Nulo, Concepto: ".$rowD["codconcepto"].", Categoria:".$rowD["categoria"].", Tipo Deuda:".$rowD["codtipodeuda"]);
                        }

                        $insD = "INSERT INTO cobranza.detpagos(codemp, codsuc, nrofacturacion, nroinscripcion, nropago,";
                        $insD .= " codconcepto, importe, codtipodeuda, tipopago, codcategoria, coddocumento, serie, nrodocumento, ";
                        $insD .= " categoria, detalle, anio, mes, item) ";
                        $insD .= "VALUES(".$codemp.", ".$codsuc.", ".$nrofacturacion.", ".$nroinscripcion.", ".$nropago.", ";
                        $insD .= " ".$rowD["codconcepto"].", '".str_replace(",", "", $rowD["importe"])."', ".$rowD["codtipodeuda"].", ";
                        $insD .= " 0, ".$itemsCat["codcategoria"].", ".$rowD["coddocumento"].", '".$rowD["serie"]."', ".$rowD["nrodocumento"].", ";
                        $insD .= " ".$rowD["categoria"].", '".$_POST["detallex".$i]."', '".$_POST["anioy".$i]."', '".$_POST["mesy".$i]."', ".$rowD['item'].")";

                        //echo $insD."<br>";

                        $result = $conexion->prepare($insD);

                        $result->execute(array());

                        if ($result->errorCode() != '00000') {
                            $conexion->rollBack();

                            $mensaje = "292 Error INSERT detpagos";
                            $data['res'] = 2;
                            $data['mensaje'] = $mensaje;

                            die(json_encode($data));
                        }

                        $updfacturacion = "UPDATE facturacion.detfacturacion ";
                        $updfacturacion .= "SET imppagado = imppagado + :imppagado ";
                        $updfacturacion .= "WHERE codemp = 1 AND codsuc = :codsuc ";
                        $updfacturacion .= " AND nrofacturacion = :nrofacturacion AND nroinscripcion = :nroinscripcion ";
                        $updfacturacion .= " AND codconcepto = :codconcepto AND codtipodeuda = ".$rowD["codtipodeuda"]." AND item = ".$rowD['item'];

                        $result = $conexion->prepare($updfacturacion);

                        $result->execute(array(":imppagado" => str_replace(",", "", $rowD["importe"]),
                            ":codsuc" => $codsuc,
                            ":nrofacturacion" => $_POST["nrofacturacion".$i],
                            ":nroinscripcion" => $nroinscripcion,
                            ":codconcepto" => $rowD["codconcepto"]));

                        if ($result->errorCode() != '00000') {
                            $conexion->rollBack();

                            $mensaje = "310 Error UPDATE detfacturacion";
                            $data['res'] = 2;
                            $data['mensaje'] = $mensaje;

                            die(json_encode($data));
                        }


                        $sqlfacturacion = "SELECT (importe - (importerebajado + imppagado)) AS importe, imppagado ";
                        $sqlfacturacion .= "FROM facturacion.detfacturacion ";
                        $sqlfacturacion .= "WHERE codemp = 1 AND codsuc = :codsuc ";
                        $sqlfacturacion .= " AND nrofacturacion = :nrofacturacion AND nroinscripcion = :nroinscripcion ";
                        $sqlfacturacion .= " AND codconcepto = :codconcepto AND codtipodeuda = ".$rowD["codtipodeuda"]." AND item = ".$rowD['item'];

                        $result = $conexion->prepare($sqlfacturacion);

                        $result->execute(array(":codsuc" => $codsuc,
                            ":nrofacturacion" => $_POST["nrofacturacion".$i],
                            ":nroinscripcion" => $nroinscripcion,
                            ":codconcepto" => $rowD["codconcepto"]));

                        if ($result->errorCode() != '00000') {
                            $conexion->rollBack();

                            $mensaje = "343 Error SELECT detfacturacion";
                            $data['res'] = 2;
                            $data['mensaje'] = $mensaje;

                            die(json_encode($data));
                        }

                        $itemsF = $result->fetchAll();

                        foreach ($itemsF as $rowF) {
                            //if($rowF["importe"]==$rowF["imppagado"])
                            if ($rowF["importe"] == 0) {
                                //---Actualiza el Detalle de la Facturacion
                                $updF = "UPDATE facturacion.detfacturacion SET estadofacturacion = 2 ";
                                $updF .= "WHERE codemp = 1 AND codsuc = :codsuc ";
                                $updF .= " AND nrofacturacion = :nrofacturacion AND nroinscripcion = :nroinscripcion ";
                                $updF .= " AND codconcepto = :codconcepto AND codtipodeuda = ".$rowD["codtipodeuda"]." AND item = ".$rowD['item'];

                                $result = $conexion->prepare($updF);

                                $result->execute(array(":codsuc" => $codsuc,
                                    ":nrofacturacion" => $_POST["nrofacturacion".$i],
                                    ":nroinscripcion" => $nroinscripcion,
                                    ":codconcepto" => $rowD["codconcepto"]));

                                if ($result->errorCode() != '00000') {
                                    $conexion->rollBack();

                                    $mensaje = "372 Error UPDATE detfacturacion";
                                    $data['res'] = 2;
                                    $data['mensaje'] = $mensaje;

                                    die(json_encode($data));
                                }

                                //---Actualiza la Cabecera de la Facturacion
                                $updCAB = "UPDATE facturacion.cabfacturacion SET estadofacturacion = 2 ";
                                $updCAB .= "WHERE codemp = 1 AND codsuc = :codsuc ";
                                $updCAB .= " AND nrofacturacion = :nrofacturacion AND nroinscripcion = :nroinscripcion";

                                $result = $conexion->prepare($updCAB);

                                $result->execute(array(":codsuc" => $codsuc,
                                    ":nrofacturacion" => $nrofacturacion,
                                    ":nroinscripcion" => $nroinscripcion));

                                if ($result->errorCode() != '00000') {
                                    $conexion->rollBack();

                                    $mensaje = "493 Error UPDATE cabfacturacion";
                                    $data['res'] = 2;
                                    $data['mensaje'] = $mensaje;

                                    die(json_encode($data));
                                }
                            }
                        }

                        //ACTUALIZAR CUOTA DE CREDITO
                        if ($rowD['nrocredito'] != '' && $rowD['nrocredito'] != 0) {
                            if ($rowD["codtipodeuda"] == 9) {//CARGOS
                                $sql = "UPDATE facturacion.detvarios ";
                                $sql .= "SET estadocuota = 3, fechapago = '".$fechareg."' ";
                                $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrocredito = ".$rowD['nrocredito']." AND nrocuota = ".$rowD['nrocuota']." ";

                                $result = $conexion->query($sql);

                                if (!$result) {
                                    $conexion->rollBack();

                                    $mensaje = "420 Error UPDATE detcreditos";
                                    $data['res'] = 2;

                                    die(json_encode($data));
                                }

                                $sql = "UPDATE facturacion.cabvarios ";
                                $sql .= "SET estareg = 3 ";
                                $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrocredito = ".$rowD['nrocredito']." ";
                            } else {
                                $sql = "UPDATE facturacion.detcreditos ";
                                $sql .= "SET estadocuota = 3, fechapago = '".$fechareg."' ";
                                $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrocredito = ".$rowD['nrocredito']." AND nrocuota = ".$rowD['nrocuota']." ";
                            }

                            $result = $conexion->query($sql);

                            if (!$result) {
                                $conexion->rollBack();

                                $mensaje = "443 Error UPDATE detcreditos";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }
                        }

                        if ($rowD['nrorefinanciamiento'] != '' && $rowD['nrorefinanciamiento'] != 0) {
                            $sql = "UPDATE facturacion.detrefinanciamiento ";
                            $sql .= "SET estadocuota = 3, fechapago = '".$fechareg."' ";
                            $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrorefinanciamiento = ".$rowD['nrorefinanciamiento']." AND nrocuota = ".$rowD['nrocuota']." ";

                            $result = $conexion->query($sql);

                            if (!$result) {
                                $conexion->rollBack();

                                $mensaje = "464 Error UPDATE detrefinanciamiento";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }
                        }
                    }//FOR DETALE DE FACTURACION
                } else {
                    //SIN NUMERO DE FACTURACION
                    $npresupuesto = $_POST["npresupuesto".$i] != "" ? $_POST["npresupuesto".$i] : 0;
                    $npresupuesto_con_expediente = (isset($_POST["codexpediente".$i])) ? $_POST["codexpediente".$i] : 0;
                    $npresupuesto_con_expediente1 = $_POST["codexpediente1"];

                    $Flag = false;
                    $UpdCorrelativo = true;
                    $codconceptox = $_POST["codconceptox".$i]; //
                    //VERIFICAR SI ES UN PRESUPUESTO AL CREDITO Y ACTUALIZAR DOCUMENTO A NOTA DE CREDITO
                    if ($npresupuesto != 0) {
                        $Sql = "SELECT tipopago, totalpresupuesto, igv, redondeo, subtotal, nrocuota ";
                        $Sql .= "FROM solicitudes.cabpresupuesto ";
                        $Sql .= "WHERE codemp = :codemp AND codsuc = :codsuc AND nropresupuesto = :nropresupuesto";

                        $result = $conexion->prepare($Sql);

                        $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nropresupuesto" => $npresupuesto));

                        $itempres = $result->fetch();

                        if ($itempres['tipopago'] == 2) {//AL CREDITO
                            //INSERTAR UN PREPAGO POR EL MONTO TOTAL
                            $sqlC = "INSERT INTO cobranza.cabprepagos(codemp, codsuc, nroinscripcion, nroprepago, car, codformapago, creador, fechareg, ";
                            $sqlC .= " subtotal, hora, codciclo, condpago, igv, redondeo, imptotal, eventual, propietario, direccion, documento, glosa, nroinspeccion, estado) ";
                            $sqlC .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$car.", ".$formapago.", ".$idusuario.", '".$fechareg."', ";
                            $sqlC .= " '".str_replace(",", "", $itempres['subtotal'])."', '".$hora."', 1, ".$condpago.", '".str_replace(",", "", $itempres['igv'])."', ";
							$sqlC .= " '".str_replace(",", "", $itempres['redondeo'])."', '".str_replace(",", "", $itempres['totalpresupuesto'])."', '".$eventualtext."', ";
							$sqlC .= " '".$propietario."', '".$direccion."', '".$docidentidad."', '".$glosa."', 0, 2)";

                            $resultC = $conexion->prepare($sqlC);

                            $resultC->execute(array());

                            if ($resultC->errorCode() != '00000') {
                                $conexion->rollBack();

                                $mensaje = "534 Error update cabpagos";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }

                            $sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, codconcepto, importe, detalle, tipopago, ";
                            $sqlD .= " nrofacturacion, coddocumento, nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
                            $sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$_POST["codconceptox".$i].", ";
							$sqlD .= " '".str_replace(",", "", $itempres['totalpresupuesto'])."', '".$_POST["detalle".$i]."', 0, ";
                            $sqlD .= " ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ".$_POST["codtipodeuda".$i].", ";
							$sqlD .= " '".$_POST["anioy".$i]."', '".$_POST["mesy".$i]."', 3, ".$_POST["codcategoria".$i].", ".$npresupuesto.")";
							
							//$data['prueba'] = $sqlD;

                            $documento = 13; //DOCUMENTO BOLETA POR EL MONTO TOTAL
                            $DocSeries = $objFunciones->GetSeries($codsuc, $documento);
                            $seriedoc = $DocSeries["serie"];
                            $numerodoc = $DocSeries["correlativo"];

                            $resultD = $conexion->prepare($sqlD);

                            $resultD->execute(array());

                            if ($resultD->errorCode() != '00000') {
                                $conexion->rollBack();

                                $mensaje = "588 Error update cabpagos";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }

                            //ACTUALIZAR CORRELATIVO
                            $sql = "UPDATE reglasnegocio.correlativos ";
                            $sql .= "SET correlativo = correlativo + 1 ";
                            $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrocorrelativo = ".$documento;

                            $result = $conexion->query($sql);

                            if (!$result) {
                                $conexion->rollBack();

                                $mensaje = "625 Error UPDATE correlativos";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }

                            //INSERTAR UN PREPAGO POR EL MONTO TOTAL
                            $documento = 16; //AQUI PONER DOCUMENTO DE NOTA DE PAGO
                            $codconceptox = 10; //CONCEPTO DEL CREDITO
                        }
                    }

                    if ($nrocargo == 0) {
                        //OBTENER CORRELATIVOS ACTUALES
                        $DocSeries = $objFunciones->GetSeries($codsuc, $documento);
                        $seriedoc = $DocSeries["serie"];
                        $numerodoc = $DocSeries["correlativo"];
                        //OBTENER CORRELATIVOS ACTUALES
                    } else {
                        $seriedoc = '001';
                        $numerodoc = $nrocargo;
                        $documento = 0;
                        $UpdCorrelativo = false;
                    }
                    //-------que no tiene nro. de facturacion----
                    // Cuando el expediente se debe registrar en detpagos
                    if (!empty($npresupuesto)) {
                        $npresupuesto_mod = $npresupuesto;
                    } else {
                        $npresupuesto_mod = $npresupuesto_con_expediente;
                        $codexpediente = $npresupuesto_con_expediente;
                    }
                    
                    // CON NRO. DE INSPECCION
                    if($codexpedienteV == 0 && $nroinspeccion<>0)
                    {
                        $VerServicio = "SELECT codconcepto, item, subtotal, igv, redondeo, imptotal ";
						$VerServicio .= "FROM catastro.detsolicitudinspeccion ";
						$VerServicio .= "WHERE codemp = 1 ";
						$VerServicio .= " AND codsuc = ".$codsuc." ";
						$VerServicio .= " AND nroinspeccion = ".$nroinspeccion." ";
						$VerServicio .= "ORDER BY item ";
						
                        $result = $conexion->prepare($VerServicio);
                        $result->execute(array());
                        
                        if ($result->errorCode() != '00000') {
                            $conexion->rollBack();

                            $mensaje = "Error SELECT detsolicitudinspeccion";
                            $data['res'] = 2;
                            $data['mensaje'] = $mensaje;
                            die(json_encode($data));
                        }
                        
                        $items = $result->fetchAll();
						
                        foreach($items as $row)
                        {
                            $insdetpagos = "INSERT INTO cobranza.detpagos(codemp, codsuc, nrofacturacion, nroinscripcion, nropago, codconcepto, ";
                            $insdetpagos .= " importe, codtipodeuda, tipopago, codcategoria, ";
                            $insdetpagos .= " coddocumento, serie, nrodocumento, categoria, detalle, anio, mes, nropresupuesto, item) ";
                            $insdetpagos .= "VALUES(".$codemp.", ".$codsuc.", ".$nrofacturacion.", ".$nroinscripcion.", ".$nropago.", ".$row['codconcepto'].", ";
                            $insdetpagos .= " '".$row['subtotal']."', ".$_POST["codtipodeuda".$i].", 0, ".$_POST["codcategoria".$i].", ";
                            $insdetpagos .= " ".$documento.", '".$seriedoc."', ".$numerodoc.", 3, '".$_POST["detalle".$i]."', '".$_POST["anioy".$i]."', ";
                            $insdetpagos .= " ".$_POST["mesy".$i].", ".$npresupuesto_mod.", ".$row['item'].");";
                            
                            $result = $conexion->prepare($insdetpagos);
                            $result->execute(array());
                            
                            if ($result->errorCode() != '00000') {
                                $conexion->rollBack();

                                $mensaje = "607 Error INSERT Detpagos - Inspeccion<br>";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje." ".$codexpediente;

                                die(json_encode($data));
                            }
                        	$i = $row['item']; 
                        }
						
                        // CON NRO. DE INSPECCION
                        
                    }else
                        {

                            $insdetpagos = "INSERT INTO cobranza.detpagos(codemp, codsuc, nrofacturacion, nroinscripcion, nropago, codconcepto, ";
                            $insdetpagos .= " importe, codtipodeuda, tipopago, codcategoria, ";
                            $insdetpagos .= " coddocumento, serie, nrodocumento, categoria, detalle, anio, mes, nropresupuesto, item) ";
                            $insdetpagos .= "VALUES(".$codemp.", ".$codsuc.", ".$nrofacturacion.", ".$nroinscripcion.", ".$nropago.", ".$codconceptox.", ";
                            $insdetpagos .= " '".str_replace(",", "", number_format($_POST["imptotal".$i], 2))."', ".$_POST["codtipodeuda".$i].", 0, ".$_POST["codcategoria".$i].", ";
                            $insdetpagos .= " ".$documento.", '".$seriedoc."', ".$numerodoc.", 3, '".$_POST["detalle".$i]."', '".$_POST["anioy".$i]."', ";
                            $insdetpagos .= " ".$_POST["mesy".$i].", ".$npresupuesto_mod.", ".$i.")";

                            //echo $insdetpagos."<br>";
                            $result = $conexion->prepare($insdetpagos);
                            $result->execute(array());

                            if ($result->errorCode() != '00000') {
                                $conexion->rollBack();

                                $mensaje = "676 Error INSERT detpagos";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }
                        }
                    
                    if ($npresupuesto != 0) {
                        $updP = "UPDATE solicitudes.cabpresupuesto ";
                        $updP .= "SET estadopresupuesto = 4, nropago = :nropago ";
                        $updP .= "WHERE codemp = :codemp AND codsuc = :codsuc AND nropresupuesto = :nropresupuesto";

                        $result = $conexion->prepare($updP);

                        $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nropresupuesto" => $npresupuesto, ":nropago" => $nropago));

                        if ($result->errorCode() != '00000') {
                            $conexion->rollBack();

                            $mensaje = "715 Error UPDATE cabpresupuesto";
                            $data['res'] = 2;
                            $data['mensaje'] = $mensaje;

                            die(json_encode($data));
                        }

                        //VERIFICAR SI ES UN PRESUPUESTO AL CREDITO
                        $Sql = "SELECT tipopago, totalpresupuesto, igv, redondeo, subtotal, nrocuota, interes ";
                        $Sql .= "FROM solicitudes.cabpresupuesto ";
                        $Sql .= "WHERE codemp = :codemp AND codsuc = :codsuc AND nropresupuesto = :nropresupuesto";

                        $result = $conexion->prepare($Sql);

                        $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nropresupuesto" => $npresupuesto));

                        $itempres = $result->fetch();

                        if ($itempres['tipopago'] == 2) {//AL CREDITO
                            $concredito = true;
                            //SE TIENE QUE INSERTAR EL PRECREDITO EN CREDITOS
                            $sqlC = "INSERT INTO facturacion.cabcreditos(codemp, codsuc, nrocredito, nroinscripcion, imptotal, igv, redondeo, subtotal, codconcepto, ";
                            $sqlC .= " nropresupuesto, observacion, creador, fechareg, nropago, nroprepago) ";
                            $sqlC .= "VALUES(:codemp, :codsuc, :nrocredito, :nroinscripcion, :imptotal, :igv, :redondeo, :subtotal, :codconcepto, ";
                            $sqlC .= " :nropresupuesto, :observacion, :creador, :fechareg, :nropago, :nroprepago)";

                            $resultC = $conexion->prepare($sqlC);

                            $resultC->execute(array(":codemp" => $codemp,
                                ":codsuc" => $codsuc,
                                ":nrocredito" => $nrocredito,
                                ":nroinscripcion" => $nroinscripcion,
                                ":imptotal" => str_replace(",", "", $itempres['totalpresupuesto']),
                                ":igv" => str_replace(",", "", $itempres['igv']),
                                ":redondeo" => str_replace(",", "", $itempres['redondeo']),
                                ":subtotal" => str_replace(",", "", $itempres['subtotal']),
                                ":codconcepto" => $_POST["codconceptox".$i],
                                ":nropresupuesto" => $nropresupuesto,
                                ":observacion" => $glosa,
                                ":creador" => $idusuario,
                                ":fechareg" => $fechareg,
                                ":nropago" => $nropago,
                                ":nroprepago" => $nroprepagocre,
                                ":interes" => $itempres['interes']));

                            if ($resultC->errorCode() != '00000') {
                                $conexion->rollBack();

                                $mensaje = "759 Error reclamos";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }

                            //////CUOTA INICIAL
                            $nrofacturacionactual = $_POST['nrofacturacionactual'];
                            $aniocredito = $_POST['anio'];
                            $mescredito = $_POST['mes'];

                            $sqlD = "INSERT INTO facturacion.detcreditos(codemp, codsuc, nrocredito, nrocuota, totalcuotas, fechavencimiento, ";
                            $sqlD .= " subtotal, igv, redondeo, imptotal, nrofacturacion, tipocuota, anio, mes, interes, estadocuota) ";
                            $sqlD .= "VALUES(:codemp, :codsuc, :nrocredito, :nrocuota, :totalcuotas, :fechavencimiento, ";
                            $sqlD .= " :subtotal, :igv, :redondeo, :imptotal, :nrofacturacion, :tipocuota, :anio, :mes, :interes, :estadocuota)";

                            $resultD = $conexion->prepare($sqlD);

                            $resultD->execute(array(":codemp" => $codemp,
                                ":codsuc" => $codsuc,
                                ":nrocredito" => $nrocredito,
                                ":nrocuota" => 1,
                                ":totalcuotas" => $itempres['nrocuota'],
                                ":fechavencimiento" => $fechareg,
                                ":subtotal" => str_replace(",", "", $_POST["imptotal".$i]),
                                ":igv" => 0,
                                ":redondeo" => 0,
                                ":interes" => 0,
                                ":imptotal" => str_replace(",", "", $_POST["imptotal".$i]),
                                ":nrofacturacion" => $nrofacturacionactual,
                                ":tipocuota" => 1,
                                ":estadocuota" => 3,
                                ":anio" => $aniocredito,
                                ":mes" => $mescredito));

                            if ($resultD->errorCode() != '00000') {
                                $conexion->rollBack();

                                $mensaje = "808 Error reclamos";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }

                            //////CUOTA INICIAL
                            $sqlD = "SELECT * FROM solicitudes.precreditos WHERE codsuc = ? AND nropresupuesto = ? ";

                            $consultaDp = $conexion->prepare($sqlD);

                            $consultaDp->execute(array($codsuc, $npresupuesto));
                            $itemsDp = $consultaDp->fetchAll();

                            $nrocuotacredito = 1;
                            $mescredito++;

                            // die(print_r(array($codsuc, $npresupuesto)));
                            if ($mescredito > 12) {
                                $mescredito = 1;
                                $aniocredito++;
                            }

                            foreach ($itemsDp as $rowDp) {
                                $nrocuotacredito++;
                                $nrofacturacionactual++;
                                $fechavencimientocredito = FechaVecimiento($mescredito, $aniocredito);

                                $sqlD = "INSERT INTO facturacion.detcreditos(codemp, codsuc, nrocredito, nrocuota, totalcuotas, fechavencimiento, ";
                                $sqlD .= " subtotal, igv, redondeo, imptotal, nrofacturacion, tipocuota, anio, mes, interes, estadocuota) ";
                                $sqlD .= "VALUES(:codemp, :codsuc, :nrocredito, :nrocuota, :totalcuotas, :fechavencimiento, ";
                                $sqlD .= " :subtotal, :igv, :redondeo, :imptotal, :nrofacturacion, :tipocuota, :anio, :mes, :interes, :estadocuota)";

                                $resultD = $conexion->prepare($sqlD);

                                $resultD->execute(array(":codemp" => $codemp,
                                    ":codsuc" => $codsuc,
                                    ":nrocredito" => $nrocredito,
                                    ":nrocuota" => $nrocuotacredito,
                                    ":totalcuotas" => $itempres['nrocuota'],
                                    ":fechavencimiento" => $objFunciones->CodFecha($fechavencimientocredito),
                                    ":subtotal" => str_replace(",", "", $rowDp["cuotamensual"]),
                                    ":igv" => str_replace(",", "", $rowDp["igv"]),
                                    ":redondeo" => str_replace(",", "", $rowDp["redondeo"]),
                                    ":interes" => $rowDp["interes"],
                                    ":imptotal" => str_replace(",", "", $rowDp["imptotal"]),
                                    ":nrofacturacion" => $nrofacturacionactual,
                                    ":tipocuota" => 0,
                                    ":estadocuota" => 0,
                                    ":anio" => $aniocredito,
                                    ":mes" => $mescredito));

                                if ($resultD->errorCode() != '00000') {
                                    $conexion->rollBack();

                                    $mensaje = "867 Error INSERT detcreditos";
                                    $data['res'] = 2;
                                    $data['mensaje'] = $mensaje;

                                    die(json_encode($data));
                                }

                                $mescredito++;

                                if ($mescredito > 12) {
                                    $mescredito = 1;
                                    $aniocredito++;
                                }
                            }

                            //SE TIENE QUE INSERTAR EL PRECREDITO EL CREDITOS
                            //ACTUALIZAR NRO CREDITO
                            if ($glosa == '') {
                                $glosa = 'PAGO DE CUOTA INICIAL, CONVENIO NRO. '.$nrocredito;
                            }

                            $sql = "UPDATE cobranza.cabpagos ";
                            $sql .= "SET nrocredito = ".$nrocredito.", glosa = '".$glosa."' ";
                            $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nropago = ".$nropago." ";

                            $result = $conexion->query($sql);

                            if (!$result) {
                                $conexion->rollBack();

                                $mensaje = "917 Error UPDATE cabpagos";
                                $data['res'] = 2;
                                $data['mensaje'] = $mensaje;

                                die(json_encode($data));
                            }
                        }
                        //VERIFICAR SI ES UN PRESUPUESTO AL CREDITO
                    }
                }
            }
        }
    } else {
        //==> DESDE ACA SE INSERTA PAGOS CON N° PREPAGO

        $sqlD = "SELECT d.nrofacturacion, d.codconcepto, d.categoria as categoria, d.importe, d.coddocumento, d.serie, d.nrodocumento,
                    d.nropresupuesto, d.codtipodeuda, d.detalle, d.anio, d.mes ";
        $sqlD .= "FROM cobranza.detprepagos as d ";
        $sqlD .= "inner join facturacion.conceptos as c on(d.codconcepto=c.codconcepto and d.codemp=c.codemp and d.codsuc=c.codsuc) ";
        $sqlD .= "inner join public.tipodeuda as td on(d.codtipodeuda=td.codtipodeuda)";
        $sqlD .= "where d.nroprepago=:nroprepago and d.codsuc=:codsuc AND d.nroinscripcion=:nroinscripcion";

        $result = $conexion->prepare($sqlD);
        $result->execute(array(":nroprepago" => $nroprepago, ":codsuc" => $codsuc, ":nroinscripcion" => $nroinscripcion));

        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "362 Error SELECT detprepagos";
            $data['res'] = 2;
            $data['mensaje'] = $mensaje;
            die(json_encode($data));
        }
        $itemsR = $result->fetchAll();

        $item = 1;
        foreach ($itemsR as $rowD) {
            $nropresupuesto = $rowD['nropresupuesto'];

            if ($rowD["nrofacturacion"] != 0) {
                $sqlCT = "SELECT catcobra.codcategoria, cobra.descripcion ";
                $sqlCT .= "FROM cobranza.catcobranza_conceptos catcobra ";
                $sqlCT .= " INNER JOIN cobranza.categoriacobranza cobra ON(catcobra.codcategoria = cobra.codcategoria) ";
                $sqlCT .= "WHERE catcobra.codconcepto = ".$rowD["codconcepto"]." ";
                $sqlCT .= " and cobra.categoria = ".$rowD["categoria"]." ";
                $sqlCT .= " and cobra.codtipodeuda = ".$rowD["codtipodeuda"]." ";
                $sqlCT .= " and codsuc = ".$codsuc." ";

                $result = $conexion->prepare($sqlCT);
                $result->execute(array());

                if ($result->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "383 Error SELECT catcobranza_conceptos";
                    $data['res'] = 2;
                    $data['mensaje'] = $mensaje;
                    die(json_encode($data));
                }
                $itemsCT = $result->fetch();

                if (!empty($npresupuesto)) {
                    $npresupuesto_mod = $npresupuesto;
                } else {
                    $npresupuesto_mod = $npresupuesto_con_expediente;
                    $codexpediente = $npresupuesto_con_expediente;
                }

                $insD = "INSERT INTO cobranza.detpagos(codemp, codsuc, nrofacturacion, nroinscripcion, nropago,";
                $insD .= "codconcepto,importe,codtipodeuda,tipopago,codcategoria,coddocumento,serie,nrodocumento,categoria,detalle,anio,
                            mes,nropresupuesto,item) ";
                $insD .= "values(".$codemp.", ".$codsuc.", ".$rowD["nrofacturacion"].", ".$nroinscripcion.", ".$nropago.", ".$rowD["codconcepto"].",
								'".str_replace(",", "", $rowD["importe"])."', ".$rowD["codtipodeuda"].",
                    0, ".$itemsCT["codcategoria"].", ".$rowD["coddocumento"].", '".$rowD["serie"]."', '".$rowD["nrodocumento"]."', ".$rowD["categoria"].",
										'".$rowD["detalle"]."', '".$rowD["anio"]."', '".$rowD["mes"]."', ".intval($npresupuesto_mod).", ".$item.")";

                $result = $conexion->prepare($insD);
                $result->execute(array());

                if ($result->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "426 Error INSERT detpagos";
                    $data['res'] = 2;
                    $data['mensaje'] = $mensaje."<br>".$insD;
                    die(json_encode($data));
                }

                $updfacturacion = "UPDATE facturacion.detfacturacion set importeacta=importeacta+:importeacta";
                $updfacturacion .= ",imppagado=imppagado+:imppagado where codemp=1 and codsuc=:codsuc";
                $updfacturacion .= " and nrofacturacion=:nrofacturacion and nroinscripcion=:nroinscripcion";
                $updfacturacion .= " and codconcepto=:codconcepto";

                $result = $conexion->prepare($updfacturacion);
                $result->execute(array(":importeacta" => str_replace(",", "", $rowD["importe"]),
                    ":imppagado" => str_replace(",", "", $rowD["importe"]),
                    ":codsuc" => $codsuc,
                    ":nrofacturacion" => $rowD["nrofacturacion"],
                    ":nroinscripcion" => $nroinscripcion,
                    ":codconcepto" => $rowD["codconcepto"]));
                if ($result->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "445 Error INSERT detpagos";
                    $data['res'] = 2;
                    $data['mensaje'] = $mensaje;
                    die(json_encode($data));
                }

                $sqlfacturacion = "SELECT (importe-(importerebajado+imppagado)) as importe,imppagado
    									FROM facturacion.detfacturacion ";
                $sqlfacturacion .= " where codemp=1 and codsuc=:codsuc";
                $sqlfacturacion .= " and nrofacturacion=:nrofacturacion and nroinscripcion=:nroinscripcion";
                $sqlfacturacion .= " and codconcepto=:codconcepto";

                $result = $conexion->prepare($sqlfacturacion);
                $result->execute(array(":codsuc" => $codsuc,
                    ":nrofacturacion" => $rowD["nrofacturacion"],
                    ":nroinscripcion" => $nroinscripcion,
                    ":codconcepto" => $rowD["codconcepto"]));
                if ($result->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "463 Error SELECT detfacturacion";
                    $data['res'] = 2;
                    $data['mensaje'] = $mensaje;
                    die(json_encode($data));
                }
                $itemsF = $result->fetchAll();

                foreach ($itemsF as $rowF) {
                    if ($rowF["importe"] == 0) {
                        //---Actualiza el Detalle de la Facturacion
                        $updFCT = "UPDATE facturacion.detfacturacion set estadofacturacion=2";
                        $updFCT .= " where codemp=1 and codsuc=:codsuc";
                        $updFCT .= " and nrofacturacion=:nrofacturacion and nroinscripcion=:nroinscripcion";
                        $updFCT .= " and codconcepto=:codconcepto";

                        $result = $conexion->prepare($updFCT);
                        $result->execute(array(":codsuc" => $codsuc,
                            ":nrofacturacion" => $rowD["nrofacturacion"],
                            ":nroinscripcion" => $nroinscripcion,
                            ":codconcepto" => $rowD["codconcepto"]));
                        if ($result->errorCode() != '00000') {
                            $conexion->rollBack();
                            $mensaje = "486 Error UPDATE detfacturacion";
                            $data['res'] = 2;
                            $data['mensaje'] = $mensaje;
                            die(json_encode($data));
                        }
                        //---Actualiza la Cabecera de la Facturacion
                        $updCAB = "UPDATE facturacion.cabfacturacion set estadofacturacion=2";
                        $updCAB .= " where codemp=1 and codsuc=:codsuc";
                        $updCAB .= " and nrofacturacion=:nrofacturacion and nroinscripcion=:nroinscripcion";

                        $result = $conexion->prepare($updCAB);
                        $result->execute(array(":codsuc" => $codsuc,
                            ":nrofacturacion" => $rowD["nrofacturacion"],
                            ":nroinscripcion" => $nroinscripcion));
                        if ($result->errorCode() != '00000') {
                            $conexion->rollBack();
                            $mensaje = "502 Error UPDATE cabfacturacion";
                            $data['res'] = 2;
                            $data['mensaje'] = $mensaje;
                            die(json_encode($data));
                        }
                    }
                    //	}
                }
            } else {
                //==> ACA SE INSERTA PREPAGOS SIN NRO DE FACTURACION
                $Flag = false;

                //YA NO SE OBTENDRAN LOS CORRELATIVOS, POR QUE EL EL PREPAGO SE DA BOLETA

                $documento = $rowD['coddocumento'];
                $seriedoc = $rowD['serie'];
                $numerodoc = $rowD['nrodocumento'];

                $item++;

                if (empty($nroprepago)) {

                    if (!empty($npresupuesto)) {
                        $npresupuesto_mod = $npresupuesto;
                    } else {
                        $npresupuesto_mod = $npresupuesto_con_expediente;
                        $codexpediente = $npresupuesto_con_expediente;
                    }
                } else {
                    $npresupuesto_mod = $rowD["nropresupuesto"];
                    $npresupuesto_con_expediente1 = $rowD["nropresupuesto"];
                }

                $insdetpagos = "INSERT INTO cobranza.detpagos(codemp, codsuc, nrofacturacion, nroinscripcion, nropago, codconcepto, ";
                $insdetpagos .= " importe, codtipodeuda, tipopago, codcategoria, coddocumento, ";
                $insdetpagos .= " serie, nrodocumento, categoria, detalle, anio, mes, nropresupuesto, item) ";
                $insdetpagos .= " VALUES(".$codemp.", ".$codsuc.", ".$rowD["nrofacturacion"].", ".$nroinscripcion.", ".$nropago.", ".$rowD["codconcepto"].", ";
                $insdetpagos .= " '".str_replace(",", "", $rowD["importe"])."', ".$rowD["codtipodeuda"].", 0, ".$rowD["categoria"].", ".$documento.", ";
                $insdetpagos .= " '".$seriedoc."', ".$numerodoc.", 3, '".$rowD["detalle"]."', '".$rowD["anio"]."', '".$rowD["mes"]."', ";
                $insdetpagos .= " ".$npresupuesto_mod.", ".$item.")";

                $result = $conexion->prepare($insdetpagos);
                $result->execute(array());
                if ($result->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "561 Error INSERT detpagos";
                    $data['res'] = 2;
                    $data['mensaje'] = $mensaje;

                    var_dump($result->errorInfo());
                    die(json_encode($data));
                }


                //==> ACTUALIZAR ESTADO DEL PRESUPUESTO : PAGADO

                if ($nropresupuesto != 0) {

                    $updP = "UPDATE solicitudes.cabpresupuesto SET estadopresupuesto=4, nropago=:nropago
                    WHERE codemp=:codemp and codsuc=:codsuc and nropresupuesto=:nropresupuesto";

                    $result = $conexion->prepare($updP);
                    $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nropresupuesto" => $nropresupuesto, ":nropago" => $nropago));
                    if ($result->errorCode() != '00000') {
                        $conexion->rollBack();
                        $mensaje = "337 Error UPDATE cabpresupuesto";
                        $data['res'] = 2;
                        $data['mensaje'] = $mensaje;
                        die(json_encode($data));
                    }

                    $VerExp = " SELECT codexpediente FROM solicitudes.cabpresupuesto
                        WHERE codemp=:codemp and codsuc=:codsuc and nropresupuesto=:nropresupuesto";
                    $result = $conexion->prepare($VerExp);
                    $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nropresupuesto" => $nropresupuesto));

                    if ($result->errorCode() != '00000') {
                        $conexion->rollBack();
                        $mensaje = "337 Error SELECT verexpediente";
                        $data['res'] = 2;
                        $data['mensaje'] = $mensaje;
                        die(json_encode($data));
                    }

                    $itsEx = $result->fetch();

                    $codexpediente = $itsEx['codexpediente'];

                    $updP = "UPDATE solicitudes.expedientes SET estadoexpediente=4
                    WHERE codemp=:codemp and codsuc=:codsuc and codexpediente=:codexpediente";

                    $result = $conexion->prepare($updP);
                    $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":codexpediente" => $codexpediente));
                    if ($result->errorCode() != '00000') {
                        $conexion->rollBack();
                        $mensaje = "337 Error UPDATE expedientes";
                        $data['res'] = 2;
                        $data['mensaje'] = $mensaje;
                        die(json_encode($data));
                    }
                }
                //==> ACTUALIZAR ESTADO DEL PRESUPUESTO
                //==> ACTUALIZAR CUOTA INICIAL DEL CREDITO

                $sqlD = "SELECT c.nrocredito AS nrocredito FROM cobranza.cabprepagos as c ";
                $sqlD .= " WHERE c.nroprepago=:nroprepago AND c.codsuc=:codsuc ";
                $sqlD .= " AND c.nroinscripcion=:nroinscripcion ";

                $result = $conexion->prepare($sqlD);
                $result->execute(array(":nroprepago" => $nroprepago, ":codsuc" => $codsuc, ":nroinscripcion" => $nroinscripcion));
                $itemcredito = $result->fetch();

                if (!empty($itemcredito['nrocredito'])) {

                    // Actualizamos primero la cabecera
                    $sql = "UPDATE facturacion.cabvarios ";
                    $sql .= "SET estareg = 2 ";
                    $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrocredito = ".$itemcredito['nrocredito']." ";
                    $result = $conexion->query($sql);

                    if (!$result) {
                        $conexion->rollBack();

                        $mensaje = "500 Error UPDATE cabvarios desde prepagos";
                        $data['res'] = 2;
                        $data['mensaje'] = $mensaje;

                        die(json_encode($data));
                    }

                    // Actualizamos el detalle de detvarios

                    $sql = "UPDATE facturacion.detvarios ";
                    $sql .= "SET estadocuota = 3, fechapago = '".$fechareg."' ";
                    $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrocredito = ".$itemcredito['nrocredito']." ";

                    $result = $conexion->query($sql);

                    if (!$result) {
                        $conexion->rollBack();

                        $mensaje = "500 Error UPDATE detvarios";
                        $data['res'] = 2;
                        $data['mensaje'] = $mensaje;

                        die(json_encode($data));
                    }
                }
                //==> ACTUALIZAR CUOTA INICIAL DEL CREDITO
            }
        }
    }
} else {
    $concredito = false;
    //
    $sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
    $sql .= "FROM facturacion.periodofacturacion ";
    $sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = 1 AND facturacion = 0";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codsuc));

    $row = $consulta->fetch();

    $TasaInteres = $row['tasapromintant'] / 100;

    $Sql = "SELECT MAX(nrocredito) FROM facturacion.cabvarios WHERE codsuc = ".$codsuc;

    $rowC = $conexion->query($Sql)->fetch();

    $nrocredito = $rowC[0];

    if ($nrocredito == '' || $nrocredito == 0) {
        $nrocredito = 1;
    } else {
        $nrocredito++;
    }
    
    //
	$sqlC = "INSERT INTO facturacion.cabvarios";
    $sqlC .= "(codemp, codsuc, nrocredito, nroinscripcion, imptotal, igv, redondeo, subtotal, codconcepto, ";
    $sqlC .= " nropresupuesto, observacion, creador, fechareg, nropago, nroprepago, interes, codantiguo, propietario, direccion, hora) ";
    $sqlC .= "VALUES(".$codemp.", ".$codsuc.", ".$nrocredito.", ".$nroinscripcion.", '".str_replace(",", "", $_POST['total'])."', ";
	$sqlC .= " '".str_replace(",", "", $_POST['igv'])."', '".str_replace(",", "", $_POST['redondeo'])."', '".str_replace(",", "", $_POST['subtotal'])."', ";
	$sqlC .= " ".$_POST["codconceptox1"].", 0, '".$glosa."', ".$idusuario.", '".$fechareg."', 0, 0, '".$row['tasapromintant']."', ";
	$sqlC .= " '".$objFunciones->CodUsuario($codsuc, $nroinscripcion)."', '".$propietario."', '".$direccion."', '".date('H:i')."'); ";

    if (trim($glosa) == '') {
        $consultax = $conexion->prepare("SELECT * FROM facturacion.conceptos WHERE codconcepto = ? AND codsuc = ?");
        $consultax->execute(array($_POST["codconceptox1"], $codsuc));

        $rowx = $consultax->fetch();
        $glosa = $rowx['descripcion'];
    }

    $resultC = $conexion->prepare($sqlC);
    $resultC->execute(array());

    if ($resultC->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        $data['res'] = 2;
        die(json_encode($data));
    }

    //CUOTA INICIAL
    $nrofacturacionactual = $_POST['nrofacturacionactual'];
    $aniocredito = $_POST['anio'];
    $mescredito = $_POST['mes'];

    $nrocuotacredito = 1;

    $fechavencimientocredito = FechaVecimiento($mescredito, $aniocredito);
    $cuotamensual = str_replace(",", "", $_POST['total']) * (pow((1 + $TasaInteres), 1) * ($TasaInteres)) / (pow((1 + $TasaInteres), 1) - 1);
    $interes = str_replace(",", "", $_POST['total']) * $TasaInteres;
    $interes = number_format($interes, 1);
    $afecto_igv = 0;

    $vigv = $_POST['igv'];
    
    $imp = str_replace(",", "", $_POST['subtotal']) + $vigv + $interes;
    $redondeo = CalcularRedondeo($imp);
    //$Subtt = $_POST['subtotal']
    $imp = round($imp + $redondeo, 1);

	$sqlD = "INSERT INTO facturacion.detvarios";
	$sqlD .= "(codemp, codsuc, nrocredito, nrocuota, totalcuotas, fechavencimiento, ";
	$sqlD .= " subtotal, igv, redondeo, imptotal, nrofacturacion, tipocuota, anio, mes, interes, estadocuota) ";
	$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nrocredito.", ".$nrocuotacredito.", 1, '".$objFunciones->CodFecha($fechavencimientocredito)."', ";
	$sqlD .= " '".str_replace(",", "", $_POST['subtotal'])."', '".str_replace(",", "", $vigv)."', '".str_replace(",", "", $redondeo)."', ";
	$sqlD .= " '".str_replace(",", "", $imp)."', ".$nrofacturacionactual.", 0, '".$aniocredito."', '".$mescredito."', '".$interes."', 0); ";

    $resultD = $conexion->prepare($sqlD);
    $resultD->execute(array());
	
    if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        $data['res'] = 2;
        die(json_encode($data));
    }
    //creditos detalle de colaterales

	for ($i = 1; $i <= $contador; $i++)
	{
		if (isset($_POST["codconceptox".$i]))
		{
			$igvv = 0;
			$redd = 0;
			
			$tott = $_POST['imptotal'.$i] + $igvv + $redd;
			
			$sqlD = "INSERT INTO facturacion.detcabvarios";
			$sqlD .= " (codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nrocredito.", ".$_POST["codconceptox".$i].", ".$i.", ";
			$sqlD .= " '".str_replace(",", "", $_POST['imptotal'.$i])."', '".$igvv."', '".$redd."', '".str_replace(",", "", $tott)."');";

            $resultD = $conexion->prepare($sqlD);
            $resultD->execute(array());
			
            if ($resultD->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }
}

//---Actualiza los Montos en las Cabeceras---
$imptotal = 0;
$impredondeo = 0;
$impigv = 0;
//ACTUALIZA CORRELATIVOS//
if ($UpdCorrelativo) {
    $sql = "UPDATE  reglasnegocio.correlativos
	SET  correlativo = correlativo+1
	WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocorrelativo=".$documento."";
    $result = $conexion->query($sql);
    if (!$result) {
        $conexion->rollBack();
        $mensaje = "619 Error UPDATE correlativos";
        $data['res'] = 2;
        $data['mensaje'] = $mensaje;
        die(json_encode($data));
    }
}
//ACTUALIZAR CREDITO
if ($concredito) {
    $objFunciones->setCorrelativosVarios(8, $codsuc, "UPDATE", $nrocredito);
    if ($condpago == 0) //CONTADO
        $objFunciones->setCorrelativosVarios(6, $codsuc, "UPDATE", $nroprepagocre);
    $concredito = 1;
}
//ACTUALIZAR PREPAGO
if ($nroprepago != 0) {
    $Sql = "update cobranza.cabprepagos set estado=2
     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$nroprepago .
            " AND nroinscripcion=".$nroinscripcion;
    $result = $conexion->query($Sql);
    if (!$result) {
        $conexion->rollBack();
        $mensaje = "619 Error UPDATE correlativos";
        $data['res'] = 2;
        $data['mensaje'] = $mensaje;
        die(json_encode($data));
    }

    //VERIFICAR SI EL PREPAGO A SIDO CON SOLICITUD DE INSPECCION
    $Sql = "SELECT nroinspeccion FROM cobranza.cabprepagos
     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$nroprepago .
            " AND nroinscripcion=".$nroinscripcion;
    $result = $conexion->query($Sql);
    $row = $result->fetch();
    //ACTUALIZAR A PAGADO EL PREPAGO
    if ($row['nroinspeccion'] != '' && $row['nroinspeccion'] != 0) {
        $upd = "update catastro.solicitudinspeccion set estado=2 ,cancelado=1
        where codemp=1 AND  codsuc=".$codsuc." and nroinspeccion=:nroinspeccion";
        $result = $conexion->prepare($upd);
        $result->execute(array(":nroinspeccion" => $row['nroinspeccion']));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error update cabpagos";
            $data['res'] = 2;
            die(json_encode($data));
        }
    }
    //ACTUALIZAR A PAGADO EL PREPAGO
    //VERIFICAR SI EL PREPAGO A SIDO CON SOLICITUD DE INSPECCION
    $Sql = "SELECT nrocredito,origen FROM cobranza.cabprepagos
     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$nroprepago .
            " AND nroinscripcion=".$nroinscripcion;
    $result = $conexion->query($Sql);
    $row = $result->fetch();
    //ACTUALIZAR A PAGADO EL PREPAGO
    if ($row['origen'] == 1) {
        if ($row['nrocredito'] != '' && $row['nrocredito'] != 0) {
            $sql = "UPDATE  facturacion.detcreditos
                SET  estadocuota = 3,fechapago='".$fechareg."'
                WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocredito=".$row['nrocredito']."
                AND nrocuota=1 AND tipocuota=1";
            $result = $conexion->query($sql);
            if (!$result) {
                $conexion->rollBack();
                $mensaje = "Error UPDATE detcreditos";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }
    if ($row['origen'] == 2) {
        if ($row['nrocredito'] != '' && $row['nrocredito'] != 0) {
            $sql = "UPDATE  facturacion.detrefinanciamiento
                SET  estadocuota = 3,fechapago='".$fechareg."'
                WHERE codemp=1 AND  codsuc=".$codsuc." AND nrorefinanciamiento=".$row['nrocredito']."
                AND nrocuota=1 AND estadocuota=0";
            $result = $conexion->query($sql);
            if (!$result) {
                $conexion->rollBack();
                $mensaje = "Error UPDATE detrefinanciamiento";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }
    if ($row['origen'] == 3) {
        if ($row['nrocredito'] != '' && $row['nrocredito'] != 0) {
            $Sql = "SELECT nrocuota FROM facturacion.cabcreditosadelantos
                    WHERE codsuc=".$codsuc." AND nrocredito=".$row['nrocredito']."
                    AND nroprepago=".$nroprepago."";
            $consultaAd = $conexion->prepare($Sql);
            $consultaAd->execute();
            $itemAD = $consultaAd->fetchAll();
            foreach ($itemAD as $rowAD) {
                $sql = "UPDATE  facturacion.detcreditos
                SET  estadocuota = 3,fechapago='".$fechareg."'
                WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocredito=".$row['nrocredito']."
                AND nrocuota=".$rowAD['nrocuota'];
                $result = $conexion->query($sql);
                if (!$result) {
                    $conexion->rollBack();
                    $mensaje = "Error UPDATE detcreditos";
                    $data['res'] = 2;
                    die(json_encode($data));
                }
            }
            //ACTUALIZAR TODO
            $Sql = "UPDATE facturacion.cabcreditosadelantos SET fechapago='".$fechareg."'
                    WHERE codsuc=".$codsuc." AND nrocredito=".$row['nrocredito']."
                    AND nroprepago=".$nroprepago."";
            $consultaAd = $conexion->prepare($Sql);
            $consultaAd->execute();
            $consultaAd->fetch();
            //ACTUALIZAR TODO
            //Actualizar Cabecera de crédito
        }
    }
    if ($row['origen'] == 4) {
        if ($row['nrocredito'] != '' && $row['nrocredito'] != 0) {
            $Sql = "SELECT nrocuota FROM facturacion.cabrefinanciamientosadelantos
                    WHERE codsuc=".$codsuc." AND nrorefinanciamiento=".$row['nrocredito']."
                    AND nroprepago=".$nroprepago."";
            $consultaAd = $conexion->prepare($Sql);
            $consultaAd->execute();
            $itemAD = $consultaAd->fetchAll();
            foreach ($itemAD as $rowAD) {
                $sql = "UPDATE  facturacion.detrefinanciamiento
                SET  estadocuota = 3,fechapago='".$fechareg."'
                WHERE codemp=1 AND  codsuc=".$codsuc." AND nrorefinanciamiento=".$row['nrocredito']."
                AND nrocuota=".$rowAD['nrocuota'];
                $result = $conexion->query($sql);
                if (!$result) {
                    $conexion->rollBack();
                    $mensaje = "Error UPDATE detcreditos";
                    $data['res'] = 2;
                    die(json_encode($data));
                }
            }
            //ACTUALIZAR TODO
            $Sql = "UPDATE facturacion.cabrefinanciamientosadelantos SET fechapago='".$fechareg."'
                    WHERE codsuc=".$codsuc." AND nrorefinanciamiento=".$row['nrocredito']."
                    AND nroprepago=".$nroprepago."";
            $consultaAd = $conexion->prepare($Sql);
            $consultaAd->execute();
            $consultaAd->fetch();
            //ACTUALIZAR TODO
        }
    }
    //ACTUALIZAR A PAGADO EL PREPAGO
}
//ACTUALIZAR SOLICITUD DE INSPECCION
if ($nroinspeccion != 0) {

    $upd = "update catastro.solicitudinspeccion set estado=2 ,cancelado=1
            where codemp=1 AND  codsuc=".$codsuc." and nroinspeccion=:nroinspeccion";
    $result = $conexion->prepare($upd);
    $result->execute(array(":nroinspeccion" => $nroinspeccion));
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error update cabpagos";
        $data['res'] = 2;
        die(json_encode($data));
    }
}

$codexpediente = (empty($npresupuesto_con_expediente1)) ? $codexpediente : $npresupuesto_con_expediente1;

if ($codexpediente != 0) {
    $Sql = "UPDATE  solicitudes.expedientes
            SET  estadoexpediente=4,nropago=:nropago
        WHERE codemp = :codemp AND codsuc = :codsuc AND  codexpediente = :codexpediente ;";

    $result = $conexion->prepare($Sql);
    $result->execute(array(
        ":nropago" => $nropago,
        ":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":codexpediente" => $codexpediente
    ));
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error update Expediente";
        $data['res'] = 2;
        die(json_encode($data));
    }
}
if ($nrocargo != 0) {
    $sql = "UPDATE  facturacion.detvarios
            SET  estadocuota = 3,fechapago='".$fechareg."'
            WHERE codemp=1 AND  codsuc=".$codsuc."
            AND nrocredito=".$nrocargo." ";
    $result = $conexion->query($sql);
    if (!$result) {
        $conexion->rollBack();
        $mensaje = "Error UPDATE detcreditos";
        $data['res'] = 2;
        die(json_encode($data));
    }
    $sql = "UPDATE  facturacion.cabvarios
            SET  estareg = 3
            WHERE codemp=1 AND  codsuc=".$codsuc."
            AND nrocredito=".$nrocargo."  ";
    $result = $conexion->query($sql);
    if (!$result) {
        $conexion->rollBack();
        $mensaje = "Error UPDATE detcreditos";
        $data['res'] = 2;
        die(json_encode($data));
    }
}

//VALIDAR INCOSISTENCIAS
if ($condpago == 0) {
    $Sql = "SELECT SUM(d.importe) ";
    $Sql .= "FROM cobranza.detpagos d ";
    $Sql .= "WHERE nropago = ".$nropago." ";
    $Sql .= " AND codemp = 1 ";
    $Sql .= " AND codsuc = ".$codsuc." ";
    $Sql .= " AND nroinscripcion = ".$nroinscripcion;

    $result = $conexion->query($Sql);

    if (!$result) {
        $conexion->rollBack();
        $mensaje = "655 Error SELECT detpagos";
        $data['res'] = 2;
        $data['mensaje'] = $mensaje;
        die(json_encode($data));
    }

    $row2 = $result->fetch();

    $data['totalimp'] = floatval(str_replace(",", "", $_POST["total"]));
    $data['totacal']  = floatval(str_replace(",", "", $_POST["subtotal"])) + floatval(str_replace(",", "", $_POST["igv"])) + floatval(str_replace(",", "", $_POST["redondeo"]));
    $data['totaldet'] = str_replace(",", "", number_format($row2[0], 2));
    //echo $data['totalimp']."==". $data['totacal']."==".$data['totaldet'];

    if (floatval($data['totalimp']) != floatval($data['totaldet'])) {
        
        $mensaje = "¡Inconsitencia! No se Registro el Pago.";
        $data['res'] = 2;
        $data['X'] = $X;
        $data['mensaje'] = $mensaje;

        die(json_encode($data));
        
    }
}
////VALIDAR INCOSISTENCIAS
//REVISAR PAGOS VENCIDOS
$Sql = "SELECT c.nroinscripcion,ci.propietario,ci.nrodocumento,ci.codciclo,ci.codantiguo,
                 cl.descripcion as calle,ci.nrocalle,
                MAX(d.nrofacturacion) as nrofacturacion,f.fechavencimiento,tc.descripcioncorta
            FROM  cobranza.cabpagos c
              INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
              AND (c.codsuc = d.codsuc)
              AND (c.nroinscripcion = d.nroinscripcion)
              AND (c.nropago = d.nropago)
              INNER JOIN facturacion.cabfacturacion f ON (f.codemp = d.codemp)  AND (f.codsuc = d.codsuc)  AND (f.nrofacturacion = d.nrofacturacion)  AND (f.nroinscripcion = d.nroinscripcion)
              --INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp)  AND (f.codsuc = df.codsuc)  AND (f.nrofacturacion = df.nrofacturacion)  AND (f.nroinscripcion = df.nroinscripcion)
              INNER JOIN catastro.clientes ci ON (ci.codemp = c.codemp) AND (ci.codsuc = c.codsuc)
               AND (ci.nroinscripcion = c.nroinscripcion)
               INNER JOIN public.calles as cl on(ci.codemp=cl.codemp and ci.codsuc=cl.codsuc and ci.codcalle=cl.codcalle and ci.codzona=cl.codzona)
                inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
              WHERE c.codsuc=".$codsuc." and c.anulado=0 AND d.nrofacturacion<>0
              and c.fechareg>f.fechavencimiento AND c.nropago=".$nropago."
              AND c.nroinscripcion=".$nroinscripcion."  --c.fechareg between   '2015-01-01' and '2015-01-31'
              group by c.nroinscripcion,ci.propietario,ci.nrodocumento,ci.codciclo,ci.codantiguo,
              cl.descripcion,ci.nrocalle,f.fechavencimiento,tc.descripcioncorta ";

$Consulta = $conexion->query($Sql);
$i = 0;
$codconcepto = 10013; //CONCEPTO INTERES
$Sql = "SELECT max(nrocredito)  FROM facturacion.cabvarios WHERE codsuc=".$codsuc;
$rowc = $conexion->query($Sql)->fetch();
$nrocabvarios = $rowc[0];

$sql = "SELECT nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
            FROM facturacion.periodofacturacion
            WHERE codemp=1 and codsuc=? and codciclo=1 and facturacion=0";
$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc));
$row = $consulta->fetch();
$TasaInteres = $row['tasapromintant'] / 100;
$anio = $row['anio'];
$mes = $row['mes'];
$nrofacturacionactual = $row['nrofacturacion'];
$fechavencimiento = FechaVecimiento($mes, $anio);
$TotalMora = 0;
$TotalDias = 0;
$observacion = '';
foreach ($Consulta->fetchAll() as $row) {
    $nroinscripcion = $row['nroinscripcion'];
    $fechavencimientorecibo = $row['fechavencimiento'];
    $Sql = "SELECT SUM(d.importe),d.serie,d.nrodocumento
                FROM  cobranza.cabpagos c
              INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
              AND (c.codsuc = d.codsuc)
              AND (c.nroinscripcion = d.nroinscripcion)
              AND (c.nropago = d.nropago)
              WHERE c.codsuc=".$codsuc." and c.anulado=0 AND d.nrofacturacion<>0
              and c.fechareg>'".$fechavencimientorecibo."' AND c.nroinscripcion=".$nroinscripcion."
              AND d.nrofacturacion=".$row['nrofacturacion']."
              GROUP BY d.serie,d.nrodocumento";
    $row2 = $conexion->query($Sql)->fetch();

    $dias = dias_transcurridos($fechavencimientorecibo, $fechareg);
    $importe = $row2[0];
    $importe = round($importe * $dias * ($TasaInteres / 30), 1);
    $TotalMora+=$importe;
    $TotalDias+=$dias;
    $observacion.='RECIBO '.$row2[1]."-".$row2[2].",por S/. ".number_format($row2[0], 2);
    $observacion.=", Vencido hace ".$dias." dia(s), Interes: S/. ".number_format($importe, 2)."\n";
}
if ($TotalMora > 0) {
    $propietario = strtoupper($row["propietario"]);
    $direccion = strtoupper($row["descripcioncorta"]." ".$row["calle"]." ".$row["nrocalle"]);
    $codciclo = $row['codciclo'];
    $codantiguo = $row["codantiguo"];
    $nrocabvarios++;

    $sqlC = "INSERT INTO facturacion.cabvarios(codemp,codsuc,nrocredito,nroinscripcion,imptotal,igv,redondeo,subtotal,codconcepto,
                          nropresupuesto,observacion,creador,fechareg,nroprepago,codantiguo,
                          propietario,direccion,hora) VALUES(:codemp,:codsuc,:nrocredito,:nroinscripcion,:imptotal,:igv,:redondeo,
                          :subtotal,:codconcepto,:nropresupuesto,:observacion,:creador,:fechareg,:nroprepago,:codantiguo,:propietario,:direccion,:hora)";

    $resultC = $conexion->prepare($sqlC);
    $resultC->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nrocredito" => $nrocabvarios,
        ":nroinscripcion" => $nroinscripcion,
        ":imptotal" => str_replace(",", "", $TotalMora),
        ":igv" => 0,
        ":redondeo" => 0,
        ":subtotal" => str_replace(",", "", $TotalMora),
        ":codconcepto" => $codconcepto,
        ":nropresupuesto" => 0,
        ":observacion" => $observacion, //'INTERES POR '.$TotalDias.' DIAS (S).',
        ":creador" => $idusuario,
        ":fechareg" => $fechareg,
        ":nroprepago" => 0,
        ":codantiguo" => $codantiguo,
        ":propietario" => $propietario,
        ":direccion" => $direccion,
        ":hora" => date('H:i')));
    if ($resultC->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        $data['res'] = 2;
        die(json_encode($data));
    }

    $sqlD = "INSERT INTO facturacion.detvarios(codemp,codsuc,nrocredito,nrocuota,totalcuotas,fechavencimiento,subtotal,igv,
                redondeo,imptotal,nrofacturacion,tipocuota,anio,mes,interes,estadocuota) values(:codemp,:codsuc,:nrocredito,:nrocuota,:totalcuotas,:fechavencimiento,:subtotal,
                :igv,:redondeo,:imptotal,:nrofacturacion,:tipocuota,:anio,:mes,:interes,:estadocuota)";

    $resultD = $conexion->prepare($sqlD);
    $resultD->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nrocredito" => $nrocabvarios,
        ":nrocuota" => 1,
        ":totalcuotas" => 1,
        ":fechavencimiento" => $objFunciones->CodFecha($fechavencimiento),
        ":subtotal" => str_replace(",", "", $TotalMora),
        ":igv" => 0,
        ":redondeo" => 0,
        ":interes" => 0,
        ":imptotal" => str_replace(",", "", $TotalMora),
        ":nrofacturacion" => $nrofacturacionactual,
        ":tipocuota" => 0,
        ":estadocuota" => 0,
        ":anio" => $anio,
        ":mes" => $mes));

    if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    }

    $sqlD = "INSERT INTO facturacion.detcabvarios ";
    $sqlD .= "(  codemp,  codsuc,  nrocredito,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal)
                VALUES (  :codemp,  :codsuc,  :nrocredito,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";

    $resultD = $conexion->prepare($sqlD);
    $resultD->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nrocredito" => $nrocabvarios,
        ":codconcepto" => $codconcepto,
        ":item" => 1,
        ":subtotal" => str_replace(",", "", $TotalMora),
        ":igv" => 0,
        ":redondeo" => 0,
        ":imptotal" => $TotalMora,
    ));
    if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        $data['res'] = 2;
        die(json_encode($data));
    }
}

$EsServicio = 0;
if (!$Flag) {
    $EsServicio = 1;
}

$conexion->commit();
$objFunciones->setCorrelativosVarios(5, $codsuc, "UPDATE", $nropago);

$data['res'] = 1;
$data['esservicio'] = $EsServicio;
$data['pago'] = $nropago;
$data['tipo'] = 2; //PREPAGO
$data['concredito'] = $concredito; //CON CREDITO
$data['nrocredito'] = $nrocredito; //CON CREDITO
$data['nroprepago'] = $nroprepagocre; //CON CREDITO
echo json_encode($data);

function dias_transcurridos($fecha_i, $fecha_f) {
    $dias = (strtotime($fecha_i) - strtotime($fecha_f)) / 86400;
    $dias = abs($dias);
    $dias = floor($dias);
    return $dias;
}

function FechaVecimiento($mes, $anio) {
    $dia = UltimoDia($mes);

    if (strlen($mes) == 1) {
        $mes = "0".$mes;
    }
    return $dia."/".$mes."/".$anio;
}

function UltimoDia($Mes) {
    $NroDias = 0;

    switch ($Mes) {
        case 1:$NroDias = 31;
            break;
        case 2:$NroDias = 28;
            break;
        case 3:$NroDias = 31;
            break;
        case 4:$NroDias = 30;
            break;
        case 5:$NroDias = 31;
            break;
        case 6:$NroDias = 30;
            break;
        case 7:$NroDias = 31;
            break;
        case 8:$NroDias = 31;
            break;
        case 9:$NroDias = 30;
            break;
        case 10:$NroDias = 31;
            break;
        case 11:$NroDias = 30;
            break;
        case 12:$NroDias = 31;
            break;
    }

    return $NroDias;
}

function CalcularRedondeo($Imp) {
    $red1 = round($Imp, 1);
    $red2 = round($Imp, 2);
    $redondeo = $red1 - $red2;

    return $redondeo;
}

?>
