<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "CANCELACION DE RECIBOS";
	$Activo = 0;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc = $_SESSION['IdSucursal'];
	
	$objMantenimiento = new clsDrop();
	
	$Op				= $_GET["Op"];
	$codsuc 		= $_SESSION["IdSucursal"];
	$fechaserver 	= isset($_GET["fdescarga"])?$_GET["fdescarga"]:$objMantenimiento->FechaServer();
	$cars			= isset($_GET["cars"])?$_GET["cars"]:"";
	$formapago		= isset($_GET["formapago"])?$_GET["formapago"]:"1";
	
	$paramae		= $objMantenimiento->getParamae("IMPIGV", $codsuc);
	
	$nrorecibo		= isset($_GET["nrorecibo"])?$_GET["nrorecibo"]:'';
	$ventana 				= "cobranza";
	$_SESSION["ventana"] 	= "cobranza";

	$sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
	$sql .= "FROM facturacion.periodofacturacion ";
	$sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = 1 AND facturacion = 0";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc));
	
	$row = $consulta->fetch();
	
	$TasaInteres = $row['tasapromintant'] / 100;
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_cobranza.js"></script>
<style type="text/css">
	a.ui-tabs-anchor {
    	font-size: 12px !important;
	}
</style>
<script>
$(document).ready(function() { 
	$("#BtnPagos").val('Pre Pagos');
	$("#BtnPagos").attr('title', 'Ir a Pre Pagos');
	
	$(".verprepago, #nrorecibo").hide();
	
	$('form').keypress(function(e){   
		if(e == 13){
			return false;
		}
	});

	$('input').keypress(function(e){
		if(e.which == 13){
			return false;
		}
	});
});

	var codsuc			= <?=$codsuc?>;
	var urldir			= "<?php echo $_SESSION['urldir'];?>";
	var ventana			= "cobranza" ;
	var TasaInteres     = <?=$TasaInteres?>;
	var cont_fact 		= 0;
	var cont_cob		= 0;
	var contador		= 0;
	
	var subt			= 0;
	var red				= 0;
	var impigv			= <?=$paramae["valor"]?>;
	var tot				= 0;
	
	var index			= 0;
	var object	= "";
		
	function ValidarForm(Op)
	{
		if($("#cont_validar").val() > 0)
		{
			alert("Hay Conceptos sin asignar la categoria de cobranza...asignelos antes de poder continuar");
			
			return false;	
		}
		
		if($("#nroinscripcion").val() == "")
		{
			alert("Digite el Nro. de Inscripcion del Usuario");
			
			$("#nroinscripcion").focus();
			
			return false;
		}
		
		if($("#eventualtext").val() == 1)
		{
			if($("#cliente").val() == "")
			{
				Msj($("#cliente"), "Digite Nombres del Cliente");
				
				$("#cliente").focus();
				
				return false;
			}
			
			if($("#direccion").val() == "")
			{
				Msj($("#direccion"), "Digite la Dirección del Cliente");
				
				$("#direccion").focus();
				
				return false;
			}
			
			if($("#docidentidad").val() == "")
			{
				Msj($("#docidentidad"), "Digiteel N° de Documento del Cliente");
				
				$("#docidentidad").focus();
				
				return false;
			}
		}
		
		if(contador == 0)
		{
			alert('No hay Ningun Item Ingresado para Facturar');
			
			return false;
		}
		
		if($("#nroprepago").val() == "")
		{
			$("#nroprepago").val(0);
		}
		
		$("#tpagar").val($("#total").val());
		$("#efectivo").val($("#total").val());
		
		calcular_vuelto($("#efectivo"));
		
		$("#efectivo").focus();
		
		if($("#condpago").val() == 0)
		{
			$("#dialog-form-vuelto").dialog("open");
			
			$("#efectivo").focus().select();
		}
		else
		{
			//VALIDAR QUE NO GRAVE MAS DE UN CONCEPTO
			if(contador > 1)
			{
				alert('Solo se premite un colateral, como Cargo');
			}
			
			if(contador == 1)
			{
				GuardarTodo();
			}
		}
		
		return false;
	}
	
	function calcular_vuelto(obj)
	{
		obj = $("#efectivo");
		var v = 0;
		var tpagar = $("#tpagar").val();
		
		if(obj.val() == "")
		{
			v = 0;
		}
		else
		{
			v = parseFloat(obj.val()) - parseFloat(tpagar);
		}

		$("#vueltox").val(parseFloat(v).toFixed(2));
	}
	
	function Cancelar()
	{
		location.href = 'index.php?Op=1';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
	<table width="1200" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    	<tbody>
			<tr>
	  			<td colspan="9" >
      			<? include("include/cabecera.php"); ?>
      			</td>
			</tr>
	
			<tr>
			<td colspan="9" class="TitDetalle">
			<div id="tabs">
			  <ul>
			    <li style="font-size:10px"><a href="#tabs-1">Facturacion</a></li>
			    <li style="font-size:10px"><a href="#tabs-2">Otros Servicios</a></li>
			    </ul>
			  <div id="tabs-1" style="height:140px">
			    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			      <tr>
			        <td>
			          <div style="overflow:auto; height:140px">
			            <? include("include/facturacion.php"); ?>
			          </div>
			          </td>
			        </tr>
			      </table>
			    </div>
			  <div id="tabs-2" style="height:140px">
			    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
			      <tr>
			        <td colspan="2">
			          <? include("include/conceptos.php"); ?>
			          </td>
			        </tr>
			   
			      <tr>
			        <td width="47%">Buscar Conceptos : 
			          <input type="text" name="buscarconcepto" id="buscarconcepto" class="inputtext" size="45" maxlength="45" value=""  /></td>
			        <td width="53%">
			        	<div id="div_valir" align="center" style="color:#F00; font-weight:bold; font-size:11px; padding:4px"><?=$mensaje?></div>
			        </td>
			        </tr>
			      </table>                	
			    </div>                
			  </div>                
			</td>
			</tr>
	
			<tr>
			  <td colspan="9" class="TitDetalle">
		    	<div style="height:160px; overflow:scroll">
		        	<? include("include/cobranza.php"); ?>
		        </div>  
		      </td>
			</tr>
			<tr style="display:none">
			  <td width="14" class="TitDetalle">
			  	<input type="hidden" name="cont_facturacion" id="cont_facturacion" value="0" />
			    <input type="hidden" name="cont_cobranza" id="cont_cobranza" value="0" />
			</td>
			  <td colspan="8" class="CampoDetalle">
			  	<input type="hidden" name="efectivo_x" id="efectivo_x" value="0" />
			  	<input type="hidden" name="vuelto" id="vuelto" value="0" />
			    <input type="hidden" name="cont_validar" id="cont_validar" value="<?=$contvalidar?>" />
			</td>
			  </tr>
		    </tbody>
	  	<tfoot>
	  		<tr>
			<td colspan="9" style="padding:4px; height:45px">
            	<table width="100%" border="0">
                	<tr>
                    	<td align="left">
                        <input name="btnAceptar" class="button" type="button" id="btnAceptar" value="Aceptar" onclick="return ValidarForm();"/></td>
                   		<td>
                   			<table width="100%" style="border:1px #000000 dashed"cellspacing="0" cellpadding="0">
                      			<tr>
                        			<td style="background-color:#CCCCCC; padding:4px; color:#000000; font-size:12px; font-weight:bold" align="right">Sub. Total : </td>
                        			<td style="padding-left:4px">
                        				<label>
                          				<input type="text" name="subtotal" id="subtotal" class="inputtext" readonly="readonly" size="15" maxlength="15" value="0.00" style="text-align:right" />
                       					</label>
                       				</td>
			                        <td style="background-color:#CCCCCC; padding:4px; color:#000000; font-size:12px; font-weight:bold" align="right">Igv : </td>
			                        <td style="padding-left:4px">
										<input type="text" name="igv" id="igv" class="inputtext" readonly="readonly" size="15" maxlength="15" value="0.00" style="text-align:right" />
									</td>
			                        <td style="background-color:#CCCCCC; padding:4px; color:#000000; font-size:12px; font-weight:bold" align="right">Redondeo : </td>
			                        <td style="padding-left:4px"><input type="text" name="redondeo" id="redondeo" class="inputtext" readonly="readonly" size="15" maxlength="15" value="0.00" style="text-align:right" /></td>
			                        <td style="background-color:#CCCCCC; padding:4px; color:#000000; font-size:12px; font-weight:bold" align="right">Total : </td>
			                        <td style="padding-left:4px"><input type="text" name="total" id="total" class="inputtext" readonly="readonly" size="15" maxlength="15" value="0.00" style="text-align:right" />
			                            </span></td>
			                        <td>&nbsp;</td>
                      			</tr>
                    		</table>
                    	</td>
				    	<td align="right">
				    		<!-- <input name="btnCancelar" class="button" type="button" id="btnCancelar" value="Cancelar" onclick="Cancelar();"  /> -->
				    	</td>
                	</tr>
                </table>
            </td>
            </tr>
	 	</tfoot>
  	</table>
	<div id="dialog-form-validar-remove" title="Validacion de Usuario"  >
	<table width="100%" border="0" >
  		<tr style="height:5px">
    		<td colspan="3" align="right"></td>
    	</tr>
    	<tr>
    		<td align="right">Usuario</td>
    		<td align="center">:</td>
    		<td>
    			<? $objMantenimiento->drop_usuarios_sucursales($codsuc); ?>
    		</td>
		</tr>
		<tr>
			<td width="34%" align="right"> Contrase&ntilde;a</td>
			<td width="3%" align="center">:</td>
			<td width="63%">
			<input type="password" name="contra_remove" id="contra_remove" size="30" maxlength="20" class="inputtext" style="width:200px;" onkeypress="ValidarUserD(event);">
			</td>
		</tr>
		<tr style="height:5px">
			<td colspan="3" align="center">
			<div id="mensaje_remover" style="color:#F00; font-weight:bold; font-size:11px; padding:4px">
			</div>
			</td>
		</tr>
	</table>
	</div>
	<div id="DivGlosa" title="Observaciones"  >
	<table width="100%" border="0">
       <tr>
		<td> Glosa:</td>
		<td>
			<textarea class="inputtext ui-corner-all" id="GlosaTemp" title="Glosa" cols="50" rows="5" ></textarea>
		</td>
		</tr>
	</table>
	</div>

	<div id="dialog-form-vuelto" title="Vuelto"  >
		<table width="100%" bgcolor="#EEEEEE">
	       <tr>
			<td width="100%" align="center" valign="middle" class="tdconfir" id="btn_conf2">
				<fieldset>
	            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                    <td width="3%">&nbsp;</td>
	                    <td width="33%">&nbsp;</td>
	                    <td width="6%">&nbsp;</td>
	                    <td width="55%">&nbsp;</td>
	                    <td width="3%">&nbsp;</td>
	                  </tr>
	                  <tr>
	                    <td>&nbsp;</td>
	                    <td style="font-size:18px; font-weight:bold">Total a Pagar</td>
	                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
	                    <td><label>
	                      <input type="text" name="tpagar" id="tpagar" readonly="readonly" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right; background-color:#CCC" value="0.00" />
	                    </label></td>
	                    <td>&nbsp;</td>
	                  </tr>
	                  <tr>
	                    <td>&nbsp;</td>
	                    <td style="font-size:18px; font-weight:bold">Efectivo</td>
	                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
	                    <td><input type="text" name="efectivo" id="efectivo" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right" onkeypress="ValidarEfect(event);return permite(event,'num');" onkeyup="calcular_vuelto(this);" value="0.00" /></td>
	                    <td>&nbsp;</td>
	                  </tr>
	                  <tr>
	                    <td>&nbsp;</td>
	                    <td style="font-size:18px; font-weight:bold">Vuelto</td>
	                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
	                    <td><input type="text" name="vueltox" id="vueltox" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right; background-color:#CCC" readonly="readonly" value="0.00" /></td>
	                    <td>&nbsp;</td>
	                  </tr>
	                  <tr style="height:5px">
	                    <td colspan="5"></td>
	                  </tr>
	                </table>
	            </fieldset>
			</td>
	    </tr>
	    <tr>
	    	<td>
	    		<div id='DivIntMor' style='color:#FF0000; font-size:13px; font-weight:bold; padding:1px; display:inline'></div>
	    	</td>
	    </tr>
	 </table>    	
	</div>
	<div id="dialog-form-detalle-facturacion" title="Ver Detalle de Facturacion"  >
		<div id="detalle-facturacion">
	    </div>
	</div>
	<div id="dialog-form-update-importe" title="Modificar Importe"  >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="3%">&nbsp;</td>
                <td width="33%">&nbsp;</td>
                <td width="6%">&nbsp;</td>
                <td width="55%">&nbsp;</td>
                <td width="3%">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td style="font-size:18px; font-weight:bold">Concepto</td>
                <td align="center" style="font-size:18px; font-weight:bold">:</td>
                <td><label id="LblUpdateImporteColateral"></label></td>
                <td>&nbsp; <input id="ItemUpdateImporte" type="hidden"> <input id="UpdateImporteColateralOriginal" type="hidden"></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td style="font-size:18px; font-weight:bold">Importe</td>
                <td align="center" style="font-size:18px; font-weight:bold">:</td>
                <td>
                	<input type="text"  id="UpdateImporteColateralImporte" size="15" maxlength="15" onkeypress="ValidarUpdateImporte(event);" class="numeric"/>
                </td>
                <td>&nbsp;</td>
              </tr>
             
            </table>	
	</div>
	<div id="DivDetCredito" title="Detalle de Credito">
	  <table border="1" align="center" cellspacing="0" class="ui-widget myTable01"  width="100%" id="DetCredtio" rules="all" >
			<thead class="ui-widget-header" >
        <tr align="center">
          <th width="8%" >Nro. Cuota </th>
          <th width="10%" >Fecha de Vencimiento</th>
          <th width="8%" >Saldo</th>
          <th width="8%" >Amortizaci&oacute;n</th>
          <th width="8%" >Interes</th>
          <th width="5%" >Igv</th>
          <th width="9%" >Importe</th>
          <th width="12%" >Categoria</th>
          <th width="10%" >Estado</th>
        </tr>
		</thead>
		<tbody></tbody>
      </table>
	  </div>
	<script>cargar_datos_periodo_facturacion(1)</script>
</form>
</div>
<div id="DivCuentaCorriente" title="Ver Cuenta Corriente"  >
    <div id="detalle-CuentaCorriente">
    </div>
</div>
<? 
	if($Op==1)
	{
		include("include/from_validar.php"); 
	}
?>

<?php   CuerpoInferior();?>