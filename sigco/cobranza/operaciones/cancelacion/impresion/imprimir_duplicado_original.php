<?php

date_default_timezone_set("America/Lima");

include("../../../../../objetos/clsReporte.php");
include("../../../../../admin/clases/num2letra.php");
include("../../../../../vma/include/funciones.php");
require_once '../../../../../include/Image/Barcode.php';

ini_set("memory_limit", "512M");

class clsImpresionDuplicado extends clsReporte {

    function Header() {
        
    }

    function Contenido($tipo, $nroinscripion, $codcatastro, $ruc, $facturacion, $recibo, $femision, $propietario, $direccion, $servicio, $tfact, $tarifa, $nromed, $fechalectanterior, $lectanterior, $consumo, $fechalectultima, $lectultima, $mensaje, $fvencimientodeudores, $fcorte, $fvencimiento, $docidentidad, $nrodocidentidad, $mensajenormal, $codantiguo, $zona, $tipoactividad, $catetar, $consumofact, $ruta, $serie, $vma, $nrofacturacion, $horasabastecimiento, $codtiposervicio, $tiposervicio, $fecha_vencimiento_periodo, $periodo) {

        global $tfacturacion, $conexion, $codsuc, $itemP, $meses, $Empresa;

        $x = 1;
        $y = 1;

        $paramae = $this->getParamae("IMPIGV", $codsuc);

        if ($tfact == 0) {
            $img = $this->generargraficos($nroinscripion, $codsuc);
        }

        if (!empty($codcatastro)):
            $grafico = new Graph();
            $grafico = Image_Barcode::draw($codantiguo, 'code128', 'jpg', false);
            imagejpeg($grafico, $codantiguo.'.jpg');
            $this->Image($codantiguo.'.jpg', 6, 26.5, 10, 1.2);
        endif;


        $this->SetX($x);
        $y = $this->GetY();

        $this->SetXY($x + 5.5, $y - 0.3);
        $this->SetFont('Arial', '', 9);
        $this->SetXY($x + 7.8, $y + 0.3);

        $this->SetFont('Arial', '', 11);
        $this->SetXY($x + 0.2, $y - 0.4);
        $this->Cell(3.5, 0.6, utf8_decode("Código"), 0, 0, 'L');

        $this->SetFont('Arial', 'B', 14);
        $this->Cell(6.2, 0.6, $codcatastro, 0, 1, 'R');

        $this->SetX($x + 14.3);
        $this->SetFont('Arial', '', 10);
        $this->SetXY($x + 0.2, $y + 0.2);
        $this->Cell(3.5, 0.6, utf8_decode("Inscripción"), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 14);
        $this->Cell(6.2, 0.6, $codantiguo, 0, 1, 'R');


        $MesA = $meses[$itemP["mes"] - 1];
        if ($itemP["mes"] == 1)
            $MesA = "DICIEMBRE";

        $this->Ln(0.2);
        //$this->SetX($x + 0.1);

        $h = 0.5;
        $this->SetFont('Arial', 'B', 10);

        $this->Cell(10.5, $h, utf8_decode($propietario), 0, 1, 'L');

        //$this->SetX($x + 0.1);
        $this->SetFont('Arial', '', 10);

        $this->Cell(10.5, $h, utf8_decode(strtoupper($direccion)), 0, 1, 'L');

        // Unidades de Uso
        $SqlCat = "SELECT un.nroinscripcion, un.catetar, un.porcentaje, ca.nomtar, urb.tipo || ' ' || urb.descripcion AS urbanizacion";
        $SqlCat .= " FROM facturacion.unidadesusofacturadas un ";
        $SqlCat .= " 	JOIN catastro.clientes cl ON(cl.codemp = un.codemp) AND (cl.codsuc = un.codsuc) AND (cl.nroinscripcion = un.nroinscripcion) ";
        $SqlCat .= " 	JOIN facturacion.tarifas ca ON(ca.codsuc = un.codsuc) AND (ca.catetar = un.catetar) ";
        $SqlCat .= " 	JOIN catastro.urbanizaciones urb ON(cl.codsuc = urb.codsuc AND cl.codzona = urb.codzona and cl.codurbanizacion = urb.codurbanizacion)";
        $SqlCat .= " WHERE un.codsuc = ".$codsuc." AND ca.estado = 1";
        $SqlCat .= " AND un.nroinscripcion = '".$nroinscripion."' ";
        $SqlCat .= " AND un.nrofacturacion = ".$nrofacturacion;

        $rCat = $conexion->query($SqlCat)->fetchAll();

        $nroCat = 0;
        $Tar = '';
        $nombre_tarifa_ciclica = '';

        foreach ($rCat as $rowD) :
            $nroCat ++;
            $nombre_tarifa = substr($rowD['nomtar'], 0, 3);

            if ($nombre_tarifa != $nombre_tarifa_ciclica) :

                if (!strpos($Tar, $nombre_tarifa."-".$rowD['catetar'])):
                    $Tar .= $nombre_tarifa."-".$rowD['catetar'];
                endif;

            endif;

            $nombre_tarifa_ciclica = $nombre_tarifa;
            $urbanizacion = $rowD['urbanizacion'];

        endforeach;

        $this->SetFont('Arial', 'B', 9);
        $this->Cell(8, $h, utf8_decode(trim($urbanizacion)), 0, 0, 'L');
        $this->Cell(2.5, $h, "U. USO ".$nroCat, 0, 1, 'L');

        $DocId = "D.N.I.";

        if (strlen(trim($nrodocidentidad)) > 8) {
            $DocId = "R.U.C.";
        }

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(12, 0.3, "", 0, 1, 'L');
        $this->Cell(1, $h, $DocId, 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $this->Cell(4, $h, $nrodocidentidad, 0, 0, 'L');

        $this->SetFont('Arial', 'B', 9);

        $this->Cell(1, $h, "Ruta", 0, 0, 'L');

        $this->SetFont('Arial', '', 10);

        $ruta = $this->getRutas($codsuc, $nroinscripion);

        $this->Cell(2.5, $h, $ruta, 0, 1, 'L');


        $this->SetFont('Arial', '', 10);
        $this->SetXY($this->GetX() + 1, $this->GetY() + 0.2);

        $this->Cell(4, 0.65, utf8_decode("Periodo/Ciclo"), 0, 0, 'C');

        $this->SetX($this->GetX() + 0.7);
        $this->Cell(4, 0.65, utf8_decode("Número de Recibo"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetX($this->GetX() + 1);
        $this->Cell(4, 0.45, $facturacion, 0, 0, 'C');

        $serie = str_pad($serie, 3, "0", STR_PAD_LEFT);
        $this->SetX($this->GetX() + 0.7);
        $this->Cell(4, 0.45, $serie."-".str_pad($recibo, 8, '0', STR_PAD_LEFT), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);

        $this->Cell(6, 0.60, "SERVICIO PRESTADO ", 0, 0, 'L');
        $this->SetFont('Arial', '', 8);
        $this->Cell(4.5, 0.60, utf8_decode(strtoupper($tiposervicio)), 0, 1, 'C');

        if ($tfact == 0) {
            $t = "L";
            $ConsumoT = $consumo;
            $estadomedidor = utf8_decode($estadomedidor);
        }
        if ($tfact == 1) {
            $t = "P";
            $ConsumoT = intval($lecturapromedio);
            $estadomedidor = 'PROMEDIADO';
        }
        if ($tfact == 2) {
            $t = "A";
            $ConsumoT = $consumo;
            $estadomedidor = 'DIRECTO';
            $consumofact = $ConsumoT;
        }

        $consumo = intval($consumo);
        $consumofact = intval($consumofact);

        $this->SetXY($x, $y + 5.2);

        $this->SetFont('Arial', '', 10);
        $this->Cell(6, 0.5, utf8_decode("N° de Medidor"), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(4.5, 0.5, $nromed, 0, 1, 'R');

        $this->SetFont('Arial', '', 10);
        $this->Cell(6, 0.5, utf8_decode("Categoria Tarifaria"), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(4.5, 0.5, $tarifa." (".$t.")", 0, 0, 'R');

        $this->SetFont('Arial', '', 10);
        $this->Cell(0.5, 0.5, "", 0, 0, 'C');
        $this->Cell(2, 0.5, utf8_decode(""), 0, 0, 'C');
        $this->Cell(2, 0.5, utf8_decode("LECTURAS"), 0, 0, 'C');
        $this->Cell(2, 0.5, utf8_decode("Cod"), 0, 0, 'C');
        $this->Cell(1.7, 0.5, utf8_decode("Periodo"), 0, 1, 'C');

        $sql = "SELECT ta.* FROM facturacion.tarifas ta ";
        $sql .= "WHERE ta.codemp = 1 ";
        $sql .= " AND ta.codsuc = ".$codsuc." ";
        $sql .= " AND ta.catetar = ".$catetar." ";
        $sql .= " AND ta.estado = 1";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetch();

        $costo = '';

        switch ($codtiposervicio) :

            //Agua y Desague
            case 1:

                $costo = number_format($items['impconsmin'], 4)."(0-".number_format($items['hastarango1'], 0)."m3)";
                $items['impconsexec'] = (float) $items['impconsexec'];
                $costo .= (empty($items['impconsexec'])) ? "-".number_format($items['impconsmedio'], 4)."(".((int) $items['hastarango1'] + 1)."- a mas)" : "-".number_format($items['impconsmedio'], 4)."(".((int) $items['hastarango1'] + 1)."-".number_format($items['hastarango2'], 0).") -".number_format($items['impconsexec'], 4)."(".((int) $items['hastarango3'])."- a mas)";

                break;

            // Agua
            case 2:
                $costo = number_format($items['impconsmin'], 4)."(0-".number_format($items['hastarango1'], 0)."m3)";
                $items['impconsexec'] = (float) $items['impconsexec'];
                $costo .= (empty($items['impconsexec'])) ? "-".number_format($items['impconsmedio'], 4)."(".((int) $items['hastarango1'] + 1)."- a mas)" : "-".number_format($items['impconsmedio'], 4)."(".((int) $items['hastarango1'] + 1)."-".number_format($items['hastarango2'], 0).") -".number_format($items['impconsexec'], 4)."(".((int) $items['hastarango3'])."- a mas)";
                break;

            // Desague
            case 3:
                $costo = number_format($items['impconsmindesa'], 4)."(0-".number_format($items['hastarango1'], 0)."m3)";
                $items['impconsexecdesa'] = (float) $items['impconsexecdesa'];
                $costo .= (empty($items['impconsexecdesa'])) ? "-".number_format($items['impconsmediodesa'], 4)."(".((int) $items['hastarango1'] + 1)."- a mas)" : "-".number_format($items['impconsmediodesa'], 4)."(".((int) $items['hastarango1'] + 1)."-".number_format($items['hastarango2'], 0).") -".number_format($items['impconsexecdesa'], 4)."(".((int) $items['hastarango3'])."- a mas)";
                break;

        endswitch;

        $this->SetFont('Arial', '', 10);
        $this->Cell(10.5, 0.5, utf8_decode("COSTO X m3 ".utf8_decode($costo)), 0, 0, 'L');

        $this->Cell(0.5, 0.5, "", 0, 0, 'C');
        $this->Cell(2, 0.5, utf8_decode("Actual"), 0, 0, 'L');
        $this->Cell(2, 0.5, number_format($lectultima, 0), 0, 0, 'C');
        $this->Cell(2, 0.5, utf8_decode(""), 0, 0, 'C');
        $this->Cell(1.7, 0.5, utf8_decode($fechalectultima), 0, 1, 'C');

        $this->SetFont('Arial', '', 10);
        $this->Cell(10.5, 0.4, utf8_decode("HORARIO DE SUMINISTRO Horas ".$horasabastecimiento), 0, 0, 'L');

        $this->Cell(0.5, 0.4, "", 0, 0, 'C');
        $this->Cell(2, 0.4, utf8_decode("Anterior"), 0, 0, 'L');
        $this->Cell(2, 0.4, number_format($lectanterior, 0), 0, 0, 'C');
        $this->Cell(2, 0.4, utf8_decode(""), 0, 0, 'C');
        $this->Cell(1.7, 0.4, ($fechalectanterior), 0, 1, 'C');

        $this->SetFont('Arial', '', 10);
        $this->Cell(10.5, 0.4, utf8_decode("Contrato N°"), 0, 0, 'L');

        $this->Cell(0.5, 0.4, "", 0, 0, 'C');
        $this->Cell(2, 0.4, utf8_decode("Consumo"), 0, 0, 'L');
        $this->Cell(2, 0.4, utf8_decode((int) $consumo." m3"), 0, 0, 'C');
        $this->Cell(2, 0.4, utf8_decode(""), 0, 0, 'C');
        $this->Cell(1.7, 0.4, utf8_decode(""), 0, 1, 'C');

        $xt1 = 10;
        $this->SetXY($x, $y + 8);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(10.5, 0.4, "DESCRIPCION DE CONCEPTOS", 0, 0, 'C');
        $this->Cell(6, 0.4, "NO IMPONIBLE", 0, 0, 'R');
        $this->Cell(2, 0.4, "IMPORTE", 0, 1, 'R');
        $this->Ln(0.3);

        $h1 = 0.5;
        $this->SetFont('Arial', '', 6);
        $this->Cell(4.5, $h1, "", 0, 0, 'C');
        $this->Cell(4.5, $h1, "", 0, 0, 'C');
        $this->Cell(2, $h1, "", 0, 0, 'C');
        $this->Cell(3.5, $h1, "", 0, 0, 'C');
        $this->Cell(3.5, $h1, utf8_decode(""), 0, 1, 'C');
        $this->Ln(0.3);

        $lectultima = intval($lectultima);
        $lectanterior = intval($lectanterior);
        $this->Ln(0.2);

        $this->Cell(3.5, 0.9, " ", 0, 0, 'C');
        $this->Cell(2.5, 0.9, " ", 0, 0, 'C');
        $this->Cell(2.5, 0.9, " ", 0, 1, 'C');
        $this->Ln(1.2);

        $this->SetFont('Arial', 'B', 8);
        $this->SetXY($x - 0.1, $y + 14);
        $this->MultiCell(7, 0.5, utf8_decode(strtoupper($mensajenormal)), 0, 'C');

        $this->Ln(0.25);
        $msj = " ";
        $this->MultiCell(7, 0.5, utf8_decode(strtoupper("")), 0, 'J');
        $this->Ln(0.5);
        $this->SetFont('Arial', 'B', 17);

        $this->Ln(3.5);
        $this->SetXY($x + 9.5, $y + 4.5);

        $subtotMes = 0;
        $interesMes = 0;
        $redondeo = 0;
        $igvMes = 0;
        $totrecibo = 0;

        $Cuotas = 0;

        $sql2 = "";
        switch ($tipo):

            case 1: $sql1 = " SUM(d.importe) AS monto, ";
                break;
            case 2: $sql1 = " SUM(d.importe - d.importerebajado) AS monto, ";
                break;
            case 3:
                $sql1 = " SUM(d.importe - d.importerebajado) AS monto, ";
                // $sql2 = " AND d.estadofacturacion = 1 ";
                break;

        endswitch;

        $sqlDet = "SELECT d.nroinscripcion, d.codconcepto, d.concepto AS descripcion, d.nrocredito, ";
        $sqlDet .= $sql1;
        $sqlDet .= " c.categoria, SUM(importerebajado) AS imprebajado, c.afecto_igv ";
        $sqlDet .= "FROM facturacion.cabfacturacion ca ";
        $sqlDet .= " JOIN facturacion.detfacturacion d ON(ca.codemp = d.codemp) AND (ca.codsuc = d.codsuc) AND (ca.nrofacturacion = d.nrofacturacion) AND (ca.nroinscripcion = d.nroinscripcion) AND (ca.codciclo = d.codciclo) ";
        $sqlDet .= " JOIN facturacion.conceptos c ON(d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto) ";
        $sqlDet .= "WHERE d.codsuc = ".$codsuc." ";
        $sqlDet .= $sql2;
        $sqlDet .= " AND d.nrofacturacion = ".$nrofacturacion." ";
        $sqlDet .= " AND d.nroinscripcion = ".$nroinscripion." ";
        $sqlDet .= " AND d.categoria = 0 ";
        $sqlDet .= " AND enreclamo = 0 ";
        $sqlDet .= "GROUP BY d.nroinscripcion, d.codconcepto, d.concepto, d.nrocredito, c.categoria, c.afecto_igv ";
        $sqlDet .= "ORDER BY d.codconcepto; ";
        //echo $sqlDet; exit;

        $consultaD = $conexion->prepare($sqlDet);
        $consultaD->execute(array());
        $itemsD = $consultaD->fetchAll();

        $cn = 0.5;

        $this->SetXY($x + 1, 10);

        $ConRebaja = 0;

        $meses = $this->calcular_deuda($nroinscripion, $codsuc, $nrofacturacion, $tipo, $periodo);

        foreach ($itemsD as $rowD) {
            if ($rowD["imprebajado"] <> 0) {
                $ConRebaja = 1;
            }

            $y = $y + $cn;

            $CodC = str_pad($rowD["codconcepto"], 5, "00", STR_PAD_LEFT);

            $this->SetX($x + 1);

            if ($rowD["categoria"] != 3 && $rowD["categoria"] != 4 && $rowD["categoria"] != 7) {
                $this->SetFont('Arial', '', 10);

                if (($rowD["nrocredito"] <> 0 || $rowD["nrorefinanciamiento"] <> 0) && $rowD["codtipodeuda"] != 9 && $rowD["categoria"] != 6) {
                    $this->Cell(9.2, 0.5, strtoupper($CodC."   ".utf8_decode($rowD["descripcion"])).$cuota, 0, 0, 'L');
                    //$this->Cell(2.5, 0.5, number_format($rowD["monto"], 2), 0, 1, 'R');

                    $Cuotas += $rowD["monto"];
                } else {
                    $this->Cell(14.8, 0.5, strtoupper($CodC."   ".utf8_decode($rowD["descripcion"])).$cuota, 0, 0, 'L');
                    //$this->Cell(2.5, 0.5, number_format($rowD["monto"], 2), 0, 1, 'R');

                    if ($rowD["categoria"] != 10 || $rowD["afecto_igv"] == 1) {
                        $subtotMes += $rowD["monto"];
                    }
                }

//                if ($rowD["categoria"] == 10 && $rowD["afecto_igv"] == 0) {
//                    // $this->SetX($this->GetX() - 2);
//                    $subtotMes += $rowD["monto"];
//                }
//
//                // $this->Cell(2.5, 0.5, number_format($rowD["monto"], 2), 0, 1, 'R');
//                // Cambios por el mes unico de Enero
//                // Se debe modificar para el siguiente mes de Febrero
//                if ($rowD["codconcepto"] == 1 && $meses["deuda"] != 0) {
//                    $this->Cell(2.5, 0.5, number_format($rowD["monto"], 2), 0, 1, 'R');
//                    $this->Ln();
//                    $eny = $this->GetY();
//                } else {
//                    $this->Cell(2.5, 0.5, number_format($rowD["monto"], 2), 0, 1, 'R');
//                }
                if ($rowD["categoria"] == 10 && $rowD["afecto_igv"] == 0)
                {
                        $this->SetX($this->GetX() - 2);
                        $Cuotas += $rowD["monto"];
                }

                $this->Cell(2.5, 0.5, number_format($rowD["monto"], 2), 0, 1, 'R');
            }

            if ($rowD["categoria"] == 3) {
                $interesMes += $rowD["monto"];
            }
            if ($rowD["categoria"] == 4) {
                $igvMes += $rowD["monto"];
            }
            if ($rowD["categoria"] == 7) {
                $redondeo += $rowD["monto"];
            }
        }

        $this->SetX($x + 2.3);

        $this->SetFont('Arial', '', 10);

        if ($interesMes > 0) {
            $this->Cell(7.8, 0.5, "MORA(S)", 0, 0, 'L');
            $this->Cell(6.2, 0.5, number_format($interesMes, 2), 0, 1, 'R');
        }

        //echo $meses["deuda"];
        if ($meses["deuda"] != 0) {
            $deuda_anterior = number_format($meses["deuda"], 2);
            //$this->SetXY($x + 2.3, $eny - 0.5);
            $this->SetX($x+2.3);
            $this->Cell(7.8, 0.5, strtoupper("Mes (es) pendientes de ".$meses["meses"]), 0, 0, 'L');
            $this->Cell(6.6, 0.5, $deuda_anterior." + ", 0, 1, 'R');
        }

        // Para los diferentes tipos de recibos
        switch ($tipo) {
            case 1:
                $mensaje_duplicado = "DUPLICADO";
                break;
            case 2:
                $mensaje_duplicado = "DUPLICADO REBAJADO";
                break;
            case 3:
                $mensaje_duplicado = "DUPLICADO CANCELADO";
                //Con Rebaja
                if ($ConRebaja == 1) {
                    $mensaje_duplicado = "DUPLICADO CANCELADO";
                }
                break;
        };

        $this->SetFont('Arial', 'B', 16);
        $this->SetXY(2, (17 + 1.000125));
        $this->Cell(5, 0.5, $mensaje_duplicado, 0, 1, 'C');


        // Ultimo cuadro
        if (intval($meses['mesesdeuda']) > 0) {
            if ($meses['mesesdeuda'] >= 2) {
                $fechavencimiento = $fecha_vencimiento_periodo;
            } else {
                $fvencimiento = $fvencimiento;
                $fechacorte = $fcorte;
                $fv = $fvencimientodeudores;
            }
        } else {

            $fvencimiento = $fvencimiento;
            $fechacorte = "";
            $fv = $fvencimiento;
        }
        
        $this->SetFont('Arial', '', 10);

        $this->SetY(16.2);
        $this->SetX($x + 9);
        $this->Cell(2, 0.5, "SUBTOTAL", 0, 0, 'L');

        if ($deuda_anterior <> 0 or $interesMes <> 0 or $Cuotas <> 0) {  //if ($meses["deuda"] != 0) {
            $this->Cell(5.8, 0.4, number_format(($deuda_anterior + $interesMes + $Cuotas), 2)." + ", 0, 0, 'R');
        } else {
            $this->Cell(5.8, 0.4, " ", 0, 0, 'R');
        }

        $this->Cell(1.4, 0.5, number_format($subtotMes, 2), 0, 1, 'R');

        //$this->SetFont('Arial', '', 10);
        $this->SetX($x + 9);
        $this->Cell(2, 0.5, "Igv ".$paramae["valor"]."%", 0, 0, 'R');
        $this->Cell(7.2, 0.4, number_format($igvMes, 2), 0, 1, 'R');

        $this->SetX($x + 9);
        $this->Cell(2, 0.5, "Redondeo", 0, 0, 'R');
        $this->Cell(7.2, 0.4, number_format($redondeo, 2), 0, 1, 'R');

        if ($vma != 1) {
            if ($meses["deuda"] != 0) {
                $this->Image("../../../../../images/logo_corte.jpg", 8.5, 17.4, 0.9, 0.9);
                $this->Image("../../../../../images/logo_triste.jpg", 7.9, 15, 2.2, 2.2);
            } else {
                $this->Image("../../../../../images/logo_feliz.jpg", 7.9, 15, 2.2, 2.2);
            }
        } else {
            $CodUnidadUso = 0;
            $Tabla1 = TablaParametros($codsuc, $nroinscripion, $CodUnidadUso);
            $ConUU = count($Tabla1);

            $this->SetFont('Arial', 'B', 7);
            $this->SetY(12);
            $this->Cell(2.9, 0.45, "Parametros de Augas", 'LTR', 1, 'C');
            $this->Cell(2.9, 0.45, "Residuales", 'LBR', 0, 'C');
            $this->SetXY($x + 2.9, 12);
            $this->Cell(1.2, 0.9, "DBO5", 1, 0, 'C');
            $this->Cell(1.2, 0.9, "DQO", 1, 0, 'C');
            $this->Cell(1.2, 0.9, "SST", 1, 0, 'C');
            $this->Cell(1.2, 0.9, "AyG", 1, 0, 'C');
            $this->SetXY($x + 7.7, 12);
            $this->Cell(1.2, 0.9, "Factor", 'LTR', 1, 'C');

            $this->Cell(2.9, 0.45, "VMA (mg/L)", 1, 0, 'L');
            $this->Cell(1.2, 0.45, "500", 1, 0, 'C');
            $this->Cell(1.2, 0.45, "1,000", 1, 0, 'C');
            $this->Cell(1.2, 0.45, "500", 1, 0, 'C');
            $this->Cell(1.2, 0.45, "100", 1, 0, 'C');
            $this->SetXY($x + 7.7, 12.45);
            $this->Cell(1.2, 0.9, "Total", 'LBR', 1, 'C');

            $this->SetFont('Arial', '', 7.5);

            for ($i = 0; $i < $ConUU; $i++) {
                $ii = $Tabla1[$i + 1][4][1][2];
                $iii = $Tabla1[$i + 1][4][2][2];
                $iiii = $Tabla1[$i + 1][4][3][2];
                $iiiii = $Tabla1[$i + 1][4][4][2];
                $timp = $Tabla1[$i + 1][3];

                $this->Cell(2.9, 0.5, utf8_decode("Valor de Análisis ").$Tabla1[$i + 1][1], 1, 0, 'L');
                $this->Cell(1.2, 0.5, number_format($Tabla1[$i + 1][4][1][1], 1), 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($Tabla1[$i + 1][4][2][1], 1), 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($Tabla1[$i + 1][4][3][1], 1), 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($Tabla1[$i + 1][4][4][1], 1), 1, 0, 'C');
                $this->Cell(1.2, 0.5, "", 'LTR', 1, 'C');

                $this->Cell(2.9, 0.5, utf8_decode("Factores Individuales "), 1, 0, 'L');
                $this->Cell(1.2, 0.5, number_format($ii, 0)." %", 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($iii, 0)." %", 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($iiii, 0)." %", 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($iiiii, 0)." %", 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($timp, 0)." %", 'LBR', 1, 'C');
            }
        }


        $this->Ln(0.9);
        $this->SetFont('Arial', 'B', 8);

        $totrecibo = $meses["deuda"] + $igvMes + ($redondeo) + $subtotMes + $interesMes + $Cuotas;
        //$this->Cell(15, 0.5, "TOTAL A PAGAR S/.", 0, 0, 'R');
        // $this->SetX($x+9);
        $this->SetXY($x + 9, 18.5);
        $this->SetFont('Arial', 'B', 11);
        $this->Cell(5.5, 0.5, "Total a Pagar", 0, 0, 'R');

        $totrecibo = number_format($totrecibo, 2);
        $TotalF = str_pad($totrecibo, 8, "*", STR_PAD_LEFT);
        $TotalF = "S./ ".$TotalF;

        // $this->SetXY($x+15.8, 18.1);
        $this->SetFont('Arial', 'B', 16);
        $this->SetFillColor(195, 195, 195);
        $this->Cell(3.7, 0.5, $TotalF, 0, 1, 'R', true);
        $this->Ln(0.1);

        $this->SetXY(1.5, 17);

        $NunLetras = CantidadEnLetra($totrecibo);

        if ($this->GetStringWidth($NunLetras) > 84) :
            $NunLetras = $NunLetras." SOLES";
            $this->SetFont('Arial', 'B', 8);
            $this->MultiCell(110, 4, $NunLetras." SOLES", 0, 'J');
        else :
            $this->SetFont('Arial', 'B', 10);
            $this->MultiCell(110, 4, $NunLetras." SOLES", 0, 'J');
            $NunLetras = $NunLetras." SOLES";
        endif;


        /* ---- MENSAJES ---- */
        $y = 10.5;
        $this->SetXY($x, $y + 7.3);
        $this->SetFont('Arial', '', 8.5);
        $this->Cell(18, 0.5, "", 0, 0, 'C');
        $this->Ln(2);

        $this->SetFont('Arial', 'B', 8);

        $this->MultiCell(18, 0.5, utf8_decode(strtoupper($mensaje)), 0, 'C');
        $this->Ln(0.7);

        $this->SetFont('Arial', 'B', 10);
        $this->SetXY($x + 1, $y + 11);

        $this->SetFont('Arial', 'B', 10);

        $this->Cell(11, 0.5, "FECHA DE EMISION: ".$femision, 0, 0, 'L');
        //$this->Cell(7.5, 0.5, utf8_decode($horasabastecimiento), 0, 0, 'L');
        $this->Cell(4.2, 0.5, "ULTIMO DIA DE PAGO: ".$fvencimiento, 0, 0, 'L');
        //$this->Cell(4.5, 0.5, utf8_decode($horasabastecimiento), 0, 1, 'R');

        $this->SetFont('Arial', '', 8);

        $this->SetXY($x + 6.5, $y + 13.5);

        $this->Cell(7, 0.5, utf8_decode($propietario), 0, 0, 'R');
        $this->Cell(3, 0.5, $codcatastro, 0, 0, 'R');
        $this->Cell(2.2, 0.5, $codantiguo, 0, 1, 'R');

        $this->Ln(2);

        $this->SetFont('Arial', 'B', 7);



        if ($meses["deuda"] != 0) {
            $fechavencimiento = $fvencimientodeudores;
            $fechacorte = $fcorte;
            $fv = $fvencimientodeudores;
        } else {
            $fechavencimiento = $fvencimiento;
            $fechacorte = "";
            $fv = $fvencimiento;
        }

        $this->SetFont('Arial', 'B', 9);

        $this->SetXY($x, $y + 14.3);

        $this->Cell(3.9, 0.5, 'FECHA DE EMISION', 0, 0, 'C');
        $this->Cell(5, 0.5, 'ULTIMO DIA DE PAGO', 0, 0, 'C');
        $this->Cell(5, 0.5, $facturacion, 0, 0, 'C');
        $this->Cell(4.8, 0.55, $serie."-".str_pad($recibo, 8, '0', STR_PAD_LEFT), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 12);

        $this->Cell(3.9, 0.7, $femision, 0, 0, 'C');
        $this->Cell(5, 0.7, $fvencimiento, 0, 0, 'C');

        $this->SetFont('Arial', '', 7);
        $this->Cell(5, 0.7, "TOTAL A PAGAR", 0, 0, 'C');
        $this->SetFont('Arial', 'B', 12);

        $this->Cell(4.3, 0.6, utf8_decode(" ".$TotalF), 0, 1, 'R', true);
        $this->Ln(0.4);

        $this->SetFont('Arial', 'B', 8);

        $this->SetXY($x + 2, $y + 15.5);

        $this->Cell(3.4, 0.6, utf8_decode($descripcion_moneda), 0, 0, 'C');
        $this->Cell(3.8, 0.6, utf8_decode(""), 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8.5);

        $this->SetXY(1.5, 26.1);

        $this->SetFont('Arial', '', 11);
        //$this->SetX($x + $xt);
        $this->Cell(18, 0.4, $NunLetras, 0, 1, 'C');

        //Con Rebaja
        if ($ConRebaja == 1 && $tipo != 1) {
            $this->SetXY(12.1, 8.1);
            $this->Cell(7.3, 0.5, "REFACTURADO : ".$consumofact." m3", 0, 0, 'C', true);
        }

        if ($consumofact <> $consumo && $ConRebaja <> 1) {
            $this->SetXY(12.5, 8.2);

            $this->SetFont('Arial', 'B', 9);

            $this->Cell(7, 0.4, "Consumo Fact. ".$consumofact." m3", 0, 0, 'C', true);
            //$this->Cell(2, 0.4, utf8_decode("Consumo Fact."), 0, 0, 'L');
//				$this->Cell(2, 0.4, utf8_decode((int)$consumofact." m3"), 0, 0, 'C');
//				$this->Cell(2, 0.4, utf8_decode(""), 0, 0, 'C');
//				$this->Cell(1.7, 0.4, utf8_decode(""), 0, 1, 'C');
        }

        $this->SetFont('Arial', 'B', 9);

        $this->SetXY(11.9, 0.5);
        $this->Cell(7.8, 0.4, '', 0, 0, 'C', 0); // $Empresa[0]

        $this->SetFont('Arial', '', 8);

        $this->SetXY(11.9, 0.9);
        $this->Cell(7.8, 0.4, '', 0, 0, 'C', 0); // $Empresa[1]

        $this->SetXY(11.9, 1.3);
        $this->Cell(7.8, 0.4, '', 0, 0, 'C', 0); //'R.U.C. : '.$Empresa[3]
    }

    function Footer() {
        
    }

    //  Para los cuadros con bordes redondeados o sin ellos
    function RoundedRect($x, $y, $w, $h, $r, $style = '') {
        $k = $this->k;
        $hp = $this->h;
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op = 'S';
        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m', ($x + $r) * $k, ($hp - $y) * $k));
        $xc = $x + $w - $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - $y) * $k));

        $this->_Arc($xc + $r * $MyArc, $yc - $r, $xc + $r, $yc - $r * $MyArc, $xc + $r, $yc);
        $xc = $x + $w - $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc + $r, $yc + $r * $MyArc, $xc + $r * $MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x + $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - ($y + $h)) * $k));
        $this->_Arc($xc - $r * $MyArc, $yc + $r, $xc - $r, $yc + $r * $MyArc, $xc - $r, $yc);
        $xc = $x + $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1 * $this->k, ($h - $y1) * $this->k, $x2 * $this->k, ($h - $y2) * $this->k, $x3 * $this->k, ($h - $y3) * $this->k));
    }

}

$nroinscripcion = $_GET["nroinscripcion"];
$codsuc   = $_GET["codsuc"];
$codciclo = $_GET["codciclo"];
$tipo     = $_GET["tipo"];
// $items          = array();

if ($_SESSION["nrofacturacion"] == '') {
    $sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
    $sql .= "FROM facturacion.periodofacturacion ";
    $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND codciclo = ".$codciclo." AND facturacion = 0";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array());
    $items = $consulta->fetch();

    $_SESSION["nrofacturacion"] = $items["nrofacturacion"];
}

$nrofacturacion = $_SESSION["nrofacturacion"] - 1; //238

define('Cubico', chr(179));

$sqlP = "SELECT nrofacturacion, anio, mes, fechaemision, mensajegeneral, fechavencnormal, ";
$sqlP .= " fechavencdeudores, fechacorte, mensajenormal ";
$sqlP .= "FROM facturacion.periodofacturacion ";
$sqlP .= "WHERE codsuc = ".$codsuc." AND codciclo = ".$codciclo." ";
$sqlP .= " AND nrofacturacion = ".$nrofacturacion;

$consultaP = $conexion->prepare($sqlP);
$consultaP->execute(array());
$itemP = $consultaP->fetch();

$objReporte = new clsImpresionDuplicado('L', 'cm', array(28.00, 20.00));

$sql = "SELECT * FROM facturacion.view_cabfacturacion ";
$sql .= "WHERE codsuc = ".$codsuc." ";
$sql .= " AND nroinscripcion = ".$nroinscripcion." ";
$sql .= " AND nrofacturacion = ".$nrofacturacion." ";
$sql .= "ORDER BY codrutdistribucion, loteantiguo ";

$consulta = $conexion->prepare($sql);
$consulta->execute(array());
$items = $consulta->fetch();

if ($items["codcatastro"] == '') {
    $Sql = "SELECT MAX(nrofacturacion) ";
    $Sql .= "FROM facturacion.cabfacturacion ";
    $Sql .= "WHERE codsuc = ".$codsuc." ";
    $Sql .= " AND nroinscripcion = ".$nroinscripcion;

    $Consulta = $conexion->query($Sql);
    $row = $Consulta->fetch();
    //echo $row[0];
    if ($row[0] == '') {
        echo '<script type="text/javascript">alert("No se encontro datos");close();</script>';
        die();
    }

    $sql = "SELECT * FROM facturacion.view_cabfacturacion ";
    $sql .= "WHERE codsuc = ".$codsuc." ";
    $sql .= " AND nroinscripcion = ".$nroinscripcion." ";
    $sql .= " AND nrofacturacion = ".$row[0]." ";
    $sql .= "ORDER BY codrutdistribucion, loteantiguo";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array());
    $items = $consulta->fetch();
    $nrofacturacion = $row[0];
}

$Empresa = $objReporte->datos_empresa($codsuc);

$objReporte->AddPage();
$objReporte->SetAutoPageBreak(true, 1);
$facturacion = substr($meses[$itemP["mes"]], 0, 3)."-".$itemP["anio"];

$objReporte->SetFillColor(255, 255, 255);
$objReporte->RoundedRect(1, 0.5, 10.5, 1.3, 0, 'DF');  // Para la cabecera

$objReporte->RoundedRect(1, 4.3, 10.5, 1.3, 0, 'DF');  // Para el periodo

$objReporte->RoundedRect(1, 6.2, 10.5, 2.5, 0, 'DF');  // Para los lectura periodos 1°
$objReporte->RoundedRect(12, 6.6, 7.7, 2.1, 0, 'DF');  // Para los lectura periodos 2°


$objReporte->RoundedRect(1, 9.5, 18.7, 10, 0.3, 'DF');  // Para los conceptos
$objReporte->RoundedRect(1, 19.65, 18.7, 2.5, 0.3, 'DF'); // Para el mensaje
$objReporte->Line(1, 21.3, 19.7, 21.3); // Cuadro para bauche caja

$objReporte->RoundedRect(1, 24.5, 18.7, 1.5, 0.3, 'DF'); // Cuadro para bauche caja
$objReporte->Line(4.9, 24.5, 4.9, 26); // 1° linea Cuadro para bauche caja
$objReporte->Line(9.9, 24.5, 9.9, 26); // 2° linea Cuadro para bauche caja
$objReporte->Line(9.9, 25.3, 19.7, 25.3); // 3° linea horizontal Cuadro para bauche caja

$objReporte->Contenido($tipo, $nroinscripcion, $items["codcatastro"], $items["ruc"], $facturacion, $items["nrodocumento"], $objReporte->DecFecha($itemP["fechaemision"]), $items["propietario"], $items["direccion"], $items["descripcion"], $items["tipofacturacion"], $items["nomtar"], $items["nromed"], $objReporte->DecFecha1($items["fechalectanterior"]), $items["lecturaanterior"], $items["consumo"], $objReporte->DecFecha1($items["fechalectultima"]), $items["lecturaultima"], $itemP["mensajegeneral"], $objReporte->DecFecha($itemP["fechavencdeudores"]), $objReporte->DecFecha($itemP["fechacorte"]), $objReporte->DecFecha($items["fechavencimiento"]), $items["docidentidad"], $items["nrodocidentidad"], $itemP['mensajenormal'], $items['codantiguo'], $items['zona'], $items['tipoactividad'], $items['catetar'], $items['consumofact'], $items['ruta'], $items['serie'], $items['vma'], $nrofacturacion, $items['observacionabast'], $items['codtiposervicio'], $items['descripcion'], $items['fechavencimiento3'], $itemP['anio'].$itemP['mes']);

foreach (glob("*.jpg") as $filename) {
    unlink($filename);
}

//$objReporte->Output();
$objReporte->SetTitle(':: DUPLICADO ORIGINAL - '.$nroinscripcion.' ::');
if($tipo ==2 ) $objReporte->SetTitle(':: DUPLICADO REBAJADO - '.$nroinscripcion.' ::');
$modo="I";
$nombre_archivo = "DuplicadoOriginal_".$nroinscripcion.'.pdf';
if($tipo ==2 ) $nombre_archivo = "DuplicadoRebajado_".$nroinscripcion.'.pdf';
$objReporte->Output($nombre_archivo,$modo);

?>
