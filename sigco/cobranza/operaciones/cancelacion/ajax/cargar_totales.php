<?php 
	include("../../../../../config.php");
	
	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$facturaciones 	= $_POST["FacturacionesDel"]?$_POST["FacturacionesDel"]:'';
	$imptotal 		= 0;
	$impredondeo	= 0;
	$impigv			= 0;
	
	if($facturaciones != '')
	{
		$facturaciones = " (".substr($facturaciones, 0, strlen($facturaciones) - 1).") ";
        $facturaciones = " AND d.nrofacturacion NOT IN ".$facturaciones;
	}
	
	$sql = "SELECT SUM(d.importe - (d.importerebajado + d.imppagado)) AS imptotal, c.categoria ";
	$sql .= "FROM facturacion.cabfacturacion cab ";
	$sql .= " INNER JOIN facturacion.detfacturacion d ON(cab.codemp = d.codemp) AND (cab.codsuc = d.codsuc) ";
	$sql .= "  AND (cab.nrofacturacion = d.nrofacturacion) AND (cab.nroinscripcion = d.nroinscripcion) AND (cab.codciclo = d.codciclo) ";
	$sql .= " INNER JOIN facturacion.conceptos c ON(c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.codconcepto = d.codconcepto) ";
	$sql .= "WHERE d.codemp = 1 AND d.codsuc = ".$codsuc." ";
	$sql .= " AND d.nroinscripcion = ".$nroinscripcion." ";
	$sql .= " AND d.categoria <> 2 AND d.estadofacturacion IN(1, 3) ";
	$sql .= " AND enreclamo = 0  ".$facturaciones." ";
	$sql .= " AND codtipodeuda NOT IN (11) ";
	$sql .= "GROUP BY c.categoria";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array());
	$items = $consulta->fetchAll();
	
	$temp = 0;
	
	foreach($items as $row)
	{
		if($row["categoria"] != 4 && $row["categoria"] != 7){$imptotal += $row["imptotal"];}
		if($row["categoria"] == 7){$impredondeo += $row["imptotal"];}
		if($row["categoria"] == 4){$impigv += $row["imptotal"];}
		if($row["categoria"] == 7){$temp += $row["imptotal"];}
	}
//	$imptotal=$imptotal-$temp;
//	$imptotal = round($imptotal,1);
	//$imptotal=$imptotal-$temp;

	echo number_format($imptotal, 2)."|".number_format($impredondeo, 2)."|".number_format($impigv, 2)."|".number_format(($imptotal + $impredondeo + $impigv), 2);
	
?>