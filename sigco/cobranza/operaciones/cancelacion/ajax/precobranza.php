<?php 
	include("../../../../../config.php");
	
	$nroprepago = $_POST["nroprepago"];
	$codsuc		= $_POST["codsuc"];
	$nroinscripcion		= $_POST["nroinscripcion"];
	$sqlC = "select propietario, direccion,documento,eventual,igv,redondeo,nroinspeccion,glosa
			from cobranza.cabprepagos 
			where codemp=1 and codsuc=? and nroprepago=? and nroinscripcion=?";
	
	$consultaC = $conexion->prepare($sqlC);	
	$consultaC->execute(array($codsuc,$nroprepago,$nroinscripcion));
	$itemsC = $consultaC->fetch();

	$sql = "select * from cobranza.view_detalleprepago 
	where codemp=1 and codsuc=? and  nroprepago=? and nroinscripcion=?
	order by codtipodeuda";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroprepago,$nroinscripcion));
	$itemsP = $consulta->fetchAll();

	$tr		= "";
	$count 	= 0;
	
	$igv = $itemsC["igv"] / count($itemsP);
	$red = $itemsC["redondeo"] / count($itemsP);
	
	foreach($itemsP as $rowP)
	{
		

		$SubT = str_replace(',', '', $rowP["imptotal"]);
		
		$SubT = $SubT - $igv - $red;
		
		$count++;
		
		$tr .= "<tr style='background-color:#FFF' id='tr_".$count."'>";
		$tr .= "<td align='center'>
				<input type='hidden' name='nrofacturacion".$count."' id='nrofacturacion".$count."' value='".$rowP["nrofacturacion"]."' />".$rowP["nrofacturacion"]."</td>";
		$tr .= "<td align='center'>".$rowP["documento"]."</td>";
		$tr .= "<td align='center'>".$rowP["nrodocumento"]."</td>";
		$tr .= "<td align='center'>
				<input type='hidden' name='anioy".$count."' id='anioy".$count."' value='".$rowP["anio"]."' />
				<input type='hidden' name='mesy".$count."' id='mesy".$count."' value='".$rowP["mes"]."' />".$rowP["anio"]." - ".$meses[$rowP["mes"]]."</td>";
		$tr .= "<td align='center'>
				<input type='hidden' name='codtipodeuda".$count."' id='codtipodeuda".$count."' value='".$rowP["codtipodeuda"]."' />".$rowP["tipodeu"]."</td>";
		$tr .= "<td align='center'>
				<input type='hidden' name='codconceptox".$count."' id='codconceptox".$count."' value='0'/>COBRANZA - ".$rowP["detalle"]."
				<input type='hidden' name='detallex".$count."' id='detallex".$count."' value='".$rowP["detalle"]."'/></td>";
		$tr .= "<td align='center'>
				<input type='hidden' name='codcategoria".$count."' id='codcategoria".$count."' value='".$rowP["codcategoria"]."'/>".$rowP["descategoria"]."</td>";
		$tr .= "<td align='right'>
				<input type='hidden' name='igvadd".$count."' id='igvadd".$count."' value='".$igv."' />	
				<input type='hidden' name='redadd".$count."' id='redadd".$count."' value='".$red."' />
				<input type='hidden' name='npresupuesto".$count."' id='npresupuesto".$count."' value='".$rowP["nropresupuesto"]."' />
				<input type='hidden' name='imptotal".$count."' id='imptotal".$count."' value='".$SubT."' />".number_format($rowP["imptotal"],2)."</td>";
		$tr .= "<td align='center'>&nbsp;</td>";
		$tr .= "</tr>";
		//}
	}	
	
	echo $tr."|".$count."|".$itemsC["propietario"]."|".$itemsC["direccion"]."|".$itemsC["documento"]."|".$itemsC["eventual"]."|".$itemsC["nroinspeccion"]."|".$itemsC["glosa"];
	
?>