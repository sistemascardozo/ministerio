<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../config.php");
	
	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"]; 
		
	$count 	= 0;
	$sql = "SELECT * FROM cobranza.view_cobranza ";
	$sql .= "WHERE codemp = 1 AND codsuc = ? AND nroinscripcion = ? /*and imptotal>0 */ ";
	$sql .= "ORDER BY nrofacturacion";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroinscripcion));
	$items = $consulta->fetchAll();
//var_dump($consulta->errorInfo());
	$tr		= "";
	$count	= 0;
	
	$car = isset($_SESSION["car2"])?$_SESSION["car2"]:0;
	
	$Igv = 0;
	$Red = 0;
	$Int = 0;
	
	foreach($items as $row)
	{
		if($row['codtipodeuda'] != 11) //NO QUEBRADO
		{
			$count++;
			$onClickDetalle = "abrir_detalle_facturacion(".$row["nrofacturacion"].", ".$row["cat"].", ".$row["codtipodeuda"].")";
			
			$Igv = 0;
			$Red = 0;
			$Int = 0;
			
			if ($row["codtipodeuda"] == 1 or $row["codtipodeuda"] == 10)
			{
				$sqlIgv = "SELECT importe - importerebajado FROM facturacion.detfacturacion ";
				$sqlIgv .= "WHERE codemp = 1 ";
				$sqlIgv .= " AND codsuc = ".$codsuc." ";
				$sqlIgv .= " AND nrofacturacion = ".$row["nrofacturacion"]." ";
				$sqlIgv .= " AND nroinscripcion = ".$nroinscripcion." ";
				$sqlIgv .= " AND codconcepto = 5 ";
				$sqlIgv .= "ORDER BY nrofacturacion";
				
				$consultaIgv = $conexion->prepare($sqlIgv);
				$consultaIgv->execute(array());
				$itemIgv = $consultaIgv->fetch();
				
				$Igv = $itemIgv[0];
				
				$sqlRed = "SELECT importe - importerebajado FROM facturacion.detfacturacion ";
				$sqlRed .= "WHERE codemp = 1 ";
				$sqlRed .= " AND codsuc = ".$codsuc." ";
				$sqlRed .= " AND nrofacturacion = ".$row["nrofacturacion"]." ";
				$sqlRed .= " AND nroinscripcion = ".$nroinscripcion." ";
				$sqlRed .= " AND (codconcepto = 8 OR codconcepto = 7) ";
				$sqlRed .= "ORDER BY nrofacturacion";
				
				$consultaRed = $conexion->prepare($sqlRed);
				$consultaRed->execute(array());
				$itemRed = $consultaRed->fetch();
				
				$Red = $itemRed[0];
				
				$sqlInt = "SELECT importe - importerebajado FROM facturacion.detfacturacion ";
				$sqlInt .= "WHERE codemp = 1 ";
				$sqlInt .= " AND codsuc = ".$codsuc." ";
				$sqlInt .= " AND nrofacturacion = ".$row["nrofacturacion"]." ";
				$sqlInt .= " AND nroinscripcion = ".$nroinscripcion." ";
				$sqlInt .= " AND codconcepto IN (3, 4, 9, 71, 72, 10009, 10013) ";
				$sqlInt .= "ORDER BY nrofacturacion";
				
				$consultaInt = $conexion->prepare($sqlInt);
				$consultaInt->execute(array());
				$itemInt = $consultaInt->fetch();
				
				$Int = isset($itemInt[0])?$itemInt[0]:0;
			}
			
			if($_SESSION["ventana"] == "cobranza")
			{
				$onclick = "agregar_facturacion(".$count.")";
			}
			else
			{
				$onclick  = "cargar_monto_amortizar(".$nroinscripcion.", ".$row["nrofacturacion"].", ".$count.", ".$row["coddocumento"].", ".$row["nrodocumento"];
				$onclick .= ", ".$row["codtipodeuda"].", ".$row["anio"].", ".$row["mes"].", ".$row["cat"].");";
			}
?>
<tr style='background-color:#FFF' >
	<td align='center'>
		<input type='hidden' name='nrofact<?=$count?>' id='nrofact<?=$count?>' value='<?=$row["nrofacturacion"]?>' /><?=$row["nrofacturacion"]?></td>
	<td align='center'>
		<input type='hidden' name='doc<?=$count?>' id='doc<?=$count?>' value='<?=$row["abreviado"]?>' /><?=$row["abreviado"]?></td>
	<td align='center'>
		<input type='hidden' name='nrodoc<?=$count?>' id='nrodoc<?=$count?>' value='<?=$row["nrodocumento"]?>' /><?=$row["nrodocumento"]?></td>
	<td align='left' style="padding-left:5px;">
        <input type='hidden' name='anio<?=$count?>' id='anio<?=$count?>' value='<?=$row["anio"]?> - <?=$meses[$row["mes"]]?>' />
        <input type='hidden' name='anioz<?=$count?>' id='anioz<?=$count?>' value='<?=$row["anio"]?>' />
        <input type='hidden' name='mesz<?=$count?>' id='mesz<?=$count?>' value='<?=$row["mes"]?>' /><?=$row["anio"]?> - <?=$meses[$row["mes"]]?></td>
	<td align='left' style="padding-left:5px;">
        <input type='hidden' name='codtipodeuda<?=$count?>' id='codtipodeuda<?=$count?>' value='<?=$row["codtipodeuda"]?>' />
        <input type='hidden' name='tipd<?=$count?>' id='tipd<?=$count?>' value='<?=$row["deuda"]?>' /><?=$row["deuda"]?></td>
	<td align='center'>
        <input type='hidden' name='catg<?=$count?>' id='catg<?=$count?>' value='<?=$row["cat"]?>' />
        <input type='hidden' name='cat<?=$count?>' id='cat<?=$count?>' value='<?=$row["categoria"]?>' /><?=$row["categoria"]?></td>
	<td align='right' style="padding-right:5px;">
		<input type='hidden' name='int<?=$count?>' id='int<?=$count?>' value='<?=$Int?>' />
		<input type='hidden' name='igv<?=$count?>' id='igv<?=$count?>' value='<?=$Igv?>' />
		<input type='hidden' name='red<?=$count?>' id='red<?=$count?>' value='<?=$Red?>' />
		<input type='hidden' name='imp<?=$count?>' id='imp<?=$count?>' value='<?=$row["imptotal"]?>' />
		<?=number_format($row["imptotal"], 2)?></td>
	<td align='center'>
		<!--<img src='../../../../images/ver.png' width='16' height='16' onclick='<?=$onClickDetalle?>' title='Ver Detalle de Facturacion' style='cursor:pointer' />-->
		<span class='icono-icon-detalle' onclick='<?=$onClickDetalle?>' title='Ver Detalle de Facturacion' ></span>
<?php
			if($row['enreclamo'] == 0 && $car == 0)
			{
?>
		<!--<img src='../../../../images/add.png' width='16' height='16' onclick='<?=$onclick?>' title='Agregar Facturacion para la Cobranza' style='cursor:pointer' />-->
		<span class='icono-icon-add' onclick='<?=$onclick?>' title='Agregar Facturacion para la Cobranza'></span>
<?php
			}
			else
			{
				if($row['enreclamo'] != 0)
				{
?>
		<span class='icono-icon-info' title='Facturación con Reclamo en proceso'></span>
<?php
				}
			}
			
?>
	</td>
</tr>
<?php
		}
	}
	
	echo $tr."|".$count;
	
?>