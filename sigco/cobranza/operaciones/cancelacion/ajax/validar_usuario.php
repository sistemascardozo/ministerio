<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsseguridad.php");
	
	$objSeguridad = new clsSeguridad();
	
	$codsuc		= $_SESSION['IdSucursal'];
	
	$car		= $_POST["car"];
	$fecha		= $_POST["fdescarga"];
	
	/*$usuario 	= strtoupper($_SESSION['user']);
	$contra		= strtoupper($_POST['contra']);
	$contra 	= $objSeguridad->encriptar($contra);*/

	$usuario 	= trim(strtoupper($_SESSION['user']));
	$contra 	= strtoupper($_POST["contra"]);
	$contra = strtoupper(trim($objSeguridad->encriptar($contra)));
	
	$sql  = "SELECT usu.nombres, cjusu.nrocaja ";
	$sql .= "FROM seguridad.usuarios AS usu ";
	$sql .= "INNER JOIN cobranza.cajasxusuario AS cjusu ON(usu.codusu = cjusu.codusu)";
	$sql .= "WHERE UPPER(usu.login) = ? AND UPPER(usu.contra) = UPPER(?) AND cjusu.codsuc = ?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($usuario,$contra,$codsuc));
	
	if($items = $consulta->fetch())
	{
		$res 	 = 0;
		$mensaje = "";
		
		$_SESSION["nocaja"] = $items["nrocaja"];
		$_SESSION["car"] 	= $car;
	}
	else
	{
		$res = 1;
		$mensaje = "La Contraseña del Usuario no es valida.<br>";
        $mensaje .= "O el usuario no tiene asignada una CAJA";
	}
	
	echo $res."|".$mensaje;
?>