<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();

	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$codestadoservicio = $_POST["codestadoservicio"];
	$TasaInteres = $_POST["TasaInteres"];
	$fdescarga = $objFunciones->CodFecha($_POST["fdescarga"]);

	$sql = "SELECT * from cobranza.view_cobranza ";
	$sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
	$sql .= " AND nroinscripcion = ".$nroinscripcion." AND imptotal>0 ";
	$sql .= "ORDER BY nrofacturacion";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array());
	$items = $consulta->fetchAll();
//var_dump($consulta->errorInfo());
	$tr		= "";
	$count	= 0;
	$dias 	=0;
	$importe =0;

	$car = isset($_SESSION["car2"])?$_SESSION["car2"]:0;

	$Meses = 0;
	$Nfact = 0;
	$NfactA = 0;

	foreach($items as $row)
	{
		if($row['enreclamo'] == 0 && $row['codtipodeuda'] != 11)
		{
			$count++;

			$Nfact = $row["nrofacturacion"];

			if ($Nfact != $NfactA && $row["nrodocumento"] > 0)
			{
				$Meses++;

				$NfactA = $Nfact;
			}

			$tr .= "<tr style='background-color:#FFF' id='tr_".$count."'>";
			$tr .= "<td align='center'>
					<input type='hidden' name='nrofacturacion".$count."' id='nrofacturacion".$count."' value='".$row["nrofacturacion"]."' />".$row["nrofacturacion"]."</td>";
			$tr .= "<td align='center'>".$row["abreviado"]."</td>";
			$tr .= "<td align='center'>".$row["nrodocumento"]."</td>";
			$tr .= "<td align='left' style='padding-left:5px;'>
				    <input type='hidden' name='anioy".$count."' id='anioy".$count."' value='".$row["anio"]."' />
					<input type='hidden' name='mesy".$count."' id='mesy".$count."' value='".$row["mes"]."' />".$row["anio"]." - ".$meses[$row["mes"]]."</td>";
			$tr .= "<td align='left' style='padding-left:5px;'>
					<input type='hidden' name='codtipodeuda".$count."' id='codtipodeuda".$count."' value='".$row["codtipodeuda"]."' />".$row["deuda"]."</td>";
			$tr .= "<td align='center'>
					<input type='hidden' name='codconceptox".$count."' id='codconceptox".$count."' value='0'/>COBRANZA - ".$row["deuda"]."
					<input type='hidden' name='detallex".$count."' id='detallex".$count."' value='COBRANZA - ".$row["deuda"]."'/></td>";
			$tr .= "<td align='center'>
					<input type='hidden' name='codcategoria".$count."' id='codcategoria".$count."' value='0'/>".$row["categoria"]."</td>";
			$tr .= "<td align='right' style='padding-right:5px;'>
					<input type='hidden' name='imptotal".$count."' id='imptotal".$count."' value='".$row["imptotal"]."' />".number_format($row["imptotal"],2)."</td>";
			$tr .= "<td align='center'>";
			//$tr .= "<img src='../../../../images/cancel.png' width='16' height='16' title='Borrar Registros' style='cursor:pointer' onclick='open_validar_remover_item(".$count.");' />";
if ($car == 0)
{
			$tr .= "<span class='icono-icon-trash' title='Quitar Registro' onclick='open_validar_remover_item(".$count.");'></span>";
}
			$tr .= "</td>";
			$tr .= "</tr>";
			/*if($codestadoservicio==2)//CORTADO
			{*/
				$fechavencimiento = $row["fechavencimiento"];

				if($fdescarga > $fechavencimiento)
				{
					$dias = $objFunciones->dias_transcurridos($fechavencimiento, $fdescarga);
					$importe += round($row["imptotal"] * $dias * ($TasaInteres / 30), 1);

					//echo $fdescarga." ".$fechavencimiento." ".$TasaInteres."<br>";
					//die($fechavencimiento.','.$fdescarga.'==>'.$dias);
				}
			/*}*/
		}
	}

	echo $tr."|".$count."|".$dias."|".$importe."|".$Meses;

?>
