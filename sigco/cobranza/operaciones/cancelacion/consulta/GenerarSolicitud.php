<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../objetos/clsFunciones.php");

	$objFunciones	= new clsFunciones();
	$Pago			= $_POST["pago"]?$_POST["pago"]:0;
	$tipo			= $_POST["tipo"];
	$codsuc			= $_POST["codsuc"];
	$nroinscripcion	= $_POST["nroinscripcion"];
	$nrocredito		= $_POST["nrocredito"]?$_POST["nrocredito"]:0;
	$nrorefinanciamiento = $_POST["nrorefinanciamiento"]?$_POST["nrorefinanciamiento"]:0;
	$nrocargo		= $_POST["nrocargo"]?$_POST["nrocargo"]:0;
	$horareg		= $objFunciones->HoraServidor();
	$codemp			= 1;
	$codzonat		= 0;
	$codsectort		= 0;
	$codmanzanat	= 0;
	$lotet			= '';
	$sublotet		= '';
	$codclientet	= 0;
	$codcallet		= 0;
	$nrocallet		= '';
	$propietariot	= '';
	$direcciont		= '';
	$cancelado		= 1;

	if($tipo == 1) //PREPAGO
	{
		$cancelado = 0;

		$Sql = "SELECT * FROM cobranza.cabprepagos WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nroprepago=:nroprepago";

		$result = $conexion->prepare($Sql);
		$result->execute(array(":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":nroinscripcion"=>$nroinscripcion,
						   ":nroprepago"=>$Pago));

		$row          = $result->fetch();

		$codinspector = 0;//defecto
		$glosa        = $row["glosa"];
		$fechaemision = $row["fechareg"];
		$idusuario    = $_SESSION['id_user'];
		$propietariot = $row['propietario'];
		$direcciont   = $row['direccion'];

		$Sql ="SELECT * FROM cobranza.detprepagos WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nroprepago=:nroprepago";

		$result = $conexion->prepare($Sql);
		$result->execute(array(":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":nroinscripcion"=>$nroinscripcion,
						   ":nroprepago"=>$Pago));

		$row = $result->fetch();
		$codconcepto = $row["codconcepto"];
	}
	else
	{
		if($Pago != 0)
		{
			$Sql ="SELECT * FROM cobranza.cabpagos WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nropago=:nropago";

			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
							   ":codsuc"=>$codsuc,
							   ":nroinscripcion"=>$nroinscripcion,
							   ":nropago"=>$Pago));

			$row = $result->fetch();

			$codinspector	= 0;//defecto
			$glosa			= $row["glosa"];
			$fechaemision	= $row["fechareg"];
			$idusuario		= $_SESSION['id_user'];
			$direccion		= $row['direccion'];
			$propietario	= $row['propietario'];
			$propietariot 	= $row["propietario"];
			$direcciont 	= $row["direccion"];

			$Sql ="SELECT * FROM cobranza.detpagos WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nropago=:nropago";

			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
							   ":codsuc"=>$codsuc,
							   ":nroinscripcion"=>$nroinscripcion,
							   ":nropago"=>$Pago));

			$row = $result->fetch();
			$codconcepto	= $row["codconcepto"];
		}
		else
		{
			if($nrocredito != 0)
			{
				$cancelado = 0;

				$Sql = "SELECT * FROM facturacion.cabcreditos WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nrocredito=:nrocredito";

				$result = $conexion->prepare($Sql);
				$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":nroinscripcion"=>$nroinscripcion,
								   ":nrocredito"=>$nrocredito));

				$row = $result->fetch();

				$codinspector	= 0;//defecto
				$glosa			= $row["glosa"];
				$fechaemision	= $row["fechareg"];
				$idusuario		= $_SESSION['id_user'];
				$Cnroprepago    = $row["nroprepago"];
				$CnroprepagoInicial = $row["nroprepagoinicial"];
				$codconcepto	= $row["codconcepto"];// estaba igualando a cero

				$propietariot	= $row["propietario"];
				$direcciont 	= $row["direccion"];
				$codzonat		= 0;
				$codsectort		= 0;
				$codmanzanat	= 0;
				$lotet			= '';
				$sublotet		= '';
				$codclientet	= 0;
				$codcallet		= 0;
				$nrocallet		= '';
			}

			if($nrorefinanciamiento != 0)
			{
				$cancelado = 0;

				$Sql ="SELECT * FROM facturacion.cabrefinanciamiento
								WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion
								AND nrorefinanciamiento=:nrorefinanciamiento";

				$result = $conexion->prepare($Sql);
				$result->execute(array(":codemp"=>$codemp,
									   ":codsuc"=>$codsuc,
									   ":nroinscripcion"=>$nroinscripcion,
									   ":nrorefinanciamiento"=>$nrorefinanciamiento));

				$row = $result->fetch();

				$codinspector	= 0;//defecto
				$glosa			= $row["glosa"];
				$fechaemision	= $row["fechaemision"];
				$idusuario		= $_SESSION['id_user'];
				$Cnroprepago    = $row["nroprepago"];
				$CnroprepagoInicial = $row["nroprepagoinicial"];
				$codconcepto	= $row["codconcepto"];// estaba igualando a cero
			}

			if($nrocargo != 0)
			{
				$cancelado = 0;

				$Sql = "SELECT * FROM facturacion.cabvarios WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nrocredito=:nrocredito";

				$result = $conexion->prepare($Sql);
				$result->execute(array(":codemp"=>$codemp,
							   ":codsuc"=>$codsuc,
							   ":nroinscripcion"=>$nroinscripcion,
							   ":nrocredito"=>$nrocargo));

				$row = $result->fetch();

				$codinspector	= 0;//defecto
				$glosa			= $row["glosa"];
				$fechaemision	= $row["fechareg"];
				$idusuario		= $_SESSION['id_user'];
				$Cnroprepago    = $row["nroprepago"];
				$CnroprepagoInicial = $row["nroprepagoinicial"];
				$codconcepto	= $row["codconcepto"];// estaba igualando a cero

				$propietariot	= $row["propietario"];
				$direcciont		= $row["direccion"];
				$codzonat		= 0;
				$codsectort		= 0;
				$codmanzanat	= 0;
				$lotet			= '';
				$sublotet		= '';
				$codclientet	= 0;
				$codcallet		= 0;
				$nrocallet		= '';
			}
		}
	}

	$consulta   = $conexion->prepare("SELECT * FROM facturacion.conceptos WHERE codconcepto=? AND codsuc=?");
	$consulta->execute(array($codconcepto, $codsuc));
	$row = $consulta->fetch();

	$diasatencion = 0;//$row["diasatencion"];

	if($glosa == '')
	{
		$glosa = '  ';//$row["descripcion"];
	}
	if($diasatencion == '')
	{
		$diasatencion = 0;
	}

	//$id   			= $objFunciones->setCorrelativos("solicitudinspeccion",$codsuc,"0");
	//$nroinspeccion 	= $id[0];
	$nroinspeccion = $objFunciones->setCorrelativosVarios(21, $codsuc, "SELECT", 0);

	if($nroinscripcion != 0)
	{
			$sql = "SELECT * FROM catastro.clientes WHERE codemp=? and codsuc=? and nroinscripcion=?";

			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codemp, $codsuc, $nroinscripcion));
			$row = $consulta->fetch();

			if($row[0]!='')
			{
				$codzona = $row['codzona'];
				$codsector = $row['codsector'];
				$codmanzana = $row['codmanzanas'];
				$lote = $row['lote'];
				$sublote = $row['sublote'];
				$codcliente = $row['codcliente'];
				$propietario = $row['propietario'];
				$codcalle = $row['codcalle'];
				$nrocalle = $row['nrocalle'];

				$Sql = "select tipcal.descripcioncorta || ' ' || cal.descripcion
						FROM  public.calles as cal
						inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
						WHERE cal.codemp=:codemp and cal.codsuc=:codsuc and cal.codcalle=:codcalle AND cal.codzona=:codzona";
				$result = $conexion->prepare($Sql);
				$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":codcalle"=>$codcalle,
								   ":codzona"=>$codzona));
				$row = $result->fetch();
				$direccion = $row[0].' #'.$nrocalle;
			}
		}
		else
		{

			$codzona    = $codzonat;
			$codsector  = $codsectort ;
			$codmanzana = $codmanzanat;
			$lote       = $lotet;
			$sublote    = $sublotet;
			$codcliente = $codclientet ;
			$codcalle   = $codcallet;
			$nrocalle   = $nrocallet;
		}
		if($propietario=='')
		{
			$propietario=$propietariot;
			$direccion = $direcciont;
		}
		$conexion->beginTransaction();

		$sql = "insert into catastro.solicitudinspeccion
					(nroinspeccion,codemp,codsuc,nroinscripcion,fechaemision,glosa,creador,codinspector,codconcepto,diasatencion,horareg,
						codzona,codsector,codmanzanas,lote,sublote,codcliente,propietario,codcalle,nrocalle,nuevocliente,estado,direccion,cancelado)
				values(:nroinspeccion,:codemp,:codsuc,:nroinscripcion,:fechaemision,:glosa,:creador,:codinspector,:codconcepto,:diasatencion,:horareg,
				:codzona,:codsector,:codmanzana,:lote,:sublote,:codcliente,:propietario,:codcalle,:nrocalle,0,2,:direccion,:cancelado)";

	$result = $conexion->prepare($sql);
	$result->execute(array(":nroinspeccion"=>$nroinspeccion,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":nroinscripcion"=>$nroinscripcion,
						   ":fechaemision"=>$fechaemision,
						   ":glosa"=>$glosa,
						   ":creador"=>$idusuario,
						   ":codinspector"=>$codinspector,
						   ":codconcepto"=>$codconcepto,
						   ":diasatencion"=>$diasatencion,
						   ":horareg"=>$horareg,
							":codzona"=>$codzona,
							":codsector"=>$codsector,
							":codmanzana"=>$codmanzana,
							":lote"=>$lote,
							":sublote"=>$sublote,
							":codcliente"=>$codcliente,
							":propietario"=>$propietario,
							":codcalle"=>$codcalle,
							":nrocalle"=>$nrocalle,
							":direccion"=>$direccion,
							":cancelado"=>$cancelado
						   ));

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		$data['res']    =2;
    	die(json_encode($data));
	}

//ACTUALIZAR EL NUMERO DE INSPECCION
$addglosa="\n";
if($tipo==1) //PREPAGO
{
	/////////INSERTO DETALLE DE LA ISNPECCION
		$Sql ="SELECT * FROM cobranza.detprepagos WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nroprepago=:nroprepago";
	$result = $conexion->prepare($Sql);
	$result->execute(array(":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":nroinscripcion"=>$nroinscripcion,
						   ":nroprepago"=>$Pago));
		$itemsD = $result->fetchAll();
		$item=0;
		foreach($itemsD as $rowD)
		{
			$item++;
			$sqlD = "INSERT INTO catastro.detsolicitudinspeccion
			(  codemp,  codsuc,  nroinspeccion,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal)
			VALUES (  :codemp,  :codsuc,  :nroinspeccion,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";

			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array(":codemp"=>$codemp,
									":codsuc"=>$codsuc,
									":nroinspeccion"=>$nroinspeccion,
									":codconcepto"=>$rowD['codconcepto'],
									":item"=>$item,
									":subtotal"=>$rowD["importe"],
									":igv"=>0,
									":redondeo"=>0,
									":imptotal"=>$rowD["importe"],
									));
			if ($resultD->errorCode() != '00000')
		    {
		        $conexion->rollBack();
		        $mensaje = "Error reclamos";
		        $data['res']    =2;
                die(json_encode($data)) ;
		    }
		}
		/////////INSERTO DETALLE DE LA ISNPECCION


	$Sql ="UPDATE cobranza.cabprepagos SET nroinspeccion = :nroinspeccion
			WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nroprepago=:nroprepago";
	$result = $conexion->prepare($Sql);
	$result->execute(array(":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":nroinscripcion"=>$nroinscripcion,
						   ":nroprepago"=>$Pago,
						   ":nroinspeccion"=>$nroinspeccion));

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		$data['res']    =2;
    	die(json_encode($data));
	}
}
else
{
	if($Pago!=0)
	{
		/////////INSERTO DETALLE DE LA ISNPECCION
		$Sql ="SELECT SUM(d.importe) as importe,c.descripcion as concepto,d.codconcepto
		FROM cobranza.detpagos d
		INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc)
						AND (d.codconcepto = c.codconcepto)
		WHERE d.codemp = :codemp AND d.codsuc=:codsuc AND d.nroinscripcion=:nroinscripcion AND d.nropago=:nropago
		GROUP BY c.descripcion,d.codconcepto";
		$result = $conexion->prepare($Sql);
		$result->execute(array(":codemp"=>$codemp,
							   ":codsuc"=>$codsuc,
							   ":nroinscripcion"=>$nroinscripcion,
							   ":nropago"=>$Pago));
		$itemsD = $result->fetchAll();
		$item=0;
		foreach($itemsD as $rowD)
		{
			$item++;
			$addglosa.=$rowD['concepto']."\n";
			$sqlD = "INSERT INTO catastro.detsolicitudinspeccion
			(  codemp,  codsuc,  nroinspeccion,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal)
			VALUES (  :codemp,  :codsuc,  :nroinspeccion,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";

			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array(":codemp"=>$codemp,
									":codsuc"=>$codsuc,
									":nroinspeccion"=>$nroinspeccion,
									":codconcepto"=>$rowD['codconcepto'],
									":item"=>$item,
									":subtotal"=>$rowD["importe"],
									":igv"=>0,
									":redondeo"=>0,
									":imptotal"=>$rowD["importe"],
									));
			if ($resultD->errorCode() != '00000')
		    {
		        $conexion->rollBack();
		        $mensaje = "Error reclamos";
		        $data['res']    =2;
                die(json_encode($data)) ;
		    }
		}
		/////////INSERTO DETALLE DE LA ISNPECCION

		$Sql ="UPDATE cobranza.cabpagos SET nroinspeccion = :nroinspeccion
		WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nropago=:nropago";
		$result = $conexion->prepare($Sql);
		$result->execute(array(":codemp"=>$codemp,
							   ":codsuc"=>$codsuc,
							   ":nroinscripcion"=>$nroinscripcion,
							   ":nropago"=>$Pago,
							   ":nroinspeccion"=>$nroinspeccion));
		if($result->errorCode()!='00000')
		{
			$conexion->rollBack();
			$mensaje = "Error al Grabar Registro";
			$data['res']    =2;
	    	die(json_encode($data));
		}

	}
	else
	{
		if($nrocredito!=0)
		{
			/////////INSERTO DETALLE DE LA ISNPECCION
			$Sql ="SELECT d.* ,c.descripcion as concepto
			FROM facturacion.detcabcreditos d
			INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc)
						AND (d.codconcepto = c.codconcepto)
			WHERE d.codemp = :codemp AND d.codsuc=:codsuc AND d.nrocredito=:nrocredito";
			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":nrocredito"=>$nrocredito));
			$itemsD = $result->fetchAll();
			foreach($itemsD as $rowD)
			{
				$addglosa.=$rowD['concepto']."\n";
				$sqlD = "INSERT INTO catastro.detsolicitudinspeccion
				(  codemp,  codsuc,  nroinspeccion,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal)
				VALUES (  :codemp,  :codsuc,  :nroinspeccion,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";

				$resultD = $conexion->prepare($sqlD);
				$resultD->execute(array(":codemp"=>$codemp,
										":codsuc"=>$codsuc,
										":nroinspeccion"=>$nroinspeccion,
										":codconcepto"=>$rowD['codconcepto'],
										":item"=>$rowD["item"],
										":subtotal"=>$rowD["subtotal"],
										":igv"=>$rowD["igv"],
										":redondeo"=>$rowD["redondeo"],
										":imptotal"=>$rowD["imptotal"],
										));
				if ($resultD->errorCode() != '00000')
			    {
			        $conexion->rollBack();
			        $mensaje = "Error reclamos";
			        $data['res']    =2;
	                die(json_encode($data)) ;
			    }
			}
			/////////INSERTO DETALLE DE LA ISNPECCION
			$Sql ="UPDATE facturacion.cabcreditos
			SET nroinspeccion = :nroinspeccion
			WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nrocredito=:nrocredito";
			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":nroinscripcion"=>$nroinscripcion,
								   ":nrocredito"=>$nrocredito,
								   ":nroinspeccion"=>$nroinspeccion));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error al Grabar Registro";
				$data['res']    =2;
		    	die(json_encode($data));
			}
			if($Cnroprepago!=0)
			{
			    $Sql="update cobranza.cabprepagos set nroinspeccion=".$nroinspeccion."
			     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$Cnroprepago;
			    $result = $conexion->query($Sql);
			    if (!$result)
			    {
			        $conexion->rollBack();
			        $mensaje = "619 Error UPDATE cabprepagos";
			        $data['res']    =2;
			        $data['mensaje']    =$mensaje;
			        die(json_encode($data));
			    }
			}
			if($CnroprepagoInicial!=0)
			{
			    $Sql="update cobranza.cabprepagos set nroinspeccion=".$nroinspeccion."
			     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$CnroprepagoInicial;
			    $result = $conexion->query($Sql);
			    if (!$result)
			    {
			        $conexion->rollBack();
			        $mensaje = "619 Error UPDATE cabprepagos";
			        $data['res']    =2;
			        $data['mensaje']    =$mensaje;
			        die(json_encode($data));
			    }
			}

		}
		if($nrorefinanciamiento!=0)
		{
			/////////INSERTO DETALLE DE LA ISNPECCION
			$Sql ="SELECT d.*,c.descripcion as concepto
					FROM facturacion.detcabrefinanciamiento d
					INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc)
						AND (d.codconcepto = c.codconcepto)
			WHERE d.codemp = :codemp AND d.codsuc=:codsuc AND d.nrorefinanciamiento=:nrorefinanciamiento";
			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":nrorefinanciamiento"=>$nrorefinanciamiento));
			$itemsD = $result->fetchAll();

			foreach($itemsD as $rowD)
			{
				$addglosa.=$rowD['concepto']."\n";
				$sqlD = "INSERT INTO catastro.detsolicitudinspeccion
				(  codemp,  codsuc,  nroinspeccion,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal)
				VALUES (  :codemp,  :codsuc,  :nroinspeccion,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";

				$resultD = $conexion->prepare($sqlD);
				$resultD->execute(array(":codemp"=>$codemp,
										":codsuc"=>$codsuc,
										":nroinspeccion"=>$nroinspeccion,
										":codconcepto"=>$rowD['codconcepto'],
										":item"=>$rowD["item"],
										":subtotal"=>$rowD["subtotal"],
										":igv"=>$rowD["igv"],
										":redondeo"=>$rowD["redondeo"],
										":imptotal"=>$rowD["imptotal"],
										));
				if ($resultD->errorCode() != '00000')
			    {
			        $conexion->rollBack();
			        $mensaje = "Error reclamos";
			        $data['res']    =2;
	                die(json_encode($data)) ;
			    }
			}
			/////////INSERTO DETALLE DE LA ISNPECCION

			$Sql ="UPDATE facturacion.cabrefinanciamiento
			SET nroinspeccion = :nroinspeccion
			WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion
			AND nrorefinanciamiento=:nrorefinanciamiento";
			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":nroinscripcion"=>$nroinscripcion,
								   ":nrorefinanciamiento"=>$nrorefinanciamiento,
								   ":nroinspeccion"=>$nroinspeccion));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error al Grabar Registro";
				$data['res']    =2;
		    	die(json_encode($data));
			}
			if($Cnroprepago!=0)
			{
			    $Sql="update cobranza.cabprepagos set nroinspeccion=".$nroinspeccion."
			     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$Cnroprepago;
			    $result = $conexion->query($Sql);
			    if (!$result)
			    {
			        $conexion->rollBack();
			        $mensaje = "619 Error UPDATE cabprepagos";
			        $data['res']    =2;
			        $data['mensaje']    =$mensaje;
			        die(json_encode($data));
			    }
			}
			if($CnroprepagoInicial!=0)
			{
			    $Sql="update cobranza.cabprepagos set nroinspeccion=".$nroinspeccion."
			     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$CnroprepagoInicial;
			    $result = $conexion->query($Sql);
			    if (!$result)
			    {
			        $conexion->rollBack();
			        $mensaje = "619 Error UPDATE cabprepagos";
			        $data['res']    =2;
			        $data['mensaje']    =$mensaje;
			        die(json_encode($data));
			    }
			}

		}
		if($nrocargo!=0)
		{
			/////////INSERTO DETALLE DE LA ISNPECCION
			$Sql ="SELECT d.* ,c.descripcion as concepto
			FROM facturacion.detcabvarios d
			INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc)
						AND (d.codconcepto = c.codconcepto)
			WHERE d.codemp = :codemp AND d.codsuc=:codsuc AND d.nrocredito=:nrocredito";
			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":nrocredito"=>$nrocargo));
			$itemsD = $result->fetchAll();
			foreach($itemsD as $rowD)
			{
				$addglosa.=$rowD['concepto']."\n";
				$sqlD = "INSERT INTO catastro.detsolicitudinspeccion
				(  codemp,  codsuc,  nroinspeccion,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal)
				VALUES (  :codemp,  :codsuc,  :nroinspeccion,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";

				$resultD = $conexion->prepare($sqlD);
				$resultD->execute(array(":codemp"=>$codemp,
										":codsuc"=>$codsuc,
										":nroinspeccion"=>$nroinspeccion,
										":codconcepto"=>$rowD['codconcepto'],
										":item"=>$rowD["item"],
										":subtotal"=>$rowD["subtotal"],
										":igv"=>$rowD["igv"],
										":redondeo"=>$rowD["redondeo"],
										":imptotal"=>$rowD["imptotal"],
										));
				if ($resultD->errorCode() != '00000')
			    {
			        $conexion->rollBack();
			        $mensaje = "Error reclamos";
			        $data['res']    =2;
	                die(json_encode($data)) ;
			    }
			}
			/////////INSERTO DETALLE DE LA ISNPECCION
			$Sql ="UPDATE facturacion.cabvarios
			SET nroinspeccion = :nroinspeccion
			WHERE codemp = :codemp AND codsuc=:codsuc AND nroinscripcion=:nroinscripcion AND nrocredito=:nrocredito";
			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":nroinscripcion"=>$nroinscripcion,
								   ":nrocredito"=>$nrocargo,
								   ":nroinspeccion"=>$nroinspeccion));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error al Grabar Registro";
				$data['res']    =2;
		    	die(json_encode($data));
			}
			if($Cnroprepago!=0)
			{
			    $Sql="update cobranza.cabprepagos set nroinspeccion=".$nroinspeccion."
			     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$Cnroprepago;
			    $result = $conexion->query($Sql);
			    if (!$result)
			    {
			        $conexion->rollBack();
			        $mensaje = "619 Error UPDATE cabprepagos";
			        $data['res']    =2;
			        $data['mensaje']    =$mensaje;
			        die(json_encode($data));
			    }
			}
			if($CnroprepagoInicial!=0)
			{
			    $Sql="update cobranza.cabprepagos set nroinspeccion=".$nroinspeccion."
			     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$CnroprepagoInicial;
			    $result = $conexion->query($Sql);
			    if (!$result)
			    {
			        $conexion->rollBack();
			        $mensaje = "619 Error UPDATE cabprepagos";
			        $data['res']    =2;
			        $data['mensaje']    =$mensaje;
			        die(json_encode($data));
			    }
			}

		}
	}

	//ACTUALIZAR GLOSA DE LA SOLICITUD
			$Sql ="UPDATE catastro.solicitudinspeccion
			SET glosa = glosa || '".$addglosa."'
			WHERE codemp = :codemp AND codsuc=:codsuc AND nroinspeccion=:nroinspeccion";
			$result = $conexion->prepare($Sql);
			$result->execute(array(":codemp"=>$codemp,
								   ":codsuc"=>$codsuc,
								   ":nroinspeccion"=>$nroinspeccion));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error al Grabar Registro";
				$data['res']    =2;
		    	die(json_encode($data));
			}
			//ACTUALIZAR GLOSA DE LA SOLICITUD

}
 $conexion->commit();
 	$objFunciones->setCorrelativosVarios(21,$codsuc,"UPDATE",$nroinspeccion);
	$data['res']    =  1;
	$data['nroinspeccion']    =  $nroinspeccion;
	echo json_encode($data);
