<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../objetos/clsReporte.php");
include("../../../../../admin/clases/num2letra.php");
global $meses;
$objFunciones = new clsFunciones();

//$objMantenimiento 	= new clsDrop();
$codsuc = $_GET['codsuc'];
$nroinscripcion = $_GET['nroinscripcion'];
$codpago = $_GET['codpago'];
$Presicion = 2;

$Sql = "SELECT * FROM admin.empresas";
$Consulta = $conexion->query($Sql);
$row = $Consulta->fetch();
$RazonSocial = strtoupper($row["razonsocial"]);
$Empresa = strtoupper($row["razonsocial"]);
$Ruc = $row["ruc"];
$Direccion = $row["direccion"];
$Telefono = $row["telefono"];
$Email = $row["razonsocial"];
$FechaActual = date('d/m/Y');
$Hora = date('h:i:s a');
$NroAutorizacionSunat = "0093845057911";
$MensajeComprobante = "¡El Agua es Vida, No la Desperdicies!";
$SerieImpresora = "FFCF216492";

$Select = "SELECT coddocumento, serie, nrodocumento, nropago ";
$Select .= "FROM cobranza.detpagos ";
$Select .= "WHERE codemp = 1 AND codsuc=" . $codsuc . " AND nroinscripcion=" . $nroinscripcion . " AND nropago=" . $codpago . " ";
$Select .= "GROUP BY coddocumento, serie, nrodocumento, nropago ";
$Select .= "ORDER BY coddocumento, nrodocumento ";


$Consulta = $conexion->query($Select);
$row = $Consulta->fetch();

$DocSerie = $row['serie'];
//$DocNumero = $row['nrodocumento'];
$DocNumero = $row['nropago'];

$IdTipoDocumento = $row['coddocumento'];

$Select = "SELECT * FROM cobranza.cabpagos ";
$Select .= "WHERE codemp = 1 AND codsuc=" . $codsuc . " AND nroinscripcion=" . $nroinscripcion . " AND nropago=" . $codpago . " ";
$Consulta = $conexion->query($Select);
$row = $Consulta->fetch();

$NroInscrip = "0" . $codsuc . substr("000000", strlen($row['nroinscripcion'])) . $row['nroinscripcion'];

$IdCar = $row['car'];

$Estado = $row['estado'];
$EstadoD = $row['estadod'];
$Fecha = $objFunciones->DecFecha($fechareg['fechareg']);
$TipoCambio = $row['tipocambio'];
$Aigv = $row['igv'];
$Dscto = $row['porcentajedescuento'];
$IdProveedor = $row['nroinscripcion'];
$Glosa = $row['glosa'];

$IdMoneda = $row['idmoneda'];
$PorcentajeIgv = $row['porcentajeigv'];
$IdFormaPago = $row['codformapago'];

$Ticket_Serie = $row['ticket_serie'];
$Ticket_Numero = substr("000000", strlen($row['ticket_numero'])) . $row['ticket_numero'];

$sql = "SELECT s.car, ";
$sql .= " (SELECT descripcion FROM public.ubigeo WHERE codubigeo =" . $objFunciones->SubString('s.codubigeo', 1, 4) . " || '00') AS provincia, ";
$sql .= " (SELECT descripcion FROM public.ubigeo WHERE codubigeo = s.codubigeo) AS distrito ";
$sql .= "FROM cobranza.car AS s INNER JOIN public.ubigeo AS u ON u.codubigeo = s.codubigeo ";
$sql .= "WHERE s.car = " . $IdCar;

$Consul = $conexion->query($sql);
$rows = $Consul->fetch();

$prov = $rows['provincia'];
$dist = $rows['distrito'];

function Separar($texto) {
    /* $arr = str_split($texto);
      $texto='';
      $nulo='&nbsp;';
      $Cont = 1;
      for ($i=0; $i <=count($arr) ; $i++)
      {
      $texto .= $arr[$i].$nulo;
      } */
    //print_r($arr);
    //return mb_strtoupper(trim($texto));
    return (trim($texto));
}

$LetraTipo = '"Tahoma"';
$LetraTamanio = '10px';
?>
<style>
    body{
        margin-left:0px;
    }

    .Cabecera{
        font-family:<?=$LetraTipo ?>;
        font-size:<?=$LetraTamanio ?>;
        height:20px;
    }
    .InfoVenta{
        font-family:<?=$LetraTipo ?>;
        font-size:<?=$LetraTamanio ?>;
    }
    .Detalle{
        font-family:<?=$LetraTipo ?>;
        font-size:<?=$LetraTamanio ?>;
    }
    .Total{
        font-family:<?=$LetraTipo ?>;
        font-size:<?=$LetraTamanio ?>;
    }
</style>
<script language="javascript">
    function printThis() {
        //$('#PrintDD').jqprint();
        window.print();
        return false;
        //self.close();
    }
</script>
<body onLoad="printThis();">
    <table width="300" border="0" cellspacing="0" cellpadding="0" id="PrintDD">
        <tr>
            <td colspan="3" align="center" class="Cabecera" ><?=Separar($Empresa) ?></td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="Cabecera"><?=Separar("RUC " . $Ruc) ?></td>
        </tr>
        <tr>
            <td colspan="3" class="Cabecera">TICKET <?=$Ticket_Serie . "-" . $Ticket_Numero ?>&nbsp;&nbsp;<?=Separar($FechaActual) ?>&nbsp;&nbsp;<?=substr(Separar($Hora), 0, 8) ?></td>
        </tr>
        <tr>
            <td colspan="3" class="Cabecera"></td>
        </tr>
        <!--Datos del Propietario-->
        <tr>
            <td align="left" class="Cabecera" colspan="3"><?=$NroInscrip ?>&nbsp;&nbsp;<?=substr($row['propietario'], 0, 25) ?></td>
        </tr>
        <tr>
            <td colspan="3" align="center" class="Cabecera"></td>
        </tr>
        <!--Detalle de Pagos-->
        <tr>
            <td colspan="3" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <?php
                    $TotalPagado = 0;

                    $SqlD = "SELECT dp.serie, dp.nrodocumento, cf.fechareg, SUM(dp.importe) AS Importe ";
                    $SqlD .= "FROM cobranza.detpagos dp INNER JOIN facturacion.cabfacturacion cf ON (dp.codsuc = cf.codsuc) ";
                    $SqlD .= " AND (dp.nrofacturacion = cf.nrofacturacion) AND (dp.nroinscripcion = cf.nroinscripcion)";
                    $SqlD .= "WHERE dp.nroinscripcion = " . $IdProveedor . " AND dp.codsuc = " . $codsuc . " AND dp.nropago = " . $codpago . " ";
                    $SqlD .= " AND dp.coddocumento = 4 ";
                    $SqlD .= "GROUP BY dp.serie, dp.nrodocumento, cf.fechareg ORDER BY dp.nrodocumento";

                    $ConsultaD = $conexion->query($SqlD);

                    foreach ($ConsultaD->fetchAll() as $rowD) {
                        $TotalPagado += $rowD[3];
                        ?>
                        <tr>
                            <?php
                            $fecha_facturacion = explode('/', $objFunciones->DecFecha($rowD[2]));
                            ?>
                            <td align="left" class="Detalle"><?=$rowD[0] . "-" . $rowD[1] ?>&nbsp;&nbsp;<?=substr($meses_literal[$fecha_facturacion[1]], 0, 3) . "-" . $fecha_facturacion[2]; ?></td>
                            <td align="right" class="Detalle">S/. <?=number_format($rowD[3], 2) ?></td>
                        </tr>
                        <?php
                    }
                    //Otros Comprobantes
                    $SqlD2 = "SELECT dp.serie, dp.nrodocumento, SUM(dp.importe) AS Importe, cp.fechareg, d.descripcion ";
                    $SqlD2 .= "FROM cobranza.detpagos dp INNER JOIN cobranza.cabpagos cp ON (dp.codemp = cp.codemp) ";
                    $SqlD2 .= " AND (dp.codsuc = cp.codsuc) AND (dp.nroinscripcion = cp.nroinscripcion) AND (dp.nropago = cp.nropago) ";
                    $SqlD2 .= " INNER JOIN reglasnegocio.documentos d ON (dp.coddocumento = d.coddocumento) ";
                    $SqlD2 .= " AND (dp.codsuc = d.codsuc) ";
                    $SqlD2 .= "WHERE dp.nroinscripcion = " . $IdProveedor . " AND dp.codsuc = " . $codsuc . " AND dp.nropago = " . $codpago . " ";
                    $SqlD2 .= " AND dp.coddocumento <> 4 ";
                    $SqlD2 .= "GROUP BY dp.serie, dp.nrodocumento, cp.fechareg, d.descripcion";

                    $ConsultaD2 = $conexion->query($SqlD2);

                    foreach ($ConsultaD2->fetchAll() as $rowD2) {
                        $TotalPagado += $rowD2[2];
                        ?>
                        <tr>
                            <td align="left" class="Detalle"><?=$rowD2[0] . "-" . $rowD2[1] ?>&nbsp;&nbsp;<?=$objFunciones->DecFecha($rowD2[3]) ?>
                            <td align="right" class="Detalle">S/. <?=number_format($rowD2[2], 2) ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td align="left" class="Cabecera">TOTAL PAGO :</td>
                        <td align="right" class="Cabecera">S/. <?=number_format($TotalPagado, 2) ?></td>
                    </tr>
                    <?php
                    $SqlCaj = "SELECT c.descripcion ";
                    $SqlCaj .= "FROM cobranza.cajasxusuario cu INNER JOIN cobranza.cajas c ON (cu.nrocaja = c.nrocaja) ";
                    $SqlCaj .= "WHERE cu.codsuc = " . $codsuc . " AND cu.codusu = " . $_SESSION['id_user'];
                    $ConsultaCaj = $conexion->query($SqlCaj);
                    $RowCaj = $ConsultaCaj->fetch();
                    ?>
                    <tr>
                        <td colspan="3" align="center" class="InfoVenta"><?=Separar("Caja : " . $_SESSION['nombre']) ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="Detalle">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
