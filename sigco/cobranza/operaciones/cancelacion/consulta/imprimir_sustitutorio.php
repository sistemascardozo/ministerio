<?php 	
	include("../../../../../objetos/clsReporte.php");
	
	include("../../../../../admin/clases/num2letra.php");
	require_once  'Image/Barcode.php';
	
	class clsImpresionDuplicado extends clsReporte
	{
		function Header()
		{
			
		}
		function Contenido($nroinscripion,$codcatastro,$facturacion,$recibo,$femision,$propietario,$direccion,$servicio,$tfact,$tarifa,
						   $nromed,$fechalectanterior,$lectanterior,$consumo,$fechalectultima,$lectultima,$mensaje,$fvencimientodeudores,
						   $fcorte,$fvencimiento,$docidentidad,$nrodocidentidad,$mensajenormal,$fechapago,$codantiguo,$codpago)
		{
			global $tfacturacion,$conexion,$codsuc,$itemP,$meses;
			
			$sql="select DISTINCT(max(nropago))
				from cobranza.detpagos as d 
				inner join facturacion.conceptos as c on (d.codconcepto=c.codconcepto and d.codemp=c.codemp and d.codsuc=c.codsuc) 
				where d.nroinscripcion=? and d.codsuc=? /*and d.categoria=0 */
				group by d.nroinscripcion,d.codconcepto,c.descripcion,c.categoria";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($nroinscripion,$codsuc));
	$nropago = $consulta->fetchAll();

			$x=1;
			$y=1;
			
			$paramae = $this->getParamae("IMPIGV",$codsuc);
			
			//if($tfact == 0)
//			{
//				$img = $this->generargraficos($nroinscripion, $codsuc);
//				$this->Image($img, 14.0, 4.5, 4,3);
//			}
			
			//$this->EAN13(13,25.5,$codcatastro);			
			$grafico = new Graph();
			$grafico = Image_Barcode::draw($nroinscripion, 'Code128', 'jpg', false);
			
			imagejpeg($grafico, $codcatastro.'.jpg');
			
			if($grafico != false)
				if(file_exists($codcatastro.'.jpg'))
					if($codcatastro!='')
						$this->Image($codcatastro.'.jpg', 8, 24.5, 5, 1.5);
					else
					{
						echo '<script type="text/javascript">alert("Documento no Imprimible");close();</script>';
						die();
					}
					if($codcatastro=='')		
			{
				echo '<script type="text/javascript">alert("Documento no Imprimible");close();</script>';
						die();
			}


			$this->SetX($x);
			$this->SetFont('Arial','',11);
			
			$y=$this->GetY();
			$this->Rect($x+12,$y,6,2);
			
			$this->Line($x+12,$y+1,19,$y+1);
			
			$this->SetXY($x+12,$y);
			$this->Cell(3, 0.5,"Inscripcion",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->Cell(2.5, 0.5,$nroinscripion." - ".$codantiguo,0,1,'R');
			
			$this->SetX($x+12);
			
			$this->Cell(3, 0.5,"Codigo Catastro",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->Cell(2.5, 0.5,$codcatastro,0,1,'R');
			
			$this->SetX($x+12);
			
			$this->Cell(3, 0.5,"Periodo",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->Cell(2.5, 0.5,$facturacion,0,1,'R');
			
			$this->SetX($x+12);
			
			$this->Cell(3, 0.5,"Nro. Recibo",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->Cell(2.5, 0.5,$nropago[0][0],0,1,'R');
			
			$MesA =$meses[$itemP["mes"]-1];
			if($itemP["mes"]==1)
				$MesA="DICIEMBRE";

			$this->SetX($x+12);
			$this->SetFont('Arial','',10);
			$this->Cell(3, 0.5,"Consumo",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->SetFont('Arial','B',10);
			$this->Cell(2.5, 0.5,$MesA,0,1,'R');
			
			$this->SetFont('Arial','',10);
			//$this->Ln();
					
			$this->Cell(3, 0.5,"Nombre",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->SetFont('Arial','B',10);
			$this->Cell(5, 0.5,utf8_decode(strtoupper($propietario)),0,1,'L');
			$this->SetFont('Arial','',10);
			
			$this->Cell(3, 0.5,"Direccion",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->SetFont('Arial','B',10);
			$this->Cell(5, 0.5,$direccion,0,1,'L');
			
			$this->SetFont('Arial','',10);
			
			//$this->Cell(3, 0.5,$docidentidad,0,0,'L');
			$this->Cell(3, 0.5,'Doc. Identidad',0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->SetFont('Arial','B',10);
			$this->Cell(5, 0.5,intval($nrodocidentidad)==0?'':$nrodocidentidad,0,1,'L');
			/*$this->Cell(3, 0.5,"Servicios",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->Cell(5, 0.5,$servicio,0,0,'L');
			
			$this->Cell(3, 0.5,"Tipo Facturacion",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->Cell(5, 0.5,$tfacturacion[$tfact],0,1,'L');*/
			$this->SetFont('Arial','',10);
			$this->Cell(3, 0.5,"Unidad Uso",0,0,'L');
			$this->Cell(0.5, 0.5,":",0,0,'C');
			$this->SetFont('Arial','B',10);
			$this->Cell(5, 0.5,"1 - ".$tarifa,0,1,'L');
			$this->SetFont('Arial','',10);
			
			$y=$this->GetY();
			$this->Rect($x,$y+0.5,12,1.5);
			$this->Rect($x,$y+0.5,5,1.5);
			$this->Rect($x,$y+0.5,12,0.5);
			$this->Ln();
			$this->Cell(5, 0.5,"MEDIDOR",1,0,'C');
			$this->Cell(2, 0.5,"Periodo",0,0,'C');
			$this->Cell(5, 0.5,"LECTURAS",0,1,'C');
			
			$this->Cell(1, 0.5,"",0,0,'L');
			$this->Cell(0.5, 0.5,"",0,0,'C');
			$this->Cell(3.5, 0.5,'',0,0,'C');
			
			$this->Cell(2, 0.5,"",0,0,'C');
			
			$this->Cell(1.5, 0.5,"",0,0,'C');
			$this->Cell(3.5, 0.5,"",0,1,'C');
			
			if($tipofacturacion==0){$t="L";}
			if($tipofacturacion==1){$t="P";}
			if($tipofacturacion==2){$t="A";}
			
			$this->Cell(1, 0.5,"",0,0,'L');
			$this->Cell(0.5, 0.5,"",0,0,'C');
			//$this->Cell(1.5, 0.5,$t,0,0,'C');
			$this->Cell(2.5, 0.5,"",0,0,'R');
			$this->Cell(1, 0.5,'',0,0,'R');
			$this->Cell(2, 0.5,"",0,0,'C');
			
			$this->Cell(1.5, 0.5,"",0,0,'C');
			$this->Cell(3.5, 0.5,"",0,1,'C');
			
			/*$this->Cell(1, 0.5,"",0,0,'L');
			$this->Cell(0.5, 0.5,"",0,0,'C');
			$this->Cell(3.5, 0.5,"",0,0,'C');
			
			$this->Cell(2, 0.5,"",0,0,'C');
			
			$this->Cell(1.5, 0.5,$tipofacturacion==0?"Dif.":"",0,0,'C');
			$this->Cell(3.5, 0.5,$tipofacturacion==0?$consumo:"",0,1,'C');
			*/
			$this->SetFont('Arial','B',10);
			$this->Ln();
			
			$y=$this->GetY();
			$this->Rect($x,$y+0.5,18,9);

			$subtotMes	= 0;
			$interesMes = 0;
			$redondeo	= 0;
			$igvMes		= 0;
			$totrecibo	= 0;
			
			$this->Cell(15, 0.5,"DESCRIPCION DE CONCEPTOS",0,0,'L');
			$this->Cell(3, 0.5,"IMPORTE",0,1,'C');
			
			$this->SetFont('Arial','',10);
			
		/*$sqlD = "select d.nroinscripcion,d.codconcepto,c.descripcion,'',sum(d.importe) as monto,sum(d.importe) as monto2,
					 c.categoria
					 from cobranza.detpagos as d
					 inner join facturacion.conceptos as c on (d.codconcepto=c.codconcepto and d.codemp=c.codemp and d.codsuc=c.codsuc)
					 where  d.nroinscripcion=? and d.codsuc=? 
					 group by d.nroinscripcion,d.codconcepto,c.descripcion,c.categoria
					 order by d.codconcepto; ";
			
			$consultaD = $conexion->prepare($sqlD);
			$consultaD->execute(array($nroinscripion,$codsuc));*/
			$Sql="SELECT df.nroinscripcion,df.codconcepto,c.descripcion,df.nrocredito,sum(dp.importe) as monto,c.categoria
					FROM
					  facturacion.detfacturacion df
					  INNER JOIN facturacion.conceptos c ON (df.codemp = c.codemp)
					  AND (df.codsuc = c.codsuc)
					  AND (df.codconcepto = c.codconcepto)
					  INNER JOIN cobranza.detpagos dp ON (df.codemp = dp.codemp)
					  AND (df.codsuc = dp.codsuc)
					  AND (df.nrofacturacion = dp.nrofacturacion)
					  AND (df.nroinscripcion = dp.nroinscripcion)
					   AND (df.codconcepto = dp.codconcepto)
					where df.nroinscripcion=".$nroinscripion." and df.codsuc=".$codsuc."
							AND dp.nropago=".$codpago."
					  group by df.nroinscripcion,df.codconcepto,c.descripcion,df.nrocredito,c.categoria 
					order by df.codconcepto";
			$consultaD=$conexion->query($Sql);
			$itemsD = $consultaD->fetchAll();

			foreach($itemsD as $rowD)
			{
				$MontoTem=$rowD["monto"];
				//if(floatval($MontoTem)==0)
				//	$MontoTem=$rowD["monto2"];
				if($rowD["categoria"]!=3 && $rowD["categoria"]!=4 && $rowD["categoria"]!=7)
				{
					$this->Cell(15, 0.5,strtoupper($rowD["descripcion"]).$cuota,0,0,'L');
					$this->Cell(2.5, 0.5,number_format($MontoTem,2),0,1,'R');
					
					$subtotMes += $MontoTem;
				}
				if($rowD["categoria"]==3){$interesMes += $MontoTem;}
				if($rowD["categoria"]==7){$redondeo	  += $MontoTem;}
				if($rowD["categoria"]==4){$igvMes	  += $MontoTem;}
				
			}
			
			if($interesMes>0)
			{
				$this->Cell(15, 0.5,"INTERESES Y MORA",0,0,'L');
				$this->Cell(2.5, 0.5,number_format($interesMes,2),0,1,'R');
			}
			
			$subtotMes = $subtotMes + $interesMes;
			
			$this->SetFont('Arial','',10);
			$this->SetY(14.5);
		
			$this->Cell(15, 0.5,"SUBTOTAL",0,0,'R');
			$this->Cell(2.5, 0.5,number_format($subtotMes,2),0,1,'R');
			
			$this->Cell(15, 0.5,"Igv ".$paramae["valor"]."%",0,0,'R');
			$this->Cell(2.5, 0.5,number_format($igvMes,2),0,1,'R');
			
			$this->Cell(15, 0.5,"Redondeo",0,0,'R');
			$this->Cell(2.5, 0.5,number_format($redondeo,2),0,1,'R');
			
			$meses = $this->calcular_deuda_ant($nroinscripion,$codsuc);
			if($meses["deuda"]!=0)
			{
				//$this->Cell(15,0.5,$meses["meses"],0,0,'R');
				//$this->Cell(2.5,0.5,number_format($meses["deuda"],2),0,0,'R');
			}
			
			$this->Ln(1);
			$this->SetFont('Arial','B',14);
			
			$totrecibo = /*$meses["deuda"] +*/ $igvmes + ($redondeo) + $subtotMes;
			$this->Cell(15, 0.5,"TOTAL A PAGAR S/.",0,0,'R');
			$this->Cell(2.5, 0.5,number_format($totrecibo,2),0,1,'R');
			
			$this->SetFont('Arial','',12);
			$this->Ln(0.2);
			$y=$this->GetY();
			$this->SetXY(5,13);
			$this->SetFont('Arial','B',17);
			$this->Cell(12, 0.5,"SUSTITUTORIO",0,1,'L');
			$this->SetXY(7,15);
			$this->Cell(12, 0.5,"CANCELADO",0,1,'L');
			$this->SetXY(7,16);
			$this->SetFont('Arial','',10);
			$this->Cell(12, 0.5,"Fecha Cancelacion : ".$fechapago,0,0,'L');

			$this->SetY($y);
			$this->SetFont('Arial','B',14);
			$this->Cell(12, 0.5,"SON : ".CantidadEnLetra($totrecibo)." NUEVOS SOLES",0,0,'L');
			$this->Cell(3, 0.5,"",0,1,'C');
			$this->SetFont('Arial','B',11);
			$this->Ln(0.2);
			//$this->MultiCell(18,0.7,utf8_decode(strtoupper($mensaje)),1,'L');
			$this->MultiCell(18,0.5,utf8_decode(strtoupper($mensajenormal)),1,'C');		
			$this->SetFont('Arial','',12);
			if($meses["deuda"]!=0)
			{
				//$this->Image("../../../../../images/logo_corte.jpg",1.5,14,3,2);
				//$this->Image("../../../../../images/logo_triste.jpg",4.8,14.4,2,2);
				//$fechavencimiento 	= "FECHA DE VENCIMIENTO : ".$fvencimientodeudores;
				//$fechacorte			= "FECHA DE CORTE : ".$fcorte;
				$fv 				= $fechapago;
			}else{
				//$this->Image("../../../../../images/logo_feliz.jpg",4.8,14.4,2,2);
				//$fechavencimiento 	= "FECHA DE VENCIMIENTO : ".$fvencimiento;
				$fechacorte			= "";			
				$fv 				= $fechapago;
			}
			
			
			
			
			$this->Cell(10, 0.5,'FECHA DE EMISION : '.$fechapago,0,0,'L');
			$this->Cell(8, 0.5,$fechavencimiento,0,1,'R');

			$this->Ln(0.1);
			$this->SetFont('Arial','B',16);
			$this->Ln(0.1);
			$this->Cell(10, 0.5,'',0,0,'L');
			$this->Cell(8, 0.5,$fechacorte,0,0,'R');
			$this->Cell(3, 0.5,"",0,0,'C');
			$this->Cell(3, 0.5,"",0,1,'C');
			
			$this->Rect($x,23,18,1);
			
			$this->SetFont('Arial','',9);
			$this->SetY(22.5);
			
			$this->Cell(6, 0.5,"",0,0,'C');
			$this->Cell(4, 0.5,"",0,0,'C');
			$this->Cell(8, 0.5,$codcatastro."  ".$nroinscripion." - ".$codantiguo."  ".utf8_decode(strtoupper($propietario)),0,1,'R');
			
			$this->SetFont('Arial','',10);
			
			
			$this->Cell(6, 0.5,"FECHA DE EMISION",'R',0,'C');
			$this->Cell(6, 0.5,"",'R',0,'C');
			$this->Cell(3, 0.5,$facturacion,'B',0,'L');
			$this->Cell(3, 0.5,$nropago[0][0],'B',1,'R');
			
			$this->Cell(6, 0.5,$fechapago,'R',0,'C');
			$this->Cell(6, 0.5,'','R',0,'C');
			$this->Cell(3, 0.5,"TOTAL A PAGAR S/.",'B',0,'L');
			$this->Cell(3, 0.5,number_format($totrecibo,2),'B',1,'R');
			$this->Cell(18, 0.5,"SON : ".CantidadEnLetra($totrecibo)." NUEVOS SOLES",0,1,'L');
			$this->SetFont('Arial','B',10);
			$this->SetY(26.1);
			$this->MultiCell(18,0.3,utf8_decode(strtoupper($mensaje)),0,'C');
		}
		function Footer()
		{		
			 
		}
	 }
	
	$nroinscripcion		= $_GET["nroinscripcion"];
	$codsuc				= $_GET["codsuc"];
	$codciclo			= $_GET["codciclo"];
	$fechapago			= $_GET["fechapago"];
	//$nrofacturacion		= $_SESSION["nrofacturacion"] - 1;
	$codpago			= $_GET["codpago"];
	$Sql ="SELECT max(nrofacturacion) 
			FROM cobranza.detpagos 
			WHERE codemp=1 AND codsuc=".$codsuc."
			AND nroinscripcion=".$nroinscripcion."
			AND nropago=".$codpago;
	$Consulta = $conexion->query($Sql);
	$row = $Consulta->fetch();
	$nrofacturacion=$row[0];
	$sqlP = "select nrofacturacion,anio,mes,fechaemision,mensajegeneral,fechavencnormal,fechavencdeudores,fechacorte,mensajenormal
			from facturacion.periodofacturacion where codsuc=? and codciclo=? and nrofacturacion=?";
	
	$consultaP = $conexion->prepare($sqlP);
	$consultaP->execute(array($codsuc,$codciclo,$nrofacturacion));
	$itemP = $consultaP->fetch();	

	$objReporte	=	new clsImpresionDuplicado('L','cm',array(28.00,20.00));
	
	$sql = "select * from facturacion.view_cabfacturacion where nroinscripcion=? and nrofacturacion=? and codsuc=?
			ORDER BY codrutdistribucion, loteantiguo";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($nroinscripcion,$itemP["nrofacturacion"],$codsuc));
	$items = $consulta->fetch();

	$objReporte->AddPage();
	$objReporte->SetAutoPageBreak(true, 1);
	$facturacion = $meses[$itemP["mes"]]."-".$itemP["anio"];
	
	$objReporte->Contenido($nroinscripcion,$items["codcatastro"],$facturacion,$items["nrodocumento"],$objReporte->DecFecha($itemP["fechaemision"]),
						   $items["propietario"],$items["direccion"],$items["descripcion"],$items["tipofacturacion"],$items["nomtar"],
						   $items["nromed"],$objReporte->DecFecha($items["fechalectanterior"]),$items["lecturaanterior"],$items["consumo"],
						   $objReporte->DecFecha($items["fechalectultima"]),$items["lecturaultima"],$itemP["mensajegeneral"],
						   $objReporte->DecFecha($itemP["fechavencdeudores"]),$objReporte->DecFecha($itemP["fechacorte"]),
						   $objReporte->DecFecha($itemP["fechavencnormal"]), $items["docidentidad"], $items["nrodocidentidad"],$itemP['mensajenormal'],$objReporte->DecFecha($fechapago),$items['codantiguo'],$codpago);
	
	$objReporte->Output();	
	
	array_map('unlink', glob("*.jpg"));
?>