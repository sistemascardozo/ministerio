<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../../objetos/clsReporte.php");

    include("../../../../../admin/clases/num2letra.php");
    //require_once 'Image/Barcode.php';

    class clsImpresionDuplicado extends clsReporte {

        function Header() {

        }

        function Contenido($nroinscripion, $codcatastro, $facturacion, $recibo, $femision, $propietario, $direccion, $servicio, $tfact, $tarifa, $nromed, $fechalectanterior, $lectanterior, $consumo, $fechalectultima, $lectultima, $mensaje, $fvencimientodeudores, $fcorte, $fvencimiento, $docidentidad, $nrodocidentidad, $mensajenormal, $fechapago, $codpago, $nrosolicitud, $recibon, $recibonombre, $codantiguo, $glosa, $DocCliente, $PeriodoC) {
            global $tfacturacion, $conexion, $codsuc, $itemP, $meses;

            $x = 1;
            $y = 1;

            $paramae = $this->getParamae("IMPIGV", $codsuc);

            $this->SetX($x);
            $this->SetFont('Arial', 'B', 11);
            //$this->Rect($x + 5, $y, 8, 2);
//            $this->SetXY($x + 5, $y);
//            $this->Cell(8, 2, utf8_decode(strtoupper($recibonombre)), 1, 0, 'C');
            $y = $this->GetY() + 2;
            
            $this->Rect($x + 5, $y, 8, 2);
            $this->SetXY($x + 5, $y);
            $this->Cell(3, 0.5, utf8_decode("Código"), 0, 0, 'L');

            $this->Cell(5, 0.5, $codcatastro, 0, 1, 'R');
            $this->SetX($x + 5);

            $this->Cell(3, 0.5, utf8_decode("Inscripción"), 0, 0, 'L');
            $this->Cell(5, 0.5, $codantiguo, 0, 1, 'R');

            $this->SetFont('Arial', '', 10);
            $this->SetX($x + 5);
            $this->Cell(4, 0.5, "PERIODO DE EMISION", 1, 0, 'L');
            $this->Cell(4, 0.5, "NUMERO DE RECIBO", 1, 1, 'R');

            $this->SetFont('Arial', 'B', 11);
            $this->SetX($x + 5);
            $this->Cell(4, 0.5, $facturacion, 1, 0, 'R');
            $this->Cell(4, 0.5, $recibo, 0, 1, 'R');

            $this->Ln(1);
            $this->SetFont('Arial', '', 10);

            $this->Cell(2.5, 0.5, "Nombre", 0, 0, 'L');
            $this->Cell(0.5, 0.5, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(5, 0.5, utf8_decode(strtoupper($propietario)), 0, 1, 'L');
            $this->SetFont('Arial', '', 10);

            $this->Cell(2.5, 0.5, "Direccion", 0, 0, 'L');
            $this->Cell(0.5, 0.5, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(5, 0.5, $direccion, 0, 1, 'L');
             $this->SetFont('Arial', '', 10);
            if( strlen(trim($DocCliente))==11) $DocCliente ="RUC: ".$DocCliente;
            if( strlen(trim($DocCliente))==8) $DocCliente ="DNI: ".$DocCliente;
            $this->Cell(3, 0.5, $DocCliente, 0, 1, 'L');
			
			$this->Cell(2.5, 0.5, "Periodo Med.", 0, 0, 'L');
            $this->Cell(0.5, 0.5, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(5, 0.5, $PeriodoC, 0, 1, 'L');
			
            $this->SetFont('Arial', 'B', 10);
            $this->Ln(0.3);

            $y = $this->GetY();
            $this->Rect($x, $y + 0.5, 13, 5);

            $subtotMes = 0;
            $interesMes = 0;
            $redondeo = 0;
            $igvMes = 0;
            $totrecibo = 0;

            $this->Cell(6, 0.5, "DESCRIPCION DE CONCEPTOS", 0, 0, 'L');
            $this->Cell(4, 0.5, "NO IMPONIBLE", 0, 0, 'C');
            $this->Cell(3, 0.5, "IMPONIBLE", 0, 1, 'C');

            $this->SetFont('Arial', '', 10);
            //OBTENER FACTURACION DE ESE PAGO
            $Sql = "SELECT d.nroinscripcion,d.codconcepto,c.descripcion,sum(d.importe) as monto,c.categoria
                FROM cobranza.detprepagos d
                inner join facturacion.conceptos as c on (d.codconcepto=c.codconcepto and d.codemp=c.codemp and d.codsuc=c.codsuc)
                WHERE  d.codemp=1 AND d.codsuc=".$codsuc." and d.nroinscripcion=".$nroinscripion."
                and  d.nroprepago=".$codpago."
                group by d.nroinscripcion,d.codconcepto,c.descripcion,c.categoria
                order by d.codconcepto; ";
            $Consulta = $conexion->query($Sql);
            $itemsD = $Consulta->fetchAll();
            foreach ($itemsD as $rowD) {
                $MontoTem = $rowD["monto"];
                if ($rowD["categoria"] != 3 && $rowD["categoria"] != 4 && $rowD["categoria"] != 7) {
                    $this->Cell(6, 0.5, $rowD["codconcepto"]." ".strtoupper($rowD["descripcion"]), 0, 0, 'L');
                    $this->Cell(4, 0.5, '', 0, 0, 'R');
                    $this->Cell(3, 0.5, number_format($MontoTem, 2), 0, 1, 'R');

                    $subtotMes += $MontoTem;
                }
                if ($rowD["categoria"] == 3) {
                    $interesMes += $MontoTem;
                }
                if ($rowD["categoria"] == 7) {
                    $redondeo += $MontoTem;
                }
                if ($rowD["categoria"] == 4) {
                    $igvMes += $MontoTem;
                }
            }

            if ($interesMes > 0) {
                $this->Cell(6, 0.5, "INTERESES Y MORA", 0, 0, 'L');
                $this->Cell(3, 0.5, number_format($interesMes, 2), 0, 1, 'R');
            }

            $subtotMes = $subtotMes + $interesMes;

            $this->SetFont('Arial', '', 7);
            $this->SetY(11.5);
            if ($glosa != "")
                $glosa = "=>".$glosa;
			
            $this->Cell(6, 0.5, $glosa, 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Cell(4, 0.5, "SUBTOTAL", 0, 0, 'R');
            $this->Cell(3, 0.5, number_format($subtotMes, 2), 0, 1, 'R');

            $this->Cell(10, 0.5, "Igv ".$paramae["valor"]."%", 0, 0, 'R');
            $this->Cell(3, 0.5, number_format($igvMes, 2), 0, 1, 'R');

            $this->Cell(10, 0.5,"Redondeo",0,0,'R');
            $this->Cell(3, 0.5,number_format($redondeo,2),0,1,'R');

            $this->SetFont('Arial', 'B', 14);

            $totrecibo = $meses["deuda"] + $igvMes + ($redondeo) + $subtotMes;
            $this->Cell(10, 0.5, "TOTAL A PAGAR S/.", 0, 0, 'R');
            $this->Cell(3, 0.5, number_format($totrecibo, 2), 0, 1, 'R');
            $this->SetFont('Arial', 'B', 10);
            $this->Ln(0.2);
            $this->Cell(12, 0.5, "SON : ".CantidadEnLetra($totrecibo)." NUEVOS SOLES", 0, 1, 'L');
            
            $this->Rect($x, 13.7, 13, 0.8);
            $this->SetFont('Arial', '', 10);
            $this->Cell(8, 0.7, "Solicitud de Servicio: ".str_pad($nrosolicitud, 8, "0", STR_PAD_LEFT), 0, 1, 'R');
            
            $this->Rect($x, 14.5, 13, 0.8);
            $this->SetY(14.7);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(3.7, 0.7, 'FECHA DE EMISION', 0, 0, 'L');
            $this->SetFont('Arial', '', 12);
            $this->Cell(3, 0.7, $femision, 0, 0, 'L');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(4, 0.7, 'ULTIMO DIA DE PAGO', 0, 0, 'L');
            $this->SetFont('Arial', '', 12);
            $this->Cell(3, 0.7, $fvencimientodeudores, 0, 0, 'L');
            $this->SetFont('Arial', 'B', 16);
            $this->Cell(10, 0.7, '', 0, 0, 'L');
            $this->Cell(8, 0.7, $fechacorte, 0, 0, 'R');
            $this->Cell(3, 0.7, "", 0, 0, 'C');
            $this->Cell(3, 0.7, "", 0, 1, 'C');


            $this->SetFont('Arial', '', 9);
            $this->SetY(16.6);
            $this->Cell(10.5, 0.5, $codcatastro, 0, 0, 'R');
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(3, 0.5, $codantiguo, 0, 1, 'C');
            $this->Cell(0, 0.5, utf8_decode(strtoupper($propietario)), 0, 1, 'R');
            
            $this->Rect($x, 17.6, 13, 1);
            $this->SetFont('Arial', '', 9);
            $this->Cell(3.5, 0.5, "FECHA DE EMISION", 'R', 0, 'C');
            $this->Cell(3.5, 0.5, "ULTIMO DIA DE PAGO", 'R', 0, 'C');
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(3, 0.5, $facturacion, 'B', 0, 'L');
            $this->Cell(3, 0.5, $recibo, 'B', 1, 'R');

            $this->Cell(3.5, 0.5, $femision, 'R', 0, 'C');
            $this->Cell(3.5, 0.5, $fvencimientodeudores, 'R', 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell(3, 0.5, "TOTAL A PAGAR S/.", 'B', 0, 'L');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(3, 0.5, number_format($totrecibo, 2), 'B', 1, 'R');
            $this->Cell(18, 0.5, "SON : ".CantidadEnLetra($totrecibo)." NUEVOS SOLES", 0, 1, 'L');
            $this->SetFont('Arial', 'B', 10);
            $this->SetY(19.5);
            $this->SetFont('Arial', 'B', 20);
            $this->Cell(0, 0.5, "*".$recibon."*", 0, 1, 'C');
        }
        
        function Footer() {

        }

    }

    $nroinscripcion = $_GET["nroinscripcion"];
    $codsuc = $_GET["codsuc"];
    $codemp = 1;

    $nrofacturacion = $_SESSION["nrofacturacion"] -1;
    $codpago = $_GET["codpago"];
	
    //$codciclo=1;
    $Sql = "SELECT car, codformapago, fechareg, subtotal, hora, estado, codciclo, condpago, igv,
        redondeo, imptotal, nrocredito, eventual, propietario, direccion, documento, glosa, nroinspeccion, documento
        FROM cobranza.cabprepagos 
        WHERE codemp=? AND codsuc=? AND nroinscripcion=? AND nroprepago=?";
	
    $result = $conexion->prepare($Sql);
    $result->execute(array($codemp, $codsuc, $nroinscripcion, $codpago));
    $row = $result->fetch();

    $codciclo = $row["codciclo"];
    $propietario = $row["propietario"];
    $direccion = $row["direccion"];
    $fechapago = $row["fechareg"];
    $nroinspeccion = $row["nroinspeccion"];
    $fechaemision = $row["fechareg"];
    $glosa = $row["glosa"];
    $DocCliente=$row["documento"];
	
    //$sqlP = "SELECT anio,mes, fechavencdeudores
//        FROM facturacion.periodofacturacion
//        WHERE codsuc=? and codciclo=? and nrofacturacion=?";
//
//    $consultaP = $conexion->prepare($sqlP);
//    $consultaP->execute(array($codsuc, $codciclo, $nrofacturacion));
//    $itemP = $consultaP->fetch();
//	
//    $facturacion = $meses[$itemP["mes"]]."-".$itemP["anio"];
	$Mes = intval(substr($fechapago, 5, 2));
	$Anio = intval(substr($fechapago, 0, 4));
		
	$facturacion = $meses[$Mes]."-".$Anio;
		
    $objReporte = new clsImpresionDuplicado($orientation = 'P', $unit = 'cm', $format = 'A5');
    //con el numero de inspecion verificare si es un usuario nuevo
    if ($nroinspeccion != 0 && $nroinspeccion != '') {
        $Sql = "SELECT   nroinscripcion,  fechaemision,  estado,  glosa,  fechareg,  creador,  codinspector,
                fechatencion,  resuelve,  codconcepto,  diasatencion,  horareg,  codzona,  codsector,  codmanzanas,  lote,
                sublote,  codcliente,  propietario,  nrocalle,  codcalle,  nuevocliente
            FROM   catastro.solicitudinspeccion 
            WHERE codemp=? AND codsuc=? AND nroinspeccion=?";
        $result = $conexion->prepare($Sql);
        $result->execute(array($codemp, $codsuc, $nroinscripcion));
        $row = $result->fetch();
        $nuevocliente = $row['nuevocliente'];
        if ($nuevocliente == 0) {
            $Sql = "SELECT sol.nroinspeccion,upper(clie.propietario),upper(tipdoc.abreviado),clie.nrodocumento,
                upper(tipcal.descripcioncorta || ' ' || cal.descripcion ),sol.glosa,
                upper(insp.nombres),upper(tdoc.abreviado),insp.nrodocumento,sol.resuelve,sol.horareg,sol.fechaemision,
                sol.diasatencion,co.descripcion as concepto,sol.codconcepto,clie.nrocalle,zo.descripcion as zona,
                ".$objReporte->getCodCatastral("clie.").",clie.nroinscripcion,clie.nromed,clie.ruc, clie.codantiguo
                FROM catastro.solicitudinspeccion as sol 
                inner join catastro.clientes as clie on(sol.codemp=clie.codemp and sol.codsuc=clie.codsuc and 
                sol.nroinscripcion=clie.nroinscripcion)
                inner join public.tipodocumento as tipdoc on(clie.codtipodocumento=tipdoc.codtipodocumento)
                inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
                inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
                inner join reglasnegocio.inspectores as insp on(sol.codinspector=insp.codinspector) AND (sol.codsuc=insp.codsuc)
                inner join public.tipodocumento as tdoc on(insp.codtipodocumento=tdoc.codtipodocumento)
                inner join facturacion.conceptos as co on(sol.codsuc=co.codsuc AND sol.codconcepto=co.codconcepto)
                inner join admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)
                WHERE sol.nroinspeccion=? AND sol.codsuc=?";
            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array($nroinspeccion, $codsuc));
            $items = $consulta->fetch();
        } else {
            $Sql = "SELECT sol.nroinspeccion,upper(sol.propietario),'','',
                upper(tipcal.descripcioncorta || ' ' || cal.descripcion ),sol.glosa,
                upper(insp.nombres),upper(tdoc.abreviado),insp.nrodocumento,sol.resuelve,sol.horareg,sol.fechaemision,
                sol.diasatencion,co.descripcion as concepto,sol.codconcepto,sol.nrocalle,zo.descripcion as zona,
                ".$objReporte->getCodCatastral("sol.").",sol.nroinscripcion,'',''
                FROM catastro.solicitudinspeccion as sol 
                inner join public.calles as cal on(sol.codemp=cal.codemp and sol.codsuc=cal.codsuc and sol.codcalle=cal.codcalle and sol.codzona=cal.codzona )
                inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
                inner join reglasnegocio.inspectores as insp on(sol.codinspector=insp.codinspector) AND (sol.codsuc=insp.codsuc)
                inner join public.tipodocumento as tdoc on(insp.codtipodocumento=tdoc.codtipodocumento)
                inner join facturacion.conceptos as co on(sol.codsuc=co.codsuc AND sol.codconcepto=co.codconcepto)
                inner join admin.zonas as zo on(sol.codemp=zo.codemp AND sol.codsuc=zo.codsuc AND sol.codzona=zo.codzona)
                WHERE sol.nroinspeccion=? AND sol.codsuc=?";
            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array($nroinspeccion, $codsuc));
            $items = $consulta->fetch();
        }
    } else {
        //DIRECTO SIN SERVICIO
        $Sql = "SELECT upper(clie.propietario),upper(tipdoc.abreviado),clie.nrodocumento,
            upper(tipcal.descripcioncorta || ' ' || cal.descripcion ),
            clie.nrocalle,zo.descripcion as zona,
            ".$objReporte->getCodCatastral("clie.").", clie.nroinscripcion, clie.nromed, clie.ruc, clie.codantiguo
            FROM catastro.clientes as clie 
            inner join public.tipodocumento as tipdoc on(clie.codtipodocumento=tipdoc.codtipodocumento)
            inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
            inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
            inner join admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)
            WHERE clie.codemp=? AND clie.codsuc=? AND clie.nroinscripcion=?";
        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array($codemp, $codsuc, $nroinscripcion));
        $items = $consulta->fetch();
    }


    $Sql = "SELECT d.abreviado, p.serie, p.nrodocumento, d.descripcion, p.nrofacturacion
        FROM reglasnegocio.documentos d
        INNER JOIN cobranza.detprepagos p ON (d.coddocumento = p.coddocumento) AND (d.codsuc = p.codsuc)
        WHERE p.codemp=? AND  p.codsuc=? AND p.nroinscripcion=? AND p.nroprepago=?";
    //	print_r(array($codemp,$codsuc,$nroinscripcion,$codpago));
    $result = $conexion->prepare($Sql);
    $result->execute(array($codemp, $codsuc, $nroinscripcion, $codpago));
    $row = $result->fetch();
    $recibo = $row[0]." ".$row[1]."-".str_pad($row[2], 8, "0", STR_PAD_LEFT);
    $recibon = $row[1]."-".str_pad($row[2], 8, "0", STR_PAD_LEFT);
    $recibonombre = $row['descripcion'];

    $codantiguo = $items['codantiguo'];	//$objReporte->CodUsuario($codsuc, $nroinscripcion);
	
	//Periodo Consumo
	$sqlP = "SELECT anio, mes, fechavencdeudores
        FROM facturacion.periodofacturacion
        WHERE codsuc=? and codciclo=? and nrofacturacion=?";

    $consultaP = $conexion->prepare($sqlP);
    $consultaP->execute(array($codsuc, $codciclo, $row[4]));
    $itemP = $consultaP->fetch();

    $PeriodoC = $meses[$itemP["mes"]]."-".$itemP["anio"];
	
    $objReporte->AddPage();
    $objReporte->SetAutoPageBreak(true, 1);

    $objReporte->Contenido($nroinscripcion, $items["codcatastro"], $facturacion, $recibo, 
        $objReporte->DecFecha($fechaemision), $propietario, $direccion, $items["descripcion"], 
        $items["tipofacturacion"], $items["nomtar"], $items["nromed"], 
        $objReporte->DecFecha($items["fechalectanterior"]), $items["lecturaanterior"], 
        $items["consumo"], $objReporte->DecFecha($items["fechalectultima"]), 
        $items["lecturaultima"], $itemP["mensajegeneral"], 
        $objReporte->DecFecha($fechaemision), 
        $objReporte->DecFecha($itemP["fechacorte"]), 
        $objReporte->DecFecha($itemP["fechavencnormal"]), $items["docidentidad"], 
        $items["nrodocidentidad"], $itemP['mensajenormal'], $objReporte->DecFecha($fechapago), 
        $codpago, $nroinspeccion, $recibon, $recibonombre, $codantiguo, $glosa,$DocCliente, $PeriodoC);

    $objReporte->Output();

?>