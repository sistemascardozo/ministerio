<?php
include("../../../../../objetos/clsReporte.php");
include("../../../../../admin/clases/num2letra.php");
include("../../../../../vma/include/funciones.php");
//require_once  'Image/Barcode.php';
require_once '../../../../../include/Image/Barcode.php';

class clsImpresionDuplicado extends clsReporte {

    function Header() {

    }

    function Contenido($nroinscripion, $codcatastro, $facturacion, $recibo, $femision, $propietario, $direccion,
            $servicio, $tfact, $tarifa, $nromed, $fechalectanterior, $lectanterior, $consumo, $fechalectultima,
            $lectultima, $mensaje, $fvencimientodeudores, $fcorte, $fvencimiento, $docidentidad, $nrodocidentidad,
            $mensajenormal, $fechapago, $codpago, $serie, $codantiguo, $zona, $consumofact,$coddocumento, $nrofacturacion) {
        global $tfacturacion, $conexion, $codsuc, $itemP, $meses;

        $x = 1;
        $y = 1;

        $paramae = $this->getParamae("IMPIGV", $codsuc);
        
        $DocId = "D.N.I.";

        if (strlen(trim($nrodocidentidad)) > 8) {
            $DocId = "R.U.C.";
        }
        
        /*if ($tfact == 0) {
            $img = $this->generargraficos($nroinscripion, $codsuc);
            if ($img != false)
                $this->Image($img, 14.0, 4.5, 4, 3);
        }*/

        if(!empty($codcatastro)):
          $grafico = new Graph();
          $grafico = Image_Barcode::draw($nroinscripion, 'code128', 'jpg', false);
          imagejpeg($grafico, $codcatastro.'.jpg');
          $this->Image($codcatastro.'.jpg', 6, 26.5, 10, 1.2);
        endif;

        //$this->EAN13(13,25.5,$codcatastro);
        //$grafico = new Graph();
        //$grafico=Image_Barcode::draw($nroinscripion, 'code128', 'jpg',false);
        /* imagejpeg($grafico,$codcatastro.'.jpg');

          if($img!=false)
          if(file_exists($codcatastro.'.jpg'))
          if($codcatastro!='')
          $this->Image($codcatastro.'.jpg',8,24.5,5,1.5);
          else
          {
          echo '<script type="text/javascript">alert("Documento no Imprimible");close();</script>';
          die();
          }

          if($codcatastro=='')
          {
          echo '<script type="text/javascript">alert("Documento no Imprimible");close();</script>';
          die();
          }
         */
        $this->SetX($x);
        $y = $this->GetY();

        $this->SetFont('Arial', '', 11);
        $this->SetXY($x, $y-0.4);
        $this->Cell(0, 0.6, "Inscripcion ", 0, 0, 'L');

        $this->SetFont('Arial', 'B', 14);
        $this->SetX($x+1);
        $this->Cell(9, 0.6, $codantiguo, 0, 1, 'C');
        //$this->SetX($x);

        $this->SetFont('Arial', '', 10);
        $this->SetXY($x, $y + 0.2);
        $this->Cell(0, 0.6, "Codigo ", 0, 0, 'L');

        $this->SetX($x);
        $this->Cell(11, 0.6, $codcatastro, 0, 1, 'C');

        $this->SetXY($x, $y + 0.9);
        $this->Cell(2.5, 0.4, utf8_decode("Periodo/Ciclo"), 0, 0, 'C');
        $this->SetX($x + 3);
        $this->Cell(3, 0.4, utf8_decode("Numero de Recibo"), 0, 1, 'C');

        $this->SetXY($x, $y + 1.6);
        $this->Cell(2.5, 0.4, $facturacion, 0, 0, 'C');
        $this->SetX($x + 3);
        $this->Cell(3, 0.4, $serie."-".str_pad($recibo, 8, '0', STR_PAD_LEFT), 0, 1, 'C');



        // $this->SetXY($x + 5.5, $y - 0.3);
        // $this->SetFont('Arial', '', 9);
        // $this->Cell(6, 0.5, utf8_decode(strtoupper($propietario)), 0, 1, 'R');
        // $this->SetXY($x + 7.8, $y + 0.3);

        //$this->Cell(2, 0.5, "CODIGO ", 0, 0, 'R');
        // $this->Cell(2, 0.5, " ", 0, 0, 'R');
        // $this->SetFont('Arial', 'B', 10);
        // $this->Cell(2.5, 0.5, $codcatastro, 0, 1, 'R');

        // $this->SetFont('Arial', 'B', 12);
        // //$this->Rect($x + 14, $y, 4, 2);
        // //$this->Line($x + 14, $y + 1, 19, $y + 1);
        //
        // $this->SetXY($x + 14.3, $y - 0.2);
        // //$this->Cell(0, 0.5, "Inscripcion", 0, 1, 'C');
        // $this->Cell(0, 0, " ", 0, 1, 'C');
        // $this->SetX($x + 14.7);

        // $this->Cell(0, 0.6, $codantiguo, 0, 1, 'C');
        // $this->SetX($x + 14.3);
        //$this->Cell(0, 0.5, utf8_decode("Recibo N°"), 0, 1, 'C');
        // $this->Cell(0, 0.9, utf8_decode(" "), 0, 1, 'C');
        // $this->SetX($x + 14.5);
        // $this->Cell(0, 0.5, $serie."-".$recibo, 0, 1, 'C');

        $MesA = $meses[$itemP["mes"] - 1];
        if ($itemP["mes"] == 1)
            $MesA = "DICIEMBRE";

        $h=0.5;
        $this->Ln(0.8);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(3, $h, "Nombre", 0, 0, 'L');
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(10, $h, utf8_decode(strtoupper($propietario)), 0, 1, 'L');

        $this->SetFont('Arial', '', 10);
        $this->Cell(3, $h, "Direccion", 0, 0, 'L');
        $this->SetFont('Arial', '', 10);
        $this->Cell(10, $h, utf8_decode(strtoupper($direccion)), 0, 1, 'L');

        if ($meses["deuda"] != 0) {

            $fechavencimiento = $fvencimientodeudores;
            $fechacorte = $fcorte;
            $fv = $fvencimientodeudores;
        } else {

            $fechavencimiento = $fvencimiento;
            $fechacorte = "";
            $fv = $fvencimiento;
        }

        // Unidades de Uso
        $SqlCat = "SELECT un.nroinscripcion, un.catetar, un.porcentaje, ca.nomtar, urb.tipo || ' ' || urb.descripcion AS urbanizacion";
        $SqlCat .= " FROM facturacion.unidadesusofacturadas un ";
        $SqlCat .= " INNER JOIN catastro.clientes cl ON(cl.codemp = un.codemp) AND (cl.codsuc = un.codsuc) AND (cl.nroinscripcion = un.nroinscripcion) ";
        $SqlCat .= " INNER JOIN facturacion.tarifas ca ON(ca.codsuc = un.codsuc) AND (ca.catetar = un.catetar) ";
        $SqlCat .= " INNER JOIN catastro.urbanizaciones urb ON(cl.codsuc = urb.codsuc AND cl.codzona = urb.codzona and cl.codurbanizacion = urb.codurbanizacion)";
        $SqlCat .= " WHERE un.codsuc = ".$codsuc." ";
        $SqlCat .= " AND un.nroinscripcion = '".$nroinscripion."' ";
        $rCat = $conexion->query($SqlCat)->fetchAll();
        $nroCat = 0;
        $Tar = '';
        $nombre_tarifa_ciclica = '';

        foreach ($rCat as $rowD) :
            $nroCat ++;
            $nombre_tarifa = substr($rowD['nomtar'],0,3);
            if($nombre_tarifa != $nombre_tarifa_ciclica) :

              if(!strpos($Tar, $nombre_tarifa . "-" . $rowD['catetar'])):
                $Tar .= $nombre_tarifa . "-" . $rowD['catetar'];
              endif;

            endif;
            $nombre_tarifa_ciclica = $nombre_tarifa;
            $urbanizacion = $rowD['urbanizacion'];
        endforeach;

        $this->SetFont('Arial', 'B', 9);
        $this->Cell(3, $h,'', 0, 0, 'L');
        $this->Cell(6.5, $h, $urbanizacion, 0, 0, 'L');
        $this->Cell(2, $h, "U. USO ". $nroCat, 0, 1, 'L');

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(12, 0.3, "", 0, 1, 'L');
        $this->Cell(1, $h, $DocId, 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $this->Cell(4, $h, $nrodocidentidad, 0, 0, 'L');

        $this->SetFont('Arial', '', 9);
        $this->Cell(1, $h, "Ruta", 0, 0, 'L');
        $this->SetFont('Arial', 'B', 9);
        $ruta= $this->getRutas($codsuc, $nroinscripion);
        $this->Cell(2.5, $h, $ruta, 0, 0, 'L');

        $this->SetFont('Arial', '', 9);
        $this->Cell(1, $h, "Tarifa:", 0, 0, 'L');
        $this->SetFont('Arial', 'B', 9);

        $this->SetFont('Arial', '', 8);
        $this->Cell(1.3, 0.5, $Tar, 0, 0, 'C');

        $this->SetXY($x, $y + 5.2);
        $this->Cell(3, 0.4, "Medidor", 0, 0, 'C');
        $this->Cell(3, 0.4, "Periodo", 0, 0, 'C');
        $this->Cell(3, 0.4, "Lectura", 0, 0, 'C');
        $this->Cell(1.5, 0.4, "Cod", 0, 1, 'C');

        $this->SetXY($x, $y + 5.9);
        $this->Cell(1, 0.4, "Nro", 0, 0, 'C');
        $this->SetFont('Arial', '', 8);
        $this->Cell(2, 0.4, $nromed, 0, 0, 'C');
        $this->Cell(2, 0.4, $fechalectanterior, 0, 0, 'C');
        $this->Cell(1.5, 0.4, "Anterior", 0, 0, 'C');
        $this->Cell(2, 0.4, (int)$lectanterior, 0, 0, 'C');
        $this->Cell(2, 0.4, $consumo, 0, 1, 'C');


        if ($tfact == 0)
        {
            $t = "L";
            $ConsumoT = $consumo;
            $estadomedidor = utf8_decode($estadomedidor);
        }
        if ($tfact == 1)
        {
            $t = "P";
            $ConsumoT = intval($lecturapromedio);
            $estadomedidor='PROMEDIADO';
        }
        if ($tfact == 2)
        {
            $t = "A";
            $ConsumoT = $consumo;
            $estadomedidor='DIRECTO';
            $consumofact=$ConsumoT;
        }

        $this->SetFont('Arial', 'B', 9);
        $this->Cell(1, 0.4, "Cons", 0, 0, 'C');
        $this->SetFont('Arial', '', 8);
        $this->Cell(0.5, 0.4, $t, 0, 0, 'C');
        $this->Cell(1.5, 0.4, $consumofact . " m". Cubico, 0, 0, 'C');
        $this->Cell(2, 0.4, $fechalectultima, 0, 0, 'C');
        $this->Cell(1.5, 0.4, "Actual", 0, 0, 'C');
        $this->Cell(2, 0.4, (int)$lectultima, 0, 0, 'C');
        $this->Cell(2, 0.4, $consumo, 0, 1, 'C');

        $diferido = (int)$lectultima - (int)$lectanterior;

        $this->SetFont('Arial', '', 8);
        $this->Cell(1, 0.4, "", 0, 0, 'C');
        $this->Cell(2, 0.4, "", 0, 0, 'C');
        $this->Cell(2, 0.4, "", 0, 0, 'C');
        $this->Cell(1.5, 0.4, "Dif.", 0, 0, 'C');
        $this->Cell(2, 0.4, $diferido, 0, 0, 'C');
        $this->Cell(2, 0.4, "", 0, 1, 'C');

        $xt1 = 10;
        $this->SetXY($x, $y + 8);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(8, 0.4, "DESCRIPCION DE CONCEPTOS", 0, 0, 'C');
        $this->Cell(5, 0.4, "NO IMPONIBLE", 0, 0, 'R');
        $this->Cell(5.4, 0.4, "IMPORTE", 0, 1, 'R');

        $this->Ln(0.3);

        $h1= 0.5;
        $this->SetFont('Arial', '', 6);
        $this->Cell(4.5, $h1, "", 0, 0, 'C');
        $this->Cell(4.5, $h1, "", 0, 0, 'C');
        $this->Cell(2, $h1, "", 0, 0, 'C');
        $this->Cell(3.5, $h1, "", 0, 0, 'C');
        $this->Cell(3.5, $h1, utf8_decode(""), 0, 1, 'C');

        // $this->SetFont('Arial', '', 10.5);
        // $ruta = $this->getRutas($codsuc, $nroinscripion);
        // // $this->Cell(5.5, 0.4, $ruta, 0, 1, 'C');
        // $this->Ln(0.3);

        //$this->SetFont('Arial', '', 6);
        /* $this->Cell(9, 0.4, "PROPIETARIO", 0, 0, 'C');
          $this->Cell(9, 0.4, utf8_decode("DIRECCIÓN DEL SUMINISTRO"), 0, 1, 'C'); */
        // $this->Cell(10, 0.4, " ", 0, 0, 'C');
        // $this->Cell(8, 0.4, utf8_decode(" "), 0, 1, 'C');
        //
        // $this->SetFont('Arial', 'B', 9.5);
        // $this->Cell(1, 0.5, " ", 0, 0, 'C');
        // $this->Cell(9, 0.5, utf8_decode(strtoupper($propietario)), 0, 0, 'L');
        // $this->Cell(8, 0.5, utf8_decode($direccion), 0, 1, 'L');
        // $this->Ln(0.3);

        // $h1 = 0.5;
        // $this->SetFont('Arial', '', 6);
        // /* $this->Cell(4.5, 0.4, "DISTRITO", 0, 0, 'C');
        //   $this->Cell(4.5, 0.4, "ACTIVIDAD", 0, 0, 'C');
        //   $this->Cell(2, 0.4, "UNID. USO", 0, 0, 'C');
        //   $this->Cell(3.5, 0.4, "PERIODO CONSUMO", 0, 0, 'C');
        //   $this->Cell(3.5, 0.4, utf8_decode("TIPO FATUCARCIÓN"), 0, 1, 'C'); */
        // $this->Cell(4.5, $h1, "", 0, 0, 'C');
        // $this->Cell(4.5, $h1, "", 0, 0, 'C');
        // $this->Cell(2, $h1, "", 0, 0, 'C');
        // $this->Cell(3.5, $h1, "", 0, 0, 'C');
        // $this->Cell(3.5, $h1, utf8_decode(""), 0, 1, 'C');

        // if ($tfact == 0) {
        //     $t = "L";
        //     $ConsumoT = $consumo;
        //     $estadomedidor = utf8_decode($estadomedidor);
        // }
        // if ($tfact == 1) {
        //     $t = "P";
        //     $ConsumoT = intval($lecturapromedio);
        //     $estadomedidor = 'PROMEDIADO';
        // }
        // if ($tfact == 2) {
        //     $t = "A";
        //     $ConsumoT = $consumo;
        //     $estadomedidor = 'DIRECTO';
        // }

        // $this->SetFont('Arial', '', 9);
        // $this->Cell(0.5, 0.5, " ", 0, 0, 'C');
        // $this->Cell(4.1, 0.4, utf8_decode($zona), 0, 0, 'C');
        // //$this->Cell(4.2, 0.4, utf8_decode($tipoactividad), 0, 0, 'C');
        // $this->Cell(4.2, 0.4, utf8_decode(''), 0, 0, 'C');
        // $this->Cell(2.6, 0.4, $nroCat, 0, 0, 'C');
        //
        // //$this->Cell(2.5, 0.4, "1 - ".$tarifa, 0, 0, 'C');
        // $this->Cell(4.5, 0.4, $fechalectanterior." - ".$fechalectultima, 0, 0, 'C');
        // $this->Cell(2.8, 0.4, $t, 0, 1, 'C');
        // $this->Ln(0.3);

        /* $this->Cell(9, 0.5, "INFORMACION DE MEDIDOR", 0, 0, 'C');
        //   $this->Cell(9, 0.5, "EVOLUCION DEL CONSUMO", 0, 1, 'C'); */
        // $this->Cell(8, 0.5, "", 0, 0, 'C');
        // $this->Cell(9, 0.5, "", 0, 1, 'C');
        //
        // $y = $this->GetY();
        //
        // $this->Cell(3.5, 0.7, " ", 0, 0, 'C');
        // $this->Cell(2.5, 0.7, " ", 0, 0, 'C');
        // $this->Cell(2.5, 0.7, " ", 0, 1, 'C');

        //echo $vma;
        if ($vma == 1) {
            $this->SetFont('Arial', '', 7);
            $this->SetXY($x + 11.5, $y);

            $ConDes = "SELECT d.nroinscripcion,d.codconcepto,c.descripcion,d.nrocredito,
                sum(d.importe-(importerebajado+imppagado)) as monto, c.categoria
                FROM facturacion.detfacturacion as d
                INNER JOIN facturacion.conceptos as c on (d.codconcepto=c.codconcepto AND d.codemp=c.codemp AND d.codsuc=c.codsuc)
                WHERE d.estadofacturacion = 1 AND d.nroinscripcion='".$nroinscripion."'
                AND d.codsuc=".$codsuc." AND d.categoria=0 AND d.codconcepto=2
                GROUP BY d.nroinscripcion,d.codconcepto,c.descripcion,d.nrocredito,c.categoria
                ORDER BY d.codconcepto ";
            $res = $conexion->query($ConDes)->fetch();
            $monto = $res["monto"];

            $this->Cell(6, 0.4, "Detalle Importe Facturado Desague ", 1, 1, 'C');
            $this->SetX($x + 11.5);
            $this->Cell(2.3, 0.4, "Total (3 Unid. Uso) ", 1, 0, 'C');
            $this->Cell(1.8, 0.4, "100% ", 1, 0, 'C');
            $this->Cell(1.9, 0.4, number_format($monto, 2), 1, 1, 'R');
            //$rowMBA=$this->TablaParametros($codsuc, $nroinscripcion, $CodCt);

            $cont_unidades = 0;
            $tot_porcentaje = 0;

            $sql_unidades = "SELECT codsuc, nroinscripcion, codunidad_uso, descripcion, porcentaje, factor ";
            $sql_unidades .= "FROM vma.unidades_uso ";
            $sql_unidades .= "WHERE codsuc=".$codsuc." AND nroinscripcion= ".$nroinscripion;
            $consulta_unidades = $conexion->query($sql_unidades);
            //$consulta_unidades->execute(array($codsuc, $nroinscripcion));
            $items_unidad = $consulta_unidades->fetchAll();

            foreach ($items_unidad as $rowUnidades) {
                $cont_unidades++;
                $tot_porcentaje += $rowUnidades["porcentaje"];
                $porc = $rowUnidades["porcentaje"];
                $ImpPorc = ($porc * $monto) / 100;

                $this->SetX($x + 11.5);
                $this->Cell(2.3, 0.4, "Unid. Uso ".$rowUnidades['descripcion'], 1, 0, 'C');
                $this->Cell(1.8, 0.4, number_format($rowUnidades["porcentaje"], 2), 1, 0, 'C');
                $this->Cell(1.9, 0.4, number_format($ImpPorc, 2), 1, 1, 'R');
            }
        }

        $this->Ln(0.3);

        $lectultima = intval($lectultima);
        $lectanterior = intval($lectanterior);

        $this->Ln(0.2);

        $this->Cell(3.5, 0.9, " ", 0, 0, 'C');
        $this->Cell(2.5, 0.9, " ", 0, 0, 'C');
        $this->Cell(2.5, 0.9, " ", 0, 1, 'C');
        $this->Ln(1.2);

        $this->SetFont('Arial', 'B', 8);
        $this->SetXY($x-0.1, $y + 14.5);
        $this->MultiCell(7, 0.5, utf8_decode(strtoupper($mensajenormal)), 0, 'C');

        $this->Ln(0.25);
        $msj=" ";
        $this->MultiCell(7, 0.5, utf8_decode(strtoupper("")), 0, 'J');
        $this->Ln(0.5);
        $this->SetFont('Arial', 'B', 17);

        $this->SetXY($x + 1, $y + 17);
        $this->Cell(5, 0.5, "DUPLICADO", 0, 1, 'C');
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(6, 0.5, "CANCELADO ", 0, 1, 'C');

        $this->Ln(3.5);



        // $this->SetFont('Arial', '', 9.5);
        // $this->SetXY($x, $y + 1);
        // $this->Cell(3.5, 0.5, $nromed, 0, 0, 'C');
        // $this->Cell(2.5, 0.5, $tfact == 0 ? $lectultima."m".Cubico : "", 0, 0, 'C');
        // $this->Cell(2.5, 0.5, $tfact == 0 ? $lectanterior."m".Cubico : "", 0, 1, 'C');

        //$this->Rect($x, $y+1.2, 4, 1.2);
        //$this->Rect($x+4, $y+1.2, 2.5, 1.2);
        //$this->Rect($x+6.5, $y+1.2, 2.5, 1.2);
        // $this->Ln(0.2);
        /*
          $this->Cell(4, 0.3, "CODIGO DE LECTURA", 0, 0, 'C');
          $this->Cell(2.5, 0.3, "Vol. Consumo", 0, 0, 'C');
          $this->Cell(2.5, 0.3, "Vol. Facturado", 0, 1, 'C'); */
        // $this->Cell(3.5, 0.9, " ", 0, 0, 'C');
        // $this->Cell(2.5, 0.9, " ", 0, 0, 'C');
        // $this->Cell(2.5, 0.9, " ", 0, 1, 'C');
        // $this->Ln(0.2);
        //
        // $consumo = intval($consumo);
        // $consumofact = intval($consumofact);
        //
        // $this->Cell(3.5, 0.5, $nromed, 0, 0, 'C');
        // $this->Cell(2.5, 0.5, $consumo != '' ? $consumo."m".Cubico : "", 0, 0, 'C');
        // $this->Cell(2.5, 0.5, $consumofact != '' ? $consumofact."m".Cubico : "", 0, 1, 'C');
        //
        // //$this->Rect($x, $y+2.4, 9, 0.5);
        // //$this->Rect($x+9, $y+2.4, 9, 0.5);
        // $this->Ln(1);
        // /**/
        // //$this->Cell(8, 0.5, "INFORMACION COMPLEMENTARIA", 0, 0, 'C');
        // //$this->Cell(9, 0.5, "DETALLE DE LA FACTURACION", 0, 1, 'C');
        // $this->Cell(8, 0.5, " ", 0, 0, 'C');
        // $this->Cell(9, 0.5, " ", 0, 1, 'C');

        //$this->Rect($x, $y+2.9, 9, 6);
        //$this->Rect($x+9, $y+2.9, 9, 6);
        $this->Ln(0.25);
        $msj = " ";
        $this->MultiCell(7, 0.5, utf8_decode(strtoupper("")), 0, 'J');
        //$this->Ln(0.5);
        $this->SetFont('Arial', 'B', 17);
        // if ($vma != 1) {
        //     $this->Cell(8, 0.5, "DUPLICADO", 0, 1, 'C');
        // }

        $this->Ln(3);
        /* CONCEPTOS */

        $subtotMes = 0;
        $interesMes = 0;
        $redondeo = 0;
        $igvMes = 0;
        $totrecibo = 0;

        $cn = 0.5;
        $this->SetXY($x+1, 10);
        //  $this->SetXY($x + 9.5, $y + 4.5);

       $this->SetFont('Arial', '', 10);
            //OBTENER FACTURACION DE ESE PAGO
            $Sql = "SELECT d.nroinscripcion,d.codconcepto,c.descripcion,sum(d.importe) as monto,c.categoria
                FROM cobranza.detpagos d
                inner join facturacion.conceptos as c on (d.codconcepto=c.codconcepto and d.codemp=c.codemp and d.codsuc=c.codsuc)
                where  d.codemp=1 AND d.codsuc=".$codsuc." and d.nroinscripcion=".$nroinscripion."
                and  d.nropago=".$codpago." and d.coddocumento=".$coddocumento."
                group by d.nroinscripcion,d.codconcepto,c.descripcion,c.categoria
                order by d.codconcepto; ";
            $Consulta = $conexion->query($Sql);
            $itemsD = $Consulta->fetchAll();
            foreach ($itemsD as $rowD) {
                // $this->SetX($x + 9);
                $this->SetX($x+1);
                $MontoTem = $rowD["monto"];
                if ($rowD["categoria"] != 3 && $rowD["categoria"] != 4 && $rowD["categoria"] != 7) {
                    $this->Cell(14.8, 0.5, $rowD["codconcepto"]." ".strtoupper($rowD["descripcion"]), 0, 0, 'L');
                    //$this->Cell(4, 0.5, '', 0, 0, 'R');
                    $this->Cell(2.5, 0.5, number_format($MontoTem, 2), 0, 1, 'R');

                    $subtotMes += $MontoTem;
                }
                if ($rowD["categoria"] == 3) {
                    $interesMes += $MontoTem;
                }
                if ($rowD["categoria"] == 7) {
                    $redondeo += $MontoTem;
                }
                if ($rowD["categoria"] == 4) {
                    $igvMes += $MontoTem;
                }
            }

            $this->SetX($x + 2.3);
            if ($interesMes > 0) {
                $this->Cell(13.4, 0.4, "MORA(S)", 0, 0, 'L');
                $this->Cell(2.6, 0.5, number_format($interesMes, 2), 0, 1, 'R');
            }

            $subtotMes = $subtotMes + $interesMes;

            $this->SetFont('Arial', '', 7);
           // $this->SetY(11);
            if ($glosa != "")
                $glosa = "=>".$glosa;
            $this->Cell(6, 0.5, $glosa, 0, 1, 'L');

            if ($vma != 1) {
              if ($meses["deuda"] != 0)
              {
                  $this->Image("../../../../../images/logo_corte.jpg", 8.6, 17.4, 0.8, 0.8);
                  $this->Image("../../../../../images/logo_triste.jpg", 8, 15, 2, 2);
              }
              else
              {
                  $this->Image("../../../../../images/logo_feliz.jpg", 8, 15, 2, 2);

              }
        } else {
            $CodUnidadUso = 0;
            $Tabla1 = TablaParametros($codsuc, $nroinscripion, $CodUnidadUso);
            $ConUU = count($Tabla1);

            $this->SetFont('Arial', 'B', 7);
            $this->SetY(12);
            $this->Cell(2.9, 0.45, "Parametros de Augas", 'LTR', 1, 'C');
            $this->Cell(2.9, 0.45, "Residuales", 'LBR', 0, 'C');
            $this->SetXY($x + 2.9, 12);
            $this->Cell(1.2, 0.9, "DBO5", 1, 0, 'C');
            $this->Cell(1.2, 0.9, "DQO", 1, 0, 'C');
            $this->Cell(1.2, 0.9, "SST", 1, 0, 'C');
            $this->Cell(1.2, 0.9, "AyG", 1, 0, 'C');
            $this->SetXY($x + 7.7, 12);
            $this->Cell(1.2, 0.9, "Factor", 'LTR', 1, 'C');

            $this->Cell(2.9, 0.45, "VMA (mg/L)", 1, 0, 'L');
            $this->Cell(1.2, 0.45, "500", 1, 0, 'C');
            $this->Cell(1.2, 0.45, "1,000", 1, 0, 'C');
            $this->Cell(1.2, 0.45, "500", 1, 0, 'C');
            $this->Cell(1.2, 0.45, "100", 1, 0, 'C');
            $this->SetXY($x + 7.7, 12.45);
            $this->Cell(1.2, 0.9, "Total", 'LBR', 1, 'C');

            $this->SetFont('Arial', '', 7.5);

            for ($i = 0; $i < $ConUU; $i++) {
                $ii = $Tabla1[$i + 1][4][1][2];
                $iii = $Tabla1[$i + 1][4][2][2];
                $iiii = $Tabla1[$i + 1][4][3][2];
                $iiiii = $Tabla1[$i + 1][4][4][2];
                $timp = $Tabla1[$i + 1][3];

                $this->Cell(2.9, 0.5, utf8_decode("Valor de Análisis ").$Tabla1[$i + 1][1], 1, 0, 'L');
                $this->Cell(1.2, 0.5, number_format($Tabla1[$i + 1][4][1][1], 1), 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($Tabla1[$i + 1][4][2][1], 1), 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($Tabla1[$i + 1][4][3][1], 1), 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($Tabla1[$i + 1][4][4][1], 1), 1, 0, 'C');
                $this->Cell(1.2, 0.5, "", 'LTR', 1, 'C');

                $this->Cell(2.9, 0.5, utf8_decode("Factores Individuales "), 1, 0, 'L');
                $this->Cell(1.2, 0.5, number_format($ii, 0)." %", 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($iii, 0)." %", 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($iiii, 0)." %", 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($iiiii, 0)." %", 1, 0, 'C');
                $this->Cell(1.2, 0.5, number_format($timp, 0)." %", 'LBR', 1, 'C');
            }
        }

        $totrecibo = $meses["deuda"] + $igvmes + ($redondeo) + $subtotMes;
        $this->SetFont('Arial', '', 10);
        $this->Cell(12, 0.5, "Fecha Cancelacion : ".$fechapago, 0, 1, 'C');

        $this->SetFont('Arial', '', 9);
        $this->SetY(16.2);
        $this->SetX($x+9);
        $this->Cell(2, 0.4, "SUBTOTAL", 0, 0, 'L');
        if ($meses["deuda"] != 0) {
            $this->Cell(1.7, 0.4, $deuda_anterior . " + ", 0, 0, 'R');
        }else{
          $this->Cell(1.7, 0.4," ", 0, 0, 'R');
        }

        $this->Cell(5.5, 0.4, number_format($subtotMes, 2), 0, 1, 'R');
        // $this->SetFont('Arial', 'B', 12);
        // $this->Cell(6, 0.5, "CANCELADO ", 0, 0, 'C');
        $this->SetFont('Arial', '', 10);
        $this->SetX($x + 9);
        $this->Cell(2, 0.4, "Igv ".$paramae["valor"]."%", 0, 0, 'L');
        $this->Cell(7.2, 0.4, number_format($igvMes, 2), 0, 1, 'R');
        $this->SetX($x + 9);
        $this->Cell(2, 0.4, "Redondeo", 0, 0, 'L');
        $this->Cell(7.2, 0.4, number_format($redondeo, 2), 0, 1, 'R');
        //$this->Cell(14, 0.5,"Redondeo",0,0,'R');
        //$this->Cell(3, 0.5,number_format($redondeo,2),0,1,'R');

        // $this->Ln(0.2);
        $this->SetFont('Arial', 'B', 8);
        $NunLetras = CantidadEnLetra($totrecibo);
        $this->SetXY($x+1,$y+18);
        $descripcion_moneda = '';
        if ($this->GetStringWidth($NunLetras) > 100) {
            $this->SetFont('Arial', 'B', 8);
            $descripcion_moneda = $NunLetras." NUEVOS SOLES";
            $this->Cell(8, 0.5, "".$NunLetras." NUEVOS SOLES", 0, 1, 'L');
        } else
          $descripcion_moneda = $NunLetras." NUEVOS SOLES";
          $this->Cell(8, 0.5, "".$NunLetras." NUEVOS SOLES", 0, 1, 'L');

        $this->Ln(0.9);
        $this->SetFont('Arial', 'B', 8);

        $totrecibo = $meses["deuda"] + $igvMes + ($redondeo) + $subtotMes;
        //$this->Cell(15, 0.5, "TOTAL A PAGAR S/.", 0, 0, 'R');

        $this->SetXY($x+9, 18.5);
        $this->SetFont('Arial', 'B', 11);
        $this->Cell(5.5, 0.5, " Total a Pagar", 0, 0, 'R');

        // $this->SetX($x + 9);
        // $this->Cell(5.5, 0.9, " ", 0, 0, 'R');

        $totrecibo = number_format($totrecibo, 2);
        $TotalF = str_pad($totrecibo, 10, "*", STR_PAD_LEFT);

        // $this->SetXY($x + 15, 18.1);
        $this->SetFont('Arial', 'B', 16);
        $this->SetFillColor(195, 195, 195);
        $this->Cell(3.7, 0.5, $TotalF, 0, 1, 'R', true);
        // $this->Cell(2.5, 0.9, $TotalF, 0, 1, 'R');
        $this->Ln(0.1);

        /* ---- MENSAJES ---- */

        $y= 10.5;
        $this->SetXY($x, $y+7.7);
        $this->SetFont('Arial', '', 8.5);
        $this->Cell(18, 0.5, "", 0, 0, 'C');
        $this->Ln(1.5);

        $this->SetFont('Arial', 'B', 9);
        $this->MultiCell(18, 0.5, utf8_decode(strtoupper($mensaje." HOLAA")), 0, 'C');
        $this->Ln(0.6);

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(3.8, 0.5, "FECHA DE EMISION: " . $femision, 0, 0, 'L');
        $this->Cell(6.5, 0.5, utf8_decode($horasabastecimiento), 0, 0, 'L');
        $this->Cell(4.2, 0.5, "ULTIMO DIA DE PAGO: ".$fvencimiento, 0, 0, 'L');
        $this->Cell(4.5, 0.5, utf8_decode($horasabastecimiento), 0, 1, 'L');


        $this->SetFont('Arial', '', 8);
        $this->SetXY($x + 6.5, $y + 13.5);
        $this->Cell(7, 0.5, utf8_decode(strtoupper($propietario)), 0, 0, 'L');
        $this->Cell(3, 0.5, $codcatastro, 0, 0, 'L');
        $this->Cell(2.2, 0.5, $codantiguo, 0, 1, 'L');

        $this->Ln(2);

        $this->SetFont('Arial', 'B', 7);

        // $y = 10.5;
        // $this->SetXY($x, $y + 7.7);
        // $this->SetFont('Arial', '', 8.5);
        // //$this->Rect($x, $y+5, 18, 0.5);
        // //$this->Cell(18, 0.5, "MENSAJES", 0, 0, 'C');
        // $this->Cell(18, 0.5, "", 0, 0, 'C');
        //
        // //$this->Rect($x, $y+5, 18, 2.5);
        // $this->Ln(2);
        // $this->MultiCell(18, 0.5, utf8_decode(strtoupper($mensajenormal)), 0, 'L');


        // $this->SetXY($x + 8.8, $y + 12.3);
        // $this->SetFont('Arial', '', 10);
        // $this->Cell(5, 0.5, utf8_decode(strtoupper($propietario)), 0, 1, 'R');
        //
        // $this->Ln(2);

        // $this->SetFont('Arial', '', 6);

        // $this->SetXY($x + 8.5, $y + 14.2);
        // $this->Cell(3.6, 0.4, "", 0, 0, 'C');
        // $this->Cell(3.6, 0.4, utf8_decode(""), 0, 0, 'C');
        // $this->Cell(3.6, 0.4, "", 0, 0, 'C');
        // $this->Cell(3.6, 0.4, utf8_decode(""), 0, 0, 'C');
        // $this->Cell(3.6, 0.4, utf8_decode(""), 0, 1, 'C');

        if ($meses["deuda"] != 0) {
            $fechavencimiento = $fvencimientodeudores;
            $fechacorte = $fcorte;
            $fv = $fvencimientodeudores;
        } else {

            $fechavencimiento = $fvencimiento;
            $fechacorte = "";
            $fv = $fvencimiento;
        }

        $this->SetFont('Arial', 'B', 9);
        $this->SetXY($x, $y + 14.3);
        $this->Cell(3.9, 0.5, 'FECHA DE EMISION', 0, 0, 'C');
        $this->Cell(5, 0.5, 'ULTIMO DIA DE PAGO', 0, 0, 'C');
        $this->Cell(5, 0.5, $facturacion, 0, 0, 'C');
        $this->Cell(4.8, 0.55, $serie."-".str_pad($recibo, 8, '0', STR_PAD_LEFT), 0, 1, 'C');

        $this->Cell(3.9, 0.7, $femision, 0, 0, 'C');
        $this->Cell(5, 0.7, $fvencimiento, 0, 0, 'C');
        $this->SetFont('Arial', '', 7);
        $this->Cell(5, 0.7, "TOTAL A PAGAR", 0, 0, 'C');
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(4.3, 0.6, utf8_decode(" ".$TotalF), 0, 1, 'R', true);
        $this->Ln(0.4);

        $this->SetFont('Arial', 'B', 8);
        $this->SetXY($x+2, $y + 15.4);
        $this->Cell(3.4, 0.6, utf8_decode($descripcion_moneda), 0, 0, 'C');
        $this->Cell(3.8, 0.6, utf8_decode(""), 0, 1, 'C');
        $this->SetFont('Arial', '', 8.5);

        // $this->SetFont('Arial', '', 9);
        // $this->Cell(3.8, 0.4, $facturacion, 0, 0, 'C');
        // $this->Cell(3.6, 0.4, $femision, 0, 0, 'C');
        // $this->SetFont('Arial', 'B', 11);
        // $this->Cell(3.6, 0.4, $fvencimiento, 0, 0, 'C');
        // $this->SetFont('Arial', 'B', 11);
        // $this->Cell(3.7, 0.4, $serie."-".$recibo, 0, 0, 'C');
        // $this->SetFont('Arial', 'B', 18);
        // $this->Cell(3.7, 0.4, utf8_decode(" ".$TotalF), 0, 1, 'C');
        // $this->Ln(0.4);
        //
        // $this->SetFont('Arial', '', 8);
        // /* $this->Cell(3.6, 0.4, utf8_decode("INSCRIPCIÓN"), 0, 0, 'C');
        //   $this->Cell(3.6, 0.4, utf8_decode("CÓDIGO"), 0, 1, 'C'); */
        // $this->Cell(3.4, 0.6, utf8_decode(""), 0, 0, 'C');
        // $this->Cell(3.8, 0.6, utf8_decode(""), 0, 1, 'C');
        // $this->SetFont('Arial', '', 8.5);
        // //$this->Cell(3.4, 0.4, $nroinscripion." - ".$codantiguo, 0, 0, 'C');
        // $this->SetFont('Arial', 'B', 13);
        // $this->Cell(3.4, 0.4, $codantiguo, 0, 0, 'C');
        // $this->SetFont('Arial', '', 10);
        // $this->Cell(3.8, 0.4, $codcatastro, 0, 1, 'C');
        // //$this->Ln(0.2);
        // $this->SetXY($x + 6, $y + 15.8);
        // $recibon = $serie."-".str_pad($recibo, 8, "0", STR_PAD_LEFT);
        // $this->SetFont('Arial', 'B', 20);
        // $this->Cell(0, 0.5, "*".$recibon."*", 0, 1, 'C');
    }

    function Footer() {

    }

    //  Para los cuadros con bordes redondeados o sin ellos
    function RoundedRect($x, $y, $w, $h, $r, $style = '') {
      $k = $this->k;
      $hp = $this->h;
      if($style=='F')
        $op='f';
      elseif($style=='FD' || $style=='DF')
        $op='B';
      else
        $op='S';
      $MyArc = 4/3 * (sqrt(2) - 1);
      $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
      $xc = $x+$w-$r ;
      $yc = $y+$r;
      $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

      $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
      $xc = $x+$w-$r ;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
      $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
      $xc = $x+$r ;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
      $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
      $xc = $x+$r ;
      $yc = $y+$r;
      $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
      $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
      $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
     $h = $this->h;
     $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
     $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }


}

$nroinscripcion = $_GET["nroinscripcion"];
$codsuc = $_GET["codsuc"];
$codciclo = $_GET["codciclo"];
$fechapago = $_GET["fechapago"];
$nrofacturacion = $_SESSION["nrofacturacion"] - 1;
$codpago = $_GET["codpago"];
//

define('Cubico', chr(179));
 $Sql = "SELECT coddocumento,nrofacturacion
    FROM cobranza.detpagos d
    WHERE  d.codemp=1 AND d.codsuc=".$codsuc." and d.nropago=".$codpago."
    group by coddocumento,nrofacturacion
    order by coddocumento, nrofacturacion ";
    $ConsultaX = $conexion->query($Sql);
    $itemsDX = $ConsultaX->fetchAll();
    foreach ($itemsDX as $rowDX)
    {
        $nrofacturacion = $rowDX[1];

        $sqlP = "SELECT nrofacturacion,anio,mes,fechaemision,mensajegeneral,fechavencnormal,
            fechavencdeudores,fechacorte,mensajenormal
            FROM facturacion.periodofacturacion
            WHERE codsuc=? and codciclo=? and nrofacturacion=?";

        $consultaP = $conexion->prepare($sqlP);
        $consultaP->execute(array($codsuc, $codciclo, $nrofacturacion));
        $itemP = $consultaP->fetch();
        $objReporte = new clsImpresionDuplicado('L', 'cm', array(28.00, 20.00));

        $sql = "SELECT * FROM facturacion.view_cabfacturacion WHERE nroinscripcion=? and nrofacturacion=? and codsuc=?";
        $itemP["nrofacturacion"];
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($nroinscripcion, $itemP["nrofacturacion"], $codsuc));
        $items = $consulta->fetch();

        $objReporte->AddPage();
        $objReporte->SetAutoPageBreak(true, 1);
        $facturacion = substr($meses[$itemP["mes"]], 0, 3)."-".$itemP["anio"];

        $objReporte->SetFillColor(255, 255, 255);
        $objReporte->RoundedRect(1, 0.5, 7, 2.5, 0, 'DF');  // Para la cabecera
        $objReporte->Line(1, 1.8, 8, 1.8); // Linea Horizontal Cuadro para bauche caja
        $objReporte->Line(3.7, 1.8, 3.7, 3); // Linea Vertical Cuadro para bauche caja

        $objReporte->RoundedRect(1, 6.2, 10.5, 2.5, 0, 'DF');  // Para los lectura periodos
        $objReporte->Line(4, 6.2, 4, 8.7); // Cuadro para bauche caja
        $objReporte->Line(1, 6.6, 11.5, 6.6); // Linea veritical Cuadro para bauche caja

        $objReporte->RoundedRect(0.7, 9.5, 19, 10, 0.3, 'DF');  // Para los conceptos
        $objReporte->RoundedRect(0.7, 19.65, 19, 2.5, 0.3, 'DF'); // Para el mensaje
        $objReporte->Line(1, 21.3, 19.7, 21.3); // Cuadro para bauche caja

        $objReporte->RoundedRect(0.7, 24.5, 19, 1.5, 0.3, 'DF'); // Cuadro para bauche caja
        $objReporte->Line(4.9, 24.5, 4.9, 26); // 1° linea Cuadro para bauche caja
        $objReporte->Line(9.9, 24.5, 9.9, 26); // 2° linea Cuadro para bauche caja
        $objReporte->Line(9.9, 25.3, 19.7, 25.3); // 3° linea horizontal Cuadro para bauche caja

        $objReporte->Contenido($nroinscripcion, $items["codcatastro"], $facturacion, $items["nrodocumento"],
        $objReporte->DecFecha($itemP["fechaemision"]), $items["propietario"], $items["direccion"],
        $items["descripcion"], $items["tipofacturacion"], $items["nomtar"], $items["nromed"],
        $objReporte->DecFecha($items["fechalectanterior"]), $items["lecturaanterior"], $items["consumo"],
        $objReporte->DecFecha($items["fechalectultima"]), $items["lecturaultima"], $itemP["mensajegeneral"],
        $objReporte->DecFecha($itemP["fechavencdeudores"]), $objReporte->DecFecha($itemP["fechacorte"]),
        $objReporte->DecFecha($itemP["fechavencnormal"]), $items["docidentidad"], $items["nrodocidentidad"],
        $itemP['mensajenormal'], $objReporte->DecFecha($fechapago), $codpago, $items['serie'],
        $items['codantiguo'], $items['zona'], $items['consumofact'],$rowDX['coddocumento'], $nrofacturacion);
}

$objReporte->SetTitle(':: DUPLICADO CANCELADO - '.$nroinscripcion.' ::');
$modo="I";
$nombre_archivo = "DuplicadoCancelado_".$nroinscripcion.'.pdf';
$objReporte->Output($nombre_archivo,$modo);

?>
