<?php

	session_name("pnsu");
	if(!session_start()){session_start();}

    include("clsactualizacion.php");
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op             = $_GET["Op"];

    $objFunciones   = new clsFunciones();
    $codtultimo     = 0;

    $codemp         = 1;
    $cont           = 1;
    $nItems         = $_SESSION["oactualizacion"]->item;

    $idusuario      = $_SESSION['id_user'];
    $codsuc         = $_SESSION['IdSucursal'];
    $observacion    = $_POST["obs"];
    $flagnuevotext  = $_POST["flagnuevotext"];
    $nroinscripcion = $_POST["nroinscripcion"];
    $codcliente     = $_POST["codcliente"];

    $estadoservicio = $_POST["estadoservicio"];

    $consultaM      = $conexion->prepare("SELECT MAX(nromodificaciones) FROM catastro.modificaciones WHERE codsuc = " . $codsuc);
    $consultaM->execute();
    if ($rowM = $consultaM->fetch()) {
        $correlativo = $rowM[0];
    }

    $campoold = "";
    $valorold = "";
    $entro = 0;

    if ($flagnuevotext == 1) {
        $codcliente = $objFunciones->SETCorrelativosVarios(1, $codsuc, "SELECT", 0);
        $n = $objFunciones->SETCorrelativosVarios(1, $codsuc, "UPDATE", $codcliente);
    }

    for ($i = 0; $i < $nItems; $i++) {
        $campo = $_SESSION["oactualizacion"]->campo[$i];
        $valor = $_SESSION["oactualizacion"]->valor[$i];
        $tipo  = $_SESSION["oactualizacion"]->tipo[$i];

        if ($tipo == 0) :
            $sql = "SELECT ".$campo." FROM catastro.clientes WHERE codemp = ? AND codsuc = ? AND nroinscripcion = ?";
        else :
            $sql = "SELECT ".$campo." FROM catastro.conexiones WHERE codemp = ? AND codsuc = ? AND nroinscripcion = ?";
				endif;

        $consultaF = $conexion->prepare($sql);
        $consultaF->execute(array($codemp, $codsuc, $nroinscripcion));
        $items = $consultaF->fetch();
        $campoold = $campo;
        $valorold = $items[0];

        $correlativo++;

        $instModificaciones = "INSERT INTO catastro.modificaciones(codemp,codsuc,nroinscripcion,
            nromodificaciones,campo, valoractual,valorultimo,observacion,creador)
            VALUES( ".$codemp.", ".$codsuc.", '".$nroinscripcion."', ".$correlativo.", '".$campoold."', '".$valorold."', '".$valor."', '".$observacion."', ".$idusuario." )";
        $result = $conexion->query($instModificaciones);
        $campo = $_SESSION["oactualizacion"]->campo[$i];
    }

    //Cambio de Categoria y Actividad
    $SqlTA = "SELECT codtipoactividad, catetar FROM catastro.clientes ";
    $SqlTA .= "WHERE codemp=1 and codsuc=".$codsuc." and nroinscripcion=".$nroinscripcion."";

    $resultTA = $conexion->prepare($SqlTA);
    $resultTA->execute(array());
    $itemsTA = $resultTA->fetch();

    $tipoactividadA = $itemsTA[0];
    $catetarA = $itemsTA[1];

    //die();
	$updC  = "UPDATE catastro.clientes ";
	$updC .= "SET codestadoservicio = " . $estadoservicio . " ";
	$updC .= "WHERE codemp = " . $codemp . " AND codsuc = " . $codsuc . " ";
	$updC .= " AND nroinscripcion = '" . $nroinscripcion . "' ";
  $result = $conexion->query($updC);

  if(!$result):
		$conexion->rollBack();
		var_dump($result->errorInfo());
		die(2);
	else:
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res = 1;
	endif;
?>
