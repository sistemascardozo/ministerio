<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../objetos/clsDrop.php");

	$objMantenimiento = new clsDrop();

	$Consulta = isset($_SESSION["Consulta"])?$_SESSION["Consulta"]:'0';

	$Op = isset($_POST["Op"])?$_POST["Op"]:$_GET["Op"];

	$Id = isset($_POST["Id"])?$_POST["Id"]:'';

	if ($Op == 100)
	{
		include("../../../../include/claseindex.php");

		$Id = $_GET['NroInscripcion'];

		$Consulta = 1;
	}

	$codsuc = $_SESSION['IdSucursal'];

	$guardar = "op=".$Op;


	unset($_SESSION["oactualizacion"]);

	$sucursal = $objMantenimiento->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);


$sql = "	SELECT 	c.nroinscripcion, c.codcliente, c.codsector, codmanzanas, c.codcalle, c.lote,
								c.sublote, c.codestadoservicio, conx.codtiporesponsable,
    						c.propietario,
						    e.descripcion as estser, c.codantiguo
  				FROM catastro.clientes AS c
  					JOIN catastro.conexiones AS conx ON(c.codemp=conx.codemp AND c.codsuc=conx.codsuc AND c.nroinscripcion=conx.nroinscripcion)
  					JOIN public.estadoservicio AS e ON(c.codestadoservicio = e.codestadoservicio)
    			WHERE c.codemp = 1 AND c.codsuc = ? AND c.nroinscripcion = ? ";
//echo $codsuc."-".$Id;
$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc, $Id));
$items = $consulta->fetch();
//var_dump($consulta->errorInfo());
$guardar = $guardar."&Id2=".$Id;

if ($Op == 100)
{
?>
<link rel="stylesheet" href="<?php echo $_SESSION['urldir'];?>css/css_base.css" type="text/css" />
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery-ui-1.9.2.custom.min.js"></script>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery_sistema.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $_SESSION['urldir'];?>images/empresa.ico" />

<!-- Código CheGuimo Inicio -->
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/interface.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir'];?>css/interface_menu.css">
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones_interface.js"></script>
<script>
	var urlDir = '<?php echo $_SESSION['urldir'];?>';
	var codsuc = <?=$codsuc?>;
</script>
<?php
}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_estadoservicio.js" language="JavaScript"></script>

<style type="text/css">

    #tbCampos * {
        font-size: 10px;
    }
</style>
<script>
    var codsuc = <?=$codsuc?>;
    var urldir = '<?php echo $_SESSION['urldir'];?>';

    VImportePozo(<?=$conpozo?>);

    $(document).ready(function () {
        $("#tabs").tabs();

        $('form').keypress(function (e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
		});


    $("#DivLecturas").dialog({
            autoOpen: false,
            height: 250,
            width: 520,
            modal: true,
            resizable: true,
            buttons: {
                "Aceptar": function () {
                    if (objIdx == "promediar")
                    {
                        $("#lecturaultima").val($("#lectpromedio").val())
                        $("#consumo").val($("#lectpromedio").val())
                        $("#lecturapromedio").val($("#lectpromedio").val())
                        //calcularconsumo($("#lectpromedio").val())
                    } else {
                        if ($("#obstem").val() == '')
                        {
                            Msj($("#obstem"), "Digite el Motivo de Modificación")
                            return false;
                        }
                        $("#obs").val($("#obstem").val())
                        GuardarP(1)
                    }
                    $(this).dialog("close");
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {}
        });


    })

	function BloquearTodo()
	{
		if('<?=$Consulta?>' == '1')
		{
			$("input").each(function (index)
			{
				$(this).attr("readonly", "readonly");
			})

			$("textarea").each(function (index)
			{
				$(this).attr("readonly", "readonly");
			})

			$("select").each(function (index)
			{
				$(this).attr("disabled", "disabled");
			})

			$("radio").each(function (index)
			{
				$(this).attr("disabled", "disabled");
			})

			//Iconos de Buqueda y otros
			$('#imgBuscarU').hide();
			$('#imgBuscarC').hide();

			$('#flagnuevo').hide();
			$('#lblUsuarioN').hide();

			$('#altoconsumidor').hide();
			$('#lblAltoConsumidor').hide();

			$('#imgAsignarC').hide();
			$('#imgPromediarL').hide();

			//Fecha
			//$('#fechalecturaanterior').empty();

			$('.trUnidUso').hide();
			$('.imgQuitarUU').hide();

			$('#button-maceptar').hide();
		}
	}

    function ValidarForm(Op)
    {
        //alert('');
        if ($("#cCatastro").val() > 0 && <?=$Op?> != 2)
        {
            alert("El Codigo Catastral ya Existe")
            return false
        }

        if ($("#contmod").val() == 0)
        {
            alert('No se ha Realizado Ninguna Modificacion');
            return false;
        }


        if ($("#estadoservicio").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#estadoservicio"), "Seleccione el Estado de Servicio")
            return false;
        }

        $.ajax({
            url: 'observacion.php',
            type: 'POST',
            async: true,
            data: '',
            success: function (datos) {
                $("#div_lecturas").html(datos)
            }
        })
        objIdx = "guardar";
        // vCatastroSave()
				 $("#DivLecturas").dialog("open");
        return false
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }

    // function vCatastroSave()
    // {
    //     var sector = $("#sector").val();
    //     var manzanas = $("#manzanas").val();
    //     var lote = $("#lote").val();
		//
    //     if (sector != "0" && manzanas != "0" && lote != "")
    //     {
    //         $.ajax({
    //             url: 'vcatastro.php',
    //             type: 'POST',
    //             async: true,
    //             data: 'Op=1&sector=' + sector + '&manzanas=' + manzanas + '&lote=' + lote + '&nroinscripcion=' + $("#nroinscripcion").val(),
    //             dataType: "json",
    //             success: function (datos)
    //             {
    //                 if (datos.resultado == 1)
    //                 {
    //                     $("#tabs").tabs({selected: 0});
		//
    //                     Msj($("#propietario"), 'Existe un usuario con el mismo Catastro : ' + datos.propietario);
		//
    //                     return false;
    //                 }
    //                 else
		// 			{
    //                     $("#DivLecturas").dialog("open");
		// 			}
    //             }
    //         })
    //     }
    // }

		// function vCatastro()
    // {
    //     var sector = $("#sector").val();
    //     var manzanas = $("#manzanas").val();
    //     var lote = $("#lote").val();
		//
    //     if (sector != "0" && manzanas != "0" && lote != "")
    //     {
    //         $.ajax({
    //             url: 'vcatastro.php',
    //             type: 'POST',
    //             async: true,
    //             data: 'Op=1&sector=' + sector + '&manzanas=' + manzanas + '&lote=' + lote + '&nroinscripcion=' + $("#nroinscripcion").val(),
    //             dataType: "json",
    //             success: function (datos)
    //             {
    //                 if (datos.resultado == 1)
    //                 {
    //                     $("#tabs").tabs({selected: 0});
		//
    //                     Msj($("#propietario"), 'Existe un usuario con el mismo Catastro : ' + datos.propietario);
		//
    //                     return false;
    //                 }
    //                 else
		// 			{
    //                     return true;
		// 			}
    //             }
    //         })
    //     }
    // }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">

            					<tbody>

                								<tr>
                    								<td colspan="3" class="TitDetalle">
																			<input type="hidden" name="contmod" id="contmod" value="0" />
                        								<div id="tabs">
													                            <ul>
													                                <li style="font-size:10px"><a href="#tabs-2">Datos del Cliente</a></li>
													                            </ul>
													                            <div id="tabs-2" style="height:370px">
													                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
													                                    <tr>
													                                        <td width="16%" align="right">Nro. Inscripcion</td>
													                                        <td width="3%" align="center">:</td>
													                                        <td width="19%">
													                                            <input type="text" class="inputtext"  id="codantiguo" readonly="readonly" maxlentgth="10" size="10" value="<?=$items['codantiguo'] ?>"/>
													                                            <input type="hidden" class="inputtext"  id="nroinscripcion" name="nroinscripcion" readonly="readonly" maxlentgth="10" size="10" value="<?=$Id ?>"/>
													                                            <input type="hidden" name="codcliente" id="codcliente" value="<?=isset($items["codcliente"]) ? $items["codcliente"] : 0 ?>" />
													                                        <td width="10%" align="right">Sucursal</td>
													                                        <td width="3%" align="center">:</td>
													                                        <td width="49%">
													                                            <input type="text" class="inputtext"  id="sucursal1" name="sucursal" readonly="readonly" maxlentgth="30" size="30" value="<?=$sucursal["descripcion"] ?>"/>
													                                            <input type="hidden" name="cCatastro" id="cCatastro" value="0" />
													                                            <input type="hidden" name="obs" id="obs" value="" />
													                                        </td>
													                                    </tr>
																															<tr>
													                                        <td align="right">Propietario</td>
													                                        <td align="center">:</td>
													                                        <td colspan="4">
													                                            <input readonly="readonly" type="text" class="inputtext"  id="propietario" name="propietario" maxlentgth="200" size="100" value="<?=$items["propietario"] ?>" onkeyup="actualizacion(this, 0)" />
													                                        </td>
													                                    </tr>

													                                    <tr>
													                                        <td align="right">Est. de Servicio</td>
													                                        <td align="center">:</td>
													                                        <td><?php $objMantenimiento->drop_estado_servicio($items["codestadoservicio"],"onchange='actualizacion(this,0);'"); ?></td>
													                                    </tr>
													                                    <tr>
													                                        <td align="right">&nbsp;</td>
													                                        <td align="center">&nbsp;</td>
													                                        <td>&nbsp;</td>
													                                        <td align="left">&nbsp;</td>
													                                        <td align="left">&nbsp;</td>
													                                        <td align="left">&nbsp;</td>
													                                    </tr>
													                                </table>
													                            </div>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>

            </tbody>

        </table>
        <div id="DivLecturas" title="Actualizacion y Modificacion Catastral"  >
            <div id="div_lecturas"></div>
        </div>
    </form>
</div>
<div id="DivTarifas" title="Tarifas" style="display: none;">
    <div id="DivTarifasAcordion">
        <?php
        $sql = "select * FROM facturacion.tarifas WHERE codsuc=".$codsuc." ORDER BY catetar";
        $ConsultaTar = $conexion->query($sql);
        foreach ($ConsultaTar->fetchAll() as $tarifas) {
            ?>
            <h3><?=strtoupper($tarifas["nomtar"]) ?></h3>
            <div style="height: 100px; display: block;">
                <table width="100%" border="0" align="center" cellspacing="0" class="ui-widget" rules="cols">
                    <thead class="ui-widget-header" style="font-size:10px">
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center">Volumen</td>
                            <td align="center">Importe Agua</td>
                            <td align="center">Importe Desague</td>
                            <td align="center">&nbsp;</td>
                        </tr>
                    </thead>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Inicial</td>
                        <td align="right">
                            <label class="inputtext" id="hastarango1" ><?=number_format($tarifas["hastarango1"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmin" ><?=$tarifas["impconsmin"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmindesa"><?=$tarifas["impconsmindesa"] ?></label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango1"], 0) ?>')"></span></td>
                    </tr>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Intermedio </td>
                        <td align="right">
                            <label class="inputtext" id="hastarango2"><?=number_format($tarifas["hastarango2"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmedio"><?=$tarifas["impconsmedio"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmediodesa"><?=$tarifas["impconsmediodesa"] ?> </label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango2"], 0) ?>')"></span></td>
                    </tr>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Final </td>
                        <td align="right">
                            <label class="inputtext" id="hastarango3"><?=number_format($tarifas["hastarango3"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsexec"><?=$tarifas["impconsexec"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsexecdesa"><?=$tarifas["impconsexecdesa"] ?></label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango3"], 0) ?>')"></span></td>
                    </tr>

                </table>
            </div>
    <?php
}
?>
    </div>
</div>
<script>
	setTimeout(BloquearTodo(), 2000);
</script>
