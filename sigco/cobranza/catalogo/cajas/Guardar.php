<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$nrocaja 		= $_POST["nrocaja"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("cajas","0","0");
			$nrocaja = $id[0];
			
			$sql = "INSERT INTO cobranza.cajas(nrocaja, descripcion,estareg) VALUES(:nrocaja, :descripcion, :estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":nrocaja"=>$nrocaja,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update cobranza.cajas set descripcion=:descripcion, estareg=:estareg 
			where nrocaja=:nrocaja";
			$result = $conexion->prepare($sql);
			$result->execute(array(":nrocaja"=>$nrocaja,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update cobranza.cajas set estareg=:estareg
				where nrocaja=:nrocaja";
			$result = $conexion->prepare($sql);
			$result->execute(array(":nrocaja"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
