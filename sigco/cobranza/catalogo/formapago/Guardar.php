<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codformapago 	= $_POST["codformapago"];
	$descripcion 	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	
	$result = $conexion->prepare($sql);
	$result->execute(array(":codformapago"=>$codformapago,":descripcion"=>$descripcion,":estareg"=>$estareg));
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("formapago","0","0");
			$codformapago = $id[0];
			
			$sql = "insert into public.formapago(codformapago,descripcion,estareg) values(:codformapago,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codformapago"=>$codformapago,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.formapago set descripcion=:descripcion, estareg=:estareg 
			where codformapago=:codformapago";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codformapago"=>$codformapago,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.formapago set estareg=:estareg
				where codformapago=:codformapago";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codformapago"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
