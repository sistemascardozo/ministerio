<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsDrop.php");
    
    $objMantenimiento = new clsDrop();
    
    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"])?$_POST["Id"]:'';
    $guardar	= "op=".$Op;
    $codsuc = $_SESSION['IdSucursal'];
    
    if($Id!='')
    {
        $consulta = $conexion->prepare("SELECT * from facturacion.periodofacturacion 
            WHERE codsuc= ? AND nrofacturacion=? ");
        $consulta->execute(array($codsuc,$Id));
        $row = $consulta->fetch();
        
        $cont= $conexion->prepare("SELECT
            Count(intd.nrofacturacion)
            FROM
            facturacion.interesdiario AS intd
            WHERE
            intd.nrofacturacion= ? ");
        $cont ->execute(array($Id));
        $rowss = $cont->fetch();
        $contador= $rowss[0];
        $guardar	= $guardar."&Id2=".$Id;
        
        $sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);
        $empresa = $objMantenimiento->datos_empresa($codsuc);
        $fechaserver = $objMantenimiento->FechaServer();
    }
    
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_facturacion.js" language="JavaScript"></script>
<style type="text/css">
    .button {cursor:pointer;}
</style>
<script type="text/javascript">
    var codsuc = <?=$codsuc?>;
    $(document).ready(function() {
        $(".button").button();
        datos_facturacion('<?=$row["codciclo"]?>', '<?=$row["nrofacturacion"]?>');
    });
    
    function ValidarForm(Op)
    {
        GuardarP(Op);
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }
    function calcularintpor()
    {
        var intvalpor = $("#interespor").val()
        //alert(intval);
        if(intvalpor=="")
        {
            intvalpor=0;
        }
        
        var cal= parseFloat((intvalpor)/100);
            cal= cal.toFixed(4);
        $("#interesval").val(cal)
    }
    
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>

                <tr>
                    <td colspan="5" class="TitDetalle" style="padding:4px">
                        <fieldset class="fieldset">
                            <legend class="legend ui-state-default ui-corner-all">Datos de la Empresa</legend>
                            
                            <table id="table-cab" width="620px" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td width="100" align="right">Empresa</td>
                                    <td width="30" align="center">:</td>
                                    <td colspan="4">
                                        <textarea name="empresa" id="empresa" cols="50" rows="1" readonly="readonly" class="inputtext"><?=$empresa["razonsocial"] ?></textarea>                                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Ciclo</td>
                                    <td align="center">:</td>
                                    <td colspan="4">
                                        <?php $objMantenimiento->drop_ciclos($codsuc, $row['codciclo'], "onchange='datos_facturacion(this.value);'"); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">A&ntilde;o</td>
                                    <td align="center">:</td>
                                    <td width="70"><input type="text" name="anio" id="anio" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:45px;" /></td>
                                    <td width="70" align="right">Mes</td>
                                    <td width="30" align="center">:</td>
                                    <td>
                                        <input type="text" name="mes" id="mes" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:120px;"/>
                                        <input type="hidden" name="mes_num" id="mes_num" value="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Facturacion</td>
                                    <td align="center">:</td>
                                    <td>
                                        <input type="text" name="nrofacturacion" id="nrofacturacion" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:40px;"/>
                                    </td>
                                    <td align="right">Fecha</td>
                                    <td align="center">:</td>
                                    <td>
                                        <input type="text" name="fechafacturacion" id="fechafacturacion" readonly="readonly" class="inputtext" maxlength="10" value="<?=$fechaserver?>" style="width:80px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Interes</td>
                                    <td align="center">:</td>
                                    <td>
                                        <input type="text" name="intfact" id="intfact" class="inputtext" size="10" maxlength="10" value="" />
                                    </td>
                                    <td align="right">Promedio Interes</td>
                                    <td align="center">:</td>
                                    <td>
                                        <input type="text" name="promedioint" id="promedioint" class="inputtext" size="10" maxlength="10" readonly />
                                        &nbsp;&nbsp;&nbsp;
                                        <a href="javascript:" id="Promediar" class="button"  title="Promediar Insteres">
                                            <span class="box-boton boton-new">Promediar</span>
                                        </a>
                                    </td>

                                </tr>                                                           

                            </table>
                        </fieldset>      
                    </td>
                </tr>
                
                <tr style="padding:4px">
                    
                    <td colspan="5" class="TitDetalle">
                        <table id="table-per" width="100%" border="0" style="border:1px #000000 dashed" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="TitDetalle">
                                    <legend class="legend ui-state-default ui-corner-all">DETALLE</legend>
                                </td>
                            </tr>
                            <tr>
                              <td align="center">
                              <table width="580" border="0" cellspacing="1" cellpadding="1">
                                <tr style="height:23px">
                                  <td width="200" align="center">&nbsp;</td>
                                  <td width="90" align="center" class="ui-widget-header">Insteres (%)</td>
                                  <td width="90" align="center" class="ui-widget-header">Insteres (Valor)</td>
                                  <td width="110" align="center" class="ui-widget-header">Fecha</td>
                                  <td width="190">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">&nbsp;</td>
                                  <td align="center"><input type="text" name="interespor" id="interespor" onchange="calcularintpor()" class="inputtext" size="10" maxlength="10" value=".00" /></td>
                                  <td align="center"><input type="text" name="interesval" id="interesval" onchange="calcularint();" class="inputtext" size="10" maxlength="10" value=".00"/></td>
                                  <td align="center"><input type="text" name="fechadia" id="fechadia" value="<?=$fechaserver ?>" class="inputtext" style="text-align:center; width:80px;" maxlength="10"  /></td>
                                  <td align="right"><a href="javascript:" id="addDetail" class="button"  title="Agregar Detalle"><span class="box-boton boton-new">Agregar Detalle</span></a></td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <table id="table-detalle" class="ui-widget ui-widget-content" style="margin: 0 auto; width:340px" border="1">
                                        <thead class="ui-widget ui-widget-content">
                                            <tr class="ui-widget-header" style="height:23px">
                                                <th align="center" width="200px">Insteres (%)</th>
                                                <th align="center" width="200px">Insteres (Valor)</th>                                                
                                                <th align="center" width="140px">Fecha</th>
                                                <th width="30px">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
	if ($contador != 0)
	{
		$SelectD = "SELECT nrofacturacion,
                                                    fecha, 
                                                    tasainteres, valtasainteres
                                                    FROM facturacion.interesdiario 
                                                WHERE nrofacturacion = ? ";
                                                $ConsultaD= $conexion->prepare($SelectD);
                                                $ConsultaD->execute(array($Id));
                                                $result = $ConsultaD->fetchAll();

                                                foreach ($result as $rowD) {
                                                    $i ++;
?>
												<tr id="<?php echo $i;?>" class="tr-detalle" style="height: 20px">
													<td align="right" style="padding-right:5px;"><?php echo number_format($rowD["tasainteres"], 4);?><input type="hidden" name="interesporcentaje[]" value="<?php echo $rowD['tasainteres'];?>" /></td>
													<td align="right" style="padding-right:5px;"><?php echo $rowD["valtasainteres"]?><input type="hidden" name="interesvalor[]" value="<?php echo $rowD['valtasainteres'];?>" /></td>
													<td align="center">
                                                    	<input type="hidden" name="fecha[]" value= "<?php echo $objMantenimiento->DecFecha($rowD['fecha']);?>" />
														<label class="fecha"><?php echo $objMantenimiento->DecFecha($rowD['fecha']);?></label>
													</td>
													<td align="center" >
                                                    	<img style="border:none; cursor: pointer" src="../../../../images/iconos/cancel.png" title="Quitar" class="boton-delete" href="#" />
													</td>
												</tr>
<?php
                                                }
                                            }
?>    
                                        </tbody>
                                        <tfoot>
                                            <tr>               
                                                <td bgcolor="#6EB6D5" colspan="4">&nbsp;</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                            
                    </td>
                </tr>
                <tr>
                    <td width="107" class="TitDetalle">&nbsp;</td>
                    <td width="573" colspan="4" class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
