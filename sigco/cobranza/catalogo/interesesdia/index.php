<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	$TituloVentana = "INTERES DIARIO";
	
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	$FormatoGrilla = array ();
	
	$Sql = "SELECT
    p.nrofacturacion,
    p.anio,
    case p.mes
        when '1' then 'ENERO' 
        when '2' then 'FEBRERO' 
        when '3' then 'MARZO' 
        when '4' then 'ABRIL' 
        when '5' then 'MAYO' 
        when '6' then 'JUNIO' 
        when '7' then 'JULIO' 
        when '8' then 'AGOSTO' 
        when '9' then 'SETIEMBRE' 
        when '10' then 'OCTUBRE' 
        when '11' then 'NOVIEMBRE' 
        when '12' then 'DICIEMBRE'
    else '' end,
    p.tasainteres,    
    case p.facturacion
        when 0 then '<p style=\"color:red;font-weight: bold;\">FALTA FACTURAR</p>'
        when 1 then '<p style=\"color:green;font-weight: bold;\">FACTURADO</p>'        
    else '' end, 1    
    
    FROM
    facturacion.periodofacturacion AS p ";
  
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'p.nrofacturacion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Nro. Facturaci&oacute;n', 'T2'=>'Año', 'T3'=>'Mes', 'T4'=>'Tasa Interes', 'T5'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'center', 'A4'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'80', 'W2'=>'60', 'W3'=>'130', 'W4'=>'120', 'W5'=>'80', 'W6'=>'60');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 600;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " and (p.codemp=1 and p.codsuc=".$codsuc.") ORDER BY p.nrofacturacion DESC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
            'NB'=>'1',          //Número de Botones a agregar
            'BtnId1'=>'BtnModificar',   //Nombre del Boton
            'BtnI1'=>'modificar.png',   //Imagen a mostrar
            'Btn1'=>'Detalle',       //Titulo del Botón
            'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
            'BtnCI1'=>'6',  //Item a Comparar
            'BtnCV1'=>'1',    //Valor de comparación
        );
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 820, 520);
  Pie();
  
?>
<script type="text/javascript">
  $("#BtnNuevoB").remove();

</script>
<?php CuerpoInferior();?>

