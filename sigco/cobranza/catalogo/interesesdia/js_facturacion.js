// JavaScript Document
$(document).ready(function() {
    
    $("#table-per").on('click','#addDetail',function(){
        addDetail();
    });
    
    $("#table-cab").on('click','#Promediar',function(){
        
        var nro= $("#nrofacturacion").val()
        PromediarInt(nro);
    });
    $("#table-detalle").on('click', '.boton-delete',function(){var v = $(this).parent().parent().remove();})
    
    $("#DivPorcentaje").dialog({
        autoOpen: false,
        height: 500,
        width: 400,
        modal: true,
        resizable: false,
        buttons: {
            "Aceptar": function() {
                grabar_porcentaje();
            },
            "Cancelar": function() {
                $(this).dialog("close");
            }
        },
        close: function() {

        }
    });
    
    $( "#fechadia" ).datepicker({
        showOn: 'button',
            direction: 'up',
            buttonImage: '../../../../images/iconos/calendar.png',
            buttonImageOnly: true,
            showOn: 'both',
                    showButtonPanel: true,
            beforeShow: function(input, inst)
            {
                inst.dpDiv.css({marginTop: (input.offsetHeight) - 20 + 'px', marginLeft: (input.offsetWidth) - 90 + 'px'});
            }
    });
});

    function datos_facturacion(codciclo,nrofact)
    {
        $.ajax({
            url: '../../../../ajax/periodo_fact.php',
            type: 'POST',
            async: true,
            data: 'codciclo='+codciclo+'&codsuc='+codsuc+'&nrofact='+nrofact,
            success: function(datos) {
                var r = datos.split("|")

                $("#anio").val(r[1])
                $("#mes").val(r[2])
                $("#mes_num").val(r[3])
                $("#nrofacturacion").val(r[0])            
                $("#control_cierre_cobranza").val(r[4])
                $("#control_cierre_lecturas").val(r[5])
                $("#control_porcentaje_mes").val(r[6])
                //var int= r[6]
                $("#intfact").val(r[6])
               $("#promedioint").val(r[7])           
            }
        })
    }
    
    function PromediarInt(nro)
    {
        $.ajax({
            url: '../../../../ajax/promedio_interes.php',
            type: 'POST',
            async: true,
            data: 'nro='+nro+'&codsuc='+codsuc,
            success: function(datos) {
                var r = datos.split("|")
                $("#promedioint").val(r[0])                
            }
        })
    }
    
    function addDetail()
    {

        if($("#interesval").val()!='' && $("#interespor").val()!='' )
        {     
            
            var interespor = $("#interespor").val();
            var interesval= $("#interesval").val();
            //interesval= interesval.toFixed(4);

            var fecha= $("#fechadia").val();

            var j=parseInt($("#table-detalle tbody tr").length);
            
            //alert(j);
            for(var i1=1; i1<=j; i1++)
            {   
                Codigo = $("#table-detalle tbody tr#"+i1+" label.fecha").text();
                //alert(Codigo);
                if (Codigo==fecha)
                    {
                        alert('La fecha ya se ha agregado')
			return false;
                    }
            }
            var c=j+1;

            var html = '';
            html += '<tr id="'+c+'" class="tr-detalle" style="height: 20px">';
            html += '<td align="right">'+interespor+'<input type="hidden" name="interesporcentaje[]" value="'+interespor+'" /></td>';
            html += '<td align="right">'+interesval+'<input type="hidden" name="interesvalor[]" value="'+interesval+'" /></td>';            
            html += '<td align="center"><label class="fecha">'+fecha+'</label><input type="hidden" name="fecha[]" value="'+fecha+'" /></td>';
            html += '<td align="center"><img style="border:none; cursor: pointer" ';
            html+=' src="../../../../images/iconos/cancel.png" class="boton-delete" href="#" title="Quitar" /></td>'; 
            html += '</tr>';    
            $("#table-detalle").find('tbody').append(html);
        }
        else
            {
                alert("Digite Los valores (%)")
                return false;
            }
    }
    
function cierre_cobranza()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }

    if ($("#control_cierre_cobranza").val() == 1)
    {
        Msj($("#cierrecobranza"), "El Cierre de Cobranza ya se ha Realizado...Proceda al Siguiente Paso")
        return false
    }

    var r = confirm('Desea Realizar el Cierre de Cobranza?')
    if (r == false)
    {
        return 	true
    }

    window.parent.blokear_pantalla("Se Esta Realizando el Cierre de Cobranza Espere por Favor")

    $.ajax({
        url: 'calculo/cobranza.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc,
        success: function(datos) {
            var r = datos.split("|")

            window.parent.establecer_texto(r[0], r[1])

            $("#control_cierre_cobranza").val(r[2])
        }
    })

}
function cierre_lecturas()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }

    if ($("#control_cierre_lecturas").val() == 1)
    {
        Msj($("#cierrelecturas"), "El Cierre de Lecturas ya se ha Realizado...Proceda al Siguiente Paso")
        return false
    }

    var r = confirm('Desea Realizar el Cierre de Lecturas?')
    if (r == false)
    {
        return 	true
    }

    window.parent.blokear_pantalla("Se Esta Realizando el Cierre de Lecturas. Espere por Favor!!")

    $.ajax({
        url: 'calculo/lecturas.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc,
        success: function(datos) {
            var r = datos.split("|")

            window.parent.establecer_texto(r[0], r[1])

            $("#control_cierre_lecturas").val(r[2])
        }
    })

}
function cargar_porcentaje()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }
    if ($("#control_cierre_cobranza").val() == 0)
    {
        Msj($("#porcinteres"), "Para Grabar el Porcentaje de Interes es Necesario que realize el Cierre de Cobranza")
        return false
    }
    if ($("#control_cierre_lecturas").val() == 0)
    {
        Msj($("#porcinteres"), "Para Grabar el Porcentaje de Interes es Necesario que realize el Cierre de Lecturas")
        return false
    }

    cargar_pantalla_porcentaje($("#ciclo").val())
    $("#DivPorcentaje").dialog("open");
}
    
function grabar_porcentaje()
{
    if ($("#porcentaje").val() == 0 || $("#porcentaje").val() == "")
    {
        Msj($("#porcentaje"), "El Valor del Porcentaje no es Valido!!!")
        return
    }

    $.ajax({
        url: 'calculo/guardar_porcentaje.php',
        type: 'POST',
        async: true,
        data: 'nrofacturacion=' + $("#nrofacturacion").val() + '&codsuc=' + codsuc + '&codciclo=' + $("#ciclo").val() + '&porcentaje=' + $("#porcentaje").val(),
        success: function(datos) {
            var r = datos.split("|")

            $("#control_porcentaje_mes").val(r[2])
            $("#DivPorcentaje").dialog("close");
            Msj($("#porcinteres"), r[1])
        }
    })
}
function generar_facturacion()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }

    if ($("#control_cierre_cobranza").val() == 0)
    {
        Msj($("#cierrecobranza"), "Para Generar el Proceso de Facturacion realize el Cierre de Cobranza")
        return false
    }
    if ($("#control_cierre_lecturas").val() == 0)
    {
        Msj($("#cierrelecturas"), "Para Generar el Proceso de Facturacion realize el Cierre de Lecturas")
        return false
    }
    if ($("#control_porcentaje_mes").val() == 0)
    {
        Msj($("#porcinteres"), "El Porcentaje de Interes Ingresado no es Valido!!!")
        return false
    }

    var r = confirm('Desea Realizar el Proceso de Facturacion')
    if (r == false)
    {
        return 	true
    }

    window.parent.blokear_pantalla("Se Esta Realizando el Proceso de facturacion. Espere por Favor!!")

    $.ajax({
        url: 'calculo/facturacion.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc + '&porcentaje=' + $("#control_porcentaje_mes").val() +
                '&nrofacturacion=' + $("#nrofacturacion").val() + '&factura_alcantarillado=' + $("#factura_alcantarillado").val() +
                '&mes=' + $("#mes_num").val() + '&anio=' + $("#anio").val(),
        success: function(datos) {
            var r = datos.split("|")

            window.parent.establecer_texto(r[0], r[1])

        }
    })
}
function actualizar_saldos()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }

    var r = confirm('Desea Realizar el Proceso de Generación de Históricos')
    if (r == false)
    {
        return 	true
    }

    window.parent.blokear_pantalla("Se Esta Generando los datos Hist&oacute;ricos. Espere por Favor!!!")

    $.ajax({
        url: 'calculo/actualizasaldo.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc,
        success: function(datos) {
            var r = datos.split("|")

            window.parent.establecer_texto(r[0], r[1])

        }
    })
}
