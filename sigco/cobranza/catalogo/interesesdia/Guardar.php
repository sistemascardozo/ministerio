<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include('../../../../objetos/clsFunciones.php');

    
    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();
    $codsuc       = $_SESSION["IdSucursal"];
    $codemp       = 1;
    $nrofacturacion = $_POST["nrofacturacion"];
    $ciclo          = $_POST["ciclo"];
    $intfact        = $_POST["intfact"];
    $codusu          = $_SESSION['id_user'];
    $promedioint  = $_POST["promedioint"];    

    $conexion->beginTransaction();

    switch ($Op) {
        case 1:

            if($nrofacturacion!='')
            {
                $SqlP = "DELETE FROM facturacion.interesdiario WHERE nrofacturacion = ? AND codemp= ? AND codsuc= ? ";                  
                $result = $conexion->prepare($SqlP); 
                $result->execute(array($nrofacturacion, $codemp, $codsuc));
                
            }  

            if($promedioint== '' || $promedioint== 0)
            {
                $promedioint=0;
            }
            else{ $intfact= $promedioint; }

            $upd= "UPDATE facturacion.periodofacturacion SET tasainteres= :tasainteres, tasapromint= :tasapromint
            WHERE codemp= :codemp AND codsuc= :codsuc AND nrofacturacion= :nrofacturacion AND codciclo= :codciclo ";
            $res = $conexion->prepare($upd);
            $res->execute(array(":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nrofacturacion"=>$nrofacturacion,
                ":tasainteres"=>$intfact,":codciclo"=>$ciclo, ":tasapromint"=>$promedioint));

            if ($res->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error UPDATE periodofacturacion";
                die(2);
            }

            if($_POST['interesporcentaje']!='')
            {

                foreach ($_POST['interesporcentaje'] as $key=>$value) {
                    
                    $id  = "SELECT max(codintdiario) FROM facturacion.interesdiario WHERE codsuc = ".$codsuc;
                    $consulta = $conexion->query($id);
                    $row = $consulta->fetch();
                    $codintdiario   = $row[0];

                    if ($codintdiario == null || $codintdiario == '')
                        { $codintdiario=1;}                        
                        else
                            {
                                $codintdiario ++;
                            }                    
                   

                    $interesval =$_POST["interesvalor"][$key];
                    $fecha      =$objFunciones->CodFecha($_POST["fecha"][$key]);
                    $fechareg   = date('Y-m-d');
                    
                    $sql = "INSERT INTO facturacion.interesdiario( codintdiario, codemp, codsuc, nrofacturacion, 
                        fechareg, fecha, tasainteres, valtasainteres, codusu)
                        VALUES (:codintdiario, :codemp, :codsuc, :nrofacturacion, :fechareg, :fecha, 
                        :tasainteres, :valtasainteres, :codusu) ";
                    $result = $conexion->prepare($sql);
                    $result->execute(array(":codemp"=>$codemp, 
                        ":codsuc"=>$codsuc,
                        ":codintdiario"=>$codintdiario,
                        ":nrofacturacion"=>$nrofacturacion,
                        ":fechareg"=>$fechareg,
                        ":fecha"=>$fecha,
                        ":tasainteres"=>$value,
                        ":valtasainteres"=>$interesval,
                        ":codusu"=>$codusu ));
                    
                }
            }

            break;

    }

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
    
?>
