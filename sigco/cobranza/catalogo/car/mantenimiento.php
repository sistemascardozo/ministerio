<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");

    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
    $codsuc = $_SESSION["IdSucursal"];
    $guardar = "op=" . $Op;
    $IdUbigeo     ='250101';
    //$objMantenimiento = new clsMantenimiento();
    $objMantenimiento 	= new clsDrop();
    if ($Id != '') {
        $consulta = $conexion->prepare($objMantenimiento->Sentencia("car") . " where car=? and codsuc=?");
        $consulta->execute(array($Id, $codsuc));
        $row = $consulta->fetch();
        $IdUbigeo = $row["codubigeo"];
        $guardar = $guardar . "&Id2=" . $Id;
    }
    
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    var urldir = '<?php echo $_SESSION['urldir'];?>';
	
    $(document).ready(function() {
        cargar_provincia('<?=substr($IdUbigeo, 0, 2)."0000"?>', '<?=$IdUbigeo?>')
        cargar_distrito('<?=substr($IdUbigeo, 0, 4)."00"?>', '<?=$IdUbigeo?>');
    });
    function ValidarForm(Op)
    {
        if ($("#Nombre").val() == '')
        {
            alert('La Descripcion del Centro Autorizado de Recaudacion no puede ser NULo');
            return false;
        }
        GuardarP(Op);
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td width="30" class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
                <tr>
                    <td class="TitDetalle">Id</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                  <input name="car" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/></td>
                </tr>
                <tr>
                    <td class="TitDetalle">Descripcion</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <input class="inputtext" name="descripcion" type="text" id="Nombre" maxlength="200" value="<?=isset($row[1])?$row[1]:''?>" style="width:400px;"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Zona</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <select name="codzona" id="codzona" style="width:228px">
                            <?php
                                $Sql = "SELECT * FROM admin.zonas WHERE estareg = 1 AND codsuc=".$codsuc;
                                $Consulta = $conexion->query($Sql);
                                foreach ($Consulta->fetchAll() as $row1) {
                                    $Sel = "";
                                    if ($row["codzona"] == $row1["codzona"])
                                        $Sel = 'selected="selected"';
                                    ?>
                                    <option value="<?php echo $row1["codzona"]; ?>" <?= $Sel ?>  ><?php echo $row1["descripcion"]; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Departamento</td>
                    <td align="center" class="TitDetalle">:</td>                    
                    <td class="CampoDetalle">
                        <?php $objMantenimiento->drop_ubigeo("departamento",".: Seleccione el Departamento :.",
                            " where codubigeo like ? order by codubigeo",array("%0000"),
                            "onchange='cargar_provincia(this.value);'",substr($IdUbigeo,0,2)."0000"); 
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Provincia</td>
                    <td align="center" class="CampoDetalle">:</td>
                    <td class="CampoDetalle" >
                        <div id="div_provincia">
                            <select id="provincia" name="provincia" style="width:220px" class="select">
                                <option value="0">--Seleccione la Provincia--</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Distrito</td>
                    <td align="center" class="TitDetalle">:</td>                    
                    <td class="CampoDetalle">
                        <div id="div_distrito">
                            <select id="distrito" name="distrito" style="width:220px" class="select">
                                <option value="0">--Seleccione un Distrito--</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Estado</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <?php include("../../../../include/estareg.php"); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    $("#Nombre").focus();
</script>