<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();

    $codsuc         = $_SESSION["IdSucursal"];
    $car            = $_POST["car"];
    $descripcion    = strtoupper($_POST["descripcion"]);
    $estareg        = $_POST["estareg"];
    $codubigeo      = $_POST["distrito"];
    $codzona        = $_POST["codzona"];

    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("car", "0", "0");
            $car = $id[0];

            $sql = "insert into cobranza.car(car,descripcion,estareg,codemp,codsuc, codubigeo,codzona)
                    values(:car,:descripcion,:estareg,:codemp,:codsuc,:codubigeo,:codzona)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":car" => $car, ":descripcion" => $descripcion, ":estareg" => $estareg, 
                ":codemp" => 1, ":codsuc" => $codsuc,":codubigeo"=>$codubigeo,":codzona"=>$codzona));
            break;
        case 1:
            $sql = "update cobranza.car 
                SET descripcion=:descripcion,
                estareg=:estareg,
                codubigeo= :codubigeo,
                codzona = :codzona
                where car=:car and codemp=:codemp and codsuc=:codsuc";
            $result = $conexion->prepare($sql);
            $result->execute(array(":car" => $car, ":descripcion" => $descripcion, ":estareg" => $estareg,
                ":codemp" => 1, ":codsuc" => $codsuc,":codubigeo"=>$codubigeo,":codzona"=>$codzona));
            break;
        case 2:case 3:
            $sql = "update cobranza.car set estareg=:estareg
                where car=:car and codemp=:codemp and codsuc=:codsuc";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codemp" => 1, ":codsuc" => $codsuc, ":car" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
