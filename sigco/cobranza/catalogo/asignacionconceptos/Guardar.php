<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$cont 			= $_POST["cont"];
	$codsuc			= $_SESSION["IdSucursal"];
	$codcategoria 	= $_POST["codcategoria"];
	
	$sqlDel 		= "delete from cobranza.catcobranza_conceptos where codsuc=? and codcategoria=?";
	$result 	= $conexion->prepare($sqlDel);
	$result->execute(array($codsuc,$codcategoria));
	
	for($i=1;$i<=$cont;$i++)
	{
		if(isset($_POST["codconcepto".$i]))
		{
			$sqlI = "insert into cobranza.catcobranza_conceptos(codcategoria,codconcepto,codemp,codsuc)
					values(:codcategoria,:codconcepto,:codemp,:codsuc)";
					
			$result = $conexion->prepare($sqlI);
			$result->execute(array(":codcategoria"=>$codcategoria,
									  ":codconcepto"=>$_POST["codconcepto".$i],
									  ":codemp"=>1,
									  ":codsuc"=>$codsuc));
		}
	}
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
