<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	 	
	include("../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc		= $_SESSION["IdSucursal"];
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsMantenimiento();
	
	$sql = "select descripcion from cobranza.categoriacobranza where codcategoria=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($Id));
	$items = $consulta->fetch();

?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var cont 	= 0; 
	var nDest 	= 0;
	var nDestC 	= 0;
	
	function ValidarForm(Op)
	{
		if (document.form1.cont.value == 0)
		{
			alert('No hay Ningun Usuario para asignar su caja');
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function buscar_conceptos()
	{
		AbrirPopupBusqueda('../../../facturacion/catalogo/conceptos/?Op=5',900,600)
	}
	function Recibir(id,concepto)
	{
		$("#codconcepto").val(id)
		datos_conceptos(id)
	}
	function datos_conceptos(id)
	{
		 $.ajax({
			 url:"../../../../ajax/conceptos.php",
			 type:'POST',
			 async:true,
			 data:'codconcepto='+id+'&codsuc=<?=$codsuc?>',
			 success:function(datos){
				$("#concepto").val(datos)
			 }
		}) 
	}
	function Agregar()
	{
		var codconcepto  = $("#codconcepto").val()
		var concepto   	 = $("#concepto").val()
        
		if(codconcepto=='')
		{
			alert('Seleccione un Concepto')
			return
		}
		
		var Dest = 1;
		var x ;

		for (x = 1; x <= $("#cont").val(); x++) 
		{ 	
			try
			{
				if($("#codconcepto"+x).val()==codconcepto)
				{
					alert('El Concepto ya Se Encuentra Agregado en la Lista')
					return
				}
			}catch(exp)
			{
				
			}
		} 

		cont 	= cont + 1;
		
		$("#tbusuarios tbody").append("<tr id='tr_"+cont+"' >"+
									  "<td align='center'><input type='hidden' id='codconcepto"+cont+"' name='codconcepto"+cont+"' value='"+codconcepto+"' />"+codconcepto+"</td>"+
									  "<td>"+concepto+"</td>"+
									  "<td align='center'> <span class='icono-icon-trash'  onClick='remove_item("+cont+")' ></span> </td>"+
									  "</tr>");
		
		$("#cont").val(cont)
	}
	function remove_item(id)
	{	
		$("#tr_"+id).remove()
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
        <td class="TitDetalle">Id</td>
        <td width="30" align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
		<input name="codcategoria" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
		<input type="hidden" name="cont" id="cont" value="0" /></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="0form1_descripcion" type="text" id="Nombre" maxlength="200" readonly="readonly" value="<?=$items["descripcion"]?>" style="width:400px;"/></td>
	  </tr>
	<tr style="height:5px">
	  <td colspan="3" class="TitDetalle"></td>
	  </tr>
	<tr style="background-color:#0099CC; border: 1 #000000 solid;">
	  <td colspan="3" class="TitDetalle" style="padding:4px; color: white; font-weight: bold">Agregar Conceptos de Facturacion </td>
	  </tr>
	<tr style="height:5px">
	  <td colspan="3" class="TitDetalle"></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Concepto</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><label>
	    <input type="text" name="codconcepto" id="codconcepto" class="inputtext" size="10" maxlength="10" readonly="readonly" />
	    
	    <span class="MljSoft-icon-buscar" title="Buscar Conceptos" onclick="buscar_conceptos();"></span>
	    <input type="text" name="concepto" id="concepto" class="inputtext" maxlength="60" readonly="readonly" style="width:350px;" />
	  
	      <span class="icono-icon-add" title="Agregar Concepto" onclick="Agregar();"></span>
	</label></td>
	  </tr>
	<tr>
	  <td colspan="3" class="TitDetalle" style="padding:4px">
	     <table border="1" align="center" cellspacing="0" class="ui-widget"  width="99%" id="tbusuarios" rules="all" >

	      <tr class="ui-widget-header">
	        <td width="80" align="center" >Codigo</td>
	        <td align="center" >Concepto</td>
	        <td align="center" >&nbsp;</td>
	        </tr>
	      <?php
			$count = 0;
			
			$sqlD  = "select cobr.codconcepto,conc.descripcion ";
			$sqlD .= "from cobranza.catcobranza_conceptos as cobr ";
			$sqlD .= "inner join facturacion.conceptos as conc on(cobr.codemp=conc.codemp and cobr.codsuc=conc.codsuc and cobr.codconcepto=conc.codconcepto) ";
			$sqlD .= "where cobr.codcategoria=? and cobr.codsuc=?";
			
			$consultaD = $conexion->prepare($sqlD);
			$consultaD->execute(array($Id,$codsuc));
			$itemsD = $consultaD->fetchAll();

			foreach($itemsD as $rowD)
			{
				$count = $count + 1;
		?> 
	      <tr id="tr_<?=$count?>">
	        <td align="center" >
	          <input type="hidden" name="codconcepto<?=$count?>" id="codconcepto<?=$count?>" value="<?=$rowD[0]?>" />
	          <?=$rowD[0]?>
	          </td>
	        <td align="left" style="padding-left:5px;" >
	          <?=strtoupper($rowD[1])?>
	          </td>
	        <td align="center" >
	        	<span class='icono-icon-trash' onClick='remove_item(<?=$count?>);' ></span> 
	          </td>
	        </tr>
	      <?php } ?>
	      </table>
	    </td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>
    </table>
 </form>
</div>
<script>
  	var cont 	= <?=$count?>;
	$("#cont").val(<?=$count?>)
</script>