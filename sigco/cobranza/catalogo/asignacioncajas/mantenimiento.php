<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc		= $_SESSION["IdSucursal"];
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsMantenimiento();
	
	$consulta = $conexion->prepare($objMantenimiento->Sentencia("cajas")." where nrocaja=?");
	$consulta->execute(array($Id));
	$items = $consulta->fetch();
	
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var cont 	= 0; 
	
	function ValidarForm(Op)
	{
		if (document.form1.cont.value == 0)
		{
			alert('No hay Ningun Usuario para asignar su caja');
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function buscar_usuario()
	{
		AbrirPopupBusqueda('../../../../seguridad/usuarios/?Op=5', 920, 450)
	}
	function Recibir(id,nombre)
	{
		$("#codusuario").val(id)
		$("#usuario").val(nombre)
		validausuario(id)
	}
	
	function agregar_row()
	{
		var codusuario  = $("#codusuario").val()
		var nombres   	= $("#usuario").val()
        
		if(codusuario=='')
		{
			alert('Seleccione un Usuario')
			return
		}
		if($("#valida").val()>=1)
		{
			alert('El Usuario ya Tiene un caja asignado')
			return
		}
		
		var Dest = 1;
		var x ;

		for (x = 1; x <= $("#cont").val(); x++) 
		{ 	
			try
			{
				if($("#idusuario"+x).val()==codusuario)
				{
					alert('El Usuario ya se encuentra agregado a la Caja')
					return
				}
			}catch(exp)
			{
				
			}
		} 
		
		
		cont++;
		
		$("#tbusuarios tbody").append("<tr style='background-color:#FFFFFF;' >"+
									  "<td  align='center'><input type='hidden' id='idusuario"+cont+"' name='idusuario"+cont+"' value='"+codusuario+"' />"+codusuario+"</td>"+
									  "<td>"+nombres+"</td>"+
									  "<td align='center'><span class='icono-icon-trash' onClick='remote_items("+cont+")' ></span> </td>"+
									  "</tr>"
									  )
		
		$("#cont").val(cont)
	
	}
	function remote_items(id)
	{
		$("#tr_"+id).remove()
	
	}
	function validausuario(id)
	{
		$.ajax({
			 url:'valida_asignaciones.php',
			 type:'POST',
			 async:true,
			 data:'idusuario='+id+'&codsuc=<?=$codsuc?>',
			 success:function(datos){
				$("#valida").val(datos)
			 }
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
        <td class="TitDetalle">Id</td>
        <td width="30" align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
		<input name="nrocaja" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
		<input type="hidden" name="cont" id="cont" value="0" />
		<input type="hidden" name="valida" id="valida" value="0" /></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Nombre" size="70" maxlength="200" readonly="readonly" value="<?=$items["descripcion"]?>"/></td>
	  </tr>
	<tr style="height:5px">
	  <td colspan="3" class="TitDetalle"></td>
	  </tr>
	<tr style="background-color:#0099CC; border: 1 #000000 solid;">
	  <td colspan="3" class="TitDetalle" style="padding:4px; color: white; font-weight: bold">Agregue los Usuarios </td>
	  </tr>
	<tr style="height:5px">
	  <td colspan="3" class="TitDetalle"></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Usuario</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><label>
	    <input type="text" name="codusuario" id="codusuario" class="inputtext" size="10" maxlength="10" readonly="readonly" />
	    <img src="../../../../images/iconos/buscar.png" width="16" height="16" style="cursor:pointer" alt="Buscar Usuario" onclick="buscar_usuario();" />
	    <input type="text" name="usuario" id="usuario" class="inputtext" maxlength="60" readonly="readonly" style="width:400px;" />
	    <img src="../../../../images/iconos/add.png" width="16" height="16" style="cursor:pointer" alt="Agregar Usuario a Caja" onclick="agregar_row();" /></label></td>
	  </tr>
	 <tr>
	   <td colspan="3" class="TitDetalle" style="padding:4px">
	   <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbusuarios" rules="all" >
			<thead class="ui-widget-header" >
         <tr class="CabIndex">
           <td width="80" align="center" >Codigo</td>
           <td align="center" >Usuario</td>
           <td align="center" >&nbsp;</td>
         </tr>
     </thead>
     <tbody>
		<?php
			$count = 0;
			
			$sqlD  = "select cu.codusu,usu.nombres ";
			$sqlD .= "from cobranza.cajasxusuario as cu ";
			$sqlD .= "inner join seguridad.usuarios as usu on(cu.codusu=usu.codusu) ";
			$sqlD .= "where cu.nrocaja=? and cu.codemp=1 and cu.codsuc=?";
			
			$consultaD = $conexion->prepare($sqlD);
			$consultaD->execute(array($Id,$codsuc));
			$itemsD = $consultaD->fetchAll();
		
			foreach($itemsD as $rowD)
			{
				$count = $count + 1;
		?> 
			 <tr id="tr_<?=$count?>">
			   <td align="center" >
			   	<input type="hidden" name="idusuario<?=$count?>" id="idusuario<?=$count?>" value="<?=$rowD[0]?>" />
			   	<?=$rowD[0]?>
			   </td>
			   <td align="left" style="padding-left:5px;" >
			   	<?=strtoupper($rowD[1])?>
			   </td>
			   <td align="center" >
			   	<span class="icono-icon-trash" onClick='remote_items(<?=$count?>)' ></span>
			   </td>
			 </tr>
		 <?php } ?>
		 </tbody>
       </table>
	   </td>
	   </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>
    </table>
 </form>
</div>
<script>  
  	var cont 	= <?=$count?>;
	$("#cont").val(<?=$count?>)
</script>