<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codsuc		= $_SESSION["IdSucursal"];
	$cont		= $_POST["cont"];
	$nrocaja 	= $_POST["nrocaja"];
	
	$sqlD = "delete from cobranza.cajasxusuario where nrocaja=? and codemp=1 and codsuc=?";
	$result = $conexion->prepare($sqlD);
	$result->execute(array($nrocaja,$codsuc));
	
	for($i=1;$i<=$cont;$i++)
	{
		if(isset($_POST["idusuario".$i]))
		{
			$sqlI = "insert into cobranza.cajasxusuario(nrocaja,codusu,codemp,codsuc) values(:nrocaja,:codusu,:codemp,:codsuc)";
			$result = $conexion->prepare($sqlI);
			$result->execute(array(":nrocaja"=>$nrocaja,
									  ":codusu"=>$_POST["idusuario".$i],
									  ":codemp"=>1,
									  ":codsuc"=>$codsuc));
		}	
	}
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
