<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsDrop();

	if($Id!='')
	{
		$consulta = $conexion->prepare("select * from cobranza.categoriacobranza where codcategoria=?");
		$consulta->execute(array($Id));
		$row = $consulta->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if ($("#Nombre").val() == '')
		{
			alert('La Descripcion de la Categoria no puede ser NULO');
			return false;
		}
		if($("#tipodeuda").val()==0)
		{
			alert("Seleccione el Tipo de la Deuda");
			return false
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
        <td class="TitDetalle">Id</td>
        <td align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
		<input name="codcategoria" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Nombre" maxlength="200" value="<?=$row["descripcion"]?>" style="width:400px;"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Categoria</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><label>
	    <select name="categoria" id="categoria" class="select" style="width:220px">
        	<option value="0" <?=$d="";$row["categoria"]==0?$d="selected='selected'":$d="";echo $d;?> >FACTURACION</option>
            <option value="1" <?=$d="";$row["categoria"]==1?$d="selected='selected'":$d="";echo $d;?> >SALDO</option>
            <option value="2" <?=$d="";$row["categoria"]==2?$d="selected='selected'":$d="";echo $d;?> >SALDO REFINANCIADO</option>
            <option value="3" <?=$d="";$row["categoria"]==3?$d="selected='selected'":$d="";echo $d;?> >PRESTACION DE SERVICIOS EN CAJA</option>
	    </select>
	    </label></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Tipo de Deuda</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><? $objMantenimiento->drop_tipo_deuda($row["codtipodeuda"]); ?></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	  	<? include("../../../../include/estareg.php"); ?>
      </td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>
    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>