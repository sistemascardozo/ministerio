<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	$TituloVentana = "CATEGORIA DE COBRANZA";
	$Activo=1;
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	
	$FormatoGrilla = array ();
	
	$Sql = "select c.codcategoria,
            c.descripcion,
            case when c.categoria=0 then  'FACTURACION' else
            case when c.categoria=1 then 'SALDO' else
            case when c.categoria=2 then 'SALDO REFINANCIADO' else 
            'PRESTACION DE SERVICIOS EN CAJA' end end end,
            t.descripcion,e.descripcion, c.estareg
            from cobranza.categoriacobranza as c
            inner join public.tipodeuda as t on(c.codtipodeuda=t.codtipodeuda)
          INNER JOIN public.estadoreg e ON (c.estareg=e.id)";
  
   $FormatoGrilla[0] =eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'c.codcategoria', '2'=>'c.descripcion','3'=>'t.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Descripci&oacute;n', 'T3'=>'Categoria',
                      'T4'=>'Tipo de Deuda', 'T5'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A5'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W2'=>'300', 'W5'=>'90');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1050;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  ORDER BY c.codcategoria ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'6', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'6', 
              'BtnCV3'=>'0');
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 650, 300);
  Pie();
  CuerpoInferior();
?>

