<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codcategoria 	= $_POST["codcategoria"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$categoria		= $_POST["categoria"];
	$tipodeuda		= $_POST["tipodeuda"];
	$estareg		= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("categoriacobranza","0","0");
			$codcategoria 	= $id[0];
			$sql = "insert into cobranza.categoriacobranza(codcategoria,descripcion,estareg,categoria,codtipodeuda) 
				values(:codcategoria,:descripcion,:estareg,:categoria,:codtipodeuda)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codcategoria"=>$codcategoria,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg,
						   ":categoria"=>$categoria,
						   ":codtipodeuda"=>$tipodeuda));
		break;
		case 1:
			$sql = "update cobranza.categoriacobranza set descripcion=:descripcion,estareg=:estareg,categoria=:categoria,
				codtipodeuda=:codtipodeuda where codcategoria=:codcategoria";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codcategoria"=>$codcategoria,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg,
						   ":categoria"=>$categoria,
						   ":codtipodeuda"=>$tipodeuda));
		break;
		case 2:case 3:
			$sql = "update cobranza.categoriacobranza set estareg=:estareg
				where codcategoria=:codcategoria";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codcategoria"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>

