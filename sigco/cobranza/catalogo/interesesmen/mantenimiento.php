<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
    
    include("../../../../objetos/clsDrop.php");
    
    $objMantenimiento = new clsDrop();    
    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"])?$_POST["Id"]:'';
    $guardar	= "op=".$Op;
    $codsuc = $_SESSION['IdSucursal'];
    
    if($Id!='')
    {
        $consulta = $conexion->prepare("SELECT * from cobranza.interesmensual 
            WHERE codsuc= ? AND codinteresmen=? ");
        $consulta->execute(array($codsuc,$Id));
        $row = $consulta->fetch();
        $fecha= $objMantenimiento->DecFecha($row['fechareg']);
        $guardar= $guardar."&Id2=".$Id;
        $anio= $row['anio'];
        $mes= $row['mes'];
        $ciclo=$row["codciclo"];
        
    }
    
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_intmensual.js" language="JavaScript"></script>
<style type="text/css">
    .button {cursor:pointer;}

</style>
<script type="text/javascript">
    var codsuc = '<?=$codsuc?>';
    var urldir = '<?php echo $_SESSION['urldir'];?>';
    var anioact= '<?=$anio?>';
    var mesact= '<?=$mes?>';
    var Id= '<?=$Id?>';

    $(document).ready(function() {
        $(".button").button();
        
        ver('<?=$mes?>');
        if(Id!='')
        {
            cargar_anio('<?=$ciclo?>', '<?=$anio?>');        
            cargar_mes('<?=$ciclo?>', '<?=$codsuc?>', '<?=$anio?>');
        }
        

    });
    
    function ValidarForm(Op)
    {
        GuardarP(Op);
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }
    function calcularintpor()
    {
        var intvalpor = $("#interespor").val()
        //alert(intval);
        if(intvalpor=="")
        {
            intvalpor=0;
        }
        
        $("#interesval").val(parseFloat((intvalpor)/100))
    }
    
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
      <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                            <tr>
                              <td width="100" class="TitDetalle">&nbsp;</td>
                              <td align="center">&nbsp;</td>
                              <td class="CampoDetalle">&nbsp;</td>
                          </tr>
                            <tr>
                                <td class="TitDetalle">Id</td>
                                <td width="30" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input name="codinteresmen" type="text" id="Id" value="<?php echo $Id; ?>" size="4" maxlength="2" readonly="readonly" class="inputtext"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Fecha Inicio</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" id="fecha" name="fecha" style="width:80px; text-align: center;" value="<?php if($row['fechareg']=='') echo date('d/m/Y'); else echo $fecha; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Ciclo</td>
                                <td align="center">:</td>
                                <td>
                                    <?php $objMantenimiento->drop_ciclos($codsuc,$row['codciclo'],"onchange='cargar_anio(this.value,1);'"); ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">A&ntilde;o</td>
                                <td align="center">:</td>
                                <td>
                                    <div id="div_anio">
                                        <?php $objMantenimiento->drop_anio($codsuc, 0); ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Mes</td>
                                <td align="center">:</td>
                                <td>
                                    <div id="div_meses">
                                        <?php $objMantenimiento->drop_mes($codsuc,0,0); ?>
                                    </div>
                                    <input type="hidden" name="nrofacturacion" id="nrofacturacion" value="<?php echo $nrofacturacion; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Insteres (%)</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="interespor" id="interespor" onchange="calcularintpor()" class="inputtext" size="10" maxlength="10" value="<?php if($row['tasainteres']=='') echo '.00'; else echo (number_format($row['tasainteres'],4)); ?>" />
                                </td>
                            </tr>                                                 
                            <tr>
                                <td class="TitDetalle">Insteres (Valor)</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="interesval" id="interesval" onchange="calcularint();" class="inputtext" size="10" maxlength="10" value="<?php if($row['valtasainteres']=='') echo '.00'; else echo (number_format($row['valtasainteres'],4)); ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="TitDetalle">Observacion</td>
                                <td align="center" valign="top">:</td>
                                <td colspan="4">
                                    <textarea name="observacion" id="observacion" class="text ui-widget-content ui-corner-all" style="width:400px; height:100px;" ><?=$row["observacion"]; ?></textarea>
                              </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Estado</td>
                                <td align="center">:</td>
                                <td colspan="4" class="CampoDetalle">
                                    <?php include("../../../../include/estareg.php"); ?>
                                </td>
                            </tr>
                        </table>
    </form>
</div>
