<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
    
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
    $TituloVentana = "INTERES MENSUAL";
    $Activo = 1;
    CuerpoSuperior($TituloVentana);
    
    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
    $codsuc = $_SESSION['IdSucursal'];
    $FormatoGrilla = array();

    $Sql = "SELECT
        intmen.codinteresmen,
        intmen.tasainteres,
        intmen.valtasainteres,
        intmen.anio, 
        case intmen.mes
            when '1' then 'ENERO' 
            when '2' then 'FEBRERO' 
            when '3' then 'MARZO' 
            when '4' then 'ABRIL' 
            when '5' then 'MAYO' 
            when '6' then 'JUNIO' 
            when '7' then 'JULIO' 
            when '8' then 'AGOSTO' 
            when '9' then 'SETIEMBRE' 
            when '10' then 'OCTUBRE' 
            when '11' then 'NOVIEMBRE' 
            when '12' then 'DICIEMBRE'
        else '' end,
        e.descripcion,
        intmen.estareg
        FROM
        cobranza.interesmensual AS intmen
        INNER JOIN public.estadoreg e ON (intmen.estareg= e.id)";

    $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'intmen.tasainteres');          //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;                                                            //Operacion
    $FormatoGrilla[3] = array('T1' => 'C&oacute;digo', 'T2' => 'Insteres (%)', 'T3' => 'Insteres (Valor)', 'T4' => 'Año',
        'T5' => 'Mes', 'T6' => 'Estado');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'center', 'A3' => 'center', 'A4' => 'center', 'A6' => 'center');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '70', 'W2' => '150', 'W3' => '150', 'W4' => '120', 'W5' => '100', 'W6' => '90', 'W7' => '90');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 700;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = " AND (intmen.codemp=1 AND intmen.codsuc=".$codsuc.") ORDER BY intmen.codinteresmen DESC ";                                   //Orden de la Consulta
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '3', //Número de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'modificar.png', //Imagen a mostrar
        'Btn1' => 'Detalle', //Titulo del Botón
        'BtnF1' => 'onclick="Mostrar(this.id, 1);"', //Eventos del Botón
        'BtnCI1' => '7', //Item a Comparar
        'BtnCV1' => '1', //Valor de comparación
        'BtnId2'=>'BtnEliminar', 
        'BtnI2'=>'eliminar.png', 
        'Btn2'=>'Eliminar', 
        'BtnF2'=>'onclick="Eliminar(this.id)"', 
        'BtnCI2'=>'7', //campo 3
        'BtnCV2'=>'1',//igua a 1
        'BtnId3'=>'BtnRestablecer', //y aparece este boton
        'BtnI3'=>'restablecer.png', 
        'Btn3'=>'Restablecer', 
        'BtnF3'=>'onclick="Restablecer(this.id)"', 
        'BtnCI3'=>'7', 
        'BtnCV3'=>'0');

    $_SESSION['Formato'] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 650, 440);
    Pie();
    CuerpoInferior();
?>

