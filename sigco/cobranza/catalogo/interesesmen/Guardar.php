<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();
    
    $codsuc  = $_SESSION["IdSucursal"];
    $codemp  = 1;
    $idusu   = $_SESSION['id_user'];
    $codinteresmen = $_POST["codinteresmen"];
    
    $tasainteres    = $_POST["interespor"];
    $valtasainteres = $_POST["interesval"];
    $observacion    = strtoupper($_POST["observacion"]);
    $anio = $_POST["anio"];
    $mes  = $_POST["mes"];
    $fechareg = $objFunciones->CodFecha($_POST["fecha"]);
    $estareg  = $_POST["estareg"];
    $codciclo = $_POST["ciclo"];
    $nrofacturacion= $_POST["nrofacturacion"];
    
    switch ($Op) {
        case 0:

            //ACTUALIZA EL INTERES PARA CREDITOS
            $upd="UPDATE facturacion.periodofacturacion
                SET tasapromintant= :tasapromintant
                WHERE codemp= :codemp AND codsuc= :codsuc AND nrofacturacion=:nrofacturacion 
                AND codciclo= :codciclo";
            $conn = $conexion->prepare($upd);
            $conn->execute(array(":codemp"=>$codemp, ":codsuc"=>$codsuc, ":tasapromintant"=>$tasainteres,
                ":nrofacturacion"=>$nrofacturacion, ":codciclo"=>$codciclo));

            $id = $objFunciones->setCorrelativos("interes_mensual", $codsuc, "0");
            $codinteresmen = $id[0];

            $sql = "INSERT INTO cobranza.interesmensual(codinteresmen, codemp, codsuc, codusu, tasainteres, 
                valtasainteres, fechareg, observacion, estareg, anio, mes,codciclo)
                VALUES (:codinteresmen, :codemp, :codsuc, :codusu, :tasainteres, 
                :valtasainteres, :fechareg, :observacion, :estareg, :anio, :mes, :codciclo)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codinteresmen"=>$codinteresmen,
                ":codemp"=>$codemp, ":codsuc"=>$codsuc,
                ":codusu"=>$idusu, ":tasainteres"=>$tasainteres,
                ":valtasainteres"=>$valtasainteres, ":fechareg"=>$fechareg,
                ":observacion"=>$observacion, ":estareg"=>$estareg,
                ":anio"=>$anio, ":mes"=>$mes, ":codciclo"=>$codciclo));           
            
            
            break;

        case 1:
        
            $sql = "UPDATE cobranza.interesmensual SET 
                tasainteres= :tasainteres, valtasainteres= :valtasainteres, 
                fechareg= :fechareg, observacion= :observacion, anio= :anio, 
                mes= :mes, codciclo= :codciclo, estareg=:estareg
                WHERE codinteresmen= :codinteresmen AND codemp= :codemp AND codsuc= :codsuc ";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codinteresmen"=>$codinteresmen,
                ":codemp"=>$codemp, ":codsuc"=>$codsuc,
                ":tasainteres"=>$tasainteres,
                ":valtasainteres"=>$valtasainteres, ":fechareg"=>$fechareg,
                ":observacion"=>$observacion,":estareg"=>$estareg,
                ":anio"=>$anio, ":mes"=>$mes, ":codciclo"=>$codciclo));
            
            //ACTUALIZA EL INTERES PARA CREDITOS
            $upd="UPDATE facturacion.periodofacturacion
                SET tasapromintant= :tasapromintant
                WHERE codemp= :codemp AND codsuc= :codsuc AND nrofacturacion=:nrofacturacion 
                AND codciclo= :codciclo";
            $conn = $conexion->prepare($upd);
            $conn->execute(array(":codemp"=>$codemp, ":codsuc"=>$codsuc,
                ":tasapromintant"=>$tasainteres,
                ":nrofacturacion"=>$nrofacturacion, ":codciclo"=>$codciclo));
            
            break;
        case 2:case 3:
            $sql = "update cobranza.interesmensual
                SET estareg= :estareg
                WHERE codinteresmen= :codinteresmen AND codemp= :codemp AND codsuc= :codsuc";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codinteresmen" => $_POST["1form1_id"], 
                ":estareg" => $estareg,
                ":codemp"=>$codemp, ":codsuc"=>$codsuc));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
