<?php
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$conexion->beginTransaction();
	
	set_time_limit(0);
	 
	ini_set("memory_limit","64M");
	
	$codsuc 		= 1;//$_POST["codsuc"];
	$codciclo 		= 1;//$_POST["codciclo"];
	$porcentaje		= 1.5;//$_POST["porcentaje"];
	$idusuario		= 1;//$_SESSION['id_user'];
	$nrofacturacion = "202";//$_POST["nrofacturacion"];
	$calcalc		= 0;//$_POST["factura_alcantarillado"];	
	$anio			= '2013';//$_POST["anio"];
	$mes			= '10';//$_POST["mes"];
	
	/*Recupera los Datos de los documentos para la facturacion*/
	$sqldoc  = "select coddocumento,serie,correlativo ";
	$sqldoc .= "from reglasnegocio.correlativos ";
	$sqldoc .= "where coddocumento=4 and codemp=1 and codsuc=?";
	
	$consDoc = $conexion->prepare($sqldoc);
	$consDoc->execute(array($codsuc));
	$itemsDoc = $consDoc->fetch();
	
	$documento	 	= $itemsDoc["coddocumento"];
	$seriedoc		= $itemsDoc["serie"];
	$correlativo	= $itemsDoc["correlativo"];
	
	/*Obtiene el Porcentaje de Interes del Mes para la Deuda*/
	$porcentaje	= $porcentaje/100;
	
	$porcalc 	= $objFunciones->getParamae("PORALC",$codsuc);
	$Pigv		= $objFunciones->getParamae("IMPIGV",$codsuc);
	$Pigv		=$Pigv["valor"];
	
		/*Inicio del Proceso de Calculo de Facturacion*/
		$sqlC  	 	= "select * from facturacion.view_usuariofacturar 
					   where codemp=1 and codsuc=? and codestadoservicio<>7 and 
					   codtipousuario=1 and estareg=1 and codciclo=? AND nroinscripcion=741
					   ORDER by nroorden";
		
		$consultaC = $conexion->prepare($sqlC);
		$consultaC->execute(array($codsuc,$codciclo));
		$itemsC = $consultaC->fetchAll();
		
		foreach($itemsC as $rowC)
		{
			$conscategoria 	= 0;
			$impagua		= 0;
			$impdesague		= 0;
			$impigv			= 0;
			
			/*Recupero el Consumo Facturado*/
			$sqlFACT 	= "select * from facturacion.f_getconsfacturado(:codsuc,:catetar,:consumo,:tipofacturacion)";
			$consFact	= $conexion->prepare($sqlFACT);
			$consFact->execute(array(":codsuc"=>$codsuc,
									 ":catetar"=>$rowC["catetar"],
									 ":consumo"=>$rowC["consumo"],
									 ":tipofacturacion"=>$rowC["tipofacturacion"]));
			$consumo_facturado = $consFact->fetch();
	
			if($rowC["codestadoservicio"]!=1)
			{
			echo	$insCab 	 = "insert into facturacion.cabfacturacion(codemp,codsuc,codciclo,nrofacturacion,nroinscripcion,coddocumento,nrodocumento,
								catetar,lecturaultima,fechalectultima,lecturaanterior,fechalectanterior,consumo,consumofact,tipofacturacion,codestadoservicio,
								creador,anio,mes,propietario,serie,codtiposervicio,fecharevmedidor,lecturapromedio,exoneraalc)
								values(:codemp,:codsuc,:codciclo,:nrofacturacion,:nroinscripcion,:coddocumento,:nrodocumento,:catetar,:lecturaultima,:fechalectultima,
								:lecturaanterior,:fechalectanterior,:consumo,:consumofact,:tipofacturacion,:codestadoservicio,:creador,:anio,:mes,:propietario,
								:serie,:codtiposervicio,:fecharevmedidor,:lecturapromedio,:exoneraalc)";
				
			/*	$consCab = $conexion->prepare($insCab);
				$consCab->execute(array(":codemp"=>1,
										":codsuc"=>$codsuc,
										":codciclo"=>$codciclo,
										":nrofacturacion"=>$nrofacturacion,
										":nroinscripcion"=>$rowC["nroinscripcion"],
										":coddocumento"=>$documento,
										":nrodocumento"=>$correlativo,
										":catetar"=>$rowC["catetar"],
										":lecturaultima"=>$rowC["lecturaultima"],
										":fechalectultima"=>$rowC["fechalecturaultima"],
										":lecturaanterior"=>$rowC["lecturaanterior"],
										":fechalectanterior"=>$rowC["fechalecturaanterior"],
										":consumo"=>$rowC["consumo"],
										":consumofact"=>$consumo_facturado[0],
										":tipofacturacion"=>$rowC["tipofacturacion"],
										":codestadoservicio"=>$rowC["codestadoservicio"],
										":creador"=>$idusuario,
										":anio"=>$anio,
										":mes"=>$mes,
										":propietario"=>$rowC["propietario"],
										":serie"=>$seriedoc,
										":codtiposervicio"=>$rowC["codtiposervicio"],
										":fecharevmedidor"=>$rowC["fecharevmedidor"],
										":lecturapromedio"=>$rowC["lecturapromedio"],
										":exoneraalc"=>$rowC["exoneralac"]));
			*/	
			}

			if($rowC["codestadoservicio"]==1)
			{
				echo $insCab 	 = "insert into facturacion.cabfacturacion(codemp,codsuc,codciclo,nrofacturacion,nroinscripcion,coddocumento,nrodocumento,
								catetar,lecturaultima,fechalectultima,lecturaanterior,fechalectanterior,consumo,consumofact,tipofacturacion,codestadoservicio,
								creador,anio,mes,propietario,serie,codtiposervicio,fecharevmedidor,lecturapromedio,exoneraalc)
								values(:codemp,:codsuc,:codciclo,:nrofacturacion,:nroinscripcion,:coddocumento,:nrodocumento,:catetar,:lecturaultima,:fechalectultima,
								:lecturaanterior,:fechalectanterior,:consumo,:consumofact,:tipofacturacion,:codestadoservicio,:creador,:anio,:mes,:propietario,
								:serie,:codtiposervicio,:fecharevmedidor,:lecturapromedio,:exoneraalc)";
								
				/*$consCab = $conexion->prepare($insCab);
				$consCab->execute(array(":codemp"=>1,
										":codsuc"=>$codsuc,
										":codciclo"=>$codciclo,
										":nrofacturacion"=>$nrofacturacion,
										":nroinscripcion"=>$rowC["nroinscripcion"],
										":coddocumento"=>$documento,
										":nrodocumento"=>$correlativo,
										":catetar"=>$rowC["catetar"],
										":lecturaultima"=>$rowC["lecturaultima"],
										":fechalectultima"=>$rowC["fechalecturaultima"],
										":lecturaanterior"=>$rowC["lecturaanterior"],
										":fechalectanterior"=>$rowC["fechalecturaanterior"],
										":consumo"=>$rowC["consumo"],
										":consumofact"=>$consumo_facturado[0],
										":tipofacturacion"=>$rowC["tipofacturacion"],
										":codestadoservicio"=>$rowC["codestadoservicio"],
										":creador"=>$idusuario,
										":anio"=>$anio,
										":mes"=>$mes,
										":propietario"=>$rowC["propietario"],
										":serie"=>$seriedoc,
										":codtiposervicio"=>$rowC["codtiposervicio"],
										":fecharevmedidor"=>$rowC["fecharevmedidor"],
										":lecturapromedio"=>$rowC["lecturapromedio"],
										":exoneraalc"=>$rowC["exoneralac"]));
				*/
			/*Reecupera los importe y datos de las unidades de uso*/
				$num_total_unidades = $rowC["domestico"]+$rowC["social"]+$rowC["comercial"]+$rowC["estatal"]+$rowC["industrial"];
								
				if($num_total_unidades==1)				
				{
					$conscategoria = round($rowC["consumo"]*(100/100),2);
					
					$sqlimporte 	= "select * from facturacion.f_getimportetarifa(:codsuc,:catetar,:tipofacturacion,:consumo)";
					
					$consimporte 	= $conexion->prepare($sqlimporte);
					$consimporte->execute(array(":codsuc"=>$codsuc,
												":catetar"=>$rowC["catetar"],
												":tipofacturacion"=>$rowC["tipofacturacion"],
												":consumo"=>$conscategoria));
					$items = $consimporte->fetch();
					/*$sqlimporte 	= "select * from facturacion.f_getimportetarifa(".$codsuc.",'".$rowC["catetar"]."','".$rowC["tipofacturacion"]."','".$conscategoria."')";
					$result=$conexion->query($sqlimporte);
					if (!$result)
						die($sqlimporte);*/
					$impagua    = empty($items[0])?0:$items[0];
					$impdesague = empty($items[1])?0:$items[1];
					/*if($rowC["nroinscripcion"]=='699')
					{
						
						echo "<br>FUNCION impagua-->".round($impagua,2);
						echo "<br>FUNCION impdesague-->".round($impdesague,2);
						

					}*/

					if($calcalc==1)
					{
						$impdes = round($items[0]*($porcalc/100),2);
					}else{
						$impdes	= round($items[1],2);
					}
					
					$insU = "insert into facturacion.unidadesusofacturadas(codemp,codsuc,nrofacturacion,nroinscripcion,codunidaduso,catetar,porcentaje,
							 principal,consumo,importe,importealc,codciclo)
							 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codunidaduso,:catetar,:porcentaje,:principal,:consumo,
									:importe,:importealc,:codciclo)";
					
					$consU = $conexion->prepare($insU);
					$consU->execute(array( 	":codemp"=>1,
										   	":codsuc"=>$codsuc,
										   	":nrofacturacion"=>$nrofacturacion,
											":nroinscripcion"=>$rowC["nroinscripcion"],
											":codunidaduso"=>1,
											":catetar"=>$rowC["catetar"],
											":porcentaje"=>100,
											":principal"=>1,
											":consumo"=>$conscategoria,
											":importe"=>str_replace(",","",round($impagua,1)),
											":importealc"=>str_replace(",","",round($impdes,1)),
											":codciclo"=>$codciclo));

						
				}else
				{
					//---cuando tienes mas de 1 unidades de uso------
					$sqlU = "select catetar,porcentaje,principal from catastro.unidadesusoclientes where codemp=1 and
							 codsuc=? and nroinscripcion=?";
					
					$consultaU = $conexion->prepare($sqlU);
					$consultaU->execute(array($codsuc,$rowC["nroinscripcion"]));
					$itemsU = $consultaU->fetchAll();

					foreach($itemsU as $rowU)
					{
						$conscategoria = round($rowC["consumo"]*($rowU["porcentaje"]/100),2);
		
						$sqlI 	= "select * from facturacion.f_getimportetarifa(:codsuc,:catetar,:tipofacturacion,:consumo)";
						
						$consultaI = $conexion->prepare($sqlI);
						$consultaI->execute(array(":codsuc"=>$codsuc,
												  ":catetar"=>$rowU["catetar"],
												  ":tipofacturacion"=>$rowC["tipofacturacion"],
												  ":consumo"=>$conscategoria));

						$itemsI = $consultaI->fetch();
						
						$impagua    += $itemsI[0];
						
						
						if($calcalc==1)
						{
							$impdes = round($itemsI[0]*($porcalc/100),2);
						}else{
							$impdes	= round($itemsI[1],2);
						}
						
						$impdesague += floatval(str_replace(",","",$impdes));//$impdes;

						$insU = "insert into facturacion.unidadesusofacturadas(codemp,codsuc,nrofacturacion,nroinscripcion,codunidaduso,catetar,porcentaje,
							 	 principal,consumo,importe,importealc,codciclo)
							 	 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codunidaduso,:catetar,:porcentaje,:principal,:consumo,
								 :importe,:importealc,:codciclo)";
					
						$consU = $conexion->prepare($insU);
						$consU->execute(array( 	":codemp"=>1,
												":codsuc"=>$codsuc,
												":nrofacturacion"=>$nrofacturacion,
												":nroinscripcion"=>$rowC["nroinscripcion"],
												":codunidaduso"=>1,
												":catetar"=>$rowU["catetar"],
												":porcentaje"=>$rowU["porcentaje"],
												":principal"=>$rowU["principal"],
												":consumo"=>$conscategoria,
												":importe"=>str_replace(",","",round($itemsI[0],2)),
												":importealc"=>str_replace(",","",round($impdes,2)),
												":codciclo"=>$codciclo));

					}
				}
			
				/*Valida e inserta el concepto de cargo fijo siempre y cuando sea mayor a cero*/
				$sqlT = "select impcargofijo1 from facturacion.tarifas where codemp=1 and codsuc=? and catetar=?";
				
				$consultaT = $conexion->prepare($sqlT);
				$consultaT->execute(array($codsuc,$rowC["catetar"]));
				$itemsT = $consultaT->fetch();
				
				$TotalParaIgv=0;	
				if($itemsT["impcargofijo1"]>0)
				{
					$consulta_conceptos_cargo = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=5");
					$consulta_conceptos_cargo->execute(array($codsuc));
					$items_conceptos_cargo = $consulta_conceptos_cargo->fetchAll();
					
					foreach($items_conceptos_cargo as $row_conceptos)
					{	
					echo	$inscargo  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
									  importe,codtipodeuda,codciclo) 
									  values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
						
					/*	$consulta_cargo = $conexion->prepare($inscargo);
						$consulta_cargo->execute(array(":codemp"=>1,
													   ":codsuc"=>$codsuc,
													   ":nrofacturacion"=>$nrofacturacion,
													   ":nroinscripcion"=>$rowC["nroinscripcion"],
													   ":codconcepto"=>$row_conceptos["codconcepto"],
													   ":importe"=>str_replace(",","",round($itemsT["impcargofijo1"],2)),
													   ":codtipodeuda"=>1,
													   ":codciclo"=>$codciclo));
													   */
						$TotalParaIgv=floatval($TotalParaIgv)+floatval($itemsT["impcargofijo1"]);	
					}
				}

				/*Valida el Tipo de Servicio*/
				if($rowC["codtiposervicio"]==1) //Agua y Desague
				{
					/*Calculo del Agua*/
					//INSERTAR SI IMPORTE ES MAYOR CERO
					if(floatval($impagua)>0)
					{

						$consulta_conceptos_agua = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=1");
						$consulta_conceptos_agua->execute(array($codsuc));
						$items_conceptos_agua = $consulta_conceptos_agua->fetchAll();
			
						foreach($items_conceptos_agua as $rowAgua_conceptos)
						{
							echo $insagua  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
										  importe,codtipodeuda,codciclo) 
										  values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
							
							/*$consulta_agua = $conexion->prepare($insagua);
							$consulta_agua->execute(array( ":codemp"=>1,
														   ":codsuc"=>$codsuc,
														   ":nrofacturacion"=>$nrofacturacion,
														   ":nroinscripcion"=>$rowC["nroinscripcion"],
														   ":codconcepto"=>$rowAgua_conceptos["codconcepto"],
														   ":importe"=>str_replace(",","",round($impagua,2)),
														   ":codtipodeuda"=>1,
														   ":codciclo"=>$codciclo));
*/
							$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impagua,2)));
						}
					}
					/*Calculo del Desague*/
					if($calcalc==1)
					{
						$impdesague = round($impagua*($porcalc/100),2);
					}else{
						$impdesague	= round($impdesague,2);
					}
					/*if($rowC["nroinscripcion"]=='699')
					{
						
						echo "<br>Agua-->".round($impagua,2);
						echo "<br>Desague-->".round($impdesague,2);
						

					}
				*/
					//INSERTAR SI IMPORTE ES MAYOR CERO
					if(floatval($impdesague)>0)
					{
						$consulta_conceptos_desague = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=2");
						$consulta_conceptos_desague->execute(array($codsuc));
						$items_conceptos_desague = $consulta_conceptos_desague->fetchAll();
				
						foreach($items_conceptos_desague as $rowDesague_conceptos)
						{
							
							echo $insdesague  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
										    importe,codtipodeuda,codciclo) 
										    values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
							
							/*$consulta_desague = $conexion->prepare($insdesague);
							$consulta_desague->execute(array( ":codemp"=>1,
														   	  ":codsuc"=>$codsuc,
														      ":nrofacturacion"=>$nrofacturacion,
														      ":nroinscripcion"=>$rowC["nroinscripcion"],
														      ":codconcepto"=>$rowDesague_conceptos["codconcepto"],
														      ":importe"=>str_replace(",","",round($impdesague,2)),
														      ":codtipodeuda"=>1,
														      ":codciclo"=>$codciclo));
														      */
							$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impdesague,2)));
						}
					}
					$impigv = round(($impagua+$impdesague)*($igv/100),2);
					
				}
				if($rowC["codtiposervicio"]==2) //Agua
				{
					$impdesague=0;
					
					/*Calculo del Agua*/
					if(floatval($impagua)>0)
					{
						$consulta_conceptos_agua = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=1");
						$consulta_conceptos_agua->execute(array($codsuc));
						$items_conceptos_agua = $consulta_conceptos_agua->fetchAll();
			
						foreach($items_conceptos_agua as $rowAgua_conceptos)
						{
							echo $insagua  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
										  importe,codtipodeuda,codciclo) 
										  values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
							
							/*$consulta_agua = $conexion->prepare($insagua);
							$consulta_agua->execute(array( ":codemp"=>1,
														   ":codsuc"=>$codsuc,
														   ":nrofacturacion"=>$nrofacturacion,
														   ":nroinscripcion"=>$rowC["nroinscripcion"],
														   ":codconcepto"=>$rowAgua_conceptos["codconcepto"],
														   ":importe"=>str_replace(",","",round($impagua,2)),
														   ":codtipodeuda"=>1,
														   ":codciclo"=>$codciclo));
							*/
							$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impagua,2)));
						}
					}
					
					$impigv = round(($impagua)*($igv/100),2);
				}
				if($rowC["codtiposervicio"]==3) //Desague
				{
					$impagua=0;
					/*Calculo del Desague*/
					if($calcalc==1)
					{
						$impdesague = round($impagua*($porcalc/100),2);
					}else{
						$impdesague	= round($impdesague,2);
					}
					if(floatval($impdesague)>0)
					{
						$consulta_conceptos_desague = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=2");
						$consulta_conceptos_desague->execute(array($codsuc));
						$items_conceptos_desague = $consulta_conceptos_desague->fetchAll();
						
						foreach($items_conceptos_desague as $rowDesague_conceptos)
						{
							
							echo $insdesague  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
										    importe,codtipodeuda,codciclo) 
										    values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
							
							/*$consulta_desague = $conexion->prepare($insdesague);
							$consulta_desague->execute(array( ":codemp"=>1,
														   	  ":codsuc"=>$codsuc,
														      ":nrofacturacion"=>$nrofacturacion,
														      ":nroinscripcion"=>$rowC["nroinscripcion"],
														      ":codconcepto"=>$rowDesague_conceptos["codconcepto"],
														      ":importe"=>str_replace(",","",round($impdesague,2)),
														      ":codtipodeuda"=>1,
														      ":codciclo"=>$codciclo));
							*/
							$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impdesague,2)));
						}
					}
					$impigv = round(($impdesague)*($igv/100),2);
				}
				//$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impdesague,2)));
				/*Inserta el Concepto de igv siempre y cuando sea mayor a cero(0)*/
				if($TotalParaIgv>0)
				{

					$impigv = round(floatval($TotalParaIgv)*(floatval($Pigv)/100),2);
					if($impigv>0)
					{
						$consulta_conceptos_igv = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=4");
						$consulta_conceptos_igv->execute(array($codsuc));
						$items_conceptos_igv = $consulta_conceptos_igv->fetchAll();
						
						foreach($items_conceptos_igv as $rowIgv_conceptos)
						{
							echo $insigv  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
										    importe,codtipodeuda,codciclo) 
										    values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
							
							/*$consulta_igv = $conexion->prepare($insigv);
							$consulta_igv->execute(array( ":codemp"=>1,
														  ":codsuc"=>$codsuc,
														  ":nrofacturacion"=>$nrofacturacion,
														  ":nroinscripcion"=>$rowC["nroinscripcion"],
														  ":codconcepto"=>$rowIgv_conceptos["codconcepto"],
														  ":importe"=>str_replace(",","",round($impigv,2)),
														  ":codtipodeuda"=>1,
														  ":codciclo"=>$codciclo));
														  */
						}
					}
					
				}

				//$correlativo=$correlativo+1; ERA ACAAA

			} //---Fin para los clientes con estado de servicio 1(operativo con servicio)---
				
			$correlativo=$correlativo+1;			
			//--Verifica si tiene credito el usuario que esta facturando--
			$impcreditos = 0;
			
			$sqlcreditos  = "select det.nrocuota,det.totalcuotas,cab.codconcepto,
			det.imptotal,det.nrocredito,det.subtotal,det.igv,det.redondeo ";
			$sqlcreditos .= "from facturacion.detcreditos as det ";
			$sqlcreditos .= "inner join facturacion.cabcreditos as cab on(det.codemp=cab.codemp and det.codsuc=cab.codsuc and ";
			$sqlcreditos .= "det.nrocredito=cab.nrocredito) ";
			$sqlcreditos .= "where det.codemp=1 and det.codsuc=:codsuc ";
			$sqlcreditos .= "and cab.nroinscripcion=:nroinscripcion and det.nrofacturacion=:nrofacturacion ";
			$sqlcreditos .= "and estadocuota=0 and cab.estareg=1 and tipocuota=0";
			
			$consulta_creditos = $conexion->prepare($sqlcreditos);
			$consulta_creditos->execute(array(":codsuc"=>$codsuc,":nroinscripcion"=>$rowC["nroinscripcion"],":nrofacturacion"=>$nrofacturacion));
			$items_creditos = $consulta_creditos->fetchAll();
			$SubtotalCreditos=0;
			$SubTotalIgvCreditos=0;
			$SubTotalRed = 0;
			foreach($items_creditos as $row_creditos)
			{			
				$impcreditos += $row_creditos[3];
				$SubtotalCreditos += $row_creditos[6];
				$SubTotalRed += $row_creditos[7];
				/*Inserta el credito en la facturacion SUB TOTAL*/
				$inscreditos_det  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
								 	 importe,codtipodeuda,codciclo,nrocredito,nrocuota) 
								 	 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,
											:codconcepto,:importe,:codtipodeuda,:codciclo,:nrocredito,:nrocuota)";
						
				$consulta_creditos_det = $conexion->prepare($inscreditos_det);
				$consulta_creditos_det->execute(array(  ":codemp"=>1,
											  			":codsuc"=>$codsuc,
											  			":nrofacturacion"=>$nrofacturacion,
											  			":nroinscripcion"=>$rowC["nroinscripcion"],
											  			":codconcepto"=>$row_creditos[2],
											  			":importe"=>str_replace(",","",round($row_creditos[5],2)),
											  			":codtipodeuda"=>4,
											  			":codciclo"=>$codciclo,
											  			":nrocredito"=>$row_creditos[4],
											  			":nrocuota"=>$row_creditos[0]."/".$row_creditos[1]));
				

				/*Inserta el credito en la facturacion IGV*/
				$inscreditos_det  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
								 	 importe,codtipodeuda,codciclo,nrocredito,nrocuota) 
								 	 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,
											:codconcepto,:importe,:codtipodeuda,:codciclo,:nrocredito,:nrocuota)";
						
				$consulta_creditos_det = $conexion->prepare($inscreditos_det);
				$consulta_creditos_det->execute(array(  ":codemp"=>1,
											  			":codsuc"=>$codsuc,
											  			":nrofacturacion"=>$nrofacturacion,
											  			":nroinscripcion"=>$rowC["nroinscripcion"],
											  			":codconcepto"=>5,
											  			":importe"=>str_replace(",","",round($row_creditos[6],2)),
											  			":codtipodeuda"=>4,
											  			":codciclo"=>$codciclo,
											  			":nrocredito"=>$row_creditos[4],
											  			":nrocuota"=>$row_creditos[0]."/".$row_creditos[1]));

				/*Inserta el credito en la facturacion REDONDEO*/
				$inscreditos_det  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
								 	 importe,codtipodeuda,codciclo,nrocredito,nrocuota) 
								 	 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,
											:codconcepto,:importe,:codtipodeuda,:codciclo,:nrocredito,:nrocuota)";
						
				$consulta_creditos_det = $conexion->prepare($inscreditos_det);
				$consulta_creditos_det->execute(array(  ":codemp"=>1,
											  			":codsuc"=>$codsuc,
											  			":nrofacturacion"=>$nrofacturacion,
											  			":nroinscripcion"=>$rowC["nroinscripcion"],
											  			":codconcepto"=>8,
											  			":importe"=>str_replace(",","",round($row_creditos[7],2)),
											  			":codtipodeuda"=>4,
											  			":codciclo"=>$codciclo,
											  			":nrocredito"=>$row_creditos[4],
											  			":nrocuota"=>$row_creditos[0]."/".$row_creditos[1]));


				/*Actualiza el estado en la Tabla de creditos*/
				$updcreditos  = "update facturacion.detcreditos set estadocuota=1 where codemp=1 and codsuc=:codsuc and ";
				$updcreditos .= "nrocredito=:nrocredito and nrofacturacion=:nrofacturacion and ";
				$updcreditos .= "estadocuota=0";
				
				$update_creditos = $conexion->prepare($updcreditos);
				$update_creditos->execute(array(":codsuc"=>$codsuc,":nrocredito"=>$row_creditos[4],":nrofacturacion"=>$nrofacturacion));
			}
			
			//--Verificamos si el Usuario tiene Refinanciamiento y lo Agregamos en la Facturacion--
			$imprefinanciamiento=0;
			
			$sqlrefinanciamiento="select d.codconcepto,d.importe,d.nrorefinanciamiento,d.nrocuota,d.totalcuotas
								  from facturacion.detrefinanciamiento as d 
								  inner join facturacion.cabrefinanciamiento as c on(d.nrorefinanciamiento=c.nrorefinanciamiento and d.codemp=c.codemp and d.codsuc=c.codsuc)
								  where c.nroinscripcion=? and d.nrofacturacion=? and c.codsuc=? and d.estadocuota=0";
								  
			$consulta_refinanciamiento = $conexion->prepare($sqlrefinanciamiento);
			$consulta_refinanciamiento->execute(array($rowC["nroinscripcion"],$nrofacturacion,$codsuc));
			$items_refinanciamiento = $consulta_refinanciamiento->fetchAll();
			
			foreach($items_refinanciamiento as $row_refinanciamiento)
			{
				/*Inserta los conceptos en la facturacion*/
				$insrefinanciamiento_det  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
								 	 		 importe,codtipodeuda,codciclo,nrorefinanciamiento,nrocuota) 
								 	 		 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,
											        :codconcepto,:importe,:codtipodeuda,:codciclo,:nrorefinanciamiento,:nrocuota)";
						
				$consulta_refinanciamiento_det = $conexion->prepare($insrefinanciamiento_det);
				$consulta_refinanciamiento_det->execute(array(  ":codemp"=>1,
											  					":codsuc"=>$codsuc,
											  					":nrofacturacion"=>$nrofacturacion,
											  					":nroinscripcion"=>$rowC["nroinscripcion"],
											  					":codconcepto"=>$row_refinanciamiento[0],
											  					":importe"=>str_replace(",","",round($row_refinanciamiento[1],2)),
											  					":codtipodeuda"=>3,
											  					":codciclo"=>$codciclo,
											  					":nrorefinanciamiento"=>$row_refinanciamiento[2],
											  					":nrocuota"=>$row_refinanciamiento[3]."/".$row_refinanciamiento[4]));
				
				
				/*Actualiza el estado en la Tabla de refeinanciamiento*/
				$updaterefinanciamiento  = "update facturacion.detrefinanciamiento set estadocuota=1 where ";
				$updaterefinanciamiento .= "nrorefinanciamiento=:nrorefinanciamiento and nrofacturacion=:nrofacturacion and codsuc=:codsuc and ";
				$updaterefinanciamiento .= "estadocuota=0";
				
				$update_refinanciamiento = $conexion->prepare($updaterefinanciamiento);
				$update_refinanciamiento->execute(array(":codsuc"=>$codsuc,":nrorefinanciamiento"=>$row_refinanciamiento[2],":nrofacturacion"=>$nrofacturacion));
			}
			
			
			//--Verifica si el usuario tiene deuda anterior para calcular el credito--
			$impdeudaagua 		= 0;
			$impdeudaalc		= 0;
			$intereses			= 0;
			$impdeudaigv		= 0;
			$impdeudacargo		= 0;
			$impdeudacredito	= 0;
			$tipoconcepto		= 1;

			$consulta_deuda = $conexion->prepare("select * from facturacion.f_importedeuda(?,?)");
			$consulta_deuda->execute(array($rowC["nroinscripcion"],$codsuc));
			$items_deuda = $consulta_deuda->fetch();
			
			$agua 		= ($items_deuda["agua"] + $items_deuda["intagua"])*$porcentaje;
			$desague 	= ($items_deuda["desague"]+$items_deuda["intdesague"])*$porcentaje;
			$igv		= ($items_deuda["importeigv"]+$items_deuda["intigv"])*$porcentaje;
			$cargo_fijo	= ($items_deuda["importecargo"]+$items_deuda["intcargo"])*$porcentaje;
			$credito 	= ($items_deuda["importecredito"]+$items_deuda["interescredito"])*$porcentaje;

			/*insertamos los datos de los intereses en la tabla detfacturacion*/
			if(floatval(str_replace(",","",round($agua,2)))<>0)
			{
				$consulta_conceptos_interes_agua = $conexion->prepare("select * from facturacion.conceptos 
																	  where codsuc=? and categoria=3 and categoria_intereses=1");
				$consulta_conceptos_interes_agua->execute(array($codsuc));
				$items_conceptos_interes_agua = $consulta_conceptos_interes_agua->fetchAll();
		
				foreach($items_conceptos_interes_agua as $row_conceptos_interes_agua)
				{
					$ins_interes_agua  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
									      importe,codtipodeuda,codciclo) 
									      values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
						
					$consulta_interes_agua = $conexion->prepare($ins_interes_agua);
					$consulta_interes_agua->execute(array(  ":codemp"=>1,
												  			":codsuc"=>$codsuc,
												  			":nrofacturacion"=>$nrofacturacion,
												  			":nroinscripcion"=>$rowC["nroinscripcion"],
												  			":codconcepto"=>$row_conceptos_interes_agua["codconcepto"],
												  			":importe"=>str_replace(",","",round($agua,2)),
												  			":codtipodeuda"=>1,
												  			":codciclo"=>$codciclo));
				}
			}
			if($desague>0)
			{
				$consulta_conceptos_interes_desague = $conexion->prepare("select * from facturacion.conceptos 
																	  	  where codsuc=? and categoria=3 and categoria_intereses=2");
				$consulta_conceptos_interes_desague->execute(array($codsuc));
				$items_conceptos_interes_desague = $consulta_conceptos_interes_desague->fetchAll();
				
				foreach($items_conceptos_interes_desague as $row_conceptos_interes_desague)
				{
					$ins_interes_desague  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
											 importe,codtipodeuda,codciclo) 
											 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
						
					$consulta_interes_desague = $conexion->prepare($ins_interes_desague);
					$consulta_interes_desague->execute(array(   ":codemp"=>1,
																":codsuc"=>$codsuc,
																":nrofacturacion"=>$nrofacturacion,
																":nroinscripcion"=>$rowC["nroinscripcion"],
																":codconcepto"=>$row_conceptos_interes_desague["codconcepto"],
																":importe"=>str_replace(",","",round($desague,2)),
																":codtipodeuda"=>1,
																":codciclo"=>$codciclo));
				}
			}
			if(floatval(str_replace(",","",round($igv,2)))<>0)
			{
				$consulta_conceptos_interes_igv = $conexion->prepare("select * from facturacion.conceptos 
																	  	  where codsuc=? and categoria=3 and categoria_intereses=3");
				$consulta_conceptos_interes_igv->execute(array($codsuc));
				$items_conceptos_interes_igv = $consulta_conceptos_interes_igv->fetchAll();
				
				foreach($items_conceptos_interes_igv as $row_conceptos_interes_igv)
				{
					$ins_interes_igv  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
											 importe,codtipodeuda,codciclo) 
											 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
						
					$consulta_interes_igv = $conexion->prepare($ins_interes_igv);
					$consulta_interes_igv->execute(array(   ":codemp"=>1,
															":codsuc"=>$codsuc,
															":nrofacturacion"=>$nrofacturacion,
															":nroinscripcion"=>$rowC["nroinscripcion"],
															":codconcepto"=>$row_conceptos_interes_igv["codconcepto"],
															":importe"=>str_replace(",","",round($igv,2)),
															":codtipodeuda"=>1,
															":codciclo"=>$codciclo));
				}
			}
			if($cargo_fijo>0)
			{
				$consulta_conceptos_interes_cargo = $conexion->prepare("select * from facturacion.conceptos 
																	  	where codsuc=? and categoria=3 and categoria_intereses=4");
				$consulta_conceptos_interes_cargo->execute(array($codsuc));
				$items_conceptos_interes_cargo = $consulta_conceptos_interes_cargo->fetchAll();
				
				foreach($items_conceptos_interes_cargo as $row_conceptos_interes_cargo)
				{
					$ins_interes_cargo  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
											 importe,codtipodeuda,codciclo) 
											 values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
						
					$consulta_interes_cargo = $conexion->prepare($ins_interes_cargo);
					$consulta_interes_cargo->execute(array( ":codemp"=>1,
															":codsuc"=>$codsuc,
															":nrofacturacion"=>$nrofacturacion,
															":nroinscripcion"=>$rowC["nroinscripcion"],
															":codconcepto"=>$row_conceptos_interes_cargo["codconcepto"],
															":importe"=>str_replace(",","",round($cargo_fijo,2)),
															":codtipodeuda"=>1,
															":codciclo"=>$codciclo));
				}
			}
			if(round($credito,2)>0)
			{
				$consulta_conceptos_interes_credito = $conexion->prepare("select * from facturacion.conceptos 
																	  	where codsuc=? and categoria=3 and categoria_intereses=5");
				$consulta_conceptos_interes_credito->execute(array($codsuc));
				$items_conceptos_interes_credito = $consulta_conceptos_interes_credito->fetchAll();
				
				foreach($items_conceptos_interes_credito as $row_conceptos_interes_credito)
				{
					$ins_interes_credito  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
										     importe,codtipodeuda,codciclo) 
										     values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
						
					$consulta_interes_credito = $conexion->prepare($ins_interes_credito);
					$consulta_interes_credito->execute(array( 	":codemp"=>1,
																":codsuc"=>$codsuc,
																":nrofacturacion"=>$nrofacturacion,
																":nroinscripcion"=>$rowC["nroinscripcion"],
																":codconcepto"=>$row_conceptos_interes_credito["codconcepto"],
																":importe"=>str_replace(",","",round($credito,2)),
																":codtipodeuda"=>1,
																":codciclo"=>$codciclo));
				}
			}			
			
			
			/*suma los totales del usuario facturado*/
			
			/*$totfacturacion = round($itemsT["impcargofijo1"],2)+round($impagua,2)+round($impdesague,2)+
							  round($impigv,2)+round($impcreditos,2)+round($agua,2)+round($desague,2)+round($igv,2)+
							  round($cargo_fijo,2)+round($credito,2);*/
			/*if($rowC["nroinscripcion"]=='699')
					{
						echo "impcargofijo1-->".round($itemsT["impcargofijo1"],2);
						echo "<br>impagua-->".round($impagua,2);
						echo "<br>impdesague-->".round($impdesague,2);
						echo "<br>TOTAL-->".$TotalParaIgv;
						echo "<br>IGV-->".$impigv;
						echo "<br>IGV-->".$Pigv;
						echo "<br>impcreditos-->".round($impcreditos,2);
						echo "<br>agua-->".round($agua,2);
						echo "<br>desague-->".round($desague,2);
						echo "<br>igv-->".round($igv,2);
						echo "<br>cargo_fijo-->".round($cargo_fijo,2);
						echo "<br>credito-->".round($credito,2);
						//die("Fin");

					}
*/
			$totfacturacion = round($TotalParaIgv,2)+
							  round($impigv,2)+round($impcreditos,2)+round($agua,2)+round($desague,2)+round($igv,2)+
							  round($cargo_fijo,2)+round($credito,2);		
			//if($rowC["nroinscripcion"]=='2946')
			//	die($totfacturacion.' = '.round($TotalParaIgv,2).'+'.round($impigv,2).'+'.round($impcreditos,2).'+'.round($agua,2).'+'.round($desague,2).'+'.round($igv,2).'+'.round($cargo_fijo,2).'+'.round($credito,2)	);
			$tot1 	= round($totfacturacion,2);
			$tot2	= round($totfacturacion,1);
			
			$redctual = round($tot2-$tot1,2);
			
			/*Inserta el redondeo actual si es mayor q cero*/
			if(floatval($redctual)<>0)
			{

				$consulta_conceptos_redondeo_actual = $conexion->prepare("select * from facturacion.conceptos 
																		  where codsuc=? and categoria=7 and categoria_redondeo=1");
				$consulta_conceptos_redondeo_actual->execute(array($codsuc));
				$items_conceptos_redondeo_actual = $consulta_conceptos_redondeo_actual->fetchAll();
				
				foreach($items_conceptos_redondeo_actual as $row_conceptos_redondeo_actual)
				{
					$ins_redondeo_actual  = "insert into facturacion.detfacturacion(codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
											     importe,codtipodeuda,codciclo) 
											     values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,:importe,:codtipodeuda,:codciclo)";
							
					$consulta_redondeo_actual = $conexion->prepare($ins_redondeo_actual);
					$consulta_redondeo_actual->execute(array(":codemp"=>1,
															 ":codsuc"=>$codsuc,
															 ":nrofacturacion"=>$nrofacturacion,
															 ":nroinscripcion"=>$rowC["nroinscripcion"],
															 ":codconcepto"=>$row_conceptos_redondeo_actual["codconcepto"],
															 ":importe"=>str_replace(",","",round($redctual,2)),
															 ":codtipodeuda"=>1,
															 ":codciclo"=>$codciclo));
				}
			}
			
			/*actualiza la tabla clientes con el valor del redondeo*/
			$updcliente = $conexion->prepare("update catastro.clientes set redondeo=? where codemp=1 and codsuc=? and nroinscripcion=?");
			$updcliente->execute(array(str_replace(",","",round($redctual,2)),$codsuc,$rowC["nroinscripcion"]));
			//REINICIAR VARIABLES
			$TotalParaIgv=0;
			$impigv=0;
			$impcreditos=0;
			$agua=0;
			$desague=0;
			$igv=0;
			$cargo_fijo=0;
			$credito=0;
				
		}
	
		/*actualiza la tabla correlativos*/
		$upddocumento = $conexion->prepare("update reglasnegocio.correlativos set correlativo=? where codemp=1 and codsuc=? and coddocumento=4");
		$upddocumento->execute(array($correlativo,$codsuc));

		/*Actualizamos el Periodo actualiza de facturacion e insertamos el nuevo periodo*/	
		$updperiodo = $conexion->prepare("update facturacion.periodofacturacion set facturacion=1 where codemp=1 and codsuc=? and nrofacturacion=? 
										  and codciclo=?");
		$updperiodo->execute(array($codsuc,$nrofacturacion,$codciclo));
		
		$newnrofact = $nrofacturacion+1;
		$mes		= $mes+1;
		if($mes>12)
		{
			$mes	 = 1;
			$anio	+= 1;
		}
		
		/*Insertamos el Nuevo Periodo de Facturacion*/
		$insperiodo  = "insert into facturacion.periodofacturacion(codemp,codsuc,nrofacturacion,anio,mes,creador,codciclo) ";
		$insperiodo .= "values(1,:codsuc,:nrofacturacion,:anio,:mes,:idusuario,:codciclo)"; 
		
		$insertar_periodo = $conexion->prepare($insperiodo);
		$insertar_periodo->execute(array(":codsuc"=>$codsuc,":nrofacturacion"=>$newnrofact,
										 ":anio"=>$anio,":mes"=>$mes,":idusuario"=>$idusuario,":codciclo"=>$codciclo));

		if(!$consCab)
		{
			$conexion->rollBack();
			
			$img 		= "<img src='".$urldir."images/error.png' width='31' height='31' />";
			$mensaje 	= "Error al Realizar la Facturacion";
			$res		= 0;
		}else{
			$conexion->commit();
			
			$img 		 = "<img src='".$urldir."images/Ok.png' width='31' height='31' />";
			$mensaje 	 = "La Facturacion se ha Realizado Correctamente";
			$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
			$res		 = 1;
		}
		
		echo $img."|".$mensaje."|".$res;
	
?>