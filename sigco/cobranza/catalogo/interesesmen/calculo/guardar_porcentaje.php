<?php 
	include("../../../../../config.php");
	
	$conexion->beginTransaction();
	
	$nrofacturacion = $_POST["nrofacturacion"];
	$codsuc			= $_POST["codsuc"];
	$codciclo		= $_POST["codciclo"];
	$porcentaje		= $_POST["porcentaje"];
	
	$upd = "update facturacion.periodofacturacion set tasainteres=? where codsuc=? and codciclo=? and nrofacturacion=?";
	$consulta 	= $conexion->prepare($upd);
	$consulta->execute(array($porcentaje,$codsuc,$codciclo,$nrofacturacion));	
	
	if(!$consulta)
	{
		$conexion->rollBack();
		
		$res=0;
		$mensaje = "Error Cuando se ha Actualizado el Porcentaje";
	}else{
		$conexion->commit();
		
		$res=1;
		$mensaje = "Porcentaje Actualizado Correctamente";
	}
	
	echo $res."|".$mensaje."|".$porcentaje;
	
?>