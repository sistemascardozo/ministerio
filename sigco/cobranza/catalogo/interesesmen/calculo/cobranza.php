<?php
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 		= $_POST["codsuc"];
	$codciclo		= $_POST["codciclo"];
	
	$conexion->beginTransaction();
	
	$facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
			
	$upd 		= "update facturacion.detfacturacion set categoria=1 where estadofacturacion in(1,3) 
				   and categoria<>2 and categoria=0 and codsuc=? and codciclo=?";
	$consulta 	= $conexion->prepare($upd);
	$consulta->execute(array($codsuc,$codciclo));	
	/*var_dump($result->errorInfo());
		die();*/
	$updP		= "update facturacion.periodofacturacion set saldo=1 where codsuc=? and codciclo=? and anio=? and mes=?";
	$consultaP 	= $conexion->prepare($updP);
	$consultaP->execute(array($codsuc,$codciclo,$facturacion["anio"],$facturacion["mes"]));
	
	if(!$consulta || !$consultaP)
	{
		$conexion->rollBack();
		
		$img 		= "<img src='".$urldir."images/error.png' width='31' height='31' />";
		$mensaje 	= "Error al Tratar de Realizar el Cierre de Cobranza";
		$res		= 0;
	}else{
		$conexion->commit();
		
		$img 		 = "<img src='".$urldir."images/Ok.png' width='31' height='31' />";
		$mensaje 	 = "El Cierre de Cobranza se ha Realizado Correctamente";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
	}
	
	echo $img."|".$mensaje."|".$res;
	
?>
