<?php
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 	= $_POST["codsuc"];
	$codciclo 	= $_POST["codciclo"];
	
	$facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
	
	
	$sql = "select anio,mes,tasainteres from facturacion.periodofacturacion where codsuc=? and codciclo=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$codciclo));
	$items = $consulta->fetchAll();

?>
<div id="divtasainteres" style="overflow:auto; height:300px">
 <table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%"  >
    <thead class="ui-widget-header" style="font-size: 11px">
    <th>Año</th>
    <th>Mes</th>
    <th>Porcentaje</th>
  </tr>
</thead>
<tbody>
  <?php
  	foreach($items as $row)
	{
  ?>
      <tr>
        <td align="center"><?=$row[0]?></td>
        <td align="center"><?=$meses[$row[1]]?></td>
        <td align="right"><?=number_format($row[2],2)?></td>
      </tr>
   <?php } ?> 
   </tbody>
</table>
</div>
<div id="enter" style="height:10px">
	<hr>
</div>
<table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%"  >
     <thead class="ui-widget-header" style="font-size: 11px">
    <th>Año</th>
    <th>Mes</th>
    <th>Porcentaje</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td align="center"><?=$facturacion["anio"]?></td>
    <td align="center"><?=$meses[$facturacion["mes"]]?></td>
    <td align="right"><label>
      <input type="text" name="porcentaje" id="porcentaje" class="inputtext numeric" style="text-align:right" size="10" maxlength="10" value="<?=number_format($facturacion[6],2)?>">
    </label></td>
  </tr>
  </tbody>
</table>
