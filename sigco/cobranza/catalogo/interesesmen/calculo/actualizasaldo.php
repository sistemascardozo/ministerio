<?php 
	set_time_limit(0);
	
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$conexion->beginTransaction();
	
	$codsuc 	= $_POST["codsuc"];
	$codciclo 	= $_POST["codciclo"];
	
	$facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
	
	$control_facturacion = $conexion->prepare("select count(*) from facturacion.cabfacturacion where codsuc=? and codciclo=? and nrofacturacion=?");
	$control_facturacion->execute(array($codsuc,$codciclo,($facturacion["nrofacturacion"]-1)));
	$items_facturacion = $control_facturacion->fetch();
	
	$Sql ="SELECT anio||''||mes FROM facturacion.periodofacturacion WHERE nrofacturacion=".intval(intval($facturacion["nrofacturacion"])-1)."";
	$Consula = $conexion->query($Sql);
	$row = $Consula->fetch();
	$Periodo = $row[0];

	if($items_facturacion[0]<=0)
	{
		$img 		 = "<img src='".$urldir."images/error.png' width='31' height='31' />";
		$mensaje 	 = "Aun no se ha Realizado la Facturacion";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
		
		echo $img."|".$mensaje."|".$res;
		
		die();
	}
	
	$consulta_validar 	= $conexion->prepare("select count(*) from facturacion.cabctacorriente where anio=? and mes=? and codsuc=? and codciclo=?");
	$consulta_validar->execute(array($facturacion["anio"],$facturacion["mes"],$codsuc,$codciclo));
	$items_validar = $consulta_validar->fetch();
	
	if($items_validar[0]<=0)
	{
		$consulta_saldo = $conexion->prepare("select * from facturacion.f_ctacorriente_saldo(:codsuc,:codciclo,:anio,:mes,:nrofacturacion,:periodo)");
		$consulta_saldo->execute(array(":codsuc"=>$codsuc,
									   ":codciclo"=>$codciclo,
									   ":anio"=>$facturacion["anio"],
									   ":mes"=>$facturacion["mes"],
									   ":nrofacturacion"=>($facturacion["nrofacturacion"]-1),
									   ":periodo"=>$Periodo));
		$items_saldo = $consulta_saldo->fetch();
		
		$consulta_facturacion = $conexion->prepare("select * from facturacion.f_ctacorriente_facturacion(:codsuc,:codciclo,:anio,:mes,:nrofacturacion,:periodo)");
		$consulta_facturacion->execute(array(":codsuc"=>$codsuc,
											 ":codciclo"=>$codciclo,
											 ":anio"=>$facturacion["anio"],
											 ":mes"=>$facturacion["mes"],
											 ":nrofacturacion"=>($facturacion["nrofacturacion"]-1),
											 ":periodo"=>$Periodo));
		$items_facturacion = $consulta_facturacion->fetch();

		

		if(!$items_saldo || !$items_facturacion)
		{
			$conexion->rollBack();
			
			$img 		= "<img src='".$urldir."images/error.png' width='31' height='31' />";
			$mensaje 	= "Error al Tratar de Actualizar los Saldo";
			$res		= 0;
		}else{
			$conexion->commit();
			
			$img 		 = "<img src='".$urldir."images/Ok.png' width='31' height='31' />";
			$mensaje 	 = "Los Hist&oacute;ricos se han Actualizado Correctamente";
			$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
			$res		 = 1;
		}
	}else{
		$img 		 = "<img src='".$urldir."images/error.png' width='31' height='31' />";
		$mensaje 	 = "Los Saldos Ya Fueron Actualizados no es Posible volver a generalos";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
	}
	
	echo $img."|".$mensaje."|".$res;
?>