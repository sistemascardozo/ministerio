// JavaScript Document
$(document).ready(function() {
    
    $("#table-per").on('click','#addDetail',function(){
        addDetail();
    });

    $("#DivPorcentaje").dialog({
        autoOpen: false,
        height: 500,
        width: 400,
        modal: true,
        resizable: false,
        buttons: {
            "Aceptar": function() {
                grabar_porcentaje();
            },
            "Cancelar": function() {
                $(this).dialog("close");
            }
        },
        close: function() {

        }
    });
    
    $( "#fecha" ).datepicker({
        showOn: 'button',
            direction: 'up',
            buttonImage: '../../../../images/iconos/calendar.png',
            buttonImageOnly: true,
            showOn: 'both',
            showButtonPanel: true,
            beforeShow: function(input, inst)
            {
                inst.dpDiv.css({marginTop: (input.offsetHeight) - 20 + 'px', marginLeft: (input.offsetWidth) - 90 + 'px'});
            }
    });
    
});

    function cargar_anio(codciclo,condicion)
    {
        $.ajax({
            url:urldir + "ajax/anio_drop.php",
            type:'POST',
            async:true,
            data:'codsuc='+codsuc+'&codciclo='+codciclo+'&condicion='+condicion,
            success:function(datos){
                   $("#div_anio").html(datos)
                   $("#anio").val(anioact)
            }
        }) 

    }
    
    function cargar_mes(codciclo,suc,anio)
    {
        $.ajax({
            url:urldir+"ajax/mes_drop.php",
            type:'POST',
            async:true,
            data:'codsuc='+suc+'&codciclo='+codciclo+'&anio='+anio,
            success:function(datos){
                   $("#div_meses").html(datos)
                   $("#mes").val(mesact)
                //ver(codciclo,suc,anio)  
            }
        }) 

    }
    
    function ver(mes)
    {
        var codciclo= $("#ciclo").val()
        var anio= $("#anio").val()
        if(anio==0) anio= anioact
        $.ajax({
            url:urldir+"ajax/nrofact_drop.php",
            type:'POST',
            async:true,
            data:'codsuc='+codsuc+'&codciclo='+codciclo+'&anio='+anio+'&mes='+mes,
            success:function(datos){
                var r = datos.split("|")
                $("#nrofacturacion").val(r[0])
                $("#interespor").val(r[1])
            }
        })
    }