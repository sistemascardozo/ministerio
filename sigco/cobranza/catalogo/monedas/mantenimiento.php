<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsMantenimiento();

	if($Id!='')
	{
		$consulta = $conexion->prepare($objMantenimiento->Sentencia("moneda")." where codmoneda=?");
		$consulta->execute(array($Id));
		$row = $consulta->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;
	}
	
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if ($("#Nombre").val() == '')
		{
			alert('La Descripcion de la moneda no puede ser NULo');
			return false;
		}
		if (document.form1.simbolo.value == '')
		{
			alert('El Simbolo de la Moneda no puede ser NULO');
			return false;
		}
		if (document.form1.avreviado.value == '')
		{
			alert('La abreviatura de la Moneda no puede ser NULO');
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="450" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">    
	  </tr>
	<tr>
        <td class="TitDetalle">Id</td>
        <td align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
		<input name="codmoneda" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Nombre" maxlength="200" value="<?=isset($row[1])?$row[1]:''?>" style="width:300px;"/>	    </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Simbolo</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
        <input class="inputtext" name="simbolo" type="text" id="simbolo" maxlength="100" value="<?=isset($row[2])?$row[2]:''?>" style="width:100px;"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Avreviado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
        <input class="inputtext" name="avr" type="text" id="avreviado" maxlength="100" value="<?=isset($row[3])?$row[3]:''?>" style="width:100px;"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <? include("../../../../include/estareg.php"); ?></td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>
    </table>
 </form>
</div>
<script>
  $("#Nombre").focus();
</script>
<?php
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>