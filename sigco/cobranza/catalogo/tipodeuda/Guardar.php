<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codtipodeuda 	= $_POST["codtipodeuda"];
	$descripcion 	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipodeuda","0","0");
			$codtipodeuda = $id[0];
			
			$sql = "insert into public.tipodeuda(codtipodeuda,descripcion,estareg) values(:codtipodeuda,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipodeuda"=>$codtipodeuda,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipodeuda set descripcion=:descripcion, estareg=:estareg 
			where codtipodeuda=:codtipodeuda";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipodeuda"=>$codtipodeuda,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipodeuda set estareg=:estareg
				where codtipodeuda=:codtipodeuda";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipodeuda"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
