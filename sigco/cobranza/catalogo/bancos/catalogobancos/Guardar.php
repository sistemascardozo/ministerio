<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codbanco 		= $_POST["codbanco"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("bancos","0","0");
			$codbanco = $id[0];
			
			$sql = "insert into public.bancos(codbanco,descripcion,estareg) values(:codbanco,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codbanco"=>$codbanco,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.bancos set descripcion=:descripcion, estareg=:estareg 
			where codbanco=:codbanco";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codbanco"=>$codbanco,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.bancos set estareg=:estareg
				where codbanco=:codbanco";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codbanco"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
