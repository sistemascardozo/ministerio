<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp			= 1;
	$codsuc			= $_SESSION["IdSucursal"];
	$codctabanco 	= $_POST["codctabanco"];
	$bancos			= $_POST["bancos"];
	$tipoctasbanco	= $_POST["tipoctasbanco"];
	$nroctabanco	= $_POST["nroctabanco"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$nroctacontable	= $_POST["nroctacontable"];
	$monedas		= $_POST["monedas"];
	$estareg		= $_POST["estareg"];

	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("ctasxbanco","0","0");
			$codctabanco 	= $id[0];
		
			$sql = "insert into public.ctasxbanco(codctabanco,codemp,codbanco,codtipoctabanco,codmoneda,nroctabanco,descripcion,
				nroctacontable,estareg,codsuc) values(:codctabanco,:codemp,:codbanco,:codtipoctabanco,:codmoneda,:nroctabanco,
				:descripcion,:nroctacontable,:estareg,:codsuc)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codctabanco"=>$codctabanco,
						   ":codemp"=>$codemp,
						   ":codbanco"=>$bancos,
						   ":codtipoctabanco"=>$tipoctasbanco,
						   ":codmoneda"=>$monedas,
						   ":nroctabanco"=>$nroctabanco,
						   ":descripcion"=>$descripcion,
						   ":nroctacontable"=>$nroctacontable,
					 	   ":estareg"=>$estareg,
						   ":codsuc"=>$codsuc));
		break;
		case 1:
			$sql = "update public.ctasxbanco set codbanco=:codbanco,codtipoctabanco=:codtipoctabanco,codmoneda=:codmoneda,
				nroctabanco=:nroctabanco,descripcion=:descripcion,nroctacontable=:nroctacontable,estareg=:estareg 
				where codemp=:codemp and codsuc=:codsuc and codctabanco=:codctabanco";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codctabanco"=>$codctabanco,
						   ":codemp"=>$codemp,
						   ":codbanco"=>$bancos,
						   ":codtipoctabanco"=>$tipoctasbanco,
						   ":codmoneda"=>$monedas,
						   ":nroctabanco"=>$nroctabanco,
						   ":descripcion"=>$descripcion,
						   ":nroctacontable"=>$nroctacontable,
					 	   ":estareg"=>$estareg,
						   ":codsuc"=>$codsuc));
		break;
		case 2:case 3:
			$sql = "update public.ctasxbanco set estareg=:estareg
					where codemp=:codemp and codsuc=:codsuc and codctabanco=:codctabanco";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":codctabanco"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
