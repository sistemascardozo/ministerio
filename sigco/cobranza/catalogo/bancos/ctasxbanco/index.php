<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");
	
	$TituloVentana = "CUENTAS BANCOS";
	$Activo=1;
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	
	$FormatoGrilla = array ();
	
	$Sql = "SELECT cta.codctabanco, cta.descripcion AS ctabanco, banc.descripcion AS banco, tipcta.descripcion AS tipocta, cta.nroctabanco AS nroctabanco,
          cta.nroctacontable, UPPER(mon.descripcion), e.descripcion, cta.estareg
          FROM public.ctasxbanco as cta
          inner join public.bancos as banc on(cta.codbanco=banc.codbanco)
          inner join public.tipoctasbanco as tipcta on(cta.codtipoctabanco=tipcta.codtipoctabanco)
          inner join public.monedas as mon on(cta.codmoneda=mon.codmoneda) 
          INNER JOIN public.estadoreg e ON (cta.estareg=e.id)";
		  
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL     
  $FormatoGrilla[1] = array('1'=>'cta.codctabanco','2'=>'banc.descripcion','3'=>'tipcta.descripcion','4'=>'cta.nroctabanco',
                            '5'=>'cta.descripcion','6'=>'cta.nroctacontable','7'=>'mon.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op; //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Descripcion','T3'=>'Banco','T4'=>'Tipo de Cta.', 'T5'=>'Nro. Cuenta',
                            'T6'=>'Cta. Contable', 'T7'=>'Moneda', 'T8'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center', 'A5'=>'center', 'A6'=>'center', 'A7'=>'center', 'A8'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W2'=>'250', 'W4'=>'150', 'W8'=>'95');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " and (cta.codemp=1 and cta.codsuc=".$codsuc.") ORDER BY cta.codctabanco ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'9',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'9', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'9', 
              'BtnCV3'=>'0');
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 650, 380);
  Pie();
  CuerpoInferior();
?>

