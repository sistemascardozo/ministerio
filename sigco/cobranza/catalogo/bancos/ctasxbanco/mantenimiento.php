<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsDrop.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc 	= $_SESSION["IdSucursal"];
	
	$objMantenimiento 	= new clsDrop();
	
	if($Id!='')
	{
		$consulta = $conexion->prepare("select * from ctasxbanco where codctabanco=? and codsuc=?");
		$consulta->execute(array($Id,$codsuc));
		$row = $consulta->fetch();
	
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if($("#bancos").val()==0)
		{
			alert("Seleccione el Banco")
			return false
		}
		if($("#tipoctasbanco").val()==0)
		{
			alert("Seleccione el Tipo de Cta. Banco")
			return false
		}
		if($("#Nombre").val()=="")
		{
			alert('El Nro. de la Cuenta de Banco no puede ser NULO');
			return false
		}
		if($("#descripcion").val()=="")
		{
			alert('La Descripcion del Nro. de Cuenta no puede ser NULO');
			return false
		}
		if($("#ctacontable").val()=="")
		{
			alert('El Nro. de Cta. Contable no puede ser NULO');
			return false
		}

		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">    
	  </tr>
	<tr>
        <td class="TitDetalle">Id</td>
        <td align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
		<input name="codctabanco" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
		</tr>
	<tr>
	  <td class="TitDetalle">Banco</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<? $objMantenimiento->drop_banco($row["codbanco"]); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Tipo de Cuenta</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><? $objMantenimiento->drop_tipo_cta_banco($row["codtipoctabanco"]); ?></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Nro. de Cuenta</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="nroctabanco" type="text" id="Nombre" maxlength="200" value="<?=$row["nroctabanco"]?>" style="width:200px;"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><input class="inputtext" name="descripcion" type="text" id="descripcion" maxlength="200" value="<?=isset($row["descripcion"])?$row["descripcion"]:''?>"  style="width:400px;"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Cta. Contable</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><input class="inputtext" name="nroctacontable" type="text" id="ctacontable" maxlength="15" value="<?=isset($row["nroctacontable"])?$row["nroctacontable"]:''?>"  style="width:90px;"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Moneda</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><? $objMantenimiento->drop_monedas($row["codmoneda"]); ?></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	  	<? include("../../../../../include/estareg.php"); ?>
      </td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>
    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../../admin/validaciones/estareg.php"); 
?>