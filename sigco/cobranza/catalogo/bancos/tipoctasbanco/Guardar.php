<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codtipoctabanco 	= $_POST["codtipoctabanco"];
	$descripcion		= strtoupper($_POST["descripcion"]);
	$estareg			= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipoctasbanco","0","0");
			$codtipoctabanco = $id[0];
			
			$sql = "insert into public.tipoctasbanco(codtipoctabanco,descripcion,estareg) values(:codtipoctabanco,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoctabanco"=>$codtipoctabanco,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipoctasbanco set descripcion=:descripcion, estareg=:estareg 
			where codtipoctabanco=:codtipoctabanco";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoctabanco"=>$codtipoctabanco,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipoctasbanco set estareg=:estareg
				where codtipoctabanco=:codtipoctabanco";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoctabanco"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
