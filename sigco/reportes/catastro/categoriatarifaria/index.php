<?php

session_name("pnsu");
if(!session_start()){session_start();}

include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$TituloVentana = "CATEGORIA TARIFARIA";
$Activo = 1;
CuerpoSuperior($TituloVentana);
$codsuc = $_SESSION['IdSucursal'];

$objMantenimiento = new clsDrop();

$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);

$Fecha = date('d/m/Y');
?>

<script type="text/javascript" src="<?=$urldir ?>js/funciones.js" language="JavaScript"></script>
<script>

    jQuery(function($)
    {
        $("#DivTipos").buttonset();
    });
    var urldir = "<?=$urldir ?>"
    var codsuc = <?=$codsuc ?>

    function ValidarForm(Op)
    {
        var ciclos = $("#ciclo").val()

        if (ciclos == 0)
        {
            alert("Seleccione el Ciclo")
            return false
        }

        var EstadoServicio = ''
        if ($('#Todos').attr("checked") != "checked")
        {
            if ($("#estadoservicio").val() == 0)
            {
                alert('Seleccione Estado de Servicio')
                return false
            }

            EstadoServicio = '&EstadoServicio=' + $("#estadoservicio").val() + '&EstadoServicioN=' + $("#estadoservicio option:selected").html()
        }
        var CategoriaTarifaria = ''
        if ($('#Todoscategoria').attr("checked") != "checked")
        {
            if ($("#tarifas").val() == 0)
            {
                alert('Seleccione la Categoria Tarifaria')
                return false
            }

            CategoriaTarifaria = '&codtarifa=' + $("#tarifas").val() + '&descripcioncatetar=' + $("#tarifas option:selected").html()
        }

        url = "imprimir.php?ciclo=" + ciclos + "&anio=" + $("#anio").val() + "&mes=" + $("#mes").val() + "&codsuc=" + codsuc + "&medidor=" + $("#medidor").val();


        url += EstadoServicio
        url += CategoriaTarifaria
        AbrirPopupImpresion(url, 800, 600)

        return false
    }

    function Cancelar() {
        location.href = '<?=$urldir ?>/admin/indexB.php'
    }

    function TodosEstados() {
        if ($('#Todos').attr("checked") == "checked")
        {


            $('#estadoservicio').attr('disabled', 'disabled');


        }
        else
        {

            $('#estadoservicio').attr('disabled', false);

        }
    }

    function TodosEstadosCategoria() {
        if ($('#Todoscategoria').attr("checked") == "checked")
        {


            $('#tarifas').attr('disabled', 'disabled');


        }
        else
        {

            $('#tarifas').attr('disabled', false);

        }
    }

    function cargar_anio_drop(obj)
    {
        $.ajax({
            url: '../../../../ajax/anio_drop.php',
            type: 'POST',
            async: true,
            data: 'codsuc=<?=$codsuc ?>&codciclo=' + obj + '&condicion=1',
            success: function(datos) {
                $("#div_anio").html(datos)
            }
        })
    }
    function cargar_mes(ciclo, suc, anio)
    {
        $.ajax({
            url: '../../../../ajax/mes_drop.php',
            type: 'POST',
            async: true,
            data: 'codciclo=' + ciclo + '&codsuc=' + suc + '&anio=' + anio,
            success: function(datos) {
                $("#div_meses").html(datos)
            }
        })
    }
    function quitar_disabled(obj, input)
    {
        if (obj.checked)
        {
            $("#" + input).attr("disabled", true)
        } else {
            $("#" + input).attr("disabled", false)
        }
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>

                <tr>
                    <td colspan="2">
                        <fieldset style="padding:4px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="11%" align="right">Sucursal</td>
                                    <td width="2%" align="center">:</td>
                                    <td width="23%">
                                        <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="13%" align="right">Ciclo</td>
                                    <td width="3%" align="center">:</td>
                                    <td width="48%">
                                        <?php $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio_drop(this.value);'"); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Est. de Servicio</td>
                                    <td align="center">:</td>
                                    <td><?php $objMantenimiento->drop_estado_servicio($items["codestadoservicio"],""); ?></td>
                                    <td><input type="checkbox" id="Todos" onclick="TodosEstados();"></td>
                                    <td>
                                        Todos
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Categoria Tarifaria</td>
                                    <td align="center">:</td>
                                    <td><?php $objMantenimiento->drop_tarifas($codsuc,""); ?></td>
                                    <td><input type="checkbox" id="Todoscategoria" onclick="TodosEstadosCategoria();"></td>
                                    <td>
                                        Todos
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Medidor</td>
                                    <td align="center">:</td>
                                    <td>
                                        <select name="medidor" id="medidor">
                                            <option value="1">Con Medidor</option>
                                            <option value="2">Sin Medidor</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td colspan="3" align="center"> <input type="button" onclick="return ValidarForm();" value="Generar" id=""></td>
                                <input type="hidden" name="anio" id="anio" value="<?=date('Y') ?>">
                                <input type="hidden" name="mes" id="mes" value="<?=date('m') ?>">
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<?php CuerpoInferior(); ?>