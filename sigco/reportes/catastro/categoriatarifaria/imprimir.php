<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsReporte.php");
// include("../../../../objetos/clsFunciones.php");
$clfunciones = new clsFunciones();

class clsEstructura extends clsReporte {

    function cabecera() {
        global $mes, $anio, $meses, $x, $EstadoServicioN;

        $h = 4;
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(0, $h + 1, "REPORTE DE CONEXIONES POR CATEGORIA TARIFARIA", 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(0, $h + 1, "MES DE REFERENCIA ".$meses[$mes]." - ".$anio, 0, 1, 'C');
        if ($EstadoServicioN != "")
            $this->Cell(0, $h + 1, "ESTADO DE SERVICIO : ".$EstadoServicioN, 0, 1, 'C');
        $this->Ln(2);
    }

    function Leyenda() {

        global $x, $conexion;
    }

    //Consumos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,0,$CondEstSer)
    function Consumos($periodo, $Tarifa, $codsuc, $ciclo, $RangoInicial, $RangoFinal, $TipoFacturacion, $Descripcion, $CondEstSer) {

        global $x, $h, $conexion;
        $Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
            count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
            FROM facturacion.detctacorriente as d
            INNER JOIN facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
            d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
            d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
            INNER JOIN facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
            and d.codconcepto=cp.codconcepto)

            WHERE  c.periodo ='".$periodo."' AND c.catetar='".$Tarifa."' and 
            c.codsuc=".$codsuc." 
            and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
            AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' 
            AND c.tipofacturacion='".$Tipofacturacion."' ".$CondEstSer;
        $ConsultaMo = $conexion->query($Sql);
        $rowMo = $ConsultaMo->fetch();
        $this->SetFont('Arial', '', 6);
        if ($rowMo[1] > 0) {
            $this->Cell(70, $h, '', 0, 0, 'L');
            $this->Cell(40, $h, $Descripcion, 0, 0, 'L');
            $this->Cell(20, $h, number_format($rowMo[0], 2), 0, 0, 'R');
            $this->Cell(20, $h, $rowMo[1], 0, 0, 'R');
            $this->Cell(20, $h, number_format($rowMo[2], 2)."m".Cubico, 0, 1, 'R');
        }
        //$this->Ln(1);
    }

    //Rangos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,$Descripcion,$CondEstSer)
    function Rangos($periodo, $Tarifa, $codsuc, $ciclo, $RangoInicial, $RangoFinal, $Descripcion, $CondEstSer) {

        global $x, $h, $conexion;
        $Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
            count(DISTINCT(c.nroinscripcion)) as nrousaurio ,sum(c.consumofact)
            FROM facturacion.detctacorriente as d
            INNER JOIN facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
            d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
            d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
            INNER JOIN facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
            and d.codconcepto=cp.codconcepto)

            WHERE  c.periodo ='".$periodo."' AND c.catetar='".$Tarifa."' and 
            c.codsuc=".$codsuc." 
            and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
            AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
        $ConsultaMo = $conexion->query($Sql);
        $rowMo = $ConsultaMo->fetch();
        if ($rowMo[1] > 0) {
            $this->SetFont('Arial', 'B', 6);
            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(30, $h, '', 0, 0, 'L');
            $this->Cell(70, $h, $Descripcion, 0, 0, 'L');
            $this->Cell(20, $h, number_format($rowMo[0], 2), 0, 0, 'R');
            $this->Cell(20, $h, $rowMo[1], 0, 0, 'R');
            $this->Cell(20, $h, number_format($rowMo[2], 2)."m".Cubico, 0, 1, 'R');


            //CONSUMO LEIDO//
            $this->Consumos($periodo, $Tarifa, $codsuc, $ciclo, $RangoInicial, $RangoFinal, 0, 'Consumo Leido', $CondEstSer);
            //CONSUMO LEIDO//
            //CONSUMO PROMEDIADO//
            $this->Consumos($periodo, $Tarifa, $codsuc, $ciclo, $RangoInicial, $RangoFinal, 1, 'Consumo Promediado', $CondEstSer);
            //CONSUMO PROMEDIADO//
            //CONSUMO ASIGNADO//
            $this->Consumos($periodo, $Tarifa, $codsuc, $ciclo, $RangoInicial, $RangoFinal, 2, 'Consumo Asignado', $CondEstSer);
            //CONSUMO ASIGNADO//
        }
    }

}

define('Cubico', chr(179));

$ciclo = $_GET["ciclo"];
$codsuc = $_GET["codsuc"];
$EstadoServicio = $_GET["EstadoServicio"];
$EstadoServicioN = $_GET["EstadoServicioN"];
$codcatetar = $_GET["codtarifa"];
$catetardesc = $_GET["descripcioncatetar"];
$EstadoServicioN = $_GET["EstadoServicioN"];
$anio = $_GET["anio"];
$mes = $_GET["mes"];
$medidor = $_GET["medidor"];

$Desde = "1900/".$mes."/01";
$Dias = date('t', mktime(0, 0, 0, $mes, 1, $anio));
$Hasta = $anio."/".$mes."/".$Dias;
$Fechas = "  and c.fechareg between '".$Desde."' and '".$Hasta."'";

$x = 5;
$h = 4;
$objreporte = new clsEstructura();
$objreporte->AliasNbPages();
$objreporte->AddPage("H");
$CondEstSer = "";
$CondCatTar = "";
if ($EstadoServicio != ""):
    $CondEstSer = " AND c.codestadoservicio=".$EstadoServicio." ";
endif;

if ($codcatetar != ""):
    $CondCatTar = " AND c.catetar=".$codcatetar." ";
endif;

$sqlC = "SELECT c.catetar as catetar,cat.descripcion as descripcion,
    SUM(CASE (c.codtiposervicio) WHEN 1 THEN 1 WHEN 1 THEN 0 END),
    SUM(CASE (c.codtiposervicio) WHEN 2 THEN 1 WHEN 1 THEN 0 END),
    SUM(CASE (c.codtiposervicio) WHEN 3 THEN 1 WHEN 1 THEN 0 END)
    FROM catastro.clientes as c ";
$sqlC .= "INNER JOIN facturacion.tarifas as tar ON(c.codemp=tar.codemp and c.codsuc=tar.codsuc and ";
$sqlC .= "c.catetar=tar.catetar) ";
$sqlC .= "INNER JOIN facturacion.categoriatarifaria as cat ON(tar.codcategoriatar=cat.codcategoriatar) ";
$sqlC .= "WHERE tar.estado=1 AND c.codsuc=".$codsuc." and c.codciclo=".$ciclo." ".$CondEstSer.$CondCatTar.$Fechas;
$sqlC .= "group by c.catetar,cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";

$consultaC = $conexion->query($sqlC);
$itemsC = $consultaC->fetchALl();
$objreporte->SetFont('Arial', '', 8);

foreach ($itemsC as $rowC) {
    $objreporte->Ln(2);
    $objreporte->SetX($x);
    $objreporte->SetFont('Arial', 'B', 8);
    $objreporte->SetTextColor(0, 0, 0);
    $objreporte->Cell(40, $h, "CATEGORIAS", 1, 0, 'C');
    $objreporte->Cell(250, $h, "TOTAL", 1, 1, 'R');

    switch ($rowC['catetar']) {
        case 1:
            $objreporte->SetTextColor(255, 0, 0); //rojo
            break;
        case 2:
            $objreporte->SetTextColor(11, 60, 8); //verde
            break;
        case 3:
            $objreporte->SetTextColor(5, 35, 72); //azul
            break;
        case 4:
            $objreporte->SetTextColor(10, 177, 189); //celeste o turquesa
            break;
        case 4:
            $objreporte->SetTextColor(223, 217, 27); //Amarillo
            break;
        default:
            $objreporte->SetTextColor(0, 0, 0); //Anaranjado
            break;
    }

    switch ($rowC['tipofacturacion']) {
        case 0:
            $volumen = $rowT['consumo'];
            break;
        case 1:
            $volumen = $rowT['promedio'];
            break;
        case 2:
            $volumen = $rowT['consumo'];
            break;
    }


    $objreporte->SetX($x);
    $objreporte->Cell(40, $h, strtoupper($rowC[1]), 0, 0, 'C');
    $tot = $rowC[2] + $rowC[3] + $rowC[4] + $rowC[5];
    $objreporte->Cell(250, $h, $tot, 0, 1, 'R');

    $sqlT = "SELECT ".$clfunciones->getCodCatastral("c.").",
        c.nroinscripcion AS nroinscripcion, 
        c.propietario as propietario, 
        upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || c.nrocalle) as direccion,
        CASE WHEN c.tipofacturacion=0 THEN 'Consumo Leido' 
        WHEN c.tipofacturacion=1 THEN 'Consumo Promediado' ELSE 'Consumo Promediado' END AS tipofacturacion,
        c.nromed as medidor,
        tar.hastarango1 as volumen,
        tpse.descripcion as tiposervicio,
        c.consumo as consumo,
        c.lecturapromedio as promedio
        FROM catastro.clientes as c 
        INNER JOIN public.calles as cal ON(c.codemp = cal.codemp AND c.codsuc = cal.codsuc AND c.codcalle = cal.codcalle AND c.codzona = cal.codzona)
        INNER JOIN public.tiposcalle as tipcal ON(cal.codtipocalle = tipcal.codtipocalle)
        INNER JOIN facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and c.catetar=tar.catetar)
        INNER JOIN public.tiposervicio as tpse on(c.codtiposervicio=tpse.codtiposervicio)
        WHERE tar.estado=1 AND c.codemp = ? AND c.codsuc = ? AND c.catetar = ? AND c.codciclo = ?";

    if ($medidor == 1) {
        $sqlT .= " AND trim(c.nromed) <> '' ";
    } else {
        $sqlT .= " AND trim(c.nromed) = '' ";
    }
    $sqlT .= " ORDER BY c.nroinscripcion";
    $consultaT = $conexion->prepare($sqlT);
    $consultaT->execute(array(1, $codsuc, $rowC['catetar'], $ciclo));

    // var_dump($consultaT->errorInfo());exit;

    $itemsT = $consultaT->fetchAll();
    $objreporte->SetFont('Arial', 'B', 8);

    $objreporte->Ln(2);
    $objreporte->SetX($x + 5);
    $objreporte->Cell(30, $h, "COD. CATASTRAL", 1, 0, 'C');
    $objreporte->Cell(25, $h, "INSCRIPCION", 1, 0, 'C');
    $objreporte->Cell(65, $h, "PROPIETARIO", 1, 0, 'C');
    $objreporte->Cell(65, $h, "DIRECCION", 1, 0, 'C');
    $objreporte->Cell(15, $h, "VOL", 1, 0, 'C');
    $objreporte->Cell(15, $h, "TP. SERV.", 1, 0, 'C');
    $objreporte->Cell(40, $h, "TIPO FACTURACION", 1, 0, 'C');
    $objreporte->Cell(30, $h, "NRO. MEDIDOR", 1, 1, 'C');

    foreach ($itemsT as $rowT) {


        switch ($rowC['catetar']) {
            case 1:
                $objreporte->SetTextColor(255, 0, 0); //rojo
                break;
            case 2:
                $objreporte->SetTextColor(11, 60, 8); //verde
                break;
            case 3:
                $objreporte->SetTextColor(5, 35, 72); //azul
                break;
            case 4:
                $objreporte->SetTextColor(10, 177, 189); //celeste o turquesa
                break;
            case 4:
                $objreporte->SetTextColor(223, 217, 27); //Amarillo
                break;
            default:
                $objreporte->SetTextColor(0, 0, 0); //Anaranjado
                break;
        }

        switch ($rowC['tipofacturacion']) {
            case 0:
                $volumen = $rowT['consumo'];
                break;
            case 1:
                $volumen = $rowT['promedio'];
                break;
            case 2:
                $volumen = $rowT['consumo'];
                break;
            default:$volumen = 0;
                break;
        }

        $numeroinscripcion = $clfunciones->CodUsuario($codsuc, $rowT['nroinscripcion']);
        $objreporte->SetFont('Arial', '', 8);
        $objreporte->SetX($x + 5);
        $objreporte->Cell(30, $h, strtoupper(utf8_decode($rowT['codcatastro'])), 0, 0, 'C');
        $objreporte->Cell(25, $h, strtoupper(utf8_decode($numeroinscripcion)), 0, 0, 'C');
        $objreporte->Cell(65, $h, strtoupper(utf8_decode($rowT['propietario'])), 0, 0, 'L');
        $objreporte->Cell(65, $h, strtoupper(utf8_decode($rowT['direccion'])), 0, 0, 'L');
        $objreporte->Cell(15, $h, number_format($volumen, 0), 0, 0, 'C');
        $objreporte->Cell(15, $h, strtoupper(utf8_decode(substr($rowT['tiposervicio'], 0, 3))), 0, 0, 'C');
        $objreporte->Cell(40, $h, strtoupper(utf8_decode($rowT['tipofacturacion'])), 0, 0, 'C');
        $objreporte->Cell(40, $h, strtoupper(utf8_decode(($rowT['medidor']))), 0, 1, 'C');
    }
}

$objreporte->Ln(5);

$Sql = "SELECT 
    SUM(CASE (c.codtiposervicio) WHEN 1 THEN 1 WHEN 1 THEN 0 END),
    SUM(CASE (c.codtiposervicio) WHEN 2 THEN 1 WHEN 1 THEN 0 END),
    SUM(CASE (c.codtiposervicio) WHEN 3 THEN 1 WHEN 1 THEN 0 END),
    count(c.nroinscripcion)
    FROM catastro.clientes as c ";
$Sql .= "INNER JOIN facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and c.catetar=tar.catetar) ";
$Sql .= "INNER JOIN facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
$Sql .= "WHERE tar.estado=1 AND c.codsuc=".$codsuc." and c.codciclo=".$ciclo." ".$CondEstSer.$Fechas;

$ConsultaMo = $conexion->query($Sql);
$rowMo = $ConsultaMo->fetch();
$objreporte->SetFont('Arial', 'B', 8);
$objreporte->SetX($x);
$objreporte->Cell(160, '0.01', '', 1, 1, 'l');
$objreporte->SetX($x);
$objreporte->SetTextColor(255, 0, 0);
$objreporte->Cell(30, $h, 'TOTAL ', 0, 0, 'l');
$objreporte->Cell(20, $h, $rowMo[0], 0, 0, 'R');
$objreporte->Cell(20, $h, $rowMo[1], 0, 0, 'R');
$objreporte->Cell(20, $h, $rowMo[2], 0, 0, 'R');
$objreporte->Cell(20, $h, $rowMo[3], 0, 1, 'R');

$objreporte->Output();
?>