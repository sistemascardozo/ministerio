
<?php
include("../../../../config.php");

$IdSucursal = $_POST["IdSucursal"];
$IdSector = $_POST["IdSector"];
$Sql = "SELECT DISTINCT(c.codcliente),c.propietario
    FROM  catastro.clientes c
    INNER JOIN catastro.unidadesusoclientes uc ON (c.codemp = uc.codemp) AND (c.codsuc = uc.codsuc) AND (c.nroinscripcion = uc.nroinscripcion) 
    WHERE  c.codsuc=".$IdSucursal." AND c.codsector=".$IdSector."
    ORDER BY c.codcliente";
$Consulta = $conexion->query($Sql);
$c = 0;
?>
<table class="ui-widget treeTable" border="1" cellspacing="0" id="TbConsulta" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">

        <tr title="Cabecera">
            <th scope="col" width="100" align="center">CÓDIGO</th>
            <th scope="col" width="100" align="center">CATEGORIA</th>
            <th scope="col" width="400" align="center">USUARIO</th>
        </tr>
    </thead>
    <tbody style="font-size:12px">
        <?php
        foreach ($Consulta->fetchAll() as $row) {
            $c++;
            $ContG = $c;
            ?>
            <tr id="<?=$c ?>">
                <td><?=$row[0] ?></td>
                <td>&nbsp;</td>
                <td><?=(strtoupper($row[1])) ?></td>
            </tr>
            <?php
            $Sql = "SELECT DISTINCT c.nroinscripcion,c.propietario
		FROM  catastro.clientes c
		INNER JOIN catastro.unidadesusoclientes uc ON (c.codemp = uc.codemp)AND (c.codsuc = uc.codsuc)AND (c.nroinscripcion = uc.nroinscripcion) 
                WHERE  c.codsuc=".$IdSucursal." AND c.codsector=".$IdSector."
                AND c.codcliente=".$row[0]."
                ORDER BY c.nroinscripcion";
            $Consulta2 = $conexion->query($Sql);
            foreach ($Consulta2->fetchAll() as $row2) {
                $c++;
                $ContG1 = $c;

                $Sql = "SELECT COUNT(*)
		FROM catastro.unidadesusoclientes uc
		INNER JOIN facturacion.tarifas t ON (uc.codemp = t.codemp) AND (uc.codsuc = t.codsuc) AND (uc.catetar = t.catetar)
		WHERE t.estado=1 AND uc.codsuc=".$IdSucursal." AND uc.nroinscripcion=".$row2[0]."";
                $ConsultaC = $conexion->query($Sql);
                $RowC = $ConsultaC->fetch();
                $NroConexiones = $RowC[0];
                ?>
                <tr id="<?=$c ?>" class="child-of-<?=$ContG ?>" style="font-weight:bold;">
                    <td colspan="2">Nro Inscripci&oacute;n&nbsp;
                        <?=$row2[0] ?>
                    </td>
                    <td>Total Unidades de Uso : <?=$NroConexiones ?></td>

                </tr>
                <?php
                $Sql = "SELECT t.nomtar, uc.porcentaje
		FROM catastro.unidadesusoclientes uc
		INNER JOIN facturacion.tarifas t ON (uc.codemp = t.codemp) AND (uc.codsuc = t.codsuc)  AND (uc.catetar = t.catetar)
		WHERE t.estado=1 AND uc.codsuc=".$IdSucursal." AND uc.nroinscripcion=".$row2[0]." 
		ORDER BY t.nomtar";
                $Consulta3 = $conexion->query($Sql);
                foreach ($Consulta3->fetchAll() as $row3) {
                    $c++;
                    ?>
                    <tr id="<?=$c ?>" class="child-of-<?=$ContG1 ?>">
                        <td>&nbsp;</td>
                        <td><?=utf8_decode($row3[0]) ?></td>
                        <td><?=number_format($row3[1], 2) ?>%</td>

                    </tr>


                    <?php
                }
            }
        }
        ?>



    </tbody>

</table>