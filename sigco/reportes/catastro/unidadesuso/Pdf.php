<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsReporte.php");

class clsModificaciones extends clsReporte {

    function cabecera() {
        $h = 4;
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(190, $h + 2, "REPORTE DE UNIDADES DE USO", 0, 1, 'C');

        $this->Ln(3);
        $this->SetFont('Arial', 'B', 10);
        $this->Ln(3);

        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(30, 70, 90));
        $this->SetAligns(array("C", "C", "C"));
        $this->Row(array(utf8_decode("CÓDIGO"),
            "CATEGORIA",
            "USUARIO"));
    }

    function contenido($IdSucursal, $IdSector) {
        global $conexion;

        $h = 4;
        $this->SetFont('Arial', '', 6);

        $Sql = "SELECT DISTINCT(c.codcliente),c.propietario
            FROM  catastro.clientes c 
            INNER JOIN catastro.unidadesusoclientes uc ON (c.codemp = uc.codemp) AND (c.codsuc = uc.codsuc) AND (c.nroinscripcion = uc.nroinscripcion) 
            WHERE  c.codsuc=".$IdSucursal." AND c.codsector=".$IdSector."
            ORDER BY c.codcliente";
        $Consulta = $conexion->query($Sql);
        $c = 0;
        foreach ($Consulta->fetchAll() as $row) {
            $c++;
            $ContG = $c;
            $this->Cell(30, $h, utf8_decode($row[0]), 1, 0, 'C', false);
            $this->Cell(70, $h, '', 1, 0, 'L', false);
            $this->Cell(90, $h, utf8_decode(strtoupper($row[1])), 1, 1, 'L', false);


            $Sql = "SELECT DISTINCT(c.nroinscripcion),c.propietario
		FROM  catastro.clientes c 
                INNER JOIN catastro.unidadesusoclientes uc ON (c.codemp = uc.codemp)
			 AND (c.codsuc = uc.codsuc) AND (c.nroinscripcion = uc.nroinscripcion) 
			 WHERE  c.codsuc=".$IdSucursal." AND c.codsector=".$IdSector."
			 AND c.codcliente=".$row[0]."
			 ORDER BY c.nroinscripcion";
            $Consulta2 = $conexion->query($Sql);
            foreach ($Consulta2->fetchAll() as $row2) {
                $c++;
                $ContG1 = $c;

                $Sql = "SELECT COUNT(*)
		FROM catastro.unidadesusoclientes uc
		INNER JOIN facturacion.tarifas t ON (uc.codemp = t.codemp) AND (uc.codsuc = t.codsuc)  AND (uc.catetar = t.catetar)
		WHERE t.estado=1 AND  uc.codsuc=".$IdSucursal." AND uc.nroinscripcion=".$row2[0]."";
                $ConsultaC = $conexion->query($Sql);
                $RowC = $ConsultaC->fetch();
                $NroConexiones = $RowC[0];

                $this->Cell(30, $h, utf8_decode('Nro de Inscripción'), 1, 0, 'C', false);
                $this->Cell(70, $h, $row2[0], 1, 0, 'L', false);
                $this->Cell(90, $h, 'Total Unidades de Uso : '.$NroConexiones, 1, 1, 'L', false);

                $Sql = "SELECT t.nomtar, uc.porcentaje
		FROM catastro.unidadesusoclientes uc
		INNER JOIN facturacion.tarifas t ON (uc.codemp = t.codemp) AND (uc.codsuc = t.codsuc)  AND (uc.catetar = t.catetar)
		WHERE t.estado=1 AND  uc.codsuc=".$IdSucursal." AND uc.nroinscripcion=".$row2[0]."
		ORDER BY t.nomtar";
                $Consulta3 = $conexion->query($Sql);
                foreach ($Consulta3->fetchAll() as $row3) {
                    $c++;
                    $this->Cell(30, $h, '', 1, 0, 'C', false);
                    $this->Cell(70, $h, utf8_decode($row3[0]), 1, 0, 'L', false);
                    $this->Cell(90, $h, number_format($row3[1], 2).'%', 1, 1, 'L', false);
                }
            }
        }
    }

}

$IdSucursal = $_GET["IdSucursal"];
$IdSector = $_GET["IdSector"];
$codsuc = $IdSucursal;
$objReporte = new clsModificaciones();
$objReporte->AliasNbPages();
$objReporte->AddPage();
$objReporte->contenido($IdSucursal, $IdSector);
$objReporte->Output();
?>