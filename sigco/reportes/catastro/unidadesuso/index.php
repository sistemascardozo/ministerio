<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$TituloVentana = "UNIDADES DE USO";
$Activo = 1;
CuerpoSuperior($TituloVentana);
$objMantenimiento = new clsMantenimiento();

$sucursal = $objMantenimiento->setSucursales();

$codsuc = $_SESSION['IdSucursal'];
$codusu = $_SESSION['id_user'];
$cajero = $_SESSION['nombre'];
?>


<script type="text/javascript" src="<?=$urldir ?>js/funciones.js" language="JavaScript"></script>

<script>
    $(document).ready(function() {

/////		   
        $("button ").button();
    });

    function Cancelar()
    {
        location.href = '<?=$urldir ?>/admin/indexB.php'
    }

    function cSector()
    {
        var IdSucursal = $("#sucursal").val()
        $.ajax({
            url: '../../clases/cSector.php',
            type: 'POST',
            async: true,
            data: 'IdSucursal=' + IdSucursal,
            success: function(datos)
            {
                $("#Sector").html(datos)
            }
        })

    }
    function Consultar()
    {
        $('#DivConsulta').fadeOut(500)
        $('#ImgLoad').fadeIn();
        var IdSucursal = $("#sucursal").val()
        var IdSector = $("#Sector").val()
        $.ajax({
            url: 'Consulta.php',
            type: 'POST',
            async: true,
            data: 'IdSucursal=' + IdSucursal + '&IdSector=' + IdSector,
            success: function(datos)
            {
                //
                $('#ImgLoad').fadeOut(500, function() {
                    $("#DivConsulta").empty().append(datos);
                    $('#DivConsulta').fadeIn(500, function() {
                        var theTable = $('#TbConsulta')
                        $("#Filtro").keyup(function() {

                            $.uiTableFilter(theTable, this.value)

                        })
                        $("#TbConsulta").treeTable();
                    })
                })
                //
            }
        })
    }

    function Excel()
    {

        var IdSucursal = $("#sucursal").val()
        var IdSector = $("#Sector").val()
        var ventana = window.open('Excel.php?IdSucursal=' + IdSucursal + '&IdSector=' + IdSector, 'Planilla', 'resizable=yes, scrollbars=yes');
        ventana.focus();

    }
    function Pdf()
    {

        var IdSucursal = $("#sucursal").val()
        var IdSector = $("#Sector").val()
        var ventana = window.open('Pdf.php?IdSucursal=' + IdSucursal + '&IdSector=' + IdSector, 'Planilla', 'resizable=yes, scrollbars=yes');
        ventana.focus();

    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>

                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset style="padding:4px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="TitDetalle">Sucursal :</td>
                                    <td class="CampoDetalle">
                                        <select name="sucursal" id="sucursal" class="select" style="width:310px" onchange="cSector()">

                                            <?php
                                            foreach ($sucursal as $rowsucursal) {
                                                $selected = "";
                                                if ($rowsucursal["codsuc"] == $codsuc) {
                                                    $selected = "selected='selected'";
                                                }

                                                echo "<option value='".$rowsucursal["codsuc"]."' ".$selected." >".$rowsucursal["descripcion"]."</option>";
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TitDetalle">Sector:</td>
                                    <td class="CampoDetalle">
                                        <select id="Sector" class="select" style="width:310px">
                                        <?php
                                            $Sql = "SELECT codsector, descripcion
				      		FROM  public.sectores WHERE  estareg=1 AND codemp=1 AND codsuc=".$codsuc." ";
                                            $Consulta = $conexion->query($Sql);
                                            foreach ($Consulta->fetchAll() as $row) {
                                                ?>
                                                <option value="<?=$row[0] ?>" title="<?=$row[1] ?>"><?=$row[1] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="40" align="center">

                        <table width="500" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center">
                                    <input type="button" value="Guardar" id="btnAceptar" onclick="Consultar();" class="button"/>
                                </td>
                                <td align="center">
                                    <input id="BtnExcel" type="button" value="Excel" onclick="Excel();" class="BtnIndex button"/></td>
                                <td align="center">
                                    <input id="BtnPdf" type="button" value="Pdf" onclick="Pdf();"  class="BtnIndex button"/>
                                </td>
                                <td align="center">
                                    <input id="btnCerrar" type="button" value="Cancelar" onclick="Cancelar();" class="button"  />
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        Buscar:&nbsp;<input id="Filtro" class="inputtext" value="" maxlength="30" size="30" type="text" /></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">


                        <fieldset>
                            <legend>Resultados de la Consulta</legend>
                            <div id="ImgLoad" style="text-align: center"><img src="../../../../images/avance.gif" /><br/>Cargando ...</div>
                            <div style="height:auto; overflow:auto; display:none" align="center" id="DivConsulta">


                            </div>

                        </fieldset>


                    </td>
                </tr>

            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">

    $('#ImgLoad').fadeOut(500, function() {

        $('#DivConsulta').fadeIn(500)

    }
    )

    $("#btnAceptar").attr('value', 'Consultar')
    $("#btnAceptar").css('background-image', 'url(../../../../css/images/ver.png)')
</script>
<?php CuerpoInferior(); ?>