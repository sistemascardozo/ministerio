<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
  
  $TituloVentana = "PLANTILLA PRESUPUESTAL";
  $Activo = 1;
  
  CuerpoSuperior($TituloVentana);
  
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  $Criterio = 'Materiales';
  
  $FormatoGrilla = array ();
  
  $Sql = "SELECT c.nroconfeccion,
    tp.descripcion AS tp,
    c.descripcion,    
    CASE WHEN c.tipoconexion=1 THEN 'AGUA' ELSE 'DESAGUE' END,
    c.cantidad,".$objFunciones->Convert("c.preciounitario","NUMERIC (18,2)").",
    ".$objFunciones->Convert("c.total","NUMERIC (18,2)").",
    u.abreviado,
    CASE WHEN c.afectaigv=1 THEN 'Afecta' ELSE 'No Afecta' END,
    c.estareg
    FROM solicitudes.confecpreagudes as c
    INNER JOIN public.unidades as u on(c.unimed=u.unimed)
    INNER JOIN public.tipopresupuesto AS tp ON tp.codtipopresupuesto = c.codtipopresupuesto ";
  $FormatoGrilla[0] =eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                           //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'c.nroconfeccion', '2'=>'tp.descripcion', '3'=>'c.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo','T2'=>'Tipo Presupuesto','T3'=>'Material','T4'=>'Conexion','T5'=>'Cantidad','T6'=>'Prec. Unitario',
                'T7'=>'Total','T8'=>'Und','T9'=>'Afc. Igv.');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'center', 'A5'=>'right', 'A6'=>'right', 'A7'=>'right', 'A8'=>'center', 'A9'=>'center', 'A11'=>'right');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W6'=>'60');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  AND (c.codemp = 1 and c.codsuc = ".$codsuc.") ORDER BY c.nroconfeccion DESC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'10',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'10', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'10', 
              'BtnCV3'=>'0');
              
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  
  Cabecera('', $FormatoGrilla[7], 800, 500);
  
  Pie();
  CuerpoInferior();
?>
