<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include('../../../../objetos/clsFunciones.php');
    $conexion->beginTransaction();

    $Op = $_GET["Op"];
    $objFunciones = new clsFunciones();

    $codsuc         = $_SESSION['IdSucursal'];
    $codemp         = 1;
    $nroconfeccion  = $_POST["nroconfeccion"];
    $tipoconexion   = $_POST["tipoconexion"];
    $tipopresupuesto = $_POST["codtipopresupuesto"];
    $descripcion    = strtoupper($_POST["descripcion"]);
    $cantidad       = $_POST["cantidad"];
    $preciounitario = $_POST["preciounitario"];
    $total          = $_POST["total"];
    $montoempresa   = $_POST["montoempresa"];
    $montousuario   = $_POST["montousuario"];
    $tipocostos     = $_POST["tipocostos"];
    $grupocostos    = $_POST["grupocostos"];
    $unidad         = $_POST["unidad"];
    $afectaigv      = $_POST["afectaigv"];
    $impigv         = $_POST["impigv"];
    $estareg        = $_POST["estareg"];
    $requerido      = $_POST["requerido"];
    $codusu         = $_SESSION['id_user'];

    switch ($Op) {
        case 0:
            $id = $objFunciones->SETCorrelativos("confecpreagudes", $codsuc, "0");
            $nroconfeccion = $id[0];
            $sql = "INSERT INTO solicitudes.confecpreagudes(codemp,codsuc,nroconfeccion,tipoconexion,descripcion,cantidad,preciounitario,total,montoempresa,montousuario,";
            $sql .= "codtipocostos,unimed,afectaigv,impigv,estareg,codusu,requerido,codgrupocostos,codtipopresupuesto)";
            $sql .= "values(".$codemp.",".$codsuc.",".$nroconfeccion.",".$tipoconexion.",'".$descripcion."','".$cantidad."', ";
            $sql .= " '".$preciounitario."', '".$total."', '".$montoempresa."','".$montousuario."', ".$tipocostos.",".$unidad.", ";
            $sql .= " ".$afectaigv.",".$impigv.",".$estareg.",".$codusu.",".$requerido.",".$grupocostos.",".$tipopresupuesto." ) ";
            //$result = $conexion->prepare($sql);
            /*$result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nroconfeccion" => $nroconfeccion, ":tipoconexion" => $tipoconexion, ":descripcion" => $descripcion,
                ":cantidad" => $cantidad, ":preciounitario" => $preciounitario, ":total" => $total, ":montoempresa" => $montoempresa, ":montousuario" => $montousuario,
                ":tipocostos" => $tipocostos, ":unidad" => $unidad, ":afectaigv" => $afectaigv, ":impigv" => $impigv, ":estareg" => $estareg, ":codusu" => $codusu, ":requerido" => $requerido,
                ":codgrupocostos" => $grupocostos, ":codtipopresupuesto" => $tipopresupuesto
                ));*/
            $result = $conexion->query($sql);
            break;
        case 1:
            $sql = "UPDATE solicitudes.confecpreagudes SET 
                    tipoconexion=".$tipoconexion.", descripcion='".$descripcion."',
                    cantidad=".$cantidad.", preciounitario='".$preciounitario."',";
            $sql .= " total='".$total."', montoempresa='".$montoempresa."', montousuario='".$montousuario."',
                    codtipocostos=".$tipocostos.", unimed=".$unidad.", afectaigv=".$afectaigv.", ";
            $sql .= " impigv=".$impigv.", estareg=".$estareg.", codusu=".$codusu.",requerido=".$requerido.",
                    codgrupocostos=".$grupocostos.", codtipopresupuesto=".$tipopresupuesto."
                    WHERE codemp=".$codemp." and codsuc=".$codsuc." and nroconfeccion=".$nroconfeccion." ";
            /*$result = $conexion->prepare($sql);
            $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nroconfeccion" => $nroconfeccion, ":tipoconexion" => $tipoconexion, ":descripcion" => $descripcion,
                ":cantidad" => $cantidad, ":preciounitario" => $preciounitario, ":total" => $total, ":montoempresa" => $montoempresa, ":montousuario" => $montousuario,
                ":tipocostos" => $tipocostos, ":unidad" => $unidad, ":afectaigv" => $afectaigv, ":impigv" => $impigv, ":estareg" => $estareg, ":codusu" => $codusu, ":requerido" => $requerido,
                ":codgrupocostos" => $grupocostos, ":codtipopresupuesto" => $tipopresupuesto
                ));*/
            $result = $conexion->query($sql);
            break;
        case 2:case 3:
            $sql = "UPDATE solicitudes.confecpreagudes SET estareg=:estareg
                WHERE codemp=:codemp and codsuc=:codsuc and nroconfeccion=:nroconfeccion";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nroconfeccion" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
    
?>
