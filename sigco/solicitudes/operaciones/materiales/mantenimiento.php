<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
    
	include("../../../../objetos/clsDrop.php");
	
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc 	= $_SESSION['IdSucursal'];
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsDrop();
	$impigv				= $objMantenimiento->getParamae("IMPIGV",$codsuc);

	if($Id != '')
	{
		$materiales 	= $objMantenimiento->setMateriales(" WHERE codemp=1 AND codsuc=? AND nroconfeccion=?", array($codsuc, $Id));
		$guardar		= $guardar."&Id2=".$Id;
	}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var ImpIgv = <?=$impigv["valor"]?>;
	
	$(document).ready(function() {

	  $('form').keypress(function(e){   
		if(e == 13){
		  return false;
		}
	  });

	  $('input').keypress(function(e){
		if(e.which == 13){
		  return false;
		}
	  });

	});

	function ValidarForm(Op)
	{
		if ($("#tipoconexion").val()==100)
		{
			alert('Seleccione el Tipo de Conexion');
			return false;
		}
		if ($("#codtipopresupuesto").val()=="0")
		{
			alert('Seleccione el Tipo de Presupuesto');
			return false;
		}
		if($("#Descripcion").val()=='')
		{
			alert('La Descripcion del Material no puede ser NULO')
			return false;
		}
		if( $("#PUnitario").val()=='' )
		{
			alert('El Precio Unitario del Material no puede ser NULO')
			return false;
		}
		if($("#tipocostos").val()==0)
		{
			alert('Seleccione el Tipo de Costos')
			return false;
		}
		if($("#grupocostos").val()=='0')
		{
			alert('Seleccione el Grupo de Costos')
			return false;
		}
		if($("#unidad").val()==0)
		{
			alert('Seleccione la Unidad de Medida')
			return false;
		}
		GuardarP(Op)
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function CalcultarTotal(Obj,Id)
	{
	 	var v		= str_replace(document.getElementById(Id).value,",","")
		var Total 	= parseFloat(Obj.value)*v;
		$("#Total").val(parseFloat(Total).toFixed(2))
		
		if($("#afectaigv").val()==1)
		{
			var CIgv  = CalculaIgv(Total,ImpIgv)
		}else{
			var CIgv  = 0
		}
		$("#Igv").val(parseFloat(CIgv).toFixed(2))
	}
	function _CambiarEstado(Check)
	{
		var Total = $("#Total").val()
		var CIgv  = CalculaIgv(Total,ImpIgv)
		
		if(Check.checked==true)
		{
			$("#afectaigv").val(1)
			$("#Igv").val(parseFloat(CIgv).toFixed(2))
		}else{
			$("#afectaigv").val(0)
			$("#Igv").val(parseFloat(0).toFixed(2))
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td width="134" class="TitDetalle">&nbsp;</td>
	  <td width="20" class="TitDetalle">&nbsp;</td>
	  <td colspan="7" class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle" align="right">Nro. Confeccion </td>
      <td class="TitDetalle" align="center">:</td>
      <td colspan="7" class="CampoDetalle">
		<input name="nroconfeccion" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?=$Id?>" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle" align="right">Tipo de Conexion </td>
	  <td class="TitDetalle" align="center">:</td>
	  <td colspan="7" class="CampoDetalle">
    	<? $objMantenimiento->drop_tipoconexion($materiales["tipoconexion"]); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle" align="right">Tipo de Presupuesto </td>
	  <td class="TitDetalle" align="center">:</td>
	  <td colspan="7" class="CampoDetalle">
    	<? $objMantenimiento->drop_tipopresupuesto($materiales["codtipopresupuesto"]); ?>
      </td>
	</tr>	
	<tr>
	  <td class="TitDetalle" align="right">Material</td>
	  <td class="TitDetalle" align="center">:</td>
	  <td colspan="7" class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Descripcion" size="70" maxlength="200" value="<?=htmlspecialchars($materiales["descripcion"])?>" onkeypress="CambiarFoco(event,'Cantidad')"/></td>
	  </tr>
	<tr>
	  <td colspan="9" class="TitDetalle"><hr /></td>
	</tr>	
	<tr>
	  <td class="TitDetalle" align="right">Cantidad</td>
	  <td class="TitDetalle" align="center">:</td>
	  <td width="91" class="CampoDetalle">
	    <input type="text" name="cantidad" id="Cantidad" class="inputtext" size="10" value="<?=isset($materiales["cantidad"])?$materiales["cantidad"]:1?>" onkeyup="CalcultarTotal(this,'PUnitario');" onkeypress="CambiarFoco(event,'PUnitario');" />
      </td>
	  <td width="96" class="CampoDetalle" align="right">P. Unitario</td>
	  <td width="18" class="CampoDetalle" align="center">:</td>
	  <td width="89" class="CampoDetalle">
      	<input type="text" name="preciounitario" id="PUnitario" class="inputtext" size="10" value="<?=isset($materiales["preciounitario"])?$materiales["preciounitario"]:"0.00"?>" onkeyup="CalcultarTotal(this,'Cantidad');" onkeypress="CambiarFoco(event,'MontEmpresa');return permite(event,'num');" />
      </td>
	  <td width="47" class="CampoDetalle" align="right">Total</td>
	  <td width="17" class="CampoDetalle" align="center">:</td>
	  <td width="157" class="CampoDetalle">
      	<input type="text" name="total" id="Total" class="inputtext" size="10" value="<?=isset($materiales["total"])?$materiales["total"]:"0.00"?>"  readonly="readonly"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle" align="right">Monto Empresa </td>
	  <td class="TitDetalle" align="center">:</td>
	  <td class="CampoDetalle">
      	<input type="text" name="montoempresa" id="MontEmpresa" class="inputtext" size="10" value="<?=isset($materiales["montoempresa"])?$materiales["montoempresa"]:"0.00"?>" onkeypress="CambiarFoco(event,'MontUsuario');return permite(event,'num');" /></td>
	  <td class="CampoDetalle" align="right">Monto Usuario</td>
	  <td class="CampoDetalle" align="center">:</td>
	  <td class="CampoDetalle"><input type="text" name="montousuario" id="MontUsuario" class="inputtext" size="10" value="<?=isset($materiales["montousuario"])?$materiales["montousuario"]:"0.00"?>" onkeypress="CambiarFoco(event,'MontUsuario');return permite(event,'num');" /></td>
	  <td class="CampoDetalle" align="right">&nbsp;</td>
	  <td class="CampoDetalle" align="center">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="9" align="right" class="TitDetalle"><hr /></td>
	  </tr>
	<tr>
	  <td class="TitDetalle" align="right">Tipo de Costos </td>
	  <td class="TitDetalle" align="center">:</td>
	  <td colspan="7" class="CampoDetalle">
    	<? $objMantenimiento->drop_tipocostos($materiales["codtipocostos"]); ?>  
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle" align="right">Grupo de Costos </td>
	  <td class="TitDetalle" align="center">:</td>
	  <td colspan="7" class="CampoDetalle">
    	<? $objMantenimiento->drop_grupocostos($materiales["codgrupocostos"]); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle" align="right">Unidad de Medida </td>
	  <td class="TitDetalle" align="center">:</td>
	  <td colspan="7" class="CampoDetalle">
    	<? $objMantenimiento->drop_unidad_medida($materiales["unimed"]); ?>    
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle" align="right">Afecta Igv </td>
	  <td class="TitDetalle" align="center">:</td>
	  <td class="CampoDetalle">
	    <input type="checkbox" name="AfectaIgv" value="checkbox" onclick="_CambiarEstado(this)" <?=$d="";$materiales["afectaigv"]?$d="checked='checked'":$d="";echo $d;?> />
	    <input type="hidden" name="afectaigv" id="afectaigv" value="<?=isset($materiales["afectaigv"])?$materiales["afectaigv"]:0?>" />
      </td>
	  <td class="CampoDetalle" align="right">Imp. Igv </td>
	  <td class="CampoDetalle" align="center">:</td>
	  <td class="CampoDetalle">
      	<input type="text" name="impigv" id="Igv" class="inputtext" size="10" value="<?=isset($materiales["impigv"])?$materiales["impigv"]:0.00?>" readonly="readonly" />
      </td>
	  <td class="CampoDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
	  <td class="TitDetalle" align="right">Requerido </td>
	  <td class="TitDetalle" align="center">:</td>
	  <td class="CampoDetalle">
	    <input type="checkbox" value="checkbox" onclick="CambiarEstado(this,'requerido')" <?=$d="";$materiales["requerido"]?$d="checked='checked'":$d="";echo $d;?> />
	    <input type="hidden" name="requerido" id="requerido" value="<?=isset($materiales["requerido"])?$materiales["requerido"]:0?>" />
      </td>
	  <td class="CampoDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td colspan="7" class="CampoDetalle">
      	<? include("../../../../include/estareg.php"); ?>
      </td>
	</tr>
   </tbody>
 
  </table>
 </form>
</div>
<?php
	$est = isset($materiales["estareg"])?$materiales["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>