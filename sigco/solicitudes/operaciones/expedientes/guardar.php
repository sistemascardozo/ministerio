<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include('../../../../objetos/clsFunciones.php');
include("../../../../objetos/clsseguridad.php");

$objSeguridad = new clsSeguridad();
$conexion->beginTransaction();
$objFunciones = new clsFunciones();

$cont = $_POST["contador"];
$codemp = 1;
$codsuc = $_SESSION['IdSucursal'];
$codsistema = $_SESSION['idsistema'];
$nroinspeccion = $_POST["nroinspeccion"] ? $_POST["nroinspeccion"] : 0;
$nroinscripcion = $_POST["nroinscripcion"];
$interes = $_POST["interescredito"] ? $_POST["interescredito"] : 0;
$Op = $_GET['Op'];
$codcliente = $_POST['codcliente'];
$propietario = $_POST['propietario'];
$direccion = $_POST['direccion'];
$fechaemision = $objFunciones->CodFecha($_POST['fechaemision']);
$tipopago = $_POST["tipopago"];
$subtotal = str_replace(",", "", $_POST["subtotal"]);
$igv = str_replace(",", "", $_POST["igv"]);
$redondeo = str_replace(",", "", $_POST["redondeo"]);
$total = str_replace(",", "", $_POST["imptotal"]);
$cuotainicial = str_replace(",", "", $_POST["minicial"]);
$nrocuota = $_POST["nrocuotas"] != "" ? $_POST["nrocuotas"] : 0;
$cuotames = $_POST["cmensual"] != "" ? $_POST["cmensual"] : 0.00;
$fechareg = date('Y-m-d');
$codusu = $_SESSION['id_user'];
$interes = $_POST['interescredito'];
$nropago = 0;
$concredito = 0;
$nrocontrato = 0;
$codexpediente = $_POST['codexpediente'];
$nroexpediente = $_POST['nroexpediente'];
$documento = $_POST['documento'];
$tipopresupuesto = $_POST['tipopresupuesto'];
switch ($Op) {
    case 0:
        $codexpediente = $objFunciones->setCorrelativos("codexpediente", $codsuc, "0");
        $codexpediente = $codexpediente[0];
        $nroexpediente = $objFunciones->setCorrelativosVarios(18, $codsuc, "SELECT", 0);
        $Sql = "INSERT INTO solicitudes.expedientes 
                        (codemp,codsuc,codexpediente, codcliente,  propietario,  direccion,  fechaemision,  tipopago,
                          subtotal,  igv,  redondeo,  total,  cuotainicial,  nrocuota,  cuotames,
                         fechareg,  codusu,  interes,  nropago,  concredito,  nrocontrato,  nroexpediente,nroinscripcion,documento, codtiposervicio) 
                VALUES (  :codemp,  :codsuc,  :codexpediente,  :codcliente,  :propietario,  :direccion,  :fechaemision, :tipopago, 
                  :subtotal,  :igv,  :redondeo,  :total,  :cuotainicial,  :nrocuota,:cuotames,  
                  :fechareg,  :codusu,  :interes,  :nropago,  :concredito,  :nrocontrato,  :nroexpediente,:nroinscripcion,:documento, :tipopresupuesto);";

        $result = $conexion->prepare($Sql);
        $result->execute(array(
            ":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":codexpediente" => $codexpediente,
            ":codcliente" => $codcliente,
            ":propietario" => $propietario,
            ":direccion" => $direccion,
            ":fechaemision" => $fechaemision,
            ":tipopago" => $tipopago,
            ":subtotal" => $subtotal,
            ":igv" => $igv,
            ":redondeo" => $redondeo,
            ":total" => $total,
            ":cuotainicial" => $cuotainicial,
            ":nrocuota" => $nrocuota,
            ":cuotames" => $cuotames,
            ":fechareg" => $fechareg,
            ":codusu" => $codusu,
            ":interes" => $interes,
            ":nropago" => $nropago,
            ":concredito" => $concredito,
            ":nrocontrato" => $nrocontrato,
            ":nroexpediente" => $nroexpediente,
            ":nroinscripcion" => $nroinscripcion,
            ":documento" => $documento,
            ":tipopresupuesto" => $tipopresupuesto
        ));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error UPDATE Solicitud";
            $data['res'] = 2;
            die(json_encode($data));
        }

        break;
    case 1:

        $Sql = "UPDATE  solicitudes.expedientes  
                SET   tipopago = :tipopago,subtotal = :subtotal,  igv = :igv,  redondeo = :redondeo,
              total = :total,  cuotainicial = :cuotainicial,  nrocuota = :nrocuota,  cuotames = :cuotames,interes = :interes
            WHERE codemp = :codemp AND codsuc = :codsuc AND  codexpediente = :codexpediente ;";

        $result = $conexion->prepare($Sql);
        $result->execute(array(
            ":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":codexpediente" => $codexpediente,
            ":tipopago" => $tipopago,
            ":subtotal" => $subtotal,
            ":igv" => $igv,
            ":redondeo" => $redondeo,
            ":total" => $total,
            ":cuotainicial" => $cuotainicial,
            ":nrocuota" => $nrocuota,
            ":cuotames" => $cuotames,
            ":interes" => $interes
        ));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error UPDATE Solicitud";
            $data['res'] = 2;
            die(json_encode($data));
        }

        $Sql = "DELETE FROM solicitudes.precreditos WHERE codemp=? AND codsuc=? AND  codexpediente=? ;";
        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array($codemp, $codsuc, $codexpediente));
        $Sql = "DELETE FROM solicitudes.expedientesdetalle WHERE codemp=? AND codsuc=? AND  codexpediente=? ;";
        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array($codemp, $codsuc, $codexpediente));
        break;
    case 2:
        $Sql = "UPDATE  solicitudes.expedientes  
                        SET  estadoexpediente=3
                    WHERE codemp = :codemp AND codsuc = :codsuc AND  codexpediente = :codexpediente ;";

        $result = $conexion->prepare($Sql);
        $result->execute(array(
            ":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":codexpediente" => $codexpediente
        ));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error UPDATE Solicitud";
            $data['res'] = 2;
            die(json_encode($data));
        }
        //LIBERAR PRESUPUESTOS
        $Sql = "SELECT nropresupuesto,nrosolicitud FROM  solicitudes.expedientesdetalle 
            WHERE codemp=? AND codsuc=? AND  codexpediente=?";
        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array($codemp, $codsuc, $codexpediente));
        $itemsD = $consulta->fetchAll();
        foreach ($itemsD as $rowD) {
            if ($rowD["nrosolicitud"] != '' && $rowD["nrosolicitud"] != '0') {
                $consulta2 = $conexion->prepare("SELECT * FROM solicitudes.solicitudes where codemp=1 and codsuc=? and nrosolicitud=?");
                $consulta2->execute(array($codsuc, $rowD["nrosolicitud"]));
                $items2 = $consulta2->fetch();
                $nroinspeccion = $items2['nroinspeccion'];
                if ($nroinspeccion != "" && $nroinspeccion != 0) {
                    $upd = "UPDATE catastro.solicitudinspeccion
                                    SET estadopresupuesto= 1   
                                    WHERE codemp=1 AND  codsuc=".$codsuc." and nroinspeccion=:nroinspeccion";
                    $result = $conexion->prepare($upd);
                    $result->execute(array(":nroinspeccion" => $nroinspeccion));
                    if ($result->errorCode() != '00000') {
                        $conexion->rollBack();
                        $mensaje = "Error UPDATE cabpagos";
                        $data['res'] = 2;
                        die(json_encode($data));
                    }
                }
            }
            $upd = "UPDATE solicitudes.cabpresupuesto SET
                    tipopago=?,estadopresupuesto=1
                    WHERE codemp=? and codsuc=? and nropresupuesto=?";
            $result = $conexion->prepare($upd);
            $result->execute(array($tipopago, $codemp, $codsuc, $rowD["nropresupuesto"]));

            if ($result->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error UPDATE Solicitud";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
        $objSeguridad->seguimiento($codemp, $codsuc, $codsistema, $codusu, "SE HA ANULADO EL EXPEDIENTE CON CODIGO ".$codexpediente, 145);
        break;
}
if ($Op == 0 || $Op == 1) {
    //PRECREDITOS
    for ($i = 1; $i <= $cont; $i++)
	{
        if (isset($_POST["nrocuota".$i]))
		{
            $sql = "INSERT INTO solicitudes.precreditos(codemp,codsuc,codexpediente,nrocuota,totalcuotas,cuotamensual,interes,
                igv,redondeo,imptotal) VALUES(:codemp,:codsuc,:codexpediente,:nrocuota,:totalcuotas,:cuotamensual,:interes,:igv,
                :redondeo,:imptotal)";
				
            $result = $conexion->prepare($sql);
            $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":codexpediente" => $codexpediente,
                ":nrocuota" => $_POST["nrocuota".$i], ":totalcuotas" => $_POST["totcuotas".$i],
                ":cuotamensual" => $_POST["cuotames".$i], ":interes" => $_POST["interes".$i],
                ":igv" => $_POST["impigv".$i], ":redondeo" => $_POST["red".$i], ":imptotal" => $_POST["tot".$i]));
				
            if ($result->errorCode() != '00000')
			{
                $conexion->rollBack();
                $mensaje = "Error UPDATE Solicitud";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }

    //PRESUPUESTOS
    $NroItems = $_POST['NroItems'];
	
    for ($i = 1; $i <= $NroItems; $i++) {
        if (isset($_POST["NroPresupuestoC".$i]))
		{
            $nrosol= $_POST["NroSolicitud".$i];
            
            $sqlD = "INSERT INTO solicitudes.expedientesdetalle 
                (  codemp,  codsuc,  codexpediente, nropresupuesto, codtipopresupuesto,  codconcepto, nrosolicitud,  subtotal,  igv,  redondeo,  total) 
                VALUES (  :codemp,  :codsuc,  :codexpediente, :nropresupuesto, :codtipopresupuesto, :codconcepto, :nrosolicitud, :subtotal,  :igv,  :redondeo, :total);";

            $resultD = $conexion->prepare($sqlD);
            $resultD->execute(array(":codemp" => $codemp,
                ":codsuc" => $codsuc,
                ":codexpediente" => $codexpediente,
                ":nropresupuesto" => $_POST['NroPresupuestoC'.$i],
                ":codtipopresupuesto" => $_POST['CodTipoPresupuestoC'.$i],
                ":codconcepto" => $_POST['codconceptoC'.$i],
                ":nrosolicitud" => $_POST["NroSolicitud".$i],
                ":subtotal" => str_replace(",", "", $_POST["subtotalC".$i]),
                ":igv" => str_replace(",", "", $_POST["igvC".$i]),
                ":redondeo" => str_replace(",", "", $_POST["redondeoC".$i]),
                ":total" => str_replace(",", "", $_POST["imptotalC".$i]),
            ));
            if ($resultD->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                $data['res'] = 2;
                die(json_encode($data));
            }
            if ($_POST["NroSolicitud".$i] != '' && $_POST["NroSolicitud".$i] != '0') {
                $consulta2 = $conexion->prepare("SELECT * FROM solicitudes.solicitudes where codemp=1 and codsuc=? and nrosolicitud=?");
                $consulta2->execute(array($codsuc, $_POST["NroSolicitud".$i]));
                $items2 = $consulta2->fetch();
                $nroinspeccion = $items2['nroinspeccion'];
                if ($nroinspeccion != "" && $nroinspeccion != 0) {
                    $upd = "UPDATE catastro.solicitudinspeccion
                        SET estadopresupuesto= 2   
                        WHERE codemp=1 AND  codsuc=".$codsuc." and nroinspeccion=:nroinspeccion";
                    $result = $conexion->prepare($upd);
                    $result->execute(array(":nroinspeccion" => $nroinspeccion));
                    if ($result->errorCode() != '00000') {
                        $conexion->rollBack();
                        $mensaje = "Error UPDATE cabpagos";
                        $data['res'] = 2;
                        die(json_encode($data));
                    }
                }
            }
            $upd = "UPDATE solicitudes.cabpresupuesto SET
                    tipopago=?,estadopresupuesto=2,codexpediente=?
                    WHERE codemp=? and codsuc=? and nropresupuesto=?";
            $result = $conexion->prepare($upd);
            $result->execute(array($tipopago, $codexpediente, $codemp, $codsuc, $_POST["NroPresupuestoC".$i]));

            if ($result->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error UPDATE Solicitud";
                $data['res'] = 2;
                die(json_encode($data));
            }

            //ACTUALIZAR SOLICITUDES DE CONECIONES
            if ($_POST["NroSolicitud".$i] != '' && $_POST["NroSolicitud".$i] != '0') {
                $Sql = "UPDATE solicitudes.solicitudes set estareg=2 WHERE codemp=:codemp and codsuc=:codsuc and nrosolicitud=:nrosolicitud";
                $result = $conexion->prepare($Sql);
                $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nrosolicitud" => $_POST["NroSolicitud".$i]));
                if ($result->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "Error detpresupuesto";
                    $data['res'] = 2;
                    die(json_encode($data));
                }
            }
        }
    }

    //INSERTAR REDONDEO
//        $Red = $_POST['redondeo'];
//        if ($Red <> 0) {
//            $CodConcep = 7;
//
//            if ($Red > 0) {
//                $CodConcep = 8;
//            }
//
//            $redondeo = str_replace(",", "", $Red);
//
//
//            $sqlD = "INSERT INTO solicitudes.expedientesdetalle(  codemp,  codsuc,  codexpediente, nropresupuesto,";
//            $sqlD .= "codtipopresupuesto,  codconcepto, nrosolicitud,  subtotal,  igv,  redondeo,  total) 
//            VALUES ( $codemp, $codsuc, $codexpediente, 0, 0, $CodConcep,";
//            $sqlD .= " $nrosol, '$redondeo', '0.00',  '0.00', '$redondeo') ";
//
//            $resultD = $conexion->prepare($sqlD);
//            $resultD->execute(array());
//            if ($resultD->errorCode() != '00000') {
//                $conexion->rollBack();
//                $data['res'] = 'Error REDONDEO';
//                die(json_encode($data));
//            }
//        }
    
}


$conexion->commit();
if ($Op == 0)
    $objFunciones->setCorrelativosVarios(18, $codsuc, "UPDATE", $nroexpediente);
$mensaje = "El Registro se ha Grabado Correctamente";
$data['res'] = 1;
echo json_encode($data);

?>
