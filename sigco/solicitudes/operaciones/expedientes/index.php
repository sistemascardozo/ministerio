<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$Criterio = 'Expediente';
$TituloVentana = "GENERACIÓN DE EXPEDIENTES";
$Activo = 1;
$Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : 200;

if ($Op != 5 && $Op != 6 && $Op != 7) {
    CuerpoSuperior($TituloVentana);
}

$codsuc = $_SESSION['IdSucursal'];
$documento = 2;
$and = "";

if ($Op == 5) {
    $and = " AND (c.estadoexpediente = 4 OR c.estadoexpediente = 6) AND c.concontrato = 0";
}
//---Para la Pantalla de Creditos------------------
if ($Op == 7) {
    $and = " AND c.estadoexpediente IN(1) AND c.tipopago IN(1, 2)";
}
//---Para la Pantalla de Pagos------------------
if ($Op == 6) {
    $and = " AND c.estadoexpediente IN(1) AND c.tipopago IN(1) ";
}

$FormatoGrilla = array();

$Sql = "SELECT c.nroexpediente, " . $objFunciones->FormatFecha('c.fechaemision') . ", ";
$Sql .= " CASE WHEN c.tipopago = 1 THEN 'CONTADO' ELSE 'CREDITO' END, c.codcliente, c.propietario, c.direccion AS direccion, ";
$Sql .= " " . $objFunciones->Convert("c.total", "NUMERIC (18, 2)") . ", ep.descripcion AS estadopresupuesto, c.estadoexpediente, c.codexpediente, 1 ";
$Sql .= "FROM solicitudes.expedientes c ";
$Sql .= " INNER JOIN public.estadopresupuesto ep ON(ep.estadopresupuesto = c.estadoexpediente)";

$FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL                                                           //Sentencia SQL
$FormatoGrilla[1] = array('1' => 'c.codexpediente', '2' => 'c.codcliente', '3' => 'c.propietario', '4' => 'c.nroexpediente', '5' => 'c.nrocontrato');          //Campos por los cuales se hará la búsqueda
$FormatoGrilla[2] = $Op;                                                            //Operacion
$FormatoGrilla[3] = array('T1' => 'Nro. Exp.', 'T2' => 'F. Emision', 'T3' => 'Forma Pago', 'T4' => 'Cod. Cliente', 'T5' => 'Propietario',
    'T6' => 'Direccion', 'T7' => 'Total', 'T8' => 'Estado');   //Títulos de la Cabecera
$FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'center', 'A3' => 'center', 'A4' => 'center', 'A7' => 'right', 'A8' => 'center');                        //Alineación por Columna
$FormatoGrilla[5] = array('W1' => '60', 'W2' => '40', 'W4' => '70', 'W8' => '180', 'W8' => '60', 'W9' => '40');                                 //Ancho de las Columnas
$FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
$FormatoGrilla[7] = 1080;                                                            //Ancho de la Tabla
$FormatoGrilla[8] = "  AND (c.codemp = 1 AND c.codsuc = " . $codsuc . $and . ") ORDER BY c.codexpediente DESC ";                               //Orden de la Consulta

if ($Op != 5 && $Op != 6 && $Op != 7)
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '3', //Número de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'modificar.png', //Imagen a mostrar
        'Btn1' => 'Editar', //Titulo del Botón
        'BtnF1' => 'onclick="Modificar(this);"', //Eventos del Botón
        'BtnCI1' => '11', //Item a Comparar
        'BtnCV1' => '1', //Valor de comparación
        'BtnId2' => 'BtnEliminar',
        'BtnI2' => 'eliminar.png',
        'Btn2' => 'Anula Expediente',
        'BtnF2' => 'onclick="Anular(this)"',
        //'BtnF2'=>'onclick="Anular(this)"', 
        'BtnCI2' => '11', //campo 3
        'BtnCV2' => '1', //igua a 1
        'BtnId3' => 'BtnRestablecer', //y aparece este boton
        'BtnI3' => 'imprimir.png',
        'Btn3' => 'Imprimir',
        'BtnF3' => 'onclick="Imprimir(this)"',
        'BtnCI3' => '11',
        'BtnCV3' => '1',
        'BtnId4' => 'BtnGenerar', //y aparece este boton
        'BtnI4' => 'documento.png',
        'Btn4' => 'Atender Presupuesto',
        'BtnF4' => 'onclick="AtenderPresupuesto(this)"',
        'BtnCI4' => '11',
        'BtnCV4' => '1');
else {
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '1', //Número de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'ok.png', //Imagen a mostrar
        'Btn1' => 'Seleccionar', //Titulo del Botón
        'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
        'BtnCI1' => '11', //Item a Comparar
        'BtnCV1' => '1'    //Valor de comparación
    );
}

$FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 10), array('Name' => 'estado', 'Col' => 9)); //DATOS ADICIONALES           
$FormatoGrilla[11] = 8; //FILAS VISIBLES
$_SESSION['Formato' . $Criterio] = $FormatoGrilla;

Cabecera('', $FormatoGrilla[7], 1000, 500);
Pie();
?>
<script type="text/javascript">
    var urldir = "<?php echo $_SESSION['urldir']; ?>"

    $(document).ready(function () {

    })
    /*
     function Anular(obj)
     {
     var Id = $(obj).parent().parent().data('id')
     var html = '<form id="form1" name="form1">';
     html+='<input type="hidden" name="1form1_id" id="Id" value="'+Id+'" />';
     html+='</form>'
     $("#DivEliminacion").html(html);
     $("#ConfirmaEliminacion").dialog("open");
     }
     */
    /* function Operacion(Op)
     {
     if(Op!=4)
     {
     if(est!=3)
     {
     var url = 'mantenimiento.php?Op='+Op;
     if(Op!=1)
     {
     if(Id=="")
     {
     alert('Seleccione un Elemento del Item')
     return
     }
     url=url+'&'+Id;
     }
     if(Op!=5)
     {
     if(est==5 && Op!=1)
     {
     alert('No se puede modificar puede que el presupuesto se encuentre transferido al Catastro de Usuarios');
     return
     }
     location.href=url;
     }else{
     
     }
     }else{
     alert('El Presupuesto se Encuentra Anulado')
     }
     }else{
     location.href='<?=$urldir ?>/admin/indexB.php'
     }
     }*/


    function Imprimir(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        var url = '';

        url = 'imprimir.php';

        AbrirPopupImpresion(url + '?Id=' + Id + '&codsuc=<?=$codsuc ?>', 800, 600)
    }


    function Modificar(obj)
    {
        var Estado = $(obj).parent().parent().data('estado')
        if (Estado == 3)
        {
            alert('No se puede Modificar el Expediente...Se encuentra Anulado')
            return
        }
        if (Estado == 4)
        {
            alert('No se puede Modificar el Expediente...Se encuentra Pagado')
            return
        }
        if (Estado == 6)
        {
            alert('No se puede Modificar el Expediente...Se encuentra Financiado')
            return
        }
        var Id = $(obj).parent().parent().data('id')
        //location.href='atender?Id='+Id+'&codsuc=<?=$codsuc ?>';
        $("#Modificar").dialog({title: 'Modificar Expediente'});

        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $("#button-maceptar").children('span.ui-button-text').text('Modificar Expediente')
        $.ajax({
            url: 'mantenimiento.php?Op=1',
            type: 'POST',
            async: true,
            data: 'Id=' + Id + '&codsuc=<?=$codsuc ?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function AtenderPresupuesto(obj)
    {
        var Estado = $(obj).parent().parent().data('estado')
        if (Estado == 2)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Atendido')
            return
        }
        if (Estado == 3)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Anulado')
            return
        }
        if (Estado == 4)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Pagado')
            return
        }
        if (Estado == 5)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Transferido al Catastro de Usuarios')
            return
        }
        var Id = $(obj).parent().parent().data('id')
        //location.href='atender?Id='+Id+'&codsuc=<?=$codsuc ?>';
        $("#Modificar").dialog({title: 'Atender Presupuesto'});

        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function () {
            GuardarOk();
        });
        $("#button-maceptar").children('span.ui-button-text').text('Atender Presupuesto')
        $.ajax({
            url: 'atender/index.php',
            type: 'POST',
            async: true,
            data: 'Id=' + Id + '&codsuc=<?=$codsuc ?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function GuardarOk()
    {
        $.ajax({
            url: 'atender/Guardar.php?Op=0',
            type: 'POST',
            async: true,
            data: $('#form1').serialize(),
            success: function (data) {
                //alert(data);
                OperMensaje(data)
                $("#Mensajes").html(data);
                $("#DivModificar").html('');
                $("#Modificar").dialog("close");
                Buscar(0);
            }
        })
    }
    function Enviar(obj)
    {
        var Id = $(obj).parent().parent().data('id')

        opener.Recibir(Id);

        window.close();
    }
    function Anular(obj)
    {

        var Estado = $(obj).parent().parent().data('estado')
        if (Estado == 3)
        {
            alert('No se puede Anular el Expediente...Se encuentra Anulado')
            return
        }
        if (Estado == 4)
        {
            alert('No se puede Anular el Expediente...Se encuentra Pagado')
            return
        }
        $("#form1").remove();
        $("#DivEliminacion").html('');
        var Id = $(obj).parent().parent().data('id')
        var html = '<form id="form1" name="form1">';
        html += '<input type="hidden" name="codexpediente" id="Id" value="' + Id + '" />';
        html += '<input type="hidden" name="estareg" id="estareg" value="0" />';
        html += '</form>'
        $("#DivEliminacion").html(html);
        $("#ConfirmaEliminacion").dialog("open");
    }

</script>

<?php
if ($Op != 5 && $Op != 6 && $Op != 7)
    CuerpoInferior();
else {
    ?>
    <script type="text/javascript">$("#BtnNuevoB").hide();</script>
<?php } ?>