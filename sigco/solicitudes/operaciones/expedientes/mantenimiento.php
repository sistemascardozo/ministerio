<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");

    $objMantenimiento = new clsMantenimiento();
    $Op = $_GET["Op"];
    $id = $_POST["Id"];
    $codsuc = $_SESSION['IdSucursal'];
    $paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);
    $fecha = date("d/m/Y");

    if ($id != '') {
        $consulta = $conexion->prepare("select * from solicitudes.expedientes where codemp=1 and codsuc=? and codexpediente=?");
        $consulta->execute(array($codsuc, $id));
        $items = $consulta->fetch();
        $fech = new DateTime($items["fechaemision"]);
        $fecha = $fech->format("d/m/Y");

		$Intttt = $items['interes'];

		if ($Intttt == 0)
		{
			$sql = "select nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
                                    from facturacion.periodofacturacion
                                    where codemp=1 and codsuc=".$codsuc." and codciclo=1 and facturacion=0";
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array());
			$row = $consulta->fetch();

			$Intttt = $row['tasapromintant'];
		}

        $TasaInteres = $Intttt / 100;
        $tasapromintant = $Intttt;
    } else {
        $sql = "select nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
                                    from facturacion.periodofacturacion
                                    where codemp=1 and codsuc=".$codsuc." and codciclo=1 and facturacion=0";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $row = $consulta->fetch();
        $TasaInteres = $row['tasapromintant'] / 100;
        $tasapromintant = $row['tasapromintant'];
    }
    ////////77
?>
<style type="text/css">
    .CodTipoPresupuesto,.NroPresupuesto,.CodConcepto,.NroSolicitud{display: none;}
</style>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    var IdAnt = ''
    var filaant = 0
    var impigv = <?=$paramae["valor"]?>;
    var contador = 0;
    var cont = 0;

    $(document).ready(function () {
        $("#tabs").tabs();
        FormaPagoUpd()
        $('form').keypress(function (e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
        });

    });

    function ValidarForm(Op)
    {
        if ($("#codcliente").val() == 0 || $("#codcliente").val() == '')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#codcliente"), "Seleccione un Usuario")
            return false
        }
        $("#codcliente").removeClass("ui-state-error");

        if (parseInt($("#TbIndex tbody tr").length) == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#TbIndex"), "Cliente no presenta Presupuestos Pendientes")
            return false
        }
        if ($("#contador").val() == 0 && $("#tipopago").val() == 2)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#tbcreditos"), 'No hay ningun cuota creada')
            return false
        }
				$(".ui-dialog-buttonpane button:contains('Agregar')").button("disable");
        GuardarAPresupuesto(Op)
    }
    function GuardarAPresupuesto(Op)
    {   //alert(Op)

        ///CALCULAR IMPUT DE COLATERALES
        var id = parseInt($("#TbIndex tbody tr").length)
        var tr = '';
        var NroPresupuesto, SubTotal, IGV, Redondeo, CodTipoPresupuesto, CodConcepto, NroSolicitud = '';
        for (var i = 1; i <= id; i++)
        {

            CodConcepto = $("#TbIndex tbody tr#"+i+" label.CodConcepto").text()
            NroPresupuesto = $("#TbIndex tbody tr#"+i+" label.NroPresupuesto").text()
            CodTipoPresupuesto = $("#TbIndex tbody tr#"+i+" label.CodTipoPresupuesto").text()
            NroSolicitud = $("#TbIndex tbody tr#"+i+" label.NroSolicitud").text()
            SubTotal = $("#TbIndex tbody tr#"+i+" label.SubTotal").text()
            IGV = $("#TbIndex tbody tr#"+i+" label.IGV").text()
            Redondeo = $("#TbIndex tbody tr#"+i+" label.Redondeo").text()
            Importe = $("#TbIndex tbody tr#"+i+" label.Importe").text()
            //Colateral   =$("#TbIndex tbody tr#"+i+" label.Colateral").text()
            SubTotal = str_replace(SubTotal, ',', '');
            IGV = str_replace(IGV, ',', '');
            Redondeo = str_replace(Redondeo, ',', '');
            Importe = str_replace(Importe, ',', '');

            tr += '<input type="hidden" name="codconceptoC'+i+'"  value="'+CodConcepto+'"/>';
            tr += '<input type="hidden" name="CodTipoPresupuestoC'+i+'"  value="'+CodTipoPresupuesto+'"/>';
            tr += '<input type="hidden" name="NroPresupuestoC'+i+'"  value="'+NroPresupuesto+'"/>';
            tr += '<input type="hidden" name="subtotalC'+i+'"  value="'+SubTotal+'"/>';
            tr += '<input type="hidden" name="igvC'+i+'"  value="'+IGV+'"/>';
            tr += '<input type="hidden" name="redondeoC'+i+'"  value="'+Redondeo+'"/>';
            tr += '<input type="hidden" name="imptotalC'+i+'"  value="'+Importe+'"/>';
            tr += '<input type="hidden" name="NroSolicitud'+i+'"  value="'+NroSolicitud+'"/>';
        }
        tr += '<input type="hidden" name="NroItems" id="Items"  value="'+id+'"/>';
        $("#DivSave").html(tr)
        ///CALCULAR IMPUT DE COLATERALES


        $.ajax({
            url: 'guardar.php?Op=<?=$Op?>',
            type: 'POST',
            async: true,
            dataType: "json",
            data: $('#form1').serialize(), //+'&0form1_idusuario=<?=$IdUsuario?>&3form1_fechareg=<?=$Fecha ?>',
            success: function (datos) {

							  $(".ui-dialog-buttonpane button:contains('Agregar')").button("enable");
                OperMensaje(datos.res)
                $("#Mensajes").html(datos.res);
                $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                $("#Nuevo").dialog("close");
                $("#Modificar").dialog("close");
                $("#ConfirmaEliminacion").dialog("close");
                $("#ConfirmaRestauracion").dialog("close");
                /*if(datos.res==1)
                 {
                 if(confirm("Desea Imprimir el Presupuesto de Conexion?"))
                 AbrirPopupImpresion(urldir+"sigco/solicitudes/operaciones/presupuesto/imprimir_ficha.php?Id="+datos.nropresupuesto+"&codsuc=<?=$codsuc ?>",800,600)
                 }*/
                Buscar(Op);
            }
        })
    }
    function Cancelar()
    {
        location.href = '../';
    }
    function Agregar()
    {
        var Dest = 1;
        var nrocuotas = $("#nrocuotas").val();
        var montoinicial = $("#minicial").val();
        var cuotames = $("#cmensual").val();
        var stotal = 0;

        /*if(montoinicial=="" || montoinicial==0 || montoinicial==0.00)
         {
         alert('El Monto Inicial no es Valido')
         return
         }*/
        if (nrocuotas == '' || nrocuotas == 0)
        {
            Msj($("#nrocuotas"), 'El Numero de Cuotas no es valido')
            return
        }
        if (parseFloat(montoinicial) > parseFloat($("#stotal").val()))
        {
            Msj($("#minicial"), 'El Monto Inicial no puede ser Mayor al Monto Total del Presupuesto')
            return
        }
        $("#tbcreditos tbody").html('');

        var cuota = 0;
        var K = str_replace($("#francionar").val(), ",", "");

        for (i = 1; i <= nrocuotas; i++)
        {
            var interes = K * <?=$TasaInteres?>;
            interes = parseFloat(interes).toFixed(2)
            //var igv = ((cuotames - interes)/100) * impigv;
			var igv = CalculaIgv((cuotames - interes) / 1.18, impigv);
            //var igv = impigv / 100 * K
            cuota = cuotames - igv - interes

			//var igv = CalculaIgv(cuota, impigv);
            var imp = parseFloat(cuota) + parseFloat(igv) + parseFloat(interes);
            var redondeo = CalcularRedondeo(imp)
            var total = parseFloat(imp) + parseFloat(redondeo)

            stotal = Round(parseFloat(stotal) + parseFloat(total), 1)

            $("#tbcreditos tbody").append(
                    "<tr align='center'>" +
                    "<td><input type='hidden' value='" + i + "' name='nrocuota" + i + "' />" + i + "</td>" +
                    "<td><input type='hidden' value='" + nrocuotas + "' name='totcuotas" + i + "' />" + i + "/" + nrocuotas + "</td>" +
                    "<td><input type='hidden' value='"+parseFloat(cuota).toFixed(2)+"' name='cuotames"+i+"' id='cuotames"+i+"' /><label id='cuotamesx"+i+"'>"+parseFloat(cuota).toFixed(2)+"</label></td>" +
                    "<td><input type='hidden' value='"+interes+"' name='interes"+i+"' />"+parseFloat(interes).toFixed(2)+"</td>" +
                    "<td><input type='hidden' value='"+parseFloat(igv).toFixed(2)+"' name='impigv"+i+"' id='impigv"+i+"' /><label id='impigvx"+i+"'>"+parseFloat(igv).toFixed(2)+"</label></td>" +
                    "<td><input type='hidden' value='"+parseFloat(redondeo).toFixed(2)+"' name='red"+i+"' id='red"+i+"' /><label id='redx"+i+"'>"+parseFloat(redondeo).toFixed(2)+"</label></td>" +
                    "<td><input type='hidden' value='"+parseFloat(total).toFixed(2)+"' name='tot"+i+"' id='tot"+i+"' /><label id='totx"+i+"'>"+parseFloat(total).toFixed(2)+"</label></td>" +
                    "</tr>"
                    );

            K = K - cuota
        }

        $("#contador").val(nrocuotas)

    }

    function Inicial(obj)
    {
        var inicial = obj.value;

        if (inicial == '')
		{
            inicial = 0;
        }

		var totalpresupuesto = $("#total").val();

		totalpresupuesto = str_replace(totalpresupuesto, ',', '');

		var montoref = parseFloat(totalpresupuesto) - parseFloat(inicial);

		$("#francionar").val(parseFloat(montoref).toFixed(2));

		Cuotas($("#nrocuotas"));
	}

    function Cuotas(obj)
    {
        var montoref = str_replace($("#francionar").val(), ",", "");
        var nrocuota = obj.value;

        if (nrocuota == "" || nrocuota == 0 || isNaN(nrocuota))
		{
            return false;
		}

        //var cuotames = parseFloat(montoref)/nrocuota
        var TasaInteres = <?=$TasaInteres?>;

		if (TasaInteres == 0)
		{
			var cuotames = montoref / nrocuota;
		}
		else
		{
        	var cuotames = montoref * (Math.pow((1 + TasaInteres), nrocuota) * (TasaInteres)) / (Math.pow((1 + TasaInteres), nrocuota) - 1);
		}
		//alert(TasaInteres);
        cuotames = parseFloat(cuotames).toFixed(1)
        $("#cmensual").val(parseFloat(cuotames).toFixed(2))

    }

    function FormaPagoUpd(obj)
    {

        if ($("#tipopago").val() == 2)
        {
            $(".Credito").show();
            //$("#LblDMonto").html('Monto a Fraccionar')
        } else {
            $(".Credito").hide();
        }
    }
    function FormaPago(obj)
    {
        if (obj.value == 2)
        {
            $(".Credito").show();
            $("#minicial").val('0.00')
            $("#francionar").val($("#total").val())
            //$("#LblDMonto").html('Monto a Fraccionar')
        } else {
            $(".Credito").hide();
            //$("#LblDMonto").html('Monto a Pagar')
            $("#mrefinanciar").val($("#total").val())
        }
    }
    function calcularnrocuota(obj)
    {
        var nrocuota = parseFloat($("#mrefinanciar").val()) / obj.value
        $("#nrocuotas").val(Round(nrocuota, 0))
    }
    function buscar_presupuesto()
    {
        object = "nropresupuesto";
        AbrirPopupBusqueda('../../../../buscar/clientes_presupuesto.php?Op=8', 1100, 450);
    }
    function Recibir(id)
    {
        if (object == "nropresupuesto")
        {
            datos_presupuesto(id)
        }
    }

    function datos_presupuesto(id)
    {
        $.ajax({
            url: '../../../../ajax/mostrar/presupuesto.php',
            type: 'POST',
            async: true,
            data: 'nropresupuesto='+id+"&codsuc=<?=$codsuc ?>&Op=1",
            dataType: "json",
            success: function (datos)
            {
                $("#nroinscripcion").val(datos.nroinscripcion)
                $("#codcliente").val(datos.codcliente)
                $("#propietario").val(datos.propietario)
                $("#direccion").val(datos.calle)
                $("#documento").val(datos.abreviado+' - '+datos.nrodocumento)
                $("#TbIndex tbody").html(datos.trexp)
                $("#subtotaltotal").val(datos.subtotaltotalexp)
                $("#igvtotal").val(datos.igvtotalexp)
                $("#redondeototal").val(datos.redondeototalexp)
                $("#importetotal").val(datos.importetotalexp)
                $("#total").val(datos.importetotalexp)
                $("#LblSubtotal").html(datos.subtotaltotalexp)
                $("#Lbligv").html(datos.igvtotalexp)
                $("#Lblredondeo").html(datos.redondeototalexp)
                $("#Lbltotal").html(datos.importetotalexp)
                $("#mrefinanciar").val(datos.importetotalexp)
                $("#tipopresupuesto").val(datos.tipopresupuesto)
            }
        })
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?=$Guardar ?>" enctype="multipart/form-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos del Expediente</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Forma de Pago <div id="DivSave"></div></a></li>
                            </ul>
                            <div id="tabs-1" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="12" class="TitDetalle">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right"><span class="CampoDetalle">Fecha de Emsion </span></td>
                                        <td class="TitDetalle" align="center">:</td>
                                        <td colspan="10" class="CampoDetalle">
                                            <input name="fechaemision" type="text" id="fechaemision" maxlength="10" class="inputtext fecha" value="<?=$fecha?>" style="width:85px;"/>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Usuario</td>
                                        <td class="TitDetalle" align="center">:</td>
                                        <td colspan="10" class="CampoDetalle">
                                            <input name="codexpediente" type="hidden" value="<?=$items['codexpediente'] ?>">
                                            <input name="nroexpediente" type="hidden" value="<?=$items['nroexpediente'] ?>">
                                            <input name="tipopresupuesto" id="tipopresupuesto" type="hidden" value="<?=$items['codtiposervicio'] ?>">
                                            <input type="hidden" name="nroinscripcion" id="nroinscripcion" value="<?=$items['nroinscripcion'] ?>"/>
                                            <input type="text" name="codcliente" id="codcliente" size="10" class="inputtext" readonly="readonly" value="<?=$items["codcliente"] ?>" />
                                            <span class="MljSoft-icon-buscar buscarcliente" title="Buscar Clientes" onclick="buscar_presupuesto();"></span>
                                            <input class="inputtext" name="propietario" type="text" id="propietario" size="60" maxlength="200" value="<?=utf8_encode($items["propietario"]) ?>" placeholder="Datos del Uusario" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Direccion</td>
                                        <td align="center">:</td>
                                        <td ><input type="text" name="direccion" id="direccion" class="inputtext" size="60" maxlength="60" readonly="readonly" value="<?=utf8_encode($items["direccion"]) ?>" placeholder="Direccion del Usuario" /></td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Documento</td>
                                        <td align="center">:</td>
                                        <td ><input type="text" name="documento" id="documento" class="inputtext" size="20" maxlength="60" readonly="readonly" value="<?=utf8_encode($items["documento"]) ?>"placeholder="Nro. documento del Uusario" /></td>
                                    </tr>
                                    <tr>
                                    <tr style="height:5px">
                                        <td colspan="12" class="TitDetalle">
                                            <fieldset class="fieldset">
                                                <legend class="legend ui-state-default ui-corner-all">Detalle de Presupuestos</legend>
                                                <table width="100%">
                                                  <!-- <tr class="TrAddItem">
                                                    <td colspan="2"  >
                                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" rules="all" >
                                                        <tr>
                                                          <td align="center">
                                                            <input type="text" class="entero text inputtext ui-corner-all" title="C&oacute;digo del Colateral " id="CodConcepto" style="width:80px"  onkeypress="ValidarEnterCre(event,2);"/>
                                                            <span class="MljSoft-icon-buscar" title="Buscar Colateral" onclick="buscar_conceptos();"></span>
                                                            <input type="text" class="text inputtext ui-corner-all" style="width:350px" title="Descripci&oacute;n del Colateral" id="Colateral"  />

                                                            <input type="text" id="subtotal" title="Sub Total" class="numeric inputtext ui-corner-all text" value=""  style="width:80px" onkeypress="ValidarEnterCre(event,1);" value="0.00" onkeyup="CalcularImporte();"/>
                                                            <input type="text" id="igv"  class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                            <input type="text" id="redondeo" class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                            <input type="text" id="importe" class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                            <span class="icono-icon-add" title="Agregar Producto" onclick="AgregarItem()"></span>
                                                              <input type="hidden" name="afecto_igv" id="afecto_igv" value="0" />

                                                          </td>
                                                        </tr>
                                                      </table>
                                                    </td>
                                                  </tr> -->
                                                    <tr>
                                                        <td colspan="2" >
                                                            <div style="height:auto; overflow:auto" align="center" id="DivDetalle">
                                                                <table width="800" border="0" align="center" cellspacing="0" class="ui-widget" id="TbIndex">
                                                                    <thead class="ui-widget-header" >
                                                                        <tr height="20px">
                                                                            <th width="43" align="center" scope="col">Item</th>
                                                                            <th align="center" scope="col">Tipo</th>
                                                                            <th width="80" align="center" scope="col">Subtotal</th>
                                                                            <th width="80" align="center" scope="col">IGV</th>
                                                                            <th width="80" align="center" scope="col">Redondeo</th>
                                                                            <th width="80" align="center" scope="col">Importe</th>
                                                                            <th width="5" align="center" scope="col">&nbsp;</th>
                                                                        </tr>

                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                            $tr = '';
                                                                            if ($id != '') {
                                                                            $Sql = "SELECT tp.codtipopresupuesto, UPPER(tp.descripcion) as descripcion, c.subtotal,
                                                                                    c.igv , c.total  AS imptotal,
                                                                                    c.redondeo AS redondeo,c.nropresupuesto,tp.codconcepto,c.nrosolicitud
                                                                                FROM  solicitudes.expedientesdetalle c
                                                                                INNER JOIN public.tipopresupuesto tp ON (tp.codtipopresupuesto = c.codtipopresupuesto)
                                                                                WHERE c.codemp=? AND c.codsuc=? AND c.codexpediente=?
                                                                                ORDER BY  c.nropresupuesto";
                                                                            $consulta = $conexion->prepare($Sql);
                                                                            $consulta->execute(array(1, $codsuc, $id));
                                                                            $itemsD = $consulta->fetchAll();
                                                                            $subtotaltotal = 0;
                                                                            $igvtotal = 0;
                                                                            $redondeototal = 0;
                                                                            $importetotal = 0;
                                                                            $i = 0;
                                                                            foreach ($itemsD as $rowD) {
                                                                                $subtotaltotal +=$rowD['subtotal'];
                                                                                $igvtotal +=$rowD['igv'];
                                                                                $redondeototal +=$rowD['redondeo'];
                                                                                $importetotal +=$rowD['imptotal'];
                                                                                $i++;
                                                                                $tr.='<tr id="'.$i.'" onclick="SeleccionaId(this);">';
                                                                                $tr.='<td align="center" ><label class="Item">'.$i.'</label></td>';
                                                                                $tr.='<td align="left"><label class="CodTipoPresupuesto">'.$rowD['codtipopresupuesto'].'</label>';
                                                                                $tr.='<label class="Colateral">'.$rowD['descripcion'].'</label>';
                                                                                $tr.='<label class="CodConcepto">'.$rowD['codconcepto'].'</label>';
                                                                                $tr.='<label class="NroSolicitud">'.$rowD['nrosolicitud'].'</label>';
                                                                                $tr.='<label class="NroPresupuesto">'.$rowD['nropresupuesto'].'</label></td>';
                                                                                $tr.='<td align="right"><label class="SubTotal">'.number_format($rowD['subtotal'], 2).'</label></td>';
                                                                                $tr.='<td align="right"><label class="IGV">'.number_format($rowD['igv'], 2).'</label></td>';
                                                                                $tr.='<td align="right"><label class="Redondeo">'.number_format($rowD['redondeo'], 2).'</label></td>';
                                                                                $tr.='<td align="right"><label class="Importe">'.number_format($rowD['imptotal'], 2).'</label></td>';
                                                                                $tr.='<td align="center" ><a href="javascript:QuitaItemc('.$i.')" class="Del" >';
                                                                                $tr.='<span class="icono-icon-trash" title="Quitar Presupuesto" style="display:none;"></span> ';
                                                                                $tr.='</a></td></tr>';
                                                                            }
                                                                        }
                                                                        echo $tr;
                                                                        ?>
                                                                    </tbody>
                                                                    <tfoot class="ui-widget-header">

                                                                        <tr>
                                                                            <td colspan="2" align="right" >Total :</td>
                                                                            <td align="right" >
                                                                                <input type="text" id="subtotaltotal" name="subtotal" value="<?=number_format($subtotaltotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                            </td>
                                                                            <td align="right" >
                                                                                <input type="text" id="igvtotal" name="igv" value="<?=number_format($igvtotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                            </td>
                                                                            <td align="right" >
                                                                                <input type="text" id="redondeototal" name="redondeo" value="<?=number_format($redondeototal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                            </td>
                                                                            <td align="right" >
                                                                                <input type="text" id="importetotal" name="imptotal" value="<?=number_format($importetotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                            </td>
                                                                            <td >&nbsp;</td>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="TitDetalle" align="right">Sub. Total </td>
                                        <td class="TitDetalle" align="center">:</td>
                                        <td width="47" class="CampoDetalle"><label id="LblSubtotal"><?=number_format($items["subtotal"], 2) ?></label></td>
                                        <td width="26" align="right" class="CampoDetalle">Igv</td>
                                        <td width="13" align="center" class="CampoDetalle">:</td>
                                        <td width="87" class="CampoDetalle"><label id="Lbligv"><?=number_format($items["igv"], 2) ?></label></td>
                                        <td width="87" class="CampoDetalle" align="right">Redondeo</td>
                                        <td width="22" class="CampoDetalle" align="center">:</td>
                                        <td width="71" class="CampoDetalle"><label id="Lblredondeo"><?=number_format($items["redondeo"], 2) ?></label></td>
                                        <td width="42" class="CampoDetalle" align="right">Total</td>
                                        <td width="23" class="CampoDetalle" align="center">:</td>
                                        <td width="96" class="CampoDetalle">
                                            <label id="Lbltotal"><?=number_format($items["total"], 2) ?></label>
                                            <input type="hidden" name="total" id="total" value="<?=$items["total"] ?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Forma de Pago </td>
                                        <td class="TitDetalle" align="center">:</td>
                                        <td class="CampoDetalle">
                                            <select name="tipopago" id="tipopago" class="select" onchange="FormaPago(this)">
                                                <option value="1" <?php if ($items['tipopago'] == 1) echo "selected='selected'" ?> >CONTADO</option>
                                                <option value="2" <?php if ($items['tipopago'] == 2) echo "selected='selected'" ?> >CREDITO</option>
                                            </select>
                                        </td>
                                        <td class="CampoDetalle">&nbsp;</td>
                                        <td class="CampoDetalle">&nbsp;</td>
                                        <td colspan="2" class="CampoDetalle" align="right"><label id="LblDMonto"> Monto a Pagar</label> </td>
                                        <td class="CampoDetalle" align="center">:</td>
                                        <td class="CampoDetalle">
                                            <input type="text" name="mrefinanciar" id="mrefinanciar" class="inputtext" size="13" maxlength="10" value="<?=number_format($items["total"], 2) ?>" readonly="readonly" />
                                        </td>
                                        <td colspan="3" class="CampoDetalle">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="12" class="TitDetalle">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="12" align="center"  width="100%">
                                            <div style="height:auto; overflow:auto" align="center"  width="100%">
                                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
                                                    <tr class="Credito" >
                                                        <td class="TitDetalle" align="right">Monto Inicial </td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="minicial" id="minicial" class="inputtext numeric" size="13" maxlength="10" value="<?=number_format($items["cuotainicial"], 2) ?>" onkeyup="Inicial(this);" />
                                                        </td>

                                                    </tr>
                                                    <tr class="Credito" >
                                                        <td class="TitDetalle" align="right">Monto a Francionar </td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="francionar" id="francionar" class="inputtext numeric" size="13" maxlength="10" value="<?=number_format($items["total"] - $items["cuotainicial"], 2) ?>" readonly="readonly"/>
                                                        </td>

                                                    </tr>
                                                    <tr class="Credito" >
                                                        <td class="TitDetalle" align="right">Nro. de Cuotas </td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="nrocuotas" id="nrocuotas" class="entero inputtext" size="13" maxlength="10" onkeyup="Cuotas(this)"  value="<?=$items['nrocuota'] ?>"  />
                                                        </td>
                                                    </tr>
                                                    <tr class="Credito" >
                                                        <td class="TitDetalle" align="right">Interes Mensual </td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <label ><?=number_format($tasapromintant, 2) ?></label>%
                                                            <input type="hidden" id="interescredito" name="interescredito" value="<?=number_format($tasapromintant, 2) ?>"/>
                                                        </td>
                                                    </tr>
                                                    <tr class="Credito" >
                                                        <td class="TitDetalle" align="right">Cuota Mensual </td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="cmensual" id="cmensual" class="inputtext numeric" size="13" maxlength="10" readonly="readonly"  value="<?=number_format($items['cuotames'], 2) ?>"/>
                                                            <input type="button" class="button" name="Generar" value="Generar Cuotas de Credito" onclick="Agregar();" />
                                                        </td>
                                                    </tr>
                                                    <tr class="Credito" width="100%">
                                                        <td colspan="3" align="center" class="TitDetalle">
                                                            <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbcreditos" rules="all" >
                                                                <thead class="ui-widget-header" >
                                                                    <tr class="CabIndex">
                                                                        <td width="12%" align="center" >Item</td>
                                                                        <td width="12%" align="center" >Nro Cuota</td>
                                                                        <td width="21%" align="center" >Amortizaci&oacute;n </td>
                                                                        <td width="13%" align="center" >Interes.</td>
                                                                        <td width="11%" align="center" >Monto Igv </td>
                                                                        <td width="13%" align="center" >Redondeo  </td>
                                                                        <td width="13%" align="center" >Importe</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    $i = 0;
                                                                    if ($id != '') {
                                                                        $Sql = "SELECT *
                                                                            FROM  solicitudes.precreditos
                                                                            WHERE codemp=? AND codsuc=? AND  codexpediente=?
                                                                            ORDER BY  nrocuota";
                                                                        $consulta = $conexion->prepare($Sql);
                                                                        $consulta->execute(array(1, $codsuc, $id));
                                                                        $itemsD = $consulta->fetchAll();

                                                                        $tr = '';
                                                                        foreach ($itemsD as $rowD) {
                                                                            $tr.="<tr align='center'>";
                                                                            $tr.="<td><input type='hidden' value='".$i."' name='nrocuota".$i."' />".$i."</td>";
                                                                            $tr.="<td><input type='hidden' value='".$rowD['nrocuota']."' name='totcuotas".$i."' />".$i."/".$rowD['nrocuota']."</td>";
                                                                            $tr.="<td><input type='hidden' value='".$rowD['cuotamensual']."' name='cuotames".$i."' id='cuotames".$i."' /><label id='cuotamesx".$i."'>".number_format($rowD['cuotamensual'], 2)."</label></td>";
                                                                            $tr.="<td><input type='hidden' value='".$rowD['interes']."' name='interes".$i."' />".number_format($rowD['interes'], 2)."</td>";
                                                                            $tr.="<td><input type='hidden' value='".$rowD['igv']."' name='impigv".$i."' id='impigv".$i."' /><label id='impigvx".$i."'>".number_format($rowD['igv'], 2)."</label></td>";
                                                                            $tr.="<td><input type='hidden' value='".$rowD['redondeo']."' name='red".$i."' id='red".$i."' /><label id='redx".$i."'>".number_format($rowD['redondeo'], 2)."</label></td>";
                                                                            $tr.="<td><input type='hidden' value='".$rowD['imptotal']."' name='tot".$i."' id='tot".$i."' /><label id='totx".$i."'>".number_format($rowD['imptotal'], 2)."</label></td>";
                                                                            $tr.="</tr>";
                                                                        }
                                                                        echo $tr;
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <input type="hidden" name="contador" id="contador" value="<?=$i ?>" />
                                                        </td>

                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
