<?php 
	include("../../../../objetos/clsReporte.php");
	class clsPresupuesto extends clsReporte
	{
		function cabecera()
		{			
		}
		function contenido($id,$codsuc,$impgasadm)
		{
			global $conexion;
			
			$h = 4;
			$s = 2;
			
			$paramae = $this->getParamae("RESSOL",$codsuc);
			
			$sql = "select *
					from solicitudes.expedientes as c
					where c.codemp=1 and c.codsuc=? and c.codexpediente=?";
			$consulta=$conexion->prepare($sql);
			$consulta->execute(array($codsuc,$id));
			$row = $consulta->fetch();
			$this->SetFont('Arial','B',8);
			$Anio = substr($this->DecFecha($row['fechaemision']),6,4);
			$tit1 = "EXPEDIENTE ".str_pad($row['nroexpediente'],4,"0",STR_PAD_LEFT)." - ".$Anio;
			$RIO='';
			$this->SetY(20);
			$this->Cell(0,4,utf8_decode($RIO),0,1,'R');
			$this->SetFont('Arial','B',10);
			$this->Cell(0,5,utf8_decode($tit1),0,1,'C');	

			$this->SetFont('Arial','',8);
			$this->Ln(2);
			$this->Cell(25,$h,"Nombre",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(60,$h,utf8_decode(strtoupper($row["propietario"])),0,0,'L');
			
			$NroIncrip = utf8_decode(strtoupper($row["nroinscripcion"]));
			$NroIncrip = "0".$codsuc.substr("000000", strlen($NroIncrip)).$NroIncrip ;
			
			$this->Cell(25,$h,"Inscripción",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(75,$h, $NroIncrip,0,1,'L');
			
			$this->Cell(25,$h,"Direccion",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,strtoupper($row["direccion"]),0,1,'L');
			$this->Ln(5);
			$Dim = array('1'=>10,'2'=>15,'3'=>90,'4'=>15);
			$this->Cell($Dim[1],5,utf8_decode('Tipo'),1,0,'C',false);
			$this->Cell($Dim[2],5,('Número'),1,0,'C',false);
			$this->Cell($Dim[3],5,utf8_decode('Tipo Presupuesto'),1,0,'C',false);
			$this->Cell($Dim[4],5,utf8_decode('Total'),1,1,'C',false);
			
			$cadena 	= "";
			$cadena2 	= "";
			$subtotal 	= 0;
			$impigv		= 0;
			
			$Sql="SELECT tp.codtipopresupuesto, tp.descripcion, c.subtotal,c.igv , c.total  AS imptotal,0 AS redondeo,
			c.nropresupuesto,tp.codconcepto,c.nrosolicitud,tp.code
  			FROM  solicitudes.expedientesdetalle c
  			INNER JOIN public.tipopresupuesto tp ON (tp.codtipopresupuesto = c.codtipopresupuesto)
  			WHERE c.codemp=? AND c.codsuc=? AND  c.codexpediente=? 
  			ORDER BY  c.nropresupuesto";
  			$consulta = $conexion->prepare($Sql);
			$consulta->execute(array(1,$codsuc,$id));
			$itemsD = $consulta->fetchAll();
			$subtotaltotal =0;
			$igvtotal      =0;
			$redondeototal =0;
			$importetotal  =0;
			$i             =0;
			$this->SetFillColor(255,255,255); //Color de Fondo
		$this->SetTextColor(0);
			foreach($itemsD as $rowD)
			{
				$importetotal  +=$rowD['imptotal'];
				$this->Cell($Dim[1],5,$rowD['code'],0,0,'C',true);
				$this->Cell($Dim[2],5,$rowD['nropresupuesto'],0,0,'C',true);
				$this->Cell($Dim[3],5,strtoupper(utf8_decode($rowD['descripcion'])),0,0,'L',true);
				$this->Cell($Dim[4],5,number_format($rowD['imptotal'],2),0,1,'R',true);
				
			}
				$this->Cell(($Dim[1]+$Dim[2]+$Dim[3]),5,utf8_decode('TOTAL S/.'),1,0,'R',false);
				$this->Cell($Dim[4],5,number_format($importetotal,2),1,1,'R',true);
			
			$this->Ln(5);
			$X1=' ';
			$X2='X';
			$cuotainicial=number_format($row['cuotainicial'],2);
			$nrocuota=$row['nrocuota'];
			if($row['tipopago']==1)
			{
				$X1='X';
				$X2=' ';
				$cuotainicial="___";
				$nrocuota="___";
			}
			//$X1=' ';
			//$X2=' ';

			$this->SetFont('Arial','B',8);
			$this->Cell(25,$h,"FORMA DE PAGO",0,0,'L');
			$this->Cell(5,$h,":",0,0,'R');
			$this->Cell(35,$h,"(".$X1.") CONTADO",0,0,'C');
			$this->Cell(40,$h,"(".$X2.") FINANCIAMIENTO",0,1,'C');
			$this->Ln(5);

			$this->Cell(25,$h,"FECHA: ".$this->DecFecha($row['fechaemision']),0,0,'L');
			$this->Cell(50,$h,"",0,0,'L');
			$this->Cell(40,$h,"CUOTA INICIAL: ",0,0,'L');
			$this->Cell(25,$h,"S/.".$cuotainicial,0,1,'L');
			$this->Ln(2);
			$this->Cell(25,$h,"",0,0,'L');
			$this->Cell(50,$h,"",0,0,'L');
			$this->Cell(40,$h,"NUMERO DE CUOTAS: ",0,0,'L');
			$this->Cell(25,$h,$nrocuota." MESES",0,1,'L');

			$this->Ln(15);
			$this->Cell(40,$h,"",0,0,'L');
			$this->Cell(25,0.01,"",1,1,'C');
			$this->Ln(1);
			$this->Cell(40,$h,"",0,0,'L');
			$this->Cell(25,$h,"Comercialización",0,1,'C');
			
			
		}
	}

	

	
	//print_r($impgasadm);
	$impgasadm=$impgasadm['valor'];
	$codsuc 		= $_GET["codsuc"];
	$codexpediente	= $_GET["Id"];

	$objReporte = new clsPresupuesto($orientation='P',$unit='mm',$format='A5');
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codexpediente,$codsuc,$impgasadm);
	$objReporte->Output();
	
?>