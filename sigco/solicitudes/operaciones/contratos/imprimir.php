<?php
	include("../../../../objetos/clsReporte.php");
	include("../../../../admin/clases/num2letra.php");
	
	class clsPresupuesto extends clsReporte
	{
		function Header()
		{
			global $codsuc, $nrocontrato, $Sede;
			$empresa = $this->datos_empresa($codsuc);
			$this->SetFont('Arial','',6);
			$this->SetTextColor(0,0,0);
			$tit1=strtoupper($empresa["razonsocial"]);
			$tit2=strtoupper($empresa["direccion"]);
			$tit3="SUCURSAL: ".strtoupper($empresa["descripcion"]);

			$this->SetY(5);
			$this->SetFont('Arial','B',10);
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			$this->Ln(2);
			$this->SetFont('Arial','B',10);
			$tit2 = "Contrato de Prestaci�n de Servicios de Saneamiento N� ".$nrocontrato;
			$this->Cell(190,$h,$tit2,0,1,'C');
			$this->SetY(5);
			$this->SetFont('Arial','B',10);
			$tit1 = "";
			$this->Cell(0,5,utf8_decode($tit1),0,1,'R');	
			//DATOS DEL FORMATO
			
			$x 	= 23;
            $y 	= 5;
			$h	= 3;
		    $this->Image($_SESSION['path']."images/logo_empresa.jpg",6,1,17,16);
           
			
			$this->SetY(8);
			$this->SetFont('Arial','',6);
			$this->Cell(0,4,'Pag. '.$this->PageNo().' de {nb}',0,1,'R');
			$this->SetY(18);
			$this->Cell(0,.1,"",1,1,'C',true);
            $this->cabecera();     
						
		}
		function cabecera()
		{			
		}
		function contenido($id,$codsuc)
		{
			global $conexion;
			$this->SetY(22);
			$h = 5;
			$s = 2;
			$Letra = 9;
			$Espacio=2;
			$empresa = $this->datos_empresa($codsuc);
			$sql = "select *
					from solicitudes.expedientes as c
					where c.codemp=1 and c.codsuc=? and c.codexpediente=?";
			$consulta=$conexion->prepare($sql);
			$consulta->execute(array($codsuc,$id));
			$row = $consulta->fetch();

			$this->SetLeftMargin(14);
			$this->SetRightMargin(14);
			$this->SetFont('Arial', '', $Letra);
			
			$text ="   Conste por el presente documento el Contrato de Prestaci�n de Servicios de Agua Potable";
			$text .=" y Alcantarillado Sanitario que celebran, de una parte ".utf8_decode(strtoupper($empresa['razonsocial']))." (en adelante la";
			$text .=" EPS) con RUC N� ".strtoupper($empresa['ruc'])." y con domicilio en el ".utf8_decode($empresa['direccion'])." y";
			$text .=" de otra parte: ".utf8_decode(strtoupper($row['titular']))." cuyos datos figuran en la Cl�usula Primera - Anexo 2";
			$text .=" (en adelante el Titular de la Conexion Domiciliaria), de acuerdo con los siguientes t�rminos";
			$text .=" y condiciones:";
			
			$this->MultiCell(0, $h, $text, 0, 'J');
			
			$this->SetFont('Arial','B',$Letra);
			$this->Ln($Espacio);
			
			$text ="Cl�usula Segunda.-Objeto del contrato";
			
			$this->MultiCell(0,$h,$text,0,'J');
			
			$this->SetFont('Arial','',$Letra);
			
			$text ="Constituye objeto del presente contrato la prestaci�n de los servicios seg�n lo solicitado en el anexo2.";
			
			$this->MultiCell(0,$h,$text,0,'J');

			$this->SetFont('Arial','B',$Letra);
			$this->Ln($Espacio);
			
			$text ="Cl�usula Tercera.-Obligaciones de las partes";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','',$Letra);
			
			$text ="La EPS se obliga a la prestaci�n de los servicios se�alados en la cl�usula anterior, en las condiciones de calidad establecidas en el contrato de Prestaci�n de Servicios de Saneamiento en el Reglamento de Calidad de los Servicios de Saneamiento emitido por la Superintendencia Nacional de los Servicios de Saneamiento (en adelante SUNASS), y en concordancia con el informe de factibilidad elaborado por la EPS en el procedimiento seguido para el acceso al servicio, y que forma parte integrante del presente contrato.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			
			$text ="EL TITULAR DE LA CONEXI�N DOMICILIARIA, se obliga al pago de los servicios de saneamiento prestados por la EPS y otros conceptos que se incluyan en el comprobante de pago que la EPS emita de acuerdo con la normativa sobre la facturaci�n vigente aprobada por la SUNASS.";
			
			$this->MultiCell(0, $h, $text, 0, 'J');
			
			//////7
			$this->SetFont('Arial','B',$Letra);
			$this->Ln($Espacio);
			
			$text ="Cl�usula Cuarta.-Modificaci�n de las condiciones de prestaci�n de servicio";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','',$Letra);
			
			$text ="Las partes se comprometen a realizar una comunicaci�n previa de las modificaciones, de ser posibles seg�n el caso.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			
			$text = "EL TITULAR DE LA CONEXI�N y la EPS, se comprometen a informar a la otra parte acerca cualquier variaci�n en las condiciones";
			$text .= " pactadas en el presente contrato, en los plazos establecido en el Regrlamento de Calidad de Servicios de Saneamiento.";
			
			$this->MultiCell(0, $h, $text, 0, 'J');
			
			$text = "En caso de no haber un plazo establecido en dicha norma, deber�n comunicar el cambio a la otra parte a mas tardar a los tres (3) d�as h�biles de ocurrida la modificaci�n. En caso una de las partes no comunique a la otra el cambio oportunamente, y el cambio afecte a la facturaci�n de los servicios en su favor, se aplicar� la siguiente penalidad:";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			
			$text ="Modificaciones no comunicadas por la EPS: el cambio no se aplica a la facturaci�n.";
			$this->Cell(5, $h, "a)", 0, 0, 'C');
			$this->Cell(0, $h, $text, 0, 1, 'J');
			
			$text = "Modificaciones no comunicadas por el TITULAR DE LA CONEXI�N, en caso de instalaciones no autorizadas, destinadas a burlar el consumo de la conexi�n registrada, se aplicar� el art�culo 96Q inciso b del Reglamento de Calidad de los Servicios de Saneamiento.";
			$this->Cell(5, $h, "b)", 0, 0, 'C');
			$this->MultiCell(0, $h, $text, 0, 'J');
			$this->Ln($Espacio);
			
			$text = "En otros casos, la EPS podr� presumir que el cambio se ha realizado con doce (12) meses de anterioridad.";
			$this->MultiCell(0, $h,$text,0,'J');
			
			//////7
			$this->SetFont('Arial','B',$Letra);
			$this->Ln($Espacio);
			
			$text = "Cl�usula Quinta.- Responsabilidad por Infraestructura y bienes instalados.-";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','',$Letra);
			
			$text = "La infraestructura y los dem�s bienes instalados por la EPS, para la prestaci�n del servicio hasta la Conexion Domiciliaria de Agua Potable o Alcantarillado, incluida est� �ltima, son de su responsabilidad y forman parte de la red p�blica.";
			
			$this->MultiCell(0, $h, $text, 0, 'J');
			$this->Ln($Espacio);
			
			$text = "La EPS es responsable de la operatividad y mantenimiento de la infraestructura que va desde la fuente de agua hasta la Conexion Domiciliaria de Agua Potable inclusive.";
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			
			$text = "En los casos que la caja del medidor se ubique al interior del predio, la EPS es tambi�n responsable de la operatividad y mantenimiento del tramo de tuber�a ubicado al interior del predio hasta la caja del medidor.";
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			
			$text = "En los casos de Conjuntos Habitacionales o Quintas, la EPS es tambi�n responsable de la operatividad y mantenimiento de los tramos de tuber�a ubicados al interior del predio hasta la caja  del medidor, seg�n lo establecido por el art�culo 32Q de la norma t�cnica  A.020 del Reglamento Nacional de Edificaciones Comunes.";
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			
			$text = "El usuario es responsable del estado y conservaci�n de las instalaciones sanitarias internas del predio.";
			$this->MultiCell(0,$h,$text,0,'J');
			
			
			$this->SetFont('Arial','B',$Letra);
			$this->Ln($Espacio);
			
			$text = "Cl�usula Sexta.- Comprobante de pago y cobro de inter�s moratorio.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','',$Letra);
			
			$text = "Los comprobantes de pago por los servicios de saneamiento deber�n ser entregados en el domicilio se�alado por el TITULAR DE LA CONEXI�N, con una anticipaci�n no menor de diez (10) d�as antes de la fecha de vencimiento. EL TITULAR DE LA CONEXI�N es responsable del pago de dichos comprobantes.";
			
			$this->MultiCell(0, $h, $text, 0, 'J');
			
			$this->Ln(8);
			
			$this->pie($h, $nrocontrato, utf8_decode(strtoupper($row['titular'])), $row['direccion']);
			
			$this->AddPage();

			$this->Ln($Espacio);
			////
			
			$text = "La  falta  de  entrega del comprobante no suspende la obligaci�n de pagar por la prestaci�n del servicio, en las fechas establecidas previamente por la EPS y conocidas por EL TITULAR DE LA CONEXION.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			
			$text = "La EPS podr� cobrar el inter�s moratorio a partir del d�a siguiente del plazo de vencimiento Dicho inter�s no podr� exceder de la tasa m�xima correspondiente fijada por el Banco Central del Per� para operaciones que realizan las personas ajenas al sistema financiero nacional en moneda nacional (TAMN).";
			
			$this->MultiCell(0,$h,$text,0,'J');
			
			$text = "La EPS tiene el derecho de iniciar las acciones administrativas, operativas, legales y judiciales que sean necesarias para la cobranza  de los montos adecuados, asumiento el TITULAR DE LA CONEXION DOMICILIARIA, los gastos y costos que se originen de dichas acciones. La EPS podr� suspender la prestaci�n del servicio de agua potable, sin necesidad de previo aviso ni de intervenci�n de autoridad alguna, en caso de incumplimiento en el pago de la tarifa de dos (2) meses, as� como cobrar el costo de suspensi�n y reposici�n del servicio. En caso de que la EPS no suspenda el servicio, no podr� cobrar por el consumo que se realice en adelante.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			
			///
			$this->SetFont('Arial','B',$Letra);

			$text = "Cl�usula S�ptima.- Cierre del servicio.";
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','',$Letra);
			
			$text = "Asimismo, la EPS tiene la facultad de cerrar el servicio por la comisi�n de infracciones expre-samente previstas y de acuerdo con los mecanismos contemplados en el Reglamento de Cali�dad de los Servicios de Saneamiento.";
			
			$this->SetFont('Arial','',$Letra);
			$this->MultiCell(0,$h,$text,0,'J');
			
			$text = "EL TITULAR DE LA CONEXION DOMICILIARIA tiene derecho a solicitar el cierre del servicio en cualquier momento, con una anticipaci�n m�nima de treinta (30) d�as calendarios.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			
			$text = "El Cierre Temporal del Servicio solicitado no podr� exceder por m�s de seis (06) meses. Vencido el plazo se�alado la EPS se reserva el derecho de anular la conexi�n domiciliaria y se�guidamente se proceder� a retirar del Padr�n de Usuarios, tom�ndose las medidas legales res�pecto a la recuperaci�n de la deuda en caso la hubiese.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			
			$text = "La Solicitud del Cierre Temporal del Servicio de Agua Potable incluye obligatoriamente el Cierre del Servicio de Alcantarillado.";
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			
			$this->SetFont('Arial','B',$Letra);
			
			$text ="Cl�usula Octava.- Resoluci�n del Contrato.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','',$Letra);
			
			$text="El TITULAR DE LA CONEXI�N DOMICILIARIA puede resolver el presente contrato mediante comunicaci�n escrita  dirigida por a la EPS conforme al procedimiento establecido en el el Reglamento de Calidad de la Prestaci�n de los Servicios.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			
			$text="En los casos de Servicios Temporales EL TITULAR DE LA CONEXI�N DOMICILIARIA podr� ponerle fin al Contrato antes del plazo estipulado, en cuyo caso la garant�a referida en el articulo 33Q del Reglamento de Calidad de la Prestaci�n de los Servicios de Saneamiento ser� devuelta previo pago de las deudas existentes por la prestaci�n de los servicios, servicios colaterales, y cualquier otro concepto adeudado.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			
			$text = "La EPS puede resolver el contrato en los casos que se aplique el levantamiento de la conexi�n como sanci�n, de acuerdo a lo establecido en el Reglamento de Calidad de los Servicios de Saneamiento. No se requerir� comunicaci�n ni tr�mite adicional a la aplicaci�n de la sanci�n.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio);
			$this->SetFont('Arial','B',$Letra);
			
			$text = "Cl�usula Novena.- Jurisdicci�n.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','',$Letra);
			
			$text = "Para todo lo relacionado con el Contrato, las partes renuncian al fuero de sus domicilios y se someten a la jurisdicci�n de los Jueces y Tribunales de la ciudad de Sicuani, se�alando como su domicilio los indicados en la introducci�n y cl�usula primera del presente Contrato, donde se efectuar�n todas las notificaciones judiciales o extrajudiciales. Cualquier Variaci�n de domicilio deber� comunicarse por escrito.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','B',$Letra);
			$this->Ln($Espacio);
			
			$text ="Cl�usula D�cima.- Otros derechos y obligaciones de las partes.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('Arial','',$Letra);
			
			$text="Los dem�s derechos y obligaciones de las partes, as� como lo no contemplado en el presente contrato se regulan por el Reglamento de Calidad de la Prestaci�n de los Servicios, otras normas que emita la SUNASS y las normas vigentes del sector.";
			
			$this->MultiCell(0,$h,$text,0,'J');
			
			$Dia = substr($this->DecFecha($row['fechacontrato']),0,2);
			$Mes = substr($this->DecFecha($row['fechacontrato']),3,2);
			$Anio = substr($this->DecFecha($row['fechacontrato']),6,4);
			global $meses;
			
			$text = "El presente Contrato de Prestaci�n de Servicios de Saneamiento es sucrito en la ciudad de Sicuani a los ".CantidadEnLetraSin($Dia)." d�as del mes de ".($meses[intval($Mes)])."  de ".$Anio;
			$this->MultiCell(0,$h,$text,0,'J');
			
			
			
			

			$this->Ln(20);
			$this->Cell(30,$h,"",0,0,'L');
			$this->Cell(50,0.01,"",1,0,'C');
			$this->Cell(30,$h,"",0,0,'L');
			$this->Cell(50,0.01,"",1,1,'C');
			$this->Ln(1);
			$this->Cell(30,$h,"",0,0,'L');
			$this->Cell(50,$h,utf8_decode(strtoupper($empresa['razonsocial'])),0,0,'C');
			$this->Cell(30,$h,"",0,0,'L');
			$this->Cell(50,$h,utf8_decode(strtoupper($row['titular'])),0,1,'C');
			$this->Cell(30,$h,"",0,0,'L');
			$this->Cell(50,$h,'',0,0,'C');
			$this->Cell(30,$h,"",0,0,'L');
			$this->Cell(50,$h,$row['titulardni'],0,1,'C');
			$this->Ln(10);
			
			$this->pie($h,$nrocontrato,utf8_decode(strtoupper($row['titular'])),$row['direccion']);
			
/////			
			$this->SetLeftMargin(12);
			$this->SetRightMargin(12);
			$this->AddPage();
			
			$this->SetFont('Arial','B',12);
			$this->Ln($Espacio);
			$text="ANEXO 2";
			$this->MultiCell(0,$h,$text,0,'C');
			$this->Ln($Espacio*3);

			$this->SetFont('courier','B',$Letra);
			$this->Ln($Espacio);
			$text ="CLAUSULA PRIMERA.- DATOS GENERALES";
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln($Espacio*4);
			$text ="DATOS DEL TITULAR DE LA CONEXION DOMICILIARIA";
			$this->MultiCell(0,$h,$text,0,'J');
			
			$this->SetFont('courier','',$Letra);
			$this->Ln($Espacio*2);
			$this->Cell(19,$h,'Nombre',0,0,'L');
			$this->Cell(100,$h,utf8_decode(strtoupper($row['titular'])),1,0,'L');
			$this->Cell(11,$h,'',0,0,'L');
			$this->SetFont('courier','B',$Letra);
			$this->Cell(30,$h,'Inscripci�n',0,0,'R');
			$codigo=$this->CodUsuario($codsuc,$row['nroinscripcion']);
			$this->Cell(26,$h,$codigo,1,0,'C');

			$this->Ln(4);

			$this->SetFont('courier','',$Letra);
			$this->Ln($Espacio*2);
			$this->Cell(19,$h,'D.N.I',0,0,'L');
			$this->Cell(24,$h,$row['titulardni'],1,0,'L');
			$this->Cell(46,$h,'R.U.C',0,0,'R');
			$this->Cell(30,$h,$row['titularruc'],1,0,'L');
			$this->Cell(11,$h,'',0,0,'L');
			$this->Cell(30,$h,'Fecha',0,0,'R');
			$this->Cell(26,$h,$this->DecFecha($row['fechacontrato']),1,0,'C');

			$this->Ln(4);

			$this->Ln($Espacio*2);
			$this->Cell(19,$h,'Direcc.',0,0,'L');
			$this->Cell(100,$h,$row['titulardir'],1,0,'L');
			$this->Cell(5,$h,'',0,0,'L');

			$this->Cell(30,$h,'Distrito',0,0,'R');
			$Sql="SELECT descripcion FROM public.ubigeo WHERE codubigeo='".$row['titularubigeo']."'";
			$consulta=$conexion->prepare($Sql);
			$consulta->execute();
			$rowU = $consulta->fetch();
			$this->Cell(32,$h,$rowU[0],1,0,'L');
			//////////
			$this->Ln(12);
			$this->SetFont('courier','B',$Letra);
			$text ="Representante Legal :";
			$this->MultiCell(0,$h,$text,0,'J');
			
			$this->SetFont('courier','',$Letra);
			$this->Ln($Espacio*2);
			$this->Cell(19,$h,'Nombre',0,0,'L');
			$this->Cell(100,$h,utf8_decode(strtoupper($row['representante'])),1,0,'L');
			$this->Cell(11,$h,'',0,0,'L');
			$Anio = substr($this->DecFecha($row['fechaemision']),6,4);
			$Exp = str_pad($row['nroexpediente'],4,"0",STR_PAD_LEFT)."-".$Anio;
			$this->Cell(30,$h,'Expediente',0,0,'R');
			$this->Cell(26,$h,$Exp,1,0,'C');

			$this->Ln(4);

			$this->SetFont('courier','',$Letra);
			$this->Ln($Espacio*2);
			$this->Cell(19,$h,'D.N.I',0,0,'L');
			$this->Cell(24,$h,$row['representantedni'],1,0,'L');
			$this->Cell(46,$h,'R.U.C',0,0,'R');
			$this->Cell(30,$h,$row['representanteruc'],1,0,'L');
			$this->Cell(11,$h,'',0,0,'L');
			$this->Cell(30,$h,'',0,0,'R');
			$this->Cell(26,$h,'',0,0,'C');

			$this->Ln(4);

			$this->Ln($Espacio*2);
			$this->Cell(19,$h,'Direcc.',0,0,'L');
			$this->Cell(100,$h,$row['representantedir'],1,0,'L');
			$this->Cell(5,$h,'',0,0,'L');

			$this->Cell(30,$h,'Distrito',0,0,'R');
			$Sql="SELECT descripcion FROM public.ubigeo WHERE codubigeo='".$row['representanteubigeo']."'";
			$consulta=$conexion->prepare($Sql);
			$consulta->execute();
			$rowU = $consulta->fetch();
			$this->Cell(32,$h,$rowU[0],1,0,'L');
			/////////
			//////////
			$this->Ln(16);
			$this->Cell(0,0.01,'',1,0,'L');
			$this->Ln(6);
			$this->SetFont('courier','B',$Letra);
			$text ="DATOS DE LA CONEXION";
			$this->MultiCell(0,$h,$text,0,'J');
			$this->Ln(6);
			$text ="Lugar de Instalaci�n";
			$this->MultiCell(0,$h,$text,0,'J');
			$this->SetFont('courier','',$Letra);

			$this->Ln(4);

			$this->Ln($Espacio*2);
			$this->Cell(19,$h,'Direcc.',0,0,'L');
			$this->Cell(100,$h,$row['direccion'],1,0,'L');
			$this->Cell(5,$h,'',0,0,'L');

			$this->Cell(30,$h,'Distrito',0,0,'R');
			$Sql="SELECT descripcion FROM public.ubigeo WHERE codubigeo='".$row['titularubigeo']."'";
			$consulta=$conexion->prepare($Sql);
			$consulta->execute();
			$rowU = $consulta->fetch();
			$this->Cell(32,$h,$rowU[0],1,0,'L');

			$this->Ln(4);
			$this->Ln($Espacio*2);
			$this->Cell(24,$h,'Servicio',0,0,'L');
			$Sql="SELECT descripcion FROM public.tiposervicio WHERE codtiposervicio=".$row['codtiposervicio']."";
			$consulta=$conexion->prepare($Sql);
			$consulta->execute();
			$rowU = $consulta->fetch();
			$this->Cell(40,$h,$rowU[0],1,0,'L');
			$this->Cell(10,$h,'',0,0,'L');
			$this->Cell(14,$h,'Tipo',0,0,'R');
			$periodo="PERMANENTE";
			if($row['periodo']==2) $periodo="TEMPORAL";
			$this->Cell(40,$h,$periodo,1,0,'L');
			$this->Cell(10,$h,'',0,0,'L');
			$this->Cell(14,$h,'Meses',0,0,'R');
			$this->Cell(10,$h,$row['meses'],1,0,'L');

			///////////
			$this->Ln(4);
			$this->Ln($Espacio*2);
			$this->Cell(24,$h,'Medidor',0,0,'L');
			$this->Cell(40,$h,$row['nromedidor'],1,0,'L');
			$this->Cell(10,$h,'',0,0,'L');
			$this->Cell(14,$h,'Diametro',0,0,'R');
			$Sql="SELECT descripcion FROM public.diametrosagua WHERE coddiametrosagua=".$row['coddiametrosagua']."";
			$consulta=$conexion->prepare($Sql);
			$consulta->execute();
			$rowU = $consulta->fetch();
			$this->Cell(20,$h,$rowU[0],1,0,'L');
			$this->Cell(10,$h,'',0,0,'L');
			$this->Cell(24,$h,'Categor�a',0,0,'R');
			$Sql="SELECT nomtar FROM facturacion.tarifas WHERE catetar=".$row['catetar']." AND codsuc=".$codsuc;
			$consulta=$conexion->prepare($Sql);
			$consulta->execute();
			$rowU = $consulta->fetch();
			$this->Cell(40,$h,$rowU[0],1,0,'L');


			///////////
			$this->Ln(8);
			$this->Ln($Espacio*2);
			$this->Cell(58,$h,'Emisi�n Facturaciones',0,0,'L');
			$this->Cell(38,$h,'FIN DE MES',1,0,'L');
			$this->Cell(8,$h,'',0,0,'L');
			$this->Cell(60,$h,'Existen Puntos de Agua',0,0,'R');
			$puntos="SI";
			if($row['puntosagua']==2) $puntos="NO";
			$this->Cell(8,$h,$puntos,1,0,'L');

			///////////
			$this->Ln(4);
			$this->Ln($Espacio*2);
			$this->Cell(35,$h,'Forma de Pago',0,0,'L');
			$tipopago="CONTADO";
			$nrocuota= '';
			$cuotames= '';
			$interes ='';
			if($row['tipopago']==2)
			{
				$tipopago="CREDITO";
				$nrocuota= $row['nrocuota'];
				$cuotames= number_format($row['cuotames'],2);
				$interes= number_format($row['interes'],2)."%";
			}
			$this->Cell(25,$h,$tipopago,1,0,'L');
			$this->Cell(7,$h,'',0,0,'L');
			$this->Cell(14,$h,'Cuotas',0,0,'R');
			$this->Cell(10,$h,$nrocuota,1,0,'L');
			$this->Cell(20,$h,'',0,0,'L');
			$this->Cell(14,$h,'Mensual S/.',0,0,'R');
			$this->Cell(20,$h,$cuotames,1,0,'R');
			$this->Cell(10,$h,'',0,0,'L');
			$this->Cell(12,$h,'Interes ',0,0,'R');
			$this->Cell(15,$h,$interes,1,0,'L');
			///////////
			$this->SetFont('courier','',$Letra-2);
			$this->Ln(4);
			$this->Ln($Espacio*2);
			$this->Cell(112,$h,'Solo para Servicios Temporales Fondo de Garant�a S/.',0,0,'L');
			$penalidad='';
			$garantia='';
			if($row['periodo']==2)
			{
				$penalidad=number_format($row['penalidad'],2);
				$garantia=number_format($row['garantia'],2);
			}
			$this->Cell(20,$h,$garantia,1,0,'R');
			$this->Cell(35,$h,'Penalidad S/.',0,0,'R');
			$this->Cell(20,$h,$penalidad,1,0,'R');



		}
		function pie($h,$nrocontrato,$Nombre,$direccion)
		{
			$text="Contrato ".$nrocontrato.".          Nombre ".$Nombre.".        Direcci�n ".$direccion;
			$this->MultiCell(0,$h,$text,0,'J');
		}
		function Footer()
		{}
	}

	

	
	//print_r($impgasadm);
	$codsuc 		= $_GET["codsuc"];
	$codexpediente	= $_GET["Id"];
	
	$consulta = $conexion->prepare("SELECT * FROM solicitudes.expedientes WHERE codemp = 1 AND codsuc = ? AND codexpediente = ?");
	$consulta->execute(array($codsuc, $codexpediente));
	$row = $consulta->fetch();
	
	$nrocontrato = $row['nrocontrato'];
	$objReporte = new clsPresupuesto($orientation='P', $unit='mm', $format='A4');
	
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codexpediente, $codsuc);
	$objReporte->Output();
?>