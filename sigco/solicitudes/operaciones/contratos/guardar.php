<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include('../../../../objetos/clsFunciones.php');
    include("../../../../objetos/clsseguridad.php");
	
    $objSeguridad = new clsSeguridad();
	
    $conexion->beginTransaction();
	
    $objFunciones = new clsFunciones();
	
    $codsuc              = $_SESSION['IdSucursal'];
    $fechacontrato       = $objFunciones->CodFecha($_POST['fechaemision']);
    $codtiposervicio     = $_POST['tiposervicio'];
    $periodo             = $_POST['periodo'];
    $meses               = $_POST['meses']?$_POST['meses']:0;
    $garantia            = $_POST['garantia']?$_POST['garantia']:0;
    $penalidad           = $_POST['penalidad']?$_POST['penalidad']:0;
    $nromedidor          = $_POST['nromedidor'];
    $puntosagua          = $_POST['puntosagua'];
    $coddiametrosagua    = $_POST['diametrosagua'];
    $coddiametrosdesague = $_POST['diametrosdesague'];
    $catetar             = $_POST['tarifas'];
    $titular             = $_POST['titular'];
    $titulardir          = $_POST['titulardir'];
    $titulardni          = $_POST['titulardni'];
    $titularruc          = $_POST['titularruc'];
    $titularubigeo       = $_POST['titularubigeo'];
    $representante       = $_POST['representante'];
    $representantedir    = $_POST['representantedir'];
    $representantedni    = $_POST['representantedni'];
    $representanteruc    = $_POST['representanteruc'];
    $representanteubigeo = $_POST['representanteubigeo'];
    $codexpediente       = $_POST['codexpediente'];
    $concontrato         = 1;
    $Op                  = $_GET['Op'];
    $codemp              = 1;
    
    $nroinspeccion= $_POST['nroinspeccion'];
    $codcliente   = $_POST['codcliente'];
    $nroinscripcion= $_POST['nroinscripcion'];
    $propietario= $_POST['propietario'];
    $direccion= $_POST['titulardir'];
    $tipopago= 1;
    $fechareg= date('Y-m-d');
    $codusu = $_SESSION['id_user'];
    /*$propietario= $_POST['propietario'];
    $propietario= $_POST['propietario'];
    $propietario= $_POST['propietario'];
    $propietario= $_POST['propietario'];*/
    
    switch ($Op) 
    {
        case 0:
       
            $nrocontrato = $objFunciones->setCorrelativosVarios(12, $codsuc, "SELECT", 0);
            if($nroinspeccion == '')
            {
                $Sql="UPDATE  solicitudes.expedientes
                    SET 
                      nrocontrato = :nrocontrato,
                      fechacontrato = :fechacontrato,
                      codtiposervicio = :codtiposervicio,
                      periodo = :periodo,
                      meses = :meses,
                      garantia = :garantia,
                      penalidad = :penalidad,
                      concontrato = :concontrato,
                      nromedidor = :nromedidor,
                      puntosagua = :puntosagua,
                      titular = :titular,
                      titulardir = :titulardir,
                      titulardni = :titulardni,
                      titularruc = :titularruc,
                      representante = :representante,
                      representantedir = :representantedir,
                      representantedni = :representantedni,
                      representanteruc = :representanteruc,
                      representanteubigeo = :representanteubigeo,
                      titularubigeo = :titularubigeo,
                      coddiametrosagua = :coddiametrosagua,
                      coddiametrosdesague = :coddiametrosdesague,
                      catetar = :catetar
                      WHERE codemp = :codemp AND codsuc = :codsuc AND  codexpediente = :codexpediente ;";

                $result = $conexion->prepare($Sql);
                $result->execute(array(
                            ":nrocontrato"=>$nrocontrato,
                            ":fechacontrato"=>$fechacontrato,
                            ":codtiposervicio"=>$codtiposervicio,
                            ":periodo"=>$periodo,
                            ":meses"=>$meses,
                            ":garantia"=>$garantia,
                            ":penalidad"=>$penalidad,
                            ":concontrato"=>$concontrato,
                            ":nromedidor"=>$nromedidor,
                            ":puntosagua"=>$puntosagua,
                            ":titular"=>$titular,
                            ":titulardir"=>$titulardir,
                            ":titulardni"=>$titulardni,
                            ":titularruc"=>$titularruc,
                            ":representante"=>$representante,
                            ":representantedir"=>$representantedir,
                            ":representantedni"=>$representantedni,
                            ":representanteruc"=>$representanteruc,
                            ":representanteubigeo"=>$representanteubigeo,
                            ":titularubigeo"=>$titularubigeo,
                            ":coddiametrosagua"=>$coddiametrosagua,
                            ":coddiametrosdesague"=>$coddiametrosdesague,
                            ":catetar"=>$catetar,
                            ":codemp"=>$codemp,
                            ":codsuc"=>$codsuc,
                            ":codexpediente"=>$codexpediente
                                    
                                    ));
                if ($result->errorCode() != '00000') 
                {
                    $conexion->rollBack();
                    $mensaje = "Error update Solicitud";
                    $data['res']       = 2; 
                    die(json_encode($data));
                }

            }else
                {
                    $codexpediente = $objFunciones->setCorrelativos("codexpediente", $codsuc, "0");
                    $codexpediente = $codexpediente[0];
                    
                    $sql="INSERT INTO solicitudes.expedientes( codemp, codsuc, codexpediente, nroexpediente, 
                        codcliente, nroinscripcion, propietario, direccion, fechaemision, tipopago, 
                        fechareg, codusu, nrocontrato, documento, fechacontrato, codtiposervicio, periodo, meses, 
                        concontrato, nromedidor, puntosagua, titular, titulardir, 
                        titulardni, titularruc, representante, representantedir, representantedni, 
                        representanteruc, representanteubigeo, titularubigeo, coddiametrosagua, 
                        coddiametrosdesague, catetar)
                    VALUES ( ".$codemp.", ".$codsuc.", ".$codexpediente.", 0, 
                        ".$codcliente.", '".$nroinscripcion."', '".$propietario."', '".$direccion."',
                        '".$fechareg."', ".$tipopago.", '".$fechareg."', ".$codusu.",
                        '".$nrocontrato."', '".$documento."', '".$fechacontrato."', ".$codtiposervicio.", 
                        '".$periodo."', '".$meses."', ".$concontrato.", '".$nromedidor."',
                        ".$puntosagua.", '".$titular."', '".$titulardir."', 
                        '".$titulardni."', '".$titularruc."', '".$representante."', '".$representantedir."',
                        '".$representantedni."', '".$representanteruc."', '".$representanteubigeo."',
                        '".$titularubigeo."', ".$coddiametrosagua.", ".$coddiametrosdesague.", ".$catetar." )";
                    $result = $conexion->query($sql);
                    
                    if ($result->errorCode() != '00000') 
                    {
                        $conexion->rollBack();
                        $mensaje = "Error Insertar Solicitud";
                        $data['res']       = 2; 
                        die(json_encode($data." ".$mensaje));
                        
                    }
                }
            
                    
        break;
        case 1:
    
           $Sql="UPDATE  solicitudes.expedientes
                    SET 
                      fechacontrato = :fechacontrato,
                      codtiposervicio = :codtiposervicio,
                      periodo = :periodo,
                      meses = :meses,
                      garantia = :garantia,
                      penalidad = :penalidad,
                      concontrato = :concontrato,
                      nromedidor = :nromedidor,
                      puntosagua = :puntosagua,
                      titular = :titular,
                      titulardir = :titulardir,
                      titulardni = :titulardni,
                      titularruc = :titularruc,
                      representante = :representante,
                      representantedir = :representantedir,
                      representantedni = :representantedni,
                      representanteruc = :representanteruc,
                      representanteubigeo = :representanteubigeo,
                      titularubigeo = :titularubigeo,
                      coddiametrosagua = :coddiametrosagua,
                      coddiametrosdesague = :coddiametrosdesague,
                      catetar = :catetar
                      WHERE codemp = :codemp AND codsuc = :codsuc AND  codexpediente = :codexpediente ;";

            $result = $conexion->prepare($Sql);
            $result->execute(array(
                            ":fechacontrato"=>$fechacontrato,
                            ":codtiposervicio"=>$codtiposervicio,
                            ":periodo"=>$periodo,
                            ":meses"=>$meses,
                            ":garantia"=>$garantia,
                            ":penalidad"=>$penalidad,
                            ":concontrato"=>$concontrato,
                            ":nromedidor"=>$nromedidor,
                            ":puntosagua"=>$puntosagua,
                            ":titular"=>$titular,
                            ":titulardir"=>$titulardir,
                            ":titulardni"=>$titulardni,
                            ":titularruc"=>$titularruc,
                            ":representante"=>$representante,
                            ":representantedir"=>$representantedir,
                            ":representantedni"=>$representantedni,
                            ":representanteruc"=>$representanteruc,
                            ":representanteubigeo"=>$representanteubigeo,
                            ":titularubigeo"=>$titularubigeo,
                            ":coddiametrosagua"=>$coddiametrosagua,
                            ":coddiametrosdesague"=>$coddiametrosdesague,
                            ":catetar"=>$catetar,
                            ":codemp"=>$codemp,
                            ":codsuc"=>$codsuc,
                            ":codexpediente"=>$codexpediente
                                    
                                    ));
            if ($result->errorCode() != '00000') 
            {
                $conexion->rollBack();
                $mensaje = "Error update Solicitud";
                $data['res']       = 2; 
                die(json_encode($data));
            }
    break;
    case 2:
        $Sql="UPDATE  solicitudes.expedientes  
                    SET  estadoexpediente=3
                WHERE codemp = :codemp AND codsuc = :codsuc AND  codexpediente = :codexpediente ;";

        $result = $conexion->prepare($Sql);
        $result->execute(array(
                                ":codemp"=>$codemp,
                                ":codsuc"=>$codsuc,
                                ":codexpediente"=>$codexpediente
                              
                               
                                ));
        if ($result->errorCode() != '00000') 
        {
            $conexion->rollBack();
            $mensaje = "Error update Solicitud";
            $data['res']       = 2; 
            die(json_encode($data));
        }
        //LIBERAR PRESUPUESTOS
        $Sql="SELECT nropresupuesto,nrosolicitud
        FROM  solicitudes.expedientesdetalle 
        WHERE codemp=? AND codsuc=? AND  codexpediente=?";
        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array($codemp,$codsuc,$codexpediente));
        $itemsD = $consulta->fetchAll();
        foreach($itemsD as $rowD)
        {   
            if($rowD["nrosolicitud"]!='' && $rowD["nrosolicitud"]!='0')
                {
                    $consulta2 = $conexion->prepare("select * from solicitudes.solicitudes where codemp=1 and codsuc=? and nrosolicitud=?");
                    $consulta2->execute(array($codsuc,$rowD["nrosolicitud"]));
                    $items2 = $consulta2->fetch();
                    $nroinspeccion=$items2['nroinspeccion'];
                    if($nroinspeccion!="" && $nroinspeccion !=0)
                    {
                         $upd = "update catastro.solicitudinspeccion
                                SET estadopresupuesto= 1   
                                WHERE codemp=1 AND  codsuc=".$codsuc." and nroinspeccion=:nroinspeccion";
                        $result = $conexion->prepare($upd);
                        $result->execute(array(":nroinspeccion"=>$nroinspeccion));
                        if($result->errorCode()!='00000')
                        {
                            $conexion->rollBack();
                            $mensaje = "Error update cabpagos";
                            $data['res']       = 2; 
                            die(json_encode($data));
                        }
                    }
                }
             $upd = "update solicitudes.cabpresupuesto SET
                tipopago=?,estadopresupuesto=1
                WHERE codemp=? and codsuc=? and nropresupuesto=?";
                $result = $conexion->prepare($upd);
                $result->execute(array($tipopago, $codemp, $codsuc,$rowD["nropresupuesto"]));
                
                if ($result->errorCode() != '00000') 
                {
                    $conexion->rollBack();
                    $mensaje = "Error update Solicitud";
                    $data['res']       = 2; 
                    die(json_encode($data));
                }
        }
        $objSeguridad->seguimiento($codemp,$codsuc,$codsistema,$codusu,"SE HA ANULADO EL EXPEDIENTE CON CODIGO ".$codexpediente,145);
    break;
}
    if ($Op == 0 || $Op==1) 
    {
        //PRECREDITOS
        for ($i = 1; $i <= $cont; $i++) 
        {
            if (isset($_POST["nrocuota".$i])) {
                $sql = "insert into solicitudes.precreditos(codemp,codsuc,codexpediente,nrocuota,totalcuotas,cuotamensual,interes,
                    igv,redondeo,imptotal) values(:codemp,:codsuc,:codexpediente,:nrocuota,:totalcuotas,:cuotamensual,:interes,:igv,
                    :redondeo,:imptotal)";
                $result = $conexion->prepare($sql);
                $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":codexpediente" => $codexpediente,
                    ":nrocuota" => $_POST["nrocuota".$i], ":totalcuotas" => $_POST["totcuotas".$i],
                    ":cuotamensual" => $_POST["cuotames".$i], ":interes" => $_POST["interes".$i],
                    ":igv" => $_POST["impigv".$i], ":redondeo" => $_POST["red".$i], ":imptotal" => $_POST["tot".$i]));
                if ($result->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "Error update Solicitud";
                    $data['res']       = 2; 
                    die(json_encode($data));
                }
            }
        }

        //PRESUPUESTOS
        $NroItems=$_POST['NroItems'];
        for($i=1;$i<=$NroItems;$i++)
        {
            if(isset($_POST["NroPresupuestoC".$i]))
            {
                $sqlD = "INSERT INTO solicitudes.expedientesdetalle 
                (  codemp,  codsuc,  codexpediente, nropresupuesto, codtipopresupuesto,  codconcepto, nrosolicitud,  subtotal,  igv,  redondeo,  total) 
                VALUES (  :codemp,  :codsuc,  :codexpediente, :nropresupuesto, :codtipopresupuesto, :codconcepto, :nrosolicitud, :subtotal,  :igv,  :redondeo, :total);";
                
                $resultD = $conexion->prepare($sqlD);
                $resultD->execute(array(":codemp"=>$codemp,
                                        ":codsuc"=>$codsuc,
                                        ":codexpediente"=>$codexpediente,
                                        ":nropresupuesto"=>$_POST['NroPresupuestoC'.$i],
                                        ":codtipopresupuesto"=>$_POST['CodTipoPresupuestoC'.$i],
                                        ":codconcepto"=>$_POST['codconceptoC'.$i],
                                        ":nrosolicitud"=>$_POST["NroSolicitud".$i],
                                        ":subtotal"=>str_replace(",","",$_POST["subtotalC".$i]),
                                        ":igv"=>str_replace(",","",$_POST["igvC".$i]),
                                        ":redondeo"=>str_replace(",","",$_POST["redondeoC".$i]),
                                        ":total"=>str_replace(",","",$_POST["imptotalC".$i]),
                                        ));
                if ($resultD->errorCode() != '00000') 
                {
                    $conexion->rollBack();
                    $mensaje = "Error reclamos";
                    $data['res']    =2;
                    die(json_encode($data)) ;
                }
                if($_POST["NroSolicitud".$i]!='' && $_POST["NroSolicitud".$i]!='0')
                {
                    $consulta2 = $conexion->prepare("select * from solicitudes.solicitudes where codemp=1 and codsuc=? and nrosolicitud=?");
                    $consulta2->execute(array($codsuc,$_POST["NroSolicitud".$i]));
                    $items2 = $consulta2->fetch();
                    $nroinspeccion=$items2['nroinspeccion'];
                    if($nroinspeccion!="" && $nroinspeccion !=0)
                    {
                         $upd = "update catastro.solicitudinspeccion
                                SET estadopresupuesto= 2   
                                WHERE codemp=1 AND  codsuc=".$codsuc." and nroinspeccion=:nroinspeccion";
                        $result = $conexion->prepare($upd);
                        $result->execute(array(":nroinspeccion"=>$nroinspeccion));
                        if($result->errorCode()!='00000')
                        {
                            $conexion->rollBack();
                            $mensaje = "Error update cabpagos";
                            $data['res']       = 2; 
                            die(json_encode($data));
                        }
                    }
                }
                $upd = "update solicitudes.cabpresupuesto SET
                tipopago=?,estadopresupuesto=2,codexpediente=?
                WHERE codemp=? and codsuc=? and nropresupuesto=?";
                $result = $conexion->prepare($upd);
                $result->execute(array($tipopago,$codexpediente, $codemp, $codsuc,$_POST["NroPresupuestoC".$i]));
                
                if ($result->errorCode() != '00000') 
                {
                    $conexion->rollBack();
                    $mensaje = "Error update Solicitud";
                    $data['res']       = 2; 
                    die(json_encode($data));
                }

                
            }
        }
    }


    $conexion->commit();
    if($Op==0) 
    $objFunciones->setCorrelativosVarios(12,$codsuc,"UPDATE",$nrocontrato);   
    $mensaje = "El Registro se ha Grabado Correctamente";
    $data['res']       = 1; 
    echo json_encode($data);
    
    
?>
