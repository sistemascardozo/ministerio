<?php
    session_name("pnsu");
    if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");

    $objMantenimiento = new clsDrop();
    $Op = $_POST["Op"];
    $id = $_POST["Id"];
    $codsuc = $_SESSION['IdSucursal'];

    $paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);

    $fecha = date("d/m/Y");
    $puntosagua = 1;

    $nrocontrato = $objMantenimiento->setCorrelativosVarios(12, $codsuc, "SELECT", 0);

    $Sql = "SELECT codubigeo FROM admin.sucursales WHERE codsuc=?";
    $consulta = $conexion->prepare($Sql);
    $consulta->execute(array($codsuc));
    $rowS = $consulta->fetch();
    $distrito = $rowS['codubigeo'];
    $distrito = substr($distrito, 0, 4)."%";
    $seleccion1 = $rowS['codubigeo'];
    $seleccion2 = $rowS['codubigeo'];

    if ($id != '') {
        $consulta = $conexion->prepare("SELECT * FROM solicitudes.expedientes WHERE codemp = 1 AND codsuc = ? AND codexpediente = ?");
        $consulta->execute(array($codsuc, $id));
        $row = $consulta->fetch();

        $fech = new DateTime($row["fechaemision"]);
        $fecha = $fech->format("d/m/Y");
        $TasaInteres = $row['interes'] / 100;
        $tasapromintant = $row['interes'];
        $nrocontrato = $row['nrocontrato'];
        $puntosagua = $row['puntosagua'];

        $seleccion1 = $row['titularubigeo'];
        $seleccion2 = $row['representanteubigeo'];
    }

////////77
?>
<style type="text/css">
    .CodTipoPresupuesto, .NroPresupuesto,.CodConcepto,.NroSolicitud{display: none;}
</style>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    var IdAnt = ''
    var filaant = 0
    var impigv = <?=$paramae["valor"]?>;
    var contador = 0;
    var cont = 0;
    var urldir = '<?php echo $_SESSION['urldir'];?>';

    $(document).ready(function () {
        $("#tabs").tabs();
        PeriodoUpd2()
        $('form').keypress(function (e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
        });

    });

    function ValidarForm(Op)
    {

        if ($("#codcliente").val() == 0 || $("#codcliente").val() == '')
        {
            Msj($("#codcliente"), "Seleccione Expediente")
            return false
        }
        $("#nrosolicitud").removeClass("ui-state-error");

        if ($("#tiposervicio").val() == 0 || $("#tiposervicio").val() == '')
        {
            Msj($("#tiposervicio"), 'Seleccione Tipo de Servicio')
            return false
        }
        $("#nrosolicitud").removeClass("ui-state-error");

        if ($("#tarifas").val() == 0)
        {
            Msj($("#tarifas"), 'Seleccione Tarifa')
            return false
        }
        $("#nrosolicitud").removeClass("ui-state-error");

        t = parseInt($("#titulardni").val().length);
        if (t > 0)
        {
            if (t != 8)
            {
                Msj($("#titulardni"), "Ingrese un DNI correcto");
                $("#titulardni").focus();
                return false;
            }
            $("#titulardni").removeClass("ui-state-error");
        }

        t = parseInt($("#titularruc").val().length);
        if (t > 0)
        {
            if (t != 11)
            {
                Msj($("#titularruc"), "Ingrese un RUC correcto");
                $("#titularruc").focus();
                return false;
            }
            $("#titularruc").removeClass("ui-state-error");
        }

        t = parseInt($("#representantedni").val().length);
        if (t > 0)
        {
            if (t != 8)
            {
                Msj($("#representantedni"), "Ingrese un DNi correcto");
                $("#representantedni").focus();
                return false;
            }
            $("#representantedni").removeClass("ui-state-error");
        }

        t = parseInt($("#representanteruc").val().length);
        if (t > 0)
        {
            if (t != 11)
            {
                Msj($("#representanteruc"), "Ingrese un RUC correcto");
                $("#representanteruc").focus();
                return false;
            }
            $("#representanteruc").removeClass("ui-state-error");
        }
        $(".ui-dialog-buttonpane button:contains('Agregar')").button("disable");
        GuardarAPresupuesto(Op)
    }

    function GuardarAPresupuesto(Op)
    {
        ///CALCULAR IMPUT DE COLATERALES

        $.ajax({
            url: 'guardar.php?Op=<?=$Op?>',
            type: 'POST',
            async: true,
            dataType: "json",
            data: $('#form1').serialize(), //+'&0form1_idusuario=<?=$IdUsuario?>&3form1_fechareg=<?=$Fecha?>',
            success: function (datos) {

                $(".ui-dialog-buttonpane button:contains('Agregar')").button("enable");
                OperMensaje(datos.res)

                if (datos.res == 1)
                {
                    if (confirm("Desea Imprimir el Contrato?"))
                        AbrirPopupImpresion("imprimir.php?Id=" + $("#codexpediente").val() + "&codsuc=<?=$codsuc?>", 800, 600)

                    $("#Mensajes").html(datos.res);
                    $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                    $("#Nuevo").dialog("close");
                    $("#Modificar").dialog("close");
                    $("#ConfirmaEliminacion").dialog("close");
                    $("#ConfirmaRestauracion").dialog("close");
                }
                Buscar(Op);
            }
        })
    }
    function Cancelar()
    {
        location.href = '../';
    }


    function PeriodoUpd(obj)
    {

        if ($("#periodo").val() == 1)
        {
            $(".dTemporal").hide();
            //$("#LblDMonto").html('Monto a Fraccionar')
        } else {
            $(".dTemporal").show();
        }

        $("#meses,#garantia,#penalidad").val(0)
    }
    function PeriodoUpd2(obj)
    {
        if ($("#periodo").val() == 1)
        {
            $(".dTemporal").hide();
            //$("#LblDMonto").html('Monto a Fraccionar')
        } else {
            $(".dTemporal").show();
        }
    }

    function Recibir(id)
    {

        if (object == "nropresupuesto")
        {
            datos_expediente(id)
        }
    }

    function datos_expediente(id)
    {
        $("#codexpediente").val(id)
        $.ajax({
            url: '../../../../ajax/mostrar/expediente.php',
            type: 'POST',
            async: true,
            data: 'codexpediente='+id+"&codsuc=<?=$codsuc?>&Op=1",
            dataType: "json",
            success: function (datos)
            {
                $("#nroexpediente").val(datos.nroexpediente)
                $("#nroinscripcion").val(datos.nroinscripcion)
                $("#codcliente").val(datos.codcliente)
                $("#propietario,#titular,#representante").val(datos.propietario)
                $("#titulardir,#representantedir").val(datos.direccion)
                $("#tipopago").val(datos.tipopago)
                $("#nrocuota").val(datos.nrocuota)
                $("#cuotames").val(parseFloat(datos.cuotames).toFixed(2))
                $("#interes").val(parseFloat(datos.interes).toFixed(2))
                if (datos.tipopago == 1)
                    $(".Fpago").hide()
                else
                    $(".Fpago").show()
                //datos_usuarios(datos.nroinscripcion)

				if (datos.documento.substr(0, 3) == 'DNI')
				{
					$("#propietariodni, #titulardni, #representantedni").val(datos.documento.substr(6));
				}
				if (datos.documento.substr(0, 3) == 'RUC')
				{
					$("#propietarioruc, #titularruc, #representanteruc").val(datos.documento.substr(6));
				}
				//alert($('#tiposervicio').val());
				$('#tiposervicio option[value="' + $('#tiposervicio').val() + '"]').removeAttr("selected");
				$('#tiposervicio option[value="' + datos.codtiposervicio + '"]').attr("selected", "selected").change();
				//$("#tiposervicio").val(datos.codtiposervicio);
            }
        })
    }

    function buscar_expediente()
    {
        object = "nropresupuesto";
        AbrirPopupBusqueda('../../../solicitudes/operaciones/expedientes/?Op=5', 1100, 500);
    }

    function  buscar_servicio()
    {
        object = "solicitudinspeccion"
        AbrirPopupBusqueda('../../../catastro/operaciones/solicitud/indexInspeccion.php?Op=5', 1110, 450)
    }

    function RecibirP(id,nroinscripcion)
    {
        if (object == "solicitudinspeccion")
        {            //alert('');
            datos_solicitudinspeccion(id,nroinscripcion)
        }
    }

    function datos_solicitudinspeccion(nroinspeccion,nroinscripcion)
    {
        $.ajax({
            url: urldir+'ajax/usernuevoontrato.php',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: 'codsuc=<?=$codsuc?>&nroinspeccion='+nroinspeccion,
            success: function (datos) {

                $("#propietario").val(datos.propietario)
                $("#codcliente").val(datos.codcliente)
                $("#nroinscripcion").val(datos.nroinscripcion)
                $("#tiposervicio").val(datos.codtiposervicio)
                $("#titulardni").val(datos.nrodocumento)
                $("#representantedni").val(datos.nrodocumento)

                $("#titulardir").val(datos.direccion)
                $("#titular").val(datos.propietario)
                $("#representante").val(datos.propietario)
                $("#representantedir").val(datos.direccion)
                $("#nroinspeccion").val(datos.nroinspeccion)

                //cargar_datos_periodo_facturacion(datos.codciclo)
                //$("#codciclo").val(datos.codciclo)
            }
        })


    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?=$Guardar?>" enctype="multipart/form-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td class="TitDetalle" align="right">N° Contrato</td>
                    <td align="center">:</td>
                    <td >
                        <input type="text" id="nrocontrato" class="inputtext" maxlength="60" readonly="readonly" value="<?=$nrocontrato?>" style="width:60px;"/>
                    </td>
                </tr>
                <tr>
                <tr>
                    <td class="TitDetalle" align="right"><span class="CampoDetalle">Fecha de Emsion </span></td>
                    <td class="TitDetalle" align="center">:</td>
                    <td colspan="10" class="CampoDetalle">
                        <input name="fechaemision" type="text" id="fechaemision" maxlength="10" class="inputtext fecha" value="<?=$fecha?>" style="width:85px;"/>

                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">N° Expediente</td>
                    <td align="center">:</td>
                    <td >
                        <input type="text" id="nroexpediente" name="nroexpediente" class="inputtext" maxlength="60" readonly="readonly" value="<?=$row['nroexpediente']?>" style="width:60px;"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Nro. Estudio </td>
                    <td class="TitDetalle" align="center">:</td>
                    <td class="CampoDetalle" colspan="6">
                        <input name="nroinspeccion" type="text" id="nroinspeccion" maxlength="10" value="<?=$solicitudes["nroinspeccion"]?>" readonly="readonly" class="inputtext" style="width:60px;" />
                        <span class="MljSoft-icon-buscar" title="Buscar Estudio de Factibilidad" onclick="buscar_servicio();"></span>
                        Solo contrato
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Usuario</td>
                    <td class="TitDetalle" align="center">:</td>
                    <td colspan="10" class="CampoDetalle">
                        <input name="codexpediente" id="codexpediente" type="hidden" value="<?=$row['codexpediente'] ?>">

                        <input type="hidden" name="nroinscripcion" id="nroinscripcion" value="<?=$row['nroinscripcion'] ?>"/>
                        <input type="text" name="codcliente" id="codcliente" class="inputtext" readonly="readonly" value="<?=$row["codcliente"] ?>" style="width:60px;" />
                        <span class="MljSoft-icon-buscar buscarcliente" title="Buscar Clientes" onclick="buscar_expediente();"></span>
                        <input class="inputtext" name="propietario" type="text" id="propietario" size="60" maxlength="200" value="<?=($row["propietario"]) ?>" placeholder="Datos del Usuario" />
                    </td>
                </tr>
                <tr style="height:5px">
                    <td colspan="12" class="TitDetalle">
                    <legend class="legend ui-state-default ui-corner-all">Datos de la Conexión</legend>
                </td>
                </tr>

                <tr>
                    <td align="right">Tipo de Servicio</td>
                    <td align="center">:</td>
                    <td>
                        <?php $objMantenimiento->drop_tiposervicio($row["codtiposervicio"],"",''); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right">Periodo</td>
                    <td align="center">:</td>
                    <td>
                        <select name="periodo" id="periodo" class="select" onchange="PeriodoUpd(this)">
                            <option value="1" <?php if ($row['periodo'] == 1) echo "selected='selected'" ?> >INDEFINIDO</option>
                            <option value="2" <?php if ($row['periodo'] == 2) echo "selected='selected'" ?> >TEMPORAL</option>
                        </select>
                        &nbsp;&nbsp;
                        <div style="display:inline" class="dTemporal">
                            &nbsp;Meses&nbsp;:&nbsp;
                            &nbsp;<input type="text" name="meses" id="meses" class="entero inputtext" size="5" maxlength="10" value="<?=$row['meses'] ?>"  />                            &nbsp;Garantía&nbsp;:&nbsp;
                            &nbsp;<input type="text" id="garantia" name="garantia" class="numeric inputtext" size="13" maxlength="10" value="<?=number_format($row['garantia'], 2) ?>"  />
                            &nbsp;Penalidad&nbsp;:&nbsp;
                            &nbsp;<input type="text" id="penalidad" name="penalidad" class="numeric inputtext" size="13" maxlength="10" value="<?=number_format($row['penalidad'], 2) ?>"  />
                        </div>

                    </td>
                </tr>
                <tr>
                    <td align="right">N° Medidor</td>
                    <td align="center">:</td>
                    <td>
                        <input type="text" name="nromedidor" id="nromedidor" class="inputtext" size="20" maxlength="10" value="<?=$row['nromedidor']?>" placeholder="Nro. Medidor" />
                    </td>
                </tr>
                <tr>
                    <td align="right">Puntos de Agua</td>
                    <td align="center">:</td>
                    <td>
                        <?php
                        if ($puntosagua == 1) {
                            $ck1 = "checked=''";
                            $ck2 = "";
                        } else {
                            $ck1 = "";
                            $ck2 = "checked";
                        }
                        ?>
                        <div class="buttonset" style="display:inline">
                            <input type="radio" name="puntosagua" id="puntosagua1" value="1" <?php echo $ck1; ?> />
                            <label for="puntosagua1">Si</label>
                            <input type="radio" name="puntosagua" id="puntosagua0" value="0" <?php echo $ck2; ?> />
                            <label for="puntosagua0">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right">Diametro Agua</td>
                    <td align="center">:</td>
                    <td>
                        <?php $objMantenimiento->drop_diametros_agua($row["coddiametrosagua"], ''); ?>
                        <div style="display:inline" >
                            &nbsp;Diametro Desague&nbsp;:&nbsp;
                            <?php $objMantenimiento->drop_diametros_desague($row["coddiametrosdesague"], ''); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right">Tarifa</td>
                    <td align="center">:</td>
                    <td>
                        <?php $objMantenimiento->drop_tarifas($codsuc, $row['catetar']); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right">Fomr Pago</td>
                    <td align="center">:</td>
                    <td>
                        <select name="tipopago" id="tipopago" class="select" disabled="disabled">

                            <option value="1" <?php if ($row['tipopago'] == 1) echo "selected='selected'" ?> >CONTADO</option>
                            <option value="2" <?php if ($row['tipopago'] == 2) echo "selected='selected'" ?> >CREDITO</option>
                        </select>
                        &nbsp;&nbsp;
                        <div style="display:inline" class="Fpago">
                            &nbsp;Cuotas&nbsp;:&nbsp;
                            &nbsp;<input type="text" id="nrocuota" class="entero inputtext" size="5" maxlength="10" value="<?=$row['nrocuota'] ?>"  />
                            &nbsp;Mensual&nbsp;:&nbsp;
                            &nbsp;<input type="text" name="cuotames" id="cuotames" class="numeric inputtext" size="13" maxlength="10" value="<?=number_format($row['garantia'], 2) ?>"  />
                            &nbsp;Interes&nbsp;:&nbsp;
                            &nbsp;<input type="text" name="interes" id="interes" class="numeric inputtext" size="13" maxlength="10" value="<?=number_format($row['penalidad'], 2) ?>"  />
                        </div>
                    </td>
                </tr>
                <tr style="height:5px">
                    <td colspan="12" class="TitDetalle">
                        <legend class="legend ui-state-default ui-corner-all">Datos del Titular</legend>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Nombres</td>
                    <td align="center">:</td>
                    <td ><input class="inputtext" name="titular" type="text" id="titular" size="60" maxlength="200" value="<?=($row["titular"]) ?>" placeholder="DATOS DEL TITULAR" /></td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Direccion</td>
                    <td align="center">:</td>
                    <td ><input type="text" name="titulardir" id="titulardir" class="inputtext" size="60" maxlength="60"  value="<?=utf8_encode($row["titulardir"]) ?>" placeholder="DIRECCION DEL TITULAR" /></td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">DNI</td>
                    <td align="center">:</td>
                    <td >
                        <input type="text" name="titulardni" id="titulardni" class="inputtext entero" maxlength="8"  value="<?=utf8_encode($row["titulardni"]) ?>" style="width:80px;" placeholder="DNI" />

                        <div style="display:inline">
                            &nbsp;RUC&nbsp;:&nbsp;
                            &nbsp;<input type="text" id="titularruc" name="titularruc" class="entero inputtext" size="13" maxlength="11" value="<?=$row['titularruc'] ?>" style="width:100px;" placeholder="RUC"  />
                            &nbsp;Distrito&nbsp;:&nbsp;
                            &nbsp;

                            <?php
                                $objMantenimiento->drop_ubigeo("titularubigeo", "--Seleccione un Distrito--", " where codubigeo like ? and not codubigeo like '%00'", array($distrito), "", $seleccion1);
                            ?>

                        </div>
                    </td>
                </tr>
                <tr style="height:5px">
                    <td colspan="12" class="TitDetalle">
                    <legend class="legend ui-state-default ui-corner-all">Datos del Representante</legend>
                </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Nombres</td>
                    <td align="center">:</td>
                    <td ><input class="inputtext" name="representante" type="text" id="representante" size="60" maxlength="200" value="<?=($row["representante"]) ?>" placeholder="DATOS DEL REPRESENTANTE"  /></td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Direccion</td>
                    <td align="center">:</td>
                    <td ><input type="text" name="representantedir" id="representantedir" class="inputtext" size="60" maxlength="60"  value="<?=utf8_encode($row["representantedir"]) ?>" placeholder="DIRECCION DEL REPRESENTANTE" /></td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">DNI</td>
                    <td align="center">:</td>
                    <td >
                        <input type="text" name="representantedni" id="representantedni" class="inputtext entero" maxlength="8"  value="<?=utf8_encode($row["representantedni"]) ?>" style="width:80px;" placeholder="DNI" />

                        <div style="display:inline">
                            &nbsp;RUC&nbsp;:&nbsp;
                            &nbsp;<input type="text" id="representanteruc" name="representanteruc" class="entero inputtext" maxlength="11" value="<?=$row['representanteruc'] ?>" style="width:100px;" placeholder="RUC" />
                            &nbsp;Distrito&nbsp;:&nbsp;
                            &nbsp;

                            <?php
                                $objMantenimiento->drop_ubigeo("representanteubigeo", "--Seleccione un Distrito--", " where codubigeo like ? and not codubigeo like '%00'", array($distrito), "", $seleccion2);
                            ?>

                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
