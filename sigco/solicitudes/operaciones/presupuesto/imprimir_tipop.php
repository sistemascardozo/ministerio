<?php
    include("../../../../objetos/clsReporte.php");

    class clsPresupuesto extends clsReporte {

        function cabecera() {
            global $nropresupuesto, $anio; //nn
            $this->SetFont('Arial', 'B', 10);

            $tit1 = "PRESUPUESTO N° ".substr("0000", 0, 4 - strlen($nropresupuesto)).$nropresupuesto."  ".$anio;
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
        }

        function contenido($id, $codsuc, $impgasadm) {
            global $conexion;

            $h = 4;
            $s = 2;

            $paramae = $this->getParamae("RESSOL", $codsuc);

            $sql = "SELECT c.tipopresupuesto,c.propietario,c.nrosolicitud,c.fechaemision as fechapre,
                tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,c.area,td.abreviado,c.nrodocumento,
                c.metros,tr.descripcion as tipoterreno,da.descripcion as diamAgua,
                c.nroinscripcion,dd.descripcion as diamDesague,ep.descripcion as estadopresupuesto,
                tp.descripcion tpd1, tp.descripcion2 tpd2, u.descripcion unid_descripcion,
                c.impgasadm, c.redondeo,cp.nroinspeccion, dp.serie, dp.nrodocumento AS cor,ex.nrocontrato,
                cp.fechareg, so.nrosolicitud, ex.nroexpediente, ex.concredito as concredito,ex.codexpediente as codexpediente, c.igv
                FROM solicitudes.cabpresupuesto as c
                INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
                INNER JOIN public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
                INNER JOIN public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
                INNER JOIN public.tipoterreno as tr on(c.codtipoterreno=tr.codtipoterreno)
                INNER JOIN public.diametrosagua as da on(c.coddiametrosagua=da.coddiametrosagua)
                INNER JOIN public.diametrosdesague as dd on (c.coddiametrosdesague=dd.coddiametrosdesague) 
                INNER JOIN public.estadopresupuesto ep on (ep.estadopresupuesto=c.estadopresupuesto)
                INNER JOIN public.tipopresupuesto tp on (cast(c.codtipopresupuesto as integer)=tp.codtipopresupuesto)
                LEFT  join public.unidades as u on(tp.unimed=u.unimed)
                LEFT JOIN solicitudes.solicitudes so on (so.codemp=c.codemp) AND (so.codsuc=c.codsuc) AND (so.nrosolicitud=c.nrosolicitud)
                LEFT JOIN cobranza.detpagos AS dp on (dp.codemp= c.codemp AND dp.codsuc= c.codsuc AND dp.nropago= c.nropago)
                LEFT JOIN solicitudes.expedientesdetalle AS dex ON (c.codemp= dex.codemp AND c.codsuc= dex.codsuc AND c.nropresupuesto= dex.nropresupuesto)
                LEFT JOIN solicitudes.expedientes AS ex ON (dex.codemp= ex.codemp AND dex.codsuc= ex.codsuc AND dex.codexpediente= ex.codexpediente)
                LEFT JOIN cobranza.cabpagos AS cp ON (cp.codemp = dp.codemp AND cp.codsuc = dp.codsuc AND cp.nropago = dp.nropago AND cp.nroinscripcion = dp.nroinscripcion)
                WHERE 
                    c.codemp=1 and c.codsuc=? and c.nropresupuesto=?
                GROUP BY
                    ex.codexpediente, c.tipopresupuesto,c.propietario,c.nrosolicitud,c.fechaemision, tc.descripcioncorta,cl.descripcion,
                    c.nrocalle,c.area,td.abreviado,c.nrodocumento, c.metros,tr.descripcion,da.descripcion, 
                    c.nroinscripcion ,dd.descripcion,ep.descripcion , ex.concredito,
                    tp.descripcion, tp.descripcion2, u.descripcion, c.impgasadm, c.redondeo,
                    cp.nroinspeccion, dp.serie, dp.nrodocumento, ex.nrocontrato, cp.fechareg, so.nrosolicitud, ex.nroexpediente, c.igv";
            $consulta = $conexion->prepare($sql);
            $consulta->execute(array($codsuc, $id));
            $presupuesto = $consulta->fetch();
            if ($presupuesto['tpd1'] == '') {
                $sql = "SELECT c.tipopresupuesto,c.propietario,c.nrosolicitud,c.fechaemision as fechapre,
                    tc.descripcioncorta,cl.descripcion as calle,cp.nroinspeccion,
                    c.nrocalle,c.area,td.abreviado,c.nrodocumento,c.metros,tr.descripcion as tipoterreno,
                    da.descripcion as diamAgua, dp.serie, dp.nrodocumento AS cor,
                    c.nroinscripcion, dd.descripcion as diamDesague,ep.descripcion as estadopresupuesto,'' as fechasol, 
                    tp.descripcion tpd1, tp.descripcion2 tpd2, u.descripcion unid_descripcion,c.impgasadm, c.redondeo, ex.nroexpediente, c.igv
                    FROM solicitudes.cabpresupuesto as c
                    INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
                    INNER JOIN public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
                    INNER JOIN public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
                    INNER JOIN public.tipoterreno as tr on(c.codtipoterreno=tr.codtipoterreno)
                    INNER JOIN public.diametrosagua as da on(c.coddiametrosagua=da.coddiametrosagua)
                    INNER JOIN public.diametrosdesague as dd on (c.coddiametrosdesague=dd.coddiametrosdesague) 
                    INNER JOIN public.estadopresupuesto ep on (ep.estadopresupuesto=c.estadopresupuesto)
                    INNER JOIN public.tipopresupuesto tp on (cast(c.codtipopresupuesto as integer)=tp.codtipopresupuesto)
                    LEFT JOIN public.unidades as u on(tp.unimed=u.unimed)
                    INNER JOIN solicitudes.solicitudes so on (so.codemp=c.codemp AND so.codsuc=c.codsuc AND so.nrosolicitud=c.nrosolicitud)
                    LEFT JOIN cobranza.detpagos AS dp on (dp.codemp= c.codemp AND dp.codsuc= c.codsuc AND dp.nropago= c.nropago)
                    LEFT JOIN solicitudes.expedientesdetalle AS dex ON (c.codemp= dex.codemp AND c.codsuc= dex.codsuc AND c.nropresupuesto= dex.nropresupuesto)
                    LEFT JOIN solicitudes.expedientes AS ex ON (dex.codemp= ex.codemp AND dex.codsuc= ex.codsuc AND dex.codexpediente= ex.codexpediente)
                    LEFT JOIN cobranza.cabpagos AS cp ON (cp.codemp = dp.codemp AND cp.codsuc = dp.codsuc AND cp.nropago = dp.nropago AND cp.nroinscripcion = dp.nroinscripcion)
                    WHERE 
                        c.codemp=1 and c.codsuc=? and c.nropresupuesto=?
                    GROUP BY
                        c.tipopresupuesto,c.propietario,c.nrosolicitud,c.fechaemision, tc.descripcioncorta,cl.descripcion,
                        c.nrocalle,c.area,td.abreviado,c.nrodocumento, c.metros,tr.descripcion,da.descripcion, 
                        c.nroinscripcion ,dd.descripcion,ep.descripcion , 
                        tp.descripcion, tp.descripcion2, u.descripcion, c.impgasadm, c.redondeo,
                        cp.nroinspeccion, dp.serie, dp.nrodocumento, ex.nrocontrato, ex.nroexpediente, c.igv ";
                $consulta = $conexion->prepare($sql);
                $consulta->execute(array($codsuc, $id));
                $presupuesto = $consulta->fetch();
            }

            if ($presupuesto['concredito'] == 1) {

                // Nropresupuesto es igual a nroexpediente en este caso
                $mql = "SELECT DISTINCT cab.nroinspeccion as nroinspeccion, 
                dco.serie,
                dco.nrodocumento,
                co.fechareg
                FROM facturacion.cabcreditos cab
                INNER JOIN cobranza.cabprepagos AS co ON (cab.codemp = co.codemp AND cab.codsuc = co.codsuc AND cab.nrocredito = co.nrocredito AND cab.nroinscripcion = co.nroinscripcion)
                INNER JOIN cobranza.detprepagos AS dco ON (dco.codemp = co.codemp AND dco.codsuc = co.codsuc AND dco.nroinscripcion = co.nroinscripcion AND dco.nroprepago = co.nroprepago)
                WHERE cab.nropresupuesto = ? AND co.estado = 3";
                $consultam = $conexion->prepare($mql);
                $consultam->execute(array($presupuesto['codexpediente']));
                $dat = $consultam->fetch();
                $presupuesto['fechareg']      = $dat['fechareg'];  
                $presupuesto['nroinspeccion'] = $dat['nroinspeccion'];  
                $presupuesto['serie']         = $dat['serie'];  
                $presupuesto['cor']           = $dat['nrodocumento'];  
            }
            
            $this->SetFont('Arial', '', 7);
            $tit3 = utf8_decode("Costos unitarios máximos y cantidades de bienes y servicios, rendimientos de mano de obra, depreciación, gastos generalesy utilidad.");
            $this->Cell(190, 5, $tit3, 0, 1, 'L');
			
			
			$impgasadmV = $impgasadm;
			
            $impgasadm = $presupuesto['impgasadm'];
			$igvC = $presupuesto['igv'];
			
            $this->Cell(25, $h, "Servicio Colateral", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(160, $h, utf8_decode(strtoupper($presupuesto['tpd1']." ".$presupuesto['tpd2'])), 0, 1, 'L');
            $this->SetFont('Arial', '', 7);

            $this->Cell(25, $h, "Unidad de Medida", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(40, $h, $presupuesto['unid_descripcion'], 0, 0, 'L');
            $this->SetFont('Arial', '', 7);

            $this->Cell(25, $h, "Metrado", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(40, $h, $presupuesto["metros"], 0, 0, 'L');

            $this->Cell(25, $h, utf8_decode("Fecha de Emisión"), 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(40, $h, $this->DecFecha($presupuesto["fechapre"]), 0, 1, 'L');
            $this->SetFont('Arial', '', 7);

            $this->Cell(25, $h, "Nombre", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(108, $h, utf8_decode(strtoupper($presupuesto["propietario"])), 0, 0, 'L');

            $nroinscripcion= $presupuesto["nroinscripcion"];
            $this->SetFont('Arial', '', 7);
            $this->Cell(25, $h, utf8_decode("Inscripción"), 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(40, $h, $this->CodUsuario($codsuc, $nroinscripcion), 0, 1, 'L');

            $this->Cell(25, $h, "Direccion", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(108, $h, strtoupper($presupuesto["descripcioncorta"]." ".$presupuesto["calle"]." ".$presupuesto["nrocalle"]), 0, 0, 'L');
            
            $this->SetFont('Arial', '', 7);
            $this->Cell(25, $h, "Contrato", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(40, $h, $presupuesto["nrocontrato"], 0, 1, 'L');
            
            $nrosolicitudInsp= $presupuesto["nroinspeccion"];
            $this->Cell(25, $h, "Solicitud Inspeccion", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(40, $h, str_pad($nrosolicitudInsp, 8, "0", STR_PAD_LEFT), 0, 0, 'L');

            $serie= str_pad($presupuesto["serie"], 3, "0", STR_PAD_LEFT);
            $cor= str_pad($presupuesto["cor"], 8, "0", STR_PAD_LEFT);

            $this->SetFont('Arial', '', 7);
            $this->Cell(25, $h, "Comprobante", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(40, $h, $serie." - ".$cor, 0, 0, 'L');

            $this->SetFont('Arial', '', 7);
            $estado = $presupuesto["estadopresupuesto"];
            $this->Cell(25, $h, "Estado", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(40, $h, utf8_decode(strtoupper($estado)), 0, 1, 'L');

            //
            $this->SetFont('Arial', '', 7);
            $nrosolicitud= $presupuesto["nrosolicitud"];
            $this->Cell(25, $h, "Solicitud", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(40, $h, str_pad($nrosolicitud, 8, "0", STR_PAD_LEFT), 0, 0, 'L');

            $this->SetFont('Arial', '', 7);
            $this->Cell(25, $h, "Fecha de Pago", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(40, $h, $this->DecFecha($presupuesto["fechareg"]), 0, 0, 'L');

            $this->SetFont('Arial', '', 7);
            $this->Cell(25, $h, "Nro. Expediente", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(40, $h, $presupuesto["nroexpediente"], 0, 1, 'L');

            $this->SetFont('Arial', 'B', 8);
            $this->SetWidths(array(10, 70, 15, 10, 15, 15, 15, 15, 10, 15));
            $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "C"));
            $this->Row(array("Item", "Descripcion", "Und.", "Cant", "P. Unit.", "Importe", "Imp. Emp.", "Imp. Usua.", "Af. Igv.", "Mont. Igv."));

            $cadena = "";
            $cadena2 = "";
            $subtotal = 0;
            $impigv = 0;

            $sqlD = "SELECT c.codtipocostos,t.descripcion as tipocostos,d.item,c.descripcion as material,u.abreviado,
                d.cantidad,d.preciounitario,d.subtotal,d.importempresa,d.afectaigv,d.impigv, c.codgrupocostos, 
                gc.descripcion as dgrupocostos
                FROM solicitudes.detpresupuesto d
                INNER JOIN solicitudes.confecpreagudes as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.nroconfeccion=c.nroconfeccion)
                INNER JOIN public.tipocostos as t on(c.codtipocostos=t.codtipocostos)
                INNER JOIN public.grupocostos as gc on (gc.codgrupocostos=c.codgrupocostos)
                INNER JOIN public.unidades as u on(d.unimed=u.unimed)
                WHERE d.codemp=1 and d.codsuc=? and d.nropresupuesto=? order by d.item";
            $consultaD = $conexion->prepare($sqlD);
            $consultaD->execute(array($codsuc, $id));
            $items = $consultaD->fetchAll();

            foreach ($items as $rowD) {
                if ($cadena != $rowD["codtipocostos"]) {
                    $this->SetFont('Arial', 'B', 8);
                    $this->SetWidths(array(190));
                    $this->SetAligns(array("L"));
                    $this->Row(array(strtoupper($rowD["tipocostos"])), array("0"));
                }

                if ($cadena2 != $rowD["codgrupocostos"]) {
                    $this->SetWidths(array(190));
                    $this->SetAligns(array("L"));
                    $this->SetFont('Arial', 'B', 8);
                    $this->Row(array(strtoupper($rowD["dgrupocostos"])), array("0"));
                }
                $this->SetFont('Arial', '', 8);
                $this->SetWidths(array(10, 70, 15, 10, 15, 15, 15, 15, 10, 15));
                $this->SetAligns(array("C", "L", "C", "C", "R", "R", "R", "R", "C", "R"));
                $this->Row(array($rowD["item"], utf8_decode(strtoupper($rowD["material"])), strtoupper($rowD["abreviado"]), number_format($rowD["cantidad"], 2),
                    number_format($rowD["preciounitario"], 2), number_format($rowD["subtotal"], 2), number_format($rowD["importempresa"], 2),
                    number_format($rowD["importeusuario"], 2), $rowD["afectaigv"] == 1 ? "S" : "N", number_format($rowD["impigv"], 2)));

                $impigv += $rowD["impigv"];
                $subtotal += $rowD["subtotal"];
                $cadena = $rowD["codtipocostos"];
                $cadena2 = $rowD["codgrupocostos"];
            }

            $total = $subtotal + $igvC;
            //$redondeoAnt = round($total, 1);
            //$redondeoAct = round($total, 2);
            $redondeo = $presupuesto['redondeo'];

            $this->Ln(8);
            $h = 5;

            $this->Cell(70, $h, utf8_decode("Este Presupuesto no tiene caracter cancelatorio"), 'T', 0, 'L');
            $this->Cell(100, $h, "SUB. TOTAL COSTOS DE LA OPERACION", 'T', 0, 'R');
            $this->Cell(3, $h, ":", 'T', 0, 'R');
            $this->Cell(16, $h, number_format($subtotal, 2), 'T', 1, 'R');

            $this->Cell(170, $h, "GASTOS DE GENERALES Y UTILIDAD (".$impgasadmV."%)", '0', 0, 'R');
            $this->Cell(3, $h, ":", '0', 0, 'R');
            $this->Cell(16, $h, number_format($impgasadm, 2), '0', 1, 'R');

            $this->Cell(170, $h, "IGV", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(16, $h, number_format($igvC, 2), 0, 1, 'R');
            $this->Cell(170, $h, "REDONDEO", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(16, $h, number_format($redondeo, 2), 0, 1, 'R');

            $this->Line(20, $this->GetY(), 50, $this->GetY());
            $this->Line(72, $this->GetY(), 100, $this->GetY());

            $this->Cell(50, $h, utf8_decode("Técnico Comercialización"), 0, 0, 'C');
            $this->Cell(50, $h, utf8_decode("Promoción y Ventas"), 0, 0, 'C');
            $this->Cell(20, $h, "", 0, 0, 'C');

            $this->Cell(50, $h, "TOTAL A CANCELAR", 'TB', 0, 'R');
            $this->Cell(3, $h, ":", 'TB', 0, 'R');
            $this->Cell(16, $h, number_format($total + $redondeo + $impgasadm, 2), 'TB', 1, 'R');
        }

    }

    //print_r($impgasadm);
    $impgasadm = $impgasadm['valor'];
    $codsuc = $_GET["codsuc"];
    $nropresupuesto = $_GET["Id"];

    $consulta = $conexion->prepare("SELECT * FROM reglasnegocio.parame WHERE tippar=? and codsuc=?");
    $consulta->execute(array("IMPPTO", $codsuc));
    $item = $consulta->fetch();
	
    $impgasadm = $item['valor'];
    
	$objReporte = new clsPresupuesto();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->contenido($nropresupuesto, $codsuc, $impgasadm);
    $objReporte->Output();

?>