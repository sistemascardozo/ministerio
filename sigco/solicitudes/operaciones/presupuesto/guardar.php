<?php

//ini_set('display_errors', '1');
//error_reporting(E_ALL);
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include('../../../../objetos/clsFunciones.php');

$objFunciones = new clsFunciones();

$Op = $_GET["Op"];

$codemp = 1;
$codsuc = $_SESSION['IdSucursal'];
$codzona = $_POST["codzona"];
$nropresupuesto = $_POST["nropresupuesto"];
$nrosolicitud = isset($_POST["nrosolicitud"]) ? $_POST["nrosolicitud"] : 0;

if ($nrosolicitud == '') {
    $nrosolicitud = 0;
}

$tipopresupuesto = $_POST["tipopresupuesto"];
$codtipopresupuesto = $_POST["codtipopresupuesto"];
$diametroagua = $_POST["tipopresupuesto"] == 2 ? $_POST["diametrosagua"] : 0;
$diametrodesague = $_POST["tipopresupuesto"] == 3 ? $_POST["diametrosdesague"] : 0;
$metros = $_POST["metros"];
$tipoterreno = $_POST["tipoterreno"];
$totalpresupuesto = str_replace(",", "", $_POST["totpresupuesto"]);
$igv = str_replace(",", "", $_POST["subigv"]);
$subtotal = str_replace(",", "", $_POST["subtotal"]);
$tipodocumento = $_POST["tipodocumento"];
$nrodocumento = $_POST["nrodocumento"];
$area = $_POST["area"];
$codcliente = $_POST["codcliente"];
$propietario = strtoupper($_POST["propietario"]);
$codcalle = $_POST["calles"];
$nrocalle = $_POST["nrocalle"];
$codsector = $_POST["sector"];
$redondeo = $_POST["subredondeo"];
$consolicitud = $_POST["consolicitud"];
$idusuario = $_SESSION['id_user'];
$fechaemision = $objFunciones->CodFecha($_POST["fechaemision"]);
$cont = $_POST["cont"];
$nroinscripcion = $_POST["nroinscripcion"] ? $_POST["nroinscripcion"] : 0;
$nroinspeccion = $_POST["nroinspeccion"] ? $_POST["nroinspeccion"] : 0;
$impgasadm = $_POST["impgasadm"];
$porcentajeigv = $_POST["porcentajeigv"];
$porcentajeimpgasadm = $_POST["porcentajeimpgasadm"];
$correo = $_POST["correo"];
$nrocontrato = $_POST["nrocontrato"];

$estareg = 3;

if ($Op == 0) {
    $nropresupuesto = $objFunciones->setCorrelativosVarios(2, $codsuc, "SELECT", 0);
    $nrocontrato = '0'; //$objFunciones->setCorrelativosVarios(12, $codsuc, "SELECT", 0);

    $sql = "INSERT INTO solicitudes.cabpresupuesto ";
    $sql .= " (codemp, codsuc, nropresupuesto, nrosolicitud, tipopresupuesto, coddiametrosagua, coddiametrosdesague,";
    $sql .= "  metros, codtipoterreno, totalpresupuesto, igv, subtotal, codtipodocumento, nrodocumento, ";
    $sql .= " area, codcliente, propietario, codcalle, nrocalle, fechaemision, codsector, codusu, ";
    $sql .= " redondeo, consolicitud, nrocontrato, impgasadm, porcentajeigv, porcentajeimpgasadm, ";
    $sql .= " codzona, nroinscripcion, correo, codtipopresupuesto) ";
    $sql .= "VALUES(".$codemp.", ".$codsuc.", ".$nropresupuesto.", ".$nrosolicitud.", ".$tipopresupuesto.", ".$diametroagua.", ".$diametrodesague.", ";
    $sql .= " ".$metros.", ".$tipoterreno.", '".$totalpresupuesto."', '".$igv."', '".$subtotal."', ".$tipodocumento.", '".$nrodocumento."', ";
    $sql .= " ".$area.", ".$codcliente.", '".$propietario."', ".$codcalle.", '".$nrocalle."', '".$fechaemision."', ".$codsector.", ".$idusuario.", ";
    $sql .= " '".$redondeo."', ".$consolicitud.", '".$nrocontrato."', '".$impgasadm."', '".$porcentajeigv."', '".$porcentajeimpgasadm."', ";
    $sql .= " ".$codzona.", ".$nroinscripcion.", '".$correo."', ".$codtipopresupuesto.")";
}
if ($Op == 1) {
    $sql = "UPDATE solicitudes.cabpresupuesto ";
    $sql .= "SET nrosolicitud = ".$nrosolicitud.", tipopresupuesto = ".$tipopresupuesto.", coddiametrosagua = ".$diametroagua.", ";
    $sql .= " coddiametrosdesague = ".$diametrodesague.", metros = ".$metros.", codtipoterreno = ".$tipoterreno.", ";
    $sql .= " totalpresupuesto = '".$totalpresupuesto."', igv = '".$igv."', subtotal = '".$subtotal."', codtipodocumento = ".$tipodocumento.", ";
    $sql .= " nrodocumento = '".$nrodocumento."', area = ".$area.", codcliente = ".$codcliente.", propietario = '".$propietario."', ";
    $sql .= " codcalle = ".$codcalle.", nrocalle = '".$nrocalle."', fechaemision = '".$fechaemision."', codsector = ".$codsector.", ";
    $sql .= " codusu = ".$idusuario.", redondeo = '".$redondeo."', consolicitud = ".$consolicitud.", nrocontrato = '".$nrocontrato."', ";
    $sql .= " impgasadm = '".$impgasadm."', porcentajeigv = '".$porcentajeigv."', porcentajeimpgasadm = '".$porcentajeimpgasadm."', ";
    $sql .= " codzona = ".$codzona.", nroinscripcion = ".$nroinscripcion.", correo = '".$correo."', codtipopresupuesto = ".$codtipopresupuesto." ";
    $sql .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND nropresupuesto = ".$nropresupuesto." ";
}

if ($Op == 1 || $Op == 0) {
    $conexion->beginTransaction();
    $result = $conexion->prepare($sql);

    $result->execute(array());

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error INSERT - UPDATE cabpresupuesto";
        $data['res'] = 2;
        $data['mensaje'] = $mensaje." ==>".$sql;

        die(json_encode($data));
    }
}

if ($Op == 2) {
    $conexion->beginTransaction();

    $sql = "UPDATE solicitudes.cabpresupuesto ";
    $sql .= "SET estadopresupuesto = ".$estareg." ";
    $sql .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND nropresupuesto = ".$_POST["1form1_id"]." ";

    $result = $conexion->prepare($sql);
    $result->execute(array());

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error UPDATE cabpresupuesto -ANULAR";
        $data['mensaje'] = $mensaje." ==>".$sql;
        $data['res'] = 2;
        die(json_encode($data));
    }
}

if ($Op == 1 || $Op == 0) {
    $del = "DELETE FROM solicitudes.detpresupuesto ";
    $del .= "WHERE codemp = ".$codemp." ";
    $del .= " AND codsuc = ".$codsuc." ";
    $del .= " AND nropresupuesto = ".$nropresupuesto." ";

    $result = $conexion->prepare($del);

    $result->execute(array());

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error DELETE detpresupuesto";
        $data['mensaje'] = $mensaje." ==>".$del;
        $data['res'] = 2;
        die(json_encode($data));
    }

    for ($i = 1; $i <= $cont; $i++) {
        if (isset($_POST["nroconfeccion".$i])) {
            $item = $_POST["item".$i];
            $cantidad = $_POST["cantidad".$i];
            $preciounitario = str_replace(",", "", $_POST["punitario".$i]);
            $subtotal = str_replace(",", "", $_POST["total".$i]);
            $nroconfeccion = $_POST["nroconfeccion".$i];
            $descripcion = strtoupper($_POST["descripcion".$i]);
            $importempresa = str_replace(",", "", $_POST["impempresa".$i]);
            $importeusuario = str_replace(",", "", $_POST["impusuario".$i]);
            $unimed = $_POST["unimed".$i];
            $impigv = str_replace(",", "", $_POST["impigv".$i]);
            $afectaigv = $_POST["afectaigvtext".$i];
            $afectaempresa = $_POST["afectaempresatext".$i];

            $sqlDet = "INSERT INTO solicitudes.detpresupuesto ";
            $sqlDet .= " (codemp, codsuc, nropresupuesto, item, cantidad, preciounitario, subtotal, ";
            $sqlDet .= " nroconfeccion, descripcion, importempresa, importeusuario, unimed, ";
            $sqlDet .= " impigv, afectaigv, afectaempresa) ";
            $sqlDet .= "VALUES(".$codemp.", ".$codsuc.", ".$nropresupuesto.", ".$item.", '".$cantidad."', '".$preciounitario."', '".$subtotal."', ";
            $sqlDet .= " ".$nroconfeccion.", '".$descripcion."', '".$importempresa."', '".$importeusuario."', ".$unimed.", ";
            $sqlDet .= " '".$impigv."', ".$afectaigv.",".$afectaempresa.")";

            $result = $conexion->query($sqlDet);

            if ($result->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error detpresupuesto";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }
}

if ($nroinspeccion != "" && $nroinspeccion != 0) {
    $upd = "UPDATE catastro.solicitudinspeccion SET estado = 4, nropresupuesto = ".$nropresupuesto.", estadopresupuesto = 1 ";
    $upd .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nroinspeccion = ".$nroinspeccion;

    $result = $conexion->prepare($upd);
    $result->execute(array());

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error update cabpagos";
        $data['res'] = 2;
        die(json_encode($data));
    }
}


$conexion->commit();
//$objFunciones->setCorrelativosVarios(2, $codsuc, "UPDATE", $nropresupuesto);
if ($Op != 2) {
    $objFunciones->setCorrelativosVarios(2, $codsuc, "UPDATE", $nropresupuesto);
}
//$objFunciones->setCorrelativosVarios(12, $codsuc, "UPDATE", $nrocontrato);
$mensaje = "El Registro se ha Grabado Correctamente";
$data['res'] = 1;
$data['nropresupuesto'] = $nropresupuesto;

echo json_encode($data);

?>
