<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsReporte.php");
	
	class clsPresupuesto extends clsReporte
	{
		function cabecera()
		{			
			$this->SetFont('Arial', 'B', 10);
			
			$tit1 = "PRESUPUESTO DE INSTALACION DE CONEXION DOMICILIARIA ";
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
		}
		function contenido($id,$codsuc,$impgasadm)
		{
			global $conexion;
			
			$h = 4;
			$s = 2;
			
			$paramae = $this->getParamae("RESSOL",$codsuc);
			
			$sql = "select c.tipopresupuesto,c.propietario,c.nrosolicitud,c.fechaemision as fechapre,tc.descripcioncorta,cl.descripcion as calle,
					c.nrocalle,c.area,td.abreviado,c.nrodocumento,c.metros,tr.descripcion as tipoterreno,da.descripcion as diamAgua,
					dd.descripcion as diamDesague,ep.descripcion as estadopresupuesto,so.fechaemision as fechasol,c.impgasadm, c.igv, c.redondeo
					from solicitudes.cabpresupuesto as c
					INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
					inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
					inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
					inner join public.tipoterreno as tr on(c.codtipoterreno=tr.codtipoterreno)
					inner join public.diametrosagua as da on(c.coddiametrosagua=da.coddiametrosagua)
					inner join public.diametrosdesague as dd on (c.coddiametrosdesague=dd.coddiametrosdesague) 
					inner join public.estadopresupuesto ep on (ep.estadopresupuesto=c.estadopresupuesto)
					inner join solicitudes.solicitudes so on (so.codemp=c.codemp) AND (so.codsuc=c.codsuc) AND (so.nrosolicitud=c.nrosolicitud)
					where c.codemp=1 and c.codsuc=? and c.nropresupuesto=?";
			$consulta=$conexion->prepare($sql);
			$consulta->execute(array($codsuc,$id));
			$presupuesto = $consulta->fetch();
			if($presupuesto[0]=='')
			{
				$sql = "select c.tipopresupuesto,c.propietario,c.nrosolicitud,c.fechaemision as fechapre,tc.descripcioncorta,cl.descripcion as calle,
					c.nrocalle,c.area,td.abreviado,c.nrodocumento,c.metros,tr.descripcion as tipoterreno,da.descripcion as diamAgua,
					dd.descripcion as diamDesague,ep.descripcion as estadopresupuesto,'' as fechasol,c.impgasadm, c.igv, c.redondeo
					from solicitudes.cabpresupuesto as c
					INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
					inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
					inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
					inner join public.tipoterreno as tr on(c.codtipoterreno=tr.codtipoterreno)
					inner join public.diametrosagua as da on(c.coddiametrosagua=da.coddiametrosagua)
					inner join public.diametrosdesague as dd on (c.coddiametrosdesague=dd.coddiametrosdesague) 
					inner join public.estadopresupuesto ep on (ep.estadopresupuesto=c.estadopresupuesto)
					/*inner join solicitudes.solicitudes so on (so.codemp=c.codemp) AND (so.codsuc=c.codsuc) AND (so.nrosolicitud=c.nrosolicitud)*/
					where c.codemp=1 and c.codsuc=? and c.nropresupuesto=?";
				$consulta=$conexion->prepare($sql);
				$consulta->execute(array($codsuc,$id));
				$presupuesto = $consulta->fetch();
			}
			$PresD="AGUA";
			if($presupuesto[0]==2) $PresD="DESAGUE";
			$tit3	= "DE : ".$PresD;
			$this->Cell(190,5,utf8_decode($tit3),0,1,'C');
			
			$impgasadm = $presupuesto['impgasadm'];
			$igvC = $presupuesto['igv'];
			
			$estado=$presupuesto["estadopresupuesto"];
			$this->Cell(25,$h,"Estado",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,utf8_decode(strtoupper($estado)),0,1,'L');

			$this->Ln(5);
			$this->SetFont('Arial','',7);
			
			$this->Cell(25,$h,"Nro. Presupuesto",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,$id,0,0,'L');
			$this->Cell(25,$h,"Fecha Presupuesto",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(40,$h,$this->DecFecha($presupuesto["fechapre"]),0,1,'L');

			$this->Cell(25,$h,"Nro. Solicitud",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,$presupuesto["nrosolicitud"],0,0,'L');
			
			$this->Cell(25,$h,"Fecha Solicitud",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(40,$h,$this->DecFecha($presupuesto["fechasol"]),0,1,'L');
			
			$this->Cell(25,$h,"Usuario",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,utf8_decode(strtoupper($presupuesto["propietario"])),0,1,'L');
			
			$this->Cell(25,$h,"Direccion",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,strtoupper($presupuesto["descripcioncorta"]." ".$presupuesto["calle"]." ".$presupuesto["nrocalle"]),0,1,'L');
			
			$this->Cell(25,$h,"Area del Inmueble",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,$presupuesto["area"],0,0,'L');
			
			$this->Cell(25,$h,"Documento",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(40,$h,$presupuesto["abreviado"]." - ".$presupuesto["nrodocumento"],0,1,'L');
			
			$this->Cell(25,$h,"Distancia en Metros",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,$presupuesto["metros"],0,0,'L');
			
			$this->Cell(25,$h,"Tipo de Terreno",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(40,$h,strtoupper($presupuesto["tipoterreno"]),0,1,'L');
			
			if($presupuesto["tipopresupuesto"]==1)
			{
				$diametros = $presupuesto["diamagua"];
			}else{
				$diametros = $presupuesto["diamdesague"];
			}
			
			$this->Cell(25,$h,"Diametro",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(40,$h,utf8_decode($diametros),0,1,'L');
			
			$this->SetWidths(array(10,75,10,10,15,15,15,15,10,15));
			$this->SetAligns(array("C","C","C","C","C","C","C","C","C","C"));
			$this->Row(array("Item","Descripcion","Und.","Cant","P. Unit.","Importe","Imp. Emp.","Imp. Usua.","Af. Igv.","Mont. Igv."));
			
			$cadena 	= "";
			$cadena2 	= "";
			$subtotal 	= 0;
			$impigv		= 0;
			
			$sqlD = "select c.codtipocostos,t.descripcion as tipocostos,d.item,c.descripcion as material,u.abreviado,d.cantidad,
					d.preciounitario,d.subtotal,d.importempresa,d.afectaigv,d.impigv, c.codgrupocostos, gc.descripcion as dgrupocostos
					from solicitudes.detpresupuesto d
					inner join solicitudes.confecpreagudes as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.nroconfeccion=c.nroconfeccion)
					inner join public.tipocostos as t on(c.codtipocostos=t.codtipocostos)
					inner join public.grupocostos as gc on (gc.codgrupocostos=c.codgrupocostos)
					inner join public.unidades as u on(d.unimed=u.unimed)
					where d.codemp=1 and d.codsuc=? and d.nropresupuesto=? order by d.item";
			$consultaD = $conexion->prepare($sqlD);
			$consultaD->execute(array($codsuc,$id));
			$items = $consultaD->fetchAll();

			foreach($items as $rowD)
			{
				if($cadena!=$rowD["codtipocostos"])
				{
					$this->SetWidths(array(190));
					$this->SetAligns(array("L"));
					$this->Row(array(strtoupper($rowD["tipocostos"])),array("0"));
				}

				if($cadena2!=$rowD["codgrupocostos"])
                {
                    $this->SetWidths(array(190));
					$this->SetAligns(array("L"));
					$this->Row(array(strtoupper($rowD["dgrupocostos"])),array("0"));
                }
				
				$this->SetWidths(array(10,75,10,10,15,15,15,15,10,15));
				$this->SetAligns(array("C","L","C","C","R","R","R","R","C","R"));
				$this->Row(array($rowD["item"],strtoupper($rowD["material"]),strtoupper($rowD["abreviado"]),number_format($rowD["cantidad"],2),
						   number_format($rowD["preciounitario"],2),number_format($rowD["subtotal"],2),number_format($rowD["importempresa"],2),
						   number_format($rowD["importeusuario"],2),$rowD["afectaigv"]==1?"S":"N",number_format($rowD["impigv"],2)));
				
				$impigv	   += $rowD["impigv"];
				$subtotal  += $rowD["subtotal"];
				$cadena		= $rowD["codtipocostos"];
				$cadena2 	= $rowD["codgrupocostos"];
				
			}
			
			$total 			= $subtotal + $igvC;
			//$redondeoAnt 	= round($total,1);
			//$redondeoAct	= round($total,2);
			$redondeo		= $presupuesto['redondeo'];
			
			

			$this->Ln(8);
			$h=5;
			$this->Cell(170,$h,"SUB. TOTAL COSTOS DE LA OPERACION",'T',0,'R');
			$this->Cell(3,$h,":",'T',0,'R');
			$this->Cell(16,$h,number_format($subtotal,2),'T',1,'R');

			$this->Cell(170,$h,"GASTOS DE ADMINISTRACION",'0',0,'R');
			$this->Cell(3,$h,":",'0',0,'R');
			$this->Cell(16,$h,number_format($impgasadm,2),'0',1,'R');

			$this->Cell(170,$h,"IGV",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(16,$h,number_format($igvC,2),0,1,'R');
			$this->Cell(170,$h,"REDONDEO",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(16,$h,number_format($redondeo,2),0,1,'R');
			$this->Cell(170,$h,"TOTAL A CANCELAR",'B',0,'R');
			$this->Cell(3,$h,":",'B',0,'R');
			$this->Cell(16,$h,number_format($total + $redondeo + $impgasadm,2),'B',1,'R');
			
			$this->Ln(5);
			$h=3;
			$this->Cell(20,$h,"NOTA",0,0,'R');
			$this->Cell(3,$h,":",0,1,'R');
			
			$tit1  = "Manipuleo y da�os que ocasione el cliente al medidor dar�n lugar al cierre ";
			$tit1 .= "del servicio y cobro por la reposici�n.";
			
			$this->Cell(20,$h,"-",0,0,'R');
			$this->Cell(150,$h,$tit1,0,1,'L');
			
			$tit2  = "El aceptar el presente Contrato implica el compromiso del Usuario hacer ";
			$tit2 .= "uso de la red instalada no aceptandose ";
						
			$this->Cell(20,$h,"-",0,0,'R');
			$this->Cell(150,$h,$tit2,0,1,'L');
			
			$tit3  = "CierreTemporal de este servicio en un periodo menor a los 12 meses.";
									
			$this->Cell(20,$h,"",0,0,'R');
			$this->Cell(150,$h,$tit3,0,1,'L');
			
			$this->Ln(10);
			
			$this->Cell(75,$h,"APROBACION DEL PRESUPUESTO",0,0,'L');
			$this->Cell(0,$h,"CONFORMIDAD DE LOS TRABAJOS EJECUTADOS",0,1,'R');
			
			$this->Ln(12);
			$this->SetX(85);
			$this->Cell(40,.1,"",1,1,'C',true);
			$this->Cell(0,$h+1,utf8_decode(strtoupper($paramae["despar"])),0,1,'C');
			$this->Cell(0,$h+1,$paramae["valor"],0,1,'C');

			$this->Line(10,$this->GetY()+10,60,$this->GetY()+10);
			$this->Line(140,$this->GetY()+10,190,$this->GetY()+10);
			
			$this->SetXY(10,$this->GetY()+10);
			
			$this->Cell(50,$h+1,utf8_decode(strtoupper($presupuesto["propietario"])),0,0,'C');
			$this->SetX(140);
			$this->Cell(50,$h+1,utf8_decode(strtoupper($presupuesto["propietario"])),0,1,'C');
			
			$this->SetX(10);
			$this->Cell(50,$h+1,$presupuesto["abreviado"]." - ".$presupuesto["nrodocumento"],0,0,'C');
			$this->SetX(140);
			$this->Cell(50,$h+1,$presupuesto["abreviado"]." - ".$presupuesto["nrodocumento"],0,1,'C');
			

			$this->Ln(12);
			$this->SetX(85);
			$this->Cell(40,.1,"",1,1,'C',true);
			$this->Cell(0,$h+1,utf8_decode(strtoupper('GERENCIAL COMERCIAL')),0,1,'C');


			$this->Ln(10);
			$this->Cell(20,$h,"NOTA",0,0,'R');
			$this->Cell(3,$h,":",0,1,'R');
			
			$tit1  = "EL PRESENTE DOCUMENTO TIENE VALOR POR 30 DIAS DESPUES DE LA FECHA DE PRESUPUESTO.";
			
			$this->Cell(20,$h,"-",0,0,'R');
			$this->Cell(150,$h,$tit1,0,1,'L');
			
			$tit2 = "LA ROTURA DE TUBERIA MATRIZ Y LOS GASTOS OCASIONADOS SERAN ASUMIDOS POR EL CLIENTE.";
						
			$this->Cell(20,$h,"-",0,0,'R');
			$this->Cell(150,$h,$tit2,0,1,'L');
			
		}
	}

	

	
	//print_r($impgasadm);
	$impgasadm=$impgasadm['valor'];
	$codsuc 		= $_GET["codsuc"];
	$nropresupuesto	= $_GET["Id"];
	
	$consulta = $conexion->prepare("SELECT * FROM reglasnegocio.parame WHERE tippar=? AND codsuc=?");
	$consulta->execute(array("IMPPTO", $codsuc));
	
	$item = $consulta->fetch();
	$impgasadm = $item['valor'];
	
	$objReporte = new clsPresupuesto();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($nropresupuesto,$codsuc,$impgasadm);
	$objReporte->Output();
	
?>