<?php
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
    $TituloVentana = "AGREGAR MATERIALES";
    $Criterio = 'materiales';
    $Activo = 1;

  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  $Criterio='Materiales';
  $FormatoGrilla = array ();
  $Sql = "SELECT c.nroconfeccion,
    tp.descripcion,
    tc.descripcion,
    gc.descripcion,
    c.descripcion,    
    ".$objFunciones->Convert("c.cantidad","NUMERIC (18,2)").",
    ".$objFunciones->Convert("c.preciounitario","NUMERIC (18,2)").",
    ".$objFunciones->Convert("c.total","NUMERIC (18,2)").",    
    c.estareg, 
    c.codtipocostos, 
    c.codgrupocostos,1
    FROM solicitudes.confecpreagudes as c
    INNER JOIN public.unidades as u on(c.unimed=u.unimed)
    INNER JOIN public.tipopresupuesto AS tp ON tp.codtipopresupuesto = c.codtipopresupuesto
    inner join public.tipocostos as tc on (tc.codtipocostos=c.codtipocostos)
    INNER JOIN public.grupocostos as gc on (gc.codgrupocostos=c.codgrupocostos)  ";
  $FormatoGrilla[0] =eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                           //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'c.nroconfeccion', '2'=>'gc.descripcion', '3'=>'c.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo','T2'=>'Tipo Presupuesto','T3'=>'Tipo Costo','T4'=>'Grupo Costos','T5'=>'Material','T6'=>'Cantidad',
                'T7'=>'Prec. Unitario', 'T8'=>'Total');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'left', 'A5'=>'left', 'A6'=>'left', 'A7'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'55');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1000;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  AND (c.codemp=1 and c.codsuc=".$codsuc.") ORDER BY c.nroconfeccion DESC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '1',                       //Número de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'ok.png', //Imagen a mostrar
        'Btn1' => 'Seleccionar', //Titulo del Botón
        'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
        'BtnCI1' => '12', //Item a Comparar
        'BtnCV1' => '1'    //Valor de comparación
    );
              
	$FormatoGrilla[10] = array(array('Name' => 'nroconfeccion', 'Col' => 1), 
			array('Name' => 'codtipocostos', 'Col' => 10),
			array('Name' => 'codgrupocostos', 'Col' => 11) ); //DATOS ADICIONALES           
	$FormatoGrilla[11] = 7; //FILAS VISIBLES            
	$_SESSION['Formato'.$Criterio] = $FormatoGrilla;    
	Cabecera('', $FormatoGrilla[7], 800, 500);
	Pie();
?>

<script>
	var urldir 	= "<?php echo $_SESSION['urldir'];?>";
	
    function BuscarB(Op)
    {
        var Valor = document.getElementById('Valor').value
        //var Fecha = document.getElementById('Fecha').value
        var Op2 = ''
        if (Op != 0)
        {
            Op2 = '&Op='+Op;
        }
        location.href = 'index.php?Valor='+Valor+'&pagina='+Pagina+Op2;
    }
    
    $(document).ready(function () {

        $("#BtnNuevoB").remove();
    });

    function Enviar(obj)
    {
    	var Id = $(obj).parent().parent().data('nroconfeccion')
        var Tipo = $(obj).parent().parent().data('codtipocostos')
        var Grupo = $(obj).parent().parent().data('codgrupocostos')
        //alert(Id,Tipo,Grupo);
        opener.RecibirConcepto(Id,Tipo,Grupo);
    	window.close();
    }

</script>

