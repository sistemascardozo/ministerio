<?php

    session_name("pnsu");
    if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");

$Op = $_POST["Op"];
$Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
$IdSolicitud = isset($_POST["IdSolicitud"]) ? $_POST["IdSolicitud"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=".$Op;
$readonly = "";
$disabled = "";

$objMantenimiento = new clsDrop();

$impigvparamae = $objMantenimiento->getParamae("IMPIGV", $codsuc);
$impgasadm = $objMantenimiento->getParamae("IMPPTO", $codsuc);
$impgasadmV = $impgasadm['valor']?$impgasadm['valor'] : 0;

$impgasadm = $impgasadm['valor']?$impgasadm['valor'] : 0;

$fecha = date("d/m/Y");

if ($Id != '' && $Id != '0') {
    $consulta = $conexion->prepare("select * from solicitudes.cabpresupuesto where codemp=1 and codsuc=? and nropresupuesto=?");
    $consulta->execute(array($codsuc, $Id));
    $cabpresupuesto = $consulta->fetch();

    $guardar = $guardar."&Id2=".$Id;

    $fech = new DateTime($cabpresupuesto["fechaemision"]);
    $fecha = $fech->format("d/m/Y");
    $impgasadmV = $cabpresupuesto["porcentajeimpgasadm"];
    $impgasadm = $cabpresupuesto["impgasadm"];
    $readonly = "readonly='readonly'";
    $disabled = "disabled='disabled'";
    $redondeo = $cabpresupuesto['redondeo'];
}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>

<script type="text/javascript">
    var urldir = '<?php echo $_SESSION['urldir'];?>';
    var IdAnt = ''

    $(document).ready(function () {

        if ('<?=$IdSolicitud ?>' != '')
        {
            $("#nrosolicitud").val(<?=$IdSolicitud ?>)
            cargar_solicitud(<?=$IdSolicitud ?>)
        }
        ConSoli()

        $(document).on('click', '.eliminarreg', function (event) {
            // QuitaItem($(this).attr('data'));
            QuitaItem($(this).parent().parent().attr('data'));

        });

    });
    $(function () {
        $("#fechaemision").datepicker(
                {
                    showOn: 'button',
                    direction: 'up',
                    buttonImage: '../../../../images/iconos/calendar.png',
                    buttonImageOnly: true,
                    showOn: 'both',
                            showButtonPanel: true,
                    beforeShow: function (input, inst)
                    {
                        inst.dpDiv.css({marginTop: (input.offsetHeight) - 20+'px', marginLeft: (input.offsetWidth) - 90+'px'});
                    }
                }
        );
    });

    function ValidarForm(Op)
    {
        var v = validar_formB()
        if (v == false)
        {
            return false;
        }
        var id = parseInt($("#tbpresupuesto tbody tr").length);//
        $("#cont").val(id)
        if ($("#cont").val() == 0)
        {
            alert('No hay Ningun valor asignado para el Presupuesto')
            $("#cont").focus()
            return false;
        }
        $(".ui-dialog-buttonpane button:contains('Agregar')").button("disable");
        GuardarPresupuesto(Op)
    }
    function GuardarPresupuesto(Op)
    {   //alert(Op)
        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>/sigco/solicitudes/operaciones/presupuesto/guardar.php?Op='+Op,
            type: 'POST',
            async: true,
            dataType: "json",
            data: $('#form1').serialize(), //+'&0form1_idusuario=<?=$IdUsuario ?>&3form1_fechareg=<?=$Fecha ?>',
            success: function (datos) {
              
                $(".ui-dialog-buttonpane button:contains('Agregar')").button("enable");
                OperMensaje(datos.res)
                $("#Mensajes").html(datos.res);
                $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                $("#Nuevo").dialog("close");
                $("#Modificar").dialog("close");
                $("#ConfirmaEliminacion").dialog("close");
                $("#ConfirmaRestauracion").dialog("close");
                if (datos.res == 1)
                {
                    if (confirm("Desea Imprimir el Presupuesto de Conexion?"))
					{
                        AbrirPopupImpresion(urldir + "sigco/solicitudes/operaciones/presupuesto/imprimir_tipop.php?Id="+datos.nropresupuesto+"&codsuc=<?=$codsuc ?>", 800, 600);
					}
                }
                Buscar(Op);
            }
        })
    }
    function Imprimir_Presupuesto()
    {
        AbrirPopupImpresion(urldir + "sigco/solicitudes/operaciones/presupuesto/imprimir_ficha.php?Id=<?=$Id ?>&codsuc=<?=$codsuc ?>", 800, 600)
    }
    function BuscarConceptos()
    {
        var tipoconexion = $("#tipopresupuesto").val()
        // var ventana = window.open('conceptos.php?tipoconexion='+tipoconexion, 'BuscarConceptos', 'width=730, height=380, resizable=no, scrollbars=no');
        // ventana.focus();
        // AbrirPopupBusqueda('conceptos.php?Op=5', 1200, 500)
        AbrirPopupBusqueda('bConceptos.php?tipoconexion=' + tipoconexion, 1150, 450)
    }
    function validar_formB()
    {
        if ($("#codtipopresupuesto").val() == '0')
        {
            Msj($("#codtipopresupuesto"), 'Seleccione el Tipo de Presupuesto');
            return false;
        }
        $("#codtipopresupuesto").removeClass("ui-state-error");

        if ($("#nrosolicitud").val() == '' && $("#consolicitud").val() == 1)
        {
            Msj($("#nrosolicitud"), 'El Nro de la Solicitud no puede ser NULO');
            return false;
        }
        $("#nrosolicitud").removeClass("ui-state-error");

        if ($("#propietario").val() == '')
        {
            Msj($("#propietario"), 'El Nombre del Propietario no puede ser NULo')
            return false;
        }
        $("#propietario").removeClass("ui-state-error");

        if ($("#sector").val() == 0)
        {
            Msj($("#sector"), 'Seleccione el Sector')
            return false;
        }
        $("#sector").removeClass("ui-state-error");

        if ($("#calles").val() == 0)
        {
            Msj($("#calles"), 'Seleccione la Calle')
            return false;
        }
        $("#calles").removeClass("ui-state-error");

        if ($("#nrocalle").val() == '')
        {
            Msj($("#nrocalle"), 'Digite el Numero de Calle')
            return false;
        }
        $("#nrocalle").removeClass("ui-state-error");

        if ($("#codtipodocumento").val() == 0)
        {
            Msj($("#codtipodocumento"), 'Seleccione el Tipo de Documento')
            return false;
        }
        $("#codtipodocumento").removeClass("ui-state-error");

        if ($("#nrodocumento").val() == '')
        {
            Msj($("#nrodocumento"), 'Digite el Numero de Documento')
            return false;
        }
        $("#nrodocumento").removeClass("ui-state-error");

        if ($("#area").val() == '')
        {
            Msj($("#area"), 'El Area del Inmueble no puede ser NULO')
            return false
        }
        $("#area").removeClass("ui-state-error");

        if ($("#metros").val() == '')
        {
            Msj($("#metros"), 'La Distancia en Metros no puede ser NULO')
            return false
        }
        $("#metros").removeClass("ui-state-error");

        if ($("#tipoterreno").val() == 0)
        {
            Msj($("#tipoterreno"), 'Seleccione el Tipo de Terreno')
            return false;
        }
        $("#tipoterreno").removeClass("ui-state-error")

        if ($("#div_diametros select").val() == 0)
        {
            Msj($("#div_diametros select"), 'Seleccione el Diametro del Tubo')
            return false;
        }
        $("#div_diametros").removeClass("ui-state-error")
    }
    function Cancelar()
    {
        location.href = 'index.php';
    }

    var object = ''
    function BuscarSolicitudes()
    {
        object == "registro";

        AbrirPopupBusqueda('../registro/?Op=5', 1150, 450)
    }
    function Recibir(Id)
    {
        if (object == "codcliente")
        {
            datos_usuarios(Id)
        }
        else
        {
            $("#nrosolicitud").val(Id)
            cargar_solicitud(Id)
        }
    }
    function datos_usuarios(id)
    {
        $.ajax({
            url: '../../../../ajax/clientes.php',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: 'nroinscripcion='+id+'&codsuc=<?=$codsuc ?>',
            success: function (datos) {
                $("#nroinscripcion").val(id)
                $("#codcliente").val(datos.codcliente)
                $("#propietario").val(datos.propietario)
                cargar_diametros($("#tipopresupuesto").val(), 0)
                $("#nrocalle").val(datos.nrocalle)
                $("#tipodocumento").val(datos.codtipodocumento)
                $("#nrodocumento").val(datos.nrodocumento)
                $("#area").val(datos.area)
                $("#codzona").val(datos.codzona)
                $("#sector").val(datos.codsector)
                // $("#nroinscripcion").val(datos.nroinscripcion)
                $("#nroinspeccion").val(datos.nroinspeccion)
                $("#correo").val(datos.correo)
                cargar_sectores(<?=$codsuc ?>, datos.codzona, datos.codsector, 1)
                cargar_calles(datos.codsector,<?=$codsuc ?>, datos.codzona, datos.codcalle)


            }
        })
    }
    function cargar_solicitud(nrosolicitud)
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>ajax/mostrar/solicitudes.php',
            type: 'POST',
            async: true,
            data: 'nrosolicitud='+nrosolicitud+'&codsuc=<?=$codsuc ?>',
            dataType: "json",
            success: function (datos)
            {

                $("#codcliente").val(datos.codcliente)
                $("#propietario").val(datos.propietario)

                //if (datos.codtiposervicio == 2)
//                    ts = 2;
//                else
//                    ts = 3;
				//alert(datos.codtiposervicio);

				$("#tipopresupuesto").val(datos.codtiposervicio);
                cargar_diametros(datos.codtiposervicio, 0);

                $("#nrocalle").val(datos.nrocalle)
                $("#tipodocumento").val(datos.codtipodocumento)
                $("#nrodocumento").val(datos.nrodocumento)
                $("#area").val(datos.area)
                $("#codzona").val(datos.codzona)
                $("#sector").val(datos.codsector)
                $("#nroinscripcion").val(datos.nroinscripcion)
                $("#nroinspeccion").val(datos.nroinspeccion)
                $("#correo").val(datos.correo)

                cargar_sectores(<?=$codsuc ?>, datos.codzona, datos.codsector, 1)
                cargar_calles(datos.codsector,<?=$codsuc ?>, datos.codzona, datos.codcalle)

            }
        })
    }

    var TipoP = 0
    function presupuestar()
    {
        TipoP = 1
        var v = validar_formB()
        if (v == false) {
            return
        }

        var metros = $("#metros").val()
        var tipoconexion = $("#tipopresupuesto").val()
        var tipopresupuesto = $("#codtipopresupuesto").val()

        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>/sigco/solicitudes/operaciones/presupuesto/presupuestar.php',
            type: 'POST',
            async: true,
            data: 'metros='+metros+'&codtipopresupuesto='+tipopresupuesto+'&tipoconexion='+tipoconexion+'&codsuc=<?=$codsuc ?>&utilidad='+$("#porcentajeimpgasadm").val(),
            success: function (datos) {
                $("#div_detalle").html(datos)
            }
        })

    }

    function calcular_importe(obj1, obj2, idx)
    {
        var objeto2 = str_replace($("#"+obj2).val(), ",", "")
        var objeto1 = str_replace(obj1.value, ",", "")

        var imp = parseFloat(objeto1) * parseFloat(objeto2)

        $("#total"+idx).val(parseFloat(imp).toFixed(2))
        var afecta = $("#afectaigvtext"+idx).val();

        if (afecta == 1)
        {

            var igv = CalculaIgv(parseFloat(imp).toFixed(2),<?=$impigvparamae["valor"] ?>);

            $("#impigv"+idx).val(parseFloat(igv).toFixed(2))
        }

        sumar_totales();
    }
    function calcular_igv_x(obj, objTotal, objIgv)
    {
        if (obj.checked)
        {
            var igv = CalculaIgv(objTotal,<?=$impigvparamae["valor"] ?>);
            $("#"+objIgv).val(parseFloat(igv).toFixed(2));
        } else {
            $("#"+objIgv).val("0.00");
        }
        sumar_totales();
    }
    function sumar_totales()
    {
        var subtotal = 0;
        var subigv = 0;
        var redondeoAnt = 0;
        var redondeoAct = 0;
        var redondeo = 0;
        var total = 0;
        var impgasadm = <?=$impgasadmV ?>;

        var replace_total = "";
        var replace_igv = "";

        var id = parseInt($("#tbpresupuesto tbody tr").length);//$("#cont").val()
        for (var i = 1; i <= id; i++)
        {
            //alert($("#tbpresupuesto tbody tr#"+i).attr('id'))
            if ($("#tbpresupuesto tbody tr#"+i).attr('id') != undefined)
            {
                replace_total = str_replace($("#total"+i).val(), ",", "")
                replace_igv = str_replace($("#impigv"+i).val(), ",", "")

                subtotal += parseFloat(replace_total)
                subigv += parseFloat(replace_igv)
            }
        }

        $("#subtotal").val(parseFloat(subtotal).toFixed(2))
        subtotal=parseFloat(subtotal).toFixed(2)
        impgasadm = parseFloat(impgasadm) / 100 * parseFloat(subtotal)
        impgasadm=parseFloat(impgasadm).toFixed(2)
        //alert(impgasadm);
        subigv = parseFloat(subigv)+parseFloat(CalculaIgv(impgasadm,<?=$impigvparamae["valor"] ?>))
        redondeoAnt = Round(parseFloat(subtotal)+parseFloat(subigv)+parseFloat(impgasadm), 1);
        redondeoAct = Round(parseFloat(subtotal)+parseFloat(subigv)+parseFloat(impgasadm), 2);
        redondeo = parseFloat(redondeoAnt) - parseFloat(redondeoAct)

        total = parseFloat(subtotal)+parseFloat(subigv)+parseFloat(redondeo)+parseFloat(impgasadm)

        $("#impgasadm").val(parseFloat(impgasadm).toFixed(2))
        $("#subigv").val(parseFloat(subigv).toFixed(2))
        $("#subredondeo").val(parseFloat(redondeo).toFixed(2))
        $("#totpresupuesto").val(parseFloat(total).toFixed(2))
    }

    function QuitaItem(tr)
    {
        var id = parseInt($("#tbpresupuesto tbody tr").length);
        clase = $("#tbpresupuesto tbody tr#"+tr).attr('class');
        $("#tbpresupuesto tbody tr#"+tr).remove();
        var nextfila = parseInt(tr)+1;
        var j;
        for (var i = nextfila; i <= id; i++)
        {

            // console.log($("#tbpresupuesto tbody tr#"+i+" label.Item").text());
            if ($("#tbpresupuesto tbody tr#"+i).attr('id') != undefined)
            {
                j = i - 1;
                $("#tbpresupuesto tbody tr#"+i+" label.Item").text(j);
                $("#tbpresupuesto tbody tr#"+i+" a.Del").attr("href", "javascript:QuitaItem("+j+")");
                $("#item"+i).attr("name", "item"+j);
                $("#item"+i).attr("id", "item"+j);

                $("#nroconfeccion"+i).attr("name", "nroconfeccion"+j);
                $("#nroconfeccion"+i).attr("id", "nroconfeccion"+j);

                $("#descripcion"+i).attr("name", "descripcion"+j);
                $("#descripcion"+i).attr("id", "descripcion"+j);

                $("#unimed"+i).attr("name", "unimed"+j);
                $("#unimed"+i).attr("id", "unimed"+j);

                $("#cantidad"+i).attr("name", "cantidad"+j);
                $("#cantidad"+i).attr("onkeyup", "calcular_importe(this,'punitario"+j+"',"+j+")");
                $("#cantidad"+i).attr("id", "cantidad"+j);


                $("#punitario"+i).attr("name", "punitario"+j);
                $("#punitario"+i).attr("onkeyup", "calcular_importe(this,'cantidad"+j+"',"+j+")");
                $("#punitario"+i).attr("id", "punitario"+j);

                $("#total"+i).attr("name", "total"+j);
                $("#total"+i).attr("id", "total"+j);

                $("#impempresa"+i).attr("name", "impempresa"+j);
                $("#impempresa"+i).attr("id", "impempresa"+j);

                $("#impusuario"+i).attr("name", "impusuario"+j);
                $("#impusuario"+i).attr("id", "impusuario"+j);


                $("#afectaigvtext"+i).attr("name", "afectaigvtext"+j);
                $("#afectaigvtext"+i).attr("id", "afectaigvtext"+j);
                //
                $("#afectaigv"+i).attr("name", "afectaigv"+j);
                //$("#afectaigv"+i).attr("onclick","");
                $("#afectaigv"+i).removeAttr("onclick", "");
                $("#afectaigv"+i).attr("id", "afectaigv"+j);
                $("#afectaigv"+j).click(function () {
                    var x = parseInt(this.id.substr(9, (this.id).length))
                    calcular_igv_x(this, $('#total'+x).val(), 'impigv'+x);
                    CambiarEstado(this, 'afectaigvtext'+x);

                });

                //
                $("#impigv"+i).attr("name", "impigv"+j);
                $("#impigv"+i).attr("id", "impigv"+j);

                $("#afectaempresatext"+i).attr("name", "afectaempresatext"+j);
                $("#afectaempresatext"+i).attr("id", "afectaempresatext"+j);


                $("#afectaempresa"+i).attr("name", "afectaempresa"+j);
                // $("#afectaempresa"+i).attr("onclick","CambiarEstado(this,'afectaempresatext"+j+"');");
                $("#afectaempresa"+i).attr("id", "afectaempresa"+j);

                $("#afectaempresa"+j).click(function () {
                    var x = parseInt(this.id.substr(13, (this.id).length))
                    CambiarEstado(this, 'afectaempresatext'+x);

                });

                // $("#tbpresupuesto tbody tr#"+i).attr("id", j);
                $("#tbpresupuesto tbody tr#"+i).attr({'id': j, 'data': j})

            }

        }
        sumar_totales()
    }

    function RecibirConcepto(Id, Tipo, Grupo)
    {
        //alert(Id, Tipo, Grupo);
        var id = parseInt($("#tbpresupuesto tbody tr").length);
        var clase = "";
        var counttipo = 0;
        var countgrupo = 0;
        var i = 1;
        var msj = 0;

        $('#tbpresupuesto tbody tr').each(function (indice, elemento) {

            if ($(elemento).attr('id') != undefined)
            {
                var valor = $(elemento).children('td:eq(0)').children("input:eq(1)").val();
                if (valor == Id)
                {
                    Msj("#tbpresupuesto", 'El Concepto ya esta agregado', 1000, 'above', '', false);
                    msj = 1;
                }
            }
        });

        if (msj == 1) return false

        counttipo = parseInt($("#tbpresupuesto tbody tr.TipoGasto"+Tipo).length);
        var posicion = 0
        var nrotipos = counttipo
        if (counttipo > 0) {
            countgrupo = parseInt($("#tbpresupuesto tbody tr.GrupoGasto"+Grupo).length)
            if (countgrupo > 0)
                posicion = parseInt($("#tbpresupuesto tbody tr.TipoGastoGrupo"+Tipo+Grupo+":last").attr('id'))
            else
                nrogrupotipos = 0
        }
        else
            nrotipos = 0

        if (nrotipos > 0)
            nrotipos = posicion+1;

        var v = validar_formB()
        if (v == false) {
            return
        }
        var metros = $("#metros").val()
        var tipoconexion = $("#tipopresupuesto").val()
        var tppresupuesto = $("#codtipopresupuesto").val()

        $.ajax({
            url: 'dConcepto.php',
            type: 'POST',
            async: true,
            data: 'metros='+metros+'&tipopresupuesto='+tppresupuesto+'&tipoconexion='+tipoconexion+'&codsuc=<?=$codsuc ?>&Id='+Id+'&count='+counttipo+'&countprupo='+countgrupo+'&nrotipos='+nrotipos+'&TipoP='+TipoP,
            success: function (datos)
            {

                if (posicion > 0) {
                    ActItems(nrotipos);
                    $("#tbpresupuesto tbody tr#"+posicion).after(datos);
                } else {
                    $("#tbpresupuesto tbody").append(datos);
                }
            }

        })
    }

    function ActItems(tr) {

        var valor = 1;
        var nextfila = parseInt(tr)+1;
        var j = tr;
        var id = parseInt($("#tbpresupuesto tbody tr").length);

        $('#tbpresupuesto tbody tr').each(function (indice, elemento) {

            if ($(elemento).attr('id') != undefined) {

                if ($(elemento).attr('id') >= tr) {

                    j++;

                    $(elemento).children('td:eq(0)').children("label").text(j);

                    // $(elemento).children('td:last-child').children("a").attr("href", "javascript:QuitaItem("+j+")");

                    $(elemento).children('td:eq(0)').children("input:eq(0)").attr("name", "item"+j);
                    $(elemento).children('td:eq(0)').children("input:eq(0)").attr("id", "item"+j);
                    $(elemento).children('td:eq(0)').children("input:eq(0)").attr("value", j);
                    $(elemento).children('td:eq(0)').children("input:eq(1)").attr("name", "nroconfeccion"+j);
                    $(elemento).children('td:eq(0)').children("input:eq(1)").attr("id", "nroconfeccion"+j);

                    $(elemento).children('td:eq(1)').children("input:eq(0)").attr("name", "descripcion"+j);
                    $(elemento).children('td:eq(1)').children("input:eq(0)").attr("id", "descripcion"+j);

                    $(elemento).children('td:eq(2)').children("input:eq(0)").attr("id", "unimed"+j);
                    $(elemento).children('td:eq(2)').children("input:eq(0)").attr("name", "unimed"+j);

                    $(elemento).children('td:eq(3)').children("input:eq(0)").attr("id", "cantidad"+j);
                    $(elemento).children('td:eq(3)').children("input:eq(0)").attr("name", "cantidad"+j);
                    $(elemento).children('td:eq(3)').children("input:eq(0)").attr("onkeyup", "calcular_importe(this,'punitario"+j+"',"+j+")");

                    $(elemento).children('td:eq(4)').children("input:eq(0)").attr("id", "punitario"+j);
                    $(elemento).children('td:eq(4)').children("input:eq(0)").attr("name", "punitario"+j);
                    $(elemento).children('td:eq(4)').children("input:eq(0)").attr("onkeyup", "calcular_importe(this,'cantidad"+j+"',"+j+")");

                    $(elemento).children('td:eq(5)').children("input:eq(0)").attr("id", "total"+j);
                    $(elemento).children('td:eq(5)').children("input:eq(0)").attr("name", "total"+j);

                    $(elemento).children('td:eq(6)').children("input:eq(0)").attr("id", "impempresa"+j);
                    $(elemento).children('td:eq(6)').children("input:eq(0)").attr("name", "impempresa"+j);

                    $(elemento).children('td:eq(7)').children("input:eq(0)").attr("id", "impusuario"+j);
                    $(elemento).children('td:eq(7)').children("input:eq(0)").attr("name", "impusuario"+j);

                    $(elemento).children('td:eq(8)').children("input:eq(0)").attr("id", "afectaigvtext"+j);
                    $(elemento).children('td:eq(8)').children("input:eq(0)").attr("name", "afectaigvtext"+j);

                    $(elemento).children('td:eq(8)').children("input:eq(1)").attr("id", "afectaigv"+j);
                    $(elemento).children('td:eq(8)').children("input:eq(1)").attr("name", "afectaigv"+j);
                    $(elemento).children('td:eq(8)').children("input:eq(1)").attr("onclick", "");
                    $(elemento).children('td:eq(8)').children("input:eq(1)").removeAttr("onclick", "");
                    $(elemento).children('td:eq(8)').children("input:eq(1)").attr("onclick", "");
                    $(elemento).children('td:eq(8)').children("input:eq(1)").click(function () {
                        var x = parseInt(this.id.substr(9, (this.id).length))
                        calcular_igv_x(this, $('#total'+x).val(), 'impigv'+x);
                        CambiarEstado(this, 'afectaigvtext'+x);
                    });

                    $(elemento).children('td:eq(9)').children("input:eq(0)").attr("id", "impigv"+j);
                    $(elemento).children('td:eq(9)').children("input:eq(0)").attr("name", "impigv"+j);

                    $(elemento).children('td:eq(10)').children("input:eq(0)").attr("id", "afectaempresatext"+j);
                    $(elemento).children('td:eq(10)').children("input:eq(0)").attr("name", "afectaempresatext"+j);
                    $(elemento).children('td:eq(10)').children("input:eq(0)").attr("onclick", "CambiarEstado(this,'afectaempresatext"+j+"');");

                    $(elemento).children('td:eq(10)').children("input:eq(0)").click(function () {
                        var x = parseInt(this.id.substr(13, (this.id).length))
                        CambiarEstado(this, 'afectaempresatext'+x);
                    });

                    // $("#tbpresupuesto tbody tr#"+i).attr("id", j);
                    $(elemento).attr("id", j);
                    $(elemento).attr("data", j);
                }
            }
        })
    }

    function ConSoli()
    {
        if ($("#consol").attr('checked'))
        {
            $(".buscarcliente").hide()
            $(".buscarsolicitud").show()
        }
        else
        {
            $(".buscarcliente").show()
            $(".buscarsolicitud").hide()

        }

    }
    function buscar_usuario()
    {
        object = "codcliente";

        AbrirPopupBusqueda('../../../catastro/operaciones/actualizacion/?Op=5', 1150, 450);
    }
    function UpdTilidad()
    {
        //  alert($("#codtipopresupuesto option:selected").data('utilidad'))
        $("#porcentajeimpgasadm").val($("#codtipopresupuesto option:selected").data('utilidad'));

		//cargar_diametros($("#tipopresupuesto").val(), 0);
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?=$guardar ?>" enctype="multipart/form-data">
        <table width="950" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="7" class="TitDetalle">&nbsp;</td>
                </tr>
                <tr>
                    <td width="120" align="right">Nro. Presupuesto</td>
                    <td width="20" align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="nropresupuesto" type="text" id="Id" size="10" maxlength="10" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
                        <input type="hidden" name="solreferencia" id="solreferencia" />
                        <input type="hidden" name="nroinscripcion" id="nroinscripcion" value="<?=$cabpresupuesto['nroinscripcion'] ?>"/>
                        <input type="hidden" name="nroinspeccion" id="nroinspeccion" />
                        <input type="hidden"  id="correo" name="correo" value="<?=$cabpresupuesto["correo"] ?>" />
                    </td>
                    <td width="10" class="CampoDetalle">&nbsp;</td>
                    <td width="140" align="right">Tipo de Servicio</td>
                    <td width="20" align="center" class="CampoDetalle">:</td>
                    <td width="225" class="CampoDetalle">
                        <select name="tipopresupuesto" id="tipopresupuesto" style="width:100px" class="select" onChange='cargar_diametros(this.value, 0)'>
                            <option value="2" <?= $d = "";
                            $cabpresupuesto["tipopresupuesto"] == 2 ? $d = "selected='selected'" : $d = "";
                            echo $d
                            ?> >AGUA</option>
                            <option value="3" <?=
                            $d = "";
                            $cabpresupuesto["tipopresupuesto"] == 3 ? $d = "selected='selected'" : $d = "";
                            echo $d
                            ?> >DESAGUE</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Tipo de Presupuesto </td>
                    <td class="TitDetalle" align="center">:</td>
                    <td colspan="7" class="CampoDetalle">
                        <?php $objMantenimiento->drop_tipopresupuesto($cabpresupuesto["codtipopresupuesto"]); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Nro. Solicitud </td>
                    <td class="TitDetalle" align="center">:</td>
                    <td class="CampoDetalle"><input name="nrosolicitud" type="text" id="nrosolicitud" size="10" maxlength="10" value="<?=$cabpresupuesto["nrosolicitud"] ?>" readonly="readonly" class="inputtext" />

                        <span class="MljSoft-icon-buscar buscarsolicitud" title="Buscar la Solicitud de Conexion" onclick="BuscarSolicitudes();" style="display:none"></span>
                        <?php
                        $checked = "checked='checked'";
                        $disabled = "";
                        if (isset($cabpresupuesto["consolicitud"])) {
                            if ($cabpresupuesto["consolicitud"] == 0) {
                                $checked = "";
                            }
                            $disabled = 'disabled="disabled"';
                        }
                        ?>
                        <input type="checkbox" name="consol" id="consol" <?=$checked ?> onclick="CambiarEstado(this, 'consolicitud');
                                ConSoli();" <?=$disabled ?>/>
                        Con Solicitud
                        <input type="hidden" name="consolicitud" id="consolicitud" value="<?=isset($cabpresupuesto["consolicitud"]) ? $cabpresupuesto["consolicitud"] : 1 ?>" />
                        <input type="hidden" name="porcentajeigv" value="<?=$impigvparamae["valor"] ?>">
                        <input type="hidden" name="porcentajeimpgasadm" id="porcentajeimpgasadm" value="<?=$impgasadmV?>">
                    </td>
                    <td width="10" class="CampoDetalle">&nbsp;</td>
                    <td class="CampoDetalle" align="right">Fecha de Emsion </td>
                    <td class="CampoDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="fechaemision" type="text" id="fechaemision" maxlength="10" class="inputtext" value="<?=$fecha?>" style="width:80px;"/>
                    </td>

                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Cliente</td>
                    <td class="TitDetalle" align="center">:</td>
                    <td colspan="4" class="CampoDetalle">
                        <input type="text" name="codcliente" id="codcliente" size="10" class="inputtext" readonly="readonly" value="<?=$cabpresupuesto["codcliente"] ?>" />
                        <span class="MljSoft-icon-buscar buscarcliente" title="Buscar Clientes" onclick="buscar_usuario();" style="display:none"></span>
                        <input class="inputtext" name="propietario" type="text" id="propietario" maxlength="200" value="<?=utf8_encode($cabpresupuesto["propietario"])?>" style="width:330px;" />
                    </td>
                    <td colspan="1" class="CampoDetalle">
                        <?php if ($Op != 0) { ?>
                            <input type="button" class="button" name="ImprimirPresupuesto" id="ImprimirPresupuesto" value="Imprimir Presupuesto" onclick="Imprimir_Presupuesto();" />
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Zona</td>
                    <td class="TitDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <?php
                        echo $objMantenimiento->drop_zonas($codsuc, $cabpresupuesto["codzona"], "onchange='cargar_sectores(".$codsuc.",this.value,0,0)';");
                        ?>
                    </td>
                    <td width="10" class="CampoDetalle">&nbsp;</td>
                    <td class="CampoDetalle" align="right">Nro Contrato</td>
                    <td class="CampoDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="nrocontrato" type="text" id="nrocontrato" size="13" maxlength="10" class="inputtext" value="<?=$cabpresupuesto["nrocontrato"] ?>" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Sector</td>
                    <td class="TitDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <div id="div_sector">
                            <select name="sector" id="sector" style="width:220px" class="select">
                                <option value="0">.:: Seleccione el Sector ::.</option>
                            </select>
                        </div>
                    </td>
                    <td width="10" class="CampoDetalle">&nbsp;</td>
                    <td class="CampoDetalle" align="right">Area Inmueble </td>
                    <td align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="area" type="text" id="area" size="10" maxlength="10" class="inputtext" value="<?=$cabpresupuesto["area"]?>" />
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Calle</td>
                    <td class="TitDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <div id="div_calles">
                            <select id="calles" name="calles" style="width:220px" class="select">
                                <option value="0">.:: Seleccione la Calle ::.</option>
                            </select>
                        </div>
                    </td>
                    <td width="10" class="CampoDetalle">&nbsp;</td>
                    <td class="CampoDetalle" align="right">Nro. Calle</td>
                    <td width="6" class="CampoDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="nrocalle" type="text" id="nrocalle" size="13" maxlength="10" class="inputtext" value="<?=$cabpresupuesto["nrocalle"] ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Documento</td>
                    <td class="TitDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <?php $objMantenimiento->drop_tipodocumento($cabpresupuesto["codtipodocumento"]); ?>
                    </td>
                    <td width="10" class="CampoDetalle">&nbsp;</td>
                    <td class="CampoDetalle" align="right">Nro. Documento </td>
                    <td class="CampoDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="nrodocumento" type="text" id="nrodocumento" size="13" maxlength="10" class="inputtext" onkeypress="return permite(event, 'num')" value="<?=$cabpresupuesto["nrodocumento"] ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle" align="right">Tipo Terreno </td>
                    <td class="TitDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <?php $objMantenimiento->drop_tipo_terreno($cabpresupuesto["codtipoterreno"]); ?>
                    </td>
                    <td align="right" >&nbsp;</td>
                    <td class="TitDetalle" align="right" >Diametro</td>
                    <td align="center">:</td>
                    <td>
                        <div id="div_diametros">
                            <select id="diametros" name="diametros" style="width:180px;" class="select">
                                <option value="0">--Seleccione el Diametro--</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="CampoDetalle" align="right">Distancia en Metros </td>
                    <td class="CampoDetalle" align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="metros" type="text" id="metros" size="13" maxlength="10" class="inputtext" onkeypress="return permite(event, 'num')" value="<?=$cabpresupuesto["metros"] ?>" />
                    </td>
                    <td align="right" >&nbsp;</td>
                    <td align="right" >&nbsp;</td>
                    <td align="right" >&nbsp;</td>
                    <td class="CampoDetalle">
                        <input type="button" name="btnpresupuestar" id="btnpresupuestar" value="Presupuestar" class="button" onclick="presupuestar();"  <?=$disabled2 ?> />
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="TitDetalle">
                        <div id="div_detalle">
                            <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbpresupuesto" rules="all" >
                                <thead class="ui-widget-header" >
                                    <tr class="CabIndex">
                                        <td width="9%" align="center" >Item</td>
                                        <td width="23%" align="center" >Descripcion</td>
                                        <td width="5%" align="center" >Und.</td>
                                        <td width="8%" align="center" >Cantidad</td>
                                        <td width="9%" align="center" >P. Unit. </td>
                                        <td width="7%" align="center" >Importe</td>
                                        <td width="9%" align="center" >Imp. Emp. </td>
                                        <td width="12%" align="center" >Imp. Usua. </td>
                                        <td width="6%" align="center" >Af. Igv. </td>
                                        <td width="10%" align="center" >Monto Igv</td>
                                        <td width="4%" align="center" >Af. Emp</td>
                                        <th width="5%" align="center" scope="col">
                                            <span class="ui-icon-add" title="Agregar Concepto" onclick="BuscarConceptos(this)"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id=''>
                                    <?php
                                    $cadena = '';
                                    $cadena2 = '';
                                    $count = 0;
                                    $subtotal = 0;
                                    $subigv = 0;

                                    if ($Id != '') {
                                        $sqlD = "SELECT d.item,c.codtipocostos,t.descripcion as tipocosto,d.nroconfeccion,d.descripcion,d.unimed,u.abreviado,d.cantidad,
                                            d.preciounitario,d.subtotal,d.importempresa,d.importeusuario,d.afectaigv,d.impigv, c.codgrupocostos as idgrupocostos,
                                            gc.descripcion as dgrupocostos
                                            FROM solicitudes.detpresupuesto as d
                                            INNER JOIN solicitudes.confecpreagudes as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.nroconfeccion=c.nroconfeccion)
                                            INNER JOIN public.tipocostos as t on(c.codtipocostos=t.codtipocostos)
                                            INNER JOIN public.grupocostos as gc on (gc.codgrupocostos=c.codgrupocostos)
                                            INNER JOIN public.unidades as u on(d.unimed=u.unimed)
                                            WHERE d.codemp=1 and d.codsuc=? and d.nropresupuesto=? order by d.item";
                                        $consultaD = $conexion->prepare($sqlD);
                                        $consultaD->execute(array($codsuc, $Id));
                                        $itemsD = $consultaD->fetchAll();
                                        foreach ($itemsD as $rowD) {
                                            $count++;

                                            if ($cadena != $rowD["codtipocostos"]) {
                                                echo "<tr style='padding:4px;' class='TipoGasto".$rowD[1]."'>";
                                                echo "<td colspan='12' style='font-weight:bold; font-size:12px; color:#003399; font-weight:bold'>";
                                                echo strtoupper($rowD["tipocosto"]);
                                                echo "</td>";
                                            }

                                            if ($cadena2 != $rowD["codgrupocostos"]) {
                                                echo "<tr style='padding:4px;' class='TipoGasto".$rowD[1]."'>";
                                                echo "<td colspan='12' style='font-weight:bold; font-size:12px; color:#004466; padding-left:20px'>".strtoupper($rowD["dgrupocostos"])."</td>";
                                                echo "</tr>";
                                            }

                                            $subtotal += str_replace(",", "", $rowD["subtotal"]);
                                            $subigv += str_replace(",", "", $rowD["impigv"]);
                                            ?>
                                            <tr style="padding:4px;" id="<?=$count ?>" class="TipoGasto<?=$rowD[1].$rowD[14] ?>">
                                                <td align="center" >
                                                    <input type="hidden" name="item<?=$count ?>" id="item<?=$count ?>" value="<?=$rowD["item"] ?>" />
                                                    <input type="hidden" name="nroconfeccion<?=$count ?>" id="nroconfeccion<?=$count ?>" value="<?=$rowD["nroconfeccion"] ?>" />
                                                    <label class="Item"><?=$rowD["item"] ?></label>
                                                </td>
                                                <td align="left" >
                                                    <input type="hidden" name="descripcion<?=$count ?>" id="descripcion<?=$count ?>" value="<?=$rowD["descripcion"] ?>" />
                                                    <?=strtoupper($rowD["descripcion"]) ?>
                                                </td>
                                                <td align="center" >
                                                    <input type="hidden" name="unimed<?=$count ?>" id="unimed<?=$count ?>" value="<?=$rowD["unimed"] ?>" />
                                                    <?=strtoupper($rowD["abreviado"]) ?>
                                                </td>
                                                <td align="center" >
                                                    <input type="text" id="cantidad<?=$count ?>" style="text-align:center" size="5" name="cantidad<?=$count ?>" value="<?=number_format($rowD["cantidad"], 2)?>" class="inputtext" onkeyup="calcular_importe(this, 'punitario<?=$count ?>',<?=$count ?>);" onkeypress="return permite(event, 'num')" />
                                                </td>
                                                </td>
                                                <td align="center" >
                                                    <input type="text" id="punitario<?=$count ?>" name="punitario<?=$count ?>" style="text-align:right" size="10" value="<?=$rowD["preciounitario"] ?>" class="inputtext" onkeyup="calcular_importe(this, 'cantidad<?=$count ?>',<?=$count ?>);" onkeypress="return permite(event, 'num')" />
                                                </td>
                                                <td align="center" >
                                                    <input type="text" id="total<?=$count ?>" name="total<?=$count ?>" style="text-align:right" size="10" value="<?=number_format($rowD["subtotal"], 2) ?>" class="inputtext" readonly="readonly" />
                                                </td>
                                                <td align="center" >
                                                    <input type="text" size="8" name="impempresa<?=$count ?>" id="impempresa<?=$count ?>" class="inputtext" style="text-align:right" value="<?=$rowD["importempresa"] ?>" onkeypress="return permite(event, 'num')" />
                                                </td>
                                                <td align="center" >
                                                    <input type="text" size="8" name="impusuario<?=$count ?>" id="impusuario<?=$count ?>" class="inputtext" style="text-align:right" value="<?=$rowD["importeusuario"] ?>" onkeypress="return permite(event, 'num')" />
                                                </td>
                                                <td align="center" >
                                                    <input type="hidden" name="afectaigvtext<?=$count ?>" id="afectaigvtext<?=$count ?>" value="<?=$rowD["afectaigv"] == 1 ? 1 : 0 ?>" />
                                                    <input type="checkbox" name="afectaigv<?=$count ?>" id="afectaigv<?=$count ?>" value="checkbox" <?=$d = "";
                                            $rowD["afectaigv"] == 1 ? $d = "checked='checked'" : $d = "";
                                            echo $d; ?> onclick="calcular_igv_x(this, $('#total<?=$count ?>').val(), 'impigv<?=$count ?>');
                                                            CambiarEstado(this, 'afectaigvtext<?=$count ?>');" />
                                                </td>
                                                <td align="center" >
                                                    <input type="text" size="8" name="impigv<?=$count ?>" id="impigv<?=$count ?>" class="inputtext" style="text-align:right" value="<?=number_format($rowD["impigv"], 2) ?>" readonly="readonly" />
                                                </td>
                                                <td>
                                                    <input type="hidden" name="afectaempresatext<?=$count ?>" id="afectaempresatext<?=$count ?>" value="1" />
                                                    <input type="checkbox" name="afectaempresa<?=$count ?>" id="afectaempresa<?=$count ?>" value="checkbox" checked='checked' onclick="CambiarEstado(this, 'afectaempresatext<?=$count ?>');" />
                                                </td>
                                                <td align="right" >
                                                    <a href="javascript:QuitaItem(<?=$count ?>)" class="Del" >
                                                        <span class="ui-icon-delete" title="Quitar"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                            $cadena = $rowD[codtipocostos];
                                            $cadena2 = $rowD[codgrupocostos];
                                        }
                                    }

                                    //$impgasadm = floatval($impgasadm) / 100 * floatval($subtotal);
                                    $subigv = floatval($subigv)+floatval($impgasadm * ($impigvparamae["valor"] / 100));
                                    //$redondeoAnt = round($subtotal+$subigv+$impgasadm, 1);
                                    //$redondeoAct = round($subtotal+$subigv+$impgasadm, 2);
                                    //$redondeo = $redondeoAnt - $redondeoAct;
                                    $totalpresupuesto = $subtotal+$subigv+$redondeo+$impgasadm;
                                    ?>
                                </tbody>
                            </table>
                            <div style="height:20px">
                                <input name="cont" id="cont" type="hidden" value="<?=$count ?>" />
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr align="right">
                                    <td>
                                        <table border="1" align="center" cellspacing="0" class="ui-widget"   width="300" id="tbtotales" rules="rows" >
                                            <thead class="ui-widget-header" >
                                                <tr class="CabIndex">
                                                    <td colspan="3" align="center">Montos Totales del Costo de la Operacion</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr style="padding:4px">
                                                    <td width="275">Sub. Total Costos de Operacion </td>
                                                    <td width="23" align="center">:</td>
                                                    <td width="94" align="right">
                                                        <input type="text" size="10" name="subtotal" id="subtotal" class="inputtext" style="text-align:right" value="<?=number_format($subtotal, 2) ?>" readonly="readonly" />
                                                    </td>
                                                </tr>
                                                <tr style="padding:4px">
                                                    <td width="275">Gastos de Administraci&oacute;n y/o Utilidad </td>
                                                    <td width="23" align="center">:</td>
                                                    <td width="94" align="right">
                                                        <input type="text" size="10" name="impgasadm" id="impgasadm" class="inputtext" style="text-align:right" value="<?=number_format($impgasadm, 2) ?>" readonly="readonly" />
                                                    </td>
                                                </tr>
                                                <tr style="padding:4px">
                                                    <td>Igv</td>
                                                    <td align="center">:</td>
                                                    <td align="right">
                                                        <input type="text" size="10" name="subigv" id="subigv" class="inputtext" style="text-align:right" value="<?=number_format($subigv, 2) ?>" readonly="readonly" />
                                                    </td>
                                                </tr>
                                                <tr style="padding:4px">
                                                    <td>Redondeo</td>
                                                    <td align="center">:</td>
                                                    <td align="right">
                                                        <input type="text" size="10" name="subredondeo" id="subredondeo" class="inputtext" style="text-align:right" value="<?=number_format($redondeo, 2) ?>" readonly="readonly" />
                                                    </td>
                                                </tr>
                                                <tr style="padding:4px">
                                                    <td>Total del Costo de la Operacion </td>
                                                    <td align="center">:</td>
                                                    <td align="right">
                                                        <input type="text" size="10" name="totpresupuesto" id="totpresupuesto" class="inputtext" style="text-align:right" value="<?=number_format($totalpresupuesto, 2) ?>" readonly="readonly" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="TitDetalle">&nbsp;</td>
                    <td colspan="5" class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>

        </table>
    </form>
</div>
<?php
if ($Id != "") {
    if ($cabpresupuesto["tipopresupuesto"] == 2) {
        $diametro = $cabpresupuesto["coddiametrosagua"];
    } else {
        $diametro = $cabpresupuesto["coddiametrosdesague"];
    }
    ?>
    <script>
        //cargar_calles(<?=$cabpresupuesto["codsector"] ?>,<?=$codsuc ?>,<?=$cabpresupuesto["codcalle"] ?>)
        cargar_diametros('<?=$cabpresupuesto["tipopresupuesto"]?>', '<?=$diametro?>');
        cargar_sectores(<?=$codsuc ?>, '<?=$cabpresupuesto["codzona"] ?>', '<?=$cabpresupuesto["codsector"] ?>', 1);
        cargar_calles('<?=$cabpresupuesto["codsector"] ?>',<?=$codsuc ?>, '<?=$cabpresupuesto["codzona"] ?>', '<?=$cabpresupuesto["codcalle"] ?>')
    </script>
    <?php
}
?>
