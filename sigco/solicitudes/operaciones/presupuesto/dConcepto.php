<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	//include("../../../../config.php");
	include("../../../../objetos/clsDrop.php");
	$objMantenimiento 	= new clsDrop();
	$impigvparamae	= $objMantenimiento->getParamae("IMPIGV",$codsuc);

	$metros       = $_POST["metros"];
	$tipoconexion = $_POST["tipoconexion"];
	$codsuc       = $_POST["codsuc"];
	$Id           = $_POST["Id"];
	$count        = $_POST["count"];
	$countgrupo   = $_POST["countprupo"];
	$nrotipos     = $_POST["nrotipos"];
	$TipoP        = $_POST["TipoP"];
	$subtotal     = 0;
	$subigv       = 0;

	//var_dump($_POST);exit;
		
	$sql = "SELECT c.codtipocostos,t.descripcion as tipocostos,c.descripcion,c.nroconfeccion,u.abreviado,c.cantidad,c.preciounitario,c.unimed,c.total,
		c.montoempresa,c.montousuario,c.afectaigv,c.impigv,c.unimed, c.codgrupocostos, gc.descripcion as dgrupocostos
		FROM solicitudes.confecpreagudes as c
		INNER JOIN public.tipocostos as t on(c.codtipocostos=t.codtipocostos)
		INNER JOIN public.grupocostos as gc on (gc.codgrupocostos=c.codgrupocostos)
		INNER JOIN public.unidades as u on(c.unimed=u.unimed)
		WHERE /*c.estareg=1 and*/ c.codsuc=? AND c.nroconfeccion=? order by c.codtipocostos";
		/*------> and c.tipoconexion=? --------------*/
	$consulta = $conexion->prepare($sql);
	//$consulta->execute(array($tipoconexion,$codsuc,$Id));
	$consulta->execute(array($codsuc,$Id));
	$items = $consulta->fetchAll();
		
		$codtipocostos = "";
		$Col=11;
		 if($TipoP==1)
		 	$Col=12;
		foreach($items as $row)
		{
		
			if($count==0)
			{
			 	echo "<tr style='padding:4px;' class='TipoGasto".$row[0]."'>";
			 	echo "<td colspan='".$Col."' style='font-weight:bold; font-size:12px; color:#003399;'>".strtoupper($row["tipocostos"])."</td>";
			 	echo "</tr>";

			 	if($countgrupo==0){
					echo "<tr style='padding:4px;' class='GrupoGasto".$row["codgrupocostos"]."'>";
					echo "<td colspan='".$Col."' style='font-weight:bold; font-size:12px; color:#004466; padding-left:20px'>".strtoupper($row["dgrupocostos"])."</td>";
					echo "</tr>";
				}
			}
			
			$count    = $nrotipos;
			$cantidad = $row["cantidad"];
			$importe  = $row["total"];
			$impigv   = $row["impigv"];

			//if($row["codgrupocostos"]==2) //podria ser
			//{
				//$cantidad 	= $metros;
				//$importe 	= $cantidad * $row["preciounitario"]* $metros;
			//}
			$importe 	= $cantidad * $row["preciounitario"]* $metros;
			if($row["afectaigv"]==1)
				{
					$impigv = $importe*($impigvparamae["valor"]/100); 
				}else{
					$impigv = "0.00";
				}
			$codtipocostos=$row["codtipocostos"];
			
			$subtotal   += $importe;
			$subigv		+= $impigv;
?>
	<tr style="padding:4px;" id="<?=$count?>" class="TipoGastoGrupo<?=$row[0].$row[14]?>">
	    <td align="center">
			<input type="hidden" name="item<?=$count?>" id="item<?=$count?>" value="<?=$count?>" />
			<input type="hidden" name="nroconfeccion<?=$count?>" id="nroconfeccion<?=$count?>" value="<?=$row["nroconfeccion"]?>" />
			<label class="Item"><?=$count?></label>
		</td>
        <td>
        	<input type="hidden" name="descripcion<?=$count?>" id="descripcion<?=$count?>" value="<?=$row["descripcion"]?>" />
            <?=strtoupper($row["descripcion"])?>
        </td>
        <td align="center">
        	<input type="hidden" name="unimed<?=$count?>" id="unimed<?=$count?>" value="<?=$row["unimed"]?>" />
            <?=strtoupper($row["abreviado"])?>
        </td>
        <td>
        	<input type="text" id="cantidad<?=$count?>" style="text-align:center" size="5" name="cantidad<?=$count?>" value="<?=$cantidad?>" class="inputtext" onkeyup="calcular_importe(this,'punitario<?=$count?>',<?=$count?>);" onkeypress="return permite(event,'num')" /></td>
        </td>
        <td>
        	<input type="text" id="punitario<?=$count?>" name="punitario<?=$count?>" style="text-align:right" size="10" value="<?=$row["preciounitario"]?>" class="inputtext" onkeyup="calcular_importe(this,'cantidad<?=$count?>',<?=$count?>);" onkeypress="return permite(event,'num')" />
        </td>
        <td>
        	<input type="text" id="total<?=$count?>" name="total<?=$count?>" style="text-align:right" size="10" value="<?=number_format($importe,2)?>" class="inputtext" readonly="readonly" />
        </td>
        <td>
        	<input type="text" size="8" name="impempresa<?=$count?>" id="impempresa<?=$count?>" class="inputtext" style="text-align:right" value="<?=$row["montoempresa"]?>" onkeypress="return permite(event,'num')" />
        </td>
        <td>
        	<input type="text" size="8" name="impusuario<?=$count?>" id="impusuario<?=$count?>" class="inputtext" style="text-align:right" value="<?=$row["montousuario"]?>" onkeypress="return permite(event,'num')" />
        </td>
        <td align="center">
        	<input type="hidden" name="afectaigvtext<?=$count?>" id="afectaigvtext<?=$count?>" value="<?=$row["afectaigv"]==1?1:0?>" />
        	<input type="checkbox" name="afectaigv<?=$count?>" id="afectaigv<?=$count?>" value="checkbox" <?=$d="";$row["afectaigv"]==1?$d="checked='checked'":$d="";echo $d;?> onclick="calcular_igv_x(this,$('#total<?=$count?>').val(),'impigv<?=$count?>');CambiarEstado(this,'afectaigvtext<?=$count?>');" />
        </td>
        <td align="center">
        	<input type="text" size="8" name="impigv<?=$count?>" id="impigv<?=$count?>" class="inputtext" style="text-align:right" value="<?=number_format($impigv,2)?>" readonly="readonly" />
        </td>
        <?php
        if($TipoP==1)
        {
        ?>
        <td>
        	<input type="hidden" name="afectaempresatext<?=$count?>" id="afectaempresatext<?=$count?>" value="1" />
        	<input type="checkbox" name="afectaempresa<?=$count?>" id="afectaempresa<?=$count?>" value="checkbox" checked='checked' onclick="CambiarEstado(this,'afectaempresatext<?=$count?>');" />
        </td>
        <?php
    }
        ?>
        <td align="right" >
         <a href="javascript:QuitaItem(<?=$count?>)" class="Del" >
          <span class="ui-icon-delete" title="Quitar"></span></a>
      </td>
      </tr>
<?php	
	}
	
	
	
?>
