<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsDrop.php");
// ini_set("display_errors", 1);
// error_reporting(E_ALL);
$objMantenimiento = new clsDrop();

$codsuc = $_POST["codsuc"];

$impgasadm = $objMantenimiento->getParamae("IMPPTO", $codsuc); //$_POST['utilidad'];//$impgasadm['valor'];//

$impgasadmV = $impgasadm['valor'];

$metros = $_POST["metros"];
$tipoconexion = $_POST["tipoconexion"];

$tppresupuesto = $_POST["codtipopresupuesto"];

$impigvparamae = $objMantenimiento->getParamae("IMPIGV", $codsuc);
//$impgasadm        = $objMantenimiento->getParamae("IMPPTO",$codsuc);
?>
<table border="1" align="center" cellspacing="0" class="ui-widget"  width="930" id="tbpresupuesto" rules="all" >
    <thead class="ui-widget-header" >
        <tr class="CabIndex">
            <th width="40" align="center" >Item</th>
            <th align="center" >Descripcion</th>
            <th width="40" align="center" >Und.</th>
            <th width="50" align="center" >Cant</th>
            <th width="70" align="center" >P. Unit. </th>
            <th width="70" align="center" >Importe</th>
            <th width="70" align="center" >Imp. Emp. </th>
            <th width="70" align="center" >Imp. Usua. </th>
            <th width="40" align="center" >Af. Igv. </th>
            <th width="70" align="center" >Mont. Igv</th>
            <th width="40" align="center" >Af. Emp</th>
            <th width="25" align="center" scope="col">
                <span class="ui-icon-add" title="Agregar Concepto" onclick="BuscarConceptos(this)"></span>
            </th>

        </tr>
    </thead>
    <tbody id="datos">
        <?php
        $count = 0;
        $subtotal = 0;
        $subigv = 0;

        $sql = "SELECT c.codtipocostos,t.descripcion as tipocostos,c.descripcion,c.nroconfeccion,u.abreviado,c.cantidad,c.preciounitario,c.unimed,c.total,
            c.montoempresa,c.montousuario,c.afectaigv,c.impigv,c.unimed, c.codgrupocostos, gc.descripcion as dgrupocostos
            from solicitudes.confecpreagudes as c
            INNER JOIN public.tipocostos as t on(c.codtipocostos=t.codtipocostos)
            INNER JOIN public.grupocostos as gc on (gc.codgrupocostos=c.codgrupocostos)
            INNER JOIN public.unidades as u on(c.unimed=u.unimed)
            WHERE c.estareg=1 and c.requerido=1 and c.codsuc=? and c.codtipopresupuesto=? 
            ORDER BY c.codtipocostos, c.codgrupocostos,c.descripcion";
        // var_dump($sql);exit;
        // var_dump($codsuc,$tppresupuesto);exit;

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $tppresupuesto));
        $items = $consulta->fetchAll();

        $codtipocostos = "";
        $codgrupocostos = "";
        foreach ($items as $row) {
            $count++;

            if ($codtipocostos != $row["codtipocostos"]) {
                echo "<tr style='padding:4px;' class='TipoGasto" . $row["codtipocostos"] . "'>";
                echo "<td colspan='12' style='font-weight:bold; font-size:12px; color:#003399;'>" . strtoupper($row["tipocostos"]) . "</td>";
                echo "</tr>";
            }

            if ($codgrupocostos != $row["codgrupocostos"]) {
                echo "<tr style='padding:4px;' class='GrupoGasto" . $row["codgrupocostos"] . "'>";
                echo "<td colspan='12' style='font-weight:bold; font-size:12px; color:#004466; padding-left:20px'>" . strtoupper($row["dgrupocostos"]) . "</td>";
                echo "</tr>";
            }

            $cantidad = $metros; //$row["cantidad"];
            $importe = $row["total"];
            $impigv = $row["impigv"];
            $importe = $cantidad * $row["preciounitario"]; // * $metros
            
            if ($row["afectaigv"] == 1) {
                $impigv = $importe * ($impigvparamae["valor"] / 100);
            } else {
                $impigv = "0.00";
            }
            $codtipocostos = $row["codtipocostos"];
            $codgrupocostos = $row["codgrupocostos"];

            $subtotal += $importe;
            $subigv += $impigv;
            ?>
            <tr style="padding:4px;" id="<?=$count ?>" class="TipoGastoGrupo<?=$row[0] . $row[14] ?>" data="<?=$count ?>">
                <td align="center">
                    <input type="hidden" name="item<?=$count ?>" id="item<?=$count ?>" value="<?=$count ?>" />
                    <input type="hidden" name="nroconfeccion<?=$count ?>" id="nroconfeccion<?=$count ?>" value="<?=$row["nroconfeccion"] ?>" />
                    <label class="Item"><?=$count ?></label>
                </td>
                <td style="padding-left:5px;">
                    <input type="hidden" name="descripcion<?=$count ?>" id="descripcion<?=$count ?>" value="<?=$row["descripcion"] ?>" />
                    <?=strtoupper($row["descripcion"]) ?>
                </td>
                <td align="center">
                    <input type="hidden" name="unimed<?=$count ?>" id="unimed<?=$count ?>" value="<?=$row["unimed"] ?>" />
                    <?=strtoupper($row["abreviado"]) ?>
                </td>
                <td>
                    <input type="text" id="cantidad<?=$count ?>" style="text-align:center; width:50px" name="cantidad<?=$count ?>" value="<?=intval(number_format($cantidad, 2))?>" class="inputtext" onkeyup="calcular_importe(this, 'punitario<?=$count ?>',<?=$count ?>);" onkeypress="return permite(event, 'num')" /></td>
                </td>
                <td>
                    <input type="text" id="punitario<?=$count ?>" name="punitario<?=$count ?>" style="text-align:right; width:70px;" value="<?=number_format($row["preciounitario"], 2) ?>" class="inputtext" onkeyup="calcular_importe(this, 'cantidad<?=$count ?>',<?=$count ?>);" onkeypress="return permite(event, 'num')" />
                </td>
                <td>
                    <input type="text" id="total<?=$count ?>" name="total<?=$count ?>" style="text-align:right; width:70px;" value="<?=number_format($importe, 2)?>" class="inputtext" readonly="readonly" />
                </td>
                <td>
                    <input type="text" name="impempresa<?=$count ?>" id="impempresa<?=$count ?>" class="inputtext" style="text-align:right; width:70px;" value="<?=number_format($row["montoempresa"], 2)?>" onkeypress="return permite(event, 'num')" />
                </td>
                <td>
                    <input type="text" name="impusuario<?=$count ?>" id="impusuario<?=$count ?>" class="inputtext" style="text-align:right; width:70px;" value="<?=number_format($row["montousuario"], 2)?>" onkeypress="return permite(event, 'num')" />
                </td>
                <td align="center">
                    <input type="hidden" name="afectaigvtext<?=$count ?>" id="afectaigvtext<?=$count ?>" value="<?=$row["afectaigv"] == 1 ? 1 : 0 ?>" />
                    <input type="checkbox" name="afectaigv<?=$count ?>" id="afectaigv<?=$count ?>" value="checkbox" <?=$d = ""; $row["afectaigv"] == 1 ? $d = "checked='checked'" : $d = ""; echo $d; ?> onclick="calcular_igv_x(this, $('#total<?=$count ?>').val(), 'impigv<?=$count ?>');
                            CambiarEstado(this, 'afectaigvtext<?=$count ?>');" />
                </td>
                <td>
                    <input type="text" name="impigv<?=$count ?>" id="impigv<?=$count ?>" class="inputtext" style="text-align:right; width:70px;" value="<?=number_format($impigv, 2) ?>" readonly="readonly" />
                </td>
                <td align="center">
                    <input type="hidden" name="afectaempresatext<?=$count ?>" id="afectaempresatext<?=$count ?>" value="1" />
                    <input type="checkbox" name="afectaempresa<?=$count ?>" id="afectaempresa<?=$count ?>" value="checkbox" checked='checked' onclick="CambiarEstado(this, 'afectaempresatext<?=$count ?>');" />
                </td>
                <td align="center" >
                    <a class="eliminarreg Del" href="javascript:void(0)" data="<?=$count ?>">
                        <span class="ui-icon-delete" title="Quitar"></span>
                    </a>
                </td>
            </tr>
    <?php
}
/*
$impgasadm = floatval($impgasadm) / 100 * floatval($subtotal);
$subigv = floatval($subigv) + floatval($impgasadm * ($impigvparamae["valor"] / 100));
$SubT= $subtotal + $subigv + $impgasadm;
$redondeoAnt = round($SubT, 1, PHP_ROUND_HALF_EVEN);

$redondeoAct = round($SubT, 2);
$redondeo = $redondeoAnt - $redondeoAct;
$totalpresupuesto = $subtotal + $subigv + $redondeo + $impgasadm;
*/
$impgasadmI	= floatval($impgasadmV) / 100 * floatval($subtotal);
$subigv 	= floatval($subigv) + floatval($impgasadmI * ($impigvparamae["valor"] / 100));

$TTotal		= floatval(number_format($subtotal + $subigv + $impgasadmI, 2));
$redondeoAnt 	= round($TTotal, 1);
$redondeoAct 	= round($TTotal, 2);
$redondeo 	= $redondeoAnt - $redondeoAct;
$totalpresupuesto = $subtotal + $subigv + $redondeo + $impgasadmI;
        
?>
    </tbody>
</table>
<div style="height:20px">
    <input name="cont" id="cont" type="hidden" value="<?=$count ?>" />
</div>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr align="right">
        <td>
            <table border="1" align="center" cellspacing="0" class="ui-widget"   width="400" id="tbtotales"  rules="rows" >
                <thead class="ui-widget-header" >
                    <tr class="CabIndex">
                        <td colspan="3" align="center">Montos Totales del Costo de la Operacion</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="275" style="padding:5px" >Sub. Total Costos de Operacion</td>
                        <td width="23" align="center">:</td>
                        <td width="94" align="right">
                            <input type="text" size="10" name="subtotal" id="subtotal" class="inputtext" style="text-align:right" value="<?=number_format($subtotal, 2) ?>" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td width="275" style="padding:5px" >Gastos de Administraci&oacute;n y Utilidad (<?=$impgasadmV?>%)</td>
                        <td width="23" align="center">:</td>
                        <td width="94" align="right">
                            <input type="text" size="10" name="impgasadm" id="impgasadm" class="inputtext" style="text-align:right" value="<?=number_format($impgasadmI, 2) ?>" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:5px" >Igv</td>
                        <td align="center">:</td>
                        <td align="right">
                            <input type="text" size="10" name="subigv" id="subigv" class="inputtext" style="text-align:right" value="<?=number_format($subigv, 2) ?>" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:5px">Redondeo</td>
                        <td align="center">:</td>
                        <td align="right">
                            <input type="text" size="10" name="subredondeo" id="subredondeo" class="inputtext" style="text-align:right" value="<?=number_format($redondeo, 2) ?>" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:5px">Total del Costo de la Operacion </td>
                        <td align="center">:</td>
                        <td align="right">
                            <input type="text" size="10" name="totpresupuesto" id="totpresupuesto" class="inputtext" style="text-align:right" value="<?=number_format($totalpresupuesto, 2) ?>" readonly="readonly" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>