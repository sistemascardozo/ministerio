<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include('../../../../objetos/clsFunciones.php');

    $Op = $_GET["Op"];
    $objFunciones = new clsFunciones();

    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $nrosolicitud   = $_POST["nrosolicitud"];
    $codcliente     = $_POST["codcliente"];
    $propietario    = strtoupper($_POST["propietario"]);
    $codzona = $_POST["codzona"];
    $sector  = $_POST["sector"];
    $area    = $_POST["area"];
    $calles  = $_POST["calles"];
    $nrocalle = $_POST["nrocalle"];
    $tipodocumento = $_POST["tipodocumento"];
    $nrodocumento  = $_POST["nrodocumento"];
    $fechaemision  = $objFunciones->CodFecha($_POST["fechaemision"]);
    $tiposervicio  = $_POST["tiposervicio"];
    $distrito   = $_POST["distrito"];
    $Referencia = $_POST["Referencia"];
    $grafico    = $_POST["grafico"];
    $idusuario  = $_SESSION['id_user'];
    $estareg    = 1; //EM PROCESO;

    $representante = $_POST["representante"];
    $sectorrep  = $_POST["sectorrep"];
    $manzanarep = $_POST["manzanarep"];
    $callerep   = $_POST["callerep"];
    $loterep    = $_POST["loterep"];
    $nrocallerep = $_POST["nrocallerep"];
    $tipodocumentorep = $_POST["tipodocumentorep"];
    $nrodocumentorep  = $_POST["nrodocumentorep"];
    $distritorep = $_POST["distritorep"];
    $lotesol     = $_POST["lotesol"];
    $callesol    = $_POST["callesol"];
    $manzanasol  = $_POST["manzanasol"];
    $sectorsol = $_POST["sectorsol"];
    $manzanas  = $_POST["manzanas"];
    $lote = $_POST["lote"];
    $nrocallepred = $_POST["nrocallepred"];
    $distritopred = $_POST["distritopred"];
    $correo = $_POST["correo"];
    $nropartidaregistral = $_POST["nropartidaregistral"];
    $nroinspeccion       = $_POST["nroinspeccion"] ? $_POST["nroinspeccion"] : 0;
    $nroinscripcion      = $_POST["nroinscripcion"] ? $_POST["nroinscripcion"] : 0;
    
    $crepresentante = $_POST['crepresentante'];
    
    if ($Op != 2) {
        if ($Op == 0) {
            
            $id = $objFunciones->setCorrelativosVarios(15, $codsuc, "SELECT", 0);
            $nrosolicitud = $id;
            if (empty($codcliente))            
                $codcliente = $objFunciones->setCorrelativosVarios(1, $codsuc, "SELECT", 0);
            if (empty($nroinscripcion))
            {
                $nroinscripcion = $objFunciones->setCorrelativosVarios(10, $codsuc, "SELECT", 0);
            }                
            
            if($crepresentante==1)
            {
                $representante = $propietario;
                $manzanarep    = $manzanasol;
                $callerep      = $callesol;
                $loterep       = $lotesol;
                $nrocallerep   = $nrocalle;
                $tipodocumentorep = $tipodocumento;
                $nrodocumentorep  = $nrodocumento;
                $distritorep      = $distritopred;
            }
            
            $sql = "INSERT INTO solicitudes.solicitudes(codemp,codsuc,nrosolicitud,codcliente,";
            $sql .= " propietario,codcalle,nrocalle,fechaemision,codtipodocumento,nrodocumento,";
            $sql .= "codtiposervicio,grafico,codubigeo,referencia,codusu,codsector,area,estareg, representante, sectorrep, ";
            $sql .= "manzanarep, callerep, loterep, nrocallerep, codtipodocumentorep, nrodocumentorep, codubigeorep, lotesol, callesol, manzanasol, ";
            $sql .= "sectorsol, codmanzanas, lote, nrocallepred, codubigeopred,nropartidaregistral,codzona,nroinspeccion,nroinscripcion,correo) ";
            $sql .= "values(:codemp,:codsuc,:nrosolicitud,:codcliente,:propietario,:calles,:nrocalle,:fechaemision,:tipodocumento,:nrodocumento,";
            $sql .= ":tiposervicio,:grafico,:distrito,:Referencia,:idusuario,:sector,:area,:estareg,:representante,:sectorrep, :manzanarep, :callerep,";
            $sql .= ":loterep,:nrocallerep,:tipodocumentorep,:nrodocumentorep,:distritorep,:lotesol,:callesol,:manzanasol,:sectorsol,:manzanas,";
            $sql .= ":lote,:nrocallepred,:distritopred,:nropartidaregistral,:codzona,:nroinspeccion,:nroinscripcion,:correo) ";
            
            /*----> FACTIBILIDAD DESDE CAJA ---*/
            /*------- ACTUALIZAR SOLICITUD INSPECCION --------*/
            $UpdSol="UPDATE catastro.solicitudinspeccion
                SET nroinscripcion=:nroinscripcion, codzona=:codzona, 
                    codsector=:codsector, codmanzanas=:codmanzanas, lote=:lote, sublote=:sublote, 
                    codcliente=:codcliente, codcalle=:codcalle,nuevocliente=1
                WHERE codemp=:codemp and codsuc=:codsuc and nroinspeccion=:nroinspeccion";
            $result = $conexion->prepare($UpdSol);
            $result->execute(array(":codemp"=> $codemp, ":codsuc"=> $codsuc, ":nroinspeccion"=> $nroinspeccion,
                    ":nroinscripcion"=>$nroinscripcion, ":codzona"=>$codzona, 
                    ":codsector"=>$sector, ":codmanzanas"=>$manzanas, ":lote"=>$lote, ":sublote"=>$sublote, 
                    ":codcliente"=>$codcliente, ":codcalle"=>$calles
                ));
            //print_r(array(":codemp"=> $codemp, ":codsuc"=> $codsuc, ":nroinspeccion"=> $nroinspeccion));
        }
        if ($Op == 1) {
            $sql = "UPDATE solicitudes.solicitudes set codcliente=:codcliente,propietario=:propietario,codcalle=:calles,nrocalle=:nrocalle,fechaemision=:fechaemision,";
            $sql .= "codtipodocumento=:tipodocumento,nrodocumento=:nrodocumento,codtiposervicio=:tiposervicio,grafico=:grafico,codubigeo=:distrito,referencia=:Referencia,";
            $sql .= "codusu=:idusuario,codsector=:sector,area=:area ,estareg=:estareg, representante=:representante,sectorrep=:sectorrep, manzanarep=:manzanarep, callerep=:callerep,";
            $sql .= "loterep=:loterep,nrocallerep=:nrocallerep,codtipodocumentorep=:tipodocumentorep,nrodocumentorep=:nrodocumentorep,codubigeorep=:distritorep,lotesol=:lotesol,callesol=:callesol,";
            $sql .= "manzanasol=:manzanasol,sectorsol=:sectorsol,codmanzanas=:manzanas,lote=:lote,nrocallepred=:nrocallepred,codubigeopred=:distritopred,";
            $sql .= "nropartidaregistral=:nropartidaregistral,codzona= :codzona,nroinspeccion=:nroinspeccion,nroinscripcion=:nroinscripcion,correo=:correo";
            $sql .= " WHERE codemp=:codemp and codsuc=:codsuc and nrosolicitud=:nrosolicitud";
        }
        $conexion->beginTransaction();
        $result = $conexion->prepare($sql);
        $result->execute(array(":codemp"=> $codemp, ":codsuc"=> $codsuc, ":nrosolicitud"=> $nrosolicitud, ":codcliente"=> $codcliente, ":propietario"=> $propietario,
            ":calles"=> $calles, ":nrocalle"=> $nrocalle, ":fechaemision"=> $fechaemision, ":tipodocumento"=> $tipodocumento, ":nrodocumento"=> $nrodocumento,
            ":tiposervicio"=> $tiposervicio, ":grafico"=> $grafico, ":distrito"=> $distrito, ":Referencia"=> $Referencia, ":idusuario"=> $idusuario,
            ":sector"=> $sector, ":area"=> $area, ":estareg"=> $estareg, ":representante"=> $representante, ":sectorrep"=> $sectorrep, ":manzanarep"=> $manzanarep,
            ":callerep"=> $callerep, ":loterep"=> $loterep, ":nrocallerep"=> $nrocallerep, ":tipodocumentorep"=> $tipodocumentorep, ":nrodocumentorep"=> $nrodocumentorep,
            ":distritorep"=> $distritorep, ":lotesol"=> $lotesol, ":callesol"=> $callesol, ":manzanasol"=> $manzanasol, ":sectorsol"=> $sectorsol, ":manzanas"=> $manzanas,
            ":lote"=> $lote, ":nrocallepred"=> $nrocallepred, ":distritopred"=> $distritopred, ":nropartidaregistral"=> $nropartidaregistral,
            ":codzona"=> $codzona, ":nroinspeccion"=> $nroinspeccion, ":nroinscripcion"=> $nroinscripcion, ":correo"=> $correo));
        
        $Del="DELETE FROM solicitudes.detsolicitud_requisito WHERE nrosolicitud= ? and codsuc=? ";
        $res = $conexion->prepare($Del);
        $res->execute(array($nrosolicitud, $codsuc));
        
        $SqlReq="SELECT codrequisitos, descripcion FROM solicitudes.requisitos WHERE estareg=1 ";
        $conn = $conexion->prepare($SqlReq);
        $conn->execute(array());
        $itemsD = $conn->fetchAll();
                        
        foreach ($itemsD as $rowD) {
            $id= $rowD['codrequisitos'];
            $codrequisitos= $_POST['chk_'.$id];
            if($codrequisitos==1)
            {
                $Ins="INSERT INTO solicitudes.detsolicitud_requisito(codemp, codsuc, nrosolicitud, codrequisitos)
                    VALUES(".$codemp.", ".$codsuc.", ".$nrosolicitud.", ".$id.") ";
                $result = $conexion->query($Ins);
                //$result->execute(array(,, , ));
            }
        }
        
    } else {
        $conexion->beginTransaction();
        $estareg = 0;
        $sql = "UPDATE solicitudes.solicitudes set estareg=:estareg where codemp=:codemp and codsuc=:codsuc and nrosolicitud=:nrosolicitud";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codemp"=> $codemp, ":codsuc"=> $codsuc, ":nrosolicitud"=> $_POST['1form1_id'], ":estareg"=> $estareg));
		
		$nrosolicitud = $_POST['1form1_id'];
    }
    if ($Op != 2) {
        if ($nroinspeccion != '') {
            $sql = "UPDATE catastro.solicitudinspeccion set nrosolicitud=:nrosolicitud where codemp=:codemp and codsuc=:codsuc and nroinspeccion=:nroinspeccion";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codemp"=> $codemp, ":codsuc"=> $codsuc, ":nrosolicitud"=> $nrosolicitud, ":nroinspeccion"=> $nroinspeccion));
        }
    }

    //var_dump($result->errorInfo());
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        $data['res'] = 2;
        die(json_encode($data));
    } else {

        $conexion->commit();
        if ($Op == 0)
            $objFunciones->setCorrelativosVarios(15, $codsuc, "UPDATE", $nrosolicitud);
        if ($Op == 0)
        {
            if (empty($_POST["codcliente"]))
            $n = $objFunciones->setCorrelativosVarios(1, $codsuc, "UPDATE", $codcliente);
            if (empty($_POST["nroinscripcion"]))
            $n = $objFunciones->setCorrelativosVarios(10, $codsuc, "UPDATE", $nroinscripcion);
        }
            
        $res = 1;
    }

    $data['res'] = $res;
    $data['nrosolicitud'] = $nrosolicitud;
    echo json_encode($data);

?>
