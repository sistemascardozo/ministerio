<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsDrop.php");

$Op = $_POST["Op"];
$Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
$IdInspeccion = isset($_POST["IdInspeccion"]) ? $_POST["IdInspeccion"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=".$Op;
$Rep = 0;

$objMantenimiento = new clsDrop();

$fecha = date("d/m/Y");

$VerUbigueo="SELECT codubigeo FROM admin.sucursales WHERE codemp=1 AND codsuc='$codsuc' ";
$res= $conexion->query($VerUbigueo)->fetch();
$IdUbigeo = $res[0];
$IdUbigeoRep = $res[0];
$IdUbigeoPred = $res[0];

if ($Id != '' && $Id != '0') {
    $solicitudes = $objMantenimiento->setSolicitudes(" WHERE s.codemp=1 and s.codsuc=? and s.nrosolicitud=?", array($codsuc, $Id));
    $guardar = $guardar."&Id2=".$Id;
    $IdUbigeo = $solicitudes["codubigeo"];
    $IdUbigeoRep = $solicitudes["codubigeorep"];
    $IdUbigeoPred = $solicitudes["codubigeopred"];

    $fech = new DateTime($solicitudes["fechaemision"]);
    $fecha = $fech->format("d/m/Y");
}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript">
    var IdAnt = ''
    var IdGrafico = ''
    var urldir = '<?php echo $_SESSION['urldir']; ?>';
    var codsuc = '<?=$codsuc ?>';
    $(document).ready(function ()
    {
        if ('<?=$IdInspeccion ?>' != '')
            datos_solicitudinspeccion(<?=$IdInspeccion ?>)
        $("#tabs").tabs();
        $("#DivServicio").buttonset();

        //$( "#dialog:ui-dialog" ).dialog( "destroy" );
        $("#DivGrafico").dialog({
            autoOpen: false,
            height: 650,
            width: 500,
            modal: true,
            /*			show: "blind",
             hide: "scale",*/
            buttons: {
                "Aceptar": function () {
                    $("#grafico").val(IdGrafico)
                    $(this).dialog("close");
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            }
        });

        function formatResult(row) {
            return row[0];
        }
        function formatItem2(row) {
            r = "<table width='400'>";
            r = r+"<td width='20'>"+row[0]+"</td>";
            r = r+"<td width='250'>"+row[1]+"</td>";
            r = r+"<td width='220'>"+row[2]+"</td>";
            r = r+"<td width='20'>"+row[3]+"</td>";
            r = r+"</tr>"
            r = r+"</table>";
            return r;
        }

        $('#Cliente').autocomplete('../../../../autocomplet/clientes.php', {
            autoFill: true,
            width: 900,
            selectFirst: true,
            extraParams: {Op: 1, codsuc:<?=$codsuc ?>},
            formatItem: formatItem2,
            formatResult: formatResult,
            //mustMatch : true
            cacheLength: 0 //CACHE
                    //extraParams: {"Al": function() { return $("#IdAlmacenEgreso").val();}, "IdProyecto": function() { return $("#IdProyecto").val();}  }
        }).result(function (event, item) {
            !item ? '' : $("#codcliente").val(item[0])
            !item ? '' : $("#Cliente").val(item[1]);
            !item ? '' : $("#tipodocumento").val(item[5])
            !item ? '' : $("#nrodocumento").val(item[7])
            cargar_datos_usuario(item[7])
        });

        $("#fechaemision").datepicker(
                {
                    showOn: 'button',
                    direction: 'up',
                    buttonImage: '../../../../images/iconos/calendar.png',
                    buttonImageOnly: true,
                    showOn: 'both',
                            showButtonPanel: true,
                    beforeShow: function (input, inst)
                    {
                        inst.dpDiv.css({marginTop: (input.offsetHeight) - 20+'px', marginLeft: (input.offsetWidth) - 90+'px'});
                    }
                }
        );
        cargar_sectores(<?=$codsuc ?>, '<?=$solicitudes["codzona"] ?>', '<?=$solicitudes["codsector"] ?>', 1);
        cargar_manzana(<?=$codsuc ?>, '<?=$solicitudes["codsector"] ?>', '<?=$solicitudes["codmanzanas"] ?>', 1);
        cargar_calles('<?=$solicitudes["codsector"] ?>',<?=$codsuc ?>, '<?=$solicitudes["codzona"] ?>', '<?=$solicitudes["codcalle"] ?>')
        //cargar_calles('<?=$solicitudes["codsector"] ?>','<?=$codsuc ?>','<?=$solicitudes["codcalle"] ?>')
        //cargar_manzana('<?=$codsuc ?>','<?=$solicitudes["codsector"] ?>','<?=$solicitudes["codmanzanas"] ?>',1);
        cargar_provincia('<?=substr($IdUbigeo, 0, 2)."0000" ?>', '<?=$IdUbigeo ?>')
        cargar_distrito('<?=substr($IdUbigeo, 0, 4)."00" ?>', '<?=$IdUbigeo ?>');

        cargar_provinciarep('<?=substr($IdUbigeoRep, 0, 2)."0000" ?>', '<?=$IdUbigeoRep ?>')
        cargar_distritorep('<?=substr($IdUbigeoRep, 0, 4)."00" ?>', '<?=$IdUbigeoRep ?>');

        cargar_provinciapred('<?=substr($IdUbigeoPred, 0, 2)."0000" ?>', '<?=$IdUbigeoPred ?>')
        cargar_distritopred('<?=substr($IdUbigeoPred, 0, 4)."00" ?>', '<?=$IdUbigeoPred ?>');
    });
    function actualizacion()
    {

    }
    function buscar_usuarios()
    {
        object = "usuario"
        AbrirPopupBusqueda('<?php echo $_SESSION['urldir']; ?>sigco/catastro/operaciones/actualizacion/?Op=5', 1100, 550)
    }
    function Recibir(id)
    {
        if (object == "usuario")
        {
            $("#nroinscripcion").val(0)//NO ES NUEVO CLIENTE
            cargar_datos_usuario(id)
        }

    }

    function RecibirP(id, nroinscripcion)
    {
        //alert('--'+nroinscripcion);
        if (object == "solicitudinspeccion")
        {
            datos_solicitudinspeccion(id, nroinscripcion)
        }
    }

    function datos_solicitudinspeccion(nroinspeccion, nroinscripcion)
    {
        if (nroinscripcion == 0 || nroinscripcion == 0)
        {
            //alert('');
            /*-- PAGOS DESDE CAJA --*/
            $.ajax({
                url: urldir+'ajax/solicitudinspeccionCaja.php',
                type: 'POST',
                async: true,
                dataType: 'json',
                data: 'codsuc=<?=$codsuc ?>&nroinspeccion='+nroinspeccion,
                success: function (datos) {
                    $("#Cliente").val(datos.propietario)
                    $("#nroinspeccion").val(datos.nroinspeccion)
                    $("#representante").val(datos.propietario)
                }
            })
        }
        else
        {
            $.ajax({
                url: urldir+'ajax/solicitudinspeccion.php',
                type: 'POST',
                async: true,
                dataType: 'json',
                data: 'codsuc=<?=$codsuc ?>&nroinspeccion='+nroinspeccion,
                success: function (datos) {
                    $("#nuevocliente").val(datos.nuevocliente)
                    if (datos.nuevocliente == 0)
                    {
                        $("#nroinscripcion").val(0)//NO ES NUEVO CLIENTE
                        cargar_datos_usuario(datos.nroinscripcion)
                    }
                    else
                    {

                        $("#Cliente").val(datos.propietario)
                        $("#codcliente").val(datos.codcliente)
                        $("#nroinscripcion").val(datos.nroinscripcion)
                        $("#codzona").val(datos.codzona)
                        $("#lote").val(datos.lote)
                        $("#nrocallepred").val(datos.nrocalle)

                        $("#sectorsol").val(datos.sector)
                        $("#callesol").val(datos.direccion)
                        $("#nrocalle").val(datos.nrocalle)
                        $("#lotesol").val(datos.lote)

                        cargar_sectores(<?=$codsuc ?>, datos.codzona, datos.codsector, 0)
                        cargar_manzana(<?=$codsuc ?>, datos.codsector, datos.codmanzanas, 0);
                        cargar_calles(datos.codsector,<?=$codsuc ?>, datos.codzona, datos.codcalle)
                        if ($('input[name="crepresentante"]:checked').val() == 1)
                        {
                            $("#representante").val(datos.propietario)
                            $("#tipodocumentorep").val(datos.codtipodocumento)
                            $("#nrodocumentorep").val(datos.nrodocumento)
                            $("#sectorrep").val(datos.sector)
                            $("#callerep").val(datos.direccion)
                            $("#loterep").val(datos.lote)
                            $("#nrocallerep").val(datos.nrocalle)
                            $("#correo").val(datos.correo)
                        }

                    }
                    $("#nroinspeccion").val(datos.nroinspeccion)

                    //cargar_datos_periodo_facturacion(datos.codciclo)
                    //$("#codciclo").val(datos.codciclo)
                }
            })
        }

    }
    function cargar_datos_usuario(nroinscripcion)
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir']; ?>ajax/clientes.php',
            type: 'POST',
            async: true,
            data: 'codsuc=<?=$codsuc ?>&nroinscripcion='+nroinscripcion,
            dataType: "json",
            success: function (datos) {
                $("#codcliente").val(datos.codcliente)
                $("#Cliente").val(datos.propietario)
                $("#tipodocumento").val(datos.codtipodocumento)
                $("#nrodocumento").val(datos.nrodocumento)

                $("#nroinscripcion").val(nroinscripcion)
                $("#sectorsol").val(datos.sector)
                $("#callesol").val(datos.calle)
                $("#nrocalle").val(datos.nrocalle)
                $("#lotesol").val(datos.lote)
                $("#codzona").val(datos.codzona)

                cargar_sectores(<?=$codsuc ?>, datos.codzona, datos.codsector, 1)
                cargar_manzana(<?=$codsuc ?>, datos.codsector, datos.codmanzanas, 1)
                cargar_calles(datos.codsector,<?=$codsuc ?>, datos.codzona, datos.codcalle)

                //$("#zonarep").val(datos.zona)
                $("#nrocallepred").val(datos.nrocalle)
                $("#lote").val(datos.lote)

                if ($('input[name="crepresentante"]:checked').val() == 1)
                {
                    $("#representante").val(datos.propietario)
                    $("#tipodocumentorep").val(datos.codtipodocumento)
                    $("#nrodocumentorep").val(datos.nrodocumento)
                    $("#sectorrep").val(datos.sector)
                    $("#callerep").val(datos.calle)
                    $("#loterep").val(datos.lote)
                    $("#nrocallerep").val(datos.nrocalle)
                }


            }
        });
    }
    //803
    function  buscar_servicio()
    {
        object = "solicitudinspeccion"
        AbrirPopupBusqueda('../../../catastro/operaciones/solicitud/indexNew.php?Op=6&IdConcepto=803', 1080, 440)
    }

    function ValidarForm(Op)
    {
        if ($("#Cliente").val() == "")
        {
            $("#tabs").tabs({selected: 0});
            Msj("#Cliente", 'El Nombre del Propietario no puede ser NULO')
            return false;
        }
        $("#Cliente").removeClass("ui-state-error");
        
        if ($("#sector").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj("#sector", 'Seleccione Sector')
            return false;
        }
        $("#sector").removeClass("ui-state-error");
        
        if ($("#area").val() == '' || $("#area").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj("#area", 'El Area del Inmueble Ingresado no es Valido!!')
            return false;
        }
        $("#area").removeClass("ui-state-error");
        
        if ($("#calles").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj("#calles", 'Seleccione Calle')
            return false;
        }
        $("#calles").removeClass("ui-state-error");
        
        if ($("#nrocallepred").val() == '')
        {
            $("#tabs").tabs({selected: 0});
            Msj("#nrocallepred", 'Digite el Numero de Calle')
            return false;
        }
        $("#nrocallepred").removeClass("ui-state-error");
        
        if ($("#tiposervicio").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj("#tiposervicio", 'Seleccione el Tipo de Servicio')
            return false;
        }
        $("#tiposervicio").removeClass("ui-state-error");
        
        if ($("#departamentopred").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj("#departamento", 'Seleccione el Departamento')
            return false;
        }
        $("#departamentopred").removeClass("ui-state-error");
        
        if ($("#provinciapred").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj("#provincia", 'Seleccione la Provincia')
            return false;
        }
        $("#provinciapred").removeClass("ui-state-error");
        
        if ($("#distritopred").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj("#distrito", 'Seleccione el Distrito')
            return false;
        }
        $("#distritopred").removeClass("ui-state-error");
        
        if ($("#grafico").val() == "")
        {
            $("#tabs").tabs({selected: 0});
            Msj("#Generar", 'Genere el Grafico de Referencia')
            return false;
        }
        $("#grafico").removeClass("ui-state-error");
        
        if ($("#tipodocumento").val() == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj("#tipodocumento", 'Seleccione el Tipo de Documento')
            return false;
        }
        $("#tipodocumento").removeClass("ui-state-error");
        
        if ($("#nrodocumento").val() == '')
        {
            $("#tabs").tabs({selected: 1});
            Msj("#nrodocumento", 'Digite el Numero de Documento')
            return false;
        }
        $("#nrodocumento").removeClass("ui-state-error");
        
        if ($("#tipodocumentorep").val() == '')
            $("#tipodocumentorep").val(0)

        GuardarSolicitud(Op)
    }
    function GuardarSolicitud(Op)
    {
        var manzanas = $("#manzanas option:selected").html()
        var calledes = $("#calles option:selected").html()
        var lote = $("#lote").val()
        var nrocalle = $("#nrocallepred").val()
        //var distrito= $("#distritopred").val()
        //var tpdoc   = $("#tipodocumento").val()
        //var nrodoc  = $("#nrodocumento").val()
        //var propietario  = $("#Cliente").val()
        //var sol     = $("#representante1").val()
        /*---> Solicitante ---*/
        $("#manzanasol").val(manzanas)
        $("#callesol").val(calledes)
        $("#lotesol").val(lote)
        $("#nrocalle").val(nrocalle)

        $.ajax({
            url: '<?php echo $_SESSION['urldir']; ?>/sigco/solicitudes/operaciones/registro/guardar.php?Op='+Op,
            type: 'POST',
            async: true,
            dataType: "json",
            data: $('#form1').serialize(), //+'&0form1_idusuario=<?=$IdUsuario ?>&3form1_fechareg=<?=$Fecha ?>',
            success: function (datos) {
                OperMensaje(datos.res)
                $("#Mensajes").html(datos.res);
                $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                $("#Nuevo").dialog("close");
                $("#Modificar").dialog("close");
                $("#ConfirmaEliminacion").dialog("close");
                $("#ConfirmaRestauracion").dialog("close");
                if (datos.res == 1)
                {
                    if (confirm("Desea Imprimir la Solicitud de Conexion?"))
                        AbrirPopupImpresion(urldir+"sigco/solicitudes/operaciones/registro/imprimir.php?Id="+datos.nrosolicitud+"&codsuc=<?=$codsuc ?>", 800, 600)
                }
                Buscar(Op);
            }
        })
    }
    function Imprimir_Solicitud()
    {
        AbrirPopupImpresion(urldir+"sigco/solicitudes/operaciones/registro/imprimir.php?Id=<?=$Id ?>&codsuc=<?=$codsuc ?>", 800, 600)
    }
    function Cancelar()
    {
        location.href = 'index.php';
    }
    function cargar_zonas_abastecimiento()
    {

    }
    function cargar_rutas_lecturas()
    {

    }
    function  cargar_rutas_distribucion()
    {

    }
    function PintarGrafico(Obj)
    {
        var color = "rgb(153, 153, 153)";
        if (document.all) {
            color = "#999999";
        }
        if (Obj.style.backgroundColor != color)
        {
            if (IdAnt != '')
            {
                Limpiar(IdAnt)
            } else {
                for (i = 1; i <= 16; i++)
                {
                    document.getElementById('Div_'+i).style.backgroundColor = "#cccccc"
                }
            }

            IdAnt = Obj.id
            IdGrafico = Obj.id
            Obj.style.backgroundColor = "#FF0000";
        }
    }
    function Limpiar(Obj)
    {
        document.getElementById(Obj).style.backgroundColor = "#cccccc"
    }
    function AceptarGrafico()
    {
        $.ajax({
            url: "proceso.php",
            type: 'POST',
            async: true,
            data: 'idgrafico='+IdGrafico,
            success: function (datos) {
            }
        })
        close_frmpop()
    }

    function cRepresentante()
    {
        if ($('input[name="crepresentante"]:checked').val() == 1)
        {
            $("#representante").val($("#Cliente").val())
            $("#tipodocumentorep").val($("#tipodocumento").val())
            $("#nrodocumentorep").val($("#nrodocumento").val())
            $("#sectorrep").val($("#sectorsol").val())
            $("#callerep").val($("#callesol").val())
            $("#nrocallerep").val($("#nrocalle").val())
            $("#manzanarep").val($("#manzanasol").val())
            $("#loterep").val($("#lotesol").val())
        }
        else
        {
            $("#representante").val('')
            $("#tipodocumentorep").val(0)
            $("#nrodocumentorep").val('')
            $("#sectorrep").val('')
            $("#callerep").val('')
            $("#nrocallerep").val('')
            $("#manzanarep").val('')
            $("#loterep").val('')
        }

    }

    function req(Id)
    {
        var ver = $("#chk_"+Id).val()
        if (ver == 0)
        {
            $("#chk_"+Id).val(1)
            $("#chk_"+Id).attr('checked', true);
        } else
        {
            $("#chk_"+Id).val(0)
            $("#chk_"+Id).attr('checked', false);
        }



    }
    function buscar_calle()
    {
        object = "calles"
        var codzona = $("#codzona").val()
        var codsector = $("#sector").val()
        if (codzona == 0)
        {
            alert('Seleccione una Zona');
            return false;
        }

        if (codsector == 0)
        {
            alert('Seleccione un Sector');
            return false;
        }
        AbrirPopupBusqueda("../../../../buscar/calles.php?codzona="+codzona+"&codsector="+codsector+"&codsuc="+codsuc, 700, 450)
    }

    function recibir_calles(Id, Des)
    {
        $("#calles").val(Id)
        $("#calles").prop('selected', true)
        $("#callesol").val(Des)
    }
    
    function ValidarDoc()
    {
        var idtipodoc= $("#tipodocumento").val();
        if(idtipodoc=='' || idtipodoc== 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj("#tipodocumento", 'Seleccione el Tipo de Documento')
            return false;
        }
        else
            {
                var nrodoc  = $("#nrodocumento").val();
                var cantidad= nrodoc.length;
                switch (nrodoc) {
                    case '1':
                        var cantidadmax = 8;
                        //alert(cantidad);
                        if(cantidad == cantidadmax)
                        { 
							$("#nrodocumento").css( "background-color", "grey" );
						}
                        else
						{
							$("#nrodocumento").attr("css", { backgroundColor: "red" });
						}
                        break; 
                    case '2':
                        cantidadmax = 20;
                        break;
                    case '3':
                        cantidadmax= 11;
                        break;
                }
            }
    }
    
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>  
                <tr>
                    <td colspan="8" class="TitDetalle">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Información General</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Solicitante</a></li>
                                <li style="font-size:10px"><a href="#tabs-3">Representante</a></li>
                                <li style="font-size:10px"><a href="#tabs-4">Requisitos</a></li>
                            </ul>
                            <div id="tabs-1">
                                <table width="860" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="TitDetalle" align="right">Nro. Solicitud</td>
                                        <td width="30" align="center">:</td>
                                        <td colspan="3" class="CampoDetalle">
                                            <input name="nrosolicitud" type="text" id="Id" size="4" maxlength="2" value="<?php echo $Id; ?>" class="inputtext"/>
                                        </td>
                                        <td class="TitDetalle" align="right">Fecha Emision</td>
                                        <td width="30" align="center">:</td>
                                        <td class="CampoDetalle">                                            
                                            <input name="fechaemision" id="fechaemision" type="text" maxlength="10" class="inputtext" value="<?=$fecha ?>" style="width:85px;"/>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td class="TitDetalle" align="right">Nro. Estudio </td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle" colspan="6">
                                            <input name="nroinspeccion" type="text" id="nroinspeccion" size="10" maxlength="10" value="<?=$solicitudes["nroinspeccion"] ?>" readonly="readonly" class="inputtext" />
                                            <span class="MljSoft-icon-buscar" title="Buscar Estudio de Factibilidad" onclick="buscar_servicio();"></span>
                                            Con Estudio de Factibilidad 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Cliente</td>
                                        <td align="center">:</td>
                                        <td colspan="6" class="CampoDetalle">
                                            <input type="text" name="codcliente" id="codcliente" size="10" class="inputtext" readonly="readonly" value="<?=$solicitudes["codcliente"] ?>"/>
                                            <!-- <span class="MljSoft-icon-buscar" title="Buscar Personas" onclick="buscar_usuarios();"></span> -->
                                            <input class="inputtext" name="propietario" type="text" id="Cliente" size="50" maxlength="200" value="<?=$solicitudes["propietario"] ?>" placeholder="Ingresar datos de cliente" onkeypress="CambiarFoco(event, 'sectorsol')" />
                                            <input type="hidden" value="<?=$solicitudes["nroinscripcion"] ?>" id="nroinscripcion" name="nroinscripcion">
                                            <input type="hidden"  id="correo" name="correo" value="<?=$solicitudes["correo"] ?>" />
                                            <?php if ($Op != 0) { ?>
                                                <input type="button" class="button" name="ImprimirSolicitud" id="ImprimirSolicitud" value="Imprimir Solicitud" onclick="Imprimir_Solicitud();" />
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">
                                            <fieldset class="fieldset">
                                                <legend class="legend ui-state-default ui-corner-all">DATOS DEL PREDIO</legend>
                                                <table width="810" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tbody>             
                                                        <tr>
                                                            <td width="112" class="TitDetalle" align="right">Zona</td>
                                                            <td width="30" align="center">:</td>
                                                            <td width="223" class="CampoDetalle">
                                                                <?php
                                                                echo $objMantenimiento->drop_zonas($codsuc, $solicitudes["codzona"], "onchange='cargar_sectores(".$codsuc.",this.value,0,0)';");
                                                                ?>
                                                            </td>
                                                            <td width="147" align="right" class="TitDetalle">Sector</td>
                                                            <td width="30" align="center" >:</td>
                                                            <td class="CampoDetalle">
                                                                <div id="div_sector">
                                                                    <select name="sector" id="sector" style="width:200px" class="select">
                                                                        <option value="0">.:: Seleccione el Sector ::.</option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td width="38" align="center" class="TitDetalle">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">Manzana</td>
                                                            <td align="center">:</td>
                                                            <td>
                                                                <div id="div_manzanas">
                                                                    <select name="manzanas" id="manzanas" style="width:200px" class="select">
                                                                        <option value="0">.:: Seleccione la Manzana ::.</option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="TitDetalle" align="right">Calle</td>
                                                            <td align="center">:</td>
                                                            <td class="CampoDetalle">
                                                                <div id="div_calles">
                                                                    <select id="calles" name="calles" style="width:200px" class="select">
                                                                        <option value="0">.:: Seleccione la Calle ::.</option>
                                                                    </select>                                                                    
                                                                </div>                                                                
                                                            </td>
                                                            <td>
                                                                <span style="position:relative" class="MljSoft-icon-buscar" title="Buscar Calle" onclick="buscar_calle();"></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="CampoDetalle" align="right">Lote</td>
                                                            <td align="center">:</td>
                                                            <td class="CampoDetalle">
                                                                <input type="text" name="lote" id="lote" class="inputtext" size="10" value="<?=$solicitudes["lote"] ?>" placeholder="Lote" onkeypress="CambiarFoco(event, 'area')" />
                                                            </td>
                                                            <td class="CampoDetalle" align="right">Area del Inmueble </td>
                                                            <td align="center">:</td>
                                                            <td width="226" class="CampoDetalle">
                                                                <input type="text" name="area" id="area" class="inputtext" size="10" value="<?=$solicitudes["area"] ?>" placeholder="Area en m2" onkeypress="CambiarFoco(event, 'nrocalle')" />
                                                                m<sup>2</sup>
                                                            </td>
                                                            <td class="TitDetalle" align="center">&nbsp;</td>
                                                        </tr>
                                                        <tr>                    
                                                            <td width="112" class="CampoDetalle" align="right">Nro. calle</td>
                                                            <td align="center">:</td>
                                                            <td class="CampoDetalle">
                                                                <input type="text" name="nrocallepred" id="nrocallepred" class="inputtext" size="10" value="<?=$solicitudes["nrocallepred"] ?>" placeholder="Nro. Calle"  onkeypress="CambiarFoco(event, 'nrodocumento');" />
                                                            </td>

                                                            <td class="TitDetalle" align="right">N&ordm; Partida Regustral</td>
                                                            <td align="center">:</td>
                                                            <td class="CampoDetalle">
                                                                <input type="text" name="nropartidaregistral" id="nropartidaregistral" class="inputtext"  value="<?=$solicitudes["nropartidaregistral"] ?>" onkeypress="CambiarFoco(event, 'nrodocumento');"/>
                                                            </td>
                                                            <td class="TitDetalle" align="center">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="CampoDetalle" align="right">Tipo de Servicio</td>
                                                            <td align="center">:</td>
                                                            <td class="CampoDetalle">
                                                                <?php $objMantenimiento->drop_tiposervicio($solicitudes["codtiposervicio"]); ?>
                                                            </td>
                                                            <td class="TitDetalle" align="right">Departamento</td>
                                                            <td align="center">:</td>
                                                            <td class="CampoDetalle">
                                                                <?php
                                                                $objMantenimiento->drop_ubigeo("departamentopred", "--Seleccione el Departamento--", " where codubigeo like ? order by codubigeo", array("%0000"), "onchange='cargar_provinciapred(this.value);'", substr($IdUbigeoPred, 0, 2)."0000");
                                                                ?>
                                                            </td>
                                                            <td class="TitDetalle" align="center">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="CampoDetalle" align="right">Provincia</td>
                                                            <td align="center">:</td>
                                                            <td class="CampoDetalle" >
                                                                <div id="div_provinciapred">
                                                                    <select id="provinciapred" name="provinciapred" style="width:220px" class="select">
                                                                        <option value="0">.:: Seleccione la Provincia ::.</option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="TitDetalle" align="right">Distrito</td>
                                                            <td align="center">:</td>
                                                            <td class="CampoDetalle">
                                                                <div id="div_distritopred">
                                                                    <select id="distritopred" name="distritopred" style="width:220px" class="select">
                                                                        <option value="0">.:: Seleccione un Distrito ::.</option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="TitDetalle" align="center">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="top" class="TitDetalle">Referencia</td>
                                                            <td align="center" valign="top">:</td>
                                                            <td colspan="7" class="CampoDetalle"><textarea name="Referencia" id="Referencia" class="inputtext" cols="80" rows="4" style="font-size:12px"><?=$solicitudes["referencia"] ?>
                                                                </textarea></td>
                                                        </tr>  
                                                        <tr>
                                                            <td class="TitDetalle" align="right">&nbsp;</td>
                                                            <td class="TitDetalle">&nbsp;</td>
                                                            <td colspan="6" class="CampoDetalle">&nbsp;</td>
                                                        </tr>                              
                                                        <tr>
                                                            <td class="TitDetalle">&nbsp;</td>
                                                            <td class="TitDetalle">&nbsp;</td>
                                                            <td colspan="7" class="CampoDetalle">
                                                                <label>
                                                                    <input type="button" class="button" name="Generar" id="Generar" value="Generar Grafico Referencial" onclick="open_registro();" />
                                                                    <input type="hidden" name="grafico" id="grafico" value="<?=$solicitudes["grafico"] ?>" />

                                                                </label>
                                                            </td>
                                                        </tr>     
                                                    </tbody>        
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="right">Manzana</td>
                                        <td width="30" align="center">:</td>
                                        <td class="CampoDetalle">
                                            <input type="text" name="manzanasol" id="manzanasol" value="<?=$solicitudes["manzanasol"] ?>" onkeypress="CambiarFoco(event, 'callesol')" class="inputtext" style="width:220px" />
                                        </td>
                                        <td class="TitDetalle" align="right">Calle</td>
                                        <td width="30" align="center">:</td>
                                        <td width="220" class="CampoDetalle">
                                            <input type="text" name="callesol" id="callesol" value="<?=$solicitudes["callesol"] ?>" onkeypress="CambiarFoco(event, 'lotesol')" class="inputtext" style="width:220px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="CampoDetalle" align="right">Lote</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <input type="text" name="lotesol" id="lotesol" class="inputtext" size="10" value="<?=$solicitudes["lotesol"] ?>" onkeypress="CambiarFoco(event, 'nrocalle')" />
                                        </td>
                                        <td width="117" class="CampoDetalle" align="right">Nro. calle</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle" colspan="3">
                                            <input type="text" name="nrocalle" id="nrocalle" class="inputtext" size="10" value="<?=$solicitudes["nrocalle"] ?>" />
                                        </td>                    
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Documento</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <?php $objMantenimiento->drop_tipodocumento($solicitudes["codtipodocumento"]); ?>
                                        </td>
                                        <td class="CampoDetalle" align="right">Nro. Doc. </td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle" colspan="3">
                                            <input type="text" name="nrodocumento" id="nrodocumento" class="inputtext"  size="12" value="<?=$solicitudes["nrodocumento"] ?>" onkeyup="ValidarDoc()" />
                                        </td>
                                    </tr>                
                                    <tr>
                                        <td class="TitDetalle" align="right">Departamento</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <?php
                                                $objMantenimiento->drop_ubigeo("departamento", ".:: Seleccione el Departamento ::.", " where codubigeo like ? order by codubigeo", array("%0000"), "onchange='cargar_provincia(this.value);'", substr($IdUbigeo, 0, 2)."0000");
                                            ?>
                                        </td>
                                        <td class="CampoDetalle" align="right">Provincia</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle" colspan="3">
                                            <div id="div_provincia">
                                                <select id="provincia" name="provincia" style="width:220px" class="select">
                                                    <option value="0">.:: Seleccione la Provincia ::.</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Distrito</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <div id="div_distrito">
                                                <select id="distrito" name="distrito" style="width:220px" class="select">
                                                    <option value="0">.:: Seleccione un Distrito ::.</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td class="CampoDetalle" align="right">&nbsp;</td>
                                        <td class="CampoDetalle" align="center">&nbsp;</td>
                                        <td class="CampoDetalle" colspan="3">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-3">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="TitDetalle" align="right">Solicitante ? </td>
                                        <td width="30" align="center">:</td>
                                        <td colspan="3" class="CampoDetalle">                        
                                            <div id="DivServicio" style="display: inline;">
                                                <input type="radio" id="representante1" name="crepresentante" value='1' onchange="cRepresentante()" checked="checked"/>
                                                <label for="representante1">SI</label>
                                                <input type="radio" id="representante0" name="crepresentante" value='0'  onchange="cRepresentante()" />
                                                <label for="representante0">NO</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Representante</td>
                                        <td align="center">:</td>
                                        <td colspan="6" class="CampoDetalle">                       
                                            <input class="inputtext" name="representante" type="text" id="representante" size="75" maxlength="200"value="<?=$solicitudes["representante"] ?>" onkeypress="CambiarFoco(event, 'area')"   />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Manzana</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <input type="text" name="manzanarep" id="manzanarep" value="<?=$solicitudes["manzanarep"] ?>" onkeypress="CambiarFoco(event, 'area')" class="inputtext" style="width:220px" />
                                        </td>
                                        <td class="TitDetalle" align="right">Calle</td>
                                        <td width="30" align="center">:</td>
                                        <td width="220" class="CampoDetalle">
                                            <input type="text" name="callerep" id="callerep" value="<?=$solicitudes["callerep"] ?>" onkeypress="CambiarFoco(event, 'area')" class="inputtext" style="width:220px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="CampoDetalle" align="right">Lote</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <input type="text" name="loterep" id="loterep" class="inputtext" size="10" value="<?=$solicitudes["lotesol"] ?>" onkeypress="CambiarFoco(event, 'area')" />
                                        </td>
                                        <td width="117" class="CampoDetalle" align="right">Nro. calle</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle" colspan="3">
                                            <input type="text" name="nrocallerep" id="nrocallerep" class="inputtext" size="10" value="<?=$solicitudes["nrocallerep"] ?>" onkeypress="CambiarFoco(event, 'nrodocumento');" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Documento</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <?php $objMantenimiento->drop_tipodocumento($solicitudes["codtipodocumento"], '', "tipodocumentorep"); ?>
                                        </td>
                                        <td class="CampoDetalle" align="right">Nro. Doc. </td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle" colspan="3">
                                            <input type="text" name="nrodocumentorep" id="nrodocumentorep" class="inputtext"  size="10" value="<?=$solicitudes["nrodocumentorep"] ?>"/>
                                        </td>
                                    </tr>                
                                    <tr>
                                        <td class="TitDetalle" align="right">Departamento</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <?php
                                                $objMantenimiento->drop_ubigeo("departamentorep", ".:: Seleccione el Departamento--", " where codubigeo like ? order by codubigeo", array("%0000"), "onchange='cargar_provinciarep(this.value);'", substr($IdUbigeoRep, 0, 2)."0000");
                                            ?>
                                        </td>
                                        <td class="CampoDetalle" align="right">Provincia</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle" colspan="3">
                                            <div id="div_provinciarep">
                                                <select id="provinciarep" name="provinciarep" style="width:220px" class="select">
                                                    <option value="0">.:: Seleccione la Provincia ::.</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle" align="right">Distrito</td>
                                        <td align="center">:</td>
                                        <td class="CampoDetalle">
                                            <div id="div_distritorep">
                                                <select id="distritorep" name="distritorep" style="width:220px" class="select">
                                                    <option value="0">.:: Seleccione un Distrito ::.</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td class="CampoDetalle" align="right">&nbsp;</td>
                                        <td class="CampoDetalle" align="center">&nbsp;</td>
                                        <td class="CampoDetalle" colspan="3">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>

                            <div id="tabs-4" style="height:370px;">
                                <br />
                                <table>
                                    <?php
                                    if ($Id == '') {
                                        $req = "SELECT codrequisitos, descripcion FROM solicitudes.requisitos WHERE estareg=1 ";
                                        $conn = $conexion->prepare($req);
                                        $conn->execute(array());
                                        $itemsD = $conn->fetchAll();
                                        foreach ($itemsD as $rowD) {
                                            ?>
                                            <tr>
                                                <td align="center">
                                                    <input type="checkbox" id="chk_<?php echo $rowD['codrequisitos']; ?>" name="chk_<?php echo $rowD['codrequisitos']; ?>" onclick="req(<?php echo $rowD['codrequisitos']; ?>);" value="0">
                                                </td>
                                                <td style="text-align: justify;">
                                                    <?php echo $rowD['descripcion']."<br />"; ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        $req = "SELECT codrequisitos, descripcion FROM solicitudes.requisitos WHERE estareg=1 ";
                                        $conn = $conexion->prepare($req);
                                        $conn->execute(array());
                                        $itemsD = $conn->fetchAll();
                                        foreach ($itemsD as $rowD) {
                                            $codreq = $rowD['codrequisitos'];
                                            $ver = "SELECT COUNT(codrequisitos) FROM solicitudes.detsolicitud_requisito
                                                    WHERE nrosolicitud=? and codrequisitos=? ";
                                            $conex = $conexion->prepare($ver);
                                            $conex->execute(array($Id, $codreq));
                                            $conex = $conex->fetch();
                                            if ($conex[0] == 1) {
                                                ?>  
                                                <tr>
                                                    <td align="center">
                                                        <input type="checkbox" id="chk_<?php echo $rowD['codrequisitos']; ?>" name="chk_<?php echo $rowD['codrequisitos']; ?>" onclick="req(<?php echo $rowD['codrequisitos']; ?>);" value="1" checked="checked">
                                                    </td>
                                                    <td style="text-align: justify;font-weight: bold;">
                                                        <?php echo $rowD['descripcion']."<br />"; ?>  
                                                    </td>
                                                </tr>    
                                                        <?php
                                                    } else {
                                                        ?>  
                                                <tr>
                                                    <td align="center">
                                                        <input type="checkbox" id="chk_<?php echo $rowD['codrequisitos']; ?>" name="chk_<?php echo $rowD['codrequisitos']; ?>" onclick="req(<?php echo $rowD['codrequisitos']; ?>);" value="0">
                                                    </td>
                                                    <td style="text-align: justify;">
                                                        <?php echo $rowD['descripcion']."<br />"; ?>  
                                                    </td>
                                                </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>

                                </table>
                            </div>

                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </form>
</div>
<div id="DivGrafico" title="Seleccionar Grafico Referencial"  >
    <div id="div_grafico"></div>
</div>
<script type="text/javascript">
    $("#Nombre").focus();

    function open_registro()
    {
        if ($("#calles").val() == 0)
        {
            Msj($("#calles"), "Seleccione la Calle")
            return
        }
        if ($("#nrocallepred").val() == "")
        {
            Msj($("#nrocallepred"), "Digite el Nro. de la Calle")
            return
        }
        generarGrafico();
        $("#DivGrafico").dialog("open");
    }
    function generarGrafico()
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir']; ?>/sigco/solicitudes/operaciones/registro/grafico.php',
            type: 'POST',
            async: true,
            data: 'calle='+$("#calles option:selected").text()+"&nrocalle="+$("#nrocalle").val()+"&grafico="+$("#grafico").val(),
            success: function (datos) {
                $("#div_grafico").html(datos)
            }
        })
    }
</script>
<?php
if ($Id != "") {
    ?>
    <script>

    </script>
    <?php
}
?>