<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../objetos/clsFunciones.php');

	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp              = 1;
	$codsuc              = $_SESSION['IdSucursal'];
	$solreferencia       = $_POST["nrosolicitud"];
	$codcliente          = $_POST["codcliente"];
	$propietario         = strtoupper($_POST["propietario"]);
	$codzona             = $_POST["codzona"];
	$sector              = $_POST["codsector"];
	$area                = $_POST["area"];
	$calles              = $_POST["codcalle"];
	$nrocalle            = $_POST["nrocalle"];
	$tipodocumento       = $_POST["coddocumento"];
	$nrodocumento        = $_POST["nrodocumento"];
	$fechaemision        = $objFunciones->CodFecha($_POST["fechaemision"]);
	$tiposervicio        = $_POST["codtiposervicio"];
	$distrito            = $_POST["codubigeo"];
	$Referencia          = $_POST["Referencia"];
	$grafico             = $_POST["grafico"];
	$idusuario           = $_SESSION['id_user'];
	$estareg             =1;
	
	$representante       = $_POST["representante"];
	$sectorrep           = $_POST["sectorrep"]; 
	$manzanarep          = $_POST["manzanarep"]; 
	$callerep            = $_POST["callerep"];
	$loterep             = $_POST["loterep"];
	$nrocallerep         = $_POST["nrocallerep"];
	$tipodocumentorep    = $_POST["codtipodocumentorep"];
	$nrodocumentorep     = $_POST["nrodocumentorep"];
	$distritorep         = $_POST["codubigeorep"];
	$lotesol             = $_POST["lotesol"];
	$callesol            = $_POST["callesol"];
	$manzanasol          = $_POST["manzanasol"];
	$sectorsol           = $_POST["sectorsol"];
	$manzanas            = $_POST["codmanzanas"];
	$lote            = $_POST["lote"];
	$nrocallepred        = $_POST["nrocallepred"];
	$distritopred        = $_POST["codubigeopred"];
	
	$nropartidaregistral = $_POST["nropartidaregistral"];
	$nroinspeccion       = $_POST["nroinspeccion"]?$_POST["nroinspeccion"]:0;
	$nroinscripcion      = $_POST["nroinscripcion"]?$_POST["nroinscripcion"]:0;
	$nrosolicitud   	= $objFunciones->setCorrelativosVarios(15, $codsuc, "SELECT", 0);

	$conexion->beginTransaction();
	$sql  = "INSERT INTO solicitudes.solicitudes(codemp,codsuc,nrosolicitud,codcliente,propietario,codcalle,nrocalle,fechaemision,codtipodocumento,nrodocumento,";
	$sql .= "codtiposervicio,grafico,codubigeo,referencia,codusu,codsector,area,solreferencia,estareg,representante, sectorrep, ";
    $sql .= "manzanarep, callerep, loterep, nrocallerep, codtipodocumentorep, nrodocumentorep, codubigeorep, lotesol, callesol, manzanasol, ";
    $sql .= "sectorsol, codmanzanas, lote, nrocallepred, codubigeopred,nropartidaregistral,codzona,nroinspeccion,nroinscripcion) values(:codemp,:codsuc,:nrosolicitud,:codcliente,:propietario,:calles,:nrocalle,"; 
	$sql .= ":fechaemision,:tipodocumento,:nrodocumento,:tiposervicio,:grafico,:distrito,:Referencia,:idusuario,:sector,:area,:solreferencia,:estareg,:representante,:sectorrep, :manzanarep, :callerep,";
	$sql .= ":loterep,:nrocallerep,:tipodocumentorep,:nrodocumentorep,:distritorep,:lotesol,:callesol,:manzanasol,:sectorsol,:manzanas,";
    $sql .= ":lote,:nrocallepred,:distritopred,:nropartidaregistral,:codzona,:nroinspeccion,:nroinscripcion) ";

	$result = $conexion->prepare($sql);
	$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":nrosolicitud"=>$nrosolicitud,":codcliente"=>$codcliente,":propietario"=>$propietario,
		   ":calles"=>$calles,":nrocalle"=>$nrocalle,":fechaemision"=>$fechaemision,":tipodocumento"=>$tipodocumento,":nrodocumento"=>$nrodocumento,
		   ":tiposervicio"=>$tiposervicio,":grafico"=>$grafico,":distrito"=>$distrito,":Referencia"=>$Referencia,":idusuario"=>$idusuario,
		   ":sector"=>$sector,":area"=>$area,":solreferencia"=>$solreferencia,":estareg"=>$estareg,":representante"=>$representante,":sectorrep"=>$sectorrep,":manzanarep"=>$manzanarep, 
           ":callerep"=>$callerep,":loterep"=>$loterep,":nrocallerep"=>$nrocallerep,":tipodocumentorep"=>$tipodocumentorep,":nrodocumentorep"=>$nrodocumentorep,
           ":distritorep"=>$distritorep,":lotesol"=>$lotesol,":callesol"=>$callesol,":manzanasol"=>$manzanasol,":sectorsol"=>$sectorsol,":manzanas"=>$manzanas,
           ":lote"=>$lote,":nrocallepred"=>$nrocallepred,":distritopred"=>$distritopred,":nropartidaregistral"=>$nropartidaregistral,":codzona"=>$codzona,
           ":nroinspeccion"=>$nroinspeccion,":nroinscripcion"=>$nroinscripcion));

	if ($result->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }

	$sql  = "update solicitudes.solicitudes set solreferencia=:solreferencia
				where codemp=:codemp and codsuc=:codsuc and nrosolicitud=:nrosolicitud";
	$result = $conexion->prepare($sql);
	$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":nrosolicitud"=>$solreferencia,":solreferencia"=>$nrosolicitud));
	if ($result->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }

	$conexion->commit();
	$objFunciones->setCorrelativosVarios(15, $codsuc, "UPDATE", $nrosolicitud);
	$mensaje = "El Registro se ha Grabado Correctamente";
	echo $res=1;
	
?>
