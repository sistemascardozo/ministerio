<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsDrop.php");

	$Op 		= 1;
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc 	= $_POST['codsuc'];
	$guardar	= "Op=".$Op;
	
	$objMantenimiento = new clsDrop();
	$fecha = date("d/m/Y");
	
	$sql = "SELECT s.nrosolicitud,s.codcliente,s.propietario,tc.descripcioncorta,c.descripcion as calle,s.fechaemision,
			td.descripcion as documento,s.nrodocumento,s.codubigeo,s.estareg,s.codtiposervicio,s.solreferencia,s.codsector,s.codcalle,s.area,s.nrocalle,
			tdr.descripcion as documentorep,s.codtipodocumento,s.grafico,s.referencia,es.descripcion as estadosolicitud,s.representante, s.sectorrep, 
            s.manzanarep, s.callerep, s.loterep, s.nrocallerep, s.codtipodocumentorep, 
            s.nrodocumentorep, s.codubigeorep, s.lotesol, s.callesol, s.manzanasol,
            sc.descripcion AS sector,ma.descripcion AS manzana,s.codzona, z.descripcion AS zona,
                (select descripcion from public.ubigeo where codubigeo=".$objMantenimiento->SubString('s.codubigeo',1,2)." || '0000') as departamento,
		(select descripcion from public.ubigeo where codubigeo=".$objMantenimiento->SubString('s.codubigeo',1,4)." || '00') as provincia,
		(select descripcion from public.ubigeo where codubigeo=s.codubigeo) as distrito,
                (select descripcion from public.ubigeo where codubigeo=".$objMantenimiento->SubString('s.codubigeorep',1,2)." || '0000') as departamentorep,
		(select descripcion from public.ubigeo where codubigeo=".$objMantenimiento->SubString('s.codubigeorep',1,4)." || '00') as provinciarep,
		(select descripcion from public.ubigeo where codubigeo=s.codubigeorep) as distritorep,
                (select descripcion from public.ubigeo where codubigeo=".$objMantenimiento->SubString('s.codubigeopred',1,2)." || '0000') as departamentopred,
		(select descripcion from public.ubigeo where codubigeo=".$objMantenimiento->SubString('s.codubigeopred',1,4)." || '00') as provinciapred,
                (select descripcion from public.ubigeo where codubigeo=s.codubigeopred) as distritopred,
            s.sectorsol, s.codmanzanas, s.lote, s.nrocallepred, s.codubigeopred,s.nropartidaregistral,s.nroinspeccion,s.nroinscripcion
			FROM solicitudes.solicitudes as s			
			inner join public.calles as c on(s.codcalle=c.codcalle and s.codemp=c.codemp and s.codsuc=c.codsuc AND s.codzona=c.codzona)
            INNER JOIN public.sectores as sc on(s.codemp=sc.codemp and s.codsuc=sc.codsuc AND s.codsector= sc.codsector )
			INNER JOIN public.tiposcalle as tc on(c.codtipocalle=tc.codtipocalle)
			INNER JOIN public.tipodocumento as td on(s.codtipodocumento=td.codtipodocumento)
            INNER JOIN public.tipodocumento as tdr on(s.codtipodocumentorep=tdr.codtipodocumento)
			INNER JOIN public.estadosolicitud es on(s.estareg=es.idestadosolicitud)
            INNER JOIN public.manzanas AS ma ON (ma.codsector = sc.codsector AND ma.codemp = sc.codemp AND ma.codsuc = sc.codsuc AND ma.codmanzanas= s.codmanzanas)
			INNER JOIN admin.zonas AS z ON (z.codzona = s.codzona AND z.codemp= s.codemp AND z.codsuc= s.codsuc )
            WHERE s.codemp=1 and s.codsuc=? and s.nrosolicitud=?";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$Id));
	$solicitudes = $consulta->fetch();
	
	$codtiposervicio 	= 0;
	$tiposervicio		= 0;
	
	if($solicitudes["codtiposervicio"]==2)
	{
		$codtiposervicio 	= 3;
		$tiposervicio		= "DESAGUE";
	}
	if($solicitudes["codtiposervicio"]==3)
	{
		$codtiposervicio 	= 2;
		$tiposervicio		= "AGUA";
	}

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var IdAnt		= ''
	var IdGrafico	= ''
	var urldir		= '<?php echo $_SESSION['urldir'];?>';
	
	$(document).ready(function() {
         $( "#tabs" ).tabs();
	  $('form').keypress(function(e){   
		if(e == 13){
		  return false;
		}
	  });

	  $('input').keypress(function(e){
		if(e.which == 13){
		  return false;
		}
	  });

	});
	
	jQuery(function($)
	{ 
		$("#fechaemision").mask("99/99/9999");
	});
	
	$(document).ready(function(){
		
		//$( "#dialog:ui-dialog" ).dialog( "destroy" );

		$( "#DivGrafico" ).dialog({
			autoOpen: false,
			height: 350,
			width: 500,
			modal: true,
			show: "blind",
			hide: "scale",
			buttons: {
				"Cancelar": function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {

			}
		});
   })
	
	$(function() {
		$( "#fechaemision" ).datepicker(
				{
					showOn: 'button',
					direction: 'up',
					buttonImage: '<?php echo $_SESSION['urldir'];?>images/iconos/calendar.png',
					buttonImageOnly: true,
					showOn: 'both',
					showButtonPanel: true,

					beforeShow: function(input, inst) 
					{ 
						inst.dpDiv.css({marginTop: (input.offsetHeight ) - 20  + 'px', marginLeft: (input.offsetWidth) - 90 + 'px' }); 
					}
				}
			);
	});
	
	function ValidarForm()
	{
		return true;
	}
	
	function Cancelar()
	{
		location.href = '../index.php';
	}
</script>
<div align="center">

<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
        <tbody>  
        <tr>
            <td colspan="8" class="TitDetalle">
                <div id="tabs">
                    <ul>
                        <li style="font-size:10px"><a href="#tabs-1">Información General</a></li>
                        <li style="font-size:10px"><a href="#tabs-2">Solicitante</a></li>
                        <li style="font-size:10px"><a href="#tabs-3">Representante</a></li>
                    </ul>
                    <div id="tabs-1">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="TitDetalle" align="right">Nro. Solicitud</td>
                                <td align="center">:</td>
                                <td colspan="3" class="CampoDetalle">
                                    <input name="nrosolicitud" type="text" id="Id" size="4" maxlength="2" value="<? echo $Id; ?>" class="inputtext"/>
                                </td>
                                <td class="TitDetalle" align="right">Fecha Emision</td>
                                <td align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" value="14/12/2013" maxlentgth="6" class="inputtext hasDatepicker" id="fechaemision" name="fechaemision" style="width:80px;" /><img class="ui-datepicker-trigger" src="../../../../images/iconos/calendar.png" alt="..." title="..." />
                                </td>
                            </tr> 
                             <tr>
                                <td class="TitDetalle" align="right">Nro. Estudio </td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input name="nroinspeccion" type="text" id="nroinspeccion" size="10" maxlength="10" value="<?= $solicitudes["nroinspeccion"] ?>" readonly="readonly" class="inputtext" />
                                        
                                                Con Estudio de Factibilidad 
                                </td>
                            <tr>
                                <td class="TitDetalle" align="right">Cliente</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td colspan="4" class="CampoDetalle">
                                    <input type="text" name="codcliente" id="codcliente" size="10" class="inputtext" readonly="readonly" value="<?=$solicitudes["codcliente"]?>"/>
                                    <!--<span class="MljSoft-icon-buscar" title="Buscar Personas" onclick="buscar_usuarios();"></span>-->
                                    <input class="inputtext" name="propietario" type="text" id="Cliente" size="50" maxlength="200"value="<?=$solicitudes["propietario"]?>" onkeypress="CambiarFoco(event,'sectorsol')"   />
                                    <input type="hidden" value="<?=$solicitudes["nroinscripcion"]?>" id="nroinscripcion" name="nroinscripcion">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                     <fieldset>
                                        <legend class="ui-state-default ui-corner-all">DATOS DEL PREDIO</legend>
                                           <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                                <tbody>             
                                                    <tr>
                                                        <td class="TitDetalle" align="right">Zona</td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="zona" id="zona"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["zona"]?>" />
                                                            <input type="hidden" name="codzona" id="codzona" value="<?=$solicitudes["codzona"]?>" />
                                                        </td>
                                                        <td class="TitDetalle" align="right">Sector</td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="sector" id="sector"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["sector"]?>" />
                                                            <input type="hidden" name="codsector" id="codsector" value="<?=$solicitudes["codsector"]?>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">Manzana</td>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <input type="text" name="manzana" id="manzana"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["manzana"]?>" />
                                                            <input type="hidden" name="codmanzanas" id="codmanzanas" value="<?=$solicitudes["codmanzanas"]?>" />
                                                        </td>
                                                        <td class="TitDetalle" align="right">Calle</td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td width="220" class="CampoDetalle">
                                                            <input type="text" name="calle" id="calle"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["calle"]?>" />
                                                            <input type="hidden" name="codcalle" id="codcalle" value="<?=$solicitudes["codcalle"]?>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CampoDetalle" align="right">Lote</td>
                                                        <td class="CampoDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="lote" id="lote" class="inputtext" maxlentgth="50" size="10" value="<?=$solicitudes["lote"]?>" onkeypress="CambiarFoco(event,'area')" />
                                                        </td>
                                                        <td class="CampoDetalle" align="right">Area del Inmueble </td>
                                                        <td class="CampoDetalle" align="center">:</td>
                                                        <td width="220" class="CampoDetalle">
                                                            <input type="text" name="area" id="area" class="inputtext" maxlentgth="50" size="10" value="<?=$solicitudes["area"]?>" onkeypress="CambiarFoco(event,'nrocalle')" />
                                                            m<sup>2</sup>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="117" class="CampoDetalle" align="right">Nro. calle</td>
                                                        <td class="CampoDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="nrocallepred" id="nrocallepred" class="inputtext" maxlentgth="6" size="10" value="<?=$solicitudes["nrocallepred"]?>" onkeypress="CambiarFoco(event,'nrodocumento');" />
                                                        </td>
                                                        
                                                        <td class="TitDetalle" align="right">N&ordm; Partida Regustral</td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="nropartidaregistral" id="nropartidaregistral" class="inputtext"  value="<?=$solicitudes["nropartidaregistral"]?>" onkeypress="CambiarFoco(event,'nrodocumento');"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CampoDetalle" align="right">Tipo de Servicio</td>
                                                        <td class="CampoDetalle" align="center">:</td>
                                                        <td class="CampoDetalle" >
                                                            <input type="text" name="tiposervicio" id="tiposervicio"class="inputtext" style="width:220px" readonly="readonly" value="<?=$tiposervicio?>" />
                                                            <input type="hidden" name="codtiposervicio" id="codtiposervicio" value="<?=$codtiposervicio?>" />
                                                        </td>
                                                        <td class="TitDetalle" align="right">Departamento</td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["departamentopred"]?>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CampoDetalle" align="right">Provincia</td>
                                                        <td class="CampoDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["provinciapred"]?>" />
                                                        </td>
                                                        <td class="TitDetalle" align="right">Distrito</td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td class="CampoDetalle">
                                                            <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["distritopred"]?>" />
                                                            <input type="hidden" name="codubigeopred" id="codubigeopred" value="<?=$solicitudes["codubigeopred"]?>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TitDetalle" align="right">Referencia</td>
                                                        <td class="TitDetalle" align="center">:</td>
                                                        <td colspan="6" class="CampoDetalle">&nbsp;</td>
                                                    </tr>  
                                                    <tr>
                                                        <td class="TitDetalle" align="right">&nbsp;</td>
                                                        <td class="TitDetalle">&nbsp;</td>
                                                        <td colspan="6" class="CampoDetalle">
                                                            <label>
                                                                <textarea name="Referencia" id="Referencia" class="inputtext" cols="60" rows="5" style="font-size:12px"><?=$solicitudes["referencia"]?></textarea>
                                                            </label>
                                                        </td>
                                                    </tr>                              
                                                    <tr>
                                                        <td class="TitDetalle">&nbsp;</td>
                                                        <td class="TitDetalle">&nbsp;</td>
                                                        <td colspan="6" class="CampoDetalle">
                                                            <label>
                                                                <input type="button" class="button" name="Generar" id="Generar" value="Generar Grafico Referencial" onclick="open_registro();" />
                                                                <input type="hidden" name="grafico" id="grafico" value="<?=$solicitudes["grafico"]?>" />
                                                            </label>
                                                        </td>
                                                    </tr>                              
                                                       
                                                </tbody>        
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="tabs-2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="right">Manzana</td>
                                <td align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="manzanasol" id="manzanasol" value="<?=$solicitudes["manzanasol"]?>" onkeypress="CambiarFoco(event,'callesol')" class="inputtext" style="width:220px" readonly="readonly" />
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle" align="right">Calle</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td width="220" class="CampoDetalle">
                                    <input type="text" name="callesol" id="callesol" value="<?=$solicitudes["callesol"]?>" onkeypress="CambiarFoco(event,'lotesol')" class="inputtext" style="width:220px" readonly="readonly" />
                                </td>
                                <td class="CampoDetalle" align="right">Lote</td>
                                <td class="CampoDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="lotesol" id="lotesol" class="inputtext" maxlentgth="50" size="10" value="<?=$solicitudes["lotesol"]?>" onkeypress="CambiarFoco(event,'nrocalle')" readonly="readonly" />
                                </td>
                            </tr>
                            <tr>
                                <td width="117" class="CampoDetalle" align="right">Nro. calle</td>
                                <td class="CampoDetalle" align="center">:</td>
                                <td class="CampoDetalle" colspan="3">
                                    <input type="text" name="nrocalle" id="nrocalle" class="inputtext" maxlentgth="6" size="10" value="<?=$solicitudes["nrocalle"]?>" onkeypress="CambiarFoco(event,'nrodocumento');" readonly="readonly" />
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="TitDetalle" align="right">Documento</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="documento" id="documento" class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["documento"]?>" />
                                    <input type="hidden" name="coddocumento" id="coddocumento" value="<?=$solicitudes["codtipodocumento"]?>" readonly="readonly" />
                                </td>
                                <td class="CampoDetalle" align="right">Nro. Doc. </td>
                                <td width="16" class="CampoDetalle" align="center">:</td>
                                <td class="CampoDetalle" colspan="3">
                                    <input type="text" name="nrodocumento" id="nrodocumento" class="inputtext"  size="10" value="<?=$solicitudes["nrodocumento"]?>" onkeypress="CambiarFoco(event,'fechaemision');return permite(event,'num');" readonly="readonly" />
                                </td>
                            </tr>                
                            <tr>
                                <td class="TitDetalle" align="right">Departamento</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["departamento"]?>" />
                                </td>
                                <td class="CampoDetalle" align="right">Provincia</td>
                                <td class="CampoDetalle" align="center">:</td>
                                <td class="CampoDetalle" colspan="3">
                                    <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" value="<?=$solicitudes["provincia"]?>" readonly="readonly" />
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle" align="right">Distrito</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" value="<?=$solicitudes["distrito"]?>" readonly="readonly" />
                                    <input type="hidden" name="codubigeo" id="codubigeo" value="<?=$solicitudes["codubigeo"]?>" />
                                </td>
                                <td class="CampoDetalle" align="right">&nbsp;</td>
                                <td class="CampoDetalle" align="center">&nbsp;</td>
                                <td class="CampoDetalle" colspan="3">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <div id="tabs-3">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                             <tr>
                                <td class="TitDetalle" align="right">Representante</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td colspan="6" class="CampoDetalle">
                                    
                                    <input class="inputtext" name="representante" type="text" id="representante" size="75" maxlength="200"value="<?=$solicitudes["representante"]?>" readonly="readonly" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Manzana</td>
                                <td align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="manzanarep" id="manzanarep" value="<?=$solicitudes["manzanarep"]?>" class="inputtext" style="width:220px" readonly="readonly" />
                                </td>
                            </tr>
                                <tr>
                                <td class="TitDetalle" align="right">Calle</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td width="220" class="CampoDetalle">
                                    <input type="text" name="callerep" id="callerep" value="<?=$solicitudes["callerep"]?>" class="inputtext" style="width:220px" readonly="readonly" />
                                </td>
                                <td class="CampoDetalle" align="right">Lote</td>
                                <td class="CampoDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="loterep" id="loterep" class="inputtext" maxlentgth="50" size="10" value="<?=$solicitudes["lotesol"]?>" readonly="readonly" />
                                </td>
                            </tr>
                            <tr>
                                <td width="117" class="CampoDetalle" align="right">Nro. calle</td>
                                <td class="CampoDetalle" align="center">:</td>
                                <td class="CampoDetalle" colspan="3">
                                    <input type="text" name="nrocallerep" id="nrocallerep" class="inputtext" maxlentgth="6" size="10" value="<?=$solicitudes["nrocallerep"]?>" readonly="readonly" />
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="TitDetalle" align="right">Documento</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="documentorep" id="documentorep"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["documentorep"]?>" />
                                    <input type="hidden" name="codtipodocumentorep" id="codtipodocumentorep" value="<?=$solicitudes["codtipodocumentorep"]?>" />
                                </td>
                                <td class="CampoDetalle" align="right">Nro. Doc. </td>
                                <td width="16" class="CampoDetalle" align="center">:</td>
                                <td class="CampoDetalle" colspan="3">
                                    <input type="text" name="nrodocumentorep" id="nrodocumentorep" class="inputtext"  size="10" value="<?=$solicitudes["nrodocumentorep"]?>" readonly="readonly" />
                                </td>
                            </tr>                
                            <tr>
                                <td class="TitDetalle" align="right">Departamento</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["departamentorep"]?>" />
                                </td>
                                <td class="CampoDetalle" align="right">Provincia</td>
                                <td class="CampoDetalle" align="center">:</td>
                                <td class="CampoDetalle" colspan="3">
                                    <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["provinciarep"]?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle" align="right">Distrito</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                    <input type="text" name="distrito" id="distrito"class="inputtext" style="width:220px" readonly="readonly" value="<?=$solicitudes["distritorep"]?>" />
                                    <input type="hidden" name="codubigeorep" id="codubigeorep" value="<?=$solicitudes["codubigeorep"]?>" />
                                </td>
                                <td class="CampoDetalle" align="right">&nbsp;</td>
                                <td class="CampoDetalle" align="center">&nbsp;</td>
                                <td class="CampoDetalle" colspan="3">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
                      
                             
        </tbody>
    </table>
      
    
 </form>
</div>
<div id="DivGrafico" title="Seleccionar Grafico Referencial" >
    <div id="div_grafico"></div>
</div>
<script>  
	function open_registro()
	{
		generarGrafico();
		
		$("#DivGrafico").dialog("open");
	}
	
	function generarGrafico()
	{
		$.ajax({
			url:'grafico.php',
			type:'POST',
			async:true,
			data:'calle=' + $("#calles option:selected").text() + "&nrocalle=" + $("#nrocalle").val() + "&grafico=" + $("#grafico").val(),
			success:function(datos){		 	
				$("#div_grafico").html(datos)
			}
		}) 
	}
</script>