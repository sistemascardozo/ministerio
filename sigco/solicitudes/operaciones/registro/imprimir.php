<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsReporte.php");
	
    $clfunciones = new clsFunciones();

    class clsSolicitud extends clsReporte {

        function Header() {
            $this->SetFont('Arial', 'B', 10);

            $tit1 = "ANEXO 1";
            $tit2 = "Solicitud de Acceso a los Servicios de Saneamiento";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Cell(0, 5, utf8_decode($tit2), 0, 1, 'C');
            $this->ln(5);
        }

        function Footer() {

        }

        function contenido($id, $codsuc) {
            global $conexion, $clfunciones;

            $h = 4.2;
            $s = 2;

            $paramae = $this->getParamae("RESSOL", $codsuc);
            $empresa = $this->datos_empresa($codsuc);
            
            $sql = "SELECT 
                t.descripcion as servicio,
                s.propietario,
                s.nroinscripcion as nroinscripcion,
                s.representante,td.abreviado,
                s.nrodocumento,s.nrodocumentorep,
                s.fechaemision,tc.descripcioncorta,
                c.descripcion as calle,s.callesol,s.callerep,s.nrocalle,nrocallerep,s.nrocallepred,
                s.manzanasol,s.manzanarep,s.lotesol,s.loterep,s.lote,
                (select descripcion from public.ubigeo WHERE codubigeo=".$this->SubString('s.codubigeo', 1, 2)." || '0000') as departamento,
                (select descripcion from public.ubigeo WHERE codubigeo=".$this->SubString('s.codubigeo', 1, 4)." || '00') as provincia,
                (select descripcion from public.ubigeo WHERE codubigeo=s.codubigeo) as distrito,
                (select descripcion from public.ubigeo WHERE codubigeo=".$this->SubString('s.codubigeorep', 1, 2)." || '0000') as departamentorep,
                (select descripcion from public.ubigeo WHERE codubigeo=".$this->SubString('s.codubigeorep', 1, 4)." || '00') as provinciarep,
                (select descripcion from public.ubigeo WHERE codubigeo=s.codubigeorep) as distritorep,
                (select descripcion from public.ubigeo WHERE codubigeo=".$this->SubString('s.codubigeopred', 1, 2)." || '0000') as departamentopred,
                (select descripcion from public.ubigeo WHERE codubigeo=".$this->SubString('s.codubigeopred', 1, 4)." || '00') as provinciapred,
                (select descripcion from public.ubigeo WHERE codubigeo=s.codubigeopred) as distritopred,
                s.area,s.grafico,es.descripcion as estadosolicitud,s.referencia,t.codtiposervicio,
                ma.descripcion AS manzanapred, s.nropartidaregistral
                FROM solicitudes.solicitudes as s
                INNER JOIN public.tiposervicio as t on(s.codtiposervicio=t.codtiposervicio)
                INNER JOIN public.tipodocumento as td on(s.codtipodocumento=td.codtipodocumento)
                INNER JOIN public.calles as c on(s.codemp=c.codemp and s.codsuc=c.codsuc and s.codcalle=c.codcalle  and s.codzona=c.codzona )
                INNER JOIN public.tiposcalle as tc on(c.codtipocalle=tc.codtipocalle)
                INNER JOIN public.estadosolicitud es on(s.estareg=es.idestadosolicitud)
                LEFT JOIN public.manzanas AS ma ON (s.codemp = ma.codemp AND s.codsuc = ma.codsuc AND s.codsector = ma.codsector AND ma.codmanzanas = s.codmanzanas)
                WHERE s.codemp=1 and s.codsuc=? and s.nrosolicitud=?";
            $consulta = $conexion->prepare($sql);
            $consulta->execute(array($codsuc, $id));
            $solicitud = $consulta->fetch();
            $this->Rect(5, 25, 195, 265);
            //$this->SetFillColor(186, 186, 186); //COLOR DE FONDO
            $this->SetX(120);
            $this->SetFont('Arial', '', 7);
            $this->Cell(25, $h, "Lugar", 1, 0, 'L');
            switch ($codsuc) {
                case 6:
                    $lugar = "SICUANI"; break;
                case 2:
                    $lugar="---"; break;
                case 3:
                    $lugar="--"; break;
            }
            $this->Cell(0, $h, $lugar, 1, 1, 'L');

            $this->SetX(120);
            $this->Cell(25, $h, "Fecha", 1, 0, 'L');
            $this->Cell(0, $h, $this->DecFecha($solicitud["fechaemision"]), 1, 1, 'L');

            $this->SetX(120);
            $numeroinscripcion = $clfunciones->CodUsuario($codsuc,$solicitud["nroinscripcion"]);
            $this->Cell(25, $h, "EPS", 1, 0, 'L');
            $this->Cell(0, $h, strtoupper($empresa["razonsocial"]), 1, 1, 'L');
            $this->SetX(5);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(115, $h, "I. DATOS DEL SOLICITANTE", 0, 0, 'L');
            $this->SetFont('Arial', '', 7);
            $this->Cell(25, $h,utf8_decode("N° INSCRIPCION"), 1,0, 'C');
            $this->Cell(0, $h, $numeroinscripcion, 0, 1, 'L');
            $this->SetX(5);
            
            $this->Cell(115, $h, utf8_decode(strtoupper($solicitud["propietario"])), 1, 0, 'L');
            $this->Cell(25, $h, $solicitud["abreviado"]." - ".$solicitud["nrodocumento"], 1, 0, 'L');
            $this->Cell(0, $h,$solicitud["nropartidaregistral"], 1, 1, 'C');

            $this->SetX(5);
            $this->Cell(115, $h, "Nombre", 1, 0, 'C');
            $this->Cell(25, $h, "DNI", 1, 0, 'C');
            $this->SetFont('Arial', '', 6);
            $this->Cell(0, $h, utf8_decode('N° de Partida registral de inscripción del predio (si aplica)'), 1, 1, 'C');
            $this->SetFont('Arial', '', 7);
            $this->SetX(5);

            $this->Cell(115, $h, strtoupper($solicitud["callesol"]), 1, 0, 'L');
            $this->Cell(25, $h, $solicitud["nrocalle"], 1, 0, 'C');
            $this->Cell(25, $h, strtoupper($solicitud["manzanasol"]), 1, 0, 'C');
            $this->Cell(0, $h, strtoupper($solicitud["lotesol"]), 1, 1, 'C');

            $this->SetX(5);
            $this->Cell(115, $h, 'domicilio (Calle, Jiron, Avenida)', 1, 0, 'C');
            $this->Cell(25, $h, utf8_decode('N°'), 1, 0, 'C');
            $this->Cell(25, $h, 'Mz.', 1, 0, 'C');
            $this->Cell(0, $h, 'Lt.', 1, 1, 'C');

            $this->SetX(5);
            $this->Cell(75, $h, '', 1, 0, 'L');
            $this->Cell(40, $h, strtoupper($solicitud["provincia"]), 1, 0, 'L');
            $this->Cell(0, $h, strtoupper($solicitud["distrito"]), 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(75, $h, utf8_decode('(Urbanización, barrio)'), 1, 0, 'C');
            $this->Cell(40, $h, 'Provincia', 1, 0, 'C');
            $this->Cell(0, $h, 'Distrito', 1, 1, 'C');

            $this->SetX(5);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(0, $h, "II. DATOS DEL REPRESENTANTE", 0, 1, 'L');
            $this->SetFont('Arial', '', 7);

            $this->SetX(5);
            $this->Cell(115, $h, utf8_decode(strtoupper($solicitud["representante"])), 1, 0, 'L');
            $this->Cell(25, $h, $solicitud["abreviado"]." - ".$solicitud["nrodocumentorep"], 1, 0, 'L');
            $this->Cell(0, $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(115, $h, "Nombre", 1, 0, 'C');
            $this->Cell(0, $h, "DNI", 1, 1, 'C');
            $this->SetX(5);
            $this->Cell(115, $h, strtoupper($solicitud["callerep"]), 1, 0, 'L');
            $this->Cell(25, $h, $solicitud["nrocallerep"], 1, 0, 'C');
            $this->Cell(25, $h, strtoupper($solicitud["manzanarep"]), 1, 0, 'C');
            $this->Cell(0, $h, strtoupper($solicitud["loterep"]), 1, 1, 'C');

            $this->SetX(5);
            $this->Cell(115, $h, utf8_decode('domicilio (Calle, Jirón, Avenida)'), 1, 0, 'C');
            $this->Cell(25, $h, utf8_decode('N°'), 1, 0, 'C');
            $this->Cell(25, $h, 'Mz.', 1, 0, 'C');
            $this->Cell(0, $h, 'Lt.', 1, 1, 'C');

            $this->SetX(5);
            $this->Cell(75, $h, '', 1, 0, 'L');
            $this->Cell(40, $h, strtoupper($solicitud["provinciarep"]), 1, 0, 'L');
            $this->Cell(0, $h, strtoupper($solicitud["distritorep"]), 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(75, $h, utf8_decode('(Urbanización, barrio)'), 1, 0, 'C');
            $this->Cell(40, $h, 'Provincia', 1, 0, 'C');
            $this->Cell(0, $h, 'Distrito', 1, 1, 'C');

            $this->SetX(5);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(0, $h, "III. DATOS DEL PREDIO (marcar con X)", 0, 1, 'L');
            $this->SetFont('Arial', '', 7);

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, utf8_decode('En Construcción'), 1, 1, 'L');
            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, 'Habilitado', 1, 1, 'L');
            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, 'Otros (especificar)', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(82.5, $h, $solicitud["descripcioncorta"]." - ".$solicitud["calle"], 1, 0, 'C');
            $this->Cell(20, $h, $solicitud["nrocallepred"], 1, 0, 'C');
            $this->Cell(25, $h, $solicitud["manzanapred"], 1, 0, 'C');
            $this->Cell(37.5, $h, $solicitud["lote"], 1, 1, 'C');

            $this->SetX(5);
            $this->Cell(82.5, $h, utf8_decode('Ubicación (Calle, Jirón, Avenida)'), 1, 0, 'C');
            $this->Cell(20, $h, utf8_decode('N°'), 1, 0, 'C');
            $this->Cell(25, $h, 'Mz', 1, 0, 'C');
            $this->Cell(37.5, $h, 'Lt', 1, 1, 'C');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, '', 1, 1, 'L');
            $this->ln(2);
            $this->SetX(5);
            $this->Cell(0, $h, utf8_decode('Mediante la presente solicitud el solicitante manifiesta su voluntad de acceder a la prestación de los siguientes servicios'), 0, 1, 'L');
            $this->ln(2);
            $this->SetX(5);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(0, $h, '(Marque con una "X" indicando el tipo de servicio)', 0, 1, 'L');
            
            $this->SetFont('Arial', '', 7);
            $this->SetX(5);
            $this->Cell(15, $h, $solicitud["codtiposervicio"] == 1 ? "X" : "", 1, 0, 'C');
            $this->Cell(87.5, $h, 'SERVICIO DE AGUA POTABLE Y ALCANTARILLADO SANITARIO', 1, 1, 'L');            
            
            $this->SetX(5);
            $this->Cell(15, $h, $solicitud["codtiposervicio"] == 2 ? "X" : "", 1, 0, 'C');
            $this->Cell(87.5, $h, 'SERVICIO DE AGUA POTABLE', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, $solicitud["codtiposervicio"] == 3 ? "X" : "", 1, 0, 'C');
            $this->Cell(87.5, $h, 'SERVICIO DE ALCANTARILLADO SANITARIO', 1, 1, 'L');

            $this->SetX(5);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(102.5, $h, '(Marque con una "X" indicando el tipo de servicio)', 0, 0, 'L');
            $this->SetFont('Arial', '', 7);
            $this->Cell(102.5, $h, utf8_decode('Número de unidades de uso que serán indicadas'), 0, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(87.5, $h, utf8_decode('DOMÉSTICO'), 1, 0, 'L');
            $this->Cell(25, $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(87.5, $h, 'COMERCIAL', 1, 0, 'L');
            $this->Cell(25, $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(87.5, $h, 'INDUSTRIAL', 1, 0, 'L');
            $this->Cell(25, $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(87.5, $h, 'ESTATAL', 1, 0, 'L');
            $this->Cell(25, $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(87.5, $h, 'SOCIAL', 1, 0, 'L');
            $this->Cell(25, $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(87.5, $h, 'TEMPORAL', 1, 0, 'L');
            $this->Cell(25, $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(87.5, $h, utf8_decode('PILETA PÚBLICA'), 1, 0, 'L');
            $this->Cell(25, $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(0, $h, utf8_decode('La conexión se solicita para ser instalada en el predio ubicado en el numeral III. Por lo cual adjunto los  documentos siguientes'), 0, 1, 'L');
            $this->SetX(5);
            /*
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, utf8_decode('Documento que acredita la propiedad o poseción'), 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, utf8_decode('Copia simple del documento de identidad del solicitante o los documentos que la representación'), 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, utf8_decode('Plano de ubicación o croquis del predio'), 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, 'Recibo de pago por derecho de factibilidad', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, 3 * $h, '', 1, 0, 'C');
            $text = utf8_decode("Resumen del sistema de tratamiento y evacuación de los desagues, detacando el punto de muestreo, los");
            $text.=utf8_decode(" análisis físicos, químicos y bacteorologicos del fuente (en caso de uso intensivo del agua de tipo");
            $text.=" comercial o industrial)";
            $this->MultiCell(150, $h, $text, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, 2 * $h, '', 1, 0, 'C');
            $text = utf8_decode("Plano de diseño de salidas de alcantarillado sanitario (para casos de edificaciones en propiedad exclusiva y");
            $text.= utf8_decode(" común)");
            $this->MultiCell(150, $h, $text, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, 2 * $h, '', 1, 0, 'C');
            $text = utf8_decode("Descripción de la servidumbre pactada o legal entre las partes por donde pasarán las tuberias de agua ");
            $text.=" potable y/o alcantarillado sanitario (en caso de independizaciones)";
            $this->MultiCell(150, $h, $text, 1, 'L');
                        
            $this->SetX(5);
            $this->Cell(15, $h, '', 1, 0, 'C');
            $this->Cell(150, $h, 'Otros', 1, 1, 'L');
            */
            $SqlReq="SELECT codrequisitos, descripcion FROM solicitudes.requisitos WHERE estareg=1 ";
            $conn = $conexion->prepare($SqlReq);
            $conn->execute(array());
            $itemsD = $conn->fetchAll();
            foreach($itemsD as $rowD) {
                $codreq= $rowD['codrequisitos'];
                $ver="SELECT COUNT(codrequisitos)
                    FROM solicitudes.detsolicitud_requisito
                    WHERE nrosolicitud=? and codrequisitos=? ";
                $conex= $conexion->prepare($ver);
                $conex->execute(array($id, $codreq));
                $conex = $conex->fetch();
                //echo $id." ".$codreq." - ".$conex[0]."<BR />";
                $hcc= 150;
                if($conex[0]==1)
                {   
                    if(strlen($rowD['descripcion'])>100)
                    {
                        $this->Cell(15, 2*$h, ' X ', 1, 0, 'C');
                    }else
                        {
                            $this->Cell(15, $h, ' X ', 1, 0, 'C');
                        }
                    
                    $this->MultiCell($hcc, $h, utf8_decode($rowD['descripcion']), 1, 'L');
                    $this->SetX(5);
                }else
                    {   
                        if(strlen($rowD['descripcion'])>116)
                        {
                            $this->Cell(15, 2*$h, '', 1, 0, 'C');
                        }else
                            {
                                $this->Cell(15, $h, '', 1, 0, 'C');
                            }
                        //$this->Cell(15, $h, '', 1, 0, 'C');
                        $this->MultiCell($hcc, $h, utf8_decode($rowD['descripcion']), 1, 'L');
                        $this->SetX(5);
                    }
            }
            /*$ver= "Plano de diseño de salidas de alcantarillado sanitario (para casos de edificaciones en propiedad exclusiva y comun)";
            echo strlen($ver);*/
            $this->ln(5);
            $this->SetX(5);
            $this->Cell(15, $h, 'Atentamente', 0, 1, 'L');

            $this->ln(5);
            $this->SetX(5);
            $this->Cell(15, 2 * $h, '', 0, 0, 'L');
            $this->Cell(60, 2 * $h, '', 1, 0, 'L');
            $this->Cell(25, 2 * $h, '', 0, 0, 'L');
            $this->Cell(65, 2 * $h, '', 1, 1, 'L');

            $this->SetX(5);
            $this->Cell(15, $h, '', 0, 0, 'L');
            $this->Cell(60, $h, 'FIRMA DEL SOLICITANTE', 1, 0, 'C');
            $this->Cell(25, $h, '', 0, 0, 'L');
            $this->Cell(65, $h, 'SELLO DE RECEPCION DE EPS', 1, 1, 'C');

            $this->SetX(5);
            $this->Cell(15, $h, '', 0, 0, 'L');
            $this->Cell(60, $h, 'o su representante', 1, 1, 'C');

            $this->ln(25);
            $this->SetX(5);
            $this->Cell(15, $h, utf8_decode('Nota: Este formato tiene carácter de Declaración Jurada'), 0, 1, 'L');


            $this->Ln(2);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(5, $h, "", 0, 0, 'R');
            $this->Cell(80, $h, "CROQUIS DEL PREDIO", 0, 1, 'L');
            $this->SetFont('Arial', '', 7);
            $this->Cell(25, $h, "Direccion", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(70, $h, strtoupper($solicitud["descripcioncorta"]." ".$solicitud["calle"]." ".$solicitud["nrocallepred"]), 0, 1, 'L');

            $this->Cell(25, $h, "Departamento", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(70, $h, strtoupper($solicitud["departamentopred"]), 0, 1, 'L');

            $this->Cell(25, $h, "Provincia", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(70, $h, strtoupper($solicitud["provinciapred"]), 0, 1, 'L');

            $this->Cell(25, $h, "Distrito", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(70, $h, strtoupper($solicitud["distritopred"]), 0, 1, 'L');

            $this->Cell(25, $h, "Referencia", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            //$this->MultiCell(0,$h,strtoupper($solicitud["referencia"]),0,'J',0);
            $this->MultiCell(130, $h, strtoupper(utf8_decode($solicitud["referencia"])), 0, 'J', 0);

            $this->Cell(25, $h, "Area del Inmueble", 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'R');
            $this->Cell(70, $h, $solicitud["area"]." m ".Cuadrado, 0, 1, 'L');

            $this->Ln(10);

            /* --DATOS PARA LA GENERACION DEL GRAFICO-- */
            $this->Line(50, $this->GetY() + 2, 160, $this->GetY() + 2);

            $this->Line(42, $this->GetY() + 10.2, 42, $this->GetY() + 34.8);
            $this->Line(168, $this->GetY() + 10.2, 168, $this->GetY() + 34.8);
            $this->Line(42, $this->GetY() + 46, 42, $this->GetY() + 146);
            $this->Line(168, $this->GetY() + 46, 168, $this->GetY() + 146);
            $this->Line(20, $this->GetY() + 35, 42, $this->GetY() + 35);
            $this->Line(20, $this->GetY() + 46, 42, $this->GetY() + 46);
            $this->Line(20, $this->GetY() + 146, 42, $this->GetY() + 146);
            $this->Line(168, $this->GetY() + 35, 190, $this->GetY() + 35);
            $this->Line(168, $this->GetY() + 46, 190, $this->GetY() + 46);
            $this->Line(168, $this->GetY() + 146, 190, $this->GetY() + 146);

            $this->Line(50, $this->GetY() + 160, 160, $this->GetY() + 160);
            $this->Line(20, $this->GetY() + 160, 42, $this->GetY() + 160);
            $this->Line(168, $this->GetY() + 160, 190, $this->GetY() + 160);
            $this->Line(42, $this->GetY() + 160, 42, $this->GetY() + 170);
            $this->Line(50, $this->GetY() + 160, 50, $this->GetY() + 170);
            $this->Line(160, $this->GetY() + 160, 160, $this->GetY() + 170);
            $this->Line(168, $this->GetY() + 160, 168, $this->GetY() + 170);
            $this->Ln(8);
            $this->Rect(50, $this->GetY() + 5, 110, 22);
            $this->Rect(50, $this->GetY() + 38, 110, 100);


            $div_1 = false;
            $div_2 = false;
            $div_3 = false;
            $div_4 = false;
            $div_5 = false;
            $div_6 = false;
            $div_7 = false;
            $div_8 = false;
            $div_9 = false;
            $div_10 = false;
            $div_11 = false;
            $div_12 = false;
            $div_13 = false;
            $div_14 = false;
            $div_15 = false;
            $div_16 = false;

            if ($solicitud["grafico"] == "Div_1") {
                $div_1 = true;
            }
            if ($solicitud["grafico"] == "Div_2") {
                $div_2 = true;
            }
            if ($solicitud["grafico"] == "Div_3") {
                $div_3 = true;
            }
            if ($solicitud["grafico"] == "Div_4") {
                $div_4 = true;
            }
            if ($solicitud["grafico"] == "Div_5") {
                $div_5 = true;
            }
            if ($solicitud["grafico"] == "Div_6") {
                $div_6 = true;
            }
            if ($solicitud["grafico"] == "Div_7") {
                $div_7 = true;
            }
            if ($solicitud["grafico"] == "Div_8") {
                $div_8 = true;
            }
            if ($solicitud["grafico"] == "Div_9") {
                $div_9 = true;
            }
            if ($solicitud["grafico"] == "Div_10") {
                $div_10 = true;
            }
            if ($solicitud["grafico"] == "Div_11") {
                $div_11 = true;
            }
            if ($solicitud["grafico"] == "Div_12") {
                $div_12 = true;
            }
            if ($solicitud["grafico"] == "Div_13") {
                $div_13 = true;
            }
            if ($solicitud["grafico"] == "Div_14") {
                $div_14 = true;
            }
            if ($solicitud["grafico"] == "Div_15") {
                $div_15 = true;
            }
            if ($solicitud["grafico"] == "Div_16") {
                $div_16 = true;
            }

            $this->SetXY(50, $this->GetY() + 5);
            $this->Cell(13, $h + 18, "", 1, 0, 'C', $div_1);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_2);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_3);
            $this->Cell(13, $h + 18, "", 1, 0, 'C', $div_4);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_5);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_6);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_7);
            $this->Cell(14, $h + 18, "", 1, 1, 'C', $div_8);
            $this->Ln(3);
            $this->Cell(190, $h, strtoupper($solicitud["descripcioncorta"]." ".$solicitud["calle"]." ".$solicitud["nrocalle"]), 0, 0, 'C');
            $this->Ln(8);
            $this->SetX(50);
            $this->Cell(13, $h + 18, "", 1, 0, 'C', $div_9);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_10);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_11);
            $this->Cell(13, $h + 18, "", 1, 0, 'C', $div_12);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_13);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_14);
            $this->Cell(14, $h + 18, "", 1, 0, 'C', $div_15);
            $this->Cell(14, $h + 18, "", 1, 1, 'C', $div_16);

            $this->SetY(140);
            //$this->Cell(190,$h,strtoupper($solicitud["descripcioncorta"]." ".$solicitud["calle"]." ".$solicitud["nrocalle"]),0,1,'C');
            /* --FIN DE LOS DATOS DE LA GENERACION DEL GRAFICO-- */
        }

    }

    $codsuc = $_GET["codsuc"];
    $nrosolicitud = $_GET["Id"];
    define('Cuadrado', chr(178));
    $objReporte = new clsSolicitud();
    $objReporte->SetAutoPageBreak(true, 1);
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->contenido($nrosolicitud, $codsuc);
    $objReporte->Output();
    
?>