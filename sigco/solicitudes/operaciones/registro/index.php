<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$TituloVentana = "SOLICITUDES DE CONEXION";
$Activo = 1;

$Op = isset($_GET['Op']) ? $_GET['Op'] : 0;

if ($Op != 5) {
    CuerpoSuperior($TituloVentana);
}

$Criterio = 'Solicitud_Registro';
$codsuc = $_SESSION['IdSucursal'];
$and = "";

if ($_GET['Op'] == 5) {
    $and = " and s.estareg=1 ";
}

$FormatoGrilla = array();

$Sql = "SELECT s.nrosolicitud, t.descripcion AS servicio, " . $objFunciones->getCodAntiguo('s.') . " , s.propietario, ";
$Sql .= " tc.descripcioncorta || ' ' || c.descripcion || '#' || s.nrocalle AS calle, ";
$Sql .= " " . $objFunciones->FormatFecha('s.fechaemision') . ", ";
$Sql .= " es.descripcion AS estadosolicitud, s.estareg, s.solreferencia, 1 ";
$Sql .= "FROM solicitudes.solicitudes s ";
$Sql .= " INNER JOIN public.tiposervicio t ON(s.codtiposervicio = t.codtiposervicio)";
$Sql .= " INNER JOIN public.calles c ON(s.codcalle = c.codcalle) AND (s.codemp = c.codemp) AND (s.codsuc = c.codsuc) AND (s.codzona = c.codzona) ";
$Sql .= " INNER JOIN public.tiposcalle tc ON(c.codtipocalle = tc.codtipocalle) ";
$Sql .= " INNER JOIN public.tipodocumento td ON(s.codtipodocumento = td.codtipodocumento) ";
$Sql .= " INNER JOIN public.estadosolicitud es ON(s.estareg = es.idestadosolicitud) ";
$Sql .= " INNER JOIN public.ubigeo u ON(u.codubigeo = s.codubigeo)";

//  to_char(fechacreacion,'dd-MM-yyyy') 
$FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL                                                           //Sentencia SQL
$FormatoGrilla[1] = array('1' => 's.nrosolicitud', '2' => 't.descripcion', '3' => 's.propietario', '4' => 's.nrodocumento', '5' => "to_char(s.codsuc,'00')||''||TRIM(to_char(s.nroinscripcion,'000000'))");          //Campos por los cuales se hará la búsqueda
$FormatoGrilla[2] = $Op;                                                            //Operacion
$FormatoGrilla[3] = array('T1' => 'Nro. Solicitud', 'T2' => 'Servicio', 'T3' => 'Inscripci&oacute;n', 'T4' => 'Propietario',
    'T5' => 'Direccion', 'T6' => 'Fecha', 'T7' => 'Estado');   //Títulos de la Cabecera
$FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'center', 'A4' => 'left', 'A6' => 'center', 'A7' => 'center');                        //Alineación por Columna
$FormatoGrilla[5] = array('W1' => '60', 'W2' => '80', 'W6' => '80', 'W11' => '80', 'W3' => '70', 'W8' => '90');                                 //Ancho de las Columnas
$FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
$FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
$FormatoGrilla[8] = "  AND (s.codemp = 1 AND s.codsuc = " . $codsuc . $and . ") ORDER BY s.nrosolicitud DESC ";
//Orden de la Consulta
if ($Op != 5) {
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '4', //Número de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'modificar.png', //Imagen a mostrar
        'Btn1' => 'Editar', //Titulo del Botón
        'BtnF1' => 'onclick="Mostrar(this.id, 1);"', //Eventos del Botón
        'BtnCI1' => '8', //Item a Comparar
        'BtnCV1' => '1', //Valor de comparación
        'BtnId2' => 'BtnEliminar',
        'BtnI2' => 'eliminar.png',
        'Btn2' => 'Eliminar',
        'BtnF2' => 'onclick="Anular(this.id)"',
        'BtnCI2' => '8', //campo 3
        'BtnCV2' => '1', //igua a 1
        'BtnId3' => 'BtnRestablecer', //y aparece este boton
        'BtnI3' => 'imprimir.png',
        'Btn3' => 'Imprimir',
        'BtnF3' => 'onclick="Imprimir(this.id)"',
        'BtnCI3' => '10',
        'BtnCV3' => '1',
        'BtnId4' => 'BtnGenerar', //y aparece este boton
        'BtnI4' => 'documento.png',
        'Btn4' => 'Generar Solicitud Agua-Desague',
        'BtnF4' => 'onclick="GenerarSolicitud(this)"',
        'BtnCI4' => '10',
        'BtnCV4' => '1'
    );
} else {
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '1', //Número de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'ok.png', //Imagen a mostrar
        'Btn1' => 'Seleccionar', //Titulo del Botón
        'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
        'BtnCI1' => '10', //Item a Comparar
        'BtnCV1' => '1'    //Valor de comparación
    );
    $FormatoGrilla[5] = array('W1' => '60', 'W6' => '80', 'W11' => '20', 'W2' => '70', 'W3' => '70', 'W10' => '90');
}

$FormatoGrilla[10] = array(array('Name' => 'ref', 'Col' => 12),
    array('Name' => 'id', 'Col' => 1),
    array('Name' => 'estado', 'Col' => 8)); //DATOS ADICIONALES
$FormatoGrilla[11] = 7; //FILAS VISIBLES

$_SESSION['Formato' . $Criterio] = $FormatoGrilla;

Cabecera('', $FormatoGrilla[7], 950, 560);
Pie();
?>
<script type="text/javascript">
    var urldir = "<?php echo $_SESSION['urldir']; ?>";

    function Enviar(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        opener.Recibir(Id);

        window.close();
    }
    function Imprimir(Id)
    {
        var url = '';
        url = 'imprimir.php';

        Id = Id.substr(Id.indexOf('-') + 1);

        AbrirPopupImpresion(url + '?Id=' + Id + '&codsuc=<?=$codsuc ?>', 800, 500)
    }
    function Anular(Id)
    {
        $("#form1").remove();
        var estado = $('#' + Id).data('estado')

        if (estado != 0)
            Eliminar(Id)
        else
            Msj($(obj).parent().parent(), 'No se Puede Anular')

    }
    function GenerarSolicitud(obj)
    {
        //$(obj).parent().parent()

        var ref = $(obj).parent().parent().data('ref')
        var Id = $(obj).parent().parent().data('id')

        if (ref != 0)
        {
            alert('La Solicitud de Conexion ya se encuentra relacionado con otra solicitud')
            return
        }
        //location.href = "generar/?Id="+id+"&IdSucursal=<?=$codsuc ?>";
        $("#Modificar").dialog({title: 'Generar Solicitud'});
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function () {
            GuardarOk();
        });
        $("#button-maceptar").children('span.ui-button-text').text('Generar Solicitud')
        $.ajax({
            url: 'generar/index.php',
            type: 'POST',
            async: true,
            data: 'Id=' + Id + '&codsuc=<?=$codsuc ?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function GuardarOk()
    {
        $.ajax({
            url: 'generar/Guardar.php?Op=0',
            type: 'POST',
            async: true,
            data: $('#form1').serialize(),
            success: function (data) {
                //alert(data);
                OperMensaje(data)
                $("#Mensajes").html(data);
                $("#DivModificar").html('');
                $("#Modificar").dialog("close");
                Buscar(0);
            }
        })
    }
</script>
<?php
if ($Op != 5) {
    CuerpoInferior();
}
?>
