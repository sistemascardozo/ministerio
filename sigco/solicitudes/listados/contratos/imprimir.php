<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses,$Desde,$Hasta;
			
			$periodo = $this->DecFechaLiteral();
			
			$this->SetFont('Arial','B',10);
			
			$h = 4;
			$this->Cell(278, $h+2,"LISTA DE CONTRATOS",0,1,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(278, $h+2,'Fecha de Registro desde: '.$Desde." hasta: ".$Hasta,0,1,'C');
			
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(8,20,30,25,25,60,60,20,30));
			$this->SetAligns(array(C,C,C,C,C,C,C,C,C,C));
			$this->Row(array(utf8_decode("N°"),
							"NRO. CONTRATO",
							"NRO. EXPEDIENTE",
							utf8_decode("FECHA EMISIÓN"),
							 "NRO. INSC.",
							 "PROPIETARIO",
							 "DIRECCION",
							 "IMPORTE",
							 "ESTADO",							
							 ));

		}
		function contenido($codsuc,$Desde,$Hasta)
		{
			global $conexion;
			
			$h = 4;
			$s = 2;			
			
			$Condicion = " WHERE c.codsuc = ".$codsuc." AND c.fechaemision BETWEEN CAST ('".$Desde."' AS DATE) AND CAST('".$Hasta."' AS DATE) ";
			
			$Sql = "SELECT c.nroexpediente, ".$this->FormatFecha('c.fechaemision').", c.nrocontrato, c.nroinscripcion, c.propietario, ";
			$Sql .= " c.direccion AS direccion, ".$this->Convert("c.total", "NUMERIC (18,2)").", ep.descripcion AS estadopresupuesto, ";
			$Sql .= " c.estadoexpediente, c.codexpediente ";
			$Sql .= "FROM solicitudes.expedientes c ";
			$Sql .= " INNER JOIN public.estadopresupuesto ep ON(ep.estadopresupuesto = c.estadoexpediente) ".$Condicion;

			$count= 0;
			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			foreach($items as $row)
			{
				$count++;
				$nroreclamo = $row["nroreclamo"];
				
				$this->SetWidths(array(8,20,30,25,25,60,60,20,30));
				$this->SetAligns(array(C,C,"C","C",C,L,L,R,"L","L"));
				$this->Row(array($count,
					$row["nrocontrato"], 
					$row["nroexpediente"], 
					$row[1],
					$this->CodUsuario($codsuc,$row["nroinscripcion"]),
					trim(utf8_decode(strtoupper($row["propietario"]))),
					utf8_decode(strtoupper($row["direccion"])),
					number_format($row[6], 2),
					strtoupper($row["estadopresupuesto"])
				
				));				
				
			}
			
			$this->Ln(3);
			$this->Cell(278, $h+2,"CANTIDAD DE CONTRATOS ".$count,'TB',1,'C');
			$this->SetFont('Arial','B',8);
			$this->Ln(3);
						
		}
	}
	
	$codsuc   = $_GET["codsuc"];
	$Desde    = $_GET["Desde"];
	$Hasta    = $_GET["Hasta"];

	
	$objReporte = new clsPadron("L");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsuc,$Desde,$Hasta);
	$objReporte->Output();
	
?>