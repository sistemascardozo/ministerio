<?php 
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "LISTA DE CONTRATOS";
	$Activo=1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>

	jQuery(function($){ 

		$("#fdesde").mask("99/99/9999");
		$("#fhasta").mask("99/99/9999");
		$( "#DivTipos" ).buttonset();
		
		$( "#fdesde" ).datepicker({
					showOn: 'button',
					direction: 'up',
					buttonImage: '../../../../images/iconos/calendar.png',
					buttonImageOnly: true,
					showOn: 'both',
					showButtonPanel: true
		});
		
		$( "#fhasta" ).datepicker({
					showOn: 'button',
					direction: 'up',
					buttonImage: '../../../../images/iconos/calendar.png',
					buttonImageOnly: true,
					showOn: 'both',
					showButtonPanel: true
		})
	});

	function cargar_conceptos_reclamos(tipo,seleccion)
	{
	$.ajax({
		 url:'<?php echo $_SESSION['urldir'];?>ajax/conceptos_reclamos_drop.php',
		 type:'POST',
		 async:true,
		 data:'tiporeclamo='+tipo+'&seleccion='+seleccion,
		 success:function(datos){
                    $("#div_conceptos_reclamos").html(datos);
                    $("#tiporeclamo").val(tipo);
                    $(".buttonset").buttonset('refresh');
                    $("#chkconcepto").prop('checked', false);
                    $("#todosconceptoreclamo").val(0);
		 }
	}) 
	}
	var c = 0
	function ValidarForm(Op)
	{
		

		var Desde = $("#fdesde").val()
		var Hasta = $("#fhasta").val()
		if (Trim(Desde)=='')
		{
			
			Msj('#fdesde','Seleccione la Fecha Inicial',1000,'above','',false)
			return false;
			
		}
		if (Trim(Hasta)=='')
		{
			
			Msj('#fhasta','Seleccione la Fecha Final',1000,'above','',false)
			return false;
			
		}
		Fechas = '&Desde='+Desde+'&Hasta='+Hasta
		if($("#todostiporeclamo").val()==0)
		{
			estadoreclamo=$("#codestadoreclamo").val()
			if(estadoreclamo==10000)
			{
				alert("Seleccione el Estado de Reclamo")
				return false
			}
		}else{
			estadoreclamo="%"
		}

		if($("#todosconceptoreclamo").val()==0)
		{
			conceptoreclamo=$("#codconcepto").val()
			if(conceptoreclamo==0)
			{
				alert("Seleccione el Concepto de Reclamo")
				return false
			}
		}else{
			conceptoreclamo="%"
		}


		if(document.getElementById("rptpdf").checked==true)
		{
			url="imprimir.php";
		}	
		url += "?codsuc=<?=$codsuc?>"+Fechas+"&conceptoid="+conceptoreclamo+"&estadoreclamo="+estadoreclamo
		AbrirPopupImpresion(url,800,600)
		
		return false;
	}

	function Cancelar()
	{
		location.href = '<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function buscar_tipo_reclamos()
	{
		AbrirPopupBusqueda("../../catalogo/conceptosreclamo/?Op=5",1100,500)
	}
	function RecibirTipoReclamo(id,tipo)
	{
		$( "#tabs" ).tabs("select",1);
		$("#tipo_reclamo"+tipo).attr("checked", true);
		$(".buttonset").buttonset('refresh');
		cargar_conceptos_reclamos(tipo,id)
		$("#glosa").focus().select();
		$("#todosconceptoreclamo").val(0);

	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="400" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">				  
					
					<tr>
                    	<td>Desde</td>
                    	<td width="20" align="center">:</td>
                    	<td>
                        	<input type="text" name="fdesde" id="fdesde" value="<?=date('d/m/Y')?>" class="inputtext" style="width:80px;" />
                        </td>
                    	<td align="right">Hasta</td>
                    	<td width="20" align="center">:</td>
                    	<td>
                        	<input type="text" name="fhasta" id="fhasta" value="<?=date('d/m/Y')?>" class="inputtext" style="width:80px;" />
                     	</td>
                  	</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td align="center">&nbsp;</td>
					  <td>&nbsp;</td>
					  <td align="right">&nbsp;</td>
					  <td align="center">&nbsp;</td>
					  <td>&nbsp;</td>
				  </tr>
					<tr>
						<td colspan="6" align="center">
							<div id="DivTipos" style="display:inline">
								<input type="radio" name="rabresumen" id="rptpdf" value="radio" checked="checked" />
								<label for="rptpdf">PDF</label>
							</div>	
							<input type="button" onclick="return ValidarForm();" value="Generar" id="">
						</td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
 
	 </tbody>

    </table>
 </form>
</div>
<?php 
	CuerpoInferior();
?>