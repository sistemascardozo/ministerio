<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsDrop.php");

    $conexion->beginTransaction();
    $Op = $_GET["Op"];
    $objFunciones = new clsFunciones();

    $codrequisitos = $_POST["codrequisitos"];
    $descripcion = $_POST["descripcion"];
    $estareg = $_POST["estareg"];

    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("requisitos", "0", "0");
            $codrequisitos = $id[0];
            $sql = "INSERT INTO solicitudes.requisitos(codrequisitos,descripcion,estareg) values(:codrequisitos,:descripcion,:estareg)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codrequisitos" => $codrequisitos, ":descripcion" => $descripcion, ":estareg" => $estareg));
            break;
        case 1:
            $sql = "UPDATE solicitudes.requisitos set descripcion=:descripcion,estareg=:estareg WHERE codrequisitos=:codrequisitos";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codrequisitos" => $codrequisitos, ":descripcion" => $descripcion, ":estareg" => $estareg));
            break;
        case 2:case 3:
            $sql = "UPDATE solicitudes.requisitos set estareg=:estareg WHERE codrequisitos=:codrequisitos";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codrequisitos" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
    
?>

