<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsDrop.php");
    $objMantenimiento = new clsDrop();
    
    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
    $guardar = "op=".$Op;

    if ($Id != '')
	{
        $Sql="SELECT * FROM solicitudes.requisitos r WHERE r.codrequisitos = ?";
        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array($Id));
        $requisitos = $consulta->fetch();
        $guardar = $guardar."&Id2=".$Id;
	}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    function ValidarForm(Op)
    {
        if ($("#Descripcion").val() == "")
        {
            alert('La Descripcion del Grupo de Costos no puede ser NULO');
            return false;
        }
        GuardarP(Op);
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="650" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td width="20" class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
                <tr>
                    <td class="TitDetalle">Id</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <input name="codrequisitos" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?= $Id ?>" class="inputtext"/>
                    </td>
                </tr>	
                <tr>
                    <td valign="top" class="TitDetalle">Descripcion</td>
                    <td align="center" valign="top" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
						<textarea name="descripcion" rows="5" class="inputtext" id="Descripcion" style="width:500px;"><?= $requisitos["descripcion"] ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Estado</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <?php include("../../../../include/estareg.php"); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>

        </table>
    </form>
</div>
<?php
    $est = isset($requisitos["estareg"]) ? $requisitos["estareg"] : 1;
    include("../../../../admin/validaciones/estareg.php");
?>