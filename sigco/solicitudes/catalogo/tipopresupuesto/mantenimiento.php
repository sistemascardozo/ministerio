<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsDrop.php");
	$objMantenimiento 	= new clsDrop();
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc = $_SESSION['IdSucursal'];
	if($Id!='')
	{
		$tipopresupuesto 	= $objMantenimiento->setTipoPresupuesto(" WHERE codtipopresupuesto=? ",$Id);
		//print_r($tipopresupuesto);
		//$Sql="SELECT * FROM public.tipopresupuesto WHERE codtipopresupuesto = ".$Id;
        //$consulta = $conexion->prepare($Sql);
        //$consulta->execute(array($Id));
        //$consulta 	= $conexion->query($Sql);
        //$requisitos = $consulta->fetch();
		$guardar	= $guardar."&Id2=".$Id;
	}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
$(document).ready(function(){
	if('<?=$Op?>' == 1)
	{
		datos_conceptos(<?=$tipopresupuesto["codconcepto"]?>);
	}
});

	function ValidarForm(Op)
	{
		if($("#Code").val()=="")
		{
			alert('El Codigo no puede ser NULO');
			return false;
		}
		if($("#Descripcion").val()=="")
		{
			alert('La Descripcion del Tipo de Presupuesto no puede ser NULO');
			return false;
		}
		if($("#Descripcion2").val()=="")
		{
			alert('La Descripcion 2 del Tipo de Presupuesto no puede ser NULO');
			return false;
		}
		if($("#unidad").val()=="0")
		{
			alert('Seleccione una Unidad de Medida');
			return false;
		}

		GuardarP(Op);
	}
	
	function buscar_conceptos()
	{
		AbrirPopupBusqueda('<?php echo $_SESSION['urldir'];?>sigco/facturacion/catalogo/conceptos/?Op=5&Tipo=2',1000,500);
	}

function datos_conceptos(id)
{
	$.ajax({
		 url:'<?php echo $_SESSION['urldir'];?>ajax/mostrar/conceptos.php',
		 type:'POST',
		 async:true,
		 data:'conceptos='+id+'&codsuc=<?=$codsuc?>',
		  dataType: 'json',
		 success:function(datos){
			if(datos.codconcepto!=null)
			{
				var igv=0;
				$("#codconcepto").val(datos.codconcepto)
				$("#colateral").val(datos.descripcion)
				
			}
			
		 }
	}) 
}

	function Recibir(id)
	{
		datos_conceptos(id);
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
 <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
  <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id :</td>
      <td class="CampoDetalle">
		<input name="codtipopresupuesto" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?=$Id?>" class="inputtext"/>
      </td>
	</tr>	
	<tr>
      <td class="TitDetalle">Codigo :</td>
      <td class="CampoDetalle">
		<input name="code" type="text" id="Code" size="8" maxlength="5" value="<?=$tipopresupuesto["code"]?>" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion :</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Descripcion" maxlength="200" value="<?=htmlspecialchars($tipopresupuesto["descripcion"])?>" style="width:450px;"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion 2 :</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion2" type="text" id="Descripcion2" maxlength="200" value="<?=htmlspecialchars($tipopresupuesto["descripcion2"])?>" style="width:450px;"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Colateral :</td>
	  <td class="CampoDetalle">
	    <input type="text" id="colateral" maxlength="80" class="inputtext" readonly="readonly" onclick="buscar_conceptos();" style="width:420px;"/>
	    <span class="MljSoft-icon-buscar" title="Buscar Colateral" onclick="buscar_conceptos();"></span>
		<input type="hidden" name="codconcepto" id="codconcepto" value='<?=$tipopresupuesto["codconcepto"]?>' />
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Utilidad :</td>
	  <td class="CampoDetalle">
	    <input type="text" id="utilidad" name="utilidad" size="8" maxlength="20" class="inputtext numeric" value="<?=number_format($tipopresupuesto["utilidad"],2)?>"/> %
	   
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Unidad de Medida :</td>
	  <td class="CampoDetalle">
    	<? $objMantenimiento->drop_unidad_medida($tipopresupuesto["unimed"]); ?>    
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Estado :</td>
	  <td class="CampoDetalle">
    	<? include("../../../../include/estareg.php"); ?>
      </td>
	</tr>
	<tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	</tr>
  </tbody>
 
 </table>
</form>
</div>
<?php
	$est = isset($tipopresupuesto["estareg"])?$tipopresupuesto["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>