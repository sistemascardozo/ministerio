<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
	
  $Criterio='TipoP';
  $TituloVentana = "TIPO PRESUPUESTO";
  $Activo=1;
  
  CuerpoSuperior($TituloVentana);
  
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];

  $FormatoGrilla = array ();
  $Sql = "select codtipopresupuesto, t.code, t.descripcion, t.descripcion2, u.descripcion, e.descripcion, t.estareg 
          from public.tipopresupuesto t
          LEFT JOIN public.unidades u ON t.unimed=u.unimed
          INNER JOIN public.estadoreg e ON (t.estareg=e.id)";
  $FormatoGrilla[0] =eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                           //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'codtipopresupuesto', '2'=>'t.code', '5'=>'t.descripcion', '3'=>'t.descripcion2', '4' => 'u.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Id', 'T2' => 'C&oacute;digo','T3'=>'Descripci&oacute;n','T4'=>'Descripci&oacute;n 2', 'T5' => 'Unidad de Medida','T6'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'left', 'A4'=>'left', 'A5'=>'center', 'A6' => 'center', 'A7' => 'left');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'50', 'W2'=>'50', 'W6'=>'90');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1000;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " ORDER BY codtipopresupuesto ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'7',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'7', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'7', 
              'BtnCV3'=>'0');
              
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 700, 400);
  Pie();
  CuerpoInferior();
?>
  
