<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsMantenimiento();

	if($Id != '')
	{
		$unidad 	= $objMantenimiento->setUnidadMedida(" WHERE unimed=? ",$Id);
		$guardar	= $guardar."&Id2=".$Id;
	}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if($("#abreviado").val()=='')
		{
			alert('La Descripcion Abreviada de la Unidad no puede ser NULO')
			return false
		}
		if($("#Descripcion").val()=="")
		{
			alert('La Descripcion de la Unidad no puede ser NULO');
			return false;
		}
		GuardarP(Op);
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
 <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
  <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id :</td>
      <td class="CampoDetalle">
		<input name="unimed" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?=$Id?>" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Abreviado : </td>
	  <td class="CampoDetalle">
      	<input name="abreviado" type="text" id="abreviado" size="6" maxlength="6" value="<?=$unidad["abreviado"]?>" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion :</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Descripcion" size="70" maxlength="200" value="<?=$unidad["descripcion"]?>"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Estado :</td>
	  <td class="CampoDetalle">
		<? include("../../../../include/estareg.php"); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
   </tbody>
  </table>
 </form>
</div>
<script>
  $("#Abreviado").focus();
</script>
<?php
	$est = isset($unidad["estareg"])?$unidad["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>