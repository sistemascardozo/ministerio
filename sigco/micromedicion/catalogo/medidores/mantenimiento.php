<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;	
    	
	$objMantenimiento 	= new clsMantenimiento();
    $codsuc = $_SESSION['IdSucursal'];
    $codemp=1;

	if($Id!='')
	{
		$sql = $objMantenimiento->Sentencia("bancomedidor")." WHERE codbancomedidor= ? AND codemp=? AND codsuc=? ";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($Id, $codemp, $codsuc));
		$row = $consulta->fetch();
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if ($("#descripcion").val() == '')
		{
			alert('El nombre del Medidor no puede ser NULO');
			return false;
		}
        
        if ($("#codalmacen").val() == '')
		{
			alert('Asigne un almacen al Medidor');
			return false;
		}
        
        
		GuardarP(Op); 
	}
	function CambiarEstado(Obj)
	{
		if(Obj.checked==true)
		{
			$("#EstaReg").val(1)
		}else{
			$("#EstaReg").val(0)
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    <tbody>
	
	<tr>
        <td class="TitDetalle">Id :</td>
        <td class="CampoDetalle">
		  <input name="codbancomedidor" type="text" id="Id" size="4" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
        </td>
	</tr>
    <tr>
        <td class="TitDetalle" align="left">Sucursal : </td>
        <td class="CampoDetalle">
            <select name="codsuc" id="codsuc" style="width:200px">
                <?php
                $Sql = "SELECT * FROM admin.sucursales WHERE estareg = 1 AND codemp=".$codemp." ";
                $Consulta = $conexion->query($Sql);
                foreach ($Consulta->fetchAll() as $row1) {
                    $Sel = "";
                    if ($row["codsuc"] == $row1["codsuc"])
                        $Sel = 'selected="selected"';
                    ?>
                    <option value="<?php echo $row1["codsuc"]; ?>" <?= $Sel ?>  ><?php echo $row1["descripcion"]; ?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="TitDetalle">Descripcion :</td>
        <td class="CampoDetalle">
            <input class="inputtext" name="descripcion" type="text" id="descripcion" size="70" maxlength="200" value="<?=isset($row["descripcion"])?$row["descripcion"]:''?>"/>
        </td>
    </tr>    
    <tr>
        <td class="TitDetalle">Direccion :</td>
        <td class="CampoDetalle">
            <input class="inputtext" name="direccion" type="text" id="direccion" size="70" maxlength="200" value="<?=isset($row["direccion"])?$row["direccion"]:''?>"/>
        </td>
    </tr>
	<tr>
        <td class="TitDetalle">Modelo :</td>
        <td class="CampoDetalle">
            <select id="codmodelo" name="codmodelo" class="select" style="width:220px">                
                <?php
                    $sqlM = "SELECT * from public.modelomedidor";            
                    $consultaM = $conexion->prepare($sqlM);
                    $consultaM->execute();
                    $itemsM    = $consultaM->fetchAll();
                
                    foreach($itemsM as $rowM)
                    {	
                        $selected = "";
                        if($row["codmodelo"]==$rowM["codmodelo"])
                        {
                        	$selected="selected='selected'";						
                        }
                        echo "<option value='".$rowM["codmodelo"]."' ".$selected." >".strtoupper($rowM["descripcion"])."</option>";
                    }
                ?> 
            </select>
        </td>
    </tr>
    <tr>
        <td class="TitDetalle">Marca :</td>
        <td class="CampoDetalle">
            <select id="codmarca" name="codmarca" class="select" style="width:220px">                
                <?php
                    $sqlMa = "SELECT * from public.marcamedidor";            
                    $consultaMa = $conexion->prepare($sqlMa);
                    $consultaMa->execute();
                    $itemsMa    = $consultaMa->fetchAll();
                
                    foreach($itemsMa as $rowMa)
                    {	
                        $selected = "";
                        if($row["codmarca"]==$rowMa["codmarca"])
                        {
                        	$selected="selected='selected'";						
                        }
                        echo "<option value='".$rowMa["codmarca"]."' ".$selected." >".strtoupper($rowMa["descripcion"])."</option>";
                    }
                ?> 
            </select>
        </td>
    </tr>
    <tr>
        <td class="TitDetalle">Capacidad x Hora :</td>
        <td class="CampoDetalle">
            <input class="inputtext" name="capacidadxhora" type="text" id="capacidadxhora" size="40" value="<?=isset($row["capacidadxhora"])?$row["capacidadxhora"]:'0.00'?>"/>
        </td>
    </tr>
    <tr>
        <td class="TitDetalle">Turno x Hora :</td>
        <td class="CampoDetalle">
            <input class="inputtext" name="turnoxhora" type="text" id="turnoxhora" size="40" value="<?=isset($row["turnoxhora"])?$row["turnoxhora"]:'0.00'?>"/>
        </td>
    </tr>
	<tr>
	  <td class="TitDetalle">Estado :</td>
	  <td class="CampoDetalle">
	    <? include("../../../../include/estareg.php"); ?></td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>

    </table>
 </form>
</div>
<script>
 $("#descripcion").focus();
</script>
