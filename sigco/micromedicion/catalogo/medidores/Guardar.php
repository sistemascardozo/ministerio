<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
    $codemp			 = 1;
	$codsuc 		 = $_POST["codsuc"];
    
	$codbancomedidor = $_POST["codbancomedidor"];
	$descripcion	 = strtoupper($_POST["descripcion"]);
	$codmodelo       = $_POST["codmodelo"];
    $codmarca	     = $_POST["codmarca"];
    $direccion	     = $_POST["direccion"];
    $capacidadxhora	 = $_POST["capacidadxhora"];
    $turnoxhora	     = $_POST["turnoxhora"];

	$estareg		 = $_POST["estareg"];
	

	switch ($Op) 
	{
		case 0:
			$id  = $objFunciones->setCorrelativos("bancomedidor",$codsuc,"0");
			echo $codbancomedidor 	= $id[0];
			
			$sql = "INSERT INTO micromedicion.bancomedidor(codbancomedidor,descripcion,direccion,codmarca,codmodelo, ";
            $sql .=" estareg,capacidadxhora,turnoxhora,codemp,codsuc) values(:codbancomedidor,:descripcion,:direccion,:codmarca,:codmodelo, ";
			$sql .=" :estareg,:capacidadxhora,:turnoxhora,:codemp,:codsuc)";
            $result = $conexion->prepare($sql);
			$result->execute(array(":codbancomedidor"=>$codbancomedidor,
						   ":direccion"=>$direccion,
						   ":descripcion"=>$descripcion,
                           ":codmodelo"=>$codmodelo,
                           ":codmarca"=>$codmarca,
                           ":capacidadxhora"=>$capacidadxhora,
                           ":turnoxhora"=>$turnoxhora,
						   ":estareg"=>$estareg,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc));

		break;
		case 1:
			$sql = "UPDATE micromedicion.bancomedidor set
                    descripcion= :descripcion,
                    estareg= :estareg,
                    direccion= :direccion,
                    codmodelo= :codmodelo,
                    codmarca= :codmarca,
                    capacidadxhora= :capacidadxhora,
                    turnoxhora= :turnoxhora
                    
				WHERE codemp=:codemp AND codsuc=:codsuc AND codbancomedidor=:codbancomedidor";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codbancomedidor"=>$codbancomedidor,
						   ":direccion"=>$direccion,
						   ":descripcion"=>$descripcion,
                           ":codmodelo"=>$codmodelo,
                           ":codmarca"=>$codmarca,
                           ":capacidadxhora"=>$capacidadxhora,
                           ":turnoxhora"=>$turnoxhora,
						   ":estareg"=>$estareg,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc));

		break;
		case 2:case 3:
			$sql = "update micromedicion.bancomedidor set estareg=:estareg
				WHERE codbancomedidor=:codbancomedidor";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codbancomedidor"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
