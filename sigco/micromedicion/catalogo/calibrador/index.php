<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
    
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
    
    $TituloVentana = "CALIBRADORES";
    $Activo = 1;

    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
	
    if ($Op != 5)
	{
		CuerpoSuperior($TituloVentana);
	}
	
    $codsuc = $_SESSION['IdSucursal'];

    $FormatoGrilla = array();
    $Sql = "SELECT i.codcalibrador,s.descripcion as sucursal,
        i.nombres,t.descripcion,i.nrodocumento,e.descripcion,i.estareg ";
    $Sql .= "FROM micromedicion.calibrador as i ";
    $Sql .= " INNER JOIN admin.sucursales as s on(i.codemp=s.codemp and i.codsuc=s.codsuc)
               INNER JOIN public.tipodocumento as t on(i.codtipodocumento=t.codtipodocumento)
               INNER JOIN public.estadoreg e ON (i.estareg=e.id) ";

    $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql);                                                           //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'i.codcalibrador', '2' => 's.descripcion', '3' => 'i.nombres', '4' => 't.descripcion', '5' => 'i.nrodocumento');          //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;                                                            //Operacion
    $FormatoGrilla[3] = array('T1' => 'C&oacute;digo', 'T2' => 'Sucursal', 'T3' => 'Nombre y Apellidos', 'T4' => 'Documento', 'T5' => 'Nro Documento', 'T6' => 'Estado');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'left', 'A4' => 'left');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '60');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = " AND (i.codemp=1 and i.codsuc=".$codsuc." and not i.codcalibrador=0) ORDER BY i.codcalibrador ASC ";                                   //Orden de la Consulta
    if ($Op != 5)
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '3', //Número de Botones a agregar
            'BtnId1' => 'BtnModificar', //Nombre del Boton
            'BtnI1' => 'modificar.png', //Imagen a mostrar
            'Btn1' => 'Editar', //Titulo del Botón
            'BtnF1' => 'onclick="Mostrar(this.id, 1);"', //Eventos del Botón
            'BtnCI1' => '7', //Item a Comparar
            'BtnCV1' => '1', //Valor de comparación
            'BtnId2' => 'BtnEliminar',
            'BtnI2' => 'eliminar.png',
            'Btn2' => 'Eliminar',
            'BtnF2' => 'onclick="Eliminar(this.id)"',
            'BtnCI2' => '7', //campo 3
            'BtnCV2' => '1', //igua a 1
            'BtnId3' => 'BtnRestablecer', //y aparece este boton
            'BtnI3' => 'restablecer.png',
            'Btn3' => 'Restablecer',
            'BtnF3' => 'onclick="Restablecer(this.id)"',
            'BtnCI3' => '7',
            'BtnCV3' => '0');
    else {
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '1', //Número de Botones a agregar
            'BtnId1' => 'BtnModificar', //Nombre del Boton
            'BtnI1' => 'ok.png', //Imagen a mostrar
            'Btn1' => 'Seleccionar', //Titulo del Botón
            'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
            'BtnCI1' => '7', //Item a Comparar
            'BtnCV1' => '1'    //Valor de comparación
        );
    }
    $FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 1)); //DATOS ADICIONALES            
    $_SESSION['Formato'] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 700, 350);
    Pie();
    if ($Op != 5)
        CuerpoInferior();
?>
<script type="text/javascript">
    function Enviar(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        opener.Recibir(Id);
        window.close();
    }
    if (<?= $Op ?> == 5)
        $("#")
</script>