<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}
include("../../../../objetos/clsDrop.php");

$Op = $_POST["Op"];
$Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
$guardar = "op=" . $Op;

$objMantenimiento = new clsDrop();

if ($Id != '') {
    $SQL = "SELECT * FROM micromedicion.calibrador AS i WHERE i.codcalibrador= ?";
    $conn = $conexion->prepare($SQL);
    $conn->execute(array($Id));
    $calibrador = $conn->fetch();
    $guardar = $guardar . "&Id2=" . $Id;
}
?>

<script type="text/javascript" src="<?=$urldir ?>js/funciones.js" language="JavaScript"></script>
<script>
    function ValidarForm(Op)
    {
        if ($("#sucursal").val() == 0)
        {
            Msj($("#sucursal"), "Seleccione la Sucursal");
            return false
        }
        if ($("#descripcion").val() == "")
        {
            Msj($("#descripcion"), "Ingrese Nombre y Apellido del Inspector");
            return false
        }
        if ($("#tipodocumento").val() == "")
        {
            Msj($("#tipodocumento"), "Seleccione el Tipo de Documento");
            return false
        }
        if ($("#nrodocumento").val() == "")
        {
            Msj($("#nrodocumento"), "Ingrese el Nro. Documento");
            return false
        }

        GuardarP(Op);
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td width="139" class="TitDetalle">&nbsp;</td>
                    <td width="460" class="CampoDetalle">&nbsp;</td>
                </tr>
                <tr>
                    <td class="TitDetalle">Id :</td>
                    <td class="CampoDetalle">
                        <input name="codcalibrador" type="text" id="Id" size="4" maxlength="2" value="<?=$Id ?>" readonly="readonly" class="inputtext"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Sucursal :</td>
                    <td class="CampoDetalle">
                        <?=$objMantenimiento->drop_sucursal($calibrador["codsuc"]); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Nombres y Apellidos :</td>
                    <td class="CampoDetalle">
                        <input class="inputtext" name="nombres" type="text" id="descripcion" size="55" maxlength="200" value="<?=$calibrador["nombres"] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Documento :</td>
                    <td class="CampoDetalle">
                        <?=$objMantenimiento->drop_tipodocumento($calibrador["codtipodocumento"]); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Nro. Documento :</td>
                    <td class="CampoDetalle">
                        <input class="inputtext entero" name="nrodocumento" type="text" id="nrodocumento" size="15" maxlength="15" value="<?=$calibrador["nrodocumento"] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Estado :</td>
                    <td class="CampoDetalle">
                        <?php include("../../../../include/estareg.php"); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    $("#descripcion").focus();
</script>
<?php
$est = isset($calibrador["estareg"]) ? $calibrador["estareg"] : 1;
include("../../../../admin/validaciones/estareg.php");
?>