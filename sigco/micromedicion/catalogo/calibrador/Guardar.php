<?php

    session_name("pnsu");
    if (!session_start()) {
        session_start();
    }
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();

    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $codcalibrador = $_POST["codcalibrador"] ? $_POST["codcalibrador"] : $_POST["1form1_id"];
    $sucursal = $_POST["sucursal"];
    $nombres = strtoupper($_POST["nombres"]);
    $tipodocumento = $_POST["tipodocumento"];
    $nrodocumento = $_POST["nrodocumento"];
    $estareg = $_POST["estareg"];

switch ($Op) {
    case 0:
        $id = $objFunciones->SETCorrelativos("calibrador", $sucursal, "0");
        $codcalibrador = $id[0];
        $sql = "INSERT INTO micromedicion.calibrador(codcalibrador,codemp,codsuc,nombres,
                codtipodocumento,nrodocumento,estareg) 
                values(:codcalibrador,:codemp,:codsuc,:nombres,:codtipodocumento,:nrodocumento,:estareg)";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codcalibrador" => $codcalibrador, ":codemp" => $codemp, ":codsuc" => $sucursal, ":nombres" => $nombres, ":codtipodocumento" => $tipodocumento,
            ":estareg" => $estareg, ":nrodocumento" => $nrodocumento));
        break;
    case 1:
        $sql = "UPDATE micromedicion.calibrador SET nombres=:nombres,
                codtipodocumento=:codtipodocumento,nrodocumento=:nrodocumento,estareg=:estareg
                WHERE codsuc=:codsuc AND codemp=:codemp AND codcalibrador=:codcalibrador";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codcalibrador" => $codcalibrador, ":codemp" => $codemp, ":codsuc" => $sucursal, ":nombres" => $nombres, ":codtipodocumento" => $tipodocumento,
            ":estareg" => $estareg, ":nrodocumento" => $nrodocumento));
        break;
    case 2:case 3:
        $sql = "UPDATE micromedicion.calibrador SET estareg=:estareg
                WHERE codemp=:codemp and codcalibrador=:codcalibrador";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codcalibrador" => $codcalibrador, ":estareg" => $estareg, ":codemp" => $codemp));
        break;
}

if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    echo $res = 2;
} else {
    $conexion->commit();
    $mensaje = "El Registro se ha Grabado Correctamente";
    echo $res = 1;
}
?>
