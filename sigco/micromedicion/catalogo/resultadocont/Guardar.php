<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();
    $codresulcont = $_POST["codresulcont"];
    
    $descripcion = strtoupper($_POST["descripcion"]);    
    $estareg = $_POST["estareg"];


    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("resultadocontrastacion", "0", "0");
            $codresulcont = $id[0];

            $sql = "INSERT INTO public.resultadocontrastacion(codresulcont,descripcion,estareg) 
                    values(:codresulcont,:descripcion,:estareg)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codresulcont" => $codresulcont,
                ":descripcion" => $descripcion,                
                ":estareg" => $estareg ));
            break;
        case 1:
            $sql = "UPDATE public.resultadocontrastacion SET
                        descripcion=:descripcion,                            
                        estareg=:estareg 
                WHERE codresulcont=:codresulcont";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codresulcont" => $codresulcont,
                ":descripcion" => $descripcion,                
                ":estareg" => $estareg ));

            break;
        case 2:case 3:
            $sql = "update public.resultadocontrastacion 
                set estareg=:estareg
                where codresulcont=:codresulcont";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codresulcont" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
