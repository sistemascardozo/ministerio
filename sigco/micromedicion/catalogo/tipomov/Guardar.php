<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();
    $codtipomov = $_POST["codtipomov"];
    
    $descripcion = strtoupper($_POST["descripcion"]);    
    $estareg = $_POST["estareg"];


    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("tipomovimiento", "0", "0");
            $codtipomov = $id[0];

            $sql = "INSERT INTO public.tipomovimiento(codtipomov,descripcion,estareg) 
                    values(:codtipomov,:descripcion,:estareg)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipomov" => $codtipomov,
                ":descripcion" => $descripcion,                
                ":estareg" => $estareg ));
            break;
        case 1:
            $sql = "UPDATE public.tipomovimiento SET
                        descripcion=:descripcion,                            
                        estareg=:estareg 
                WHERE codtipomov=:codtipomov";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipomov" => $codtipomov,
                ":descripcion" => $descripcion,                
                ":estareg" => $estareg ));

            break;
        case 2:case 3:
            $sql = "update public.tipomovimiento 
                set estareg=:estareg
                where codtipomov=:codtipomov";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipomov" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
