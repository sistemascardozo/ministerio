<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();

    $codtipoensayocaudal = $_POST["codtipoensayocaudal"];
    $descripcion = $_POST["descripcion"];
    $tiporetiro = $_POST["tiporetiro"];
    $volumenpatronenservicio = $_POST["volumenpatronenservicio"];
    if($volumenpatronenservicio=='') $volumenpatronenservicio=0;
    $volumenpatronnuevos = $_POST["volumenpatronnuevos"];
    if($volumenpatronnuevos=='') $volumenpatronnuevos=0;
    $permisible = $_POST["permisible"];
    
    $estareg = $_POST["estareg"];


    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("tipoensayocaudal", "0", "0");
            $codtipoensayocaudal = $id[0];

            $sql = "INSERT INTO public.tipoensayocaudal(codtipoensayocaudal,descripcion,volumenpatronnuevos, ";
            $sql .="volumenpatronenservicio,tiporetiro,  estareg,permisible) ";
            $sql .="values(:codtipoensayocaudal,:descripcion,:volumenpatronnuevos,:volumenpatronenservicio,:tiporetiro, ";
            $sql .=" :estareg,:permisible)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipoensayocaudal" => $codtipoensayocaudal,
                ":volumenpatronnuevos" => $volumenpatronnuevos,
                ":descripcion" => $descripcion,
                ":tiporetiro" => $tiporetiro,
                ":volumenpatronenservicio" => $volumenpatronenservicio,
                ":permisible" => $permisible,
                ":estareg" => $estareg ));

            break;
        case 1:
            $sql = "UPDATE public.tipoensayocaudal set
                        descripcion= :descripcion,
                        estareg= :estareg,
                        volumenpatronnuevos= :volumenpatronnuevos,
                        tiporetiro= :tiporetiro,
                        volumenpatronenservicio= :volumenpatronenservicio,
                        permisible= :permisible

                WHERE codtipoensayocaudal=:codtipoensayocaudal";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipoensayocaudal" => $codtipoensayocaudal,
                ":volumenpatronnuevos" => $volumenpatronnuevos,
                ":descripcion" => $descripcion,
                ":tiporetiro" => $tiporetiro,
                ":volumenpatronenservicio" => $volumenpatronenservicio,
                ":permisible" => $permisible, 
                ":estareg" => $estareg ));

            break;
        case 2:case 3:
            $sql = "update public.tipoensayocaudal set estareg=:estareg
                WHERE codtipoensayocaudal=:codtipoensayocaudal";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipoensayocaudal" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
    
?>
