<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  $TituloVentana = "TIPO DE ENSAYO DE CAUDAL";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  $FormatoGrilla = array ();
  
  $Sql = "SELECT tp.codtipoensayocaudal, ";
  $Sql .= " tp.descripcion, ";
  $Sql .= " CASE tp.tiporetiro WHEN 1 THEN 'LABORATORIO' WHEN 2 THEN 'CAMPO' ELSE '' END, ";
  $Sql .= " tp.volumenpatronenservicio, tp.permisible, tp.volumenpatronnuevos, e.descripcion, tp.estareg ";
  $Sql .= "FROM public.tipoensayocaudal tp INNER JOIN public.estadoreg e ON(e.id = tp.estareg) ";
        //ma.descripcion,
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL   //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'tp.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                   //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Descripci&oacute;n', 'T3'=>'Tipo Contrastacion',
        'T4'=>'Volumen (S)','T5'=>'Permisible','T6'=>'Volumen (N)','T7'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'right', 'A5'=>'right', 'A6'=>'right', 'A7'=>'center');  //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'70', 'W3'=>'150','W4'=>'100','W5'=>'80','W6'=>'80','W7'=>'80'); //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 950;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  ORDER BY tp.descripcion ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'8', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'8', 
              'BtnCV3'=>'0');
              
    $_SESSION['Formato'] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 750, 350);
    Pie();
    CuerpoInferior();
?>
