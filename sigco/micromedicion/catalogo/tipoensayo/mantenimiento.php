<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsMantenimiento.php");

    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
    $guardar = "op=" . $Op;

    $objMantenimiento = new clsMantenimiento();

    if ($Id != '') {
        $sql = "SELECT * FROM public.tipoensayocaudal WHERE codtipoensayocaudal= ? ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($Id));
        $row = $consulta->fetch();
        $guardar = $guardar . "&Id2=" . $Id;
    }
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    function ValidarForm(Op)
    {
        if ($("#descripcion").val() == '')
        {
            alert('El nombre del Medidor no puede ser NULO');
            return false;
        }

        if ($("#codalmacen").val() == '')
        {
            alert('Asigne un almacen al Medidor');
            return false;
        }


        GuardarP(Op);
    }
    function CambiarEstado(Obj)
    {
        if (Obj.checked == true)
        {
            $("#EstaReg").val(1)
        } else {
            $("#EstaReg").val(0)
        }
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td width="30" class="CampoDetalle">&nbsp;</td>
                    <td >&nbsp;</td>

                </tr>
                <tr>
                    <td class="TitDetalle">Id</td>
                    <td align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="codtipoensayocaudal" type="text" id="Id" size="4" readonly="readonly" value="<?php echo $Id; ?>" class="inputtext"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Tipo de Contrastación</td>
                    <td align="center">:</td>
                    <td class="CampoDetalle">
                        <select id="tiporetiro" name="tiporetiro" style="width:220px" class="select" >                            
                            <option value="1" <?=$row["tiporetiro"] == 1 ? "selected='selected'" : ""?> >LABORATORIO</option>
                            <option value="2" <?=$row["tiporetiro"] == 2 ? "selected='selected'" : ""?> >CAMPO</option>                           
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Descripcion</td>
                    <td align="center">:</td>
                    <td class="CampoDetalle">
                        <input name="descripcion" type="text" id="descripcion" maxlength="200" value="<?=isset($row["descripcion"]) ? $row["descripcion"] : '' ?>" style="width:300px;"/>
                    </td>
                </tr>                
                <tr>
                    <td class="TitDetalle">Volumen patron para medidores en servicio</td>
                    <td align="center">:</td>
                    <td class="CampoDetalle">
                        <input class="inputtext" name="volumenpatronenservicio" type="text" id="volumenpatronenservicio" size="10" maxlength="200" value="<?=isset($row["volumenpatronenservicio"]) ? $row["volumenpatronenservicio"] : '0.00' ?>"/>
                    </td>
                </tr>
                
                <tr>
                    <td class="TitDetalle">Volumen patron para medidores nuevos</td>
                    <td align="center">:</td>
                    <td class="CampoDetalle">
                        <input class="inputtext" name="volumenpatronnuevos" type="text" id="volumenpatronnuevos" size="10" value="<?=isset($row["volumenpatronnuevos"]) ? $row["volumenpatronnuevos"] : '0.00' ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Valor Permisible (%)</td>
                    <td align="center">:</td>
                    <td class="CampoDetalle">
                        <input class="inputtext" name="permisible" type="text" id="permisible" size="10" value="<?=isset($row["permisible"]) ? $row["permisible"] : '0.00' ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Estado</td>
                    <td align="center">:</td>
                    <td class="CampoDetalle">
                        <?php include("../../../../include/estareg.php"); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                    <td >&nbsp;</td>

                </tr>
            </tbody>

        </table>
    </form>
</div>
<script>
    $("#descripcion").focus();
</script>
