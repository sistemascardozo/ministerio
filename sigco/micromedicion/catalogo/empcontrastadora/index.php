<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");

    $TituloVentana = "EMPRESA CONTRASTADORA";
    $Activo = 1;

    CuerpoSuperior($TituloVentana);
    $Op = isset($_GET['Op'])?$_GET['Op']:0;
    $codsuc = $_SESSION['IdSucursal'];

    $FormatoGrilla = array ();
    $Sql = "SELECT codempcont,ruc,razonsocial,telefono,1
            FROM micromedicion.empresacontrastadora ";
    $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL
    $FormatoGrilla[1] = array('1'=>'ruc', '2'=>'razonsocial');          //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;                                                            //Operacion
    $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'RUC','T3'=>'Raz&oacute;n Social','T4'=>'Tel&eacute;fono');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'right');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1'=>'60');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = " AND codsuc=".$codsuc." ORDER BY razonsocial  ASC ";                                   //Orden de la Consulta
    $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
                'NB'=>'3',          //Número de Botones a agregar
                'BtnId1'=>'BtnModificar',   //Nombre del Boton
                'BtnI1'=>'modificar.png',   //Imagen a mostrar
                'Btn1'=>'Editar',       //Titulo del Botón
                'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
                'BtnCI1'=>'5',  //Item a Comparar
                'BtnCV1'=>'1',
                'BtnCI1' => '5', //Item a Comparar
                'BtnCV1' => '1', //Valor de comparación
                'BtnId2' => 'BtnEliminar',
                'BtnI2' => 'eliminar.png',
                'Btn2' => 'Eliminar',
                'BtnF2' => 'onclick="Eliminar(this.id)"',
                'BtnCI2' => '5', //campo 3
                'BtnCV2' => '1', //igua a 1
                'BtnId3' => 'BtnRestablecer', //y aparece este boton
                'BtnI3' => 'restablecer.png',
                'Btn3' => 'Restablecer',
                'BtnF3' => 'onclick="Restablecer(this.id)"',
                'BtnCI3' => '5',
                'BtnCV3' => '0'    //Valor de comparación
            );
                
    $_SESSION['Formato'] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 800, 360);
    Pie();
    CuerpoInferior();
?>
<script type="text/javascript">	
	//$('#BtnNuevoB').css('display', 'none');
</script>