<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");
	
	$Op = $_POST["Op"];
    $Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
    $guardar = "op=" . $Op;

    $codemp = 1;
    $IdUbigeo = '250101';
    $objMantenimiento = new clsDrop();

    if ($Id != '') {
        $consulta = $conexion->prepare(" SELECT * FROM micromedicion.empresacontrastadora WHERE codempcont= ? ");
		$consulta->execute(array($Id));
		$items = $consulta->fetch(); 

		$IdUbigeo = $items["codubigeo"];      
        $guardar = $guardar."&Id2=".$Id;
    }
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var urldir	= '<?php echo $_SESSION['urldir'];?>';

	$(document).ready(function() {

		cargar_provincia('<?=substr($IdUbigeo, 0, 2)."0000"?>', '<?=$IdUbigeo?>')
        cargar_distrito('<?=substr($IdUbigeo, 0, 4)."00"?>', '<?=$IdUbigeo?>');
	});

	function ValidarForm(Op)
	{
		if (document.form1.empresa.value == '')
		{
			alert('La Razon Social de la Empresa no puede ser NULO');
			document.form1.empresa.focus();
			return false;
		}
		if (document.form1.direccion.value == '')
		{
			alert('La Direccion de la Empresa no puede ser NULO');
			document.form1.direccion.focus();
			return false;
		}
		if (document.form1.ruc.value == '')
		{
			alert('El RUC de la Empresa no puede ser NULO');
			document.form1.ruc.focus();
			return false;
		}
		
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='../indexB.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
	<table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
				<td width="80" class="TitDetalle">&nbsp;</td>
				<td width="30" class="TitDetalle">&nbsp;</td>
				<td class="CampoDetalle">&nbsp;</td>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td class="TitDetalle" align="right">Id </td>
				<td class="TitDetalle" align="center">:</td>
				<td class="CampoDetalle">
					<input name="codempcont" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?=$items["codempcont"]?>" class="inputtext"/>
				</td>
				<td colspan="3">&nbsp;</td>
			</tr>	
			<tr>
                <td class="TitDetalle" align="right">Sucursal </td>
                <td class="TitDetalle" align="center">:</td>
                <td class="CampoDetalle">
                    <select name="sucursal" id="sucursal" style="width:200px">
                        <?php
                        $Sql = "SELECT * FROM admin.sucursales WHERE estareg = 1 AND codemp=".$codemp." ";
                        $Consulta = $conexion->query($Sql);
                        foreach ($Consulta->fetchAll() as $row1) {
                            $Sel = "";
                            if ($items["codsuc"] == $row1["codsuc"])
                                $Sel = 'selected="selected"';
                            ?>
                            <option value="<?php echo $row1["codsuc"]; ?>" <?= $Sel ?>  ><?php echo $row1["descripcion"]; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td width="30" align="right" class="TitDetalle">Empresa</td>
                <td width="30" align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="razon_social" type="text" id="empresa" maxlength="200" value="<?=$items["razonsocial"]?>" style="width:200px;"/>
				</td>
				<td colspan="2">&nbsp;</td>
            </tr>			
			<tr>
				<td class="TitDetalle" align="right">Direccion </td>
				<td class="TitDetalle" align="center">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="direccion" type="text" id="direccion" size="30" maxlength="200" value="<?=$items["direccion"]?>"/>
				</td>
				<td class="TitDetalle" align="right">Nro. Calle </td>
				<td class="TitDetalle" align="center">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="nrocalle" type="text" id="nrocalle" size="20" maxlength="20" value="<?=$items["nrocalle"]?>"/>
				</td>
			</tr>
			<tr>
				<td class="TitDetalle" align="right">Barrio </td>
				<td class="TitDetalle" align="center">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="barrio" type="text" id="barrio" size="20" maxlength="20" value="<?=$items["barrio"]?>"/>
				</td>
				<td class="TitDetalle" align="right">Manzana </td>
				<td class="TitDetalle" align="center">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="manzana" type="text" id="manzana" size="20" maxlength="20" value="<?=$items["manzana"]?>"/>
				</td>				
			</tr>			
			<tr>
				<td class="TitDetalle" align="right">Lote </td>
				<td class="TitDetalle" align="center">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="lote" type="text" id="lote" size="20" maxlength="20" value="<?=$items["lote"]?>"/>
				</td>
				<td class="TitDetalle" align="right">RUC </td>
				<td class="TitDetalle" align="center">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="ruc" type="text" id="ruc" size="20" maxlength="20" value="<?=$items["ruc"]?>"/>
				</td>
				
			</tr>
			<tr>
				<td class="TitDetalle" align="right">Telefono</td>
				<td class="TitDetalle" align="center">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="telefono" type="text" id="telefono" size="20" maxlength="20" value="<?=$items["telefono"]?>"/>
				</td>
                <td class="TitDetalle" align="right">Departamento </td>
                <td class="TitDetalle" align="center">:</td>
                <td class="CampoDetalle">
                    <?php $objMantenimiento->drop_ubigeo("departamento", "--Seleccione el Departamento--",
						  " where codubigeo like ? order by codubigeo",array("%0000"),
						  "onchange='cargar_provincia(this.value);'", substr($IdUbigeo, 0, 2)."0000"); 
                    ?>
                </td>
                
            </tr>
            <tr>
            	<td class="CampoDetalle" align="right">Provincia </td>
            	<td class="CampoDetalle" align="center">:</td>
                <td class="CampoDetalle">
                    <div id="div_provincia">
                        <select id="provincia" name="provincia" style="width:220px" class="select">
                            <option value="0">--Seleccione la Provincia--</option>
                        </select>
                    </div>
                </td>
                <td class="TitDetalle" align="right">Distrito</td>
                <td class="TitDetalle" align="center">:</td>
                <td class="CampoDetalle">
                    <div id="div_distrito">
                        <select id="distrito" name="distrito" style="width:220px" class="select">
                            <option value="0">--Seleccione un Distrito--</option>
                        </select>
                    </div>
                </td>
               
            </tr>
			<tr>
		        <td class="TitDetalle" align="right">Estado</td>
		        <td class="TitDetalle" align="center">:</td>
		        <td class="CampoDetalle">
		            <?php include("../../../../include/estareg.php"); ?>
		        </td>
		        <td colspan="3">&nbsp;</td>
		    </tr>
		    <tr>
		        <td class="TitDetalle">&nbsp;</td>
		        <td class="TitDetalle">&nbsp;</td>
		        <td class="CampoDetalle">&nbsp;</td>
		        <td colspan="3">&nbsp;</td>
		    </tr>
		</tbody>
	</table>
 </form>
</div>