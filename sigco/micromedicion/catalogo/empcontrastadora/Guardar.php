<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	$Op = $_GET["Op"];

    $objFunciones = new clsFunciones();

	$codempcont = $_POST["codempcont"] ? $_POST["codempcont"] : $_POST["1form1_id"];
	$razonsocial= $_POST["razon_social"];
	$direccion 	= $_POST["direccion"];
	$ruc		= $_POST["ruc"];
	$telefono	= $_POST["telefono"];
	$estareg    = $_POST["estareg"];
	$sucursal   = $_POST["sucursal"];

    $nrocalle   = $_POST["nrocalle"];
    $manzana    = $_POST["manzana"];
    $lote       = $_POST["lote"];
    $codubigeo  = $_POST["distrito"];
    $barrio   = $_POST["barrio"];

	switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("empresacont", $sucursal, "0");
            $codempcont = $id[0];
            $sql = "INSERT INTO micromedicion.empresacontrastadora(codempcont,telefono,codsuc,razonsocial,
                direccion,ruc,estareg,nrocalle,manzana,lote,barrio,codubigeo ) 
                VALUES(:codempcont,:telefono,:codsuc,:razonsocial,:direccion,:ruc,:estareg,
                :nrocalle,:manzana,:lote,:barrio,:codubigeo)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codempcont"=>$codempcont, ":codsuc"=>$sucursal, ":telefono"=>$telefono,
            	":razonsocial"=>$razonsocial, ":direccion"=>$direccion, ":ruc"=>$ruc,":estareg"=>$estareg,
                ":nrocalle"=>$nrocalle,":manzana"=>$manzana,":lote"=>$lote,":barrio"=>$barrio,":codubigeo"=>$codubigeo ));
            break;
        case 1:
            $sql = "UPDATE micromedicion.empresacontrastadora SET 
            	razonsocial=:razonsocial, telefono=:telefono,
                direccion=:direccion,ruc=:ruc, estareg=:estareg,
                nrocalle= :nrocalle,manzana= :manzana,lote= :lote,barrio= :barrio, codubigeo=:codubigeo
                WHERE codsuc=:codsuc and codempcont=:codempcont ";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codempcont"=>$codempcont,":codsuc"=>$sucursal, 
            	":razonsocial"=>$razonsocial, ":direccion"=>$direccion, ":estareg"=>$estareg, 
                ":ruc"=>$ruc, ":telefono"=>$telefono,
                ":nrocalle"=>$nrocalle,":manzana"=>$manzana,":lote"=>$lote,":barrio"=>$barrio,":codubigeo"=>$codubigeo ));
            break;
        case 2:case 3:
            $sql = "UPDATE micromedicion.empresacontrastadora SET estareg=:estareg
                WHERE codsuc=:codsuc and codempcont=:codempcont";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codempcont"=>$codempcont, ":estareg"=>$estareg, ":codsuc"=>$sucursal));
            break;
    }

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }

?>
