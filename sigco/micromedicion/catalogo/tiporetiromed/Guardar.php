<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();
    $codtiporetiromed = $_POST["codtiporetiromed"];
    
    $descripcion = strtoupper($_POST["descripcion"]);    
    $estareg = $_POST["estareg"];


    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("tiporetiromedidor", "0", "0");
            $codtiporetiromed = $id[0];

            $sql = "INSERT INTO micromedicion.tiporetiromedidor(codtiporetiromed,descripcion,estareg) 
                    values(:codtiporetiromed,:descripcion,:estareg)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtiporetiromed" => $codtiporetiromed,
                ":descripcion" => $descripcion,                
                ":estareg" => $estareg ));
            break;
        case 1:
            $sql = "UPDATE micromedicion.tiporetiromedidor SET
                        descripcion=:descripcion,                            
                        estareg=:estareg 
                WHERE codtiporetiromed=:codtiporetiromed";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtiporetiromed" => $codtiporetiromed,
                ":descripcion" => $descripcion,                
                ":estareg" => $estareg ));

            break;
        case 2:case 3:
            $sql = "update micromedicion.tiporetiromedidor 
                set estareg=:estareg
                where codtiporetiromed=:codtiporetiromed";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtiporetiromed" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
