<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsMantenimiento.php");

    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
    $guardar = "op=" . $Op;

    $objMantenimiento = new clsMantenimiento();

    if ($Id != '') {
        
        $SQL= "SELECT * FROM micromedicion.clasemetrologica AS c WHERE c.codclasemetrologica= ?";
        $consulta = $conexion->prepare($SQL);
        $consulta->execute(array($Id));
        $row = $consulta->fetch();

        //print_r($row['descripcion']);

        $guardar = $guardar . "&Id2=" . $Id;
    }
    
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    function ValidarForm(Op)
    {
        if ($("#descripcion").val() == '')
        {
            alert('La Descripcion del Medidor no puede ser NULO');
            return false;
        }

        GuardarP(Op);
    }

</script>

<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
                <tr>
                    <td class="TitDetalle">Id :</td>
                    <td class="CampoDetalle">
                        <input name="codclasemetrologica" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
                    </td>
                </tr>	
                <tr>
                    <td class="TitDetalle">Descripcion :</td>
                    <td class="CampoDetalle">
                        <input class="inputtext" name="descripcion" type="text" id="Nombre" size="70" maxlength="200"  value="<?= isset($row['descripcion']) ? $row['descripcion'] : '' ?>"/>	    
                    </td>
                </tr>             
                <tr>
                    <td class="TitDetalle">Estado :</td>
                    <td class="CampoDetalle">
                        <?php include("../../../../include/estareg.php"); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>

        </table>
    </form>
</div>
<script>
    $("#Nombre").focus();
</script>
<?php
$est = isset($row["estareg"]) ? $row["estareg"] : 1;
include("../../../../admin/validaciones/estareg.php");
?>