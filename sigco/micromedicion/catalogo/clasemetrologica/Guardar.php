<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones        = new clsFunciones();
    $codclasemetrologica = $_POST["codclasemetrologica"];
    $descripcion         = strtoupper($_POST["descripcion"]);
    $estareg             = $_POST["estareg"];


    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("clasemetrologica", "0", "0");
            $codclasemetrologica = $id[0];

            $sql = "INSERT INTO micromedicion.clasemetrologica (codclasemetrologica,descripcion,estareg) 
                    values(:codclasemetrologica,:descripcion,:estareg)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codclasemetrologica" => $codclasemetrologica,
                ":descripcion" => $descripcion,
                ":estareg" => $estareg));


            break;
        case 1:
            $sql = "UPDATE micromedicion.clasemetrologica SET
                            descripcion=:descripcion,                        
                            estareg=:estareg 
                WHERE codclasemetrologica=:codclasemetrologica";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codclasemetrologica"=>$codclasemetrologica,
                ":descripcion"=>$descripcion,
                ":estareg"=>$estareg));

            break;
        case 2:case 3:
            $sql = "update micromedicion.codclasemetrologica set estareg=:estareg
                                    where codclasemetrologica=:codclasemetrologica";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codclasemetrologica" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
