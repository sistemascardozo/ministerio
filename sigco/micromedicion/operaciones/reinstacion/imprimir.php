<?php
    include("../../../../objetos/clsReporte.php");

    class clsSolicitud extends clsReporte {

        function cabecera() {            
            $this->Cell(190, 5, utf8_decode($tit1." N° 0"), 0, 1, 'C');
            $this->Ln(2);
            $this->SetFont('Arial', 'B', 14);
            $tit1 = "FORMATO";
            $this->Cell(190, 5, utf8_decode($tit1." N° 0"), 0, 1, 'C');
            $this->SetFont('Arial', '', 9);
            $this->Cell(190, 5, utf8_decode("Acta de Instalación del Medidor de Agua Potable"), 0, 1, 'C');
            $this->Cell(0,0,'',1,1,'C');
            $this->Ln(5);
        }

        function Contenido($cod, $propietario, $direccion, $nrocalle, $codsuc, $codzona, $codsector, $ruta, $tipomov,$nrosumi, $emp,$suc,
            $rnomed,$marca,$diametro,$posicion,$tipolec,$anio,$fecha,$estado,$modelo,$tipo,$capacidad,$clase,$tipofac,
                $traslado,$filtro,$precinto,$filtroest,$observacion,$consumo) 
        {

            $h = 4;
            $this->SetFont('Arial', '', 8);
            $this->SetTextColor(0, 0, 0);
            
            $text="Siendo las ";            
            $text2= "la Entidad Constrastadora";
            $text1= " ha procedido a:";
            
            $nrosuc = substr("00", 0, 5 - strlen($codsuc)) . $codsuc;
            $nrosec = substr("00", 0, 5 - strlen($codsector)) . $codsector;
            $fech= date('d-m-y');
            $horareg  = date('H:i:s');
            
            $this->Cell(0,5,utf8_decode($text."".$horareg." horas, del día ".$fech.", ".$text2." ".strtoupper($emp).", ".$text1),0, 1, 'J');
            $x 	= 130;
            $y 	= 49;
            $this->SetXY($x,$y);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(10, $h, "Suc :", 0, 0, 'R');
            $this->Cell(7, $h, $nrosuc, 0, 0, 'R');
            $this->Cell(15, $h, "Sector :", 0, 0, 'R');
            $this->Cell(7, $h, $nrosec, 0, 0, 'R');
            $this->Cell(13, $h, "Ruta :", 0, 0, 'R');
            $this->Cell(13, $h, $ruta, 0, 0, 'R');
            $es= 12;
            $hc= 5;
            $xx=56;
            
            $this->SetFont('Arial', '', 8);
            $this->Rect(10, $xx, 190, $hc, 'D');
            $this->SetXY(10, $xx);
            $this->Cell(53,$hc,utf8_decode('Empresa Prestadora'),0,0,'L');
            $this->Cell(85,$hc,strtoupper($emp),0,0,'L');
            $this->Rect(10, $xx+$hc, 190, $hc, 'D');
            $this->SetXY(10, $xx+$hc);
            $this->Cell(53,$hc,utf8_decode('Localidad o Centro de Servicio'),0,0,'L');
            $this->Cell(85,$hc,strtoupper($suc),0,0,'L');
            $this->Ln($es);            
            
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(0, $h, utf8_decode("Motivo de la Instalación del Medidor :"), 0, 1, 'L');
            
            $this->SetFont('Arial', '', 8);
            $this->Rect(10, $xx+$hc+$es+$h, 190, 5, 'D');
            $this->SetXY(10, $xx+$hc+$es+$h);
            $this->Cell(13,5,utf8_decode(''),0,0,'L');
            $this->Cell(85,5,strtoupper('INSTALACION DE MEDIDOR DE AGUA'),0,0,'L');
            $this->Ln($es);
            
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(0, $h, utf8_decode("Informacion de titular :"), 0, 1, 'L');
            
            $this->SetFont('Arial', '', 8);
            $this->Rect(10, $xx+$hc+(2*$es)+(2*$h), 190, 5, 'D');
            $this->SetXY(10, $xx+$hc+(2*$es)+(2*$h));
            $this->Cell(30,$hc,utf8_decode('Nombres'),0,0,'L');
            $this->Cell(85,$hc,strtoupper($propietario),0,0,'L');
            
            $this->Rect(10, $xx+(2*$hc)+(2*$es)+(2*$h), 190, 5, 'D');
            $this->SetXY(10, $xx+(2*$hc)+(2*$es)+(2*$h));
            $this->Cell(30,$hc,utf8_decode('Dirección'),0,0,'L');
            $this->Cell(75,$hc,strtoupper($direccion),0,0,'L');
            $this->Cell(15,$hc,utf8_decode('Número :'),0,0,'L');
            $this->Cell(75,$hc,strtoupper($nrocalle),0,0,'L');
            
            $this->Rect(10, $xx+(3*$hc)+(2*$es)+(2*$h), 190, 5, 'D');
            $this->SetXY(10, $xx+(3*$hc)+(2*$es)+(2*$h));
            $this->Cell(30,$hc,utf8_decode('Nro. Suministro'),0,0,'L');
            $this->Cell(85,$hc,strtoupper($nrosumi),0,0,'L');
            $this->Ln($es);           
            
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(0, $h, utf8_decode("Informacion del Medidor :"), 0, 1, 'L');            
            
            $this->SetFont('Arial', '', 8);
            $this->Rect(10, $xx+(3*$hc)+(3*$es)+(3*$h), 95, 5, 'D');
            $this->SetXY(10, $xx+(3*$hc)+(3*$es)+(3*$h));
            $this->Cell(30,$hc,utf8_decode('Nro. Medidor'),0,0,'L');
            $this->Cell(65,$hc,strtoupper($rnomed),0,0,'L');
            $this->Rect(105, $xx+(3*$hc)+(3*$es)+(3*$h), 95, 5, 'D');
            $this->SetXY(105, $xx+(3*$hc)+(3*$es)+(3*$h));
            $this->Cell(15,$hc,utf8_decode('Diametro :'),0,0,'L');
            $this->Cell(80,$hc,strtoupper($diametro),0,0,'L');
            
            $this->Rect(10, $xx+(4*$hc)+(3*$es)+(3*$h), 95, 5, 'D');
            $this->SetXY(10, $xx+(4*$hc)+(3*$es)+(3*$h));
            $this->Cell(30,$hc,utf8_decode('Marca de Medidor'),0,0,'L');
            $this->Cell(65,$hc,strtoupper($marca),0,0,'L');
            $this->Rect(105, $xx+(4*$hc)+(3*$es)+(3*$h), 95, 5, 'D');
            $this->SetXY(105, $xx+(4*$hc)+(3*$es)+(3*$h));
            $this->Cell(25,$hc,utf8_decode('Clase Metrológica :'),0,0,'L');
            $this->Cell(80,$hc,strtoupper($clase),0,0,'L');
                
            $this->Rect(10, $xx+(5*$hc)+(3*$es)+(3*$h), 95, 5, 'D');
            $this->SetXY(10, $xx+(5*$hc)+(3*$es)+(3*$h));
            $this->Cell(30,$hc,utf8_decode('Modelo de Medidor'),0,0,'L');
            $this->Cell(45,$hc,strtoupper($modelo),0,0,'L');
            $this->Cell(10,$hc,utf8_decode('Año :'),0,0,'L');
            $this->Cell(10,$hc,strtoupper($anio),0,0,'L');
            $this->Rect(105, $xx+(5*$hc)+(3*$es)+(3*$h), 95, 5, 'D');
            $this->SetXY(105, $xx+(5*$hc)+(3*$es)+(3*$h));
            $this->Cell(31,$hc,utf8_decode('Capacidad de Medidor :'),0,0,'L');
            $this->Cell(64,$hc,strtoupper($capacidad),0,0,'L');
            
            
            $this->Rect(10, $xx+(6*$hc)+(3*$es)+(3*$h), 190, 5, 'D');
            $this->SetXY(10, $xx+(6*$hc)+(3*$es)+(3*$h));
            $this->Cell(30,$hc,utf8_decode('Estado del Registro'),0,0,'L');
            $this->Cell(47,$hc,("El medidor registra un volumen de "),0,0,'L');
            $this->Cell(18,$hc,strtoupper($consumo),0,0,'L');            
            $this->Cell(0,$hc,utf8_decode('m3'),0,0,'L');
            
            
            $this->Rect(10, $xx+(7*$hc)+(3*$es)+(3*$h), 190, 5, 'D');
            $this->SetXY(10, $xx+(7*$hc)+(3*$es)+(3*$h));
            $this->Cell(130,$hc,utf8_decode('El medidor se traslado en una bolsa cerrada con precinto de seguridad'),0,0,'L');
            if($traslado== 1)
            {
                $this->Cell(6,$hc,"[ X ]",0,0,'C');
                $this->Cell(12,$hc,"SI",0,0,'L');
                $this->Cell(6,$hc,"[   ]",0,0,'C');
                $this->Cell(6,$hc,"NO",0,0,'C');
                $this->Ln($es);
            }else
                {
                    $this->Cell(6,$hc,"[   ]",0,0,'C');
                    $this->Cell(12,$hc,"SI",0,0,'L');
                    $this->Cell(6,$hc,"[ X ]",0,0,'C');
                    $this->Cell(6,$hc,"NO",0,0,'C');
                    $this->Ln($es);
                }          
                            
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(0, $h, utf8_decode("Reporte visual del Medidor :"), 0, 1, 'L');
            
            $this->SetFont('Arial', '', 8);
            $this->Rect(10, $xx+(7*$hc)+(4*$es)+(4*$h), 190, 5, 'D');
            $this->SetXY(10, $xx+(7*$hc)+(4*$es)+(4*$h));
            $this->Cell(130,$hc,utf8_decode('Medidor con Precinto de Seguridad'),0,0,'L');
            if($precinto== 1)
            {
                $this->Cell(6,$hc,"[ X ]",0,0,'C');
                $this->Cell(12,$hc,"SI",0,0,'L');
                $this->Cell(6,$hc,"[   ]",0,0,'C');
                $this->Cell(6,$hc,"NO",0,0,'C');
                $this->Ln($es);
            }else
                {
                    $this->Cell(6,$hc,"[   ]",0,0,'C');
                    $this->Cell(12,$hc,"SI",0,0,'L');
                    $this->Cell(6,$hc,"[ X ]",0,0,'C');
                    $this->Cell(6,$hc,"NO",0,0,'C');
                    $this->Ln($es);
                }
            
            $this->Rect(10, $xx+(8*$hc)+(4*$es)+(4*$h), 190, 5, 'D');
            $this->SetXY(10, $xx+(8*$hc)+(4*$es)+(4*$h));
            $this->Cell(130,$hc,utf8_decode('Medidor con Filtro'),0,0,'L');
            if($filtro== 1)
            {
                $this->Cell(6,$hc,"[ X ]",0,0,'C');
                $this->Cell(12,$hc,"SI",0,0,'L');
                $this->Cell(6,$hc,"[   ]",0,0,'C');
                $this->Cell(6,$hc,"NO",0,0,'C');
                $this->Ln($es);
            }else
                {
                    $this->Cell(6,$hc,"[   ]",0,0,'C');
                    $this->Cell(12,$hc,"SI",0,0,'L');
                    $this->Cell(6,$hc,"[ X ]",0,0,'C');
                    $this->Cell(6,$hc,"NO",0,0,'C');
                    $this->Ln($es);
                }
            
            $this->Rect(10, $xx+(9*$hc)+(4*$es)+(4*$h), 190, 5, 'D');
            $this->SetXY(10, $xx+(9*$hc)+(4*$es)+(4*$h));
            $this->Cell(130,$hc,utf8_decode('El Filtro esta en buen Estado de Conservación'),0,0,'L');
            if($filtroest== 1)
            {
                $this->Cell(6,$hc,"[ X ]",0,0,'C');
                $this->Cell(12,$hc,"SI",0,0,'L');
                $this->Cell(6,$hc,"[   ]",0,0,'C');
                $this->Cell(6,$hc,"NO",0,0,'C');
                $this->Ln($es);
            }else
                {
                    $this->Cell(6,$hc,"[   ]",0,0,'C');
                    $this->Cell(12,$hc,"SI",0,0,'L');
                    $this->Cell(6,$hc,"[ X ]",0,0,'C');
                    $this->Cell(6,$hc,"NO",0,0,'C');
                    $this->Ln($es);
                }
            $this->Ln($es);
            
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(0, $h, utf8_decode("Observacion :"), 0, 1, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Rect($this->GetX(), $this->GetY(), 190, 30);

            $this->MultiCell(190, 5, strtoupper($observacion), 0, 'L');

            
        }

    }

    $id = $_GET["Id"];
    $codsuc = $_GET["codsuc"];
    $idemp=1;
    
    $sql="SELECT codemp, razonsocial FROM admin.empresas
    WHERE codemp= ?  ";
    $cons = $conexion->prepare($sql);
    $cons->execute(array($idemp));
    $rowemp = $cons->fetch();
    
    $sqls="SELECT codemp, descripcion FROM admin.sucursales
    WHERE codsuc= ?  AND codemp= ?";
    $consu = $conexion->prepare($sqls);
    $consu->execute(array($codsuc,$idemp));
    $rowsuc = $consu->fetch();
    //;
    $objReporte = new clsSolicitud();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    
    $titular="SELECT
        d.coddetmedidor,
        c.propietario,
        tc.descripcioncorta||' '||cl.descripcion AS direcion,
        c.nrocalle,
        c.codsuc,
        c.codzona,
        c.codsector,
        ru.descripcion,
        d.codtipomovimiento,
        c.nroinscripcion
        FROM
        micromedicion.detmedidor AS d
        INNER JOIN catastro.clientes AS c ON (c.codcliente = d.codcliente AND c.nroinscripcion = d.nroinscripcion AND c.codemp = d.codemp AND c.codsuc = d.codsuc)
        INNER JOIN public.calles AS cl ON (cl.codcalle = c.codcalle AND cl.codemp = c.codemp AND cl.codsuc = c.codsuc AND cl.codzona = c.codzona)
        INNER JOIN public.tiposcalle AS tc ON tc.codtipocalle = cl.codtipocalle
        INNER JOIN public.rutasmaedistribucion AS ru ON (ru.codrutdistribucion = c.codrutdistribucion AND ru.codemp = c.codemp AND ru.codsuc = c.codsuc AND ru.codsector= c.codsector )
        
        WHERE d.coddetmedidor=? AND c.codsuc= ?";
    $con = $conexion->prepare($titular);
    $con->execute(array($id,$codsuc));
    $rowT = $con->fetch();
    
    $medidor="SELECT
        d.coddetmedidor, d.nromed,
        ma.descripcion AS marca,
        di.descripcion AS diametro,
        d.posicionmed, d.tipolectura,
        d.aniofabmed, d.fechamov,
        es.descripcion AS estado,
        mo.descripcion AS modelo,
        tip.descripcion AS tipo,
        ca.descripcion AS capacidad,
        cla.descripcion AS clase,
        d.tipofacturacion,
        d.trasladomed,
        d.filtromed, d.precintomed,
        d.filtromedbueno, d.observacion,
        d.consumo
        FROM
        micromedicion.detmedidor AS d
        INNER JOIN public.marcamedidor AS ma ON ma.codmarca = d.marcamed
        INNER JOIN public.modelomedidor AS mo ON mo.codmodelo = d.modelomed
        INNER JOIN public.diametrosmedidor AS di ON di.coddiametrosmedidor = d.coddiametro
        INNER JOIN public.estadomedidor AS es ON es.codestadomedidor = d.estadomedidores 
        INNER JOIN public.tipomedidor AS tip ON tip.codtipomedidor = d.codtipomedidor
        INNER JOIN public.capacidadmedidor AS ca ON ca.codcapacidadmedidor = d.codcapacidadmedidor
        INNER JOIN micromedicion.clasemetrologica AS cla ON cla.codclasemetrologica = d.codclasemetrologica
        INNER JOIN public.ubicacionllavemedidor AS ub ON ub.codubicacion = d.ubicacionllavemedidor
        
        WHERE d.coddetmedidor=? AND d.codsuc= ? ";
        $conn = $conexion->prepare($medidor);
        $conn->execute(array($id,$codsuc));
        $rowM = $conn->fetch();
        
    $objReporte->Contenido($rowT[0], $rowT[1], $rowT[2], $rowT[3], $rowT[4], $rowT[5], $rowT[6], $rowT[7], $rowT[8],$rowT[9],$rowemp[1],$rowsuc[1],
        $rowM[1],$rowM[2],$rowM[3],$rowM[4],$rowM[5],$rowM[6],$rowM[7],$rowM[8],$rowM[9],$rowM[10],$rowM[11],$rowM[12],$rowM[13],
            $rowM[14],$rowM[15],$rowM[16],$rowM[17],$rowM[18],$rowM[19]);
    $objReporte->Output();
?>