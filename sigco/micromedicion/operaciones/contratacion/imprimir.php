<?php

include("../../../../objetos/clsReporte.php");

class clsFormato1 extends clsReporte {

    function Header() {
        global $codsuc;
        //DATOS DEL FORMATO
        $this->SetY(7);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "FORMATO N° 7";
        $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
        $this->Ln(1);
        $this->SetFont('Arial', '', 7);
        $hc = 5;
        $tit2 = "Solicitud de Contrastación de medidor de agua potable";
        //$tit3 = strtoupper("Comerciales no relativos a la Facturacion y Problemas Operacionales");

        $this->Cell(190, 4, utf8_decode($tit2), 0, 1, 'C');
        //$this->Cell(190, 4, $tit3, 0, 1, 'C');
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
        //DATOS DEL FORMATO
        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoupper($empresa["razonsocial"]);
        $tit2 = strtoupper($empresa["direccion"]);
        $tit3 = "SUCURSAL: " . strtoupper($empresa["descripcion"]);
        $x = 23;
        $y = 5;
        $h = 3;
        $this->Image($_SESSION['path'] . "/images/logo_empresa.jpg", 6, 1, 17, 16);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

        $this->SetY(8);
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');
        $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
        $this->Cell(0, 3, "Fecha     : " . $FechaActual . "   ", 0, 1, 'R');
        $this->Cell(0, 3, "Hora       : " . $Hora . " ", 0, 1, 'R');
        $this->SetY(17);
        $this->SetLineWidth(.1);
        //$this->Cell(0, .1, "", 1, 1, 'C');
        $this->cabecera();
    }

    function cabecera() {
        
    }

    function Contenido($codsuc, $coddetmedidor) {
        global $conexion;
        $empresa = $this->datos_empresa($codsuc);
        $h = 4;
        $s = 2;
        $x = 13;
        $this->SetFillColor(192, 192, 192);
        //$this->Rect(10, $this->GetY() + 15, 190, 240);

        $Sql = "SELECT
            de.coddetmedidor,
            de.fechamov,
            cli.codantiguo,
            cli.propietario,
            cli.nrodocumento,
            ti.descripcioncorta||' '||ca.descripcion AS direccion,
            cli.nrocalle,
            cli.codmanzanas,
            cli.lote,
            ur.descripcion,
            de.nromed,
            cli.consumo,
            de.tpcontrastacion,
            em.razonsocial,
            em.direccion AS diremp,
            em.nrocalle AS nrocaemp,
            em.manzana AS manemp,
            em.lote AS lotemp,
            em.barrio baremp,
            ub.descripcion ubemp
            FROM
            micromedicion.detmedidor AS de
            INNER JOIN catastro.clientes AS cli ON cli.nroinscripcion = de.nroinscripcion AND cli.codsuc = de.codsuc AND cli.codemp = de.codemp
            INNER JOIN public.calles AS ca ON ca.codemp = cli.codemp AND ca.codsuc = cli.codsuc AND ca.codcalle = cli.codcalle AND ca.codzona = cli.codzona
            INNER JOIN public.tiposcalle AS ti ON ti.codtipocalle = ca.codtipocalle
            INNER JOIN catastro.urbanizaciones AS ur ON ur.codurbanizacion = cli.codurbanizacion AND ur.codsuc = cli.codsuc AND ur.codzona = cli.codzona
            LEFT JOIN micromedicion.empresacontrastadora AS em ON em.codempcont = de.codempcont AND em.codsuc = de.codsuc
            LEFT JOIN public.ubigeo AS ub ON ub.codubigeo = em.codubigeo ";
        $Sql .= "WHERE de.codsuc=$codsuc and de.coddetmedidor=$coddetmedidor";

        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array());
        $row = $consulta->fetch();

        $this->Ln(5);

        $this->Ln(2);
        $this->SetX($x);
        $this->Cell(35, $h, "", 0, 0, 'L');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(20, $h, '', 0, 0, 'L');

        $this->SetX($x + 118);
        $this->Cell(40, $h, "CODIGO DEL RECLAMO Nro.", 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $Anio = substr($this->DecFecha($row['fechamov']), 6, 4);
        $this->Cell(20, $h, $Anio . " - " . str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 1, 1, 'R');

        $this->Ln(2);
        $this->SetX($x);
        $this->Cell(35, $h, "Nro. DE SUMINISTRO", 0, 0, 'L');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $codantiguo = $row['codantiguo']; //$this->CodUsuario($codsuc, $row[1]);
        $this->Cell(20, $h, $codantiguo, 1, 1, 'L');
        $this->Ln(2);
        $this->Rect(12, $this->GetY(), 184, 11);
        $this->Ln(1);
        $x = 15;
        $this->SetX($x);
        $this->Cell(90, $h, "NOMBRE DE LA EMPRESA PRESTADORA", 0, 0, 'L');
        $this->Cell(80, $h, strtoupper($empresa["razonsocial"]), 1, 1, 'L');
        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(90, $h, "LOCALIDAD O CENTRO DE SERVICIO", 0, 0, 'L');
        $this->Cell(80, $h, strtoupper($empresa["descripcion"]), 1, 1, 'L');
        $this->Ln(2);
        ////
        $this->Rect(12, $this->GetY(), 184, 58);
        $this->Ln(1);
        $this->SetX($x);
        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(190, $h, "NOMBRE DEL RECLAMANTE O REPRESENTANTE", 0, 1, 'L');
        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(178, $h, strtoupper(utf8_decode($row['propietario'])), 1, 1, 'C');
        $this->SetX($x);
        $this->Cell(178, $h, 'APELLIDOS Y NOMBRES', 1, 1, 'C');

        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(35, $h, "NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)", 0, 0, 'L');
        $this->SetX($x + 115);
        $this->Cell(40, $h, "", 0, 0, 'R');
        $this->Cell(5, $h, "", 0, 0, 'C');
        $this->Cell(18, $h, $row['nrodocumento'], 1, 1, 'L');

        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(35, $h, "RAZON SOCIAL", 0, 0, 'L');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(138, $h, "", 1, 1, 'L');

        $this->Ln(1);

        ///
        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(35, $h, "UBICACION DEL PREDIO", 0, 1, 'L');
        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(123, $h, strtoupper($row['direccion']), 1, 0, 'L');
        $this->Cell(20 , $h, strtoupper($row['nrocalle']), 1, 0, 'C');
        $this->Cell(20 , $h, $row['codmanzanas'], 1, 0, 'C');
        $this->Cell(15 , $h, $row['lote'], 1, 1, 'C');

        $this->SetX($x);
        $this->Cell(123, $h, "(Calle, Jiron, Avenida )", 1, 0, 'C');
        $this->Cell(20, $h, "Nro.", 1, 0, 'C');
        $this->Cell(20, $h, "Mz.", 1, 0, 'C');
        $this->Cell(15, $h, "Lote", 1, 1, 'C');

        $this->SetTextColor(0, 0, 0);
        $this->SetX($x);
        $this->Cell(61.5, $h, $row['descripcion'], 1, 0, 'L');
        $this->Cell(61.5, $h, "CANCHIS", 1, 0, 'C');
        $this->Cell(55,   $h, $empresa["descripcion"], 1, 1, 'C');

        $this->SetX($x);
        $this->Cell(61.5, $h, "(Urbanizacion, barrio)", 1, 0, 'C');
        $this->Cell(61.5, $h, "Provincia", 1, 0, 'C');
        $this->Cell(55, $h, "Distrito", 1, 1, 'C');

        $this->SetTextColor(0, 0, 0);
        $this->SetX($x);
        $this->Cell(61.5, $h, "", 1, 0, 'L');
        $this->Cell(61.5, $h, $row['nromed'], 1, 0, 'C');
        $this->Cell(55, $h, "", 1, 1, 'C');

        $this->SetX($x);
        $this->Cell(61.5, $h, "Telefono / Celular", 1, 0, 'C');
        $this->Cell(61.5, $h, utf8_decode("N° de Medidor"), 1, 0, 'C');
        $this->Cell(55, $h, utf8_decode("Diámetro de la conexión (mm)"), 1, 1, 'C');

        //
        $x = 11;
        $this->Ln(5);
        $this->SetX($x);
        $this->Cell(0, $h, "DEFINICIONES", 0, 1, 'L');
        $this->Rect(12, $this->GetY(), 184, 16);
        $x = 15;
        $this->Ln(1);
        $this->SetX($x);
        $this->SetFont('Arial', '', 6);
        $this->Cell(4, 2, "1.-", 0, 0, 'L');
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(15, 2, utf8_decode("Contrastación:"), 0, 0, 'L');
        $text = "Procedimiento técnico que determina el grado de precisión del medidor de agua potable, de acuerdo a las normas metrológicas vigentes ";
        $this->SetFont('Arial', '', 6);
        $this->Cell(15, 2, utf8_decode($text), 0, 1, 'L');
        $text = " y las recomendaciones de la SUNASS, por comparación con un padrón certificado por el INDECOPI.";
        $this->SetX($x + 19);
        $this->Cell(15, 2, utf8_decode($text), 0, 1, 'L');

        $this->SetX($x);
        $this->Cell(4, 2, "2.-", 0, 0, 'L');
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(26, 2, utf8_decode("Contrastación en campo:"), 0, 0, 'L');
        $text = "Contrastación realizada sin retirar el medidor de agua potable de la conexión domiciliaria, bajo las condiciones";
        $this->SetFont('Arial', '', 6);
        $this->Cell(15, 2, utf8_decode($text), 0, 1, 'L');
        $text = "hidráulicas correspondientes al servicio que recibe el usuario";
        $this->SetX($x + 19);
        $this->Cell(15, 2, utf8_decode($text), 0, 1, 'L');

        $this->SetX($x);
        $this->Cell(4, 2, "2.-", 0, 0, 'L');
        $this->SetFont('Arial', 'B', 6);
        $this->Cell(30, 2, utf8_decode("Contrastación en laboratorio:"), 0, 0, 'L');
        $text = "Contrastación realizada en un laboratorio, bajo condiciones hidráulicas controladas que pueden diferir de las";
        $this->SetFont('Arial', '', 6);
        $this->Cell(15, 2, utf8_decode($text), 0, 1, 'L');
        $text = "condiciones del servicio que recibe el usuario, para lo cual se retirará el medidor de la conexión domiciliaria. El laboratorio";
        $this->SetX($x + 19);
        $this->Cell(15, 2, utf8_decode($text), 0, 1, 'L');
        $text = "puede ser una instalación permanente o móvil, que cumpla con los requisitos establecidos por el INDECOPI";
        $this->SetX($x + 19);
        $this->Cell(15, 2, utf8_decode($text), 0, 1, 'L');
        $this->SetFont('Arial', '', 7);

        //
        $x = 11;
        $this->Ln(5);
        $this->SetX($x);
        $this->Cell(0, $h, utf8_decode("SELECCIÓN"), 0, 1, 'L');
        $this->Rect(12, $this->GetY(), 184, 55);
        $x = 15;
        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(0, $h, utf8_decode("TIPO DE CONTRASTACIÓN (marcar)"), 0, 1, 'L');
        $this->SetX($x);
        if($row['tpcontrastacion']==2){ $x1='X'; $x2=' '; }else{ $x1=' '; $x2='X'; }
        $this->Cell(50, $h, utf8_decode("1.- CONTRASTACIÓN EN CAMPO"), 0, 0, 'L');
        $this->Cell(20, $h, utf8_decode($x1), 1, 0, 'C');
        $this->Cell(30, $h, utf8_decode(""), 0, 0, 'L');
        $this->Cell(50, $h, utf8_decode("2.- CONTRASTACIÓN EN LABORATORIO"), 0, 0, 'L');
        $this->Cell(20, $h, utf8_decode($x2), 1, 1, 'C');
        $this->Ln(4);
        $this->SetX($x);
        $this->Cell(0, $h, utf8_decode("ENTIDAD CONTRASTADORA"), 0, 1, 'L');
        $this->SetFont('Arial', '', 6);
        $this->SetX($x);
        $this->Cell(178, 3, utf8_decode("Escribir el nombre de la Entidad Contrastadora seleccionada por el usuario del listado proporcionado por la Empresa Prestadora."), 0, 1, 'L');
        $this->SetFont('Arial', '', 7);
        $this->SetX($x);
        $this->Cell(178, $h, $row['razonsocial'], 1, 1, 'L');

        ///
        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(35, $h, utf8_decode("DIRECCIÓN DE LA ENTIDAD CONTRASTADORA"), 0, 1, 'L');
        $this->Ln(1);
        $this->SetX($x);
        $this->Cell(123, $h, $row['diremp'], 1, 0, 'L');
        $this->Cell(20 , $h, $row['nrocaemp'], 1, 0, 'C');
        $this->Cell(20 , $h, $row['manemp'], 1, 0, 'L');
        $this->Cell(16 , $h, $row['lotemp'], 1, 1, 'L');


        $this->SetX($x);
        $this->Cell(123, $h, "(Calle, Jiron, Avenida )", 1, 0, 'C');
        $this->Cell(20, $h, "Nro.", 1, 0, 'C');
        $this->Cell(20, $h, "Mz.", 1, 0, 'C');
        $this->Cell(16, $h, "Lote", 1, 1, 'C');

        $this->SetTextColor(0, 0, 0);
        $this->SetX($x);
        $this->Cell(61.5, $h, $row['baremp'], 1, 0, 'L');
        $this->Cell(61.5, $h, 'CANCHIS', 1, 0, 'C');
        $this->Cell(56, $h, $row['ubemp'], 1, 1, 'C');


        $this->SetX($x);
        $this->Cell(61.5, $h, "(Urbanizacion, barrio)", 1, 0, 'C');
        $this->Cell(61.5, $h, "Provincia", 1, 0, 'C');
        $this->Cell(56, $h, "Distrito", 1, 1, 'C');
        //
        $this->Ln(2);
        $this->SetX($x);
        $this->Cell(45, $h, utf8_decode("COSTO DE LA CONTRASTACIÓN"), 0, 0, 'L');
        $this->Cell(35, $h, utf8_decode(""), 1, 0, 'L');
        $this->Cell(35, $h, utf8_decode("NUEVOS SOLES"), 0, 1, 'L');
        $this->Ln(1);

        $x = 11;
        $this->Ln(5);
        $this->SetX($x);
        $this->Cell(0, $h, utf8_decode("DECLARACIÓN RESPECTO AL COSTO DE LA CONTRASTACIÓN"), 0, 1, 'L');
        $this->Rect(12, $this->GetY(), 184, 6);
        $x = 15;
        $this->Ln(1);
        $this->SetX($x);
        $this->SetFont('Arial', '', 6);
        $this->Cell(0, $h, utf8_decode("Me comprometo a asumir el costo de la Contrastación, si se comprobará que el medidor no sobreregistra."), 0, 1, 'L');
        $this->SetX($x);
        $this->SetFont('Arial', '', 7);

        ///
        $x = 13;
        $this->Ln(10);
        $this->Rect($x, $this->GetY(), 80, 11);
        $this->Rect($x + 85, $this->GetY(), 40, 11);
        $this->Rect($x + 130, $this->GetY(), 53, 11);

        $this->Ln(11);
        $this->SetX($x);
        $this->Cell(80, $h, "Firma Reclamante", 0, 0, 'C');
        $this->SetX($x + 85);
        $this->Cell(40, $h, "Huella digital*", 0, 0, 'C');
        $this->SetX($x + 130);
        $this->Cell(53, $h, "Fecha", 0, 1, 'C');
        $this->SetX($x + 85);
        $this->Cell(40, 2, "(Indice derecho)", 0, 1, 'C');
        $this->SetX($x);
        $this->Cell(183, $h, "* En caso de no saber firmar o estar impedido bastara con la huella digital.", 0, 1, 'L');
    }

}

$coddetmedidor = $_GET["Id"];
$codsuc        = $_GET["codsuc"];

$objReporte = new clsFormato1();
$objReporte->AliasNbPages();
$objReporte->AddPage();
$objReporte->Contenido($codsuc, $coddetmedidor);
$objReporte->Output();

?>