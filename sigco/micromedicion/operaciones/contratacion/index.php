<?php
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");

    $TituloVentana = "CONTRASTACION DEL MEDIDOR";
    $Activo = 1;
    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;

    if ($Op != 5)
        CuerpoSuperior($TituloVentana);
    $codsuc = $_SESSION['IdSucursal'];

    $Criterio= 'contrastacion';

    $and = "";
    if ($_GET['Op'] == 5) {
        $and = " and d.estareg=4 ";
    }

    $FormatoGrilla = array(); //
    $Sql = "SELECT
        d.coddetmedidor,
        cl.propietario,
        d.nromed,
        t.descripcion, 
        " . $objFunciones->FormatFecha('d.fechareg') . ",
        case d.estareg 
            when 1 then '<p style=\"color:green;font-weight: bold;\">INSTALADO</p>'
            when 2 then '<p style=\"color:red;font-weight: bold;\">ATENDIDO</p>'
            when 3 then '<p style=\"color:red;font-weight: bold;\">RETIRADO</p>'
            when 4 then '<p style=\"color:red;font-weight: bold;\">CONTRASTADO</p>'
        else '' end, 1,d.estareg,d.nroinscripcion

        FROM
        micromedicion.detmedidor AS d
        INNER JOIN catastro.clientes AS cl ON (cl.codcliente = d.codcliente AND cl.nroinscripcion = d.nroinscripcion)
        INNER JOIN public.marcamedidor AS ma ON ma.codmarca = d.marcamed
        INNER JOIN public.modelomedidor AS mo ON mo.codmodelo = d.modelomed
        INNER JOIN public.tipomovimiento AS t ON t.codtipomov = d.codtipomovimiento";
    //  to_char(fechacreacion,'dd-MM-yyyy') 
    $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL                                                           //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'cl.propietario', '2' => 'd.nromed');          //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;                                                            //Operacion
    $FormatoGrilla[3] = array('T1' => 'C&oacute;digo', 'T2' => 'Propietario', 'T3' => 'Nro. Medidor', 'T4' => 'Movimiento',
        'T5' => 'Fecha Reg.', 'T6' => 'Estado');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'left', 'A4' => 'left', 'A5' => 'center', 'A6' => 'center');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '45', 'W2' => '200', 'W3' => '75', 'W4' => '120', 'W5' => '45', 'W6' => '70', 'W7' => '65');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 960;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = " AND d.codtipomovimiento=4  AND (d.codemp=1 and d.codsuc=".$codsuc.$and.") ORDER BY d.coddetmedidor DESC ";                                   //Orden de la Consulta
    if ($Op != 5)
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '2', //Número de Botones a agregar
            'BtnId1' => 'BtnModificar', //Nombre del Boton
            'BtnI1' => 'modificar.png', //Imagen a mostrar
            'Btn1' => 'Editar', //Titulo del Botón
            'BtnF1' => 'onclick="Mostrar(this.id, 1);"', //Eventos del Botón
            'BtnCI1' => '7', //Item a Comparar
            'BtnCV1' => '1', //Valor de comparación
            //'BtnId2'=>'BtnEliminar', 
            //'BtnI2'=>'eliminar.png', 
            //'Btn2'=>'Eliminar', 
            //'BtnF2'=>'onclick="Eliminar(this.id)"', 
            //'BtnCI2'=>'8', //campo 3
            //'BtnCV2'=>'1',//igua a 1*/
            //'BtnId2'=>'BtnRestablecer', //y aparece este boton
            //'BtnI2'=>'restablecer.png', 
            'BtnId2' => 'BtnRestablecer', //y aparece este boton
            'BtnI2' => 'imprimir.png',
            'Btn2' => 'Imprimir',
            'BtnF2' => 'onclick="Imprimir(this.id)"',
            'BtnCI2' => '7',
            'BtnCV2' => '1',
            'BtnId3' => 'BtnGenerar', //y aparece este boton
            'BtnI3' => 'documento.png',
            'Btn3' => 'Atender Instalacion',
            'BtnF3' => 'onclick="AtenderInst(this)"',
            'BtnCI3' => '7',
            'BtnCV3' => '1'
        );
    else {
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '1', //Número de Botones a agregar
            'BtnId1' => 'BtnModificar', //Nombre del Boton
            'BtnI1' => 'ok.png', //Imagen a mostrar
            'Btn1' => 'Seleccionar', //Titulo del Botón
            'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
            'BtnCI1' => '7', //Item a Comparar
            'BtnCV1' => '1'    //Valor de comparación
        );
        $FormatoGrilla[5] = array('W1' => '45', 'W2' => '225', 'W3' => '70', 'W4' => '80', 'W5' => '70', 'W6' => '70', 'W7' => '70');
    }
    $FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 1), array('Name' => 'estado', 'Col' => 8),
             array('Name' => 'nroinscripcion', 'Col' => 9)); //DATOS ADICIONALES           
    $FormatoGrilla[11] = 6; //FILAS VISIBLES
    $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 900, 550);
    Pie();
?>
<script type="text/javascript">
    var urldir = '<?php echo $urldir; ?>';
    $(document).ready(function() {
        $("#Modificar1").dialog({
            autoOpen: false,
            height: 150,
            width: 350,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    GuardarOk()
                },
                "Cancelar": function() {
                    $(this).dialog("close");
                }
            },
            close: function() {

            }
        });
    })
    function Enviar(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        //alert(Id);
        //var res = Id.split('-');
        //Id= res[1];
        opener.Recibir(Id);
        window.close();
    }
    function Imprimir(Id)
    {
        var url = '';
        url = 'imprimir.php';
        var res = Id.split('-');
        Id= res[1];
        AbrirPopupImpresion(url+'?Id='+Id+'&codsuc=<?=$codsuc ?>', 800, 500)
    }

    var Cod = ''
    function AtenderInst(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        Cod = Id

        var Estado = $(obj).parent().parent().data('estado')
        if (Estado == 2)
        {
            alert('No se puede Atender la instalacion...Se encuentra Atendido')
            return
        }

        $("#Modificar1").dialog({title: 'Confirmar Atensión'});
        $("#Modificar1").dialog("open");
    }

    function GuardarOk()
    {
        $.ajax({
            url: 'procesar.php',
            type: 'POST',
            async: true,
            data: 'Id='+Cod+'&codsuc=<?=$codsuc ?>',
            success: function(data) {
                OperMensaje(data)
                $("#Mensajes").html(data);
                $("#Modificar1").dialog("close");
                Buscar(0);
            }
        })
    }
</script>

<div id="Modificar1" style="display: none;"  >
    <table width="300px" border="0">
        <tr>
            <td width="5%" align="center">&nbsp;</td>
            <td width="95%">&nbsp;</td>
        </tr>
        <tr>
            <td colspan=""width="100%">
        <center>Esta seguro que desea atender la Instalación?</center>
        </td>
        </tr>
        <tr>
            <td width="5%" align="center">&nbsp;</td>
            <td width="95%">&nbsp;</td>
        </tr>
    </table>
</div>
<?php
if ($Op != 5)
    CuerpoInferior();
?>
