<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	  
    include('../../../../objetos/clsFunciones.php');
        
    $objFunciones = new clsFunciones();

    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $coddetmedidorreg = $_POST["Id"];
    $idusuario  = $_SESSION['id_user'];
    $codtipomovimiento= 2;
    $estareg= 2;
    $fecha= date('Y-m-d');
    $horareg  = date('H:i:s');
    
    $id= $objFunciones->setCorrelativos("instalacion_med",$codsuc,"0");
    $coddetmedidor = $id[0];
    $conexion->beginTransaction();
    
    /* ATENDER LA INSTALACION */
    $upd="UPDATE micromedicion.detmedidor
        SET fechainstalacion= :fecha
        WHERE codemp=:codemp and codsuc=:codsuc and coddetmedidor= :coddetmedidor";
    $resultad = $conexion->prepare($upd);
    $resultad->execute(array(":coddetmedidor"=> $coddetmedidorreg, 
        ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":fecha"=>$fecha));
    
    if ($resultad->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error UPDATE detmedidor";
            die(2);
        }

			       
    $sql1="SELECT
        d.coddetmedidor, d.codcliente,
        d.nroinscripcion, d.nromed,
        d.codinspector, d.marcamed,
        d.coddiametro, d.estadomedidores,
        d.posicionmed, d.modelomed, d.tipolectura,
        d.aniofabmed, d.codtipomedidor,
        d.fechamov, d.codcapacidadmedidor,
        d.codclasemetrologica, d.tipofacturacion,
        d.ubicacionllavemedidor, d.lecturaanterior,
        d.fechalecturaanterior, d.lecturaultima,
        d.fechalecturaultima, d.consumo,
        d.lecturapromedio, d.filtromed,
        d.precintomed, d.filtromedbueno,
        d.observacion, d.trasladomed,d.estareg,
        d.horamov,codempcont
    FROM
        micromedicion.detmedidor AS d
        INNER JOIN catastro.clientes AS c ON (c.codcliente = d.codcliente AND c.nroinscripcion = d.nroinscripcion)
        INNER JOIN public.calles AS cl ON (c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona)
        INNER JOIN public.tiposcalle AS tc ON (cl.codtipocalle = tc.codtipocalle)
        INNER JOIN reglasnegocio.inspectores AS inp ON (inp.codinspector = d.codinspector AND inp.codemp = d.codemp AND inp.codsuc = d.codsuc)
    WHERE d.codsuc=? and d.coddetmedidor=? ";
    $consulta = $conexion->prepare($sql1);
    $consulta->execute(array($codsuc,$coddetmedidorreg));
    $item = $consulta->fetch();
    
    $codcl =$item['codcliente'];
    /* INSERTAR EL REGISTRO DE ATENDIDO */
        
    $sql = "INSERT INTO micromedicion.detmedidor(coddetmedidor, codemp, codsuc, codcliente, nroinscripcion, ";
        $sql .= "nromed, marcamed, coddiametro, estadomedidores, posicionmed, modelomed, ";
        $sql .= "tipolectura, aniofabmed, codtipomedidor, fechamov, codcapacidadmedidor, ";
        $sql .= "codclasemetrologica, tipofacturacion, ubicacionllavemedidor, lecturaanterior, ";
        $sql .= "fechalecturaanterior, lecturaultima, fechalecturaultima, consumo, lecturapromedio, ";
        $sql .= "codinspector, filtromed, precintomed, filtromedbueno, observacion, fechareg, ";
        $sql .= "horareg, codtipomovimiento, estareg, trasladomed, codusu, horamov,codempcont) ";
        $sql .= "VALUES ( :coddetmedidor, :codemp, :codsuc, :codcliente, :nroinscripcion,:nromed, ";
        $sql .= ":marcamed, :coddiametro, :estadomedidores, :posicionmed, :modelomed, :tipolectura, ";
        $sql .= ":aniofabmed, :codtipomedidor, :fechamov, :codcapacidadmedidor, :codclasemetrologica, ";
        $sql .= ":tipofacturacion, :ubicacionllavemedidor, :lecturaanterior, :fechalecturaanterior, ";
        $sql .= ":lecturaultima, :fechalecturaultima, :consumo, :lecturapromedio, :codinspector, ";
        $sql .= ":filtromed, :precintomed, :filtromedbueno, :observacion, :fechareg, :horareg, ";
        $sql .= ":codtipomovimiento, :estareg, :trasladomed, :codusu, :horamov, :codempcont)";
    $res = $conexion->prepare($sql);
    $res->execute(array(":coddetmedidor"=> $coddetmedidor, 
            ":codemp"=>$codemp, ":codsuc"=>$codsuc, 
            ":codcliente"=>$codcl, 
            ":nroinscripcion"=>$item['nroinscripcion'], 
            ":nromed"=>$item['nromed'], 
            ":marcamed"=>$item['marcamed'], 
            ":coddiametro"=>$item['coddiametro'], 
            ":estadomedidores"=>$item['estadomedidores'], 
            ":posicionmed"=>$item['posicionmed'], 
            ":modelomed"=>$item['modelomed'],
            ":tipolectura"=>$item['tipolectura'], 
            ":aniofabmed"=>$item['aniofabmed'], 
            ":codtipomedidor"=>$item['codtipomedidor'], 
            ":fechamov"=>$item['fechamov'], 
            ":codcapacidadmedidor"=>$item['codcapacidadmedidor'], 
            ":codclasemetrologica"=>$item['codclasemetrologica'],
            ":tipofacturacion"=>$item['tipofacturacion'], 
            ":ubicacionllavemedidor"=>$item['ubicacionllavemedidor'], 
            ":lecturaanterior"=>$item['lecturaanterior'], 
            ":fechalecturaanterior"=>$item['fechalecturaanterior'], 
            ":lecturaultima"=>$item['lecturaultima'], 
            ":fechalecturaultima"=>$item['fechalecturaultima'], 
            ":consumo"=>$item['consumo'],
            ":lecturapromedio"=>$item['lecturapromedio'], 
            ":codinspector"=>$item['codinspector'], 
            ":filtromed"=>$item['filtromed'],
            ":precintomed"=>$item['precintomed'], 
            ":filtromedbueno"=>$item['filtromedbueno'],
            ":observacion"=>$item['observacion'],
            ":fechareg"=>$fecha, 
            ":horareg"=>$horareg,
            ":codtipomovimiento"=>$codtipomovimiento, 
            ":estareg"=>$estareg , 
            ":trasladomed"=>$item['trasladomed'], ":codusu"=>$idusuario,
            ":horamov"=>$item['horamov'], ":codempcont"=>$item['codempcont'] ));
        
    if ($res->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERTAR detmedidor";
            die(2);
        }else{
    		$conexion->commit();		
    		echo $resu=1;
	    }
        
?>