<?php
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
    
    $TituloVentana = "INSTALACION DE MEDIDOR";
    $Activo = 1;
    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;

    if ($Op != 5)
        CuerpoSuperior($TituloVentana);
    $codsuc = $_SESSION['IdSucursal'];

    $Criterio= 'instalacion';
    
    $and = "";
    if ($_GET['Op'] == 5) {
        $and = " and d.codtipomovimiento=2 and d.estareg=2 ";
    }

    $FormatoGrilla = array ();
    $Sql = "SELECT
        d.coddetmedidor,
        cl.propietario,
        d.nromed,
        t.descripcion, 
        to_char(d.fechareg,'dd/mm/yyyy' ),
        to_char(d.fechamov,'dd/mm/yyyy' ),        
        case d.estareg 
            when 1 then '<p style=\"color:green;font-weight: bold;\">INSTALADO</p>'
            when 2 then '<p style=\"color:red;font-weight: bold;\">ATENDIDO</p>'
            when 3 then '<p style=\"color:red;font-weight: bold;\">RETIRADO</p>'
        else '' end, 1,d.estareg,cl.codantiguo

        FROM
        micromedicion.detmedidor AS d
        INNER JOIN catastro.clientes AS cl ON (cl.codcliente = d.codcliente AND cl.nroinscripcion = d.nroinscripcion)
        INNER JOIN public.marcamedidor AS ma ON ma.codmarca = d.marcamed
        INNER JOIN public.modelomedidor AS mo ON mo.codmodelo = d.modelomed
        INNER JOIN public.tipomovimiento AS t ON t.codtipomov = d.codtipomovimiento";
    //  to_char(fechacreacion,'dd-MM-yyyy') 
    $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'cl.propietario', '2' => 'cl.codantiguo', '3' => 'd.nromed');          //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;                                                        //Operacion
    $FormatoGrilla[3] = array('T1' => 'Item', 'T2' => 'Propietario', 'T3' => 'Nro. Medidor', 'T4' => 'Movimiento',
        'T5' => 'Fecha Reg.', 'T6' => 'Fecha Inst.', 'T7' => 'Estado');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'left', 'A4' => 'left', 'A5' => 'center', 'A6' => 'center', 'A7' => 'center');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '45', 'W2' => '200', 'W3' => '75', 'W4' => '120', 'W5' => '45', 'W6' => '70', 'W7' => '70', 'W8' => '65');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                              //Registro por Páginas
    $FormatoGrilla[7] = 950;                                                        //Ancho de la Tabla
    $FormatoGrilla[8] = " AND (d.codtipomovimiento=1 OR d.codtipomovimiento=2) AND (d.codemp=1 and d.codsuc=".$codsuc.$and.") ORDER BY d.fechamov desc ";                                   //Orden de la Consulta
    if ($Op != 5)
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '3',                      //Número de Botones a agregar
            'BtnId1' => 'BtnModificar',       //Nombre del Boton
            'BtnI1' => 'modificar.png',       //Imagen a mostrar
            'Btn1' => 'Editar',               //Titulo del Botón
            'BtnF1' => 'onclick="Mostrar(this.id,1);"', //Eventos del Botón
            'BtnCI1' => '8',                 //Item a Comparar
            'BtnCV1' => '1',                 //Valor de comparación            
            'BtnId2' => 'BtnRestablecer',    //y aparece este boton
            'BtnI2' => 'imprimir.png',
            'Btn2' => 'Imprimir',
            'BtnF2' => 'onclick="Imprimir(this.id)"',
            'BtnCI2' => '8',
            'BtnCV2' => '1',
            'BtnId3' => 'BtnGenerar',        //y aparece este boton
            'BtnI3' => 'documento.png',
            'Btn3' => 'Atender Instalacion',
            'BtnF3' => 'onclick="AtenderInst(this)"',
            'BtnCI3' => '8',
            'BtnCV3' => '1'
        );
    else {
        $FormatoGrilla[9] = array('Id' => '9', //Botones de Acciones
            'NB' => '1',                       //Número de Botones a agregar
            'BtnId1' => 'BtnModificar',        //Nombre del Boton
            'BtnI1' => 'ok.png',               //Imagen a mostrar
            'Btn1' => 'Seleccionar',           //Titulo del Botón
            'BtnF1' => 'onclick="Enviar(this.id);"', //Eventos del Botón
            'BtnCI1' => '8',                   //Item a Comparar
            'BtnCV1' => '1'                    //Valor de comparación
        );
        $FormatoGrilla[5] = array('W1' => '45', 'W2' => '225', 'W3' => '70', 'W4' => '80', 'W5' => '70', 'W6' => '70', 'W7' => '70');
    }

    $FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 1), array('Name' => 'estado', 'Col' => 8)); //DATOS ADICIONALES           
    $FormatoGrilla[11] = 7; //FILAS VISIBLES
    $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 830, 600);
    Pie();
?>

<script type="text/javascript">

var urldir = '<?php echo $urldir; ?>';

function ValidarEnter(e, Op)
{
    switch(e.keyCode)
        {   
            case $.ui.keyCode.UP: 
                for (var i = 10; i >= 1; i--) 
                    {
                        if ($('tr[tabindex=\''+i +'\']') != null )
                          {
                            var obj = $('tr[tabindex=\'' +i +'\']')
                            if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                            {
                              $('tr[tabindex=\''+i +'\']').focus();
                               e.preventDefault();
                               return false;
                               break;
                           }

                         }
                    };
                    $('#Valor').focus().select();
            break;
            case $.ui.keyCode.DOWN:

                   for (var i = 1; i <= 10; i++) 
                    {
                        if ($('tr[tabindex=\''+i +'\']') != null )
                          {
                            var obj = $('tr[tabindex=\'' +i +'\']')
                            if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                            {
                              $('tr[tabindex=\''+i +'\']').focus();
                               e.preventDefault();
                               return false;
                               break;
                           }

                         }
                    };

                    $('#Valor').focus().select();
            break;
            default:  if (VeriEnter(e)) Buscar(Op);


        }


        //ValidarEnterG(evt, Op)
}

    /*function Modificar(obj)
    {
      $("#form1").remove();
      var estado = $(obj).parent().parent().data('estado')
      var Id = $(obj).parent().parent().data('id')
      var texto = $(obj).parent().parent().data('texto')
       
     
      $("#Modificar").dialog({title: 'Modificar Reclamo'});
      $("#Modificar").dialog("open");
      $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
       $.ajax({
          url: 'mantenimiento.php',
          type: 'POST',
          async: true,
          data: 'Id=' + Id + '&codsuc=<?=$codsuc ?>&Op=1',
          success: function(data) {
              $("#DivModificar").html(data);
          }
      })    

    }*/

    $(document).ready(function() {
        $("#Modificar1").dialog({
            autoOpen: false,
            height: 150,
            width: 350,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    GuardarOk()
                },
                "Cancelar": function() {
                    $(this).dialog("close");
                }
            },
            close: function() {

            }
        });
    })

    function Enviar(Id)
    {
        //var Id = $(obj).parent().parent().data('id')
        opener.Recibir(Id);
        window.close();
    }

    function Imprimir(Id)
    {
        var url = '';
        url = 'imprimir.php';
        AbrirPopupImpresion(url+'?Id='+Id+'&codsuc=<?=$codsuc ?>', 800, 500)
    }

    var Cod = ''
    function AtenderInst(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        Cod = Id

        var Estado = $(obj).parent().parent().data('estado')
        if (Estado == 2)
        {
            alert('No se puede Atender la instalacion...Se encuentra Atendido')
            return
        }

        $("#Modificar1").dialog({title: 'Confirmar Atensión'});
        $("#Modificar1").dialog("open");
    }

    function GuardarOk()
    {
        $.ajax({
            url: 'procesar.php',
            type: 'POST',
            async: true,
            data: 'Id='+Cod+'&codsuc=<?=$codsuc ?>',
            success: function(data) {
                OperMensaje(data)
                $("#Mensajes").html(data);
                $("#Modificar1").dialog("close");
                Buscar(0);
            }
        })
    }
</script>

<div id="Modificar1" style="display: none;"  >
    <table width="300px" border="0">
        <tr>
            <td width="5%" align="center">&nbsp;</td>
            <td width="95%">&nbsp;</td>
        </tr>
        <tr>
            <td colspan=""width="100%">
        <center>Esta seguro que desea atender la Instalación?</center>
        </td>
        </tr>
        <tr>
            <td width="5%" align="center">&nbsp;</td>
            <td width="95%">&nbsp;</td>
        </tr>
    </table>
</div>

<?php
if ($Op != 5)
    CuerpoInferior();
?>
