<?php   
    session_name("pnsu");
    if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");

    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"])?$_POST["Id"]: '';
    $codsuc = $_SESSION['IdSucursal'];
    $guardar = "op=".$Op;
    $Rep = 0;
    $objMantenimiento = new clsDrop();
    
    $fechamov = date("d/m/Y");
    $horamov = date("H:m");
    $fechalecturaanterior= date("d/m/Y");
    $fechalecturaultima = date("d/m/Y");
    
    if ($Id != '') {
        $sql = "SELECT
            d.coddetmedidor, d.codcliente,
            d.nroinscripcion, c.propietario,
            tc.descripcioncorta||' '||cl.descripcion AS direccion,
            c.nrocalle, d.nromed, d.codempcont,
            d.codinspector, d.marcamed,
            d.coddiametro, d.estadomedidores,
            d.modelomed, d.tipolectura,
            d.aniofabmed, d.codtipomedidor,
            d.fechamov, d.codcapacidadmedidor,
            d.codclasemetrologica, d.tipofacturacion,
            d.ubicacionllavemedidor, d.lecturaanterior,
            d.fechalecturaanterior, d.lecturaultima,
            d.fechalecturaultima, d.consumo,
            d.lecturapromedio, d.filtromed,
            d.precintomed, d.filtromedbueno,
            d.observacion, d.trasladomed,d.estareg,
            c.codantiguo
            FROM
            micromedicion.detmedidor AS d
            INNER JOIN catastro.clientes AS c ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.codcliente = d.codcliente AND c.nroinscripcion = d.nroinscripcion)
            INNER JOIN public.calles AS cl ON (c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona)
            INNER JOIN public.tiposcalle AS tc ON (cl.codtipocalle = tc.codtipocalle)
            LEFT JOIN reglasnegocio.inspectores AS inp ON ( inp.codemp = d.codemp AND inp.codsuc = d.codsuc AND inp.codinspector = d.codinspector)
            
            WHERE d.codemp=1 and d.codsuc=? and d.coddetmedidor= ? ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc,$Id));
        $items = $consulta->fetch();
        //print_r($items);
        $guardar = $guardar . "&Id2=" . $Id;
        
        //$ver= $instalacion["nroinscripcion"];
        $fech = new DateTime($items["fechamov"]);
        $fechamov = $fech->format("d/m/Y");
        
        $fec = new DateTime($items["fechalecturaanterior"]);
        $fechalecturaanterior = $fec->format("d/m/Y");
        
        $fe = new DateTime($items["fechalecturaultima"]);
        $fechalecturaultima = $fe->format("d/m/Y");
        
        $medfil= $items["filtromed"];        
        $medprec= $items["precintomed"];
        $filtroest= $items["filtromedbueno"];
        $trasladomed= $items["trasladomed"];
    }  else {
        $medfil= 1;        
        $medprec= 1;
        $filtroest= 1;
        $trasladomed=1;
    }
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    var IdAnt = ''
    var IdGrafico = ''
    var urldir = '<?php echo $_SESSION['urldir'];?>';
    var codsuc  = <?=$codsuc?>;
    
    $(document).ready(function() {
        $("#tabs").tabs();
        $("#MedFilt").buttonset();
        $("#MedPrecSeg").buttonset();
        $("#FiltroEst").buttonset();
        $("#TrasladoMedidor").buttonset();
        
        $('form').keypress(function(e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function(e) {
            if (e.which == 13)
            {
                return false;
            }
        });        
        
        $("#fechainstmedidor,#fechalecturaanterior,#fechalecturaultima").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                showButtonPanel: true
            }
        );

        $( "#DivLecturas" ).dialog({
                autoOpen: false,
                height: 350,
                width: 550,
                modal: true,
                resizable:true,
                buttons: {
                        "Aceptar": function() {
                                if(objIdx=="promediar")
                                {
                                        $("#lecturaultima").val($("#lectpromedio").val())
                                        $("#consumo").val($("#lectpromedio").val())
                                        $("#lecturapromedio").val($("#lectpromedio").val())
                                        //calcularconsumo($("#lectpromedio").val())
                                }else{
                                        if($("#obstem").val()=='')
                                        {
                                                Msj($("#obstem"),"Digite el Motivo de Modificación")
                                                return false;
                                        }
                                        $("#obs").val($("#obstem").val())
                                        GuardarP(1)
                                }
                                $( this ).dialog( "close" );
                        },
                        "Cancelar": function() {
                                $( this ).dialog( "close" );
                        }
                },
                close: function() {

                }
            });
        
    });

    function buscar_usuarios()
    {
        object = "usuario";
        
        AbrirPopupBusqueda('<?php echo $_SESSION['urldir'];?>sigco/catastro/operaciones/actualizacion/index.php?Op=5', 1150, 450)
    }
    
    function Recibir(id)
    {
        if (object == "usuario")
        {
            $("#nroinscripcion").val(id);
            
            cargar_datos_usuario(id);
        }

    }
    
    function cargar_datos_usuario(nroinscripcion)
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>ajax/clientes_instalacion.php',
            type: 'POST',
            async: true,
            data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion,
            dataType: "json",
            success: function(datos) {
                $("#codcliente").val(datos.codcliente)
                $("#Cliente").val(datos.propietario)
                $("#direccion").val(datos.direccion)
                $("#nrocalle").val(datos.nrocalle)
                
                $("#nromedidor").val(datos.nromed)
                $("#marcamedidor").val(datos.codmarca)
                $("#diametrosmedidor").val(datos.coddiametrosmedidor)
                $("#estadomedidor").val(datos.codestadomedidor)
                $("#posicionmedidor").val(datos.posicionmed)
                $("#modelomedidor").val(datos.codmodelo)                
                $("#tipolectura").val(datos.tipolectura)
                $("#aniofab").val(datos.aniofabmed)
                $("#tipomedidor").val(datos.codtipomedidor)
                $("#fechainstmedidor").val(datos.fechainsmed)
                $("#capacidadmedidor").val(datos.codcapacidadmedidor)
                $("#codclasemetrologica").val(datos.codclasemetrologica)
                $("#tipofacturacion").val(datos.tipofacturacion)
                $("#ubicacionllavemedidor").val(datos.codubicacion)
                $("#lecturaanterior").val(datos.lecturaanterior)
                $("#fechalecturaanterior").val(datos.fechalecturaanterior)
                $("#lecturaultima").val(datos.lecturaultima)
                $("#fechalecturaultima").val(datos.fechalecturaultima)
                $("#consumo").val(datos.consumo)
                $("#lecturapromedio").val(datos.lecturapromedio)
                $("#nroinscripcion").val(datos.nroinscripcion)
                $("#codantiguo").val(datos.nroinscripcion)
                //alert(datos.consumo)
            }
        });
    }
    
    function ValidarForm(Op)
    {

        if ($("#Cliente").val() == "")
        {
            Msj("#Cliente", 'El Nombre del Propietario no puede ser NULO')
            return false;
        }
        
        if ($("#tipofacturacion").val() == '')
        {
            Msj("#tipofacturacion", 'Seleccione el Tipo de Facturacion')
            return false;
        }
        if ($("#fechainstmedidor").val() == '')
        {
            Msj("#fechainstmedidor", 'Digite la Fecha de Instalacion')
            return false;
        }
        if ($("#fechalecturaanterior").val() == 0)
        {
            Msj("#fechalecturaanterior", 'Digite la Fecha de Lectura Anterior')
            return false;
        }
        if ($("#fechalecturaultima").val() == 0)
        {
            Msj("#fechalecturaultima", 'Digite la Fecha Ultima de Lectura')
            return false;
        }
        
        GuardarP(Op)
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }
    
    function VerTarifas()
    {
            if($("#tipofacturacion").val()!=2)
            {
                    alert("El Tipo de Facturacion no permite promediar asignar Consumos")
                    return false;
            }
            $("#DivTarifas").dialog('open');
    }
    
    function AddTarifa(consumo)
    {
            $("#DivTarifas").dialog('close');
            $("#consumo").val(consumo);
            actualizacion(document.getElementById('consumo'),0)
    }
    
    function calcularconsumo(obj)
    {
        var anterior = $("#lecturaanterior").val()
        var ultima  = obj

        if(ultima=="")
        {
                ultima=0;
        }
        var consumo = parseFloat(ultima)-parseFloat(anterior);
        if(consumo='NaN')
        { consumo='0.0'; }
        
        $("#consumo").val(consumo)
    }
    
    function promediar_lecturas()
    {
        if($("#tipofacturacion").val()!=1)
        {
                alert("El Tipo de Facturacion no permite promediar las Lecturas")
                return false;
        }
        verlecturas();
        objIdx = "promediar";
        $( "#DivLecturas" ).dialog( "open" );
    }
    function verlecturas()
    {
            $.ajax({
                     url:'ver_lecturas.php',
                     type:'POST',
                     async:true,
                     data:'codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val(),
                     success:function(datos){
                            $("#div_lecturas").html(datos)
                     }
            }) 
    }
    function asignarconsumo()
    {
            var catetar="";
            var tfacturacion=$("#tipofacturacion").val();

            if(tfacturacion!=2)
            {
                    alert('El Tipo de Facturacion no Admite que se asigne el Consumo')
                    return
            }

            for (i=1;i<=$("#cont_unidades").val();i++) 
            {       
                    try
                    {
                            if($("#principal"+i).val()==1)
                            {
                                    catetar=$("#tarifas"+i).val()
                            }
                    }catch(exp)
                    {

                    }
            }

            mostrarconsumo(catetar)
    }
    function mostrarconsumo(catetar)
    {
            $.ajax({
                url:'mostrar_volumen.php',
                type:'POST',
                async:true,
                data:'catetar='+catetar+'&codsuc='+codsuc,
                success:function(datos){
                       $("#lecturaanterior").val(0)
                       $("#lecturaultima").val(0)
                       $("#consumo").val(datos)
                }
            }) 
    }
    
    function MostrarDatosUsuario(datos)
    {

        $("#nroinscripcion").val(datos.nroinscripcion)
        $("#Cliente").val(datos.propietario)
        $("#direccion").val(datos.soldireccion)
        $("#nrocalle").val(datos.nrocalle)
        cargar_datos_usuario(datos.nroinscripcion);
        //Consultar()
    }
</script>

<div align="">
    
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <div id="tabs">
            <ul>
                <li style="font-size:10px"><a href="#tabs-2">Datos del Titular</a></li>
                <li style="font-size:10px"><a href="#tabs-3">Datos del Medidor</a></li>
                <li style="font-size:10px"><a href="#tabs-4">Otros Datos</a></li>            
            </ul>            
            
            <div id="tabs-2" style="height:330px">

                <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                    <tbody>
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="80" class="TitDetalle">Nro. Inscripcion</td>
                            <td width="30" align="center">:</td>
                            <td>
                                <input type="hidden" name="nroinscripcion" id="nroinscripcion" size="15" class="inputtext" readonly="readonly" value="<?=$items["nroinscripcion"] ?>"/>
                                <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" value="<?php echo $items["codantiguo"] ?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:70px;"/>
                                <span class="MljSoft-icon-buscar" title="Buscar Personas" onclick="buscar_usuarios();"></span>
                            </td>
                            <td  colspan="3">
                                <?php 
                                    
                                    $EST=$items['estareg']; 
                                    switch ($EST) {                                    
                                        case 1:
                                            $EST= "INSTALADO"; break;
                                        case 2:
                                            $EST= "ATENDIDO"; break;
                                        case 3:
                                            $EST= "RETIRADO"; break;
                                        case 4:
                                            $EST= "ATENDIDO"; break;
                                    }
                                ?>
                                <span style="color: red;font-weight: bold;"><?=$EST?></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="TitDetalle">Nombre Titular</td>
                            <td align="center">:</td>
                            <td colspan="4" class="CampoDetalle">
                                <input type="hidden" name="codcliente" id="codcliente" size="10" class="inputtext" readonly="readonly" value="<?=$items["codcliente"] ?>"/>
                                <!-- <span class="MljSoft-icon-buscar" title="Buscar Personas" onclick="buscar_usuarios();"></span>-->
                                <input class="inputtext" name="propietario" type="text" id="Cliente" value="<?=$items["propietario"] ?>" size="50" maxlength="200" readonly="readonly" />

                            </td>
                        </tr>                
                        <tr>
                            <td class="TitDetalle">Direccion</td>
                            <td align="center">:</td>
                            <td class="CampoDetalle">
                                <input type="text" name="direccion" id="direccion" value="<?=$items["direccion"] ?>" readonly="readonly" class="inputtext" style="width:220px" />
                            </td>
                          <td width="117" class="CampoDetalle" align="right">Nro. calle</td>
                            <td width="30" align="center">:</td>
                            <td class="CampoDetalle" colspan="3">
                                <input type="text" name="nrocalle" id="nrocalle" class="inputtext" maxlentgth="6" value="<?=$items["nrocalle"] ?>" readonly="readonly" style="width:200px;" />
                          </td> 
                        </tr>               
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <br />
          </div>

            <div id="tabs-3">
                <center>
                    <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                        <tbody>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>                            
                            <tr>
                                <td align="right">Cod. Medidor</td>
                                <td align="center">:</td>
                                <td >
                                    <input type="text" name="coddetmedidor" id="coddetmedidor" class="inputtext" size="15" value="<?=$items["coddetmedidor"] ?>" readonly />
                                </td>
                                <td align="right">Nro. Medidor</td>
                                <td align="center">:</td>
                                <td >
                                    <input type="text" name="nromedidor" id="nromedidor" class="inputtext" size="15" value="<?=$items["nromed"] ?>" />
                                </td>                        
                            </tr>                                          
                            <tr>
                                <td width="19%" align="right">Marca</td>
                                <td width="2%" align="center">:</td>
                                <td width="25%">
                                    <?php $objMantenimiento->drop_marca_medidor($items["marcamed"], ''); ?>
                                </td>
                                <td width="20%" align="right">Diametro</td>
                                <td width="2%" align="center">:</td>
                                <td width="32%">
                                    <?php $objMantenimiento->drop_diametros_medidor($items["coddiametro"], ''); ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Estado</td>
                                <td align="center">:</td>
                                <td>
                                    <?php $objMantenimiento->drop_estado_medidor($items["estadomedidores"], ''); ?>
                                </td>
                                <td align="right">Posicion</td>
                                <td align="center">:</td>
                                <td>
                                    <select id="posicionmedidor" name="posicionmedidor" style="width: 220px" class="select" >
                                        <option value="1" <?=$items["posicionmed"] == 1 ? "selected='selected'" : "" ?> >CORRECTA</option>
                                        <option value="2" <?=$items["posicionmed"] == 2 ? "selected='selected'" : "" ?> >INCORRECTA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Modelo</td>
                                <td align="center">:</td>
                                <td>
                                    <?php $objMantenimiento->drop_modelo_medidor($items["modelomed"], ''); ?>
                                </td>
                                <td align="right">Tipo de Lectura</td>
                                <td align="center">:</td>
                                <td>
                                    <select id="tipolectura" name="tipolectura" style="width: 220px" class="select" >
                                        <option value="1" <?=$items["tipolectura"] == 1 ? "selected='selected'" : "" ?> >CIRCULAR</option>
                                        <option value="2" <?=$items["tipolectura"] == 2 ? "selected='selected'" : "" ?> >DIRECTA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">A&ntilde;o de Fabricacion</td>
                                <td align="center">:</td>
                                <td><input type="text" name="aniofab" id="aniofab" class="inputtext" maxlength="4" size="15" value="<?=$items["aniofabmed"] ?>"   /></td>
                                <td align="right">Tipo Medidor</td>
                                <td align="center">:</td>
                                <td>
                                    <?php $objMantenimiento->drop_tipo_medidor($items["codtipomedidor"], ''); ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Fecha Instalacion</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="fechainstmedidor" id="fechainstmedidor" class="inputtext" maxlentgth="6" size="12" value="<?=$fechamov ?>" />
                                </td>
                                <td class="TitDetalle" align="right">Hora Instalacion</td>
                                <td>:</td>
                                <td>
                                    <input  id="horareinstalacion" name="horareinstalacion" type="text" value="<?=$horamov ?>" size="10" maxlentgth="6" class="inputtext">
                                </td>
                            </tr>
                            <tr>                                                       
                                <td align="right">Capacidad</td>
                                <td align="center">:</td>
                                <td>
                                    <?php $objMantenimiento->drop_capacidad_medidor($items["codcapacidadmedidor"], ''); ?>
                                </td>
                                <td align="right">Tipo de Facturacion</td>
                                <td align="center">:</td>
                                <td>
                                    <select id="tipofacturacion" name="tipofacturacion" style="width:220px" class="select" >
                                        <option value="100" >--Seleccione el Tipo--</option>
                                        <option value="0"   <?=$items["tipofacturacion"] == 0 ? "selected='selected'" : "" ?> >CONSUMO LEIDO</option>
                                        <option value="1"   <?=$items["tipofacturacion"] == 1 ? "selected='selected'" : "" ?> >CONSUMO PROMEDIADO</option>
                                        <option value="2"   <?=$items["tipofacturacion"] == 2 ? "selected='selected'" : "" ?> >CONSUMO ASIGNADO</option>
                                    </select>
                                </td>      
                            </tr>                              
                            <tr>
                                <td align="right">Ub. Llave</td>
                                <td align="center">:</td>
                                <td>
                                    <?php $objMantenimiento->drop_ubicacion_llave_medidor($items["ubicacionllavemedidor"], ''); ?>
                                </td>
                                <td align="right">Lectura Anterior</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="lecturaanterior" id="lecturaanterior" class="inputtext" maxlentgth="6" size="15" value="<?=$items["lecturaanterior"] ?>" onkeyup="calcularconsumo(document.getElementById('lecturaultima').value);"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Fecha Lect. Anterior</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="fechalecturaanterior" id="fechalecturaanterior" class="inputtext" maxlentgth="6" size="12" value="<?=$fechalecturaanterior ?>"  />
                                </td>
                                <td align="right">Lectura Ultima</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="lecturaultima" id="lecturaultima" class="inputtext" maxlentgth="6" size="15" value="<?=$items["lecturaultima"] ?>" onkeyup="calcularconsumo(this.value);"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Fecha Lect. Ultima</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="fechalecturaultima" id="fechalecturaultima" class="inputtext" maxlentgth="6" size="12" value="<?=$fechalecturaultima ?>" />
                                </td>
                                <td align="right">Consumo</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="consumo" id="consumo" class="inputtext" maxlentgth="6" size="15" value="<?=$items["consumo"] ?>"  readonly="readonly"  />
                                    <span class="icono-icon-info" title="Asignar Consumos" onclick="VerTarifas()"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td align="right">Promedio</td>
                                <td align="center">:</td>
                                <td>
                                    <input type="text" name="lecturapromedio" id="lecturapromedio" class="inputtext" maxlentgth="6" size="15" value="<?=$items["lecturapromedio"] ?>"  readonly="readonly" />
                                    <span class="icono-icon-info" title="Promediar Lecturas" onclick="promediar_lecturas()"></span>
                                </td>
                            </tr>
                            <tr>                        
                                <td colspan="4" class="TitDetalle" align="right">El Medidor fue trasladado en bolsa cerrada con precinto de seguridad</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">  
                                        <?php 
                                            
                                            if($trasladomed==1 || $trasladomed==0)
                                                {
                                                    if($trasladomed==1){$act="checked='checked' "; //$inac="";
                                                    }
                                                    else {$inac="checked='checked' ";}
                                                }
                                                else {$act = "checked='checked' ";}
                                        ?>
                                    <div id="TrasladoMedidor" style="display: inline;">
                                        <input type="radio" id="trasladomed1" name="trasladomed" value='1' <?php echo $act; ?> />
                                        <label for="trasladomed1">SI</label>
                                        <input type="radio" id="trasladomed0" name="trasladomed" value='0' <?php echo $inac; ?> />
                                        <label for="trasladomed0">NO</label>
                                    </div>                            
                                </td>                        
                            </tr>
                            <tr>                                                                  
                                <td align="center" colspan="6">&nbsp;</td>                        
                            </tr> 
                            <tr>
                                <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Reporte Visual del Medidor</b></td>                                              
                                <td align="center" colspan="3">&nbsp;</td>                        
                            </tr>                    
                            <tr>
                                <td class="TitDetalle" align="right">Medidor con filtro</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">                        
                                    <div id="MedFilt" style="display: inline;">
                                        <?php                                     
                                            if($medfil==1 || $medfil==0)
                                                {
                                                    if($medfil==1){$act="checked='checked' "; //$inac="";
                                                    }
                                                    else {$inac="checked='checked' ";}
                                                }
                                                else {$act = "checked='checked' ";}
                                        ?>
                                        <input type="radio" id="medidorfiltro1" name="medidorfiltro" value='1' <?php echo $act; ?> />
                                        <label for="medidorfiltro1">SI</label>
                                        <input type="radio" id="medidorfiltro0" name="medidorfiltro" value='0'  <?php echo $inac; ?> />
                                        <label for="medidorfiltro0">NO</label>
                                    </div>                            
                                </td>
                                <td class="TitDetalle" align="right">Meiddor con precinto de seguridad</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">
                                        <?php 
                                            
                                            if($medprec==1 || $medprec==0)
                                                {
                                                    if($medprec==1){$act="checked='checked' "; //$inac="";
                                                    }
                                                    else {$inac="checked='checked' ";}
                                                }
                                                else {$act = "checked='checked' ";}
                                        ?>
                                    <div id="MedPrecSeg" style="display: inline;">
                                        <input type="radio" id="medprecseg1" name="medprecseg" value='1' <?php echo $act; ?> />
                                        <label for="medprecseg1">SI</label>
                                        <input type="radio" id="medprecseg0" name="medprecseg" value='0' <?php echo $inac; ?> />
                                        <label for="medprecseg0">NO</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                                <td colspan="2" class="TitDetalle" align="right">El Filtro esta en buen estado de conservacion</td>
                                <td class="TitDetalle" align="center">:</td>
                                <td class="CampoDetalle">  
                                        <?php 
                                            //echo $filtroest;
                                            if($filtroest==1 || $filtroest==0)
                                                {
                                                    if($filtroest==1){$act="checked='checked' "; //$inac="";
                                                    }
                                                    else {$inac="checked='checked' ";}
                                                }
                                                else {$act = "checked='checked' ";}
                                        ?>
                                    <div id="FiltroEst" style="display: inline;">
                                        <input type="radio" id="filtroest1" name="filtroestado" value='1' <?php echo $act; ?> />
                                        <label for="filtroest1">SI</label>
                                        <input type="radio" id="filtroest0" name="filtroestado" value='0' <?php echo $inac; ?> />
                                        <label for="filtroest0">NO</label>
                                    </div>                            
                                </td>
                                
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </center>
            </div>

            <div id="tabs-4">
                <center>
                <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right">Emp. Contrastadora</td>
                        <td align="center">:</td>
                        <td class="CampoDetalle" colspan="2">
                        <select name="codempcont" id="codempcont" style="width:250px">
                            <?php
                            $Sql = "SELECT * FROM micromedicion.empresacontrastadora AS e WHERE e.estareg = 1 AND e.codsuc= ".$codsuc;
                            $Consulta = $conexion->query($Sql);
                            foreach ($Consulta->fetchAll() as $rows) {
                                $Sel = "";
                                if ($items["codempcont"] == $rows["codempcont"])
                                    $Sel = 'selected="selected"';
                                ?>
                                <option value="<?php echo $rows["codempcont"]; ?>" <?=$Sel ?>  ><?php echo strtoupper(utf8_decode($rows["razonsocial"])); ?></option>
                            <?php } ?>
                        </select>
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="147" align="right">Inspector</td>
                        <td width="10" align="center">:</td>
                        <td width="215" class="CampoDetalle">
                        <select name="codinspector" id="codinspector" style="width:200px">
                            <?php
                            $Sql = "SELECT * FROM reglasnegocio.inspectores AS insp WHERE insp.estareg = 1 AND insp.codsuc= ".$codsuc;
                            $Consulta = $conexion->query($Sql);
                            foreach ($Consulta->fetchAll() as $rows) {
                                $Sel = "";
                                if ($items["codinspector"] == $rows["codinspector"])
                                    $Sel = 'selected="selected"';
                                ?>
                                <option value="<?php echo $rows["codinspector"]; ?>" <?=$Sel ?>  ><?php echo strtoupper(utf8_decode($rows["nombres"])); ?></option>
                            <?php } ?>
                        </select>
                        </td>
                        <td width="183">&nbsp;</td>
                        <td width="8">&nbsp;</td>
                        <td width="173">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right">Clase Metrológica</td>
                        <td align="center">:</td>
                        <td class="CampoDetalle">
                            <select name="codclasemetrologica" id="codclasemetrologica" style="width:200px">
                                <?php
                                $Sql = "SELECT * FROM micromedicion.clasemetrologica WHERE estareg = 1 ";
                                $Consulta = $conexion->query($Sql);
                                foreach ($Consulta->fetchAll() as $row1) {
                                    $Sel = "";
                                    if ($instalacion["codclasemetrologica"] == $row1["codclasemetrologica"])
                                        $Sel = 'selected="selected"';
                                    ?>
                                    <option value="<?php echo $row1["codclasemetrologica"]; ?>" <?=$Sel ?>  ><?php echo $row1["descripcion"]; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td colspan="3">&nbsp;</td>
                        
                    </tr>
                    <tr>
                        <td align="right" valign="top">Observacion</td>
                        <td align="center" valign="top">:</td>
                        <td colspan="3" valign="top">
                        <textarea name="observacion" id="observacion" class="text ui-widget-content ui-corner-all" style="width: 100%;height: 100px"  cols="10" rows="5" ><?=$items["observacion"]; ?></textarea>
                        </td>
                        <td>&nbsp;</td>
                    </tr>  
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                </center>
                <br />
            </div>

        </div>
        
        

    </form>
    
    <div id="DivLecturas" title="Actualizacion y Modificacion Catastral"  >
        <div id="div_lecturas"></div>
    </div>
</div>

<div id="DivTarifas" title="Tarifas" style="display: none;">
    <div id="DivTarifasAcordion">
    <?php
    $sql = "select * from facturacion.tarifas ORDER BY catetar";
    $ConsultaTar =$conexion->query($sql);
    foreach($ConsultaTar->fetchAll() as $tarifas)
    {
    ?>
  <h3><?=strtoupper($tarifas["nomtar"])?></h3>
  <div style="height: 100px; display: block;">
    <table width="100%" border="0" align="center" cellspacing="0" class="ui-widget" rules="cols">
        <thead class="ui-widget-header" style="font-size:10px">
          <tr>
            <td>&nbsp;</td>
            <td align="center">Volumen</td>
            <td align="center">Importe Agua</td>
            <td align="center">Importe Desague</td>
            <td align="center">&nbsp;</td>
            </tr>
          </thead>
          <tr>
            <td align="right" class="ui-widget-header" style="font-size:10px">Rango Inicial</td>
            <td align="right">
              <label class="inputtext" id="hastarango1" ><?=number_format($tarifas["hastarango1"],2)?></label>
              </td>
            <td align="right">
              <label class="inputtext" id="impconsmin" ><?=$tarifas["impconsmin"]?></label>
              </td>
            <td align="right">
              <label class="inputtext" id="impconsmindesa"><?=$tarifas["impconsmindesa"]?></label>
              </td>
              <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango1"],2)?>')"></span></td>
            </tr>
          <tr>
            <td align="right" class="ui-widget-header" style="font-size:10px">Rango Intermedio </td>
            <td align="right">
              <label class="inputtext" id="hastarango2"><?=number_format($tarifas["hastarango2"],2)?></label>
              </td>
            <td align="right">
              <label class="inputtext" id="impconsmedio"><?=$tarifas["impconsmedio"]?></label>
              </td>
            <td align="right">
              <label class="inputtext" id="impconsmediodesa"><?=$tarifas["impconsmediodesa"]?> </label>
              </td>
              <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango2"],2)?>')"></span></td>
            </tr>
          <tr>
            <td align="right" class="ui-widget-header" style="font-size:10px">Rango Final </td>
            <td align="right">
              <label class="inputtext" id="hastarango3"><?=number_format($tarifas["hastarango3"],2)?></label>
              </td>
            <td align="right">
              <label class="inputtext" id="impconsexec"><?=$tarifas["impconsexec"]?></label>
              </td>
            <td align="right">
              <label class="inputtext" id="impconsexecdesa"><?=$tarifas["impconsexecdesa"]?></label>
              </td>
              <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango3"],2)?>')"></span></td>
            </tr>
          </table>
  </div>
  <?php
    }
  ?>
</div>
</div>