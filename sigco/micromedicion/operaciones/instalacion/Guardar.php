<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
    
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();
    
    $Op = $_GET["Op"];
    $objFunciones = new clsFunciones();

    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $coddetmedidor = $_POST["coddetmedidor"];
    $codcliente    = $_POST["codcliente"];
    $nroinscripcion= $_POST["nroinscripcion"];
    
    $codempcont = $_POST["codempcont"];

    $nromed     = $_POST["nromedidor"];
    $marcamed   = $_POST["marcamedidor"];
    $coddiametro       = $_POST["diametrosmedidor"];
    $estadomedidores   = $_POST["estadomedidor"];
    $posicionmed   = $_POST["posicionmedidor"];
    $modelomed     = $_POST["modelomedidor"];
    $tipolectura = $_POST["tipolectura"];
    $aniofabmed  = $_POST["aniofab"];   
    $codtipomedidor  = $_POST["tipomedidor"];
    $fechamov        = $objFunciones->CodFecha($_POST["fechainstmedidor"]);    
    $codcapacidadmedidor = $_POST["capacidadmedidor"];
    $codclasemetrologica = $_POST["codclasemetrologica"];
    $tipofacturacion     = $_POST["tipofacturacion"];
    $ubicacionllavemedidor = $_POST["ubicacionllavemedidor"];
    $lecturaanterior       = $_POST["lecturaanterior"];
    if($lecturaanterior=='')
    {
        $lecturaanterior=0;
    }
    $fechalecturaanterior  = $objFunciones->CodFecha($_POST["fechalecturaanterior"]);
    $lecturaultima         = $_POST["lecturaultima"];
    $fechalecturaultima  = $objFunciones->CodFecha($_POST["fechalecturaultima"]);
    $consumo             = $_POST["consumo"];
    $lecturapromedio = $_POST["lecturapromedio"];
    
    if($lecturapromedio=='')
    {
        $lecturapromedio=0;
    }
    
    $codinspector    = $_POST["codinspector"];
    $trasladomed     = $_POST["trasladomed"];
    $filtromed       = $_POST["medidorfiltro"];
    $precintomed = $_POST["medprecseg"];
    
    $filtromedbueno = $_POST["filtroestado"];
    $observacion    = $_POST["observacion"];
    $horamov         = $_POST["horareinstalacion"];

    $fechareg = date('Y-m-d');
    $horareg  = date('H:i:s');
    $codtipomovimiento = 1;
    
    $idusuario  = $_SESSION['id_user'];
    $estareg    = 1; //EM PROCESO;    
    
    //if ($Op != 2) {
    if ($Op == 0) {
        
        $id= $objFunciones->setCorrelativos("instalacion_med",$codsuc,"0");
        $coddetmedidor = $id[0];
        //die($coddetmedidor);
        $sql = "INSERT INTO micromedicion.detmedidor(coddetmedidor, codemp, codsuc, codcliente, nroinscripcion, ";
        $sql .= "nromed, marcamed, coddiametro, estadomedidores, posicionmed, modelomed, tipolectura, ";
        $sql .= "aniofabmed, codtipomedidor, fechamov, codcapacidadmedidor, codclasemetrologica, ";
        $sql .= "tipofacturacion, ubicacionllavemedidor, lecturaanterior, fechalecturaanterior, ";
        $sql .= "lecturaultima, fechalecturaultima, consumo, lecturapromedio, codinspector, filtromed, ";
        $sql .= "precintomed, filtromedbueno, observacion, fechareg, horareg, codtipomovimiento, ";
        $sql .= " estareg, trasladomed, codusu, horamov, codempcont) ";
        $sql .= "VALUES ( :coddetmedidor, :codemp, :codsuc, :codcliente, :nroinscripcion,:nromed, ";
        $sql .= ":marcamed, :coddiametro, :estadomedidores, :posicionmed, :modelomed, :tipolectura, ";
        $sql .= ":aniofabmed, :codtipomedidor, :fechamov, :codcapacidadmedidor, :codclasemetrologica, ";
        $sql .= ":tipofacturacion, :ubicacionllavemedidor, :lecturaanterior, :fechalecturaanterior, ";
        $sql .= ":lecturaultima, :fechalecturaultima, :consumo, :lecturapromedio, :codinspector, ";
        $sql .= ":filtromed, :precintomed, :filtromedbueno, :observacion, :fechareg, :horareg, ";
        $sql .= ":codtipomovimiento, :estareg, :trasladomed, :codusu, :horamov, :codempcont )";
        
        $resultado = $conexion->prepare($sql);
        $resultado->execute(array(":coddetmedidor"=> $coddetmedidor, 
            ":codemp"=>$codemp,":codsuc"=>$codsuc, 
            ":codcliente"=>$codcliente, ":nroinscripcion"=>$nroinscripcion, 
            ":nromed"=>$nromed, ":marcamed"=>$marcamed, 
            ":coddiametro"=>$coddiametro, ":estadomedidores"=>$estadomedidores, 
            ":posicionmed"=>$posicionmed, ":modelomed"=>$modelomed,
            ":tipolectura"=>$tipolectura, ":aniofabmed"=>$aniofabmed, 
            ":codtipomedidor"=>$codtipomedidor, ":fechamov"=>$fechamov, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, 
            ":codclasemetrologica"=>$codclasemetrologica,
            ":tipofacturacion"=>$tipofacturacion, 
            ":ubicacionllavemedidor"=>$ubicacionllavemedidor, 
            ":lecturaanterior"=>$lecturaanterior, 
            ":fechalecturaanterior"=>$fechalecturaanterior, 
            ":lecturaultima"=>$lecturaultima, 
            ":fechalecturaultima"=>$fechalecturaultima, 
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, 
            ":codinspector"=>$codinspector, ":filtromed"=>$filtromed,
            ":precintomed"=>$precintomed, ":filtromedbueno"=>$filtromedbueno,
            ":observacion"=>$observacion, ":fechareg"=>$fechareg, 
            ":horareg"=>$horareg, ":codtipomovimiento"=>$codtipomovimiento, 
            ":estareg"=>$estareg , ":trasladomed"=>$trasladomed,
            ":codusu"=>$idusuario, ":horamov"=>$horamov, ":codempcont"=>$codempcont ));
        
        if ($resultado->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detmedidor";
            die(2);
        }

		
        $upd ="UPDATE catastro.conexiones SET ";
        $upd .="codubicacion= :codubicacion, codmarca= :codmarca, coddiametrosmedidor= :coddiametrosmedidor, codestadomedidor= :codestadomedidor, ";
        $upd .="posicionmed= :posicionmed, tipolectura= :tipolectura, fechainsmed= :fechainsmed, codmodelo= :codmodelo, aniofabmed= :aniofabmed, ";
        $upd .="codtipomedidor= :codtipomedidor, codcapacidadmedidor= :codcapacidadmedidor, nromed= :nromed, tipofacturacion= :tipofacturacion, ";
        $upd .="lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, lecturaultima= :lecturaultima, ";
        $upd .="fechalecturaultima= :fechalecturaultima, consumo= :consumo, lecturapromedio= :lecturapromedio, codclasemetrologica= :codclasemetrologica";
        $upd .=" WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
    
        $result = $conexion->prepare($upd);
        $result->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":codubicacion"=>$ubicacionllavemedidor, ":codmarca"=>$marcamed, ":coddiametrosmedidor"=>$coddiametro, 
            ":codestadomedidor"=>$estadomedidores, ":posicionmed"=>$posicionmed, ":tipolectura"=>$tipolectura,             
            ":fechainsmed"=>$fechamov, ":codmodelo"=>$modelomed, ":aniofabmed"=>$aniofabmed, ":codtipomedidor"=>$codtipomedidor, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, ":nromed"=>$nromed, ":tipofacturacion"=>$tipofacturacion, 
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,  
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, ":codclasemetrologica"=>$codclasemetrologica ));
        
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar conexiones";
            die();
        }

        
        $updC ="UPDATE catastro.clientes SET 
            tipofacturacion= :tipofacturacion, nromed= :nromed, lecturaultima= :lecturaultima, 
            fechalecturaultima= :fechalecturaultima, lecturaanterior= :lecturaanterior, 
            fechalecturaanterior= :fechalecturaanterior, 
            consumo= :consumo, lecturapromedio= :lecturapromedio
        WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion ";
        
        $res = $conexion->prepare($updC);
        $res->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":tipofacturacion"=>$tipofacturacion, ":nromed"=>$nromed,
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio ));
        
        if ($res->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar clientes";
            die();
        }else{
    		$conexion->commit();		
    		echo $resu=1;
	    }
    }

    //*****----------------***
    // ACTUALIZAR TODO
    //*****----------------***

    if ($Op == 1) {
        $sql = "UPDATE micromedicion.detmedidor SET 
            codcliente= :codcliente, nromed= :nromed, marcamed= :marcamed, coddiametro= :coddiametro, 
            estadomedidores= :estadomedidores, posicionmed= :posicionmed, 
            modelomed= :modelomed, tipolectura= :tipolectura, aniofabmed= :aniofabmed, 
            codtipomedidor= :codtipomedidor, fechamov= :fechamov, 
            codcapacidadmedidor= :codcapacidadmedidor, codclasemetrologica= :codclasemetrologica, 
            tipofacturacion= :tipofacturacion, ubicacionllavemedidor= :ubicacionllavemedidor,  
            lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, 
            lecturaultima= :lecturaultima, fechalecturaultima= :fechalecturaultima, 
            consumo= :consumo, lecturapromedio= :lecturapromedio, 
            codinspector= :codinspector, filtromed= :filtromed, precintomed= :precintomed, 
            filtromedbueno= :filtromedbueno, observacion= :observacion, trasladomed= :trasladomed,
            horamov= :horamov, codempcont= :codempcont ";
        $sql .= " WHERE codemp=:codemp AND codsuc=:codsuc AND coddetmedidor=:coddetmedidor AND nroinscripcion= :nroinscripcion ";
        $resultado = $conexion->prepare($sql);
        $resultado->execute(array(":coddetmedidor"=> $coddetmedidor, 
            ":codemp"=>$codemp, ":codsuc"=>$codsuc, 
            ":codcliente"=>$codcliente, ":nroinscripcion"=>$nroinscripcion, 
            ":nromed"=>$nromed, ":marcamed"=>$marcamed, 
            ":coddiametro"=>$coddiametro, ":estadomedidores"=>$estadomedidores, 
            ":posicionmed"=>$posicionmed, ":modelomed"=>$modelomed,
            ":tipolectura"=>$tipolectura, ":aniofabmed"=>$aniofabmed, 
            ":codtipomedidor"=>$codtipomedidor, ":fechamov"=>$fechamov, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, 
            ":codclasemetrologica"=>$codclasemetrologica,
            ":tipofacturacion"=>$tipofacturacion, ":ubicacionllavemedidor"=>$ubicacionllavemedidor, 
            ":lecturaanterior"=>$lecturaanterior,  ":fechalecturaanterior"=>$fechalecturaanterior, 
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima, 
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, 
            ":codinspector"=>$codinspector, ":filtromed"=>$filtromed,
            ":precintomed"=>$precintomed, ":filtromedbueno"=>$filtromedbueno,
            ":observacion"=>$observacion, ":trasladomed"=>$trasladomed, ":horamov"=>$horamov,
            ":codempcont"=>$codempcont ));
        
        if ($resultado->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detmedidor";
            die();
        }

        
        $upd ="UPDATE catastro.conexiones SET ";
        $upd .="codubicacion= :codubicacion, codmarca= :codmarca, coddiametrosmedidor= :coddiametrosmedidor, codestadomedidor= :codestadomedidor, ";
        $upd .="posicionmed= :posicionmed, tipolectura= :tipolectura, fechainsmed= :fechainsmed, codmodelo= :codmodelo, aniofabmed= :aniofabmed, ";
        $upd .="codtipomedidor= :codtipomedidor, codcapacidadmedidor= :codcapacidadmedidor, nromed= :nromed, tipofacturacion= :tipofacturacion, ";
        $upd .="lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, lecturaultima= :lecturaultima, ";
        $upd .="fechalecturaultima= :fechalecturaultima, consumo= :consumo, lecturapromedio= :lecturapromedio, codclasemetrologica= :codclasemetrologica";
        $upd .=" WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
    
        $result = $conexion->prepare($upd);
        $result->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":codubicacion"=>$ubicacionllavemedidor, ":codmarca"=>$marcamed, ":coddiametrosmedidor"=>$coddiametro, 
            ":codestadomedidor"=>$estadomedidores, ":posicionmed"=>$posicionmed, ":tipolectura"=>$tipolectura,             
            ":fechainsmed"=>$fechamov, ":codmodelo"=>$modelomed, ":aniofabmed"=>$aniofabmed, ":codtipomedidor"=>$codtipomedidor, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, ":nromed"=>$nromed, ":tipofacturacion"=>$tipofacturacion, 
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,  
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, ":codclasemetrologica"=>$codclasemetrologica ));
        
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar conexiones";
            die();
        }

        
        $updC ="UPDATE catastro.clientes SET 
            tipofacturacion= :tipofacturacion, nromed= :nromed, lecturaultima= :lecturaultima, 
            fechalecturaultima= :fechalecturaultima, lecturaanterior= :lecturaanterior, 
            fechalecturaanterior= :fechalecturaanterior, 
            consumo= :consumo, lecturapromedio= :lecturapromedio
        WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion ";
        
        $res = $conexion->prepare($updC);
        $res->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":tipofacturacion"=>$tipofacturacion, ":nromed"=>$nromed,
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio ));
        
        if ($res->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar clientes";
            die();
        }else{
    		$conexion->commit();		
    		echo $resu=1;
	    }
    }

        
?>
