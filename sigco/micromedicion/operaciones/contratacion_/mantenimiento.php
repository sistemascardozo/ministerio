<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");

    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"])?$_POST["Id"]: '';
    $codsuc = $_SESSION['IdSucursal'];
    $guardar = "op=".$Op;
    $Rep = 0;
    $objMantenimiento = new clsDrop();
    
    
    $fechalecturaanterior= date("d/m/Y");
    $fechalecturaultima = date("d/m/Y");
    $fechacontlab= date("d/m/Y");
    $fechacontcam= date("d/m/Y");
    $horacontlab = date("H:m");
    $horacontcam= date("H:m");
    
    if ($Id != '') {
        $sql = "SELECT
        d.coddetmedidor, d.codcliente,
        d.nroinscripcion, c.propietario,
        tc.descripcioncorta||' '||cl.descripcion AS direccion,
        c.nrocalle, d.nromed, d.marcamed, d.coddiametro,
        d.estadomedidores, d.modelomed, d.tipolectura,
        d.aniofabmed, d.codtipomedidor,d.codempcont,
        d.fechamov, d.codcapacidadmedidor,
        d.codclasemetrologica, d.tipofacturacion,
        d.ubicacionllavemedidor, d.lecturaanterior,
        d.fechalecturaanterior, d.lecturaultima,
        d.fechalecturaultima, d.consumo, d.lecturapromedio,
        d.observacion, d.estareg, d.codtiporetiromed,
        d.tpcontrastacion, d.fechacontlab,
        ".$objMantenimiento->SubString('d.horacontlab',1,5)." AS horacontlab,
        d.fechacontcam,
        ".$objMantenimiento->SubString('d.horacontcam',1,5)." AS horacontcam, 
        d.infocaja,
        d.codresulcont, d.codcalibrador,
        d.codbancomedidor, d.medidorresultest
        
        FROM micromedicion.detmedidor AS d 
        INNER JOIN catastro.clientes AS c ON c.codcliente = d.codcliente AND c.nroinscripcion = d.nroinscripcion 
        INNER JOIN public.calles AS cl ON (c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona) 
        INNER JOIN public.tiposcalle AS tc ON (cl.codtipocalle = tc.codtipocalle) 
        INNER JOIN micromedicion.calibrador AS ca ON (ca.codcalibrador = d.codcalibrador AND ca.codemp = d.codemp AND ca.codsuc = d.codsuc)
            
        WHERE d.codemp=1 and d.codsuc=? and d.coddetmedidor= ? ";
        
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc,$Id));
        $items = $consulta->fetch();
        //print_r($items);
        $guardar = $guardar . "&Id2=" . $Id;
        
        $nro= $items["nroinscripcion"];
        $horacontcam= $items["horacontcam"];
        $horacontlab= $items["horacontlab"];
        
        $fec = new DateTime($items["fechalecturaanterior"]);
        $fechalecturaanterior = $fec->format("d/m/Y");
        
        $fe = new DateTime($items["fechalecturaultima"]);
        $fechalecturaultima = $fe->format("d/m/Y");
        
        if($items["fechacontcam"] != '')
        {
            $f = new DateTime($items["fechacontcam"]);
            $fechacontcam= $f->format("d/m/Y");
        }
        
        if($items["fechacontlab"] != '')
        {
            $fc = new DateTime($items["fechacontlab"]);
            $fechacontlab= $fc->format("d/m/Y");
        }
        
    }  
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    var Cod = '<?=$Id ?>'
    var IdGrafico = ''
    var urldir = '<?php echo $_SESSION['urldir'];?>';
    var codsuc 	= <?=$codsuc?>;
    
    if(Cod!='')
    {TpContrastacionDev('<?=$items["tpcontrastacion"] ?>')}   
    
    $(document).ready(function() {
        $("#tabs").tabs();
        
        $("#DivTipos").buttonset();
        $("#DivResultados").buttonset();
                
        $('form').keypress(function(e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function(e) {
            if (e.which == 13)
            {
                return false;
            }
        });        
       
        $("#fechainstmedidor,#fechalecturaanterior,#fechalecturaultima,#fechacontlab,#fechacontcam").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                showButtonPanel: true
            }
        );

        $( "#DivLecturas" ).dialog({
                autoOpen: false,
                height: 350,
                width: 550,
                modal: true,
                resizable:true,
                buttons: {
                        "Aceptar": function() {
                                if(objIdx=="promediar")
                                {
                                        $("#lecturaultima").val($("#lectpromedio").val())
                                        $("#consumo").val($("#lectpromedio").val())
                                        $("#lecturapromedio").val($("#lectpromedio").val())
                                        //calcularconsumo($("#lectpromedio").val())
                                }else{
                                        if($("#obstem").val()=='')
                                        {
                                                Msj($("#obstem"),"Digite el Motivo de Modificación")
                                                return false;
                                        }
                                        $("#obs").val($("#obstem").val())
                                        GuardarP(1)
                                }
                                $( this ).dialog( "close" );
                        },
                        "Cancelar": function() {
                                $( this ).dialog( "close" );
                        }
                },
                close: function() {

                }
            });
        
    });
    
    function TpContrastacionDev(Op)
    {
        //alert(Op);
        if(Op==1)
        {
            $("#fechacontcam").attr('disabled',true)
            $("#fechacontcam").val('00/00/0000')           
            $("#horacontcam").attr('disabled',true)
            
            $("#fechacontlab").attr('disabled',false)
            //$("#fechacontlab").datepicker('show')
            $("#horacontlab").attr('disabled',false)            
            
        }
        else
            {
                $("#fechacontcam").attr('disabled',false)
                //$("#fechacontcam").datepicker('show')
                $("#horacontcam").attr('disabled',false)
                
                $("#fechacontlab").attr('disabled',true)
                $("#fechacontlab").val('00/00/0000')
                $("#horacontlab").attr('disabled',true)                
                
            }
    }
    
    function buscar_usuarios()
    {
        object = "usuario"
        AbrirPopupBusqueda('<?php echo $_SESSION['urldir'];?>sigco/micromedicion/operaciones/retiro/?Op=5', 1000, 430)
    }
    
    function Recibir(id)
    {
        if (object == "usuario")
        {
            $("#nroinscripcion").val(id)
            cargar_datos_usuario(id)
        }

    }
    
    function cargar_datos_usuario(id)
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>ajax/medidor_instalacion.php',
            type: 'POST',
            async: true,
            data: 'codsuc='+codsuc+'&Id='+id,
            dataType: "json",
            success: function(datos) {
                $("#codcliente").val(datos.codcliente)
                $("#Cliente").val(datos.propietario)
                $("#direccion").val(datos.direccion)
                $("#nrocalle").val(datos.nrocalle)
                
                $("#nromedidor").val(datos.nromed)
                $("#marcamedidor").val(datos.codmarca)
                $("#diametrosmedidor").val(datos.coddiametrosmedidor)
                $("#estadomedidor").val(datos.codestadomedidor)
                $("#posicionmedidor").val(datos.posicionmed)
                $("#modelomedidor").val(datos.codmodelo)                
                $("#tipolectura").val(datos.tipolectura)
                $("#aniofab").val(datos.aniofabmed)
                $("#tipomedidor").val(datos.codtipomedidor)                
                $("#capacidadmedidor").val(datos.codcapacidadmedidor)
                $("#codclasemetrologica").val(datos.codclasemetrologica)
                $("#tipofacturacion").val(datos.tipofacturacion)
                $("#ubicacionllavemedidor").val(datos.codubicacion)
                $("#lecturaanterior").val(datos.lecturaanterior)
                $("#fechalecturaanterior").val(datos.fechalecturaanterior)
                $("#lecturaultima").val(datos.lecturaultima)
                $("#fechalecturaultima").val(datos.fechalecturaultima)
                $("#consumo").val(datos.consumo)
                $("#lecturapromedio").val(datos.lecturapromedio)
                $("#nroinscripcion").val(datos.nroinscripcion)       
                $("#coddetmedidor").val(datos.coddetmedidor)

                $("#codempcont").val(datos.codempcont)

                var Ope= datos.tpcontrastacion
                if(Ope==1)
                {
                    //constlab
                    $("#constlab").attr('checked',true)
                }
                else
                    {
                        //medidorinap
                        $("#medidorinap").attr('checked',true)
                    }

                $("#DivTipos").buttonset('refresh')

                TpContrastacion(Ope)

                $("#codtiporetiromed").val(datos.codtiporetiromed)
                $("#codclasemetrologica").val(datos.codclasemetrologica)                

                
               
            }
        });
    }
    
    function ValidarForm(Op)
    {

        if ($("#Cliente").val() == "")
        {
            Msj("#Cliente", 'El Nombre del Propietario no puede ser NULO')
            return false;
        }
        
        if ($("#tipofacturacion").val() == '')
        {
            Msj("#tipofacturacion", 'Seleccione el Tipo de Facturacion')
            return false;
        }
        if ($("#fechainstmedidor").val() == '')
        {
            Msj("#fechainstmedidor", 'Digite la Fecha de Instalacion')
            return false;
        }
        if ($("#fechalecturaanterior").val() == 0)
        {
            Msj("#fechalecturaanterior", 'Digite la Fecha de Lectura Anterior')
            return false;
        }
        if ($("#fechalecturaultima").val() == 0)
        {
            Msj("#fechalecturaultima", 'Digite la Fecha Ultima de Lectura')
            return false;
        }
        
        GuardarP(Op)
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }
    
    function VerTarifas()
    {
        if($("#tipofacturacion").val()!=2)
        {
                alert("El Tipo de Facturacion no permite promediar asignar Consumos")
                return false;
        }
        $("#DivTarifas").dialog('open');
    }
    
    function AddTarifa(consumo)
    {
        $("#DivTarifas").dialog('close');
        $("#consumo").val(consumo);
        actualizacion(document.getElementById('consumo'),0)
    }
    
    function calcularconsumo(obj)
    {
        anterior = $("#lecturaanterior").val()
        ultima	= obj

        if(ultima=="")
        {
                ultima=0;
        }
        $("#consumo").val(parseFloat(ultima)-parseFloat(anterior))
    }
    
    function promediar_lecturas()
    {
        if($("#tipofacturacion").val()!=1)
        {
                alert("El Tipo de Facturacion no permite promediar las Lecturas")
                return false;
        }
        verlecturas();
        objIdx = "promediar";
        $( "#DivLecturas" ).dialog( "open" );
    }
    
    function verlecturas()
    {
        $.ajax({
            url:'ver_lecturas.php',
            type:'POST',
            async:true,
            data:'codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val(),
            success:function(datos){
                   $("#div_lecturas").html(datos)
            }
        }) 
    }
    
    function asignarconsumo()
    {
        var catetar="";
        var tfacturacion=$("#tipofacturacion").val();

        if(tfacturacion!=2)
        {
            alert('El Tipo de Facturacion no Admite que se asigne el Consumo')
            return
        }

        for (i=1;i<=$("#cont_unidades").val();i++) 
        { 		
            try
            {
                if($("#principal"+i).val()==1)
                {
                        catetar=$("#tarifas"+i).val()
                }
            }catch(exp)
                {

                }
        }

        mostrarconsumo(catetar)
    }
    
    function mostrarconsumo(catetar)
    {
        $.ajax({
            url:'mostrar_volumen.php',
            type:'POST',
            async:true,
            data:'catetar='+catetar+'&codsuc='+codsuc,
            success:function(datos){
                   $("#lecturaanterior").val(0)
                   $("#lecturaultima").val(0)
                   $("#consumo").val(datos)
            }
        }) 
    }
    
    function TpContrastacion(Op)
    {
        //alert(Op);
        if(Op==1)
        {
            $("#fechacontcam").attr('disabled',true)
            $("#fechacontcam").val('00/00/0000')           
            $("#horacontcam").attr('disabled',true)
            
            $("#fechacontlab").attr('disabled',false)
            //$("#fechacontlab").datepicker('show')
            $("#horacontlab").attr('disabled',false)            
            AddDetalleResultado(1);
        }
        else
            {
                $("#fechacontcam").attr('disabled',false)
                //$("#fechacontcam").datepicker('show')
                $("#horacontcam").attr('disabled',false)
                
                $("#fechacontlab").attr('disabled',true)
                $("#fechacontlab").val('00/00/0000')
                $("#horacontlab").attr('disabled',true)                
                AddDetalleResultado(2);
            }
    }
    
    function AddDetalleResultado(Op)
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>ajax/tpcontrastacion.php',
            type: 'POST',
            async: true,
            data: 'Op='+Op,            
            success: function(datos) {                
                $("#DetResultado tbody").html(datos);
            }
        })
    }
    
    function CalcularD(Op)
    {   
        var lectinicial = $("#lecturainicio_"+Op).val()
        var lectfin = $("#lecturafin_"+Op).val()
                
        var diferencia= parseFloat(lectfin) - parseFloat(lectinicial)
        //var cuotamensual= parseFloat(importe)/parseFloat(nrocuotas)
        if(isNaN(diferencia))   diferencia=0
        
        $("#diferencialectura_"+Op).val(parseFloat(diferencia).toFixed(2))        
    }
    
</script>

<div align="">
    
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        
        <div id="tabs">
            <ul>
                <li style="font-size:10px"><a href="#tabs-2">Datos del Titular</a></li>
                <li style="font-size:10px"><a href="#tabs-3">Contrastación del Medidor</a></li>
                <li style="font-size:10px"><a href="#tabs-4">Datos del Medidor</a></li>
                <li style="font-size:10px"><a href="#tabs-5">Resultado de Contrastación</a></li>            
            </ul>            
            
            <div id="tabs-2" style="height:330px">
                
                <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                    <tbody>
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="TitDetalle">Nro. Inscripcion</td>
                            <td width="30" align="center">:</td>
                            <td>
                                <input type="text" name="nroinscripcion" id="nroinscripcion" class="inputtext" readonly="readonly" value="<?=$items["nroinscripcion"] ?>" style="width:70px;"/>
                                <span class="MljSoft-icon-buscar" title="Buscar Personas" onclick="buscar_usuarios();"></span>
                            </td>
                            <td  colspan="2">
                                <?php 

                                    $EST=$items['estareg']; 
                                    switch ($EST) {                                    
                                        case 1:
                                            $EST= "INSTALADO"; break;
                                        case 2:
                                            $EST= "ATENDIDO"; break;
                                        case 3:
                                            $EST= "RETIRADO"; break;
                                        case 4:
                                            $EST= "ATENDIDO"; break;
                                    }
                                ?>
                                <span style="color: red;font-weight: bold;"><?=$EST?></span>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="TitDetalle">Nombre Titular</td>
                            <td align="center">:</td>
                            <td colspan="4" class="CampoDetalle">
                                <input type="hidden" name="codcliente" id="codcliente" size="10" class="inputtext" readonly="readonly" value="<?=$items["codcliente"] ?>"/>
                                <!-- <span class="MljSoft-icon-buscar" title="Buscar Personas" onclick="buscar_usuarios();"></span>-->
                                <input class="inputtext" name="propietario" type="text" id="Cliente" size="50" maxlength="200"value="<?=$items["propietario"] ?>" onkeypress="CambiarFoco(event, 'sectorsol')"   />

                            </td>
                        </tr>                
                        <tr>
                            <td class="TitDetalle" >Direccion</td>
                            <td align="center">:</td>
                            <td class="CampoDetalle">
                                <input type="text" name="direccion" id="direccion" value="<?=$items["direccion"] ?>" onkeypress="CambiarFoco(event, 'callesol')" class="inputtext" style="width:220px" />
                            </td>
                            <td width="117" class="CampoDetalle" align="right">Nro. calle</td>
                            <td width="30" align="center">:</td>
                            <td class="CampoDetalle" colspan="3">
                                <input type="text" name="nrocalle" id="nrocalle" class="inputtext" maxlentgth="6" value="<?=$items["nrocalle"] ?>" onkeypress="CambiarFoco(event, 'nrodocumento');" style="width:200px;" />
                            </td> 
                        </tr>               
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div id="tabs-3">     
                <center>
                <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                    <tr>
                        <td colspan="8" >&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" class="TitDetalle" align="right">Tipo retiro</td>
                        <td align="center" width="30">:</td>
                        <td colspan="4">
                            <select name="codtiporetiromed" id="codtiporetiromed" style="width:320px">
                                <?php
                                $Sql = "SELECT * FROM micromedicion.tiporetiromedidor
                                        WHERE estareg = 1 ORDER BY descripcion ASC";
                                $Consulta = $conexion->query($Sql);
                                foreach ($Consulta->fetchAll() as $row1) {
                                    $Sel = "";
                                    if ($items["codtiporetiromed"] == $row1["codtiporetiromed"])
                                        $Sel = 'selected="selected"';
                                   ?>
                                    <option value="<?php echo $row1["codtiporetiromed"];?>" <?=$Sel?>  ><?php echo $row1["descripcion"];?></option>
                                <?php }?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">Emp. Contrastadora</td>
                        <td align="center">:</td>
                        <td class="CampoDetalle" colspan="4">
                            <select name="codempcont" id="codempcont" style="width:250px">
                                <?php
                                $Sql = "SELECT * FROM micromedicion.empresacontrastadora AS e WHERE e.estareg = 1 AND e.codsuc= ".$codsuc;
                                $Consulta = $conexion->query($Sql);
                                foreach ($Consulta->fetchAll() as $rows) {
                                    $Sel = "";
                                    if ($items["codempcont"] == $rows["codempcont"])
                                        $Sel = 'selected="selected"';
                                    ?>
                                    <option value="<?php echo $rows["codempcont"]; ?>" <?=$Sel ?>  ><?php echo strtoupper(utf8_decode($rows["razonsocial"])); ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>                      
                    <tr>                        
                        <td colspan="8" class="TitDetalle" align="center">                        
                            <br />
                            <?php
                                if($items["tpcontrastacion"]!= '')
                                {
                                    if($items["tpcontrastacion"]==1)
                                    {
                                        $ver= "checked='checked'";
                                    }else {$ver1= "checked='checked'";}
                                }
                            ?>
                            <div id="DivTipos" style="display:inline">
                                <input type="radio" name="tpcontrastacion" id="constlab" value="1" onchange="TpContrastacion(1)" <?=$ver?> />
                                <label for="constlab">Constratación en Laboratorio</label>
                                <input type="radio" name="tpcontrastacion" id="medidorinap" value="2" onchange="TpContrastacion(2)"  <?=$ver1?> />
                                <label for="medidorinap">Medidor Inoperativo en constratación en campo</label>
                            </div> 
                            
                        </td>
                        
                    </tr>  
                    <tr>
                        <td class="TitDetalle" colspan="3" align="right">Fecha Contrastacion Laboratorio</td>
                        <td align="center">:</td>
                        <td>
                            <input type="text" name="fechacontlab" id="fechacontlab" class="inputtext" maxlentgth="6" value="<?=$fechacontlab ?>" style="width:85px;" />
                        </td>
                        <td class="TitDetalle" align="right">Fecha Contrastacion Campo</td>
                        <td width="30" align="center">:</td>
                        <td width="115">
                            <div id="fechcontcam">
                                <input type="text" name="fechacontcam" id="fechacontcam" class="inputtext" maxlentgth="6" value="<?=$fechacontcam ?>" style="width:85px;" />
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="TitDetalle" colspan="3" align="right">Hora Contrastacion Laboratorio</td>
                        <td align="center">:</td>
                        <td>
                            <input  id="horacontlab" name="horacontlab" type="text" value="<?=$horacontlab ?>" maxlentgth="6" class="inputtext" style="width:50px;">
                        </td>
                        <td class="TitDetalle" align="right">Hora Contrastacion Campo</td>
                        <td align="center">:</td>
                        <td>
                            <input  id="horacontcam" name="horacontcam" type="text" value="<?=$horacontcam ?>" maxlentgth="6" class="inputtext" style="width:50px;">
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="8" >&nbsp;</td>
                    </tr>
                    <tr  bgcolor="#42C1FF">
                        <td class="TitDetalle" colspan="5"><b>El resultado de la contrastacion indica que el medidor es :</b></td>
                        <td>
                            <select name="codresulcont" id="codresulcont" style="width:220px">
                                <?php
                                $Sql = "SELECT * FROM public.resultadocontrastacion
                                    WHERE estareg = 1 ORDER BY descripcion asc ";
                                $Consulta = $conexion->query($Sql);
                                foreach ($Consulta->fetchAll() as $row1) {
                                    $Sel = "";
                                    if ($items["codresulcont"] == $row1["codresulcont"])
                                        $Sel = 'selected="selected"';
                                   ?>
                                    <option value="<?php echo $row1["codresulcont"];?>" <?=$Sel?>  ><?php echo $row1["descripcion"];?></option>
                                <?php }?>
                            </select>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="8" >&nbsp;</td>
                    </tr>
                    <tr>                        
                        <td class="TitDetalle" align="right" valign="top">Información de la caja del Medidor</td>
                        <td width="30" align="center" valign="top">:</td>
                        <td colspan="6" valign="top">
                            <textarea name="infocaja" id="infocaja" class="text ui-widget-content ui-corner-all" style="width: 410px;height: 50px"  cols="10" rows="5" ><?=$items["infocaja"]; ?></textarea></td>
                    </tr>
                    <tr>                        
                        <td class="TitDetalle" align="right" valign="top">Observacion</td>
                        <td align="center" valign="top">:</td>
                        <td colspan="6" valign="top">
                            <textarea name="observacion" id="observacion" class="text ui-widget-content ui-corner-all" style="width: 410px;height: 50px"  cols="10" rows="5" ><?=$items["observacion"]; ?></textarea></td>
                    </tr>
                    <tr>
                      <td class="TitDetalle" align="right" valign="top">&nbsp;</td>
                      <td align="center" valign="top">&nbsp;</td>
                      <td colspan="6" valign="top">&nbsp;</td>
                    </tr>  
                    
                </table>
                </center>
                <br />
            </div>
            <div id="tabs-4">
                
                <center>
                <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                    <tbody>
                        <tr><td colspan="6">&nbsp;</td></tr>
                        <tr>
                            <td align="right">Cod. Medidor</td>
                            <td width="30" align="center">:</td>
                            <td >
                                <input type="text" name="coddetmedidor" id="coddetmedidor" class="inputtext" size="15" value="<?=$items["coddetmedidor"] ?>" readonly />
                            </td>
                            <td align="right">Nro. Medidor</td>
                            <td width="30" align="center">:</td>
                            <td >
                                <input type="text" name="nromedidor" id="nromedidor" class="inputtext" size="15" value="<?=$items["nromed"] ?>" />
                            </td>                        
                        </tr>
                        <tr>
                            <td align="right">Calibrador</td>
                            <td align="center">:</td>
                            <td class="CampoDetalle">
                                <select name="codcalibrador" id="codcalibrador" style="width:200px">
                                    <?php
                                    $Sql = "SELECT * FROM micromedicion.calibrador AS ca WHERE ca.estareg = 1 AND ca.codsuc= ".$codsuc;
                                    $Consulta = $conexion->query($Sql);
                                    foreach ($Consulta->fetchAll() as $rows) {
                                        $Sel = "";
                                        if ($items["codcalibrador"] == $rows["codcalibrador"])
                                            $Sel = 'selected="selected"';
                                        ?>
                                        <option value="<?php echo $rows["codcalibrador"]; ?>" <?=$Sel ?>  ><?php echo strtoupper(utf8_decode($rows["nombres"])); ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td colspan="3">&nbsp;</td>
                        </tr>                    
                        <tr>
                            <td width="19%" align="right">Marca</td>
                            <td width="2%" align="center">:</td>
                            <td width="25%">
                                <?php $objMantenimiento->drop_marca_medidor($items["codmarca"], ''); ?>
                            </td>
                            <td width="20%" align="right">Diametro</td>
                            <td width="2%" align="center">:</td>
                            <td width="32%">
                                <?php $objMantenimiento->drop_diametros_medidor($items["coddiametro"], ''); ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Estado</td>
                            <td align="center">:</td>
                            <td>
                                <?php $objMantenimiento->drop_estado_medidor($items["estadomedidores"], ''); ?>
                            </td>
                            <td align="right">Posicion</td>
                            <td align="center">:</td>
                            <td>
                                <select id="posicionmedidor" name="posicionmedidor" style="width:150px" class="select" >
                                    <option value="1" <?=$items["posicionmed"] == 1 ? "selected='selected'" : "" ?> >CORRECTA</option>
                                    <option value="2" <?=$items["posicionmed"] == 2 ? "selected='selected'" : "" ?> >INCORRECTA</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Modelo</td>
                            <td align="center">:</td>
                            <td>
                                <?php $objMantenimiento->drop_modelo_medidor($items["modelomed"], ''); ?>
                            </td>
                            <td align="right">Tipo de Lectura</td>
                            <td align="center">:</td>
                            <td>
                                <select id="tipolectura" name="tipolectura" style="width:150px" class="select" >
                                    <option value="1" <?=$items["tipolectura"] == 1 ? "selected='selected'" : "" ?> >CIRCULAR</option>
                                    <option value="2" <?=$items["tipolectura"] == 2 ? "selected='selected'" : "" ?> >DIRECTA</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">A&ntilde;o de Fabricacion</td>
                            <td align="center">:</td>
                            <td><input type="text" name="aniofab" id="aniofab" class="inputtext" maxlength="4" size="15" value="<?=$items["aniofabmed"] ?>"   /></td>
                            <td align="right">Tipo Medidor</td>
                            <td align="center">:</td>
                            <td>
                                <?php $objMantenimiento->drop_tipo_medidor($items["codtipomedidor"], ''); ?>
                            </td>
                        </tr>
                        <tr>
                            <!--
                            <td align="right">Fecha Instalacion</td>
                            <td align="center">:</td>
                            <td>
                                <input type="text" name="fechainstmedidor" id="fechainstmedidor" class="inputtext" maxlentgth="6" size="12" value="<?=$fechamov ?>" />
                            </td>-->
                            <td colspan="3">&nbsp;</td>
                            <td align="right">Capacidad</td>
                            <td align="center">:</td>
                            <td>
                                <?php $objMantenimiento->drop_capacidad_medidor($items["codcapacidadmedidor"], ''); ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Clase Metrológica</td>
                            <td align="center">:</td>
                            <td class="CampoDetalle">
                                <select name="codclasemetrologica" id="codclasemetrologica" style="width:150px">
                                    <?php
                                    $Sql = "SELECT * FROM micromedicion.clasemetrologica WHERE estareg = 1 ";
                                    $Consulta = $conexion->query($Sql);
                                    foreach ($Consulta->fetchAll() as $row1) {
                                        $Sel = "";
                                        if ($items["codclasemetrologica"] == $row1["codclasemetrologica"])
                                            $Sel = 'selected="selected"';
                                        ?>
                                        <option value="<?php echo $row1["codclasemetrologica"]; ?>" <?=$Sel ?>  ><?php echo $row1["descripcion"]; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td align="right">Tipo de Facturacion</td>
                            <td align="center">:</td>
                            <td>
                                <select id="tipofacturacion" name="tipofacturacion" style="width:150px" class="select" >
                                    <option value="100" >--Seleccione el Tipo--</option>
                                    <option value="0"   <?=$items["tipofacturacion"] == 0 ? "selected='selected'" : "" ?> >CONSUMO LEIDO</option>
                                    <option value="1"   <?=$items["tipofacturacion"] == 1 ? "selected='selected'" : "" ?> >CONSUMO PROMEDIADO</option>
                                    <option value="2"   <?=$items["tipofacturacion"] == 2 ? "selected='selected'" : "" ?> >CONSUMO ASIGNADO</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Ub. Llave</td>
                            <td align="center">:</td>
                            <td>
                                <?php $objMantenimiento->drop_ubicacion_llave_medidor($items["ubicacionllavemedidor"], ''); ?>
                            </td>
                            <td align="right">Lectura Anterior</td>
                            <td align="center">:</td>
                            <td>
                                <input type="text" name="lecturaanterior" id="lecturaanterior" class="inputtext" maxlentgth="6" size="15" value="<?=$items["lecturaanterior"] ?>" onkeyup="calcularconsumo(document.getElementById('lecturaultima').value);"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Fecha Lect. Anterior</td>
                            <td align="center">:</td>
                            <td>
                                <input type="text" name="fechalecturaanterior" id="fechalecturaanterior" class="inputtext" maxlentgth="6" value="<?=$fechalecturaanterior ?>" style="width:80px;" />
                            </td>
                            <td align="right">Lectura Ultima</td>
                            <td align="center">:</td>
                            <td>
                                <input type="text" name="lecturaultima" id="lecturaultima" class="inputtext" maxlentgth="6" size="15" value="<?=$items["lecturaultima"] ?>" onkeyup="calcularconsumo(this.value);"  />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Fecha Lect. Ultima</td>
                            <td align="center">:</td>
                            <td>
                                <input type="text" name="fechalecturaultima" id="fechalecturaultima" class="inputtext" maxlentgth="6" value="<?=$fechalecturaultima ?>" style="width:80px;" />
                            </td>
                            <td align="right">Consumo</td>
                            <td align="center">:</td>
                            <td>
                                <input type="text" name="consumo" id="consumo" class="inputtext" maxlentgth="6" size="15" value="<?=$items["consumo"] ?>"  readonly="readonly"  />
                                <span class="icono-icon-info" title="Asignar Consumos" onclick="VerTarifas()"></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">&nbsp;</td>
                            <td align="center">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">Promedio</td>
                            <td align="center">:</td>
                            <td>
                                <input type="text" name="lecturapromedio" id="lecturapromedio" class="inputtext" maxlentgth="6" size="15" value="<?=$items["lecturapromedio"] ?>"  readonly="readonly" />
                                <span class="icono-icon-info" title="Promediar Lecturas" onclick="promediar_lecturas()"></span>
                            </td>
                        </tr>                        
                        <tr><td colspan="6">&nbsp;</td></tr>
                    </tbody>
                </table>
                </center>
            </div>
            <div id="tabs-5" style="height:330px">
                <center>
                <table width="820" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                    <tr>
                      <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="291" align="right">Banco de medidor</td>
                        <td width="10" align="center">:</td>
                        <td width="427">                        
                            <select name="codbancomedidor" id="codbancomedidor" style="width:200px">
                                <?php
                                $Sql = "SELECT * FROM micromedicion.bancomedidor AS bn WHERE bn.estareg = 1 AND bn.codsuc= ".$codsuc;
                                $Consulta = $conexion->query($Sql);
                                foreach ($Consulta->fetchAll() as $rows) {
                                    $Sel = "";
                                    if ($items["codbancomedidor"] == $rows["codbancomedidor"])
                                        $Sel = 'selected="selected"';
                                    ?>
                                    <option value="<?php echo $rows["codbancomedidor"]; ?>" <?=$Sel ?>  ><?php echo strtoupper(utf8_decode($rows["descripcion"])); ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                      <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <?php
                                $med=$items['medidorresultest'];                                
                                switch ($med) {
                                    case 1:
                                        $act1=" checked='checked' "; break;
                                    case 2:
                                        $act2=" checked='checked' "; break;
                                    case 3:
                                        $act3=" checked='checked' "; break;
                                }
                            ?>
                            <div id="DivResultados" style="display:inline">
                                <input type="radio" name="tpmedidor" id="medidornew" value="1" <?=$act1 ?> checked='checked' />
                                <label for="medidornew">Medidor Nuevo</label>
                                <input type="radio" name="tpmedidor" id="medidorser" value="2" <?=$act2 ?>/>
                                <label for="medidorser">Medidor en Servicio</label>
                                <input type="radio" name="tpmedidor" id="medidorino" value="3" <?=$act3 ?>/>
                                <label for="medidorino">Medidor en Inoperativo</label>
                            </div>
                        </td>
                    </tr>                    
                    <tr>
                      <td colspan="3">&nbsp;</td>
                    </tr>
                </table>
                <br />
                
                <table width="800" border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="DetResultado" rules="all">
                    <thead class="ui-widget-header" >
                        <tr align="center" valign="top">
                            <td width="33" rowspan="2" >Item</td>
                            <td colspan="2" width="103" rowspan="2">Caudal de Ensayo (Q)</td>
                            <td width="74" rowspan="2">Presion (Bar)</td>
                            <td width="79" rowspan="2">Temperatu C°</td>
                            <td width="66" rowspan="2">Volumen Patron (L)(1)</td>
                            <td width="65" rowspan="2">Lectura Inicial (2)</td>
                            <td width="65" rowspan="2">Lectura Final (3)</td>
                            <td width="101" rowspan="2">
                                <p>Diferencias de lecturas </p>
                                <p>(4)=(3)-(2)</p></td>
                            <td colspan="2">Error %</td>
                        </tr>
                        <tr align="center">
                            <td width="74" height="23">Relativa (5)</td>
                            <td width="76">Permisible</td>
                        </tr>
                    </thead>
                    <?php
                        if($Id!='')
                        {
                            $Sql = "SELECT
                                d.item, d.codtipoensayocaudal,
                                tp.descripcion,
                                d.caudalensayo,
                                d.presion,
                                d.temperatura,
                                d.volumenpatron,
                                d.lecturainicio,
                                d.lecturafin,
                                d.diferencialectura,
                                d.errorrelativa,
                                d.errorpermisible
                                FROM
                            micromedicion.detcontrastacion AS d
                            INNER JOIN public.tipoensayocaudal AS tp ON tp.codtipoensayocaudal = d.codtipoensayocaudal";
                            $Sql .=" WHERE d.coddetmedidor = ".$Id." AND d.codsuc= ".$codsuc." AND d.nroinscripcion= ".$nro." ";
                            $Sql.=" ORDER BY d.item ASC  ";
                            $conn=$conexion->query($Sql);
                            //die($Sql);    
                            $i = 0;
                            $tr="";
                            foreach($conn->fetchAll() as $rowD)
                            {
                                $i ++;
                                $tr.='<tr id="'.$i.'" class="tr-detalle" style="height: 20px">';
                                $tr.='<td>'.$i.'</td>';
                                $tr.='<td>'.$rowD["descripcion"].'<input type="hidden" name="codtipoensayocaudal[]" value="'.$rowD['codtipoensayocaudal'].'" /></td>';
                                $tr.='<td align="center"><input type="text" name="caudalensayo[]" value="'.$rowD['caudalensayo'].'" size="6" /></td>';
                                $tr.='<td align="center"><input type="text" name="presion[]" value="'.$rowD['presion'].'" size="6" /></td>';
                                $tr.='<td align="center"><input type="text" name="temperatura[]" value="'.$rowD['temperatura'].'" size="6" /></td>';
                                $tr.='<td align="center"><input type="text" name="volumenpatron[]" value="'.$rowD['volumenpatron'].'" size="6" /></td>';
                                $tr.='<td align="center"><input type="text" id="lecturainicio_'.$i.'" name="lecturainicio[]" value="'.$rowD['lecturainicio'].'" size="6" onkeyup="CalcularD('.$i.');" /></td>';
                                $tr.='<td align="center"><input type="text" id="lecturafin_'.$i.'" name="lecturafin[]" value="'.$rowD['lecturafin'].'" size="6" onkeyup="CalcularD('.$i.');" /></td>';        
                                $tr.='<td align="center"><input type="text" id="diferencialectura_'.$i.'" name="diferencialectura[]" value="'.$rowD['diferencialectura'].'" size="6" /></td>';
                                $tr.='<td align="center"><input type="text" name="errorrelativa[]" value="'.$rowD['errorrelativa'].'" size="6" /></td>';
                                $tr.='<td align="center"><input type="text" name="errorpermisible[]" value="'.$rowD['errorpermisible'].'" size="6" /></td>';
                                $tr.='</tr>';

                            }
                            echo $tr;
                            }
                        ?>
                    <tbody>                        
                    </tbody>
                    <tfoot class="ui-widget-header" >
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tfoot>
                </table>
                </center>
            </div>
            
        </div>    
        

    </form>
    
    <div id="DivLecturas" title="Actualizacion y Modificacion Catastral"  >
        <div id="div_lecturas"></div>
    </div>
</div>

<div id="DivTarifas" title="Tarifas" style="display: none;">
	<div id="DivTarifasAcordion">
	<?php
	$sql = "select * from facturacion.tarifas ORDER BY catetar";
	$ConsultaTar =$conexion->query($sql);
	foreach($ConsultaTar->fetchAll() as $tarifas)
	{
	?>
  <h3><?=strtoupper($tarifas["nomtar"])?></h3>
  <div style="height: 100px; display: block;">
    <table width="100%" border="0" align="center" cellspacing="0" class="ui-widget" rules="cols">
	    <thead class="ui-widget-header" style="font-size:10px">
	      <tr>
	        <td>&nbsp;</td>
	        <td align="center">Volumen</td>
	        <td align="center">Importe Agua</td>
	        <td align="center">Importe Desague</td>
	        <td align="center">&nbsp;</td>
	        </tr>
	      </thead>
	      <tr>
	        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Inicial</td>
	        <td align="right">
	          <label class="inputtext" id="hastarango1" ><?=number_format($tarifas["hastarango1"],2)?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsmin" ><?=$tarifas["impconsmin"]?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsmindesa"><?=$tarifas["impconsmindesa"]?></label>
	          </td>
	          <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango1"],2)?>')"></span></td>
	        </tr>
	      <tr>
	        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Intermedio </td>
	        <td align="right">
	          <label class="inputtext" id="hastarango2"><?=number_format($tarifas["hastarango2"],2)?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsmedio"><?=$tarifas["impconsmedio"]?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsmediodesa"><?=$tarifas["impconsmediodesa"]?> </label>
	          </td>
	          <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango2"],2)?>')"></span></td>
	        </tr>
	      <tr>
	        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Final </td>
	        <td align="right">
	          <label class="inputtext" id="hastarango3"><?=number_format($tarifas["hastarango3"],2)?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsexec"><?=$tarifas["impconsexec"]?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsexecdesa"><?=$tarifas["impconsexecdesa"]?></label>
	          </td>
	          <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango3"],2)?>')"></span></td>
	        </tr>
	      </table>
  </div>
  <?php
  	}
  ?>
</div>
</div>