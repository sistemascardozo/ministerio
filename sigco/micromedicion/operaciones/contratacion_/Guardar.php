<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
    
    include('../../../../objetos/clsFunciones.php');
    $conexion->beginTransaction();
    
    $Op = $_GET["Op"];
    $objFunciones = new clsFunciones();

    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $coddetmedidorant = $_POST["coddetmedidor"];
    $coddetmedidor = $_POST["coddetmedidor"];
    $codcliente    = $_POST["codcliente"];
    $nroinscripcion= $_POST["nroinscripcion"];
    
    $codempcont = $_POST["codempcont"];

    $nromed     = $_POST["nromedidor"];
    $marcamed   = $_POST["marcamedidor"];
    $coddiametro       = $_POST["diametrosmedidor"];
    $estadomedidores   = $_POST["estadomedidor"];
    $posicionmed   = $_POST["posicionmedidor"];
    $modelomed     = $_POST["modelomedidor"];
    $tipolectura = $_POST["tipolectura"];
    $aniofabmed  = $_POST["aniofab"];   
    $codtipomedidor  = $_POST["tipomedidor"];    
    $codcapacidadmedidor = $_POST["capacidadmedidor"];
    $codclasemetrologica = $_POST["codclasemetrologica"];
    $tipofacturacion     = $_POST["tipofacturacion"];
    $ubicacionllavemedidor = $_POST["ubicacionllavemedidor"];
    $lecturaanterior       = $_POST["lecturaanterior"];
    $fechalecturaanterior  = $objFunciones->CodFecha($_POST["fechalecturaanterior"]);
    $lecturaultima         = $_POST["lecturaultima"];
    $fechalecturaultima  = $objFunciones->CodFecha($_POST["fechalecturaultima"]);
    $consumo             = $_POST["consumo"];
    $lecturapromedio     = $_POST["lecturapromedio"];
    
    if($lecturapromedio=='')
    {
        $lecturapromedio=0;
    }
    
    $codresulcont     = $_POST["codresulcont"];
    $codcalibrador    = $_POST["codcalibrador"];   
    $codtiporetiromed = $_POST["codtiporetiromed"];
    $tpcontrastacion  = $_POST["tpcontrastacion"];
    if($tpcontrastacion== 1)
    {
        $fechacontlab = $objFunciones->CodFecha($_POST["fechacontlab"]);
        $horacontlab = $_POST["horacontlab"];
        $fechacontcam= null;
        $horacontcam= null;
    }
    else {
        $fechacontlab = null;
        $horacontlab = null;
        $fechacontcam= $objFunciones->CodFecha($_POST["fechacontcam"]);
        $horacontcam= $_POST["horacontcam"];
    }       
    
    $observacion    = $_POST["observacion"];
    $infocaja = $_POST["infocaja"];
    $codbancomedidor= $_POST["codbancomedidor"];
    $tpmedidor= $_POST["tpmedidor"];
    
    $fechareg = date('Y-m-d');
    $horareg  = date('H:i:s');
    $codtipomovimiento = 4;
    
    $idusuario  = $_SESSION['id_user'];
    $estareg    = 4; //EM PROCESO;    
    
    //if ($Op != 2) {
    if ($Op == 0) {
        
        // INSERTAR DETALLE MEDIDOR
        $id= $objFunciones->setCorrelativos("instalacion_med",$codsuc,"0");
        $coddetmedidor = $id[0];
        //die($coddetmedidor);
        $sql = "INSERT INTO micromedicion.detmedidor(coddetmedidor, codemp, codsuc, codcliente, nroinscripcion, ";
        $sql .= "nromed, marcamed, coddiametro, estadomedidores, posicionmed, modelomed, tipolectura, ";
        $sql .= "aniofabmed, codtipomedidor, codcapacidadmedidor, codclasemetrologica, ";
        $sql .= "tipofacturacion, ubicacionllavemedidor, lecturaanterior, fechalecturaanterior, ";
        $sql .= "lecturaultima, fechalecturaultima, consumo, lecturapromedio, ";
        $sql .= "codcalibrador, fechacontlab, horacontlab,infocaja, codtiporetiromed, observacion, ";
        $sql .= "fechareg, horareg, codtipomovimiento, estareg, tpcontrastacion, codusu, ";
        $sql .= "fechacontcam, medidorresultest, horacontcam,codresulcont, codempcont ) ";
        $sql .= " VALUES ( :coddetmedidor, :codemp, :codsuc, :codcliente, :nroinscripcion,:nromed, ";
        $sql .= ":marcamed, :coddiametro, :estadomedidores, :posicionmed, :modelomed, :tipolectura, ";
        $sql .= ":aniofabmed, :codtipomedidor, :codcapacidadmedidor, :codclasemetrologica, ";
        $sql .= ":tipofacturacion, :ubicacionllavemedidor, :lecturaanterior, :fechalecturaanterior, ";
        $sql .= ":lecturaultima, :fechalecturaultima, :consumo, :lecturapromedio, :codcalibrador, ";
        $sql .= ":fechacontlab, :horacontlab, :infocaja, :codtiporetiromed, :observacion, :fechareg, :horareg, ";
        $sql .= ":codtipomovimiento, :estareg, :tpcontrastacion, :codusu, :fechacontcam, ";
        $sql .=":medidorresultest, :horacontcam , :codresulcont, :codempcont )";
        
        $resultado = $conexion->prepare($sql);
        $resultado->execute(array(":coddetmedidor"=>$coddetmedidor, 
            ":codemp"=>$codemp, ":codsuc"=>$codsuc, 
            ":codcliente"=>$codcliente, ":nroinscripcion"=>$nroinscripcion, 
            ":nromed"=>$nromed, ":marcamed"=>$marcamed, 
            ":coddiametro"=>$coddiametro, ":estadomedidores"=>$estadomedidores, 
            ":posicionmed"=>$posicionmed, ":modelomed"=>$modelomed,
            ":tipolectura"=>$tipolectura, ":aniofabmed"=>$aniofabmed, 
            ":codtipomedidor"=>$codtipomedidor, ":codcapacidadmedidor"=>$codcapacidadmedidor, 
            ":codclasemetrologica"=>$codclasemetrologica, ":tipofacturacion"=>$tipofacturacion, 
            ":ubicacionllavemedidor"=>$ubicacionllavemedidor, ":lecturaanterior"=>$lecturaanterior, 
            ":fechalecturaanterior"=>$fechalecturaanterior, ":lecturaultima"=>$lecturaultima, 
            ":fechalecturaultima"=>$fechalecturaultima, ":consumo"=>$consumo,
            ":lecturapromedio"=>$lecturapromedio,  ":codcalibrador"=>$codcalibrador, 
            ":fechacontlab"=>$fechacontlab, ":horacontlab"=>$horacontlab,
            ":infocaja"=>$infocaja, ":codtiporetiromed"=>$codtiporetiromed,
            ":observacion"=>$observacion, ":fechareg"=>$fechareg, 
            ":horareg"=>$horareg, ":codtipomovimiento"=>$codtipomovimiento, 
            ":estareg"=>$estareg , ":tpcontrastacion"=>$tpcontrastacion,
            ":codusu"=>$idusuario, ":fechacontcam"=>$fechacontcam ,
            ":medidorresultest"=>$tpmedidor, ":horacontcam"=>$horacontcam,
            ":codresulcont"=>$codresulcont, ":codempcont"=>$codempcont ));
        
        if ($resultado->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detmedidor";
            die(2);
        }

        // INSERTAR DETALLE RESULTADO DE CONTRASTACION
            

            if($coddetmedidor!='')
            {
                $SqlP = "DELETE FROM micromedicion.detcontrastacion WHERE nroinscripcion = ? AND codemp= ? 
                AND codsuc= ? AND coddetmedidor= ? ";                  
                $result = $conexion->prepare($SqlP); 
                $result->execute(array($nroinscripcion, $codemp, $codsuc, $coddetmedidor));
                
            }

            
            $i=0;
            $estdet=1;
            
            if($_POST['codtipoensayocaudal']!='')
            {

                foreach ($_POST['codtipoensayocaudal'] as $key=> $val) {

                    /*$id  = "SELECT max(codintdiario) FROM facturacion.interesdiario WHERE codsuc = ".$codsuc;
                    $consulta = $conexion->query($id);
                    $row = $consulta->fetch();
                    $codintdiario   = $row[0];*/

                    $i ++;
                    $caudalensayo = $_POST["caudalensayo"][$key];
                    $presion      = $_POST["presion"][$key];
                    $temperatura  =$_POST["temperatura"][$key];
                    $volumenpatron= $_POST["volumenpatron"][$key];
                    
                    $lecturainicio=$_POST["lecturainicio"][$key];
                    $lecturafin   = $_POST["lecturafin"][$key];
                    $lecturainicio=$_POST["lecturainicio"][$key];
                    $lecturafin   = $_POST["lecturafin"][$key];
                    
                    $diferencialectura = $_POST["diferencialectura"][$key];
                    $errorrelativa     = $_POST["errorrelativa"][$key];
                    $errorpermisible   = $_POST["errorpermisible"][$key];
                    
                    $insert= "INSERT INTO micromedicion.detcontrastacion(codemp, codsuc, nroinscripcion, nromed, 
                        coddetmedidor, codtipoensayocaudal, item, tiporetiro, caudalensayo, presion, temperatura, 
                        volumenpatron, lecturainicio, lecturafin, diferencialectura, errorrelativa, errorpermisible,
                        estareg, codusu, fechareg, codbancomedidor, medidorresultest )";
                    $insert.= "VALUES (:codemp, :codsuc, :nroinscripcion, :nromed, :coddetmedidor, :codtipoensayocaudal, :item,  
                        :tiporetiro, :caudalensayo, :presion, :temperatura, :volumenpatron, :lecturainicio, :lecturafin, 
                        :diferencialectura, :errorrelativa, :errorpermisible, :estareg, :codusu, :fechareg, 
                        :codbancomedidor, :medidorresultest)";
                    $conn = $conexion->prepare($insert);
                    $conn->execute(array(":codemp"=>$codemp, ":codsuc"=>$codsuc,
                        ":nroinscripcion"=>$nroinscripcion, ":nromed"=>$nromed,
                        ":coddetmedidor"=>$coddetmedidor, ":codtipoensayocaudal"=>$val,
                        ":item"=>$i, ":tiporetiro"=>$tpcontrastacion,
                         ":caudalensayo"=>$caudalensayo, ":presion"=>$presion,
                        ":temperatura"=>$temperatura, ":volumenpatron"=>$volumenpatron,
                        ":lecturainicio"=>$lecturainicio,  ":lecturafin"=>$lecturafin, 
                        ":diferencialectura"=>$diferencialectura,
                        ":errorrelativa"=>$errorrelativa, ":errorpermisible"=>$errorpermisible,
                        ":estareg"=>$estdet, ":codusu"=>$idusuario, ":fechareg"=>$fechareg,
                        ":codbancomedidor"=>$codbancomedidor,":medidorresultest"=>$tpmedidor ));
                }
            }            
        
        if ($conn->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detcontrastacion";
            die(2);
        }
        
        // ACTUALIZAR CONEXIONES
        $upd ="UPDATE catastro.conexiones SET ";
        $upd .="codubicacion= :codubicacion, codmarca= :codmarca, coddiametrosmedidor= :coddiametrosmedidor, codestadomedidor= :codestadomedidor, ";
        $upd .="posicionmed= :posicionmed, tipolectura= :tipolectura, codmodelo= :codmodelo, aniofabmed= :aniofabmed, ";
        $upd .="codtipomedidor= :codtipomedidor, codcapacidadmedidor= :codcapacidadmedidor, nromed= :nromed, tipofacturacion= :tipofacturacion, ";
        $upd .="lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, lecturaultima= :lecturaultima, ";
        $upd .="fechalecturaultima= :fechalecturaultima, consumo= :consumo, lecturapromedio= :lecturapromedio, codclasemetrologica= :codclasemetrologica";
        $upd .=" WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
    
        $result = $conexion->prepare($upd);
        $result->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":codubicacion"=>$ubicacionllavemedidor, ":codmarca"=>$marcamed, ":coddiametrosmedidor"=>$coddiametro, 
            ":codestadomedidor"=>$estadomedidores, ":posicionmed"=>$posicionmed, ":tipolectura"=>$tipolectura,             
            ":codmodelo"=>$modelomed, ":aniofabmed"=>$aniofabmed, ":codtipomedidor"=>$codtipomedidor, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, ":nromed"=>$nromed, ":tipofacturacion"=>$tipofacturacion, 
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,  
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, ":codclasemetrologica"=>$codclasemetrologica ));
        
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar conexiones";
            die();
        }
       
        // ACTUALIZAR CLIENTES
        $updC ="UPDATE catastro.clientes SET 
            tipofacturacion= :tipofacturacion, nromed= :nromed, lecturaultima= :lecturaultima, 
            fechalecturaultima= :fechalecturaultima, lecturaanterior= :lecturaanterior, 
            fechalecturaanterior= :fechalecturaanterior, 
            consumo= :consumo, lecturapromedio= :lecturapromedio
        WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion ";
        
        $res = $conexion->prepare($updC);
        $res->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":tipofacturacion"=>$tipofacturacion, ":nromed"=>$nromed,
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio ));
        
        if ($res->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar clientes";
            die();
        }else{
			$conexion->commit();		
			echo $resu=1;
		}
    }
    
    //*------------------*
    // ACTUALIZAR TODO
    //*------------------*
    
    if ($Op == 1) {
        $sql = "UPDATE micromedicion.detmedidor SET 
            codcliente= :codcliente, nromed= :nromed, marcamed= :marcamed, coddiametro= :coddiametro, 
            estadomedidores= :estadomedidores, posicionmed= :posicionmed, 
            modelomed= :modelomed, tipolectura= :tipolectura, aniofabmed= :aniofabmed, 
            codtipomedidor= :codtipomedidor,codbancomedidor=:codbancomedidor,
            codcapacidadmedidor= :codcapacidadmedidor, codclasemetrologica= :codclasemetrologica, 
            tipofacturacion= :tipofacturacion, ubicacionllavemedidor= :ubicacionllavemedidor,  
            lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, 
            lecturaultima= :lecturaultima, fechalecturaultima= :fechalecturaultima, 
            consumo= :consumo, lecturapromedio= :lecturapromedio, 
            codcalibrador= :codcalibrador, fechacontlab= :fechacontlab, infocaja= :infocaja, 
            codtiporetiromed= :codtiporetiromed, observacion= :observacion, tpcontrastacion= :tpcontrastacion,
            fechacontcam= :fechacontcam, medidorresultest= :medidorresultest, horacontcam= :horacontcam,
            codresulcont= :codresulcont, codempcont= :codempcont ";
        $sql .= " WHERE codemp=:codemp AND codsuc=:codsuc AND 
            coddetmedidor=:coddetmedidor AND nroinscripcion= :nroinscripcion ";
        $resultado = $conexion->prepare($sql);
        $resultado->execute(array(":coddetmedidor"=> $coddetmedidor, 
            ":codemp"=>$codemp, ":codsuc"=>$codsuc, 
            ":codcliente"=>$codcliente, ":nroinscripcion"=>$nroinscripcion, 
            ":nromed"=>$nromed, ":marcamed"=>$marcamed, 
            ":coddiametro"=>$coddiametro, ":estadomedidores"=>$estadomedidores, 
            ":posicionmed"=>$posicionmed, ":modelomed"=>$modelomed,
            ":tipolectura"=>$tipolectura, ":aniofabmed"=>$aniofabmed, 
            ":codtipomedidor"=>$codtipomedidor, ":codbancomedidor"=>$codbancomedidor,
            ":codcapacidadmedidor"=>$codcapacidadmedidor, 
            ":codclasemetrologica"=>$codclasemetrologica, ":tipofacturacion"=>$tipofacturacion, 
            ":ubicacionllavemedidor"=>$ubicacionllavemedidor, ":lecturaanterior"=>$lecturaanterior, 
            ":fechalecturaanterior"=>$fechalecturaanterior, ":lecturaultima"=>$lecturaultima, 
            ":fechalecturaultima"=>$fechalecturaultima, ":consumo"=>$consumo,
            ":lecturapromedio"=>$lecturapromedio, ":codcalibrador"=>$codcalibrador, 
            ":fechacontlab"=>$fechacontlab, ":infocaja"=>$infocaja, 
            ":codtiporetiromed"=>$codtiporetiromed,
            ":observacion"=>$observacion, ":tpcontrastacion"=>$tpcontrastacion,
            ":fechacontcam"=>$fechacontcam ,
            ":medidorresultest"=>$tpmedidor, ":horacontcam"=>$horacontcam,
            ":codresulcont"=>$codresulcont, ":codempcont"=>$codempcont ));
        
        if ($resultado->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detmedidor";
            die();
        }


        // INSERTAR DETALLE RESULTADO DE CONTRASTACION
            if($coddetmedidorant!='')
            {
                $SqlP = "DELETE FROM micromedicion.detcontrastacion WHERE coddetmedidor= ".$coddetmedidorant."
                    AND codsuc= ".$codsuc." AND nroinscripcion= ".$nroinscripcion." ";
                $result=$conexion->query($SqlP);      
            }
            
            $i=0;
            $estdet=1;
            
            if($_POST['codtipoensayocaudal']!='')
            {
                //echo $codbancomedidor;
                foreach ($_POST['codtipoensayocaudal'] as $key=> $val) {
                    $i ++;
                    $caudalensayo = $_POST["caudalensayo"][$key];
                    $presion      = $_POST["presion"][$key];
                    $temperatura  =$_POST["temperatura"][$key];
                    $volumenpatron= $_POST["volumenpatron"][$key];                    
                    $lecturainicio=$_POST["lecturainicio"][$key];
                    $lecturafin   = $_POST["lecturafin"][$key];
                    $lecturainicio=$_POST["lecturainicio"][$key];
                    $lecturafin   = $_POST["lecturafin"][$key];                    
                    $diferencialectura = $_POST["diferencialectura"][$key];
                    $errorrelativa     = $_POST["errorrelativa"][$key];
                    $errorpermisible   = $_POST["errorpermisible"][$key];
                    
                    $insert= "INSERT INTO micromedicion.detcontrastacion(codemp, codsuc, nroinscripcion, nromed, 
                        coddetmedidor, codtipoensayocaudal, item, tiporetiro, caudalensayo, presion, temperatura, 
                        volumenpatron, lecturainicio, lecturafin, diferencialectura, errorrelativa, errorpermisible,
                        estareg, codusu, fechareg, codbancomedidor, medidorresultest )";
                    $insert.= "VALUES (:codemp, :codsuc, :nroinscripcion, :nromed, :coddetmedidor, :codtipoensayocaudal, :item,  
                        :tiporetiro, :caudalensayo, :presion, :temperatura, :volumenpatron, :lecturainicio, :lecturafin, 
                        :diferencialectura, :errorrelativa, :errorpermisible, :estareg, :codusu, :fechareg, 
                        :codbancomedidor, :medidorresultest)";
                    $conn = $conexion->prepare($insert);
                    $conn->execute(array(":codemp"=>$codemp, ":codsuc"=>$codsuc,
                        ":nroinscripcion"=>$nroinscripcion, ":nromed"=>$nromed,
                        ":coddetmedidor"=>$coddetmedidor, ":codtipoensayocaudal"=>$val,
                        ":item"=>$i, ":tiporetiro"=>$tpcontrastacion,
                         ":caudalensayo"=>$caudalensayo, ":presion"=>$presion,
                        ":temperatura"=>$temperatura, ":volumenpatron"=>$volumenpatron,
                        ":lecturainicio"=>$lecturainicio,  ":lecturafin"=>$lecturafin, 
                        ":diferencialectura"=>$diferencialectura,
                        ":errorrelativa"=>$errorrelativa, ":errorpermisible"=>$errorpermisible,
                        ":estareg"=>$estdet, ":codusu"=>$idusuario, ":fechareg"=>$fechareg,
                        ":codbancomedidor"=>$codbancomedidor,":medidorresultest"=>$tpmedidor ));
                }
            }            
        
        if ($conn->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detcontrastacion";
            die(2);
        }
        
        // ACTUALIZAR CONEXIONES
        $upd ="UPDATE catastro.conexiones SET ";
        $upd .="codubicacion= :codubicacion, codmarca= :codmarca, coddiametrosmedidor= :coddiametrosmedidor, codestadomedidor= :codestadomedidor, ";
        $upd .="posicionmed= :posicionmed, tipolectura= :tipolectura, codmodelo= :codmodelo, aniofabmed= :aniofabmed, ";
        $upd .="codtipomedidor= :codtipomedidor, codcapacidadmedidor= :codcapacidadmedidor, nromed= :nromed, tipofacturacion= :tipofacturacion, ";
        $upd .="lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, lecturaultima= :lecturaultima, ";
        $upd .="fechalecturaultima= :fechalecturaultima, consumo= :consumo, lecturapromedio= :lecturapromedio, codclasemetrologica= :codclasemetrologica";
        $upd .=" WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
    
        $result = $conexion->prepare($upd);
        $result->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":codubicacion"=>$ubicacionllavemedidor, ":codmarca"=>$marcamed, ":coddiametrosmedidor"=>$coddiametro, 
            ":codestadomedidor"=>$estadomedidores, ":posicionmed"=>$posicionmed, ":tipolectura"=>$tipolectura,             
            ":codmodelo"=>$modelomed, ":aniofabmed"=>$aniofabmed, ":codtipomedidor"=>$codtipomedidor, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, ":nromed"=>$nromed, ":tipofacturacion"=>$tipofacturacion, 
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,  
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, ":codclasemetrologica"=>$codclasemetrologica ));
        
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar conexiones";
            die();
        }


        // ACTUALIZAR CLIENTES
        $updC ="UPDATE catastro.clientes SET 
            tipofacturacion= :tipofacturacion, nromed= :nromed, lecturaultima= :lecturaultima, 
            fechalecturaultima= :fechalecturaultima, lecturaanterior= :lecturaanterior, 
            fechalecturaanterior= :fechalecturaanterior, 
            consumo= :consumo, lecturapromedio= :lecturapromedio
        WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion ";
        
        $res = $conexion->prepare($updC);
        $res->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":tipofacturacion"=>$tipofacturacion, ":nromed"=>$nromed,
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio ));
        
        if ($res->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar clientes";
            die();
        }else{
			$conexion->commit();		
			echo $resu=1;
		}
    }

        
?>
