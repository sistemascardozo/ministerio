<?php
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
  
  $TituloVentana = "RETIRO DE MEDIDOR";
  $Activo=1;
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  if($Op!=5) CuerpoSuperior($TituloVentana);  
  $codsuc = $_SESSION['IdSucursal'];

  $Criterio= 'retiro';

  $and="";
  if($_GET['Op']==5){$and=" and d.codtipomovimiento=3 and d.estareg=3 ";}
  $FormatoGrilla = array ();//
  $Sql = "SELECT
    d.coddetmedidor,
    cl.codantiguo,
    cl.propietario,
    d.nromed,
    case d.codtipomovimiento
        when 1 then '<p style=\"color:green;font-weight: bold;\">INSTALACION DE MEDIDOR</p>'
        when 2 then '<p style=\"color:green;font-weight: bold;\">ATENCION DE INSTALACION</p>'
        when 3 then '<p style=\"color:green;font-weight: bold;\">RETIRO DE MEDIDOR</p>'
    else '' end, 
    ".$objFunciones->FormatFecha('d.fechamov').",
    case d.estareg 
        when 3 then '<p style=\"color:green;font-weight: bold;\">RETIRADO</p>'        
    else '' end, 1
    
    FROM
    micromedicion.detmedidor AS d
    INNER JOIN catastro.clientes AS cl ON (cl.codcliente = d.codcliente AND cl.nroinscripcion = d.nroinscripcion)
    INNER JOIN public.marcamedidor AS ma ON ma.codmarca = d.marcamed
    INNER JOIN public.modelomedidor AS mo ON mo.codmodelo = d.modelomed ";
      //  to_char(fechacreacion,'dd-MM-yyyy') 
  $FormatoGrilla[0] =eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                           //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'cl.propietario','2'=>'cl.codantiguo', '3'=>'d.nromed');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo','T2'=>'Nro. Insc.','T3'=>'Propietario','T4'=>'Nro. Medidor','T5'=>'Movimiento',
  		'T6'=>'Fecha Reg.','T7'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'left', 'A5'=>'center', 'A6'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W2'=>'100', 'W3'=>'225', 'W4'=>'80','W5'=>'120','W6'=>'60','W7'=>'70','W8'=>'50');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 930;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " AND d.codtipomovimiento=3 AND (d.codemp=1 and d.codsuc=".$codsuc.$and.") ORDER BY d.coddetmedidor DESC ";                                   //Orden de la Consulta
  if($Op!=5)
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
        'NB'=>'2',          //Número de Botones a agregar
        'BtnId1'=>'BtnModificar',   //Nombre del Boton
        'BtnI1'=>'modificar.png',   //Imagen a mostrar
        'Btn1'=>'Editar',       //Titulo del Botón
        'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
        'BtnCI1'=>'8',  //Item a Comparar
        'BtnCV1'=>'1',    //Valor de comparación
        //'BtnId2'=>'BtnEliminar', 
        //'BtnI2'=>'eliminar.png', 
        //'Btn2'=>'Eliminar', 
        //'BtnF2'=>'onclick="Eliminar(this.id)"', 
        //'BtnCI2'=>'8', //campo 3
        //'BtnCV2'=>'1',//igua a 1*/
        //'BtnId2'=>'BtnRestablecer', //y aparece este boton
        //'BtnI2'=>'restablecer.png', 
        'BtnId2'=>'BtnRestablecer', //y aparece este boton
        'BtnI2'=>'imprimir.png', 
        'Btn2'=>'Imprimir', 
        'BtnF2'=>'onclick="Imprimir(this.id)"', 
        'BtnCI2'=>'8', 
        'BtnCV2'=>'1',
        'BtnId3'=>'BtnGenerar', //y aparece este boton
        'BtnI3'=>'documento.png', 
        'Btn3'=>'Atender Instalacion', 
        'BtnF3'=>'onclick="AtenderInst(this)"', 
        'BtnCI3'=>'8', 
        'BtnCV3'=>'1'
    );  
else
    {
        $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
                'NB'=>'1',          //Número de Botones a agregar
                'BtnId1'=>'BtnModificar',   //Nombre del Boton
                'BtnI1'=>'ok.png',   //Imagen a mostrar
                'Btn1'=>'Seleccionar',       //Titulo del Botón
                'BtnF1'=>'onclick="Enviar(this.id);"',  //Eventos del Botón
                'BtnCI1'=>'8',  //Item a Comparar
                'BtnCV1'=>'1'    //Valor de comparación
                );
        $FormatoGrilla[5] = array('W1'=>'40','W3'=>'225','W4'=>'70','W5'=>'120','W6'=>'60','W7'=>'70','W8'=>'50');
    }
    
    $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 830, 600);
    Pie();
	
	if($_GET['Op']!=5)
	{
    	CuerpoInferior();
	}

?>
<script type="text/javascript">
    var urldir = '<?php echo $urldir;?>';
function Enviar(Id)
  {
    //var Id = $(obj).parent().parent().data('id')
    opener.Recibir(Id);
    window.close();
  }
function Imprimir(Id)
  {
    var url='';
    url='imprimir.php';
    AbrirPopupImpresion(url+'?Id='+Id + '&codsuc=<?=$codsuc?>',800,500)
  }
  
  function AtenderInst(obj)
  {   
    $("#Modificar").dialog({title:'Confirmar Atensión'});
    $("#Modificar").dialog("open");
    $("#DivModificar").html("<center><span>Esta seguro que desea atender la Instalación?</span></center>");
    $("#button-maceptar").unbind( "click" );
    $("#button-maceptar").click(function(){GuardarOk();});
    $("#button-maceptar").children('span.ui-button-text').text('Aceptar')
    $.ajax({
       url:'generar/index.php',
       type:'POST',
       async:true,
       data:'Id=' + Id+ '&codsuc=<?=$codsuc?>',
       success:function(data){
        $("#DivModificar").html(data);
       }
    })
  }
  
  function GuardarOk()
  {
    $.ajax({
      url:'generar/Guardar.php?Op=0' ,
      type:'POST',
      async:true,
      data:$('#form1').serialize(),
      success:function(data){
        //alert(data);
        OperMensaje(data) 
        $("#Mensajes").html(data);
        $("#DivModificar").html('');
        $("#Modificar").dialog("close");
        Buscar(0);
      }
    })
  }
</script>

