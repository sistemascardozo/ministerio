<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();
    
    $Op = $_GET["Op"];
    $objFunciones = new clsFunciones();

    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $coddetmedidorant = $_POST["coddetmedidor"];
    $coddetmedidorAct=$_POST["coddetmedidor"];
    $codcliente    = $_POST["codcliente"];
    $nroinscripcion= $_POST["nroinscripcion"];
    
    $codempcont = $_POST["codempcont"];

    $nromed     = $_POST["nromedidor"];
    $marcamed   = $_POST["marcamedidor"];
    $coddiametro       = $_POST["diametrosmedidor"];
    $estadomedidores   = $_POST["estadomedidor"];
    $posicionmed   = $_POST["posicionmedidor"];
    $modelomed     = $_POST["modelomedidor"];
    $tipolectura = $_POST["tipolectura"];
    $aniofabmed  = $_POST["aniofab"];   
    $codtipomedidor  = $_POST["tipomedidor"];
    $fechamov        = $objFunciones->CodFecha($_POST["fecharetiro"]);    
    $codcapacidadmedidor = $_POST["capacidadmedidor"];
    $codclasemetrologica = $_POST["codclasemetrologica"];
    $tipofacturacion     = $_POST["tipofacturacion"];
    $ubicacionllavemedidor = $_POST["ubicacionllavemedidor"];
    $lecturaanterior       = $_POST["lecturaanterior"];
    if($lecturaanterior=='' || $lecturaanterior=='0'){$lecturaanterior='0.00';}
    
    $fechalecturaanterior  = $objFunciones->CodFecha($_POST["fechalecturaanterior"]);
    $lecturaultima = $_POST["lecturaultima"];
    if($lecturaultima=='' || $lecturaultima=='0'){$lecturaultima='0.00';}
    
    $fechalecturaultima  = $objFunciones->CodFecha($_POST["fechalecturaultima"]);
    $consumo             = $_POST["consumo"];
    if($consumo=='NAN'){$consumo='0.00';}
    
    $lecturapromedio = $_POST["lecturapromedio"];
    $codinspector    = $_POST["codinspector"];
    $trasladomed     = $_POST["trasladomed"];
    
    $punteromed       = $_POST["punteromed"];
    $precintomed = $_POST["medprecseg"];    
    $visorlect = $_POST["visorlec"];
    
    $observacion    = $_POST["observacion"];
    $fechareg = date('Y-m-d');
    $horareg  = date('H:i:s');
    $codtipomovimiento = 3;
    // 1 Instlacion
    // 2 Atension
    // 3 Retiro
    
    $idusuario  = $_SESSION['id_user'];
    $estareg    = 3; //EM PROCESO;    
    
    $codtiporetiromed= $_POST["codtiporetiromed"];
    $tpcontrastacion= $_POST["tpcontrastacion"];
    $horamov= $_POST["horare"];
    //if ($Op != 2) {
    if ($Op == 0) {
        
        $id= $objFunciones->setCorrelativos("instalacion_med",$codsuc,"0");
        $coddetmedidor = $id[0];
        //die($coddetmedidor);
        $sql = "INSERT INTO micromedicion.detmedidor(coddetmedidor, codemp, codsuc, codcliente, nroinscripcion, ";
        $sql .= "nromed, marcamed, coddiametro, estadomedidores, posicionmed, modelomed, tipolectura, aniofabmed, ";
        $sql .= "codtipomedidor, fechamov, codcapacidadmedidor, codclasemetrologica, tipofacturacion, ";
        $sql .= "ubicacionllavemedidor, lecturaanterior, fechalecturaanterior, lecturaultima, ";
        $sql .= "fechalecturaultima, consumo, lecturapromedio, codinspector, punteromed, ";
        $sql .= "precintomed, visorlect, observacion, fechareg, horareg, codtipomovimiento, estareg, ";
        $sql .= "trasladomed, codusu, codtiporetiromed, tpcontrastacion, horamov, codempcont) ";
        $sql .= "VALUES ( :coddetmedidor, :codemp, :codsuc, :codcliente, :nroinscripcion,:nromed, :marcamed, :coddiametro, ";
        $sql .= ":estadomedidores, :posicionmed, :modelomed, :tipolectura, :aniofabmed, :codtipomedidor, :fechamov, ";
        $sql .= ":codcapacidadmedidor, :codclasemetrologica, :tipofacturacion, :ubicacionllavemedidor, :lecturaanterior, ";        
        $sql .= ":fechalecturaanterior, :lecturaultima, :fechalecturaultima, :consumo, :lecturapromedio, :codinspector, ";
        $sql .= ":punteromed, :precintomed, :visorlect, :observacion, :fechareg, :horareg, :codtipomovimiento, ";
        $sql .= ":estareg, :trasladomed, :codusu, :codtiporetiromed, :tpcontrastacion, :horamov, :codempcont)";
        
        $resultado = $conexion->prepare($sql);
        $resultado->execute(array(":coddetmedidor"=> $coddetmedidor, 
            ":codemp"=>$codemp, ":codsuc"=>$codsuc, 
            ":codcliente"=>$codcliente, ":nroinscripcion"=>$nroinscripcion, 
            ":nromed"=>$nromed, ":marcamed"=>$marcamed, 
            ":coddiametro"=>$coddiametro, ":estadomedidores"=>$estadomedidores, 
            ":posicionmed"=>$posicionmed, ":modelomed"=>$modelomed,
            ":tipolectura"=>$tipolectura, ":aniofabmed"=>$aniofabmed, 
            ":codtipomedidor"=>$codtipomedidor, ":fechamov"=>$fechamov, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, 
            ":codclasemetrologica"=>$codclasemetrologica,
            ":tipofacturacion"=>$tipofacturacion, 
            ":ubicacionllavemedidor"=>$ubicacionllavemedidor, 
            ":lecturaanterior"=>$lecturaanterior, 
            ":fechalecturaanterior"=>$fechalecturaanterior, 
            ":lecturaultima"=>$lecturaultima, 
            ":fechalecturaultima"=>$fechalecturaultima, 
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, 
            ":codinspector"=>$codinspector, ":punteromed"=>$punteromed,
            ":precintomed"=>$precintomed, ":visorlect"=>$visorlect,
            ":observacion"=>$observacion, ":fechareg"=>$fechareg, 
            ":horareg"=>$horareg, ":codtipomovimiento"=>$codtipomovimiento, 
            ":estareg"=>$estareg , ":trasladomed"=>$trasladomed,
            ":codusu"=>$idusuario , ":codtiporetiromed"=>$codtiporetiromed,
            ":tpcontrastacion"=>$tpcontrastacion, ":horamov"=>$horamov, ":codempcont"=>$codempcont ));
        
        if ($resultado->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detmedidor";
            die();
        }

   
        /* ACTUALIZAR EL ESTADO DEL ANTERIOR REGISTRO (2->ATENDIDO)         
        $updAnt="UPDATE micromedicion.detmedidor
            SET estareg= 3
            WHERE codemp=:codemp AND codsuc=:codsuc AND coddetmedidor= :coddetmedidor";
        $resultad = $conexion->prepare($updAnt);
        $resultad->execute(array(":coddetmedidor"=> $coddetmedidorant, ":codemp"=>$codemp, ":codsuc"=>$codsuc ));

        if ($resultad->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error UPDATE detmedidor (ATENDIDO)";
                die(2);
        */

        /* ACTUALIZAR LA TABLA CONEXIONES */     
        $marcamed=0; $coddiametro=0;
        $ubicacionllavemedidor=0; 
        $estadomedidores=0;
        $posicionmed=1; $tipolectura=1;
        $fechamov=null; $modelomed=0;
        $codtipomedidor=0; $codcapacidadmedidor=1;
        $nromed='';

        $upd ="UPDATE catastro.conexiones SET ";
        $upd .="codubicacion= :codubicacion, codmarca= :codmarca, coddiametrosmedidor= :coddiametrosmedidor, codestadomedidor= :codestadomedidor, ";
        $upd .="posicionmed= :posicionmed, tipolectura= :tipolectura, fechainsmed= :fechainsmed, codmodelo= :codmodelo, aniofabmed= :aniofabmed, ";
        $upd .="codtipomedidor= :codtipomedidor, codcapacidadmedidor= :codcapacidadmedidor, nromed= :nromed, tipofacturacion= :tipofacturacion, ";
        $upd .="lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, lecturaultima= :lecturaultima, ";
        $upd .="fechalecturaultima= :fechalecturaultima, consumo= :consumo, lecturapromedio= :lecturapromedio, codclasemetrologica= :codclasemetrologica";
        $upd .=" WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
    
        $result = $conexion->prepare($upd);
        $result->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":codubicacion"=>$ubicacionllavemedidor, ":codmarca"=>$marcamed, ":coddiametrosmedidor"=>$coddiametro, 
            ":codestadomedidor"=>$estadomedidores, ":posicionmed"=>$posicionmed, ":tipolectura"=>$tipolectura,             
            ":fechainsmed"=>$fechamov, ":codmodelo"=>$modelomed, ":aniofabmed"=>$aniofabmed, ":codtipomedidor"=>$codtipomedidor, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, ":nromed"=>$nromed, ":tipofacturacion"=>$tipofacturacion, 
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,  
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, ":codclasemetrologica"=>$codclasemetrologica ));
        
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error ACTUALIZAR conexiones";
            die();
        }
        
        /* ACTUALIZAR LA TABLA CLIENTES */
        $updC ="UPDATE catastro.clientes SET 
            tipofacturacion= :tipofacturacion, nromed= :nromed, lecturaultima= :lecturaultima, 
            fechalecturaultima= :fechalecturaultima, lecturaanterior= :lecturaanterior, 
            fechalecturaanterior= :fechalecturaanterior, 
            consumo= :consumo, lecturapromedio= :lecturapromedio
        WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion ";
        
        $res = $conexion->prepare($updC);
        $res->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":tipofacturacion"=>$tipofacturacion, ":nromed"=>$nromed,
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio ));
        
        if ($res->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error ACTUALIZAR clientes";
            die();
        }else{
    		$conexion->commit();		
    		echo $resu=1;
	    }
    }

    //**-------------------------**
    // ACTUALIZACION DE TODO
    //**-------------------------**

    if ($Op == 1) {
        $sql = "UPDATE micromedicion.detmedidor SET 
            codcliente= :codcliente, nroinscripcion= :nroinscripcion, 
            nromed= :nromed, marcamed= :marcamed, coddiametro= :coddiametro, 
            estadomedidores= :estadomedidores, posicionmed= :posicionmed, 
            modelomed= :modelomed, tipolectura= :tipolectura, aniofabmed= :aniofabmed, 
            codtipomedidor= :codtipomedidor, fechamov= :fechamov, 
            codcapacidadmedidor= :codcapacidadmedidor, codclasemetrologica= :codclasemetrologica, 
            tipofacturacion= :tipofacturacion, ubicacionllavemedidor= :ubicacionllavemedidor,  
            lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, 
            lecturaultima= :lecturaultima, fechalecturaultima= :fechalecturaultima, 
            consumo= :consumo, lecturapromedio= :lecturapromedio, 
            codinspector= :codinspector, punteromed= :punteromed, 
            precintomed= :precintomed, 
            visorlect= :visorlect, observacion= :observacion, trasladomed= :trasladomed,
            codtiporetiromed= :codtiporetiromed, tpcontrastacion= :tpcontrastacion, 
            horamov= :horamov, codempcont= :codempcont";
        $sql .= " WHERE codemp=:codemp and codsuc=:codsuc and coddetmedidor=:coddetmedidor";
        $resultado = $conexion->prepare($sql);
            
        $resultado->execute(array(":coddetmedidor"=> $coddetmedidorAct, 
            ":codemp"=>$codemp, ":codsuc"=>$codsuc, 
            ":codcliente"=>$codcliente, ":nroinscripcion"=>$nroinscripcion, 
            ":nromed"=>$nromed, ":marcamed"=>$marcamed, 
            ":coddiametro"=>$coddiametro, ":estadomedidores"=>$estadomedidores, 
            ":posicionmed"=>$posicionmed, ":modelomed"=>$modelomed,
            ":tipolectura"=>$tipolectura, ":aniofabmed"=>$aniofabmed, 
            ":codtipomedidor"=>$codtipomedidor, ":fechamov"=>$fechamov, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, 
            ":codclasemetrologica"=>$codclasemetrologica,
            ":tipofacturacion"=>$tipofacturacion, 
            ":ubicacionllavemedidor"=>$ubicacionllavemedidor, 
            ":lecturaanterior"=>$lecturaanterior, 
            ":fechalecturaanterior"=>$fechalecturaanterior, 
            ":lecturaultima"=>$lecturaultima, 
            ":fechalecturaultima"=>$fechalecturaultima, 
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, 
            ":codinspector"=>$codinspector, ":punteromed"=>$punteromed,
            ":precintomed"=>$precintomed, ":visorlect"=>$visorlect,
            ":observacion"=>$observacion, ":trasladomed"=>$trasladomed,
            ":codtiporetiromed"=>$codtiporetiromed,
            ":tpcontrastacion"=>$tpcontrastacion, ":horamov"=>$horamov, ":codempcont"=>$codempcont));
        
        if ($resultado->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detmedidor";
            die(2);
        }

        
        /**/
        $marcamed=0; $coddiametro=0;
        $ubicacionllavemedidor=0; 
        $estadomedidores=0;
        $posicionmed=1; $tipolectura=1;
        $fechamov=null; $modelomed=0;
        $codtipomedidor=0; $codcapacidadmedidor=1;
        $nromed='';
        
        $upd ="UPDATE catastro.conexiones SET ";
        $upd .="codubicacion= :codubicacion, codmarca= :codmarca, coddiametrosmedidor= :coddiametrosmedidor, codestadomedidor= :codestadomedidor, ";
        $upd .="posicionmed= :posicionmed, tipolectura= :tipolectura, fechainsmed= :fechainsmed, codmodelo= :codmodelo, aniofabmed= :aniofabmed, ";
        $upd .="codtipomedidor= :codtipomedidor, codcapacidadmedidor= :codcapacidadmedidor, nromed= :nromed, tipofacturacion= :tipofacturacion, ";
        $upd .="lecturaanterior= :lecturaanterior, fechalecturaanterior= :fechalecturaanterior, lecturaultima= :lecturaultima, ";
        $upd .="fechalecturaultima= :fechalecturaultima, consumo= :consumo, lecturapromedio= :lecturapromedio, codclasemetrologica= :codclasemetrologica";
        $upd .=" WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
    
        $result = $conexion->prepare($upd);
        $result->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":codubicacion"=>$ubicacionllavemedidor, ":codmarca"=>$marcamed, ":coddiametrosmedidor"=>$coddiametro, 
            ":codestadomedidor"=>$estadomedidores, ":posicionmed"=>$posicionmed, ":tipolectura"=>$tipolectura,             
            ":fechainsmed"=>$fechamov, ":codmodelo"=>$modelomed, ":aniofabmed"=>$aniofabmed, ":codtipomedidor"=>$codtipomedidor, 
            ":codcapacidadmedidor"=>$codcapacidadmedidor, ":nromed"=>$nromed, ":tipofacturacion"=>$tipofacturacion, 
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,  
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio, ":codclasemetrologica"=>$codclasemetrologica ));
        
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar conexiones";
            die(2);
        }

        
        $updC ="UPDATE catastro.clientes SET 
            tipofacturacion= :tipofacturacion, nromed= :nromed, lecturaultima= :lecturaultima, 
            fechalecturaultima= :fechalecturaultima, lecturaanterior= :lecturaanterior, 
            fechalecturaanterior= :fechalecturaanterior, 
            consumo= :consumo, lecturapromedio= :lecturapromedio
        WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion ";
        
        $res = $conexion->prepare($updC);
        $res->execute(array( ":codemp"=>$codemp, ":codsuc"=>$codsuc, ":nroinscripcion"=>$nroinscripcion, 
            ":tipofacturacion"=>$tipofacturacion, ":nromed"=>$nromed,
            ":lecturaultima"=>$lecturaultima, ":fechalecturaultima"=>$fechalecturaultima,
            ":lecturaanterior"=>$lecturaanterior, ":fechalecturaanterior"=>$fechalecturaanterior,
            ":consumo"=>$consumo, ":lecturapromedio"=>$lecturapromedio ));
        
        if ($res->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Actualizar clientes";
            die(2);
        }else{
			$conexion->commit();		
			echo $resu=1;
			
		}
    }

        
?>
