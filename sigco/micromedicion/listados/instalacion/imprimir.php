<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses,$Desde,$Hasta;
			
			$periodo = $this->DecFechaLiteral();
			
			$this->SetFont('Arial','B',10);
			
			$h = 4;
			$this->Cell(278, $h+2,"INSTALACIONES DE MEDIDORES",0,1,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(278, $h+2,'Fecha de Registro desde: '.$Desde." hasta: ".$Hasta,0,1,'C');
			
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(23,15,63,45,15,16,12,17,37,37));
			$this->SetAligns(array(C,C,C,C,C,C,C,C,C,C));
			$this->Row(array("COD. CATASTRAL",
							 "NRO. INSC.",
							 "USUARIO",
							 "DIRECCION",
							 "NRO. MED.",
							 "EST. SERV.",
							 "T. SERV.",							
							 "FECHA INST.",
							 "EMP. CONT",
							 "INSPECTOR",
							 ));

		}
		function contenido($codsuc,$Desde,$Hasta)
		{
			global $conexion;
			
			$h = 4;
			$s = 2;			
			
			if($Desde!="")
			{
				$Desde = $this->CodFecha($Desde);
                $Hasta = $this->CodFecha($Hasta);
				$Condicion=" AND d.fechamov BETWEEN CAST ('".$Desde."' AS DATE) AND CAST ( '".$Hasta."' AS DATE ) ";
			}
			
			$sql = "SELECT 
				DISTINCT d.coddetmedidor, 
				d.nroinscripcion,
				tc.descripcioncorta||' '||cl.descripcion AS direccion,
				c.propietario, 
				c.nrocalle, 
				e.descripcion AS estadoservicio,
				c.codtiposervicio,
				d.nromed,
				inp.nombres,
				c.codemp|| '-' ||c.codsuc|| '-'||c.codsector ||'-'||c.codmanzanas ||'-'||c.lote AS codcatastro,
				d.fechamov,
				emp.razonsocial,
				c.codantiguo
				FROM micromedicion.detmedidor AS d 
				INNER JOIN catastro.clientes AS c ON (c.codemp=d.codemp AND c.codcliente = d.codcliente AND c.nroinscripcion = d.nroinscripcion)
				INNER JOIN public.calles AS cl ON (c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona) 
				INNER JOIN public.tiposcalle AS tc ON (cl.codtipocalle = tc.codtipocalle) 
				INNER JOIN public.estadoservicio AS e ON (c.codestadoservicio = e.codestadoservicio)
				LEFT JOIN reglasnegocio.inspectores AS inp ON inp.codinspector = d.codinspector 
				INNER JOIN catastro.conexiones AS cx ON (c.codemp = cx.codemp AND c.codsuc = cx.codsuc AND c.nroinscripcion = cx.nroinscripcion)
				INNER JOIN micromedicion.empresacontrastadora AS emp ON (emp.codempcont = d.codempcont AND emp.codsuc= d.codsuc)

				WHERE c.codemp=1 and c.codsuc=? AND d.estareg<>3 ".$Condicion." ORDER BY d.fechamov ";
						
			$count= 0;
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc));
			$items = $consulta->fetchAll();
	
			foreach($items as $row)
			{
				$count++;
				$codcatastro = $row["codcatastro"];
				
				if($row["codtiposervicio"]==1){$ts="Ag/Des";}
				if($row["codtiposervicio"]==2){$ts="Ag";}
				if($row["codtiposervicio"]==3){$ts="Des";}
				
				$this->SetWidths(array(23,15,63,45,15,16,12,17,37,37));
				$this->SetAligns(array(C,C,"L","L",C,C,C,C,"L","L"));
				$this->Row(array($codcatastro, 
				$row["codantiguo"],
				trim(utf8_decode(strtoupper($row["propietario"]))),
				utf8_decode(strtoupper($row[2]." N° ".$row["nrocalle"])),
				trim($row["nromed"]),
				strtoupper($row["estadoservicio"]),
				$ts,
				$this->DecFecha($row["fechamov"]),
				utf8_decode($row["razonsocial"]),
				utf8_decode($row["nombres"]) 
				));				
				
			}
			
			$this->Ln(3);
			$this->Cell(278, $h+2,"Instalaciones de Medidores Registrados ".$count,'TB',1,'C');
			$this->SetFont('Arial','B',8);
			$this->Ln(3);
						
		}
	}
	
	$codsuc         = $_GET["codsuc"];
	$Desde          = $_GET["Desde"];
	$Hasta          = $_GET["Hasta"];
	
	$objReporte = new clsPadron("L");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsuc,$Desde,$Hasta);
	$objReporte->Output();
	
?>