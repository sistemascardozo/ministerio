<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "RUTAS DE REPARTO";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	$sucursal = $objDrop->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_resumenfacturacion.js" language="JavaScript"></script>
<script>
jQuery(function($)
	{ 
		
		$( "#DivTipos" ).buttonset();
		$("#consumo_ini").val(0);
		$("#consumo_fin").val(0);

	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc = <?=$codsuc?>;

	function ValidarForm(Op){
		var rutas    = ""
		var sectores = ""
		var datas    = ''

		if ($('#orden').val() == 0 ) {

			if ($("#codzona").val() == 0)
	        {
	            Msj($("#codzona"), "Seleccione el Zona")
	            return false;
	        }
	        if ($("#codsector").val() == 0)
	        {
	            Msj($("#codsector"), "Seleccione el Sector")
	            return false;
	        }
	        if ($("#rutasdistribucion").val() == 0)
	        {
	            Msj($("#rutasdistribucion"), "Seleccione la ruta de distribucion")
	            return false;
	        }
	        datas += "&zona="+$("#codzona").val()+"&sector="+$("#sector").val()+"&rutasdistribucion="+$("#rutasdistribucion").val(); 
	    }else{
	    	datas += "&orden=1";
	    }

		if($("#anio").val()==0)
		{
            Msj($("#anio"), "Seleccione el Anio a consultar")
            return false;
		}
		if($("#mes").val()==0)
		{
			Msj($("#mes"), "Seleccione el Mes a consultar")
            return false;
		}

		var texto = $("#mes option:selected").html();

		if(document.getElementById("rabresumen").checked==true)
		{
			url="imprimir_resumen.php";
		}

		url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>"+datas;
		AbrirPopupImpresion(url,800,600);
		return false;
	}

	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function cargar_rutas_lecturas(obj, cond)
	{
		$.ajax({
			 url:'../../../../ajax/rutas_lecturas_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+obj+'&codsuc=<?=$codsuc?>&condicion='+cond,
			 success:function(datos){
				$("#div_rutaslecturas").html(datos)
				$("#chkrutas").attr("checked",true)
				$("#todosrutas").val(1)
			 }
		}) 
	}
	function validar_rangoconsumo(obj){
		if(obj.checked)
		{
			$("#div_rangoconsumo").show();
		}else{
			$("#div_rangoconsumo").hide().find('input').val('');
			
		}
	}
	function validar_sectores(obj,idx)
	{
		$("#rutaslecturas").attr("disabled",true)
		$("#chkrutas").attr("checked",true)
		$("#todosrutas").val(1)
		
		if(obj.checked)
		{
			$("#"+idx).val(1)
			$("#codsector").attr("disabled",true)
		}else{
			$("#"+idx).val(0)
			$("#codsector").attr("disabled",false)
		}
	}
	function validar_rutas(obj,idx)
	{
		if(obj.checked)
		{
			$("#"+idx).val(1)
			$("#rutaslecturas").attr("disabled",true)
		}else{
			$("#"+idx).val(0)
			$("#rutaslecturas").attr("disabled",false)
		}
	}
	function quitar_disabled(obj,input)
	{
		if(obj.checked)
		{
			$("#"+input).attr("disabled",true)
		}else{
			$("#"+input).attr("disabled",false)
		}
	}
</script>
<div align="center">
	<table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
		<tr>
            <td colspan="2">&nbsp;</td>
		</tr>
        <tr>
            <td colspan="2">
				<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
					<fieldset>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
	                        <td align="right">Zona</td>
	                        <td width="30" align="center">:</td>
                            <td>
                                <?php
                                echo $objDrop->drop_zonas($codsuc, $items["codzona"], "onchange='cargar_sectores(". $codsuc.", this.value, 0, 0)';");
								?>
                            </td>
                            <td colspan="6">&nbsp;</td>
                        </tr>
						<tr>
						  <td align="right">Sector</td>
						  <td align="center">:</td>
						  <td><div id="div_sector"><select name="codsector" id="codsector" style="width:220px" class="select">
						    <option value="0">--Seleccione el Sector--</option>
					      </select></div></td>
						  <td colspan="6">&nbsp;</td>
					  </tr>
						<tr>
						  <td align="right">Rut. Dist</td>
						  <td align="center">:</td>
						  <td><div id="div_rutdistri">
                                    <select name="rutasdistribucion" id="rutasdistribucion" style="width:220px" class="select">
                                        <option value="0">--Seleccione la Ruta Distribucion--</option>
                                    </select>
                                </div></td>
						  <td colspan="6"><input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this,'orden');quitar_disabled(this,'codzona');quitar_disabled(this,'sector');quitar_disabled(this,'rutasdistribucion');" />
					      Todos las Z,S,R </td>
					  </tr>
						<tr>
						    <td>A&ntilde;o</td>
						    <td align="center">:</td>
						    <td>
			                	<div id="div_anio">
			                    	<? $objDrop->drop_anio($codsuc,1,"onchange='cargar_mes(1,{$codsuc},this.value)'"); ?>
			                    </div>
			                </td>
						    <td colspan="4" align="right">Mes</td>
						    <td align="center">:</td>
							<td>
								<div id="div_meses">
							    	<? $objDrop->drop_mes($codsuc,0,0); ?>
							    </div>
							</td>
						</tr>
					 	<tr>
   							<td colspan="3" style="padding:4px;"  align="center">
							<div id="DivTipos" style="display:inline">
		                       	<input checked="checked" type="radio" name="rabresumen" id="rabresumen" value="radio" />
		                       	<label for="rabresumen">PDF</label>   
					 			<input type="button" onclick="return ValidarForm();" value="Generar" id="">							
							</div>	
							</td>
  						</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
						<tr>
							<td>&nbsp;</td>
							<td align="center">&nbsp;</td>
							<td colspan="2">&nbsp;</td>
							<td align="center">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
							    <input type="hidden" name="orden" id="orden" value="1" />
							</td>
						</tr>
					</table>
					</fieldset>
				</form>
			</td>
		</tr>
	</table>
</div>
<div id="mostrar_datos"></div>
<script>
	$("#codzona").attr("disabled",true)
	$("#codsector").attr("disabled",true)
	$("#rutasdistribucion").attr("disabled",true)
</script>
<?php CuerpoInferior(); ?>