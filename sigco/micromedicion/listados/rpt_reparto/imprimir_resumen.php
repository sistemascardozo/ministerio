<?php 

    if(!session_start()){session_start();}

    // ini_set("display_errors",1);
    // error_reporting(E_ALL);

	include("../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses,$fechaactua;
			global $Dim,$mes;
			
			$periodo = $this->DecFechaLiteral($fechaactua);

			// if(empty($periodo["mes"])): $periodo["mes"] = $this->obtener_nombre_mes($mes); endif;

            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RUTAS DE REPARTO";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,strtoupper($periodo["mes"])." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(5);
			$this->SetFillColor(18,157,176);
			$this->SetTextColor(255);
			$h=5;
            $h1= 10;
			$h2= 15;

			$this->SetFont('Arial','B',10);
            $this->Cell($Dim[1],$h,utf8_decode('Datos'),1,0,'C',true);
            $this->Cell($Dim[2],$h,utf8_decode('Total'),1,0,'C',true);
            $this->Cell($Dim[5],$h1,utf8_decode('Observaciones'),1,1,'C',true);


			$this->SetY($this->GetY()-$h);
			$this->Cell($Dim[3],$h,utf8_decode('N°'),1,0,'C',true);
			$this->Cell($Dim[4],$h,utf8_decode('Distrito'),1,0,'C',true);
			$this->Cell($Dim[4],$h,utf8_decode('Sector'),1,0,'C',true);
			$this->Cell($Dim[4],$h,utf8_decode('Ruta de Reparto'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Fact. Emi.'),1,0,'C',true);
            $this->Cell($Dim[8],$h,utf8_decode('Conexiones'),1,1,'C',true);

		}

        function Contenido($cont, $distrito, $sector, $ruta, $tcone, $emi, $cam = 0)
        {
			global $Dim;

			switch ($cam) :
				case 0:

					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','B',9);
					$this->Cell($Dim[3],$h,utf8_decode($cont),0,0,'C',false);
					$this->Cell($Dim[4],$h,utf8_decode($distrito),0,0,'C',false);
					$this->Cell($Dim[4],$h,utf8_decode($sector),0,0,'C',false);
					$this->Cell($Dim[4],$h,utf8_decode($ruta),0,0,'C',false);
					$this->Cell($Dim[6],$h,$emi = (!empty($emi)) ? $emi : 0,0,0,'C',false);				
					$this->Cell($Dim[8],$h,$tcone = (!empty($tcone)) ? $tcone : 0,0,0,'C',false);				
					$this->Cell($Dim[5],$h,'.................................................',0,1,'C',false);				

				break;
				case 1:

					$this->SetFillColor(18,157,176);
					$this->SetTextColor(255);
					$h = 5;
					$this->SetFont('Arial','B',9);
					$this->Cell($Dim[1],$h,utf8_decode($cont),0,0,'C',true);
					$this->Cell($Dim[6],$h,$distrito,0,0,'C',true);
					$this->Cell($Dim[8],$h,$sector,0,0,'C',true);				
					$this->Cell($Dim[5],$h,'',0,1,'L',true);	

				break;
				
			endswitch;
		}
    }

    $Dim = array('1'=>135,'2'=>90,'3'=>15,'4'=>40,'5'=>50,'6'=>45,'7'=>30,'8'=>45);

	// Actual
	$codemp   = 1;
	$ciclo 	  = 1;
	$qls      = " ";
	$anio     = $_GET['anio'];
	$anio     = $_GET['anio'];
	$mes      = $_GET['mes'];
	$mestexto = $_GET['mestexto'];
	$orden    = $_GET['orden'];
	$codsuc   = $_GET['codsuc'];
	$zona     = (!empty($_GET['zona'])) ? $_GET['zona'] : 0 ;
	$sector   = (!empty($_GET['sector'])) ? $_GET['sector'] : 0 ;
	$rutadistri = (!empty($_GET['rutasdistribucion'])) ? $_GET['rutasdistribucion'] : 0 ;

	if(intval($mes)<10): $mess="0".$mes;else:$mess=$mes;endif;

	$fechaactua = '01/'.$mess.'/'.$anio;


	if( ($zona != 0) AND ($sector != 0) AND ($rutadistri != 0) ):

		$qls = " AND sec.codzona = " . $zona . " AND cli.codsector = " . $sector . " AND cli.codrutdistribucion = " . $rutadistri;
	
	endif;

	// Para obtener el periodo de faturacion

	$sqlLect = "select nrofacturacion
		FROM facturacion.periodofacturacion
		WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
	$nrofact = $consultaL->fetch();

	// Sector
	$sqlLect1   = "SELECT codsector,descripcion FROM sectores WHERE codsuc = {$codsuc} ORDER BY codsector";
	$consultaL = $conexion->prepare($sqlLect1);
	$consultaL->execute();
	$sector    = $consultaL->fetchAll();
	
	// Zona
	$sqlLect2   = "SELECT codzona,descripcion FROM admin.zonas WHERE codsuc = {$codsuc} ORDER BY codzona";
	$consultaL = $conexion->prepare($sqlLect2);
	$consultaL->execute();
	$zona      = $consultaL->fetchAll();

	// Ruta
	$sqlLect3   = "SELECT codrutdistribucion FROM rutasdistribucion WHERE codsuc = {$codsuc} ORDER BY codrutdistribucion";
	$consultaL = $conexion->prepare($sqlLect3);
	$consultaL->execute();
	$ruta      = $consultaL->fetchAll();


    $Sql =  "(
    			/* conexiones */
				SELECT 
				sec.codzona ||''|| sec.codsector ||''|| cli.codrutdistribucion ||''|| 3 as id,
				cli.codsuc as codsuc, 
				sec.codzona as codzona, 
				sec.codsector as codsector,
				cli.codrutdistribucion as ruta, 
				zo.descripcion as distrito, 
				sec.descripcion as sector,  
				COUNT(cli.codrutdistribucion) as total_fact,
				3 AS ORDEN 
				FROM catastro.clientes cli 
				INNER JOIN facturacion.cabfacturacion cab ON (cli.codemp = cab.codemp AND cli.codsuc = cab.codsuc AND cli.codciclo = cab.codciclo AND cli.nroinscripcion = cab.nroinscripcion) 
				INNER JOIN sectores sec ON (sec.codemp = cli.codemp AND sec.codsuc = cli.codsuc AND sec.codsector = cli.codsector) 
				INNER JOIN admin.zonas zo ON (sec.codzona = zo.codzona AND sec.codsuc = zo.codsuc AND sec.codemp = zo.codemp) 
				WHERE cli.codsuc = {$codsuc} 
				AND cab.nrofacturacion = '{$nrofact['nrofacturacion']}' " . $qls . " 
				GROUP BY cli.codsuc,sec.codzona, zo.descripcion, sec.descripcion,cli.codrutdistribucion,sec.codsector
				ORDER BY cli.codsuc,sec.codzona, sec.codsector,cli.codrutdistribucion
			)
			UNION
			(
				/* emitidas */ 
				SELECT 
				sec.codzona ||''|| sec.codsector ||''|| cli.codrutdistribucion ||''|| 1 as id,
				cli.codsuc as codsuc, 
				sec.codzona as codzona, 
				sec.codsector as codsector,
				cli.codrutdistribucion as ruta,
				zo.descripcion as distrito, 
				sec.descripcion as sector, 
				COUNT(cab.*) as total_fact,
				1 AS ORDEN   
				FROM facturacion.cabfacturacion cab
				INNER JOIN catastro.clientes cli ON (cab.codemp = cli.codemp  AND cab.codsuc = cli.codsuc AND cab.nroinscripcion = cli.nroinscripcion)
				INNER JOIN sectores sec ON (sec.codemp = cli.codemp AND sec.codsuc = cli.codsuc AND sec.codsector = cli.codsector) 
				INNER JOIN admin.zonas zo ON (sec.codzona = zo.codzona AND sec.codsuc = zo.codsuc AND sec.codemp = zo.codemp) 
				WHERE cab.codsuc = {$codsuc} 
				AND cab.nrofacturacion = '{$nrofact['nrofacturacion']}'  
				AND cab.nrodocumento > 0  " . $qls . "
				GROUP BY cli.codsuc,sec.codzona, zo.descripcion, sec.descripcion,cli.codrutdistribucion,sec.codsector
				ORDER BY cli.codsuc,sec.codzona, sec.codsector,cli.codrutdistribucion
			)
			UNION
			(
				/* no emitidas*/
				SELECT 
				sec.codzona ||''|| sec.codsector ||''|| cli.codrutdistribucion ||''|| 2  as id,
				cli.codsuc as codsuc, 
				sec.codzona as codzona, 
				sec.codsector as codsector,
				cli.codrutdistribucion as ruta,
				zo.descripcion as distrito, 
				sec.descripcion as sector, 
				COUNT(cab.*) as total_fact,
				2 AS ORDEN   
				FROM facturacion.cabfacturacion cab
				INNER JOIN catastro.clientes cli ON (cab.codemp = cli.codemp  AND cab.codsuc = cli.codsuc AND cab.nroinscripcion = cli.nroinscripcion)
				INNER JOIN sectores sec ON (sec.codemp = cli.codemp AND sec.codsuc = cli.codsuc AND sec.codsector = cli.codsector) 
				INNER JOIN admin.zonas zo ON (sec.codzona = zo.codzona AND sec.codsuc = zo.codsuc AND sec.codemp = zo.codemp) 
				WHERE cab.codsuc = {$codsuc} 
				AND cab.nrofacturacion = '{$nrofact['nrofacturacion']}'  
				AND cab.nrodocumento = 0  " . $qls . "
				GROUP BY cli.codsuc,sec.codzona, zo.descripcion, sec.descripcion,cli.codrutdistribucion,sec.codsector
				ORDER BY cli.codsuc,sec.codzona, sec.codsector,cli.codrutdistribucion
			)
			ORDER BY codsuc,codzona,codsector,ruta, orden";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    $cantidaddatos = count($row);

	$cabecera  = array();  
    $cabcuerpo = array();  

    foreach ($row as $value):

        if (!array_key_exists($value['id'], $cabecera)):
            $cabecera[$value['id']] = array();
            array_push($cabecera[$value['id']], $value['distrito'], $value['sector'], $value['ruta'],$value['total_fact']);
        else:
            array_push($cabecera[$value['id']],$value['total_fact']);            
        endif;

    endforeach;

    $cabcuerpo = array();

    foreach ($zona as $zon):

    	foreach ($sector as $sec):
    	
    		foreach ($ruta as $rut):
            	
            	$padre = $zon['codzona'].$sec['codsector'].$rut['codrutdistribucion'];

		        for ($h=1; $h <=3; $h++):

		            $indice = $zon['codzona'].$sec['codsector'].$rut['codrutdistribucion'].$h;

		            if (array_key_exists($indice, $cabecera)):
						$distritu = $cabecera[$indice][0];
						$sectore  = $cabecera[$indice][1];
						$ruti     = $cabecera[$indice][2];
						$importe  = $cabecera[$indice][3];
		            else:
						$distritu = $zon[$indice][0];
						$sectore  = $sec[$indice][1];
						$ruti     = $rut[$indice][1];
						$importe  = 0;
		            endif;

		            if (!array_key_exists($padre, $cabcuerpo)):
		                $cabcuerpo[$padre] = array();
		                array_push($cabcuerpo[$padre], $distritu, $sectore, $ruti,$importe);
		            else:
		                array_push($cabcuerpo[$padre],$importe);            
		            endif;

		        endfor;
    
			endforeach;
		
		endforeach;
	
	endforeach;



 	// Construccion de Cabecera	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    // $objReporte->SetLeftMargin(10);
    // $objReporte->SetAutoPageBreak(true,5);
	$objReporte->AddPage('H');

	$contador = 0;
	$sub1     = 0;
	$sub2     = 0;
	$sub3     = 0;

    foreach ($cabcuerpo as $value): 

    	if( !empty($value[0]) AND !empty($value[1]) AND !empty($value[2]) ) :

	        $contador++;
			$sub1 += $value[3]; // emitidas 
    		$sub3 += $value[5]; // conexiones

			$objReporte->contenido($contador, strtoupper($value[0]), strtoupper($value[1]), $value[2],$value[5],$value[3]);

    	endif;

    	
    endforeach;

	$objReporte->contenido(strtoupper('TOTAL'),$sub1,$sub3,'','','',1);

	$objReporte->Output();	
		
?>