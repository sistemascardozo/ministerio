<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "INSTALACIONES DE MEDIDORES";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
jQuery(function($)
	{ 
		
		$( "#DivTipos" ).buttonset();

		var dates = $( "#FechaDesde, #FechaHasta" ).datepicker({
			//defaultDate: "+1w",
			buttonText: 'Selecione Fecha de Busqueda',
			showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			//showOn: 'both',
			showButtonPanel: true,
			numberOfMonths: 2,
			onSelect: function( selectedDate ) {
				var option = this.id == "FechaDesde" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});

	});
	var c = 0
	function ValidarForm(Op)
	{
		/*var sectores 		= "";
		var tiposervicio	= "";
		var estadoservicio	= "";
		
		if($("#todosectores").val()==0)
		{
			sectores=$("#sector").val();
			
			if(sectores==0)
			{
				alert("Seleccione el Sector");
				return false;
			}
		}else{
			sectores="%";
		}
		
		if($("#todotiposervicio").val()==0)
		{
			tiposervicio=$("#tiposervicio").val()
			if(tiposervicio==0)
			{
				alert("Seleccione el Tipo de Servicio")
				return false;
			}
		}else{
			
			tiposervicio="%"
		}
		
		if($("#todoestadoservicio").val()==0)
		{
			estadoservicio=$("#estadoservicio").val()

			if(estadoservicio==0)
			{
				alert("Seleccione el Estado de Servicio")
				return false
			}
		}else{
			estadoservicio="%"
		}
		
		if($("#todosciclos").val()==0)
		{
			ciclos = $("#ciclo").val()
			if(ciclos==0)
			{
				alert("Seleccione el Ciclo de Facturacion")
				return false
			}
		}else{
			ciclos = "%"
		}*/

		var Fechas=''
		if($('#ChckFechaReg').attr("checked")!="checked")
		{
			var Desde = $("#FechaDesde").val()
			var Hasta = $("#FechaHasta").val()
			if (Trim(Desde)=='')
			{
				
				Msj('#FechaDesde','Seleccione la Fecha Inicial',1000,'above','',false)
				return false;
				
			}
			if (Trim(Hasta)=='')
			{
				
				Msj('#FechaHasta','Seleccione la Fecha Final',1000,'above','',false)
				return false;
				
			}
			Fechas = '&Desde='+Desde+'&Hasta='+Hasta
		}


		if(document.getElementById("rptpdf").checked==true)
		{
			url="imprimir.php";
		}
		if(document.getElementById("rptexcel").checked==true)
		{
			url="excel.php";
		}	
		url += "?codsuc=<?=$codsuc?>"+Fechas
		AbrirPopupImpresion(url,800,600)
		
		return false;
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function quitar_disabled(obj,input)
	{
		if(obj.checked)
		{
			$("#"+input).attr('disabled', true);
		}else{
			$("#"+input).attr('disabled', false);
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">				  
					<tr>
						<td align="right">Fecha de Retiro</td>
						<td width="30" align="center">:</td>
						<td colspan="4">
							<input type="text" class="inputtext" id="FechaDesde" maxlength="10" value="<?=$Fecha?>" style="width:80px;" />
							-
							<input type="text" class="inputtext" id="FechaHasta" maxlength="10" value="<?=$Fecha?>" style="width:80px;" />
							<input type="checkbox"  id="ChckFechaReg" checked="checked"  />
							Todos
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td align="center">&nbsp;</td>
						<td colspan="2">
						    <input type="hidden" name="todosectores" id="todosectores" value="1" />
						    <input type="hidden" name="todotiposervicio" id="todotiposervicio" value="1" />
						    <input type="hidden" name="todoestadoservicio" id="todoestadoservicio" value="1" />
							<input type="hidden" name="todosciclos" id="todosciclos" value="1" />
						</td>
						<td align="center">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="6">
							<div id="DivTipos" style="display:inline">
								<input type="radio" name="rabresumen" id="rptpdf" value="radio" checked="checked" />
								<label for="rptpdf">PDF</label>
								<input type="radio" name="rabresumen" id="rptexcel" value="radio2" />
								<label for="rptexcel">EXCEL</label>				            
							</div>	
							<input type="button" onclick="return ValidarForm();" value="Generar" id="">
						</td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
 
	 </tbody>

    </table>
 </form>
</div>

<?php CuerpoInferior(); ?>