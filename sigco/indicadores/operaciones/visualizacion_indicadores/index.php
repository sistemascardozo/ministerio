<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "Visualizacion de Indicadores de Gestion";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	$sucursal = $objDrop->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_volumen.js" language="JavaScript"></script>
<script>
jQuery(function($)
	{ 
		
		$( "#DivTipos" ).buttonset();
		$("#consumo_ini").val(0);
		$("#consumo_fin").val(0);

	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc = <?=$codsuc?>;

	function ValidarForm(obj){
		var rutas    = ""
		var sectores = ""

		if($("#anio").val()==0)
		{
			alert('Seleccione el Anio a Consultar')
			return false
		}

		if($("#valormes").val()!=1)
		{
			if($("#mes").val()==0)
			{
				alert('Seleccione el Mes a Consultar')
				return false
			}
		}

		if($("#indicador").val()==0)
		{
			alert('Seleccione el Indicador para visualizacion del reporte')
			return false
		}

		var texto = $("#mes option:selected").html();

		if(obj.id == 'rabconsultar')
		{
			$.ajax({
			 url:'consulta.php',
			 type:'POST',
			 async:true,
			 data:"anio="+$("#anio").val()+"&codindicador="+$("#indicador").val()+"&mes="+$("#mes").val()+"&codsuc=<?=$codsuc?>",
			 success:function(datos){
				$("#mostrar_datos").html(datos)
			 }
			}) 
		}
		if(obj.id == 'rabpdf')
		{
			url="grafico.php";
			url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>"
			AbrirPopupImpresion(url,800,600);
			return false;

		}

	}

	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function cargar_rutas_lecturas(obj,cond)
	{
		$.ajax({
			 url:'../../../../ajax/rutas_lecturas_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+obj+'&codsuc=<?=$codsuc?>&condicion='+cond,
			 success:function(datos){
				$("#div_rutaslecturas").html(datos)
				$("#chkrutas").attr("checked",true)
				$("#todosrutas").val(1)
			 }
		}) 
	}
</script>
<div align="center">
	<table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
		<tr>
            <td colspan="2">&nbsp;</td>
		</tr>
        <tr>
            <td colspan="2">
				<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
					<fieldset>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="8%">Sucursal</td>
							<td width="3%" align="center">:</td>
							<td width="33%">
							  <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
							</td>
						</tr>
						<tr>
						    <td>A&ntilde;o</td>
						    <td align="center">:</td>
						    <td>
			                	<div id="div_anio">
			                    	<? $objDrop->drop_anio($codsuc,1,"onchange='cargar_mes(1,{$codsuc},this.value)';") ?>
			                    </div>
			                </td>
						    <td>&nbsp;</td>
							<td align="right">Mes</td>
							<td align="center">:</td>
							<td>
								<div id="div_meses">
							    	<? $objDrop->drop_mes($codsuc,0,0); ?>
							    </div>
							</td>
							<td>&nbsp;</td>
							<td align="center"><input checked="checked" type="checkbox" name="chkmeses" id="chkmeses" value="checkbox" onclick="CambiarEstado(this,'valormes');quitar_disabled(this,'anio');quitar_disabled(this,'mes');" /></td>
			    			<td>Todos los Meses </td>
						</tr>
						<tr>
							<td align="left">Indicador</td>
							<td width="2%" align="center">:</td>
							<td colspan="6" width="48%">
								<? $objDrop->drop_indicadores($codsuc,0,"onchange=''"); ?>
							</td>
						</tr>
						<tr>
							<td colspan="10">&nbsp;</td>
						</tr>
					 	<tr>
   							<td colspan="10" style="padding:4px;"  align="center">
								<div id="DivTipos" style="display:inline">
			                       	<input type="radio" name="rabconsultar" id="rabconsultar" value="radio" onclick="return ValidarForm(this);"  />
			                       	<label for="rabconsultar">Generar</label>
			                        <input type="radio" name="rabpdf" id="rabpdf" value="radio4" onclick="return ValidarForm(this);"  />
			                        <label for="rabpdf">Grafico</label>    
								</div>	
							</td>
  						</tr>
						<tr>
							<td>
							    <input type="hidden" name="valormes" id="valormes" value="1" />
							</td>
						</tr>
					</table>
					</fieldset>
				</form>
			</td>
		</tr>
		<tr>
			<td>
				<div id="mostrar_datos"></div>
			</td>
		</tr>
	</table>
</div>
<script>
	$("#anio").attr("disabled",true)
	$("#mes").attr("disabled",true)
</script>
<?php CuerpoInferior(); ?>