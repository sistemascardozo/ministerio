<?php 
    if(!session_start()){session_start();}

    include("../../../../config.php");

    // Actual
    $codemp       = 1;
    $anio         = $_POST['anio'];
    $mes          = $_POST['mes'];
    $codindicador = $_POST['codindicador'];
    $ciclo        = $_POST['ciclo'];
    $codsuc       = $_POST['codsuc'];


    // Para obtener las variables
    $sqlLect = "SELECT v.codvariable, v.descripcion, d.valor
        FROM indicadores.variables v
        INNER JOIN indicadores.det_variables d ON (d.codvariable = v.codvariable )
        WHERE v.codemp = ? AND v.codsuc = ? AND v.codindicador = ?  
        AND d.anio = ? AND d.mes = ? AND v.estadovariable = 1
        ORDER BY v.codvariable";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$codindicador,$anio,$mes));
    $variables = $consultaL->fetchAll();
    //echo $sqlLect;exit();
    $dat = array();
    foreach ($variables  as $key):

        $dat[]    = $key['valor'];
        $datdes[] = $key['descripcion'];

    endforeach;


    
    switch ($codindicador):
            case 1:
                if(!empty($dat)):
                $resultado  = number_format((($dat[0]-$dat[1])*100)/$dat[0],2);
                $formula    = "Dato = (". $datdes[0] ."-" . $datdes[1] .")X100/".$datdes[0];
                endif;
            break;
            case 2:
                if(!empty($dat)):
                    $resultado = number_format(($dat[0]*100)/$dat[1],2);
                    $formula    = "Dato = (". $datdes[0]."X100)/".$datdes[1];

                endif;
            break;
            case 3:
                if(!empty($dat)):
                    $resultado = number_format(($dat[0]*100)/$dat[1],2);
                    $formula    = "Dato = (". $datdes[0]." x 100)/".$datdes[1];
                endif;
            break;
            case 4:
                if(!empty($dat)):
                    $resultado = number_format((($dat[0]*$dat[1])*100)/$dat[2],2);
                    $formula    = "Cobertura por conex. = (". $datdes[0]." x ". $datdes[1] .") x 100 / ".$datdes[2];
                    $resultado1 = number_format((($dat[3]*$dat[1])*100)/$dat[2],2);
                    $formula1   = "Cobertura por piletas = (". $datdes[3] ." x ". $datdes[1] .") x 100 / ".$datdes[2];

                endif;
            break;
            case 5:
                if(!empty($dat)):
                    $resultado = number_format((($dat[0]*$dat[1])*100)/$dat[2],2);
                    $formula   = "Cobertura de alcant. = (". $datdes[0]." x ". $datdes[1] .") x 100 / ".$datdes[2];
                endif;
            break;
            case 6:
                if(!empty($dat)):
                    $resultado = number_format(($dat[0]*100)/$dat[1],2);
                    $formula   = "Eficencia cobra. = ". $datdes[0] . " x 100 / ".$datdes[1];
                endif;
            break;
            case 7:
                if(!empty($dat)):
                    $resultado = number_format(($dat[0]*100)/$dat[1],2);
                    $formula   = "Efectividad cort. servi. = ". $datdes[0]." x 100 / ".$datdes[1];
                endif;
            break;
            case 8:
                if(!empty($dat)):
                    $resultado = number_format($dat[0]/$dat[1],2);
                    $formula   = "Nivel de Morosidad. = ". $datdes[0]." / ".$datdes[1];
                endif;
            break;
            case 9:
                if(!empty($dat)):
                    $resultado = number_format($dat[0]/$dat[1],2);
                    $formula   = "Tarifa media = ". $datdes[0]." / ".$datdes[1];
                endif;
            break;
            case 11:
                if(!empty($dat)):
                    $resultado = number_format(($dat[0]*100)/$dat[1],2);
                    $formula   = "Densidad Recla. = ". $datdes[0]." x 100 / ".$datdes[1];
                endif;
            break;
            case 12:
                if(!empty($dat)):
                    $resultado = number_format(($dat[0]*100)/$dat[1],2);
                    $formula   = "Indice de Recla. = ". $datdes[0]." x 100 / ".$datdes[1];
                endif;
            break;
    endswitch;

?>
<center> 
<table border="1" aling="center">
    <thead>
        <tr>
            <td align="center">Formula</td>
            <td align="center">Resultado</td>
        </tr>
    <tbody>
        <?php if(!empty($dat)): ?>
        <tr>
            <td><?=$formula?></td>
            <td align="right" style="font-weight: bold;"><?=$resultado.'%'?></td>
        </tr>
        <?php else: ?>
        <tr>
            <td colspan="2" style="font-weight: bold;color:red;">No existe variables asiganas ala formula</td>
        </tr>
        <?php endif; 
        if(!empty($formula1)): ?>
        <tr>
            <td><?=$formula1?></td>
            <td align="right" style="font-weight: bold;"><?=$resultado1.'%'?></td>
        </tr>
        <?php endif; ?>
    </tbody>
</table>
</center> 

