<?php
	include("../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral();
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RESUMEN DE VOLUMEN DE FACTURACION";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(20);
			$this->SetFillColor(18,157,176);
			$this->SetTextColor(255);
			$h=5;
			$h1= 10;

			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			$this->SetFont('Arial','B',10);
			$this->Cell($Dim[1],$h,utf8_decode('Categorias'),1,0,'C',true);
			$this->Cell($Dim[2],$h1,utf8_decode('Consumo Leido'),1,0,'C',true);
			$this->Cell($Dim[3],$h1,utf8_decode('Consumo Promediado'),1,0,'C',true);
			$this->Cell($Dim[4],$h1,utf8_decode('Consumo Asignado'),1,0,'C',true);
			$this->Cell($Dim[5],$h1,utf8_decode('Total'),1,1,'C',true);

			$this->SetY($this->GetY()-$h);
			$this->Cell($Dim[6],$h,utf8_decode('N°'),1,0,'C',true);
			$this->Cell($Dim[7],$h,utf8_decode('Tarifas'),1,1,'C',true);


		}

        function Contenido($cont,$descripcion, $num1, $num2, $num3, $subtotal,$dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',10);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode($num1),1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($num2),1,0,'C',true);
					$this->Cell($Dim[4],$h,utf8_decode($num3),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($subtotal),1,1,'C',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',9);
					$this->Cell($Dim[6],$h,utf8_decode($cont),1,0,'C',true);
					$this->Cell($Dim[7],$h,utf8_decode($descripcion),1,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($num1),1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($num2),1,0,'C',true);
					$this->Cell($Dim[4],$h,utf8_decode($num3),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($subtotal),1,1,'C',true);
					break;
			}

		}
    }

    $Dim = array('1'=>105,'2'=>40,'3'=>40,'4'=>40,'5'=>25,'6'=>15,'7'=>90);

	// Actual
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = $_GET['mes'];
    $mestexto = $_GET['mestexto'];
    $ciclo    = 1;//$_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND codciclo = ".$ciclo." AND anio = '".$anio."' AND mes = '".$mes."'";
		
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array());
    $nrofact = $consultaL->fetch();
    
    // Tipo de Reclamos o Sub-Categoria
    $Sql        = " SELECT catetar,nomtar FROM facturacion.tarifas WHERE codsuc = 1";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):

            // Vol. Asignacion de Consumo
            $sql = "SELECT count(cab.consumofact) as asignacion 
            FROM facturacion.cabfacturacion cab
            WHERE cab.tipofacturacionfact = 0
            AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
            AND cab.mes = '{$mes}' AND cab.anio = '{$anio}'
            AND cab.catetar = {$value['catetar']}";
            $consulta = $conexion->query($sql);
            $valor    = $consulta->fetch();

            // Vol. Promediado
            $sql1 = "SELECT count(cab.consumofact) as promediado
            FROM facturacion.cabfacturacion cab
            WHERE cab.tipofacturacionfact = 1
            AND cab.nrofacturacion =  {$nrofact['nrofacturacion']} 
            AND cab.mes = '{$mes}' AND cab.anio = '{$anio}'
            AND cab.catetar = {$value['catetar']}";
            $consulta1 = $conexion->query($sql1);
            $valor1    = $consulta1->fetch();

            // Vol. Leido
            $sql2 = "SELECT count(cab.consumofact) as leido
            FROM facturacion.cabfacturacion cab
            WHERE cab.tipofacturacionfact = 2
            AND cab.nrofacturacion =  {$nrofact['nrofacturacion']}
            AND cab.mes = '{$mes}' AND cab.anio = '{$anio}'
            AND cab.catetar = {$value['catetar']}";
            $consulta2 = $conexion->query($sql2);
            $valor2    = $consulta2->fetch();


            if( !empty($valor['asignacion']) || !empty($valor1['promediado']) || !empty($valor2['leido'])):
                $cabecera[$value['catetar']] = array();
                array_push($cabecera[$value['catetar']], $value['nomtar'],$valor['asignacion'],$valor1['promediado'],$valor2['leido']);
            endif;      

        endif;

    endforeach;
	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(25);
    $objReporte->SetAutoPageBreak(true,5);
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $subtotal3 = 0;
    $subtotal4 = 0;
    $contador  = 0;

    foreach ($cabecera as $key => $value): 
        $contador++;
        $subtotal1 += $value[1];
        $subtotal2 += $value[2];
        $subtotal3 += $value[3];
        $subtotal4 += $value[1]+$value[2]+$value[3];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2], $value[3],($value[1]+$value[2]+$value[3]));
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,$subtotal4,1);

	$objReporte->Output();	
		
?>