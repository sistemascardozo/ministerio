<?php
  session_name("pnsu");
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");

  $TituloVentana = "CATALOGOS DE INDICADORES";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];

  $FormatoGrilla = array ();
  $Sql  = "SELECT DISTINCT i.codindicador,i.descripcion,i.estadoindicador ";
  $Sql .= "FROM indicadores.indicadores i";

  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'i.codindicador', '2'=>'i.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Descripcion','T3'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'right');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " AND i.codsuc = $codsuc ORDER BY i.codindicador  ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1', //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'3',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar',
              'BtnI2'=>'eliminar.png',
              'Btn2'=>'Eliminar',
              'BtnF2'=>'onclick="Eliminar(this.id)"',
              'BtnCI2'=>'3', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png',
              'Btn3'=>'Restablecer',
              'BtnF3'=>'onclick="Restablecer(this.id)"',
              'BtnCI3'=>'3',
              'BtnCV3'=>'0');

  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 650, 250);
  Pie();
  CuerpoInferior();
?>
