<?php
	session_name("pnsu");
	if(!session_start()){session_start();}


	include("../../../../objetos/clsDrop.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc 	= $_SESSION['IdSucursal'];


	$objMantenimiento = new clsMantenimiento();

	if($Id!='')
	{
		$SQL  = "SELECT * FROM indicadores.variables AS va ";
		$SQL .= "WHERE va.codvariable = ? AND codsuc = $codsuc ";
		$medio = $conexion->prepare($SQL);
		$medio->execute(array($Id));
		$row = $medio->fetch();

		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var urldir 	= "<?php echo $_SESSION['urldir'];?>"
	function ValidarForm(Op)
	{
		if ($("#Nombre").val() == '')
		{
			Msj($("#valor"), "Por favor digite el nombre de la descripcion")
			$("#Nombre").focus();
			return false;
		}
		if ( $("#valor").val() == 0 || $("#valor").val() == '')
        {
            Msj($("#valor"), "Digite el el valor de la variable");
            $("#valor").focus();
            return false;
        }
        if ( $("#anio").val() == 0 || $("#valor").val() == '')
        {
            Msj($("#anio"), "Escoja el año a ingresar");
            $("#anio").focus();
            return false;
        }
        if ( $("#mes").val() == 0 || $("#mes").val() == '')
        {
            Msj($("#mes"), "Escoja el mes a ingresar");
            $("#mes").focus();
            return false;
        }
		GuardarP(Op);
		return false;
	}
	function cargar_anio(codciclo,condicion)
	{
	    $.ajax({
			 url:urldir+"ajax/anio_drop.php",
			 type:'POST',
			 async:true,
			 data:'codsuc='+codsuc+'&codciclo='+codciclo+'&condicion='+condicion,
			 success:function(datos){
				$("#div_anio").html(datos)
				$("#anio [option='0']").prop("selected",true)
			 }
	    })

	}
	function cargar_mes(codciclo,suc,anio)
	{
	    $.ajax({
			 url:urldir+"ajax/mes_drop.php",
			 type:'POST',
			 async:true,
			 data:'codsuc='+suc+'&codciclo='+codciclo+'&anio='+anio,
			 success:function(datos){
				$("#div_meses").html(datos)
			 }
	    })

	}


</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
            <td class="TitDetalle">Id :</td>
            <td class="CampoDetalle">
		<input name="codvariable" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/></td>
	</tr>

	<tr>
	  <td class="TitDetalle">Indicador :</td>
	  <td class="CampoDetalle">
	    	<select name="codindicador" id="codindicador" style="width:280px">
                <?php
                $Sql  = "SELECT * FROM indicadores.indicadores ";
								$Sql .= "WHERE estadoindicador = 1 AND codsuc = $codsuc ORDER BY descripcion ASC";

                $Consulta = $conexion->query($Sql);
                foreach ($Consulta->fetchAll() as $row1) {
                    $Sel = "";
                    if ($row["codindicador"] == $row1["codindicador"])
                        $Sel = 'selected="selected"';
                   ?>
                    <option value="<?php echo $row1["codindicador"];?>" <?=$Sel?>  ><?php echo $row1["descripcion"];?></option>
                <?php }?>
            </select>
	    </td>
	</tr>
	<tr>
		<td class="TitDetalle">Descripcion :</td>
		<td class="CampoDetalle">
			<input class="inputtext" name="descripcion" type="text" id="Nombre" maxlength="200" value="<?=isset($row[4])?$row[4]:''?>" style="width:400px;"/>
		</td>
	</tr>
	<tr>
		<td class="TitDetalle">Abreviatura:</td>
		<td class="CampoDetalle">
			<input class="inputtext" name="abreviatura" type="text" id="abreviatura" maxlength="200" value="<?=isset($row['abreviatura'])?$row['abreviatura']:''?>" style="width:400px;"/>
		</td>
	</tr>
	<tr>
		<td class="TitDetalle">Calculado :</td>
		<td class="CampoDetalle">
			<input name="estcalculado" type="checkbox" id="estcalculado" onclick="CambiarEstado(this,'calculado');" <?=(empty($row['calculado'])) ? '' : 'checked="checked"' ;?> />
			<input type="hidden" name="calculado" id="calculado" value="<?=(empty($row['calculado'])) ? '0' : $row['calculado'] ;?>" />
		</td>
	</tr>
	<tr>
	  <td class="TitDetalle">Estado :</td>
	  <td class="CampoDetalle">
      	<?php include("../../../../include/estareg.php"); ?>
      </td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>
    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php
	$est = isset($row["estareg"])?$row["estareg"]:1;

	include("../../../../admin/validaciones/estareg.php");
?>
