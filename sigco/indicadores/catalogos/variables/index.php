<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  $TituloVentana = "VARIABLES";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  $FormatoGrilla = array ();
  $Sql  = "SELECT DISTINCT va.codvariable, ind.descripcion, va.descripcion, va.abreviatura, va.estadovariable ";
  $Sql .= "FROM indicadores.variables AS va ";
	$Sql .= "	JOIN indicadores.indicadores AS ind ON ind.codemp = va.codemp AND ind.codsuc = va.codsuc AND ind.codindicador = va.codindicador ";
	$Sql .= " JOIN public.estadoreg e ON (va.estadovariable = e.id)";

  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'ind.codindicador', '2'=>'va.descripcion', '3'=>'ind.descripcion', '4'=>'va.abreviatura');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Indicador', 'T3'=>'Descripci&oacute;n', 'T4'=>'Abreviatura', 'T5'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center', 'A4'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'5', 'W2'=>'40', 'W3'=>'100', 'W4'=>'20');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " AND va.codsuc = $codsuc ORDER BY va.codvariable";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'5',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar',
              'BtnI2'=>'eliminar.png',
              'Btn2'=>'Eliminar',
              'BtnF2'=>'onclick="Eliminare(this.id)"',
              'BtnCI2'=>'5', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png',
              'Btn3'=>'Restablecer',
              'BtnF3'=>'onclick="Restablecer(this.id)"',
              'BtnCI3'=>'5',
              'BtnCV3'=>'0');

  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 650, 280);
  Pie();
  CuerpoInferior();
?>
<script src="js_variables.js" type="text/javascript"></script>
<script type="text/javascript">

function Eliminare(Id)
{
  $("#DivEliminacion").html('<form id="form1" name="form1"><input type="hidden" name="codvariable" id="codvariable" value="' + Id + '" /><input type="hidden" name="estareg" id="estareg" value="0" /></form>');
  $("#ConfirmaEliminacion1").dialog("open");
}
</script>
<div id="ConfirmaEliminacion1" title="Confirmaci&oacute;n de Eliminaci&oacute;n" style="display:none;">
    <table width="100%">
        <tr>
            <td style="font-size:10px"><div id="DivEliminacion" style="width:100%">&nbsp;</div></td>
        </tr>
        <tr>
            <td><span class="ui-icon ui-icon-trash" style="float:left; margin:0 7px 20px 0;"></span>&iquest;Desea Anular este Registro?</td>
        </tr>
    </table>
</div>
