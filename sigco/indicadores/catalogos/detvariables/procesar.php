<?php

  session_name("pnsu");
  if(!session_start()){session_start();}

  error_reporting(E_ALL);
  ini_set("display_errors",1);

  include("../../../../config.php");
  include("../../../../objetos/funciones_indicadores.php");

  $clsindicadores = new clsFunciones_Indicadores();

  $anio         = $_POST["anio"];
  $mes          = $_POST["mes"];
  $codindicador = $_POST["Id"];
  $codsuc       = (!empty($_POST["sucursal"])) ? $_POST["sucursal"] : $_POST["codsuc"];

  $sql  = "SELECT va.codvariable ";
  $sql .= "FROM indicadores.variables AS va ";
  $sql .= "WHERE va.codemp = 1 AND va.codindicador = $codindicador AND va.calculado = 1 ";
  $sql .= " AND va.codsuc = $codsuc ";
  $sql .= "ORDER BY codvariable ";

  // die($sql); exit;

  $consulta = $conexion->query($sql);
  $items    = $consulta->fetchAll();

  $data = array();
  $cnt = 0;

  switch ($codindicador) {

    case 1:

      foreach ($items as $value) {

        if($value['codvariable'] == 1) {
          $monto = $clsindicadores->monto_facturado($codsuc,$anio,$mes);
        }
        if($value['codvariable'] == 2) {
          $monto = $clsindicadores->volumen_facturado(1, $codsuc,1,$anio,$mes );
        }

        $data[$value['codvariable']]['id'] = $value['codvariable'];
        $data[$value['codvariable']]['monto'] = (float)$monto;

      }

    break;

    case 2:

      foreach ($items as $value) {

        if($value['codvariable'] == 3) {
          $monto = $clsindicadores->monto_facturado_agua_alcantarrillado(1, $codsuc,$anio,$mes , 1);
        }
        if($value['codvariable'] == 4) {
          $monto = $clsindicadores->usuarios_conexiones_agua(1, $codsuc,$anio,$mes , 1);
        }

        $data[$value['codvariable']]['id'] = $value['codvariable'];
        $data[$value['codvariable']]['monto'] = (float)$monto;

      }
    break;
    case 5:

      foreach ($items as $value) {

        if($value['codvariable'] == 10) {
          $monto = $clsindicadores->poblacion_conexiones_agua(1, $codsuc,$anio,$mes , 1);
        }
        if($value['codvariable'] == 11) {
          $monto = $clsindicadores->poblacion_conexiones_agua_pileta(1, $codsuc,$anio,$mes , 1);
        }

        $data[$value['codvariable']]['id'] = $value['codvariable'];
        $data[$value['codvariable']]['monto'] = (float)$monto;

      }
    break;
    case 6:

      foreach ($items as $value) {

        if($value['codvariable'] == 13) {
          $monto = $clsindicadores->poblacion_conexiones_alcantarrillado(1, $codsuc, $anio, $mes, 1);
        }

        $data[$value['codvariable']]['id'] = $value['codvariable'];
        $data[$value['codvariable']]['monto'] = (float)$monto;

      }
    break;

    case 9:

      foreach ($items as $value) {

        if($value['codvariable'] == 2) {
          $monto = $clsindicadores->monto_facturado($codsuc,$anio,$mes);
        }
        if($value['codvariable'] == 1) {
          $monto = $clsindicadores->volumen_facturado($codsuc,$anio,$mes);
        }

        $data[$value['codvariable']]['id'] = $value['codvariable'];
        $data[$value['codvariable']]['monto'] = (float)$monto;

      }

    break;

    case 10:

      foreach ($items as $value) {

        if($value['codvariable'] == 21) {
          $monto = $clsindicadores->cuentas_comerciales_cobrar(1, $codsuc, $anio, $mes, 1);
        }
        if($value['codvariable'] == 22) {
          $monto = $clsindicadores->ingresos_operativos(1, $codsuc, $anio, $mes, 1);
        }

        $data[$value['codvariable']]['id'] = $value['codvariable'];
        $data[$value['codvariable']]['monto'] = (float)$monto;

      }

    break;

    case 11:

      foreach ($items as $value) {

        if($value['codvariable'] == 23) {
          $monto = $clsindicadores->conexiones_activas_agua(1, $codsuc, $anio, $mes, 1);
        }
        if($value['codvariable'] == 24) {
          $monto = $clsindicadores->conexiones_totales_agua(1, $codsuc,$anio,$mes , 1);
        }

        $data[$value['codvariable']]['id'] = $value['codvariable'];
        $data[$value['codvariable']]['monto'] = (float)$monto;

      }
    break;
    case 12:

      foreach ($items as $value) {

        if($value['codvariable'] == 25) {
          $monto = $clsindicadores->ingresos_operativos_totales(1, $codsuc, $anio, $mes, 1);
        }

        $data[$value['codvariable']]['id'] = $value['codvariable'];
        $data[$value['codvariable']]['monto'] = (float)$monto;

      }
    break;

  }

  echo json_encode($data);

?>
