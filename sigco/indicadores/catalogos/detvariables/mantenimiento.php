<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../objetos/clsDrop.php");

	$objDrop          = new clsDrop();
	$objMantenimiento = new clsMantenimiento();

	$clsFunciones = new clsFunciones();

	$Id        = isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc 	 = $_SESSION['IdSucursal'];

	if($Id!='')
	{
		$sql   = "SELECT  ind.codindicador as codindicador, ind.descripcion AS indicador, ";
		$sql  .= "va.descripcion AS variable, va.calculado, va.codvariable ";
		$sql  .= "FROM indicadores.indicadores AS ind ";
 		$sql  .= "	JOIN indicadores.variables AS va ON ind.codemp = va.codemp AND ind.codsuc = va.codsuc AND ind.codindicador = va.codindicador ";
		$sql  .= "WHERE ind.codindicador = $Id AND ind.codsuc = $codsuc ORDER BY va.codvariable ";

		$data = $conexion->query($sql);
		$row = $data->fetchAll();

		$guardar	= $guardar."&Id2=".$Id;
	}

	$cnt = 0;


?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery.numeric.min.js" language="JavaScript"></script>
<script>
	var count=0;
	var contador=0;
	var meses = {1:"ENERO",
				2:"FEBRERO",
				3:"MARZO",
				4:"ABRIL",
				5:"MAYO",
				6:"JUNIO",
				7:"JULIO",
				8:"AGOSTO",
				9:"SETIEMBRE",
				10:"OCTUBRE",
				11:"NOVIEMBRE",
				12:"DICIEMBRE"};

	var urldir 	= "<?php echo $_SESSION['urldir'];?>";
	$('#valor').numeric('.');

	$(document).ready(function() {
		$("#DivTipos").buttonset();
	});


	function Cancelar()
	{
		location.href='index.php';
	}

	function cargar_subsistemas(obj,sel)
	{
		$.ajax({
			url:'<?php echo $_SESSION['urldir'];?>ajax/subsistema.php',
			type:'POST',
			async:true,
			data:'sistema='+obj+'&seleccion='+sel,
			success:function(datos){
				$("#div_subsistema").html(datos)
			}
		})
	}

	function cargar_perfiles(obj)
	{
		$.ajax({
			url:'<?php echo $_SESSION['urldir'];?>ajax/perfiles.php',
			type:'POST',
			async:true,
			data:'codsubsistema='+obj,
			success:function(datos){
				$("#div_perfiles").html(datos)
			}
		})
	}

	function cargar_mes(codciclo,suc,anio)
	{
	    $.ajax({
			 url:urldir+"ajax/mes_drop.php",
			 type:'POST',
			 async:true,
			 data:'codsuc='+suc+'&codciclo='+codciclo+'&anio='+anio,
			 success:function(datos){
				$("#div_meses").html(datos)
			 }
	    })

	}

	function ValidarFormu(obj){
		var id = $(obj).attr('id');

		if($("#anio").val()==0)
		{
			alert('Seleccione el Anio')
			return false
		}
		if($("#mes").val()==0)
		{
			alert('Seleccione el Mes')
			return false
		}

		var form  = $('#form1').serialize();
		var monto = 0;

		if(id === 'procesar'){

			$.ajax({
				 url:'procesar.php',
				 type:'POST',
				 async:true,
				 dataType : 'json',
				 data: form,
				 success:function(datos){

					if (typeof datos.no == "undefined"){
						$("#tbvariable tbody tr").each(function(index, el){

							iddat = $(this).attr("data");
							ide = $(this).attr("ide");

							if(ide === 'procesar'){

								if(typeof datos[iddat] != "undefined"){
									monto = datos[iddat].monto
									$(this).children('td:eq(1)').children('div').children('label').html(monto);
									$(this).children('td:eq(1)').children('div').children('label').css( "font-weight", "none" );
									$("#cambio").val(1);
								}
							}

						});

					}

				 }
			})

		}

		if(id === 'agregar'){

			var iddat = '';

			if($("#cambio").val() == 0){
				Msj('#tbvariable','Aun no se a procesados las variables, haz clic sobre el boton PROCESAR')
				return false;
			}

			var cnt = 0;
			var estado = $("#tbvariable tbody tr").each(function(index, el){

				iddat = $(this).attr("ide");

				if(iddat === 'noprocesar'){

					monto = $(this).children('td:eq(1)').children('input').val();
					if(monto === ''){
						Msj('#tbvariable','Los campos de las variables que NO SE PROCESAN estan vacios')
						$(this).children('td:eq(1)').children('input').focus();
						cnt++;
						return false;
					}

				}

			});


			if(cnt > 0){ return false;}

			var tr          = "";
			var contador    = $("#cont").val();

			var descripcion = '';
			var idevar      = '';
			var aniomes     = $("#anio").val()+$("#mes").val();
			// $("#tbperfiles tbody").html('');
			$("#tbperfiles tbody tr").each(function(index, el){
				idevar = $(this).attr('data-variable');
				if(idevar === aniomes){
					$(this).remove();
				}
			});


			$("#tbvariable tbody tr").each(function(index, el){

				iddat = $(this).attr("ide");

				if(iddat === 'procesar' || iddat === 'noprocesar'){

					contador++;

					id = $(this).attr("data");
					descripcion = $(this).children('td:eq(0)').html();;
					tr = "<tr style='text-align: center;' data-variable ='"+aniomes+"'>";

					if(iddat === 'procesar'){
						valor = $(this).children('td:eq(1)').children('div').children('label').html();
					}
					if(iddat === 'noprocesar'){
						valor = $(this).children('td:eq(1)').children('input').val();
					}

					tr += "<td>"+descripcion+"</td>";
					tr += "<td><input type='hidden' name='anio"+contador+"' id='anio"+contador+"' value='"+$("#anio").val()+"'>"+$("#anio").val()+"</td>";
					tr += "<td><input type='hidden' name='mes"+contador+"' id='mes"+contador+"' value='"+$("#mes").val()+"'>"+meses[$("#mes").val()]+"</td>";
					tr += "<td style='text-align: right;'><input type='hidden' name='valor"+contador+"' id='valor"+contador+"' value='"+valor+"'>"+valor+"</td>";
					tr += "<td style='text-align: right;'><input type='hidden' id='idvariable"+contador+"' name='idvariable"+contador+"' value='"+id+"' /> &nbsp;</td>";
					tr += "</tr>";
					$("#cont").val(contador);
					$("#tbperfiles").append(tr);

				}

			});

		}


	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
  <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id :</td>
      <td class="CampoDetalle">
		<input name="Id" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
		<input name="codsuc" type="hidden" id="codsuc" size="4" value="<? echo $codsuc; ?>" class="inputtext"/>
		<input name="cambio" type="hidden" id="cambio" value="1" />
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Indicador :</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="indicador" type="text" id="indicador" size="70" maxlength="200" value="<?=$row[0]["indicador"]?>"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
	  <td colspan="2" class="TitDetalle" style="text-align: center; background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px">
    	Seleccione el Año y Mes
      </td>
	</tr>
	<tr>
	  <td colspan="2" class="TitDetalle">
          <fieldset>
          	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbvariable">
          	<tr>
				  		<td class="TitDetalle" style="width: 25%;">Anio:</td>
				  		<td class="CampoDetalle" style="width: 25%;">
                <? $objDrop->drop_anio($codsuc,1,"onchange='cargar_mes(1,{$codsuc},this.value)'"); ?>
			    		</td>
							<td class="TitDetalle" style="width: 25%;">Mes:</td>
							<td class="CampoDetalle" style="width: 25%;">
								<div id="div_meses">
										<? $objDrop->drop_mes($codsuc,0,0); ?>
									</div>
							</td>
						</tr>
						<tr style="text-align:center; background-color:#06F; color:#FFF; font-size:11px; font-weight:bold; padding:4px">
							<td colspan="2" >VARIALBLES</td>
							<td colspan="2">&nbsp;</td>
						</tr>
                <?php foreach ($row as $val) { $cnt++;?>
									<?php  if($val['calculado'] == 1){ ?>
										<tr style="padding:10px; text-align: right;" data="<?=$val['codvariable']?>" ide="procesar">
											<td style="text-align: left;" colspan="2"><?=$val['variable'];?></td>
											<td colspan="2"><div style="font-weight: bold;"><label ide="valor_monto">POR PROCESAR</label></div></td>
										</tr>
									<?php } else{ ?>
										<tr style="padding:10px; text-align: right;" data="<?=$val['codvariable']?>" ide="noprocesar">
											<td style="text-align: left;" colspan="2"><?=$val['variable'];?></td>
											<td colspan="2"><input class="numeric" name="valor" type="text" id="valor" ide="valor_monto" size="35" maxlength="200" value=""/></td>
										</tr>
									<?php } ?>
                <?php } ?>
						<tr>
        			<td colspan="4" style="text-align:center; padding:10px">
        				<div id="DivTipos" style="display:inline">
                 <input type="radio" name="rabresumen" id="procesar" value="radio" onclick="return ValidarFormu(this);" />
                  <label for="procesar">PROCESAR</label>
                  <input type="radio" name="rabresumen" id="agregar" value="radio2" onclick="return ValidarFormu(this);" />
                 <label for="agregar">AGREGAR</label>
							 </div>
        			</td>
        		</tr>
            </table>
          </fieldset></td>
	  </tr>
	<tr>
	  <td colspan="2" class="TitDetalle">
         <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbperfiles" rules="all" >
					 <thead>
						<tr class="ui-widget-header" >
              <td align="center" >Variable</td>
              <td align="center" >Año</td>
              <td align="center" >Mes</td>
              <td align="center" >Valor</td>
              <td align="center" >&nbsp;</td>
            </tr>
					</thead>
					<tbody>
			<?php
				$cont = 0;

				$sqlD  = "SELECT v.descripcion as variable, v.codvariable, d.valor, d.anio, d.mes ";
				$sqlD .= "FROM indicadores.det_variables d ";
				$sqlD .= "	JOIN indicadores.variables v ON(v.codsuc = d.codsuc AND v.codvariable = d.codvariable) ";
				$sqlD .= "WHERE v.codindicador = $Id AND d.codsuc = $codsuc ORDER BY d.anio, CAST(d.mes AS INTEGER) DESC ";
				$consultaD = $conexion->prepare($sqlD);
				$consultaD->execute();
				$itemsD = $consultaD->fetchAll();

				foreach($itemsD as $rowD)
				{
					$cont += 1;
			?>
                <tr data-variable="<?=$rowD["anio"].$rowD["mes"]?>">
                  <td align="center">
                  	<?=strtoupper($rowD["variable"])?>
                  </td>
                  <td align="center">
                  	<input type='hidden' name='anio<?=$cont?>' id='anio<?=$cont?>' value='<?=$rowD["anio"]?>'>
                  	<?=strtoupper($rowD["anio"])?>
                  </td>
                  <td align="center">
                  	<input type='hidden' name='mes<?=$cont?>' id='mes<?=$cont?>' value='<?=$rowD["mes"]?>'>
                  	<?=strtoupper($clsFunciones->obtener_nombre_mes($rowD["mes"]))?>
                  </td>
                  <td align="right">
                  	<input type='hidden' name='valor<?=$cont?>' id='valor<?=$cont?>' value='<?=$rowD["valor"]?>'>
                  	<?=$rowD["valor"]?>
              	</td>
                  <td align="center">&nbsp;
										<input type='hidden' id='<?=$cont?>' name='idvariable<?=$cont?>' value="<?=$rowD["codvariable"];?>" />
                  	<!-- <span class="icono-icon-trash" onClick='QuitaFilaD(this,1)' ></span> -->
                  </td>
                </tr>
            <?php }?>
	          <script>
							count	 = <?=$cont?>;
							contador = <?=$cont?>;
						</script>
						</tbody>
          </table>
				</td>
	  </tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle"><input type="hidden" name="cont" id="cont" value="<?=$cont?>" /></td>
	  </tr>
    </tbody>

  </table>
 </form>
</div>
