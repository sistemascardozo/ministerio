<?php 
    if(!session_start()){session_start();}

    // ini_set("display_errors",1);
    // error_reporting(E_ALL);

	include("../../../../config.php");

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    
    // Tipo de Reclamos o Sub-Categoria
    $Sql        = " SELECT catetar,nomtar FROM facturacion.tarifas WHERE codsuc = 1";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):

            // Vol. Asignacion de Consumo
            $sql = "SELECT count(cab.consumofact) as asignacion 
            FROM facturacion.cabfacturacion cab
            WHERE cab.tipofacturacionfact = 0
            AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
            AND cab.mes = '{$mes}' AND cab.anio = '{$anio}'
            AND cab.catetar = {$value['catetar']}";
            $consulta = $conexion->query($sql);
            $valor    = $consulta->fetch();

            // Vol. Promediado
            $sql1 = "SELECT count(cab.consumofact) as promediado
            FROM facturacion.cabfacturacion cab
            WHERE cab.tipofacturacionfact = 1
            AND cab.nrofacturacion =  {$nrofact['nrofacturacion']} 
            AND cab.mes = '{$mes}' AND cab.anio = '{$anio}'
            AND cab.catetar = {$value['catetar']}";
            $consulta1 = $conexion->query($sql1);
            $valor1    = $consulta1->fetch();

            // Vol. Leido
            $sql2 = "SELECT count(cab.consumofact) as leido
            FROM facturacion.cabfacturacion cab
            WHERE cab.tipofacturacionfact = 2
            AND cab.nrofacturacion =  {$nrofact['nrofacturacion']}
            AND cab.mes = '{$mes}' AND cab.anio = '{$anio}'
            AND cab.catetar = {$value['catetar']}";
            $consulta2 = $conexion->query($sql2);
            $valor2    = $consulta2->fetch();


            if( !empty($valor['asignacion']) || !empty($valor1['promediado']) || !empty($valor2['leido'])):
                $cabecera[$value['catetar']] = array();
                array_push($cabecera[$value['catetar']], $value['nomtar'],$valor['asignacion'],$valor1['promediado'],$valor2['leido']);
            endif;      

        endif;

    endforeach;

?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2">Categorias</td>
            <td align="center" rowspan="2">Consumo Leido</td>
            <td align="center" rowspan="2">Consumo Promediado</td>
            <td align="center" rowspan="2">Consumo Asignado</td>
            <td align="center" rowspan="2">Total</td>
        </tr>
        <tr>
            <td align="center" width="5%">N°</td>
            <td align="center">Tarifas</td>
        </tr>
	<tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal4 = 0;
        $contador  = 0;
        foreach ($cabecera as $key => $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?=$value[1];?></td>
                <td align="right"><?=$value[2];?></td>
                <td align="right"><?=$value[3];?></td>
                <td align="right"><?=($value[1]+$value[2]+$value[3]);?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal4 += $value[1]+$value[2]+$value[3];
            endforeach;?>
		<tr style="font-weight: bold">
            <td colspan="2" >Total</td>
            <td align="right"><?=$subtotal1;?></td>
            <td align="right"><?=$subtotal2;?></td>
            <td align="right"><?=$subtotal3;?></td>
            <td align="right"><?=$subtotal4;?></td>
		</tr>
	</tbody>
</table>
</center> 

