<?php 
	include("../../../../../objetos/clsReporte.php");
	
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $mes,$anio,$meses,$x,$EstadoServicioN;
			
			$h=4;
			$this->SetFont('Arial','B',8);
			$this->Cell(0, $h+1,"ESTRUCTURA TARIFARIA - IMPORTE ",0,1,'C');
			$this->SetFont('Arial','B',8);
			
			if($mes==1)
				{
					$mesa=12;
					$anioa=$anio;
					$aniob=$anio-1;

				}
			else
			{
				$mesa=$mes;
				$mesb=$mes-1;
				$anioa=$anio;
				$aniob=$anio;
				if($mesa==1)
					{
						$mesb=12;
						$aniob=$anio-1;
					}
			}
			$this->Cell(0, $h+1,"FACTURACION DE ".$meses[$mesa]."-".$anioa." DEL CONSUMO DE ".$meses[$mesb]." - ".$aniob,0,1,'C');
			if($EstadoServicioN!="")
				$this->Cell(0, $h+1,"ESTADO DE SERVICIO : ".$EstadoServicioN,0,1,'C');
			$this->Ln(2);
			
			
			
			
		}
		function Leyenda()
		{
			global $x,$conexion;

		}
    
    }
	
	$anio           = $_GET["anio"];
	$mes            = $_GET["mes"];
	$ciclo          = $_GET["ciclo"];
	$codsuc         = $_GET["codsuc"];
	$EstadoServicio = $_GET["EstadoServicio"];
	$EstadoServicioN = $_GET["EstadoServicioN"];
    $periodo =$anio.$mes;
    $x = 30;
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("P");
	$CondEstSer = "";
	if($EstadoServicio!="")
		$CondEstSer = " AND c.codestadoservicio=".$EstadoServicio." ";

	$sqlC  = "SELECT cat.codcategoriatar,cat.descripcion 
	FROM facturacion.cabctacorriente as c ";
	$sqlC .= "inner join facturacion.tarifas as tar on(c.codemp=tar.codemp AND c.codsuc=tar.codsuc AND ";
	$sqlC .= "c.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "WHERE c.periodo='".$periodo."' AND c.codsuc=".$codsuc." 
	AND c.codciclo=".$ciclo." 
	AND c.tipo=0 AND c.tipoestructura=0 ".$CondEstSer;
	$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
	
	$consultaC = $conexion->query($sqlC);
	$itemsC = $consultaC->fetchALl();
	$h=4;
	$objreporte->SetFont('Arial','B',7);
	$objreporte->SetX($x-10);
	$objreporte->Cell(70, $h,'- FACTURACION DEL MES',0,1,'l');
	$objreporte->SetFont('Arial','',6);
		
	$objreporte->Ln(2);
	$objreporte->SetX($x);
	$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
	$objreporte->Cell(30, $h,"RANGOS",1,0,'L');
	$objreporte->Cell(40, $h,"CONCEPTOS",1,0,'L');
	$objreporte->Cell(20, $h,"IMPORTE",1,0,'C');
	$objreporte->Cell(20, $h,"USUARIOS",1,1,'C');
	foreach($itemsC as $rowC)
	{
		
		$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				FROM facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc 
				AND d.codciclo=c.codciclo AND 
				d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
				d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp AND d.codsuc=cp.codsuc 
				AND d.codconcepto=cp.codconcepto)
				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp AND 
				c.codsuc=tar.codsuc AND c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar) 
				WHERE  c.periodo ='".$periodo."'  AND 
				/*d.categoria=:categoria AND*/ c.codsuc=".$codsuc." 
				AND c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0
				AND tar.codcategoriatar='".$rowC[0]."'".$CondEstSer;
		$ConsultaMo =$conexion->query($Sql);
		$rowMo		= $ConsultaMo->fetch();
		$objreporte->SetTextColor(255,0,0);
		$objreporte->SetFont('Arial','B',6);
		$objreporte->SetX($x);
		$objreporte->Cell(100, $h,strtoupper($rowC[1]),0,0,'L');
		$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
		$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	
		
		
		$sqlT  = "SELECT catetar,substring(nomtar,1,3) as nomtar,hastarango1,hastarango2,hastarango3
		FROM facturacion.tarifas  
		WHERE codemp=1 AND codsuc=:codsuc  ";
		$sqlT .= " AND codcategoriatar=:codcategoriatar  ";
		$sqlT .= "order by catetar";
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			
			/*$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					FROM facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc 
					AND d.codciclo=c.codciclo AND 
					d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
					d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp AND d.codsuc=cp.codsuc 
					AND d.codconcepto=cp.codconcepto)
					WHERE  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' AND 
					c.codsuc=".$codsuc." 
					AND c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 ".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();

			$objreporte->SetFont('Arial','B',6);
			$objreporte->SetX($x+5);
			$objreporte->Cell(65, $h,strtoupper($rowT[1]),0,0,'l');
			$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
			$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
			*/
			//INICIO POR RANGOS
			//RANGO INICIAL
			$RangoInicial = 0;
			$RangoFinal = $rowT['hastarango1'];
			$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					FROM facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc AND d.codciclo=c.codciclo AND 
					d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
					d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp AND d.codsuc=cp.codsuc 
					AND d.codconcepto=cp.codconcepto)
					
					WHERE  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' AND 
					c.codsuc=".$codsuc." 
					AND c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();
			if ($rowMo[1]>0)
			{
				$objreporte->SetFont('Arial','B',6);
				$objreporte->SetTextColor(0,0,0);
				$objreporte->SetX($x);
				$objreporte->Cell(30, $h,'',0,0,'L');
				$objreporte->Cell(70, $h,'Rango Inicial ( '.number_format($RangoInicial,2).' - '.number_format($RangoFinal,2).' )',0,0,'L');
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
				$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');

				$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo
					FROM facturacion.cabctacorriente c
				  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
				  AND (c.codsuc = dc.codsuc)
				  AND (c.codciclo = dc.codciclo)
				  AND (c.nrofacturacion = dc.nrofacturacion)
				  AND (c.nroinscripcion = dc.nroinscripcion)
				  AND (c.anio = dc.anio)
				  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
				  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
				  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
				  AND c.periodo='".$periodo."' AND c.catetar='".$rowT["catetar"]."'
				  AND c.tipo=0 AND c.tipoestructura=0 
				  AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer."
				  ORDER BY co.ordenrecibo";
				  $ConsultaCo =$conexion->query($Sql);
				foreach($ConsultaCo->fetchAll() as $rowCo)
				{
					$objreporte->SetFont('Arial','',6);
					$objreporte->SetX($x +30);
					$objreporte->Cell(30, $h,'',0,0,'L');
					$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
					$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
						FROM facturacion.detctacorriente as d
						inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc 
						AND d.codciclo=c.codciclo AND 
						d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
						d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
						inner join facturacion.conceptos as cp on(d.codemp=cp.codemp AND d.codsuc=cp.codsuc 
						AND d.codconcepto=cp.codconcepto)
						WHERE  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' AND 
						c.codsuc=".$codsuc." 
						AND c.codciclo=".$ciclo." AND c.tipo=0 AND c.tipoestructura=0
						AND cp.codconcepto='".$rowCo[0]."' 
						AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
					$ConsultaMo =$conexion->query($Sql);
					$rowMo		= $ConsultaMo->fetch();
					$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
							//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
							
				}
			}
					//RANGO INICIAL
					//Rango Intermedio
			$RangoInicial = $rowT['hastarango1']+0.01;
			$RangoFinal = $rowT['hastarango2'];
			$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					FROM facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc AND d.codciclo=c.codciclo AND 
					d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
					d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp AND d.codsuc=cp.codsuc 
					AND d.codconcepto=cp.codconcepto)
					
					WHERE  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' AND 
					c.codsuc=".$codsuc." 
					AND c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();
			if ($rowMo[1]>0)
			{
				$objreporte->SetFont('Arial','B',6);
				$objreporte->SetTextColor(0,0,0);
				$objreporte->SetX($x);
				$objreporte->Cell(30, $h,'',0,0,'L');
				$objreporte->Cell(70, $h,'Rango Intermedio ( '.number_format($RangoInicial,2).' - '.number_format($RangoFinal,2).' )',0,0,'L');
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
				$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');

				$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo
						FROM facturacion.cabctacorriente c
					  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
					  AND (c.codsuc = dc.codsuc)
					  AND (c.codciclo = dc.codciclo)
					  AND (c.nrofacturacion = dc.nrofacturacion)
					  AND (c.nroinscripcion = dc.nroinscripcion)
					  AND (c.anio = dc.anio)
					  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
					  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
					  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
					  AND c.periodo='".$periodo."' AND c.catetar='".$rowT["catetar"]."'
					  AND c.tipo=0 AND c.tipoestructura=0 
					  AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer."
					  ORDER BY co.ordenrecibo";
					  $ConsultaCo =$conexion->query($Sql);
					foreach($ConsultaCo->fetchAll() as $rowCo)
					{
						$objreporte->SetFont('Arial','',6);
						$objreporte->SetX($x +30);
						$objreporte->Cell(30, $h,'',0,0,'L');
						$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
						$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
							FROM facturacion.detctacorriente as d
							inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc 
							AND d.codciclo=c.codciclo AND 
							d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
							d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
							inner join facturacion.conceptos as cp on(d.codemp=cp.codemp AND d.codsuc=cp.codsuc 
							AND d.codconcepto=cp.codconcepto)
							WHERE  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' AND 
							c.codsuc=".$codsuc." 
							AND c.codciclo=".$ciclo." AND c.tipo=0 AND c.tipoestructura=0
							AND cp.codconcepto='".$rowCo[0]."' 
							AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
						$ConsultaMo =$conexion->query($Sql);
						$rowMo		= $ConsultaMo->fetch();
						$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
						//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
						
					}
			}
					//Rango Intermedio
					//Rango Final
			$May=$rowT['hastarango2'];
				if($May==0)
					$May=$rowT['hastarango1'];
			$RangoInicial = $May+0.01;
			$RangoFinal = 1000000;//$rowT['hastarango3'];
			$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					FROM facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc AND d.codciclo=c.codciclo AND 
					d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
					d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp AND d.codsuc=cp.codsuc 
					AND d.codconcepto=cp.codconcepto)
					
					WHERE  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' AND 
					c.codsuc=".$codsuc." 
					AND c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();
			if ($rowMo[1]>0 )
			{

				$objreporte->SetFont('Arial','B',6);
				$objreporte->SetTextColor(0,0,0);
				$objreporte->SetX($x);
				$objreporte->Cell(30, $h,'',0,0,'L');
				$objreporte->Cell(70, $h,'Rango Final ( Mayor a '.number_format($May,2).' )',0,0,'L');
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
				$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
				$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo
						FROM facturacion.cabctacorriente c
					  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
					  AND (c.codsuc = dc.codsuc)
					  AND (c.codciclo = dc.codciclo)
					  AND (c.nrofacturacion = dc.nrofacturacion)
					  AND (c.nroinscripcion = dc.nroinscripcion)
					  AND (c.anio = dc.anio)
					  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
					  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
					  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
					  AND c.periodo='".$periodo."' AND c.catetar='".$rowT["catetar"]."'
					  AND c.tipo=0 AND c.tipoestructura=0 
					  AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer."
					  ORDER BY co.ordenrecibo";
					  $ConsultaCo =$conexion->query($Sql);
					foreach($ConsultaCo->fetchAll() as $rowCo)
					{
						$objreporte->SetFont('Arial','',6);
						$objreporte->SetX($x +30);
						$objreporte->Cell(30, $h,'',0,0,'L');
						$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
						$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
							FROM facturacion.detctacorriente as d
							inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc 
							AND d.codciclo=c.codciclo AND 
							d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
							d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
							inner join facturacion.conceptos as cp on(d.codemp=cp.codemp AND d.codsuc=cp.codsuc 
							AND d.codconcepto=cp.codconcepto)
							WHERE  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' AND 
							c.codsuc=".$codsuc." 
							AND c.codciclo=".$ciclo." AND c.tipo=0 AND c.tipoestructura=0
							AND cp.codconcepto='".$rowCo[0]."' 
							AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
						$ConsultaMo =$conexion->query($Sql);
						$rowMo		= $ConsultaMo->fetch();
						$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
						//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
						
					}
				}
					//Rango Intermedio
		}
		
		

		
	

	}
	
	$Sql = "SELECT sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
		FROM facturacion.detctacorriente as d
		inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc 
		AND d.codciclo=c.codciclo AND 
		d.nrofacturacion=c.nrofacturacion AND d.nroinscripcion=c.nroinscripcion AND 
		d.anio=c.anio AND c.mes=c.mes AND c.periodo=d.periodo AND c.tipo=d.tipo )
		WHERE  c.periodo ='".$periodo."' AND c.codsuc=".$codsuc." 
		AND c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 ".$CondEstSer;
	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();
	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(140, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(100, $h,'TOTAL FACTURACION DEL MES ',0,0,'l');
	$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');

	
	$objreporte->Output();	
	
?>