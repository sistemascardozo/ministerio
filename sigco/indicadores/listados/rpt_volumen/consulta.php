<?php 
    if(!session_start()){session_start();}

	include("../../../../config.php");

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];

    $codtipo  = " cab.tipofacturacionfact = 0 AND "; 
    $codtipo1 = " cab.tipofacturacionfact = 1 AND "; 
    $codtipo2 = " cab.tipofacturacionfact = 2 AND "; 

    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion FROM facturacion.periodofacturacion
        WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo AND anio ='$anio' AND mes = '$mes' ";
    $consultaL = $conexion->prepare($sqlLect);
    //$codemp,$codsuc,$ciclo,$anio,$mes    
    $consultaL->execute(array());
    $nrofact = $consultaL->fetch();
    
    // Tipo de tarifas
    $Sql        = " SELECT catetar,nomtar FROM facturacion.tarifas WHERE codsuc = $codsuc";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):

            // Vol. Asignado
            $sql = "SELECT cab.consumofact
            FROM facturacion.cabfacturacion cab
            WHERE cab.codsuc= $codsuc AND ".$codtipo." cab.nrofacturacion = {$nrofact['nrofacturacion']} 
            AND nrodocumento > 0";
            $consulta = $conexion->query($sql);
            $valor    = $consulta->fetchAll();
            foreach ($row as $value):
            endforeach;
            
            // Vol. Promediado
            $sql1 = "SELECT sum(cab.consumofact) as promediado
            FROM facturacion.cabfacturacion cab
            WHERE cab.codsuc= $codsuc AND ".$codtipo1."
            cab.nrofacturacion =  {$nrofact['nrofacturacion']} 
            AND nrodocumento > 0";
            $consulta1 = $conexion->query($sql1);
            $valor1    = $consulta1->fetchAll();
            foreach ($row as $value):
            endforeach;

            // Vol. Leido
            $sql2 = "SELECT sum(cab.consumofact) as leido
            FROM facturacion.cabfacturacion cab
            WHERE cab.codsuc= $codsuc AND ".$codtipo2."
            cab.nrofacturacion =  {$nrofact['nrofacturacion']}
            AND nrodocumento > 0";
            $consulta2 = $conexion->query($sql2);
            $valor2    = $consulta2->fetchAll();
            foreach ($row as $value):
            endforeach;

            if( !empty($valor['asignacion']) || !empty($valor1['promediado']) || !empty($valor2['leido'])):
                $cabecera[$value['catetar']] = array();
                array_push($cabecera[$value['catetar']], $value['nomtar'],$valor['asignacion'],$valor1['promediado'],$valor2['leido']);
            endif;      

        endif;

    endforeach;

?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2">Categorias</td>
            <td align="center" colspan="3">Volumen</td>
            <td align="center" rowspan="2">Total</td>
        </tr>
        <tr>
            <td align="center" width="5%">N°</td>
            <td align="center">Tarifas</td>
            <td align="center" >Leido Facturado</td>
            <td align="center" >Promediado Facturado</td>
            <td align="center" >Asignado Facturado</td>
        </tr>
	<tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal4 = 0;
        $contador  = 0;
        foreach ($cabecera as $key => $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?=number_format($value[1],0, ',', ' ');?></td>
                <td align="right"><?=number_format($value[2],0, ',', ' ');?></td>
                <td align="right"><?=number_format($value[3],0, ',', ' ');?></td>
                <td align="right"><?=number_format(($value[1]+$value[2]+$value[3]),0,',', ' ');?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal4 += $value[1]+$value[2]+$value[3];
            endforeach;?>
		<tr style="font-weight: bold">
            <td colspan="2" align="center">Total</td>
            <td align="right"><?=number_format($subtotal1,0,',', ' ');?></td>
            <td align="right"><?=number_format($subtotal2,0,',', ' ');?></td>
            <td align="right"><?=number_format($subtotal3,0,',', ' ');?></td>
            <td align="right"><?=number_format($subtotal4,0,',', ' ');?></td>
		</tr>
	</tbody>
</table>
</center> 

