<?php

include("../../../../objetos/clsReporte.php");
$clfunciones = new clsFunciones();

class clsNumeroUnidades extends clsReporte {

    function Header() {

        global $codsuc, $meses, $objDrop, $Dim, $anio, $mes;

        $periodo = $this->DecFechaLiteral($objDrop);
        //DATOS DEL FORMATO
        $this->SetY(7);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "RESUMEN DE UNIDADES DE USO POR CATEGORIA TARIFARIA";
        $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(277, 5, strtoupper($this->obtener_nombre_mes($mes))." - ".$anio, 0, 1, 'C');
        $this->Ln(1);
        $this->SetFont('Arial', '', 7);
        $hc = 5;
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
        //DATOS DEL FORMATO
        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoupper($empresa["razonsocial"]);
        $tit2 = strtoupper($empresa["num1"]);
        $tit3 = "SUCURSAL: ".utf8_decode(strtoupper($empresa["descripcion"]));
        $x = 23;
        $y = 5;
        $h = 3;
        $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
        // $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

        $this->SetY(10);
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');
        $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
        $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
        $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
        $this->SetY(17);
        $this->SetLineWidth(.1);
        //$this->Cell(0, .1, "", 1, 1, 'C', true);
        $this->cabecera();
    }

    function RoundedRect($x, $y, $w, $h, $r, $style = '') {
        $k = $this->k;
        $hp = $this->h;
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op = 'S';
        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m', ($x + $r) * $k, ($hp - $y) * $k));
        $xc = $x + $w - $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - $y) * $k));

        $this->_Arc($xc + $r * $MyArc, $yc - $r, $xc + $r, $yc - $r * $MyArc, $xc + $r, $yc);
        $xc = $x + $w - $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc + $r, $yc + $r * $MyArc, $xc + $r * $MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x + $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - ($y + $h)) * $k));
        $this->_Arc($xc - $r * $MyArc, $yc + $r, $xc - $r, $yc + $r * $MyArc, $xc - $r, $yc);
        $xc = $x + $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1 * $this->k, ($h - $y1) * $this->k, $x2 * $this->k, ($h - $y2) * $this->k, $x3 * $this->k, ($h - $y3) * $this->k));
    }

    function cabecera() {
        global $Dim;

        $this->Ln(10);
        $this->SetFillColor(18, 157, 176);
        $this->SetTextColor(255);
        $h = 10;
        $h1 = 10;
        $h2 = 10;
        $h3 = 10;

        $this->SetFont('Arial', 'B', 10);

        $this->SetTextColor(3, 3, 3);
        $this->RoundedRect(50, 28, 210, 7, 0, false);
        //$this->Cell($Dim[1], $h, utf8_decode(''), 0, 0, 'R', false);
        $this->Cell($Dim[5], $h, utf8_decode(''), 0, 0, 'R', false);
        $this->Cell($Dim[3], $h, utf8_decode('C/M'), 0, 0, 'R', false);
        $this->Cell($Dim[3], $h, utf8_decode('S/M'), 0, 0, 'R', false);
        $this->Cell($Dim[3], $h, utf8_decode('TOTAL'), 0, 1, 'R', false);

    }
    
    function subcabecera($Sector){
        global $Dim;
        $h = 7;
        
        $this->SetFont('Arial', 'B', 11);
        $this->SetTextColor(3, 3, 3);
        $this->SetX(50);
        $this->Cell($Dim[1], $h, utf8_decode('PROVINCIA  :'), 0, 0, 'R', false);
        $this->Cell($Dim[1], $h, utf8_decode('6   CANCHIS'), 0, 1, 'L', false);
        $this->SetX(50);
        $this->Cell($Dim[1], $h, utf8_decode('DISTRITO   :'), 0, 0, 'R', false);
        $this->Cell($Dim[1], $h, utf8_decode('1   SICUANI'), 0, 1, 'L', false);
        $this->SetX(50);
        $this->Cell($Dim[1], $h, utf8_decode('SECTOR     :'), 0, 0, 'R', false);
        $this->Cell($Dim[1], $h, utf8_decode($Sector), 0, 1, 'L', false);
        $this->Ln(3);
    }
    
    function Contenido($descripcion, $conm, $sinm, $dat = 0) {
        global $Dim;
        $h = 5;
        switch ($dat) {
            case 1:
                $this->SetX(50);
                $this->SetFillColor(7, 96, 125); //Color de Fondo
                $this->SetTextColor(0);
                $h = 12;
                $this->SetFont('Arial', 'B', 12);

                $this->Cell(($Dim[7] * 2), $h, utf8_decode($descripcion), 0, 0, 'C', false);
                $this->Cell($Dim[7], $h, $conm = empty($conm) ? 0 : $conm, 0, 0, 'C', false);
                $this->Cell($Dim[7], $h, $sinm = empty($sinm) ? 0 : $sinm, 0, 0, 'C', false);
                $this->Cell($Dim[7], $h, $conm+ $sinm, 0, 1, 'C', false);

                break;

            default:
                $this->SetX(50);
                $this->SetFillColor(255, 255, 255); //Color de Fondo
                $this->SetTextColor(0);
                $h = 10;
                $this->SetFont('Arial', '', 11);
                $this->Cell($Dim[7], $h, utf8_decode('Categoria :'), 0, 0, 'R', true);
                $this->Cell($Dim[7], $h, utf8_decode($descripcion), 0, 0, 'L', true);
                $this->Cell($Dim[7], $h, $conm = empty($conm) ? 0 : $conm, 0, 0, 'C', true);
                $this->Cell($Dim[7], $h, $sinm = empty($sinm) ? 0 : $sinm, 0, 0, 'C', true);
                $this->Cell($Dim[7], $h, $conm+ $sinm, 0, 1, 'C', true);
                break;
        }
    }

}

$Dim = array('1' => 30, '2' => 90, '3' => 30, '4' => 10, '5' => 80, '6' => 60, '7' => 30);

// Actual
$codemp = 1;
$anio = $_GET['anio'];
$mes = $_GET['mes'];
$mestexto = $_GET['mestexto'];
$ciclo = $_GET['ciclo'];
$codsuc = $_GET['codsuc'];
$fechaperiodo = '01/'.$_GET['mes'].'/'.$_GET['anio'];

// Para obtener el periodo de faturacion
$sqlLect = "SELECT nrofacturacion FROM facturacion.periodofacturacion
    WHERE codemp =$codemp AND codsuc =$codsuc AND codciclo =$ciclo AND anio ='$anio' AND mes ='$mes' ";
$consultaL = $conexion->prepare($sqlLect);
//$codemp, $codsuc, $ciclo, $anio, $mes
$consultaL->execute(array());
$nrofact = $consultaL->fetch();

$where='AND tar.estado=0';
if($mes>'2'){ $where='AND tar.estado=0'; }
    // Datos
    $Sql = "SELECT
    cab.catetar AS catetar,
    TRIM(cab.nromed) AS medidor,
    cat.codcategoriatar AS idcategoriatarifaria,
    cat.descripcion AS categoriatarifaria,
    se.codsector,
    se.descripcion
    FROM
    facturacion.cabfacturacion AS cab
    INNER JOIN facturacion.unidadesusofacturadas AS uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo)
    INNER JOIN facturacion.tarifas AS tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
    INNER JOIN facturacion.categoriatarifaria AS cat ON (tar.codcategoriatar = cat.codcategoriatar)
    INNER JOIN catastro.clientes AS cli ON cli.codemp = cab.codemp AND cli.codsuc = cab.codsuc AND cli.nroinscripcion = cab.nroinscripcion
    INNER JOIN public.sectores AS se ON se.codemp = cli.codemp AND se.codsuc = cli.codsuc AND se.codzona = cli.codzona AND se.codsector = cli.codsector
    WHERE cab.codsuc = '".$codsuc."' AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
    AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' AND cab.codtipousuario = 1 ".$where." 
    ORDER BY se.codsector ASC, cab.catetar ASC";
    $Consulta = $conexion->query($Sql);
    $row = $Consulta->fetchAll();

// Construccion de Cabecera
$array_tarifas = array();

foreach ($row as $value):

    if (!array_key_exists($value['codsector'], $array_tarifas)):
        $array_tarifas[$value['codsector']]['descripcion'] =  $value['descripcion'];
        $array_tarifas[$value['codsector']]['data'] =  array();       
        
    endif;
    
    if (!array_key_exists($value['idcategoriatarifaria'], $array_tarifas[$value['codsector']]['data'])):        
        $array_tarifas[$value['codsector']]['data'] [$value['idcategoriatarifaria']]['descripcion'] = $value['categoriatarifaria'];       
        $array_tarifas[$value['codsector']]['data'] [$value['idcategoriatarifaria']]['data'] =  array('sin_medidor'=>0,  'con_medidor'=>0 ); 
    endif;
    
    if(empty($value['medidor']))
    {
        $array_tarifas[$value['codsector']]['data'] [$value['idcategoriatarifaria']]['data']['sin_medidor']++;
    }  else {
        $array_tarifas[$value['codsector']]['data'] [$value['idcategoriatarifaria']]['data']['con_medidor']++;
    }
    
endforeach;

//echo "<pre>"; print_r($array_tarifas);   echo "</pre>"; exit;

$objReporte = new clsNumeroUnidades('L');
$contador = 0;
$objReporte->AliasNbPages();
$objReporte->SetLeftMargin(20);
$objReporte->AddPage('H');

$contador = 0;

foreach ($array_tarifas as $key => $array_categorias):

    $contador++;
    
    $subtotal1 = 0;
    $subtotal2 = 0;
    $sector = $array_categorias['descripcion'];

    $objReporte->subcabecera($sector);
    //print_r($array_categorias['data']);
    foreach ($array_categorias['data'] as $key => $value):
        
        $categoria = $value['descripcion'];
        $conm = $value['data']['con_medidor'];
        $sinm = $value['data']['sin_medidor'];
        $subtotal1+= $conm;
        $subtotal2+= $sinm;
        $objReporte->Contenido($categoria, $conm, $sinm, 0);
    endforeach;
    
    
    //echo $subtotal1;
    $objReporte->contenido( strtoupper('Total Sector'), $subtotal1, $subtotal2, 1);
endforeach;

$objReporte->Output();
?>