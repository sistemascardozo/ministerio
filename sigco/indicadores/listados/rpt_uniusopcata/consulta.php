<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../config.php");

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    
   $Sql = "SELECT
    cab.catetar AS catetar,
    TRIM(cab.nromed) AS medidor,
    cat.codcategoriatar AS idcategoriatarifaria,
    cat.descripcion AS categoriatarifaria,
    se.codsector,
    se.descripcion
    FROM
    facturacion.cabfacturacion AS cab
    INNER JOIN facturacion.unidadesusofacturadas AS uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo)
    INNER JOIN facturacion.tarifas AS tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
    INNER JOIN facturacion.categoriatarifaria AS cat ON (tar.codcategoriatar = cat.codcategoriatar)
    INNER JOIN catastro.clientes AS cli ON cli.codemp = cab.codemp AND cli.codsuc = cab.codsuc AND cli.nroinscripcion = cab.nroinscripcion
    INNER JOIN public.sectores AS se ON se.codemp = cli.codemp AND se.codsuc = cli.codsuc AND se.codzona = cli.codzona AND se.codsector = cli.codsector
    WHERE cab.codsuc = '".$codsuc."' AND tar.estado=1 AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
    AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' AND cab.codtipousuario = 1
    ORDER BY se.codsector ASC, cab.catetar ASC";
    $Consulta = $conexion->query($Sql);
    $row = $Consulta->fetchAll();

    // Construccion de Cabecera
    $array_tarifas = array();

    foreach ($row as $value):
    
        if (!array_key_exists($value['codsector'], $array_tarifas)):
            $array_tarifas[$value['codsector']]['descripcion'] =  $value['descripcion'];
            $array_tarifas[$value['codsector']]['data'] =  array();       
            
        endif;
        
        if (!array_key_exists($value['idcategoriatarifaria'], $array_tarifas[$value['codsector']]['data'])):        
            $array_tarifas[$value['codsector']]['data'] [$value['idcategoriatarifaria']]['descripcion'] = $value['categoriatarifaria'];       
            $array_tarifas[$value['codsector']]['data'] [$value['idcategoriatarifaria']]['data'] =  array('sin_medidor'=>0,  'con_medidor'=>0 ); 
        endif;
        
        if(empty($value['medidor']))
        {
            $array_tarifas[$value['codsector']]['data'] [$value['idcategoriatarifaria']]['data']['sin_medidor']++;
        }  else {
            $array_tarifas[$value['codsector']]['data'] [$value['idcategoriatarifaria']]['data']['con_medidor']++;
        }
        
    endforeach;
    
?>
<center> 
<table border="1" aling="center">
    <thead>        
        <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">C/M</td>
            <td align="center">S/M</td>
            <td align="center">Total</td>
        </tr>
    </thead>
    <tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        
        $contador  = 0;
        foreach ($array_tarifas as $key => $array_categorias):

            $contador++;
            $sector = $array_categorias['descripcion'];
            foreach ($array_categorias['data'] as $key => $value):
                $categoria = $value['descripcion'];
                $conm = $value['data']['con_medidor'];
                $sinm = $value['data']['sin_medidor'];
        ?>
            <tr>
                <td colspan="5"><?=$sector;?></td>
            </tr>
            <tr>
                <td align="left" colspan="2"><?=strtoupper($categoria);?></td>
                <td align="right"><?=$value[1] = empty($value[1]) ? 0 : $value[1];?></td>
                <td align="right"><?=$value[2] = empty($value[2]) ? 0 : $value[2];?></td>
                <td align="right"><?=$value[3] = empty($value[3]) ? 0 : $value[3];?></td>
            </tr>    
        <?php             
            endforeach;
            
            $subtotal1 += $conm;
            $subtotal2 += $sinm;
        ?>
        <tr style="font-weight: bold">
            <td colspan="2" >Total</td>
            <td align="right"><?=$subtotal1;?></td>
            <td align="right"><?=$subtotal2;?></td>
            <td align="right"><?=$subtotal3;?></td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>
</center> 

