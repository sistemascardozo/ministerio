<?php 
    if(!session_start()){session_start();}

    // ini_set("display_errors",1);
    // error_reporting(E_ALL);

	include("../../../../config.php");

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $periodo  = $anio.$mes;
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    
    $Sql        = "(
                    SELECT codestadoservicio as codestadoservicio,
                    'Estado de Servicio Propenso a Corte' as descripcion,
                    count(nromeses) as cantidad,
                    1 as orden
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' 
                    AND codsuc = {$codsuc} 
                    AND nromeses = 2
                    AND periodo = '{$periodo}' AND codestadoservicio = 1
                    GROUP BY codestadoservicio
                    ORDER BY codestadoservicio
                    )
                    UNION
                    (
                    SELECT
                    codestadoservicio as codestadoservicio,
                    'Estado de Servicio Cortado' as descripcion, 
                    count(nromeses) as cantidad,
                    2 as orden
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' AND codsuc = {$codsuc} 
                    AND nromeses = 2
                    AND periodo = '{$periodo}' AND codestadoservicio = 2
                    GROUP BY codestadoservicio 
                    ORDER BY codestadoservicio
                    )
                    UNION
                    (
                    SELECT 
                    codestadoservicio as codestadoservicio, 
                    'Estado de Servicio Propenso a Corte' as descripcion,
                    count(nromeses) as cantidad,
                    3 as orden  
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' AND codsuc = {$codsuc} 
                    AND nromeses >= 3
                    AND periodo = '{$periodo}' AND codestadoservicio = 1
                    GROUP BY codestadoservicio 
                    ORDER BY codestadoservicio
                    )
                    UNION
                    (
                    SELECT 
                    codestadoservicio as codestadoservicio,
                    'Estado de Servicio Cortado' as descripcion,
                    count(nromeses) as cantidad,
                    4 as orden
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' AND codsuc = {$codsuc} 
                    AND nromeses >= 3
                    AND periodo = '{$periodo}' AND codestadoservicio = 2
                    GROUP BY codestadoservicio 
                    ORDER BY codestadoservicio
                    )
                    UNION
                    (
                    SELECT 
                    codestadoservicio as codestadoservicio,
                    'Estado de Servicio Diferentes' as descripcion,
                    count(nromeses) as cantidad,
                    5 as orden
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' AND codsuc = {$codsuc} 
                    AND nromeses >= 3
                    AND periodo = '{$periodo}' AND codestadoservicio NOT IN(1,2)
                    GROUP BY codestadoservicio 
                    ORDER BY codestadoservicio
                    )
                    ORDER BY orden";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['codestadoservicio'], $cabecera)):
            $cabecera[$value['codestadoservicio']] = array();
            array_push($cabecera[$value['codestadoservicio']], $value['descripcion'],$value['cantidad']);
        else:
            array_push($cabecera[$value['codestadoservicio']],$value['cantidad']);            
        endif;

    endforeach;

?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2">Conceptos</td>
            <td align="center" colspan="5">Meses de Deuda</td>
            <td align="center" rowspan="3">Total</td>
        </tr>
        <tr>
            <td align="center" width="5%" rowspan="2">N°</td>
            <td align="center" width="25%" rowspan="2">Descripcion</td>
            <td align="center" colspan="2">2 Meses</td>
            <td align="center" colspan="3">3 Meses</td>
        </tr>
        <tr>
            <td align="center">Activos</td>
            <td align="center">Cortados</td>
            <td align="center">Activos</td>
            <td align="center">Cortados</td>
            <td align="center">Otro Estados</td>
        </tr>
    <thead>
        <tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal4 = 0;
        $subtotal5 = 0;
        $subtotal6 = 0;
        $contador  = 0;
        foreach ($cabecera as $key => $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?=$value[1] = empty($value[1]) ? 0 : $value[1];?></td>
                <td align="right"><?=$value[2] = empty($value[2]) ? 0 : $value[2];?></td>
                <td align="right"><?=$value[3] = empty($value[3]) ? 0 : $value[3];?></td>
                <td align="right"><?=$value[4] = empty($value[4]) ? 0 : $value[4];?></td>
                <td align="right"><?=$value[5] = empty($value[5]) ? 0 : $value[5];?></td>
                <td align="right"><?=($value[1]+$value[2]+$value[3]+$value[4]+$value[5]);?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal4 += $value[4];
            $subtotal5 += $value[5];
            $subtotal6 += $value[1]+$value[2]+$value[3]+$value[4]+$value[5];
            endforeach;?>
        <tr style="font-weight: bold">
            <td colspan="2" align="center">Total</td>
            <td align="right"><?=$subtotal1;?></td>
            <td align="right"><?=$subtotal2;?></td>
            <td align="right"><?=$subtotal3;?></td>
            <td align="right"><?=$subtotal4;?></td>
            <td align="right"><?=$subtotal5;?></td>
            <td align="right"><?=$subtotal6;?></td>
        </tr>
    </tbody> 
</table>
</center> 

