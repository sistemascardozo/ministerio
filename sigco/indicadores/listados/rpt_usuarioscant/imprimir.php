<?php 
    if(!session_start()){session_start();}

    // ini_set("display_errors",1);
    // error_reporting(E_ALL);

	include("../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral();
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RESUMEN DE CANTIDAD DE USUARIOS FACTURADOS DE 2 O 3 MESES DE DEUDA";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	

			global $Dim;

            $this->Ln(20);
            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=5;
            $h1= 15;
            $h2= 10;

            //$this->SetWidths(array(25,15,40,65,10,23,30,45));
            //$this->SetWidths(array(25,15,40,65,10,23,30,45));
            $this->SetFont('Arial','B',10);
            $this->Cell($Dim[1],$h,utf8_decode('Conceptos'),1,0,'C',true);
            $this->Cell($Dim[2],$h,utf8_decode('Meses de Deuda'),1,0,'C',true);
            $this->Cell($Dim[3],$h1,utf8_decode('Total'),1,1,'C',true);

            $this->SetY($this->GetY()-$h2);
            $this->Cell($Dim[4],$h2,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[5],$h2,utf8_decode('Tarifas'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('2 Meses'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('3 Meses'),1,1,'C',true);

            // $this->SetY($this->GetY()-$h2);
            $this->SetX($this->GetX()+100);
            $this->Cell($Dim[8],$h,utf8_decode('Activos'),1,0,'C',true);
            $this->Cell($Dim[8],$h,utf8_decode('Cortados'),1,0,'C',true);
            $this->Cell($Dim[9],$h,utf8_decode('Activos'),1,0,'C',true);
            $this->Cell($Dim[9],$h,utf8_decode('Cortados'),1,0,'C',true);
            $this->Cell($Dim[9],$h,utf8_decode('Otro Estados'),1,1,'C',true);
		}

        function Contenido($cont,$descripcion, $num1, $num2, $num3, $num4, $num5, $subtotal,$dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',10);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[8],$h,utf8_decode($num1 = empty($num1) ? 0 : $num1),1,0,'C',true);
					$this->Cell($Dim[8],$h,utf8_decode($num2 = empty($num2) ? 0 : $num2),1,0,'C',true);
                    $this->Cell($Dim[9],$h,utf8_decode($num3 = empty($num3) ? 0 : $num3),1,0,'C',true);
                    $this->Cell($Dim[9],$h,utf8_decode($num4 = empty($num4) ? 0 : $num4),1,0,'C',true);
					$this->Cell($Dim[9],$h,utf8_decode($num5 = empty($num5) ? 0 : $num5),1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($subtotal),1,1,'C',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',9);
					$this->Cell($Dim[4],$h,utf8_decode($cont),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($descripcion),1,0,'L',true);
					$this->Cell($Dim[8],$h,utf8_decode($num1 = empty($num1) ? 0 : $num1),1,0,'C',true);
					$this->Cell($Dim[8],$h,utf8_decode($num2 = empty($num2) ? 0 : $num2),1,0,'C',true);
                    $this->Cell($Dim[9],$h,utf8_decode($num3 = empty($num3) ? 0 : $num3),1,0,'C',true);
                    $this->Cell($Dim[9],$h,utf8_decode($num4 = empty($num4) ? 0 : $num4),1,0,'C',true);
					$this->Cell($Dim[9],$h,utf8_decode($num5 = empty($num5) ? 0 : $num5),1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($subtotal),1,1,'C',true);
					break;
			}

		}
    }

    $Dim = array('1'=>100,'2'=>140,'3'=>30,'4'=>10,'5'=>90,'6'=>50,'7'=>90,'8'=>25,'9'=>30);

	// Actual
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = $_GET['mes'];
    $periodo  = $anio.$mes;
    $mestexto = $_GET['mestexto'];
    $ciclo    = $_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    
    $Sql        = "(
                    SELECT codestadoservicio as codestadoservicio,
                    'Estado de Servicio Propenso a Corte' as descripcion,
                    count(nromeses) as cantidad,
                    1 as orden
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' 
                    AND codsuc = {$codsuc} 
                    AND nromeses = 2
                    AND periodo = '{$periodo}' AND codestadoservicio = 1
                    GROUP BY codestadoservicio
                    ORDER BY codestadoservicio
                    )
                    UNION
                    (
                    SELECT
                    codestadoservicio as codestadoservicio,
                    'Estado de Servicio Cortado' as descripcion, 
                    count(nromeses) as cantidad,
                    2 as orden
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' AND codsuc = {$codsuc} 
                    AND nromeses = 2
                    AND periodo = '{$periodo}' AND codestadoservicio = 2
                    GROUP BY codestadoservicio 
                    ORDER BY codestadoservicio
                    )
                    UNION
                    (
                    SELECT 
                    codestadoservicio as codestadoservicio, 
                    'Estado de Servicio Propenso a Corte' as descripcion,
                    count(nromeses) as cantidad,
                    3 as orden  
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' AND codsuc = {$codsuc} 
                    AND nromeses >= 3
                    AND periodo = '{$periodo}' AND codestadoservicio = 1
                    GROUP BY codestadoservicio 
                    ORDER BY codestadoservicio
                    )
                    UNION
                    (
                    SELECT 
                    codestadoservicio as codestadoservicio,
                    'Estado de Servicio Cortado' as descripcion,
                    count(nromeses) as cantidad,
                    4 as orden
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' AND codsuc = {$codsuc} 
                    AND nromeses >= 3
                    AND periodo = '{$periodo}' AND codestadoservicio = 2
                    GROUP BY codestadoservicio 
                    ORDER BY codestadoservicio
                    )
                    UNION
                    (
                    SELECT 
                    codestadoservicio as codestadoservicio,
                    'Estado de Servicio Diferentes' as descripcion,
                    count(nromeses) as cantidad,
                    5 as orden
                    FROM facturacion.cabctacorriente
                    WHERE nrofacturacion = '{$nrofact['nrofacturacion']}' AND codsuc = {$codsuc} 
                    AND nromeses >= 3
                    AND periodo = '{$periodo}' AND codestadoservicio NOT IN(1,2)
                    GROUP BY codestadoservicio 
                    ORDER BY codestadoservicio
                    )
                    ORDER BY orden";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['codestadoservicio'], $cabecera)):
            $cabecera[$value['codestadoservicio']] = array();
            array_push($cabecera[$value['codestadoservicio']], $value['descripcion'],$value['cantidad']);
        else:
            array_push($cabecera[$value['codestadoservicio']],$value['cantidad']);            
        endif;

    endforeach;
	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    // $objReporte->SetLeftMargin(25);
    // $objReporte->SetAutoPageBreak(true,5);
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $subtotal3 = 0;
    $subtotal4 = 0;
    $subtotal5 = 0;
    $subtotal6 = 0;
    $contador  = 0;

    foreach ($cabecera as $key => $value): 
        $contador++;
        $subtotal1 += $value[1];
        $subtotal2 += $value[2];
        $subtotal3 += $value[3];
        $subtotal4 += $value[4];
        $subtotal5 += $value[5];
        $subtotal6 += $value[1]+$value[2]+$value[3]+$value[4]+$value[5];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2], $value[3], $value[4], $value[5],($value[1]+$value[2]+$value[3]+$value[4]+$value[5]));
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,$subtotal4,$subtotal5,$subtotal6,1);
	$objReporte->Output();	
		
?>