<?php

include("../../../../objetos/clsReporte.php");
$clfunciones = new clsFunciones();

class clsNumeroUnidades extends clsReporte {

    function Header() {

        global $codsuc, $meses, $fechaperiodo;
        global $Dim;

        $periodo = $this->DecFechaLiteral($fechaperiodo);
        //DATOS DEL FORMATO
        $this->SetY(7);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "NUMERO DE UNIDADES DE USO POR TIPO DE SERVICIO";
        $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(277, 5, $periodo["mes"]." - ".$periodo["anio"], 0, 1, 'C');
        $this->Ln(1);
        $this->SetFont('Arial', '', 7);
        $hc = 5;
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
        //DATOS DEL FORMATO
        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoupper($empresa["razonsocial"]);
        $tit2 = strtoupper($empresa["num1"]);
        $tit3 = "SUCURSAL: ".utf8_decode(strtoupper($empresa["descripcion"]));
        $x = 23;
        $y = 5;
        $h = 3;
        $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
        // $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

        $this->SetY(10);
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');
        $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
        $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
        $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
        $this->SetY(17);
        $this->SetLineWidth(.1);
        //$this->Cell(0, .1, "", 1, 1, 'C', true);
        $this->cabecera();
    }

    //function Footer()
//        {       
//            $this->SetY(-10);
//            $this->SetFont('Arial','I',6);
//            $this->SetTextColor(0);
//            $this->SetLineWidth(.1);
//            $this->SetX(10);
//            $this->Cell(0,.1,"",1,1,'C',true);
//            $this->Ln(1);
//            $this->SetX(10);
//            $this->Cell(0,4,utf8_decode('Sistema Integrado de Información Comercial - SIINCO - Versión WEB'),0,0,'L');
//            $this->Cell(0,4,'Pag. '.$this->PageNo().' de {nb}',0,0,'R');
//        }

    function cabecera() {
        global $Dim;

        $this->Ln(20);
        $this->SetFillColor(18, 157, 176);
        $this->SetTextColor(255);
        $h = 9;
        $h1 = 27;
        $h2 = 9;
        $h3 = 18;

        //$this->SetWidths(array(25,15,40,65,10,23,30,45));
        $this->SetFont('Arial', 'B', 10);
        $this->Cell($Dim[1], $h, utf8_decode('Categorias'), 1, 0, 'C', true);
        $this->Cell($Dim[2], $h, utf8_decode('Numero de Conexiones'), 1, 0, 'C', true);
        $this->Cell($Dim[2], $h1, utf8_decode('Total'), 1, 1, 'C', true);

        $this->SetY($this->GetY() - $h3);
        $this->Cell($Dim[4], $h3, utf8_decode('N°'), 1, 0, 'C', true);
        $this->Cell($Dim[5], $h3, utf8_decode('Tarifas'), 1, 0, 'C', true);
        $this->Cell($Dim[2], $h2, utf8_decode('Tipo de Servicio'), 1, 1, 'C', true);

        $this->SetX($this->GetX() + 70);
        $this->Cell($Dim[3], $h2, utf8_decode('Agua y Desagüe'), 1, 0, 'C', true);
        $this->Cell($Dim[3], $h2, utf8_decode('Agua'), 1, 0, 'C', true);
        $this->Cell($Dim[3], $h2, utf8_decode('Desagüe'), 1, 0, 'C', true);
        $this->Cell($Dim[3], $h2, utf8_decode('T. Conexiones'), 1, 0, 'C', true);
        $this->Cell($Dim[3], $h2, utf8_decode('T. Agua'), 1, 0, 'C', true);
        $this->Cell($Dim[3], $h2, utf8_decode('T. Desagüe'), 1, 1, 'C', true);
    }

    function Contenido($cont, $descripcion, $num1, $num2, $num3, $num4, $num5, $num6, $subtotal, $dat = 0) {
        global $Dim;

        switch ($dat) {
            case 1:

                $this->SetFillColor(7, 96, 125);
                $this->SetTextColor(255);
                $h = 10;
                $this->SetFont('Arial', 'B', 12);
                $this->Cell(($Dim[4] + $Dim[5]), $h, utf8_decode($descripcion), 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num1 = empty($num1) ? 0 : $num1, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num2 = empty($num2) ? 0 : $num2, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num3 = empty($num3) ? 0 : $num3, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num4 = empty($num4) ? 0 : $num4, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num5 = empty($num5) ? 0 : $num5, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num6 = empty($num6) ? 0 : $num6, 1, 1, 'C', true);
                break;

            default:

                $this->SetFillColor(255, 255, 255); //Color de Fondo
                $this->SetTextColor(0);
                $h = 10;
                $this->SetFont('Arial', '', 10);
                $this->Cell($Dim[4], $h, utf8_decode($cont), 1, 0, 'C', true);
                $this->Cell($Dim[5], $h, utf8_decode($descripcion), 1, 0, 'L', true);
                $this->Cell($Dim[7], $h, $num1 = empty($num1) ? 0 : $num1, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num2 = empty($num2) ? 0 : $num2, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num3 = empty($num3) ? 0 : $num3, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num4 = empty($num4) ? 0 : $num4, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num5 = empty($num5) ? 0 : $num5, 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num6 = empty($num6) ? 0 : $num6, 1, 1, 'C', true);
                // $this->Cell($Dim[3],$h,utf8_decode($subtotal),1,1,'C',true);
                break;
        }
    }

}

$Dim = array('1' => 70, '2' => 90, '3' => 30, '4' => 10, '5' => 60, '6' => 60, '7' => 30);

// Actual
$codemp = 1;
$anio = $_GET['anio'];
$mes = $_GET['mes'];
$mestexto = $_GET['mestexto'];
$ciclo = $_GET['ciclo'];
$codsuc = $_GET['codsuc'];
$fechaperiodo = '01/'.$_GET['mes'].'/'.$_GET['anio'];

// Para obtener el periodo de faturacion
$sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
$consultaL = $conexion->prepare($sqlLect);
$consultaL->execute(array($codemp, $codsuc, $ciclo, $anio, $mes));
$nrofact = $consultaL->fetch();

// Datos
$Sql = " 
        SELECT cab.codsuc, cab.codtiposervicio as codtiposervicio, 
        cab.catetar as catetar,
        tar.nomtar as nomtar,  
        count(cab.*) as cantidad 
        FROM facturacion.cabfacturacion cab
        INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo) 
        INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
        WHERE cab.codsuc = '".$codsuc."' AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
        AND cab.anio = '{$anio}' AND cab.mes = '{$mes}'
        AND cab.codtipousuario = 1 
        GROUP BY cab.codsuc, cab.codtiposervicio, cab.catetar, tar.nomtar
        ORDER BY cab.codsuc, cab.codtiposervicio, cab.catetar
        ";
$Consulta = $conexion->query($Sql);
$row = $Consulta->fetchAll();

// Construccion de Cabecera
$cabecera = array();

foreach ($row as $value):

    if (!array_key_exists($value['catetar'], $cabecera)):
        $cabecera[$value['catetar']] = array();
        array_push($cabecera[$value['catetar']], $value['nomtar'], $value['cantidad']);
    else:
        array_push($cabecera[$value['catetar']], $value['cantidad']);
    endif;

endforeach;

$objReporte = new clsNumeroUnidades('L');
$contador = 0;
$objReporte->AliasNbPages();
$objReporte->SetLeftMargin(20);
$objReporte->AddPage('H');

$subtotal1 = 0;
$subtotal2 = 0;
$subtotal3 = 0;
$subtotal7 = 0;
$contador = 0;

// var_dump($cabecera);exit;

foreach ($cabecera as $key => $value):
    $contador++;

    $subtotal4 = $value[1] + $value[2]; // Agua
    $subtotal5 = $value[1] + $value[3]; // Desague
    $subtotal6 = $value[1] + $value[2] + $value[3]; // Agua y Desague

    $subtotal1 += $value[1];
    $subtotal2 += $value[2];
    $subtotal3 += $value[3];
    $subtotal8 += $subtotal4;
    $subtotal9 += $subtotal5;
    $subtotal10 += $subtotal6;
    $subtotal7 += $value[1] + $value[2] + $value[3];
    $objReporte->contenido($contador, strtoupper($value[0]), $value[1], $value[2], $value[3], $subtotal6, $subtotal4, $subtotal5, ($value[1] + $value[2] + $value[3]));

endforeach;


$objReporte->contenido('', strtoupper('TOTAL'), $subtotal1, $subtotal2, $subtotal3, $subtotal10, $subtotal8, $subtotal9, $subtotal7, 1);

$objReporte->Output();
?>