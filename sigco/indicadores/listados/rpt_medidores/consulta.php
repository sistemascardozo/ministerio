<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../config.php");

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    $cabfactu = "tipofacturacionfact";

    if($mes == 10 AND $anio == 2014 AND ($codsuc == 2 OR $codsuc == 3) ): $cabfactu = "tipofacturacion"; endif;

    if(intval($mes)<10): $mesmod = "0".$mes; else: $mesmod = $mes; endif;
    // Agua, Alcantarrillado y cargo fijo
    $Sql = "(
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                count(cab.*) as cantidad,
                1 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} 
                AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0'
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION
            (
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                count(cab.*) as cantidad,
                2 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo) 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0' AND cab.".$cabfactu." = 0
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION 
            (
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                sum(cab.consumofact) as cantidad,
                3 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo) 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0' AND cab.".$cabfactu." = 0
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION 
            (
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                count(cab.*) as cantidad,
                4 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0' AND cab.".$cabfactu." = 0
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION 
            (
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                sum(cab.consumofact) as cantidad,
                5 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0' AND cab.".$cabfactu." = 0
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION 
            ( 
                SELECT 
                cab.catetar as catetar, 
                tar.nomtar as nomtar, 
                count(cab.*) as cantidad, 
                6 as orden
                FROM facturacion.cabfacturacion cab 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND  cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) <> '' AND cab.nrodocumento > 0
                GROUP BY cab.catetar, tar.nomtar 
                ORDER BY cab.catetar, orden 
            ) 
            ORDER BY catetar, orden";

    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();


    // Construccion de Cabecera
    $cabecera = array();   
    $indice   = 0; 

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['cantidad']);
        else:
            array_push($cabecera[$value['catetar']],$value['cantidad']);            
        endif;

    endforeach;

?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2">Categorias</td>
            <td align="center" colspan="6">Numero</td>
        </tr>
        <tr>
            <td align="center" width="5%">N°</td>
            <td align="center" width="10%">Tarifas</td>
            <td align="center" width="15%">Medidores Instalados</td>
            <td align="center" width="15%">Uni. Uso Med. Leido</td>
            <td align="center" width="15%">Vol. Uni. Uso Con. Leido</td>
            <td align="center" width="15%">Medidor Leido</td>
            <td align="center" width="15%">Volumen Consumo Leido</td>
            <td align="center" width="15%">Conexiones Facturadas con Medidor</td>
        </tr>
    </thead>
    <tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal4 = 0;
        $subtotal5 = 0;
        $subtotal6 = 0;
        $contador  = 0;
        foreach ($cabecera as $key => $value): 
            $contador++;
            // $subtotal4 = $value[2]+$value[3];
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?= number_format($value[1] = empty($value[1]) ? 0 : $value[1],0);?></td>
                <td align="right"><?= number_format($value[2] = empty($value[2]) ? 0 : $value[2],0);?></td>
                <td align="right"><?= number_format($value[3] = empty($value[3]) ? 0 : $value[3],0);?></td>
                <td align="right"><?= number_format($value[4] = empty($value[4]) ? 0 : $value[4],0);?></td>
                <td align="right"><?= number_format($value[5] = empty($value[5]) ? 0 : $value[5],0);?></td>
                <td align="right"><?= number_format($value[6] = empty($value[6]) ? 0 : $value[6],0);?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal4 += $value[4];
            $subtotal5 += $value[5];
            $subtotal6 += $value[6];
            // $subtotal5 += $subtotal4;

            endforeach;?>
        <tr style="font-weight: bold">
            <td colspan="2" align="center" >Total</td>
            <td align="right"><?=number_format($subtotal1,0);?></td>
            <td align="right"><?=number_format($subtotal2,0);?></td>
            <td align="right"><?=number_format($subtotal3,0);?></td>
            <td align="right"><?=number_format($subtotal4,0);?></td>
            <td align="right"><?=number_format($subtotal5,0);?></td>
            <td align="right"><?=number_format($subtotal6,0);?></td>
        </tr>
    </tbody>
</table>
</center> 

