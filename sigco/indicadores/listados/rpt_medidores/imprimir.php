<?php
	include("../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses,$fechalite;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral($fechalite);
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "REPORTE DE NUMERO DE MEDIDORES";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(20);
            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=5;
            $h1= 15;
            $h2= 10;

			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
            $this->SetFont('Arial','B',10);
            $this->Cell($Dim[1],$h,utf8_decode('Categorias'),1,0,'C',true);
            $this->Cell($Dim[2],$h,utf8_decode('Numero'),1,1,'C',true);

            $this->Cell($Dim[3],$h,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[4],$h,utf8_decode('Tarifas'),1,0,'C',true);
            $this->Cell($Dim[5],$h,utf8_decode('Med. Instalados'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Uni. Uso Med. Leido'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Vol. Uni. Uso Con. Leido'),1,0,'C',true);
            $this->Cell($Dim[5],$h,utf8_decode('Medidor Leido'),1,0,'C',true);
            $this->Cell($Dim[5],$h,utf8_decode('Vol. Cons. Leido'),1,0,'C',true);
            $this->Cell($Dim[5],$h,utf8_decode('Cone. Fac. con Med.'),1,1,'C',true);

		}

        function Contenido($cont,$descripcion, $num1, $num2, $num3, $num4, $num5, $num6, $dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',10);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[5],$h,number_format($num1,0),1,0,'R',true);
					$this->Cell($Dim[7],$h,number_format($num2,0),1,0,'R',true);
                    $this->Cell($Dim[6],$h,number_format($num3,0),1,0,'R',true);
                    $this->Cell($Dim[5],$h,number_format($num4,0),1,0,'R',true);
                    $this->Cell($Dim[5],$h,number_format($num5,0),1,0,'R',true);
                    $this->Cell($Dim[5],$h,number_format($num6,0),1,1,'R',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',9);
					$this->Cell($Dim[3],$h,utf8_decode($cont),1,0,'C',true);
					$this->Cell($Dim[4],$h,utf8_decode($descripcion),1,0,'L',true);
					$this->Cell($Dim[5],$h,number_format($num1,0),1,0,'R',true);
					$this->Cell($Dim[7],$h,number_format($num2,0),1,0,'R',true);
                    $this->Cell($Dim[6],$h,number_format($num3,0),1,0,'R',true);
                    $this->Cell($Dim[5],$h,number_format($num4,0),1,0,'R',true);
                    $this->Cell($Dim[5],$h,number_format($num5,0),1,0,'R',true);
                    $this->Cell($Dim[5],$h,number_format($num6,0),1,1,'R',true);
					break;
			}
		}
    }

    $Dim = array('1'=>50,'2'=>240,'3'=>10,'4'=>40,'5'=>35,'6'=>60,'7'=>40);

	// Actual
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = $_GET['mes'];
    $mestexto = $_GET['mestexto'];
    $ciclo    = $_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];
    $fechalite = "01/".$mes."/".$anio;
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();


    $cabfactu = "tipofacturacionfact";
    if($mes == 10 AND $anio == 2014 AND ($codsuc == 2 OR $codsuc == 3) ): $cabfactu = "tipofacturacion"; endif;

    if(intval($mes)<10): $mesmod = "0".$mes; else: $mesmod = $mes; endif;
    
    // Agua, Alcantarrillado y cargo fijo
    $Sql = "(
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                count(cab.*) as cantidad,
                1 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} 
                AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0'
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION
            (
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                count(cab.*) as cantidad,
                2 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo) 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0' AND cab.".$cabfactu." = 0
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION 
            (
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                sum(cab.consumofact) as cantidad,
                3 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo) 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0' AND cab.".$cabfactu." = 0
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION 
            (
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                count(cab.*) as cantidad,
                4 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0' AND cab.".$cabfactu." = 0
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION 
            (
                SELECT cab.catetar as catetar,
                tar.nomtar as nomtar,
                sum(cab.consumofact) as cantidad,
                5 as orden 
                FROM facturacion.cabfacturacion cab
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) > '0' AND cab.".$cabfactu." = 0
                GROUP BY cab.catetar, tar.nomtar
                ORDER BY cab.catetar, orden
            )
            UNION 
            ( 
                SELECT 
                cab.catetar as catetar, 
                tar.nomtar as nomtar, 
                count(cab.*) as cantidad, 
                6 as orden
                FROM facturacion.cabfacturacion cab 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = {$codsuc} AND  cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND trim(cab.nromed) <> '' AND cab.nrodocumento > 0
                GROUP BY cab.catetar, tar.nomtar 
                ORDER BY cab.catetar, orden 
            ) 
            ORDER BY catetar, orden";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();


    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['cantidad']);
        else:
            array_push($cabecera[$value['catetar']],$value['cantidad']);            
        endif;

    endforeach;
	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(5);
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $subtotal3 = 0;
    $subtotal4 = 0;
    $subtotal5 = 0;
    $subtotal6 = 0;
    $contador  = 0;

    foreach ($cabecera as $key => $value): 
        $contador++;
        $subtotal1 += $value[1];
        $subtotal2 += $value[2];
        $subtotal3 += $value[3];
        $subtotal4 += $value[4];
        $subtotal5 += $value[5];
        $subtotal6 += $value[6];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2], $value[3], $value[4], $value[5], $value[6]);
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,$subtotal4,$subtotal5,$subtotal6,1);

	$objReporte->Output();	
		
?>