<?php 

    if(!session_start()){session_start();}


    // Hay que considerar el estado de servicio debemos agregar tambien los diferentes
    // de ese estado

	include("../../../../config.php");

    // Actual
    $codemp = 1;
    $codsuc = $_POST['codsuc'];
    $ciclo  = $_POST['ciclo'];
    $anio   = $_POST['anio'];
    $mes    = $_POST['mes'];

    // Para obtener el periodo de faturacion

    $sqlLect = "select nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    
    // Datos
    $Sql        = " SELECT uni.catetar as catetar, 
                cli.codtiposervicio as tiposervicio, 
                tar.nomtar as descripcion, 
                count(uni.*) as cantidad
                FROM catastro.clientes cli
                INNER JOIN facturacion.cabfacturacion cab ON (cli.codemp = cab.codemp AND cli.codsuc = cab.codsuc AND cli.codciclo = cab.codciclo AND cli.nroinscripcion = cab.nroinscripcion)
                INNER JOIN catastro.unidadesusoclientes uni ON (cli.codemp = uni.codemp AND cli.codsuc = uni.codsuc AND cli.nroinscripcion = uni.nroinscripcion )
                INNER JOIN facturacion.tarifas tar ON (uni.codemp = tar.codemp AND uni.codsuc = tar.codsuc AND uni.catetar = tar.catetar)
                WHERE cli.codsuc = {$codsuc} AND cli.codestadoservicio = 1
                AND cab.anio = '{$anio}' AND cab.mes = '{$mes}'
                AND cab.nrofacturacion = '{$nrofact['nrofacturacion']}'
                GROUP BY uni.catetar, cli.codtiposervicio, tar.nomtar
                ORDER BY uni.catetar, cli.codtiposervicio";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();


    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):            
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['descripcion'],$value['cantidad']);
        else:
            array_push($cabecera[$value['catetar']],$value['cantidad']);            
        endif;

    endforeach;
?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2">Tarifas</td>
            <td align="center" colspan="3" >Numero de Unidades de Uso</td>
            <td align="center" rowspan="2">Total</td>
        </tr>
        <tr>
            <td align="center" width="5%">N°</td>
            <td align="center">Descripcion</td>
            <td align="center" >Agua y Desague</td>
            <td align="center" >Desague</td>
            <td align="center" >Agua</td>
        </tr>
	<tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal4 = 0;
        $contador  = 0;
        foreach ($cabecera as $key => $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?=$value[1];?></td>
                <td align="right"><?= $value[2] = empty($value[2]) ? 0 : $value[2];?></td>
                <td align="right"><?= $value[3] = empty($value[3]) ? 0 : $value[3];?></td>
                <td align="right"><?=($value[1]+$value[2]+$value[3]);?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal4 += $value[1]+$value[2]+$value[3];
            endforeach;?>
		<tr style="font-weight: bold">
            <td colspan="2" >Total</td>
            <td align="right"><?=$subtotal1;?></td>
            <td align="right"><?=$subtotal2;?></td>
            <td align="right"><?=$subtotal3;?></td>
            <td align="right"><?=$subtotal4;?></td>
		</tr>
	</tbody>
</table>
</center> 

