<?php 
    if(!session_start()){session_start();}

    // ini_set("display_errors",1);
    // error_reporting(E_ALL);

	include("../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsNumeroUnidades extends clsReporte
	{
		function Header() {

            global $codsuc,$meses,$fechaactu;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral($fechaactu);
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "REPORTE NUMERO DE UNIDADES DE USO POR CATEGORIA";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(20);
			$this->SetFillColor(18,157,176);
			$this->SetTextColor(255);
			$h=5;
			$h1= 10;

			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			$this->SetFont('Arial','B',10);
			$this->Cell($Dim[1],$h,utf8_decode('Tarifas'),1,0,'C',true);
			$this->Cell($Dim[2],$h,utf8_decode('Numero de Unidades de Uso'),1,0,'C',true);
			$this->Cell($Dim[3],$h1,utf8_decode('Total'),1,1,'C',true);

			$this->SetY($this->GetY()-$h);
            $this->Cell($Dim[4],$h,utf8_decode('N°'),1,0,'C',true);
			$this->Cell($Dim[5],$h,utf8_decode('Descripcion'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Agua y Desague'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Desague'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Agua'),1,1,'C',true);

		}

        function Contenido($cont,$descripcion, $num1, $num2, $num3, $subtotal,$dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',10);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
                    $this->Cell($Dim[6],$h,$num1 = empty($num1) ? 0 : $num1,1,0,'C',true);
                    $this->Cell($Dim[6],$h,$num2 = empty($num2) ? 0 : $num2,1,0,'C',true);
                    $this->Cell($Dim[6],$h,$num3 = empty($num3) ? 0 : $num3,1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($subtotal),1,1,'C',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',8);
					$this->Cell($Dim[4],$h,utf8_decode($cont),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($descripcion),1,0,'L',true);
					$this->Cell($Dim[6],$h,$num1 = empty($num1) ? 0 : $num1,1,0,'C',true);
					$this->Cell($Dim[6],$h,$num2 = empty($num2) ? 0 : $num2,1,0,'C',true);
					$this->Cell($Dim[6],$h,$num3 = empty($num3) ? 0 : $num3,1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($subtotal),1,1,'C',true);
					break;
			}
		}
    }

    $Dim = array('1'=>100,'2'=>120,'3'=>40,'4'=>15,'5'=>85,'6'=>40,'7'=>90);

	// Actual
    $codemp    = 1;
    $codsuc    = $_GET['codsuc'];
    $ciclo     = $_GET['ciclo'];
    $anio      = $_GET['anio'];
    $mes       = $_GET['mes'];
    
    $fechaactu = "01/".$mes."/".$anio;

    // Para obtener el periodo de faturacion

    $sqlLect = "select nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    
    // Datos
    $Sql        = " SELECT uni.catetar as catetar, 
                cli.codtiposervicio as tiposervicio, 
                tar.nomtar as descripcion, 
                count(uni.*) as cantidad
                FROM catastro.clientes cli
                INNER JOIN facturacion.cabfacturacion cab ON (cli.codemp = cab.codemp AND cli.codsuc = cab.codsuc AND cli.codciclo = cab.codciclo AND cli.nroinscripcion = cab.nroinscripcion)
                INNER JOIN catastro.unidadesusoclientes uni ON (cli.codemp = uni.codemp AND cli.codsuc = uni.codsuc AND cli.nroinscripcion = uni.nroinscripcion )
                INNER JOIN facturacion.tarifas tar ON (uni.codemp = tar.codemp AND uni.codsuc = tar.codsuc AND uni.catetar = tar.catetar)
                WHERE cli.codsuc = {$codsuc} AND cli.codestadoservicio = 1
                AND cab.anio = '{$anio}' AND cab.mes = '{$mes}'
                AND cab.nrofacturacion = '{$nrofact['nrofacturacion']}'
                GROUP BY uni.catetar, cli.codtiposervicio, tar.nomtar
                ORDER BY uni.catetar, cli.codtiposervicio";

    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

            if (!array_key_exists($value['catetar'], $cabecera)):            
                $cabecera[$value['catetar']] = array();
                array_push($cabecera[$value['catetar']], $value['descripcion'],$value['cantidad']);
            else:
                array_push($cabecera[$value['catetar']],$value['cantidad']);            
            endif;

    endforeach;
	
	$objReporte = new clsNumeroUnidades('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(25);
    $objReporte->SetAutoPageBreak(true,5);
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $subtotal3 = 0;
    $subtotal4 = 0;
    $contador  = 0;

    foreach ($cabecera as $key => $value): 
        $contador++;
        $subtotal1 += $value[1];
        $subtotal2 += $value[2];
        $subtotal3 += $value[3];
        $subtotal4 += $value[1]+$value[2]+$value[3];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2],$value[3],($value[1]+$value[2]+$value[3]) );
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,$subtotal4,1);

	$objReporte->Output();	
		
?>