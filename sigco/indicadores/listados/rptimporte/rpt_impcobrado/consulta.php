<?php 
    if(!session_start()){session_start();}

    // ini_set("display_errors",1);
    // error_reporting(E_ALL);

	include("../../../../../objetos/clsFunciones.php");
    $clfunciones = new clsFunciones();

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $ciclo    = $_POST['ciclo'];
    $mestexto = $_POST['mestexto'];
    $codsuc   = $_POST['codsuc'];

    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
    $primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    // Conceptos
    $Sql      = " SELECT codconcepto 
                    FROM cobranza.detpagos det
                    INNER JOIN cobranza.cabpagos cab ON (det.codemp = cab.codemp 
                    AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago)
                    WHERE det.nrofacturacion = {$nrofact['nrofacturacion']} 
                    AND det.mes = '{$mes}' AND det.anio = '{$anio}'
                    AND det.codemp = {$codemp} AND cab.codciclo = {$ciclo} 
                    ORDER BY codconcepto";
    $Consulta = $conexion->query($Sql);
    $concep   = $Consulta->fetchAll();

    // Tarifas
    $tar      = " SELECT catetar FROM facturacion.tarifas 
                    WHERE codemp = {$codemp} AND codsuc = {$codsuc} 
                    ORDER BY catetar";
    $Consultat = $conexion->query($tar);
    $tari   = $Consultat->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($concep as $value):

        if (!array_key_exists(($value['codconcepto']), $cabecera)):

            $cabecera[$value['codconcepto']] = array();

            foreach ($tari as $tarifa):

                // Vol. del Mes
                $sql = "SELECT sum(det.importe) as delmes 
                FROM cobranza.detpagos det
                INNER JOIN cobranza.cabpagos cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago)
                INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND det.nrofacturacion = uni.nrofacturacion 
                AND  cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo)  
                WHERE det.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND det.mes = '{$mes}' AND det.anio = '{$anio}'
                AND det.codemp = {$codemp} AND cab.codciclo = {$ciclo}
                AND det.codconcepto = {$value['codconcepto']} 
                AND uni.catetar = {$tarifa['catetar']}";
                $consulta = $conexion->query($sql);
                $valor    = $consulta->fetch();

                array_push($cabecera[$value['codconcepto']], $value['descripcion'],$valor['delmes']);

            endforeach;

            // foreach ($tari as $tarifa1):

            //     // Vol. al Mes
            //     $sql1 = "SELECT sum(det.importe) as almes 
            //         FROM cobranza.detpagos det
            //         INNER JOIN cobranza.cabpagos cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago)
            //         INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND det.nrofacturacion = uni.nrofacturacion AND  cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo)
            //         WHERE cab.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
            //         AND det.codemp = {$codemp} AND cab.codciclo = {$ciclo}
            //         AND det.codconcepto = {$value['codconcepto']} 
            //         AND uni.catetar = {$tarifa1['catetar']}";
            //     $consulta1 = $conexion->query($sql1);
            //     $valor1    = $consulta1->fetch();

            //     if(empty($valor1['almes'])):
            //         $valor1['almes'] = 0;
            //     else:
            //         array_push($cabecera[$value['codconcepto']], $value['descripcion'],$valor1['almes']);
            //     endif;

            // endforeach;

        endif;

    endforeach;

    // var_dump($cabecera);exit;

?>

<center>
<table border="1" aling="center">
    <thead>
        <tr>
            <td align="center" colspan="2">Conceptos</td>
            <td align="center" colspan="5">Imp. Cob. del Mes</td>
            <td align="center" colspan="5">Imp. Cob. en el Mes</td>
        </tr>
        <tr>
            <td align="center" width="3%">N°</td>
            <td align="center">Descripcion</td>
            <td align="center">Domestico</td>
            <td align="center">Comercial</td>
            <td align="center">Industrial</td>
            <td align="center">Estatal</td>
            <td align="center">Social</td>
            <td align="center">Domestico</td>
            <td align="center">Comercial</td>
            <td align="center">Industrial</td>
            <td align="center">Estatal</td>
            <td align="center">Social</td>
            <td align="center">Total</td>
        </tr>
   <!--  -->
</table>
<center>
