<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    setlocale(LC_ALL, 'nl_NL');

    // ini_set("display_errors",1);
    // error_reporting(E_ALL);

	include("../../../../../objetos/clsFunciones.php");
    $clfunciones = new clsFunciones();

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = ( ($_POST['mes']-1) == 0 ) ? 12 : $_POST['mes']-1 ;
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];

    if ($mes == 12):
        $anio -= 1;
    endif;

    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();


    $sqlLect1 = " SELECT nomtar FROM  facturacion.tarifas WHERE codemp = ? AND codsuc = ? ORDER BY catetar;";
    $consultaL1 = $conexion->prepare($sqlLect1);
    $consultaL1->execute(array($codemp,$codsuc));
    $catetares = $consultaL1->fetchAll();

    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($_POST['mes'],$_POST['anio']);
    $primerdia = $clfunciones->obtener_primer_dia_mes($_POST['mes'],$_POST['anio']);

    // var_dump($primerdia);exit;
    $nombremes = $clfunciones->obtener_nombre_mes($mes);

    $cate = "det.codcategoria = 1";

    // if($codsuc == 2 AND $mes == 9):
    //     $cate = "det.codcategoria = 5 AND det.nrofacturacion = {$nrofact['nrofacturacion']}";
    // endif;

    // Tipo de Reclamos o Sub-Categoria
    $Sql    = "(SELECT 
                    det.codconcepto ||''|| cabf.catetar ||''|| 1 as catetar,
                    tar.nomtar as nomtar,
                    sum(det.importe) as importe,
                    1 as orden
                    FROM cobranza.detpagos det
                    INNER JOIN cobranza.cabpagos cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago)
                    INNER JOIN facturacion.cabfacturacion cabf ON (cabf.codemp = det.codemp AND cabf.codsuc = det.codsuc AND cabf.codciclo = cab.codciclo AND cabf.nrofacturacion = det.nrofacturacion AND cabf.nroinscripcion = det.nroinscripcion)
                    INNER JOIN facturacion.tarifas tar ON (cabf.codemp = tar.codemp AND cabf.codsuc = tar.codsuc AND cabf.catetar = tar.catetar)
                    WHERE cab.fechareg between '".$primerdia."' AND '".$ultimodia."' 
                    AND det.codemp = {$codemp} AND cab.codciclo = {$ciclo}
                    AND cab.codsuc = {$codsuc} AND ".$cate."
                    AND det.codconcepto IN(1,2,6) AND cab.nropec=0 AND cab.anulado=0 
                    GROUP BY cabf.catetar,tar.nomtar,det.codconcepto
                    ORDER BY det.codconcepto,cabf.catetar)
                    UNION
                    (
                SELECT 
                    det.codconcepto ||''|| cabf.catetar ||''|| 2 as catetar,
                    tar.nomtar as nomtar,
                    sum(det.importe) as importe, 
                    2 as orden
                    FROM cobranza.detpagos det
                    INNER JOIN cobranza.cabpagos cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago)
                    INNER JOIN facturacion.cabfacturacion cabf ON (cabf.codemp = det.codemp AND cabf.codsuc = det.codsuc AND cabf.codciclo = cab.codciclo AND cabf.nrofacturacion = det.nrofacturacion AND cabf.nroinscripcion = det.nroinscripcion)
                    INNER JOIN facturacion.tarifas tar ON (cabf.codemp = tar.codemp AND cabf.codsuc = tar.codsuc AND cabf.catetar = tar.catetar)
                    WHERE cab.fechareg between '".$primerdia."' AND '".$ultimodia."'
                    AND det.codemp = {$codemp} AND cab.codsuc = {$codsuc}
                    AND cab.codciclo = {$ciclo} AND det.codcategoria = 5
                    AND det.codconcepto IN (1,2,6) AND cab.nropec=0 AND cab.anulado=0 
                    GROUP BY cabf.catetar,tar.nomtar,det.codconcepto
                    ORDER BY det.codconcepto,cabf.catetar)
                    ORDER BY catetar, orden";
    $Consulta  = $conexion->query($Sql);
    $row       = $Consulta->fetchAll();

    $h         = 1;
    $cabecera  = array();  
    $cabcuerpo = array();  

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['importe']);
        else:
            array_push($cabecera[$value['catetar']],$value['importe']);            
        endif;

    endforeach;

    // Construccion de Cabecera

    $cabe = array(1,2,6);

    foreach ($cabe as $rol):

        for ($g=1; $g <=5; $g++):
            
            for ($h=1; $h <=2; $h++):

                $indice = $rol.$g.$h;

                if (array_key_exists($indice, $cabecera)):
                    $nombre = $cabecera[$indice][0];
                    $import = $cabecera[$indice][1];
                else:
                    $nombre = $catetares[($g-1)]['nomtar'];
                    $import = 0;
                endif;

                if (!array_key_exists($g, $cabcuerpo)):
                    $cabcuerpo[$g] = array();
                    array_push($cabcuerpo[$g], $nombre,$import);
                else:
                    array_push($cabcuerpo[$g],$import);            
                endif;

            endfor;

        endfor;
    
    endforeach;


?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2">Categorias</td>
            <td align="center" colspan="6">Recaudacion en el Mes</td>
            <td align="center" colspan="2">Total</td>
        </tr>
        <tr>
            <td align="center" width="5%" rowspan="2">N°</td>
            <td align="center" width="5%" rowspan="2">Tarifas</td>
            <td align="center" colspan="2">Agua</td>
            <td align="center" colspan="2">Alcantarillado</td>
            <td align="center" colspan="2">Cargo Fijo</td>
            <td align="center" rowspan="2">Del Mes</td>
            <td align="center" rowspan="2">Mes. Ant.</td>
        </tr>
        <tr>
            <td align="center"><?=substr($nombremes,0,3)."(Mes)";?></td>
            <td align="center">Mes Anterior</td>
            <td align="center"><?=substr($nombremes,0,3)."(Mes)";?></td>
            <td align="center">Mes Anterior</td>
            <td align="center"><?=substr($nombremes,0,3)."(Mes)";?></td>
            <td align="center">Mes Anterior</td>
        </tr>
	<thead>  
    <tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal4 = 0;
        $subtotal5 = 0;
        $subtotal6 = 0;
        $subtotal7 = 0;
        $subtotal8 = 0;
        $contador  = 0;
        foreach ($cabcuerpo as $key => $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?=number_format($value[1] = empty($value[1]) ? 0 : $value[1],2);?></td>
                <td align="right"><?=number_format($value[2] = empty($value[2]) ? 0 : $value[2],2);?></td>
                <td align="right"><?=number_format($value[3] = empty($value[3]) ? 0 : $value[3],2);?></td>
                <td align="right"><?=number_format($value[4] = empty($value[4]) ? 0 : $value[4],2);?></td>
                <td align="right"><?=number_format($value[5] = empty($value[5]) ? 0 : $value[5],2);?></td>
                <td align="right"><?=number_format($value[6] = empty($value[6]) ? 0 : $value[6],2);?></td>
                <td align="right"><?=number_format(($value[1]+$value[3]+$value[5]),2);?></td>
                <td align="right"><?=number_format(($value[2]+$value[4]+$value[6]),2);?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal4 += $value[4];
            $subtotal5 += $value[5];
            $subtotal6 += $value[6];
            $subtotal7 += $value[1]+$value[3]+$value[5];
            $subtotal8 += $value[2]+$value[4]+$value[6];
            endforeach;?>
        <tr style="font-weight: bold">
            <td colspan="2" align="center">Total</td>
            <td align="right"><?=number_format($subtotal1,2);?></td>
            <td align="right"><?=number_format($subtotal2,2);?></td>
            <td align="right"><?=number_format($subtotal3,2);?></td>
            <td align="right"><?=number_format($subtotal4,2);?></td>
            <td align="right"><?=number_format($subtotal5,2);?></td>
            <td align="right"><?=number_format($subtotal6,2);?></td>
            <td align="right"><?=number_format($subtotal7,2);?></td>
            <td align="right"><?=number_format($subtotal8,2);?></td>
        </tr>
    </tbody>  
</table>
</center> 

