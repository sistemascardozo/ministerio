<?php
	include("../../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc, $meses, $fechaact;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral($fechaact);
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "IMPORTE COBRADO DE AGUA, ALCANTARILLADO Y CARGO FIJO";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
            
            global $Dim,$nombremes;

			$this->Ln(20);
            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=5;
            $h1= 15;
            $h2= 10;
            $h3= 20;

            //$this->SetWidths(array(25,15,40,65,10,23,30,45));
            //$this->SetWidths(array(25,15,40,65,10,23,30,45));
            $this->SetFont('Arial','B',10);
            $this->Cell($Dim[1],$h2,utf8_decode('Categorias'),1,0,'C',true);
            $this->Cell($Dim[2],$h2,utf8_decode('Recaudacion en el Mes'),1,0,'C',true);
            $this->Cell($Dim[8],$h1,utf8_decode('Total'),1,1,'C',true);

            $this->SetY($this->GetY()-$h);
            $this->Cell($Dim[4],$h2,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[5],$h2,utf8_decode('Tarifas'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Agua'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Alcantarillado'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Cargo Fijo'),1,1,'C',true);

            $this->SetX($this->GetX()+50);
            $this->SetFont('Arial','B',7);
            $this->Cell($Dim[3],$h,utf8_decode(substr($nombremes,0,3)."(Mes)"),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Mes Anterior'),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode(substr($nombremes,0,3)."(Mes)"),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Mes Anterior'),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode(substr($nombremes,0,3)."(Mes)"),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Mes Anterior'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Del Mes'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Mes Anterior'),1,1,'C',true);
		}

        function Contenido($cont,$descripcion, $num1, $num2, $num3, $num4, $num5, $num6, $sub, $sub1,$dat = 0) {
			
            global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',10);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($num1,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($num2,2),1,0,'C',true);
                    $this->Cell($Dim[3],$h,number_format($num3,2),1,0,'C',true);
                    $this->Cell($Dim[3],$h,number_format($num4,2),1,0,'C',true);
                    $this->Cell($Dim[3],$h,number_format($num5,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($num6,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($sub,2),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($sub1,2),1,1,'C',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',9);
					$this->Cell($Dim[4],$h,$cont,1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($descripcion),1,0,'L',true);
					$this->Cell($Dim[3],$h,number_format($num1,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($num2,2),1,0,'C',true);
                    $this->Cell($Dim[3],$h,number_format($num3,2),1,0,'C',true);
                    $this->Cell($Dim[3],$h,number_format($num4,2),1,0,'C',true);
                    $this->Cell($Dim[3],$h,number_format($num5,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($num6,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($sub,2),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($sub1,2),1,1,'C',true);
					break;
			}
		}
    }

    $Dim = array('1'=>50,'2'=>180,'3'=>30,'4'=>10,'5'=>40,'6'=>60,'7'=>25,'8'=>50);
	
    // Actual
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = ( ($_GET['mes']-1) == 0 ) ? 12 : $_GET['mes']-1 ;
    $mestexto = $_GET['mestexto'];
    $ciclo    = $_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];

    if ($mes == 12):
        $anio -= 1;
    endif;

    $fechaact = '01/'.$_GET['mes']."/".$_GET['anio'];
    $nombremes = $clfunciones->obtener_nombre_mes($mes);



    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    $sqlLect1 = " SELECT nomtar FROM  facturacion.tarifas WHERE codemp = ? AND codsuc = ? ORDER BY catetar;";
    $consultaL1 = $conexion->prepare($sqlLect1);
    $consultaL1->execute(array($codemp,$codsuc));
    $catetares = $consultaL1->fetchAll();

    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($_GET['mes'],$_GET['anio']);
    $primerdia = $clfunciones->obtener_primer_dia_mes($_GET['mes'],$_GET['anio']);
    
    // Tipo de Reclamos o Sub-Categoria
    $Sql    = "(SELECT 
                    det.codconcepto ||''|| cabf.catetar ||''|| 1 as catetar,
                    tar.nomtar as nomtar,
                    sum(det.importe) as importe,
                    1 as orden
                    FROM cobranza.detpagos det
                    INNER JOIN cobranza.cabpagos cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago)
                    INNER JOIN facturacion.cabfacturacion cabf ON (cabf.codemp = det.codemp AND cabf.codsuc = det.codsuc AND cabf.codciclo = cab.codciclo AND cabf.nrofacturacion = det.nrofacturacion AND cabf.nroinscripcion = det.nroinscripcion)
                    INNER JOIN facturacion.tarifas tar ON (cabf.codemp = tar.codemp AND cabf.codsuc = tar.codsuc AND cabf.catetar = tar.catetar)
                    WHERE cab.fechareg between '".$primerdia."' AND '".$ultimodia."' 
                    AND det.codemp = {$codemp} AND cab.codciclo = {$ciclo}
                    AND cab.codsuc = {$codsuc} AND det.codcategoria = 1
                    AND det.codconcepto IN(1,2,6) AND cab.nropec=0 AND cab.anulado=0 
                    GROUP BY cabf.catetar,tar.nomtar,det.codconcepto
                    ORDER BY det.codconcepto,cabf.catetar)
                    UNION
                    (
                SELECT 
                    det.codconcepto ||''|| cabf.catetar ||''|| 2 as catetar,
                    tar.nomtar as nomtar,
                    sum(det.importe) as importe, 
                    2 as orden
                    FROM cobranza.detpagos det
                    INNER JOIN cobranza.cabpagos cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago)
                    INNER JOIN facturacion.cabfacturacion cabf ON (cabf.codemp = det.codemp AND cabf.codsuc = det.codsuc AND cabf.codciclo = cab.codciclo AND cabf.nrofacturacion = det.nrofacturacion AND cabf.nroinscripcion = det.nroinscripcion)
                    INNER JOIN facturacion.tarifas tar ON (cabf.codemp = tar.codemp AND cabf.codsuc = tar.codsuc AND cabf.catetar = tar.catetar)
                    WHERE cab.fechareg between '".$primerdia."' AND '".$ultimodia."'
                    AND det.codemp = {$codemp} AND cab.codsuc = {$codsuc}
                    AND cab.codciclo = {$ciclo} AND det.codcategoria = 5
                    AND det.codconcepto IN (1,2,6) AND cab.nropec=0 AND cab.anulado=0 
                    GROUP BY cabf.catetar,tar.nomtar,det.codconcepto
                    ORDER BY det.codconcepto,cabf.catetar)
                    ORDER BY catetar, orden";

    $Consulta = $conexion->query($Sql);
    $row      = $Consulta->fetchAll();
    $h         = 1;
    $cabecera  = array();  
    $cabcuerpo = array();  

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['importe']);
        else:
            array_push($cabecera[$value['catetar']],$value['importe']);            
        endif;

    endforeach;

    $cabe = array(1,2,6);

    foreach ($cabe as $rol):

        for ($g=1; $g <=5; $g++):
            
            for ($h=1; $h <=2; $h++):

                $indice = $rol.$g.$h;

                if (array_key_exists($indice, $cabecera)):
                    $nombre = $cabecera[$indice][0];
                    $import = $cabecera[$indice][1];
                else:
                    $nombre = $catetares[($g-1)]['nomtar'];
                    $import = 0;
                endif;

                if (!array_key_exists($g, $cabcuerpo)):
                    $cabcuerpo[$g] = array();
                    array_push($cabcuerpo[$g], $nombre,$import);
                else:
                    array_push($cabcuerpo[$g],$import);            
                endif;

            endfor;

        endfor;
    
    endforeach;
   
	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $subtotal3 = 0;
    $subtotal4 = 0;
    $subtotal5 = 0;
    $subtotal6 = 0;
    $subtotal7 = 0;
    $subtotal8 = 0;
    $contador  = 0;

    foreach ($cabcuerpo as $key => $value): 
        $contador++;
        $subtotal1 += $value[1];
        $subtotal2 += $value[2];
        $subtotal3 += $value[3];
        $subtotal4 += $value[4];
        $subtotal5 += $value[5];
        $subtotal6 += $value[6];
        $subtotal7 += $value[1]+$value[3]+$value[5];
        $subtotal8 += $value[2]+$value[4]+$value[6];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2], $value[3], $value[4], $value[5], $value[6],($value[1]+$value[3]+$value[5]),($value[2]+$value[4]+$value[6]));
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,$subtotal4,$subtotal5,$subtotal6,$subtotal7,$subtotal8,1);

	$objReporte->Output();	
		
?>