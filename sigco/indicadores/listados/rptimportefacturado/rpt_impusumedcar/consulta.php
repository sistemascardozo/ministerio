<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../objetos/clsFunciones.php");
    $clfunciones = new clsFunciones();

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
    $primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);

    // Cargo fijo
    $Sql = "(
            SELECT cab.catetar as catetar,
            tar.nomtar, 
            sum(det.importe) as importe
            FROM facturacion.cabfacturacion cab
            INNER JOIN facturacion.detfacturacion det ON (cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.nrofacturacion = det.nrofacturacion AND cab.nroinscripcion = det.nroinscripcion AND cab.codciclo = det.codciclo)
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE cab.codsuc = {$codsuc} AND cab.codciclo = {$ciclo} AND det.nrofacturacion = {$nrofact['nrofacturacion']} 
            AND cab.mes ='{$mes}' AND cab.anio = '{$anio}'
            AND cab.nromed <> trim('') AND
            det.codconcepto IN (6)
            GROUP BY cab.catetar,tar.nomtar
            ORDER BY cab.catetar
            )
            UNION
            (
            SELECT cab.catetar as catetar,
            tar.nomtar, 
            sum(det.importe) as importe
            FROM facturacion.cabfacturacion cab
            INNER JOIN facturacion.detfacturacion det ON (cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.nrofacturacion = det.nrofacturacion AND cab.nroinscripcion = det.nroinscripcion AND cab.codciclo = det.codciclo)
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE cab.codsuc = {$codsuc} AND cab.codciclo = {$ciclo} AND cab.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
            AND cab.nromed <> trim('') AND
            det.codconcepto IN (6)
            GROUP BY cab.catetar,tar.nomtar
            ORDER BY cab.catetar
            )
            ORDER BY catetar";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();   
    $indice   = 0; 

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['importe']);
        else:
            array_push($cabecera[$value['catetar']],$value['importe']);            
        endif;

    endforeach;

?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2">Categorias</td>
            <td align="center" colspan="2">Cargo Fijo</td>
            <td align="center" rowspan="2">Total</td>
        </tr>
        <tr>
            <td align="center" width="5%">N°</td>
            <td align="center">Tarifas</td>
            <td align="center">Fact. Actual</td>
            <td align="center">Fact. Acumulada</td>
        </tr>
    </thead>
    <tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $contador  = 0;
        foreach ($cabecera as $key => $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=  strtoupper($value[0]);?></td>
                <td align="right"><?= number_format($value[1] = empty($value[1]) ? 0 : $value[1],2);?></td>
                <td align="right"><?= number_format($value[2] = empty($value[2]) ? 0 : $value[2],2);?></td>
                <td align="right"><?= number_format($value[1]+$value[2],2);?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[1]+$value[2]+$value[3];
            endforeach;?>
        <tr style="font-weight: bold">
            <td colspan="2" align="center" >Total</td>
            <td align="right"><?=number_format($subtotal1,2);?></td>
            <td align="right"><?=number_format($subtotal2,2);?></td>
            <td align="right"><?=number_format($subtotal3,2);?></td>
        </tr>
    </tbody>
</table>
</center> 

