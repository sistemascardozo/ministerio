<?php
	include("../../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral();
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RESUMEN DE IMPORTE FACTURADO A USUARIOS CON MEDIDOR";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(20);
            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=5;
            $h1= 10;

			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
            $this->SetFont('Arial','B',10);
            $this->Cell($Dim[1],$h,utf8_decode('Categorias'),1,0,'C',true);
            $this->Cell($Dim[2],$h,utf8_decode('Cargo Fijo'),1,0,'C',true);
            $this->Cell($Dim[3],$h1,utf8_decode('Total'),1,1,'C',true);

            $this->SetY($this->GetY()-$h);
            $this->Cell($Dim[4],$h,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[5],$h,utf8_decode('Descripcion'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Fact. Actual'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Fact. Acumulada'),1,1,'C',true);

		}

        function Contenido($cont,$descripcion, $num1, $num2, $subtotal,$dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',9);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[6],$h,number_format($num1,2),1,0,'C',true);
					$this->Cell($Dim[6],$h,number_format($num2,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($subtotal,2),1,1,'C',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',7);
					$this->Cell($Dim[4],$h,utf8_decode($cont),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[6],$h,number_format($num1,2),1,0,'C',true);
					$this->Cell($Dim[6],$h,number_format($num2,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($subtotal,2),1,1,'C',true);
					break;
			}

		}
    }

    $Dim = array('1'=>70,'2'=>120,'3'=>20,'4'=>10,'5'=>60,'6'=>60,'7'=>30);

	// Actual
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = $_GET['mes'];
    $mestexto = $_GET['mestexto'];
    $ciclo    = $_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
    $primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);
    
    // cargo fijo
        $Sql = "(
            SELECT cab.catetar as catetar,
            tar.nomtar, 
            sum(det.importe) as importe
            FROM facturacion.cabfacturacion cab
            INNER JOIN facturacion.detfacturacion det ON (cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.nrofacturacion = det.nrofacturacion AND cab.nroinscripcion = det.nroinscripcion AND cab.codciclo = det.codciclo)
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE cab.codsuc = {$codsuc} AND cab.codciclo = {$ciclo} AND det.nrofacturacion = {$nrofact['nrofacturacion']} 
            AND cab.mes ='{$mes}' AND cab.anio = '{$anio}'
            AND cab.nromed <> trim('') AND
            det.codconcepto IN (6)
            GROUP BY cab.catetar,tar.nomtar
            ORDER BY cab.catetar
            )
            UNION
            (
            SELECT cab.catetar as catetar,
            tar.nomtar, 
            sum(det.importe) as importe
            FROM facturacion.cabfacturacion cab
            INNER JOIN facturacion.detfacturacion det ON (cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.nrofacturacion = det.nrofacturacion AND cab.nroinscripcion = det.nroinscripcion AND cab.codciclo = det.codciclo)
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE cab.codsuc = {$codsuc} AND cab.codciclo = {$ciclo} AND cab.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
            AND cab.nromed <> trim('') AND
            det.codconcepto IN (6)
            GROUP BY cab.catetar,tar.nomtar
            ORDER BY cab.catetar
            )
            ORDER BY catetar";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();   
    $indice   = 0; 

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['importe']);
        else:
            array_push($cabecera[$value['catetar']],$value['importe']);            
        endif;

    endforeach;
	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(35);
    $objReporte->SetAutoPageBreak(true,5);
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $subtotal3 = 0;
    $contador  = 0;

    foreach ($cabecera as $key => $value): 
        $contador++;
        $subtotal1 += $value[1];
        $subtotal2 += $value[2];
        $subtotal3 += $value[1]+$value[2];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2],($value[1]+$value[2]));
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,1);

	$objReporte->Output();	
		
?>