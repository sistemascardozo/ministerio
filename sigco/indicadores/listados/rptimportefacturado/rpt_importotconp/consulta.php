<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../objetos/clsFunciones.php");
    $clfunciones = new clsFunciones();

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $periodo  = $anio.$mes;
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    $Sql = "(
            SELECT det.codconcepto as codconcepto,
            cab.catetar as catetar,
            co.descripcion as descripcion,
            sum(det.importe) as importe
            FROM facturacion.cabctacorriente cab
            INNER JOIN facturacion.detctacorriente det ON (cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.codciclo = det.codciclo AND cab.nrofacturacion = det.nrofacturacion AND cab.nroinscripcion = det.nroinscripcion AND cab.anio = det.anio AND cab.mes = det.mes AND cab.periodo = det.periodo AND cab.tipo = det.tipo AND cab.tipoestructura = det.tipoestructura)
            INNER JOIN facturacion.conceptos co ON (det.codemp = co.codemp AND det.codsuc = co.codsuc AND det.codconcepto = co.codconcepto)
            WHERE det.periodo = '{$periodo}' AND det.codsuc = {$codsuc} 
            AND det.codconcepto NOT IN (1,2,6)
            AND (det.codtipodeuda <> 3 
            AND det.codtipodeuda <> 4 AND det.codtipodeuda <> 9)
            AND det.categoria = 0
            GROUP BY det.codconcepto,cab.catetar,co.descripcion
            ORDER BY det.codconcepto,cab.catetar
            )
            UNION
            (
            ---- Facturacion por concepto por categoria
            SELECT det.codconcepto as codconcepto,
            cab.catetar as catetar,
            co.descripcion as descripcion,
            sum(det.importe) as importe
            FROM facturacion.cabctacorriente cab
            INNER JOIN facturacion.detctacorriente det ON (cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.codciclo = det.codciclo AND cab.nrofacturacion = det.nrofacturacion AND cab.nroinscripcion = det.nroinscripcion AND cab.anio = det.anio AND cab.mes = det.mes AND cab.periodo = det.periodo AND cab.tipo = det.tipo AND cab.tipoestructura = det.tipoestructura)
            INNER JOIN facturacion.conceptos co ON (det.codemp = co.codemp AND det.codsuc = co.codsuc AND det.codconcepto = co.codconcepto)
            WHERE det.categoria = 1 
            AND det.estadofacturacion = 1 AND det.codsuc = {$codsuc}
            AND det.codconcepto NOT IN (1,2,6) AND (det.codtipodeuda <> 3 AND det.codtipodeuda <> 4 AND det.codtipodeuda <> 9)
            GROUP BY det.codconcepto,cab.catetar,co.descripcion
            ORDER BY det.codconcepto,cab.catetar
            )
            ORDER BY codconcepto,catetar";

    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();   
    $indice   = 0; 

    foreach ($row as $value):

        if (!array_key_exists($value['codconcepto'], $cabecera)):
            $cabecera[$value['codconcepto']] = array();
            array_push($cabecera[$value['codconcepto']], $value['descripcion'],$value['importe']);
        else:
            array_push($cabecera[$value['codconcepto']],$value['importe']);            
        endif;

    endforeach;
?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2" rowspan="2">Conceptos</td>
            <td align="center" colspan="10">Tarifas</td>
            <td align="center" rowspan="3">Total</td>
        </tr>
        <tr>
            <td align="center" colspan="2">Domestico</td>
            <td align="center" colspan="2">Comercial</td>
            <td align="center" colspan="2">Indutrial</td>
            <td align="center" colspan="2">Estatal</td>
            <td align="center" colspan="2">Social</td>
        </tr>
        <tr>
            <td align="center" width="5%">N°</td>
            <td align="center">Descripcion</td>
            <td>Fact. Actual</td>
            <td>Fact. Deuda</td>
            <td>Fact. Actual</td>
            <td>Fact. Deuda</td>
            <td>Fact. Actual</td>
            <td>Fact. Deuda</td>
            <td>Fact. Actual</td>
            <td>Fact. Deuda</td>
            <td>Fact. Actual</td>
            <td>Fact. Deuda</td>
        </tr>
    </thead>
    <tbody>
        <?php 
        $subtotal1  = 0;
        $subtotal2  = 0;
        $subtotal3  = 0;
        $subtotal4  = 0;
        $subtotal5  = 0;
        $subtotal6  = 0;
        $subtotal7  = 0;
        $subtotal8  = 0;
        $subtotal9  = 0;
        $subtotal10 = 0;
        $subtotal11 = 0;
        $contador   = 0;
        foreach ($cabecera as $key => $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?=number_format($value[1] = empty($value[1]) ? 0 : $value[1],2);?></td>
                <td align="right"><?=number_format($value[2] = empty($value[2]) ? 0 : $value[2],2);?></td>
                <td align="right"><?=number_format($value[3] = empty($value[3]) ? 0 : $value[3],2);?></td>
                <td align="right"><?=number_format($value[4] = empty($value[4]) ? 0 : $value[4],2);?></td>
                <td align="right"><?=number_format($value[5] = empty($value[5]) ? 0 : $value[5],2);?></td>
                <td align="right"><?=number_format($value[6] = empty($value[6]) ? 0 : $value[6],2);?></td>
                <td align="right"><?=number_format($value[7] = empty($value[7]) ? 0 : $value[7],2);?></td>
                <td align="right"><?=number_format($value[8] = empty($value[8]) ? 0 : $value[8],2);?></td>
                <td align="right"><?=number_format($value[9] = empty($value[9]) ? 0 : $value[9],2);?></td>
                <td align="right"><?=number_format($value[10] = empty($value[10]) ? 0 : $value[10],2);?></td>
                <td align="right"><?=($value[1]+$value[2]+$value[3]+$value[4]+$value[5]+$value[6]+$value[7]+$value[8]+$value[9]+$value[10]);?></td>
            </tr>    
        <?php 
            $subtotal1  += $value[1];
            $subtotal2  += $value[2];
            $subtotal3  += $value[3];
            $subtotal4  += $value[4];
            $subtotal5  += $value[5];
            $subtotal6  += $value[6];
            $subtotal7  += $value[7];
            $subtotal8  += $value[8];
            $subtotal9  += $value[9];
            $subtotal10 += $value[10];
            $subtotal11 += $value[1]+$value[2]+$value[3]+$value[4]+$value[5]+$value[6]+$value[7]+$value[8]+$value[9]+$value[10];
            endforeach;?>
        <tr style="font-weight: bold">
            <td colspan="2" align="center" >Total</td>
            <td align="right"><?=$subtotal1;?></td>
            <td align="right"><?=$subtotal2;?></td>
            <td align="right"><?=$subtotal3;?></td>
            <td align="right"><?=$subtotal4;?></td>
            <td align="right"><?=$subtotal5;?></td>
            <td align="right"><?=$subtotal6;?></td>
            <td align="right"><?=$subtotal7;?></td>
            <td align="right"><?=$subtotal8;?></td>
            <td align="right"><?=$subtotal9;?></td>
            <td align="right"><?=$subtotal10;?></td>
            <td align="right"><?=$subtotal11;?></td>
        </tr>
    </tbody>
</table>
</center> 

