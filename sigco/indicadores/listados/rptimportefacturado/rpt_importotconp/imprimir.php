<?php
	include("../../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral();
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RESUMEN DE IMPORTE FACTURADO POR CONCEPTOS";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(20);
            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=5;
            $h1= 10;
            $h2= 15;

			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
            $this->SetFont('Arial','B',10);
            $this->Cell($Dim[1],$h,utf8_decode('Conceptos'),1,0,'C',true);
            $this->Cell($Dim[2],$h,utf8_decode('Tarifas'),1,0,'C',true);
            $this->Cell($Dim[3],$h2,utf8_decode('Total'),1,1,'C',true);

            $this->SetY($this->GetY()-$h1);
            $this->Cell($Dim[4],$h1,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[5],$h1,utf8_decode('Descripcion'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Domestico'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Comercial'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Indutrial'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Estatal'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Social'),1,1,'C',true);

            $this->SetX($this->GetX()+60);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Act.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Deu.'),1,0,'C',true);

            $this->Cell($Dim[7],$h,utf8_decode('Fact. Act.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Deu.'),1,0,'C',true);
            
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Act.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Deu.'),1,0,'C',true);
            
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Act.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Deu.'),1,0,'C',true);
            
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Act.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Deu.'),1,1,'C',true);

		}

        function Contenido($cont,$descripcion, $num1, $num2, $num3, $num4, $num5, $num6, $num7, $num8, $num9, $num10, $subtotal,$dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',9);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($num1 = empty($num1) ? 0 : $num1,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num2 = empty($num2) ? 0 : $num2,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num3 = empty($num3) ? 0 : $num3,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num4 = empty($num4) ? 0 : $num4,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num5 = empty($num5) ? 0 : $num5,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num6 = empty($num6) ? 0 : $num6,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num7 = empty($num7) ? 0 : $num7,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num8 = empty($num8) ? 0 : $num8,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num9 = empty($num9) ? 0 : $num9,2),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($num10 = empty($num10) ? 0 : $num10,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($subtotal,2),1,1,'C',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',7);
					$this->Cell($Dim[4],$h,utf8_decode($cont),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($descripcion),1,0,'L',true);
					$this->Cell($Dim[7],$h,number_format($num1 = empty($num1) ? 0 : $num1,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num2 = empty($num2) ? 0 : $num2,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num3 = empty($num3) ? 0 : $num3,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num4 = empty($num4) ? 0 : $num4,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num5 = empty($num5) ? 0 : $num5,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num6 = empty($num6) ? 0 : $num6,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num7 = empty($num7) ? 0 : $num7,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num8 = empty($num8) ? 0 : $num8,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num9 = empty($num9) ? 0 : $num9,2),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($num10 = empty($num10) ? 0 : $num10,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($subtotal,2),1,1,'C',true);
					break;
			}
		}
    }

    $Dim = array('1'=>60,'2'=>200,'3'=>30,'4'=>10,'5'=>50,'6'=>40,'7'=>20);

	// Actual
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = $_GET['mes'];
    $mestexto = $_GET['mestexto'];
    $periodo  = $anio.$mes;
    $ciclo    = $_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    
    // cargo fijo
    $Sql = "(
            SELECT det.codconcepto as codconcepto,
            cab.catetar as catetar,
            co.descripcion as descripcion,
            sum(det.importe) as importe
            FROM facturacion.cabctacorriente cab
            INNER JOIN facturacion.detctacorriente det ON (cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.codciclo = det.codciclo AND cab.nrofacturacion = det.nrofacturacion AND cab.nroinscripcion = det.nroinscripcion AND cab.anio = det.anio AND cab.mes = det.mes AND cab.periodo = det.periodo AND cab.tipo = det.tipo AND cab.tipoestructura = det.tipoestructura)
            INNER JOIN facturacion.conceptos co ON (det.codemp = co.codemp AND det.codsuc = co.codsuc AND det.codconcepto = co.codconcepto)
            WHERE det.periodo = '{$periodo}' AND det.codsuc = {$codsuc} 
            AND det.codconcepto NOT IN (1,2,6)
            AND (det.codtipodeuda <> 3 
            AND det.codtipodeuda <> 4 AND det.codtipodeuda <> 9)
            AND det.categoria = 0
            GROUP BY det.codconcepto,cab.catetar,co.descripcion
            ORDER BY det.codconcepto,cab.catetar
            )
            UNION
            (
            ---- Facturacion por concepto por categoria
            SELECT det.codconcepto as codconcepto,
            cab.catetar as catetar,
            co.descripcion as descripcion,
            sum(det.importe) as importe
            FROM facturacion.cabctacorriente cab
            INNER JOIN facturacion.detctacorriente det ON (cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.codciclo = det.codciclo AND cab.nrofacturacion = det.nrofacturacion AND cab.nroinscripcion = det.nroinscripcion AND cab.anio = det.anio AND cab.mes = det.mes AND cab.periodo = det.periodo AND cab.tipo = det.tipo AND cab.tipoestructura = det.tipoestructura)
            INNER JOIN facturacion.conceptos co ON (det.codemp = co.codemp AND det.codsuc = co.codsuc AND det.codconcepto = co.codconcepto)
            WHERE det.categoria = 1 
            AND det.estadofacturacion = 1 AND det.codsuc = {$codsuc}
            AND det.codconcepto NOT IN (1,2,6) AND (det.codtipodeuda <> 3 AND det.codtipodeuda <> 4 AND det.codtipodeuda <> 9)
            GROUP BY det.codconcepto,cab.catetar,co.descripcion
            ORDER BY det.codconcepto,cab.catetar
            )
            ORDER BY codconcepto,catetar";

    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();   
    $indice   = 0; 

    foreach ($row as $value):

        if (!array_key_exists($value['codconcepto'], $cabecera)):
            $cabecera[$value['codconcepto']] = array();
            array_push($cabecera[$value['codconcepto']], $value['descripcion'],$value['importe']);
        else:
            array_push($cabecera[$value['codconcepto']],$value['importe']);            
        endif;

    endforeach;
	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(5);
    $objReporte->SetAutoPageBreak(true,10);
	$objReporte->AddPage('H');

    $subtotal1  = 0;
    $subtotal2  = 0;
    $subtotal3  = 0;
    $subtotal4  = 0;
    $subtotal5  = 0;
    $subtotal6  = 0;
    $subtotal7  = 0;
    $subtotal8  = 0;
    $subtotal9  = 0;
    $subtotal10 = 0;
    $subtotal11 = 0;
    $contador   = 0;

    foreach ($cabecera as $key => $value): 
        $contador++;
        $subtotal1  += $value[1];
        $subtotal2  += $value[2];
        $subtotal3  += $value[3];
        $subtotal4  += $value[4];
        $subtotal5  += $value[5];
        $subtotal6  += $value[6];
        $subtotal7  += $value[7];
        $subtotal8  += $value[8];
        $subtotal9  += $value[9];
        $subtotal10 += $value[10];
        $subtotal11 += $value[1]+$value[2]+$value[3]+$value[4]+$value[5]+$value[6]+$value[7]+$value[8]+$value[9]+$value[10];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2],$value[3],$value[4],$value[5],$value[6],$value[7],$value[8],$value[9],$value[10],($value[1]+$value[2]+$value[3]+$value[4]+$value[5]+$value[6]+$value[7]+$value[8]+$value[9]+$value[10]));
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,$subtotal4,$subtotal5,$subtotal6,$subtotal7,$subtotal8,$subtotal9,$subtotal10,$subtotal11,1);

	$objReporte->Output();	
		
?>