<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");

	$TituloVentana = "Rpt. Imp. Fact. de Agua, Alcantarillado y Cargo Fijo";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	$sucursal = $objDrop->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_volumen.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>/js/graficos/highcharts.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>/js/graficos/drilldown.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>/js/graficos/exporting.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>/js/graficos/temas/sand-signika.js" language="JavaScript"></script>
<script>
jQuery(function($)
	{ 
		
		$( "#DivTipos" ).buttonset();
		$("#consumo_ini").val(0);
		$("#consumo_fin").val(0);

	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc = <?=$codsuc?>;

	function ValidarForm(obj){
		var rutas    = ""
		var sectores = ""

		if($("#ciclo").val()==0)
		{
			alert('Seleccione el Ciclo')
			return false
		}
		if($("#anio").val()==0)
		{
			alert('Seleccione el Anio a Consultar')
			return false
		}
		if($("#mes").val()==0)
		{
			alert('Seleccione el Mes a Consultar')
			return false
		}

		var texto = $("#mes option:selected").html();

		if(obj.id == 'rabconsultar')
		{
			$.ajax({
			 url:'consulta.php',
			 type:'POST',
			 async:true,
			 data:"anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>",
			 success:function(datos){
				$("#mostrar_datos").html(datos)
			 }
			}) 
		}
		if(obj.id == 'rabpdf')
		{
			url="imprimir.php";
			url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>"
			AbrirPopupImpresion(url,800,600);
			return false;

		}
		if(obj.id == 'rabgrafico')
		{

			var options = {
        
		        chart: {
		            renderTo: 'contenido_grafico',
		            type: 'column'
		        },
		        exporting: {
		            url: '<?php echo $_SESSION['urldir'];?>js/graficos/exporting-server/php/index.php'
		        },
		        title: {
		            text: 'Grafico de Importe Facturado'
		        },
		        xAxis: {
		                type: 'category'
		        },
		        yAxis: [{
		                title: {
		                    text: 'Escala de Monto Facturado'
		                },
		                labels: {
		                    align: 'left',
		                    x: 3,
		                    y: 16,
		                    format: '{value:.,0f}'
		                }
				}],
				plotOptions: {
		            column: {
		                dataLabels: {
		                    enabled: true
		                }
		            }
		        },
			    series: [],
		        drilldown: {
					series: []
				}
			};

			$("#mostrar_datos").html('');
			$("#contenido_grafico").empty();
			$("#loading").remove();
			url="grafico.php";
			url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>";
		    $('<center><div id="loading" style="display:none;"><img src="<?php echo $_SESSION['urldir'];?>images/avance.gif" /></div></center>').insertBefore('#contenido_grafico');
    		$("#loading").fadeIn();			
		    
		    $.getJSON(url,  

		    	function(data) {


					var saltador = 0;
					var salconc  = 0;
					var dat      = new Array();

					// Para Principal Vista
				    $.each(data, function(lineNo, line) {

				    	if(saltador != line[0] ){

				    		if(saltador != 0 ){

								dat.push(ndat);
								serie.data = dat;
								options.series.push(serie);
								salconc    = 0;
								dat        = new Array();
				    		}
							serie               =  new Object();
							serie.name          = line[5];
				    	}

			    		if(salconc != line[1] ){
							
							if(salconc != 0 ){

								ndat.y = parseFloat(parseFloat(ndat.y).toFixed(2));
								dat.push(ndat);
				    		}

							ndat           =  new Object();
							ndat.name      = line[3];
							ndat.y         = parseFloat(line[6]);  
							ndat.drilldown = line[0]+""+line[1];						

			    		}else{

							ndat.y += parseFloat(line[6]);	    			
			    		}

						saltador = line[0]
						salconc  = line[1]
				    }); 

					dat.push(ndat);
					serie.data = dat;
					options.series.push(serie);

					saltador = 0;
					salconc  = 0;
					dat      = new Array();

					// Para el Detalle de la Vista
					$.each(data, function(lineNo, line) {

				    	if(saltador != line[1] ){

				    		if(saltador != 0 ){

								cabezal.data = dat;
								options.drilldown.series.push(cabezal);
								dat        = new Array();
				    		}
							cabezal      =  new Object();
							cabezal.name = line[5]; // Nombre del orden
							cabezal.id   = line[0]+""+line[1]; // Indice del Orden
				    	}
							
						ndat = new Array();
						ndat.push(line[4]); //Nombre de la Categoria
						ndat.push(parseFloat(line[6])); //El valor de la Categoria
						
						dat.push(ndat);
						saltador = line[1]
				    }); 

					cabezal.data = dat;
					options.drilldown.series.push(cabezal);

		        	// Creacion del Grafico
		        	var chart = new Highcharts.Chart(options);

            	}
            ).done(function() {
				$("#loading").fadeOut();
			});
			$("#contenido_grafico").css("display","block");

		}

	}

	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function cargar_rutas_lecturas(obj,cond)
	{
		$.ajax({
			 url:'../../../../ajax/rutas_lecturas_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+obj+'&codsuc=<?=$codsuc?>&condicion='+cond,
			 success:function(datos){
				$("#div_rutaslecturas").html(datos)
				$("#chkrutas").attr("checked",true)
				$("#todosrutas").val(1)
			 }
		}) 
	}
</script>
<div align="center">
	<table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
		<tr>
            <td colspan="2">&nbsp;</td>
		</tr>
        <tr>
            <td colspan="2">
				<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
					<fieldset>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="8%">Sucursal</td>
							<td width="3%" align="center">:</td>
							<td width="33%">
							  <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
							</td>
							<td colspan="2" align="right">Ciclo</td>
							<td width="2%" align="center">:</td>
							<td colspan="2" width="48%">
								<? $objDrop->drop_ciclos($codsuc,0,"onchange='cargar_anio(this.value,1);'"); ?>
							</td>
						</tr>
						<tr>
						    <td>A&ntilde;o</td>
						    <td align="center">:</td>
						    <td>
			                	<div id="div_anio">
			                    	<? $objDrop->drop_anio($codsuc,0); ?>
			                    </div>
			                </td>
						    <td>&nbsp;</td>
							<td align="right">Mes</td>
							<td align="center">:</td>
							<td>
								<div id="div_meses">
							    	<? $objDrop->drop_mes($codsuc,0,0); ?>
							    </div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
					 	<tr>
   							<td colspan="8" style="padding:4px;"  align="center">
								<div id="DivTipos" style="display:inline">
			                       	<input type="radio" name="rabconsultar" id="rabconsultar" value="radio" onclick="return ValidarForm(this);"  />
			                       	<label for="rabconsultar">Consultar</label>
			                        <input type="radio" name="rabpdf" id="rabpdf" value="radio4" onclick="return ValidarForm(this);"  />
			                        <label for="rabpdf">Pdf</label>    
			                        <input type="radio" name="rabgrafico" id="rabgrafico" value="radio5" onclick="return ValidarForm(this);"  />
			                        <label for="rabgrafico">Grafico</label>    
								</div>	
							</td>
  						</tr>
						<tr>
							<td>
							    <input type="hidden" name="orden" id="orden" value="1" />
							</td>
						</tr>
					</table>
					</fieldset>
				</form>
			</td>
		</tr>
		<tr>
			<td>
				<div id="mostrar_datos"></div>
				<div id="contenido_grafico" style="width:100%; height:350px; display:none;"></div>
			</td>
		</tr>
	</table>
</div>
<?php CuerpoInferior(); ?>