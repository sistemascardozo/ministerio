<?php

    if(!session_start()){session_start();}

	include("../../../../../config.php");

    // Parametros
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = $_GET['mes'];
    $mestexto = $_GET['mestexto'];
    $ciclo    = $_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    $sqlLect1   = " SELECT nomtar FROM  facturacion.tarifas WHERE codemp = ? AND codsuc = ? ORDER BY catetar;";
    $consultaL1 = $conexion->prepare($sqlLect1);
    $consultaL1->execute(array($codemp,$codsuc));
    $catetares  = $consultaL1->fetchAll();

    if(intval($mes)<10): $mesmod = "0".$mes; else: $mesmod = $mes; endif;
    if(intval($anio)>2014): $mesmod = $mes; endif;
    // Agua, Alcantarrillado y cargo fijo

    $Sql = "(
            SELECT 
            1 as orden,
            det.codconcepto as codconcepto,
            cab.catetar as catetar,
            co.descripcion as descripcion,
            tar.nomtar as nomtar,
            'MES ACTUAL' as descripcion_orden,
            sum(Round(det.importe,2)) as importe
            FROM facturacion.detctacorriente det
            INNER JOIN facturacion.conceptos co ON (det.codemp = co.codemp AND det.codsuc = co.codsuc AND det.codconcepto = co.codconcepto)
            INNER JOIN facturacion.cabctacorriente cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nrofacturacion = cab.nrofacturacion AND det.nroinscripcion = cab.nroinscripcion AND det.codciclo = cab.codciclo AND det.tipo = cab.tipo AND det.tipoestructura = cab.tipoestructura)
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE det.codsuc = {$codsuc} 
            AND det.periodo = '".$anio.$mesmod."'
            AND det.tipo = 0 AND det.tipoestructura = 0 
            AND det.codconcepto IN(1,2,6) 
            AND det.codtipodeuda <> 3 
            GROUP BY det.codconcepto,cab.catetar,co.descripcion,tar.nomtar
            ORDER BY det.codconcepto, catetar
            )
            UNION
            (
            SELECT 
            2 as orden,
            d.codconcepto as codconcepto, 
            c.catetar as catetar,
            co.descripcion as descripcion,
            tar.nomtar as nomtar,
            'MESES ANTERIORES' as descripcion_orden,
            sum(Round(d.importe - (d.importeacta + d.importerebajado),2)) as importe
            FROM facturacion.cabctacorriente c
            INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.codciclo = d.codciclo AND c.nrofacturacion = d.nrofacturacion AND c.nroinscripcion = d.nroinscripcion AND c.anio = d.anio AND c.mes = d.mes AND c.periodo = d.periodo AND c.tipo = d.tipo AND c.tipoestructura = d.tipoestructura)
            INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp AND d.codsuc = co.codsuc AND d.codconcepto = co.codconcepto)
            INNER JOIN facturacion.tarifas tar ON (c.codemp = tar.codemp AND c.codsuc = tar.codsuc AND c.catetar = tar.catetar)
            WHERE d.codsuc = {$codsuc}  
            AND d.periodo = '".$anio.$mesmod."' 
            AND d.categoria = 1 
            AND d.codtipodeuda <> 3 
            AND d.codtipodeuda <> 4 AND d.codtipodeuda <> 6 
            AND d.codtipodeuda <> 10  AND d.codtipodeuda <> 11 
            AND c.nrodocumento<>0 AND c.tipo = 1 AND c.tipoestructura = 0
            AND d.codconcepto IN(1,2,6)
            GROUP BY d.codconcepto,c.catetar,co.descripcion,tar.nomtar
            ORDER BY d.codconcepto, catetar
            )
            ORDER BY orden, codconcepto, catetar ";

    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $data = array();   

    foreach ($row as $value):

        $data[] = $value;


    endforeach;

    echo json_encode($data);

?>