<?php
	include("../../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses,$fechalite;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral($fechalite);
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RESUMEN DE IMPORTE FACTURADO Y DE ANTERIOR FACTURACION";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(20);
            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=5;
            $h1= 15;
            $h2= 10;

			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
            $this->SetFont('Arial','B',10);
            $this->Cell($Dim[1],$h,utf8_decode('Categorias'),1,0,'C',true);
            $this->Cell($Dim[2],$h,utf8_decode('Conceptos'),1,0,'C',true);
            $this->Cell($Dim[8],$h1,utf8_decode('Total'),1,1,'C',true);

            $this->SetY($this->GetY()-$h2);
            $this->Cell($Dim[4],$h2,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[5],$h2,utf8_decode('Descripcion'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Servicio de Agua'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Servicio de Desague'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Cargo Fijo'),1,1,'C',true);

            // $this->SetY($this->GetY()-$h2);
            $this->SetX($this->GetX()+70);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Act.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Meses Ant.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Act.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Meses Ant.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Act.'),1,0,'C',true);
            $this->Cell($Dim[7],$h,utf8_decode('Fact. Meses Ant.'),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Del Mes'),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Meses Ant.'),1,1,'C',true);
		}

        function Contenido($cont,$descripcion, $num1, $num2, $num3, $num4, $num5, $num6, $subtotal, $subtotal1,$dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',9);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($num1,2),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($num2,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num3,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num4,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num5,2),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($num6,2),1,0,'C',true);
                    $this->Cell($Dim[3],$h,$subtotal,1,0,'C',true);
					$this->Cell($Dim[3],$h,$subtotal1,1,1,'C',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',7);
					$this->Cell($Dim[4],$h,utf8_decode($cont),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($descripcion),1,0,'L',true);
					$this->Cell($Dim[7],$h,number_format($num1,2),1,0,'C',true);
					$this->Cell($Dim[7],$h,number_format($num2,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num3,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num4,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num5,2),1,0,'C',true);
                    $this->Cell($Dim[7],$h,number_format($num6,2),1,0,'C',true);
                    $this->Cell($Dim[3],$h,number_format($subtotal,2),1,0,'C',true);
					$this->Cell($Dim[3],$h,number_format($subtotal1,2),1,1,'C',true);
					break;
			}

		}
    }

    $Dim = array('1'=>70,'2'=>180,'3'=>20,'4'=>10,'5'=>60,'6'=>60,'7'=>30,'8'=>40);

	// Actual
    $codemp   = 1;
    $anio     = $_GET['anio'];
    $mes      = $_GET['mes'];
    $mestexto = $_GET['mestexto'];
    $ciclo    = $_GET['ciclo'];
    $codsuc   = $_GET['codsuc'];
    $fechalite = "01/".$mes."/".$anio;

    if($codsuc==1)
        $factoct='240';
    if($codsuc==2)
        $factoct='186';
    if($codsuc==3)
        $factoct='131';
    $TotalRestar=0;
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    if(intval($mes)<10): $mesmod = "0".$mes; else: $mesmod = $mes; endif;
    if(intval($anio)>2014): $mesmod = $mes; endif;
    
    // Agua, Alcantarrillado y cargo fijo
    $Sql = "(
            SELECT det.codconcepto AS codconcepto,
            cab.catetar as catetar,
            co.descripcion as descripcion,
            tar.nomtar as nomtar,
            sum(det.importe) as importe,
            1 as orden
            FROM facturacion.detctacorriente det
            INNER JOIN facturacion.conceptos co ON (det.codemp = co.codemp AND det.codsuc = co.codsuc AND det.codconcepto = co.codconcepto)
            INNER JOIN facturacion.cabctacorriente cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nrofacturacion = cab.nrofacturacion AND det.nroinscripcion = cab.nroinscripcion AND det.codciclo = cab.codciclo AND det.tipo = cab.tipo AND det.tipoestructura = cab.tipoestructura)
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE det.codsuc = {$codsuc} 
            AND det.periodo = '".$anio.$mesmod."' 
            AND det.codconcepto IN(1,2,6) AND det.codtipodeuda <> 3 
            --AND det.categoria = 0
            AND det.tipo = 0 AND det.tipoestructura = 0 
            GROUP BY det.codconcepto,co.descripcion,cab.catetar,tar.nomtar
            ORDER BY codconcepto, catetar
            )
            UNION
            (
            SELECT 
            d.codconcepto AS codconcepto,
            c.catetar as catetar,
            co.descripcion as descripcion,
            tar.nomtar as nomtar,
            sum(d.importe - (d.importeacta + d.importerebajado)) as importe,
            2 as orden
            FROM facturacion.cabctacorriente c
            INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.codciclo = d.codciclo AND c.nrofacturacion = d.nrofacturacion AND c.nroinscripcion = d.nroinscripcion AND c.anio = d.anio AND c.mes = d.mes AND c.periodo = d.periodo AND c.tipo = d.tipo AND c.tipoestructura = d.tipoestructura)
            INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp AND d.codsuc = co.codsuc AND d.codconcepto = co.codconcepto)
            INNER JOIN facturacion.tarifas tar ON (c.codemp = tar.codemp AND c.codsuc = tar.codsuc AND c.catetar = tar.catetar)
            WHERE d.codsuc = {$codsuc}  
            AND d.periodo = '".$anio.$mesmod."' 
            AND d.categoria = 1 
            AND d.codtipodeuda <> 3 and d.codtipodeuda <> 4 
            AND d.codtipodeuda <> 6 AND d.codtipodeuda <> 10 
            AND d.codtipodeuda <> 11 AND c.nrodocumento<>0 AND c.tipo = 1 AND c.tipoestructura = 0
            AND d.codconcepto IN(1,2,6)
            GROUP BY d.codconcepto,co.descripcion,c.catetar,tar.nomtar
            ORDER BY codconcepto, catetar
            )
            ORDER BY codconcepto,catetar, orden";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['importe']);
        else:
            array_push($cabecera[$value['catetar']],$value['importe']);            
        endif;

    endforeach;

    if(intval($mes)==10 AND intval($anio)==2014)
    {
        
        $Conceptos2 = array();
        $Sql2 = "SELECT CAST(sum(d.importe) AS NUMERIC (18,2)),
        c.descripcion, c.ctadebe,  c.ctahaber,c.codconcepto,c.signo,cab.catetar
        FROM  facturacion.detfacturacion d
        INNER JOIN facturacion.conceptos c ON ( d.codemp = c.codemp AND d.codsuc = c.codsuc AND d.codconcepto = c.codconcepto)
        INNER JOIN facturacion.cabfacturacion cab ON ( d.codemp = cab.codemp AND d.codsuc = cab.codsuc AND d.nrofacturacion = cab.nrofacturacion AND d.nroinscripcion = cab.nroinscripcion)
        INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
        WHERE d.codsuc=".$codsuc." and d.nrofacturacion=".$factoct."  and nrorefinanciamiento<>0
        AND c.codconcepto in (1,2,6)
        GROUP BY  c.descripcion,c.ctadebe,c.ctahaber,c.codantiguo,c.codconcepto, c.signo, cab.catetar
        ORDER BY c.codconcepto  ASC";

        $Consulta2 = $conexion->query($Sql2);
        
        foreach($Consulta2->fetchAll() as $row2)
        {
            if (!array_key_exists($row2['catetar'].$row2['codconcepto'], $Conceptos2))
            {
                $Conceptos2[$row2['catetar'].$row2['codconcepto']] = array(
                                                        "Id"=>$row2['codconcepto'], 
                                                        "Importe"=>$row2[0],
                                                        "Descripcion"=>$row2[1],
                                                        "catetar"=>$row2['catetar']);
            }
            else
            {
                $Conceptos2[$row2['catetar'].$row2['codconcepto']] = array(
                                                            "Id"=>$row2['codconcepto'], 
                                                            "Importe"=>$Conceptos2[$row2['codconcepto']]["Importe"] + $row2[0],
                                                            "Descripcion"=>$row2[1],
                                                            "catetar"=>$row2['catetar']);
            }
        }
    }

	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(5);
    $objReporte->SetAutoPageBreak(true,5);
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $subtotal3 = 0;
    $subtotal4 = 0;
    $subtotal5 = 0;
    $subtotal6 = 0;
    $subtotal7 = 0;
    $subtotal8 = 0;
    $contador  = 0;

    foreach ($cabecera as $key => $value): 

        if(intval($mes)==10 AND intval($anio)==2014):
                $value[1] -= $Conceptos2[$key."1"]['Importe'];
                $value[3] -= $Conceptos2[$key."2"]['Importe'];
                $value[5] -= $Conceptos2[$key."6"]['Importe'];
        endif;
        $contador++;
        $subtotal1 += $value[1];
        $subtotal2 += $value[2];
        $subtotal3 += $value[3];
        $subtotal4 += $value[4];
        $subtotal5 += $value[5];
        $subtotal6 += $value[6];
        $subtotal7 += $value[1]+$value[3]+$value[5];
        $subtotal8 += $value[2]+$value[4]+$value[6];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2], $value[3], $value[4], $value[5], $value[6],($value[1]+$value[3]+$value[5]),($value[2]+$value[4]+$value[6]));
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,$subtotal4,$subtotal5,$subtotal6,$subtotal7,$subtotal8,1);

	$objReporte->Output();	
		
?>