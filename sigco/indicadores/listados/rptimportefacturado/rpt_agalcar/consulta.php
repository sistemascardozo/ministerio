<?php 
    if(!session_start()){session_start();}

	include("../../../../../config.php");

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    if(intval($mes)<10): $mesmod = "0".$mes; else: $mesmod = $mes; endif;
    if(intval($anio)>2014): $mesmod = $mes; endif;
    // Agua, Alcantarrillado y cargo fijo
    $Sql = "(
            SELECT det.codconcepto AS codconcepto,
            cab.catetar as catetar,
            co.descripcion as descripcion,
            tar.nomtar as nomtar,
            sum(det.importe) as importe,
            1 as orden
            FROM facturacion.detctacorriente det
            INNER JOIN facturacion.conceptos co ON (det.codemp = co.codemp AND det.codsuc = co.codsuc AND det.codconcepto = co.codconcepto)
            INNER JOIN facturacion.cabctacorriente cab ON (det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nrofacturacion = cab.nrofacturacion AND det.nroinscripcion = cab.nroinscripcion AND det.codciclo = cab.codciclo AND det.tipo = cab.tipo AND det.tipoestructura = cab.tipoestructura)
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE det.codsuc = {$codsuc} 
            AND det.periodo = '".$anio.$mesmod."'
            AND det.tipo = 0 AND det.tipoestructura = 0 
            AND det.codconcepto IN(1,2,6) 
            AND det.codtipodeuda <> 3 
            GROUP BY det.codconcepto,co.descripcion,cab.catetar,tar.nomtar
            ORDER BY codconcepto, catetar
            )
            UNION
            (
            SELECT 
            d.codconcepto AS codconcepto,
            c.catetar as catetar,
            co.descripcion as descripcion,
            tar.nomtar as nomtar,
            sum(d.importe - (d.importeacta + d.importerebajado)) as importe,
            2 as orden
            FROM facturacion.cabctacorriente c
            INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.codciclo = d.codciclo AND c.nrofacturacion = d.nrofacturacion AND c.nroinscripcion = d.nroinscripcion AND c.anio = d.anio AND c.mes = d.mes AND c.periodo = d.periodo AND c.tipo = d.tipo AND c.tipoestructura = d.tipoestructura)
            INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp AND d.codsuc = co.codsuc AND d.codconcepto = co.codconcepto)
            INNER JOIN facturacion.tarifas tar ON (c.codemp = tar.codemp AND c.codsuc = tar.codsuc AND c.catetar = tar.catetar)
            WHERE d.codsuc = {$codsuc}  
            AND d.periodo = '".$anio.$mesmod."' 
            AND d.categoria = 1 
            AND d.codtipodeuda <> 3 
            AND d.codtipodeuda <> 4 AND d.codtipodeuda <> 6 
            AND d.codtipodeuda <> 10  AND d.codtipodeuda <> 11 
            AND c.nrodocumento<>0 AND c.tipo = 1 AND c.tipoestructura = 0
            AND d.codconcepto IN(1,2,6)
            GROUP BY d.codconcepto,co.descripcion,c.catetar,tar.nomtar
            ORDER BY codconcepto, catetar
            )
            ORDER BY codconcepto,catetar, orden";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();   
    $indice   = 0; 

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['importe']);
        else:
            array_push($cabecera[$value['catetar']],$value['importe']);            
        endif;

    endforeach;

    if($codsuc==1)
        $factoct='240';
    if($codsuc==2)
        $factoct='186';
    if($codsuc==3)
        $factoct='131';
    $TotalRestar=0;

    if(intval($mes)==10 AND intval($anio)==2014)
    {
        // $Sql="select sum(importe) 
        //         from facturacion.detfacturacion 
        //         where codsuc=".$codsuc." and nrofacturacion=".$factoct." 
        //         and nrorefinanciamiento<>0
        //         AND codconcepto in (1,2,6)";
        // $row = $conexion->query($Sql)->fetch();
        // $TotalRestar = $row[0];
        //DETALLE
        $Conceptos2 = array();
        $Sql2 = "SELECT CAST(sum(d.importe) AS NUMERIC (18,2)),
        c.descripcion, c.ctadebe,  c.ctahaber,c.codconcepto,c.signo,cab.catetar
        FROM  facturacion.detfacturacion d
        INNER JOIN facturacion.conceptos c ON ( d.codemp = c.codemp AND d.codsuc = c.codsuc AND d.codconcepto = c.codconcepto)
        INNER JOIN facturacion.cabfacturacion cab ON ( d.codemp = cab.codemp AND d.codsuc = cab.codsuc AND d.nrofacturacion = cab.nrofacturacion AND d.nroinscripcion = cab.nroinscripcion)
        INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
        WHERE d.codsuc=".$codsuc." and d.nrofacturacion=".$factoct."  and nrorefinanciamiento<>0
        AND c.codconcepto in (1,2,6)
        GROUP BY  c.descripcion,c.ctadebe,c.ctahaber,c.codantiguo,c.codconcepto, c.signo, cab.catetar
        ORDER BY c.codconcepto  ASC";

        $Consulta2 = $conexion->query($Sql2);
        
        foreach($Consulta2->fetchAll() as $row2)
        {
            if (!array_key_exists($row2['catetar'].$row2['codconcepto'], $Conceptos2))
            {
                $Conceptos2[$row2['catetar'].$row2['codconcepto']] = array(
                                                        "Id"=>$row2['codconcepto'], 
                                                        "Importe"=>$row2[0],
                                                        "Descripcion"=>$row2[1],
                                                        "catetar"=>$row2['catetar']);
            }
            else
            {
                $Conceptos2[$row2['catetar'].$row2['codconcepto']] = array(
                                                            "Id"=>$row2['codconcepto'], 
                                                            "Importe"=>$Conceptos2[$row2['codconcepto']]["Importe"] + $row2[0],
                                                            "Descripcion"=>$row2[1],
                                                            "catetar"=>$row2['catetar']);
            }
        }
    }

    // var_dump($Conceptos2);exit;
    // var_dump($cabecera);exit;

?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td align="center" colspan="2" rowspan="2">Categorias</td>
            <td align="center" colspan="6">Conceptos</td>
            <td align="center" colspan="2">Total</td>
        </tr>
        <tr>
            <td align="center" colspan="2">Servicio de Agua</td>
            <td align="center" colspan="2">Servicio de Desague</td>
            <td align="center" colspan="2">Cargo Fijo</td>
            <td align="center" rowspan="2">Total del Mes</td>
            <td align="center" rowspan="2">Total Meses Ant.</td>
        </tr>
        <tr>
            <td align="center" width="5%">N°</td>
            <td align="center">Tarifas</td>
            <td>Fact. Actual del Mes</td>
            <td>Fact. Deuda Meses Ant.</td>
            <td>Fact. Actual del Mes</td>
            <td>Fact. Deuda Meses Ant.</td>
            <td>Fact. Actual del Mes</td>
            <td>Fact. Deuda Meses Ant.</td>
        </tr>
    </thead>
    <tbody>
        <?php 

        $anti      = 0;

        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal4 = 0;
        $subtotal5 = 0;
        $subtotal6 = 0;
        $subtotal7 = 0;
        $subtotal8 = 0;
        $contador  = 0;

        foreach ($cabecera as $key => $value):

            if(intval($mes)==10 AND intval($anio)==2014):
                $value[1] -= $Conceptos2[$key."1"]['Importe'];
                $value[3] -= $Conceptos2[$key."2"]['Importe'];
                $value[5] -= $Conceptos2[$key."6"]['Importe'];
            endif;

            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?= number_format($value[1] = empty($value[1]) ? 0 : $value[1],2);?></td>
                <td align="right"><?= number_format($value[2] = empty($value[2]) ? 0 : $value[2],2);?></td>
                <td align="right"><?= number_format($value[3] = empty($value[3]) ? 0 : $value[3],2);?></td>
                <td align="right"><?= number_format($value[4] = empty($value[4]) ? 0 : $value[4],2);?></td>
                <td align="right"><?= number_format($value[5] = empty($value[5]) ? 0 : $value[5],2);?></td>
                <td align="right"><?= number_format($value[6] = empty($value[6]) ? 0 : $value[6],2);?></td>
                <td align="right"><?= number_format(($value[1]+$value[3]+$value[5]),2);?></td>
                <td align="right"><?= number_format(($value[2]+$value[4]+$value[6]),2);?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal4 += $value[4];
            $subtotal5 += $value[5];
            $subtotal6 += $value[6];
            $subtotal8 += $value[1]+$value[3]+$value[5];
            $subtotal7 += $value[2]+$value[4]+$value[6];
            endforeach;?>
        <tr style="font-weight: bold">
            <td colspan="2" >Total</td>
            <td align="right"><?=number_format($subtotal1,2);?></td>
            <td align="right"><?=number_format($subtotal2,2);?></td>
            <td align="right"><?=number_format($subtotal3,2);?></td>
            <td align="right"><?=number_format($subtotal4,2);?></td>
            <td align="right"><?=number_format($subtotal5,2);?></td>
            <td align="right"><?=number_format($subtotal6,2);?></td>
            <td align="right"><?=number_format($subtotal8,2);?></td>
            <td align="right"><?=number_format($subtotal7,2);?></td>
        </tr>
    </tbody>
</table>
</center> 

