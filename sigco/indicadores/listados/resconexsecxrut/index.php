<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "REP. CONEXIONES X SECTOR Y RUTA";
	$Activo = 1;

	CuerpoSuperior($TituloVentana);

	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);

	$Fecha = date('d/m/Y');
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	jQuery(function($)
	{

		$( "#DivTipos" ).buttonset();

		var dates = $( "#FechaDesde, #FechaHasta" ).datepicker({
			//defaultDate: "+1w",
			buttonText: 'Selecione Fecha de Busqueda',
			showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			//showOn: 'both',
			showButtonPanel: true,
			numberOfMonths: 2,
			onSelect: function( selectedDate ) {
				var option = this.id == "FechaDesde" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});

	});

	var c = 0
	function ValidarForm(Op)
	{
		var sectores 		 = "";
		var rutas 		   = "";
		var ciclo = $("#ciclo").val();
		var anio  = $("#anio").val();
		var mes   = $("#mes").val();

		if(ciclo==0)
		{
			alert("Seleccione el Ciclo");
			return false;
		}

		if(anio==0)
		{
			alert("Seleccione el Año");
			return false;
		}

		if(mes==0)
		{
			alert("Seleccione el Mes");
			return false;
		}

		if($("#todosectores").val()==0)
		{
			sectores=$("#codsector").val();

			if(sectores==0)
			{
				alert("Seleccione el Sector");
				return false;
			}
		}
		else{
			sectores = "%";
		}

		if($("#todosrutas").val()==0)
		{
			rutas=$("#rutaslecturas").val();

			if(rutas==0)
			{
				alert("Seleccione la Ruta");
				return false;
			}
		}
		else{
			rutas = "%";
		}

		if(document.getElementById("rptpdf").checked==true)
		{
			url="imprimir.php";
		}
		if(document.getElementById("rptexcel").checked==true)
		{
			url="excel.php";
		}
		url += "?sector="+sectores+"&ruta="+rutas+"&codsuc=<?=$codsuc?>&ciclo="+ciclo
		url += "&anio="+anio+"&mes="+mes
		AbrirPopupImpresion(url,800,600)

		return false;
	}

	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}

	function quitar_disabled(obj,input)
	{
		if(obj.checked)
		{
			$("#"+input).attr('disabled', true);
		}else{
			$("#"+input).attr('disabled', false);
		}
	}

	function cargar_anio_drop(obj)
	{
		$.ajax({
			 url:'../../../../ajax/anio_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&codciclo='+obj+'&condicion=1',
			 success:function(datos){
				$("#div_anio").html(datos)
			 }
		})
	}

	function cargar_mes(ciclo,suc,anio)
	{
		$.ajax({
			 url:'../../../../ajax/mes_drop.php',
			 type:'POST',
			 async:true,
			 data:'codciclo='+ciclo+'&codsuc='+suc+'&anio='+anio,
			 success:function(datos){
				$("#div_meses").html(datos)
			 }
		})
	}

	function cargar_rutas_lecturas(obj,cond)
	{
		$.ajax({
				 url:'../../../../ajax/rutas_lecturas_drop.php',
				 type:'POST',
				 async:true,
				 data:'codsector='+obj+'&codsuc=<?=$codsuc?>&condicion='+cond,
				 success:function(datos){
					$("#div_rutaslecturas").html(datos)
					$("#chkrutas").attr("checked",true)
					$("#todosrutas").val(1)
				 }
			})
	}

	function validar_sectores(obj, idx)
	{
		$("#rutaslecturas").attr("disabled",true)
		$("#chkrutas").attr("checked",true)
		$("#todosrutas").val(1)

	}

</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="19%">Sucursal</td>
				    <td width="4%" align="center">:</td>
				    <td width="15%"><label>
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </label></td>
				    <td width="13%" align="right">&nbsp;</td>
				    <td width="4%" align="center">&nbsp;</td>
				    <td width="53%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td>Ciclo</td>
				    <td align="center">:</td>
				    <td colspan="4">
							<? $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio_drop(this.value);'"); ?>
                        <!-- <input type="checkbox" name="chkciclos" id="chkciclos" checked="checked" onclick="CambiarEstado(this,'todosciclos');quitar_disabled(this,'ciclo')" /> -->
                        <!-- Todos los Ciclos</td> -->
		      </tr>
					<tr>
						<td>A&ntilde;o</td>
						<td align="center">:</td>
						<td colspan="4">
							<div id="div_anio">
								<? echo $objMantenimiento->drop_anio($codsuc,0); ?>
								</div>
						</td>
					</tr>
					<tr>
						<td>Mes</td>
						<td align="center">:</td>
						<td colspan="4">
							<div id="div_meses">
								<? echo $objMantenimiento->drop_mes($codsuc,0,0); ?>
								</div>
						</td>
					</tr>
				  <tr>
				    <td>Sector</td>
				    <td align="center">:</td>
				    <td colspan="4">
							<?php echo $objMantenimiento->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_lecturas(this.value,1);'"); ?>
              <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todosectores');quitar_disabled(this,'codsector'); validar_sectores(this, 'todosectores');" />
		        	Todos los Sectores
            </td>
      		</tr>
					<tr>
						<td>Ruta</td>
						<td align="center">:</td>
						<td colspan="4">
							<div id="div_rutaslecturas" style="display: inline;">
								<select name="rutaslecturas" id="rutaslecturas" style="width:220px" class="select" disabled="disabled">
										<option value="0">--Seleccione la Ruta de Lectura--</option>
								</select>
							</div>
							<input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todosrutas');quitar_disabled(this,'rutaslecturas');" />
							Todas las Rutas
						</td>
					</tr>
			      <!-- <tr>
				    <td>Fecha Reg.</td>
				    <td align="center">:</td>
				    <td colspan="4">
                      <input type="text" class="inputtext" id="FechaDesde" maxlength="10" value="<?=$Fecha?>" style="width:80px;" />
				-
				<input type="text" class="inputtext" id="FechaHasta" maxlength="10" value="<?=$Fecha?>" style="width:80px;" />
			          <input type="checkbox"  id="ChckFechaReg" checked="checked"  />
		              Todos
                    </td>
			      </tr> -->


				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td colspan="2">
              <input type="hidden" name="todosectores" id="todosectores" value="1" />
              <input type="hidden" name="todosrutas" id="todosrutas" value="1" />
            </td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>

			      <tr>
        			<td colspan="6" align="center">
        				<div id="DivTipos" style="display:inline">
	                           <input type="radio" name="rabresumen" id="rptpdf" value="radio" checked="checked" />
	                            <label for="rptpdf">PDF</label>
	                            <input type="radio" name="rabresumen" id="rptexcel" value="radio2" />
	                           <label for="rptexcel">EXCEL</label>

						</div>
						 <input type="button" onclick="return ValidarForm();" value="Generar" id="">
        			</td>
        		</tr>
				</table>
			</fieldset>
		</td>
	</tr>

	 </tbody>

    </table>
 </form>
</div>
<script>
	// $("#ciclo").attr('disabled', true);
	$("#codsector").attr('disabled', true);
	$("#rutaslecturas").attr('disabled', true);
</script>
<?php CuerpoInferior(); ?>
