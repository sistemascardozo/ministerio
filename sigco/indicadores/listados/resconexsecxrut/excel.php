<?php
	include("../../../../objetos/clsReporte.php");
	include("../../../../objetos/clsReporteExcel.php");
	header("Content-type: application/vnd.ms-excel; name='excel'");
	header("Content-Disposition: filename=ConexionesNuevas.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	$clsFunciones = new clsFunciones();
	$objReporte = new clsReporte();
	$codsuc = $_GET["codsuc"];
	$codsector = $_GET["sector"];
	$tiposervicio = $_GET["tiposervicio"];
	$estadoservicio = $_GET["estadoservicio"];
	$ciclo = $_GET["ciclo"];
	$altocon = $_GET["altocon"];
	$Desde = $_GET["Desde"];
	$Hasta = $_GET["Hasta"];

	CabeceraExcel(2, 13);
?>
<table class="ui-widget" border="0" cellspacing="0" id="TbTitulo" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:14px">

        <tr title="Cabecera">
            <th scope="col" colspan="15" align="center" >CONEXIONES NUEVAS</th>
        </tr>
        <tr>
            <th colspan="15">Fecha de Ingreso desde: <?= $Desde ?> hasta: <?= $Hasta ?> </th>
        </tr>    
    </thead>
</table>
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">

        <tr>
            <th width="50" scope="col" class="ui-widget-header">N&deg;</th>
            <th width="200" scope="col" class="ui-widget-header">COD. CATASTRAL</th>
            <th width="200" scope="col" class="ui-widget-header">COD. ANTIGUO</th>
            <th width="50" scope="col" class="ui-widget-header">NRO. INSC.</th>
            <th width="30" scope="col" class="ui-widget-header">CLIENTE</th>
            <th width="50" scope="col" class="ui-widget-header">DIRECCION</th>
            <th width="50" scope="col" class="ui-widget-header">CAT</th>
            <th width="50" scope="col" class="ui-widget-header">EST</th>
            <th width="50" scope="col" class="ui-widget-header">TS</th>
            <th width="200" scope="col" class="ui-widget-header">NRO. MED.</th>
            <th width="200" scope="col" class="ui-widget-header">SECT</th>
            <th width="50" scope="col" class="ui-widget-header">MZA.</th>
            <th width="30" scope="col" class="ui-widget-header">LTE</th>
            <th width="50" scope="col" class="ui-widget-header">SLTE</th>
            <th width="50" scope="col" class="ui-widget-header">FECHA REGISTRO</th>

        </tr>
    </thead>
    <tbody style="font-size:12px">
        <?php
        $h = 4;
        $s = 2;

        if ($altocon != 0) {
            $text = " and altocon=" . $altocon;
        }
        if ($Desde != "") {
            $Condicion = " AND c.fechareg BETWEEN CAST ('" . $Desde . "' AS DATE) AND CAST ( '" . $Hasta . "' AS DATE ) ";
        }
        $sql = "select c.nroinscripcion,c.propietario,tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,t.nomtar,
					e.descripcion as estadoservicio,c.codtiposervicio,c.nromed,s.descripcion as sector,c.codmanzanas,c.lote,
					c.sublote,c.domestico,c.comercial,c.industrial,c.social,c.estatal," . $clsFunciones->getCodCatastral("c.") . ",
					cx.altocon,c.codantiguo,c.fechareg
					from catastro.clientes as c 
					INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
					inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
					inner join facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
					inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
					inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
					inner join catastro.conexiones as cx on(c.codemp=cx.codemp and c.codsuc=cx.codsuc and c.nroinscripcion=cx.nroinscripcion)
					where c.codemp=1 and c.codsuc=? and CONVERT(CHAR,c.codsector) LIKE ? 
					and CONVERT(CHAR,c.codtiposervicio) LIKE ? and CONVERT(CHAR,c.codestadoservicio) LIKE ? 
					and CONVERT(CHAR,c.codciclo) LIKE ?" . $text . $Condicion;

        $codsector = "%" . $codsector . "%";
        $tiposervicio = "%" . $tiposervicio . "%";
        $estadoservicio = "%" . $estadoservicio . "%";
        $ciclo = "%" . $ciclo . "%";
        $count = 0;

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $codsector, $tiposervicio, $estadoservicio, $ciclo));
        $items = $consulta->fetchAll();
        $count = 0;
        foreach ($items as $row) {
            $count++;
            $codcatastro = $row["codcatastro"];

            if ($row["codtiposervicio"] == 1) {
                $ts = "Ag/Des";
            }
            if ($row["codtiposervicio"] == 2) {
                $ts = "Ag";
            }
            if ($row["codtiposervicio"] == 3) {
                $ts = "Des";
            }
            ?>

            <tr <?= $title ?> <?= $class ?> onclick="SeleccionaId(this);" id="<?= $count ?>" >
                <td align="left" valign="middle"><? echo $count; ?></td>
                <td align="left" valign="middle"><? echo ($row['codcatastro']); ?></td>
                <td align="left" valign="middle"><? echo ($row['codantiguo']); ?></td>
                <td align="left" valign="middle"><? echo ($row['nroinscripcion']); ?></td>
                <td align="left" valign="middle"><? echo (trim((strtoupper($row["propietario"])))); ?></td>
                <td align="left" valign="middle"><? echo (strtoupper($row["descripcioncorta"]." ".$row["calle"]." ".$row["nrocalle"])); ?></td>
                <td align="left" valign="middle"><? echo (substr(strtoupper($row["nomtar"]),0,3)); ?></td>
                <td align="left" valign="middle"><? echo ( strtoupper($row["estadoservicio"])); ?></td>
                <td align="left" valign="middle"><? echo ($ts); ?></td>
                <td align="left" valign="middle"><? echo ($row['nromed']); ?></td>
                <td align="left" valign="middle"><? echo (strtoupper($row["sector"])); ?></td>
                <td align="left" valign="middle"><? echo ($row['codmanzanas']); ?></td>
                <td align="left" valign="middle"><? echo ($row['lote']); ?></td> 
                <td align="left" valign="middle"><? echo ($row['sublote']); ?></td>
                <td align="left" valign="middle"><? echo ($row['fechareg']); ?></td>
                <?php
            }
            ?>
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td colspan="15" align="center" class="ui-widget-header">Usuario Registrados <?= $count ?></td>

        </tr>
    </tfoot>
</table>
<table><tr><td></td></tr></table>
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
        <tr><td class="ui-widget-header" colspan="2">RESUMEN POR ESTADOS DE SERVICIO</td></tr>
    </thead>
    <?php
    $Sql = "select c.codestadoservicio,e.descripcion,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
			from catastro.clientes as c 
			inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
			
			WHERE c.codemp=1 and c.codsuc=" . $codsuc . " and CONVERT(CHAR,c.codsector) LIKE '" . $codsector . "' 
			and CONVERT(CHAR,c.codtiposervicio) LIKE '" . $tiposervicio . "' and CONVERT(CHAR,c.codestadoservicio) LIKE '" . $estadoservicio . "' 
			and CONVERT(CHAR,c.codciclo) LIKE '" . $ciclo . "' " . $text . $Condicion . "
			GROUP BY c.codestadoservicio,e.descripcion ";
    $Consulta = $conexion->query($Sql);
    foreach ($Consulta->fetchAll() as $row) {
        ?>
        <tr>
            <td><?= $row[1] ?></td>
            <td><?= $row[2] ?></td>
        </tr>
        <?php
        }
        ?>
    </table>

    <table><tr><td></td></tr></table>
    <table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="100%" rules="rows">
        <thead class="ui-widget-header" style="font-size:10px">
            <tr><td class="ui-widget-header" colspan="2">RESUMEN POR CATEGORIAS</td></tr>
        </thead>
        <?php
        $Sql = "select c.catetar,t.nomtar,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from catastro.clientes as c 
					inner join facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
					WHERE c.codemp=1 and c.codsuc=" . $codsuc . " and CONVERT(CHAR,c.codsector) LIKE '" . $codsector . "' 
					and CONVERT(CHAR,c.codtiposervicio) LIKE '" . $tiposervicio . "' and CONVERT(CHAR,c.codestadoservicio) LIKE '" . $estadoservicio . "' 
					and CONVERT(CHAR,c.codciclo) LIKE '" . $ciclo . "' " . $text . $Condicion . "
					GROUP BY c.catetar,t.nomtar ";
        $Consulta = $conexion->query($Sql);
        foreach ($Consulta->fetchAll() as $row) {
            ?>
            <tr>
                <td><?= $row[1] ?></td>
                <td><?= $row[2] ?></td>
            </tr>
            <?php
        }
        ?>
</table>