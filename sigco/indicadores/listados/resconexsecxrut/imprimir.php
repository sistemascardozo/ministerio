<?php
	include("../../../../objetos/clsReporte.php");

	$clfunciones = new clsFunciones();

	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses,$Desde,$Hasta,$anio,$mes,$meses, $Dim;

			$periodo = $this->DecFechaLiteral();

			$this->SetFont('Arial','B',10);

			$h = 4;
			$this->Cell(278, $h+2,"RESUMEN DE CONEXIONES DE AGUA POTABLE X SECTOR - RUTA",0,1,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(278, $h+2,'Periodo: ' . $meses[$mes] . ' - ' . $anio ,0,1,'L');

			$this->Ln(3);

			$this->SetFont('Arial','B',6);

			$this->Cell($Dim[1], ($h*3), utf8_decode('SECTOR'), 1, 0, 'C', false);
			$this->Cell($Dim[2], ($h*3), utf8_decode('RUTA'), 1, 0, 'C', false);
			$this->Cell($Dim[3], $h, utf8_decode('CONEXIONES ACTIVAS'), 1, 0, 'C', false);
			$this->Cell($Dim[4], ($h*3), utf8_decode('INACTIVAS'), 1, 0, 'C', false);
			$this->Cell($Dim[5], ($h*3), utf8_decode('TOTAL'), 1, 1, 'C', false);

			$this->SetXY($this->GetX()+($Dim[1]+$Dim[2]), $this->GetY()-($h*2));
			$this->Cell($Dim[6], $h, utf8_decode('CON MEDIDOR'), 1, 0, 'C', false);
			$this->Cell($Dim[7], ($h*2), utf8_decode('SIN MEDIDOR'), 1, 1, 'C', false);

			$this->SetXY($this->GetX()+($Dim[1]+$Dim[2]), $this->GetY()-($h));
			$this->Cell($Dim[8], $h, utf8_decode('LEIDOS'), 1, 0, 'C', false);
			$this->Cell($Dim[9], $h, utf8_decode('PROMEDIADOS'), 1, 0, 'C', false);
			$this->Cell($Dim[10], $h, utf8_decode('TOTAL'), 1, 1, 'C', false);

			$this->Ln(5);

		}

		function sub_cabecera($nombre, $cantidad, $op=0) {
				global $Dim;

				$this->SetFillColor(255, 255, 255); //Color de Fondo
				$this->SetTextColor(0);
				$h = 5;

				switch ($op) {
					case 0:
						$this->SetFont('Arial', '', 8);
							$this->Cell($Dim[1], ($h*$cantidad), utf8_decode($nombre), 1, 0, 'C', true);
						break;

					case 1:

						$this->SetFont('Arial', '', 8);
							$this->Cell($Dim[2], ($h), utf8_decode($nombre), 1, 0, 'L', true);
						break;

					case 2:

							$this->SetFont('Arial', '', 8);
							$this->SetX($this->GetX()+$Dim[1]);
								$this->Cell($Dim[2], ($h), utf8_decode($nombre), 1, 0, 'L', true);
						break;

					default:
						$this->SetFont('Arial', '', 8);
							$this->Cell($Dim[1], ($h*$cantidad), utf8_decode($sector), 1, 0, 'C', true);
						break;
				}
		}


		function contenido($leidos, $promediados, $sub_activas_con_medidor, $sub_activas_sin_medidor, $sub_inactivas, $sub_total)
		{
			global $Dim;

			$h = 5;
			$this->Cell($Dim[8], $h,$leidos,1,0,'C');
			$this->Cell($Dim[9], $h,$promediados,1,0,'C');
			$this->Cell($Dim[10], $h,$sub_activas_con_medidor,1,0,'C');
			$this->Cell($Dim[7], $h,$sub_activas_sin_medidor,1,0,'C');
			$this->Cell($Dim[4], $h,$sub_inactivas,1,0,'C');
			$this->Cell($Dim[5], $h,$sub_total,1,1,'C');

			}

		function sub_footer($subtotal_conexiones)
		{
				global $Dim;
				$this->Ln(2);
				$h = 5;
				$this->SetFont('Arial', 'B', 9);
				$this->Cell(($Dim[1]+$Dim[2]), $h,"TOTAL DE CONEXIONES",0,0,'C');
				$this->Cell($Dim[9], $h,$subtotal_conexiones, 0, 1,'C');
				$this->Ln(2);

		}


	}

	$Dim = array(
		1=>30,
		2=>30,
		3=>165,
		4=>30,
		5=>20,
		6=>120,
		7=>45,
		8=>50,
		9=>50,
		10=>20,
	);


	$codsuc = $_GET["codsuc"];
	$ciclo  = $_GET["ciclo"];
	$anio   = $_GET["anio"];
	$mes    = $_GET["mes"];
	$sector = $_GET["sector"];
	$ruta   = $_GET["ruta"];

	// Para obtener el periodo de faturacion
	$sqlLect   = "SELECT nrofacturacion ";
	$sqlLect 	.= "FROM facturacion.periodofacturacion ";
	$sqlLect 	.= "WHERE codemp = 1 AND codsuc = " . $codsuc . " AND codciclo = " . $ciclo . " ";
	$sqlLect 	.= " AND anio = '" . $anio . "' AND mes = '" . $mes . "' ";
	$consultaL = $conexion->query($sqlLect);
	$nrofact   = $consultaL->fetch();

	$condicion_sector = $condicion_ruta = " ";

	if($sector != "%") {
		$condicion_sector = " AND s.codsector IN(" . $sector . ") ";
	}

	if($ruta != "%") {
		$condicion_ruta = " AND r.codrutlecturas IN(" . $ruta . ") ";
	}

	$sql  = "SELECT c.codestadoservicio AS codigo, e.descripcion AS estadoservicio, s.codsector AS codsector,	s.descripcion AS sector, ";
	$sql .= "	r.codrutlecturas AS codruta, r.codmanzanas AS ruta, COUNT(c.codestadoservicio) AS cantidad, ";
	$sql .= " c.tipofacturacion, TRIM(c.nromed) AS nromed ";
	$sql .= "FROM facturacion.cabfacturacion AS c ";
	$sql .= "	JOIN public.estadoservicio AS e ON(e.codestadoservicio = c.codestadoservicio) ";
	$sql .= "	JOIN catastro.clientes AS cl ON(cl.codemp = c.codemp AND cl.codsuc = c.codsuc AND cl.nroinscripcion = c.nroinscripcion) ";
	$sql .= "	JOIN public.sectores AS s ON(s.codemp = cl.codemp AND s.codsuc = cl.codsuc AND s.codsector = cl.codsector AND s.codzona = cl.codzona) ";
	$sql .= "	JOIN public.rutaslecturas AS r ON(cl.codemp = r.codemp AND cl.codsuc = r.codsuc AND cl.codzona = r.codzona AND cl.codsector = r.codsector AND cl.codrutlecturas = r.codrutlecturas AND cl.codmanzanas = r.codmanzanas) ";
	$sql .= "WHERE  c.codemp = 1 AND c.nrofacturacion = " . $nrofact['nrofacturacion'] . " " . $condicion_sector;
	$sql .= " AND c.codsuc = " . $codsuc . "  AND c.codciclo = " . $ciclo . " ";
	$sql .= " " . $condicion_ruta;
	$sql .= "GROUP BY c.codestadoservicio, e.descripcion, s.codsector, s.descripcion, r.codrutlecturas, r.codmanzanas, ";
	$sql .= " c.tipofacturacion, c.nromed ";
	$sql .= "ORDER BY s.codsector, r.codrutlecturas, c.codestadoservicio ";


	$consulta = $conexion->query($sql);
	$items = $consulta->fetchAll();

	$array_general = array();

	foreach ($items as $key => $value) {

		if(!array_key_exists($value['codsector'], $array_general)){
			$array_general[$value['codsector']]['nombre'] = $value['sector'];
			$array_general[$value['codsector']]['data']   = array();
			$contador_ruta                                = 0;
		}

		if(!array_key_exists($value['codruta'], $array_general[$value['codsector']]['data'])){
			$array_general[$value['codsector']]['data'][$value['codruta']]['nombre'] = "RUTA " . $value['ruta'];
			$array_general[$value['codsector']]['data']	[$value['codruta']]['data'] = array();
			$contador_ruta++;
		}

		switch ((int)$value['codigo']) {
			case 1:
				$codigo = 1;
				$codigo_nombre = "ACTIVAS";
			break;
			default:
				$codigo = 0;
				$codigo_nombre = 'INACTIVAS';
			break;
		}

		// Para saber agrupar las activas o inactivas
		if(!array_key_exists($codigo, $array_general[$value['codsector']]['data']	[$value['codruta']]['data'])){
			$array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['nombre'] = $codigo_nombre;
			$array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['data'] = array();
		}

		if(empty($value["nromed"]))
        {
			$medidor = 0;
			$medidor_tipo = "SIN MEDIDOR";
		}else{
			$medidor = 1;
			$medidor_tipo = "CON MEDIDOR";
		}

		// Para saber agrupar con medidor o sin medidor
		if(!array_key_exists($medidor, $array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['data'])){
			$array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['data'][$medidor]['nombre'] = $medidor_tipo;
			$array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['data'][$medidor]['data']   = array('promediado'=>0,'leidos'=>0);
		}

		switch ((int)$value['tipofacturacion']) {
			case 0: // Consumo Leido
				$array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['data'][$medidor]['data']['leidos'] += 1;
				break;
			case 1: // Consumo Promediado
				$array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['data'][$medidor]['data']['promediado'] += 1;
				break;
			case 2: // Consumo Asignado
				$array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['data'][$medidor]['data']['leidos'] += 1;
				break;

			default: //Consumo Leido
				$array_general[$value['codsector']]['data']	[$value['codruta']]['data'][$codigo]['data'][$medidor]['data']['leidos'] += 1;
				break;
		}

	}


	$objReporte = new clsPadron("L");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();


	// echo "<pre>"; print_r($array_general);echo "</pre>"; exit;

	foreach ($array_general as $key_sector => $data_sector) {

		if(empty($data_sector['nombre'])){
			break;
		}

		$nombre_sector  = $data_sector['nombre'];
		$cantidad_rutas = count($data_sector['data']);
		$objReporte->sub_cabecera($nombre_sector, $cantidad_rutas);
		unset($data_sector['nombre']);

		$contador_interno = $subtotal_conexiones = 0;

		foreach ($data_sector['data'] as $key_ruta => $data_ruta) {
			$contador_interno++;
			$nombre_ruta  = $data_ruta['nombre'];

			if($contador_interno == 1){
				$objReporte->sub_cabecera($nombre_ruta, '', 1);
			}else{
				$objReporte->sub_cabecera($nombre_ruta, '', 2);
			}

			unset($data_sector['nombre']);

			$leidos_activas_con_medidor      = $data_ruta['data']['1']['data']['1']['data']['leidos'];
			$promediados_activas_con_medidor = $data_ruta['data']['1']['data']['1']['data']['promediado'];
			$subtotal_activas_con_medidor    = $leidos_activas_con_medidor + $promediados_activas_con_medidor;

			$leidos_activas_sin_medidor      = $data_ruta['data']['1']['data']['0']['data']['leidos'];
			$promediados_activas_sin_medidor = $data_ruta['data']['1']['data']['0']['data']['promediado'];
			$subtotal_activas_sin_medidor    = $leidos_activas_sin_medidor + $promediados_activas_sin_medidor;

			$leidos_inactivas_sin_medidor     = $data_ruta['data']['0']['data']['0']['data']['leidos'];
			$promediado_inactivas_sin_medidor = $data_ruta['data']['0']['data']['0']['data']['promediado'];
			$leidos_inactivas_con_medidor     = $data_ruta['data']['0']['data']['1']['data']['leidos'];
			$promediado_inactivas_con_medidor = $data_ruta['data']['0']['data']['1']['data']['promediado'];
			$subtotal_inactivas               = $leidos_inactivas_sin_medidor+$promediado_inactivas_sin_medidor+$leidos_inactivas_con_medidor+$promediado_inactivas_con_medidor;

			$sub_total_fila = $subtotal_activas_con_medidor + $subtotal_activas_sin_medidor + $subtotal_inactivas;

			$objReporte->contenido($leidos_activas_con_medidor, $promediados_activas_con_medidor, $subtotal_activas_con_medidor, $subtotal_activas_sin_medidor, $subtotal_inactivas, $sub_total_fila);

			$subtotal_conexiones += $sub_total_fila;
		}

		$objReporte->sub_footer($subtotal_conexiones);


	}

	$objReporte->Output();

?>
