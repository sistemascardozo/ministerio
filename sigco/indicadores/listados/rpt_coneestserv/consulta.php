<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../config.php");

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();

    $sqlLect1 = " SELECT nomtar FROM  facturacion.tarifas WHERE codemp = ? AND codsuc = ? ORDER BY catetar;";
    $consultaL1 = $conexion->prepare($sqlLect1);
    $consultaL1->execute(array($codemp,$codsuc));
    $catetares = $consultaL1->fetchAll();
    
    $Sql = "( 
            SELECT cab.codtiposervicio ||''|| cab.catetar ||''|| 1 as catetar, 
            tar.nomtar as nomtar, 
            count(cab.*) as cantidad,
            1 as orden 
            FROM facturacion.cabfacturacion cab 
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar) 
            WHERE cab.codsuc = {$codsuc} 
            AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
            AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' 
            AND cab.codestadoservicio = 1
            AND cab.codtipousuario = 1 
            GROUP BY cab.codtiposervicio,cab.catetar, tar.nomtar 
            ORDER BY cab.codtiposervicio, cab.catetar
            ) 
            UNION
            ( 
            SELECT cab.codtiposervicio ||''|| cab.catetar ||''|| 2 as catetar, 
            tar.nomtar as nomtar, 
            count(cab.*) as cantidad,
            2 as orden 
            FROM facturacion.cabfacturacion cab 
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar) 
            WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
            AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' 
            AND cab.codestadoservicio = 2
            AND cab.codtipousuario = 1
            GROUP BY cab.codtiposervicio, cab.catetar, tar.nomtar 
            ORDER BY cab.codtiposervicio,cab.catetar
            ) 
            UNION( 
            SELECT cab.codtiposervicio ||''|| cab.catetar ||''|| 3 as catetar, 
            tar.nomtar as nomtar, 
            count(cab.*) as cantidad,
            3 as orden
            FROM facturacion.cabfacturacion cab 
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar) 
            WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
            AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' 
            AND cab.codestadoservicio > 2 
            AND cab.codtipousuario = 1 
            GROUP BY cab.codtiposervicio,cab.catetar, tar.nomtar 
            ORDER BY cab.codtiposervicio, cab.catetar
            ) 
            ORDER BY catetar,orden";

    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();


    // Construccion de Cabecera
    $cabecera  = array();  
    $cabcuerpo = array();
    $h         = 1;  

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['cantidad']);
        else:
            array_push($cabecera[$value['catetar']],$value['cantidad']);            
        endif;

    endforeach;

    for ($i=1; $i <= 3; $i++):
        
        for ($g=1; $g <=5; $g++):
            
            for ($h=1; $h <=3; $h++):            

                $indice = $i.$g.$h;

                if (array_key_exists($indice, $cabecera)):
                    $nombre = $cabecera[$indice][0];
                    $import = $cabecera[$indice][1];
                else:
                    $nombre = $catetares[($g-1)]['nomtar'];
                    $import = 0;
                endif;

                if (!array_key_exists($g, $cabcuerpo)):
                    $cabcuerpo[$g] = array();
                    array_push($cabcuerpo[$g], $nombre,$import);
                else:
                    array_push($cabcuerpo[$g],$import);            
                endif;

            endfor;

        endfor;
    endfor;

    // var_dump($cabcuerpo);exit;
?>
<center> 
<table border="1" aling="center" width="800">
    <thead>
        <tr>
            <td align="center" colspan="2">Categorias</td>
            <td align="center" colspan="9">Numero de Conexiones</td>
            <td align="center" rowspan="3" colspan="2">Total</td>
        </tr>
        <tr>
            <td align="center" width="5%" rowspan="3">N°</td>
            <td align="center" width="5%" rowspan="3">Tarifas</td>
            <td align="center" width="5%" colspan="9">Estado de Servicio</td>
        </tr>
        <tr>
            <td align="center" colspan="3">Agua y Desague</td>
            <td align="center" colspan="3">Agua</td>
            <td align="center" colspan="3">Desague</td>
        </tr>   
        <tr>
            <td align="center">Activos</td>
            <td align="center">Cerrados</td>
            <td align="center">Otros</td>
            <td align="center">Activos</td>
            <td align="center">Cerrados</td>
            <td align="center">Otros</td>
            <td align="center">Activos</td>
            <td align="center">Cerrados</td>
            <td align="center">Otros</td>
            <td align="center">Total Agua Activas</td>
            <td align="center">Total Des. Activas</td>
        </tr>   
    </thead>
    <tbody>
        <?php 
        $subtotal1  = 0;
        $subtotal2  = 0;
        $subtotal3  = 0;
        $subtotal4  = 0;
        $subtotal5  = 0;
        $subtotal6  = 0;
        $subtotal7  = 0;
        $subtotal8  = 0;
        $subtotal9  = 0;
        
        $subtotal10 = 0;        
        $subtotal11 = 0;

        $totalagua = 0;
        $totaldesa = 0;
        
        $contador  = 0;
        foreach ($cabcuerpo as $key => $value): 

            $contador++;
            $subtotal10 = $value[1] + $value[4];
            $subtotal11 = $value[1] + $value[7];

        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?= $value[1] = empty($value[1]) ? 0 : $value[1];?></td>
                <td align="right"><?= $value[2] = empty($value[2]) ? 0 : $value[2];?></td>
                <td align="right"><?= $value[3] = empty($value[3]) ? 0 : $value[3];?></td>
                <td align="right"><?= $value[4] = empty($value[4]) ? 0 : $value[4];?></td>
                <td align="right"><?= $value[5] = empty($value[5]) ? 0 : $value[5];?></td>
                <td align="right"><?= $value[6] = empty($value[6]) ? 0 : $value[6];?></td>
                <td align="right"><?= $value[7] = empty($value[7]) ? 0 : $value[7];?></td>
                <td align="right"><?= $value[8] = empty($value[8]) ? 0 : $value[8];?></td>
                <td align="right"><?= $value[9] = empty($value[9]) ? 0 : $value[9];?></td>
                <td align="right"><?= $subtotal10 = empty($subtotal10) ? 0 : $subtotal10;?></td>
                <td align="right"><?= $subtotal11 = empty($subtotal11) ? 0 : $subtotal11;?></td>
            </tr>    
        <?php

            $subtotal1  += $value[1];
            $subtotal2  += $value[2];
            $subtotal3  += $value[3];
            $subtotal4  += $value[4];
            $subtotal5  += $value[5];
            $subtotal6  += $value[6];
            $subtotal7  += $value[7];
            $subtotal8  += $value[8];
            $subtotal9  += $value[9];
            $totalagua  += $subtotal10;
            $totaldesa  += $subtotal11;

        endforeach;
        ?>
        <tr style="font-weight: bold">
            <td colspan="2" >Total</td>
            <td align="right"><?=$subtotal1;?></td>
            <td align="right"><?=$subtotal2;?></td>
            <td align="right"><?=$subtotal3;?></td>
            <td align="right"><?=$subtotal4;?></td>
            <td align="right"><?=$subtotal5;?></td>
            <td align="right"><?=$subtotal6;?></td>
            <td align="right"><?=$subtotal7;?></td>
            <td align="right"><?=$subtotal8;?></td>
            <td align="right"><?=$subtotal9;?></td>
            <td align="right"><?=$totalagua;?></td>
            <td align="right"><?=$totaldesa;?></td>
        </tr>
    </tbody>
</table>
</center> 

