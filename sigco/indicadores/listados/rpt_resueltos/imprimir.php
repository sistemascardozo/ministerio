<?php 
	if(!session_start()){session_start();} 

	include("../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsReclamos extends clsReporte
	{
		function Header() {

            global $codsuc,$meses;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral();
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RECLAMOS COMERCIALES RESUELTOS COMO1A";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,"INFORME : ". $periodo["mes"] . " - " . $periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim,$Dim1,$Dim2,$Dim3;		

			$this->Ln(5);
			$this->SetFillColor(255,255,255);
			$this->SetTextColor(0);
			$h=5;

			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			// Cabecera Principal
			$this->SetFont('Arial','',7);
			$this->Cell($Dim[1],$h,utf8_decode('Datos del Reclamo'),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode('Primera Instancia'),1,0,'C',false);
			$this->Cell($Dim[3],$h,utf8_decode('Recurso de Reconsideracion'),1,1,'C',false);
			
			// Cabecera Secundaria
			// 1
			$this->Cell($Dim1[1],$h,utf8_decode('N° Exped'),1,0,'C',false);
			$this->Cell($Dim1[2],$h,utf8_decode('N° Sumin.'),1,0,'C',false);
			$this->Cell($Dim1[3],$h,utf8_decode('F.Ingreso'),1,0,'C',false);
			$this->Cell($Dim1[4],$h,utf8_decode('Usuario'),1,0,'C',false);
			$this->Cell($Dim1[5],$h,utf8_decode('Tipo de Reclamo'),1,0,'C',false);

			// 2
			$this->Cell($Dim2[1],$h,utf8_decode('F Resoluc.'),1,0,'L',false);
			$this->Cell($Dim2[2],$h,utf8_decode('N° Dictamen Res.'),1,0,'L',false);
			$this->Cell($Dim2[3],$h,utf8_decode('F.Not'),1,0,'L',false);
			$this->Cell($Dim2[4],$h,utf8_decode('Tipo Solución'),1,0,'L',false);

			// 3
			$this->Cell($Dim3[1],$h,utf8_decode('N° Exped'),1,0,'L',false);
			$this->Cell($Dim3[2],$h,utf8_decode('N° Sumin.'),1,0,'L',false);
			$this->Cell($Dim3[3],$h,utf8_decode('F.Ingreso'),1,0,'L',false);
			$this->Cell($Dim3[4],$h,utf8_decode('F.Ingreso'),1,0,'L',false);


		}

		public function contenido($concepto,$valor,$res = 0){
			global $Dim;
			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','B',8);

			switch ($res) {
				case 0:
					// $this->Ln(2);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,0,'R',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					
					break;
				case 1:
					$this->Ln(1);
					$this->SetTextColor(146,14,14); //Color de texto
					$this->Cell($Dim[3],$h,utf8_decode($concepto),0,1,'L',true);
					break;
				case 2:
					$this->SetTextColor(0);
					$this->Cell($Dim[3],$h,utf8_decode("____________________________________________________________________________________________________________"),0,1,'L',true);
					$this->Ln(5);
					$this->SetFont('Arial','B',6);
					$this->Cell($Dim[1],$h,utf8_decode("HORA INICIAL        " . $concepto),0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("HORA FINAL          " . $valor),0,1,'C',true);
					break;
				case 3:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("__________________"),0,1,'R',true);
					
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
				case 4:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("_________________________"),0,1,'R',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
			}

		}
	}

	// Principal
    $Dim = array('1'=>120,'2'=>100,'3'=>72);
	
	// Sec 1
    $Dim1 = array('1'=>15,'2'=>15,'3'=>15,'4'=>40,'5'=>35);
	// Sec 2
    $Dim2 = array('1'=>25,'2'=>25,'3'=>25,'4'=>25);
	// Sec 3
    $Dim3 = array('1'=>18,'2'=>18,'3'=>18,'4'=>18);


	$codemp     = 1;
	$anio       = $_GET["Desde"];
	$mes        = $_GET["Hasta"];
	$codsuc     = $_SESSION['IdSucursal'];
	$annodelmes = date("Y", $_GET["Desde"]);
	$mesdelmes = date("m", $_GET["Desde"]);
	
	$objReporte = new clsReclamos("L");

	$sqlLect = "SELECT c.descripcion AS concepto,
				det.codconcepto AS codconcepto, 
				c.codtipoconcepto AS codtipoconcepto, 
				SUM(det.importe - (importeacta + importerebajado)) as importe,
				det.categoria AS categoria, 
				det.estadofacturacion AS estado 
				FROM facturacion.detctacorriente det
				INNER JOIN facturacion.conceptos c ON (det.codemp = c.codemp AND det.codsuc = c.codsuc AND det.codconcepto = c.codconcepto)
				WHERE det.codemp = ? AND det.codsuc = ? AND det.codciclo = ? AND det.periodo = ?
				GROUP BY c.descripcion, c.codtipoconcepto, det.codconcepto, det.categoria, det.estadofacturacion 
				ORDER BY det.codconcepto ASC";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp, $codsuc, $ciclo, $periodo));
	$items = $consultaL->fetchAll();
	
	$contador = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(3);
    $objReporte->SetAutoPageBreak(true,5);
	$objReporte->AddPage('H');
	
	$ap       = 0;
	$des      = 0;
	$id       = 0;
	$igv      = 0;
	$ar       = 0;
	$cotsup   = 0;
	$newarray = array();


	// $objReporte->contenido("NO EXISTE DATOS EN EL REPORTE","",1);

	$objReporte->Output();
	
?>