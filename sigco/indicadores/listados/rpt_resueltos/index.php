<?php 
   if(!session_start()){session_start();}
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "RPT. RECLAMOS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>

<script type="text/javascript" src="<?=$urldir?>js/funciones.js" language="JavaScript"></script>
<script>

jQuery(function($)
	{ 
		$( "#DivTipos" ).buttonset();

		var dates = $( "#FechaDesde, #FechaHasta" ).datepicker({
			//defaultDate: "+1w",
			buttonText: 'Selecione Fecha de Busqueda',
			showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/calendar.png',
			buttonImageOnly: true,
			//showOn: 'both',
			showButtonPanel: true,
			numberOfMonths: 2,
			onSelect: function( selectedDate ) {
				var option = this.id == "FechaDesde" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	});
	var urldir 	= "<?=$urldir?>" 
	var codsuc 	= <?=$codsuc?>
	
function ValidarForm(Op)
{
		var Desde = $("#FechaDesde").val()
		var Hasta = $("#FechaHasta").val()
		if (Trim(Desde)=='')
		{
			
			Msj('#FechaDesde','Seleccione la Fecha Inicial',1000,'above','',false)
			return false;
			
		}
		if (Trim(Hasta)=='')
		{
			
			Msj('#FechaHasta','Seleccione la Fecha Final',1000,'above','',false)
			return false;
			
		}
		Fechas = 'Desde='+Desde+'&Hasta='+Hasta

		if(document.getElementById("pdfresumen").checked==true)
		{
			url="imprimir.php";
		}


		url += "?"+Fechas;
		AbrirPopupImpresion(url,800,600);
		return false;
}	

function Cancelar(){
	location.href='<?=$urldir?>/admin/indexB.php'
}
</script>
<div align="center">
	<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
	    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
	      	<tbody>
	    		<tr>
	        		<td colspan="2">
						<fieldset style="padding:4px">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="left">Sucursal</td>
									<td align="center">:</td>
									<td>
									  <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
									</td>
								    <td align="left">Fecha Reg.</td>
								    <td align="center">:</td>
				    				<td>
                      					<input type="text" class="inputtext" id="FechaDesde" size="9" maxlength="10" value="<?=$Fecha?>" />
										-
										<input type="text" class="inputtext" id="FechaHasta" size="9" maxlength="10" value="<?=$Fecha?>" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
							       	<td colspan="8" align="center"> 
							       		<div id="DivTipos" style="display:inline">
					                       	<input checked="checked" type="radio" name="pdfresumen" id="pdfresumen" value="radio" />
	            				           	<label for="pdfresumen">PDF</label>
							       		</div>
							       		<input type="button" onclick="return ValidarForm();" value="Generar" id="consul_id">
							       	</td>
									<input type="hidden" name="anio" id="anio" value="<?=date('Y')?>">
									<input type="hidden" name="mes" id="mes" value="<?=date('m')?>">
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<div id="consulta_resultados">
	
						</div>
					</td>
				</tr>
			</tbody>
	    </table>
 	</form>
</div>
<?php CuerpoInferior(); ?>