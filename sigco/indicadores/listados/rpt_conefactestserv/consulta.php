<?php 
    if(!session_start()){session_start();}

    include("../../../../config.php");

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    $mes      = $_POST['mes'];
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];
    
    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    
    $Sql = "( SELECT cab.codsuc, cab.codestadoservicio as codestadoservicio, 
        cab.catetar as catetar,
        tar.nomtar as nomtar,  
        count(cab.*) as cantidad 
        FROM facturacion.cabfacturacion cab
        INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
        WHERE cab.codsuc = {$codsuc}
        AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
        AND cab.anio = '{$anio}' 
        AND cab.mes = '{$mes}' 
        AND cab.codestadoservicio = 1
        AND cab.nrodocumento > 0
        GROUP BY cab.codsuc, cab.codestadoservicio, cab.catetar, tar.nomtar
        )
        UNION(
            SELECT cab.codsuc as codsuc, cab.codestadoservicio as codestadoservicio, 
            cab.catetar as catetar,
            tar.nomtar as nomtar,  
            count(cab.*) as cantidad 
            FROM facturacion.cabfacturacion cab
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE cab.codsuc = {$codsuc} AND cab.nrofacturacion = {$nrofact['nrofacturacion']}
            AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' 
            AND cab.codestadoservicio = 2
            AND cab.nrodocumento > 0 
            GROUP BY cab.codsuc, cab.codestadoservicio, cab.catetar, tar.nomtar
        )
        UNION(
            SELECT cab.codsuc as codsuc, cab.codestadoservicio as codestadoservicio, 
            cab.catetar as catetar,
            tar.nomtar as nomtar,  
            count(cab.*) as cantidad 
            FROM facturacion.cabfacturacion cab
            INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
            WHERE cab.codsuc = {$codsuc} 
            AND cab.nrofacturacion = {$nrofact['nrofacturacion']}
            AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' 
            AND cab.codestadoservicio > 2
            AND cab.nrodocumento > 0
            GROUP BY cab.codsuc, cab.codestadoservicio, cab.catetar, tar.nomtar
        )
        ORDER BY codsuc, codestadoservicio, catetar";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['catetar'], $cabecera)):
            $cabecera[$value['catetar']] = array();
            array_push($cabecera[$value['catetar']], $value['nomtar'],$value['cantidad']);
        else:
            array_push($cabecera[$value['catetar']],$value['cantidad']);            
        endif;

    endforeach;
?>
<center> 
<table border="1" aling="center" width="500">
    <thead>
        <tr>
            <td align="center" colspan="2">Categorias</td>
            <td align="center" colspan="3">Numero de Conexiones</td>
            <td align="center" rowspan="3">Total</td>
        </tr>
        <tr>
            <td align="center" width="5%" rowspan="3">N°</td>
            <td align="center" width="5%" rowspan="3">Tarifas</td>
            <td align="center" width="5%" colspan="3">Facturadas</td>
        </tr>
        <tr>
            <td align="center">Activos</td>
            <td align="center">Cerrados</td>
            <td align="center">Otros</td>
        </tr>   
    </thead>
    <tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal6 = 0;
        
        $subtotal7 = 0;
        
        $contador  = 0;
        foreach ($cabecera as $key => $value): 

            $contador++;
            $subtotal6 = $value[1] + $value[2]+$value[3];
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?= $value[1] = empty($value[1]) ? 0 : $value[1];?></td>
                <td align="right"><?= $value[2] = empty($value[2]) ? 0 : $value[2];?></td>
                <td align="right"><?= $value[3] = empty($value[3]) ? 0 : $value[3];?></td>
                <td align="right"><?= $subtotal6 = empty($subtotal6) ? 0 : $subtotal6;?></td>
            </tr>    
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal7 += $value[1]+$value[2]+$value[3];
            endforeach;?>
        <tr style="font-weight: bold">
            <td colspan="2" >Total</td>
            <td align="right"><?=$subtotal1;?></td>
            <td align="right"><?=$subtotal2;?></td>
            <td align="right"><?=$subtotal3;?></td>
            <td align="right"><?=$subtotal7;?></td>
        </tr>
    </tbody>
</table>
</center> 

