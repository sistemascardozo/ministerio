<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "RPT. RECLAMOS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>

jQuery(function($)
	{ 
		$( "#DivTipos" ).buttonset();

		$("#FechaDesde").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                        showButtonPanel: true
            }
        );
        $("#FechaHasta").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                        showButtonPanel: true
            }
        );
	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc 	= <?=$codsuc?>
	
function ValidarForm(Op)
{
		var Desde = $("#FechaDesde").val()
		var Hasta = $("#FechaHasta").val()
		if (Trim(Desde)=='')
		{
			
			Msj('#FechaDesde','Seleccione la Fecha Inicial',1000,'above','',false)
			return false;
			
		}
		if (Trim(Hasta)=='')
		{
			
			Msj('#FechaHasta','Seleccione la Fecha Final',1000,'above','',false)
			return false;
			
		}
		Fechas = 'Desde='+Desde+'&Hasta='+Hasta

		$.ajax({
			url:'consulta.php',
			type:'POST',
		 	async:true,
		 	data:Fechas,
 		 	success:function(datos){
				$("#consulta_resultados").html(datos)
	 		}
    	});
}

function ImprimirPDF(Op)
{
		var Desde = $("#FechaDesde").val()
		var Hasta = $("#FechaHasta").val()
		if (Trim(Desde)=='')
		{
			
			Msj('#FechaDesde','Seleccione la Fecha Inicial',1000,'above','',false)
			return false;
			
		}
		if (Trim(Hasta)=='')
		{
			
			Msj('#FechaHasta','Seleccione la Fecha Final',1000,'above','',false)
			return false;
			
		}
		Fechas = 'Desde='+Desde+'&Hasta='+Hasta
		url="imprimir.php";

		url += "?"+Fechas;
		AbrirPopupImpresion(url,800,600);
		return false;
}
function ImprimirReclamosRegistrados(Op)
{
		var Desde = $("#FechaDesde").val()
		var Hasta = $("#FechaHasta").val()
		if (Trim(Desde)=='')
		{
			
			Msj('#FechaDesde','Seleccione la Fecha Inicial',1000,'above','',false)
			return false;
			
		}
		if (Trim(Hasta)=='')
		{
			
			Msj('#FechaHasta','Seleccione la Fecha Final',1000,'above','',false)
			return false;
			
		}
		Fechas = 'Desde='+Desde+'&Hasta='+Hasta
		url="imprimir_reclamo.php";

		url += "?"+Fechas;
		AbrirPopupImpresion(url,800,600);
		return false;
}	
function Excel()
{
	var Desde = $("#FechaDesde").val()
		var Hasta = $("#FechaHasta").val()
		if (Trim(Desde)=='')
		{
			
			Msj('#FechaDesde','Seleccione la Fecha Inicial',1000,'above','',false)
			return false;
			
		}
		if (Trim(Hasta)=='')
		{
			
			Msj('#FechaHasta','Seleccione la Fecha Final',1000,'above','',false)
			return false;
			
		}
		Fechas = 'Desde='+Desde+'&Hasta='+Hasta
	url="excel.php";
	url += "?"+Fechas;
	AbrirPopupImpresion(url,800,600);
		return false;
}
function Cancelar(){
	location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
}
</script>
<div align="center">
	<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
	    <table width="1000" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
	      	<tbody>
	    		<tr>
	        		<td colspan="2">
						<fieldset style="padding:4px">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="left">Sucursal</td>
									<td width="30" align="center">:</td>
									<td>
									  <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
									</td>
								    <td align="left">Fecha Reg.</td>
								    <td width="30" align="center">:</td>
				    				<td>
                      					<input type="text" class="inputtext" id="FechaDesde" maxlength="10" value="<?=$Fecha?>" style="width:80px" />
										-
										<input type="text" class="inputtext" id="FechaHasta" maxlength="10" value="<?=$Fecha?>" style="width:80px" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
							       	<td colspan="3" align="center"> 
							       		<input type="button" onclick="return ValidarForm();" value="Consultar" id="consul_id">
							       		<input type="button" onclick="return ImprimirPDF();" value="PDF" id="imprimirpdf">
							       		<input type="button" onclick="return ImprimirReclamosRegistrados();" value="RECLAMOS" id="imprimirreclamos">
							       		<input type="button" onclick="return Excel();" value="Excel" id="ExcelId">
							       	</td>
								</tr>
								<tr><td>&nbsp;</td>
							</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<div id="consulta_resultados">
	
						</div>
					</td>
				</tr>
			</tbody>
	    </table>
 	</form>
</div>
<?php CuerpoInferior(); ?>