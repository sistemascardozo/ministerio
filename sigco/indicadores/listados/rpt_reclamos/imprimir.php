<?php
	include("../../../../objetos/clsReporte.php");
    $clfunciones = new clsFunciones();

	class clsCriticas extends clsReporte
	{
		function Header() {

            global $codsuc,$meses,$desde;
			global $Dim;
			
			$periodo = $this->DecFechaLiteral($desde);
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "RECLAMOS POR TIPO";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
			$this->Cell(277,5,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
			global $Dim;		

			$this->Ln(5);
			$this->SetFillColor(18,157,176);
			$this->SetTextColor(255);
			$h=5;
			$h1= 10;

			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			$this->SetFont('Arial','B',9);
			$this->Cell($Dim[1],$h,utf8_decode('Tipo de Reclamo'),1,0,'C',true);
			$this->Cell($Dim[2],$h1,utf8_decode('Recibidos'),1,0,'C',true);
			$this->Cell($Dim[3],$h1,utf8_decode('Aten.'),1,0,'C',true);
			$this->Cell($Dim[4],$h1,utf8_decode('Pendientes del Mes Atn.'),1,0,'C',true);
			$this->Cell($Dim[5],$h1,utf8_decode('Pendientes al Mes'),1,0,'C',true);
            $this->Cell($Dim[6],$h1,utf8_decode('Fund. con Reclamos Atn.'),1,0,'C',true);
            $this->Cell($Dim[7],$h1,utf8_decode('Atn. dent. plz.'),1,0,'C',true);
            $this->Cell($Dim[8],$h1,utf8_decode('Atn. fuera plz.'),1,1,'C',true);

			$this->SetY($this->GetY()-$h);
			$this->Cell($Dim[10],$h,utf8_decode('N°'),1,0,'C',true);
			$this->Cell($Dim[11],$h,utf8_decode('Conceptos'),1,1,'C',true);


		}

        function Contenido($cont,$descripcion, $num1, $num2, $num3, $num4, $num5, $num6, $num7, $subtotal,$dat = 0)
        {
			global $Dim;

			switch ($dat) {
				case 1:

					$this->SetFillColor(7,96,125);
					$this->SetTextColor(255);
					$h = 10;
					$this->SetFont('Arial','B',9);
					$this->Cell($Dim[1],$h,utf8_decode($descripcion),1,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode($num1),1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($num2),1,0,'C',true);
					$this->Cell($Dim[4],$h,utf8_decode($num3),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($num4),1,0,'C',true);
					$this->Cell($Dim[6],$h,utf8_decode($num5),1,0,'C',true);
					$this->Cell($Dim[7],$h,utf8_decode($num6),1,0,'C',true);
					$this->Cell($Dim[8],$h,utf8_decode($num7),1,1,'C',true);
					// $this->Cell($Dim[9],$h,utf8_decode($subtotal),1,1,'C',true);

					break;
				
				default:

					$this->SetFillColor(255,255,255); //Color de Fondo
					$this->SetTextColor(0);
					$h = 10;
					$this->SetFont('Arial','',7);
					$this->Cell($Dim[10],$h,utf8_decode($cont),1,0,'C',true);
					$this->Cell($Dim[11],$h,utf8_decode($descripcion),1,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($num1),1,0,'C',true);
					$this->Cell($Dim[3],$h,utf8_decode($num2),1,0,'C',true);
					$this->Cell($Dim[4],$h,utf8_decode($num3),1,0,'C',true);
					$this->Cell($Dim[5],$h,utf8_decode($num4),1,0,'C',true);
					$this->Cell($Dim[6],$h,utf8_decode($num5),1,0,'C',true);
					$this->Cell($Dim[7],$h,utf8_decode($num6),1,0,'C',true);
					$this->Cell($Dim[8],$h,utf8_decode($num7),1,1,'C',true);
					// $this->Cell($Dim[9],$h,utf8_decode($subtotal),1,1,'C',true);
					break;
			}

		}
    }

    $Dim = array('1'=>85,'2'=>15,'3'=>15,'4'=>40,'5'=>30,'6'=>40,'7'=>25,'8'=>25,'9'=>15,'10'=>8,'11'=>77);

	// Actual
    $desde  = $_GET["Desde"];
    $hasta  = $_GET["Hasta"];
	$codsuc = $_SESSION["IdSucursal"];
	// Anterior
    $mesdelmes = date("m", strtotime(str_replace("/","-",$desde)));
    $aniodelmes = date("Y", strtotime(str_replace("/","-",$desde)));
    $periodo=$aniodelmes.$mesdelmes;
    $mesant=intval($mesdelmes)-1;
    $anioant=$aniodelmes;
    if(intval($mesant)==0)
    {
        $mesant='12';
        $anioant=intval($aniodelmes)-1;
    }
    if(intval($mesant)<10)
        $mesant = '0'.$mesant;

    $ultimodiamesant=$anioant.'-'.$mesant.'-'.date ('t', mktime (0,0,0, $mesant, 1, $anioant));
    
    // Tipo de Reclamos o Sub-Categoria
    $Sql        = " SELECT codconcepto,descripcion FROM reclamos.conceptosreclamos 
                    WHERE tiporeclamo = 1
                    ORDER BY codconcepto";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['codconcepto'], $cabecera)):

             // Recibido
            $sql = "SELECT count(*) as recibido
            FROM reclamos.reclamos 
            WHERE codestadoreclamo <> 10
            AND codconcepto = {$value['codconcepto']} 
            AND fechareg BETWEEN '{$desde}' AND '{$hasta}'
             ";
            $consulta = $conexion->query($sql);
            $valor    = $consulta->fetch();

            // Atendido
           $sql1 = "SELECT count(*) as atendido
                FROM reclamos.reclamos 
                WHERE codconcepto = {$value['codconcepto']} 
                AND fechareg BETWEEN '{$desde}' AND '{$hasta}'
                AND codestadoreclamo <> 10
                AND fecha_finalizacion BETWEEN '{$desde}' 
                AND '{$hasta}' /*AND fecha_finalizacion<>fechareg*/
                /*AND periodo='{$periodo}'*/";
            $consulta1 = $conexion->query($sql1);
            $valor1    = $consulta1->fetch();

            // Pendientes del Mes Anterior
           $sql2 = "SELECT count(*) as antmespendiente
            FROM reclamos.reclamos
            WHERE codconcepto = {$value['codconcepto']}
            AND codestadoreclamo NOT IN (10) 
            /*AND fin_reclamo = 0*/
            AND extract(month from fechareg) = '{$mesant}'
            /*AND periodo='{$periodo}'*/
            AND fecha_finalizacion > '{$ultimodiamesant}'"; 
            $consulta2 = $conexion->query($sql2);
            $valor2    = $consulta2->fetch();

            // Pendientes al Mes
            $sql3 = "SELECT count(*) as mespendiente
            FROM reclamos.reclamos
            WHERE codconcepto = {$value['codconcepto']}
            AND codestadoreclamo NOT IN (10) 
             AND extract(month from fechareg) = '{$mesdelmes}'
            /*AND periodo='{$periodo}'*/
            AND fecha_finalizacion >'{$hasta}'";
            $consulta3 = $conexion->query($sql3);
            $valor3    = $consulta3->fetch();

            // Fundados con reclamos Atendidos
             $sql4 = "SELECT count(*) as fundadostendidos
                FROM reclamos.reclamos 
                WHERE codconcepto = {$value['codconcepto']} 
                AND fin_reclamo = 1 AND fundado = 1
                AND fecha_finalizacion BETWEEN '{$desde}' 
                AND '{$hasta}'
                /*AND periodo='{$periodo}'*/";

            $consulta4 = $conexion->query($sql4);
            $valor4    = $consulta4->fetch();

            // Atendidos dentro del plazo
            $sql5 = "SELECT dentroplazo({$value['codconcepto']},'{$desde}','{$hasta}')";
            $consulta5 = $conexion->query($sql5);
            $valor5    = $consulta5->fetch();

            // Atendidos fuera del plazo
            $sql6 = "SELECT fueraplazo({$value['codconcepto']},'{$desde}','{$hasta}')";
            $consulta6 = $conexion->query($sql6);
            $valor6    = $consulta6->fetch();

            if( !empty($valor['recibido']) || !empty($valor1['atendido']) || !empty($valor2['antmespendiente']) || !empty($valor3['mespendiente']) || !empty($valor4['fundadostendidos']) || !empty($valor5['fundadostendidos']) || !empty($valor6['fundadostendidos']) ):
                $cabecera[$value['codconcepto']] = array();
                array_push($cabecera[$value['codconcepto']], $value['descripcion'],$valor['recibido'],$valor1['atendido'],$valor2['antmespendiente'],$valor3['mespendiente'],$valor4['fundadostendidos'],$valor5[0],$valor6[0]);
            endif;      

        endif;

    endforeach;
	
	$objReporte =	new clsCriticas('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(5);
    $objReporte->SetAutoPageBreak(true,5);
	$objReporte->AddPage('H');

    $subtotal1 = 0;
    $subtotal2 = 0;
    $subtotal3 = 0;
    $subtotal4 = 0;
    $subtotal5 = 0;
    $subtotal6 = 0;
    $subtotal7 = 0;
    $subtotal8 = 0;
    $contador  = 0;

    foreach ($cabecera as $key => $value): 
        $contador++;
        $subtotal1 += $value[1];
        $subtotal2 += $value[2];
        $subtotal3 += $value[3];
        $subtotal4 += $value[4];
        $subtotal5 += $value[5];
        $subtotal6 += $value[6];
        $subtotal7 += $value[7];
        $subtotal8 += $value[1]+$value[2]+$value[3]+$value[4]+$value[5]+$value[6]+$value[7];
		$objReporte->contenido($contador,strtoupper($value[0]),$value[1],$value[2], $value[3], $value[4], $value[5], $value[6], $value[7],($value[1]+$value[2]+$value[3]+$value[4]+$value[5]+$value[6]+$value[7]));
    endforeach;

	$objReporte->contenido('',strtoupper('TOTAL'),$subtotal1,$subtotal2,$subtotal3,$subtotal4,$subtotal5,$subtotal6,$subtotal7,$subtotal8,1);

	$objReporte->Output();	
		
?>