<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsFunciones.php");
    $objFunciones = new clsFunciones();
    $desde  =  $objFunciones->CodFecha($_GET["Desde"]);
    $hasta  = $objFunciones->CodFecha($_GET["Hasta"]);
  $codsuc = $_SESSION["IdSucursal"];
  header("Content-type: application/vnd.ms-excel; name='excel'");  
  header("Content-Disposition: filename=Reclamos.xls");  
  header("Pragma: no-cache");  
  header("Expires: 0"); 
 $Sql="SELECT 
  to_char(r.codsuc,'00')||''||TRIM(to_char(r.nroinscripcion,'000000')) as codantiguo,
  c.propietario,
  r.direccion1,
  t.nomtar,
  co.descripcion as motivo,
  r.fechareg,
  r.fin_reclamo,
  r.fecha_finalizacion,
  r.fundado
FROM
  catastro.clientes c
  INNER JOIN reclamos.reclamos r ON (c.codemp = r.codemp)
  AND (c.codsuc = r.codsuc)
  AND (c.nroinscripcion = r.nroinscripcion)
  INNER JOIN catastro.unidadesusoclientes uc ON (c.codemp = uc.codemp)
  AND (c.codsuc = uc.codsuc)
  AND (c.nroinscripcion = uc.nroinscripcion)
  INNER JOIN facturacion.tarifas t ON (uc.codemp = t.codemp)
  AND (uc.codsuc = t.codsuc)
  AND (uc.catetar = t.catetar)
  INNER JOIN reclamos.conceptosreclamos co ON (r.codconcepto = co.codconcepto)
  WHERE r.codsuc=".$codsuc." AND uc.principal=1 AND 
  r.fechareg between '".$desde."' AND '".$hasta."'
  ORDER BY r.fechareg";
  ?>
  <table border=1>
    <tr>
      <td>C&oacute;digo</td>
      <td>Nombre</td>
      <td>Direcci&oacute;n</td>
      <td>Categoria</td>
      <td>Motivo</td>
      <td>Fecha Recepci&oacute;n</td>
      <td>Fecha de Atenci&oacute;n</td>
      <td>Forma de Resoluci&oacute;n</td>
     
    </tr>
  <?php
  $i=0;
  foreach($conexion->query($Sql)->fetchAll() as $row)
  {
    $i++;
     $ft='';
    if($row['fin_reclamo']==1)
        $ft=$objFunciones->DecFecha($row['fecha_finalizacion']);
    
    if($row['fundado']==1)
      $r=utf8_decode('CONCILIACIÓN');
    else
    {
      if($row['fin_reclamo']==1)
        $r=utf8_decode('RESOLUCIÓN');
    }

    ?>
    <tr>
      <td><?=$row['codantiguo']?></td>
      <td><?=$row['propietario']?></td>
      <td><?=$row['direccion1']?></td>
      <td><?=$row['nomtar']?></td>
      <td><?= strtoupper($row['motivo'])?></td>
      <td><?=$objFunciones->DecFecha($row['fechareg'])?></td>
      <td><?=$ft?></td>
      <td><?=$r?></td>
    </tr>
    <?php

  }
  echo '</table>';
?>
<tr>
  <td colspan=8>Total Reclamos <?=$i?></td>
</tr>
</table>