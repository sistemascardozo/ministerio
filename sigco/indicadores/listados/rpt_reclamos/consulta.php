<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    ini_set('display_errors', 1);
    
    include("../../../../objetos/clsFunciones.php");
    $objFunciones = new clsFunciones();

    // Actual
    $desde  =  $objFunciones->CodFecha($_POST["Desde"]);
    $hasta  = $objFunciones->CodFecha($_POST["Hasta"]);
	$codsuc = $_SESSION["IdSucursal"];
    // Anterior
    $mesdelmes = date("m", strtotime(str_replace("/","-",$desde)));
    $aniodelmes = date("Y", strtotime(str_replace("/","-",$desde)));
    $periodo=$aniodelmes.$mesdelmes;
    $mesant=intval($mesdelmes)-1;
    $anioant=$aniodelmes;
    if(intval($mesant)==0)
    {
        $mesant='12';
        $anioant=intval($aniodelmes)-1;
    }
    if(intval($mesant)<10)
        $mesant = '0'.$mesant;

    $ultimodiamesant=$anioant.'-'.$mesant.'-'.date ('t', mktime (0,0,0, $mesant, 1, $anioant));
    // Tipo de Reclamos o Sub-Categoria
    $Sql        = " SELECT codconcepto,descripcion 
                    FROM reclamos.conceptosreclamos 
                    WHERE tiporeclamo = 1
                    ORDER BY codconcepto";
    $Consulta   = $conexion->query($Sql);
    $row        = $Consulta->fetchAll();

    // Construccion de Cabecera
    $cabecera = array();    

    foreach ($row as $value):

        if (!array_key_exists($value['codconcepto'], $cabecera)):

            // Recibido
            $sql = "SELECT count(*) as recibido
            FROM reclamos.reclamos 
            WHERE codestadoreclamo <> 10
            AND codconcepto = {$value['codconcepto']} 
            AND fechareg BETWEEN '{$desde}' AND '{$hasta}'
             ";
            $consulta = $conexion->query($sql);
            $valor    = $consulta->fetch();

            // Atendido
           $sql1 = "SELECT count(*) as atendido
                FROM reclamos.reclamos 
                WHERE codconcepto = {$value['codconcepto']} 
                AND fechareg BETWEEN '{$desde}' AND '{$hasta}'
                AND codestadoreclamo <> 10
                AND fecha_finalizacion BETWEEN '{$desde}' 
                AND '{$hasta}' /*AND fecha_finalizacion<>fechareg*/
                /*AND periodo='{$periodo}'*/";
            $consulta1 = $conexion->query($sql1);
            $valor1    = $consulta1->fetch();

            // Pendientes del Mes Anterior
            $sql2 = "SELECT count(*) as antmespendiente
            FROM reclamos.reclamos
            WHERE codconcepto = {$value['codconcepto']}
            AND codestadoreclamo NOT IN (10) 
            /*AND fin_reclamo = 0*/
            AND extract(month from fechareg) = '{$mesant}'
            /*AND periodo='{$periodo}'*/
            AND fecha_finalizacion > '{$ultimodiamesant}'"; 
            $consulta2 = $conexion->query($sql2);
            $valor2    = $consulta2->fetch();

            // Pendientes al Mes
            $sql3 = "SELECT count(*) as mespendiente
            FROM reclamos.reclamos
            WHERE codconcepto = {$value['codconcepto']}
            AND codestadoreclamo NOT IN (10) 
             AND extract(month from fechareg) = '{$mesdelmes}'
            /*AND periodo='{$periodo}'*/
            AND fecha_finalizacion >'{$hasta}'";
            $consulta3 = $conexion->query($sql3);
            $valor3    = $consulta3->fetch();

            // Fundados con reclamos Atendidos
             $sql4 = "SELECT count(*) as fundadostendidos
                FROM reclamos.reclamos 
                WHERE codconcepto = {$value['codconcepto']} 
                AND fin_reclamo = 1 AND fundado = 1
                AND fecha_finalizacion BETWEEN '{$desde}' 
                AND '{$hasta}'
                /*AND periodo='{$periodo}'*/";

            $consulta4 = $conexion->query($sql4);
            $valor4    = $consulta4->fetch();

            // Atendidos dentro del plazo
            $sql5 = "SELECT dentroplazo({$value['codconcepto']},'{$desde}','{$hasta}')";
            $consulta5 = $conexion->query($sql5);
            $valor5    = $consulta5->fetch();

            // Atendidos fuera del plazo
            $sql6 = "SELECT fueraplazo({$value['codconcepto']},'{$desde}','{$hasta}')";
            $consulta6 = $conexion->query($sql6);
            $valor6    = $consulta6->fetch();

            if( !empty($valor['recibido']) || !empty($valor1['atendido']) || !empty($valor2['antmespendiente']) || !empty($valor3['mespendiente']) || !empty($valor4['fundadostendidos']) || !empty($valor5['fundadostendidos']) || !empty($valor6['fundadostendidos']) ):
                $cabecera[$value['codconcepto']] = array();
                array_push($cabecera[$value['codconcepto']], $value['descripcion'],$valor['recibido'],$valor1['atendido'],$valor2['antmespendiente'],$valor3['mespendiente'],$valor4['fundadostendidos'],$valor5[0],$valor6[0]);
            endif;      

        endif;

    endforeach;

?>
<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tblecturas" rules="all"|>
	<thead class="ui-widget-header">
        <tr>
            <td align="center" colspan="2">Tipo de Reclamo</td>
            <td align="center" rowspan="2">Recibidos</td>
            <td align="center" rowspan="2">Atendidos</td>
            <td align="center" rowspan="2">Pendientes del Mes Anterior</td>
            <td align="center" rowspan="2">Pendientes al Mes</td>
            <td align="center" rowspan="2">Fundados con Reclamos Atendidos</td>
            <td align="center" rowspan="2">Atendidos dentro del Plazo</td>
            <td align="center" rowspan="2">Atendidos Fuera del Plazo</td>
        </tr>
        <tr>
            <td align="center" width="3%">N°</td>
            <td align="center">Categorias</td>
        </tr>
	<tbody>
        <?php 
        $subtotal1 = 0;
        $subtotal2 = 0;
        $subtotal3 = 0;
        $subtotal4 = 0;
        $subtotal5 = 0;
        $subtotal6 = 0;
        $subtotal7 = 0;
        $subtotal8 = 0;
        $contador  = 0;
        foreach ($cabecera as $key => $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="left"><?=strtoupper($value[0]);?></td>
                <td align="right"><?=$value[1];?></td>
                <td align="right"><?=$value[2];?></td>
                <td align="right"><?=$value[3];?></td>
                <td align="right"><?=$value[4];?></td>
                <td align="right"><?=$value[5];?></td>
                <td align="right"><?=$value[6];?></td>
                <td align="right"><?=$value[7];?></td>
        <?php 
            $subtotal1 += $value[1];
            $subtotal2 += $value[2];
            $subtotal3 += $value[3];
            $subtotal4 += $value[4];
            $subtotal5 += $value[5];
            $subtotal6 += $value[6];
            $subtotal7 += $value[7];
            $subtotal8 += $value[1]+$value[2]+$value[3]+$value[4]+$value[5]+$value[6]+$value[7];
            endforeach;?>
		<tr style="font-weight: bold" class="ui-widget-header">
            <td colspan="2" >Total</td>
            <td align="right"><?=$subtotal1;?></td>
            <td align="right"><?=$subtotal2;?></td>
            <td align="right"><?=$subtotal3;?></td>
            <td align="right"><?=$subtotal4;?></td>
            <td align="right"><?=$subtotal5;?></td>
            <td align="right"><?=$subtotal6;?></td>
            <td align="right"><?=$subtotal7;?></td>
		</tr>
	</tbody>
</table>

