<?php

if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsReporte.php");
$clfunciones = new clsFunciones();

class clsNumeroUnidades extends clsReporte {

    function Header() {

        global $codsuc, $meses, $fechaperiodo;
        global $Dim;

        $periodo = $this->DecFechaLiteral($fechaperiodo);
        //DATOS DEL FORMATO
        $this->SetY(7);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "NUMERO DE UNIDADES DE USO POR ESTADO DE SERVICIO";
        $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(277, 5, $periodo["mes"]." - ".$periodo["anio"], 0, 1, 'C');
        $this->Ln(1);
        $this->SetFont('Arial', '', 7);
        $hc = 5;
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
        //DATOS DEL FORMATO
        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoupper($empresa["razonsocial"]);
        $tit2 = strtoupper($empresa["num1"]);
        $tit3 = "SUCURSAL: ".utf8_decode(strtoupper($empresa["descripcion"]));
        $x = 23;
        $y = 5;
        $h = 3;
        $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

        $this->SetY(10);
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');
        $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
        $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
        $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
        $this->SetY(17);
        $this->SetLineWidth(.1);
        //$this->Cell(0, .1, "", 1, 1, 'C', true);
        $this->cabecera();
    }

    function Footer() {
        $this->SetY(-10);
        $this->SetFont('Arial', 'I', 6);
        $this->SetTextColor(0);
        $this->SetLineWidth(.1);
        $this->SetX(10);
        $this->Cell(0, .1, "", 1, 1, 'C', true);
        $this->Ln(1);
        $this->SetX(10);
        $this->Cell(0, 4, utf8_decode('Sistema Integrado de Información Comercial - SIINCO - Versión WEB'), 0, 0, 'L');
        $this->Cell(0, 4, 'Pag. '.$this->PageNo().' de {nb}', 0, 0, 'R');
    }

    function cabecera() {
        global $Dim;

        $this->Ln(20);
        $this->SetFillColor(18, 157, 176);
        $this->SetTextColor(255);
        $h = 8;
        $h1 = 16;
        $h2 = 24;
        $h3 = 32;
        $h4 = 40;

        //$this->SetWidths(array(25,15,40,65,10,23,30,45));
        $this->SetFont('Arial', 'B', 10);
        $this->Cell($Dim[8], $h1, utf8_decode('Categorias'), 1, 0, 'C', true);
        $this->Cell($Dim[2], $h1, utf8_decode('Numero de Unidades de Uso'), 1, 0, 'C', true);
        $this->Cell($Dim[1], $h3, utf8_decode('Total'), 1, 1, 'C', true);

        $this->SetY($this->GetY() - $h1);
        $this->Cell($Dim[4], $h2, utf8_decode('N°'), 1, 0, 'C', true);
        $this->Cell($Dim[5], $h2, utf8_decode('Tarifas'), 1, 0, 'C', true);
        $this->Cell($Dim[2], $h, utf8_decode('Estado de Servicio'), 1, 1, 'C', true);

        $this->SetX($this->GetX() + 60);
        $this->Cell($Dim[6], $h, utf8_decode('Activos'), 1, 0, 'C', true);
        $this->Cell($Dim[6], $h, utf8_decode('Cerrados'), 1, 0, 'C', true);
        $this->Cell($Dim[6], $h, utf8_decode('Otros'), 1, 1, 'C', true);

        $this->SetX($this->GetX() + 60);
        $this->Cell($Dim[7], $h, utf8_decode('Ag. y Des.'), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h, utf8_decode('Agua'), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h, utf8_decode('Desague'), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h, utf8_decode('Ag. y Des.'), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h, utf8_decode('Agua'), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h, utf8_decode('Desague'), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h, utf8_decode('Ag. y Des.'), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h, utf8_decode('Agua'), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h, utf8_decode('Desague'), 1, 0, 'C', true);
        $this->Cell($Dim[3], $h, utf8_decode('T. Agua'), 1, 0, 'C', true);
        $this->Cell($Dim[3], $h, utf8_decode('T. Desague'), 1, 1, 'C', true);
    }

    function Contenido($cont, $descripcion, $num1, $num2, $num3, $num4, $num5, $num6, $num7, $num8, $num9, $sub1, $sub2, $sub3, $dat = 0) {
        global $Dim;

        switch ($dat) {
            case 1:

                $this->SetFillColor(7, 96, 125);
                $this->SetTextColor(255);
                $h = 10;
                $this->SetFont('Arial', 'B', 12);
                $this->Cell(($Dim[4] + $Dim[5]), $h, utf8_decode($descripcion), 1, 0, 'C', true);
                $this->Cell($Dim[7], $h, $num1 = empty($num1) ? 0 : $num1, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num2 = empty($num2) ? 0 : $num2, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num3 = empty($num3) ? 0 : $num3, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num4 = empty($num4) ? 0 : $num4, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num5 = empty($num5) ? 0 : $num5, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num6 = empty($num6) ? 0 : $num6, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num7 = empty($num7) ? 0 : $num7, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num8 = empty($num8) ? 0 : $num8, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num9 = empty($num9) ? 0 : $num9, 1, 0, 'R', true);
                $this->Cell($Dim[3], $h, $sub1 = empty($sub1) ? 0 : $sub1, 1, 0, 'R', true);
                $this->Cell($Dim[3], $h, $sub2 = empty($sub2) ? 0 : $sub2, 1, 1, 'R', true);
                break;

            default:
                $this->SetFillColor(255, 255, 255); //Color de Fondo
                $this->SetTextColor(0);
                $h = 10;
                $this->SetFont('Arial', '', 10);
                $this->Cell($Dim[4], $h, utf8_decode($cont), 1, 0, 'C', true);
                $this->Cell($Dim[5], $h, utf8_decode($descripcion), 1, 0, 'L', true);
                $this->Cell($Dim[7], $h, $num1 = empty($num1) ? 0 : $num1, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num2 = empty($num2) ? 0 : $num2, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num3 = empty($num3) ? 0 : $num3, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num4 = empty($num4) ? 0 : $num4, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num5 = empty($num5) ? 0 : $num5, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num6 = empty($num6) ? 0 : $num6, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num7 = empty($num7) ? 0 : $num7, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num8 = empty($num8) ? 0 : $num8, 1, 0, 'R', true);
                $this->Cell($Dim[7], $h, $num9 = empty($num9) ? 0 : $num9, 1, 0, 'R', true);
                $this->Cell($Dim[3], $h, $sub1 = empty($sub1) ? 0 : $sub1, 1, 0, 'R', true);
                $this->Cell($Dim[3], $h, $sub2 = empty($sub2) ? 0 : $sub2, 1, 1, 'R', true);
                // $this->Cell($Dim[3],$h,utf8_decode($subtotal),1,1,'C',true);
                break;
        }
    }

}

$Dim = array('1' => 60, '2' => 171, '3' => 30, '4' => 10, '5' => 50, '6' => 57, '7' => 19, '8' => 60);

// Actual
$codemp = 1;
$anio = $_GET['anio'];
$mes = $_GET['mes'];
$mestexto = $_GET['mestexto'];
$ciclo = $_GET['ciclo'];
$codsuc = $_GET['codsuc'];
$fechaperiodo = '01/'.$_GET['mes'].'/'.$_GET['anio'];

// Para obtener el periodo de faturacion
$sqlLect = "SELECT nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
$consultaL = $conexion->prepare($sqlLect);
$consultaL->execute(array($codemp, $codsuc, $ciclo, $anio, $mes));
$nrofact = $consultaL->fetch();

$sqlLect1 = " SELECT nomtar FROM  facturacion.tarifas WHERE codemp = ? AND codsuc = ? ORDER BY catetar;";
$consultaL1 = $conexion->prepare($sqlLect1);
$consultaL1->execute(array($codemp, $codsuc));
$catetares = $consultaL1->fetchAll();

// Datos
$Sql = "(SELECT cab.codsuc as codsuc, 
                    (cab.codestadoservicio ||''|| cab.catetar ||''|| cab.codtiposervicio) as catetar,
                    tar.nomtar as nomtar, 
                    count(cab.*) as cantidad 
                FROM facturacion.cabfacturacion cab 
                INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo) 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = '".$codsuc."' AND cab.nrofacturacion = {$nrofact['nrofacturacion']}
                AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' 
                AND cab.codestadoservicio = 1 AND cab.codtipousuario = 1  
                GROUP BY cab.codsuc, cab.codestadoservicio, cab.catetar,cab.codtiposervicio, tar.nomtar
                ORDER BY cab.codsuc, cab.codestadoservicio, cab.catetar,cab.codtiposervicio
            )
            UNION
            (
                SELECT 
                    cab.codsuc as codsuc, 
                    (cab.codestadoservicio ||''|| cab.catetar ||''|| cab.codtiposervicio) as catetar, 
                    tar.nomtar as nomtar, 
                    count(cab.*) as cantidad 
                FROM facturacion.cabfacturacion cab 
                INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo) 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = '".$codsuc."' AND cab.nrofacturacion = {$nrofact['nrofacturacion']} AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' 
                AND cab.codestadoservicio = 2 AND cab.codtipousuario = 1 
                GROUP BY cab.codsuc, cab.codestadoservicio, cab.catetar,cab.codtiposervicio, tar.nomtar
                ORDER BY cab.codsuc, cab.codestadoservicio, cab.catetar,cab.codtiposervicio
            )
            UNION 
            (
                SELECT 
                    cab.codsuc, 
                    (cab.codestadoservicio ||''|| cab.catetar ||''|| cab.codtiposervicio) as catetar,
                    tar.nomtar as nomtar,
                    count(cab.*) as cantidad 
                FROM facturacion.cabfacturacion cab 
                INNER JOIN facturacion.unidadesusofacturadas uni ON (cab.codemp = uni.codemp AND cab.codsuc = uni.codsuc AND cab.nrofacturacion = uni.nrofacturacion AND cab.nroinscripcion = uni.nroinscripcion AND cab.codciclo = uni.codciclo) 
                INNER JOIN facturacion.tarifas tar ON (cab.codemp = tar.codemp AND cab.codsuc = tar.codsuc AND cab.catetar = tar.catetar)
                WHERE cab.codsuc = '".$codsuc."' AND cab.nrofacturacion = {$nrofact['nrofacturacion']} 
                AND cab.anio = '{$anio}' AND cab.mes = '{$mes}' 
                AND cab.codestadoservicio > 2 AND cab.codtipousuario = 1 
                GROUP BY cab.codsuc, cab.codestadoservicio, cab.catetar,cab.codtiposervicio, tar.nomtar
                ORDER BY cab.codsuc, cab.codestadoservicio, cab.catetar,cab.codtiposervicio
            )
            ORDER BY codsuc, catetar";
$Consulta = $conexion->query($Sql);
$row = $Consulta->fetchAll();

// Construccion de Cabecera
$cabecera = array();
$cabcuerpo = array();

foreach ($row as $value):

    if (!array_key_exists($value['catetar'], $cabecera)):
        $cabecera[$value['catetar']] = array();
        array_push($cabecera[$value['catetar']], $value['nomtar'], $value['cantidad']);
    else:
        array_push($cabecera[$value['catetar']], $value['cantidad']);
    endif;

endforeach;

$CodigoES = array(0, 1, 2, 6);

for ($i = 1; $i <= 3; $i++):

    for ($g = 1; $g <= 5; $g++):


        for ($h = 1; $h <= 3; $h++):

            $indice = $CodigoES[$i].$g.$h;

            if (array_key_exists($indice, $cabecera)):
                $nombre = $cabecera[$indice][0];
                $import = $cabecera[$indice][1];
            else:
                $nombre = $catetares[($g - 1)]['nomtar'];
                $import = 0;
            endif;

            if (!array_key_exists($g, $cabcuerpo)):
                $cabcuerpo[$g] = array();
                array_push($cabcuerpo[$g], $nombre, $import);
            else:
                array_push($cabcuerpo[$g], $import);
            endif;

        endfor;

    endfor;

endfor;

$objReporte = new clsNumeroUnidades('L');
$contador = 0;
$objReporte->AliasNbPages();
$objReporte->SetLeftMargin(3);
$objReporte->AddPage('H');

$subtotal1 = 0;
$subtotal2 = 0;
$subtotal3 = 0;
$subtotal4 = 0;
$subtotal5 = 0;
$subtotal6 = 0;
$subtotal7 = 0;
$subtotal8 = 0;
$subtotal9 = 0;
$subtotal10 = 0;
$subtotal11 = 0;

$subtotal12 = 0;

$subt1 = 0;
$subt2 = 0;
$subt3 = 0;

$contador = 0;

foreach ($cabcuerpo as $key => $value):

    $contador++;
    $subt1 = $value[1] + $value[2] + $value[4] + $value[5] + $value[7] + $value[8];
    $subt2 = $value[1] + $value[3] + $value[4] + $value[6] + $value[7] + $value[9];
    $subt3 = $value[1] + $value[2] + $value[3] + $value[4] + $value[5] + $value[6];

    $subtotal1 += $value[1];
    $subtotal2 += $value[2];
    $subtotal3 += $value[3];
    $subtotal4 += $value[4];
    $subtotal5 += $value[5];
    $subtotal6 += $value[6];
    $subtotal7 += $value[7];
    $subtotal8 += $value[8];
    $subtotal9 += $value[9];
    $subtotal10 += $subt1;
    $subtotal11 += $subt2;
    $subtotal12 += $subt3;

    $objReporte->contenido($contador, strtoupper($value[0]), $value[1], $value[2], $value[3], $value[4], $value[5], $value[6], $value[7], $value[8], $value[9], $subt1, $subt2, $subt3);

endforeach;

$objReporte->contenido('', strtoupper('TOTAL'), $subtotal1, $subtotal2, $subtotal3, $subtotal4, $subtotal5, $subtotal6, $subtotal7, $subtotal8, $subtotal9, $subtotal10, $subtotal11, $subtotal12, 1);

$objReporte->Output();
?>