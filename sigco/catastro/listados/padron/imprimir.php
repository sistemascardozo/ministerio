<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses;
			
			$periodo = $this->DecFechaLiteral();
			
			$this->SetFont('Arial','B',10);
			$this->SetY(8);
			$h = 4;
			$this->Cell(278, $h+2,"PADRON DE USUARIOS",0,1,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(278, $h+2,$periodo["mes"]." - ".$periodo["anio"],0,1,'C');
			
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(20,15,63,45,10,20,10,15,15,8,8,8,8,8,8,8,8));
			$this->SetAligns(array("C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C"));
			$this->Row(array("COD. CATASTRAL",
							 "NRO. INSC.",
							 "CLIENTE",
							 "DIRECCION",
							 "CAT",
							 "EST",
							 "TS",
							 "NRO. MED.",
							 "SECT",
							 "MZA",
							 "LTE",
							 "SLTE",
							 "DOM",
							 "COM",
							 "IND",
							 "SOC",
							 "EST"));

		}
		function contenido($codsector,$tiposervicio,$estadoservicio,$codsuc,$ciclo,$altocon,$Desde,$Hasta,$conpozo,$estadomedidor)
		{
			global $conexion;
			
			$h = 4;
			$s = 2;
			
			if($altocon!=0)
			{
				$text = " and altocon=".$altocon;
			}
			if($Desde!="")
			{
				$Condicion=" AND c.fechareg BETWEEN CAST ('".$Desde."' AS DATE) AND CAST ( '".$Hasta."' AS DATE ) ";
			}
			
			if($codsector!="%"){$CSector=" AND c.codsector=".$codsector;}else{$CSector="";}
			if($tiposervicio!="%"){$Ctiposervicio=" AND c.codtiposervicio=".$tiposervicio;}else{$Ctiposervicio="";}
			if($estadoservicio!="%"){$Cestadoservicio=" AND c.codestadoservicio=".$estadoservicio;}else{$Cestadoservicio="";}
			if($estadomedidor!="%"){$Cestadomedidor=" AND c.codestadomedidor=".$estadomedidor;}else{$Cestadomedidor="";}
			
			if($ciclo!="%"){$Cciclo=" AND c.codciclo=".$ciclo;}else{$Cciclo="";}
			if($conpozo==0) $conpozo=" AND cx.conpozo =0 "; else $conpozo='';
			$ord = "ORDER BY  c.codsuc,c.codsector,CAST(c.codrutlecturas AS INTEGER ),CAST(c.codmanzanas AS INTEGER) ,CAST(c.lote AS INTEGER)";
			 $sql = "select c.nroinscripcion,c.propietario,tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,t.nomtar,
					e.descripcion as estadoservicio,c.codtiposervicio,c.nromed,s.descripcion as sector,c.codmanzanas,c.lote,
					c.sublote,c.domestico,c.comercial,c.industrial,c.social,c.estatal,".$this->getCodCatastral("c.").",
					cx.altocon
					from catastro.clientes as c 
					INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
					inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
					inner join facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
					inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
					inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
					inner join catastro.conexiones as cx on(c.codemp=cx.codemp and c.codsuc=cx.codsuc and c.nroinscripcion=cx.nroinscripcion)
					where c.codemp=1 and c.codsuc=? ".$conpozo.$CSector.$Ctiposervicio. $Cestadoservicio. $Cciclo. $text.$Condicion.$Cestadomedidor;
			
			/*$codsector 		= "%".$codsector."%";
			$tiposervicio	= "%".$tiposervicio."%";
			$estadoservicio	= "%".$estadoservicio."%"; 3765
			$ciclo			= "%".$ciclo."%";*/
			$count			= 0;
			
			$consulta = $conexion->prepare($sql.$ord);
			//$consulta->execute(array($codsuc,$codsector,$tiposervicio,$estadoservicio,$ciclo));
			$consulta->execute(array($codsuc));
			$items = $consulta->fetchAll();
	
			foreach($items as $row)
			{
				$count++;
				$codcatastro = $row["codcatastro"];
				$this->SetFont('Arial','',6);
				if($row["codtiposervicio"]==1){$ts="Ag/Des";}
				if($row["codtiposervicio"]==2){$ts="Ag";}
				if($row["codtiposervicio"]==3){$ts="Des";}
				
				$this->SetWidths(array(20,15,63,45,10,20,10,15,15,8,8,8,8,8,8,8,8));
				$this->SetAligns(array("C","C","L","L","C","C","C","C","C","C","C","C","C","C","C","C","C"));
				$this->Row(array($codcatastro,
							     $this->CodUsuario($codsuc,$row["nroinscripcion"]),
							 	 trim(utf8_decode(strtoupper($row["propietario"]))),
							     strtoupper($row["descripcioncorta"]." ".$row["calle"]." ".$row["nrocalle"]),
							 	 substr(strtoupper($row["nomtar"]),0,3),
							 	 strtoupper($row["estadoservicio"]),
							 	 $ts,
							 	 $row["nromed"],
							 	 strtoupper($row["sector"]),
							     $row["codmanzanas"],
							     $row["lote"],
							 	 $row["sublote"],
							     $row["domestico"],
							     $row["comercial"],
							     $row["industrial"],
							 	 $row["social"],
							     $row["estatal"]));
				
				
			}
			
			$this->Ln(3);
			$this->Cell(278, $h+2, "Usuario Registrados ".$count, 'TB',1, 'C');
			
		}
	}
	
	$codsuc         = $_GET["codsuc"];
	$codsector      = $_GET["sector"];
	$tiposervicio   = $_GET["tiposervicio"];
	$estadoservicio = $_GET["estadoservicio"];
	$ciclo          = $_GET["ciclo"];
	$altocon        = $_GET["altocon"];
	$Desde          = $_GET["Desde"];
	$Hasta          = $_GET["Hasta"];
	$conpozo		= $_GET["conpozo"];
	$estadomedidor	= $_GET["estadomedidor"];
	
	$objReporte = new clsPadron("L");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsector, $tiposervicio, $estadoservicio, $codsuc, $ciclo, $altocon, $Desde, $Hasta, $conpozo, $estadomedidor);
	$objReporte->Output();
	
?>