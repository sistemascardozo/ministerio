<?php
    include("../../../../objetos/clsReporte.php");

    class clsEstructura extends clsReporte {

        function cabecera() {
            global $NroInscripcion, $codsuc, $objreporte, $conexion, $Dim;

            $h = 4;
            $this->SetFont('Arial', 'B', 12);
            $this->Cell(0, $h + 1, "LISTADO DE CORTES Y REAPERTURAS", 0, 1, 'C');

            $Sql = "select clie.nroinscripcion," . $objreporte->getCodCatastral("clie.") . ", 
                            clie.propietario,td.abreviado,clie.nrodocumento,
                            tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
                            es.descripcion,tar.nomtar,clie.codantiguo,ts.descripcion as tiposervicio,tipofacturacion,nromed,consumo
                            from catastro.clientes as clie 
                            inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
                            inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
                            inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
                            inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
                            inner join public.tiposervicio as ts on(ts.codtiposervicio=clie.codtiposervicio)
                            inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
                            WHERE clie.codemp=1 AND clie.codsuc=" . $codsuc . " AND clie.nroinscripcion=" . $NroInscripcion . "";
            $Consulta = $conexion->query($Sql);
            $row = $Consulta->fetch();
            define('Cubico', chr(179));

            switch ($row['tipofacturacion']) {
                case 0: $TipoFacturacion = 'CONSUMO LEIDO ( Lect. Ultima ' . $row['consumo'] . 'm' . Cubico . ')';
                    break;
                case 1: $TipoFacturacion = 'CONSUMO PROMEDIADO (' . $row['consumo'] . 'm' . Cubico . ')';
                    break;
                case 2: $TipoFacturacion = 'CONSUMO ASIGNADO (' . $row['consumo'] . 'm' . Cubico . ')';
                    break;
            }

            /* $Medidor='No';
              if(trim($row['nromed'])!="")
              $Medidor='Si ( N° '.$row['nromed'].')'; */
            $this->SetLeftMargin(30);
            $this->Ln(5);

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("N° de Inscripcion"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h,$row['codantiguo'], 0, 0, 'L');

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Código Catastral"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, $row[1], 0, 1, 'L');
            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode($row['abreviado']), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, $row['nrodocumento'], 0, 1, 'L');
            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Cliente"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h, $row['propietario'], 0, 0, 'L');

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, 'Tipo Servicio', 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, $row['tiposervicio'], 0, 1, 'L');

            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Dirección"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, strtoupper($row['direccion']), 0, 1, 'L');

            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Estado"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h, $row['descripcion'], 0, 0, 'L');

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, 'Categoria', 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, $row['nomtar'], 0, 1, 'L');
            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Tipo Facturación"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h, $TipoFacturacion, 0, 0, 'L');

            /* $this->SetFont('Arial','B',8);
              $this->Cell(30,$h,'Medidor',0,0,'L');
              $this->Cell(5,$h,":",0,0,'L');
              $this->SetFont('Arial','',8);
              $this->Cell(30,$h,utf8_decode($Medidor),0,1,'L'); */

            $this->Ln(8);
            $this->SetX(10);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell($Dim[1], 5, utf8_decode('N°'), 1, 0, 'C', false);
            $this->Cell($Dim[2], 5, utf8_decode('Accion Ejecutada'), 1, 0, 'C', false);
            $this->Cell($Dim[3], 5, utf8_decode('Fecha'), 1, 0, 'C', false);
            $this->Cell($Dim[4], 5, utf8_decode('Motivo'), 1, 0, 'C', false);
            $this->Cell($Dim[5], 5, utf8_decode('Solicitud'), 1, 0, 'C', false);
            $this->Cell($Dim[6], 5, utf8_decode('Inspector'), 1, 0, 'C', false);
            $this->Cell($Dim[7], 5, utf8_decode('Observaciones'), 1, 1, 'C', false);
        }

        function Detalle() {
            
            global $NroInscripcion, $codsuc, $TpOcurrencia,$Desde,$Hasta,$objreporte, 
                    $conexion, $Dim, $meses;
            $Orden = " ORDER BY cierr.fecha ASC ";
            $Sql  = "select cierr.fecha,motcorte.descripcion as motivo,tipcorte.descripcion as tipocorte,
                            inspec.nombres as inspector, 
                            case when cierr.tipopersonal=0 then 'PERSONAL PROPIO' else 'PERSONAL SERVICE' 
                            end as personal, cierr.observacion,cierr.tipooperacion
                            from medicion.cierreyapertura as cierr 
                            inner join public.motivocorte as motcorte on(cierr.codmotivocorte=motcorte.codmotivocorte) 
                            inner join public.tipocorte as tipcorte on(cierr.codtipocorte=tipcorte.codtipocorte) 
                        inner join reglasnegocio.inspectores as inspec on(cierr.codemp=inspec.codemp 
                          and cierr.codsuc=inspec.codsuc and cierr.codinspector=inspec.codinspector)
                          where cierr.codemp=1 and cierr.codsuc=".$codsuc." and cierr.nroinscripcion=".$NroInscripcion." ".$Orden;

            $Consulta = $conexion->query($Sql);
            $c = 0;
            $Deuda = 0;
            $this->SetFillColor(255, 255, 255); //Color de Fondo
            $this->SetTextColor(0);
            $this->SetLeftMargin(10);
            $this->SetFont('Arial', '', 8);
            foreach ($Consulta->fetchAll() as $row) {
                $c++;
                $s='';
                $Ac='REAPERT/ACTIV. SERVICIO';
                if($row['tipooperacion']==0)
                    $Ac='CIERRE DE SERVICIO';
                else
                {
                    $Sql="SELECT nroinspeccion FROM catastro.solicitudinspeccion 
                            WHERE codemp=1 and codsuc=".$codsuc." 
                            and nroinscripcion=".$NroInscripcion." 
                            and extract(day from ( '".$row['fecha']."' ::timestamp - fechareg ::timestamp))>=0
                            and extract(day from ( '".$row['fecha']."' ::timestamp - fechareg ::timestamp))<=15";
                    $row2=$conexion->query($Sql)->fetch();
                    $s=str_pad($row2[0],8,'0',STR_PAD_LEFT);
                }
                $this->SetX(10);
                 $this->SetFont('Arial', '', 8);
                $this->Cell($Dim[1], 5, $c, 1, 0, 'C', true);
                $this->Cell($Dim[2], 5, $Ac, 1, 0, 'L', true);
                $this->Cell($Dim[3], 5, $objreporte->DecFecha($row['fecha']), 1, 0, 'C', true);
                $this->SetFont('Arial', '', 6);
                $this->Cell($Dim[4], 5, strtoupper($row['motivo']), 1, 0, 'L', true);
                $this->SetFont('Arial', '', 7);
                $this->Cell($Dim[5], 5, $s, 1, 0, 'C', true);
                 
                $this->Cell($Dim[6], 5, strtoupper($row['inspector']), 1, 0, 'L', true);
                $this->Cell($Dim[7], 5, utf8_decode($row['observacion']), 1, 1, 'L', true);
            }
            $this->Ln(2);
            
        }

    }

    define('Cubico', chr(179));

    $codsuc = $_SESSION["IdSucursal"];
    $NroInscripcion = $_GET["NroInscripcion"];
    $TpOcurrencia= $_GET['TpOcurrencia'];
    $Desde= $_GET['Desde'];
    $Hasta= $_GET['Hasta'];
    $Dim = array('1' => 5, '2' => 40, '3' => 15, '4' => 40, '5' =>15, '6' =>30, '7' =>50, '8' => 20, '9' => 20);
    $x = 20;
    $h = 4;
    $objreporte = new clsEstructura();
    $objreporte->AliasNbPages();
    $objreporte->AddPage("P");
    $objreporte->Detalle("P");

    $objreporte->Output();
?>