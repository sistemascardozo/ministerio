<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsDrop.php");

$objMantenimiento = new clsDrop();

$Presicion = $_SESSION["Presicion"];
$codsuc = $_SESSION["IdSucursal"];
$NroInscripcion = $_POST["NroInscripcion"];

$Sql = "SELECT clie.nroinscripcion, " . $objMantenimiento->getCodCatastral("clie.") . ", clie.propietario, td.abreviado, clie.nrodocumento, ";
$Sql .= " tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle AS direccion, ";
$Sql .= " es.descripcion, tar.nomtar, clie.codantiguo, ts.descripcion AS tiposervicio, clie.tipofacturacion, ";
$Sql .= " clie.nromed, clie.consumo, clie.codestadoservicio, co.fechainsmed, clie.codrutlecturas, co.orden_lect ";
$Sql .= "FROM catastro.clientes clie ";
$Sql .= " INNER JOIN catastro.conexiones co ON(clie.codemp = co.codemp) AND (clie.codsuc = co.codsuc) AND (clie.nroinscripcion = co.nroinscripcion) ";
$Sql .= " INNER JOIN public.calles cal ON(clie.codemp = cal.codemp AND clie.codsuc = cal.codsuc AND clie.codcalle = cal.codcalle AND clie.codzona = cal.codzona) ";
$Sql .= " INNER JOIN public.tiposcalle tipcal ON(cal.codtipocalle = tipcal.codtipocalle) ";
$Sql .= " INNER JOIN public.tipodocumento td ON(clie.codtipodocumento = td.codtipodocumento) ";
$Sql .= " INNER JOIN public.estadoservicio es ON(es.codestadoservicio = clie.codestadoservicio) ";
$Sql .= " INNER JOIN public.tiposervicio ts ON(ts.codtiposervicio = clie.codtiposervicio) ";
$Sql .= " INNER JOIN facturacion.tarifas tar ON(clie.codemp = tar.codemp) AND (clie.codsuc = tar.codsuc) AND (clie.catetar = tar.catetar) ";
$Sql .= "WHERE clie.codemp = 1 AND clie.codsuc = " . $codsuc . " AND tar.estado = 1 ";
$Sql .= " AND clie.nroinscripcion = " . $NroInscripcion . "";

$Consulta = $conexion->query($Sql);
$row = $Consulta->fetch();

switch ($row['tipofacturacion']) {
    case 0: $TipoFacturacion = 'CONSUMO LEIDO';
        break;
    case 1: $TipoFacturacion = 'CONSUMO PROMEDIADO';
        break;
    case 2: $TipoFacturacion = 'CONSUMO ASIGNADO';
        break;
}

$sql = "SELECT uso.catetar AS catetar, tar.nomtar AS nombre, uso.porcentaje AS porcentaje ";
$sql .= "FROM catastro.unidadesusoclientes AS uso ";
$sql .= " INNER JOIN facturacion.tarifas tar ON(uso.codemp = tar.codemp) AND (uso.codsuc = tar.codsuc) AND (uso.catetar = tar.catetar) ";
$sql .= "WHERE uso.codemp = 1 AND uso.codsuc = " . $codsuc . " AND tar.estado = 1 ";
$sql .= " AND uso.nroinscripcion = " . $NroInscripcion . " ";

$Consulta = $conexion->query($sql);
$raw = $Consulta->fetchAll();

$categorias = '';

foreach ($raw as $kew) :
    $categorias .= trim(substr($kew['nombre'], 0, 7)) . "(" . number_format($kew['porcentaje']) . "%)-";
endforeach;

$categorias = substr($categorias, 0, -1);

$Medidor = 'No';

if (trim($row['nromed']) != "") {
    $Medidor = 'Si ( N° ' . $row['nromed'] . ')';
}
?>
<script type="text/javascript">
    var urldir = '<?php echo $_SESSION['urldir']; ?>';

    function abrir_detalle_facturacion2(nrofacturacion, categoria, codtipodeuda)
    {
        cargar_detalle_facturacion2(nrofacturacion, categoria, codtipodeuda);

        $("#dialog-form-detalle-facturacion").dialog("open");
    }

    function cargar_detalle_facturacion2(nrofacturacion, categoria, codtipodeuda)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_facturacion_corriente.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nroinscripcion=<?=$NroInscripcion ?>' +
                    '&nrofacturacion=' + nrofacturacion + '&categoria=' + categoria + '&codtipodeuda=' + codtipodeuda,
            success: function (datos) {
                $("#detalle-facturacion").html(datos)
            }
        })
    }

    function abrir_detalle_interes(codsuc, NroInscripcion, TasaInteres)
    {
        cargar_detalle_interes(codsuc, NroInscripcion, TasaInteres);

        $("#dialog-form-detalle-interes").dialog("open");
    }
    function cargar_detalle_interes(codsuc, NroInscripcion, TasaInteres)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_interes.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nroinscripcion=<?=$NroInscripcion ?>' +
                    '&TasaInteres=' + TasaInteres,
            success: function (datos) {
                $("#detalle-interes").html(datos)
            }
        })
    }
    function abrir_detalle_pagos(codpagos, nrofacturacion)
    {
        cargar_detalle_pagos(codpagos, nrofacturacion);

        $("#dialog-form-detalle-pagos").dialog("open");
    }

    function cargar_detalle_pagos(codpagos, nrofacturacion)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_pagos.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nroinscripcion=<?=$NroInscripcion ?>' +
                    '&codpagos=' + codpagos + '&nrofacturacion=' + nrofacturacion,
            success: function (datos) {
                $("#detalle-pagos").html(datos)
            }
        })
    }

    function abrir_detalle_colaterales(codcolateral)
    {
        cargar_detalle_colaterales(codcolateral);

        $("#dialog-form-detalle-colaterales").dialog("open");
    }

    function cargar_detalle_colaterales(codcolateral)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_colaterales.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nroinscripcion=<?=$NroInscripcion ?>' +
                    '&codcolateral=' + codcolateral,
            success: function (datos) {
                $("#detalle-colaterales").html(datos)
            }
        })
    }

    function abrir_detalle_creditos(nrocredito)
    {
        cargar_detalle_creditos(nrocredito);

        $("#dialog-form-detalle-creditos").dialog("open");
    }

    function cargar_detalle_creditos(nrocredito)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_creditos.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nrocredito=' + nrocredito,
            success: function (datos) {
                $("#detalle-creditos").html(datos)
            }
        })
    }

    function abrir_detalle_refinanciamiento(nrocredito)
    {
        cargar_detalle_refinanciamiento(nrocredito);

        $("#dialog-form-detalle-refinanciamiento").dialog("open");
    }

    function cargar_detalle_refinanciamiento(nrocredito)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_cargar_detalle_refinanciamiento.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nrocredito=' + nrocredito + '&nroinscripcion=<?=$NroInscripcion ?>',
            success: function (datos) {
                $("#detalle-refinanciamiento").html(datos)
            }
        })
    }
    function abrir_detalle_varios(nrocredito)
    {
        cargar_detalle_varios(nrocredito);

        $("#dialog-form-detalle-varios").dialog("open");
    }

    function cargar_detalle_varios(nrocredito)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_varios.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nrocredito=' + nrocredito,
            success: function (datos) {
                $("#detalle-varios").html(datos)
            }
        })
    }
    function ImprimirForRec(nroreclamo)
    {
        $('#nroreclamo').val(nroreclamo);

        $("#dialog-form-imprimir").dialog("open");
    }
    function imprimir_formatos()
    {
        var dir = "formatos/";
        var url = "";
        var nroreclamo = $('#nroreclamo').val();

        if ($("#formato").val() == 0)
        {
            alert("Seleccione el Formato a Imprimir");

            return false;
        }

        if ($("#formato").val() == 1)
        {
            url = dir + "formato1.php";
        }
        if ($("#formato").val() == 2)
        {
            url = dir + "formato2.php";
        }
        if ($("#formato").val() == 3)
        {
            url = dir + "formato3.php";
        }
        if ($("#formato").val() == 4)
        {
            url = dir + "formato4.php";
        }
        if ($("#formato").val() == 5)
        {
            url = dir + "formato5.php";
        }
        if ($("#formato").val() == 6)
        {
            url = dir + "formato6.php"
        }
        if ($("#formato").val() == 7)
        {
            url = dir + "formato7.php"
        }
        if ($("#formato").val() == 8)
        {
            url = dir + "formato8.php"
        }
        if ($("#formato").val() == 9)
        {
            url = dir + "formato9.php"
        }
        if ($("#formato").val() == 10)
        {
            url = dir + "cedula.php"
        }

        if (url != "")
        {
            AbrirPopupImpresion(urldir + "sigco/reclamos/operaciones/ingresos/" + url + '?nroreclamo=' + nroreclamo + '&codsuc=' + codsuc + '&nroinscripcion=<?=$NroInscripcion ?>', 800, 600);
        }
    }

    $(function () {
        $("#buttonsetrec").buttonset();
        $("#dialog-form-detalle-facturacion").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#dialog-form-detalle-facturacion").dialog("close");
                }
            },
            close: function () {

            }
        });

        $("#dialog-form-detalle-interes").dialog({
            autoOpen: false,
            height: 400,
            width: 700,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#dialog-form-detalle-interes").dialog("close");
                }
            },
            close: function () {

            }
        });
        $("#dialog-form-detalle-pagos").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#dialog-form-detalle-pagos").dialog("close");
                }
            },
            close: function () {

            }
        });

        $("#dialog-form-detalle-colaterales").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#dialog-form-detalle-colaterales").dialog("close");
                }
            },
            close: function () {

            }
        });

        $("#dialog-form-detalle-creditos").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#dialog-form-detalle-creditos").dialog("close");
                }
            },
            close: function () {

            }
        });
        $("#dialog-form-detalle-refinanciamiento").dialog({
            autoOpen: false,
            height: 400,
            width: 550,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#dialog-form-detalle-refinanciamiento").dialog("close");
                }
            },
            close: function () {

            }
        });
        $("#dialog-form-detalle-varios").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#dialog-form-detalle-varios").dialog("close");
                }
            },
            close: function () {

            }
        });
        $("#dialog-form-imprimir").dialog({
            autoOpen: false,
            height: 170,
            width: 450,
            modal: true,
            resizable: false,
            buttons: {
                "Imprimir": function () {
                    imprimir_formatos();
                },
                "Cerrar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            }
        });
    });
</script>
<table class="ui-widget" border="1" cellspacing="0" width="100%"  rules="rows" align="center">
    <thead style="font-size:12px;"> 
        <tr>
            <td width="300"><span class="Negrita">N&deg; de Inscripcion&nbsp;:&nbsp;</span><?=$row['codantiguo'] ?></td>
            <td width="250"><span class="Negrita">Cod. Catastral&nbsp;:</span><?=$row[1] ?></td>
            <td><span class="Negrita">Ruta - Secuencia&nbsp;:&nbsp;</span><?=$row['codrutlecturas'] . "-", $row['orden_lect'] ?></td>
            <td><input type='hidden' id='nroreclamo'>
                <?php include("SubMenu.php"); ?>
            </td>
        </tr>
        <tr>
            <td align="left"><span class="Negrita">Cliente&nbsp;:&nbsp;</span><?=$row['propietario'] ?></td>
            <td colspan="3"><span class="Negrita">Tipo Servicio&nbsp;:&nbsp;</span><?=$row['tiposervicio'] ?></td>

        </tr>
        <tr>
            <td align="left"><span class="Negrita">Direcci&oacute;n&nbsp;:&nbsp;</span><?=strtoupper($row['direccion']) ?></td>
            <td colspan="3"><span class="Negrita">Categoria&nbsp;:&nbsp;</span><?=$categorias ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">Estado&nbsp;:&nbsp;</span><?=$row['descripcion'] ?></td>
            <td colspan="3"><span class="Negrita">Fecha Activaci&oacute;n&nbsp;:</span> 
                <?php
                if ($row['codestadoservicio'] == 1) {
                    $Sql = "(SELECT MAX(cierr.fecha) AS FECHA ";
                    $Sql .= "FROM medicion.cierreyapertura AS cierr ";
                    $Sql .= "WHERE cierr.codemp = 1 AND cierr.codsuc = " . $codsuc . " ";
                    $Sql .= " AND tipooperacion = 1 AND cierr.nroinscripcion = " . $NroInscripcion . ") ";
                    $Sql .= "UNION ";
                    $Sql .= "(SELECT MAX(m.fechacontrol) AS FECHA ";
                    $Sql .= "FROM catastro.modificaciones AS m ";
                    $Sql .= "WHERE m.codsuc = " . $codsuc . " ";
                    $Sql .= " AND TRIM(m.campo) = 'codestadoservicio' ";
                    $Sql .= " AND m.valorultimo = '1' AND m.nroinscripcion = " . $NroInscripcion . ") ";
                    $Sql .= "UNION ";
                    $Sql .= "(SELECT m.fechainsagua AS FECHA ";
                    $Sql .= "FROM catastro.conexiones AS m ";
                    $Sql .= "WHERE m.codemp = 1 AND m.codsuc = " . $codsuc . " ";
                    $Sql .= " AND m.nroinscripcion = " . $NroInscripcion . ") ";
                    $Sql .= "UNION ";
                    $Sql .= "(SELECT m.fechainsdesague AS FECHA ";
                    $Sql .= "FROM catastro.conexiones AS m ";
                    $Sql .= "WHERE m.codemp = 1 AND m.codsuc = " . $codsuc . " ";
                    $Sql .= " AND m.nroinscripcion = " . $NroInscripcion . ") ";
                    $Sql .= "ORDER BY FECHA DESC";

                    $ConsultaF = $conexion->query($Sql);
                    $rowF = $ConsultaF->fetchAll();

                    if ($rowF[0][0] != '') {
                        echo $objMantenimiento->CodFecha($rowF[0][0]);
                    } else {
                        echo $objMantenimiento->CodFecha($rowF[1][0]);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><span class="Negrita">Tipo Facturaci&oacute;n&nbsp;:&nbsp;</span><?=$TipoFacturacion ?></td>
            <td><span class="Negrita">Medidor&nbsp;:&nbsp;</span><?=$Medidor ?></td>
            <td colspan="2">
<?php
if (trim($row['nromed']) != "") {
    ?>
                    <span class="Negrita">Fecha Instalaci&oacute;n&nbsp;:&nbsp;</span><?=$objMantenimiento->CodFecha($row['fechainsmed']); ?>
                    <?php
                }
                ?>
            </td>
        </tr>
    </thead>
</table>
<table class="ui-widget" border="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
        <tr>
            <th width="40" scope="col">Item</th>
            <th width="80" scope="col">Fecha</th>
            <th width="50" scope="col">Hora</th>
            <th width="75" scope="col">Mes Facturaci&oacute;n</th>
            <th width="50" scope="col">Serie</th>
            <th width="50" scope="col">N&uacute;mero</th>
            <th scope="col">Operaci&oacute;n</th>
            <th width="40" scope="col">Cat</th>
            <!--<th width="40" scope="col" data="estado_lectura">Co</th>-->
            <th width="50" scope="col">Lectura</th>
            <th width="50" scope="col">Consu</th>
            <th width="70" scope="col">Cargo</th>
            <th width="70" scope="col">Abono</th>
            <th width="70" scope="col">Saldo</th>
            <th width="60" scope="col">Estado Recibo</th>
            <th scope="col" width="10" align="center">Acciones</th>

        </tr>
    </thead>
    <tbody style="font-size:12px">
<?php
$Condicion1 = "WHERE f.nroinscripcion = " . $NroInscripcion . " AND f.codsuc = " . $codsuc . " AND tar.estado = 1";
$Orden1 = "ORDER BY f.nroinscripcion ASC ";

$Condicion2 = "WHERE p.nroinscripcion = " . $NroInscripcion . " AND d.nrofacturacion <> 0 AND p.anulado = 0 AND p.codsuc = " . $codsuc;
$Orden2 = "ORDER BY p.nroinscripcion ASC ";

$Condicion3 = "WHERE p.nroinscripcion = " . $NroInscripcion . " AND d.nrofacturacion = 0 AND p.anulado = 0 AND p.codsuc = " . $codsuc;
$Orden3 = "ORDER BY p.nroinscripcion ASC ";

$Condicion4 = "WHERE c.nroinscripcion = " . $NroInscripcion . " AND c.codsuc = " . $codsuc;
$Orden4 = "ORDER BY c.nroinscripcion ASC ";

$Condicion5 = "WHERE c.nroinscripcion = " . $NroInscripcion . " AND c.codsuc = " . $codsuc;
$Orden5 = "ORDER BY c.nroinscripcion ASC ";

$Condicion6 = "WHERE rb.nroinscripcion = " . $NroInscripcion . " AND rb.codsuc = " . $codsuc;
$Orden6 = "ORDER BY rb.nroinscripcion ASC ";

//(0)
//Facturaciones 
$Sql = "(SELECT 
					f.fechareg, 
					tar.nomtar AS categoria, 
					f.serie,
					f.nrodocumento,
					'00:00:00' AS hora,
					" . $objMantenimiento->Convert('f.anio', 'INTEGER') . " AS anio, 
					" . $objMantenimiento->Convert('f.mes', 'INTEGER') . " AS mes, 
					SUM(df.importe) AS cargo,
					'0.00' AS abono,
					'0' AS movimiento, 
					f.enreclamo AS reclamo, 
					f.lecturaultima AS lectura, 
					f.consumofact AS consumo,
					df.nrofacturacion AS facturacion,
					0 AS cat,
					0 AS codtipodeuda,
					'0' AS nropago,
					f.estadofacturacion AS estado,
					0 AS nrocredito,
					0 AS orden,
					SUM(df.importerebajado) AS rebajado,
					f.nroreclamo AS nroreclamo,
					0
                FROM facturacion.cabfacturacion f 
					INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
					INNER JOIN facturacion.tarifas tar ON (f.codemp = tar.codemp AND f.codsuc = tar.codsuc AND f.catetar = tar.catetar)
                " . $Condicion1 . " 
					/*AND df.nrocredito = 0*/ AND df.nrorefinanciamiento = 0 AND df.codconcepto <> 10012
                GROUP BY f.estadofacturacion, df.nrofacturacion, f.enreclamo, tar.nomtar, f.lecturaultima, f.consumofact, 
					f.fechareg, f.serie, f.nrodocumento, f.anio, f.mes, f.nrofacturacion, f.nroreclamo) ";
//(1)
//Pagos de Facturacion de Servicios
$Sql .= "UNION
                (SELECT 
					p.fechareg, 
					' ' AS categoria,
					d.serie, 
					d.nrodocumento,
					p.hora AS hora,
					" . $objMantenimiento->Convert('d.anio', 'INTEGER') . " AS anio,
					" . $objMantenimiento->Convert('d.mes', 'INTEGER') . " AS mes, 
					'0.00' AS cargo,
					SUM(d.importe) AS abono,
					'1' AS movimiento,
					'0' AS reclamo,
					'0' AS lectura,
					'0' AS consumo,
					d.nrofacturacion AS facturacion,
					'0' AS cat,
					'0' AS codtipodeuda,
					d.nropago AS nropago,
					'0' AS estado,
					0 AS nrocredito,
					3 AS orden,
					0 AS rebajado,
					0 AS nroreclamo,
					0
                FROM cobranza.cabpagos p
					INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp) AND (p.codsuc = d.codsuc) AND (p.nroinscripcion = d.nroinscripcion) AND (p.nropago = d.nropago) 
				" . $Condicion2 . " 
                GROUP BY  d.nropago, d.nrofacturacion, p.hora, p.fechareg, d.serie, d.nrodocumento, d.anio, d.mes
                ORDER BY p.fechareg ASC) ";
//(2)
//Pagos Colaterales
$Sql .= "UNION
                (SELECT 
					p.fechareg, 
					co.descripcion AS categoria,
					d.serie, 
					d.nrodocumento,
					p.hora AS hora,
					" . $objMantenimiento->Convert('d.anio', 'INTEGER') . " AS anio,
					" . $objMantenimiento->Convert('d.mes', 'INTEGER') . " AS mes, 
					'0.00' AS cargo,
					/*SUM(d.importe) AS abono,*/
					p.imptotal AS abono,
					'2' AS movimiento,
					'0' AS reclamo,
					'0' AS lectura,
					'0' AS consumo,
					'0' AS facturacion,
					'0' AS cat,
					d.codtipodeuda AS codtipodeuda,
					d.nropago AS nropago,
					'0' AS estado,
					0 AS nrocredito,
					4 AS orden,
					0 AS rebajado,
					0 AS nroreclamo,
					0
                FROM cobranza.cabpagos p
					INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp) AND (p.codsuc = d.codsuc) AND (p.nroinscripcion = d.nroinscripcion) AND (p.nropago = d.nropago) 
					INNER JOIN facturacion.conceptos co ON (co.codemp = d.codemp AND co.codsuc = d.codsuc AND co.codconcepto = d.codconcepto)
                " . $Condicion3 . " 
				AND d.codconcepto NOT IN(5, 8, 7)
                GROUP BY d.codtipodeuda, d.nropago, p.hora, p.fechareg, d.serie, d.nrodocumento, d.anio, d.mes, co.descripcion, p.imptotal
                ORDER BY p.fechareg ASC) ";
//(3)
//Créditos (0)Pendientes - (1)Facturados - (3)Cancelados - (4)Quebrados
$Sql .= "UNION
                (SELECT 
					c.fechareg, 
					' ' AS categoria,
					' ' AS serie, 
					'0' AS nrodocumento,
					'00:00:00' AS hora,
					" . $objMantenimiento->Convert('extract(year from c.fechareg)', 'INTEGER') . " AS anio,
					" . $objMantenimiento->Convert('extract(month from c.fechareg)', 'INTEGER') . " AS mes, 
					SUM(CAST(d.imptotal AS NUMERIC(18,2)) + d.redondeo) AS cargo,
					'0.00' AS abono,
					'3' AS movimiento,
					'0' AS reclamo,
					'0' AS lectura,
					'0' AS consumo,
					'0' AS facturacion,
					'0' AS cat,
					'0' AS codtipodeuda,
					'0' AS nropago,
					c.estareg AS estado,
					c.nrocredito AS nrocredito,
					2 AS orden,
					0 AS rebajado,
					0 AS nroreclamo,
					d.estadocuota
                FROM facturacion.cabcreditos c
					INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nrocredito = d.nrocredito)
                " . $Condicion4 . " 
					AND d.estadocuota <> 2 AND d.estadocuota <> 5
                GROUP BY c.fechareg, c.subtotal, c.estareg, c.nrocredito, d.estadocuota
                ORDER BY c.fechareg ASC) ";
//(4)
//Refinanciamientos
$Sql .= "UNION
                (SELECT 
					c.fechaemision, 
					' ' AS categoria,
					' ' AS serie, 
					'0' AS nrodocumento,
					'00:00:00' AS hora,
					" . $objMantenimiento->Convert('extract(year from c.fechaemision)', 'INTEGER') . " AS anio,
					" . $objMantenimiento->Convert('extract(month from c.fechaemision)', 'INTEGER') . " AS mes, 
					SUM(d.importe) AS cargo,
					'0.00' AS abono,
					'4' AS movimiento,
					'0' AS reclamo,
					'0' AS lectura,
					'0' AS consumo,
					'0' AS facturacion,
					'0' AS cat,
					'0' AS codtipodeuda,
					'0' AS nropago,
					c.estareg AS estado,
					c.nrorefinanciamiento AS nrocredito,
					2 AS orden,
					0 AS rebajado,
					0 AS nroreclamo,
					d.estadocuota
                FROM facturacion.cabrefinanciamiento c
					INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nrorefinanciamiento = d.nrorefinanciamiento)
                " . $Condicion5 . " /*AND d.estadocuota IN (1, 3) */
                GROUP BY c.fechaemision, c.totalrefinanciado, c.estareg, c.nrorefinanciamiento, d.estadocuota
                ORDER BY c.fechaemision ASC) ";
//(5)
//Rebajas
$Sql .= "UNION
                (SELECT 
					rb.fechareg, 
					' ' AS categoria,
					d.seriedocumento AS serie, 
					d.nrodocumentoabono AS nrodocumento,
					'00:00:00' AS hora,
					" . $objMantenimiento->Convert('d.anio', 'INTEGER') . " AS anio,
					" . $objMantenimiento->Convert('d.mes', 'INTEGER') . " AS mes, 
					'0.00' AS cargo,
					SUM(d.imprebajado) AS abono,
					'5' AS movimiento,
					'0' AS reclamo,
					'0' AS lectura,
					'0' AS consumo,
					'0' AS facturacion,
					'0' AS cat,
					'0' AS codtipodeuda,
					'0' AS nropago,
					rb.codestrebaja AS estado,
					0 AS nrocredito,
					2 AS orden,
					0 AS rebajado,
					0 AS nroreclamo,
					0
                FROM facturacion.cabrebajas rb 
					INNER JOIN facturacion.detrebajas d ON(rb.nrorebaja =  d.nrorebaja AND rb.codemp = d.codemp AND rb.codsuc = d.codsuc)
                " . $Condicion6 . " 
					/*AND rb.fechaemision > '2014-09-30'*/
                GROUP BY rb.fechareg, d.seriedocumento, d.nrodocumentoabono, d.anio, d.mes, rb.codestrebaja 
                ORDER BY rb.fechareg ASC) ";
//(6)
//Cargos
$Sql .= "UNION
                (SELECT 
					c.fechareg, 
					' ' AS categoria,
					' ' AS serie, 
					'0' AS nrodocumento,
					'00:00:00' Shora,
					" . $objMantenimiento->Convert('extract(year from c.fechareg)', 'INTEGER') . " AS anio,
					" . $objMantenimiento->Convert('extract(month from c.fechareg)', 'INTEGER') . " AS mes, 
					c.imptotal AS cargo,
					'0.00' AS abono,
					'6' AS movimiento,
					'0' AS reclamo,
					'0' AS lectura,
					'0' AS consumo,
					'0' AS facturacion,
					'0' AScat,
					'0' AS codtipodeuda,
					'0' AS nropago,
					c.estareg AS estado,
					c.nrocredito AS nrocredito,
					5 AS orden,
					0 AS rebajado,
					0 AS nroreclamo,
					0
                FROM facturacion.cabvarios c
					INNER JOIN facturacion.detvarios d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc)  AND (c.nrocredito = d.nrocredito)
                " . $Condicion4 . " 
					AND d.estadocuota = 0 
                GROUP BY c.fechareg, c.imptotal, c.estareg, c.nrocredito
                ORDER BY c.fechareg ASC) ";
//(7)
//Pagos
$Sql .= "UNION
                (SELECT 
					p.fechareg, 
					co.descripcion AS categoria,
					d.serie, 
					d.nrodocumento,
					p.hora AS hora,
					" . $objMantenimiento->Convert('d.anio', 'INTEGER') . " AS anio,
					" . $objMantenimiento->Convert('d.mes', 'INTEGER') . " AS mes, 
					/*SUM(d.importe) AS cargo,*/ 
					p.imptotal AS cargo,
					'0.00' AS abono,
					'7' AS movimiento,
					'0' AS reclamo,
					'0' AS lectura,
					'0' AS consumo,
					'0' AS facturacion,
					d.categoria AS cat,
					d.codtipodeuda AS codtipodeuda,
					d.nropago AS nropago,
					'0' AS estado,
					0 AS nrocredito,
					3 AS orden,
					0 AS rebajado,
					d.coddocumento AS nroreclamo,
					0
                FROM cobranza.cabpagos p
					INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp) AND (p.codsuc = d.codsuc) AND (p.nroinscripcion = d.nroinscripcion) AND (p.nropago = d.nropago) 
					INNER JOIN facturacion.conceptos co ON (co.codemp = d.codemp AND co.codsuc = d.codsuc AND co.codconcepto = d.codconcepto)
                " . $Condicion3 . " 
					AND d.codconcepto NOT IN(5, 10, 992, 10005)
					/*AND d.categoria <> 3*/
                GROUP BY  d.codtipodeuda, d.nropago, p.hora, p.fechareg, d.serie, d.nrodocumento, d.anio, d.mes, co.descripcion, d.categoria, d.coddocumento, p.imptotal
                ORDER BY p.fechareg ASC) ";
//(8)
//FACT Convenios, Cargos
//$Sql .= "UNION
//                (SELECT 
//					f.fechareg, 
//					tar.nomtar AS categoria, 
//					f.serie,
//					f.nrodocumento,
//					'00:00:00' AS hora,
//					".$objMantenimiento->Convert('f.anio', 'INTEGER')." AS anio, 
//					".$objMantenimiento->Convert('f.mes', 'INTEGER')." AS mes, 
//					SUM(df.importe) AS cargo,
//					'0.00' AS abono,
//					'8' AS movimiento, 
//					f.enreclamo AS reclamo, 
//					f.lecturaultima AS lectura, 
//					f.consumofact AS consumo,
//					df.nrofacturacion Sfacturacion,
//					df.categoria AS cat,
//					df.codtipodeuda AS codtipodeuda,
//					'0' AS nropago,
//					f.estadofacturacion AS estado,
//					df.nrocredito AS nrocredito,
//					1 AS orden,
//					0 AS rebajado,
//					0 AS nroreclamo,
//					0
//                FROM facturacion.cabfacturacion f 
//					INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
//					INNER JOIN facturacion.tarifas as tar ON (f.codemp=tar.codemp and f.codsuc=tar.codsuc and f.catetar=tar.catetar)
//                ".$Condicion1." 
//					AND (df.nrocredito <> 0 OR df.nrorefinanciamiento <> 0) AND tar.estado = 1 
//                GROUP BY f.estadofacturacion, df.categoria, df.nrofacturacion, f.enreclamo, tar.nomtar, f.lecturaultima, f.consumofact, f.fechareg, f.serie, f.nrodocumento, f.anio, f.mes, f.nrofacturacion, df.codtipodeuda, df.nrocredito)
$Sql .= "ORDER by fechareg, hora, anio, mes, orden ASC ";

//die($Sql);
$Consulta = $conexion->query($Sql);
$datos = $Consulta->fetchAll();
$c = 0;
$Deuda = 0;

$DeudaP = 0;
// var_dump($datos);exit;

$nrocredito = array();

if (count($datos) > 0):

    foreach ($datos as $row):
        $c++;
        $Operacion = '';
        $rec = '';

        switch ($row['movimiento']) {
            case 0:
                $Operacion = "FACT. DE PENSIONES";

                $Anio = $row['anio'];
                $Mes = $row['mes'];
                $AnioT = $row['anio'];
                $MesT = $row['mes'];

                $Deuda += $row['cargo'];

                $style = "style=color:red;";

                $onClickDetalle = "abrir_detalle_facturacion2(" . $row["facturacion"] . ", " . $row["cat"] . ", " . $row['codtipodeuda'] . ")";
                $onclick = "<span class='icono-icon-detalle' onclick='" . $onClickDetalle . "' title='Ver Detalle de Facturacion' ></span>";

                switch ($row['estado']) {
                    case 1:$row['estado'] = 'PEN';
                        break;
                    case 2:
						$row['estado'] = 'PAG';
						
						$style  = "style=color:blue;";
						
                        break;
                    case 3:$row['estado'] = 'AMO';
                        break;
                    case 4:$row['estado'] = 'ANU';

                        $Deuda -= $row['cargo'];
                        break;
					case 5:
						$row['estado'] = 'PAG AD';
                        
						$Deuda -= $row['cargo'];
						
						$style  = "style=color:blue;";
						
						break;
                    default:$row['estado'] = 'PEN';
                        break;
                };

                if ($row["cat"] == 2) {//REFINANCIADO
                    $row['estado'] = 'REF';

                    $Deuda -= $row['cargo'];
                    $Deuda += $row['rebajado'];
                }
                if ($row["codtipodeuda"] == 11) {//QUEBRADO
                    $row['estado'] = 'QBD';

                    $Deuda -= $row['cargo'];
                }
                if ($row['reclamo'] == 1) {
                    $rec = "<span class='MljSoft-icon-enreclamo' title='Facturacion con Reclamo Pendiente' onclick='ImprimirForRec(" . $row[21] . ")'></span>";
                } elseif ($row['nroreclamo'] != 0) {
                    $rec = "<span class='MljSoft-icon-reclamook' title='Facturacion con Reclamo Finalizado' onclick='ImprimirForRec(" . $row[21] . ")'></span>";
                }

                break;

            case 1:
                if ($row['anio'] == $AnioT && $row['mes'] == $MesT)
				{
                    $Operacion = "PAGO RECIBO DE PENSIONES";
                }
				else
				{
                    $Operacion = "PAGO DEUDA ACUMULADA";
                }

                $Anio = $row['anio'];
                $Mes = $row['mes'];

                $Deuda -= $row['abono'];

                $style = "style=color:green;";

                $onClickDetalle = "abrir_detalle_pagos(" . $row["nropago"] . ", " . $row["facturacion"] . ")";
                $onclick = "<span class='icono-icon-detalle' onclick='" . $onClickDetalle . "' title='Ver Detalle de Pagos' ></span>";

                $row['estado'] = '';

                break;

            case 2:
                switch ($row['codtipodeuda']) {
                    case 3:
                        $Operacion = $row['categoria'] . "(REFINANCIAMIENTO)";
                        break;
                    case 4:
                        $Operacion = $row['categoria'] . "(CREDITOS)";
                        break;
                    case 4:
                        $Operacion = $row['categoria'] . "(CREDITOS)";
                        break;
                    default:
                        $Operacion = 'PAGO ' . $row['categoria'];
                        break;
                }

                $Anio = $row['anio'];
                $Mes = $row['mes'];

                $style = "style=color:green;";

                $onclick = '';
                $onClickDetalle = "abrir_detalle_colaterales(" . $row["nropago"] . ")";
                $onclick = "<span class='icono-icon-detalle' onclick='" . $onClickDetalle . "' title='Ver Detalle de Colaterales' ></span>";

                $row['estado'] = '';

                $Deuda -= $row['abono'];

                break;

            case 3:
                $Operacion = "FACT. DE CREDITOS";

                $Anio = $row['anio'];
                $Mes = $row['mes'];

                $style = "style=color:#00008B;";
                $onClickDetalle = "abrir_detalle_creditos(" . $row["nrocredito"] . ")";
                $onclick = "<span class='icono-icon-detalle' onclick='" . $onClickDetalle . "' title='Ver Detalle de Creditos'></span>";

                switch ($row['estado']) {
                    case 0:
                        $row['estado'] = 'ANU';

                        break;

                    case 1:
                        $row['estado'] = 'PEN';

                        if ($row[22] == 0) {
                            $DeudaP += $row['cargo'];
                        }
                        if ($row[22] == 3) {
                            $row['estado'] = 'CAN';
                            $Deuda += $row['cargo'];
                        }

                        break;

                    case 2:
                        $row['estado'] = 'CAN';

                        $Deuda += $row['cargo'];

                        break;

                    case 3:
                        $row['estado'] = 'CANC';

                        break;

                    case 4:
                        $row['estado'] = 'QUE';

                        break;
                };

                if (!array_key_exists($row['nrocredito'], $nrocredito)) {
                    $nrocredito[$row['nrocredito']] = $row['nrocredito'];
                }

                break;

            case 4:
                $Operacion = "FACT. POR REFINANCIAMIENTO";

                $Anio = $row['anio'];
                $Mes = $row['mes'];

                $style = "style=color:#4B0082;";

                $onClickDetalle = "abrir_detalle_refinanciamiento(" . $row["nrocredito"] . ")";
                $onclick = "<span class='icono-icon-detalle' onclick='" . $onClickDetalle . "' title='Ver Detalle de Creditos'></span>";

                switch ($row['estado']) {
                    case 0:$row['estado'] = 'ANU';
                        break;
                    case 1:$row['estado'] = 'PEN';
                        if ($row[22] == 0) {
                            $DeudaP += $row['cargo'];
                        }
                        if ($row[22] == 3) {
                            $row['estado'] = 'CAN';
                            $Deuda += $row['cargo'];
                        }

                        //$Deuda += $row['cargo'];
                        break;
                    case 2:$row['estado'] = 'ANU';
                        break;
                    case 3:$row['estado'] = 'CAN';
                        break;

                    default:$row['estado'] = 'PEN';

                        $Deuda += $row['cargo'];
                        break;
                };
                // $Deuda -=  $row['abono'];
                break;

            case 5:
                $Operacion = "REBAJAS";

                $Anio = $row['anio'];
                $Mes = $row['mes'];

                $style = "style=color:#E28409;";

                $onclick = '';

                switch ($row['estado']) {
                    case 0:$row['estado'] = 'ANU';
                        break;
                    case 1:$row['estado'] = 'REB';

                        $Deuda -= $row['abono'];
                        break;
                    default:$row['estado'] = 'REB';

                        $Deuda -= $row['abono'];
                        break;
                };

                break;

            case 6:
                $Operacion = "FACT DE CARGOS";

                $Anio = $row['anio'];
                $Mes = $row['mes'];

                $style = "style=color:red;";

                $onClickDetalle = "abrir_detalle_varios(" . $row["nrocredito"] . ")";
                $onclick = "<span class='icono-icon-detalle' onclick='" . $onClickDetalle . "' title='Ver Detalle de Creditos'></span>";

                switch ($row['estado']) {
                    case 1:$row['estado'] = 'ING';
                        break;
                    case 0:$row['estado'] = 'ANU';
                        break;
                    default:$row['estado'] = 'ING';
                        break;
                };

                $DeudaP += $row['cargo'];

                break;

            case 7:
                switch ($row['codtipodeuda']) {
                    case 3:
                        $Operacion = $row['categoria'] . "(REFINANCIAMIENTO)";
                        break;
                    case 4:
                        $Operacion = $row['categoria'] . "(CREDITOS)";
                        break;
                    case 5:
                        $Operacion = $row['categoria'] . "(CARGOS)";
                        break;
                    default:
                        $Operacion = $row['categoria'] . "";
                        break;
                }

                $Anio = $row['anio'];
                $Mes = $row['mes'];

                $style = "style=color:red;";

                $onclick = '';
                $onClickDetalle = "abrir_detalle_colaterales(" . $row["nropago"] . ")";
                $onclick = "<span class='icono-icon-detalle' onclick='" . $onClickDetalle . "' title='Ver Detalle de Colaterales' ></span>";

                $row['estado'] = '';

                //nroreclamo = d.coddocumento
                if ($row['cat'] <> 3 || $row['nroreclamo'] == 13 || $row['nroreclamo'] == 14) {
                    $Deuda += $row['cargo'];
                }

                break;

            case 8:
                $Operacion = "FACT. DE CONVENIOS";

                $Anio = $row['anio'];
                $Mes = $row['mes'];
                $AnioT = $row['anio'];
                $MesT = $row['mes'];

                switch ($row['codtipodeuda']) {
                    case 9: //CARGOS SI DEBE SUMAR
                        $Deuda += $row['cargo'];
                        $Operacion = "FACT. DE CARGOS";

                        break;
                    case 10: //CARGOS SI DEBE SUMAR
                        $Deuda += $row['cargo'];
                        $Operacion = "FACT. DE CARGOS";

                        break;
                    case 4 or 7: //CUOTAS DE CREDITOS NO MIGRADOS
                        if (!array_key_exists($row['nrocredito'], $nrocredito)) {
                            $Deuda += $row['cargo'];
                        }

                        break;
                }

                $style = "style=color:red;";

                $onClickDetalle = "abrir_detalle_facturacion2(" . $row["facturacion"] . "," . $row["cat"] . "," . $row['codtipodeuda'] . ")";
                $onclick = "<span class='icono-icon-detalle' onclick='" . $onClickDetalle . "' title='Ver Detalle de Facturacion' ></span>";

                switch ($row['estado']) {
                    case 1:$row['estado'] = 'PEN';
                        break;
                    case 2:$row['estado'] = 'PAG';
                        break;
                    case 3:$row['estado'] = 'AMO';
                        break;
                    case 4:$row['estado'] = 'ANU';

                        $Deuda -= $row['cargo'];
                        break;
                    default:$row['estado'] = 'PEN';
                        break;
                };

                if ($row["cat"] == 2) {//REFINANCIADO
                    $row['estado'] = 'REF';
                    $Deuda -= $row['cargo'];
                }

                break;
            default:;
                break;
        }
        ?>
                <tr <?=$title ?> <?=$class ?> onclick="SeleccionaId(this);" id="<?=$c ?>" <?=$style ?> >
                    <td align="center"><?=$c ?></td>
                    <td align="center" valign="middle"><?=$objMantenimiento->DecFecha($row['fechareg']) ?></td>
                    <td align="center" valign="middle"><?=substr($row['hora'], 0, 5) ?></td>
                    <td align="left" valign="middle" style="padding-left:5px;"><?=substr($meses[$row['mes']], 0, 3) . " - " . $row['anio'] ?></td>
                    <td align="center" valign="middle"><?=$row['serie'] ?></td>
                    <td align="center" valign="middle"><?=$row['nrodocumento'] = ($row['nrodocumento'] == 0) ? '&nbsp;' : $row['nrodocumento']; ?></td>
                    <td align="left" valign="middle" style="padding-left:5px;"><?=$Operacion ?></td>
                    <td align="center" valign="middle"><?=substr($row['categoria'], 0, 7) ?></td>
                    <!--<td align="left" valign="middle">&nbsp;</td>-->
                    <td align="center" valign="middle"><?=substr($row['lectura'], 0, -3) ?></td>
                    <td align="center" valign="middle"><?=substr($row['consumo'], 0, -3) ?></td>
                    <td align="right" valign="middle"><?=$row['cargo'] = ($row['cargo'] == 0) ? '&nbsp;' : number_format($row['cargo'], 2); ?></td>
                    <td align="right" valign="middle"><?=$row['abono'] = ($row['abono'] == 0) ? '&nbsp;' : number_format($row['abono'], 2); ?></td>
                    <td align="right" valign="middle"><?=number_format($Deuda, 2) ?></td>

                    <td align="center"><?=$row['estado']; ?></td>
                    <td>
        <?=$onclick; ?> <?=$rec ?>
                    </td>
                </tr>
        <?php
    endforeach;
else:
    ?>
            <tr>
                <td colspan="16" align="center" style="color:red;">NO EXISTE NINGUN MOVIMIENTO PARA ESTE CLIENTE</td>
            </tr>
        <?php
        endif;
        ?>
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td colspan="13" align="right" >Total :</td>
            <td align="right" ><?=number_format($Deuda, 2) ?></td>
            <td scope="col" colspan="2" align="center">&nbsp;</td>
        </tr>
<?php
//CALCULO DE INTERESES A LA FECHA
$sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
$sql .= "FROM facturacion.periodofacturacion ";
$sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = 1 AND facturacion = 0";

$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc));

$row = $consulta->fetch();

$TasaInteres = $row['tasapromintant'] / 100;

$sql = "SELECT * FROM cobranza.view_cobranza ";
$sql .= "WHERE codemp = 1 AND codsuc = ? AND nroinscripcion = ? AND codtipodeuda NOT IN (11) ";
$sql .= "ORDER BY nrofacturacion";

$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc, $NroInscripcion));

$fdescarga = date('Y-m-d');
$intereses = 0;

foreach ($consulta->fetchAll() as $row) {
    $fechavencimiento = $row["fechavencimiento"];

    if ($fdescarga > $fechavencimiento) {
        $dias = $objMantenimiento->dias_transcurridos($fechavencimiento, $fdescarga);
        $intereses += round($row["imptotal"] * $dias * ($TasaInteres / 30), 1);
        //die($fechavencimiento.','.$fdescarga.'==>'.$dias);
    }
}
?>
        <tr>
            <td colspan="13" align="right" >Interes Moratorio al <?=date('d/m/Y') ?> :</td>
            <td align="right" ><?=number_format($intereses, 2) ?></td>
            <td scope="col" colspan="2" align="center">
                <span class='icono-icon-detalle' onclick='abrir_detalle_interes(<?=$codsuc ?>,<?=$NroInscripcion ?>,<?=$TasaInteres ?>)' title='Ver Detalle de Intereses' >
            </td>
        </tr>
        <tr>
            <td colspan="13" align="right" >Deuda Total al <?=date('d/m/Y') ?> :</td>
            <td align="right" ><?=number_format($intereses + $Deuda, 2) ?></td>
            <td scope="col" colspan="2" align="center">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="13" align="right" >Deuda por Facturar :</td>
            <td align="right" ><?=number_format($DeudaP, 2) ?></td>
            <td scope="col" colspan="2" align="center">&nbsp;</td>
        </tr>
    </tfoot>
</table>
<div id="dialog-form-detalle-facturacion" title="Ver Detalle de Facturacion"  >
    <div id="detalle-facturacion">
    </div>
</div>
<div id="dialog-form-detalle-pagos" title="Ver Detalle de Pagos"  >
    <div id="detalle-pagos">
    </div>
</div>

<div id="dialog-form-detalle-colaterales" title="Ver Detalle de Colaterales"  >
    <div id="detalle-colaterales">
    </div>
</div>
<div id="dialog-form-detalle-creditos" title="Ver Detalle de Creditos"  >
    <div id="detalle-creditos">
    </div>
</div>
<div id="dialog-form-detalle-refinanciamiento" title="Ver Detalle de Financiamiento"  >
    <div id="detalle-refinanciamiento">
    </div>
</div>
<div id="dialog-form-detalle-varios" title="Ver Detalle de Creditos Varios"  >
    <div id="detalle-varios">
    </div>
</div>
<div id="dialog-form-detalle-interes" title="Ver Detalle de Interes"  >
    <div id="detalle-interes">
    </div>
</div>
<div id="dialog-form-imprimir" title="Seleccione el Formato de Impresion" style="display:none" >
    <input type="hidden" name="formato" id="formato" value="0" />
    <div id="buttonsetrec" style="display:inline">
        <input type="radio" name="radio" id="rbformato1" value="rbformato1" onclick="javascript:$('#formato').val(1);" />
        <label for="rbformato1">Formato 1</label>
        <input type="radio" name="radio" id="rbformato2" value="rbformato2" onclick="javascript:$('#formato').val(2);" />
        <label for="rbformato2">Formato 2</label>
        <input type="radio" name="radio" id="rbformato3" value="rbformato3" onclick="javascript:$('#formato').val(3);" />
        <label for="rbformato3">Formato 3</label>    
        <input type="radio" name="radio" id="rbformato4" value="rbformato4" onclick="javascript:$('#formato').val(4);" />
        <label for="rbformato4">Formato 4</label>    
        <input type="radio" name="radio" id="rbformato5" value="rbformato5" onclick="javascript:$('#formato').val(5);" />
        <label for="rbformato5">Formato 5</label>
        <input type="radio" name="radio" id="rbformato6" value="rbformato6" onclick="javascript:$('#formato').val(6);" />
        <label for="rbformato6">Formato 6</label>
        <input type="radio" name="radio" id="rbformato7" value="rbformato7" onclick="javascript:$('#formato').val(7);" />
        <label for="rbformato7">Formato 7</label>
        <input type="radio" name="radio" id="rbformato8" value="rbformato8" onclick="javascript:$('#formato').val(8);" />
        <label for="rbformato8">Formato 8</label>
        <input type="radio" name="radio" id="rbformato9" value="rbformato9" onclick="javascript:$('#formato').val(9);" />
        <label for="rbformato9">Formato 9</label>
        <input type="radio" name="radio" id="rbformato10" value="rbformato10" onclick="javascript:$('#formato').val(10);" />
        <label for="rbformato10">Formato 10</label>         
    </div>
</div>