<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../objetos/clsReporte.php");

	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $NroInscripcion, $codsuc, $objreporte, $conexion, $Dim;

			$h=4;
			$this->SetFont('Arial','B',12);
			$this->SetY(8);
			$this->Cell(0, $h + 1, "ESTADO DE CUENTA CORRIENTE", 0, 1, 'C');
			$this->Ln(5);

			$Sql = "SELECT clie.nroinscripcion, ".$objreporte->getCodCatastral("clie.").", clie.propietario, td.abreviado, clie.nrodocumento, ";
			$Sql .= " tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle AS direccion, ";
			$Sql .= " es.descripcion, tar.nomtar, clie.codantiguo, ts.descripcion AS tiposervicio, clie.tipofacturacion, ";
			$Sql .= " clie.nromed, clie.consumo, clie.codestadoservicio, co.fechainsmed ";
			$Sql .= "FROM catastro.clientes AS clie ";
			$Sql .= " INNER JOIN catastro.conexiones co ON (clie.codemp = co.codemp) ";
			$Sql .= "  AND (clie.codsuc = co.codsuc) AND (clie.nroinscripcion = co.nroinscripcion) ";
			$Sql .= " INNER JOIN public.calles AS cal ON(clie.codemp = cal.codemp AND clie.codsuc = cal.codsuc AND clie.codcalle = cal.codcalle AND clie.codzona = cal.codzona) ";
			$Sql .= " INNER JOIN public.tiposcalle AS tipcal ON(cal.codtipocalle = tipcal.codtipocalle) ";
			$Sql .= " INNER JOIN public.tipodocumento AS td ON(clie.codtipodocumento = td.codtipodocumento) ";
			$Sql .= " INNER JOIN public.estadoservicio AS es ON(es.codestadoservicio = clie.codestadoservicio) ";
			$Sql .= " INNER JOIN public.tiposervicio AS ts ON(ts.codtiposervicio = clie.codtiposervicio) ";
			$Sql .= " INNER JOIN facturacion.tarifas AS tar ON(clie.codemp = tar.codemp AND clie.codsuc = tar.codsuc AND clie.catetar = tar.catetar) ";
			$Sql .= "WHERE clie.codemp = 1 AND clie.codsuc = ".$codsuc." AND clie.nroinscripcion = ".$NroInscripcion."";

			$Consulta = $conexion->query($Sql);
			$row = $Consulta->fetch();

			define('Cubico', chr(179));

			switch ($row['tipofacturacion'])
			{
				case 0: $TipoFacturacion ='CONSUMO LEIDO ( Lect. Ultima '.number_format($row['consumo'], 0).' m'.Cubico.')';break;
				case 1: $TipoFacturacion ='CONSUMO PROMEDIADO ('.number_format($row['consumo'], 0).' m'.Cubico.')';break;
				case 2: $TipoFacturacion ='CONSUMO ASIGNADO ('.number_format($row['consumo'], 0).' m'.Cubico.')';break;
			}

			$sql = "SELECT uso.catetar AS catetar, tar.nomtar AS nombre, uso.porcentaje AS porcentaje ";
			$sql .= "FROM catastro.unidadesusoclientes AS uso ";
			$sql .= " INNER JOIN facturacion.tarifas tar ON (uso.codemp = tar.codemp AND uso.codsuc = tar.codsuc AND uso.catetar = tar.catetar) ";
			$sql .= "WHERE uso.codemp = 1 AND uso.codsuc = ".$codsuc." AND uso.nroinscripcion = ".$NroInscripcion." AND tar.estado = 1";

		    $Consulta = $conexion->query($sql);
		    $raw = $Consulta->fetchAll();

		    $categorias = '';

		    foreach ($raw as $kew) :
        		$categorias .= trim(substr($kew['nombre'], 0, 7)) ."(". number_format($kew['porcentaje']) . "%)-";
		    endforeach;

		    $categorias = substr($categorias, 0, -1);

			$Medidor = 'No';
			if(trim($row['nromed'])!="")
				$Medidor='Si ( N° '.$row['nromed'].')';
			$this->SetLeftMargin(10);
			$this->Ln(5);
			$h=5;
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("N° de Inscripcion"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,$row['codantiguo'],0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Código Catastral"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,$row[1],0,1,'L');
			/*-------------*/
			// $this->SetFont('Arial','B',10);
			// $this->Cell(30,$h,utf8_decode("Código Antiguo"),0,0,'L');
			// $this->Cell(5,$h,":",0,0,'L');
			// $this->SetFont('Arial','',10);
			// $this->Cell(100,$h,$row['codantiguo'],0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode($row['abreviado']),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,$row['nrodocumento'],0,1,'L');
			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Cliente"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,utf8_decode($row['propietario']),0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,'Tipo Servicio',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,$row['tiposervicio'],0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Dirección"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,strtoupper($row['direccion']),0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Estado"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,$row['descripcion'],0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,'Categoria',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,$categorias,0,1,'L');
			/*-------------*/
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,utf8_decode("Tipo Facturación"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(100,$h,$TipoFacturacion,0,0,'L');

			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,'Medidor',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',10);
			$this->Cell(30,$h,utf8_decode($Medidor),0,1,'L');
            $this->SetFont('Arial','B',10);
            $this->Cell(30,$h,utf8_decode('Fecha de Inst.'),0,0,'L');
            $this->Cell(5,$h,":",0,0,'L');
            $this->SetFont('Arial','',10);
            $this->Cell(30,$h,$objreporte->CodFecha($row['fechainsmed']),0,1,'L');
			$h=4;
			$this->Ln(5);
			$this->SetX(10);
			$this->SetFont('Arial','B',8);
	 	 	$this->Cell($Dim[1],5,utf8_decode('N°'),1,0,'C',false);
	 	 	$this->Cell($Dim[2],5,utf8_decode('Fecha'),1,0,'C',false);
	 	 	$this->Cell($Dim[3],5,utf8_decode('Hora'),1,0,'C',false);
			$this->Cell($Dim[4],5,utf8_decode('Mes Facturación'),1,0,'C',false);
			$this->Cell($Dim[5],5,utf8_decode('Serie'),1,0,'C',false);
			$this->Cell($Dim[6],5,utf8_decode('Número'),1,0,'C',false);
			$this->Cell($Dim[7],5,utf8_decode('Operación'),1,0,'C',false);
			$this->Cell($Dim[8],5,utf8_decode('Cat'),1,0,'C',false);
			$this->Cell($Dim[9],5,utf8_decode('Co'),1,0,'C',false);
			$this->Cell($Dim[10],5,utf8_decode('Lectura'),1,0,'C',false);
			$this->Cell($Dim[11],5,utf8_decode('Consu'),1,0,'C',false);
			$this->Cell($Dim[12],5,utf8_decode('Cargo'),1,0,'C',false);
			$this->Cell($Dim[13],5,utf8_decode('Abono'),1,0,'C',false);
			$this->Cell($Dim[14],5,utf8_decode('Saldo'),1,0,'C',false);
			$this->Cell($Dim[15],5,utf8_decode('Est. Rec.'),1,1,'C',false);

		}
		function Detalle()
		{
			global $NroInscripcion, $codsuc, $objreporte, $conexion, $Dim, $meses;

			$Condicion1 = " WHERE f.nroinscripcion =" . $NroInscripcion. " AND f.codsuc =".$codsuc." AND tar.estado = 1";
			$Orden1     = " ORDER BY f.nroinscripcion ASC ";

			$Condicion2 = " WHERE p.nroinscripcion =". $NroInscripcion . "  AND d.nrofacturacion <> 0 AND p.anulado = 0 AND p.codsuc =".$codsuc;
			$Orden2     = " ORDER BY p.nroinscripcion ASC ";

			$Condicion3 = " WHERE p.nroinscripcion =" . $NroInscripcion . "  AND d.nrofacturacion = 0 AND p.anulado = 0 AND p.codsuc =".$codsuc;
			$Orden3     = " ORDER BY p.nroinscripcion ASC ";

			$Condicion4 = " WHERE c.nroinscripcion =" . $NroInscripcion. " AND c.codsuc =".$codsuc;
			$Orden4     = " ORDER BY c.nroinscripcion ASC ";

			$Condicion5 = " WHERE c.nroinscripcion =" . $NroInscripcion. " AND c.codsuc =".$codsuc;
			$Orden5     = " ORDER BY c.nroinscripcion ASC ";

			$Condicion6 = " WHERE rb.nroinscripcion =" . $NroInscripcion. " AND rb.codsuc =".$codsuc;
			$Orden6     = " ORDER BY rb.nroinscripcion ASC ";

//(0)
//Facturaciones 
			$Sql = "(SELECT
						f.fechareg,
						tar.nomtar AS categoria,
						f.serie,
						f.nrodocumento,
						'00:00:00' AS hora,
						".$this->Convert('f.anio','INTEGER')." AS anio,
						".$this->Convert('f.mes','INTEGER')." AS mes,
						sum(df.importe) AS cargo,
						'0.00' AS abono,
						'0' AS movimiento,
						f.enreclamo AS reclamo,
						f.lecturaultima AS lectura,
						f.consumofact AS consumo,
						df.nrofacturacion AS facturacion,
						0 AS cat,
						0 AS codtipodeuda,
						'0' AS nropago,
						f.estadofacturacion AS estado,
						0 AS nrocredito,
						0 AS orden,
						sum(df.importerebajado) as rebajado,
						f.nroreclamo AS nroreclamo,
						0
					FROM facturacion.cabfacturacion f
						INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
						INNER JOIN facturacion.tarifas as tar ON (f.codemp=tar.codemp and f.codsuc=tar.codsuc and f.catetar=tar.catetar)
					".$Condicion1." 
						/*AND df.nrocredito = 0*/ AND df.nrorefinanciamiento = 0 AND df.codconcepto <> 10012 
					GROUP BY f.estadofacturacion, df.nrofacturacion, f.enreclamo, tar.nomtar, f.lecturaultima, f.consumofact, 
						f.fechareg, f.serie, f.nrodocumento, f.anio, f.mes, f.nrofacturacion, f.nroreclamo) ";
//(1)
//Pagos de Facturacion de Servicios
			$Sql .= "UNION
					(SELECT
						p.fechareg,
						' ' as categoria,
						d.serie,
						d.nrodocumento,
						p.hora as hora,
						".$this->Convert('d.anio','INTEGER')." as anio,
						".$this->Convert('d.mes','INTEGER')." AS mes,
						'0.00' as cargo,
						sum(d.importe) as abono,
						'1' as movimiento,
						'0' as reclamo,
						'0' as lectura,
						'0' as consumo,
						d.nrofacturacion as facturacion,
						'0' as cat,
						'0' as codtipodeuda,
						d.nropago as nropago,
						'0' as estado,
						0 as nrocredito,
						3 as orden,
						0 as rebajado,
						0 as nroreclamo,
						0
					FROM
						cobranza.cabpagos p
						INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp) AND (p.codsuc = d.codsuc) AND (p.nroinscripcion = d.nroinscripcion) AND (p.nropago = d.nropago) 
						".$Condicion2."
					GROUP BY  d.nropago, d.nrofacturacion, p.hora, p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes
					ORDER BY p.fechareg ASC)  ";
//(2)
//Pagos Colaterales
			$Sql .= "UNION
                	(SELECT
						p.fechareg,
						co.descripcion as categoria,
						d.serie,
						d.nrodocumento,
						p.hora as hora,
						".$this->Convert('d.anio','INTEGER')." as anio,
						".$this->Convert('d.mes','INTEGER')." AS mes,
						'0.00' as cargo,
						/*SUM(d.importe) AS abono,*/
						p.imptotal AS abono,
						'2' as movimiento,
						'0' as reclamo,
						'0' as lectura,
						'0' as consumo,
						'0' as facturacion,
						'0' as cat,
						d.codtipodeuda as codtipodeuda,
						d.nropago as nropago,
						'0' as estado,
						0 as nrocredito,
						4 as orden,
						0 as rebajado,
						0 as nroreclamo,
						0
					FROM cobranza.cabpagos p
						INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp) AND (p.codsuc = d.codsuc) AND (p.nroinscripcion = d.nroinscripcion) AND (p.nropago = d.nropago)
						INNER JOIN facturacion.conceptos co ON (co.codemp = d.codemp AND co.codsuc = d.codsuc AND co.codconcepto = d.codconcepto)
                ".$Condicion3."
				AND d.codconcepto NOT IN(5, 8, 7)
                GROUP BY  d.codtipodeuda, d.nropago, p.hora, p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes, co.descripcion, p.imptotal
                ORDER BY p.fechareg ASC) ";
//(3)
//Créditos (0)Pendientes - (1)Facturados - (3)Cancelados - (4)Quebrados
			$Sql .= "UNION
					(SELECT
						c.fechareg,
						' ' as categoria,
						' '  as serie,
						'0' as nrodocumento,
						'00:00:00' as hora,
						".$this->Convert('extract(year from c.fechareg)','INTEGER')." as anio,
						".$this->Convert('extract(month from c.fechareg)','INTEGER')." AS mes,
						SUM(CAST(d.imptotal AS NUMERIC(18,2)) + d.redondeo) AS cargo,
						'0.00' as abono,
						'3' as movimiento,
						'0' as reclamo,
						'0' as lectura,
						'0' as consumo,
						'0' as facturacion,
						'0' as cat,
						'0' as codtipodeuda,
						'0' as nropago,
						c.estareg as estado,
						c.nrocredito as nrocredito,
						2 as orden,
						0 as rebajado,
						0 as nroreclamo,
						d.estadocuota
					FROM facturacion.cabcreditos c
						INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nrocredito = d.nrocredito)
					".$Condicion4." 
						AND d.estadocuota <> 2 AND d.estadocuota <> 5
					GROUP BY c.fechareg, c.subtotal, c.estareg, c.nrocredito, d.estadocuota
					ORDER BY c.fechareg ASC) ";
//(4)
//Refinanciamientos
			$Sql .= "UNION
					(SELECT
						c.fechaemision,
						' ' as categoria,
						' ' as serie,
						'0' as nrodocumento,
						'00:00:00' as hora,
						".$this->Convert('extract(year from c.fechaemision)','INTEGER')." as anio,
						".$this->Convert('extract(month from c.fechaemision)','INTEGER')." AS mes,
						SUM(d.importe) AS cargo,
						'0.00' AS abono,
						'4' as movimiento,
						'0' as reclamo,
						'0' as lectura,
						'0' as consumo,
						'0' as facturacion,
						'0' as cat,
						'0' as codtipodeuda,
						'0' as nropago,
						c.estareg as estado,
						c.nrorefinanciamiento as nrocredito,
						2 as orden,
						0 as rebajado,
						0 as nroreclamo,
						d.estadocuota
					FROM facturacion.cabrefinanciamiento c
						INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nrorefinanciamiento = d.nrorefinanciamiento)
					".$Condicion5."
					GROUP BY c.fechaemision,c.totalrefinanciado,c.estareg,c.nrorefinanciamiento, d.estadocuota
					ORDER BY c.fechaemision ASC) ";
//(5)
//Rebajas
			$Sql .= "UNION
					(SELECT
						rb.fechareg,
						' ' as categoria,
						d.seriedocumento as serie,
						d.nrodocumentoabono as nrodocumento,
						'00:00:00' as hora,
						".$this->Convert('d.anio','INTEGER')." as anio,
						".$this->Convert('d.mes','INTEGER')." AS mes,
						'0.00' as cargo,
						SUM(d.imprebajado) as abono,
						'5' as movimiento,
						'0' as reclamo,
						'0' as lectura,
						'0' as consumo,
						'0' as facturacion,
						'0' as cat,
						'0' as codtipodeuda,
						'0' as nropago,
						rb.codestrebaja as estado,
						0 as nrocredito,
						2 as orden,
						0 as rebajado,
						0 as nroreclamo,
						0
					FROM facturacion.cabrebajas rb
						INNER JOIN facturacion.detrebajas d ON ( rb.nrorebaja =  d.nrorebaja AND rb.codemp = d.codemp AND rb.codsuc = d.codsuc)
					".$Condicion6." 
						/*AND rb.fechaemision > '2014-09-30'*/
					GROUP BY rb.fechareg, d.seriedocumento, d.nrodocumentoabono, d.anio, d.mes, rb.codestrebaja
					ORDER BY rb.fechareg ASC) ";
//(6)
//Cargos
			$Sql .= "UNION
					(SELECT
						c.fechareg,
						' ' as categoria,
						' '  as serie,
						'0' as nrodocumento,
						'00:00:00' as hora,
						".$this->Convert('extract(year from c.fechareg)','INTEGER')." as anio,
						".$this->Convert('extract(month from c.fechareg)','INTEGER')." AS mes,
						c.imptotal as cargo,
						'0.00' as abono,
						'6' as movimiento,
						'0' as reclamo,
						'0' as lectura,
						'0' as consumo,
						'0' as facturacion,
						'0' as cat,
						'0' as codtipodeuda,
						'0' as nropago,
						c.estareg as estado,
						c.nrocredito as nrocredito,
						5 as orden,
						0 as rebajado,
						0 as nroreclamo,
						0
					FROM facturacion.cabvarios c
						INNER JOIN facturacion.detvarios d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc)  AND (c.nrocredito = d.nrocredito)
					".$Condicion4."
						AND d.estadocuota = 0
					GROUP BY c.fechareg,c.imptotal,c.estareg,c.nrocredito
					ORDER BY c.fechareg ASC) ";
//(7)
//Pagos
			$Sql .= "UNION
					(SELECT
						p.fechareg,
						co.descripcion as categoria,
						d.serie,
						d.nrodocumento,
						p.hora as hora,
						".$this->Convert('d.anio','INTEGER')." as anio,
						".$this->Convert('d.mes','INTEGER')." AS mes,
						/*SUM(d.importe) AS cargo,*/ 
						p.imptotal AS cargo,
						'0.00' as abono,
						'7' as movimiento,
						'0' as reclamo,
						'0' as lectura,
						'0' as consumo,
						'0' as facturacion,
						d.categoria AS cat,
						d.codtipodeuda as codtipodeuda,
						d.nropago as nropago,
						'0' as estado,
						0 as nrocredito,
						3 as orden,
						0 as rebajado,
						d.coddocumento AS nroreclamo,
						0
					FROM cobranza.cabpagos p
						INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp) AND (p.codsuc = d.codsuc) AND (p.nroinscripcion = d.nroinscripcion) AND (p.nropago = d.nropago)
						INNER JOIN facturacion.conceptos co ON (co.codemp = d.codemp AND co.codsuc = d.codsuc AND co.codconcepto = d.codconcepto)
					".$Condicion3." 
						AND d.codconcepto NOT IN(5, 10, 992, 10005)
					GROUP BY  d.codtipodeuda, d.nropago, p.hora, p.fechareg, d.serie, d.nrodocumento, d.anio, d.mes, co.descripcion, d.categoria, d.coddocumento, p.imptotal
					ORDER BY p.fechareg ASC) ";
//(8)
//FACT Convenios, Cargos
		//$Sql .= "UNION
                //(SELECT
//                f.fechareg,
//                tar.nomtar as categoria,
//                f.serie,
//                f.nrodocumento,
//                '00:00:00' as hora,
//                ".$this->Convert('f.anio','INTEGER')." as anio,
//                ".$this->Convert('f.mes','INTEGER')." AS mes,
//                sum(df.importe) as cargo,
//                '0.00' as abono,
//                '8' as movimiento,
//                f.enreclamo as reclamo,
//                f.lecturaultima as lectura,
//                f.consumofact as consumo,
//                df.nrofacturacion as facturacion,
//                df.categoria as cat,
//                df.codtipodeuda as codtipodeuda,
//                '0' as nropago,
//                f.estadofacturacion as estado,
//                df.nrocredito as nrocredito,
//                1 as orden,
//                0 as rebajado,
//				0 as nroreclamo
//                FROM facturacion.cabfacturacion f
//                INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion)
//                AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
//                INNER JOIN facturacion.tarifas as tar ON (f.codemp=tar.codemp and f.codsuc=tar.codsuc and f.catetar=tar.catetar)
//                ".$Condicion1."  AND (df.nrocredito<>0 OR df.nrorefinanciamiento<>0)
//                GROUP BY f.estadofacturacion, df.categoria,
//                df.nrofacturacion, f.enreclamo, tar.nomtar,
//                f.lecturaultima, f.consumofact, f.fechareg,
//                f.serie,f.nrodocumento,f.anio, f.mes,
//                f.nrofacturacion,df.codtipodeuda,df.nrocredito
//                )
			$Sql .= "ORDER by fechareg, hora, anio, mes, orden ASC ";  


			$c = 0;
			$Deuda = 0;
			
			$DeudaP = 0;

			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);
			$this->SetLeftMargin(10);
			$this->SetFont('Arial','',8);

            $nrocredito = array();

			foreach($conexion->query($Sql)->fetchAll() as $row)
			{
				$c++;

				$Operacion = '';

				switch ($row['movimiento']) {
                    case 0:
                        $Operacion = "FACTURACIÓN DE PENSIONES";
						
                        $Anio      = $row['anio'];
                        $Mes       = $row['mes'];
                        $AnioT     = $row['anio'];
                        $MesT      = $row['mes'];
						
                        $Deuda     +=  $row['cargo'];

                        $color = $this->SetTextColor(212, 9, 9);
						
                        switch($row['estado']){
                            case 1:$row['estado'] = 'PEN';break;
                            case 2:
								$row['estado'] = 'PAG';
								
								$color = $this->SetTextColor(4, 4, 180);
						
								break;
                            case 3:$row['estado'] = 'AMO';break;
                            case 4:$row['estado'] = 'ANU';$Deuda -= $row['cargo'];break;
							case 5:
								$row['estado'] = 'PAG AD';
								
								$Deuda -= $row['cargo'];
								
								$color = $this->SetTextColor(4, 4, 180);
								
								break;
                            default:$row['estado'] = 'PEN';break;
                        };
                        if($row["cat"]==2)//REFINANCIADO
                        {
                            $row['estado'] = 'REF';
                            $Deuda-=$row['cargo'];
                            $Deuda     +=  $row['rebajado'];
                        }
                        if($row["codtipodeuda"]==11)//QUEBRADO
                        {
                            $row['estado'] = 'QBD';
                            $Deuda-=$row['cargo'];
                        }
                        // $this->SetTextColor(255,0,0);

                        break;
                    case 1:
                        if($row['anio'] == $AnioT && $row['mes'] == $MesT)
                            $Operacion = "PAGO RECIBO DE PENSIONES";
                        else
                            $Operacion = "PAGO DEUDA ACUMULADA";

                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $Deuda -=  $row['abono'];
                        $row['estado'] = '';
                        $color = $this->SetTextColor(6, 115, 20);

                    break;
                    case 2:
                    	switch ($row['codtipodeuda']) {
                            case 3:
                                $Operacion = $row['categoria']."(REFINANCIAMIENTO)";
                                break;
                            case 4:
                                $Operacion = $row['categoria']."(CREDITOS)";
                                break;
                            case 4:
                                $Operacion = $row['categoria']."(CREDITOS)";
                                break;
                            default:
                                $Operacion = 'PAGO '.$row['categoria'];
                                break;
                        }

						// $row['categoria'] = '';
						$Anio             = $row['anio'];
						$Mes              = $row['mes'];
						$row['estado']    = '';
						$Deuda -= $row['abono'];
                        $color = $this->SetTextColor(6, 115, 20);

                    break;
                    case 3:
                        $Operacion = "FACTURACION DE CREDITOS";
                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];

                        switch($row['estado']){
                            case 0:$row['estado'] = 'ANU'; break;
                            case 1:
								$row['estado'] = 'PEN';
								
								if ($row[22] == 0) {
									$DeudaP += $row['cargo'];
								}
								if ($row[22] == 3) {
									$row['estado'] = 'CAN';
									$Deuda += $row['cargo'];
								}
								
								break;
                            case 2:$row['estado'] = 'CAN'; $Deuda += $row['cargo']; break;
                            case 3:$row['estado'] = 'CAN'; break;
                            case 4:$row['estado'] = 'QUE'; break;
                        };
                        if (!array_key_exists($row['nrocredito'], $nrocredito))
                            {
                                $nrocredito[$row['nrocredito']] = $row['nrocredito'];
                            }
                        $color = $this->SetTextColor(0,0,139);

                    break;
                    case 4:
                        $Operacion = "PAGO POR REFINANCIAMIENTO";
                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];

                        switch($row['estado']){
                            case 0:$row['estado'] = 'ANU'; break;
                            case 1:
								$row['estado'] = 'PEN';
								
								if ($row[22] == 0) {
									$DeudaP += $row['cargo'];
								}
								if ($row[22] == 3) {
									$row['estado'] = 'CAN';
									$Deuda += $row['cargo'];
								}
		
								//$Deuda += $row['cargo'];
								break;
                            case 2:$row['estado'] = 'ANU'; break;
                            case 3:$row['estado'] = 'CAN'; break;
                            default:$row['estado'] = 'PEN'; $Deuda += $row['abono']; break;
                        };
                        $color = $this->SetTextColor(32,178,170);

                    break;
                    case 5:
                        $Operacion = "REBAJAS";
                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $color = $this->SetTextColor(226, 132, 9);

                        switch($row['estado']){
                            case 0:$row['estado'] = 'ANU'; break;
                            case 1:$row['estado'] = 'REB'; $Deuda -=  $row['abono']; break;
                            default:$row['estado'] = 'REB'; $Deuda -=  $row['abono']; break;
                        };

                    break;
                    case 6:
                        $Operacion = "FACTURACION DE CARGOS";
                        $Anio  = $row['anio'];
                        $Mes   = $row['mes'];
                        $color = $this->SetTextColor(255,186,0);

                        switch($row['estado']){
                            case 1:$row['estado'] = 'ING'; break;
                            case 0:$row['estado'] = 'ANU'; break;
                            default:$row['estado'] = 'ING'; break;
                        };
                        //$Deuda     +=  $row['cargo'];
						$DeudaP += $row['cargo'];

                   		break;
                    case 7:
                        switch ($row['codtipodeuda']) {
                            case 3:
                                $Operacion = $row['categoria']."(REFINANCIAMIENTO)";
                                break;
                            case 4:
                                $Operacion = $row['categoria']."(CREDITOS)";
                                break;
                            case 4:
                                $Operacion = $row['categoria']."(CREDITOS)";
                                break;
                            default:
                                $Operacion = $row['categoria'];
                                break;
                        }

                        // $row['categoria'] = '';
                        $Anio             = $row['anio'];
                        $Mes              = $row['mes'];
                        $row['estado']    = '';

						//nroreclamo = d. coddocumento
						if ($row['cat'] <> 3 || $row['nroreclamo'] == 13 || $row['nroreclamo'] == 14)
						{
                        	$Deuda += $row['cargo'];
						}

                        //$Deuda +=  $row['cargo'];
                        $color = $this->SetTextColor(212, 9, 9);

                    	break;
                    case 8:
                        $Operacion = "FACTURACIÓN DE CONVENIOS";
                        $Anio      = $row['anio'];
                        $Mes       = $row['mes'];
                        $AnioT     = $row['anio'];
                        $MesT      = $row['mes'];

                        switch ($row['codtipodeuda'])
                        {
                            case 9: //CARGOS SI DEBE SUMAR
                                $Deuda     +=  $row['cargo'];
                                $Operacion = "FACT. DE CARGOS";
                            break;
							case 10: //CARGOS SI DEBE SUMAR
                                $Deuda     +=  $row['cargo'];
                                $Operacion = "FACT. DE CARGOS";
                            break;
                            case 4: //CUOTAS DE CREDITOS NO MIGRADOS
                                if (!array_key_exists($row['nrocredito'], $nrocredito))
                                {
                                    $Deuda     +=  $row['cargo'];
                                }

                            break;


                        }
                        $style = "style=color:red;";
                        $color = $this->SetTextColor(212, 9, 9);

                        switch($row['estado']){
                            case 1:$row['estado'] = 'PEN'; break;
                            case 2:$row['estado'] = 'PAG'; break;
                            case 3:$row['estado'] = 'AMO'; break;
                            case 4:$row['estado'] = 'ANU'; $Deuda -= $row['cargo'];break;
                            default:$row['estado'] = 'PEN'; break;
                        };
                        if($row["cat"]==2)//REFINANCIADO
                        {
                            $row['estado'] = 'REF';
                            $Deuda-=$row['cargo'];
                        }
                        // $this->SetTextColor(255,0,0);

                        break;
                    default: ;break;
                }

				$this->SetX(10);
				//$color;
				$this->Cell($Dim[1], 5, $c, 1, 0, 'C', true);
		 	 	$this->Cell($Dim[2], 5, $objreporte->DecFecha($row['fechareg']), 1, 0, 'C', true);
		 	 	$this->Cell($Dim[3], 5, substr($row['hora'], 0, 5), 1, 0, 'C', true);
				$this->Cell($Dim[4], 5, $meses[$row['mes']]." - ".$row['anio'], 1, 0, 'C', true);
				$this->Cell($Dim[5], 5, $row['serie'], 1, 0, 'C', true);
				$this->Cell($Dim[6], 5, $row['nrodocumento'], 1, 0, 'C', true);
				$this->Cell($Dim[7], 5, utf8_decode($Operacion), 1, 0, 'L', true);
				$this->Cell($Dim[8], 5, substr($row['categoria'], 0, 7), 1, 0, 'C', true);
				$this->Cell($Dim[9], 5, " ", 1, 0, 'R', true);
				$this->Cell($Dim[10], 5, substr($row['lectura'], 0, -3), 1, 0, 'C', true);
				$this->Cell($Dim[11], 5, substr($row['consumo'], 0, -3), 1, 0, 'C', true);
				$this->Cell($Dim[12], 5, $row['cargo'] = ($row['cargo']==0)?' ':number_format($row['cargo'], 2), 1, 0, 'R', true);
				$this->Cell($Dim[13], 5, $row['abono'] = ($row['abono']==0)?' ':number_format($row['abono'], 2), 1, 0, 'R', true);
				$this->Cell($Dim[14], 5, number_format($Deuda, 2), 1, 0, 'R', true);
				$this->Cell($Dim[15], 5, $row['estado'], 1, 1, 'C', true);
			}

			$this->Ln(2);
			$this->SetFont('Arial','',10);
			$this->SetTextColor(0,0,0);
			$this->Cell(array_sum($Dim)-($Dim[14] + $Dim[15]),5,'Total :',0,0,'R',true);
			$this->Cell($Dim[14],5,number_format($Deuda,2),0,1,'R',true);

            //CALCULO DE INTERESES A LA FECHA
            $sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
			$sql .= "FROM facturacion.periodofacturacion ";
			$sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = 1 AND facturacion = 0";

            $consulta = $conexion->prepare($sql);
            $consulta->execute(array($codsuc));

			$row = $consulta->fetch();

			$TasaInteres = $row['tasapromintant'] / 100;

            $sql = "SELECT * FROM cobranza.view_cobranza ";
			$sql .= "WHERE codemp = 1 AND codsuc = ? AND nroinscripcion = ? AND codtipodeuda NOT IN (11) ";
			$sql .= "ORDER BY nrofacturacion";

			$consulta = $conexion->prepare($sql);
            $consulta->execute(array($codsuc,$NroInscripcion));

			$fdescarga = date('Y-m-d');
            $intereses = 0;

			foreach($consulta->fetchAll() as $row)
            {
                $fechavencimiento = $row["fechavencimiento"];

                if($fdescarga > $fechavencimiento)
                {
                    $dias = $this->dias_transcurridos($fechavencimiento, $fdescarga);
                    $intereses += round($row["imptotal"] * $dias * ($TasaInteres / 30), 1);
                    //die($fechavencimiento.','.$fdescarga.'==>'.$dias);
                }
            }
			$this->Cell(array_sum($Dim) - ($Dim[14] + $Dim[15]), 5, 'Interes Moratorio al '.date('d/m/Y').' :', 0, 0, 'R', true);
            $this->Cell($Dim[14], 5, number_format($intereses, 2), 0, 1, 'R', true);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(array_sum($Dim) - ($Dim[14] + $Dim[15]), 5, 'Deuda Total al '.date('d/m/Y').' :', 0, 0, 'R', true);
            $this->Cell($Dim[14], 5, number_format($Deuda + $intereses, 2), 0, 1, 'R', true);
			
			$this->SetFont('Arial', '', 10);
			$this->Cell(array_sum($Dim) - ($Dim[14] + $Dim[15]), 5, 'Deuda por Facturar :', 0, 0, 'R', true);
            $this->Cell($Dim[14], 5, number_format($DeudaP, 2), 0, 1, 'R', true);
		}

    }

	define('Cubico', chr(179));

	$codsuc	= $_SESSION["IdSucursal"];
	$NroInscripcion = $_GET["NroInscripcion"];

	$Dim = array('1'=>10, '2'=>17, '3'=>12,'4'=>30, '5'=>12, '6'=>15, '7'=>57, '8'=>10, '9'=>8, '10'=>15, '11'=>15, '12'=>20, '13'=>20, '14'=>20, '15'=>15);
    $x = 20;
    $h = 4;

	$objreporte	= new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("H");
	$objreporte->Detalle("H");
	$objreporte->Output();

?>
