<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsFunciones.php");

$objFunciones = new clsFunciones();
$codsuc = $_SESSION["IdSucursal"];
$nroinscripcion = $_GET["NroInscripcion"];
?>
<style>
    .myTable{
        /*padding:4px;*/

        -moz-border-radius: 25px 10px / 10px 25px;
        border-radius: 25px 10px / 10px 25px;
        border:1px solid #000;
        cursor: pointer;
        padding: 2px;
        margin:1px;

        background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(226,222,222,1) 100%); /* FF3.6+ */
    }
    .estiloX{
        background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px
    }
    .myTableX{
        background-color:#0099CC; border: 1 #000000 solid; color:#FFF; padding:4px; font-weight:bold;
    }
    .estiloz{
        font-weight:bold; color:#F00;
    }
    body{
        padding: 0;
        margin: 0;
        background: #DADADA !important;
    }
    h4 { margin: 3px 0;}
    #box-page {padding:20px 10px;
               box-shadow: 6px 6px 8px #999;
               mox-box-shadow: 6px 5px 8px #999;                   
               background:#FFF;
               min-height: 300px; 
               border: 1px solid #CCC;}
    .box-info { border-bottom: 1px dotted #dadada; padding: 10px 0;}
    #box-info-data { width: 600px;
                     float: left;
                     border-right: 1px dotted #dadada;
    }
    #box-info-meses { width: 265px; float: left;  padding-left: 10px }
    #table-info tr td{ font-size: 11px;}
</style>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/jquery-1.8.3.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/jquery-ui-1.9.2.custom.min.js" language="JavaScript"></script>
<link href="<?php echo $_SESSION['urldir']; ?>css/css_base.css" rel="stylesheet" type="text/css">

<script>
    $(document).ready(function () {

        $("#DivAcordion").accordion({
            heightStyle: "content"
        });
        $("#DivDetRec").dialog({
            autoOpen: false,
            height: 450,
            width: 650,
            modal: true,
            resizable: false,
            buttons: {
                "Cerrar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            }
        });
    })
    function cargar_observacion_reclamos(nrodetalle)
    {
        cargar_ajax_observacion_reclamos(nrodetalle)

    }
    function cargar_ajax_observacion_reclamos(nrodetalle)
    {
        $.ajax({
            url: '../ajax/ver_detalle_reclamo.php',
            type: 'POST',
            async: true,
            data: 'nrodetalle=' + nrodetalle + '&codsuc=<?php echo $codsuc ?>',
            success: function (datos) {
                $("#div_detalle_reclamos").html(datos)
                $("#DivDetRec").dialog("open")
            }
        })
    }
</script>
<div id="DivAcordion">
    <?php
    $Sql = "SELECT c.propietario,r.nroinscripcion,m.descripcion as medio_reclamo,t.descripcion as tipo_parentesco,r.reclamante,
      r.fechaemision,r.creador,r.tiporeclamo,cr.descripcion as concepto,r.glosa,r.fecha_finalizacion,cr.tiempo,r.fin_reclamo,
      r.nroreclamo,r.correlativo
      FROM reclamos.reclamos as r 
      inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
      inner join reclamos.medioreclamo as m on(r.codmedio=m.codmedio)
      inner join public.tipoparentesco as t on(r.codtipoparentesco=t.codtipoparentesco)
      inner join reclamos.conceptosreclamos as cr on(r.codconcepto=cr.codconcepto)
      WHERE r.codsuc=" . $codsuc . " and r.nroinscripcion=" . $nroinscripcion . " ORDER BY fechaemision DESC";

    foreach ($conexion->query($Sql)->fetchAll() as $rowR) {
        $nroreclamo = $rowR["nroreclamo"];

        $sql = "SELECT c.propietario,r.nroinscripcion,m.descripcion as medio_reclamo,t.descripcion as tipo_parentesco,r.reclamante,
			r.fechaemision,r.creador,r.tiporeclamo,cr.descripcion as concepto,r.glosa,r.fecha_finalizacion,cr.tiempo,r.fin_reclamo
			FROM reclamos.reclamos as r 
			inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
			inner join reclamos.medioreclamo as m on(r.codmedio=m.codmedio)
			inner join public.tipoparentesco as t on(r.codtipoparentesco=t.codtipoparentesco)
			inner join reclamos.conceptosreclamos as cr on(r.codconcepto=cr.codconcepto)
			WHERE r.codsuc=? and r.nroreclamo=?";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $nroreclamo));
        $row = $consulta->fetch();

        $fecha_fin = $row["fecha_finalizacion"];
        $fecha_finalizacion = $objFunciones->DecFecha($fecha_fin);

        if ($row["fin_reclamo"] == 0) {
            $fecha_fin = date("Y-m-d");
            $fecha_finalizacion = "SIN FECHA DE FINALIZACION";
        }
        $dias = $objFunciones->calcular_diferencias_dias($row["fechaemision"], $fecha_fin);

        $color = "";
        if ($dias > $row["tiempo"]) {
            $color = "style='color:#F00; font-weight:bold'";
        }
        $Anio = substr($objFunciones->DecFecha($rowR['fechaemision']), 6, 4);
        ?>


        <h3>RECLAMO <?= $Anio ?>-<?= strtoupper($rowR["correlativo"]) ?></h3>
        <div style="height:700px; display: block;">
            <div style="width:900px; margin: 10px auto;">
                <div id="box-page">
                    <h2 style="text-align:center">DETALLE DEL RECLAMO</h2>
                    <div class="box-info">
                        <h3>PROPIETARIO: <?php echo strtoupper($row["propietario"]) ?></h3>
                        <div id="box-info-data">                
                            <table id="table-info" width="100%" border="0" >
                                <tr>
                                    <td width="20%">Nro. Reclamo</td>
                                    <td width="3%" align="center">:</td>
                                    <td width="35%"><?= $Anio ?>-<?= strtoupper($rowR["correlativo"]) ?></td>
                                    <td width="20%" align="right">Nro. Inscripcion</td>
                                    <td width="4%" align="center">:</td>
                                    <td><?php echo $objFunciones->CodUsuario($codsuc, $row["nroinscripcion"]) ?></td>
                                </tr>
                                <tr>
                                    <td>Propietario</td>
                                    <td align="center">:</td>
                                    <td colspan="4"><?php echo strtoupper($row["propietario"]) ?></td>
                                </tr>
                                <tr>
                                    <td>Medio de Reclamo</td>
                                    <td align="center">:</td>
                                    <td><?php echo strtoupper($row["medio_reclamo"]) ?></td>
                                    <td align="right">Parentesco</td>
                                    <td align="center">:</td>
                                    <td><?php echo strtoupper($row["tipo_parentesco"]) ?></td>
                                </tr>
                                <tr>
                                    <td>Reclamante</td>
                                    <td align="center">:</td>
                                    <td colspan="4"><?php echo strtoupper($row["reclamante"]) ?></td>
                                </tr>
                                <tr>
                                    <td>Fecha de Registro</td>
                                    <td align="center">:</td>
                                    <td><?php echo $objFunciones->DecFecha($row["fechaemision"]) ?></td>
                                    <td align="right">Registrado por</td>
                                    <td align="center">:</td>
                                    <td><?php echo $objFunciones->login($row["creador"]) ?></td>
                                </tr>
                                <tr>
                                    <td>Fecha de Finalizacion</td>
                                    <td align="center">:</td>
                                    <td><?php echo $fecha_finalizacion ?></td>
                                    <td align="right">Dias Procesados</td>
                                    <td align="center">:</td>
                                    <td>
                                        <label <?php echo $color ?> ><?php echo $dias ?></label></td>
                                </tr>
                                <tr>
                                    <td>Tipo de Reclamo</td>
                                    <td align="center">:</td>
                                    <td colspan="4"><?php echo $tiporeclamo[$row["tiporeclamo"]] ?></td>
                                </tr>
                                <tr>
                                    <td>Reclamo</td>
                                    <td align="center">:</td>
                                    <td colspan="4"><?php echo strtoupper($row["concepto"]) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td>Argumento</td>
                                    <td align="center">:</td>
                                    <td colspan="4"><?php echo strtoupper($row["glosa"]) ?></td>
                                </tr>
                            </table>
                        </div>
                        <div id="box-info-meses">
                            <h4>MESES RECLAMADOS</h4>
                            <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" rules="all" >
                                <thead class="ui-widget-header" >
                                    <tr class="myTableX" align="center">
                                        <td width="10%">A&ntilde;o </td>
                                        <td width="25%">Mes</td>
                                        <td width="15%">Importe Original</td>
                                        <td width="15%">Importe Rebajado</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlM = "SELECT anio,mes,sum(imporiginal) as importe_original,sum(imprebajado) as importe_rebajado
                                            FROM reclamos.mesesreclamados 
                                            WHERE codsuc=? and nroreclamo=? group by anio,mes order by anio,mes";

                                    $consultaM = $conexion->prepare($sqlM);
                                    $consultaM->execute(array($codsuc, $nroreclamo));
                                    $itemsM = $consultaM->fetchAll();

                                    foreach ($itemsM as $rowM) {
                                        ?>
                                        <tr>
                                            <td align="center"><?php echo $rowM["anio"] ?></td>
                                            <td align="center"><?php echo $meses[$rowM["mes"]] ?></td>
                                            <td align="right"><?php echo number_format($rowM["importe_original"], 2) ?></td>
                                            <td align="right"><?php echo number_format($rowM["importe_rebajado"], 2) ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div style="clear: both"></div>            
                    </div>
                    <div class="box-info" style="min-height: 200px">
                        <h4>DETALLE DE LOS MONTOS A REBAJAR</h4>
                        <div style="padding-bottom: 10px;">
                            <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" rules="all">
                                <thead class="ui-widget-header" >
                                    <tr class="myTableX" align="center" style="height: 22px">
                                        <td >Concepto</td>
                                        <td width="10%">Imp. Rebajado</td>
                                        <td width="10%">Imp. Original</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $cat = 100;
                                    $codt = 100;

                                    $sqlD = "SELECT c.descripcion as concepto,m.categoria,m.codtipodeuda,t.descripcion,SUM(m.imprebajado) AS imprebajado,SUM(m.imporiginal) AS imporiginal
                                        FROM reclamos.mesesreclamados as m
                                        inner join facturacion.conceptos as c on(m.codemp=c.codemp and m.codsuc=c.codsuc and m.codconcepto=c.codconcepto)
                                        inner join public.tipodeuda as t on(m.codtipodeuda=t.codtipodeuda)
                                        WHERE m.codsuc=? and m.nroreclamo=?
                                        GROUP BY c.descripcion,m.categoria,m.codtipodeuda,t.descripcion,c.ordenrecibo ORDER BY c.ordenrecibo";

                                    $consultaD = $conexion->prepare($sqlD);
                                    $consultaD->execute(array($codsuc, $nroreclamo));
                                    $itemsD = $consultaD->fetchAll();

                                    foreach ($itemsD as $rowD) {
                                        if ($cat != $rowD["categoria"]) {
                                            echo "<tr>";
                                            echo "<td colspan='3' class='estiloz' >CATEGORIA ==> " . $categoria[$rowD["categoria"]] . "</td>";
                                            echo "</tr>";
                                        }
                                        if ($codt != $rowD["codtipodeuda"]) {
                                            echo "<tr>";
                                            echo "<td colspan='3' class='estiloz' >&nbsp;&nbsp;&nbsp;TIPO DE DEUDA ==> " . strtoupper($rowD["descripcion"]) . "</td>";
                                            echo "</tr>";
                                        }
                                        ?>
                                        <tr>
                                            <td style="padding-left:10px"><?php echo strtoupper($rowD["concepto"]) ?></td>
                                            <td align="right"><?php echo number_format($rowD["imprebajado"], 2) ?></td>
                                            <td align="right"><?php echo number_format($rowD["imporiginal"], 2) ?></td>
                                        </tr>
                                        <?php
                                        $cat = $rowD["categoria"];
                                        $codt = $rowD["codtipodeuda"];
                                    }
                                    ?>
                                </tbody>
                            </table>                   
                        </div>
                    </div>
                    <div class="box-info" style="border-bottom: 0; ">
                        <h4>SEGUIMIENTO DEL RECLAMO</h4>
                        <table width="100%" border="0s" cellspacing="0" cellpadding="0">
                            <tr>
                                <?php
                                $count = 0;

                                $sqlDr = "SELECT d.nrodetalle,a.descripcion as area,d.codusu,d.fechareg
                                    FROM reclamos.detalle_reclamos as d
                                    inner join reglasnegocio.areas as a on(d.codemp=a.codemp and d.codsuc=a.codsuc and d.codarea=a.codarea)
                                    WHERE d.codsuc=? and d.nroreclamo=? order by d.nrodetalle asc ";

                                $consultaDr = $conexion->prepare($sqlDr);
                                $consultaDr->execute(array($codsuc, $nroreclamo));
                                $itemsDr = $consultaDr->fetchAll();

                                foreach ($itemsDr as $rowDr) {
                                    ?>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <img src="<?php echo $_SESSION['urldir']; ?>images/iconos/go.png" width="32" height="32" style="cursor:pointer" title="Ver Detalle del Reclamo en la Derivacion" onclick="cargar_observacion_reclamos(<?php echo $rowDr["nrodetalle"] ?>);" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr> 
                                                            <td width="22%">Area</td>
                                                            <td width="4%" align="center">:</td>
                                                            <td width="74%"><?php echo strtoupper($rowDr["area"]) ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Usuario </td>
                                                            <td align="center">:</td>
                                                            <td><?php echo $objFunciones->login($rowDr["codusu"]) ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fecha Reg.</td>
                                                            <td align="center">:</td>
                                                            <td><?php echo $objFunciones->DecFecha($rowDr["fechareg"]) ?></td>
                                                        </tr>
                                                    </table></td>
                                            </tr>
                                        </table></td>
                                    <?php $count++;
                                    if ($count == 3) {
                                        echo "</tr>";
                                    }
                                } ?>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

<?php
}
if ($rowR["nroreclamo"] == '') {
    ?>
        <script type="text/javascript">
            alert('Usuario no presenta Reclamos');
            close();
        </script>
    <?php
}
?>
</div>
<div id="DivDetRec" title="Detalle de Reclamo Derivado"><div id="div_detalle_reclamos"></div></div>