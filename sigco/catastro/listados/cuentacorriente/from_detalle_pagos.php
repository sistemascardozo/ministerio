<?php 
	include("../../../../../config.php");
	
	$codsuc			= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$nrofacturacion = $_POST["nrofacturacion"];
	$categoria		= $_POST["categoria"];
	$codtipodeuda	= $_POST["codtipodeuda"];
	
	$sql = "select d.codconcepto,c.descripcion,sum(d.importe) as imptotal, sum(d.importeacta) as importeacta,
					sum(d.importerebajado) as importerebajado,
					sum(d.importe) - sum(d.importeacta)-sum(d.importerebajado) as saldo
			from facturacion.detfacturacion as d
			inner join facturacion.conceptos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto)
			where d.codsuc=? and d.nroinscripcion=? and d.nrofacturacion=? and d.categoria=? and d.codtipodeuda=?
			group by d.codconcepto,c.descripcion,c.ordenrecibo order by c.ordenrecibo";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroinscripcion,$nrofacturacion,$categoria,$codtipodeuda));
	$items = $consulta->fetchAll();
	
	$timptotal        = 0;
	$timporteacta     = 0;
	$timporterebajado = 0;
	$tsaldo           = 0;
?>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="ui-widget-header" style="font-size:12px">
      <tr align="center">
        <th width="85%" >Concepto</th>
        <th width="15%" >Facturado</th>
        <th width="15%" >Amortizado</th>
        <th width="15%" >Rebajado</th>
        <th width="15%" >Saldo</th>
     </tr>
 </thead>
 <tbody>
     <?php
	 	foreach($items as $row)
		{
			$timptotal += $row["imptotal"];
			$timporteacta += $row["importeacta"];
			$timporterebajado += $row["importerebajado"];
			$tsaldo += $row["saldo"];
			if($timptotal>0)
			{
	 ?>
     <tr>
        <td align="left" ><?=strtoupper($row["descripcion"])?></td>
        <td align="right" ><?=number_format($row["imptotal"],2)?></td>
        <td align="right" ><?=number_format($row["importeacta"],2)?></td>
        <td align="right" ><?=number_format($row["importerebajado"],2)?></td>
        <td align="right" ><?=number_format($row["saldo"],2)?></td>

     </tr>
     <?php } } ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td align="right" >Totales ==></td>
        <td align="right" ><?=number_format($timptotal,2)?></td>
        <td align="right" ><?=number_format($timporteacta,2)?></td>
        <td align="right" ><?=number_format($timporterebajado,2)?></td>
        <td align="right" ><?=number_format($tsaldo,2)?></td>
     </tr>
 </tfoot>
 
  
</table>