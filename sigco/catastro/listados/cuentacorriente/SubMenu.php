<style type="text/css">
.minimenu2
{
    z-index: 1000000;
    position: absolute;
    cursor: pointer;

}
</style>
<script type="text/javascript">
$(function(){

        $(".ui-menu-item").mouseover(function(){$(this).siblings().removeClass('ui-state-active');$(this).addClass('ui-state-active');})
        $(".ui-menu-item").mouseout(function(){$(this).removeClass('ui-state-active'); })
        $( "#DivTipos" ).buttonset();
        $( "#rerun" )
      .buttonset()
      .next()
        .button({
          text: false,
          icons: {
            primary: "ui-icon-triangle-1-s"
          }
        })
        .click(function() {
          var menu = $( this ).parent().next().show().position({
            my: "left top",
            at: "left bottom",
            of: this
          });
          $( document ).one( "click", function() {
            menu.hide();
          });
          return false;
        })
        .parent()
          .buttonset()
          .next()
            .hide()
            .menu();

    });
function Validar()
{


    var NroInscripcion = $("#nroinscripcion").val()
    var Data=''
    if(NroInscripcion!="")
        Data +='NroInscripcion='+NroInscripcion
    else
    {
        Msj('#codantiguo','Seleccione Usuario',1000,'above','',false)
        return false;
    }
    return Data
}

  function imprimirrecordfacturacion()
  {
    var Data=Validar()
    if(Data!=false)
    {
      nroinscripcion=$("#nroinscripcion").val()
      AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/consulta/general/impresiones/recordfacturacion.php?nroinscripcion="+nroinscripcion,800,600)
  }
  }
 function tarjetalectura()
  {
    var Data=Validar()
    if(Data!=false)
    {
      nroinscripcion=$("#nroinscripcion").val()

     AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/medicion/listado/historico/Pdf.php?NroInscripcion="+nroinscripcion,800,600)
    }
  }
   function VerOcurrencias()
  {
    //alert();
     var Data=Validar()
    if(Data!=false)
    {
        nroinscripcion=$("#nroinscripcion").val()
        AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/facturacion/listado/ocurrencias/Pdf.php?NroInscripcion="+nroinscripcion,800,600)
    }
  }
  function CorAper()
  {
    //alert();
     var Data=Validar()
    if(Data!=false)
    {
        nroinscripcion=$("#nroinscripcion").val()
        AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/CortesyReaperturas.php?NroInscripcion="+nroinscripcion,800,600)
    }
  }
  function CuentaCorrientePdf()
    {
        var Data=Validar()
        if(Data!=false)
        {
            var ventana=window.open('<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/Pdf.php?'+Data, 'CuentaCorriente', 'resizable=yes, scrollbars=yes');
            ventana.focus();
        }
    }
    function Reclamos()
    {
       var Data=Validar()
        if(Data!=false)
        {
        var ventana=window.open('<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/Reclamos.php?'+Data, 'Reclamos', 'resizable=yes, scrollbars=yes');
        ventana.focus();
      }
    }
    function Modificaciones()
    {
       var Data=Validar()
        if(Data!=false)
        {
        var ventana=window.open('<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/modificaciones/imprimir.php?'+Data, 'Modificaciones', 'resizable=yes, scrollbars=yes');
        ventana.focus();
      }
    }

	function DatosCatastral()
    {
       	var Data=Validar()
        if(Data!=false)
        {
			AbrirPopupImpresion('<?php echo $_SESSION['urldir'];?>sigco/catastro/operaciones/actualizacion/mantenimiento.php?Op=100&'+Data, 950,500)
        //var ventana=window.open('<?php echo $_SESSION['urldir'];?>sigco/catastro/operaciones/actualizacion/mantenimiento.php?Op=100&'+Data, 'Modificaciones', 'resizable=yes, scrollbars=yes');
        //ventana.focus();
      }
    }
    function FichaCatastral()
      {
         	var nroinscripcion = $("#nroinscripcion").val();
         	var codsuc         = '<?php echo $_SESSION['IdSucursal']; ?>';

		      AbrirPopupImpresion('<?php echo $_SESSION['urldir'];?>sigco/catastro/operaciones/actualizacion/ficha_catastral.php?nroinscripcion='+nroinscripcion+"&codsuc="+codsuc, 950,500)

      }
</script>
<div id="DivSeguOp" style="float:right;display:table-cell;vertical-align:middle; padding-right:100px">
  <div>
    <input id="rerun" type="button" value="+Opciones"/>
    <button id="select">Seleccione Accion</button>
  </div>
  <ul class="minimenu2">
    <li class="ui-menu-item" role="menuitem" onclick="CuentaCorrientePdf();">Cuenta Corriente</li>
    <li class="ui-menu-item" role="menuitem" onclick="tarjetalectura();">Tarjeta de Lecturas</li>
    <li class="ui-menu-item" role="menuitem" onclick="imprimirrecordfacturacion();">Record de Facturacion</li>
    <li class="ui-menu-item" role="menuitem" onclick="VerOcurrencias();">Ocurrencias</li>
    <li class="ui-menu-item" role="menuitem" onclick="CorAper();">AccCobra</li>
    <li class="ui-menu-item" role="menuitem" onclick="Reclamos();">Reclamos</li>
    <li class="ui-menu-item" role="menuitem" onclick="Modificaciones();">Modificaciones</li>
    <li class="ui-menu-item" role="menuitem" onclick="FichaCatastral();">Ficha Catastral</li>
    <li class="ui-menu-item" role="menuitem" onclick="DatosCatastral();">Datos Catastro</li>

  </ul>
</div>
