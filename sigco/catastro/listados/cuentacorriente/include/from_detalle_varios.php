<?php 
	include("../../../../../config.php");

	$codsuc			= $_POST["codsuc"];
	$nrocredito		= $_POST["nrocredito"];

	//Colaterales 
	$sql1 = "SELECT DISTINCT
			det.nrocuota as item,
			c.descripcion as colateral,
			det.imptotal as imptotal
			FROM facturacion.detvarios as det
			INNER JOIN facturacion.cabvarios as cab ON ( cab.codemp = det.codemp AND cab.codsuc = det.codsuc AND cab.nrocredito = det.nrocredito)
			INNER JOIN facturacion.conceptos as c ON ( cab.codconcepto = c.codconcepto AND c.codemp = cab.codemp AND c.codsuc = cab.codsuc)
			WHERE det.codsuc=? AND det.nrocredito=?";
	
	$consulta1 = $conexion->prepare($sql1);
	$consulta1->execute(array($codsuc,$nrocredito));
	$items1 = $consulta1->fetchAll();

	$timptotal = 0;
	$timptotal1 = 0;
?>

<div style="text-align: center;">
	<h2 style="color:#000">N° CREDITO</h2>
	<h2 style="color:#00008B"><?=$nrocredito?></h2>
</div>

<h3>Colaterales</h3>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="ui-widget-header" style="font-size:12px">
      <tr align="center">
        <th width="50" >Item</th>
        <th>Colateral</th>
        <th width="100" >Importe</th>
     </tr>
 </thead>
 <tbody>
     <?php foreach($items1 as $row1): ?>

		<?php $timptotal1 += $row1["imptotal"];?>
		<?php if($timptotal1>0): ?>
		<tr>
        	<td align="center" ><?=strtoupper($row1["item"]) ?></td>
    		<td align="left" style="padding-left:5px;"><?=utf8_decode($row1["colateral"]) ?></td>
    		<td align="right" style="padding-right:5px;"><?=number_format($row1["imptotal"],2) ?></td>
 		</tr>
		<?php endif; ?>
	<?php endforeach; ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td colspan="2" align="right" style="padding-right:5px;">Totales ==></td>
        <td align="right" style="padding-right:5px;"><?=number_format($timptotal1,2)?></td>
     </tr>
 	</tfoot>  
</table>