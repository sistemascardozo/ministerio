<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	
	$codsuc			= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$TasaInteres = $_POST["TasaInteres"];
	$objFunciones = new clsFunciones();

	
	$fdescarga = date('Y-m-d');
	
	$sql = "select * from cobranza.view_cobranza 
    where codemp=1 and codsuc=? and nroinscripcion=? 
    AND codtipodeuda NOT IN (11) 
    order by nrofacturacion";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroinscripcion));
	$items = $consulta->fetchAll();
?>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="ui-widget-header" style="font-size:12px">
      <tr align="center">
        <th width="80" >Facturaci&oacute;n</th>
        <th width="100" >Documento</th>
        <th >Periodo</th>
        <th width="90" >Tipo Deuda</th>
        <th width="70" >Importe</th>
        <th width="80" >Vencimiento</th>
        <th width="50" >Dias</th>
        <th width="70" >Interes</th>
     </tr>
 </thead>
 <tbody>
     <?php
     $totalf=0;
     $totali=0;
	 	foreach($items as $row)
		{
			
			$fechavencimiento = $row["fechavencimiento"];
			if($fdescarga>$fechavencimiento)
			{
				$dias = $objFunciones->dias_transcurridos($fechavencimiento,$fdescarga);
				$importe=round($row["imptotal"]*$dias*($TasaInteres/30),1);
			}
			else
			{
				$dias=0;
				$importe=0;
			}
			$totalf += $row["imptotal"];	
			$totali += $importe;
	 ?>
     <tr>
        <td align="center" ><?=$row["nrofacturacion"]?></td>
        <td align="center" ><?=$row["abreviado"]?>:<?=$row["serie"]?>-<?=$row['nrodocumento']?></td>
        <td align="left" style="padding-left:5px;"><?=$row["anio"]." - ".$meses[$row["mes"]]?></td>
        <td align="center" ><?=$row["deuda"]?></td>
        <td align="right" style="padding-right:5px;"><?=number_format($row["imptotal"],2)?></td>
        <td align="center" ><?=$objFunciones->DecFecha($fechavencimiento)?></td>
        <td align="center" ><?=$dias?></td>
        <td align="right" style="padding-right:5px;" ><?=number_format($importe,2)?></td>

     </tr>
     <?php } //} ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td align="right" colspan=4>Totales ==></td>
        <td align="right" style="padding-right:5px;"><?=number_format($totalf,2)?></td>
        <td colspan=2>&nbsp;</td>
        <td align="right" style="padding-right:5px;"><?=number_format($totali,2)?></td>
     </tr>
 </tfoot>
 
  
</table>