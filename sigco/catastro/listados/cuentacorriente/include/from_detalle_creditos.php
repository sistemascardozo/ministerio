<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsDrop.php");
	
    $objMantenimiento = new clsDrop();
	$codsuc			= $_POST["codsuc"];
	$nrocredito		= $_POST["nrocredito"];

	//Cuotas 
	$sql = "SELECT DISTINCT
			d.nrocuota as nrocuota,
			d.fechavencimiento as vencimiento,
			ec.descripcion as estado,
			d.imptotal +d.redondeo as imptotal,
			d.fechapago
			FROM facturacion.detcreditos as d
			INNER JOIN  public.estadocuota as ec ON (d.estadocuota = ec.estadocuota)
			where d.codsuc=? AND d.nrocredito=? ORDER BY d.nrocuota";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nrocredito));
	$items = $consulta->fetchAll();

	//Colaterales 
	$sql1 = "SELECT DISTINCT
			det.item as item,
			c.descripcion as colateral,
			det.imptotal as imptotal
			FROM facturacion.detcabcreditos as det
			INNER JOIN  facturacion.conceptos as c ON ( det.codconcepto = c.codconcepto AND c.codemp = det.codemp AND c.codsuc = det.codsuc)
			where det.codsuc=? AND det.nrocredito=?";
	
	$consulta1 = $conexion->prepare($sql1);
	$consulta1->execute(array($codsuc,$nrocredito));
	$items1 = $consulta1->fetchAll();

	$timptotal = 0;
	$timptotal1 = 0;
?>

<div style="text-align: center;">
	<h2 style="color:#000">N° CREDITO</h2>
	<h2 style="color:#00008B"><?=$nrocredito?></h2>
</div>

<h3>Cronograma de Creditos</h3>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="ui-widget-header" style="font-size:12px">
      <tr align="center">
        <th width="50" >N° Cuota</th>
        <th width="100" >Fecha de Vencimiento</th>
        <th width="100" >Fecha de Pago</th>
        <th width="100" >Estado</th>
        <th>Importe</th>
     </tr>
 </thead>
 <tbody>
     <?php foreach($items as $row): ?>
		<?php $timptotal += $row["imptotal"];?>
		<?php if ($timptotal>0): ?>
		<tr>
        	<td align="center" ><?=$row["nrocuota"]?></td>
        	<td align="center" ><?=$objMantenimiento->DecFecha($row["vencimiento"])?></td>
        	<td align="center" ><?=$objMantenimiento->DecFecha($row["fechapago"])?></td>
    		<td align="center" ><?=$row["estado"]?></td>
    		<td align="right" style="padding-right:5px;"><?=number_format($row["imptotal"],2)?></td>
 		</tr>
		<?php endif; ?>
		<?php endforeach; ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td colspan="4" align="right" >Totales ==></td>
        <td align="right" style="padding-right:5px;"><?=number_format($timptotal,2)?></td>
     </tr>
 	</tfoot>  
</table>
<br>
<br>
<h3>Colaterales</h3>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="ui-widget-header" style="font-size:12px">
      <tr align="center">
        <th width="15%" >Item</th>
        <th width="65%" >Colateral</th>
        <th width="15%" >Importe</th>
     </tr>
 </thead>
 <tbody>
     <?php foreach($items1 as $row1): ?>

		<?php $timptotal1 += $row1["imptotal"];?>
		<?php if($timptotal1>0): ?>
		<tr>
        	<td align="center" ><?=strtoupper($row1["item"]) ?></td>
    		<td align="center" ><?=utf8_decode($row1["colateral"]) ?></td>
    		<td align="right" style="padding-right:5px;"><?=number_format($row1["imptotal"],2) ?></td>
 		</tr>
		<?php endif; ?>
	<?php endforeach; ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td colspan="2" align="right" >Totales ==></td>
        <td align="right" style="padding-right:5px;"><?=number_format($timptotal1,2)?></td>
     </tr>
 	</tfoot>  
</table>