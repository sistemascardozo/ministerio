<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
//	include("../../../../../config.php");
	include("../../../../../objetos/clsDrop.php");
    $objMantenimiento = new clsDrop();

	$codsuc			= $_POST["codsuc"];
	$nrocredito		= $_POST["nrocredito"];
	$nroinscripcion		= $_POST["nroinscripcion"];
	//Cuotas 
	$sql = "SELECT DISTINCT
			d.nrocuota as nrocuota,
			d.fechavencimiento as vencimiento,
			ec.descripcion as estado,
			SUM(d.importe) as imptotal,
			d.fechapago
			FROM facturacion.detrefinanciamiento as d
			INNER JOIN  public.estadocuota as ec ON (d.estadocuota = ec.estadocuota)
			where d.codsuc=? AND d.nrorefinanciamiento=? 
			group by d.nrocuota,d.totalcuotas,d.anio,d.mes,d.estadocuota,
			d.fechapago,d.fechavencimiento,ec.descripcion,d.fechapago
			ORDER BY d.nrocuota";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nrocredito));
	$items = $consulta->fetchAll();

	//Colaterales 
	$sql1 = "SELECT DISTINCT
			det.item as item,
			c.descripcion as colateral,
			det.imptotal as imptotal
			FROM facturacion.detcabrefinanciamiento as det
			INNER JOIN  facturacion.conceptos as c ON ( det.codconcepto = c.codconcepto AND c.codemp = det.codemp AND c.codsuc = det.codsuc)
			where det.codsuc=? AND det.nrorefinanciamiento=?";
	
	$consulta1 = $conexion->prepare($sql1);
	$consulta1->execute(array($codsuc,$nrocredito));
	$items1 = $consulta1->fetchAll();

	$timptotal = 0;
	$timptotal1 = 0;
?>

<div style="text-align: center;">
	<h2 style="color:#000">N° FINANCIAMIENTO</h2>
	<h2 style="color:#00008B"><?=$nrocredito?></h2>
</div>


<h3>Recibos Financiados</h3>
<table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbfacturacion" rules="all">
	<thead class="ui-widget-header" >
	      <tr align="center">
	        <th width="50" >Item</th>
	        <th>Facturacion</th>
	        <th width="100" >Tipo Recibo</th>
	        <th width="100" >Nro. Doc.</th>
	        <th width="80" >Monto</th>
	        <th width="100" >Emisión</th>
	        <th width="100" >Vencimiento</th>
	        
	        </tr>
	      </thead>
	      <tbody>
	      	<?php
	      	$Sql="SELECT f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg 
			FROM facturacion.cabfacturacion f
			  INNER JOIN facturacion.origen_refinanciamiento o ON (f.codemp = o.codemp)
			  AND (f.codsuc = o.codsuc)
			  AND (f.codciclo = o.codciclo)
			  AND (f.nrofacturacion = o.nrofacturacion)
			  AND (f.nroinscripcion = o.nroinscripcion)
			  where o.codemp=1 and o.codsuc=? and o.nrorefinanciamiento=?
			  GROUP BY f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg
			  ORDER BY  f.nrofacturacion";
		
		$consulta2 = $conexion->prepare($Sql);
		$consulta2->execute(array($codsuc,$nrocredito));
		$itemD = $consulta2->fetchAll();
		
		foreach($itemD as $rowD)
		{
			$i++;
			 $Sql="select sum(importe - (importerebajado))
					 from facturacion.detfacturacion
				where codemp=1 and codsuc=? and nroinscripcion=? AND nrofacturacion=?";
				
			$result = $conexion->prepare($Sql);
			$result->execute(array($codsuc,$nroinscripcion,$rowD['nrofacturacion']));
			$row2 = $result->fetch();
			$TotalRef+= $row2[0];
			$recibon = str_pad($rowD['serie'],3,"0",STR_PAD_LEFT)." - ".str_pad($rowD['nrodocumento'],7,"0",STR_PAD_LEFT);
			?>
			<tr>
				<td align="center"><?=$i?></td>
				<td style="padding-left:5px;"><?=$rowD['nrofacturacion']?>-PENSIONES</td>
				<td style="padding-left:5px;">RECIBO</td>
				<td align="center"><?=$recibon?></td>
				<td align="right" style="padding-right:5px;"><?=number_format($row2[0],2)?></td>
				<td align="center"><?=$objMantenimiento->DecFecha($rowD['fechareg'])?></td>
				<td align="center"><?=$objMantenimiento->DecFecha($rowD['fechareg'])?></td>
			</tr>
	<?php
		}
	      	?>
	      </tbody>
	    </table>
<h3>Colaterales</h3>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="ui-widget-header" style="font-size:12px">
      <tr align="center">
        <th width="50" >Item</th>
        <th>Colateral</th>
        <th width="100" >Importe</th>
     </tr>
 </thead>
 <tbody>
     <?php foreach($items1 as $row1): ?>

		<?php $timptotal1 += $row1["imptotal"];?>
		<?php if($timptotal1>0): ?>
		<tr>
        	<td align="center" ><?=strtoupper($row1["item"]) ?></td>
    		<td align="left" style="padding-left:5px;"><?=utf8_decode($row1["colateral"]) ?></td>
    		<td align="right" style="padding-right:5px;"><?=number_format($row1["imptotal"],2) ?></td>
 		</tr>
		<?php endif; ?>
	<?php endforeach; ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td colspan="2" align="right" style="padding-right:5px;">Totales ==></td>
        <td align="right" style="padding-right:5px;"><?=number_format($timptotal1,2)?></td>
     </tr>
 	</tfoot>  
</table>
<h3>Cronograma de Creditos</h3>
<table border="1"  class="ui-widget" width="100%" id="tbdetallefacturacion" cellspacing="0"  rules="all" >
 <thead class="ui-widget-header" style="font-size:12px">
      <tr align="center">
        <th width="50" >N° Cuota</th>
        <th width="100" >Fecha de Vencimiento</th>
        <th width="100" >Fecha de Pago</th>
        <th width="100" >Estado</th>
        <th>Importe</th>
     </tr>
 </thead>
 <tbody>
     <?php foreach($items as $row): ?>
		<?php $timptotal += $row["imptotal"];?>
		<?php if ($timptotal>0): ?>
		<tr>
        	<td align="center" ><?=$row["nrocuota"]?></td>
        	<td align="center" ><?=$objMantenimiento->DecFecha($row["vencimiento"])?></td>
        	<td align="center" ><?=$objMantenimiento->DecFecha($row["fechapago"])?></td>
    		<td align="center" ><?=$row["estado"]?></td>
    		<td align="right" style="padding-right:5px;"><?=number_format($row["imptotal"],2)?></td>
 		</tr>
		<?php endif; ?>
		<?php endforeach; ?>
     </tbody>
     <tfoot class="ui-widget-header" style="font-size:12px">
     <tr>
        <td colspan="4" align="right" style="padding-right:5px;">Totales ==></td>
        <td align="right" style="padding-right:5px;"><?=number_format($timptotal,2)?></td>
     </tr>
 	</tfoot>  
</table>