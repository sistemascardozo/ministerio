<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "CUENTA CORRIENTE";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);

	$Fecha = date('d/m/Y');
?>


<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>

<script>
$(function(){
		
		$(".ui-menu-item").mouseover(function(){$(this).siblings().removeClass('ui-state-active');$(this).addClass('ui-state-active');})
		$(".ui-menu-item").mouseout(function(){$(this).removeClass('ui-state-active'); })
		$( "#DivTipos" ).buttonset();
		$( "#rerun" )
      .buttonset()
      .next()
        .button({
          text: false,
          icons: {
            primary: "ui-icon-triangle-1-s"
          }
        })
        .click(function() {
          var menu = $( this ).parent().next().show().position({
            my: "left top",
            at: "left bottom",
            of: this
          });
          $( document ).one( "click", function() {
            menu.hide();
          });
          return false;
        })
        .parent()
          .buttonset()
          .next()
            .hide()
            .menu();

	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc 	= <?=$codsuc?>

	function buscar_usuarios()
	{
		object = "usuario";
		
		AbrirPopupBusqueda('../../../catastro/operaciones/actualizacion/?Op=5', 1150, 450);
	}
	
function Recibir(id)
{
	if(object=="usuario")
	{
		$("#nroinscripcion").val(id)
		cargar_datos_usuario(id)
	}
	
}
function cargar_datos_usuario(nroinscripcion)
{

	$.ajax({
		 url:'../../../../ajax/clientes.php',
		 type:'POST',
		 async:true,
		 data:'codsuc=<?=$codsuc?>&nroinscripcion='+nroinscripcion,
		  dataType: "json",
		 success:function(datos){
			
			$("#codantiguo").val(datos.codantiguo)
			$("#cliente").val(datos.propietario)
			Consultar()
		 }
    }) 
}
function MostrarDatosUsuario(datos)
{
	$(".minimenu2").hide();
	$("#nroinscripcion").val(datos.nroinscripcion)
	$("#cliente").val(datos.propietario)
	Consultar()
	
}
function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>admin/index.php'
	}
function Validar()
{
        
    
    var NroInscripcion = $("#nroinscripcion").val()
    var Data=''
    if(NroInscripcion!="")
        Data +='NroInscripcion='+NroInscripcion
    else
    {
        Msj('#codantiguo','Seleccione Usuario',1000,'above','',false)
        return false;
    }
    return Data 
}
function Consultar()
	{
		var Data=Validar()
		if(Data!=false)
		{
			$('#DivConsulta').fadeOut(500) 
			$('#ImgLoad').fadeIn();
			
			$("#DivResultado").empty();
			$.ajax({
			 url:'Consulta.php',
			 type:'POST',
			 async:true,
			 data:Data,
			 success:function(datos)
			 {
			 	$('#ImgLoad').fadeOut(500,function(){
			       $("#DivResultado").empty().append(datos);             
					$('#DivConsulta').fadeIn(500,function(){var theTable = $('#TbIndex')
			  		$("#Filtro").keyup(function() {$.uiTableFilter( theTable, this.value)})}) 
				})
			 }
			})
		}
	}


</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
	<table width="1000" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
				<td colspan="2">
					<fieldset style="padding:4px">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
                                <td width="70">Sucursal</td>
                                <td width="30" align="center">:</td>
                                <td>
									<input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
								</td>
                                <td width="48%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Usuario</td>
                                <td align="center">:</td>
                                <td>
									<input type="hidden" name="nroinscripcion" id="nroinscripcion" size="10" class="inputtext entero"  value="<?php echo $row["nroinscripcion"]?>" />
                                    <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="8" value="<?php echo $row["codantiguo"]?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:80px;"/>
                                    <span class="MljSoft-icon-buscar" title="Buscar Usuario" onclick="buscar_usuarios();"></span>
									<input type="text" name="cliente" id="cliente" class="inputtext" size="45" maxlength="45" value="" readonly="readonly" />
								</td>
							</tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
							<tr>
								<td colspan="4" height="40" align="center">
									<table width="500" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center">
												<input type="button" value="Guardar" id="BtnAceptar" onclick="Consultar();" />
                                            </td>
                                            <td align="center" style="display:none">
												<input id="BtnExcel" type="button" value="Excel" onclick="Excel();" class="BtnIndex"/>
											</td>
                                            <td align="center">
												<input id="BtnPdf" type="button" value="Pdf" onclick="CuentaCorrientePdf();" class="BtnIndex"/>
                                            </td>
                                            <td align="center">
												<input id="BtnCancelar" type="button" value="Cancelar" onclick="Cancelar();" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
            </tr>
            <tr>
				<td colspan="6" >
					<div id="ImgLoad" style="text-align:center;display:none">
						<span class="icono-icon-loading"></span>
						Cargando ...
					</div>
					<div id="DivConsulta" style="height:auto; overflow:auto;display:none ">
						<fieldset>
							<legend class="ui-state-default ui-corner-all" >Resultados de la Consulta</legend>
							<div style="height:auto; overflow:auto;" align="center" id="DivResultado">
								<table class="ui-widget" border="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
									<thead class="ui-widget-header" style="font-size:10px">
										<tr>
											<th colspan="8">Desde el <?=($Fecha)?> Hasta el <?=($Fecha)?>  </th>
                                        </tr>
                                        <tr>
                                            <th width="50" scope="col">N&deg;</th>
                                            <th width="50" scope="col">N&deg; Prestamo</th>
                                            <th width="100" scope="col">Fecha</th>
                                            <th width="500" scope="col">Beneficiario</th>
                                            <th width="100" scope="col">Fecha Cosecha</th>
                                            <th width="100" scope="col">Monto</th>
                                            <th width="100" scope="col">Estado</th>
                                            <th width="100" scope="col">&nbsp;</th>
										</tr>
									</thead>
									<tbody style="font-size:12px"></tbody>
									<tfoot class="ui-widget-header" style="font-size:10px">
                                        <tr>
                                            <td colspan="8" align="right" >&nbsp;</td>
                                        </tr>
									</tfoot>
								</table>
							</div>
						</fieldset>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
 </form>
</div>
<script>
	//$('#ImgLoad').fadeOut(500,function(){$('#DivConsulta').fadeIn(500)});
	$("#BtnAceptar").attr('value','Consultar')
	$("#BtnAceptar").css('background-image','url(<?php echo $_SESSION['urldir'];?>css/images/ver.png)')
	$("#codantiguo").focus()
</script>
<?php 
	CuerpoInferior();
?>