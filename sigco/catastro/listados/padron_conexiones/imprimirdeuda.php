<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses,$Dim;
			
			$periodo = $this->DecFechaLiteral();
			$this->BorrarCabecera();

			$this->SetXY(220,5);
			$this->SetFont('Arial','',6);
			$FechaActual = date('d/m/Y');
			$Hora = date('h:i:s a'); 
			$this->Cell(20, 3,utf8_decode("Página : ".$this->PageNo().' de {nb}     '), 0, 0, 'R');
	 		$this->Cell(25, 3,"Fecha : ".$FechaActual."   ", 0, 0, 'R');
			$this->Cell(20, 3,"Hora : ".$Hora." ", 0, 1, 'R');



			$this->SetFont('Arial','B',10);
			$this->SetY(6);
			$h = 4;
			$this->Cell(278, $h+2,"LISTADO SELECTIVO PARA VERIFICACION",0,1,'C');
			$this->SetFont('Arial','B',8);
			
			$this->Ln(1);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(20,18,63,45,10,20,10,15,15,8,8,8,8,8,8,8,8));
			$this->Cell($Dim[1],$h,utf8_decode('Codigo'),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode('Inscripcion'),1,0,'C',false);
			$this->Cell($Dim[3],$h,utf8_decode('Nombre del Usuario'),1,0,'C',false);
			$this->Cell($Dim[4],$h,utf8_decode('Direccion'),1,0,'C',false);
			$this->Cell($Dim[5],$h,utf8_decode('Medidor'),1,0,'C',false);
			$this->Cell($Dim[6],$h,utf8_decode('Se'),1,0,'C',false);
			$this->Cell($Dim[7],$h,utf8_decode('Sit'),1,0,'C',false);
			$this->Cell($Dim[8],$h,utf8_decode('Categ'),1,0,'C',false);
			$this->Cell($Dim[9],$h,utf8_decode('Volum'),1,0,'C',false);
			$this->Cell($Dim[10],$h,utf8_decode('Fact.S/.'),1,0,'C',false);
			$this->Cell($Dim[11],$h,utf8_decode('Meses'),1,0,'C',false);
			$this->Cell($Dim[12],$h,utf8_decode('Deuda'),1,0,'C',false);
			$this->Cell($Dim[13],$h,utf8_decode('Obs.'),1,1,'C',false);

			
		}
		function contenido($codsector,$tiposervicio,$estadoservicio,$codsuc,$ciclo,$altocon,$Desde,$Hasta,$conpozo,$rutai,$rutaf,$estadomedidor)
		{
			global $conexion,$Dim;
			
			$sql = "select nrofacturacion,anio,mes,saldo,lecturas,tasainteres
			from facturacion.periodofacturacion
			where codemp=1 and codsuc=? and codciclo=? and facturacion=0";
	
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,1));
			$items = $consulta->fetch();
			$nrofacturacion = $items['nrofacturacion'] -1;

			$h = 4;
			$s = 2;
			
			if($altocon!=0)
			{
				$text = " and altocon=".$altocon;
			}
			if($Desde!="")
			{
				$Condicion=" AND c.fechareg BETWEEN CAST ('".$Desde."' AS DATE) AND CAST ( '".$Hasta."' AS DATE ) ";
			}
			$order=" ORDER BY  c.codsuc,c.codsector,CAST(c.codrutlecturas AS INTEGER ),CAST(c.codmanzanas AS INTEGER) ,CAST(c.lote AS INTEGER) ";
			if($codsector!="%"){$CSector=" AND c.codsector=".$codsector;}else{$CSector="";}
			if($tiposervicio!="%"){$Ctiposervicio=" AND c.codtiposervicio=".$tiposervicio;}else{$Ctiposervicio="";}
			if($estadoservicio!="%"){$Cestadoservicio=" AND c.codestadoservicio=".$estadoservicio;}else{$Cestadoservicio="";}
			if($estadomedidor!="%"){$Cestadomedidor=" AND c.codestadomedidor=".$estadomedidor;}else{$Cestadomedidor="";}
			
			if($ciclo!="%"){$Cciclo=" AND c.codciclo=".$ciclo;}else{$Cciclo="";}
			if($conpozo==0) $conpozo=" AND cx.conpozo =0 "; else $conpozo='';
			if($rutai!='')
				$rutas = " AND c.codrutlecturas>=".$rutai." AND c.codrutlecturas<=".$rutaf." ";
			 $sql = "select c.nroinscripcion,c.propietario,tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,t.nomtar,
					e.descripcion as estadoservicio,c.codtiposervicio,c.nromed,s.descripcion as sector,c.codmanzanas,c.lote,
					c.sublote,c.domestico,c.comercial,c.industrial,c.social,c.estatal,".$this->getCodCatastral("c.").",
					cx.altocon,c.codantiguo
					from catastro.clientes as c 
					INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
					inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
					inner join facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
					inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
					inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
					inner join catastro.conexiones as cx on(c.codemp=cx.codemp and c.codsuc=cx.codsuc and c.nroinscripcion=cx.nroinscripcion)
					where c.codemp=1 and c.codsuc=? ".$conpozo.$CSector.$Ctiposervicio. $Cestadoservicio.$Cestadomedidor.$Cciclo. $text.$Condicion." ".$rutas.$order;
			
			/*$codsector 		= "%".$codsector."%";
			$tiposervicio	= "%".$tiposervicio."%";
			$estadoservicio	= "%".$estadoservicio."%"; 3765
			$ciclo			= "%".$ciclo."%";*/
			$count			= 0;
			
			$consulta = $conexion->prepare($sql);
			//$consulta->execute(array($codsuc,$codsector,$tiposervicio,$estadoservicio,$ciclo));
			$consulta->execute(array($codsuc));
			$items = $consulta->fetchAll();
			$this->SetFont('Arial','',8);
			foreach($items as $row)
			{
				$count++;
				$codcatastro = $row["codcatastro"];
				switch ($row["codtiposervicio"]) 
				{
					case 1:$ts="AD";break;
					case 2:$ts="A";break;
					case 3:$ts="D";break;
				}
				$Tar='';
				if(intval($row["domestico"])>0) $Tar .='DOM';
				if(intval($row["comercial"])>0) $Tar .=' COM';
				if(intval($row["industrial"])>0) $Tar .=' IND';
				if(intval($row["social"])>0) $Tar .=' SOC';
				if(intval($row["estatal"])>0) $Tar .=' EST';
				//echo  $Tar;
				$this->SetFillColor(255,255,255); //Color de Fondo
				$this->SetTextColor(0);
				$h=4;
				///
				$Sql="SELECT consumo, sum(d.importe) AS importe 
					FROM facturacion.detfacturacion d
					INNER JOIN facturacion.cabfacturacion c ON (c.codemp=d.codemp AND c.codsuc=d.codsuc 
						AND c.nrofacturacion=d.nrofacturacion AND c.nroinscripcion=d.nroinscripcion)
					WHERE  c.codemp=1 and c.codsuc=".$codsuc." AND c.nroinscripcion=".$row["nroinscripcion"]." 
					AND c.nrofacturacion=".$nrofacturacion."
					GROUP BY c.consumo";
				$consulta2 = $conexion->prepare($Sql);
				$consulta2->execute();
				$row3 = $consulta2->fetch();
				$consumo = $row3[0];
				$importe = $row3[1];
				$Sql="SELECT sum(importe-(importerebajado+imppagado)) as importe,
				( SELECT count(distinct(f.nrofacturacion))
					FROM facturacion.cabfacturacion f 
					INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) 
					AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo) 
					WHERE f.codemp=facturacion.detfacturacion.codemp and f.codsuc=facturacion.detfacturacion.codsuc 
					and f.nroinscripcion=facturacion.detfacturacion.nroinscripcion	AND df.categoria <> 2 AND df.estadofacturacion <> 2
					AND f.enreclamo=0
					having sum(df.importe - (df.imppagado + df.importerebajado)) >0
			 			) as meses

				FROM facturacion.detfacturacion 
				WHERE codemp=1 and codsuc=".$codsuc." AND (estadofacturacion=1 OR estadofacturacion=3) 
				AND nroinscripcion=".$row["nroinscripcion"]."
				group by facturacion.detfacturacion.codemp,facturacion.detfacturacion.codsuc,facturacion.detfacturacion.nroinscripcion
				having sum(importe-(importerebajado+imppagado))>0 ";
				$consulta2 = $conexion->prepare($Sql);
				$consulta2->execute();
				$row2 = $consulta2->fetch();
				$Meses = $row2[1];
				$Deuda = $row2[0];
				//
				$this->Cell($Dim[1],$h,substr($codcatastro,4),0,0,'C',true);
				$this->Cell($Dim[2],$h,utf8_decode($row["codantiguo"]),0,0,'C',true);
				$this->Cell($Dim[3],$h,trim(utf8_decode(strtoupper($row["propietario"]))),0,0,'L',true);
				$this->Cell($Dim[4],$h,strtoupper($row["descripcioncorta"]." ".$row["calle"]." ".$row["nrocalle"]),0,0,'L',true);
				$this->Cell($Dim[5],$h,utf8_decode(strtoupper($row["nromed"])),0,0,'L',true);
				$this->Cell($Dim[6],$h,utf8_decode(strtoupper(trim($ts))),0,0,'L',true);
				$this->Cell($Dim[7],$h,substr(strtoupper($row["estadoservicio"]),0,3),0,0,'C',true);
				$this->Cell($Dim[8],$h,trim($Tar),0,0,'L',true);
				$this->Cell($Dim[9],$h,intval($consumo),0,0,'C',true);
				$this->Cell($Dim[10],$h,number_format($importe,2),0,0,'R',true);
				$this->Cell($Dim[11],$h,($Meses?$Meses:0),0,0,'C',true);
				$this->Cell($Dim[12],$h,number_format($Deuda,2),0,0,'R',true);
				$this->Cell($Dim[13],$h,'','B',1,'R',true);

				/*$this->Row(array($codcatastro,
							     $row["nroinscripcion"],
							 	 trim(utf8_decode(strtoupper($row["propietario"]))),
							     strtoupper($row["descripcioncorta"]." ".$row["calle"]." ".$row["nrocalle"]),
							 	 substr(strtoupper($row["nomtar"]),0,3),
							 	 strtoupper($row["estadoservicio"]),
							 	 $ts,
							 	 $row["nromed"],
							 	 strtoupper($row["sector"]),
							     $row["codmanzanas"],
							     $row["lote"],
							 	 $row["sublote"],
							     $row["domestico"],
							     $row["comercial"],
							     $row["industrial"],
							 	 $row["social"],
							     $row["estatal"]));*/
				
				
			}
			
			$this->Ln(3);
			$this->Cell(278, $h+2,"Usuario Registrados ".$count,'TB',1,'C');
			
		}
	}
	$Dim = array('1' =>20,'2'=>18,'3'=>63,'4'=>55,'5'=>15,'6'=>10,'7'=>10,'8'=>20,'9'=>10,'10'=>15,'11'=>10,'12'=>15,'13'=>15);
	$codsuc         = $_GET["codsuc"];
	$codsector      = $_GET["sector"];
	$tiposervicio   = $_GET["tiposervicio"];
	$estadoservicio = $_GET["estadoservicio"];
	$ciclo          = $_GET["ciclo"];
	$altocon        = $_GET["altocon"];
	$Desde          = $_GET["Desde"];
	$Hasta          = $_GET["Hasta"];
	$conpozo         = $_GET["conpozo"];
	$rutai         = $_GET["rutai"];
	$rutaf         = $_GET["rutaf"];
	$estadomedidor          = $_GET["estadomedidor"];
	$objReporte = new clsPadron("L");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsector,$tiposervicio,$estadoservicio,$codsuc,$ciclo,$altocon,$Desde,$Hasta,$conpozo,$rutai,$rutaf,$estadomedidor);
	$objReporte->Output();
	
?>