<?php

date_default_timezone_set("America/Lima");
session_name("pnsu");

// ini_set("display_errors",1);
// error_reporting(E_ALL);

include("../../../../objetos/clsReporte.php");
$objFunciones = new clsFunciones();

class clsPadron extends clsReporte {

    function cabecera() {
        global $codsuc, $meses;

        $periodo = $this->DecFechaLiteral();

        $this->SetFont('Arial', 'B', 10);
        $this->SetY(8);
        $h = 4;
        $this->Cell(278, $h + 2, "PADRON DE CONEXIONES", 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(278, $h + 2, $periodo["mes"]." - ".$periodo["anio"], 0, 1, 'C');
        $this->Ln(3);
    }

    function cabecera1($distrito, $sector, $ruta) {
        global $codsuc, $meses;

        // $this->SetY(8);
        $this->Ln(1);
        $h = 4;
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(30, $h, "", 0, 0, 'L');
        $this->Cell(25, $h, "DISTRITO:", 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $this->Cell(30, $h, strtoupper($distrito), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(45, $h, "", 0, 0, 'L');
        $this->Cell(20, $h, "SECTOR:", 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        
        //var_dump($this->GetX());

        $this->SetX(182);

        $this->Cell(30, $h, strtoupper(utf8_decode($sector)), 0, 0, 'R');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(42, $h, "", 0, 0, 'L');
        $this->Cell(25, $h, "RUTA:", 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $this->Cell(30, $h, strtoupper($ruta), 0, 1, 'L');

        $this->Ln(3);
    }

    function cabecera2() {
        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(25, 10, 13, 50, 40, 25, 8, 10, 10, 8, 10, 8, 15, 15, 15, 15));
        $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"));
        $this->Row(array("COD. CAT.", 
            "RUT. SEC.",
			"NRO. INSC.",
            "NOMBRES",
            "DIRECCION",
            "URBANIZACION",
            "T. SERV",
            "EST. CONEX",
            "SIT. CONEX",
            "#U. USO",
            "SUB CATEG",
            "TAR.",
            "#MED.",
            "FEC. INST. MEDIDOR",
            "FEC. INST. AGUA",
            "FEC. INST. DESA"
        ));
    }

    function contenido($nroinscripion, $propietario, $ruc, $direccion, $urbanizacion, $tiposervicio, $estado_conexion, $estadoservicio, $unidades_uso, $subcategoria, $tarifa, $medidor, $rut_lect, $rut_dist, $codcatastro, 
						$orden_lect, $codantiguo, $fechainsmed, $fechainsagua, $fechainsdesa)
	{
        global $conexion;

        $h = 4;
        $this->SetFont('Arial', '', 6);
        $this->Cell(25, $h, $codcatastro, 0, 0, 'C');
        $this->Cell(10, $h, $rut_lect."-".$orden_lect, 0, 0, 'C');
        $this->Cell(13, $h, $codantiguo, 0, 0, 'C');
        $this->Cell(50, $h, $propietario, 0, 0, 'L');
        $this->Cell(40, $h, $direccion, 0, 0, 'L');
        $this->Cell(25, $h, trim($urbanizacion), 0, 0, 'C');
        $this->Cell(8, $h, $tiposervicio, 0, 0, 'C');
        $this->Cell(10, $h, $estado_conexion, 0, 0, 'C');
        $this->Cell(10, $h, $estadoservicio, 0, 0, 'C');
        $this->Cell(8, $h, $unidades_uso, 0, 0, 'C');
        $this->Cell(10, $h, $subcategoria, 0, 0, 'C');
        $this->Cell(8, $h, $tarifa, 0, 0, 'C');
        $this->Cell(15, $h, $medidor, 0, 0, 'C');
        $this->Cell(15, $h, $this->DecFecha($fechainsmed), 0, 0, 'C');
        $this->Cell(15, $h, $this->DecFecha($fechainsagua), 0, 0, 'C');
        $this->Cell(15, $h, $this->DecFecha($fechainsdesa), 0, 1, 'C');
    }

    function pie($val1, $val2, $val3, $val4, $val5, $val6, $val7) {
        global $codsuc, $meses;

        // $this->SetY(8);
        $this->Ln(3);
        $h = 4;
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, $h, "", 0, 0, 'L');
        $this->Cell(40, $h, "Activas", 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $this->Cell(8, $h, $val1, 0, 0, 'C');
        //$this->SetFont('Arial', 'B', 10);
//        $this->Cell(55, $h, "Conex. de Grupo", 0, 0, 'L');
//        $this->SetFont('Arial', '', 9);
//        $this->Cell(8, $h, $val2, 0, 0, 'C');
//        $this->SetFont('Arial', 'B', 10);
//        $this->Cell(55, $h, "Conex. de Normales", 0, 0, 'L');
//        $this->SetFont('Arial', '', 9);
//        $this->Cell(8, $h, $val3, 0, 0, 'C');
//        $this->SetFont('Arial', 'B', 10);
//        $this->Cell(55, $h, "Conex. Secundarias", 0, 0, 'L');
//        $this->SetFont('Arial', '', 9);
//        $this->Cell(8, $h, $val4, 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, $h, "", 0, 0, 'L');

        $this->Cell(40, $h, "No Activas", 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $this->Cell(8, $h, $val5, 0, 0, 'C');

        //$this->Cell(55, $h, "", 0, 0, 'L');
//        $this->SetFont('Arial', '', 9);
//        $this->Cell(8, $h, $val6, 0, 0, 'C');
//
//        $this->Cell(55, $h, "", 0, 0, 'L');
//        $this->SetFont('Arial', '', 9);
//        $this->Cell(8, $h, $val7, 0, 0, 'C');
//
//        $this->Cell(55, $h, "", 0, 0, 'L');
//        $this->SetFont('Arial', '', 9);
//        $this->Cell(8, $h, $val8, 0, 1, 'C');
    }

}

$codsector      = $_GET["sector"];
$manzanas       = $_GET["manzanas"];
$codsuc         = $_GET["codsuc"];
$tiposervicio   = $_GET["tiposervicio"];
$estadoservicio = $_GET["estadoservicio"];
$ciclo          = $_GET["ciclo"];
$estadomedidor  = $_GET["estadomedidor"];
$altocon        = $_GET["altocon"];
$Desde          = $_GET["Desde"];
$Hasta          = $_GET["Hasta"];
$rutai          = $_GET["rutai"];
$rutaf          = $_GET["rutaf"];
$conpozo        = $_GET["conpozo"];

if ($altocon != 0) {
    $text = " and altocon=".$altocon;
}

if ($Desde != "") {
    $Condicion = " AND c.fechareg BETWEEN CAST ('".$Desde."' AS DATE) AND CAST ( '".$Hasta."' AS DATE ) ";
}

if ($codsector != "%") {
    $CSector = " AND c.codsector=".$codsector;
} else {
    $CSector = "";
}
if ($manzanas != "%") {
    $CManzanas = " AND c.codmanzanas= '".$manzanas."'";
} else {
    $CManzanas = "";
}
if ($tiposervicio != "%") {
    $Ctiposervicio = " AND c.codtiposervicio=".$tiposervicio;
} else {
    $Ctiposervicio = "";
}
if ($estadoservicio != "%") {
    $Cestadoservicio = " AND c.codestadoservicio=".$estadoservicio;
} else {
    $Cestadoservicio = "";
}
if ($estadomedidor != "%") {
    $Cestadomedidor = " AND c.codestadomedidor=".$estadomedidor;
} else {
    $Cestadomedidor = "";
}

if ($ciclo != "%") {
    $Cciclo = " AND c.codciclo=".$ciclo;
} else {
    $Cciclo = "";
}
if ($conpozo == 0)
    $conpozo = " AND cx.conpozo =0 ";
else
    $conpozo = '';

////$ord = " ORDER BY  c.codsector, CAST(c.codmanzanas as integer)";
	//$ord = " ORDER BY c.codsector, c.codrutlecturas, cx.orden_lect";

    $ord = " ORDER BY c.codsuc, c.codsector, CAST(c.codmanzanas AS INTEGER ), CAST(c.lote AS INTEGER) ,CAST(c.sublote AS INTEGER)";

$sql = "SELECT CAST(c.codmanzanas as integer), m.descripcion as manzanas, z.descripcion AS distrito, c.nroinscripcion, c.propietario, (tc.descripcioncorta ||' '|| cl.descripcion ||' '|| c.nrocalle) AS direccion,
    c.catetar AS tarifa,
    t.nomtar AS subcategoria,
    e.descripcion AS estadoservicio,
    c.nrodocumento as ruc,
    CASE
        WHEN c.codtiposervicio = 0 THEN 'NN'
        WHEN c.codtiposervicio = 1 THEN 'A/D'
        WHEN c.codtiposervicio = 2 THEN 'A'
        ELSE 'D'
    END AS tiposervicio,
        c.nromed  AS medidor,
        s.descripcion AS sector,
        c.codtipodocumento as codtipodocumento,
        c.lote,
        c.sublote,
        c.codsector AS codsector,
        CAST(c.domestico AS INTEGER ) + CAST(c.comercial AS INTEGER) + CAST(c.industrial AS INTEGER)  + CAST(c.social AS INTEGER)  + CAST(c.estatal AS INTEGER) AS unidades_uso,
        (u.tipo ||' '|| u.descripcion) as urbanizacion, CASE WHEN c.codestadoservicio = 1 THEN 'AC' ELSE 'IN' END AS estado_conexion,
        ".$objFunciones->getCodCatastral("c.").",
        c.codrutlecturas, c.codrutdistribucion,
		cx.orden_lect, c.codantiguo, cx.fechainsmed, cx.fechainsagua, cx.fechainsdesague
	FROM catastro.clientes AS c
        JOIN public.calles AS cl ON (c.codemp=cl.codemp AND c.codsuc=cl.codsuc AND c.codcalle=cl.codcalle AND c.codzona=cl.codzona)
        JOIN public.tiposcalle AS tc ON(cl.codtipocalle=tc.codtipocalle)
        JOIN facturacion.tarifas AS t ON(c.codemp=t.codemp AND c.codsuc=t.codsuc AND c.catetar=t.catetar AND t.estado = 1)
        JOIN public.estadoservicio AS e ON(c.codestadoservicio=e.codestadoservicio)
        JOIN public.sectores AS s ON(c.codemp=s.codemp AND c.codsuc=s.codsuc AND c.codsector=s.codsector AND c.codzona=s.codzona)
        JOIN catastro.conexiones AS cx ON(c.codemp=cx.codemp AND c.codsuc=cx.codsuc AND c.nroinscripcion=cx.nroinscripcion)
        JOIN public.manzanas AS m ON( c.codemp = m.codemp AND c.codsuc = m.codsuc AND c.codzona = m.codzona AND c.codsector = m.codsector AND c.codmanzanas = m.codmanzanas)
        JOIN admin.zonas AS z ON(c.codzona = z.codzona AND c.codsuc = z.codsuc AND c.codemp = z.codemp)
        JOIN public.rutaslecturas AS rl ON(c.codemp = rl.codemp AND c.codsuc = rl.codsuc AND c.codzona = rl.codzona AND c.codsector = rl.codsector AND c.codrutlecturas = rl.codrutlecturas AND c.codmanzanas = rl.codmanzanas)
        JOIN public.rutasdistribucion AS rd ON(c.codemp = rd.codemp AND c.codsuc = rd.codsuc AND c.codzona = rd.codzona AND c.codsector = rd.codsector AND c.codrutdistribucion = rd.codrutdistribucion AND c.codmanzanas = rd.codmanzanas)
        LEFT JOIN catastro.urbanizaciones AS u ON( c.codsuc = u.codsuc AND c.codzona = u.codzona AND c.codurbanizacion = u.codurbanizacion)
        WHERE c.codemp = 1 AND c.codsuc = ".$codsuc." ".$conpozo.$CSector.$CManzanas.$Ctiposervicio.$Cestadoservicio.$Cciclo.$text.$Cestadomedidor; //$Condicion

       //$sql .= "  ORDER BY  c.codsuc, c.codsector, CAST(c.codmanzanas AS INTEGER ), CAST(c.lote AS INTEGER) ,CAST(c.sublote AS INTEGER) ";

//echo $sql.$ord;

$count          = 0;
$consulta       = $conexion->prepare($sql.$ord);
$consulta->execute(array());
$items          = $consulta->fetchAll();
$codigo_manzana = '';
$codigo_sector  = '';

$objReporte = new clsPadron("L");
$objReporte->AliasNbPages();
$objReporte->AddPage();

$cantidad_activas           = 0;
$cantidad_no_activas        = 0;

$cantidad_cone_grupo        = 0;
$cantidad_no_cone_grupo     = 0;

$cantidad_cone_normales     = 0;
$cantidad_no_cone_normales  = 0;

$cantidad_cone_secundarias      = 0;
$cantidad_no_cone_secundarias   = 0;

foreach ($items as $ind => $row):

    if ($row['codrutlecturas'] != $codigo_ruta):

        if (!empty($codigo_ruta)):
            $objReporte->pie($cantidad_activas, $cantidad_cone_grupo, $cantidad_cone_normales, $cantidad_cone_secundarias, $cantidad_no_activas, $cantidad_no_cone_grupo, $cantidad_no_cone_normales, $cantidad_no_cone_secundarias);
        	
			$cantidad_cone_grupo = 0;
			
			$objReporte->AddPage();
		endif;

        $objReporte->cabecera1($row['distrito'], $row['sector'], $row['codrutlecturas']);
		
        if ($row['codsector'] != $codigo_sector):
            $objReporte->Ln(4);
            $objReporte->cabecera2();
        endif;
    endif;

    if ($row['codtipodocumento'] != '3'):
        $row['ruc'] = '';
    endif;
	
	if ($row['estado_conexion'] == 'AC'){$cantidad_activas++;}
	else{$cantidad_no_activas++;}
	
    $objReporte->contenido($row['nroinscripcion'], $row['propietario'], $row['ruc'], $row['direccion'], $row['urbanizacion'], $row['tiposervicio'], $row['estado_conexion'], substr($row['estadoservicio'], 0, 3)
            , $row['unidades_uso'], $row['subcategoria'], $row['tarifa'], $row['medidor'], $row['codrutlecturas'], $row['codrutdistribucion'], $row['codcatastro'], 
			$row['orden_lect'], $row['codantiguo'], $row['fechainsmed'], $row['fechainsagua'], $row['fechainsdesague']);

    $codigo_ruta    = $row['codrutlecturas'];
    $codigo_sector  = $row['codsector'];
	
    $cantidad_cone_grupo++;

endforeach;

if (!empty($codigo_ruta)):
    $objReporte->pie($cantidad_activas, $cantidad_cone_grupo, $cantidad_cone_normales, $cantidad_cone_secundarias, $cantidad_no_activas, $cantidad_no_cone_grupo, $cantidad_no_cone_normales, $cantidad_no_cone_secundarias);
endif;

$objReporte->Output();

?>
