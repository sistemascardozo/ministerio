<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "PADRON DE CONEXIONES";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
jQuery(function($)
	{
		redo_select();
		$( "#DivTipos" ).buttonset();
		$( "#codsector" ).change(function() {
			  cargar_rutas_distribucion()
			});

		var dates = $( "#FechaDesde, #FechaHasta" ).datepicker({
			//defaultDate: "+1w",
			buttonText: 'Selecione Fecha de Busqueda',
			showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			//showOn: 'both',
			showButtonPanel: true,
			numberOfMonths: 2,
			onSelect: function( selectedDate ) {
				var option = this.id == "FechaDesde" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});

	});
	var c = 0

	var buscar_manzanas = function(id){
		// console.log(id.value);
		$.ajax({
			 url:'<?php echo $_SESSION['urldir'];?>ajax/manzanas.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+id.value+'&codsuc=<?=$codsuc?>'+'&seleccion=10000&opcion=2',
			 success:function(datos){
				$("#divmanzanas").html(datos)
			 }
		})
	}

	function cargar_rutas_distribucion(codsector)
	{
		var codsector =$( "#codsector" ).val()
		$.ajax({
			 url:'<?php echo $_SESSION['urldir'];?>ajax/rutas_distribucion_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+codsector+'&codsuc=<?=$codsuc?>',
			 success:function(datos){
				$("#div_rutasdistribucion,#div_rutasdistribucion_2").html(datos)
				redo_select()
			 }
		})
	}
function redo_select(){
	$("#div_rutasdistribucion_2").find('select').attr('id','rutasdistribucion_fin').attr('name','rutasdistribucion_fin');
}
	function ValidarForm(Op)
	{

		var sectores 		= "";
		var manzanas 		= "";
		var tiposervicio	= "";
		var estadoservicio	= "";

		if($("#todosectores").val() == 0)
		{
			sectores=$("#codsector").val();

			if(sectores==0)
			{
				alert("Seleccione el Sector");
				return false;
			}
		}else{
			sectores="%";
		}

		if($("#todomanzanas").val() == 0)
		{
			manzanas=$("#manzanas").val();

			if(manzanas==0)
			{
				alert("Seleccione la Manzana");
				return false;
			}
		}else{
			manzanas="%";
		}

		if($("#rutasdistribucion").val()!=0)
		{
			if($("#rutasdistribucion_fin").val()!=0)
			{
				var rutas='&rutai='+$("#rutasdistribucion").val()+'&rutaf='+$("#rutasdistribucion_fin").val();
			}
			else
			{
				alert("Seleccione Ruta Final")
				return false;
			}
		}
		else
		{
			if($("#rutasdistribucion_fin").val()!=0)
			{
				alert("Seleccione Ruta Inicial")
				return false;
			}
		}

		if($("#todotiposervicio").val()==0)
		{
			tiposervicio=$("#tiposervicio").val()
			if(tiposervicio==0)
			{
				alert("Seleccione el Tipo de Servicio")
				return false;
			}
		}else{

			tiposervicio="%"
		}
	if($("#todoestadomedidor").val()==0)
		{
			estadomedidor=$("#codestadomedidor").val()
			if(estadomedidor==0)
			{
				alert("Seleccione el Estado de Medidor")
				return false;
			}
		}else{

			estadomedidor="%"
		}

		if($("#todoestadoservicio").val()==0)
		{
			estadoservicio=$("#estadoservicio").val()

			if(estadoservicio==0)
			{
				alert("Seleccione el Estado de Servicio")
				return false
			}
		}else{
			estadoservicio="%"
		}

		if($("#todosciclos").val()==0)
		{
			ciclos = $("#ciclo").val()
			if(ciclos==0)
			{
				alert("Seleccione el Ciclo de Facturacion")
				return false
			}
		}else{
			ciclos = "%"
		}

		var Fechas=''
		if($('#ChckFechaReg').prop("checked"))
		{
			var Desde = $("#FechaDesde").val()
			var Hasta = $("#FechaHasta").val()
			if (Trim(Desde)=='')
			{

				Msj('#FechaDesde','Seleccione la Fecha Inicial',1000,'above','',false)
				return false;

			}
			if (Trim(Hasta)=='')
			{

				Msj('#FechaHasta','Seleccione la Fecha Final',1000,'above','',false)
				return false;

			}
			Fechas = '&Desde='+Desde+'&Hasta='+Hasta
		}


		if(document.getElementById("rptpdf").checked == true)
		{
				url="imprimir.php";
		}

		url += "?sector="+sectores+"&manzanas="+manzanas+"&codsuc=<?=$codsuc?>"
		url += "&tiposervicio="+tiposervicio+"&estadoservicio="+estadoservicio+"&estadomedidor="+estadomedidor
		url+="&ciclo="+ciclos+"&altocon="+$("#altocon").val()+Fechas+rutas
		url+="&conpozo="+$('input[name="conpozo"]:checked').val()

		AbrirPopupImpresion(url,800,600)

		return false;
	}
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function quitar_disabled(obj,input)
	{
		if(obj.checked)
		{
			$("#"+input).attr('disabled', true);
		}else{
			$("#"+input).attr('disabled', false);
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>

    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="19%">Sucursal</td>
				    <td width="4%" align="center">:</td>
				    <td width="15%"><label>
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </label></td>
				    <td width="13%" align="right">&nbsp;</td>
				    <td width="4%" align="center">&nbsp;</td>
				    <td width="53%">&nbsp;</td>
				  </tr>
					<tr>
				    <td>Ciclo</td>
				    <td align="center">:</td>
				    <td colspan="4">
                    	<? $objMantenimiento->drop_ciclos($codsuc); ?>
                        <input type="checkbox" name="chkciclos" id="chkciclos" checked="checked" onclick="CambiarEstado(this,'todosciclos');quitar_disabled(this,'ciclo')" />
                        Todos los Ciclos</td>
			      </tr>
				  <tr>
				    <td>Sector</td>
				    <td align="center">:</td>
				    <td colspan="4">
                    	<?php echo $objMantenimiento->drop_sectores2($codsuc, 10000, "onchange='buscar_manzanas(this)'"); ?>
                        <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todosectores');quitar_disabled(this,'codsector');" />
			        	Todos los Sectores
                    </td>
			      </tr>
						<tr>
					    <td>Manzanas</td>
					    <td align="center">:</td>
					    	<td colspan="4">
								<div>
									<span id="divmanzanas">
										<?php echo $objMantenimiento->drop_manzanas($codsuc); ?>
									</span>
                  <input type="checkbox" name="chkmanzanas" id="chkmanzanas" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todomanzanas');quitar_disabled(this,'manzanas');" />
				        	Todos las Manzanas
								</div>
								</td>
				      </tr>
						<tr>
		            <td>Ruta Inicial</td>
		            <td align="center">:</td>
		            <td>
	                	<div id="div_rutasdistribucion">
	                    	<? $objMantenimiento->drop_rutas_distribucion($codsuc,0); ?>
	                    </div>
	                </td>
	                <td>Ruta Final</td>
		            <td align="center">:</td>
		            <td colspan="2">
		            	<div id="div_rutasdistribucion_2">
	                    	<? $objMantenimiento->drop_rutas_distribucion($codsuc,0); ?>
	                    </div>
		            </td>
		          </tr>
							<tr>
						    <td>Tip.  de Serv.</td>
						    <td align="center">:</td>
						    <td colspan="4">
					          <? $objMantenimiento->drop_tiposervicio(0,""); ?>
		                      <input type="checkbox" name="tipserviciochk" id="tipserviciochk" checked="checked" onclick="CambiarEstado(this,'todotiposervicio');quitar_disabled(this,'tiposervicio');" />
				              Todos los Tipos de Servicio
		                    </td>
					      </tr>
			      <tr>
							<tr>
						    <td>Est. de Serv.</td>
						    <td align="center">:</td>
						    <td colspan="4">
		                      <? $objMantenimiento->drop_estado_servicio(); ?>
					          <input type="checkbox" name="estsevicio" id="estsevicio" checked="checked" onclick="CambiarEstado(this,'todoestadoservicio');quitar_disabled(this,'estadoservicio');" />
				              Todos los Estados de Servicio
		                    </td>
					      </tr>
								<tr>
	                     <td>Estado de Medidor</td>
	                     <td align="center">:</td>
	                     <td colspan="4"><?php echo $objMantenimiento->drop_tipomedidor($codtipomedidor, 10000, ""); ?>

	                    <input type="checkbox" name="chkestmed" id="chkestmed" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todoestadomedidor');;quitar_disabled(this,'codestadomedidor')"
	                              />Todas los estados de Medidor </td>
	                 </tr>
	 			      <tr>
				    	<td>Fecha Reg.</td>
				    	<td align="center">:</td>
				    	<td colspan="4">
            		<input type="text" class="inputtext" id="FechaDesde" maxlength="10" value="<?=$Fecha?>" style="width:80px;" /> - <input type="text" class="inputtext" id="FechaHasta" maxlength="10" value="<?=$Fecha?>" style="width:80px;" />
		          	<input type="checkbox"  id="ChckFechaReg" checked="checked"  /> Todos
            	</td>
			      </tr>
						<tr>
				    <td>Con Pozo</td>
				    <td align="center">:</td>
				    <td colspan="4">
                      <div class="buttonset" style="display:inline">
                           <input type="radio" name="conpozo" id="conpozo1" value="1"  />
                            <label for="conpozo1">SI</label>
                            <input type="radio" name="conpozo" id="conpozo0" value="0" />
                           <label for="conpozo0">NO</label>
                           <input type="radio" name="conpozo" id="conpozo2" value="2" checked="checked"/>
                           <label for="conpozo2">AMBOS</label>

					</div>
                    </td>
			      </tr>
						<tr>
					    <td>&nbsp;</td>
					    <td align="center">&nbsp;</td>
					    <td colspan="4" style="padding:4px">
					      <input type="checkbox" name="chkaltocon" id="chkaltocon" onclick="CambiarEstado(this,'altocon');" />
					      Solo Altos Consumidores
	                    </td>
				      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td colspan="2">
              <input type="hidden" name="todosectores" id="todosectores" value="1" />
            	<input type="hidden" name="todomanzanas" id="todomanzanas" value="1" />
							<input type="hidden" name="todotiposervicio" id="todotiposervicio" value="1" />
							<input type="hidden" name="todoestadomedidor" id="todoestadomedidor" value="1" />
							<input type="hidden" name="todoestadoservicio" id="todoestadoservicio" value="1" />
							<input type="hidden" name="todosciclos" id="todosciclos" value="1" />
							<input type="hidden" name="altocon" id="altocon" value="0" />
						</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>

			      <tr>
        			<td colspan="6">
        				<div id="DivTipos" style="display:inline">
									<input type="radio" name="rabresumen" id="rptpdf" value="radio" checked="checked" />
                  <label for="rptpdf">PDF</label>
                  <!-- <input type="radio" name="rabresumen" id="rptexcel" value="radio2" />
                 <label for="rptexcel">EXCEL</label>
                 <input type="radio" name="rabresumen" id="rptpdfdeuda" value="radio3" />
                 <label for="rptpdfdeuda">CON DEUDA</label> -->
						</div>
						 <input type="button" onclick="return ValidarForm();" value="Generar" id="">
        			</td>
        		</tr>
				</table>
			</fieldset>
		</td>
	</tr>

	 </tbody>

    </table>
 </form>
</div>
<script>
	$("#ciclo").attr('disabled', true);
	$("#codsector").attr('disabled', true);
	$("#manzanas").attr('disabled', true);
	$("#tiposervicio").attr('disabled', true);
	$("#estadoservicio").attr('disabled', true);
	$("#estadoservicio").attr('disabled', true);
</script>
<?php CuerpoInferior(); ?>
