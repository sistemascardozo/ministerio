<?php
	include("../../../../objetos/clsReporte.php");
	include("../../../../objetos/clsReporteExcel.php");
	
	header("Content-type: application/vnd.ms-excel; name='excel'");
	header("Content-Disposition: filename=Rpt_Grupo.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	$clsFunciones = new clsFunciones();
	$objReporte = new clsReporte();
	
	$codemp         = 1;
	$ciclo          = $_GET['ciclo'];
	$codsuc         = $_GET['codsuc'];
	$tipoentidad    = (!empty($_GET['tipoentidades'])) ? $_GET['tipoentidades'] : 0 ;
	$tipoentidadesN = (!empty($_GET['tipoentidadesN'])) ? strtoupper($_GET['tipoentidadesN']) : 'TODOS';
	$qls = "";
	
	if($tipoentidad != 0)
	{
		$qls = " WHERE cone.codtipoentidades = ". $tipoentidad;
	}
	
	CabeceraExcel(2, 13); ?>
<table class="ui-widget" border="0" cellspacing="0" id="TbTitulo" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:14px">

        <tr title="Cabecera">
            <th scope="col" colspan="6" align="center" >REPORTE POR GRUPO DE <?=$tipoentidadesN?></th>
        </tr>
    </thead>
</table>
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">

        <tr>
            <th width="50" scope="col" class="ui-widget-header">N&deg;</th>
            <th width="200" scope="col" class="ui-widget-header">NRO INSCRIPCION</th>
            <th width="30" scope="col" class="ui-widget-header">CLIENTE</th>
            <th width="50" scope="col" class="ui-widget-header">DIRECCION</th>
            <th width="50" scope="col" class="ui-widget-header">ESTADO DE SERVICIO</th>
            <th width="50" scope="col" class="ui-widget-header">GRUPO</th>        
        </tr>
    </thead>
    <tbody style="font-size:12px">
        <?php
        $h = 4;
        $s = 2;

        $sqlLect = "SELECT 
                clie.nroinscripcion as nroinscripcion,
                upper(clie.propietario) as cliente,
                upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
                estser.descripcion as estado_servicio,
                tpe.descripcion as tipo_entidad
                FROM catastro.clientes as clie
                INNER JOIN public.calles as cal on(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona) 
                INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) 
                INNER JOIN public.estadoservicio as estser on(clie.codestadoservicio=estser.codestadoservicio) 
                INNER JOIN catastro.conexiones cone ON (clie.codemp = cone.codemp AND clie.codsuc = cone.codsuc AND clie.nroinscripcion = cone.nroinscripcion)
                INNER JOIN public.tipoentidades as tpe ON(tpe.codtipoentidades=cone.codtipoentidades) " . $qls . "
                ORDER BY nroinscripcion";
                        
        $consultaL = $conexion->prepare($sqlLect);
        $consultaL->execute();
        $items = $consultaL->fetchAll();

        $count = 0;
        foreach ($items as $row) {
            $count++;
            $numeroinscripcion = $clsFunciones->CodUsuario($codsuc, $row['nroinscripcion']);
            ?>

            <tr <?= $title ?> <?= $class ?> onclick="SeleccionaId(this);" id="<?= $count ?>" >
                <td align="left" valign="middle"><? echo $count; ?></td>
                <td align="left" valign="middle"><? echo ($numeroinscripcion); ?></td>
                <td align="left" valign="middle"><? echo ($row['cliente']); ?></td>
                <td align="left" valign="middle"><? echo ($row['direccion']); ?></td>
                <td align="left" valign="middle"><? echo ($row['estado_servicio']); ?></td>
                <td align="left" valign="middle"><? echo ($row['tipo_entidad']); ?></td>
            <?php
            }
            ?>
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td colspan="12" align="center" class="ui-widget-header">Numero de Registros el grupo  <?= $tipoentidadesN ?> : <?= $count ?></td>
        </tr>
    </tfoot>
</table>
