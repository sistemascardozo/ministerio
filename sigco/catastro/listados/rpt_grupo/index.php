<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "RPT. X GRUPO";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	jQuery(function($)
	{ 
		$( "#DivTipos" ).buttonset();
	});
	
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc 	= <?=$codsuc?>
	
	function ValidarForm(Op)
	{
		var ciclos  = $("#ciclo").val()
		
		if(ciclos==0)
		{
			alert("Seleccione el Ciclo")
			return false
		}
		
		var tipoentidades=''
		if($('#Todos').attr("checked")!="checked")
		{
			if($("#tipoentidades").val()==0)
			{
				alert('Seleccione el Tipo de Entidad')
				return false
			}

			tipoentidades = '&tipoentidades='+$("#tipoentidades").val()+'&tipoentidadesN='+$("#tipoentidades option:selected").html()
		}

		if(document.getElementById("rptpdf").checked==true)
		{
			url = "imprimir.php?ciclo="+ciclos+"&codsuc="+codsuc;
		}
		if(document.getElementById("rptexcel").checked==true)
		{
			url = "excel.php?ciclo="+ciclos+"&codsuc="+codsuc;
		}
		
		
		url +=tipoentidades
		AbrirPopupImpresion(url,800,600)
		
		return false
	}	
	function Cancelar()
	{
		location.href = '<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
function TodosEstados()
	{
		if($('#Todos').attr("checked")=="checked")
		{
			
			
			$('#tipoentidades').attr('disabled', 'disabled');
		
			
		}
		else
		{
		
			$('#tipoentidades').attr('disabled', false);
			
		}
	
	}
function cargar_anio_drop(obj)
	{
		$.ajax({
			 url:'../../../../ajax/anio_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&codciclo='+obj+'&condicion=1',
			 success:function(datos){
				$("#div_anio").html(datos)
			 }
		}) 
	}
	function cargar_mes(ciclo,suc,anio)
	{
		$.ajax({
			 url:'../../../../ajax/mes_drop.php',
			 type:'POST',
			 async:true,
			 data:'codciclo='+ciclo+'&codsuc='+suc+'&anio='+anio,
			 success:function(datos){
				$("#div_meses").html(datos)
			 }
		}) 
	}
function quitar_disabled(obj,input)
{
	if(obj.checked)
	{
		$("#"+input).attr("disabled",true)
	}else{
		$("#"+input).attr("disabled",false)
	}
}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>

    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="100" align="right">Sucursal</td>
				    <td width="10" align="center">:</td>
				    <td colspan="4">
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </td>
				    
				  </tr>
					<tr>

					<td align="right">Ciclo</td>
					<td align="center">:</td>
					<td colspan="4">
						<? $objMantenimiento->drop_ciclos($codsuc, 0, "onchange='cargar_anio_drop(this.value);'"); ?>
					</td>
					</tr>
				            
			      	<tr>
                        <td align="right">Grupo</td>
                        <td align="center">:</td>
                        <td width="200"><?php $objMantenimiento->drop_tipo_entidades($items["codtipoentidades"], 'onChange="actualizacion(this, 1);"'); ?></td>
                        <td width="5" align="right">&nbsp;</td>
                        <td width="20" align="center"><input type="checkbox" id="Todos" onclick="TodosEstados();"></td>
                        <td>
                        	Todos
                        </td>
                  	</tr>
			      	<tr>
			      	  <td align="right">&nbsp;</td>
			      	  <td align="center">&nbsp;</td>
			      	  <td>&nbsp;</td>
			      	  <td align="right">&nbsp;</td>
			      	  <td align="center">&nbsp;</td>
			      	  <td>&nbsp;</td>
		      	  </tr>
			      	<tr>
       					<!-- <td colspan="6" align="center"> 
       						<input type="button" onclick="return ValidarForm();" value="Generar" id="">
       					</td> -->
       					<td colspan="6">
							<div id="DivTipos" style="display:inline">
								<input type="radio" name="rabresumen" id="rptpdf" value="radio" checked="checked" />
								<label for="rptpdf">PDF</label>
								<input type="radio" name="rabresumen" id="rptexcel" value="radio2" />
								<label for="rptexcel">EXCEL</label>				            
							</div>	
							<input type="button" onclick="return ValidarForm();" value="Generar" id="">
						</td>
					</tr>
						  
			
				</table>
			</fieldset>
		</td>
	</tr>
 
	 </tbody>

    </table>
 </form>
</div>
<?php CuerpoInferior(); ?>