<?php 
	include("../../../../objetos/clsReporte.php");
    
	$clfunciones = new clsFunciones();

	class clsVolumen extends clsReporte
	{
		function Header() {

            global $codsuc,$meses,$tipoentidadesN;
			global $Dim;
			
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "REPORTE POR GRUPO DE ". $tipoentidadesN;
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["num1"]);
            $tit3 = "SUCURSAL: ". utf8_decode(strtoupper($empresa["descripcion"]));
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

		function cabecera(){	
            
            global $Dim;

			$this->Ln(20);
            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=8;

            $this->SetFont('Arial','B',8);
            $this->Cell($Dim[1],$h,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[2],$h,utf8_decode('INSCRIPCION'),1,0,'C',true);            
            $this->Cell($Dim[3],$h,utf8_decode('NOMBRES'),1,0,'C',true);            
            $this->Cell($Dim[4],$h,utf8_decode('DIRECCION'),1,0,'C',true);            
            $this->Cell($Dim[5],$h,utf8_decode('ESTADO SERVICIO'),1,0,'C',true);            
            $this->Cell($Dim[6],$h,utf8_decode('GRUPO'),1,1,'C',true);            
		}

        function Contenido($cont,$inscripcion, $nombres, $direccion, $estaservi, $grupo) {
			
            global $Dim;

			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);
			$h = 10;
			$this->SetFont('Arial','',8);
			$this->Cell($Dim[1],$h,$cont,0,0,'C',true);
			$this->Cell($Dim[2],$h,utf8_decode($inscripcion),0,0,'C',true);
			$this->Cell($Dim[3],$h,utf8_decode($nombres),0,0,'L',true);
			$this->Cell($Dim[4],$h,utf8_decode($direccion),0,0,'L',true);
			$this->Cell($Dim[5],$h,utf8_decode($estaservi),0,0,'C',true);
			$this->Cell($Dim[6],$h,utf8_decode($grupo),0,1,'C',true);	
		}
    }

    $Dim = array('1'=>15,'2'=>20,'3'=>80,'4'=>80,'5'=>35,'6'=>50);
	
    // Actual
	$codemp         = 1;
	$ciclo          = $_GET['ciclo'];
	$codsuc         = $_GET['codsuc'];
	$tipoentidad    = (!empty($_GET['tipoentidades'])) ? $_GET['tipoentidades'] : 0 ;
	$tipoentidadesN = (!empty($_GET['tipoentidadesN'])) ? strtoupper($_GET['tipoentidadesN']) : 'TODOS';
	$qls = "";

	if($tipoentidad != 0):
		$qls = " WHERE cone.codtipoentidades = ". $tipoentidad;
	endif;

    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT 
				clie.nroinscripcion as nroinscripcion,
				upper(clie.propietario) as cliente,
				upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
				estser.descripcion as estado_servicio,
				tpe.descripcion as tipo_entidad
				FROM catastro.clientes as clie
				INNER JOIN public.calles as cal on(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona) 
				INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) 
				INNER JOIN public.estadoservicio as estser on(clie.codestadoservicio=estser.codestadoservicio) 
				INNER JOIN catastro.conexiones cone ON (clie.codemp = cone.codemp AND clie.codsuc = cone.codsuc AND clie.nroinscripcion = cone.nroinscripcion)
				INNER JOIN public.tipoentidades as tpe ON(tpe.codtipoentidades=cone.codtipoentidades) " . $qls . "
				ORDER BY nroinscripcion";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute();
    $row = $consultaL->fetchAll();
   
	
	$objReporte = new clsVolumen('L');
	$contador   = 0;
	$objReporte->AliasNbPages();
	$objReporte->AddPage('H');

    $contador  = 0;

    foreach ($row as $value): 
        $contador++;
        $numeroinscripcion = $clfunciones->CodUsuario($codsuc, $value['nroinscripcion']);
		$objReporte->contenido($contador,$numeroinscripcion,$value['cliente'],$value['direccion'], $value['estado_servicio'], $value['tipo_entidad']);
    endforeach;
	$objReporte->Output();	
		
?>