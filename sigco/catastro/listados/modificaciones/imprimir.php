<?php
	include("../../../../objetos/clsReporte.php");
	
	class clsModificaciones extends clsReporte
	{
		function camposmodificaciones($campocliente,$actual,$ultimo)
		{
			global $conexion;
			$res = array();
			$sql = "select descripcion,trim(tabla) as tabla,trim(columna) as columna,campo
					from catastro.campoclientes where campo=?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($campocliente));
			$items = $consulta->fetch();
			$res['campo']=$items["descripcion"];
			$res['valoractual']=$actual;
			$res['valorultimo']=$ultimo;
			if(trim($items["tabla"])!='' && trim($items["columna"])!='')
			{
				$Sql="SELECT ".$items["columna"]." FROM ".$items["tabla"]." WHERE ".$items["campo"]."='".$actual."'";
				$consulta = $conexion->prepare($Sql);
				$consulta->execute();
				$item = $consulta->fetch();
				$res['valoractual']=$item[0];

				$Sql="SELECT ".$items["columna"]." FROM ".$items["tabla"]." WHERE ".$items["campo"]."='".$ultimo."'";
				$consulta = $conexion->prepare($Sql);
				$consulta->execute();
				$item = $consulta->fetch();
				$res['valorultimo']=$item[0];
			}
			if($campocliente=='tipofacturacion')
			{
				$tipofacturacion[0]='CONSUMO LEIDO';
				$tipofacturacion[1]='CONSUMO PROMEDIADO';
				$tipofacturacion[2]='CONSUMO ASIGNADO';
				
				$res['valoractual'] = $tipofacturacion[$actual];
				$res['valorultimo'] = $tipofacturacion[$ultimo];
			}
			return $res;
		}
		function cabecera()
		{
			$h=4;
			$this->SetFont('Arial','B',12);
			$this->Cell(190, $h+2,"LISTADO MODIFICACIONES AL PADRON DE USUARIOS",0,1,'C');
			
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(50,45,30,15,20,30));
			$this->SetAligns(array("C","C","C","C","C","C","C"));
			$this->Row(array("CAMPO",
							 "VAL. ANTERIOR",
							 "VAL. ULTIMO",
							 "FECHA",
							 "RESPONSABLE",
							 "OBSERVACION"));

		}
		function contenido($codsuc, $fechaini, $fechafinal, $nroinscri = 0)
		{
			global $conexion;
			
			$h 				= 4;
			$nroinscripcion = "";
			
			$this->SetFont('Arial','',6);


			if( $nroinscri == 0 ) :
				$nro = " ";				
			else:
				$nro = " AND m.nroinscripcion = " . $nroinscri;
			endif;

			$sql = "select m.nroinscripcion,c.propietario,m.campo,m.valoractual,
						m.fechacontrol,m.creador,m.valorultimo,m.observacion
					from catastro.modificaciones as m
					inner join catastro.clientes as c on(m.codemp=c.codemp and m.codsuc=c.codsuc and m.nroinscripcion=c.nroinscripcion)
					where m.fechacontrol between ? and ? and m.codsuc=?" . $nro." 
					ORDER BY m.fechacontrol DESC";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($fechaini,$fechafinal,$codsuc));
			$items = $consulta->fetchAll();

			foreach($items as $row)
			{
				if($nroinscripcion!=$row["nroinscripcion"])
				{
					$this->Cell(20, $h, "Usuario", 0, 0, 'R');
					$this->Cell(5, $h, ":", 0, 0, 'C');
					$this->Cell(71, $h, $this->CodUsuario($codsuc,$row["nroinscripcion"])." - ".utf8_decode($row["propietario"]), 0, 1, 'L');
					$this->Ln(3);
				}				
				$datos = $this->camposmodificaciones($row["campo"], $row["valoractual"], $row["valorultimo"]);
				$this->SetWidths(array(50, 45, 30, 15, 20,30));
				$this->SetAligns(array("L", "C", "C", "C", "C", "C","C"));
				$this->Row(array($datos["campo"],
							 	 strtoupper(utf8_decode($datos["valoractual"])),
							 	 strtoupper(utf8_decode($datos["valorultimo"])),
							 	 $this->DecFecha($row["fechacontrol"]),
							 	 $this->login($row["creador"]),
							 	 $row["observacion"]));	
			
				$nroinscripcion = $row["nroinscripcion"];
			}			
		}
	}
	
	$fdesde         = $_GET["fdesde"];
	$fhasta         = $_GET["fhasta"];
	$codsuc         = $_GET["codsuc"];
	$nroinscripcion = (!empty($_GET["nroinscripcion"]))?$_GET["nroinscripcion"]:0 ;
	$NroInscripcion = (!empty($_GET["NroInscripcion"]))?$_GET["NroInscripcion"]:0 ;

	if($NroInscripcion != 0) $nroinscripcion = $NroInscripcion;
	if($fdesde == '') $fdesde = '2014-01-01';
	if($fhasta == '') $fhasta = date('Y-m-d');
	if($codsuc == '') $codsuc = $_SESSION['IdSucursal'];
	
	$objReporte = new clsModificaciones();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsuc, $objReporte->CodFecha($fdesde), $objReporte->CodFecha($fhasta), $nroinscripcion);
	$objReporte->Output();
	
?>