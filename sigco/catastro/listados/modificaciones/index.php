<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "MODIFICACIONES";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	jQuery(function($)
	{ 
		$("#fdesde").mask("99/99/9999");
		$("#fhasta").mask("99/99/9999");
	})
	$(function() {
	$( "#fdesde" ).datepicker(
		{
					showOn: 'button',
					direction: 'up',
					buttonImage: '../../../../images/iconos/calendar.png',
					buttonImageOnly: true,
					showOn: 'both',
					showButtonPanel: true
				}
	);
	
		$( "#fhasta" ).datepicker(
		{
					showOn: 'button',
					direction: 'up',
					buttonImage: '../../../../images/iconos/calendar.png',
					buttonImageOnly: true,
					showOn: 'both',
					showButtonPanel: true
				}
	);
		
	});
	var c              = 0
	var nroinscripcion = ''
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" ;
	var codsuc 	= "<?=$codsuc?>";
	function buscar_usuarios()
	{
		
		object="usuario" 
		AbrirPopupBusqueda('../../../catastro/operaciones/actualizacion/?Op=5', 1150, 450)
	}
	function Recibir(id)
	{
		if(object=="usuario")
		{
			$("#nroinscripcion").val(id)
			cargar_datos_usuario(id)
			$("#chkusuario").attr('checked',false)
			$("#nrotodos").val(0);
		}
		
	}
	function cargar_datos_usuario(nroinscripcion)
	{

		$.ajax({
			 url:'../../../../ajax/clientes.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&nroinscripcion='+nroinscripcion,
			  dataType: "json",
			 success:function(datos){
				
				$("#codantiguo").val(datos.codantiguo)
				$("#cliente").val(datos.propietario)
				$("#chkusuario").attr('checked',false)
				$("#nrotodos").val(0);
			 }
	    }) 
	}
	function MostrarDatosUsuario(datos)
	{
		$("#nroinscripcion").val(datos.nroinscripcion)
		$("#cliente").val(datos.propietario)
		$("#chkusuario").attr('checked',false)
		$("#nrotodos").val(0);
		
	}
	function ValidarForm(Op)
	{

		if($("#nrotodos").val() == 0 )
		{

			if( $("#nroinscripcion").val() == '' ||  $("#nroinscripcion").val() == 0)
			{
				Msj('#codantiguo','Introduce una inscripcion valida',1000,'above','',false)
				$("#codantiguo").val('');
				return false;
			}
			else
			{
				nroinscripcion = $("#nroinscripcion").val();
			}

		}else
		{
			nroinscripcion = '';			
		}
		var Fechas=''
		if($('#ChckFechaReg').attr("checked")!="checked")
		{
			Fechas = '&fdesde='+$("#fdesde").val()+"&fhasta="+$("#fhasta").val()
		}
		url = "imprimir.php?codsuc=<?=$codsuc?>"+"&nroinscripcion="+nroinscripcion+Fechas
		AbrirPopupImpresion(url,800,600)
		
		return false;
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function CambObservaciones(obj)
	{
		if(obj.checked==true)
		{
			$("#nrotodos").val(1);
			$("#nroinscripcion").val('');
			$("#nroinscripcion").attr('readonly',"readonly");
		}else{
			$("#nrotodos").val(0);
			$("#nroinscripcion").val('');
			$("#nroinscripcion").focus();
			$("#nroinscripcion").removeAttr('readonly');
		}
	}	

</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    	<tr>
        	<td colspan="2">
				<fieldset style="padding:4px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  		<tr>
							<td width="90">Sucursal</td>
                            <td width="30" align="center">:</td>
                            <td colspan="3">
				      			<input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    		</td>
                            <td colspan="2">&nbsp;</td>
				  		</tr>
				  		<tr style="height:5px">
				    		<td colspan="7"></td>
			      		</tr>
                      	<tr style="background-color:#0099CC; border: 1 #000000 solid">
                        	<td colspan="7" style="padding:4px; color: white; font-weight: bold;">Seleccione el Rango de Fecha a Consultar</td>
                      	</tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td align="center">&nbsp;</td>
                        	<td colspan="3">&nbsp;</td>
                        	<td colspan="2">&nbsp;</td>
                      	</tr>
                      	<tr>
                        	<td>Fecha Modificaacion</td>
                        	<td align="center">:</td>
                        	<td colspan="4">
                            	<input type="text" name="fdesde" id="fdesde" value="<?=date('d/m/Y')?>" class="inputtext" style="width:80px;" />
								- <input type="text" name="fhasta" id="fhasta" value="<?=date('d/m/Y')?>" class="inputtext" style="width:80px;" />
								<input type="checkbox"  id="ChckFechaReg" checked="checked"  />
		              Todos
                             </td>
                      	</tr>
                      	<tr>
                      		 <td>Usuario</td>
					        <td align="center">:</td>
					        <td>
					          		        	
					        	<input type="hidden" name="nroinscripcion" id="nroinscripcion" size="10" class="inputtext entero"  value="<?php echo $row["nroinscripcion"]?>" />
			              <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" value="<?php echo $row["codantiguo"]?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:75px;"/>
			              <span class="MljSoft-icon-buscar" title="Buscar Usuario" onclick="buscar_usuarios();"></span>
					        	
					    		</td>
					    		<td colspan='2'>
					        <input type="text" name="cliente" id="cliente" class="inputtext" size="45" maxlength="45" value="" readonly="readonly" />
					        </td>
                         	<td>
                       			<input type="checkbox" id="chkusuario" value="checkbox" checked="checked" onclick="CambObservaciones(this)" />
								Todos
                    		</td>
                      	</tr>
                      	<tr>
                      	  <td>&nbsp;</td>
                      	  <td align="center">&nbsp;</td>
                      	  <td colspan="3">&nbsp;</td>
                      	  <td>&nbsp;</td>
                   	  </tr>
				  		 <tr><td colspan="7" align="center"> <input type="button" onclick="return ValidarForm();" value="Generar" id=""></td>
				  		 	</tr>
					</table>
				</fieldset>
			</td>
		</tr>
        <tr>
         
        </tr>
	 </tbody>

    </table>
    <input type="hidden" name="nrotodos" id="nrotodos" value="1"/>

 </form>
</div>
<?php CuerpoInferior(); ?>