<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsLecturas extends clsReporte
	{
		function cabecera()
		{
			global $anio,$mes,$meses,$sectores,$rutas,$Dim;
			
			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"LISTADO PARA LA TOMA DE LECTURAS",0,1,'C');
			$this->Ln(3);
			
			$this->SetFont('Arial','',7);
			$this->Cell(10, $h,"A�O",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(10, $h,$anio,0,0,'L');
			$this->Cell(10, $h,"MES",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$meses[$mes],0,0,'L');
			$this->Cell(30, $h,strtoupper($sectores),0,0,'L');
			$this->Cell(40, $h,strtoupper($rutas),0,0,'L');
			$this->Cell(10, $h,"FECHA",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,"",0,0,'L');
			$this->Cell(10, $h,"TOMADOR",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,"",0,1,'L');
			
			//$this->SetWidths(array(25,15,40,65,10,23,30,45));
			$this->Cell($Dim[1],$h,utf8_decode('COD. CATASTRAL'),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode('INSCRIPCION'),1,0,'C',false);
			$this->Cell($Dim[3],$h,utf8_decode('DIRECCION'),1,0,'C',false);
			$this->Cell($Dim[4],$h,utf8_decode('USUARIO'),1,0,'C',false);
			$this->Cell($Dim[5],$h,utf8_decode('EST'),1,0,'C',false);
			$this->Cell($Dim[6],$h,utf8_decode('CAT'),1,0,'C',false);
			$this->Cell($Dim[7],$h,utf8_decode('NRO. MED'),1,0,'C',false);
			$this->Cell($Dim[8],$h,utf8_decode('LECTURA'),1,0,'C',false);
			$this->Cell($Dim[9],$h,utf8_decode('OBSERVACION'),1,1,'C',false);
			
			/*$this->Row(array("COD. CATASTRAL","CODIGO",
							 "DIRECCION",
							 "USUARIO",
							 "CAT",
							 "NRO. MED",
							 "LECTURA",
							 "OBSERVACION"));
			*/
			$this->SetFont('Arial','',7);
			
			
		}
		function contenido($nroinscripcion,$codantiguo,$direccion,$propietario,$nromed,$catastro,$nomtar,$estadoservicio)
		{
			global $Dim;
			/*$this->SetWidths(array(25,15,40,65,10,23,30,45));
			$this->SetAligns(array("C","C","L","L","C","C","C"));
			$this->Row(array($catastro,$codantiguo,
							 strtoupper(trim($direccion)),
							 strtoupper(trim($propietario)),
							 $nomtar,
							 $nromed,
							 "",
							 ""));
			*/
$this->SetFillColor(255,255,255); //Color de Fondo
		$this->SetTextColor(0);
			$h=5;
			$this->Cell($Dim[1],$h,utf8_decode($catastro),1,0,'C',true);
			$this->Cell($Dim[2],$h,utf8_decode($codantiguo),1,0,'C',true);
			$this->Cell($Dim[3],$h,utf8_decode(strtoupper(trim($direccion))),1,0,'L',true);
			$this->Cell($Dim[4],$h,utf8_decode(strtoupper(trim($propietario))),1,0,'L',true);
			$this->Cell($Dim[5],$h,utf8_decode($estadoservicio),1,0,'C',true);
			$this->Cell($Dim[6],$h,utf8_decode($nomtar),1,0,'C',true);
			$this->Cell($Dim[7],$h,utf8_decode($nromed),1,0,'C',true);
			$this->Cell($Dim[8],$h,utf8_decode(''),1,0,'C',true);
			$this->Cell($Dim[9],$h,utf8_decode(''),1,1,'C',true);

			
			
		}
	}
	$Dim = array('1' =>25,'2'=>18,'3'=>60,'4'=>55,'5'=>10,'6'=>10,'7'=>23,'8'=>30,'9'=>47);
	$codemp		= 1;
	$codsuc 	= $_GET["codsuc"];
	$anio		= $_GET["anio"]; 
	$mes		= $_GET["mes"]; 
	$ciclo		= $_GET["ciclo"];
	$ruta		= $_GET["ruta"];
	$sector		= $_GET["sector"];
	$orden		= $_GET["orden"];
	
	$sect 	= "";
	$rut	= "";
	
	$objReporte = new clsLecturas("L");
	
	if($orden==1)
	{
		$order = " order by c.nroorden  ";
	}else{
		$order = " order by  c.nroorden ";
	}
	if($_GET["sector"]=='%')
		$order = " order by c.codsector,c.nroorden  ";
	
	$sqlLect = "select c.nroinscripcion,c.propietario,tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,c.nromed,
				s.descripcion as sector,rtml.descripcion as rutas,".$objReporte->getCodCatastral("c.").", c.codantiguo,t.nomtar,
				es.descripcion as estadoservicio
				from medicion.lecturas as l
				inner join catastro.clientes as c on(l.codemp=c.codemp and l.codsuc=c.codsuc and l.nroinscripcion=c.nroinscripcion)
				inner join public.rutaslecturas as rl on(c.codsector=rl.codsector and c.codrutlecturas=rl.codrutlecturas and 
				c.codmanzanas=rl.codmanzanas and c.codemp=rl.codemp and c.codsuc=rl.codsuc)
				inner join public.rutasmaelecturas rtml on(rl.codrutlecturas=rtml.codrutlecturas and rl.codsector=rtml.codsector
				and rl.codemp=rtml.codemp and rl.codsuc=rtml.codsuc)
				INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
				inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
				inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
				inner join facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
				inner join public.estadoservicio as es on(es.codestadoservicio=c.codestadoservicio)
				where l.codemp=? and l.codsuc=? and l.codciclo=? and l.anio=? and l.mes=? ";
	if($sector!="%")
		$sqlLect .= " and c.codsector =".$sector;
	if($ruta!="%")
		$sqlLect .= " and c.codrutlecturas=".$ruta;
	 $sqlLect.$order;
	$consultaL = $conexion->prepare($sqlLect.$order);
	$consultaL->execute(array($codemp,
							  $codsuc,
							  $ciclo,
							  $anio,
							  $mes));
	$itemsL = $consultaL->fetchAll();
	
	foreach($itemsL as $rowL)
	{
		$sectores 	= $rowL["sector"];
		$rutas		= $rowL["rutas"];
		
		if($sect!=$sectores || $rut!=$rutas)
		{
			$objReporte->AliasNbPages();
			$objReporte->AddPage('H');
		}

		$objReporte->contenido($rowL["nroinscripcion"],$rowL['codantiguo'],$rowL["descripcioncorta"]." ".$rowL["calle"]." ".$rowL["nrocalle"],
							   utf8_decode($rowL["propietario"]),$rowL["nromed"],$rowL["codcatastro"],substr(strtoupper($rowL["nomtar"]),0,3),substr(strtoupper($rowL['estadoservicio']),0,3));
		
		$sect 	= $sectores;
		$rut	= $rutas;
	}
	$objReporte->Output();
	
?>