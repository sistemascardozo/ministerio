<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}


include("../../../../objetos/clsReporte.php");
include("../../../../objetos/clsReporteExcel.php");


header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=PadronUsuarios.xls");
header("Pragma: no-cache");
header("Expires: 0");


$clsFunciones   = new clsFunciones();
$objReporte     = new clsReporte();



/*$codsuc = $_GET["codsuc"];
$codsector = $_GET["sector"];
$tiposervicio = $_GET["tiposervicio"];
$estadoservicio = $_GET["estadoservicio"];
$ciclo = $_GET["ciclo"];
$altocon = $_GET["altocon"];
$Desde = $_GET["Desde"];
$Hasta = $_GET["Hasta"];
$conpozo = $_GET["conpozo"];*/
$codsector      = $_GET["sector"];
$manzanas       = $_GET["manzanas"];
$codsuc         = $_GET["codsuc"];
$tiposervicio   = $_GET["tiposervicio"];
$estadoservicio = $_GET["estadoservicio"];
$ciclo          = $_GET["ciclo"];
$estadomedidor  = $_GET["estadomedidor"];
$altocon        = $_GET["altocon"];
$Desde          = $_GET["Desde"];
$Hasta          = $_GET["Hasta"];
$rutai          = $_GET["rutai"];
$rutaf          = $_GET["rutaf"];
$conpozo        = $_GET["conpozo"];
$periodo        = $clsFunciones->DecFechaLiteral();

?>
<?php CabeceraExcel(2, 14); 

        $h = 4;
        $s = 2;

        if ($altocon != 0) {
            $text = " and altocon=".$altocon;
        }

        if ($Desde != "") {
            $Condicion = " AND c.fechareg BETWEEN CAST ('".$Desde."' AS DATE) AND CAST ( '".$Hasta."' AS DATE ) ";
        }

        if ($codsector != "%") {
            $CSector = " AND c.codsector=".$codsector;
        } else {
            $CSector = "";
        }
        if ($manzanas != "%") {
            $CManzanas = " AND c.codmanzanas= '".$manzanas."'";
        } else {
            $CManzanas = "";
        }
        if ($tiposervicio != "%") {
            $Ctiposervicio = " AND c.codtiposervicio=".$tiposervicio;
        } else {
            $Ctiposervicio = "";
        }
        if ($estadoservicio != "%") {
            $Cestadoservicio = " AND c.codestadoservicio=".$estadoservicio;
        } else {
            $Cestadoservicio = "";
        }
        if ($estadomedidor != "%") {
            $Cestadomedidor = " AND c.codestadomedidor=".$estadomedidor;
        } else {
            $Cestadomedidor = "";
        }

        if ($ciclo != "%") {
            $Cciclo = " AND c.codciclo=".$ciclo;
        } else {
            $Cciclo = "";
        }
        if ($conpozo == 0)
            $conpozo = " AND cx.conpozo =0 ";
        else
            $conpozo = '';

////$ord = " ORDER BY  c.codsector, CAST(c.codmanzanas as integer)";
    //$ord = " ORDER BY c.codsector, c.codrutlecturas, cx.orden_lect";

    $ord = " ORDER BY c.codsuc, c.codsector, CAST(c.codmanzanas AS INTEGER ), CAST(c.nrolote AS INTEGER) ,CAST(c.sublote AS INTEGER)";

    $sql = "SELECT CAST(c.codmanzanas as integer), m.descripcion as manzanas, z.descripcion AS distrito, c.nroinscripcion, c.propietario, 
        (tc.descripcioncorta ||' '|| cl.descripcion ||' '|| c.nrocalle) AS direccion, uni.catetar AS tarifa, t.nomtar AS subcategoria, e.descripcion AS estadoservicio, c.nrodocumento as ruc, 
        CASE WHEN c.codtiposervicio = 0 THEN 'NN' WHEN c.codtiposervicio = 1 THEN 'A/D' WHEN c.codtiposervicio = 2 THEN 'A' ELSE 'D' END AS tiposervicio, c.nromed AS medidor, 
        s.descripcion AS sector, c.codtipodocumento as codtipodocumento, c.nrolote, c.sublote, c.codsector AS codsector, 
        CAST(ca.domestico AS INTEGER ) + CAST(ca.comercial AS INTEGER) + CAST(ca.industrial AS INTEGER) + CAST(ca.social AS INTEGER) + CAST(ca.estatal AS INTEGER) AS unidades_uso, 
        (u.tipo ||' '|| u.descripcion) as urbanizacion, CASE WHEN c.codestadoservicio = 1 THEN 'AC' ELSE 'IN' END AS estado_conexion, 
        to_char(c.codsuc, '00') || '-' || TRIM(to_char(c.codsector, '00')) || '-' || TRIM(to_char(CAST(c.codmanzanas AS INTEGER), '000')) || '-' || 
        TRIM(to_char(CAST(c.nrolote AS INTEGER), '0000')) || '-' || TRIM(c.sublote) AS codcatastro , c.codrutlecturas, c.codrutdistribucion, cx.orden_lect, c.codantiguo, cx.fechainsmed, 
        cx.fechainsagua, cx.fechainsdesague 


    FROM catastro.catastro AS c 
    JOIN catastro.unidadesusocatastro AS uni ON (c.codemp=uni.codemp AND c.codsuc=uni.codsuc AND c.nroinscripcion=uni.nroinscripcion)

    LEFT JOIN catastro.clientes AS ca ON (c.codemp=ca.codemp AND c.codsuc=ca.codsuc AND c.nroinscripcion=ca.nroinscripcion) 

    JOIN public.calles AS cl ON (c.codemp=cl.codemp AND c.codsuc=cl.codsuc AND c.codcalle=cl.codcalle AND c.codzona=cl.codzona) 
    JOIN public.tiposcalle AS tc ON(cl.codtipocalle=tc.codtipocalle) 
    JOIN facturacion.tarifas AS t ON(uni.codemp=t.codemp AND uni.codsuc=t.codsuc AND uni.catetar=t.catetar AND t.estado = 1) 
    JOIN public.estadoservicio AS e ON(c.codestadoservicio=e.codestadoservicio) 
    JOIN public.sectores AS s ON(c.codemp=s.codemp AND c.codsuc=s.codsuc AND c.codsector=s.codsector AND c.codzona=s.codzona) 
    LEFT JOIN catastro.conexiones AS cx ON(c.codemp=cx.codemp AND c.codsuc=cx.codsuc AND c.nroinscripcion=cx.nroinscripcion) 
    JOIN public.manzanas AS m ON( c.codemp = m.codemp AND c.codsuc = m.codsuc AND c.codzona = m.codzona AND c.codsector = m.codsector AND c.codmanzanas = m.codmanzanas) 
    JOIN admin.zonas AS z ON(c.codzona = z.codzona AND c.codsuc = z.codsuc AND c.codemp = z.codemp) 
    JOIN public.rutaslecturas AS rl ON(c.codemp = rl.codemp AND c.codsuc = rl.codsuc AND c.codzona = rl.codzona AND c.codsector = rl.codsector AND c.codrutlecturas = rl.codrutlecturas 
    AND c.codmanzanas = rl.codmanzanas) 
    JOIN public.rutasdistribucion AS rd ON(c.codemp = rd.codemp AND c.codsuc = rd.codsuc AND c.codzona = rd.codzona AND c.codsector = rd.codsector 
    AND c.codrutdistribucion = rd.codrutdistribucion AND c.codmanzanas = rd.codmanzanas) 
    LEFT JOIN catastro.urbanizaciones AS u ON( c.codsuc = u.codsuc AND c.codzona = u.codzona AND c.codurbanizacion = u.codurbanizacion) 

        WHERE c.codemp = 1 AND c.codsuc = ".$codsuc." ".$conpozo.$CSector.$CManzanas.$Ctiposervicio.$Cestadoservicio.$Cciclo.$text.$Cestadomedidor; //$Condicion

       //$sql .= "  ORDER BY  c.codsuc, c.codsector, CAST(c.codmanzanas AS INTEGER ), CAST(c.lote AS INTEGER) ,CAST(c.sublote AS INTEGER) ";

//echo $sql.$ord;


        //echo $sql.$ord;exit;
        //die($sql);

        $count              = 0;
        $consulta           = $conexion->prepare($sql.$ord);
        $consulta->execute(array($codsuc));
        $items              = $consulta->fetchAll();
        $codigo_manzana     = '';
        $codigo_sector      = '';
        //$count = 0;
        $cantidad_activas       = 0;
        $cantidad_no_activas    = 0;

        $cantidad_cone_grupo    = 0;
        $cantidad_no_cone_grupo = 0;

        $cantidad_cone_normales     = 0;
        $cantidad_no_cone_normales  = 0;

        $cantidad_cone_secundarias      = 0;
        $cantidad_no_cone_secundarias   = 0;
        $distrito                       = $items[0]['distrito'];
        $sector                         = $items[0]['sector'];
        $manzana                        = $items[0]['manzanas'];
        ?>
            <table class="ui-widget" border="1" cellspacing="1" id="TbTitulo" width="100%" rules="rows">
            <thead class="ui-widget-header" style="font-size:14px">
            <tr title="Cabecera">
            <th scope="col" colspan="16" align="center" >PADRON DE CONEXIONES CATASTRADAS</th>
            </tr>
            <tr>
            <th colspan="16"><?=$periodo["mes"]." - ".$periodo["anio"] ?> </th>
            </tr> 
            </thead>
            <tbody style="font-size:12px">

        <?php
        foreach ($items as $ind => $row):

            if ($row['codmanzanas'] != $codigo_manzana):
                
                $manzana=$row['manzanas'];
                if (!empty($codigo_manzana)):
                    //$objReporte->pie($cantidad_activas, $cantidad_cone_grupo, $cantidad_cone_normales, $cantidad_cone_secundarias, $cantidad_no_activas, $cantidad_no_cone_grupo, $cantidad_no_cone_normales, $cantidad_no_cone_secundarias);
                    ?>
                    <tr>
                        <th colspan="2">Activas:<?=$cantidad_activas?></th>
                        <th colspan="2"></th>
                        <th colspan="2">Conex. de Grupo:<?=$cantidad_cone_grupo?></th>
                        <th colspan="2"></th>
                        <th colspan="2">Conex. de Normales:<?=$cantidad_cone_normales?></th>
                        <th colspan="2"></th>
                        <th colspan="2">Conex. Secundarias:<?=$cantidad_cone_secundarias?></th>
                        <th colspan="2"></th>
                    </tr>
                    <tr>
                        <th colspan="2">No Activas:<?=$cantidad_no_activas?></th>
                        <th colspan="2"></th>
                        <th colspan="2"><?=$cantidad_no_cone_grupo?></th>
                        <th colspan="2"></th>
                        <th colspan="2"><?=$cantidad_no_cone_normales?></th>
                        <th colspan="2"></th>
                        <th colspan="2"><?=$cantidad_no_cone_secundarias?></th>
                        <th colspan="2"></th>
                    </tr>
                    <?php
                endif;

                 //$objReporte->cabecera1($row['distrito'], $row['sector'], $row['manzanas']);
                ?>
                <tr>
                    <th colspan="4">DISTRITO :<?=$distrito?></th>
                    <th colspan="4">SECTOR :<?=$sector?></th>
                    <th colspan="8">MANZANA :<?=$manzana?></th>
                    
                </tr>
                
                <?php
                if ($row['codsector'] != $codigo_sector):
                    //$objReporte->Ln(4);
                    //$objReporte->cabecera2();
                    ?>

                        <tr>
                        <th width="200" scope="col" class="ui-widget-header">COD. CATASTRAL</th>
                        <th width="200" scope="col" class="ui-widget-header">NRO. INSC.</th>
                        <th width="50" scope="col" class="ui-widget-header">NOMBRES</th>
                        <th width="30" scope="col" class="ui-widget-header">RUC</th>
                        <th width="50" scope="col" class="ui-widget-header">DIRECCION</th>
                        <th width="50" scope="col" class="ui-widget-header">URBANIZACIÓN</th>
                        <th width="50" scope="col" class="ui-widget-header">T. SERV</th>
                        <th width="50" scope="col" class="ui-widget-header">EST. CONEX</th>         
                        <th width="200" scope="col" class="ui-widget-header">SIT. CONEX..</th>
                        <!-- <th width="200" scope="col" class="ui-widget-header">SECT</th>
                        <th width="50" scope="col" class="ui-widget-header">MZA.</th>
                        <th width="30" scope="col" class="ui-widget-header">LTE</th> -->
                        <th width="50" scope="col" class="ui-widget-header">#U. USO</th>
                        <th width="50" scope="col" class="ui-widget-header">SUB CATEG</th>
                        <th width="50" scope="col" class="ui-widget-header">TAR</th>
                        <th width="50" scope="col" class="ui-widget-header"># MED.</th>
                        <th width="50" scope="col" class="ui-widget-header">LECT. SEC.</th>
                        <th width="50" scope="col" class="ui-widget-header">REP. SEC</th>
                        </tr>
                    <?php
                    endif;
            endif;

            if ($row['codtipodocumento'] != '3'):
                $row['ruc'] = '';
            endif;
                ?>
                    <tr>
                    <!--<tr <?=$title ?> <?=$class ?> onclick="SeleccionaId(this);" id="<?=$count ?>" >
                    <td align="left" valign="middle"><?=$count; ?></td>-->
                    <td align="left" valign="middle"><?=($row['codcatastro']); ?></td>
                    <!--<td align="left" valign="middle"><?=($row['codantiguo']); ?></td>-->
                    <td align="left" valign="middle"><?=($row['nroinscripcion']); ?></td>
                    <td align="left" valign="middle"><?=(trim((strtoupper($row["propietario"])))); ?></td>
                    <td align="left" valign="middle"><?=($row['ruc']); ?></td>
                    <td align="left" valign="middle"><?=(strtoupper($row["direccion"])); ?></td>
                    <td align="left" valign="middle"><?=($row['urbanizacion']); ?></td>
                    <td align="left" valign="middle"><?=($row['tiposervicio']); ?></td>
                    <td align="left" valign="middle"><?=($row["estado_conexion"]); ?></td>
                    <td align="left" valign="middle"><?=(strtoupper(substr($row['estadoservicio'], 0, 3))); ?></td>
                    <td align="left" valign="middle"><?=($row["unidades_uso"]); ?></td>
                    <td align="left" valign="middle"><?=($row["subcategoria"]); ?></td>
                    <td align="left" valign="middle"><?=($row["tarifa"]); ?></td>
                    <td align="left" valign="middle"><?=($row['medidor']); ?></td> 
                    <td align="left" valign="middle"><?=($row['codrutlecturas']); ?></td> 
                    <td align="left" valign="middle"><?=($row['codrutdistribucion']); ?></td>  
                    <!--<td align="left" valign="middle"><?=($row['codcatastro']); ?></td> -->             
                </tr>
                <?php
                $codigo_manzana     = $row['codmanzanas'];
                $codigo_sector      = $row['codsector'];
                $cantidad_cone_grupo++;
        endforeach;
        if (!empty($codigo_manzana)):
        //$objReporte->pie($cantidad_activas, $cantidad_cone_grupo, $cantidad_cone_normales, $cantidad_cone_secundarias, $cantidad_no_activas, $cantidad_no_cone_grupo, $cantidad_no_cone_normales, $cantidad_no_cone_secundarias);
                    ?>
                    <tr>
                        <th colspan="2">Activas:<?=$cantidad_activas?></th>
                        <th colspan="2"></th>
                        <th colspan="2">Conex. de Grupo:<?=$cantidad_cone_grupo?></th>
                        <th colspan="2"></th>
                        <th colspan="2">Conex. de Normales:<?=$cantidad_cone_normales?></th>
                        <th colspan="2"></th>
                        <th colspan="2">Conex. Secundarias:<?=$cantidad_cone_secundarias?></th>
                        <th colspan="2"></th>
                    </tr>
                    <tr>
                        <th colspan="2">No Activas:<?=$cantidad_no_activas?></th>
                        <th colspan="2"></th>
                        <th colspan="2"><?=$cantidad_no_cone_grupo?></th>
                        <th colspan="2"></th>
                        <th colspan="2"><?=$cantidad_no_cone_normales?></th>
                        <th colspan="2"></th>
                        <th colspan="2"><?=$cantidad_no_cone_secundarias?></th>
                        <th colspan="2"></th>
                    </tr>
                <?php
        endif;
    ?>
    </tbody>
</table>