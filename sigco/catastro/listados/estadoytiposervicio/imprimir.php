<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $mes,$anio,$meses,$x,$EstadoServicioN;
			
			$h=4;
			$this->SetFont('Arial','B',8);
			$this->Cell(0, $h+1,"RESUMEN DE CONEXIONES POR ESTADO Y TIPO DE SERVICIO",0,1,'C');
			$this->SetFont('Arial','B',8);
			
			
					

				
		
			$this->Cell(0, $h+1,"MES DE REFERENCIA ".$meses[$mes]." - ".$anio,0,1,'C');
			if($EstadoServicioN!="")
				$this->Cell(0, $h+1,"ESTADO DE SERVICIO : ".$EstadoServicioN,0,1,'C');
			$this->Ln(2);
			
			
			
			
		}
		function Leyenda()
		{
			global $x,$conexion;

		}
		//Consumos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,0,$CondEstSer)
		function Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipoFacturacion,$Descripcion,$CondEstSer)
		{
			global $x,$h,$conexion;
			$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
					count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					
					where  c.periodo ='".$periodo."' AND c.catetar='".$Tarifa."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' 
					AND c.tipofacturacion='".$Tipofacturacion."' ".$CondEstSer;
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();
				$this->SetFont('Arial','',6);
				if ($rowMo[1]>0)
				{
					$this->Cell(70, $h,'',0,0,'L');
					$this->Cell(40, $h,$Descripcion,0,0,'L');
					$this->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
					$this->Cell(20, $h,$rowMo[1],0,0,'R');
					$this->Cell(20, $h,number_format($rowMo[2],2)."m".Cubico,0,1,'R');
				}
				//$this->Ln(1);
		}
		//Rangos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,$Descripcion,$CondEstSer)
		function Rangos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$Descripcion,$CondEstSer)
		{
			global $x,$h,$conexion;
			$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
					count(DISTINCT(c.nroinscripcion)) as nrousaurio ,sum(c.consumofact)
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					
					where  c.periodo ='".$periodo."' AND c.catetar='".$Tarifa."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();
			if ($rowMo[1]>0)
			{
				$this->SetFont('Arial','B',6);
				$this->SetTextColor(0,0,0);
				$this->SetX($x);
				$this->Cell(30, $h,'',0,0,'L');
				$this->Cell(70, $h,$Descripcion,0,0,'L');
				$this->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
				$this->Cell(20, $h,$rowMo[1],0,0,'R');
				$this->Cell(20, $h,number_format($rowMo[2],2)."m".Cubico,0,1,'R');


				//CONSUMO LEIDO//
				$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,0,'Consumo Leido',$CondEstSer);
				//CONSUMO LEIDO//
				//CONSUMO PROMEDIADO//
				$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,1,'Consumo Promediado',$CondEstSer);
				//CONSUMO PROMEDIADO//
				//CONSUMO ASIGNADO//
				$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,2,'Consumo Asignado',$CondEstSer);
				//CONSUMO ASIGNADO//
				/*$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo
					FROM facturacion.cabctacorriente c
				  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
				  AND (c.codsuc = dc.codsuc)
				  AND (c.codciclo = dc.codciclo)
				  AND (c.nrofacturacion = dc.nrofacturacion)
				  AND (c.nroinscripcion = dc.nroinscripcion)
				  AND (c.anio = dc.anio)
				  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
				  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
				  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
				  AND c.periodo='".$periodo."' AND c.catetar='".$rowT["catetar"]."'
				  AND c.tipo=0 AND c.tipoestructura=0 
				  AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer."
				  ORDER BY co.ordenrecibo";
				  $ConsultaCo =$conexion->query($Sql);
				foreach($ConsultaCo->fetchAll() as $rowCo)
				{
					$objreporte->SetFont('Arial','',6);
					$objreporte->SetX($x +30);
					$objreporte->Cell(30, $h,'',0,0,'L');
					$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
					$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
						from facturacion.detctacorriente as d
						inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
						and d.codciclo=c.codciclo and 
						d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
						d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
						inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
						and d.codconcepto=cp.codconcepto)
						where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
						c.codsuc=".$codsuc." 
						and c.codciclo=".$ciclo." AND c.tipo=0 AND c.tipoestructura=0
						AND cp.codconcepto='".$rowCo[0]."' 
						AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
					$ConsultaMo =$conexion->query($Sql);
					$rowMo		= $ConsultaMo->fetch();
					$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
							//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
							
				}
				*/
			}

		}
    
    }
	define('Cubico', chr(179));
	
	$ciclo          = $_GET["ciclo"];
	$codsuc         = $_GET["codsuc"];
	$EstadoServicio = $_GET["EstadoServicio"];
	$EstadoServicioN = $_GET["EstadoServicioN"];
	$anio          = $_GET["anio"];
	$mes         = $_GET["mes"];

	$Desde = "1900/".$mes."/01";
	$Dias=date ('t', mktime (0,0,0, $mes, 1, $anio));
	$Hasta =$anio."/".$mes."/".$Dias;
	$Fechas = "  and c.fechareg between '".$Desde."' and '".$Hasta."'"	;

    $x = 20;
    $h=4;
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("P");
	$CondEstSer = "";
	if($EstadoServicio!="")
		$CondEstSer = " AND c.codestadoservicio=".$EstadoServicio." ";
	
	$sqlC  = "select cat.codcategoriatar,cat.descripcion,
			SUM(CASE (c.codtiposervicio) WHEN 1 THEN 1 WHEN 1 THEN 0 END),
			SUM(CASE (c.codtiposervicio) WHEN 2 THEN 1 WHEN 1 THEN 0 END)
				from catastro.clientes as c ";
	$sqlC .= "inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and ";
	$sqlC .= "c.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "where c.codsuc=".$codsuc." and c.codciclo=".$ciclo." ".$CondEstSer.$Fechas;
	$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
	
	$consultaC = $conexion->query($sqlC);
	$itemsC = $consultaC->fetchALl();
	

	$objreporte->SetFont('Arial','',6);
		
	$objreporte->Ln(2);
	$objreporte->SetX($x);
	$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
	$objreporte->Cell(50, $h,"ESTADO DE SERVICIO",1,0,'L');
	$objreporte->Cell(20, $h,utf8_decode("Agua y Desague"),1,0,'L');
	$objreporte->Cell(20, $h,utf8_decode("Sólo Agua"),1,0,'C');
	$objreporte->Cell(20, $h,utf8_decode("Sólo Desague"),1,0,'C');
	$objreporte->Cell(20, $h,"Total",1,1,'C');
	foreach($itemsC as $rowC)
	{
		$objreporte->SetTextColor(255,0,0);
		$objreporte->SetFont('Arial','B',6);
		$objreporte->SetX($x);
		$objreporte->Cell(80, $h,strtoupper($rowC[1]),0,0,'L');
		$objreporte->Cell(20, $h,$rowC[2],0,0,'R');
		$objreporte->Cell(20, $h,$rowC[3],0,0,'R');
		$objreporte->Cell(20, $h,$rowC[4],0,0,'R');
		$objreporte->Cell(20, $h,$rowC[5],0,1,'R');
	
		
		
		$sqlT  = "SELECT catetar,substring(nomtar,1,3) as nomtar
		from facturacion.tarifas  
		where codemp=1 AND codsuc=:codsuc  ";
		$sqlT .= " and codcategoriatar=:codcategoriatar  ";
		$sqlT .= "order by catetar";
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			$Sql ="SELECT c.codestadoservicio,e.descripcion,
					SUM(CASE (c.codtiposervicio) WHEN 1 THEN 1 WHEN 1 THEN 0 END),
					SUM(CASE (c.codtiposervicio) WHEN 2 THEN 1 WHEN 1 THEN 0 END),
					SUM(CASE (c.codtiposervicio) WHEN 3 THEN 1 WHEN 1 THEN 0 END),
					count(c.nroinscripcion)
					FROM catastro.clientes as c 
					inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
					inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and 
					c.catetar=tar.catetar) 
					inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar)
					WHERE c.codemp=1 and c.codsuc=".$codsuc." and c.codciclo =".$ciclo." 
					AND cat.codcategoriatar=".$rowC[0]." AND c.catetar=".$rowT[0]."".$CondEstSer.$Fechas."
					GROUP BY c.codestadoservicio,e.descripcion 
					ORDER BY c.codestadoservicio";
			$ConsultaEs = $conexion->query($Sql);
			$objreporte->SetTextColor(0,0,0);
			$objreporte->SetFont('Arial','',6);
			foreach ($ConsultaEs as $rowEs) 
			{
				$objreporte->SetX($x+30);
				$objreporte->Cell(50, $h,strtoupper($rowEs[1]),0,0,'L');
				$objreporte->Cell(20, $h,$rowEs[2],0,0,'R');
				$objreporte->Cell(20, $h,$rowEs[3],0,0,'R');
				$objreporte->Cell(20, $h,$rowEs[4],0,0,'R');
				$objreporte->Cell(20, $h,$rowEs[5],0,1,'R');
				//COM MEDIDOR
				$Sql ="SELECT c.codestadoservicio,e.descripcion,
					SUM(CASE (c.codtiposervicio) WHEN 1 THEN 1 WHEN 1 THEN 0 END),
					SUM(CASE (c.codtiposervicio) WHEN 2 THEN 1 WHEN 1 THEN 0 END),
					SUM(CASE (c.codtiposervicio) WHEN 3 THEN 1 WHEN 1 THEN 0 END),
					count(c.nroinscripcion)
					FROM catastro.clientes as c 
					inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
					inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and 
					c.catetar=tar.catetar) 
					inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar)
					WHERE c.codemp=1 and c.codsuc=".$codsuc." and c.codciclo =".$ciclo." 
					AND cat.codcategoriatar=".$rowC[0]." AND c.catetar=".$rowT[0]."
					 AND c.codestadoservicio=".$rowEs[0]." ".$Fechas." AND ltrim(rtrim(c.nromed))<>''
					GROUP BY c.codestadoservicio,e.descripcion ";
			//die($Sql);
				$ConsultaMe = $conexion->query($Sql);
				$rowMe = $ConsultaMe->fetch();
				if($rowMe[5]>0)
				{
					$objreporte->SetX($x+50);
					$objreporte->Cell(30, $h,'CON MEDIDOR',0,0,'L');
					$objreporte->Cell(20, $h,$rowMe[2],0,0,'R');
					$objreporte->Cell(20, $h,$rowMe[3],0,0,'R');
					$objreporte->Cell(20, $h,$rowMe[4],0,0,'R');
					$objreporte->Cell(20, $h,$rowMe[5],0,1,'R');	
				}
				//SIN MEDIDOR
				$Sql ="SELECT c.codestadoservicio,e.descripcion,
					SUM(CASE (c.codtiposervicio) WHEN 1 THEN 1 WHEN 1 THEN 0 END),
					SUM(CASE (c.codtiposervicio) WHEN 2 THEN 1 WHEN 1 THEN 0 END),
					SUM(CASE (c.codtiposervicio) WHEN 3 THEN 1 WHEN 1 THEN 0 END),
					count(c.nroinscripcion)
					FROM catastro.clientes as c 
					inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
					inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and 
					c.catetar=tar.catetar) 
					inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar)
					WHERE c.codemp=1 and c.codsuc=".$codsuc." and c.codciclo =".$ciclo." 
					AND cat.codcategoriatar=".$rowC[0]." AND c.catetar=".$rowT[0]."
					 AND c.codestadoservicio=".$rowEs[0]." ".$Fechas." AND ltrim(rtrim(c.nromed))=''
					GROUP BY c.codestadoservicio,e.descripcion ";
				$ConsultaMe = $conexion->query($Sql);
				$rowMe = $ConsultaMe->fetch();
				if($rowMe[5]>0)
				{
					$objreporte->SetX($x+50);
					$objreporte->Cell(30, $h,'SIN MEDIDOR',0,0,'L');
					$objreporte->Cell(20, $h,$rowMe[2],0,0,'R');
					$objreporte->Cell(20, $h,$rowMe[3],0,0,'R');
					$objreporte->Cell(20, $h,$rowMe[4],0,0,'R');
					$objreporte->Cell(20, $h,$rowMe[5],0,1,'R');
				}

			}

				
		}
		
		

		
	

	}
	
	$Sql  = "select 
			SUM(CASE (c.codtiposervicio) WHEN 1 THEN 1 WHEN 1 THEN 0 END),
			SUM(CASE (c.codtiposervicio) WHEN 2 THEN 1 WHEN 1 THEN 0 END),
			SUM(CASE (c.codtiposervicio) WHEN 3 THEN 1 WHEN 1 THEN 0 END),
			count(c.nroinscripcion)
				from catastro.clientes as c ";
	$Sql .= "inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and ";
	$Sql .= "c.catetar=tar.catetar) ";
	$Sql .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$Sql .= "where c.codsuc=".$codsuc." and c.codciclo=".$ciclo." ".$CondEstSer.$Fechas;;
	
	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();
	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(160, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(80, $h,'TOTAL ',0,0,'l');
	$objreporte->Cell(20, $h,$rowMo[0],0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[1],0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[2],0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[3],0,1,'R');

	
	$objreporte->Output();	
	
?>