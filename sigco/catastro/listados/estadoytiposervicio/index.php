<?php 
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "ESTADOS Y TIPOS DE SERVICIOS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>

jQuery(function($)
	{ 
		$( "#DivTipos" ).buttonset();
	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc 	= <?=$codsuc?>
	
	function ValidarForm(Op)
	{
		var ciclos  = $("#ciclo").val()
		
		if(ciclos==0)
		{
			alert("Seleccione el Ciclo")
			return false
		}
		
		var EstadoServicio=''
		if($('#Todos').attr("checked")!="checked")
		{
			if($("#estadoservicio").val()==0)
			{
				alert('Seleccione Estado de Servicio')
				return false
			}

			EstadoServicio = '&EstadoServicio='+$("#estadoservicio").val()+'&EstadoServicioN='+$("#estadoservicio option:selected").html()
		}

		url = "imprimir.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		
		
		url +=EstadoServicio
		AbrirPopupImpresion(url, 800, 600)
		
		return false
	}	
	function Cancelar()
	{
		location.href = '<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
function TodosEstados()
	{
		if($('#Todos').attr("checked")=="checked")
		{
			
			
			$('#estadoservicio').attr('disabled', 'disabled');
		
			
		}
		else
		{
		
			$('#estadoservicio').attr('disabled', false);
			
		}
	
	}
function cargar_anio_drop(obj)
	{
		$.ajax({
			 url:'../../../../ajax/anio_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&codciclo='+obj+'&condicion=1',
			 success:function(datos){
				$("#div_anio").html(datos)
			 }
		}) 
	}
	function cargar_mes(ciclo,suc,anio)
	{
		$.ajax({
			 url:'../../../../ajax/mes_drop.php',
			 type:'POST',
			 async:true,
			 data:'codciclo='+ciclo+'&codsuc='+suc+'&anio='+anio,
			 success:function(datos){
				$("#div_meses").html(datos)
			 }
		}) 
	}
function quitar_disabled(obj,input)
{
	if(obj.checked)
	{
		$("#"+input).attr("disabled",true)
	}else{
		$("#"+input).attr("disabled",false)
	}
}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>

    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="90" align="right">Sucursal</td>
				    <td width="30" align="center">:</td>
				    <td colspan="4">
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </td>
				    
				  </tr>
				  <tr>
				
				    <td width="90" align="right">Ciclo</td>
				    <td align="center">:</td>
				    <td colspan="4">
                    	<? $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio_drop(this.value);'"); ?>
                    </td>
				  </tr>
				  <tr>
              <td align="right">A&ntilde;o</td>
              <td align="center">:</td>
              <td colspan="4">
                <div id="div_anio">
                  <? $objMantenimiento->drop_anio($codsuc,0); ?>
                  </div>
              </td>
              </tr>
            
            <tr>
              <td align="right">Meses</td>
              <td align="center">:</td>
              <td colspan="4">
                <div id="div_meses">
                  <? $objMantenimiento->drop_mes($codsuc,0,0); ?>
                  </div>
              </td>
              </tr>
			      <tr>
                        <td align="right">Est. de Servicio</td>
                        <td align="center">:</td>
                        <td width="220"><? $objMantenimiento->drop_estado_servicio($items["codestadoservicio"],""); ?></td>
                        <td width="10">&nbsp;</td>
                        <td width="30" align="center"><input type="checkbox" id="Todos" onclick="TodosEstados();"></td>
                        <td>
                        	Todos
                        </td>
                      </tr>
			      <tr>
       <td colspan="6" align="center"> <input type="button" onclick="return ValidarForm();" value="Generar" id=""></td>
	</tr>
						  
			
				</table>
			</fieldset>
		</td>
	</tr>
 
	 </tbody>

    </table>
 </form>
</div>
<?php CuerpoInferior(); ?>