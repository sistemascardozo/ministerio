<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "RPT. X TIPO DE USUARIO";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc = $_SESSION['IdSucursal'];

	$objMantenimiento = new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>

jQuery(function($)
	{ 
		$( "#DivTipos" ).buttonset();
	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc 	= <?=$codsuc?>
	
	function ValidarForm(Op)
	{
		var ciclos  = $("#ciclo").val()
		
		if(ciclos==0)
		{
			alert("Seleccione el Ciclo")
			return false
		}
		
		var tipousuario=''
		if($('#Todos').attr("checked")!="checked")
		{
			if($("#tipousuario").val()==0)
			{
				alert('Seleccione el Tipo de Usuario')
				return false
			}

			tipousuario = '&tipousuario='+$("#tipousuario").val()+'&tipousuarioN='+$("#tipousuario option:selected").html()
		}

		url = "imprimir.php?ciclo="+ciclos+"&codsuc="+codsuc;
		
		
		url +=tipousuario
		AbrirPopupImpresion(url,800,600)
		
		return false
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
function TodosEstados()
	{
		if($('#Todos').attr("checked")=="checked")
		{
			
			
			$('#codtipousuario').attr('disabled', 'disabled');
		
			
		}
		else
		{
		
			$('#codtipousuario').attr('disabled', false);
			
		}
	
	}

function quitar_disabled(obj,input)
{
	if(obj.checked)
	{
		$("#"+input).attr("disabled",true)
	}else{
		$("#"+input).attr("disabled",false)
	}
}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="550" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>

    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="100" align="right">Sucursal</td>
				    <td width="10" align="center">:</td>
				    <td colspan="4">
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </td>
				    
				  </tr>
				  <tr>
				
				    <td width="13%" align="right">Ciclo</td>
				    <td width="3%" align="center">:</td>
				    <td colspan="4">
                    	<? $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio_drop(this.value);'"); ?>
                    </td>
				  </tr>
				            
           
			      <tr>
                        <td align="right">T. Usuario</td>
                        <td align="center">:</td>
                        <td width="200"><?php $objMantenimiento->drop_tipo_usuario($items["codtipousuario"],''); ?></td>
                        <td width="5" align="right">&nbsp;</td>
                    <td width="10" align="center"><input type="checkbox" id="Todos" onclick="TodosEstados();"></td>
                        <td>
                        	Todos
                        </td>
                      </tr>
			      <tr>
			        <td align="right">&nbsp;</td>
			        <td align="center">&nbsp;</td>
			        <td>&nbsp;</td>
			        <td align="right">&nbsp;</td>
			        <td align="center">&nbsp;</td>
			        <td>&nbsp;</td>
		          </tr>
			      <tr>
       <td colspan="6" align="center"> <input type="button" onclick="return ValidarForm();" value="Generar" id=""></td>
	</tr>
						  
			
				</table>
			</fieldset>
		</td>
	</tr>
 
	 </tbody>

    </table>
 </form>
</div>
<?php CuerpoInferior(); ?>