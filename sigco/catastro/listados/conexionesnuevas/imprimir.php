<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses,$Desde,$Hasta;
			
			$periodo = $this->DecFechaLiteral();
			
			$this->SetFont('Arial','B',10);
			
			$h = 4;
			$this->Cell(278, $h+2,"CONEXIONES NUEVAS",0,1,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(278, $h+2,'Fecha de Ingreso desde: '.$Desde." hasta: ".$Hasta,0,1,'C');
			
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(30,15,63,45,10,20,10,15,15,8,8,8,30,));
			$this->SetAligns(array("C","C","C","C","C","C","C","C","C","C","C","C","C"));
			$this->Row(array("COD. CATASTRAL",
							 "NRO. INSC.",
							 "CLIENTE",
							 "DIRECCION",
							 "CAT",
							 "EST",
							 "TS",
							 "NRO. MED.",
							 "SECT",
							 "MZA",
							 "LTE",
							 "SLTE",
							 "Fecha Reg.",
							 ));

		}
		function contenido($codsector, $tiposervicio, $estadoservicio, $codsuc, $ciclo, $altocon, $Desde, $Hasta)
		{
			global $conexion;
			
			$h = 4;
			$s = 2;
			
			if($altocon!=0)
			{
				$text = " and altocon=".$altocon;
			}
			if($Desde!="")
			{
				$Condicion=" AND c.fechareg BETWEEN CAST ('".$Desde."' AS DATE) AND CAST ( '".$Hasta."' AS DATE ) ";
			}
			$sql = "select c.nroinscripcion,c.propietario,tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,t.nomtar,
					e.descripcion as estadoservicio,c.codtiposervicio,c.nromed,s.descripcion as sector,c.codmanzanas,c.lote,
					c.sublote,c.domestico,c.comercial,c.industrial,c.social,c.estatal,".$this->getCodCatastral("c.").",
					cx.altocon,c.fechareg
					from catastro.clientes as c 
					INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
					inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
					inner join facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
					inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
					inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
					inner join catastro.conexiones as cx on(c.codemp=cx.codemp and c.codsuc=cx.codsuc and c.nroinscripcion=cx.nroinscripcion)
					where c.codemp=1 and c.codsuc=? and CAST(c.codsector AS CHAR) LIKE ? 
					and CAST(c.codtiposervicio AS CHAR) LIKE ? and CAST(c.codestadoservicio AS  CHAR) LIKE ? 
					and CAST(c.codciclo AS CHAR) LIKE ? ".$text.$Condicion." ORDER BY c.fechareg";
			
			$codsector 		= "%".$codsector."%";
			$tiposervicio	= "%".$tiposervicio."%";
			$estadoservicio	= "%".$estadoservicio."%";
			$ciclo			= "%".$ciclo."%";
			$count			= 0;
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc, $codsector, $tiposervicio, $estadoservicio, $ciclo));
			$items = $consulta->fetchAll();
	
			foreach($items as $row)
			{
				$count++;
				$codcatastro = $row["codcatastro"];
				
				if($row["codtiposervicio"]==1){$ts="Ag/Des";}
				if($row["codtiposervicio"]==2){$ts="Ag";}
				if($row["codtiposervicio"]==3){$ts="Des";}
				
				$this->SetWidths(array(30,15,63,45,10,20,10,15,15,8,8,8,30,));
			$this->SetAligns(array("C","C","L","L","C","C","C","C","C","C","C","C","C"));
			$this->Row(array($codcatastro,
							     $row["nroinscripcion"],
							 	 trim(utf8_decode(strtoupper($row["propietario"]))),
							     strtoupper($row["descripcioncorta"]." ".$row["calle"]." ".$row["nrocalle"]),
							 	 substr(strtoupper($row["nomtar"]),0,3),
							 	 strtoupper($row["estadoservicio"]),
							 	 $ts,
							 	 $row["nromed"],
							 	 strtoupper($row["sector"]),
							     $row["codmanzanas"],
							     $row["lote"],
							 	 $row["sublote"],
							     $this->DecFecha($row["fechareg"])));
				
				
			}
			
			$this->Ln(3);
			$this->Cell(278, $h+2,"Nuevos Usuario Registrados ".$count,'TB',1,'C');
			$this->SetFont('Arial','B',8);
			$this->Ln(3);
			$this->Cell(0, $h+2,"RESUMEN POR ESTADOS DE SERVICIO",0,1,'L');
			$Sql ="select c.codestadoservicio,e.descripcion,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from catastro.clientes as c 
					inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
					
					WHERE c.codemp=1 and c.codsuc=".$codsuc." and CAST(c.codsector AS CHAR) LIKE '".$codsector."' 
					and CAST(c.codtiposervicio AS CHAR) LIKE '".$tiposervicio."' and CAST(c.codestadoservicio AS CHAR) LIKE '".$estadoservicio."' 
					and CAST(c.codciclo AS CHAR) LIKE '".$ciclo."' ".$text.$Condicion."
					GROUP BY c.codestadoservicio,e.descripcion ";
			//die($Sql);
			$Consulta = $conexion->query($Sql);
			$this->SetFont('Arial','',8);
			foreach ($Consulta->fetchAll() as $row)
			{
				$this->Cell(30, $h+2,$row[1],0,0,'R');
				$this->Cell(5, $h+2,':',0,0,'L');
				$this->Cell(30, $h+2,$row[2],0,1,'R');
			}
			$this->SetFont('Arial','B',8);
			$this->Ln(3);
			$this->Cell(0, $h+2,"RESUMEN POR CATEGORIAS",0,1,'L');
			$Sql ="select c.catetar,t.nomtar,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from catastro.clientes as c 
					inner join facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
					WHERE c.codemp=1 and c.codsuc=".$codsuc." and CAST(c.codsector AS CHAR) LIKE '".$codsector."' 
					and CAST(c.codtiposervicio AS CHAR) LIKE '".$tiposervicio."' and CAST(c.codestadoservicio AS CHAR) LIKE '".$estadoservicio."' 
					and CAST(c.codciclo AS CHAR) LIKE '".$ciclo."' ".$text.$Condicion."
					GROUP BY c.catetar,t.nomtar ";
			$Consulta = $conexion->query($Sql);
			$this->SetFont('Arial','',8);
			foreach ($Consulta->fetchAll() as $row)
			{
				$this->Cell(30, $h+2,$row[1],0,0,'R');
				$this->Cell(5, $h+2,':',0,0,'L');
				$this->Cell(30, $h+2,$row[2],0,1,'R');
			}

			
		}
	}
	
	$codsuc         = $_GET["codsuc"];
	$codsector      = $_GET["sector"];
	$tiposervicio   = $_GET["tiposervicio"];
	$estadoservicio = $_GET["estadoservicio"];
	$ciclo          = $_GET["ciclo"];
	$altocon        = $_GET["altocon"];
	$Desde          = $_GET["Desde"];
	$Hasta          = $_GET["Hasta"];
	
	$objReporte = new clsPadron("L");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsector, $tiposervicio, $estadoservicio, $codsuc, $ciclo, $altocon, $Desde, $Hasta);
	$objReporte->Output();
	
?>