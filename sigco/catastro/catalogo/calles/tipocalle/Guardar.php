<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipocalle		= $_POST["codtipocalle"];
	$descripcioncorta	= strtoupper($_POST["descripcioncorta"]);
	$descripcionlarga	= strtoupper($_POST["descripcionlarga"]);
	$estareg			= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tiposcalle","0","0");
			$codtipocalle = $id[0];
			$sql = "insert into public.tiposcalle(codtipocalle,descripcioncorta,descripcionlarga,estareg) values(:codtipocalle,:descripcioncorta,:descripcionlarga,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipocalle"=>$codtipocalle,
						   ":descripcioncorta"=>$descripcioncorta,
						   ":descripcionlarga"=>$descripcionlarga,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tiposcalle set descripcioncorta=:descripcioncorta,descripcionlarga=:descripcionlarga, estareg=:estareg 
			where codtipocalle=:codtipocalle";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipocalle"=>$codtipocalle,
						   ":descripcioncorta"=>$descripcioncorta,
						   ":descripcionlarga"=>$descripcionlarga,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tiposcalle set estareg=:estareg
				where codtipocalle=:codtipocalle";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipocalle"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
