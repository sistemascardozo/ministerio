<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../objetos/clsDrop.php");

$Op = $_GET["Op"];
$CodCalle = isset($_POST["Id"]) ? $_POST["Id"] : '';
$codzona = isset($_POST["codzona"]) ? $_POST["codzona"] : '';
$guardar = "Op=".$Op;
$codsuc = $_SESSION['IdSucursal'];

$objMantenimiento = new clsDrop();

if ($CodCalle != '') {
    $sql = "select * from 	public.calles where codcalle=? and codsuc=? AND codzona=?";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($CodCalle, $codsuc, $codzona));
    $row = $consulta->fetch();
    $guardar = $guardar."&CodCalle2=".$CodCalle."&codzona2=".$codzona;
}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<style type="text/css">
    .IdSector {display: none;}
</style>

<script type="text/javascript">
    $(document).ready(function () {
        CZonas()
    });
    function CZonas()
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir']; ?>ajax/cZonas.php',
            type: 'POST',
            async: true,
            data: 'codsuc=<?=$codsuc ?>&codzona=<?=$row["codzona"] ?>',
            success: function (data)
            {

                $("#codzona").html(data)
            }
        })
    }
    function AgregarItem()
    {
        var IdSector = $("#codsector").val();
        var Sector = $("#codsector option:selected").html();

        var j = parseInt($("#TbIndex tbody tr").length)

        for (var i1 = 1; i1 <= j; i1++)
        {
            Codigo = $("#TbIndex tbody tr#"+i1+" label.IdSector").text();

            if (Codigo == IdSector)
            {
                Msj('#codsector', 'El Sector '+Sector+' ya esta Agregado')
                return false;
            }
        }

        if (IdSector != "0")
        {

            var i = j+1;
            var tr = '<tr id="'+i+'" onclick="SeleccionaId(this);">';
            tr += '<td align="center" ><label class="Item">'+i+'</label></td>';
            tr += '<td align="center" ><label class="IdSector">'+IdSector+'</label><label class="Sector">'+Sector+'</label></td>';
            tr += '<td align="center" ><a href="javascript:QuitaItem('+i+')" class="Del" >';
            tr += '<span class="icono-icon-x" title="Quitar Sector"><img src="../../../../../images/iconos/cancel.png" width="16" height="16" /></span> ';
            tr += '</a></td></tr>';

            $("#TbIndex tbody").append(tr);


        }
        else
        {
            Msj('#codsector', 'Seleccione un Sector')
            return false;
        }

    }
    function QuitaItem(tr)
    {
        var id = parseInt($("#TbIndex tbody tr").length);


        $("#TbIndex tbody tr#"+tr).remove();
        var nextfila = tr+1;
        var j;
        var IdMat = "";
        for (var i = nextfila; i <= id; i++)
        {
            j = i - 1;//alert(j);
            $("#TbIndex tbody tr#"+i+" label.Item").text(j);
            $("#TbIndex tbody tr#"+i+" a.Del").attr("href", "javascript:QuitaItem("+j+")");
            $("#TbIndex tbody tr#"+i).attr("id", j);

        }

    }

    function ValidarForm(Op)
    {
        if ($("#codzona").val() == 0)
        {
            Msj($("#codzona"), "Seleccione el Zona")
            return false
        }
        if ($("#tipo").val() == 0)
        {
            Msj($("#tipo"), "Seleccione el Tipo")
            return false
        }
        if ($("#Nombre").val() == '')
        {
            Msj($("#Nombre"), 'La Descripcion del Calles no puede ser NULO');
            return false;
        }
        GenerarSave(Op)

    }
    
    function GenerarSave(Op)
    {

        var id = parseInt($("#TbIndex tbody tr").length)
        var tr = '';
        var IdSector = '';
        for (var i = 1; i <= id; i++)
        {

            IdSector = $("#TbIndex tbody tr#"+i+" label.IdSector").text()
            tr += '<input type="hidden" name="IdSector_'+i+'"  value="'+IdSector+'"/>';
        }
        tr += '<input type="hidden" name="NroItems" id="Items"  value="'+id+'"/>';
        $("#DivSave").html(tr)




        GuardarP(Op)

    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr><td>&nbsp;</td>
                    <td width="30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="TitDetalle">Id</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <input name="codcalle" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $CodCalle; ?>" class="inputtext"/></td>
                </tr>
                <tr>
                    <td class="TitDetalle">Zona</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <select name="codzona" id="codzona" style="width:220px">
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="TitDetalle">Categoria</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <select id="categoria" name="categoria" class="select" style="width:220px">
                            <option value="0">--Seleccione la Categoria--</option>
                            <?php
                            $sqlC = "select * from public.categoriacalles";

                            $consultaC = $conexion->prepare($sqlC);
                            $consultaC->execute();
                            $itemsC = $consultaC->fetchAll();

                            foreach ($itemsC as $rowC) {
                                $selected = "";
                                if ($row["codcategoriacalle"] == $rowC["codcategoriacalle"]) {
                                    $selected = "selected='selected'";
                                }
                                echo "<option value='".$rowC["codcategoriacalle"]."' ".$selected." >".strtoupper($rowC["descripcion"])."</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Tipo Calle</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <select id="tipo" name="tipo" class="select" style="width:220px">
                            <option value="0">--Seleccione el Tipo--</option>
                            <?php
                            $sqlT = "select * from public.tiposcalle";

                            $consultaT = $conexion->prepare($sqlT);
                            $consultaT->execute();
                            $itemsT = $consultaT->fetchAll();

                            foreach ($itemsT as $rowT) {
                                $selected = "";
                                if ($row["codtipocalle"] == $rowT["codtipocalle"]) {
                                    $selected = "selected='selected'";
                                }
                                echo "<option value='".$rowT["codtipocalle"]."' ".$selected." >".strtoupper($rowT["descripcionlarga"])."</option>";
                            }
                            ?>
                        </select></td>
                </tr>
                <tr>
                    <td class="TitDetalle">Descripcion</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <input class="inputtext" name="descripcion" type="text" id="Nombre" maxlength="200" value="<?php echo $row["descripcion"]; ?>" style="width:400px;"/>	    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Estado</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <? include("../../../../../include/estareg.php"); ?></td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center" style=" padding-left:5px; padding-right:5px;">
                        <fieldset>
                            <legend class="ui-state-default ui-corner-all">Sectores</legend>
                            <table width="100%" >
                                <tr class="TrAddItem">
                                    <td colspan="2">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" rules="all" >
                                            <tr>
                                                <td align="center">
                                                    <?php echo $objMantenimiento->drop_sectores2($codsuc, $row["codsector"]); ?> &nbsp;&nbsp;
                                                    <span class="icono-icon-add" title="Agregar Producto" onclick="AgregarItem()"></span>
                                                    <div id="DivSave"></div>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <div style="height:auto; overflow:auto;align:center" align="center" id="DivDetalle">
                                            <table width="300" border="0" align="center" cellspacing="1" class="ui-widget" id="TbIndex">
                                                <thead class="ui-widget-header" >
                                                    <tr >
                                                        <th width="50" align="center" scope="col">Item</th>
                                                        <th align="center" scope="col">Sector</th>
                                                        <th width="30" align="center" scope="col">&nbsp;</th>
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                <?php
                                                if ($CodCalle != '') {
                                                    $Sql = "SELECT s.codsector,s.descripcion as sector
                                                        FROM  public.sectores s
                                                        INNER JOIN public.sectores_calles sc ON (s.codemp = sc.codemp)
                                                        AND (s.codsuc = sc.codsuc) AND (s.codsector = sc.codsector) AND (s.codzona = sc.codzona)";
                                                    $Sql .=" WHERE sc.codemp = 1 AND sc.codsuc=".$codsuc." AND sc.codzona=".$codzona." AND sc.codcalle = ".$CodCalle."";
                                                    $Sql.=" ORDER BY s.codsector asc  ";

                                                    $i = 0;
                                                    $tr = "";
                                                    $SubTotal = 0;
                                                    foreach ($conexion->query($Sql)->fetchAll() as $rowD) {
                                                        $i ++;
                                                        ?>
                                                            <tr id="<?php echo $i; ?>" onclick="SeleccionaId(this);">
                                                                <td align="center" ><label class="Item"><?php echo $i; ?></label></td>
                                                                <td align="center" >
                                                                    <label class="IdSector"><?php echo $rowD['codsector']; ?></label><label class="Sector"><?php echo $rowD['sector']; ?></label>
                                                                </td>
                                                                <td align="center" ><a href="javascript:QuitaItem(<?php echo $i; ?>)" class="Del" >
                                                                        <span class="icono-icon-x" title="Quitar Sector"><img src="../../../../../images/iconos/cancel.png" width="16" height="16" /></span>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot class="ui-widget-header">
                                                    <tr>
                                                        <td colspan="3" >&nbsp;</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    $("#Nombre").focus();
</script>
<?php
$est = isset($row["estareg"])?$row["estareg"]:1;
include("../../../../../admin/validaciones/estareg.php");
?>
