<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");
	
	$TituloVentana = "CALLES";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
	$codsuc = $_SESSION['IdSucursal'];
	
	$FormatoGrilla = array();
	
	$Sql = "SELECT Call.codcalle, Call.descripcion, Sect.descripcion, Cate.descripcion, tipc.descripcioncorta, e.descripcion, Call.estareg, Sect.codzona ";
	$Sql .= "FROM public.calles Call ";
	$Sql .= " INNER JOIN admin.zonas Sect ON (Call.codzona = Sect.codzona) AND (Call.codemp = Sect.codemp) AND (Call.codsuc = Sect.codsuc) ";
	$Sql .= " INNER JOIN public.categoriacalles Cate ON (Call.codcategoriacalle = Cate.codcategoriacalle) ";
	$Sql .= " INNER JOIN public.tiposcalle tipc ON (Call.codtipocalle = tipc.codtipocalle) ";
	$Sql .= " INNER JOIN public.estadoreg e ON (Call.estareg = e.id)";
	
	$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
	$FormatoGrilla[1] = array('1' => 'Call.codcalle', '2' => 'Sect.descripcion', '3' => 'Cate.descripcion', '4' => 'tipc.descripcioncorta', '5' => 'Call.descripcion');          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1' => 'C&oacute;digo', 'T2' => 'Descripci&oacute;n', 'T3' => 'Zona', 'T4' => 'Categoria', 'T5' => 'Tipo Calle', 'T6' => 'Estado');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'center', 'A5' => 'center', 'A6' => 'center');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1' => '60', 'W5' => '70', 'W6' => '90');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = " AND (Call.codemp = 1 AND Call.codsuc = ".$codsuc.") ORDER BY Call.codcalle";                                   //Orden de la Consulta
	$FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
							'NB' => '3', //Número de Botones a agregar
							'BtnId1' => 'BtnModificar', //Nombre del Boton
							'BtnI1' => 'modificar.png', //Imagen a mostrar
							'Btn1' => 'Editar', //Titulo del Botón
							'BtnF1' => 'onclick="Modificar(this);"', //Eventos del Botón
							'BtnCI1' => '7', //Item a Comparar
							'BtnCV1' => '1', //Valor de comparación
							'BtnId2' => 'BtnEliminar',
							'BtnI2' => 'eliminar.png',
							'Btn2' => 'Eliminar',
							'BtnF2' => 'onclick="Anular(this)"',
							'BtnCI2' => '7', //campo 3
							'BtnCV2' => '1', //igua a 1
							'BtnId3' => 'BtnRestablecer', //y aparece este boton
							'BtnI3' => 'restablecer.png',
							'Btn3' => 'Restablecer',
							'BtnF3' => 'onclick="Activar(this)"',
							'BtnCI3' => '7',
							'BtnCV3' => '0');
							
	$FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 1), array('Name' => 'codzona', 'Col' => 8)); //DATOS ADICIONALES        
	$FormatoGrilla[11] = 6; //FILAS VISIBLES
	
	$_SESSION['Formato'] = $FormatoGrilla;
	
	Cabecera('', $FormatoGrilla[7], 700, 500);
	Pie();
?>
<script type="text/javascript">
    function Modificar(obj)
    {
        $("#form1").remove();
        var Id = $(obj).parent().parent().data('id')
        var codzona = $(obj).parent().parent().data('codzona')
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span class='icono-icon-loading'></span></center>");
        $.ajax({
            url: 'mantenimiento.php?Op=1',
            type: 'POST',
            async: true,
            data: 'Id=' + Id + '&codsuc=<?= $codsuc ?>' + '&codzona=' + codzona,
            success: function(data) {
                $("#DivModificar").html(data);
            }
        });
    }

    function Anular(obj)
    {
        $("#form1").remove();
        $("#DivEliminacion").html('');
        var Id = $(obj).parent().parent().data('id')
        var codzona = $(obj).parent().parent().data('codzona')
        var html = '<form id="form1" name="form1">';
        html += '<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
        html += '<input type="hidden" name="codzona" value="' + codzona + '" />';
        html += '<input type="hidden" name="estareg" id="estareg" value="0" />';
        html += '</form>'
        $("#DivEliminacion").html(html);
        $("#ConfirmaEliminacion").dialog("open");
    }
    function Activar(obj)
    {
        $("#form1").remove();
        var Id = $(obj).parent().parent().data('id')
        var codzona = $(obj).parent().parent().data('codzona')
        var html = '<form id="form1" name="form1">';
        html += '<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
        html += '<input type="hidden" name="codzona" value="' + codzona + '" />';
        html += '<input type="hidden" name="estareg" id="estareg" value="1" />';
        html += '</form>'
        $("#DivRestaurar").html(html);
        $("#ConfirmaRestauracion").dialog("open");
    }
</script>
<?php CuerpoInferior(); ?>