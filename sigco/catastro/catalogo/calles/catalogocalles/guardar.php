<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
		
    include('../../../../../objetos/clsFunciones.php');

	$Op         = $_GET['Op'];
    $conexion->beginTransaction();
	$objFunciones = new clsFunciones();
	$codemp			= 1;
	$codsuc 		= $_SESSION['IdSucursal'];
	$codcalle		= $_POST["codcalle"];
	$codzona		= $_POST["codzona"];
	$categoria		= $_POST["categoria"];
	$tipo			= $_POST["tipo"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	

	switch ($Op) 
	{
		case 0:
			$id   		= $objFunciones->setCorrelativos("calles",$codsuc ,"0");
			$codcalle 	= $id[0];
			$sql = "insert into public.calles(codcalle,codtipocalle,codcategoriacalle,codzona,descripcion,estareg,codemp,codsuc) 
				values(:codcalle,:codtipocalle,:codcategoriacalle,:codzona,:descripcion,:estareg,:codemp,:codsuc)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codcalle"=>$codcalle,
						   ":codtipocalle"=>$tipo,
						   ":codcategoriacalle"=>$categoria,
						   ":codzona"=>$codzona,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc));
		break;
		case 1:
			$sql = "update public.calles set codtipocalle=:codtipocalle,codcategoriacalle=:codcategoriacalle,
				descripcion=:descripcion,estareg=:estareg  where codemp=:codemp and codsuc=:codsuc and codcalle=:codcalle AND codzona=:codzona";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codcalle"=>$codcalle,
						   ":codtipocalle"=>$tipo,
						   ":codcategoriacalle"=>$categoria,
						   ":codzona"=>$codzona,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc));
			$sql = "DELETE FROM public.sectores_calles 
					WHERE codemp=:codemp and codsuc=:codsuc and codcalle=:codcalle AND codzona=:codzona";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codcalle"=>$codcalle,
						   ":codzona"=>$codzona,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc));
		break;
		case 2:case 3:
			ECHO $sql = " UPDATE public.calles set estareg=".$estareg." 
				WHERE codemp=".$codemp." and codsuc=".$codsuc." and codcalle=".$_POST['1form1_id']." AND codzona=".$codzona." ";
			$result = $conexion->query($sql);
			//$result->execute(array( ":codemp"=>$codemp,":codsuc"=>$codsuc,":codcalle"=>$_POST["1form1_id"], ":estareg"=>$estareg,":codzona"=>$codzona));
		break;
	}
	$NroItems = $_POST["NroItems"]?$_POST["NroItems"]:0;
	for ($i=1; $i<=$NroItems; $i+= 1)		
	{	
		if (isset($_POST["IdSector_".$i]))
		{
			$Sql="INSERT INTO public.sectores_calles
							(codemp, codsuc, codsector, codzona, codcalle ) 
				  	VALUES  (:codemp, :codsuc, :codsector, :codzona,:codcalle );";
			$result = $conexion->prepare($Sql);
			$result->execute(array( ":codemp"=>$codemp,":codsuc"=>$codsuc,":codsector"=>$_POST["IdSector_".$i],":codcalle"=>$codcalle,":codzona"=>$codzona));

		}
	}


	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
