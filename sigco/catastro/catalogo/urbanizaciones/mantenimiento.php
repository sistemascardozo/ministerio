<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");

	$Op 		= $_GET["Op"];

	$codzona	= $_POST["codzona"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	if($Id!='')
	{
		$sql = "SELECT * FROM catastro.urbanizaciones WHERE codurbanizacion=? AND codsuc=? AND codzona=?";

		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($Id, $codsuc, $codzona));
		$row = $consulta->fetch();

		$guardar	= $guardar."&Id2=".$Id;
	}

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
			if($("#codzona").val() == 0)
            {
                alert("Seleccione un codigo de Zona");
                return false;
            }
            if($("#Tipo").val() == 0 && $("#Tipo").val() == '')
            {
								alert("La Descripcion del Tipo no puede ser NULO")
                return false;
            }
            if($("#Descripcion").val() == '')
            {
                alert("La Descripcion de la Manzana no puede ser NULO")
                $("#Descripcion").focus()
                return false;
            }
            GuardarP(Op)
	}

	function cargarSector()
	{
		var codzona = $('#codzona').val();
		$.ajax({
			url: '../../../../ajax/sectores_drop.php',
			type: 'POST',
			async: true,
			data: 'codsuc=<?php echo $codsuc;?>&codzona=' + codzona + '&seleccion=<?php echo $codsector;?>',
			success: function(data) {
				$('#DivSector').html(data);
			}
		})
	}
	function Cancelar()
	{
		location.href='index.php';
	}

</script>
<div align="center">
<form id="form1" name="form1" method="post" action="guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">

      <tbody>
	<tr><td>&nbsp;</td>
	  <td width="30">&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>
	<tr>
        <td class="TitDetalle">Id</td>
        <td align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
			<input name="codurbanizacion" type="text" id="Id" size="4" maxlength="4"  value="<?php echo $Id; ?>" class="inputtext" readonly="readonly"/>
			<input type="hidden" name="Ido" id="Ido" value="<?php echo $Id; ?>" />
		</td>
	</tr>
	<tr>
	  <td class="TitDetalle">Zona</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><?php echo $objMantenimiento->drop_zonas($codsuc, $row["codzona"], ""); ?><input type="hidden" name="codzonao" id="codzonao" value="<?php echo $row["codzona"];?>" /></td>
	  </tr>
	<tr>
	    <td class="TitDetalle">Tipo</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle"><input class="inputtext" name="tipo" type="text" style="text-transform:uppercase; width:70px;" id="Tipo" maxlength="200" value="<?=$row["tipo"]?>"/></td>
	</tr>
        <tr>
	    <td class="TitDetalle">Descripcion</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
                <input class="inputtext" name="descripcion" type="text" style="text-transform:uppercase; width:400px;" id="Descripcion" maxlength="200" value="<?=$row["descripcion"]?>"/></td>
	</tr>
	 <tr>
	    <td class="TitDetalle">Estado</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
        	<? include("../../../../include/estareg.php"); ?>
        </td>
	 </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>

    </table>
 </form>
</div>
<script>
	cargarSector();

	form1.Descripcion.focus();
</script>
<?
	$est = isset($row["estareg"])?$row["estareg"]:1;

	include("../../../../admin/validaciones/estareg.php");
?>
