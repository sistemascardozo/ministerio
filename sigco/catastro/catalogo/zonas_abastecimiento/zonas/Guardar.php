<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();

	$codemp			= 1;
	$codsuc 		= $_SESSION["IdSucursal"];
	$codzona		= $_POST["codzona"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$observacion	= strtoupper($_POST["observacion"]);
	$estareg		= $_POST["estareg"];
	
	$cont_sectores		= $_POST["cont_horario"];
	$cont_reservorio	= $_POST["cont_reservorio"];
	
	$Error = 0;
	
	switch ($Op) 
	{
		case 0:
			$id   		= $objFunciones->setCorrelativos("zonasabastecimiento",$codsuc,"0","0");
			$codzona	= $id[0];
		
			$sql = "insert into public.zonasabastecimiento(codzona,codemp,codsuc,descripcion,observacion,estareg) 
				values(:codzona,:codemp,:codsuc,:descripcion,:observacion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codzona"=>$codzona,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":descripcion"=>$descripcion,
						   ":observacion"=>$observacion,
						   ":estareg"=>$estareg));
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				
			}
		break;
		case 1:
			$sql = "update public.zonasabastecimiento set descripcion=:descripcion,observacion=:observacion,estareg=:estareg 
				where codzona=:codzona and codsuc=:codsuc and codemp=:codemp";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codzona"=>$codzona,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":descripcion"=>$descripcion,
						   ":observacion"=>$observacion,
						   ":estareg"=>$estareg));
						   
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				
			}
			
			$delSectores 	= "delete from public.zonasabastecimiento_sectores where codsuc=? and codzona=?";
			$consultaDS		= $conexion->prepare($delSectores);
			$consultaDS->execute(array($codsuc,$codzona));
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				
			}
			
			$delReservorio 	= "delete from public.zonasabastecimiento_reservorios where codsuc=? and codzona=?";
			$consultaDR		= $conexion->prepare($delReservorio);
			$consultaDR->execute(array($codsuc,$codzona));
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				
			}
		break;
		case 2:case 3:
			$sql = "update public.zonasabastecimiento set estareg=:estareg
				where codzona=:codzona and codsuc=:codsuc and codemp=:codemp";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":codzona"=>$_POST["1form1_id"], ":estareg"=>$estareg));
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				
			}
		break;
	}
	if($Op==0 || $Op==1)
	{
		for($i=1; $i <= $cont_sectores; $i++)
		{
			if(isset($_POST["codsector".$i]))
			{
				$sqlS = "insert into public.zonasabastecimiento_sectores(codemp,codsuc,codzona,codhorario,codsector,horas_abastecimiento,codrutlecturas)
						 values(".$codemp.", ".$codsuc.", ".$_POST["codzona".$i].", ".$_POST["codhorario".$i].", ".$_POST["codsector".$i].", ".$_POST["horas".$i].", ".$_POST["codrutlecturas".$i].")";
				
				$consultaS = $conexion->prepare($sqlS);
				$consultaS->execute(array());
				
				if($result->errorCode()!='00000')
				{
					$Error = 1;
					
					echo $sqlS."<br>";
				}
			}
		}
		
		for($i=1;$i<=$cont_reservorio;$i++)
		{
			if(isset($_POST["codreservorio".$i]))
			{
				$sqlR = "insert into public.zonasabastecimiento_reservorios(codemp,codsuc,codzona,codreservorio)
						 values(:codemp,:codsuc,:codzona,:codreservorio)";
				
				$consultaR = $conexion->prepare($sqlR);
				$consultaR->execute(array(":codemp"=>$codemp,
										  ":codsuc"=>$codsuc,
										  ":codzona"=>$codzona,
										  ":codreservorio"=>$_POST["codreservorio".$i]));
				
				if($result->errorCode()!='00000')
				{
					$Error = 1;
					
					echo $sqlR."<br>";
				}
			}
		}
	}

	if($Error == 1)
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res = 2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
