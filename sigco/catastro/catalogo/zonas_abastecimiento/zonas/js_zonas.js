// JavaScript Document
var cont 	= 0;
var cont_1	= 0;

$(function() {
	$("#tabs").tabs();
	$("#codsector").change(function(){
		cargar_rutas_distribucion();
	});

});

	function cargar_rutas_distribucion()
	{
		var codsector = $("#codsector").val();
		var codzona = $("#codzona").val();
		
		$.ajax({
			url:'cRuta.php',
			type:'POST',
			async:true,
			data:'CodSucursal=' + codsuc + '&CodZona=' + codzona + '&CodSector=' + codsector,
			success:function(datos){
				$("#DivRuta").html(datos)
			}
		}) 
	}
	
	function chckruta(obj)
	{
		if($(obj).attr("checked"))
			$(obj).parent().addClass("multiselect-on");
		else
			$(obj).parent().removeClass("multiselect-on"); 
	}
	function chckrutaall(obj)
	{
		
		if($(obj).attr("checked"))
		{

			$('#DivRuta :input').each(function()
			 {
			 		
			 		$(this).attr("checked","checked");
			 		$(this).parent().addClass("multiselect-on"); 
			 		//chcksector(this)
			});
		}
		else
		{
			$('#DivRuta :input').each(function()
			 {
			 		$(this).attr("checked",false); 
			 		$(this).parent().removeClass("multiselect-on"); 
			 		
			});
		}
	}
	
	function AgregarHorarios()
	{
		var codzona	= $("#codzona").val();
		var decZona	= $("#codzona option:selected").text();
		var codsec 	= $("#codsector").val();
		var decSec	= $("#codsector option:selected").text(); 
		var codh	= $("#horarios").val();
		var decHor	= $("#horarios option:selected").text(); 
		var h = codh.split("/");
		var codhor 	= h[0];
		var horas	= h[1];
	
		if(codsec == 0)
		{
			Msj($("#codsector"), "Seleccione el Sector"); 
			
			return false;
		}
		
		i = 0

		$('#DivRuta :input').each(function(){
			if($(this).attr("checked"))
			{
				i++;
			}
		});
		
		if(i == 0)
		{
			Msj('#DivRuta', 'Seleccione Rutas', 1000, 'right', 'error', false);
			
			return false;
		}
		
		if(codhor == 0)
		{
			Msj($("#horarios"), "Seleccione el Horario");
			
			return false;
		}

		$('#DivRuta :input').each(function() {			
			if($(this).attr("checked"))
			{
				codrut 	= $(this).val();
				decRut	= $(this).data('nombre'); 
				noagrego = true;
				
				for(i = 1; i <= $("#cont_horario").val(); i++)
				{
					try
					{
						if(codsec == $("#codsector" + i).val() && codrut == $("#codrutlecturas" + i).val())
						{
							noagrego = false;
						}
					}
					catch(exp){}
				}
				
				if(noagrego)
				{
					cargar_sectores_zonas(codzona, decZona, codsec, decSec, codrut, decRut, codh, decHor);
				}
			}
		});
	}

	function cargar_sectores_zonas(codzona, decZona, codsec, decSec, codrut, decRut, codh, decHor)
	{
		var h = codh.split("/");
		var codhor 	= h[0];
		var horas	= h[1];
	
		cont++;
		
		$("#tbsectores tbody").append("<tr id='tr_sectores_" + cont + "'>" + 
							"<td align='left' style='padding-left:5px;'>" +
							"<input type='hidden' id='codzona" + cont + "' name='codzona" + cont + "' value='" + codzona + "' />" + decZona + "</td>" +
							"<td align='left' style='padding-left:5px;'>" +
							"<input type='hidden' id='codsector" + cont + "' name='codsector" + cont + "' value='" + codsec + "' />" + decSec + "</td>" +
							"<td align='left' style='padding-left:5px;'>" +
							"<input type='hidden' id='codrutlecturas" + cont + "' name='codrutlecturas" + cont + "' value='" + codrut + "' />" + decRut + "</td>" +
							"<td align='left' style='padding-left:5px;'>" +
							"<input type='hidden' id='codhorario" + cont + "' name='codhorario" + cont + "' value='" + codhor + "' />" + decHor + "</td>" +
							"<td align='center'>" +
							"<input type='hidden' id='horas" + cont + "' name='horas" + cont + "' value='" + horas + "' />" + horas + "</td>" +
							"<td align='center'>" +
							"<span class='icono-icon-trash' title='Borrar Registro' onclick='quitar_row_sector(" + cont + ")'></span>" +
							"</td>" +
							"</tr>");
	
		$("#cont_horario").val(cont);
	}
	
function quitar_todos()
{
	$("#tbsectores tbody").html('');
	cont=0;
	$("#cont_horario").val(0)

}
function cargar_datos_reservorios()
{
	var codreservorio 	= $("#reservorios").val()
	var reservorio		= $("#reservorios option:selected").text(); 	
	
	if(codreservorio==0)
	{
		alert("Seleccione el Reservorio")
		return false
	}
	
	for(i=1;i<=$("#cont_reservorio").val();i++)
	{
		try
		{
			if(codreservorio==$("#codreservorio"+i).val())
			{
				alert("El Reservorio ya Fue Agregado")
				return false
			}
		}catch(exp)
		{
			
		}
	
	}
	
	cont_1++;
	$("#tbreservorios").append("<tr style='background-color:#FFFFFF;' id='tr_reservorios_"+cont_1+"'>"+
							   "<td align='center'>"+
							   "<input type='hidden' id='codreservorio"+cont_1+"' name='codreservorio"+cont_1+"' value='"+codreservorio+"' />"+
							   codreservorio+"</td>"+
							   "<td>"+reservorio+"</td>"+
							   "<td align='center'>"+
							    "<span class='icono-icon-trash' title='Borrar Registro' onclick='quitar_row_reservorios("+cont_1+")'></span>"+
							   "</td>"+
							   "</tr>");
	
	$("#cont_reservorio").val(cont_1)
}
function quitar_row_reservorios(index)
{
	$("#tr_reservorios_"+index).remove();
}
function quitar_row_sector(index)
{
	$("#tr_sectores_"+index).remove();
}