<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");
	
	$TituloVentana = "ZONAS DE ABASTECIMIENTO";
	
	$Activo=1;
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	$FormatoGrilla = array ();
	
  $Sql = "SELECT s.codzona,su.descripcion,s.descripcion,e.descripcion,s.estareg
          FROM public.zonasabastecimiento s 
          INNER JOIN public.estadoreg e ON (s.estareg= e.id)
          INNER JOIN admin.sucursales su ON (su.codsuc= s.codsuc)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'s.codzona', '2'=>'s.descripcion','3'=>'su.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Sucursal', 'T3'=>'Descripci&oacute;n', 'T4'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W2'=>'250', 'W3'=>'250', 'W4'=>'80', 'W5'=>'50');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 800;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " AND su.codsuc=".$codsuc." ORDER BY codzona ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'4',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'5',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'5', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'5', 
              'BtnCV3'=>'0',
              'BtnId4'=>'BtnEliminar', 
              'BtnI4'=>'ok.png', 
              'Btn4'=>'Procesar Horarios', 
              'BtnF4'=>'onclick="Sincronizar(this.id)"', 
              'BtnCI4'=>'5', //campo 3
              'BtnCV4'=>'1');//igua a 1);
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],1000,580);
  Pie();
  CuerpoInferior();
?>
<script type="text/javascript">
	$("#BtnNuevoB").remove();//horasabastecimiento
	
	function Sincronizar (codzona) 
	{
		var r = confirm("Desea Procesar los Horarios de Abastecimiento?");
		
		if(r == false)
		{
			return false;
		}

		$.blockUI({ message: '<div><center><span class="icono-icon-loading"></span>Procesando Horarios de Abastecimiento...Espere un Momento por Favor...</center></div>',
			css: { 
				border: 'none', 
				padding: '15px', 
				backgroundColor: '#000', 
				'-webkit-border-radius': '10px', 
				'-moz-border-radius': '10px', 
				opacity: .5, 
				color: '#fff' 
			}
		});
		  
		var Id2 = $('#' + codzona).data('idreg')?$('#' + codzona).data('idreg'):'';
		var CId = $('#' + codzona).data('valor')?$('#' + codzona).data('valor'):'';
		var CNombres = $('#' + codzona).data('nombres')?$('#' + codzona).data('nombres'):'';
		var Campo = '&' + CNombres + '=' + CId;

		if (CId != '')
		{
			Campo = '&' + CNombres + '=' + CId;
		}
		else
		{
			Id2 = codzona.substr(codzona.indexOf('-') + 1);
		}
	
		$.ajax({
			url:'Sincronizar.php',
			type:'POST',
			async:true,
			data:'codsuc=<?=$codsuc?>'+'&codzona=' + Id2,
			success:function(data){
				if (parseInt(data)==0)
				{
					window.parent.OperMensaje("Error al Procesar Horarios", 2);
				}
				else
				{
					window.parent.OperMensaje("Horarios Procesados Correctamente", 1);
				}
          
				$.unblockUI();
			}
		})
	}
</script>