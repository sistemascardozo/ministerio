<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
   	$Op         = $_GET["Op"];
	$objFunciones = new clsFunciones();
	$codemp			= 1;
	$codsuc 		= $_SESSION["IdSucursal"];
	$codzona		= $_POST["codzona"];
	//
	$Sql="SELECT h.codhorario, z.horas_abastecimiento as horas,
			h.iniciomanana,h.finalmanana, h.iniciotarde, h.finaltarde,
			h.inicionoche, h.finalnoche
			FROM public.zonasabastecimiento_sectores z
			INNER JOIN public.horarios_abastecimiento h ON (z.codemp = h.codemp)
			AND (z.codsuc = h.codsuc) AND (z.codhorario = h.codhorario)
		   where z.codsuc=? and z.codzona=?";
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array($codsuc,$codzona));
	$items = $consulta->fetchAll();
	$Horarios = array();
	foreach($items as $row)
	{
		$Horario='';
		$entro=false;
		if($row['horas']!=24)
		{
			if($row['iniciomanana']!=$row['finalmanana'])
			{
				$Horario= "MAÑANAS: ".strtoupper(date_format(date_create($row["iniciomanana"]),'h:i a'))." A ".strtoupper(date_format(date_create($row["finalmanana"]),'h:i a'));
				$entro=true;
			}
			if($row['iniciotarde']!=$row['finaltarde'])
			{	
				if($entro) $Horario.= ",";
				$Horario.= "TARDES: ".strtoupper(date_format(date_create($row["iniciotarde"]),'h:i a'))." A ".strtoupper(date_format(date_create($row["finaltarde"]),'h:i a'));
				$entro=true;
			}	
			if($row['inicionoche']!=$row['finalnoche'])
			{	
				if($entro) $Horario.= ",";
				$Horario.= "NOCHES: ".strtoupper(date_format(date_create($row["inicionoche"]),'h:i a'))." A ".strtoupper(date_format(date_create($row["finalnoche"]),'h:i a'));
				$entro=true;
			}
		}
		else
			$Horario='DE 00 A 24 HORAS';
			$Horarios[$row['codhorario']]=$Horario;
	}
	//
	$Sql="SELECT c.nroinscripcion,z.horas_abastecimiento as horas,
			h.iniciomanana,h.finalmanana, h.iniciotarde, h.finaltarde,
			h.inicionoche, h.finalnoche,h.codhorario
			FROM catastro.clientes c
			INNER JOIN public.zonasabastecimiento_sectores z
			ON (c.codemp = z.codemp) AND (c.codsuc = z.codsuc)
			AND (c.codsector = z.codsector) AND (c.codrutlecturas = z.codrutlecturas)
			INNER JOIN public.horarios_abastecimiento h ON (z.codemp = h.codemp)
			AND (z.codsuc = h.codsuc) AND (z.codhorario = h.codhorario)
		   where z.codsuc=? and z.codzona=?";
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array($codsuc,$codzona));
	$items = $consulta->fetchAll();
	foreach($items as $row)
	{
		
		$Sql="UPDATE catastro.conexiones 
			SET horasabastecimiento = '".$row['horas']."',
				observacionabast='".$Horarios[$row['codhorario']]."'
		        WHERE codsuc={$codsuc} AND nroinscripcion=".$row['nroinscripcion'];
		$result = $conexion->query($Sql);
	}

	if(!$result)
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=0;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
