<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsDrop.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc 	= $_SESSION['IdSucursal'];
	
	$objMantenimiento 	= new clsDrop();

	if($Id != '')
	{
		$sql = $objMantenimiento->Sentencia("zonasbastecimiento")." WHERE codemp = 1 AND codsuc = ? AND codzona = ?";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($codsuc, $Id));
		$row = $consulta->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;
	}
?>


<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_zonas.js"></script>
<script>
var urldir = '<?php echo $_SESSION['urldir'];?>';
var codsuc = '<?=$codsuc?>';

	function ValidarForm(Op)
	{
		if($("#descripcion").val() == 0)
		{
			alert("La Descripcion de la Zona de Abastecimiento no puede ser NULO")
			return false
		}
		if (tbsectores.rows.length == 1){
			alert ( "No se ha encontrado ningun registros en la tabla Sectores" );
			return false
		}
		if (tbreservorios.rows.length == 1){
			alert ( "No se ha encontrado ningun registros en la tabla Reservorios" );
			return false
		}

		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
	function cargarSector()
	{
		var codzona = $('#codzona').val();
		$.ajax({
			url: '../../../../../ajax/sectores_drop.php',
			type: 'POST',
			async: true,
			data: 'codsuc=<?php echo $codsuc;?>&codzona=' + codzona + '&seleccion=<?php echo $codsector;?>',
			success: function(data) {
				$('#DivSector').html(data);
				$("#codsector").change(function(){
					cargar_rutas_distribucion();
				});
			}
		})
	}
	
</script>
<style type="text/css">
.multiSELECT {
    width:240px;
    height:100px;
    border:solid 1px #c0c0c0;
    overflow:auto;
}
 
.multiSELECT label {
    display:block;
}
 
.multiSELECT-on {
    color:#ffffff;
    background-color:#000099;
}
</style>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
	<table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
				<td colspan="2" class="TitDetalle">
					<div id="tabs">
						<ul>
                            <li style="font-size:10px"><a href="#tabs-2">Datos Generales</a></li>
                            <li style="font-size:10px"><a href="#tabs-3">Sectores por Zona</a></li>
                            <li style="font-size:10px"><a href="#tabs-4">Reservorio por Zona</a></li>
						</ul>
						<div id="tabs-2" style="height:250px">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
                                    <td width="16%">Id</td>
                                    <td width="4%" align="center">:</td>
                                    <td width="80%" >
										<input name="codzona" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?php echo $Id; ?>" class="inputtext"/>
									</td>
								</tr>
								<tr>
                                    <td>Descripcion</td>
                                    <td align="center">:</td>
                                    <td >
                                    	<input type="text" class="inputtext"  id="descripcion" name="descripcion" maxlentgth="200" size="60" value="<?=$row["descripcion"]?>" />
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td>Observacion</td>
                                    <td align="center">:</td>
                                    <td>
										<textarea name="observacion" id="observacion" cols="60" rows="5" class="inputtext"><?=$row["observacion"]?></textarea>
									</td>
                                </tr>
                                <tr valign="top">
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td ><? include("../../../../../include/estareg.php"); ?></td>
								</tr>
							</table>
						</div>
						<div id="tabs-3" style="height:350px">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <td>Zona</td>
								  <td align="center">:</td>
								  <td width="250" ><?php echo $objMantenimiento->drop_zonas($codsuc, $row["codzona"], "onChange='cargarSector(this); cargar_rutas_distribucion();'"); ?></td>
								  <td >&nbsp;</td>
						      </tr>
								<tr>
                                    <td width="90">Sector</td>
                                    <td width="30" align="center">:</td>
                                    <td>
										<div id="DivSector"></div>
									</td>
                                    <td ><span class="icono-icon-add" title="Agregar Horarios" onclick="AgregarHorarios();" ></span></td>
                                </tr>
                                <tr>
                                    <td>Ruta </td>
                                    <td align="center">:</td>
                                    <td>
                	<!-- <div id="div_rutasdistribucion">
										<? $objMantenimiento->drop_rutas_distribucion($codsuc,0); ?>
                    </div> -->
										<div class='multiSELECT' id='DivRuta'></div>
                                    </td>
                                    <td><div style="display:inline">
											<input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox"  onclick="chckrutaall(this);CambiarEstado(this,'todosrutas');" />
											Todas las Rutas
										</div></td>
                                    <td>&nbsp;
										
									</td>
                                </tr>
                                <tr>
                                    <td>Horarios</td>
                                    <td align="center">:</td>
                                    <td >
	              <SELECT name="horarios" id="horarios" class="SELECT" style="width:250px">
	              	<option value="0">--Seleccione el Horario--</option>
                    <?php
						$sqlH = "SELECT * FROM public.horarios_abastecimiento 
						WHERE codsuc=? and estareg=1 order by codhorario";
						$consultaH = $conexion->prepare($sqlH);
						$consultaH->execute(array($codsuc));
						$itemsH = $consultaH->fetchAll();
						
						foreach($itemsH as $rowH)
						{
							$horast=0;
							$horas 	 = $objMantenimiento->RestarHoras($rowH["iniciomanana"],$rowH["finalmanana"]);
							list($h,$m, $s) = split('[:]', $horas);
							if($m==59)$horas=intval($h)+1;// DE 00 A 24 HORAS
							elseif (intval($m)==0){$horas=intval($h);}
							else $horas=substr($horas,0,5);
							$horast+=intval($horas);

							$horas 	 = $objMantenimiento->RestarHoras($rowH["iniciotarde"],$rowH["finaltarde"]);
							list($h,$m, $s) = split('[:]', $horas);
							if($m==59)$horas=intval($h)+1;// DE 00 A 24 HORAS
							elseif (intval($m)==0){$horas=intval($h);}
							else $horas=substr($horas,0,5);
							$horast+=intval($horas);

							$horas 	 = $objMantenimiento->RestarHoras($rowH["inicionoche"],$rowH["finalnoche"]);
							list($h,$m, $s) = split('[:]', $horas);
							if($m==59)$horas=intval($h)+1;// DE 00 A 24 HORAS
							elseif (intval($m)==0){$horas=intval($h);}
							else $horas=substr($horas,0,5);
							$horast+=intval($horas);

							$opt 	 = "<option value='".$rowH["codhorario"]."/".$horast."' >";
							$opt 	.= " Ma&ntilde;ana=>".date_format(date_create($rowH["iniciomanana"]),'h:i a')."-".date_format(date_create($rowH["finalmanana"]),'h:i a');
							$opt 	.= " Tarde=>".date_format(date_create($rowH["iniciotarde"]),'h:i a')."-".date_format(date_create($rowH["finaltarde"]),'h:i a');
							$opt 	.= " Noche=>".date_format(date_create($rowH["inicionoche"]),'h:i a')."-".date_format(date_create($rowH["finalnoche"]),'h:i a')."</option>";
							//$opt .= " DE ".strtoupper(date_format(date_create($rowH["iniciomanana"]),'h:i a'))." A ".strtoupper(date_format(date_create($rowH["finalnoche"]),'h:i a'));
							$opt 	.= " </option>";
							echo $opt;
						}
					?>
                  </SELECT>
	            </td>
                                    <td >&nbsp;</td>
                              </tr>
	          <tr>
	            <td colspan="4" height="5px"></td>
	            </tr>
	          <tr>
	            <td colspan="5">
                <div id="sectores_zonas" style="overflow:auto; height:150px;">
                <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbsectores" rules="all" >
	              <thead>
	              <tr class="ui-widget-header" align="center">
	                <td width="100" height="23" >Zona</td>
	                <td width="100" >Sector</td>
	                <td width="80" >Ruta</td>
	                <td>Horario</td>
	                <td width="30" >Horas</td>
	                <td width="30" >
	                	<span class="icono-icon-trash" title='Quitar Todos' onclick='quitar_todos()'></span>
	                </td>
	              </tr>
	              </thead>
	              <tbody>
<?php
	$cont = 0;
	
	if($Id != '')
	{
		$Sql = "SELECT s.codsector, s.descripcion AS sector, r.codrutlecturas, r.descripcion AS ruta, h.codhorario, h.iniciomanana, h.finalmanana, ";
		$Sql .= " h.iniciotarde, h.finaltarde, h.inicionoche, h.finalnoche, z.horas_abastecimiento, z.codzona, mz.descripcion AS zona ";
		$Sql .= "FROM public.zonasabastecimiento_sectores z ";
		$Sql .= " INNER JOIN public.rutasmaelecturas r ON (z.codemp = r.codemp) AND (z.codsuc = r.codsuc) ";
		$Sql .= "  AND (z.codsector = r.codsector) AND (z.codrutlecturas = r.codrutlecturas) AND (z.codzona = r.codzona) ";
		$Sql .= " INNER JOIN public.sectores s ON (r.codemp = s.codemp) AND (r.codsuc = s.codsuc) ";
		$Sql .= "  AND (r.codsector = s.codsector) AND (r.codzona = s.codzona) ";
		$Sql .= " INNER JOIN public.horarios_abastecimiento h ON (z.codemp = h.codemp) AND (z.codsuc = h.codsuc) AND (z.codhorario = h.codhorario) ";
		$Sql .= " INNER JOIN admin.zonas mz ON (s.codemp = mz.codemp) AND (s.codsuc = mz.codsuc) ";
		$Sql .= "  AND (s.codzona = mz.codzona)  ";
		$Sql .= "WHERE z.codsuc = ".$codsuc." AND z.codzona = ".$Id." ";
		$Sql .= "ORDER BY s.codsector, r.descripcion";
		
		$consultaZ = $conexion->prepare($Sql);
		$consultaZ->execute(array());
		$itemsZ = $consultaZ->fetchAll();
		
		foreach($itemsZ as $rowZ)
		{
						$horast=0;
						$horas 	 = $objMantenimiento->RestarHoras($rowZ["iniciomanana"],$rowZ["finalmanana"]);
						list($h,$m, $s) = split('[:]', $horas);
						if($m==59)$horas=intval($h)+1;// DE 00 A 24 HORAS
						elseif (intval($m)==0){$horas=intval($h);}
						else $horas=substr($horas,0,5);
						$horast+=$horas;

						$horas 	 = $objMantenimiento->RestarHoras($rowZ["iniciotarde"],$rowZ["finaltarde"]);
						list($h,$m, $s) = split('[:]', $horas);
						if($m==59)$horas=intval($h)+1;// DE 00 A 24 HORAS
						elseif (intval($m)==0){$horas=intval($h);}
						else $horas=substr($horas,0,5);
						$horast+=$horas;

						$horas 	 = $objMantenimiento->RestarHoras($rowZ["inicionoche"],$rowZ["finalnoche"]);
						list($h,$m, $s) = split('[:]', $horas);
						if($m==59)$horas=intval($h)+1;// DE 00 A 24 HORAS
						elseif (intval($m)==0){$horas=intval($h);}
						else $horas=substr($horas,0,5);
						$horast+=$horas;

						$cont++;
						$horario  = "Ma&ntildeana=>".date_format(date_create($rowZ["iniciomanana"]),'h:i a')."-".date_format(date_create($rowZ["finalmanana"]),'h:i a');
						$horario .= " Tarde=>".date_format(date_create($rowZ["iniciotarde"]),'h:i a')."-".date_format(date_create($rowZ["finaltarde"]),'h:i a');
						$horario .= " Noche=>".date_format(date_create($rowZ["inicionoche"]),'h:i a')."-".date_format(date_create($rowZ["finalnoche"]),'h:i a');
						//$horario = " DE ".strtoupper(date_format(date_create($rowZ["iniciomanana"]),'h:i a'))." A ".strtoupper(date_format(date_create($rowZ["finalnoche"]),'h:i a'));
				  ?>
                  <tr align="center" id="tr_sectores_<?=$cont?>" >
                    <td align="left" style="padding-left:5px;" >
                    	<input type='hidden' id='codzona<?=$cont?>' name='codzona<?=$cont?>' value='<?=$rowZ["codzona"]?>' />
                        <?=strtoupper($rowZ["zona"])?>
                    </td>
	                <td align="left" style="padding-left:5px;" >
						<input type='hidden' id='codsector<?=$cont?>' name='codsector<?=$cont?>' value='<?=$rowZ["codsector"]?>' />
						<?=strtoupper($rowZ["sector"])?>
                    </td>
                    <td align="left" style="padding-left:5px;" >
						<input type='hidden' id='codrutlecturas<?=$cont?>' name='codrutlecturas<?=$cont?>' value='<?=$rowZ["codrutlecturas"]?>' />
						<?=strtoupper($rowZ["ruta"])?>
                    </td>
	                <td align="left" style="padding-left:5px;"  >
						<input type='hidden' id='codhorario<?=$cont?>' name='codhorario<?=$cont?>' value='<?=$rowZ["codhorario"]?>' />
						<?=$horario?>
                    </td>
	                <td >
						<input type='hidden' id='horas<?=$cont?>' name='horas<?=$cont?>' value='<?=$horast?>' />
                    	<?=$horast?>
                    </td>
	                <td >
	                	<span class="icono-icon-trash" title='Borrar Registro' onclick='quitar_row_sector(<?=$cont?>)'></span>
                    	
                    </td>
	              </tr>
                  <?php 
					}
				}
				  ?>
				  </tbody>
	              </table>
                  <script>
				  	cont = <?=$cont?>
				  </script>
                  </div>
                  </td>
	            </tr>
	          </table>
	        </div>
	      	<div id="tabs-4" style="height:250px">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="11%">Reservorios</td>
                    <td width="3%" align="center">:</td>
                    <td width="86%" >
                    	<?php $objMantenimiento->drop_reservorio($codsuc); ?>
                    	<img src="../../../../../images/iconos/add.png" width="16" height="16" title="Agregar Reservorios" style="cursor:pointer" onclick="cargar_datos_reservorios();" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" height="5px"></td>
                    </tr>
                  <tr>
                    <td colspan="3">
                    <div id="sectores_zonas" style="overflow:auto; height:250px;">
                    <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbreservorios" rules="all" >
                      <tr class="ui-widget-header" align="center">
                        <td width="13%" >Codigo</td>
                        <td width="79%" >Reservorio</td>
                        <td width="8%" >&nbsp;</td>
                      </tr>
                      <?php
					   $cont_1=0;
					   
					  	$sqlS = "SELECT z.codreservorio,r.descripcion
                            FROM public.zonasabastecimiento_reservorios as z
                            INNER JOIN public.reservorios as r on(z.codemp=r.codemp and z.codsuc=r.codsuc and z.codreservorio=r.codreservorio)
                            WHERE z.codsuc=$codsuc and z.codzona= $Id ";
						
						$consultaS = $conexion->prepare($sqlS);
                        //$codsuc,$Id
						$consultaS->execute(array());
						$itemsS = $consultaS->fetchAll();

						foreach($itemsS as $rowS)
						{
						  $cont_1++;
					  ?>
                      <tr style="background-color:#FFF;" align="center" id='tr_reservorios_<?=$cont_1?>'>
                        <td align="center" >
							<input type='hidden' id='codreservorio<?=$cont_1?>' name='codreservorio<?=$cont_1?>' value='<?=$rowS["codreservorio"]?>' />
							<?=$rowS["codreservorio"]?>
                        </td>
                        <td align="left" ><?=strtoupper($rowS["descripcion"])?></td>
                        <td >
                        	<span class="icono-icon-trash" title='Borrar Registro' onclick='quitar_row_reservorios(<?=$cont_1?>)'></span>
                        	
                        </td>
                      </tr>
                      <?php
						}
					  ?>
                      <script>
					  	cont_1 = <?=$cont_1?>;
					  </script>
                      </table>
                      </div>
                      </td>
                    </tr>
                  </table>
	        </div>
	      </div>
	    </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">
      	<input type="hidden" name="cont_horario" id="cont_horario" value="<?=$cont?>" />
        <input type="hidden" name="cont_reservorio" id="cont_reservorio" value="<?=$cont_1?>" />
      </td>
	  <td class="CampoDetalle">&nbsp;</td>
	  </tr>
          </tbody>

    </table>
 </form>
</div>
<script>
	cargarSector();
	
	$("#Nombre").focus();
</script>
<?
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../../admin/validaciones/estareg.php");
?>