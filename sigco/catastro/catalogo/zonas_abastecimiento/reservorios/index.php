<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../../../../include/main.php");
  include("../../../../../include/claseindex.php");
  $TituloVentana = "RESERVORIOS";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  $FormatoGrilla = array ();
  $Sql = "SELECT s.codreservorio,s.descripcion,s.ubicacion,s.capacidad, e.descripcion,s.estareg
          FROM public.reservorios s 
          INNER JOIN public.estadoreg e ON (s.estareg=e.id)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'s.codreservorio', '2'=>'s.descripcion', '3'=>'s.ubicacion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Descripci&oacute;n','T3'=>'Ubicaci&oacute;n','T4'=>'Capacidad', 'T5'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center', 'A4'=>'right', 'A5'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W4'=>'95', 'W5'=>'95');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " AND (codemp=1 and codsuc=".$codsuc.") ORDER BY codreservorio ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'6', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'6', 
              'BtnCV3'=>'0');
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],750,400);
  Pie();
  CuerpoInferior();
?>
