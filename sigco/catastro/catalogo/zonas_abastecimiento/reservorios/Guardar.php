<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codreservorio 	= $_POST["codreservorio"];
	$codemp			= 1;
	$codsuc 		= $_SESSION["IdSucursal"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$ubicacion		= strtoupper($_POST["ubicacion"]);
	$capacidad		= $_POST["capacidad"];
	$estareg		= $_POST["estareg"];

	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("reservorios",$codsuc,"0","0");
			$codreservorio 	= $id[0];
			
			$sql = "insert into public.reservorios(codreservorio,codemp,codsuc,descripcion,capacidad,ubicacion,estareg) 
					values(:codreservorio,:codemp,:codsuc,:descripcion,:capacidad,:ubicacion,:estareg)";
					$result = $conexion->prepare($sql);
			$result->execute(array(":codreservorio"=>$codreservorio,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":descripcion"=>$descripcion,
						   ":capacidad"=>$capacidad,
						   ":ubicacion"=>$ubicacion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.reservorios set descripcion=:descripcion,capacidad=:capacidad,ubicacion=:ubicacion,estareg=:estareg 
				where codemp=:codemp and codsuc=:codsuc AND codreservorio=:codreservorio ";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codreservorio"=>$codreservorio,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":descripcion"=>$descripcion,
						   ":capacidad"=>$capacidad,
						   ":ubicacion"=>$ubicacion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.reservorios set estareg=:estareg
				where codemp=:codemp and codsuc=:codsuc and codreservorio=:codreservorio";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":codreservorio"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
