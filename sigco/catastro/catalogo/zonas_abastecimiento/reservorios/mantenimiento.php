<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "Op=".$Op;
	$codsuc 	= $_SESSION['IdSucursal'];
	
	$objMantenimiento 	= new clsMantenimiento();

	if($Id!='')
	{
		$sql = $objMantenimiento->Sentencia("reservorios")." where codemp=1 and codsuc=? and codreservorio=?";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($codsuc,$Id));
		$row = $consulta->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>

<script>
	function ValidarForm(Op)
	{
		if ($("#Nombre").val() == '')
		{
			alert('La Descripcion del Reservorio no puede ser NULO');
			return false;
		}
		if (document.form1.ubicacion.value == '')
		{
			alert('La Descripcion de la Ubicacion no puede ser NULO');
			return false;
		}
		if (document.form1.capacidad.value == '')
		{
			alert('La Capacidad del Reservorio no puede ser NULO')
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
        <td class="TitDetalle">Id :</td>
        <td class="CampoDetalle">
		<input name="codreservorio" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?php echo $Id; ?>" class="inputtext"/>
 </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion :</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Nombre" size="70" maxlength="200" value="<?php echo $row["descripcion"]; ?>"/></td>
	  </tr>
	<tr valign="top">
	  <td class="TitDetalle">Ubicacion :</td>
	  <td class="CampoDetalle"><textarea name="ubicacion" id="ubicacion" cols="42" rows="5" class="inputtext"><?php echo $row["ubicacion"]; ?></textarea></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Capacidad :</td>
	  <td class="CampoDetalle"><input class="inputtext" name="capacidad" type="text" id="capacidad" size="40" onkeypress="return permite(event,'num');" maxlength="200" value="<?php echo $row["capacidad"]; ?>"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado :</td>
	  <td class="CampoDetalle">
	    <? include("../../../../../include/estareg.php"); ?></td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>

    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../../admin/validaciones/estareg.php");
?>