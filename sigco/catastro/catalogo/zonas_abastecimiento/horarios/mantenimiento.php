<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsDrop.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc 	= $_SESSION['IdSucursal'];
	
	$objMantenimiento 	= new clsDrop();

	if($Id!='')
	{
		$sql = $objMantenimiento->Sentencia("horarios_abastecimiento")." where codemp=1 and codsuc=? and codhorario=?";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($codsuc,$Id));
		$row = $consulta->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery_sistema.js" language="JavaScript"></script>
<script>
	$(document).ready(function() {

	  $('form').keypress(function(e){   
		if(e == 13){
		  return false;
		}
	  });

	  $('input').keypress(function(e){
		if(e.which == 13){
		  return false;
		}
	  });

	});
		
	jQuery(function($)
   {
      $.mask.definitions['H']='[012]';
      $.mask.definitions['N']='[012345]';
      $.mask.definitions['n']='[0123456789]';
	  
      $("#inicioam").mask("Hn:Nn:Nn");
	  $("#finam").mask("Hn:Nn:Nn");
	  $("#iniciopm").mask("Hn:Nn:Nn");
	  $("#finpm").mask("Hn:Nn:Nn");
	  $("#inicionoche").mask("Hn:Nn:Nn");
	  $("#finnoche").mask("Hn:Nn:Nn");
    });
	
	function ValidarForm(Op)
	{
		if($("#inicioam").val()=="")
		{
			alert("La Hora de Inicio de la Ma�ana no puede ser NULO")
			return false
		}
		if($("#finam").val()=="")
		{
			alert("La Hora Final de la Ma�ana no puede ser NULO")
			return false
		}
		if($("#iniciopm").val()=="")
		{
			alert("La Hora de Inicio de la Tarde no puede ser NULO")
			return false
		}
		if($("#finpm").val()=="")
		{
			alert("La Hora Final de la Tarde no puede ser NULO")
			return false
		}
		if($("#inicionoche").val()=="")
		{
			alert("La Hora de Inicio de la Noche no puede ser NULO")
			return false
		}
		if($("#finnoche").val()=="")
		{
			alert("La Hora Final de la Noche no puede ser NULO")
			return false
		}
		
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Id :</td>
	  <td class="CampoDetalle">
	    <input name="codhorario" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?php echo $Id; ?>" class="inputtext"/>
	    </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">
	  	<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetpagos" rules="all" >
	    <tr class="ui-widget-header">
	      <td colspan="2" align="center">Ma&ntilde;ana</td>
	      </tr>
	    <tr style="padding:4px">
	      <td width="50" align="right" class="ui-widget-header">Inicio</td>
	      <td width="164" align="center" style="background-color: white">
	      	<input class="inputtext" style="text-align:right" name="inicioam" type="text" id="inicioam" size="20" maxlength="20" value="<?=isset($row["iniciomanana"])?$row["iniciomanana"]:"00:00:00"?>" onkeypress="return permite(event,'num');"/></td>
	      </tr>
	    <tr style="padding:4px">
	      <td align="right" class="ui-widget-header">Final</td>
	      <td style="background-color: white" align="center"><input class="inputtext" style="text-align:right" name="finam" type="text" id="finam" size="20" maxlength="20" value="<?=isset($row["finalmanana"])?$row["finalmanana"]:"00:00:00"?>" onkeypress="return permite(event,'num');"/></td>
	      </tr>
	    <tr style="padding:4px">
	      <td colspan="2" align="center" class="ui-widget-header">Tarde</td>
	      </tr>
	    <tr style="padding:4px">
	      <td align="right" class="ui-widget-header">Inicio</td>
	      <td style="background-color: white" align="center"><input class="inputtext" style="text-align:right" name="iniciopm" type="text" id="iniciopm" size="20" maxlength="20" value="<?=isset($row["iniciotarde"])?$row["iniciotarde"]:"00:00:00"?>" onkeypress="return permite(event,'num');"/></td>
	      </tr>
	    <tr style="padding:4px">
	      <td align="right" class="ui-widget-header">Final</td>
	      <td style="background-color: white" align="center"><input class="inputtext" style="text-align:right" name="finpm" type="text" id="finpm" size="20" maxlength="20" value="<?=isset($row["finaltarde"])?$row["finaltarde"]:"00:00:00"?>" onkeypress="return permite(event,'num');"/></td>
	      </tr>
	    <tr style="padding:4px">
	      <td colspan="2" align="center" class="ui-widget-header">Noche</td>
	      </tr>
	    <tr style="padding:4px">
	      <td align="right" class="ui-widget-header">Inicio</td>
	      <td style="background-color: white" align="center"><input class="inputtext" style="text-align:right" name="inicionoche" type="text" id="inicionoche" size="20" maxlength="20" value="<?=isset($row["inicionoche"])?$row["inicionoche"]:"00:00:00"?>" onkeypress="return permite(event,'num');"/></td>
	      </tr>
	    <tr style="padding:4px">
	      <td align="right" class="ui-widget-header">Final</td>
	      <td style="background-color: white" align="center"><input class="inputtext" style="text-align:right" name="finnoche" type="text" id="finnoche" size="20" maxlength="20" value="<?=isset($row["finalnoche"])?$row["finalnoche"]:"00:00:00"?>" onkeypress="return permite(event,'num');"/></td>
	      </tr>
	    </table></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado :</td>
	  <td class="CampoDetalle">
	    <? include("../../../../../include/estareg.php"); ?></td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>

    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../../admin/validaciones/estareg.php");
?>