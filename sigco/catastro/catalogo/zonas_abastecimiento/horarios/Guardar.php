<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();

	$codemp			= 1;
	$codsuc 		= $_SESSION["IdSucursal"];
	$codhorario		= $_POST["codhorario"];
	$inicioam		= $_POST["inicioam"];
	$finam			= $_POST["finam"];
	$iniciopm		= $_POST["iniciopm"];
	$finpm			= $_POST["finpm"];
	$inicionoche	= $_POST["inicionoche"];
	$finnoche		= $_POST["finnoche"];
	$estareg		= $_POST["estareg"];
	switch ($Op) 
	{
		case 0:
			$id   		= $objFunciones->setCorrelativos("horarios",$codsuc,"0","0");
			$codhorario	= $id[0];
		
			$sql = "insert into public.horarios_abastecimiento(codhorario,codemp,codsuc,iniciomanana,finalmanana,iniciotarde,
				finaltarde,inicionoche,finalnoche,estareg)
				values(:codhorario,:codemp,:codsuc,:iniciomanana,:finalmanana,:iniciotarde,:finaltarde,:inicionoche,:finalnoche,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codhorario"=>$codhorario,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":iniciomanana"=>$inicioam,
						   ":finalmanana"=>$finam,
						   ":iniciotarde"=>$iniciopm,
						   ":finaltarde"=>$finpm,
						   ":inicionoche"=>$inicionoche,
						   ":finalnoche"=>$finnoche,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.horarios_abastecimiento set iniciomanana=:iniciomanana,finalmanana=:finalmanana,iniciotarde=:iniciotarde,
				finaltarde=:finaltarde,inicionoche=:inicionoche,finalnoche=:finalnoche,estareg=:estareg
				where codhorario=:codhorario and codsuc=:codsuc and codemp=:codemp";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codhorario"=>$codhorario,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":iniciomanana"=>$inicioam,
						   ":finalmanana"=>$finam,
						   ":iniciotarde"=>$iniciopm,
						   ":finaltarde"=>$finpm,
						   ":inicionoche"=>$inicionoche,
						   ":finalnoche"=>$finnoche,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.horarios_abastecimiento set estareg=:estareg
				where codemp=:codemp AND codsuc=:codsuc and codhorario=:codhorario    ";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp, ":codsuc"=>$codsuc,":codhorario"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
