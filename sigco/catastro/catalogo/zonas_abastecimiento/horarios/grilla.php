<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include('../../../../../config.php');
    include("../../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();

    $FormatoGrilla = $_SESSION['Formato'];

    $Sql = $FormatoGrilla[0];
    $Campos = $FormatoGrilla[1];
    $Valor = $_POST['Valor'];

    $Campo = $objFunciones->PreparaSQL($Campos, $Valor);
    $Sql = $objFunciones->VerificarSQL($Sql . $Campo);
    $Sql = $Sql . $FormatoGrilla[8];
    //die($Sql);
    $Consulta = $conexion->prepare($Sql);
    $Consulta->execute();
    $num_total_registros = count($conexion->query($Sql)->fetchAll());

    $TAMANO_PAGINA = $FormatoGrilla[6]['TP'];
    $total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);

    $pagina = $_POST['Pagina'];

    if ($pagina <= $total_paginas) {
        $Pag = ($pagina - 1);
    } else {
        $Pag = 0;
        $pagina = 1;
    }
    $Inicio = $TAMANO_PAGINA * $Pag;

    //$Sql = $Sql." LIMIT ".$FormatoGrilla[6]['TP']." OFFSET ".$Pag;

    $Fields = $Consulta->fetchAll();

    $Tamaño = $FormatoGrilla[7];
?>
<table width="<?=$Tamaño?>" border="1" cellspacing="1" bordercolor="#000000" id="ListaMenu" rules="all" class="ui-widget-content ">
    <thead>
        <tr align="center" id="grilla_mantenimiento">
            <th width="1" rowspan="2">Codigo</th>
            <th colspan="2">Ma&ntilde;ana</th>
            <th colspan="2">Tarde</th>
            <th colspan="2">Noche</th>
            <th width="5" rowspan="2">Estado</th>
            <th width="10" rowspan="2">&nbsp;</th>
        </tr>
        <tr align="center" id="grilla_mantenimiento">
            <th width="13%" height="22">Hora de Inicio</th>
            <th width="11%">Hora Final</th>
            <th width="15%">Hora de Inicio</th>
            <th width="13%">Hora Final</th>
            <th width="16%">Hora de Inicio</th>
            <th width="14%">Hora Final</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $NumRegs = -1;
        foreach ($Fields as $row) {
            $NumRegs = $NumRegs + 1;
            if (($Inicio <= $NumRegs) && ($NumRegs <= ($Inicio + $TAMANO_PAGINA - 1))) {
                ?>
                        <tr id="<?=$row[0]?>"  style="background-color:#FFF;">
                        <?php
                        for ($i = 1; $i <= $Consulta->columnCount() - 1; $i++) {
                            ?>
                                <td align="<?=$FormatoGrilla[4]['A'.$i]?>" valign="middle" style="padding-left:5px; padding-right:5px;"><?=strtoupper($row[$i - 1])?></td>
                            <?php
                        }

                        if ($FormatoGrilla[9]['Id'] != '') {
                            ?>
                                <td valign="middle" style="padding-left:5px; padding-right:5px; width:<?=$FormatoGrilla[9]['NB'] * 15 ?>px">
                                <?php
                                for ($ii = 1; $ii <= $FormatoGrilla[9]['NB']; $ii++) {
                                    if ($row[$FormatoGrilla[9]['BtnCI'.$ii] - 1] == $FormatoGrilla[9]['BtnCV'.$ii]) {
                                        ?>
                                            <img width="20" id="<?= $row[0] ?>" src="<?=$_SESSION['urldir'].'images/iconos/'.$FormatoGrilla[9]['BtnI'.$ii]?>" <? echo $FormatoGrilla[9]['BtnF'.$ii];?> title="<?= $FormatoGrilla[9]['Btn' . $ii] ?>" <?= $FormatoGrilla[9]['BtnF' . $ii] ?> style="cursor:pointer"/>
                                        <?php
                                    }
                                }
                                ?>
                                </td>
                                    <?php
                                }
                                ?>
                        </tr>
                                <?php
                            }
                        }
                        ?>
                        <?php
                        $registros = $num_total_registros - ($TAMANO_PAGINA * ($pagina - 1));
                        $NumColl = $Consulta->columnCount();
                        if ($FormatoGrilla[9]['Id'] != '') {
                            $NumColl = $NumColl + 1;
                        }
                        for ($i = $registros + 1; $i <= $TAMANO_PAGINA; $i++) {
                            ?>
                    <tr style="cursor:default;" bgcolor="#ECECEC"><td colspan="<?= $NumColl ?>">&nbsp;</td></tr>
                    <?php
                }
                ?>
        <tr>
            <th colspan="<?= $NumColl ?>"><div id="Pagination" align="center"></div></th>
        </tr>
    </tbody>
</table>
<input type="hidden" id="NumReg" value="<?= $num_total_registros ?>">
<input type="hidden" id="Pagina" value="<?= $pagina ?>">
<input type="hidden" id="TotalPaginas" value="<?= $total_paginas ?>">