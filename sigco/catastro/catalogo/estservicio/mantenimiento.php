<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	 
	include("../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc 			= $_SESSION['IdSucursal'];
	
	$objMantenimiento 	= new clsMantenimiento();

	if($Id!='')
	{
		$Select  = "select * from 	public.estadoservicio where codestadoservicio = ".$Id;
		$row=$conexion->query($Select)->fetch();
		$Estado = $row['estado'];
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if ($("#Nombre").val() == '')
		{
			alert('La Descripcion del Tipo Estado Servicio del Medidor no puede ser NULO');
			$("#Nombre").focus();
			return false;
		}
		$('#DivPar :input').each(function()
			 {
			 	
			 	if($(this).attr("checked"))
					$(this).val(1); 
				else
					{	$(this).attr("checked","checked");
						$(this).val(0); 
					}
			});
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function chckpert(obj)
	{
		
		if($(obj).attr("checked"))
		{

			$('#DivPar :input').each(function()
			 {
			 		
			 		$(this).attr("checked","checked");
			 		
			});
		}
		else
		{
			$('#DivPar :input').each(function()
			 {
			 		$(this).attr("checked",false); 
			 		
			 		
			});
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr><td>&nbsp;</td>
	  <td width="30">&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>
	<tr>
        <td class="TitDetalle">Id</td>
        <td align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
		<input name="codestadoservicio" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
        </td>
	</tr>
       <tr>
        <td class="TitDetalle">Id. Est. Servicio</td>
        <td align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
		<input name="idestadoservicio" type="text" id="Id" maxlength="4" value="<?=$row[1]?>" class="inputtext" style="width:50px;"/>
        </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Nombre" size="70" maxlength="200" value="<?php echo $row["descripcion"];?>"/>	    </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	     <? include("../../../../include/estareg.php"); ?></td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td align="center" class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
	   <tr>
	  <td valign="top" class="TitDetalle">Parametros</td>
	  <td align="center" valign="top" class="TitDetalle">:</td>
		<td class="CampoDetalle">
			<div id="DivPar">
                <label><input type='checkbox' id='pTodos' onclick='chckpert(this)'/>Todos</label><br /><br />
                <label><input type='checkbox' id='pmed' name='pmed' />&nbsp;Permite Medición</label><br>
                <label><input type='checkbox' id='pfac' name='pfac' />&nbsp;Permite Facturación</label><br>
                <label><input type='checkbox' id='pcob' name='pcob' />&nbsp;Permite Cobranza</label><br>
                <label><input type='checkbox' id='pcobdeumor' name='pcobdeumor' />&nbsp;Permite Cobranza de Deuda en Mora</label><br>
                <label><input type='checkbox' id='pcobresdeu' name='pcobresdeu' />&nbsp;Permite Cobranza de Resumenes de Deuda</label><br>
                <label><input type='checkbox' id='precdeu' name='precdeu' />&nbsp;Permite Reclamar Deuda</label><br>
                <label><input type='checkbox' id='pfaceve' name='pfaceve' />&nbsp;Permite Facturar Eventuales</label><br>
                <label><input type='checkbox' id='pfacplapag' name='pfacplapag' />&nbsp;Permite Facturar Planes de Pago</label><br>
                <label><input type='checkbox' id='pcobplapag' name='pcobplapag' />&nbsp;Permite Cobranza de Plan de Pago</label><br>
                <label><input type='checkbox' id='pfincar' name='pfincar' />&nbsp;Permite Financiar Cargos</label><br>
			</div>
		</td>
	  </tr>
	   <tr>
	     <td valign="top" class="TitDetalle">&nbsp;</td>
	     <td align="center" valign="top" class="TitDetalle">&nbsp;</td>
	     <td class="CampoDetalle">       
        </tr>
          </tbody>

    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
  var i=0;
  if("<?=$row['pmed']?>"==1){$('#pmed').attr("checked","checked");i++;}
  if("<?=$row['pfac']?>"==1){ $('#pfac').attr("checked","checked");i++;}
  if("<?=$row['pcob']?>"==1) {$('#pcob').attr("checked","checked");i++;}
  if("<?=$row['pcobdeumor']?>"==1){ $('#pcobdeumor').attr("checked","checked");i++;}
  if("<?=$row['pcobresdeu']?>"==1){ $('#pcobresdeu').attr("checked","checked");i++;}
  if("<?=$row['precdeu']?>"==1){ $('#precdeu').attr("checked","checked");i++;}
  if("<?=$row['pfaceve']?>"==1){ $('#pfaceve').attr("checked","checked");i++;}
  if("<?=$row['pfacplapag']?>"==1){ $('#pfacplapag').attr("checked","checked");i++;}
  if("<?=$row['pcobplapag']?>"==1){ $('#pcobplapag').attr("checked","checked");i++;}
  if("<?=$row['pfincar']?>"==1){ $('#pfincar').attr("checked","checked");i++;}
  if(i==10) $('#pTodos').attr("checked","checked");
</script>
<?
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php");
?>