<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  $TituloVentana = "ESTADO SERVICIO";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  $FormatoGrilla = array ();
  $Sql = "SELECT s.codestadoservicio,s.idestadoservicio,s.descripcion,e.descripcion,s.estareg
          FROM public.estadoservicio s 
          INNER JOIN public.estadoreg e ON (s.estareg=e.id)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'codestadoservicio', '2'=>'idestadoservicio','3'=>'s.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Id. Estado Servicio', 'T3'=>'Descripci&oacute;n', 'T4'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A4'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W2'=>'65', 'W4'=>'90');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 700;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  ORDER BY codestadoservicio ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'5',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'5', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'5', 
              'BtnCV3'=>'0');
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 750, 480);
  Pie();
  CuerpoInferior();
?>
