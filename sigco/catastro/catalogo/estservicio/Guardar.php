<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codestadoservicio		= $_POST["codestadoservicio"];
	//$codemp			= 1;
	//$unisuc 		= $_SESSION["IdSucursal"];
	$idestadoservicio=$_POST["idestadoservicio"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	$pmed		= $_POST["pmed"];
	$pfac		= $_POST["pfac"];
	$pcob		= $_POST["pcob"];
	$pcobdeumor		= $_POST["pcobdeumor"];
	$pcobresdeu		= $_POST["pcobresdeu"];
	$precdeu		= $_POST["precdeu"];
	$pfaceve		= $_POST["pfaceve"];
	$pfacplapag		= $_POST["pfacplapag"];
	$pcobplapag		= $_POST["pcobplapag"];
	$pfincar		= $_POST["pfincar"];

	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("estadoservicio","0","0");
			$codestadoservicio = $id[0];

			$sql = "insert into public.estadoservicio(codestadoservicio,idestadoservicio,descripcion,estareg,
			pmed,pfac,pcob,pcobdeumor,pcobresdeu,precdeu,pfaceve,
			pfacplapag,pcobplapag, pfincar)
			values(:codestadoservicio,:idestadoservicio,:descripcion,:estareg,
				:pmed,:pfac,:pcob,:pcobdeumor,:pcobresdeu,:precdeu,:pfaceve,
			:pfacplapag,:pcobplapag, :pfincar)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codestadoservicio"=>$codestadoservicio,
						   ":descripcion"=>$descripcion,
						   ":idestadoservicio"=>$idestadoservicio,
						   ":estareg"=>$estareg,
						   ":pmed"=>$pmed,
						   ":pfac"=>$pfac,
						   ":pcob"=>$pcob,
						   ":pcobdeumor"=>$pcobdeumor,
						   ":pcobresdeu"=>$pcobresdeu,
						   ":precdeu"=>$precdeu,
						   ":pfaceve"=>$pfaceve,
						   ":pfacplapag"=>$pfacplapag,
						   ":pcobplapag"=>$pcobplapag,
						   ":pfincar"=>$pfincar));
		break;
		case 1:
			$sql = "update public.estadoservicio set descripcion=:descripcion,idestadoservicio=:idestadoservicio, estareg=:estareg,
			pmed = :pmed, pfac = :pfac, pcob = :pcob,
			  pcobdeumor = :pcobdeumor, pcobresdeu = :pcobresdeu,
			  precdeu = :precdeu, pfaceve = :pfaceve,
			  pfacplapag = :pfacplapag,pcobplapag = :pcobplapag,
			  pfincar = :pfincar
			where codestadoservicio=:codestadoservicio";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codestadoservicio"=>$codestadoservicio,
						   ":descripcion"=>$descripcion,
						   ":idestadoservicio"=>$idestadoservicio,
						   ":estareg"=>$estareg,
						   ":pmed"=>$pmed,
						   ":pfac"=>$pfac,
						   ":pcob"=>$pcob,
						   ":pcobdeumor"=>$pcobdeumor,
						   ":pcobresdeu"=>$pcobresdeu,
						   ":precdeu"=>$precdeu,
						   ":pfaceve"=>$pfaceve,
						   ":pfacplapag"=>$pfacplapag,
						   ":pcobplapag"=>$pcobplapag,
						   ":pfincar"=>$pfincar));
		break;
		case 2:case 3:
			$sql = "update public.estadoservicio set estareg=:estareg
				where codestadoservicio=:codestadoservicio";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codestadoservicio"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
