<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../../../../../include/main.php");
  include("../../../../../../include/claseindex.php");
  $TituloVentana = "DIAMETRO DESAGUE";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  $FormatoGrilla = array ();
  $Sql = "SELECT s.coddiametrosdesague,s.descripcion,e.descripcion,s.estareg
          FROM public.diametrosdesague s 
          INNER JOIN public.estadoreg e ON (s.estareg=e.id)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'s.coddiametrosdesague', '2'=>'s.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Descripci&oacute;n', 'T3'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W3'=>'95');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  ORDER BY coddiametrosdesague ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'4',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'4', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'4', 
              'BtnCV3'=>'0');
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 750, 300);
  Pie();
  CuerpoInferior();
?>

<?php
	if(!session_start()){session_start();}
	
	include("../../../../../../include/btn_mantenimiento.php"); 
	
	include("../../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	
	$Valor 		= isset($_GET["Valor"])?strtoupper($_GET["Valor"]):"";
	$IdSucursal = $_SESSION['IdSucursal'];
	
	$Campos[1] = "coddiametrosdesague";
	$Campos[2] = "descripcion";
		
	$where = $objFunciones->PrepareSql($Campos,"diametrosdesaguex",$Valor,$TAMANO_PAGINA,$inicio," order by coddiametrosdesague");		
	
	$num_total_registros 	= $where[1];
	$total_paginas 			= ceil($num_total_registros / $TAMANO_PAGINA);
	
	Cabecera("Consulta de Diametro de Tubo de Desague");
?>

<script>
	var Id 		= ''
	var Id2		= ''
	var IdAnt	= ''
	var urldir	= '<?php echo $_SESSION['urldir'];?>';
	var Pagina 	= <?=$pagina?>;
	var nPag 	= <?=$TAMANO_PAGINA?>;
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">  
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="TitDetalle">
        <td width="8%" align="right">Buscar</td>
        <td width="3%" align="center">:</td>
        <td width="52%"><label>
          <input type="text" name="Valor" id="Valor" size="70" class="inputtext" value="<?=$Valor?>" onkeypress="ValidarEnter(event,<?=$Op?>)" />
        </label></td>
        <td width="37%" align="left">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  
  <tr>
    <td><table width="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0" style="cursor:pointer">
      <tr align="center" id="grilla_mantenimiento">
        <td>Codigo</td>
        <td>Descripcion</td>
        <td>Estado</td>
      </tr>
      <?php 
			foreach($where[2] as $row)
			{
	  ?>
          <tr onmouseover="color_over(this);" onmouseout="color_out(this);" style="cursor:pointer" id="<?=$row[0]?>" onclick="SeleccionaId(this);">
            <td align="center"><?=$row[0]?></td>
            <td><?=strtoupper($row[1])?></td>
            <td align="center"><?=$d="";$row[2]==1?$d="Activo":$d="Inactivo";echo $d;?></td>
          </tr>
       <?php } ?>
      <tr>
        <td colspan="3" class="PieIndex">
        <div id="Pagination" align="center"></div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<script>$("#Valor").focus();</script>
<?php 
	pie();
?>