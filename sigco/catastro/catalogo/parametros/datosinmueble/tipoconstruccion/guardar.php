<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipoconstruccion		= $_POST["codtipoconstruccion"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];

	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipoconstruccion","0","0");
			$codtipoconstruccion = $id[0];
			$sql = "insert into public.tipoconstruccion(codtipoconstruccion,descripcion,estareg) values(:codtipoconstruccion,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoconstruccion"=>$codtipoconstruccion,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipoconstruccion set descripcion=:descripcion, estareg=:estareg 
			where codtipoconstruccion=:codtipoconstruccion";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoconstruccion"=>$codtipoconstruccion,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipoconstruccion set estareg=:estareg
				where codtipoconstruccion=:codtipoconstruccion";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoconstruccion"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
