<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtiposervicio		= $_POST["codtiposervicio"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];

	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tiposervicio","0","0");
			$codtiposervicio = $id[0];
			
			$sql = "insert into public.tiposervicio(codtiposervicio,descripcion,estareg) values(:codtiposervicio,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtiposervicio"=>$codtiposervicio,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tiposervicio set descripcion=:descripcion, estareg=:estareg 
			where codtiposervicio=:codtiposervicio";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtiposervicio"=>$codtiposervicio,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tiposervicio set estareg=:estareg
				where codtiposervicio=:codtiposervicio";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtiposervicio"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
