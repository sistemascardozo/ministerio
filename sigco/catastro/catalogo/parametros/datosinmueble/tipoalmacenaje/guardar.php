<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipoalmacenaje		= $_POST["codtipoalmacenaje"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];

	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipoalmacenaje","0","0");
			$codtipoalmacenaje = $id[0];
			$sql = "insert into public.tipoalmacenaje(codtipoalmacenaje,descripcion,estareg) values(:codtipoalmacenaje,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoalmacenaje"=>$codtipoalmacenaje,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipoalmacenaje set descripcion=:descripcion, estareg=:estareg 
			where codtipoalmacenaje=:codtipoalmacenaje";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoalmacenaje"=>$codtipoalmacenaje,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipoalmacenaje set estareg=:estareg
				where codtipoalmacenaje=:codtipoalmacenaje";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoalmacenaje"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
