<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipoabastecimiento 		= $_POST["codtipoabastecimiento"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];

	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipoabastecimiento",0,0);
			$codtipoabastecimiento = $id[0];
			$sql = "insert into public.tipoabastecimiento(codtipoabastecimiento,descripcion,estareg) values(:codtipoabastecimiento,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoabastecimiento"=>$codtipoabastecimiento,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipoabastecimiento set descripcion=:descripcion, estareg=:estareg 
			where codtipoabastecimiento=:codtipoabastecimiento ";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoabastecimiento"=>$codtipoabastecimiento,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipoabastecimiento set estareg=:estareg
				where codtipoabastecimiento=:codtipoabastecimiento";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoabastecimiento"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>