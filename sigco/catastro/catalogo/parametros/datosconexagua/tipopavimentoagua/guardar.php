<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipopavimentoagua		= $_POST["codtipopavimentoagua"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipopavimentoagua","0","0");
			$codtipopavimentoagua = $id[0];
			$sql = "insert into public.tipopavimentoagua(codtipopavimentoagua,descripcion,estareg) values(:codtipopavimentoagua,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipopavimentoagua"=>$codtipopavimentoagua,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipopavimentoagua set descripcion=:descripcion, estareg=:estareg 
			where codtipopavimentoagua=:codtipopavimentoagua";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipopavimentoagua"=>$codtipopavimentoagua,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipopavimentoagua set estareg=:estareg
				where codtipopavimentoagua=:codtipopavimentoagua";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipopavimentoagua"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
