<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipocorte		= $_POST["codtipocorte"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	

	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipocorte","0","0");
			$codtipocorte = $id[0];
			
			$sql = "insert into public.tipocorte(codtipocorte,descripcion,estareg) values(:codtipocorte,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipocorte"=>$codtipocorte,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipocorte set descripcion=:descripcion, estareg=:estareg 
			where codtipocorte=:codtipocorte";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipocorte"=>$codtipocorte,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipocorte set estareg=:estareg
				where codtipocorte=:codtipocorte";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipocorte"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
