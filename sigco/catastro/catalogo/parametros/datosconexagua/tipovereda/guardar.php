<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipovereda		= $_POST["codtipovereda"];
	//$codemp			= 1;
	//$unisuc 		= $_SESSION["IdSucursal"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipovereda","0","0");
			$codtipovereda = $id[0];
			
			$sql = "insert into public.tipovereda(codtipovereda,descripcion,estareg) values(:codtipovereda,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipovereda"=>$codtipovereda,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipovereda set descripcion=:descripcion, estareg=:estareg 
			where codtipovereda=:codtipovereda";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipovereda"=>$codtipovereda,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipovereda set estareg=:estareg
				where codtipovereda=:codtipovereda";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipovereda"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
