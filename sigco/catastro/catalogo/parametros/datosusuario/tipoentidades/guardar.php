<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipoentidades		= $_POST["codtipoentidades"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];

	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipoentidades","0","0");
			$codtipoentidades = $id[0];
			$sql = "insert into public.tipoentidades(codtipoentidades,descripcion,estareg) values(:codtipoentidades,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoentidades"=>$codtipoentidades,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipoentidades set descripcion=:descripcion, estareg=:estareg 
			where codtipoentidades=:codtipoentidades";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoentidades"=>$codtipoentidades,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipoentidades set estareg=:estareg
				where codtipoentidades=:codtipoentidades";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoentidades"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
