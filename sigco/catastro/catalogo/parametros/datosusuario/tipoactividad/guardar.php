<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipoactividad		= $_POST["codtipoactividad"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipoactividad","0","0");
			$codtipoactividad = $id[0];
			$sql = "insert into public.tipoactividad(codtipoactividad,descripcion,estareg) values(:codtipoactividad,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoactividad"=>$codtipoactividad,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipoactividad set descripcion=:descripcion, estareg=:estareg 
			where codtipoactividad=:codtipoactividad";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoactividad"=>$codtipoactividad,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipoactividad set estareg=:estareg
				where codtipoactividad=:codtipoactividad";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoactividad"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
