<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipousuario		= $_POST["codtipousuario"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("tipousuario","0","0");
			$codtipousuario = $id[0];
			$sql = "insert into public.tipousuario(codtipousuario,descripcion,estareg) values(:codtipousuario,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipousuario"=>$codtipousuario,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipousuario set descripcion=:descripcion, estareg=:estareg 
			where codtipousuario=:codtipousuario";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipousuario"=>$codtipousuario,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipousuario set estareg=:estareg
				where codtipousuario=:codtipousuario";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipousuario"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
