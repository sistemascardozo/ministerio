<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codsector 		= $_POST["codsector"];
	$codemp			= 1;
	$unisuc 		= $_POST["codsuc"]?$_POST["codsuc"]:$_SESSION['IdSucursal'];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	$codzona	= $_POST["codzona"];
	$codzonao	= $_POST["codzonao"];
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("sectores", $unisuc, "0");
			
			$codsector = $id[0];
			
			$sql = "INSERT INTO public.sectores(codemp, codsuc, codzona, codsector, descripcion, estareg) ";
			$sql .= "VALUES(:codemp, :codsuc, :codzona, :codsector, :descripcion, :estareg)";
			
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsector"=>$codsector,
						   ":descripcion"=>$descripcion,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$unisuc,
						   ":estareg"=>$estareg,
						   ":codzona"=>$codzona
		  					));
		break;
		case 1:
			$sql = "UPDATE public.sectores ";
			$sql .= " SET codzona = :codzona,  descripcion = :descripcion, estareg = :estareg ";
			$sql .= "WHERE codemp = :codemp AND codsuc=:codsuc AND codzona = :codzonao AND codsector = :codsector";
			
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsector"=>$codsector,
						   ":descripcion"=>$descripcion,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$unisuc,
						   ":estareg"=>$estareg,
						    ":codzona"=>$codzona,
						    ":codzonao"=>$codzonao
						  	));
		break;
		case 2:case 3:
			$sql = "update public.sectores set estareg=:estareg ";
			$sql .= "WHERE codemp = :codemp AND codsuc=:codsuc AND codzona = :codzonao AND codsector = :codsector";
			
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsector"=>$_POST["1form1_id"], 
									":codsuc"=>$unisuc, 
									":estareg"=>$estareg, 
									":codzonao"=>$codzonao
									));
		break;
	
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
