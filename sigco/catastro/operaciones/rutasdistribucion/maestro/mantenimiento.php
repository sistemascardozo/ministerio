<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../objetos/clsDrop.php");

$objDrop = new clsDrop();

$Op = $_POST["Op"];
$Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
$codemp = 1;
$codsuc = $_SESSION['IdSucursal'];
//$codsector	= $_GET["codsector"];
$codsector = isset($_POST["codsector"]) ? $_POST["codsector"] : '';
$guardar = "op=" . $Op;
$disabled = "";

if ($Id != '') {
    $sql = $objDrop->Sentencia("rutasmaedistribucion") . " where codemp=? and codsuc=? and codsector=? and codrutdistribucion=?";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codemp, $codsuc, $codsector, $Id));
    $row = $consulta->fetch();

    $guardar = $guardar . "&Id2=" . $Id;
    $disabled = "disabled='disabled'";
}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script>
    function ValidarForm(Op)
    {
        if ($("#Id").val() == '')
        {
            alert("Ingrese Codigo")
            return false;
        }
        if ($("#codsector").val() == 0)
        {
            alert("Seleccione el Sector")
            return false;
        }
        if ($("#Descripcion").val() == '')
        {
            alert("La Descripcion de la Ruta no puede ser NULO")
            return false;
        }
        if ($("#orden").val() == '')
        {
            alert("Digite el Orden de la Ruta de Distribucion")
            return false;
        }
        GuardarP(Op);
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td width="30" class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
                <tr>
                    <td class="TitDetalle">Id</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <input name="codrutdistribucion" type="text" id="Id" size="4" maxlength="4" value="<? echo $Id; ?>" class="inputtext"/>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Sector</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <?php echo $objDrop->drop_sectores2($codsuc, $row ["codsector"], $disabled); ?>
                        <input type="hidden" name="sector" id="codsector" value="<?=$codsector ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Descripcion</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <input class="inputtext" name="descripcion" type="text" style="text-transform:uppercase; width:400px;" id="Descripcion" maxlength="200" value="<?=$row["descripcion"] ?>"/>            </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Orden</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle"><input class="inputtext" name="orden" type="text" style="text-transform: uppercase" id="orden" size="10" maxlength="10"value="<?=$row["orden"] ?>"/></td>
                </tr>
                <tr>
                    <td class="TitDetalle">Estado</td>
                    <td align="center" class="TitDetalle">:</td>
                    <td class="CampoDetalle">
                        <?php include("../../../../../include/estareg.php"); ?>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    form1.Descripcion.focus();
</script>
<?php
$est = isset($rutas["estareg"]) ? $rutas["estareg"] : 1;

include("../../../../../admin/validaciones/estareg.php");
?>