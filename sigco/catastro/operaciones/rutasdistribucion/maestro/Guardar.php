<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include('../../../../../objetos/clsFunciones.php');

$objFunciones = new clsFunciones();

$conexion->beginTransaction();

$Op = $_GET["Op"];

$codemp = 1;
$codsuc = $_SESSION['IdSucursal'];
$codrutdistribucion = $_POST["codrutdistribucion"];
$sector  = $Op == 1 ? $_POST["sector"] : $_POST["codsector"];
$descripcion = strtoupper($_POST["descripcion"]);
$orden = $_POST["orden"];
$estareg = $_POST["estareg"];
$codzona = 1;

switch ($Op) {
    case 0:


        $sql = "INSERT INTO public.rutasmaedistribucion(codrutdistribucion,codsector,descripcion,estareg,orden,codemp,codsuc,codzona)";
        $sql .= "values($codrutdistribucion, $sector, '$descripcion', $estareg, $orden, $codemp, $codsuc, $codzona)";
        $result = $conexion->prepare($sql);
        $result->execute(array());
        
        break;
    case 1:
        $sql = "UPDATE public.rutasmaedistribucion set descripcion=:descripcion,estareg=:estareg,orden=:orden";
        $sql .= "WHERE codemp=:codemp and codzona=:codzona and codsuc=:codsuc and codsector=:codsector and codrutdistribucion=:codrutdistribucion";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":codsector" => $sector,
            ":codrutdistribucion" => $codrutdistribucion,
            ":descripcion" => $descripcion,
            ":orden" => $orden,
            ":estareg" => $estareg,
            ":codzona" => $codzona
        ));
        break;
    case 2:case 3:
        $sql = "UPDATE public.rutasmaedistribucion set estareg=:estareg
            WHERE codemp=:codemp and codsuc=:codsuc AND codsector=:codsector and codrutdistribucion=:codrutdistribucion ";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codsector" => $sector, ":codemp" => $codemp, ":codsuc" => $codsuc, ":codrutdistribucion" => $_POST["1form1_id"], ":estareg" => $estareg));
        break;
}
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    echo $res = 2;
    $data['res'] = 2;
    $data['mensaje'] = $mensaje." ==>".$sql;
    die(json_encode($data));
} else {
    $conexion->commit();
    $mensaje = "El Registro se ha Grabado Correctamente";
    echo $res = 1;
}
?>
