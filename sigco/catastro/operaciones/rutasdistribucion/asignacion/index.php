<?php
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");

	$TituloVentana = "ASIGNACIÓN RUTAS DE DISTRIBUCION";

	$Activo = 1;
	$Criterio = 'rDistribucion';

	CuerpoSuperior($TituloVentana);

	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsector 	= isset($_GET["codsector"])?$_GET["codsector"]:1;
	$codsuc = $_SESSION['IdSucursal'];

	$FormatoGrilla = array ();

	$Sql = "SELECT s.codrutdistribucion, z.descripcion as zona, s.descripcion, s.orden, e.descripcion, s.estareg, s.codsector, s.codzona ";
	$Sql .= " FROM public.rutasmaedistribucion s";
	$Sql .= " JOIN public.estadoreg e ON (s.estareg=e.id)";
	$Sql .= " JOIN admin.zonas z ON (s.codzona = z.codzona AND s.codsuc = z.codsuc AND s.codemp = z.codemp)";

	$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
	$FormatoGrilla[1] = array('1'=>'codrutdistribucion', '2'=>'s.descripcion', '2'=>'z.descripcion');          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Zona', 'T3'=>'Ruta de Distribuci&oacute;n', 'T4'=>'Orden','T5'=>'Estado');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center', 'A4'=>'center', 'A5'=>'center');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1'=>'60', 'W3'=>'150', 'W4'=>'60', 'W5'=>'60');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 700;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = " AND (s.codemp = 1 AND s.codsuc = ".$codsuc." AND s.codsector = ".$codsector.") ORDER BY s.codrutdistribucion ASC ";                                   //Orden de la Consulta
	$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
			  'NB'=>'1',          //Número de Botones a agregar
			  'BtnId1'=>'BtnModificar',   //Nombre del Boton
			  'BtnI1'=>'modificar.png',   //Imagen a mostrar
			  'Btn1'=>'Editar',       //Titulo del Botón
			  'BtnF1'=>'onclick="Modificar(this);"',  //Eventos del Botón
			  'BtnCI1'=>'6',  //Item a Comparar
			  'BtnCV1'=>'1');
	$FormatoGrilla[10] = array(array('Name' =>'id','Col'=>1),array('Name' =>'sector','Col'=>7),array('Name' =>'zona','Col'=>8));//DATOS ADICIONALES
	$FormatoGrilla[11] = 5;//FILAS VISIBLES
	$_SESSION['Formato'.$Criterio] = $FormatoGrilla;

	$Previo=' <table width="100%" border="0" >';
	$Previo.='<tr><td valign="middle" align="left" width="65">Sector :</td><td valign="middle" align="left">';
	$Previo.= $objFunciones->drop_sectores2($codsuc, $codsector, "onchange='BuscarB(".$Op.")'");
	$Previo.='</td></tr></table>';

	Cabecera($Previo, $FormatoGrilla[7], 750, 600);
	Pie();
  //CuerpoInferior();
?>
<script type="text/javascript">
$("#BtnNuevoB").remove();

function BuscarB(Op)
  {
    var Valor     = document.getElementById('Valor').value
    var codsector   = document.getElementById('codsector').value
    var Op2 = ''
    if (Op!=0)
    {
      Op2 = '&Op=' + Op;
    }
    location.href='index.php?Valor=' + Valor + '&pagina=' + Pagina + Op2 + '&codsector='+codsector;
  }

  function Modificar(obj)
  {
    $("#form1").remove();
    var Id = $(obj).parent().parent().data('id')
    var sector = $(obj).parent().parent().data('sector')
    var zona = $(obj).parent().parent().data('zona')
    $("#Modificar").dialog("open");
    $("#DivModificar").html("<center><span class='icono-icon-loading'></span></center>");
    $.ajax({
       url:'mantenimiento.php?Op=1',
       type:'POST',
       async:true,
       data:'Id=' + Id+ '&codsuc=<?=$codsuc?>'+'&codsector='+sector+'&codzona='+zona,
       success:function(data){
        $("#DivModificar").html(data);
       }
    });
  }

  function Anular(obj)
  {
    $("#form1").remove();
    var Id = $(obj).parent().parent().data('id')
    var sector = $(obj).parent().parent().data('sector')
    var html = '<form id="form1" name="form1">';
    html+='<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
    html+='<input type="hidden" name="sector" value="' + sector + '" />';
    html+='<input type="hidden" name="estareg" id="estareg" value="0" />';
    html+='</form>'
    $("#DivEliminacion").html(html);
    $("#ConfirmaEliminacion").dialog("open");
  }

  function Activar(obj)
  {
    $("#form1").remove();
    var Id = $(obj).parent().parent().data('id')
    var sector = $(obj).parent().parent().data('sector')
    var html = '<form id="form1" name="form1">';
    html+='<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
    html+='<input type="hidden" name="sector" value="' + sector + '" />';
    html+='<input type="hidden" name="estareg" id="estareg" value="1" />';
    html+='</form>'
    $("#DivRestaurar").html(html);
    $("#ConfirmaRestauracion").dialog("open");
  }
</script>
<?php  CuerpoInferior();?>

</script>
