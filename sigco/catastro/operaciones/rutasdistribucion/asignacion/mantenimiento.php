<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../objetos/clsDrop.php");

	$objDrop     = new clsDrop();

	$Op          = $_POST["Op"];
	$Id          = isset($_POST["Id"])?$_POST["Id"]:'';
	$codemp      = 1;
	$codsuc      = $_SESSION['IdSucursal'];
	//$codsector = $_GET["codsector"];
	$codsector   = isset($_POST["codsector"])?$_POST["codsector"]: '';
	$codzona     = isset($_POST["codzona"])?$_POST["codzona"]: '';
	$guardar    = "op=".$Op;
	$disabled    = "";

	$sqlRuta = "select descripcion
				from public.rutasmaedistribucion
				where codemp=1 and codsuc=? and codsector=? and codrutdistribucion=? ";

	$consRuta = $conexion->prepare($sqlRuta);
	$consRuta->execute(array($codsuc,$codsector,$Id));
	$itemRuta = $consRuta->fetch();

	$sqlZona = "select *
				from admin.zonas z
				where codemp=1 and codsuc=? and codzona=? ";

	$consZona = $conexion->prepare($sqlZona);
	$consZona->execute(array($codsuc,$codzona));
	$itemZona = $consZona->fetch();

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
urldir = '<?php echo $_SESSION['urldir'];?>'

var cont = 0;
var contador=0;

function QuitaFilaD(x)
{
	while (x.tagName.toLowerCase() !='tr')
	{
		if(x.parentElement)
			x=x.parentElement;
		else if(x.parentNode)
			x=x.parentNode;
		else
			return;
	}

	var rowNum=x.rowIndex;
	while (x.tagName.toLowerCase() !='table')
	{
		if(x.parentElement)
			x=x.parentElement;
		else if(x.parentNode)
			x=x.parentNode;
		else
			return;
	}
	x.deleteRow(rowNum);
	contador=contador-1
}
function Agregar()
{
	var codmanzana 	= $("#manzanas").val()
	var manzanas	= $("#manzanas option:selected").text();

	if(codmanzana == 0)
	{
		//alert("Seleccione la Manzana")
		//return false
	}

	for (i = 1; i <= $("#contador").val(); i++)
	{
		try
		{
			if(eval($("#codmanzanas"+i).val()==codmanzana))
			{
				alert("La Manzana se Encuentra Agregado ");
				return false
			}
		}catch(exp)
		{

		}
	}

	cont++;
	contador++;

	$("#tbrutas tbody" ).append("<tr style='background-color:#FFFFFF; padding:4px;'>"+
								"<td align='center'><input type='hidden' name='codmanzanas"+cont+"' id='codmanzanas"+cont+"' value='"+codmanzana+"' />"+codmanzana+"</td>"+
								"<td>"+manzanas+"</td>"+
								"<td align='center' ><input name='nroorden"+cont+"' id='nroorden"+cont+"' type='text' style='text-align:center' value='0' class='inputtext' maxlength='5' size='5' /></td>"+
								"<td align='center'><span class='icono-icon-trash'  onclick='QuitaFilaD(this);' title='Borrar Registros' ></span></td>"+
								"</tr>")

	$("#contador").val(cont)
}
function ValidarForm(Op)
{
	for (x = 1; x <= $("#contador").val(); x++)
	{
			try
			{
				if($("#nroorden"+x).val()=="")
				{
					alert("No ha Ingresado el Orden en el Registro: "+x)
					return false
				}
			}catch(exp)
			{

			}
	}
	GuardarP(Op)
}
function Cancelar()
{
	location.href='index.php';
}
</script>
<form id="form1" name="form1" method="post" action="guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos" align="center">
      <tbody>
        <tr>
            <td class="TitDetalle">&nbsp;</td>
            <td class="CampoDetalle">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
            <fieldset>
               <table border="0px" width="100%">
                    <tr>
                        <td width="80" align="right">Rutas</td>
                        <td width="30" align="center">:</td>
                        <td colspan="2" align="left">
                          	<input type="text" name="ruta" id="ruta" style="width:220px" class="inputtext" readonly="readonly" value="<?=$itemRuta["descripcion"]?>" />
                        	<input type="hidden" name="codruta" id="codruta" value="<?=$Id?>" />
                        	<input type="hidden" name="codsector" id="codsector" value="<?=$codsector?>" /></td>
                    </tr>
                    <tr>
                        <td width="80" align="right">Zona</td>
                        <td width="30" align="center">:</td>
                        <td colspan="2" align="left">
                      	<input type="text" name="zona" id="zona" style="width:220px" class="inputtext" readonly="readonly" value="<?=$itemZona["descripcion"]?>" />
                      	<input type="hidden" name="codzona" id="codzona" value="<?=$itemZona['codzona']?>" />
                    </tr>
                    <tr>
                        <td align="right">Manzana</td>
                        <td align="center">:</td>
                        <td width="36%" align="left">
                        	<?php
								$sql = $objDrop->Sentencia("manzanas")." WHERE codemp=1 AND codsuc=? AND codsector=? ORDER BY descripcion ";

								$consulta = $conexion->prepare($sql);
                                //print_r($consulta);
								$consulta->execute(array($codsuc,$codsector));
								$items = $consulta->fetchAll();

								echo "<select id='manzanas' name='manzanas' class='select' style='width:220px' >";
								echo "<option value='0'>--Seleccione la Manzana--</option>";
								foreach($items as $row)
								{
									$sqlcont = "select count(*) from public.rutasdistribucion
												where codsuc=? and codsector=? AND codrutdistribucion=? and codmanzanas=? ";

									$conscont = $conexion->prepare($sqlcont);
									$conscont->execute(array($codsuc,$codsector,$Id,$row["codmanzanas"]));
									$itemCont = $conscont->fetch();

									if($itemCont[0]==0)
									{
										echo "<option value='".$row["codmanzanas"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
									}
								}
								//$objDrop->drop_manzanas($codsuc,$codsector);
							?>
                        </td>
                        <td width="49%" align="left"><img src="../../../../../images/iconos/add.png" alt="" style="cursor: pointer" onclick="Agregar()" /></td>
                    </tr>
                </table>
          </fieldset></td>
	</tr>
        <tr>
            <td class="TitDetalle">&nbsp;</td>
            <td class="CampoDetalle">&nbsp;</td>
	</tr>
        <tr>
            <td colspan="2" align="center">
                <table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tbrutas" >
          			<thead class="ui-widget-header" style="font-size: 11px">
                    	<tr style="color: white; font-weight: bold">
                        <td width="17%" align="center" s>Codigo</td>
                        <td width="64%" align="center">Manzana</td>
                        <td width="14%" align="center">Nro. Orden</td>
                        <td width="5%">&nbsp;</td>
                    </tr>
                </thead>
                <tbody>
<?php
						$cont = 0;

						$SqlD  = "SELECT CAST(r.codmanzanas AS INTEGER), m.descripcion, r.nroorden ";
						$SqlD .= "FROM public.rutasdistribucion as r ";
						$SqlD .= " INNER JOIN public.manzanas as m ON(r.codemp = m.codemp) AND (r.codsuc = m.codsuc) ";
						$SqlD .= "  AND (r.codsector = m.codsector) AND (r.codmanzanas = m.codmanzanas) ";
						$SqlD .= "WHERE r.codsuc = ".$codsuc." ";
						$SqlD .= " AND r.codsector = ".$codsector." ";
						$SqlD .= " AND r.codrutdistribucion = ".$Id." ";
						$SqlD .= "ORDER BY CAST(r.codmanzanas AS INTEGER), r.nroorden";

						$consultaD = $conexion->prepare($SqlD);
						$consultaD->execute(array());
						$itemsD = $consultaD->fetchAll();

						foreach($itemsD as $rowD)
						{
							$cont++;
					?>
                    <tr style="background:#FFF">
                      <td align="center" ><?=$rowD["codmanzanas"]?>
                      <input type="hidden" name="codmanzanas<?=$cont?>" id="codmanzanas<?=$cont?>" value="<?=$rowD["codmanzanas"]?>" />
                      </td>
                      <td align="left" style="padding-left:5px;"><?=strtoupper($rowD["descripcion"])?></td>
                      <td align="center">
                        <input name='nroorden<?=$cont?>' id="nroorden<?=$cont?>" type='text' style="text-align:center" value='<?=$rowD["nroorden"]?>' class='inputtext' maxlength='5' size='5' />
                      </td>
                      <td align="center">
                      	<span class='icono-icon-trash'  onclick='QuitaFilaD(this, 1);' title="Borrar Registros" ></span>

                      </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                    <script>
						cont 		= <?=$cont?>;
						contador 	= <?=$cont?>;
					</script>

                </table>
              </td>
	</tr>
	 <tr>
     	<td class="TitDetalle">&nbsp;</td>
	    <td class="CampoDetalle"><input type="hidden" name="contador" id="contador" value="<?=$cont?>" /></td>
	  </tr>
          </tbody>
    </table>
 </form>
