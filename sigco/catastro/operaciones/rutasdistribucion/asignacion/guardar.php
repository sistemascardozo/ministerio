<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../../objetos/clsFunciones.php');

	$objFunciones = new clsFunciones();

	$conexion->beginTransaction();
	
	$codemp   = 1;
	$contador = $_POST["contador"];
	$codsuc   = $_SESSION['IdSucursal'];

	for($i = 1; $i <= $contador; $i++)
	{
		//if($_POST["codmanzanas".$i])
//		{			
			$sqlV = "SELECT COUNT(*) FROM public.rutasdistribucion ";
			$sqlV .= "WHERE codsuc = ".$codsuc." ";
			$sqlV .= " AND codzona = ".$_POST["codzona"]." ";
			$sqlV .= " AND codsector = ".$_POST["codsector"]." ";
			$sqlV .= " AND codrutdistribucion = ".$_POST["codruta"]." ";
			$sqlV .= " AND codmanzanas = '".$_POST["codmanzanas".$i]."' ";

			$result = $conexion->prepare($sqlV);
			$result->execute(array());
			
			$itemV = $result->fetch();

			if($itemV[0] == 0)
			{
				$sql = "INSERT INTO public.rutasdistribucion(codemp,codsuc,codzona,codsector,codrutdistribucion,codmanzanas,nroorden)
					   VALUES(:codemp ,:codsuc ,:codzona ,:codsector ,:codrutdistribucion ,:codmanzanas ,:nroorden)";
			}else{
				$sql = "UPDATE public.rutasdistribucion SET nroorden=:nroorden
						WHERE codemp =:codemp  AND codzona=:codzona AND codsector=:codsector AND codrutdistribucion=:codrutdistribucion AND codmanzanas=:codmanzanas AND
						codsuc=:codsuc";
			}

			$result = $conexion->prepare($sql);
			$result->execute(array(
									":codemp"=>$codemp ,
									":nroorden"=>$_POST["nroorden".$i],
									":codzona"=>$_POST["codzona"],
							    ":codsector"=>$_POST["codsector"],
							    ":codrutdistribucion"=>$_POST["codruta"],
							    ":codmanzanas"=>$_POST["codmanzanas".$i],
							   	":codsuc"=>$codsuc
								 ));
		//}
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
