<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  
  $TituloVentana = "ACTUALIZACION DE USUARIOS";
  $Criterio='Usuariosact';
  $Activo=1;
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  if($Op!=5) CuerpoSuperior($TituloVentana);
  $codsuc = $_SESSION['IdSucursal'];
  $and = "";
    if ($_GET['Op'] == 5) {
        $and = " AND c.nromed<>' ' ";
    }
  $FormatoGrilla = array ();

  $Sql = "SELECT  c.nroinscripcion,".$objFunciones->getCodCatastral("c.").",c.codcliente,
            c.propietario,tc.descripcioncorta || ' ' ||cl.descripcion || '#' || c.nrocalle,
            s.descripcion as sector,e.descripcion, 1
            from catastro.clientes as c
            inner join public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
            inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
            inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio) ";
	
	$SqlPag = "SELECT  COUNT(*)
            from catastro.clientes as c
            inner join public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
            inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
            inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio) ";
			
	$SqlPag = eregi_replace("[\n\r\n\r]", ' ', $SqlPag);

  //$Sql= "SELECT * FROM catastro.view_index_catastroclientes AS w  ";

  $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  //$FormatoGrilla[1] = array('1'=>'c.nroinscripcion', '2'=>'c.codcliente','3'=>'c.propietario', '4'=>'c.codantiguo','5'=>'cl.descripcion','6'=>'tc.descripcioncorta','7'=>'c.nrocalle');
  $FormatoGrilla[1] = array('1'=>'c.nroinscripcion', '2'=>'c.codcliente','3'=>'c.propietario','4'=>'c.nrodocumento','5'=>'c.codantiguo');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Nro. Inscripcion','T2'=>'Cod Catastral','T3'=>'Cod. Cliente','T4'=>'Usuario','T5'=>'Direcci&oacute;n','T6'=>'Sector','T7'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W3'=>'95');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  AND  (c.codemp=1 and c.codsuc=".$codsuc.$and." ) ORDER BY codcatastro desc ";   
  //$FormatoGrilla[8] = "  AND  (w.codsuc=".$codsuc." ) ORDER BY w.nroinscripcion desc ";                                //Orden de la Consulta
  if($Op!=5)
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
  else
    {
      $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
      $FormatoGrilla[5] = array('W1'=>'60','W6'=>'80','W11'=>'20','W2'=>'70','W3'=>'70','W10'=>'90'); 
   }    
  $FormatoGrilla[10] = array(array('Name' =>'id','Col'=>1));//DATOS ADICIONALES
  $FormatoGrilla[100] = $SqlPag;
    
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],1000,600);
  Pie();
 

?>
<script type="text/javascript">
function ValidarEnter(e, Op)
        {
        switch(e.keyCode)
            {   
                case $.ui.keyCode.UP: 
                    for (var i = 10; i >= 1; i--) 
                        {
                            if ($('tr[tabindex=\'' + i  + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' +i  + '\']')
                                if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                                {
                                  $('tr[tabindex=\'' + i  + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }
                               
                             }
                        };
                        $('#Valor').focus().select();
                break;
                case $.ui.keyCode.DOWN:

                       for (var i = 1; i <= 10; i++) 
                        {
                            if ($('tr[tabindex=\'' + i  + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' +i  + '\']')
                                if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                                {
                                  $('tr[tabindex=\'' + i  + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }
                               
                             }
                        };

                        $('#Valor').focus().select();
                break;
                default:  if (VeriEnter(e)) Buscar(Op);
                
                
            }
             
        
            //ValidarEnterG(evt, Op)
        }
$("#BtnNuevoB").remove();
function Enviar(obj)
  {
    var Id = $(obj).parent().parent().data('id')
    opener.Recibir(Id);
    window.close();
  }
</script>
<?php if($Op!=5) CuerpoInferior(); ?>