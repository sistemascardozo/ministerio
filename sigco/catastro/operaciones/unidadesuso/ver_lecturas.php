<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	
	$paramae = $objFunciones->getParamae("LIMLEC", $codsuc);
	
?>
<style>
	.text1{
		font-size:11px;
		font-weight:bold;
		color:#000;
	}
	.text2{
		font-size:11px;
		font-weight:bold;
		color:#800000;
	}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr style="padding-top:3px">
    <td>
    <div id="div_lecturas" style="overflow:auto; height:180px">
        <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tblecturas" rules="all" >
				<thead class="ui-widget-header" >
            <tr  align="center">
              <th width="20%" >Mes y A&ntilde;o</th>
              <th width="16%" >Fecha de Lectura</th>
              <th width="14%" >Lect. Ultima</th>
              <th width="11%" >Lect. Anterior</th>
              <th width="10%" >Consumo</th>
              <th width="10%" >Lect. Promedio</th>
              <th width="13%" >Consumo Fact.</th>
            </tr>
        </thead>
        <tbody>
            <?php 
				$count		= 0;
				$promedio 	= 0;
				
				$limit = "limit ".$paramae["valor"];
				
				$sqlLecturas = "select anio,mes,fechalectultima,lecturaultima,lecturaanterior,consumo,lecturapromedio,consumofact
								from facturacion.cabfacturacion 
								where codemp=1 and codsuc=? and nroinscripcion=? order by anio,mes desc ".$limit;

				$consultaLecturas = $conexion->prepare($sqlLecturas);
				$consultaLecturas->execute(array($codsuc,$nroinscripcion));
				$itemL = $consultaLecturas->fetchAll();

				foreach($itemL as $rowL)
				{
					if($rowL["consumo"]>0)
					{
						$promedio+=$rowL["consumo"];
						$count++;
					}
			?>
                <tr style="color: black; background-color:#FFF; font-weight: bold; font-size: 11px" align="center">
                  <th ><?=$meses[$rowL["mes"]]." - ".$rowL["anio"]?></th>
                  <th ><?=$objFunciones->DecFecha($rowL["fechalectultima"])?></th>
                  <th ><?=$rowL["lecturaultima"]?></th>
                  <th ><?=$rowL["lecturaanterior"]?></th>
                  <th ><?=$rowL["consumo"]?></th>
                  <th ><?=$rowL["lecturapromedio"]?></th>
                  <th ><?=$rowL["consumofact"]?></th>
                </tr>
            <?php } ?>
         </tbody>
        </table>
    </div>    </td>
  </tr>
  <tr style="padding:4px" >
    <td>Promedio : 
      <input type="text" name="lectpromedio" id="lectpromedio" class="inputtext" value="<?=$count!=0?round($promedio/$count,0):0?>" />
    </td>
  </tr>
</table>
