<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("clsactualizacion.php");
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones   = new clsFunciones();
	
	$nItems         = $_SESSION["oactualizacion"]->item;
	$codemp         = 1;
	$idusuario      = $_SESSION['id_user']; //codusuario
	$codsuc         = $_SESSION['IdSucursal'];//codsuc
	$nroinscripcion = $_POST["nroinscripcion"];//nroinscripcion
	$observacion    = $_POST["obs"];
	$consumo        = $_POST["consumo"];
	$existe         = 0;
	$valorultimo    = 0;
	$cont           = 1;


	//Selecciona el maximo numero id de modificaciones y lo aumenta en 1
	$consultaM = $conexion->prepare("select max(nromodificaciones) from catastro.modificaciones WHERE codsuc=".$codsuc);
	$consultaM->execute();
	if($rowM = $consultaM->fetch()){$correlativo = $rowM[0];}
	$correlativo++; // el numero de modificacion maximo mas 1

	for($i=0;$i<$nItems;$i++){

		$campo 	= $_SESSION["oactualizacion"]->campo[$i];
		$valor 	= $_SESSION["oactualizacion"]->valor[$i];
		$tipo	= $_SESSION["oactualizacion"]->tipo[$i];

		if ($campo == "consumo") {

			if($tipo==0)
			{
				$sql = "select ".$campo." from catastro.clientes where codemp=? and codsuc=? and nroinscripcion=?";
			}else{
				$sql = "select ".$campo." from catastro.conexiones where codemp=? and codsuc=? and nroinscripcion=?";
			}

			$consultaF = $conexion->prepare($sql);
			$consultaF->execute(array($codemp,$codsuc,$nroinscripcion));
			$items = $consultaF->fetch();
			$campoold	= $campo;
			$valorold	= $items[0];
			
			$correlativo++;
			
			$instModificaciones  = "insert into catastro.modificaciones(codemp,codsuc,nroinscripcion,nromodificaciones,campo,
									valoractual,valorultimo,observacion,creador) values(:codemp,:codsuc,:nroinscripcion,:nromodificaciones,
									:campo,:valoractual,:valorultimo,:observacion,:creador)";
			$result = $conexion->prepare($instModificaciones);
			$result->execute(array(":codemp"=>$codemp,
									":codsuc"=>$codsuc,
									":nroinscripcion"=>$nroinscripcion,
									":nromodificaciones"=>$correlativo,
									":campo"=>$campoold,
									":valoractual"=>$valorold,
									":valorultimo"=>$valor,
									":observacion"=>$observacion,
									":creador"=>$idusuario));
			
			$campo 	= $_SESSION["oactualizacion"]->campo[$i];

			$existe = 1;
		}
	}

	// Seleccionar el valor actual y la concatenacion de la misma del tarifario
	$consulta = $conexion->prepare("SELECT uc.catetar as codtarifa, t.nomtar as tarifa, uc.porcentaje as porcentaje, uc.principal as estado 
									FROM catastro.unidadesusoclientes uc 
									INNER JOIN facturacion.tarifas t ON (uc.catetar = t.catetar AND uc.codemp = t.codemp AND uc.codsuc = t.codsuc)
									WHERE uc.codsuc=? AND uc.nroinscripcion=?");
	$consulta->execute(array($codsuc,$nroinscripcion));
	$rows = $consulta->fetchAll();

	foreach ($rows as $key) {

		if ($cont == 1) {
			$valorultimo  = substr($key['tarifa'],0,3);
			$valorultimo .= "(".$key['porcentaje']. "-".$key['estado'].")";
			$cont++;
		} else {
			$valorultimo .= "-" . substr($key['tarifa'],0,3);
			$valorultimo .= "(".$key['porcentaje']. "-".$key['estado'].")"; 
		}
	}

	$del = "delete from catastro.unidadesusoclientes where codemp=? and codsuc=? and nroinscripcion=?";
	$result = $conexion->prepare($del);
	$result->execute(array($codemp,$codsuc,$nroinscripcion));

	$count_unidades = $_POST["cont_unidades"];
	
	$catetar    = 0;			
	$domestico  = 0;
	$social     = 0;
	$comercial  = 0;
	$estatal    = 0;
	$industrial = 0;
	$cont       = 1;
			
	for($i=1;$i<=$count_unidades;$i++)
	{
		if(isset($_POST["tarifas".$i]))
		{
			if($_POST["principal".$i]==1){$catetar=$_POST["tarifas".$i];}
			
			$sqlT = "SELECT t.catetar as codcategoriatar, 
					uc.catetar as codtarifa, 
					t.nomtar as tarifa
					FROM catastro.unidadesusoclientes uc 
					INNER JOIN facturacion.tarifas t ON (uc.catetar = t.catetar AND uc.codemp = t.codemp AND uc.codsuc = t.codsuc) 
					where uc.codemp=? and uc.codsuc=? and uc.catetar=?";
			
			$result = $conexion->prepare($sqlT);
			$result->execute(array($codemp,$codsuc,$_POST["tarifas".$i]));
			$itemsT = $result->fetch();
			
			if($itemsT["codcategoriatar"]==1){$domestico++;}
			if($itemsT["codcategoriatar"]==2){$comercial++;}
			if($itemsT["codcategoriatar"]==3){$industrial++;}
			if($itemsT["codcategoriatar"]==4){$estatal++;}
			if($itemsT["codcategoriatar"]==5){$social++;}
			
			$instUnidades = "insert into catastro.unidadesusoclientes(codemp,codsuc,nroinscripcion,catetar,porcentaje,principal, item,codtipoactividad)
							values(:codemp,:codsuc,:nroinscripcion,:catetar,:porcentaje,:principal,:item,:codtipoactividad)";
							
			$result = $conexion->prepare($instUnidades);
			$result->execute(array(":codemp"=>$codemp,
									":codsuc"=>$codsuc,
									":nroinscripcion"=>$nroinscripcion,
									":catetar"=>$_POST["tarifas".$i],
									":porcentaje"=>$_POST["porcentaje".$i],
									":principal"=>$_POST["principal".$i],
									":item"=>$i,
									":codtipoactividad"=>$_POST["codtipoactividad".$i],
									));

			if ($cont == 1) {
				$valoractual  = substr($itemsT['tarifa'],0,3);
				$valoractual .= "(".$_POST["porcentaje".$i]. "-".$_POST["principal".$i].")";
				$cont++;

			} else {
				$valoractual .= "-" . substr($itemsT['tarifa'],0,3);
				$valoractual .= "(".$_POST["porcentaje".$i]. "-".$_POST["principal".$i].")"; 
			}
		}//Fin de if
	
	} //Fin de for

	
	if ($existe == 1) {
		$updClientes = "update catastro.clientes set catetar=:catetar,domestico=:domestico,social=:social,comercial=:comercial,estatal=:estatal,
						industrial=:industrial,consumo=:consumo where codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
		$result = $conexion->prepare($updClientes);
		$result->execute(array(
						":catetar"        =>$catetar,
						":domestico"      =>$domestico,
						":social"         =>$social,
						":comercial"      =>$comercial,
						":estatal"        =>$estatal,
						":industrial"     =>$industrial,
						":codemp"         =>$codemp,
						":codsuc"         =>$codsuc,
						":nroinscripcion" =>$nroinscripcion,
						":consumo"        =>$consumo						
					   )
					);
	}else{

		$updClientes = "update catastro.clientes set catetar=:catetar,domestico=:domestico,social=:social,comercial=:comercial,estatal=:estatal,
						industrial=:industrial where codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
		
		$result = $conexion->prepare($updClientes);
		$result->execute(array(
						":catetar"        =>$catetar,
						":domestico"      =>$domestico,
						":social"         =>$social,
						":comercial"      =>$comercial,
						":estatal"        =>$estatal,
						":industrial"     =>$industrial,
						":codemp"         =>$codemp,
						":codsuc"         =>$codsuc,
						":nroinscripcion" =>$nroinscripcion
					   )
					);

	}

	if(!$result)
	{
		$conexion->rollBack();
		var_dump($result->errorInfo());
		die(2);
		exit;
	};

	$correlativo++; 
	$instModificaciones  = "insert into catastro.modificaciones(codemp,codsuc,nroinscripcion,nromodificaciones,campo,
		valoractual,valorultimo,observacion,creador) values(:codemp,:codsuc,:nroinscripcion,:nromodificaciones,
		:campo,:valoractual,:valorultimo,:observacion,:creador)";
	$result1 = $conexion->prepare($instModificaciones);
	$result1->execute(array(":codemp"=>$codemp,
								":codsuc"=>$codsuc,
								":nroinscripcion"=>$nroinscripcion,
								":nromodificaciones"=>$correlativo,
								":campo"=>'catetar',
								":valoractual"=>$valorultimo,
								":valorultimo"=>$valoractual,
								":observacion"=>$observacion,
								":creador"=>$idusuario));
	
	if(!$result1)
	{
		$conexion->rollBack();
		var_dump($result->errorInfo());
		die(2);
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
		$IdControl = isset($nroinscripcion)?$nroinscripcion:$_POST["1form1_id"];
		
		$SqlControl = "INSERT INTO admin.control (codsuc, tabla, id, operacion) ";
		$SqlControl .= "VALUES(".$_SESSION['IdSucursal'].", 'catastro{}clientes', '".$IdControl."', ".$Op.")";
		$resultControl = $conexion->prepare($SqlControl);
		$resultControl->execute(array());
	}
	
?>

