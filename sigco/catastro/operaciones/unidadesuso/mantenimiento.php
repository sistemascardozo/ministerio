<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");
	
	$Op 				= $_POST["Op"];
	$Id 				= isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc 			= $_SESSION['IdSucursal'];
	$guardar			= "op=".$Op;

	$objMantenimiento 	= new clsDrop();
	unset($_SESSION["oactualizacion"]);
	
	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
	
	$sql = "SELECT c.nroinscripcion,c.codcliente,c.codsector,codmanzanas,c.codcalle,c.lote,c.sublote,c.codestadoservicio,conx.codtiporesponsable,
			c.propietario,c.codtipodocumento,c.nrodocumento,c.nrocalle,conx.altocon,conx.codtipoentidades,c.codtipousuario,c.direcciondistribucion,
			conx.inquilino,c.codtipoactividad,c.codciclo,conx.tipopredio,conx.codtipoconstruccion,conx.nropisos,conx.codtipoabastecimiento,conx.piscina,
			conx.codtipoalmacenaje,c.codtiposervicio,conx.referencia,conx.codtipomaterialagua,conx.codlocacajaagua,conx.coddiametrosagua,conx.codtipopavimentoagua,
			conx.codtipocorte,conx.codestadoconexion,conx.codtipovereda,conx.fechainsagua,conx.codtipocajaagua,conx.codestadocajaagua,conx.codtipotapaagu,
			conx.estadotapaagu,conx.codtipofugasagua,conx.codmarca,conx.coddiametrosmedidor,conx.codestadomedidor,conx.posicionmed,conx.codmodelo,
			conx.tipolectura,conx.aniofabmed,conx.codtipomedidor,conx.fechainsmed,conx.codcapacidadmedidor,c.nromed,c.tipofacturacion,conx.codubicacion,
			conx.coddiametrosdesague,conx.codtipomaterialdesague,conx.codlocacajadesague,conx.fechainsdesague,conx.codtipocajadesague,conx.codestadocajadesague,
			conx.codtipotapadesague,conx.estadotapadesague,conx.fugasatoros,conx.codzonaabas,conx.presionagua,conx.horasabastecimiento,conx.nrolavatorios,
			conx.nrolavaropas,conx.nrowater,conx.nroduchas,conx.nrourinarios,conx.nrogrifos,conx.nrohabitantes,conx.observacionabast,c.lecturaanterior,
			c.lecturaultima,c.consumo,c.fechalecturaanterior,c.fechalecturaultima,tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,/*t.nomtar*/ '' as nomtar ,
			e.descripcion as estser,ts.descripcion as tservicio,c.latitud,c.longitud,c.correo,conx.codtipomatconstruccion,c.nroorden,
            c.lecturapromedio,c.codrutlecturas,c.codzona,conx.conpozo,conx.conpozoimporte,c.codantiguo
			FROM catastro.clientes as c 
			inner join catastro.conexiones as conx on(c.codemp=conx.codemp and c.codsuc=conx.codsuc 
			and c.nroinscripcion=conx.nroinscripcion)
			INNER JOIN public.calles as cl on(c.codemp= cl.codemp AND c.codsuc= cl.codsuc AND c.codcalle= cl.codcalle AND c.codzona=cl.codzona)            
			INNER join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
			INNER join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
			INNER join public.tiposervicio as ts on(c.codtiposervicio=ts.codtiposervicio)
			where c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";
	//echo $codsuc."-".$Id;
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$Id));
	$items = $consulta->fetch();
//var_dump($consulta->errorInfo());
	$guardar		= $guardar."&Id2=".$Id;

	$direccion		= $items["descripcioncorta"]." ".$items["calle"]." ".$items["nrocalle"];
	$servicio		= $items["tservicio"];
	$facturacion	= $tfacturacion[$items["tipofacturacion"]];
	
	$fechinstagua  	= new DateTime($items["fechainsagua"]);
	$fechainstagua	= $fechinstagua->format("d/m/Y");
	
	$fechinstmedidor  	= new DateTime($items["fechainsmed"]);
	$fechainstmedidor	= $fechinstmedidor->format("d/m/Y");
	
	$fechinstdesague	= new DateTime($items["fechainsdesague"]);
	$fechainstdesague	= $fechinstdesague->format("d/m/Y");
	
	$fechlectanterior	= new DateTime($items["fechalecturaanterior"]);
	$fechalectanterior	= $fechlectanterior->format("d/m/Y");
	
	$fechlectultima		= new DateTime($items["fechalecturaultima"]);
	$fechalectultima	= $fechlectanterior->format("d/m/Y");
	 $conpozo= $items['conpozo']?$items['conpozo']:0;
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_actualizacion.js" language="JavaScript"></script>
<script>
  	function buscar_calle()
    {
        object = "calles"
        var codzona= $("#codzona").val()
        var codsector= $("#sector").val()
        if(codzona==0)
        {
            alert('Seleccione una Zona');
            return false;
        }
        
        if(codsector==0)
        {
            alert('Seleccione un Sector');
            return false;
        }
        AbrirPopupBusqueda("../../../../buscar/calles.php?codzona="+codzona+"&codsector="+codsector+"&codsuc="+codsuc, 700, 450)
    }
    function recibir_calles(Id,Des)
    {
        $("#calles").val(Id)
        $("#calles").prop('selected', true)
        // $("#calles").val(Des)
    }
    function buscar_actividad()
	{
	    AbrirPopupBusqueda("../../../../buscar/actividad.php", 700, 450)
	}
	function recibir_actividad(Id)
	{
	        $("#tipoactividad").val(Id)
	        $("#tipoactividad").prop('selected', true)
	}
  
</script>
<style type="text/css">

#tbCampos * {
    font-size: 10px;
}
</style>
<script>
	var codsuc 	= <?=$codsuc?>;
	var urldir	= '<?php echo $_SESSION['urldir'];?>';
	VImportePozo(<?=$conpozo?>);
	$(document).ready(function() {
		$( "#tabs" ).tabs();
        
	  $('form').keypress(function(e){   
		if(e == 13){
		  return false;
		}
	  });

	  $('input').keypress(function(e){
		if(e.which == 13){
		  return false;
		}
	  });

	});
	
	
	$(document).ready(function(){
		
		//$( "#dialog:ui-dialog" ).dialog( "destroy" );

		$( "#DivLecturas" ).dialog({
			autoOpen: false,
			height: 350,
			width: 550,
			modal: true,
/*			show: "blind",
			hide: "scale",*/
			resizable:true,
			buttons: {
				"Aceptar": function() {
					if(objIdx=="promediar")
					{
						$("#lecturaultima").val($("#lectpromedio").val())
						$("#consumo").val($("#lectpromedio").val())
						$("#lecturapromedio").val($("#lectpromedio").val())
						//calcularconsumo($("#lectpromedio").val())
					}else{
						if($("#obstem").val()=='')
						{
							Msj($("#obstem"),"Digite el Motivo de Modificación")
							return false;
						}
						$("#obs").val($("#obstem").val())
						GuardarP(1)
					}
					$( this ).dialog( "close" );
				},
				"Cancelar": function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {

			}
		});
   })
		
	function ValidarForm(Op)
	{
		if($("#cCatastro").val()>0 && <?=$Op?>!=2)
		{
			alert("El Codigo Catastral ya Existe")
			return false
		}
		// if($("#contmod").val()==0)
		// {
		// 	alert('No se ha Realizado Ninguna Modificacion');
		// 	return false;
		// }		
		c=0;
		$("#tbunidades input.principal").each(function(){
			if($(this).val()==1)
				c++;
		});
		if(c==0)
		{
			$("#tabs").tabs({ selected:6 });

			Msj('#tbunidades','Agrege Categoria Principal')
          	return false;
		}
		$.ajax({
		 	url:'observacion.php',
		 	type:'POST',
		 	async:true,
		 	data:'',
		 	success:function(datos){
				$("#div_lecturas").html(datos)
				$( "#DivLecturas" ).dialog( "open" )
			 }
		}) 
		objIdx = "guardar";
		return false
	}
	
function Cancelar()
{
	location.href='index.php';
}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   
   <tbody>

    <tr>
    	<td colspan="3" class="TitDetalle">
        	<div id="tabs">
                <ul>
<!--                     <li style="font-size:10px"><a href="#tabs-2">Datos del Cliente</a></li>
                    <li style="font-size:10px"><a href="#tabs-3">Inmueble Predio</a></li>
                    <li style="font-size:10px"><a href="#tabs-4">Conexion de Agua</a></li>
                    <li style="font-size:10px"><a href="#tabs-5">Medidor</a></li>
                    <li style="font-size:10px"><a href="#tabs-6">Desague</a></li>
                    <li style="font-size:10px"><a href="#tabs-7">Cal. de Servicio</a></li>
 -->                    <li style="font-size:10px"><a href="#tabs-2">Und. de Uso</a></li>
                </ul>
                <div id="tabs-2" style="height:350px">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="8%">&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td width="69%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td width="20%" align="right">Categoria</td>
                      <td width="3%" align="center">:</td>
                      <td>
                      	<? $objMantenimiento->drop_tarifas($codsuc); ?>
                        <input type="checkbox" name="principal_categoria_check" id="principal_categoria_check" onclick="CambiarEstado(this,'principal_categoria')"  />
                      	Categoria Principal
                      <input type="hidden" name="principal_categoria" id="principal_categoria" value="0" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="right">Actividad</td>
                        <td align="center">:</td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <div id="div_rutasdistribucion_2">
                                        <?php $objMantenimiento->drop_tipo_actividad($items["codtipoactividad"]); ?>
                                        </div>
                                        
                                    </td>
                                    <td>
                                        <span class="MljSoft-icon-buscar" title="Buscar Actividad" onclick="buscar_actividad();"></span>                                      
                                    </td>
                                </tr>
                            </table>
                        </td>
                        
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td align="right">Porcentaje</td>
                      <td align="center">:</td>
                      <td><input type="text" name="procentaje" id="procentaje" class="inputtext" onkeypress="return permite(event,'num')" value="100" />
                        <img src="../../../../images/iconos/add.png" style="cursor: pointer" onclick="agregar_unidades_uso(<?=$codsuc?>);" /></td>
                    </tr>
                    <tr>
                      <td colspan="4">
                      	<input name="cont_unidades" id="cont_unidades" type="hidden" value="0" />
                      	<input type="hidden" name="total_porct" id="total_porct" value="0" /></td>
                    </tr>
                    <tr>
                      <td colspan="4">
                      <div style="overflow:auto; height: 200px">
                        <table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tbunidades" >
          					<thead class="ui-widget-header" style="font-size: 11px">
          						<tr>
                            <th width="12%" >Codigo</th>
                            <th width="25%" >Categoria</th>
                            <th width="35%" >Actividad</th>
                            <th width="15%" >Porcentaje</th>
                            <th width="13%" >Principal</th>
                            <th width="5%" >&nbsp;</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
						  	$cont_unidades 	= 0;
							$tot_porcentaje = 0;
							
						  	if($Id != "")
							{
								$sql_unidades = "select u.catetar,t.nomtar,u.porcentaje,u.principal,
                                                    a.descripcion as actividad,a.codtipoactividad
                                                FROM catastro.unidadesusoclientes as u 
                                                INNER JOIN facturacion.tarifas as t on(u.codemp=t.codemp and u.codsuc=t.codsuc and u.catetar=t.catetar)
                                                INNER JOIN public.tipoactividad as a on(u.codtipoactividad=a.codtipoactividad)
                                                WHERE u.codemp=1 and u.codsuc=? and u.nroinscripcion=?";
								$consulta_unidades = $conexion->prepare($sql_unidades);
								$consulta_unidades->execute(array($codsuc,$Id));
								$items_unidad = $consulta_unidades->fetchAll();
								
								foreach($items_unidad as $rowUnidades)
								{					
									$cont_unidades++;
									
									$tot_porcentaje += $rowUnidades["porcentaje"];
						  ?>
                          <tr style='background-color:#FFFFFF; padding:4px'>
                            <td align="center" >
                            	<input type="hidden" name="ItemD<?=$cont_unidades?>" id="ItemD<?=$cont_unidades?>" value="<?=$cont_unidades?>" />
								<input type='hidden' id='tarifas<?=$cont_unidades?>' name='tarifas<?=$cont_unidades?>' value='<?=$rowUnidades["catetar"]?>' />
                            	<?=$rowUnidades["catetar"]?>
                            </td>
                            <td >
								<input type='hidden' id='porcentaje<?=$cont_unidades?>' name='porcentaje<?=$cont_unidades?>' value='<?=$rowUnidades["porcentaje"]?>' />
                            	<?=strtoupper($rowUnidades["nomtar"])?>
                            </td>
                             <td align="center" >
                                <input type="hidden" name="codtipoactividad<?=$cont_unidades ?>" id="codtipoactividad<?=$cont_unidades ?>" value="<?=$rowUnidades["codtipoactividad"] ?>" />
                                <?=$rowUnidades["actividad"] ?>
                            </td>
                            <td align="center" ><?=number_format($rowUnidades["porcentaje"],0)?></td>
                            <td align="center" >
								<input type='hidden' class='principal' id='principal<?=$cont_unidades?>' name='principal<?=$cont_unidades?>' value='<?=$rowUnidades["principal"]?>' />
                            	<?php //$rowUnidades["principal"]==1?"SI":"NO"?>
                              <input type="radio" name="principalUU" id="principalUU<?=$cont_unidades?>" value="<?=$rowUnidades["codunidad_uso"]?>") <?php if ($rowUnidades["principal"]==1){echo 'checked="checked"';}?> onChange='Numerar(); actualizacion(this, 3);' style="cursor:pointer" />
                            </td>
                            <td align="center" >
                            	<img src='../../../../images/iconos/cancel.png' width='16' height='16' title='Borrar Registro' onclick='QuitaFilaD(this,1,<?=$cont_unidades?>);' style='cursor:pointer'/>
                            </td>
                          </tr>
                          <?php
								}
							}
						  ?>
						  </tbody>
                          <script>
						  		cont_unidad			= <?=$cont_unidades?>;
								cont_fila_unidad 	= <?=$cont_unidades?>;
								total_porcentaje	= <?=$tot_porcentaje?>;
								
								$("#cont_unidades").val(<?=$cont_unidades?>)
								$("#total_porct").val(<?=$tot_porcentaje?>)
								
								$("#procentaje").val(100 - <?=$tot_porcentaje?>)
						  </script>
                         </table>
                       </div>
                       </td>
                    </tr>
                  </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
    	<td>
        	<input type="hidden" name="contmod" id="contmod" value="0" />&nbsp;
            <input type="hidden" name="obs" id="obs" value="" />
            <input type="hidden" name="consumo" id="consumo" value="<?=intval($items["consumo"])?>" />
            <input type="hidden" name="tipofacturacion" id="tipofacturacion" value="<?=$items["tipofacturacion"]?>" />
        	<input type="hidden" class="inputtext"  id="nroinscripcion" name="nroinscripcion" readonly="readonly" maxlentgth="10" size="10" value="<?=$Id?>"/>
        </td>
    </tr>
   </tbody>
   
  </table>
  <div id="DivLecturas" title="Actualizacion y Modificacion Catastral"  >
    <div id="div_lecturas"></div>
  </div>
 </form>
</div>
<div id="DivTarifas" title="Tarifas" style="display: none;">
	<div id="DivTarifasAcordion">
	<?php
	$sql = "select * from facturacion.tarifas WHERE codsuc=".$codsuc." ORDER BY catetar";
	$ConsultaTar =$conexion->query($sql);
	foreach($ConsultaTar->fetchAll() as $tarifas)
	{
	?>
  <h3><?=strtoupper($tarifas["nomtar"])?></h3>
  <div style="height: 100px; display: block;">
    <table width="100%" border="0" align="center" cellspacing="0" class="ui-widget" rules="cols">
	    <thead class="ui-widget-header" style="font-size:10px">
	      <tr>
	        <td>&nbsp;</td>
	        <td align="center">Volumen</td>
	        <td align="center">Importe Agua</td>
	        <td align="center">Importe Desague</td>
	        <td align="center">&nbsp;</td>
	        </tr>
	      </thead>
	      <tr>
	        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Inicial</td>
	        <td align="right">
	          <label class="inputtext" id="hastarango1" ><?=number_format($tarifas["hastarango1"],2)?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsmin" ><?=$tarifas["impconsmin"]?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsmindesa"><?=$tarifas["impconsmindesa"]?></label>
	          </td>
	          <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango1"],0)?>')"></span></td>
	        </tr>
	      <tr>
	        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Intermedio </td>
	        <td align="right">
	          <label class="inputtext" id="hastarango2"><?=number_format($tarifas["hastarango2"],2)?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsmedio"><?=$tarifas["impconsmedio"]?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsmediodesa"><?=$tarifas["impconsmediodesa"]?> </label>
	          </td>
	          <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango2"],0)?>')"></span></td>
	        </tr>
	      <tr>
	        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Final </td>
	        <td align="right">
	          <label class="inputtext" id="hastarango3"><?=number_format($tarifas["hastarango3"],2)?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsexec"><?=$tarifas["impconsexec"]?></label>
	          </td>
	        <td align="right">
	          <label class="inputtext" id="impconsexecdesa"><?=$tarifas["impconsexecdesa"]?></label>
	          </td>
	          <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango3"],0)?>')"></span></td>
	        </tr>
	      </table>
  </div>
  <?php
  	}
  ?>
</div>
</div>
<script>//setTimeout(validar_cod_catastro(),1000)</script>