<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	$TituloVentana = "TRANSFERENCIA DE USUARIOS";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc = $_SESSION['IdSucursal'];
	
	$objMantenimiento = new clsMantenimiento();
	
	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
            if($("#Cont").val()==0)
            {
                alert('No hay Ningun Usuario para Transferir al Padron')
                return false
            }
            transferir();
            detalle_transferencia();
			GuardarP(Op)
	}
	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function detalle_transferencia()
	{
		$("#div_guardar").html('<span class="icono-icon-loading"></span>Cargando....')
		$.ajax({
			 url:'detalle.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>',
			 success:function(datos){
				$("#tbrutas tbody").html(datos)
        $("#div_guardar").html('')
        $("#Cont").val(parseInt($("#tbrutas tbody tr").length))
			 }
		}) 
	}
	function transferir()
	{
		$("#div_guardar").html('<span class="icono-icon-loading"></span>Transfiriendo...')
		$.ajax({
			 url:'guardar.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>',
			 success:function(datos){
				$("#div_guardar").html(datos)
				$("#Cont").val(0)

			 },
       error: function(jqXHR, exception) {
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
   <table width="1100" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
     <tbody>

		<tr>
          <td colspan="3">
            <fieldset>
               <table width="100%" border="0px">
                 <tr>
                   <td width="10%" align="left">Sucursal :</td>
                   <td width="24%" align="left">
                   	<input type="text" class="inputtext"  id="sucursal1" name="sucursal" readonly="readonly" maxlentgth="30" size="30" value="<?=$sucursal["descripcion"]?>"/>
                   </td>
                   <td width="43%" align="left" colspan="2">
                    <input type="button" class="button" value="Mostrar Datos" onclick="detalle_transferencia();" />
                    &nbsp;
                    <input type="button" class="button" value="Transferir" onclick="transferir();" />
                    &nbsp;
                    <div id="div_guardar" style="display:inline"></div>
                  </td>
                   
                 </tr>
               </table>
            </fieldset>
          </td>
		</tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" align="center">
               <div id="div_transferencia" style="height:280px; overflow:auto; text-align:center; vertical-align:middle">
                 <table width="100%" border="1" cellspacing="1" id="tbrutas" style="vertical-align:top" rules="all" class="ui-widget-content" frame="void">
                    <thead class="ui-widget-header" >
                      <tr style="font-size: 10px">
                          <th>Item</th>
                          <th align="center">Nro. Inscripcion</th>
                          <th align="center">Codigo</th>
                          <th align="center">C. Catastral</th>
                          <th align="center">Propietario</th>
                          <th align="center">Direccion</th>
                          <th align="center">Nro. Documento</th>
                          <th align="center">Sector</th>
                          <th align="center">T. Facturacion </th>
                      </tr>
                    <thead>
                    <tbody style="font-size:10px">
                    </tbody>
                </table>
              </div>
            </td>
		</tr>
        <tr>
            <td colspan="3" style="color: blue; font-size: 14px; font-family: comic sans ms; font-weight: bold">
                <input type="hidden" id="cont" name="cont" value="0" />
            </td>
	  	</tr>
      
     </tbody>
  </table>
 </form>
</div>