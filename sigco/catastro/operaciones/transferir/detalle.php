<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../objetos/clsFunciones.php");

	$objFunciones = new clsFunciones();

	$codsuc = $_POST["codsuc"];
	$cont 	= 0;

	$sql = "SELECT c.nroinscripcion, c.codcliente, c.propietario, tc.descripcioncorta, cl.descripcion AS calle, c.nrocalle,
			td.descripcion AS documento, c.nrodocumento, s.descripcion AS sector, c.tipofacturacion
			FROM catastro.catastro c
			 JOIN public.calles cl ON(c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.codcalle = cl.codcalle) AND (c.codzona = cl.codzona)
			 JOIN public.tiposcalle tc ON(cl.codtipocalle = tc.codtipocalle)
			 JOIN public.tipodocumento td ON(c.codtipodocumento = td.codtipodocumento)
			 JOIN public.sectores s ON(c.codemp = s.codemp) AND (c.codsuc = s.codsuc) AND (c.codzona = s.codzona) AND (c.codsector = s.codsector)
			WHERE c.codemp = 1 AND c.codsuc = " . $codsuc . " AND (c.codtipousuario = 1 OR c.codtipousuario = 5 ) AND c.nuevousuario = 0 ";

	$consulta = $conexion->query($sql);
	//$consulta->execute(array($codsuc));
	$items = $consulta->fetchAll();

?>

    <?php
		foreach($items as $row)
        {
            $cont++;
    ?>
       <tr>
        <td align="center"><?=$cont?></td>
        <td align="center"><?=$row["nroinscripcion"]?></td>
        <td align="center"><?=$row["codcliente"]?></td>
        <td align="center"><?=$objFunciones->getCodCatastral_catastro($row["nroinscripcion"])?></td>
        <td align="left"><?=$row["propietario"]?></td>
        <td align="left"><?=strtoupper($row["descripcioncorta"]." ".$row["calle"]." ".$row["nrocalle"])?></td>
       <!--  <td align="center"><?=$row["documento"]?></td> -->
        <td align="center"><?=$row["nrodocumento"]?></td>
        <td align="center"><?=strtoupper($row["sector"])?></td>
        <td align="center"><?=$tfacturacion[$row["tipofacturacion"]]?></td>
      </tr>
    <?php } ?>
