<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../objetos/clsFunciones.php');

	$conexion->beginTransaction();

	$codemp		= 1;
	$codsuc 	= $_POST["codsuc"];
	$idusuario	= $_SESSION['id_user'];

	$objMantenimiento 	= new clsFunciones();

	$sql = "SELECT * FROM catastro.catastro ";
	$sql .= "WHERE codemp = 1 AND codsuc = ? AND (codtipousuario = 1 OR codtipousuario = 5) AND nuevousuario = 0 ";
	$sql .= "ORDER BY nroinscripcion DESC";

	$result = $conexion->prepare($sql);
	$result->execute(array($codsuc));
	$itemsC = $result->fetchAll();

	foreach($itemsC as $rowC)
	{
		//-----Pasa al Catalogo de Usuarios------
		//echo $rowC["nroinscripcion"]."<br>";
		/*$sqlC = "insert into catastro.clientes(nroinscripcion,codemp,codsuc,codcliente,codsector,codmanzanas,lote,sublote,
						propietario,codcalle,nrocalle,codtiposervicio,codtipousuario,direcciondistribucion,codtipodocumento,
						nrodocumento,codestadoservicio,codtipoactividad,tipofacturacion,nromed,codusu,codrutlecturas,codrutdistribucion,
						codciclo,exoneralac,nroorden,lecturaanterior,lecturaultima,consumo) values(:nroinscripcion,:codemp,:codsuc,:codcliente,:codsector,:codmanzanas,:lote,:sublote,
						:propietario,:codcalle,:nrocalle,:codtiposervicio,:codtipousuario,:direcciondistribucion,:codtipodocumento,
						:nrodocumento,:codestadoservicio,:codtipoactividad,:tipofacturacion,:nromed,:codusu,:codrutlecturas,:codrutdistribucion,
						:codciclo,:exoneralac,:nroorden,:lecturaanterior,:lecturaultima,:consumo)";


		$result = $conexion->prepare($sqlC);
		$result->execute(array(":nroinscripcion"=>$rowC["nroinscripcion"],
							   ":codemp"=>$codemp,
							   ":codsuc"=>$codsuc,
							   ":codcliente"=>$rowC["codcliente"],
							   ":codsector"=>$rowC["codsector"],
							   ":codmanzanas"=>$rowC["codmanzanas"],
							   ":lote"=>$rowC["nrolote"],
							   ":sublote"=>$rowC["sublote"],
							   ":propietario"=>$rowC["propietario"],
							   ":codcalle"=>$rowC["codcalle"],
							   ":nrocalle"=>$rowC["nrocalle"],
							   ":codtiposervicio"=>$rowC["codtiposervicio"],
							   ":codtipousuario"=>$rowC["codtipousuario"],
							   ":direcciondistribucion"=>$rowC["direcciondistribucion"],
							   ":codtipodocumento"=>$rowC["codtipodocumento"],
							   ":nrodocumento"=>$rowC["nrodocumento"],
							   ":codestadoservicio"=>$rowC["codestadoservicio"],
							   ":codtipoactividad"=>$rowC["codtipoactividad"],
							   ":tipofacturacion"=>$rowC["tipofacturacion"],
							   ":nromed"=>$rowC["nromed"],
							   ":codusu"=>$idusuario,
							   ":codrutlecturas"=>$rowC["codrutlecturas"],
							   ":codrutdistribucion"=>$rowC["codrutdistribucion"],
							   ":codciclo"=>$rowC["codciclo"],
							   ":exoneralac"=>$rowC["desagueinactivo"],
								":nroorden"=>$rowC["nroorden"],
								":lecturaanterior"=>$rowC["lecturaanterior"],
								":lecturaultima"=>$rowC["lecturaultima"],
								":consumo"=>$rowC["consumo"]
							   ));
			*/

			//VALIDAR SI YA EXITE EL CODIGO CATASTRAL
			$Sql = "SELECT propietario, ".$objMantenimiento->getCodCatastral("c.")." ";
			$Sql .= "FROM catastro.clientes c ";
			$Sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
			$Sql .= " AND codsector = ".$rowC["codsector"]." AND codmanzanas = '".$rowC["codmanzanas"]."' AND lote = '".$rowC["nrolote"]."' AND sublote = '".$rowC["sublote"]."' ";

			if($conexion->query($Sql)->rowCount()>0)
			{
				$row = $conexion->query($Sql)->fetch();
				$data['propietario'] = $row['propietario'];

				die('Exite Usuario con Catastro '.$row[1]." (".$row['propietario'].")");
				//$data['resultado']= 1;
			}

			$sqlC = "INSERT INTO catastro.clientes(nroinscripcion, codemp, codsuc, codcliente, codsector, codmanzanas, lote, sublote, ";
			$sqlC .= " propietario, codcalle, nrocalle, codtiposervicio, codtipousuario, direcciondistribucion, codtipodocumento,
						nrodocumento, codestadoservicio, codtipoactividad, tipofacturacion, nromed, codusu, codrutlecturas,codrutdistribucion,
						codciclo,exoneralac,nroorden,lecturaanterior,lecturaultima,consumo,codzona,correo,codantiguo,codestadomedidor, conpozo,
						conpozoimporte, codurbanizacion, codtipoentidades, nrocontrato)
						VALUES(".$rowC["nroinscripcion"].",".$codemp.",".$codsuc.",".$rowC["codcliente"].",".$rowC["codsector"].",'".$rowC["codmanzanas"]."',
						'".$rowC["nrolote"]."','".$rowC["sublote"]."',
						'".$rowC["propietario"]."',".$rowC["codcalle"].",'".$rowC["nrocalle"]."',".$rowC["codtiposervicio"].",".$rowC["codtipousuario"].",
						'".$rowC["direcciondistribucion"]."',".$rowC["codtipodocumento"].",
						'".$rowC["nrodocumento"]."',".$rowC["codestadoservicio"].",".$rowC["codtipoactividad"].",".$rowC["tipofacturacion"].",
						'".$rowC["nromed"]."',".$idusuario.",".$rowC["codrutlecturas"].",".$rowC["codrutdistribucion"].",
						".$rowC["codciclo"].",".$rowC["desagueinactivo"].",".$rowC["nroorden"].",".$rowC["lecturaanterior"].",
						".$rowC["lecturaultima"].",".$rowC["consumo"].",".$rowC["codzona"].",'".$rowC['correo']."','".$rowC['codantiguo']."','".$rowC['codestadomedidor']."','".$rowC['conpozo']."',
						'".$rowC['conpozoimporte']."','".$rowC["codurbanizacion"]."','".$rowC["codtipoentidades"]."', '".$rowC["nrocontrato"]."')";


		$result = $conexion->prepare($sqlC);
		$result->execute();
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error Cliente ".$sqlC;
				die($mensaje);
			}
			//-----Pasa a la Tabla de Conexion------

			$sqlConx = "INSERT INTO catastro.conexiones(nroinscripcion, codemp, codsuc, referencia, tipopredio, ";
			$sqlConx .= " codtipoconstruccion, nropisos, codtipoabastecimiento, piscina, ";
			$sqlConx .= " codtipoalmacenaje, codtiporesponsable, inquilino, altocon, ";
			$sqlConx .= " codtipoentidades, coddiametrosagua,codtipomaterialagua,codlocacajaagua,codtipopavimentoagua,codtipocorte,codestadoconexion,
						codestadocajaagua,codubicacion,codtipovereda,fechainsagua,codtipofugasagua,codtipocajaagua,codtipotapaagu,
						estadotapaagu,codmarca,coddiametrosmedidor,codestadomedidor,posicionmed,tipolectura,fechainsmed,codmodelo,
						aniofabmed,codtipomedidor,codcapacidadmedidor,coddiametrosdesague,codtipomaterialdesague,codlocacajadesague,
						fechainsdesague,codtipocajadesague,codestadocajadesague,codtipotapadesague,estadotapadesague,fugasatoros,
						presionagua,horasabastecimiento,nrohabitantes,observacionabast,codzonaabas,nrolavatorios,nrolavaropas,
						nrowater,nroduchas,nrourinarios,nrogrifos,codtipomatconstruccion,codzona,conpozo,conpozoimporte,codurbanizacion
						) values(".$rowC["nroinscripcion"].", ".$codemp.", ".$codsuc.", '".$rowC["referencia"]."', ".$rowC["tipopredio"].",
						".$rowC["codtipoconstruccion"].", ".$rowC["nropisos"].", ".$rowC["codtipoabastecimiento"].", ".$rowC["piscina"].",
						".$rowC["codtipoalmacenaje"].", ".$rowC["codtiporesponsable"].", '".$rowC["inquilino"]."', ".$rowC["altocon"].",
						".$rowC["codtipoentidades"].", ".$rowC["coddiametrosagua"].", ".$rowC["codtipomaterialagua"].",
						".$rowC["codlocacajaagua"].", ".$rowC["codtipopavimentoagua"].",
						".$rowC["codtipocorte"].", ".$rowC["codestadoconexion"].", ".$rowC["codestadocajaagua"].", ".$rowC["codubicacion"].",
						".$rowC["codtipovereda"].", '".$rowC["fechainsagua"]."', ".$rowC["codtipofugasagua"].",
						".$rowC["codtipocajaagua"].", ".$rowC["codtipotapaagu"].", ".$rowC["estadotapaagu"].", ".$rowC["codmarca"].",
						".$rowC["coddiametrosmedidor"].", ".$rowC["codestadomedidor"].",
						".$rowC["posicionmed"].", ".$rowC["tipolectura"].", '".$rowC["fechainsmed"]."', ".$rowC["codmodelo"].", '".$rowC["aniofabmed"]."',
						".$rowC["codtipomedidor"].", ".$rowC["codcapacidadmedidor"].", ".$rowC["coddiametrosdesague"].",
						".$rowC["codtipomaterialdesague"].", ".$rowC["codlocacajadesague"].", '".$rowC["fechainsdesague"]."', ".$rowC["codtipocajadesague"].",
						".$rowC["codestadocajadesague"].", ".$rowC["codtipotapadesague"].", ".$rowC["estadotapadesague"].", ".$rowC["fugasatoros"].",
						".$rowC["presionagua"].", '".$rowC["horasabastecimiento"]."', ".$rowC["nrohabitantes"].", '".$rowC["observacionabast"]."',
						".$rowC["codzonaabas"].", ".$rowC["nrolavatorios"].", ".$rowC["nrolavaropas"].", ".$rowC["nrowater"].", ".$rowC["nroduchas"].",
						".$rowC["nrourinarios"].", ".$rowC["nrogrifos"].", ".$rowC["codtipomatconstruccion"].", ".$rowC["codzona"].", ".$rowC["conpozo"].",
						'".$rowC["conpozoimporte"]."', ".$rowC["codurbanizacion"].")";

			$result = $conexion->prepare($sqlConx);
			$result->execute(array());

			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error Conexxion";
				die($mensaje);
			}

			//-----Pasa las Unidades de Uso al Catalogo------

			$catetar = 0;

			$domestico 	= 0;
			$social 	= 0;
			$comercial	= 0;
			$estatal	= 0;
			$industrial	= 0;

			$sqlUnidades = "SELECT u.catetar, t.codcategoriatar, u.porcentaje, u.principal, u.codtipoactividad ";
			$sqlUnidades .= "FROM catastro.unidadesusocatastro u ";
			$sqlUnidades .= " INNER JOIN facturacion.tarifas t ON(u.codemp = t.codemp AND u.codsuc = t.codsuc AND u.catetar = t.catetar) ";
			$sqlUnidades .= "WHERE u.codemp = ".$codemp." ";
			$sqlUnidades .= " AND u.codsuc = ".$codsuc." ";
			$sqlUnidades .= " AND u.nroinscripcion = ".$rowC["nroinscripcion"]." ";
			$sqlUnidades .= " AND t.estado = 1 ";

			$result = $conexion->prepare($sqlUnidades);
			$result->execute(array());

			$itemUnidades = $result->fetchAll();

			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error select  unidadesusocatastro";
				die($mensaje);
			}
			$cunid = 0;
			foreach($itemUnidades as $rowUnidades)
			{
				$cunid++;
				if($rowUnidades["principal"]==1){$catetar=$rowUnidades["catetar"];}

				if($rowUnidades["codcategoriatar"]==1){$domestico++;}
				if($rowUnidades["codcategoriatar"]==2){$social++;}
				if($rowUnidades["codcategoriatar"]==3){$comercial++;}
				if($rowUnidades["codcategoriatar"]==4){$estatal++;}
				if($rowUnidades["codcategoriatar"]==5){$industrial++;}

				$instUnidades = "insert into catastro.unidadesusoclientes(codemp,codsuc,nroinscripcion,catetar,porcentaje,principal,item,codtipoactividad)
								values(:codemp,:codsuc,:nroinscripcion,:catetar,:porcentaje,:principal,:item,:codtipoactividad)";

				$result = $conexion->prepare($instUnidades);
				$result->execute(array(":codemp"=>$codemp,
											   ":codsuc"=>$codsuc,
											   ":nroinscripcion"=>$rowC["nroinscripcion"],
											   ":catetar"=>$rowUnidades["catetar"],
											   ":porcentaje"=>$rowUnidades["porcentaje"],
											   ":principal"=>$rowUnidades["principal"],
											   ":codtipoactividad"=>$rowUnidades["codtipoactividad"],
											   ":item"=>$cunid));
				if($result->errorCode()!='00000')
				{
					$conexion->rollBack();
					$mensaje = "Error unidadesusoclientes";
					die($mensaje);
				}
			}

			//------recupera el consumo esperado de la categoria principal y actualiza la tabla clientes-------------------------------------------------
			$sqlTarifas = "SELECT volumenesp FROM facturacion.tarifas WHERE codemp = 1 AND codsuc = ? AND catetar = ? AND estado = 1 ";

			$result = $conexion->prepare($sqlTarifas);
			$result->execute(array($codsuc,$catetar));
			$itemsT = $result->fetch();

			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error select  tarifas";
				die($mensaje);
			}
			$updClientes = "update catastro.clientes set catetar=?,domestico=?,social=?,comercial=?,estatal=?,industrial=?
						   where codemp=? and codsuc=? and nroinscripcion=?";

			$result = $conexion->prepare($updClientes);
			$result->execute(array($catetar,$domestico,$social,$comercial,$estatal,
											  $industrial,$codemp,$codsuc,$rowC["nroinscripcion"]));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update clientes";
				die($mensaje);
			}
			////
			$Sql="SELECT nropresupuesto,tipo
				  FROM catastro.presupuesto_catastro where codemp=? and codsuc=? and nroinscripcion=?";
			$result = $conexion->prepare($Sql);
			$result->execute(array($codemp,$codsuc,$rowC["nroinscripcion"]));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error select  tarifas";
				die($mensaje);
			}
			foreach($result->fetchAll() as $rowp)
			{
				$Sql = "insert into catastro.presupuesto_clientes(codemp,codsuc,nroinscripcion,nropresupuesto,tipo)
						VALUES (:codemp,:codsuc,:nroinscripcion,:nropresupuesto,:tipo)";
				$result = $conexion->prepare($Sql);
				$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,':nroinscripcion'=>$rowC["nroinscripcion"],":nropresupuesto"=>$rowp["nropresupuesto"],":tipo"=>$rowp["tipo"]));

				if($result->errorCode()!='00000')
				{
					$conexion->rollBack();
					$mensaje = "Error presupuesto_clientes";
					die($mensaje);
				}

			}



			///////
			$updCatastro = "update catastro.catastro set nuevousuario=1 where codemp=? and codsuc=? and nroinscripcion=?";
			$result = $conexion->prepare($updCatastro);
			$result->execute(array($codemp,$codsuc,$rowC["nroinscripcion"]));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update catastro";
				die($mensaje);
			}
			//ACTUALIZAR PRESUPUESTO A ESTADO 5 TRANSFERIDO
			$Sql="UPDATE solicitudes.cabpresupuesto  SET estadopresupuesto =5";
			$Sql.=" WHERE nropresupuesto IN (SELECT nropresupuesto ";
			$Sql.="	FROM catastro.presupuesto_catastro WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion)";
			$result = $conexion->prepare($Sql);

			$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":nroinscripcion"=>$rowC["nroinscripcion"]));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update cabpresupuesto";
				die($mensaje);
			}

	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		echo "<span class='icono-icon-x'></span> No se pudo Realizar la Transferencia de Datos";
	}else{
		$conexion->commit();
		echo "<span class='icono-icon-ok'></span> Datos Transferidos Correctamente !!!!!!!!!";
	}

?>
