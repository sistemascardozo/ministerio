<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

//ini_set("display_errors",1);
//error_reporting(E_ALL);

include('../../../../objetos/clsFunciones.php');

$Op = $_GET["Op"];

$objFunciones = new clsFunciones();

$sinpresupuestotext = $_POST["sinpresupuestotext"];
$ingnroinscripcion  = $_POST["ingnroinscripcion"];
$flag_nuevo = $_POST["flagnuevotext"];
$idusuario = $_SESSION['id_user'];
//Para GIS
$IdUsuario = $_SESSION['id_user'];

//------Datos del Cliente--------
$nroinscripcion = $_POST["nroinscripcion"];
$codcliente = $_POST["codcliente"];
$codemp = 1;
$codsuc = $_SESSION['IdSucursal'];
$codzona = $_POST['codzona'];
$sector = $_POST["sector"];
$manzanas = $_POST["manzanas"];
$lote = $_POST["lote"];
$sublote = $_POST["sublote"];
$estadoservicio = $_POST["estadoservicio"];
$tiporesponsable = $_POST["tiporesponsable"];
$propietario = strtoupper($_POST["propietario"]);
$tipodocumento = $_POST["tipodocumento"];
$nrodocumento = $_POST["nrodocumento"];
$altoconsumidortext = $_POST["altoconsumidortext"];
$calles = $_POST["calles"];
$nrocalle = $_POST["nrocalle"];
$tipoentidades = $_POST["tipoentidades"];
$tipousuario = $_POST["tipousuario"];
$codrutdistribucion = $_POST["codrutdistribucion"];
$codrutlectura = $_POST["codrutlectura"];
$dirdistribucion = $_POST["dirdistribucion"];
$inquilino = $_POST["inquilino"];
$tipoactividad = $_POST["tipoactividad"];
$ciclo = $_POST["ciclo"];
$correo = $_POST["correo"];

//------Datos del Inmueble--------
$tipopredio = $_POST["tipopredio"];
$tipoconstruccion = $_POST["tipoconstruccion"];
$nropisos = $_POST["nropisos"];
$tipoabastecimiento = $_POST["tipoabastecimiento"];
$piscina = $_POST["piscina"];
$tipoalmacenaje = $_POST["tipoalmacenaje"];
$tiposervicio = $_POST["tiposervicio"];
$referencia = $_POST["referencia"];
$tipomatconstruccion = $_POST["tipomatconstruccion"];
$nroorden = trim($_POST["nroorden"]);
if ($nroorden == "")
    $nroorden = 0;
if ($codrutdistribucion == "")
    $codrutdistribucion = 0;
if ($codrutlectura == "")
    $codrutlectura = 0;
//------Conexion de Agua--------
$tipomaterialagua = $_POST["tipomaterialagua"];
$locacajaagua = $_POST["locacajaagua"];
$diametrosagua = $_POST["diametrosagua"];
$pavimentoagua = $_POST["pavimentoagua"];
$tipocorte = $_POST["tipocorte"];
$estadoconexion = $_POST["estadoconexion"];
$tipovereda = $_POST["tipovereda"];
$fechainstagua = $objFunciones->CodFecha($_POST["fechainstagua"]);
$tipocajaagua = $_POST["tipocajaagua"];
$estadocajaagua = $_POST["estadocajaagua"];
$tipotapaagua = $_POST["tipotapaagua"];
$estadotapaagua = $_POST["estadotapaagua"];
$tipofugasagua = $_POST["tipofugasagua"];

//------Medidor--------
$marcamedidor = $_POST["marcamedidor"];
$diametrosmedidor = $_POST["diametrosmedidor"];
$estadomedidor = $_POST["estadomedidor"];
$posicionmedidor = $_POST["posicionmedidor"];
$modelomedidor = $_POST["modelomedidor"];
$tipolectura = $_POST["tipolectura"];
$aniofab = $_POST["aniofab"];
$tipomedidor = $_POST["tipomedidor"];

$codurbanizacion = $_POST["codurbanizacion"];

$OrdenLect = $_POST["orden_lect"];
$OrdenDist = $_POST["orden_dist"];


$fechainstmedidor = $objFunciones->CodFecha($_POST["fechainstmedidor"]);
$capacidadmedidor = $_POST["capacidadmedidor"];
$nromedidor = $_POST["nromedidor"];
$tipofacturacion = $_POST["tipofacturacion"];
$ubicacionllavemedidor = $_POST["ubicacionllavemedidor"];
$lecturaanterior = $_POST["lecturaanterior"];
$lecturaultima = $_POST["lecturaultima"];
$consumo = $_POST["consumo"];
if ($lecturaanterior == "")
    $lecturaanterior = 0;
if ($lecturaultima == "")
    $lecturaultima = 0;
if ($consumo == "")
    $consumo = 0;

//------Desague--------
$diametrosdesague = $_POST["diametrosdesague"];
$tipomaterialdesague = $_POST["tipomaterialdesague"];
$locacajadesague = $_POST["locacajadesague"];
$fechainstdesague = $objFunciones->CodFecha($_POST["fechainstdesague"]);
$tipocajadesague = $_POST["tipocajadesague"];
$estadocajadesague = $_POST["estadocajadesague"];
$tipotapadesague = $_POST["tipotapadesague"];
$estadotapadesague = $_POST["estadotapadesague"];
$fugasyatoros = $_POST["fugasyatoros"];
$desagueinactivotext = $_POST["desagueinactivotext"];

//------Calidad del Servicio--------
$zonasbastecimiento = $_POST["zonasbastecimiento"];
$presionagua = $_POST["presionagua"];
$horasabastecimiento = $_POST["horasabastecimiento"];
$nrolavatorios = $_POST["nrolavatorios"] ? $_POST["nrolavatorios"] : 0;
$nrolavaropas = $_POST["nrolavaropas"] ? $_POST["nrolavaropas"] : 0;
$nrowater = $_POST["nrowater"] ? $_POST["nrowater"] : 0;
$nroduchas = $_POST["nroduchas"] ? $_POST["nroduchas"] : 0;
$nrourinarios = $_POST["nrourinarios"] ? $_POST["nrourinarios"] : 0;
$nrogrifos = $_POST["nrogrifos"] ? $_POST["nrogrifos"] : 0;
$nrohabitantes = $_POST["nrohabitantes"] ? $_POST["nrohabitantes"] : 0;
$observacion = $_POST["observacion"];

$conpozo = $_POST["conpozo"] ? $_POST["conpozo"] : 0;
$conpozoimporte = $_POST["conpozoimporte"] ? $_POST["conpozoimporte"] : 0;

//$codantiguo = $objFunciones->CodUsuario($codsuc, $nroinscripcion);
$nuevo = false;

if ($flag_nuevo == 1 && $Op == 0 && $codcliente == 0)
{
    $codcliente = $objFunciones->setCorrelativosVarios(1, $codsuc, "SELECT", 0);
}

if ($sinpresupuestotext == 1 && $Op == 0)
{
	$nroinscripcion = $objFunciones->setCorrelativosVarios(10, $codsuc, "SELECT", 0);
}

if ($Op == 0) {
    
    //$codantiguo = $_POST["codantiguo"];
    //echo $codcliente." - ".$nroinscripcion."<br>";
	//$nroinscripcion = substr($codantiguo, 2);
	//die($nroinscripcion);
	
    //if($ingnroinscripcion==1)
//    {
//        //$nroinscripcion = $codantiguo;
//        $Sql = "SELECT nroinscripcion FROM catastro.clientes WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
//        $consulta = $conexion->prepare($Sql);
//        $consulta->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nroinscripcion" => $nroinscripcion));
//        $result = $consulta->fetch();
//        if ($result[0] != '') {
//            //$nroinscripcion = $objFunciones->setCorrelativosVarios(10, $codsuc, "SELECT", 0);
//            //$nroinscripcion = $nroinscripcion;
//            $nuevo = true;
//        }
//    }else
//        {
            //$nroinscripcion = $objFunciones->setCorrelativosVarios(10, $codsuc, "SELECT", 0);
            $codantiguo     = $objFunciones->CodUsuario($codsuc, $nroinscripcion);
//        }
        /*
        if ($nroinscripcion == "" || $nroinscripcion == 0) {
            //die('NO SE PERMITE CREAR NUEVO CODIGO');
            $nroinscripcion = $objFunciones->setCorrelativosVarios(10, $codsuc, "SELECT", 0);
        } else {//SI VIENE CON CODIGO VALIDAR SI ESE CODIGO NO EXHISTE EN CLIENTES
            $Sql = "SELECT nroinscripcion FROM catastro.clientes WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";
            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nroinscripcion" => $nroinscripcion));
            $items = $consulta->fetch();
            if ($items[0] != '') {
                $nroinscripcion = $objFunciones->setCorrelativosVarios(10, $codsuc, "SELECT", 0);
                $nuevo = true;
            }
        }*/
        

    if ($IdUsuario == 42) {
        $sql = "INSERT INTO gis.catastro ";
    } else {
        $sql = "INSERT INTO catastro.catastro ";
    }
    $sql .= "(codcliente, nroinscripcion, codemp,codsuc,codzona,codsector,codmanzanas,nrolote,sublote,propietario,
            codcalle,nrocalle,codtipodocumento,nrodocumento,referencia,tipopredio,codtipoconstruccion,nropisos,codtiposervicio,codtipomatconstruccion,
            nroorden, codtipoabastecimiento,piscina,codtipoalmacenaje,codtipousuario,direcciondistribucion,codtiporesponsable,inquilino,
            altocon,codtipoentidades,codestadoservicio,codrutlecturas,codrutdistribucion,coddiametrosagua,codtipomaterialagua,
            codlocacajaagua,codtipopavimentoagua,codtipocorte,codestadoconexion,codestadocajaagua,codubicacion,codtipovereda,
            fechainsagua,codtipofugasagua,codtipocajaagua,codtipotapaagu,estadotapaagu,codmarca,coddiametrosmedidor,codestadomedidor,
            posicionmed,tipolectura,fechainsmed,nromed,codmodelo,aniofabmed,codtipomedidor,codcapacidadmedidor,coddiametrosdesague,
            codtipomaterialdesague,codlocacajadesague,fechainsdesague,codtipocajadesague,codestadocajadesague,codtipotapadesague,
            estadotapadesague,fugasatoros,presionagua,horasabastecimiento,nrohabitantes,observacionabast,codzonaabas,
            nrolavatorios,nrolavaropas,nrowater,nroduchas,nrourinarios,nrogrifos,codtipoactividad,tipofacturacion,sinpresupuesto,
            desagueinactivo,flag_nuevo,codusu,codciclo,lecturaanterior,lecturaultima,consumo,conpozo,conpozoimporte,correo,codantiguo, orden_lect, orden_dist, codurbanizacion)
            VALUES(".$codcliente.", ".$nroinscripcion.", ".$codemp.", ".$codsuc.", :codzona, :codsector, :codmanzanas, ".$lote.", ".$sublote.",:propietario,:codcalle,
            :nrocalle,:codtipodocumento,:nrodocumento,:referencia,:tipopredio,:codtipoconstruccion,:nropisos,:codtiposervicio,:codtipomatconstruccion,:nroorden,
            :codtipoabastecimiento,:piscina,:codtipoalmacenaje,:codtipousuario,:direcciondistribucion,:codtiporesponsable,
            :inquilino,:altocon,:codtipoentidades,:codestadoservicio,:codrutlecturas,:codrutdistribucion,:coddiametrosagua,
            :codtipomaterialagua,:codlocacajaagua,:codtipopavimentoagua,:codtipocorte,:codestadoconexion,:codestadocajaagua,:codubicacion,
            :codtipovereda,:fechainsagua,:codtipofugasagua,:codtipocajaagua,:codtipotapaagu,:estadotapaagu,:codmarca,:coddiametrosmedidor,
            :codestadomedidor,:posicionmed,:tipolectura,:fechainsmed,:nromed,:codmodelo,:aniofabmed,:codtipomedidor,:codcapacidadmedidor,
            :coddiametrosdesague,:codtipomaterialdesague,:codlocacajadesague,:fechainsdesague,:codtipocajadesague,:codestadocajadesague,
            :codtipotapadesague,:estadotapadesague,:fugasatoros,:presionagua,:horasabastecimiento,:nrohabitantes,
            :observacionabast,:codzonaabas,:nrolavatorios,:nrolavaropas,:nrowater,:nroduchas,:nrourinarios,:nrogrifos,:codtipoactividad,
            :tipofacturacion,:sinpresupuesto,:desagueinactivo,:flag_nuevo,:codusu,:codciclo,:lecturaanterior,:lecturaultima,:consumo,:conpozo,:conpozoimporte,:correo,:codantiguo,:orden_lect,:orden_dist,:codurbanizacion)";
}
if ($Op == 1) {
    if ($IdUsuario == 42) {
        $sql = "UPDATE gis.catastro ";
    } else {
        $sql = "UPDATE catastro.catastro ";
    }
    $sql .= "SET codcliente = ".$codcliente.", codsector = :codsector, codmanzanas = :codmanzanas, nrolote = ".$lote.", sublote = ".$sublote.",
            propietario=:propietario,codcalle= :codcalle,nrocalle=:nrocalle,codtipodocumento=:codtipodocumento,nrodocumento=:nrodocumento,referencia=:referencia,
            tipopredio=:tipopredio,codtipoconstruccion=:codtipoconstruccion,nropisos=:nropisos,codtiposervicio=:codtiposervicio,codtipomatconstruccion=:codtipomatconstruccion,nroorden=:nroorden,codtipoabastecimiento=:codtipoabastecimiento,
            piscina=:piscina,codtipoalmacenaje=:codtipoalmacenaje,codtipousuario=:codtipousuario,direcciondistribucion=:direcciondistribucion,
            codtiporesponsable=:codtiporesponsable,inquilino=:inquilino,altocon=:altocon,codtipoentidades=:codtipoentidades,codestadoservicio=:codestadoservicio,
            codrutlecturas=:codrutlecturas,codrutdistribucion=:codrutdistribucion,coddiametrosagua=:coddiametrosagua,codtipomaterialagua=:codtipomaterialagua,
            codlocacajaagua=:codlocacajaagua,codtipopavimentoagua=:codtipopavimentoagua,codtipocorte=:codtipocorte,codestadoconexion=:codestadoconexion,
            codestadocajaagua=:codestadocajaagua,codubicacion=:codubicacion,codtipovereda=:codtipovereda,fechainsagua=:fechainsagua,codtipofugasagua=:codtipofugasagua,
            codtipocajaagua=:codtipocajaagua,codtipotapaagu=:codtipotapaagu,estadotapaagu=:estadotapaagu,codmarca=:codmarca,coddiametrosmedidor=:coddiametrosmedidor,
            codestadomedidor=:codestadomedidor,posicionmed=:posicionmed,tipolectura=:tipolectura,fechainsmed=:fechainsmed,nromed=:nromed,codmodelo=:codmodelo,
            aniofabmed=:aniofabmed,codtipomedidor=:codtipomedidor,codcapacidadmedidor=:codcapacidadmedidor,coddiametrosdesague=:coddiametrosdesague,
            codtipomaterialdesague=:codtipomaterialdesague,codlocacajadesague=:codlocacajadesague,fechainsdesague=:fechainsdesague,codtipocajadesague=:codtipocajadesague,
            codestadocajadesague=:codestadocajadesague,codtipotapadesague=:codtipotapadesague,estadotapadesague=:estadotapadesague,fugasatoros=:fugasatoros,
            presionagua=:presionagua,horasabastecimiento=:horasabastecimiento,nrohabitantes=:nrohabitantes,observacionabast=:observacionabast,codzonaabas=:codzonaabas,
            nrolavatorios=:nrolavatorios,nrolavaropas=:nrolavaropas,nrowater=:nrowater,nroduchas=:nroduchas,nrourinarios=:nrourinarios,nrogrifos=:nrogrifos,
            codtipoactividad=:codtipoactividad,tipofacturacion=:tipofacturacion,sinpresupuesto=:sinpresupuesto,desagueinactivo=:desagueinactivo,
            flag_nuevo=:flag_nuevo,codusu=:codusu,codciclo=:codciclo,lecturaanterior=:lecturaanterior,lecturaultima=:lecturaultima,consumo=:consumo,codzona=:codzona,
            conpozo=:conpozo,conpozoimporte=:conpozoimporte,correo=:correo,codantiguo=:codantiguo,orden_lect=:orden_lect, orden_dist=:orden_dist, codurbanizacion=:codurbanizacion
            WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND nroinscripcion = ".$nroinscripcion." ";
}
$conexion->beginTransaction();
$result = $conexion->prepare($sql);
$result->execute(array(":codsector" => $sector,
    ":codmanzanas" => $manzanas,
    ":propietario" => $propietario,
    ":codcalle" => $calles,
    ":nrocalle" => $nrocalle,
    ":codtipodocumento" => $tipodocumento,
    ":nrodocumento" => $nrodocumento,
    ":referencia" => $referencia,
    ":tipopredio" => $tipopredio,
    ":codtipoconstruccion" => $tipoconstruccion,
    ":nropisos" => $nropisos,
    ":codtiposervicio" => $tiposervicio,
    ":codtipomatconstruccion" => $tipomatconstruccion,
    ":nroorden" => $nroorden,
    ":codtipoabastecimiento" => $tipoabastecimiento,
    ":piscina" => $piscina,
    ":codtipoalmacenaje" => $tipoalmacenaje,
    ":codtipousuario" => $tipousuario,
    ":direcciondistribucion" => $dirdistribucion,
    ":codtiporesponsable" => $tiporesponsable,
    ":inquilino" => $inquilino,
    ":altocon" => $altoconsumidortext,
    ":codtipoentidades" => $tipoentidades,
    ":codestadoservicio" => $estadoservicio,
    ":codrutlecturas" => $codrutlectura,
    ":codrutdistribucion" => $codrutdistribucion,
    ":coddiametrosagua" => $diametrosagua,
    ":codtipomaterialagua" => $tipomaterialagua,
    ":codlocacajaagua" => $locacajaagua,
    ":codtipopavimentoagua" => $pavimentoagua,
    ":codtipocorte" => $tipocorte,
    ":codestadoconexion" => $estadoconexion,
    ":codestadocajaagua" => $estadocajaagua,
    ":codubicacion" => $ubicacionllavemedidor,
    ":codtipovereda" => $tipovereda,
    ":fechainsagua" => $fechainstagua,
    ":codtipofugasagua" => $tipofugasagua,
    ":codtipocajaagua" => $tipocajaagua,
    ":codtipotapaagu" => $tipotapaagua,
    ":estadotapaagu" => $estadotapaagua,
    ":codmarca" => $marcamedidor,
    ":coddiametrosmedidor" => $diametrosmedidor,
    ":codestadomedidor" => $estadomedidor,
    ":posicionmed" => $posicionmedidor,
    ":tipolectura" => $tipolectura,
    ":fechainsmed" => $fechainstmedidor,
    ":nromed" => $nromedidor,
    ":codmodelo" => $modelomedidor,
    ":aniofabmed" => $aniofab,
    ":codtipomedidor" => $tipomedidor,
    ":codcapacidadmedidor" => $capacidadmedidor,
    ":coddiametrosdesague" => $diametrosdesague,
    ":codtipomaterialdesague" => $tipomaterialdesague,
    ":codlocacajadesague" => $locacajadesague,
    ":fechainsdesague" => $fechainstdesague,
    ":codtipocajadesague" => $tipocajadesague,
    ":codestadocajadesague" => $estadocajadesague,
    ":codtipotapadesague" => $tipotapadesague,
    ":estadotapadesague" => $estadotapadesague,
    ":fugasatoros" => $fugasyatoros,
    ":presionagua" => $presionagua,
    ":horasabastecimiento" => $horasabastecimiento,
    ":nrohabitantes" => $nrohabitantes,
    ":observacionabast" => $observacion,
    ":codzonaabas" => $zonasbastecimiento,
    ":nrolavatorios" => $nrolavatorios,
    ":nrolavaropas" => $nrolavaropas,
    ":nrowater" => $nrowater,
    ":nroduchas" => $nroduchas,
    ":nrourinarios" => $nrourinarios,
    ":nrogrifos" => $nrogrifos,
    ":codtipoactividad" => $tipoactividad,
    ":tipofacturacion" => $tipofacturacion,
    ":sinpresupuesto" => $sinpresupuestotext,
    ":desagueinactivo" => $desagueinactivotext,
    ":flag_nuevo" => $flag_nuevo,
    ":codusu" => $idusuario,
    ":codciclo" => $ciclo,
    ":lecturaanterior" => $lecturaanterior,
    ":lecturaultima" => $lecturaultima,
    ":consumo" => $consumo,
    ":codzona" => $codzona,
    ":conpozo" => $conpozo,
    ":conpozoimporte" => $conpozoimporte,
    ":correo" => $correo,
    ":codantiguo" => $codantiguo,
    ":orden_lect" => $OrdenLect,
    ":orden_dist" => $OrdenDist,
    ":codurbanizacion" => $codurbanizacion
));
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $res = 2;
    die(2);
}
//------Presupuesto--------
$count_presupuesto = $_POST["cont_presupuesto"];

if ($IdUsuario != 42) {
    $delpresupuesto = "DELETE FROM catastro.presupuesto_catastro where codemp=? and codsuc=? and nroinscripcion=?";
    $result = $conexion->prepare($delpresupuesto);
    $result->execute(array($codemp, $codsuc, $nroinscripcion));
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        echo $res = 2;
        die(2);
    }
}

for ($i = 1; $i <= $count_presupuesto; $i++) {
    if (isset($_POST["nropresupuesto".$i])) {
        $sql_presupuesto = "INSERT INTO catastro.presupuesto_catastro(codemp,codsuc,nroinscripcion,nropresupuesto,tipo)
                VALUES(:codemp,:codsuc,:nroinscripcion,:nropresupuesto,:tipo)";

        $result = $conexion->prepare($sql_presupuesto);
        $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nroinscripcion" => $nroinscripcion,
            ":nropresupuesto" => $_POST["nropresupuesto".$i], ":tipo" => $_POST["tipopresupuesto".$i]));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            echo $res = 2;
            die('Error al presupuesto_catastro');
        }

        $upd = "UPDATE solicitudes.cabpresupuesto SET
                                estadopresupuesto=5
                            WHERE codemp=? and codsuc=? and nropresupuesto=?";
        $result = $conexion->prepare($upd);
        $result->execute(array($codemp, $codsuc, $_POST["nropresupuesto".$i]));

        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error UPDATE Solicitud";
            die(2);
        }
    }
}

//------Unidades de Uso-------
$count_unidades = $_POST["cont_unidades"];

if ($IdUsuario == 42) {
    $delunidades = "DELETE FROM gis.unidadesusocatastro ";
} else {
    $delunidades = "DELETE FROM catastro.unidadesusocatastro ";
}
$delunidades .= "WHERE codemp=? AND codsuc=? AND nroinscripcion=?";
$result = $conexion->prepare($delunidades);
$result->execute(array($codemp, $codsuc, $nroinscripcion));
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    echo $res = 2;
    die('Error al DELETE unidadesusocatastro');
}
for ($i = 1; $i <= $count_unidades; $i++) {
    if (isset($_POST["tarifas".$i])) {
        if ($IdUsuario == 42) {
            $sql_unidades = "INSERT INTO gis.unidadesusocatastro";
        } else {
            $sql_unidades = "INSERT INTO catastro.unidadesusocatastro";
        }
        $sql_unidades .= "(codemp,codsuc,nroinscripcion,catetar,porcentaje,principal, item,codtipoactividad)
                VALUES(:codemp,:codsuc,:nroinscripcion,:catetar,:porcentaje,:principal, :item,:codtipoactividad)";
        $result = $conexion->prepare($sql_unidades);
        $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nroinscripcion" => $nroinscripcion,
            ":catetar" => $_POST["tarifas".$i], ":porcentaje" => $_POST["porcentaje".$i],
            ":principal" => $_POST["principal".$i],
            ":item" => $i,
            ":codtipoactividad" => $_POST["codtipoactividad".$i]
        ));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            echo $res = 2;
            die('Error al insert unidadesusocatastro');
        }
    }
}


$conexion->commit();
if ($flag_nuevo == 1 && $Op == 0)
    $objFunciones->setCorrelativosVarios(1, $codsuc, "UPDATE", $codcliente);

if($ingnroinscripcion!=1)
{
    if ($_POST["nroinscripcion"] == "" || $_POST["nroinscripcion"] == 0)
        $objFunciones->setCorrelativosVarios(10, $codsuc, "UPDATE", $nroinscripcion);
    else {
        if ($nuevo)
            $objFunciones->setCorrelativosVarios(10, $codsuc, "UPDATE", $nroinscripcion);
    }
    
}
echo $res = 1;
?>
