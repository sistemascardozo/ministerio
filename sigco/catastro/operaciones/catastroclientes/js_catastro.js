// JavaScript Document
var objIdx = "";
var pAgua = 0;
var pDesague = 0;

var cont_presupuesto = 0;
var cont_fila_presupuesto = 0;
var validar_cont_presupuesto = 0;

var cont_unidad = 0;
var cont_fila_unidad = 0;

var total_porcentaje = 0;
var procentaje = 0;

function actualizacion()
{

}
function vCatastro()
{
    var sector = $("#sector").val();
    var manzanas = $("#manzanas").val();
    var lote = $("#lote").val();
    var sublote = $("#sublote").val();

    if (sector != "0" && manzanas != "0" && lote != "" && sublote!="")
    {
        $.ajax({
            url: 'vcatastro.php',
            type: 'POST',
            async: true,
            data: 'sector='+sector+'&manzanas='+manzanas+'&lote='+lote+'&sublote'+sublote+'&Op='+Op,
            dataType: "json",
            success: function (datos)
            {
                if (datos.resultado == 1)
                {
                    alert('Existe un usuario con el mismo Catastro : '+datos.propietario)
                }
            }
        })
    }
}
function buscar_presupuesto()
{
    if ($("#sinpresupuestotext").val() == 1)
    {
        Msj($("#chksinpresupuesto"), "No puede Agregar Presupuesto por que ha Seleccionado la Opcion sin Presupuesto")
        return
    }
    objIdx = "presupuesto"
    AbrirPopupBusqueda("../../../solicitudes/operaciones/presupuesto/?Op=5", 1100, 500)
}

function Recibir(Id)
{
    if (objIdx == "presupuesto")
    {
        $("#nropresupuesto").val(Id)
        cargar_presupuesto(Id)
    }

    if (objIdx == "cliente")
    {
        cargar_datos_usuario(Id)
    }
}
function cargar_presupuesto(nropresupuesto)
{
    $.ajax({
        url: '../../../../ajax/mostrar/presupuesto.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: 'nropresupuesto='+nropresupuesto+'&codsuc='+codsuc,
        success: function (datos)
        {

            $("#tipopresupuesto").val(datos.tipopresupuesto)
            $("#nrosolicitud").val(datos.nrosolicitud)
            $("#prop").val(datos.propietario)
            $("#calle").val(datos.calle)
            $("#sector").val(datos.codsector)
            $("#codzona").val(datos.codzona)

            cargar_sectores(codsuc, datos.codzona, datos.codsector, 1)
            cargar_manzana(codsuc, datos.codsector, datos.codmanzanas, 1)
            cargar_calles(datos.codsector, codsuc, datos.codzona, datos.codcalle)
            cargar_rutas_lecturas(datos.codmanzanas, datos.codsector)
            cargar_rutas_distribucion(datos.codmanzanas, datos.codsector)
            cargar_zonas_abastecimiento(datos.codsector, '', 0);
            cargar_urbanizacion(codsuc, datos.codzona, '', 1);


            $("#lote").val(datos.lote)
            $("#codcliente").val(datos.codcliente)
            $("#correo").val(datos.correo)
            $("#nrocalle").val(datos.nrocalle)
            $("#codcliente").val(datos.codcliente)
            $("#tipodocumento").val(datos.codtipodocumento)
            $("#nrodocumento").val(datos.nrodocumento)
            if (datos.nroinscripcion != 0)
                {
                    $("#nroinscripcion").val(datos.nroinscripcion);
					$("#codcliente").val(datos.codcliente);
                    $("#codantiguo").val(datos.codantiguo);
                }
        }
    });
}
function agregar_presupuesto()
{
    var tipopresupuesto = "";

    if ($("#sinpresupuestotext").val() == 1)
    {
        Msj($("#chksinpresupuesto"), "No se puede Agregar Fichas Presupuestales por la Opcion sin Presupuesto esta Activada")
        return false;
    }
    if ($("#nropresupuesto").val() == "")
    {
        Msj($("#nropresupuesto"), "Seleccione el Presupuesto")
        return false;
    }

    if ($("#tipopresupuesto").val() == 2)
    {
        tipopresupuesto = "AGUA";
    } else {
        tipopresupuesto = "DESAGUE";
    }


    for (i = 1; i <= $("#cont_presupuesto").val(); i++)
    {
        try
        {
            if ($("#nropresupuesto"+i).val() == $("#nropresupuesto").val())
            {
                Msj($("#nropresupuesto"), 'El Numero de Presupuesto ya Fue Agregado');
                return false;
            }
           // if ($("#prop"+i).val() != $("#prop").val())
           if ($("#nroinscripcion"+i).val() != $("#nroinscripcion").val())
            {
                Msj($("#nropresupuesto"), 'La Inscripcion del Propietario Seleccionado no Coincide con el Nombre del Propietario Ingresado');
                return false;
            }

            if ($("#tipopresupuesto"+i).val() == 2) {
                pAgua = 1;
            }
            if ($("#tipopresupuesto"+i).val() == 3) {
                pDesague = 1;
            }

        } catch (exp)
        {

        }
    }

    if ($("#cont_presupuesto").val() > 0)
    {
        if (pAgua == 1 && pDesague == 1)
        {
            //Msj($("#nropresupuesto"), "No se puede ingresar el presupuesto por que ya se ingreso los de dos necesarios para registrar la conexion")
            //return false
        }
        if (pAgua == 1 && pDesague == 0)
        {
            if ($("#tipopresupuesto").val() == 1)
            {
               // Msj($("#nropresupuesto"), "Ya se agrego el presupuesto de agua")
               // return false
            }
            $("#tiposervicio").val(1);
        }
        if (pAgua == 0 && pDesague == 1)
        {
            if ($("#tipopresupuesto").val() == 2)
            {
               // Msj($("#nropresupuesto"), "Ya se agrego el presupuesto de desague")
                //return false
            }
            $("#tiposervicio").val(1);
        }
    } else {
        if ($("#tipopresupuesto").val() == 2) {
            $("#tiposervicio").val(2);
        }
        if ($("#tipopresupuesto").val() == 3) {
            $("#tiposervicio").val(3);
        }
    }

    cont_presupuesto++;
    cont_fila_presupuesto++;

    $("#tbpresupuesto tbody").append(
            "<tr style='background-color:#FFFFFF; padding:4px'>" +
            "<td align='center'><input type='hidden' name='nropresupuesto"+cont_presupuesto+"' id='nropresupuesto"+cont_presupuesto+"' value='"+$("#nropresupuesto").val()+"' />"+$("#nropresupuesto").val()+"</td>" +
            "<td align='center'><input type='hidden' name='tipopresupuesto"+cont_presupuesto+"' id='tipopresupuesto"+cont_presupuesto+"' value='"+$("#tipopresupuesto").val()+"' />"+tipopresupuesto+"</td>" +
            "<td align='center'><input type='hidden' name='nrosolicitud"+cont_presupuesto+"' value='"+$("#nrosolicitud").val()+"' />"+$("#nrosolicitud").val()+"" +
            "<td align='center'><input type='hidden' name='nrosolicitud"+cont_presupuesto+"' value='"+$("#nrosolicitud").val()+"' />"+$("#prop").val()+"</td>" +
            "<input type='hidden' name='nroinscripcion"+cont_presupuesto+"' id='nroinscripcion"+cont_presupuesto+"' value='"+$("#nroinscripcion").val()+"' /></td>" +
            "<td>"+$("#calle").val()+"</td>" +
            "<td align='center'><img src='../../../../images/iconos/imprimir.png' title='Imprimir Ficha de Solicitud' width='16' height='16' style='cursor:pointer' onclick='imprimir_ficha_solicitud("+$("#nrosolicitud").val()+");' /></td>" +
            "<td align='center'><img src='../../../../images/iconos/imprimir.png' title='Imprimir Ficha Presupuestal' width='16' height='16' style='cursor:pointer' onclick='imprimir_ficha_presupuestal("+$("#nropresupuesto").val()+");' /></td>" +
            "<td align='center'><span class='icono-icon-trash' title='Borrar Registro' onClick='QuitaFilaD(this,0,0);' ></span></td>" +
            "</tr>"
            )


    $("#propietario").val($("#prop").val())

    $("#nropresupuesto").val("")
    $("#cont_presupuesto").val(cont_presupuesto)
}

function imprimir_ficha_solicitud(Id)
{
    AbrirPopupImpresion('../../../solicitudes/operaciones/registro/imprimir.php?Id='+Id+'&codsuc='+codsuc, 800, 600)
}
function imprimir_ficha_presupuestal(id)
{
    AbrirPopupImpresion('../../../solicitudes/operaciones/presupuesto/imprimir_ficha.php?Id='+id+'&codsuc='+codsuc, 800, 600)
}
function QuitaFilaD(x, opcion, idx)
{
    while (x.tagName.toLowerCase() != 'tr')
    {
        if (x.parentElement)
            x = x.parentElement;
        else if (x.parentNode)
            x = x.parentNode;
        else
            return;
    }

    var rowNum = x.rowIndex;
    while (x.tagName.toLowerCase() != 'table')
    {
        if (x.parentElement)
            x = x.parentElement;
        else if (x.parentNode)
            x = x.parentNode;
        else
            return;
    }
    if (opcion == 1)
    {
        total_porcentaje = parseFloat(total_porcentaje) - parseFloat($("#porcentaje"+idx).val())
        $("#total_porct").val(total_porcentaje)
        $("#procentaje").val(parseFloat($("#procentaje").val())+parseFloat($("#porcentaje"+idx).val()))

        cont_fila_unidad = parseFloat(cont_fila_unidad) - 1;
    }
    if (opcion == 0)
    {
        cont_fila_presupuesto = parseFloat(cont_fila_presupuesto) - 1;
        if (cont_fila_presupuesto == 0) {
            $("#codcliente").val(0)
        }
    }

    x.deleteRow(rowNum);
}

jQuery(function ($)
{
    $("#fechainstagua").mask("99/99/9999");
    $("#DivTarifas").dialog({
        autoOpen: false,
        modal: true,
        width: 450,
        height: 450,
        resizable: false,
        show: "scale",
        hide: "scale",
        close: function () {

        },
        buttons: {
            "Cerrar": function () {
                $(this).dialog("close");
            }

        }
        //
    });
    $("#DivTarifasAcordion").accordion({
        heightStyle: "content"
    });
    $("#DivTarifas").dialog('close');

});

jQuery(function ($)
{
    $("#fechainstmedidor").mask("99/99/9999");
});
jQuery(function ($)
{
    $("#fechainstdesague").mask("99/99/9999");
});

$(function () {
    $("#fechainstagua").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                        showButtonPanel: true,
                beforeShow: function (input, inst)
                {
                    inst.dpDiv.css({marginTop: (input.offsetHeight) - 20+'px', marginLeft: (input.offsetWidth) - 90+'px'});
                }
            }
    );
});

$(function () {
    $("#fechainstmedidor").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                        showButtonPanel: true,
                beforeShow: function (input, inst)
                {
                    inst.dpDiv.css({marginTop: (input.offsetHeight) - 20+'px', marginLeft: (input.offsetWidth) - 90+'px'});
                }
            }
    );



});
$(function () {
    if ($("#flagnuevotext").val() == 1)
        $("#propietario").attr('readonly', false)
    $("#fechainstdesague").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                        showButtonPanel: true,
                beforeShow: function (input, inst)
                {
                    inst.dpDiv.css({marginTop: (input.offsetHeight) - 20+'px', marginLeft: (input.offsetWidth) - 90+'px'});
                }
            }
    );
});

function cargar_rutas_lecturas(obj, codsector)
{
    var cod = $("#sector").val();
    if (cod == 0)
    {
        cod = codsector;
    }
    $.ajax({
        url: '../../../../ajax/rutaslecturas.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&codsector='+cod+'&codmanzana='+obj,
        success: function (datos) {
            var r = datos.split("|")
            if (r[0] == '')
                alert('La Manzana Seleccionada no Presenta Ruta de Lecturas')
            $("#rutalecturas").val(r[2])
            //$("#ordenlecturas").val(r[1])
            $("#codrutlectura").val(r[0])
        }
    })
}

function cargar_rutas_distribucion(obj, codsector)
{
    var cod = $("#sector").val();
    if (cod == 0)
    {
        cod = codsector;
    }

    $.ajax({
        url: '../../../../ajax/rutasdistribucion.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&codsector='+cod+'&codmanzana='+obj,
        success: function (datos) {
            var r = datos.split("|")
            if (r[0] == '')
                alert('La Manzana Seleccionada no Presenta Ruta de Distribucion')
            $("#rutadistribucion").val(r[2])
            //$("#ordendistribucion").val(r[1])
            $("#codrutdistribucion").val(r[0])
        }
    })
}
function agregar_unidades_uso(codusu)
{
    if ($("#tarifas").val() == 0)
    {
        Msj($("#tarifas"), "Seleccione la Tarifa")
        return false;
    }
    if($("#tipoactividad").val()==0)
    {
        Msj($("#tipoactividad"),"Seleccione la Actividad")
        return false;
    }
    if ($("#procentaje").val() == 0 || $("#procentaje").val() == "" || $("#procentaje").val() < 0 || $("#procentaje").val() > 100)
    {
        Msj($("#procentaje"), "El Porcentaje Ingresado no es Valido")
        return false;
    }
    if ($("#total_porct").val() >= 100)
    {
        Msj($("#procentaje"), "Ya se alcanzo el 100%")
        return false;
    }


    var categoria = $("#tarifas option:selected").text();
    total_porcentaje += parseFloat($("#procentaje").val());
    var idcategoria = $("#tarifas").val();
    if (total_porcentaje > 100)
    {
        alert("Se sobrepaso el 100%")
        total_porcentaje -= parseFloat($("#procentaje").val());
        return false;
    }
    $("#total_porct").val(total_porcentaje)

    var principal = "NO";
    if ($("#principal_categoria").val() == 1)
    {
        principal = "SI";
        if ($("#tipofacturacion").val() == 2) {
            cambiarcombotipofacturacion(codsuc, idcategoria);
        }
    }

    cont_unidad++;
    cont_fila_unidad++;

    for (i = 1; i <= $("#cont_unidades").val(); i++)
    {
        try
        {
            /*if($("#tarifas"+i).val()==$("#tarifas").val())
             {
             Msj($("#tarifas"),'La Categoria Tarifaria ya Fue Agregado');
             return false;
             }*/
        } catch (exp)
        {

        }
    }

    principalUU = "";
    if ($("#principal_categoria").val() == 1)
    {
        principalUU = "checked='checked'";
    }

    $("#tbunidades tbody").append(
            "<tr style='background-color:#FFFFFF; padding:4px'>" +
            "<td align='center'><input type='hidden' name='ItemD"+cont_unidad+"' id='ItemD"+cont_unidad+"' value='"+cont_unidad+"' /><input type='hidden' id='tarifas"+cont_unidad+"' name='tarifas"+cont_unidad+"' value='"+$("#tarifas").val()+"' />"+$("#tarifas").val()+"</td>" +
            "<td>"+categoria+"</td>" +
            "<td align='center'><input type='hidden' name='codtipoactividad" + cont_unidad + "' id='codtipoactividad" + cont_unidad + "' value='" + $("#tipoactividad").val() + "' />"+$("#tipoactividad option:selected").html()+"</td>"+
            "<td align='center' ><input type='hidden' id='porcentaje"+cont_unidad+"' name='porcentaje"+cont_unidad+"' value='"+$("#procentaje").val()+"' />"+$("#procentaje").val()+"</td>" +
            "<td align='center' ><input type='hidden' class='principal' id='principal"+cont_unidad+"' name='principal"+cont_unidad+"' value='"+$("#principal_categoria").val()+"' />" +
            "<input type='radio' name='principalUU' id='principalUU"+cont_unidad+"' value="+cont_fila_unidad+" "+principalUU+" onChange='Numerar(); actualizacion(this, 3);'  style='cursor:pointer'/></td>" +
            "<td align='center' ><img src='../../../../images/iconos/cancel.png' width='16' height='16' title='Borrar Registro' onclick='QuitaFilaD(this,1,"+cont_unidad+");' style='cursor:pointer'/></td>" +
            "</tr>");

    $("#cont_unidades").val(cont_unidad)

    $("#tarifas").val(0)
    document.getElementById("principal_categoria_check").checked = false;

    $("#principal_categoria").val(0)
    $("#procentaje").val(100 - parseFloat($("#total_porct").val()))
    Numerar();

}

	function validar_estado_presupuesto(obj, input)
	{
		if (obj.checked)
		{
			if (cont_fila_presupuesto != 0)
			{
				Msj($("#tbpresupuesto"), "No se puede seleccionar esta opcion por que se encuentra agregado un presupuesto");

				obj.checked = false;

				$("#" + input).val(0);

				return false;
			}
		}
	}

function validar_nuevo_usuario(obj)
{
    if (obj.checked == false)
    {
        if ($("#flagnuevotext").val() == 1)
        {
            Msj($("#flagnuevo"), "No se puede desactivar el control por que la opcion Nuevo Usuario esta Activada")
            obj.checked = true
            $("#sinpresupuestotext").val(1)
            return false
        }
    }
}

	function quitar_readonly(obj, input)
	{
		if (obj.checked)
		{
			if ($("#sinpresupuestotext").val() == 0)
			{
				$("#tabs").tabs({selected: 0});

				Msj($("#chksinpresupuesto"), "No se puede seleccionar esta opcion por que la opcion con presupuesto esta activada");

				obj.checked = false;

				return false;
			}

			$('#' + input).attr('readonly', false);
			$("#codcliente").val(0);
			$('#' + input).val('');
		}
		else
		{
			$('#' + input).val('');
			$('#' + input).attr('readonly', true);
		}
	}

function buscar_usuarios()
{
    if ($("#sinpresupuestotext").val() == 0)
    {
        $("#tabs").tabs({selected: 0});
        Msj($("#chksinpresupuesto"), "No se puede buscar un usuario por que la opcion sin presupuesto esta desactivada")
        return false;
    }
    if (cont_fila_presupuesto != 0)
    {
        $("#tabs").tabs({selected: 0});
        Msj($("#tbpresupuesto"), "No se puede Buscar un usuario por que ya se agrego un ficha presupuestal")
        return false
    }
    if ($("#flagnuevotext").val() == 1)
    {
        $("#tabs").tabs({selected: 1});
        Msj($("#flagnuevo"), "No se puede buscar un usuario por que la opcion Nuevo Usuario esta Activada")
        return false
    }

    objIdx = "cliente"
    AbrirPopupBusqueda("../actualizacion/?Op=5", 1000, 500)
}
function cargar_datos_usuario(nroinscripcion)
{
    $.ajax({
        url: '../../../../ajax/clientes.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion,
        dataType: "json",
        success: function (datos) {
            /*var r=datos.split("|")

             $("#codcliente").val(r[0])
             $("#propietario").val(r[1])
             $("#tipodocumento").val(r[2])
             $("#nrodocumento").val(r[3])*/

            $("#codcliente").val(datos.codcliente)
            $("#propietario").val(datos.propietario)
            $("#tipodocumento").val(datos.codtipodocumento)
            $("#nrodocumento").val(datos.nrodocumento)
        }
    })
}
function validar_cod_catastro()
{
    $.ajax({
        url: '../../../../admin/validaciones/validar_catastro.php',
        type: 'POST',
        async: true,
        data: 'codrutlecturas='+$("#codrutlectura").val()+'&lote='+$("#lote").val(),
        success: function (datos) {
            $("#cCatastro").val(datos)

            //setTimeout(validar_cod_catastro(),2000)
        }
    })
}
function cargar_zonas_abastecimiento(codsector, seleccion, opcion)
{
    $.ajax({
        url: '../../../../ajax/zonas_abastecimiento_drop.php',
        type: 'POST',
        async: true,
        data: 'codsector='+codsector+'&codsuc='+codsuc+'&seleccion='+seleccion+'&opcion='+opcion,
        success: function (datos) {
            $("#div_zonas").html(datos)
        }
    })
}
function cargar_horarios_zonas(codzona, codsector)
{
    $.ajax({
        url: '../../../../ajax/horarios_abastecimiento.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&codzona='+codzona+'&codsector='+codsector+'&codrutlectura='+$("#codrutlectura").val(),
        success: function (datos) {
            $("#horasabastecimiento").val(datos)
        }
    })
}
function VerTarifas()
{
    $("#DivTarifas").dialog('open');
}
function AddTarifa(consumo)
{
    $("#DivTarifas").dialog('close');
    $("#consumo").val(consumo);
}
function actualizacionx()
{

}
function VImportePozo(op)
{
    if (op == 1)
    {
        $(".cconpozoimporte").show();
        $("#conpozoimporte").focus().select();
    }

    else
    {
        $("#conpozoimporte").val(0)
        $(".cconpozoimporte").hide();
    }

}

function cambiarcombotipofacturacion(codsuc, id) {

    $.ajax({
        url: '../../../../ajax/modificartipofacturacion.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&catetar='+id,
        success: function (datos) {
            datos = parseInt(datos);
            $("#consumo").val(datos);
        }
    })

}
function Numerar()
{
    var contt = 1;
    for (var i = 1; i <= cont_unidad; i++)
    {

        try
        {
            document.getElementById('ItemD'+i).value = contt;
            $('#principalUU'+i).val(contt);
            contt = contt+1;

            //alert(cont_unidad);

            $('#principal'+i).val(0);
            if ($('#principalUU'+i).is(':checked'))
            {
                var valor = $('#tarifas'+i).val();

                $('#principal'+i).val(1);
                $("#catetar option[value="+valor+"]").attr("selected", true);
            }
        }
        catch (err)
        {
            //alert('papap');
        }
    }
}

/*--------- BUSCAR SOLO CON FACTIBILIDAD -----------*/
function  buscar_servicio()
    {
        object = "solicitudinspeccion"
        AbrirPopupBusqueda('../../../catastro/operaciones/solicitud/indexInspeccion.php?Op=5', 1110, 450)
    }

    function RecibirP(id,nroinscripcion)
    {
        if (object == "solicitudinspeccion")
        {            //alert('');
            datos_solicitudinspeccion(id,nroinscripcion)
        }
    }

    function datos_solicitudinspeccion(nroinspeccion,nroinscripcion)
    {
        $.ajax({
            url: urldir+'ajax/usernuevoontrato.php',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: 'codsuc='+codsuc+'&nroinspeccion='+nroinspeccion,
            success: function (datos) {

                $("#propietario").val(datos.propietario)
                $("#codcliente").val(datos.codcliente)
                $("#nroinscripcion").val(datos.nroinscripcion)
                $("#tiposervicio").val(datos.codtiposervicio)
                $("#codzona").val(datos.codzona)

                cargar_sectores(codsuc, datos.codzona, datos.codsector, 1)
                cargar_manzana(codsuc, datos.codsector, datos.codmanzanas, 1)
                cargar_calles(datos.codsector, codsuc, datos.codzona, datos.codcalle)
                cargar_rutas_lecturas(datos.codmanzanas, datos.codsector)
                cargar_rutas_distribucion(datos.codmanzanas, datos.codsector)
                cargar_zonas_abastecimiento(datos.codsector, '', 0);

                //$("#sector").val(datos.codsector)
                //$("#manzanas").val(datos.codmanzanas)
                $("#lote").val(datos.lote)
                $("#sublote").val(datos.sublote)
                $("#nroorden").val(1)
                $("#nroinspeccion").val(datos.nroinspeccion)

                $("#codantiguo").val(datos.codantiguo)
                //$("#calles").val(datos.codcalle)
                $("#nrocalle").val(datos.nrocalle)
                $("#nrocalle").val(datos.nrocalle)
                //cargar_datos_periodo_facturacion(datos.codciclo)
                //$("#codciclo").val(datos.codciclo)
            }
        })


    }
