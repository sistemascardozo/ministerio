<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$TituloVentana = "REGISTRO CATASTRAL";
$Activo = 1;
$Criterio = 'Clientes';

CuerpoSuperior($TituloVentana);

$Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
$codsuc = $_SESSION['IdSucursal'];
//Para GIS
$IdUsuario = $_SESSION['id_user'];

$FormatoGrilla = array();

$Sql = "SELECT c.codantiguo, ";
$Sql .= " (SELECT codcatastro FROM catastro.view_codcatastro_catastro w ";
$Sql .= "   WHERE w.nroinscripcion = c.nroinscripcion AND w.codsuc = c.codsuc) AS codcatastro, ";
$Sql .= " c.codcliente, c.propietario, tc.descripcioncorta || ' ' || ca.descripcion || ' ' || c.nrocalle AS direccion, ";
$Sql .= " c.nromed, e.descripcion, 1, c.nroinscripcion ";

if ($IdUsuario == 42) {
    $Sql .= "FROM gis.catastro AS c ";
} else {
    $Sql .= "FROM catastro.catastro AS c ";
}

$Sql .= " INNER JOIN public.calles ca ON(c.codemp = ca.codemp AND c.codsuc = ca.codsuc AND c.codzona = ca.codzona AND c.codcalle = ca.codcalle) ";
$Sql .= " INNER JOIN public.tiposcalle tc ON(ca.codtipocalle = tc.codtipocalle) ";
$Sql .= " INNER JOIN public.sectores s ON(c.codemp = s.codemp AND c.codsuc = s.codsuc AND c.codzona = s.codzona AND c.codsector = s.codsector) ";
$Sql .= " INNER JOIN public.estadoservicio e ON(c.codestadoservicio = e.codestadoservicio) ";

$FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL                       //Sentencia SQL
$FormatoGrilla[1] = array('1' => 'c.nroinscripcion', '2' => 'c.codcliente', '3' => 'c.propietario');    //Campos por los cuales se hará la búsqueda
$FormatoGrilla[2] = $Op;                                                            //Operacion
$FormatoGrilla[3] = array('T1' => 'Nro. Inscripcion', 'T2' => 'Cod Catastral', 'T3' => 'Cod. Cliente',
    'T4' => 'Usuario', 'T5' => 'Direcci&oacute;n', 'T6' => 'N° Medidor', 'T7' => 'Estado');   //Títulos de la Cabecera
$FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'center', 'A3' => 'center', 'A7' => 'center');                        //Alineación por Columna
$FormatoGrilla[5] = array('W1' => '80', 'W2' => '120', 'W3' => '80');                                 //Ancho de las Columnas
$FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
$FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
$FormatoGrilla[8] = "  AND (c.codemp = 1 AND c.codsuc = " . $codsuc . " and c.nuevousuario = 0) ORDER BY codcatastro desc ";                                   //Orden de la Consulta
$FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
    'NB' => '1', //Número de Botones a agregar
    'BtnId1' => 'BtnModificar', //Nombre del Boton
    'BtnI1' => 'modificar.png', //Imagen a mostrar
    'Btn1' => 'Editar', //Titulo del Botón
    'BtnF1' => 'onclick="ModificarA(this);"', //Eventos del Botón
    'BtnCI1' => '8', //Item a Comparar
    'BtnCV1' => '1'    //Valor de comparación
);
$FormatoGrilla[10] = array(array('Name' => 'nroinscripcion', 'Col' => 9)); //DATOS ADICIONALES
$FormatoGrilla[11] = 7; //FILAS VISIBLES           
$_SESSION['Formato' . $Criterio] = $FormatoGrilla;

Cabecera('', $FormatoGrilla[7], 1050, 550);
Pie();
CuerpoInferior();
?>
<script type="text/javascript">
    var urldir = '<?php echo $urldir; ?>';
    function ModificarA(obj)
    {
        var nroinscripcion = $(obj).parent().parent().data('nroinscripcion');
        //alert(nroinscripcion);
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $.ajax({
            url: 'mantenimiento.php',
            type: 'POST',
            async: true,
            data: 'Op=1&Id=' + nroinscripcion + '&codsuc=<?=$codsuc ?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
</script>
