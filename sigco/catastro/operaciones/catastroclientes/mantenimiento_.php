<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

$Sede = $_SESSION['Sede'];

include("../../../../objetos/clsDrop.php");

$Op = $_POST["Op"];
$Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];
//Para GIS
$IdUsuario = $_SESSION['id_user'];

$guardar = "Op=".$Op;

$fechainstagua = date("d/m/Y");
$fechainstmedidor = date("d/m/Y");
$fechainstdesague = date("d/m/Y");

$objMantenimiento = new clsDrop();
$conpozo = 0;
$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);

if ($Id != '') {
    $sql = "SELECT * ";
    if ($IdUsuario == 42) {
        $sql .= "FROM gis.catastro ";
    } else {
        $sql .= "FROM catastro.catastro ";
    }
    $sql .= "WHERE codemp=1 AND codsuc=? AND nroinscripcion=? ";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codsuc, $Id));
    $items = $consulta->fetch();

    $guardar = $guardar."&Id2=".$Id;

    $fechinstagua = new DateTime($items["fechainsagua"]);
    $fechainstagua = $fechinstagua->format("d/m/Y");

    $fechinstmedidor = new DateTime($items["fechainsmed"]);
    $fechainstmedidor = $fechinstmedidor->format("d/m/Y");

    $fechinstdesague = new DateTime($items["fechainsdesague"]);
    $fechainstdesague = $fechinstdesague->format("d/m/Y");
    $conpozo = $items['conpozo'];
}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_catastro.js" language="JavaScript"></script>
<style type="text/css">

    #tbCampos * {
        font-size: 10px;
    }
</style>
<script type="text/javascript">

    var codsuc =<?=$codsuc ?>;
    function buscar_calle()
    {
        object = "calles"
        var codzona = $("#codzona").val()
        var codsector = $("#sector").val()
        if (codzona == 0)
        {
            alert('Seleccione una Zona');
            return false;
        }

        if (codsector == 0)
        {
            alert('Seleccione un Sector');
            return false;
        }
        AbrirPopupBusqueda("../../../../buscar/calles.php?codzona=" + codzona + "&codsector=" + codsector + "&codsuc=" + codsuc, 700, 450)
    }
    function recibir_calles(Id, Des)
    {
        $("#calles").val(Id)
        $("#calles").prop('selected', true)
        // $("#calles").val(Des)
    }
    function buscar_urbanizacion()
    {
        object = "urbanizacion"
        var codzona = $("#codzona").val()
        if (codzona == 0)
        {
            alert('Seleccione una Zona');
            return false;
        }

        AbrirPopupBusqueda("../../../../buscar/urbanizacion.php?codzona=" + codzona + "&codsuc=" + codsuc, 700, 450)
    }
    function recibir_urbanizacion(Id, Des)
    {
        $("#codurbanizacion").val(Id)
        $("#codurbanizacion").prop('selected', true)
    }
    function buscar_actividad()
    {
        AbrirPopupBusqueda("../../../../buscar/actividad.php", 700, 450)
    }
    function recibir_actividad(Id)
    {
        $("#tipoactividad").val(Id)
        $("#tipoactividad").prop('selected', true)
    }
    var codsuc = <?=$codsuc ?>;
    VImportePozo(<?=$conpozo ?>);
    var urldir = '<?=$urldir ?>';
    var Op = <?=$Op ?>;
    $(document).ready(function() {
        $("#tabs").tabs();
        $('form').keypress(function(e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });

    });
    function vCatastroSave(Op)
    {
        var sector = $("#sector").val();
        var manzanas = $("#manzanas").val();
        var lote = $("#lote").val();
        if (sector != "0" && manzanas != "0" && lote != "")
        {
            $.ajax({
                //url:'../actualizacion/vcatastro.php',
                url: 'vcatastro.php',
                type: 'POST',
                async: true,
                data: 'sector=' + sector + '&manzanas=' + manzanas + '&lote=' + lote + '&Op=1&' + '&nroinscripcion=' + $("#nroinscripcion").val(),
                dataType: "json",
                success: function(datos)
                {
                    if (datos.resultado == 1)
                    {
                        $("#tabs").tabs({selected: 1});
                        Msj($("#propietario"), 'Existe un usuario con el mismo Catastro : ' + datos.propietario)
                        return false
                    }
                    else
                        GuardarP(Op)
                }
            })
        }
    }
    function ValidarForm(Op)
    {   
        var val= $("#ingnroinscripcion").val();
        if(val==1)
        {
            if (form1.codantiguo.value.length<8) {
                $("#tabs").tabs({selected: 1});
                Msj($("#codantiguo"), "El numero de digitos no es el correcto")                
                return false;
            }
        }
        
        if (cont_fila_presupuesto == 0 && $("#sinpresupuestotext").val() == 0 && $("#nroinspeccion").val() == '')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#nropresupuesto"), "No ha Ingresado ningun numero de presupuesto")
            $("#codcliente").val(0)
            return false;
        }
        $("#nropresupuesto").removeClass("ui-state-error");
        
        if ($("#codcliente").val() == 0 && $("#sinpresupuestotext").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#codantiguo"), "No ha Ingresando el Codigo del Cliente")
            return false;
        }
        $("#codantiguo").removeClass("ui-state-error");
        
        if ($("#codzona").val() == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#codzona"), "Seleccione la Zona")
            return false;
        }
        $("#codzona").removeClass("ui-state-error");
        
        if ($("#sector").val() == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#sector"), "Seleccione el Sector")
            return false;
        }
        $("#sector").removeClass("ui-state-error");
        
        if ($("#manzanas").val() == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#manzanas"), "Seleccione el Manzana")
            return false;
        }
        $("#manzanas").removeClass("ui-state-error");
        
        if ($("#lote").val() == "")
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#lote"), "Digite el Nro. de Lote")
            return false;
        }
        $("#lote").removeClass("ui-state-error");
        
        if ($("#sublote").val() == "")
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#sublote"), "Digite el Nro. de Sub. Lote")
            return false;
        }
        $("#sublote").removeClass("ui-state-error");
        
        if ($("#nroorden").val() == "")
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#nroorden"), "Ingrese Orden")
            return false;
        }
        $("#nroorden").removeClass("ui-state-error");
        
        if ($("#estadoservicio").val() == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#estadoservicio"), "Seleccione el Estado de Servicio")
            return false;
        }
        $("#estadoservicio").removeClass("ui-state-error");
        
        if ($("#propietario").val() == "")
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#propietario"), "Digite el Nombre del Propietario")
            return false;
        }
        $("#propietario").removeClass("ui-state-error");
        
        if ($("#nrodocumento").val() == "")
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#nrodocumento"), "Digite el Nro. de Documento")
            return false;
        }
        $("#nrodocumento").removeClass("ui-state-error");
        
        if ($("#calles").val() == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#calles"), "Seleccione la Calle")
            return false;
        }
        $("#calles").removeClass("ui-state-error");
        
        if ($("#nrocalle").val() == "")
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#nrocalle"), "Digite el Nro. de Calle")
            return false;
        }
        $("#nrocalle").removeClass("ui-state-error");
        
        if ($("#rutadistribucion").val() == "")
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#rutadistribucion"), "Asigne un Ruta de Distribucion para el Sector y Manzana seleccionado")
            return false;
        }
        $("#rutadistribucion").removeClass("ui-state-error");

        if ($("#rutalecturas").val() == "")
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#rutalecturas"), "Asigne un Ruta de Lecturas para el Sector y Manzana seleccionado")
            return false;
        }
        $("#rutalecturas").removeClass("ui-state-error");
        
        if ($("#ciclo").val() == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#ciclo"), "Seleccione el Ciclo de Facturacion")
            return false;
        }
        $("#ciclo").removeClass("ui-state-error");
        
        if ($("#tiposervicio").val() == 0)
        {
            $("#tabs").tabs({selected: 2});
            Msj($("#tiposervicio"), "Seleccione el Tipo de Servicio")
            return false;
        }
        $("#tiposervicio").removeClass("ui-state-error");
        
        if ($('input[name="conpozo"]:checked').val() == 1)
        {
            if (parseFloat($("#conpozoimporte").val()) == 0 || isNaN($("#conpozoimporte").val()))
            {
                $("#tabs").tabs({selected: 2});
                Msj($("#conpozoimporte"), "Ingrese Importe Valido")
                return false;
            }
            $("#conpozoimporte").removeClass("ui-state-error");
        }
        
        if ($("#tipofacturacion").val() == 100)
        {
            $("#tabs").tabs({selected: 4});
            Msj($("#tipofacturacion"), "Seleccione el Tipo de Facturacion")
            return false;
        }
        $("#tipofacturacion").removeClass("ui-state-error");

        if ($("#zonasbastecimiento").val() == 0)
        {
            $("#tabs").tabs({selected: 6});
            Msj($("#zonasbastecimiento"), "Seleccione la Zona de abastecimiento")
            return false;
        }
        $("#zonasbastecimiento").removeClass("ui-state-error");
        
        if ($("#presionagua").val() == 0)
        {
            $("#tabs").tabs({selected: 6});
            Msj($("#presionagua"), "Seleccione la Presion de Agua")
            return false;
        }
        $("#presionagua").removeClass("ui-state-error");
        
        if (parseInt($("#tbunidades tbody tr").length) == 0)
        {
            $("#tabs").tabs({selected: 7});
            Msj($("#tbunidades"), "Aun no se ha Agregado ninguna categoria tarifaria");
            return false;
        }

        validar_cont_presupuesto = 0;
        for (var i = 1; i <= parseInt($("#cont_unidades").val()); i++)
        {
            try
            {
                if ($("#principal" + i).val() == 0)
                {
                    validar_cont_presupuesto++;
                }
            } catch (exp)
            {

            }
        }

        if ($("#principal" + i).val() == validar_cont_presupuesto)
        {
            $("#tabs").tabs({selected: 7});
            Msj($("#tbunidades"), "Aun No ha Asignado una Categoria Principal");
            return false
        }
        $("#tbunidades").removeClass("ui-state-error");
        
        //VALIDAR CATEGORIAS
        c = 0;
        $("#tbunidades input.principal").each(function() {
            if ($(this).val() == 1)
                c++;
        });
        if (c == 0)
        {
            $("#tabs").tabs({selected: 7});

            Msj('#tbunidades', 'Seleccione Categoria Principal')
            return false;
        }
        $("#tbunidades").removeClass("ui-state-error");
        
        if ($("#horasabastecimiento").val() == "") {
            $("#horasabastecimiento").val(0)
        }
        if ($("#nrolavatorios").val() == "") {
            $("#nrolavatorios").val(0)
        }
        if ($("#nrolavaropas").val() == "") {
            $("#nrolavaropas").val(0)
        }
        if ($("#nrowater").val() == "") {
            $("#nrowater").val(0)
        }
        if ($("#nroduchas").val() == "") {
            $("#nroduchas").val(0)
        }
        if ($("#nrourinarios").val() == "") {
            $("#nrourinarios").val(0)
        }
        if ($("#nrogrifos").val() == "") {
            $("#nrogrifos").val(0)
        }
        if ($("#nrohabitantes").val() == "") {
            $("#nrohabitantes").val(0)
        }

        vCatastroSave(Op)
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }

    function cargar_rutadistribucion()
    {

    }
    
    function IngNroinscripcion()
    {
        var val= $("#ingnroinscripcion").val();
        if(val==0)
        {
            $("#codantiguo").attr('disabled',false)
            $("#ingnroinscripcion").attr('checked',true)
            $("#ingnroinscripcion").val(1)
        }
        else
            {
                $("#codantiguo").attr('disabled',true)
                $("#ingnroinscripcion").attr('checked',false)
                $("#ingnroinscripcion").val(0)
            }
    }
    
    
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="1000" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>

                <tr>
                    <td colspan="3" class="TitDetalle">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Presupuesto</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Datos del Cliente</a></li>
                                <li style="font-size:10px"><a href="#tabs-3">Inmueble Predio</a></li>
                                <li style="font-size:10px"><a href="#tabs-4">Conexion de Agua</a></li>
                                <li style="font-size:10px"><a href="#tabs-5">Medidor</a></li>
                                <li style="font-size:10px"><a href="#tabs-6">Desague</a></li>
                                <li style="font-size:10px"><a href="#tabs-7">Cal. de Servicio</a></li>
                                <li style="font-size:10px"><a href="#tabs-8">Und. de Uso</a></li>
                            </ul>
                            <div id="tabs-1" style="height:330px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr height="30px">
                                        <td width="14%" align="right">Nro. Presupuesto</td>
                                        <td width="3%" align="center">:</td>
                                        <td width="83%" valign="middle" >
                                            <input type="text" name="nropresupuesto" readonly="readonly" id="nropresupuesto" size="10" maxlength="10" class="inputtext" />
                                            <!-- <span class="MljSoft-icon-buscar" title="Buscar Presupuesto" onclick="buscar_presupuesto();" ></span> -->
                                            <!--  <span class="icono-icon-add" title="Agregar Presupuesto de Conexion" onclick="agregar_presupuesto();" ></span> -->
                                            <img src="../../../../images/iconos/buscar.png" width="16" height="16" title="Buscar Presupuesto" style="cursor:pointer" onclick="buscar_presupuesto();" />
                                            <img src="../../../../images/iconos/add.png" width="16" height="16" style="cursor:pointer" title="Agregar Presupuesto de Conexion" onclick="agregar_presupuesto();" />
                                            <?php
                                            $checked = "";
                                            $disabled = "";
                                            if (isset($items["sinpresupuesto"])) {
                                                if ($items["sinpresupuesto"] == 1) {
                                                    $checked = "checked='checked'";
                                                    $disabled = "disabled='disabled'";
                                                }
                                            }
                                            ?>
                                            <input type="checkbox" name="sinpresupuesto" id="chksinpresupuesto" <?=$disabled ?> <?=$checked ?> value="checkbox" onclick="CambiarEstado(this, 'sinpresupuestotext');
                                                    validar_estado_presupuesto(this, 'sinpresupuestotext');
                                                    validar_nuevo_usuario(this);" />
                                            <input type="hidden" name="sinpresupuestotext" id="sinpresupuestotext" value="<?=isset($items["sinpresupuesto"]) ? $items["sinpresupuesto"] : 0 ?>" />
                                            Sin Presupuesto
                                            <input type="hidden" name="tipopresupuesto" id="tipopresupuesto" value="" />
                                            <input type="hidden" name="nrosolicitud" id="nrosolicitud" value="" />
                                            <input type="hidden" name="prop" id="prop" />
                                            <input type="hidden" name="calle" id="calle" />
                                            <input type="hidden" name="cont_presupuesto" id="cont_presupuesto" value="0" />
                                        </td>
                                    </tr>
                                    <tr style="display:none;">
                                        <td width="14%" align="right">Nro. Factibilidad</td>
                                        <td width="3%" align="center">:</td>
                                        <td width="83%">
                                            <input type="text" name="nroinspeccion" id="nroinspeccion" size="10" maxlength="10" class="inputtext" readonly="readonly"  />
                                            <span class="MljSoft-icon-buscar" title="Buscar Presupuesto" onclick="buscar_servicio();" ></span>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tbpresupuesto" >
                                                <thead class="ui-widget-header" style="font-size: 11px">
                                                    <tr>
                                                        <th colspan="8" >Datos del Presupuesto</th>
                                                    </tr>
                                                    <tr style="color: white; font-weight: bold; font-size: 12px" align="center">
                                                        <th width="7%" >Nro Presupuesto</th>
                                                        <th width="7%" >Categoria</th>
                                                        <th width="8%" >Nro Solicitud</th>
                                                        <th width="28%" >Propietario</th>
                                                        <th width="28%" >Direccion</th>
                                                        <th width="7%" >Sol. de Conexion</th>
                                                        <th width="7%" >Ficha Prespuestal</th>
                                                        <th width="8%" >Borrar Registro</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $count_presupuesto = 0;
                                                    if ($Id != "") {
                                                        $sql_presupuesto = "SELECT p.nropresupuesto,p.tipo,c.nrosolicitud,c.propietario,tc.descripcioncorta,cl.descripcion as calle,
                                                            c.nrocalle
                                                            FROM catastro.presupuesto_catastro as p
                                                            INNER join solicitudes.cabpresupuesto as c on(p.codemp=c.codemp and p.codsuc=c.codsuc
                                                            and p.nropresupuesto=c.nropresupuesto)
                                                            INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
                                                            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
                                                            WHERE p.codemp=1 and p.codsuc=? and p.nroinscripcion=?";
                                                        $consulta_presupuesto = $conexion->prepare($sql_presupuesto);
                                                        $consulta_presupuesto->execute(array($codsuc, $Id));
                                                        $itemPresupuesto = $consulta_presupuesto->fetchAll();

                                                        foreach ($itemPresupuesto as $rowPresupuesto) {
                                                            $count_presupuesto++;
                                                            ?>
                                                            <tr style='background-color:#FFFFFF; padding:4px'>
                                                                <td align="center">
                                                                    <input type='hidden' name='nropresupuesto<?=$count_presupuesto ?>' id='nropresupuesto<?=$count_presupuesto ?>' value='<?=$rowPresupuesto["nropresupuesto"] ?>' />
                                                                    <?=$rowPresupuesto["nropresupuesto"] ?>
                                                                </td>
                                                                <td align="center">
                                                                    <input type='hidden' name='tipopresupuesto<?=$count_presupuesto ?>' value='<?=$rowPresupuesto["tipo"] ?>' />
                                                                    <?=$rowPresupuesto["tipo"] == 1 ? "AGUA" : "DESAGUE" ?>
                                                                </td>
                                                                <td align="center">
                                                                    <input type='hidden' name='nrosolicitud<?=$count_presupuesto ?>' value='<?=$rowPresupuesto["nrosolicitud"] ?>' />
                                                                    <?=$rowPresupuesto["nrosolicitud"] ?>
                                                                </td>
                                                                <td ><?=$rowPresupuesto["propietario"] ?></td>
                                                                <td ><?=strtoupper($rowPresupuesto["descripcioncorta"]." ".$rowPresupuesto["calle"]." ".$rowPresupuesto["nrocalle"]) ?></td>
                                                                <td align="center">
                                                                    <img src='../../../../images/imprimir.png' title='Imprimir Ficha de Solicitud' width='16' height='16' style='cursor:pointer' onclick='imprimir_ficha_solicitud(<?=$rowPresupuesto["nrosolicitud"] ?>);' />
                                                                </td>
                                                                <td align="center">
                                                                    <img src='../../../../images/imprimir.png' title='Imprimir Ficha Presupuestal' width='16' height='16' style='cursor:pointer' onclick='imprimir_ficha_presupuestal(<?=$rowPresupuesto["nropresupuesto"] ?>);' />
                                                                </td>
                                                                <td align="center">
                                                                    <img src='../../../../images/cancel.png' width='16' height='16' style='cursor:pointer' title='Borrar Registro' onClick='QuitaFilaD(this, 0, 0);'/>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                                <script>
                                                    cont_presupuesto = <?=$count_presupuesto ?>;
                                                    cont_fila_presupuesto = <?=$count_presupuesto ?>;
                                                    $("#cont_presupuesto").val(<?=$count_presupuesto ?>)
                                                </script>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-2" style="height:360px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="16%" align="right">Nro. Inscripcion</td>
                                        <td width="3%" align="center">:</td>
                                        <td width="19%">
                                            <input type="text" class="inputtext" name="codantiguo" id="codantiguo" maxlength="8" size="10" value="<?=$items['codantiguo'] ?>" disabled="disabled"/>
                                            <input type="hidden" class="inputtext"  id="nroinscripcion" name="nroinscripcion" readonly="readonly" maxlentgth="10" size="10" value="<?=$Id ?>"/>
                                            <input type="hidden" name="codcliente" id="codcliente" value="<?=isset($items["codcliente"]) ? $items["codcliente"] : 0 ?>" />
                                            <?php
                                                if($Op==0)
                                                {
                                            ?>
                                            <!--<input type="checkbox" id="ingnroinscripcion" name="ingnroinscripcion" onclick="IngNroinscripcion();" value="0" /> Ingresar Nro. Inscripcion-->
                                            <?php
                                                }
                                            ?>
                                        <td width="10%" align="right">Sucursal</td>
                                        <td width="3%" align="center">:</td>
                                        <td width="49%">
                                            <input type="text" class="inputtext"  id="sucursal1" name="sucursal" readonly="readonly" maxlentgth="30" size="30" value="<?=$sucursal["descripcion"] ?>"/>
                                            <input type="hidden" name="cCatastro" id="cCatastro" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Zona</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php
                                            echo $objMantenimiento->drop_zonas($codsuc, $items["codzona"], "onchange='cargar_sectores(".$codsuc.",this.value,0,0); cargar_urbanizacion(".$codsuc.", this.value, 0, 0)';");

                                            if ($Id != "") {
                                                ?>
                                                <script>
                                                    cargar_sectores(<?=$codsuc ?>,<?=$items["codzona"] ?>, '<?=$items["codsector"] ?>', 1);
                                                    cargar_manzana(<?=$codsuc ?>,<?=$items["codsector"] ?>, '<?=$items["codmanzanas"] ?>', 1);
                                                    cargar_calles(<?=$items["codsector"] ?>,<?=$codsuc ?>,<?=$items["codzona"] ?>,<?=$items["codcalle"] ?>)
                                                    cargar_zonas_abastecimiento(<?=$items["codsector"] ?>,<?=$items["codzonaabas"] ?>, 0);
                                                    cargar_horarios_zonas(<?=$items["codzona"] ?>,<?=$items["codsector"] ?>);
                                                    cargar_rutas_lecturas('<?=$items["codmanzanas"] ?>', '<?=$items["codsector"] ?>')
                                                    cargar_rutas_distribucion('<?=$items["codmanzanas"] ?>', '<?=$items["codsector"] ?>')
                                                    cargar_urbanizacion(<?=$codsuc ?>, <?=$items["codzona"] ?>, '<?=$items["codurbanizacion"] ?>', 1);
                                                </script>
                                            <?php } ?>
                                        </td>
                                        <td align="right">Sector</td>
                                        <td align="center">:</td>
                                        <td>
                                            <div id="div_sector">
                                                <select name="sector" id="sector" style="width:220px" class="select">
                                                    <option value="0">.:: Seleccione el Sector ::.</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Manzana</td>
                                        <td align="center">:</td>
                                        <td>
                                            <div id="div_manzanas">
                                                <select name="manzanas" id="manzanas" style="width:220px" class="select">
                                                    <option value="0">.:: Seleccione la Manzana ::.</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td align="right">Habiitación</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div id="div_urbanizacion">
                                                                <select name="sector" id="sector" style="width:220px" class="select">
                                                                    <option value="0">.:: Seleccione el Urbanizacion ::.</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <span style="position:relative" class="MljSoft-icon-buscar" title="Buscar Urbanizacion" onclick="buscar_urbanizacion();" id="imgBuscarC" ></span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Lote</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php
                                            $checked = "";
                                            $disabled = "";

                                            if (isset($items["flag_nuevo"])) {
                                                if ($items["flag_nuevo"] == 1) {
                                                    $checked = "checked='checked'";
                                                    $disabled = "disabled='disabled'";
                                                }
                                            }
                                            ?>
                                            <input type="text" class="inputtext"  id="lote" name="lote" maxlentgth="10" size="10" value="<?=$items["nrolote"] ?>" onkeypress="return permite(event, 'num');" onchange="vCatastro();" placeholder="LOTE" />
                                            <input type="checkbox" name="flagnuevo" <?=$disabled ?> id="flagnuevo" <?=$checked ?> onclick="CambiarEstado(this, 'flagnuevotext');validar_estado_presupuesto(this, 'flagnuevotext');
                                                    quitar_readonly(this, 'propietario');" /> Usuario Nuevo
                                            <!--Usuario Nuevo-->
                                            <input type="hidden" name="flagnuevotext" id="flagnuevotext" value="<?=isset($items["flag_nuevo"]) ? $items["flag_nuevo"] : 0 ?>" />
                                        </td>
                                        <td align="right">Sub. Lote</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" class="inputtext"  id="sublote" name="sublote" maxlentgth="10" size="10" value="<?=$items["sublote"] ?>" onkeypress="return permite(event, 'num');" placeholder="SUB LOTE" />
                                            <!-- &nbsp;Orden&nbsp;:&nbsp; -->
                                            <input type="hidden" class="inputtext"  id="nroorden" name="nroorden" maxlentgth="10" size="11" value="0" onkeypress="return permite(event, 'num');"/>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Est. de Servicio</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_servicio($items["codestadoservicio"]); ?></td>
                                        <td align="right">C. Usuario</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_responsable($items["codtiporesponsable"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Propietario</td>
                                        <td align="center">:</td>
                                        <td colspan="4">
                                            <input type="text" class="inputtext"  id="propietario" name="propietario" readonly="readonly" maxlentgth="200" size="100" value="<?=$items["propietario"] ?>" placeholder="DATOS DEL PROPIETARIO" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Documento</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipodocumento($items["codtipodocumento"]); ?>
                                        </td>
                                        <td align="right">Nro. Doc.</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php
                                            $checked = "";
                                            if (isset($items["altocon"])) {
                                                if ($items["altocon"] == 1) {
                                                    $checked = "checked='checked'";
                                                }
                                            }
                                            ?>
                                            <input type="text" class="inputtext"  id="nrodocumento" name="nrodocumento" maxlength="11" size="10" value="<?=$items["nrodocumento"] ?>" onkeypress="return permite(event, 'num');"/>
                                            <input type="checkbox" name="altoconsumidor" id="altoconsumidor" onclick="CambiarEstado(this, 'altoconsumidortext');" <?=$checked ?> />
                                            Alto Consumidor
                                            <input type="hidden" name="altoconsumidortext" id="altoconsumidortext" value="<?=isset($items["altocon"]) ? isset($items["altocon"]) : 0 ?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Calle</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div id="div_calles">
                                                            <select id="calles" name="calles" style="width:220px" class="select">
                                                                <option value="0">.:: Seleccione la Calle ::.</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <span style="position:relative" class="MljSoft-icon-buscar" title="Buscar Calle" onclick="buscar_calle();"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">Nro. Calle</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" class="inputtext"  id="nrocalle" name="nrocalle" size="20" value="<?=$items["nrocalle"] ?>" placeholder="Nro. calle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Grupo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_entidades($items["codtipoentidades"]); ?>
                                        </td>
                                        <td align="right">T. Usuario</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_usuario($items["codtipousuario"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">R. Distri.</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" class="inputtext"  id="rutadistribucion" name="rutadistribucion" readonly="readonly" maxlentgth="10" size="10" value=""/>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secuencia&nbsp;:&nbsp;
                                            <?php
                                            // $items['orden_dist']
                                            ?>
                                            <input type="text" class="inputtext"  id="orden_dist" name="orden_dist" maxlentgth="10" size="8" value="<?php echo $items['orden_dist'] = (empty($items['orden_dist'])) ? '0' : $items['orden_dist']; ?>" onkeypress="return permite(event, 'num');" onkeyup="actualizacion(this, 0)" />
                                            <input type="hidden" name="codrutdistribucion" id="codrutdistribucion" value="0" /></td>
                                        <td align="right">R. Lect.</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" class="inputtext"  id="rutalecturas" name="rutalecturas" readonly="readonly" maxlentgth="10" size="10" value=""/>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secuencia&nbsp;:&nbsp;
                                            <input type="text" class="inputtext"  id="orden_lect" name="orden_lect" maxlentgth="10" size="8" value="<?php echo $items['orden_lect'] = (empty($items['orden_lect'])) ? '0' : $items['orden_lect']; ?>" onkeypress="return permite(event, 'num');" onkeyup="actualizacion(this, 0)" />
                                            <input type="hidden" name="codrutlectura" id="codrutlectura" value="0" /></td>
                                    </tr>
                                    <!--<tr>
                                        <td align="right">Urbanizacion:</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div id="div_urbanizacion">
                                                                <select name="sector" id="sector" style="width:220px" class="select">
                                                                    <option value="0">.:: Seleccione el Urbanizacion ::.</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <span style="position:relative" class="MljSoft-icon-buscar" title="Buscar Urbanizacion" onclick="buscar_urbanizacion();" id="imgBuscarC" ></span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>-->

                                    <tr>
                                        <td align="right">Dir. Distribucion</td>
                                        <td align="center">:</td>
                                        <td colspan="4">
                                            <input type="text" class="inputtext"  id="dirdistribucion" name="dirdistribucion" maxlentgth="200" size="100" value="<?=$items["direcciondistribucion"] ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Inquilino</td>
                                        <td align="center">:</td>
                                        <td >
                                            <input type="text" class="inputtext"  id="inquilino" name="inquilino" maxlentgth="200" size="50" value="<?=$items["inquilino"] ?>"/>
                                        </td>
                                        <td align="right">Correo</td>
                                        <td align="center">:</td>
                                        <td >
                                            <input type="text" class="inputtext3"  id="correo" name="correo" maxlentgth="200" size="50" value="<?=$items["correo"] ?>" />
                                        </td>
                                        
                                    </tr>
                                    <!--
                                    <tr>
                                        <td align="right">Correo</td>
                                        <td align="center">:</td>
                                        <td >
                                            <input type="text" class="inputtext3"  id="correo" name="correo" maxlentgth="200" size="100" value="<?=$items["correo"] ?>" />
                                        </td>
                                    </tr>
                                    -->
                                    <tr>
                                       <!--  <td align="right">Actividad</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <?php $objMantenimiento->drop_tipo_actividad($items["codtipoactividad"]); ?>
                                                    </td>
                                                    <td>
                                                        <span class="MljSoft-icon-buscar" title="Buscar Actividad" onclick="buscar_actividad();"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>   -->
                                        <td align="right">Ciclo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_ciclos($codsuc, $items["codciclo"]); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-3" style="height:330px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="15%" align="right">T. Predio</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%">
                                            <select name="tipopredio" id="tipopredio" style="width:220px" class="select">
                                                <option value="1" <?=$items["tipopredio"] == 1 ? "selected='selected'" : "" ?> >UNIFAMILIAR</option>
                                                <option value="2" <?=$items["tipopredio"] == 2 ? "selected='selected'" : "" ?> >MULTIFAMILIAR</option>
                                            </select>
                                        </td>
                                        <td width="12%" align="right">T. Construccion</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="44%">
                                            <?php $objMantenimiento->drop_tipo_construccion($items["codtipoconstruccion"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Pisos</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" class="inputtext"  id="nropisos" name="nropisos" maxlentgth="10" size="10" value="<?=isset($items["nropisos"]) ? $items["nropisos"] : 1 ?>"/>
                                        </td>
                                        <td align="right">Abastecimiento</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_abastecimiento($items["codtipoabastecimiento"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Piscina</td>
                                        <td align="center">:</td>
                                        <td>

                                            <?php
                                            if ($items['piscina'] == 1) {
                                                $ck1 = "checked=''";
                                                $ck2 = "";
                                            } else {
                                                $ck1 = "";
                                                $ck2 = "checked";
                                            }
                                            ?>
                                            <div class="buttonset" style="display:inline">
                                                <input type="radio" name="piscina" id="piscina1" value="1" <?php echo $ck1; ?> />
                                                <label for="piscina1">Si</label>
                                                <input type="radio" name="piscina" id="piscina0" value="0" <?php echo $ck2; ?> />
                                                <label for="piscina0">No</label>
                                            </div>
                                        </td>
                                        <td align="right">Almacenaje</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_almacenaje($items["codtipoalmacenaje"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tipo de Servicio</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tiposervicio($items["codtiposervicio"], ""); ?>
                                        </td>
                                        <td align="right">Tipo de Material</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipomatconstruccion($items["codtipomatconstruccion"], ""); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Pozo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php
                                            if ($items['conpozo'] == 1) {
                                                $ck1 = "checked=''";
                                                $ck2 = "";
                                            } else {
                                                $ck1 = "";
                                                $ck2 = "checked";
                                            }
                                            ?>
                                            <div class="buttonset" style="display:inline">
                                                <input type="radio" name="conpozo" id="conpozo1" value="1" <?php echo $ck1; ?> onChange="VImportePozo(1)"/>
                                                <label for="conpozo1">Si</label>
                                                <input type="radio" name="conpozo" id="conpozo0" value="0" <?php echo $ck2; ?> onChange="VImportePozo(0)"/>
                                                <label for="conpozo0">No</label>
                                            </div>
                                        </td>
                                        <td align="right" class="cconpozoimporte">Importe Pozo</td>
                                        <td align="center" class="cconpozoimporte">:</td>
                                        <td class="cconpozoimporte">
                                            <input type="text" class="inputtext numeric"  id="conpozoimporte" name="conpozoimporte" maxlentgth="10" size="10" value="<?=isset($items["conpozoimporte"]) ? $items["conpozoimporte"] : 0 ?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">Referencia</td>
                                        <td align="center" valign="top">:</td>
                                        <td colspan="4">
                                            <textarea name="referencia" id="referencia" cols="107" rows="5" class="inputtext"><?=$items["referencia"] ?></textarea>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-4" style="height:330px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="15%" align="right">Mat. Tubo</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%">
                                            <?php $objMantenimiento->drop_tipo_material_tubo_agua($items["codtipomaterialagua"]); ?>
                                        </td>
                                        <td width="18%" align="right">L. Caja</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="38%">
                                            <?php $objMantenimiento->drop_localizacion_caja_agua($items["codlocacajaagua"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Diametro Tubo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_diametros_agua($items["coddiametrosagua"]); ?>
                                        </td>
                                        <td align="right">Pavimento</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_pavimento_agua($items["codtipopavimentoagua"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tipo de Corte</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_corte($items["codtipocorte"]); ?>
                                        </td>
                                        <td align="right">Est. Conexion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_conexion($items["codestadoconexion"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Vereda</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_vereda($items["codtipovereda"]); ?>
                                        </td>
                                        <td align="right">Fecha Instalacion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="fechainstagua" id="fechainstagua" class="inputtext" maxlentgth="6" value="<?=$fechainstagua ?>" style="width:75px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_caja_agua($items["codtipocajaagua"]); ?>
                                        </td>
                                        <td align="right">Est. Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_caja_agua($items["codestadocajaagua"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tapa</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_tapa_agua($items["codtipotapaagu"]); ?>
                                        </td>
                                        <td align="right">Est. Tapa</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="estadotapaagua" name="estadotapaagua" style="width: 220px" class="select">
                                                <option value="1" <?=$items["estadotapaagu"] == 1 ? "selected='selected'" : "" ?> >EN BUEN ESTADO</option>
                                                <option value="2" <?=$items["estadotapaagu"] == 2 ? "selected='selected'" : "" ?> >EN MAL ESTADO</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Fugas</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_fugas_agua($items["codtipofugasagua"]); ?>
                                        </td>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-5" style="height:330px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="15%" align="right">Marca</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%">
                                            <?php $objMantenimiento->drop_marca_medidor($items["codmarca"]); ?>
                                        </td>
                                        <td width="18%" align="right">Diametro</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="38%">
                                            <?php $objMantenimiento->drop_diametros_medidor($items["coddiametrosmedidor"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Estado</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_medidor($items["codestadomedidor"]); ?>
                                        </td>
                                        <td align="right">Posicion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="posicionmedidor" name="posicionmedidor" style="width: 220px" class="select">
                                                <option value="1" <?=$items["posicionmed"] == 1 ? "selected='selected'" : "" ?> >CORRECTA</option>
                                                <option value="2" <?=$items["posicionmed"] == 2 ? "selected='selected'" : "" ?> >INCORRECTA</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Modelo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_modelo_medidor($items["codmodelo"]); ?>
                                        </td>
                                        <td align="right">Tipo de Lectura</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="tipolectura" name="tipolectura" style="width: 220px" class="select">
                                                <option value="1" <?=$items["tipolectura"] == 1 ? "selected='selected'" : "" ?> >CIRCULAR</option>
                                                <option value="2" <?=$items["tipolectura"] == 2 ? "selected='selected'" : "" ?> >DIRECTA</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">A&ntilde;o de Fabricacion</td>
                                        <td align="center">:</td>
                                        <td><input type="text" name="aniofab" id="aniofab" class="inputtext" maxlength="4" size="15" value="<?=$items["aniofabmed"] ?>" onkeypress="return permite(event, 'num');" /></td>
                                        <td align="right">Tipo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_medidor($items["codtipomedidor"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Fecha Instalacion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="fechainstmedidor" id="fechainstmedidor" class="inputtext" maxlentgth="6" value="<?=$fechainstmedidor ?>" style="width:80px;" />
                                        </td>
                                        <td align="right">Capacidad</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_capacidad_medidor($items["codcapacidadmedidor"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Nro. Medidor</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="nromedidor" id="nromedidor" class="inputtext" size="15" value="<?=$items["nromed"] ?>" />
                                        </td>
                                        <td align="right">Tipo de Facturacion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="tipofacturacion" name="tipofacturacion" style="width:220px" class="select">
                                                <option value="100" >--Seleccione el Tipo--</option>
                                                <option value="0" <?=$items["tipofacturacion"] == 0 ? "selected='selected'" : "" ?> >CONSUMO LEIDO</option>
                                                <option value="1" <?=$items["tipofacturacion"] == 1 ? "selected='selected'" : "" ?> >CONSUMO PROMEDIADO</option>
                                                <option value="2" <?=$items["tipofacturacion"] == 2 ? "selected='selected'" : "" ?> >CONSUMO ASIGNADO</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Ub. Llave</td>
                                        <td align="center">:</td>
                                        <td>
                                            <? $objMantenimiento->drop_ubicacion_llave_medidor($items["codubicacion"]); ?>
                                        </td>
                                        <td align="right">Lect. Anterior</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="lecturaanterior" id="lecturaanterior" class="inputtext" maxlentgth="6" size="15" value="<?=intval($items["lecturaanterior"]) ?>" onkeypress="return permite(event, 'num');"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>

                                        <td align="right">Lect. Ultima</td>
                                        <td align="center">:</td>
                                        <td><input type="text" name="lecturaultima" id="lecturaultima" class="inputtext" maxlentgth="6" size="15" value="<?=intval($items["lecturaultima"]) ?>" onkeypress="return permite(event, 'num');"  /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>

                                        <td align="right">Consumo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="consumo" id="consumo" class="inputtext" maxlentgth="6" size="15" value="<?=intval($items["consumo"]) ?>" onkeypress="return permite(event, 'num');" />
                                            <span class="icono-icon-info" title="Asignar Consumos" onclick="VerTarifas()"></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-6" style="height:330px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="15%" align="right">Diametro</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%">
                                            <?php $objMantenimiento->drop_diametros_desague($items["coddiametrosdesague"]); ?>
                                        </td>
                                        <td width="18%" align="right">Mat. Tubo</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="38%">
                                            <?php $objMantenimiento->drop_tipo_material_tubo_desague($items["codtipomaterialdesague"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Localizacion Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_localizacion_caja_desague($items["codlocacajadesague"]); ?>
                                        </td>
                                        <td align="right">Fecha Instalacion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="fechainstdesague" id="fechainstdesague" class="inputtext" maxlentgth="6" value="<?=$fechainstdesague ?>" style="width:80px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tipo de Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_caja_desague($items["codtipocajadesague"]); ?>
                                        </td>
                                        <td align="right">Estado de la Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_caja_desague($items["codestadocajadesague"]); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tipo de Tapa</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_tapa_desague($items["codtipotapadesague"]); ?>
                                        </td>
                                        <td align="right">Estado de Tapa</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="estadotapadesague" name="estadotapadesague" style="width: 220px" class="select">
                                                <option value="1" <?=$items["estadotapadesague"] == 1 ? "selected='selected'" : "" ?> >EN BUEN ESTADO</option>
                                                <option value="2" <?=$items["estadotapadesague"] == 2 ? "selected='selected'" : "" ?> >EN MAL ESTADO</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Fugas y Atoros</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="fugasyatoros" name="fugasyatoros" style="width: 220px" class="select">
                                                <option value="1" <?=$items["fugasatoros"] == 1 ? "selected='selected'" : "" ?> >NO HAY</option>
                                                <option value="2" <?=$items["fugasatoros"] == 1 ? "selected='selected'" : "" ?> >SI HAY</option>
                                            </select>
                                        </td>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">
                                            <?php
                                            $checked = "";
                                            if (isset($items["desagueinactivo"])) {
                                                if ($items["desagueinactivo"] == 1) {
                                                    $checked = "checked='checked'";
                                                }
                                            }
                                            ?>
                                            <input type="checkbox" name="desagueinactivo" id="desagueinactivo" <?=$checked ?> onclick="CambiarEstado(this, 'desagueinactivotext');" />
                                        </td>
                                        <td>
                                            Servicio Inactivo
                                            <input type="hidden" name="desagueinactivotext" id="desagueinactivotext" value="<?=isset($items["desagueinactivo"]) ? $items["desagueinactivo"] : 0 ?>" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-7" style="height:330px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="17%" align="right">Zona de abastecimiento</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="23%">
                                            <div id="div_zonas">
                                                <?php $objMantenimiento->drop_zonas_abastecimiento_sector($codsuc, 0) ?>
                                            </div>
                                        </td>
                                        <td width="18%" align="right">Presion del Agua</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="38%">
                                            <select id="presionagua" name="presionagua" style="width: 220px" class="select">
                                                <option value="0" >.:: Seleccione el Tipo ::.</option>
                                                <option value="1" <?=$items["presionagua"] == 1 ? "selected='selected'" : "" ?> >BAJA</option>
                                                <option value="2" <?=$items["presionagua"] == 2 ? "selected='selected'" : "" ?> >NORMAL</option>
                                                <option value="3" <?=$items["presionagua"] == 3 ? "selected='selected'" : "" ?> >ALTA</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="14%" align="right">Horas Abast. </td>
                                                    <td width="4%" align="center">:</td>
                                                    <td width="9%">
                                                        <?php
                                                        $abastecimiento = $items["horasabastecimiento"];
                                                        if (isset($items["horasabastecimiento"])) {
                                                            if ($items["horasabastecimiento"] != 0) {
                                                                $abastecimiento = $items["horasabastecimiento"];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="text" class="inputtext" name="horasabastecimiento" id="horasabastecimiento" readonly="readonly" maxlenght="10" size="10" value="<?=$abastecimiento ?>" />
                                                    </td>
                                                    <td width="15%" align="right">Nro. Lavatorios</td>
                                                    <td width="4%" align="center">:</td>
                                                    <td width="9%">
                                                        <?php
                                                        $nrolavatorios = "";
                                                        if (isset($items["nrolavatorios"])) {
                                                            if ($items["nrolavatorios"] != 0) {
                                                                $nrolavatorios = $items["nrolavatorios"];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="text" class="inputtext" name="nrolavatorios" id="nrolavatorios" maxlenght="10" size="10" value="<?=$nrolavatorios ?>" />
                                                    </td>
                                                    <td width="18%" align="right">Nro. Lavaropas</td>
                                                    <td width="4%" align="center">:</td>
                                                    <td width="23%">
                                                        <?php
                                                        $nrolavaropas = "";
                                                        if (isset($items["nrolavaropas"])) {
                                                            if ($items["nrolavaropas"] != 0) {
                                                                $nrolavaropas = $items["nrolavaropas"];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="text" class="inputtext" name="nrolavaropas" id="nrolavaropas" maxlenght="10" size="10" value="<?=$nrolavaropas ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Nro. Water</td>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <?php
                                                        $nrowater = "";
                                                        if (isset($items["nrowater"])) {
                                                            if ($items["nrowater"] != 0) {
                                                                $nrowater = $items["nrowater"];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="text" class="inputtext" name="nrowater" id="nrowater" maxlenght="10" size="10" value="<?=$nrowater ?>" />
                                                    </td>
                                                    <td align="right">Nro. Duchas</td>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <?php
                                                        $nroduchas = "";
                                                        if (isset($items["nroduchas"])) {
                                                            if ($items["nroduchas"] != 0) {
                                                                $nroduchas = $items["nroduchas"];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="text" class="inputtext" name="nroduchas" id="nroduchas" maxlenght="10" size="10" value="<?=$nrowater ?>" />
                                                    </td>
                                                    <td align="right">Nro. Urinarios</td>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <?php
                                                        $nrourinarios = "";
                                                        if (isset($items["nrourinarios"])) {
                                                            if ($items["nrourinarios"] != 0) {
                                                                $nrourinarios = $items["nrourinarios"];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="text" class="inputtext" name="nrourinarios" id="nrourinarios" maxlenght="10" size="10" value="<?=$nrourinarios ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Nro. Grifos</td>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <?php
                                                        $nrogrifos = "";
                                                        if (isset($items["nrogrifos"])) {
                                                            if ($items["nrogrifos"] != 0) {
                                                                $nrogrifos = $items["nrogrifos"];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="text" class="inputtext" name="nrogrifos" id="nrogrifos" maxlenght="10" size="10" value="<?=$nrogrifos ?>" />
                                                    </td>
                                                    <td align="right">Nro. Habitantes</td>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <?php
                                                        $nrohabitantes = "";
                                                        if (isset($items["nrohabitantes"])) {
                                                            if ($items["nrohabitantes"] != 0) {
                                                                $nrohabitantes = $items["nrohabitantes"];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="text" class="inputtext" name="nrohabitantes" id="nrohabitantes" maxlenght="10" size="10" value="<?=$nrohabitantes ?>" /></td>
                                                    <td align="right">&nbsp;</td>
                                                    <td align="center">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td align="right">Referencia</td>
                                        <td align="center">:</td>
                                        <td colspan="4"><textarea name="observacion" id="observacion" cols="90" rows="10" class="inputtext" style="font-size:12px" ><?=$items["observacionabast"] ?></textarea></td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-8" style="height:330px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="8%">&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td width="69%">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td width="20%" align="right">Categoria</td>
                                        <td width="3%" align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tarifas($codsuc); ?>
                                            <input type="checkbox" name="principal_categoria_check" id="principal_categoria_check" <?php if ($Op == 0) {echo 'checked="checked"';}?> onclick="CambiarEstado(this, 'principal_categoria')"  />
                                            Categoria Principal
                                            <input type="hidden" name="principal_categoria" id="principal_categoria" value="<?php if ($Op == 0) {echo '1';}else{echo '0';}?>" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="right">Actividad</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div id="div_rutasdistribucion_2">
                                                            <?php $objMantenimiento->drop_tipo_actividad($items["codtipoactividad"]); ?>
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <span class="MljSoft-icon-buscar" title="Buscar Actividad" onclick="buscar_actividad();"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="right">Porcentaje</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="procentaje" id="procentaje" class="inputtext" onkeypress="return permite(event, 'num')" value="100" />
                                            <img src="../../../../images/iconos/add.png" style="cursor: pointer" onclick="agregar_unidades_uso(<?=$codsuc ?>);" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <input name="cont_unidades" id="cont_unidades" type="hidden" value="0" />
                                            <input type="hidden" name="total_porct" id="total_porct" value="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div style="overflow:auto; height: 200px">
                                                <table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tbunidades" >
                                                    <thead class="ui-widget-header" style="font-size: 11px">
                                                        <tr>
                                                            <th width="12%" >Codigo</th>
                                                            <th width="25%" >Categoria</th>
                                                            <th width="35%" >Actividad</th>
                                                            <th width="15%" >Porcentaje</th>
                                                            <th width="13%" >Principal</th>
                                                            <th width="5%" >&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $cont_unidades = 0;
                                                        $tot_porcentaje = 0;

                                                        if ($Id != "") {
                                                            $sql_unidades = "SELECT u.catetar, t.nomtar, u.porcentaje ,u.principal,a.descripcion as actividad,a.codtipoactividad ";
                                                            if ($IdUsuario == 42) {
                                                                $sql_unidades .= "FROM gis.unidadesusocatastro AS u ";
                                                            } else {
                                                                $sql_unidades .= "FROM catastro.unidadesusocatastro AS u ";
                                                            }
                                                            $sql_unidades .= " INNER JOIN facturacion.tarifas AS t on(u.codemp=t.codemp AND u.codsuc=t.codsuc AND u.catetar=t.catetar)
                                                                    INNER JOIN public.tipoactividad as a on(u.codtipoactividad=a.codtipoactividad)
                                                                    WHERE u.codemp=1 AND u.codsuc=? AND u.nroinscripcion=? AND t.estado = 1";

                                                                $consulta_unidades = $conexion->prepare($sql_unidades);
                                                                $consulta_unidades->execute(array($codsuc, $Id));
                                                                $items_unidad = $consulta_unidades->fetchAll();

                                                                foreach ($items_unidad as $rowUnidades) {
                                                                    $cont_unidades++;

                                                                    $tot_porcentaje += $rowUnidades["porcentaje"];
                                                                    ?>
                                                                <tr style='background-color:#FFFFFF; padding:4px'>
                                                                    <td align="center" >
                                                                        <input type="hidden" name="ItemD<?=$cont_unidades ?>" id="ItemD<?=$cont_unidades ?>" value="<?=$cont_unidades ?>" />
                                                                        <input type='hidden' id='tarifas<?=$cont_unidades ?>' name='tarifas<?=$cont_unidades ?>' value='<?=$rowUnidades["catetar"] ?>' />
                                                                        <?=$rowUnidades["catetar"] ?>
                                                                    </td>

                                                                    <td >
                                                                        <input type='hidden' id='porcentaje<?=$cont_unidades ?>' name='porcentaje<?=$cont_unidades ?>' value='<?=$rowUnidades["porcentaje"] ?>' />
                                                                        <?=strtoupper($rowUnidades["nomtar"]) ?>
                                                                    </td>
                                                                    <td align="center" >
                                                                        <input type="hidden" name="codtipoactividad<?=$cont_unidades ?>" id="codtipoactividad<?=$cont_unidades ?>" value="<?=$rowUnidades["codtipoactividad"] ?>" />
                                                                        <?=$rowUnidades["actividad"] ?>
                                                                    </td>
                                                                    <td align="center" ><?=number_format($rowUnidades["porcentaje"], 0) ?></td>
                                                                    <td align="center" >
                                                                        <input type='hidden' class='principal' id='principal<?=$cont_unidades ?>' name='principal<?=$cont_unidades ?>' value='<?=$rowUnidades["principal"] ?>' />
                                                                        <?php //$rowUnidades["principal"]==1?"SI":"NO"  ?>
                                                                        <input type="radio" name="principalUU" id="principalUU<?=$cont_unidades ?>" value="<?=$rowUnidades["codunidad_uso"] ?>") <?php if ($rowUnidades["principal"] == 1) {
                                                                    echo 'checked="checked"';
                                                                } ?> onChange='Numerar();
                                                                                        actualizacion(this, 3);' style="cursor:pointer" />
                                                                    </td>
                                                                    <td align="center" >
                                                                        <img src='../../../../images/cancel.png' width='16' height='16' title='Borrar Registro' onclick='QuitaFilaD(this, 1,<?=$cont_unidades ?>);' style='cursor:pointer'/>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <script>
                                                        cont_unidad = <?=$cont_unidades ?>;
                                                        cont_fila_unidad = <?=$cont_unidades ?>;
                                                        total_porcentaje = <?=$tot_porcentaje ?>;

                                                        $("#cont_unidades").val(<?=$cont_unidades ?>)
                                                        $("#total_porct").val(<?=$tot_porcentaje ?>)

                                                        $("#procentaje").val(100 - <?=$tot_porcentaje ?>)
                                                    </script>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody>

        </table>
    </form>
</div>

<div id="DivTarifas" title="Tarifas" style="display: none;">
    <div id="DivTarifasAcordion">
    <?php
    $sql = "select * from facturacion.tarifas WHERE codsuc=".$codsuc." ORDER BY catetar";
    $ConsultaTar = $conexion->query($sql);
    foreach ($ConsultaTar->fetchAll() as $tarifas) {
        ?>
            <h3><?=strtoupper($tarifas["nomtar"]) ?></h3>
            <div style="height: 100px; display: block;">
                <table width="100%" border="0" align="center" cellspacing="0" class="ui-widget" rules="cols">
                    <thead class="ui-widget-header" style="font-size:10px">
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center">Volumen</td>
                            <td align="center">Importe Agua</td>
                            <td align="center">Importe Desague</td>
                            <td align="center">&nbsp;</td>
                        </tr>
                    </thead>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Inicial</td>
                        <td align="right">
                            <label class="inputtext" id="hastarango1" ><?=number_format($tarifas["hastarango1"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmin" ><?=$tarifas["impconsmin"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmindesa"><?=$tarifas["impconsmindesa"] ?></label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango1"], 0) ?>')"></span></td>
                    </tr>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Intermedio </td>
                        <td align="right">
                            <label class="inputtext" id="hastarango2"><?=number_format($tarifas["hastarango2"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmedio"><?=$tarifas["impconsmedio"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmediodesa"><?=$tarifas["impconsmediodesa"] ?> </label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango2"], 0) ?>')"></span></td>
                    </tr>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Final </td>
                        <td align="right">
                            <label class="inputtext" id="hastarango3"><?=number_format($tarifas["hastarango3"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsexec"><?=$tarifas["impconsexec"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsexecdesa"><?=$tarifas["impconsexecdesa"] ?></label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango3"], 0) ?>')"></span></td>
                    </tr>
                </table>
            </div>
    <?php
}
?>
    </div>
</div>

<?php
if ($Id == "") {
    ?>
    <script>
        //$("#estadoservicio").val(2)
        $("#tipofacturacion").val(2)
        $("#presionagua").val(2)
    </script>
    <?php
}
?>
