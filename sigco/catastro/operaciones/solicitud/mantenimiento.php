<?php
	session_name("pnsu");
	if (!session_start()) {session_start();}
	
	$Sede = $_SESSION['Sede'];
	
	include("../../../../objetos/clsDrop.php");
	
	$objDrop = new clsDrop();

	$Op		= $_POST["Op"];
	$Id		= isset($_POST["Id"]) ? $_POST["Id"] : '';
	$codsuc	= $_SESSION['IdSucursal'];
	$guardar = "op=".$Op;

	$nroinscripcion = $objDrop->setCorrelativosVarios(10, $codsuc, "SELECT", 0);

	$paramae = $objDrop->getParamae("IMPIGV", $codsuc);

	if ($Id != '') 
	{
    	$sql = "SELECT s.nroinspeccion, s.nroinscripcion, s.codinspector, i.nombres, s.fechareg,
            s.glosa, s.diasatencion, '' AS concepto, s.codconcepto,
            s.codzona, s.codsector, s.codmanzanas, s.codcalle, s.nrocalle, s.lote, s.sublote, s.codcliente,
            s.nroinscripcion, s.propietario,
            s.nuevocliente, s.correo,
            tc.descripcioncorta || ' ' || c.descripcion AS direccion
            FROM catastro.solicitudinspeccion s
            INNER JOIN reglasnegocio.inspectores i ON (s.codinspector = i.codinspector AND s.codsuc = i.codsuc AND i.codemp = s.codemp)
            LEFT JOIN public.calles c ON c.codcalle = s.codcalle AND c.codemp = s.codemp AND c.codsuc = s.codsuc AND c.codzona = s.codzona
            LEFT JOIN public.tiposcalle tc ON (tc.codtipocalle= c.codtipocalle)
            WHERE s.nroinspeccion = ".$Id." AND s.codsuc = ".$codsuc;

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array());
    $row = $consulta->fetch();
    
	$nroinscripcion = $row['nroinscripcion'];
    $calle = $row['direccion'];
    $guardar = $guardar."&Id2=".$Id;

}
?>
<style type="text/css">
    .CodConcepto { display: none;}
</style>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script >
    var object = "";
    var codsuc = '<?=$codsuc ?>'
    var urldir = '<?php echo $_SESSION['urldir']; ?>';

    $(function () {
        $("#tabs").tabs();
        cargar_sectores(<?=$codsuc ?>, '<?=$row["codzona"] ?>', '<?=$row["codsector"] ?>', 1);
        cargar_manzana(<?=$codsuc ?>, '<?=$row["codsector"] ?>', '<?=$row["codmanzanas"] ?>', 1);
        cargar_calles('<?=$row["codsector"] ?>',<?=$codsuc ?>, '<?=$row["codzona"] ?>', '<?=$row["codcalle"] ?>')

        $("#fechaemision").mask("99/99/9999");
        $("#fechaemision").datepicker(
                {
                    showOn: 'button',
                    direction: 'up',
                    buttonImage: '../../../../images/iconos/calendar.png',
                    buttonImageOnly: true,
                    showOn: 'both',
                            showButtonPanel: true,
                    beforeShow: function (input, inst)
                    {
                        inst.dpDiv.css({marginTop: (input.offsetHeight) - 20 + 'px', marginLeft: (input.offsetWidth) - 90 + 'px'});
                    }
                }
        );
        function formatResult(row) {
            return row[0];
        }
        function formatItem2(row) {
            r = "<table width='400'>";
            r = r + "<td width='20'>" + row[0] + "</td>";
            r = r + "<td width='250'>" + row[1] + "</td>";
            r = r + "<td width='220'>" + row[2] + "</td>";
            r = r + "<td width='20'>" + row[3] + "</td>";
            r = r + "</tr>"
            r = r + "</table>";
            return r;
        }
        $('#propietario').autocomplete('../../../../autocomplet/clientes.php', {
            autoFill: true,
            width: 900,
            SELECTFirst: true,
            extraParams: {Op: 1, codsuc:<?=$codsuc ?>},
            formatItem: formatItem2,
            formatResult: formatResult,
            //mustMatch : true
            cacheLength: 0 //CACHE
                    //extraParams: {"Al": function() { return $("#IdAlmacenEgreso").val();}, "IdProyecto": function() { return $("#IdProyecto").val();}  }
        }).result(function (event, item) {
            !item ? '' : $("#codcliente").val(item[0])
            !item ? '' : $("#propietario").val(item[1]);
            !item ? '' : datosusuario(item[7])
        });

    });
    
    function cargar_zonas_abastecimiento() {
    }
    function cargar_rutas_lecturas() {
    }
    function cargar_rutas_distribucion() {
    }
    function cargar_rutadistribucion() {
    }

    function ValidarForm(Op)
    {
        if ($("#propietario").val() == "")
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#propietario"), "Seleccione o Ingrese nombre del usuario ")
            return false;
        }
        $("#propietario").removeClass("ui-state-error");
        
        if ($("#codzona").val() == '0')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#codzona"), "Seleccione Zona")
            return false;
        }
        $("#codzona").removeClass("ui-state-error");
        
        if ($("#sector").val() == '0')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#sector"), "Seleccione Sector")
            return false;
        }
        $("#sector").removeClass("ui-state-error");
        
        if ($("#manzanas").val() == '0')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#manzanas"), "Seleccione Manzana")
            return false;
        }
        $("#manzanas").removeClass("ui-state-error");
        
        if ($("#lote").val() == '0' || $("#lote").val() == '')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#lote"), "Digite el lote que no puede ser 0")
            return false;
        }
        $("#lote").removeClass("ui-state-error");
        
        if ($("#calle").val() == '')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#calles"), "Seleccione Calle")
            return false;
        }
        $("#calle").removeClass("ui-state-error");
        
        if ($("#nrocalle").val() == '')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#nrocalle"), "Ingrese numero de Calle")
            return false;
        }
        $("#nrocalle").removeClass("ui-state-error");
        
        if (Trim($("#correo").val()) != '')
        {
            $("#tabs").tabs({selected: 0});

            if (!escorreo($("#correo").val()))
            {
                Msj($("#correo"), "Correo Invalido")
                return false;
            }
        }
        $("#calles").removeClass("ui-state-error");
        
        if ($("#codinspector").val() == '')
        {
            $("#tabs").tabs({selected: 0});
            $("#codinspector").val(0)
            // Msj($("#codinspector"),"Seleccione Inspector")
            //return false;
        }
        
        if ($("#diasatencion").val() == '')
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#diasatencion"), "Ingrese los dias de atencion")
            return false;
        }
        $("#diasatencion").removeClass("ui-state-error");
        
        ///CALCULAR IMPUT DE COLATERALES
        var id = parseInt($("#TbIndex tbody tr").length)
        if (id == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#TbIndex"), "Agregue Colateral")
            return false;

        }
        $("#TbIndex").removeClass("ui-state-error");
        
        var tr = '';
        var CodConcepto, SubTotal, IGV, Redondeo, Colateral = '';
        for (var i = 1; i <= id; i++)
        {
            CodConcepto = $("#TbIndex tbody tr#" + i + " label.CodConcepto").text()
            SubTotal = $("#TbIndex tbody tr#" + i + " label.SubTotal").text()
            IGV = $("#TbIndex tbody tr#" + i + " label.IGV").text()
            Redondeo = $("#TbIndex tbody tr#" + i + " label.Redondeo").text()
            Importe = $("#TbIndex tbody tr#" + i + " label.Importe").text()
            Colateral = $("#TbIndex tbody tr#" + i + " label.Colateral").text()
            SubTotal = str_replace(SubTotal, ',', '');
            IGV = str_replace(IGV, ',', '');
            Redondeo = str_replace(Redondeo, ',', '');
            Importe = str_replace(Importe, ',', '');
            tr += '<input type="hidden" name="itemC' + i + '"  value="' + i + '"/>';
            tr += '<input type="hidden" name="codconceptoC' + i + '"  value="' + CodConcepto + '"/>';
            tr += '<input type="hidden" name="subtotalC' + i + '"  value="' + SubTotal + '"/>';
            tr += '<input type="hidden" name="igvC' + i + '"  value="' + IGV + '"/>';
            tr += '<input type="hidden" name="redondeoC' + i + '"  value="' + Redondeo + '"/>';
            tr += '<input type="hidden" name="imptotalC' + i + '"  value="' + Importe + '"/>';
            tr += '<input type="hidden" name="Colateral' + i + '"  value="' + Colateral + '"/>';
        }
        tr += '<input type="hidden" name="NroItems" id="Items"  value="' + id + '"/>';
        $("#DivSave").html(tr)
        ///CALCULAR IMPUT DE COLATERALES
        GuardarP(Op)
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }
    function buscar_usuarios()
    {
        object = "usuarios"
        AbrirPopupBusqueda("../actualizacion/?Op=5", 1100, 500)
    }
    function BuscarInspector()
    {
        object = "inspector"
        AbrirPopupBusqueda("../../../admin/inspectores/?Op=5", 900, 500)
    }
    function buscar_calle()
    {
        object = "calles"
        var codzona = $("#codzona").val()
        var codsector = $("#sector").val()
        if (codzona == 0)
        {
            alert('Seleccione una Zona');
            return;
        }

        if (codsector == 0)
        {
            alert('Seleccione un Sector');
            return;
        }
        AbrirPopupBusqueda("../../../../buscar/calles.php?codzona=" + codzona + "&codsector=" + codsector + "&codsuc=" + codsuc, 700, 450)
    }
    function Recibir(Id)
    {
        if (object == "usuarios")
        {
            $("#nroinscripcion").val(Id)
            datosusuario(Id)
            return false
        }
        if (object == "inspector")
        {
            $("#codinspector").val(Id)
            datosinspector(Id)
            return false
        }
        if (object == "codconcepto")
        {
            datos_conceptos(Id)
            return false
        }
    }

    function recibir_calles(Id, Des)
    {
        $("#codcalle").val(Id)
        $("#calle").val(Des)
    }

    function datosusuario(id)
    {
        $.ajax({
            url: '../../../../ajax/clientes.php',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: 'nroinscripcion=' + id + '&codsuc=' + codsuc,
            success: function (datos) {
                //var r=datos.split("|")
                $("#propietario").val(datos.propietario)
                $("#codcliente").val(datos.codcliente)
                $("#nroinscripcion").val(datos.nroinscripcion)
                $("#nroinscripcionmostrar").val(datos.nroinscripcion)
                $("#nrocalle").val(datos.nrocalle)
                $("#lote").val(datos.lote)
                $("#codzona").val(datos.codzona)
                $("#sublote").val(datos.sublote)

                $("#codcalle").val(datos.codcalle)
                $("#calle").val(datos.calle)
                cargar_sectores(<?=$codsuc ?>, datos.codzona, datos.codsector, 1)
                cargar_manzana(<?=$codsuc ?>, datos.codsector, datos.codmanzanas, 1)
                //cargar_calles(datos.codsector,<?=$codsuc ?>, datos.codzona, datos.codcalle)

            }
        })
    }
    function datosinspector(id)
    {
        $.ajax({
            url: '../../../../ajax/inspector.php',
            type: 'POST',
            async: true,
            data: 'codinspector=' + id + '&codsuc=' + codsuc,
            success: function (datos) {
                var r = datos.split("|")

                $("#inspector").val(r[1])
            }
        })
    }
    function buscar_conceptos()
    {

        object = "codconcepto";
        AbrirPopupBusqueda('../../../../sigco/facturacion/catalogo/conceptos/?Op=5&Tipo=2', 900, 600);
    }
    function datos_conceptos(id)
    {
        $.ajax({
            url: '../../../../ajax/mostrar/conceptos.php',
            type: 'POST',
            async: true,
            data: 'conceptos=' + id + '&codsuc=' + codsuc,
            dataType: 'json',
            success: function (datos) {
                if (datos.codconcepto != null)
                {
                    $("#diasatencion").val(datos.diasatencion);

                    var igv = 0;

                    $("#CodConcepto").val(datos.codconcepto);
                    $("#Colateral").val(datos.descripcion);
                    $("#subtotal").val(parseFloat(datos.importe).toFixed(2));

                    $("#afecto_igv").val(datos.afecto_igv);

                    vigv = 0;

                    if (datos.afecto_igv == 1)
                    {
                        //alert(datos.importe);
                        vigv = CalculaIgv(datos.importe, '<?=$paramae["valor"] ?>');

                        $("#igv").val(vigv)
                    }

                    $("#igv").val(vigv);

                    var redondeo = CalcularRedondeo((parseFloat(datos.importe) + parseFloat(vigv)))
                    $("#redondeo").val(redondeo.toFixed(2))
                    var importe = parseFloat(datos.importe) + parseFloat(vigv) + parseFloat(redondeo)
                    $("#importe").val(importe.toFixed(2))
                    $("#subtotal").focus().SELECT();
                }
                else
                {
                    Msj($("#CodConcepto"), 'No Existe Colateral')
                }

            }
        })
    }
/////
    function ValidarEnterCre(e, op)
    {
        if (VeriEnter(e))
        {
            switch (op)
            {
                case 1:
                    AgregarItem();
                    break;
                case 2:
                    datos_conceptos($("#CodConcepto").val())
                    break;

            }
            e.returnValue = false;
        }
    }
    function AgregarItem()
    {
        var CodConcepto = $("#CodConcepto").val();
        var Colateral = $("#Colateral").val();
        var subtotal = $("#subtotal").val();
        var igv = $("#igv").val();
        var redondeo = $("#redondeo").val();
        var importe = $("#importe").val();


        var j = parseInt($("#TbIndex tbody tr").length)

        for (var i1 = 1; i1 <= j; i1++)
        {
            Codigo = $("#TbIndex tbody tr#" + i1 + " label.CodConcepto").text();

            if (Codigo == CodConcepto)
            {
                Msj('#Colateral', 'El Colateral ' + Colateral + ' ya esta Agregado')
                return false;
            }
        }

        if (CodConcepto != "" && CodConcepto != "")
        {
            //if (subtotal == "" || parseFloat(subtotal) == 0)
//            {
//                Msj("#subtotal", 'Digite un Precio v&aacute;lido')
//                return false;
//            }
            $("#subtotal").removeClass("ui-state-error");

            //subtotal = number_format(subtotal, 2)
//						var redondeo_manual = parseFloat(CalcularRedondeo(number_format(subtotal,2)));
//						subtotal = parseFloat(subtotal) + redondeo_manual
//
//						igv = number_format(igv, 2)
//						redondeo = number_format(redondeo, 2)
//						importe = number_format(importe, 2)
//						importe = parseFloat(importe) + parseFloat(redondeo_manual)

            var i = j + 1;
            var tr = '<tr id="' + i + '" onclick="SeleccionaId(this);">';
            tr += '<td align="center" ><label class="Item">' + i + '</label></td>';
            tr += '<td align="left"><label class="CodConcepto">' + CodConcepto + '</label>';
            tr += '<label class="Colateral">' + Colateral + '</label></td>';
            tr += '<td align="right"><label class="SubTotal">' + subtotal + '</label></td>';
            tr += '<td align="right"><label class="IGV">' + igv + '</label></td>';
            tr += '<td align="right"><label class="Redondeo">' + redondeo + '</label></td>';
            tr += '<td align="right"><label class="Importe">' + importe + '</label></td>';
            tr += '<td align="center" ><a href="javascript:QuitaItemc(' + i + ')" class="Del" >';
            tr += '<span class="icono-icon-trash" title="Quitar Colateral"></span> ';
            tr += '</a></td></tr>';
            //alert(Precio)

            $("#TbIndex tbody").append(tr);
            $("#CodConcepto").val("");
            $("#Colateral").val("");
            $("#subtotal").val("0.00");
            $("#igv").val('0.00')
            $("#redondeo").val('0.00');
            $("#importe").val('0.00');
            $("#CodConcepto").focus();

            CalcularTotal()

        }
        else
        {
            if (Trim(CodConcepto) == "")
            {
                Msj('#CodConcepto', 'Seleccione un Colateral')
                return false;
            }
            else
            {
                Msj('#subtotal', 'Digite una  Precio v&aacute;lido')
                return false;
            }

        }

    }
    function CalcularTotal()
    {

        var j = parseInt($("#TbIndex tbody tr").length);
        var importetotal = 0;
        var subtotaltotal = 0;
        var igvtotal = 0;
        var redondeototal = 0;
        var t = 0;
        for (var i = 1; i <= j; i++)
        {
            t = $("#TbIndex tbody tr#" + i + " label.SubTotal").text()
            t = str_replace(t, ',', '');
            subtotaltotal += parseFloat(t);
            t = $("#TbIndex tbody tr#" + i + " label.IGV").text()
            t = str_replace(t, ',', '');
            igvtotal += parseFloat(t);
            t = $("#TbIndex tbody tr#" + i + " label.Redondeo").text()
            t = str_replace(t, ',', '');
            redondeototal += parseFloat(t);
            t = $("#TbIndex tbody tr#" + i + " label.Importe").text()
            t = str_replace(t, ',', '');
            importetotal += parseFloat(t);



        }

        $('#subtotaltotal').val(number_format(subtotaltotal, 2));
        $('#igvtotal').val(number_format(igvtotal, 2));
        $('#redondeototal').val(number_format(redondeototal, 2));
        $('#importetotal').val(number_format(importetotal, 2));
        $("#TotalCalculo").html(number_format(importetotal, 2))
    }
    function QuitaItemc(tr)
    {
        var id = parseInt($("#TbIndex tbody tr").length);


        $("#TbIndex tbody tr#" + tr).remove();
        var nextfila = tr + 1;
        var j;
        var IdMat = "";
        for (var i = nextfila; i <= id; i++)
        {
            j = i - 1;//alert(j);
            $("#TbIndex tbody tr#" + i + " label.Item").text(j);
            $("#TbIndex tbody tr#" + i + " a.Del").attr("href", "javascript:QuitaItemc(" + j + ")");
            $("#TbIndex tbody tr#" + i).attr("id", j);

        }
        CalcularTotal()

    }
    function CalcularImporte()
    {
        vigv = 0;

        if ($("#afecto_igv").val() == 1)
        {
            vigv = CalculaIgv($("#subtotal").val(), $("#IgvP").val());

            $("#igv").val(vigv);
        }

        redondeo = CalcularRedondeo(parseFloat($("#subtotal").val()) + parseFloat(vigv));
        //alert(redondeo);

        importe = parseFloat($("#subtotal").val()) + parseFloat(vigv) + parseFloat(redondeo);

        if (isNaN(importe))
            importe = 0;

        $("#redondeo").val(redondeo.toFixed(2));
        $("#importe").val(importe.toFixed(2));
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos de la Solicitud</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Colaterales</a></li>

                            </ul>
                            <div id="tabs-1" style="height:420px;">

                                <fieldset class="fieldset">
                                    <legend class="legend ui-state-default ui-corner-all">Datos del Usuario</legend>

                                    <table width="800" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="TitDetalle"> </td>
                                            <td width="10" align="center" class="CampoDetalle"></td>
                                          <td class="CampoDetalle" align="right">Fecha de Emision&nbsp;:&nbsp;<input name="fechaemision" type="text" id="fechaemision" maxlength="10" value="<?=isset($row["fechareg"]) ? $objDrop->DecFecha($row["fechareg"]) : date("d/m/Y") ?>" class="inputtext" style="width:85px;" /></td>
                                        </tr>
                                        <tr>
                                            <td class="TitDetalle" align="right">Id</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <input name="nroinspeccion" type="text" id="Id" size="4" maxlength="20" readonly="readonly" value="<? echo $Id;?>" class="inputtext"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TitDetalle" align="right">Nro Inscripci&oacute;n</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <input type="text" value="<?=$nroinscripcion ?>" size="15" readonly="readonly" class="inputtext" id="nroinscripcionmostrar"/>
                                                <input type="hidden" name="nroinscripcion" id="nroinscripcion" value="<?=$row['nroinscripcion'] ?>"/>
                                                <input type="hidden" id="IgvP" value="<?=$paramae["valor"] ?>" />
                                                <div id="DivSave"></div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="TitDetalle" align="right">Cliente</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <input type="text" name="codcliente" id="codcliente" size="15" class="inputtext" readonly="readonly" value="<?=$row["codcliente"] ?>"/>
                                                <span class="MljSoft-icon-buscar" title="Buscar Cliente" onclick="buscar_usuarios();"></span>

                                                <input type="hidden" name="nuevocliente" id="nuevocliente" value="<?=$row["nuevocliente"] ?>">
                                                <input name="propietario" type="text" id="propietario" value="<?=$row["propietario"] ?>" size="60" maxlength="80" class="inputtext" placeholder="Ingresar datos de cliente nuevo" />

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TitDetalle" align="right">Zona</td>
                                            <td class="TitDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <?php
                                                echo $objDrop->drop_zonas($codsuc, $row["codzona"], "onchange='cargar_sectores(".$codsuc.", this.value, 0, 0)';");
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TitDetalle" align="right">Sector</td>
                                            <td class="TitDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <div id="div_sector">
                                                    <SELECT name="sector" id="sector" style="width:220px" class="SELECT">
                                                        <option value="0">--Seleccione el Sector--</option>
                                                    </SELECT>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">Manzana</td>
                                            <td align="center">:</td>
                                            <td>
                                                <div id="div_manzanas">
                                                    <SELECT name="manzanas" id="manzanas" style="width:220px" class="SELECT">
                                                        <option value="0">--Seleccione la Manzana--</option>
                                                    </SELECT>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="CampoDetalle" align="right">Lote</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <input type="text" name="lote" id="lote" class="inputtext entero" size="10" value="<?=$row["lote"] ?>" placeholder="Lote" onkeypress="CambiarFoco(event, 'area')" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="CampoDetalle" align="right">Sub Lote</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <input type="text" name="sublote" id="sublote" class="inputtext" size="10" value="<?=$row["sublote"] ? $row["sublote"] : 1 ?>" placeholder="Sub Lote" onkeypress="CambiarFoco(event, 'correo')" />
                                            </td>
                                        </tr>                                    
                                        <tr>
                                            <td class="TitDetalle" align="right">Calle</td>
                                            <td align="center">:</td>
                                            <td align="left">
                                                <input type="hidden" name="calles" id="codcalle" value="<?=$row["codcalle"] ?>" />
                                                <input type="text" name="calle" id="calle" size="50" class="inputtext" value="<?=$calle; ?>" placeholder="Calle" readonly/>
                                                <span class="MljSoft-icon-buscar" title="Buscar Calle" onclick="buscar_calle();"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="117" class="CampoDetalle" align="right">Nro. calle</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <input type="text" name="nrocalle" id="nrocalle" class="inputtext" size="10" value="<?=$row["nrocalle"] ?>" placeholder="Nro. Calle" onkeypress="CambiarFoco(event, 'nrodocumento');" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="CampoDetalle" align="right">Correo</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <input type="text" class="inputtext"  id="correo" name="correo" size="80" value="<?=$row["correo"] ?>" placeholder="Correo" />
                                            </td>
                                        </tr>
                                        <tr style="display:none">
                                            <td class="TitDetalle" align="right">Inspector</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle"><input name="codinspector" type="text" id="codinspector" size="15" maxlength="10" value="<?=$row["codinspector"] ?>" class="inputtext"/>
                                                <span class="MljSoft-icon-buscar" onclick="BuscarInspector();"> </span>
                                                <input name="inspector" type="text" id="inspector" size="60" maxlength="80" readonly="readonly" value="<?=$row["nombres"] ?>" class="inputtext"/></td>
                                        </tr>                                   
                                        <tr>
                                            <td class="TitDetalle" align="right">Tiempo Atenci&oacute;n</td>
                                            <td class="CampoDetalle" align="center">:</td>
                                            <td class="CampoDetalle">
                                                <input class="inputtext" name="diasatencion" type="text" id="diasatencion" maxlength="25"  value="<?=$row["diasatencion"] ?>" onkeypress="return permite(event, 'num');" style="width:80px;"/>D&iacute;as
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="TitDetalle" valign="top" align="right">Glosa</td>
                                            <td class="CampoDetalle" align="center" valign="top">:</td>
                                            <td class="CampoDetalle">
                                                <textarea name="glosa" id="glosa" rows="3" class="inputtext" style="font-size:11px; width:400px"><?=$row["glosa"] ?></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                            <div id="tabs-2"  style="height:420px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr style="height:5px">
                                        <td colspan="6" class="TitDetalle">
                                            <fieldset class="fieldset">
                                                <legend class="legend ui-state-default ui-corner-all">Detalle de Colaterales</legend>
                                                <table width="100%">
                                                    <tr class="TrAddItem">
                                                        <td colspan="2"  >
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" rules="all" >
                                                                <tr>
                                                                    <td align="center">
                                                                        <input type="text" class="entero text inputtext ui-corner-all" title="C&oacute;digo del Colateral " id="CodConcepto" style="width:80px"  onkeypress="ValidarEnterCre(event, 2);"/>
                                                                        <span class="MljSoft-icon-buscar" title="Buscar Colateral" onclick="buscar_conceptos();"></span>
                                                                        <input type="text" class="text inputtext ui-corner-all" style="width:350px" title="Descripci&oacute;n del Colateral" id="Colateral"  />
                                                                        <input type="text" id="subtotal" title="Sub Total" class="numeric inputtext ui-corner-all text" style="width:80px" onkeypress="ValidarEnterCre(event, 1);" value="0.00" onkeyup="CalcularImporte();"/>
                                                                        <input type="text" id="igv"  class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                        <input type="text" id="redondeo" class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                        <input type="text" id="importe" class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                        <span class="icono-icon-add" title="Agregar Colateral" onclick="AgregarItem()"></span>
                                                                        <input type="hidden" name="afecto_igv" id="afecto_igv" value="0" />

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" >
                                                            <div style="height:auto; overflow:auto" align="center" id="DivDetalle">
                                                                <table width="800" border="0" align="center" cellspacing="0" class="ui-widget" id="TbIndex">
                                                                    <thead class="ui-widget-header" >
                                                                        <tr >
                                                                            <th width="43" align="center" scope="col">Item</th>
                                                                            <th align="center" scope="col">Colateral</th>
                                                                            <th width="80" align="center" scope="col">Subtotal</th>
                                                                            <th width="80" align="center" scope="col">IGV</th>
                                                                            <th width="80" align="center" scope="col">Redondeo</th>
                                                                            <th width="80" align="center" scope="col">Importe</th>
                                                                            <th width="5" align="center" scope="col">&nbsp;</th>
                                                                        </tr>

                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        $tr = '';
                                                                        if ($Id != '') {
                                                                            $Sql = "SELECT d.codconcepto,  c.descripcion,  d.item,  d.subtotal,  d.igv,
                                                                                    d.redondeo,  d.imptotal
                                                                                FROM  catastro.detsolicitudinspeccion d
                                                                                INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc)  AND (d.codconcepto = c.codconcepto)
                                                                                WHERE d.codemp=? AND d.codsuc=? AND  d.nroinspeccion=?
                                                                                ORDER BY  d.item";
                                                                            $consulta = $conexion->prepare($Sql);
                                                                            $consulta->execute(array(1, $codsuc, $Id));
                                                                            $itemsD = $consulta->fetchAll();
                                                                            $subtotaltotal = 0;
                                                                            $igvtotal = 0;
                                                                            $redondeototal = 0;
                                                                            $importetotal = 0;
                                                                            foreach ($itemsD as $rowD) {
                                                                                $subtotaltotal +=$rowD['subtotal'];
                                                                                $igvtotal +=$rowD['igv'];
                                                                                $redondeototal +=$rowD['redondeo'];
                                                                                $importetotal +=$rowD['imptotal'];

                                                                                $tr.='<tr id="'.$rowD['item'].'" onclick="SeleccionaId(this);">';
                                                                                $tr.='<td align="center" ><label class="Item">'.$rowD['item'].'</label></td>';
                                                                                $tr.='<td align="left"><label class="CodConcepto">'.$rowD['codconcepto'].'</label>';
                                                                                $tr.='<label class="Colateral">'.$rowD['descripcion'].'</label></td>';
                                                                                $tr.='<td align="right"><label class="SubTotal">'.number_format($rowD['subtotal'], 2).'</label></td>';
                                                                                $tr.='<td align="right"><label class="IGV">'.number_format($rowD['igv'], 2).'</label></td>';
                                                                                $tr.='<td align="right"><label class="Redondeo">'.number_format($rowD['redondeo'], 2).'</label></td>';
                                                                                $tr.='<td align="right"><label class="Importe">'.number_format($rowD['imptotal'], 2).'</label></td>';
                                                                                $tr.='<td align="center" ><a href="javascript:QuitaItemc('.$rowD['item'].')" class="Del" >';
                                                                                $tr.='<span class="icono-icon-trash" title="Quitar Colateral"></span> ';
                                                                                $tr.='</a></td></tr>';
                                                                            }
                                                                        }
                                                                        echo $tr;
                                                                        ?>
                                                                    </tbody>
                                                                    <tfoot class="ui-widget-header">

                                                                        <tr>
                                                                            <td colspan="2" align="right" >Total :</td>
                                                                            <td align="right" >
                                                                                <input type="text" id="subtotaltotal" name="subtotal" value="<?=number_format($subtotaltotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                            </td>
                                                                            <td align="right" >
                                                                                <input type="text" id="igvtotal" name="igv" value="<?=number_format($igvtotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                            </td>
                                                                            <td align="right" >
                                                                                <input type="text" id="redondeototal" name="redondeo" value="<?=number_format($redondeototal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                            </td>
                                                                            <td align="right" >
                                                                                <input type="text" id="importetotal" name="imptotal" value="<?=number_format($importetotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                            </td>
                                                                            <td >&nbsp;</td>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td></tr>


        </table>
    </form>
</div>
