<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsDrop.php");

	$objDrop = new clsDrop();
	
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc 	= $_SESSION['IdSucursal'];
	$guardar	= "op=".$Op;
        
	if($Id!='')
	{
		$sql  = "select s.nroinspeccion,s.propietario,s.nroinscripcion,s.codinspector,i.nombres,s.fechareg,s.glosa,s.diasatencion,
				co.descripcion as concepto,s.codconcepto,
				s.codzona,s.codsector,s.codmanzanas,s.codcalle,s.nrocalle,s.lote,s.sublote,s.codcliente,s.nroinscripcion
				 from catastro.solicitudinspeccion as s
				 inner join reglasnegocio.inspectores as i on (s.codinspector=i.codinspector) AND (s.codsuc=i.codsuc)
				 inner join facturacion.conceptos as co on(s.codsuc=co.codsuc AND s.codconcepto=co.codconcepto)
				 where s.nroinspeccion=?";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($Id));
		$row = $consulta->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;
                
	}	

?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var object="";
	var codsuc=<?=$codsuc?>

	function ValidarForm(Op)
	{
            if($("#nroinscripcion").val() == "")
            {
                alert("Ingrese el Codigo del Usuario")
                $("#nroinscripcion").focus()
                return false;
            }
            if($("#codinspector").val() == '')
            {
                alert("Ingrese el Codigo del Inspector")
                return false;
            }
           GuardarP(Op);
	}
	function Cancelar()
	{
		location.href='../index.php';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="700" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    <tbody>
	<tr>
	  <td width="132" class="TitDetalle">&nbsp;</td>
	  <td width="26" class="CampoDetalle">&nbsp;</td>
	  <td width="645" class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
      <td class="TitDetalle">Id</td>
      <td class="CampoDetalle" align="center">:</td>
      <td class="CampoDetalle">
		<input name="nroinspeccion" type="text" id="Id" size="4" maxlength="20" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
	  </td>
	</tr>
	<tr>
	    <td class="TitDetalle">Cliente</td>
	    <td class="CampoDetalle" align="center">:</td>
	    <td class="CampoDetalle"><input name="nroinscripcion" type="text" id="nroinscripcion" readonly="readonly" size="15" maxlength="10" class="inputtext" value="<?=$row["nroinscripcion"]?>"/>
	      <input name="propietario" type="text" id="propietario" value="<?=$row["propietario"]?>" size="60" maxlength="80" readonly="readonly" class="inputtext"/></td>
	</tr>
        <tr>
	    <td class="TitDetalle">Inspector</td>
	    <td class="CampoDetalle" align="center">:</td>
	    <td class="CampoDetalle"><input name="codinspector" type="text" id="codinspector" size="15" maxlength="10" value="<?=$row["codinspector"]?>" class="inputtext"/>
	      <input name="inspector" type="text" id="inspector" size="60" maxlength="80" readonly="readonly" value="<?=$row["nombres"]?>" class="inputtext"/></td>
	</tr>
	<tr>
		<td class="TitDetalle">Colateral</td>
		<td class="CampoDetalle" align="center">:</td>
		<td class="CampoDetalle"><label>
		  <input type="text" name="colateral" id="colateral" size="80" maxlength="80" class="inputtext" readonly="readonly" value="<?=$row["concepto"]?>"/>
		  <input type="hidden" name="codconcepto" id="codconcepto" value="<?=$row["codconcepto"]?>" />
		  <input type="hidden" name="afecto_igv" id="afecto_igv" value="0" />
		</label></td>
	  </tr>
	   <tr>
	  <td class="TitDetalle">Tiempo Atenci&oacute;n</td>
	  <td class="CampoDetalle" align="center">:</td>
		<td class="CampoDetalle">
	  	<input class="inputtext" name="diasatencion" type="text" id="diasatencion" size="25" maxlength="25"  value="<?=$row["diasatencion"]?>" onkeypress="return permite(event,'num');" readonly="readonly"/>D&iacute;as
	  </td>
	  </tr>

        <tr>
          <td class="TitDetalle" valign="top">Glosa</td>
          <td class="CampoDetalle" align="center" valign="top">:</td>
          <td class="CampoDetalle"><label>
            <textarea name="glosa" id="glosa" cols="80" rows="5" class="inputtext" style="font-size:11px" readonly="readonly"><?=$row["glosa"]?></textarea>
          </label></td>
        </tr>
	    <tr>
	      <td class="TitDetalle">Fecha Emisi&oacute;n </td>
	      <td class="CampoDetalle" align="center">:</td>
	      <td class="CampoDetalle">
	      	<input name="fechaemision" readonly="readonly" type="text" id="fechaemision" maxlength="10" value="<?=isset($row["fechareg"])?$objDrop->DecFecha($row["fechareg"]):date("d/m/Y")?>" class="inputtext" style="width:80px;"/>
	      	&nbsp;Fecha Atenci&oacute;n &nbsp;:&nbsp;
	      	<input name="fechaatencion" type="text" id="fechaatencion" maxlength="10" value="<?=isset($row["fechaatencion"])?$objDrop->DecFecha($row["fechaatencion"]):date("d/m/Y")?>" class="inputtext fecha" style="width:80px;"/>
	      </td>
        </tr>
       <!--  <tr>
	      <td class="TitDetalle">Fecha Atenci&oacute;n </td>
	      <td class="CampoDetalle" align="center">:</td>
	      <td class="CampoDetalle"><input name="fechaatencion" type="text" id="fechaatencion" size="15" maxlength="10" value="<?=isset($row["fechaatencion"])?$objDrop->DecFecha($row["fechaatencion"]):date("d/m/Y")?>" class="inputtext fecha"/></td>
        </tr> -->
	    <tr>
	      <td colspan="3" class="TitDetalle" style="font-size:12px; font-weight:bold">Se Resuelve </td>
        </tr>
        <tr>
	      <td class="TitDetalle">&nbsp;</td>
	      <td class="CampoDetalle" align="center">&nbsp;</td>
	      <td class="CampoDetalle"><textarea name="resuelve" id="resuelve" cols="80" rows="5" class="inputtext" style="font-size:11px" ></textarea></td>
        </tr>
        <tr>
	  <td width="112" class="TitDetalle">&nbsp;</td>
	  <td width="26" class="CampoDetalle">&nbsp;</td>
	  <td width="655" class="CampoDetalle">&nbsp;</td>
	  </tr>
          </tbody>
	
    </table>
 </form>
</div>