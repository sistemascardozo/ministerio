<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
	
    $TituloVentana = "SOLICITUD DE FACTIBILIDAD";
    $Activo = 1;
    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
    $and = '';
	
    if ($Op < 5)
    {
        CuerpoSuperior($TituloVentana);
    }
    if ($Op == 5)
    {
        $and = " AND ( sol.estado=1 AND sol.cancelado = 0 OR sol.estado = 3) "; //ERA ASI /*sol.estado=1*/ LE QUITE PARA Q EN CAJA SALE SOLO LOS PENDIENTES
    }
    if ($Op == 6)
    {
        $and = " AND ( sol.codconcepto = 10004 OR sol.codconcepto = 10003 OR sol.codconcepto = 1000 OR sol.codconcepto = ".$_GET['IdConcepto']." ) AND sol.estado = 2 ";
    }
    
	$Criterio = 'Solicitud_Inspeccion';
    
	$codsuc = $_SESSION['IdSucursal'];
    
	$FormatoGrilla = array();
    
	$Sql = "SELECT sol.nroinspeccion, sol.propietario,sol.direccion, ".$objFunciones->FormatFecha('sol.fechaemision').", ";
    $Sql .= " CASE WHEN sol.estado=1 THEN 'PROCESO' WHEN sol.estado=2 THEN 'CANCELADO' ELSE 'ATENDIDO' END, ";
    $Sql .= " sol.estado, 1, sol.nuevocliente, upper(insp.nombres), sol.nrosolicitud, sol.nropresupuesto, sol.estadopresupuesto ";
    $Sql .= "FROM catastro.solicitudinspeccion sol ";
    $Sql .= " LEFT JOIN reglasnegocio.inspectores insp ON (sol.codinspector = insp.codinspector) AND (sol.codsuc = insp.codsuc)";
		
    $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]", ' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'sol.nroinspeccion', '2' => 'sol.propietario');          //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;                                                            //Operacion
    $FormatoGrilla[3] = array('T1' => 'Nro. Inspeccion', 'T2' => 'Usuario', 'T3' => 'Direccion',
                            'T4' => 'Fecha Emision', 'T5' => 'Estado');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'left', 'A4' => 'center', 'A5' => 'center');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '110', 'W2' => '200', 'W3' => '200', 'W6' => '119', 'W5' => '60', 'W4' => '60');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = " AND (sol.codemp = 1 AND sol.codsuc = ".$codsuc.") ".$and." ORDER BY sol.nroinspeccion DESC ";                                   //Orden de la Consulta
    
    if ($Op < 5)
    {
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '5', //Número de Botones a agregar
            'BtnId1' => 'BtnModificar', //Nombre del Boton
            'BtnI1' => 'modificar.png', //Imagen a mostrar
            'Btn1' => 'Editar', //Titulo del Botón
            'BtnF1' => 'onclick="Modificar(this);"', //Eventos del Botón
            'BtnCI1' => '6', //Item a Comparar
            'BtnCV1' => '1',
            'BtnId2' => 'BtnRestablecer', //y aparece este boton
            'BtnI2' => 'imprimir.png',
            'Btn2' => 'Imprimir',
            'BtnF2' => 'onclick="Imprimir(this)"',
            'BtnCI2' => '7',
            'BtnCV2' => '1',
            'BtnId3' => 'BtnGenerar', //y aparece este boton
            'BtnI3' => 'documento.png',
            'Btn3' => 'Atender Solicitud',
            'BtnF3' => 'onclick="atendersolicitud(this)"',
            'BtnCI3' => '7',
            'BtnCV3' => '1',
            'BtnId4' => 'BtnSolicitud', //y aparece este boton
            'BtnI4' => 'ok.png',
            'Btn4' => 'Solicitud de Conexion',
            'BtnF4' => 'onclick="Solicitud(this)"',
            'BtnCI4' => '12',
            'BtnCV4' => '1',
            'BtnId5' => 'BtnSolicitud', //y aparece este boton
            'BtnI5' => 'Dinero.png',
            'Btn5' => 'Presupuesto de Conexion',
            'BtnF5' => 'onclick="Presupuesto(this)"',
            'BtnCI5' => '12',
            'BtnCV5' => '1'
			/*,
            'BtnId6' => 'BtnSolicitud', //y aparece este boton
            'BtnI6' => 'documento.png',
            'Btn6' => 'Atender Presupuesto de Conexion',
            'BtnF6' => 'onclick="AtenderPresupuesto(this)"',
            'BtnCI6' => '7',
            'BtnCV6' => '1',*/
        );
    }
    else {
        $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
            'NB' => '1', //Número de Botones a agregar
            'BtnId1' => 'BtnModificar', //Nombre del Boton
            'BtnI1' => 'ok.png', //Imagen a mostrar
            'Btn1' => 'Seleccionar', //Titulo del Botón
            'BtnF1' => 'onclick="Enviar(this);"', //Eventos del Botón
            'BtnCI1' => '7', //Item a Comparar
            'BtnCV1' => '1'    //Valor de comparación
        );
    }

    $FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 1), 
                        array('Name' => 'estado', 'Col' => 6),
                        array('Name' => 'nuevocliente', 'Col' => 8), 
                        array('Name' => 'nrosolicitud', 'Col' => 10),
                        array('Name' => 'nropresupuesto', 'Col' => 11),
                        array('Name' => 'estadopresupuesto', 'Col' => 12)); //DATOS ADICIONALES      
		      
    $FormatoGrilla[11] = 5; //FILAS VISIBLES
    $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
	
    Cabecera('', $FormatoGrilla[7], 900, 600);
    Pie();
?>
<script type="text/javascript">
	var urldir 	= "<?php echo $_SESSION['urldir'];?>";
	
    function AtenderPresupuesto(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        var estado = $(obj).parent().parent().data('estado')
        var nrosolicitud = $(obj).parent().parent().data('nrosolicitud')
        var nropresupuesto = $(obj).parent().parent().data('nropresupuesto')
        var estadopresupuesto = $(obj).parent().parent().data('estadopresupuesto')
        /*if(estado==1)
         {
         alert("La Solicitud de Inspecion, no ha sido cancelada")
         return
         }*/
        if (estadopresupuesto == 0)
        {
            alert("Primero genere la Solicitud de Conexion")
            return
        }

        if (estadopresupuesto == 2)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Atendido')
            return
        }
        if (estadopresupuesto == 3)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Anulado')
            return
        }
        if (estadopresupuesto == 4)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Pagado')
            return
        }
        if (estadopresupuesto == 5)
        {
            alert('No se puede Atender el Presupuesto...Se encuentra Transferido al Catastro de Usuarios')
            return
        }

        var Op = 0
        if (nropresupuesto != '0')
            Op = 1;
        $("#form1").remove();
        var Id = $(obj).parent().parent().data('id')
        $("#Modificar").dialog("open");
        $("#Modificar").dialog({title: 'Atender Presupuesto'});
        $("#DivModificar").html("<center><span class='icono-icon-loading'></span></center>");
        $("#button-maceptar").children('span.ui-button-text').text('Atender Presupuesto')
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function () {
            ValidarForm(Op);
        });

        $.ajax({
            url: '../../../solicitudes/operaciones/presupuesto/atender/index.php?Op=' + Op,
            type: 'POST',
            async: true,
            data: 'IdSolicitud=' + nrosolicitud + '&codsuc=<?=$codsuc?>&Op=' + Op + '&Id=' + nropresupuesto,
            success: function (data) {
                $("#DivModificar").html(data);
            }
        });
    }
    function Presupuesto(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        var estado = $(obj).parent().parent().data('estado')
        var nrosolicitud = $(obj).parent().parent().data('nrosolicitud')
        var nropresupuesto = $(obj).parent().parent().data('nropresupuesto')
        /*if(estado==1)
         {
         alert("La Solicitud de Inspecion, no ha sido cancelada")
         return
         }*/
        if (nrosolicitud == 0)
        {
            alert("Primero genere la Solicitud de Conexion")
            return
        }
        var Op = 0
        if (nropresupuesto != '0')
            Op = 1;
        $("#form1").remove();
        var Id = $(obj).parent().parent().data('id')
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span class='icono-icon-loading'></span></center>");
        $("#button-maceptar").children('span.ui-button-text').text('Generar Presupuesto')
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function () {
            ValidarForm(Op);
        });

        $.ajax({
            url: '../../../solicitudes/operaciones/presupuesto/mantenimiento.php?Op=' + Op,
            type: 'POST',
            async: true,
            data: 'IdSolicitud=' + nrosolicitud + '&codsuc=<?=$codsuc?>&Op=' + Op + '&Id=' + nropresupuesto,
            success: function (data) {
                $("#DivModificar").html(data);
            }
        });
    }
    function Solicitud(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        var estado = $(obj).parent().parent().data('estado')
        var nrosolicitud = $(obj).parent().parent().data('nrosolicitud')
        /*if(estado==1)
         {
         alert("La Solicitud de Inspecion, no ha sido cancelada")
         return
         }*/
        var Op = 0
        if (nrosolicitud != '0')
            Op = 1;
        $("#form1").remove();
        var Id = $(obj).parent().parent().data('id')
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span class='icono-icon-loading'></span></center>");
        $("#button-maceptar").children('span.ui-button-text').text('Generar Solcitud')
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function () {
            ValidarForm(Op);
        });

        $.ajax({
            url: '../../../solicitudes/operaciones/registro/mantenimiento.php?Op=' + Op,
            type: 'POST',
            async: true,
            data: 'IdInspeccion=' + Id + '&codsuc=<?=$codsuc?>&Op=' + Op + '&Id=' + nrosolicitud,
            success: function (data) {
                $("#DivModificar").html(data);
            }
        });
    }
    function GuardarSolicitud()
    {
        $.ajax({
            url: '../../../solicitudes/operaciones/registro/Guardar.php?Op=0',
            type: 'POST',
            async: true,
            data: $('#form1').serialize(),
            success: function (data) {
                //alert(data);
                OperMensaje(data)
                $("#Mensajes").html(data);
                $("#DivModificar").html('');
                $("#Modificar").dialog("close");
                Buscar(0);
            }
        })
    }
    function Modificar(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        var estado = $(obj).parent().parent().data('estado')
        if (estado == 2)
        {
            alert("No se puede Modificar la solicitud por que se encuentra Cancelada")
            return
        }
        if (estado == 3)
        {
            alert("No se puede Modificar la solicitud por que se encuentra atendida")
            return
        }
        $("#form1").remove();
        var Id = $(obj).parent().parent().data('id')
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span class='icono-icon-loading'></span></center>");
        $.ajax({
            url: 'mantenimiento.php?Op=1',
            type: 'POST',
            async: true,
            data: 'Id=' + Id + '&codsuc=<?=$codsuc?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        });
    }
    function Imprimir(obj)
    {
        var id = $(obj).parent().parent().data('id')
        var nuevocliente = $(obj).parent().parent().data('nuevocliente')
        AbrirPopupImpresion('imprimir.php?nrosolicitud=' + id + '&codsuc=<?=$codsuc?>&nuevocliente=' + nuevocliente, 800, 600)
    }
    function atendersolicitud(obj)
    {
        $("#form1").remove();
        var Id = $(obj).parent().parent().data('id')
        var estado = $(obj).parent().parent().data('estado')
        if (estado == 3)
        {
            alert("No se puede Modificar la solicitud por que se encuentra atendida")
            return
        }
        //location.href="atencion/?Id="+id;
        $("#Modificar").dialog({title: 'Atender Solcitud'});
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function () {
            GuardarOk();
        });
        $("#button-maceptar").children('span.ui-button-text').text('Atender Solcitud')
        $.ajax({
            url: 'atencion/index.php',
            type: 'POST',
            async: true,
            data: 'Id=' + Id + '&codsuc=<?=$codsuc?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function GuardarOk()
    {
        $.ajax({
            url: 'atencion/Guardar.php?Op=0',
            type: 'POST',
            async: true,
            data: $('#form1').serialize(),
            success: function (data) {
                //alert(data);
                OperMensaje(data)
                $("#Mensajes").html(data);
                $("#DivModificar").html('');
                $("#Modificar").dialog("close");
                Buscar(0);
            }
        })
    }
    function Enviar(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        opener.Recibir(Id);
        window.close();
    }
</script>
<?php
if ($Op < 5)
    CuerpoInferior();?>