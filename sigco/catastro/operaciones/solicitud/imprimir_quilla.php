<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include($_SESSION['path']."objetos/clsReporte.php");
	
	class clsSolicitud extends clsReporte
	{
		function cabecera()
		{
			$this->Ln(10);
			$this->SetFont('Arial','B',14);
			$tit1 = "SOLICITUD DE INSPECCION";
			$this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');	
		}
		function Contenido($nrosolicitud,$propietario,$documento,$nrodocumento,$direccion,$glosa,$inspector,
						  $insdocumento,$insnrodocumento,$resuelve)
		{
			$h=4;
			
			$this->SetFont('Arial','',8);
			$this->SetTextColor(0,0,0);
			
			$nrosolicitud	= substr("00000",0,5-strlen($nrosolicitud)).$nrosolicitud;
			
			$this->Cell(30, $h,"Nro. de Solicitud",0,0,'L');
			$this->Cell(5, $h,":",0,0,'L');
			$this->Cell(40, $h,$nrosolicitud,0,1,'L');
			
			$this->Cell(30, $h,"Documento",0,0,'L');
			$this->Cell(5, $h,":",0,0,'L');
			$this->Cell(40, $h,$documento." - ".$nrodocumento,0,1,'L');
			
			$this->Cell(30, $h,"Usuario",0,0,'L');
			$this->Cell(5, $h,":",0,0,'L');
			$this->Cell(40, $h,utf8_decode($propietario),0,1,'L');
			
			$this->Cell(30, $h,"Direccion",0,0,'L');
			$this->Cell(5, $h,":",0,0,'L');
			$this->Cell(40, $h,$direccion,0,1,'L');
			
			$this->Ln(2);
			$this->SetFont('Arial','B',10);
			$this->Cell(30, $h,"Motivo de la Solicitud",0,1,'L');
			
			$this->Ln(2);
			$this->SetFont('Arial','',8);
			$this->Rect($this->GetX(),$this->GetY(),190,50);
			
			$this->MultiCell(190,5,strtoupper($glosa),0,'L');
			
			$this->Ln(50);
			$this->SetFont('Arial','B',10);
			$this->Cell(30, $h,"Se Resuelve",0,1,'L');
			
			$this->Ln(2);
			$this->Rect($this->GetX(),$this->GetY(),190,50);
			$this->SetFont('Arial','',8);
			$this->MultiCell(190,5,strtoupper($resuelve),0,'L');
			$this->Ln(100);
			
			$this->SetFont('Arial','',8);
			$this->Cell(10,0,"",0,0,'L');
			$this->Cell(50,0,"",'T',0,'L');
			$this->Cell(60,0,"",0,0,'L');
			$this->Cell(50,0,"",'T',1,'L');
			$this->Cell(10,$h,"",0,0,'L');
			$this->Cell(50, $h,utf8_decode($propietario),0,0,'C');
			$this->Cell(60,0,"",0,0,'L');
			$this->Cell(50, $h,$inspector,0,1,'C');
			$this->Cell(10,$h,"",0,0,'L');
			$this->Cell(50, $h,$documento." - ".$nrodocumento,0,0,'C');
			$this->Cell(60,0,"",0,0,'L');
			$this->Cell(50, $h,$insdocumento." - ".$insnrodocumento,0,1,'C');

		}
    }
	
	$nrosolicitud 	= $_GET["nrosolicitud"];
	$codsuc			= $_GET["codsuc"]; 
        
	$objReporte	= new clsSolicitud();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
		
	$sqlcabecera  = "select sol.nroinspeccion,upper(clie.propietario),upper(tipdoc.abreviado),clie.nrodocumento,
					upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle ),sol.glosa,
					upper(insp.nombres),upper(tdoc.abreviado),insp.nrodocumento,sol.resuelve
					from catastro.solicitudinspeccion as sol 
					inner join catastro.clientes as clie on(sol.codemp=clie.codemp and sol.codsuc=clie.codsuc and 
					sol.nroinscripcion=clie.nroinscripcion)
					inner join public.tipodocumento as tipdoc on(clie.codtipodocumento=tipdoc.codtipodocumento)
					inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
					inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
					inner join reglasnegocio.inspectores as insp on (sol.codinspector=insp.codinspector) AND (sol.codsuc=insp.codsuc)
					inner join public.tipodocumento as tdoc on(insp.codtipodocumento=tdoc.codtipodocumento)
					where sol.nroinspeccion=?";
	
	$consulta = $conexion->prepare($sqlcabecera);
	$consulta->execute(array($nrosolicitud));
	$rowcabecera = $consulta->fetch();

	$objReporte->Contenido($rowcabecera[0],$rowcabecera[1],$rowcabecera[2],$rowcabecera[3],$rowcabecera[4],$rowcabecera[5]
					,$rowcabecera[6],$rowcabecera[7],$rowcabecera[8],$rowcabecera['resuelve']);
	$objReporte->Output();	
	
?>