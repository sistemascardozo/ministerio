<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}
/*
  error_reporting(E_ALL);
  ini_set("display_errors", 1);
 */
include('../../../../objetos/clsFunciones.php');

$conexion->beginTransaction();

$Op = $_GET["Op"];

$objFunciones = new clsFunciones();

$nroinspeccion = $_POST["nroinspeccion"];
$nroinscripcion = $_POST["nroinscripcion"];
$codsuc = $_SESSION['IdSucursal'];
$codinspector = $_POST["codinspector"];
$glosa = $_POST["glosa"];
$fechaemision = $objFunciones->CodFecha($_POST["fechaemision"]);
$idusuario = $_SESSION['id_user'];
$codconcepto = $_POST["codconceptoC1"];
$diasatencion = $_POST["diasatencion"];
$horareg = $objFunciones->HoraServidor();
$codzona = $_POST["codzona"];
$codsector = $_POST["sector"];
$codmanzana = $_POST["manzanas"];
$codcalle = $_POST["calles"];
$nrocalle = $_POST["nrocalle"];
$lote = $_POST["lote"];
$sublote = $_POST["sublote"];
$codcliente = $_POST["codcliente"];
$propietario = $_POST["propietario"];
$correo = $_POST["correo"];
$codemp = 1;
if ($diasatencion == '')
    $diasatencion = 0;
//DIRECCION
$Sql = "SELECT tipcal.descripcioncorta || ' ' || cal.descripcion
        FROM  public.calles as cal 
        INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) 
        WHERE cal.codemp=1 and cal.codsuc= '" . $codsuc . "' and cal.codcalle= '" . $codcalle . "' AND cal.codzona= '" . $codzona . "' ";
$result = $conexion->prepare($Sql);
$result->execute(array(
        /* ":codemp" => $codemp,
          " '".$codsuc."'" => $codsuc,
          " '".$codcalle."'" => $codcalle,
          " '".$codzona."'" => $codzona */
));
$row = $result->fetch();
$direccion = $row[0] . ' #' . $nrocalle;
if ($Op == 0) {
    $nuevocliente = 1;
    //$id  = $objFunciones->setCorrelativos("solicitudinspeccion",$codsuc,"0");
    //$nroinspeccion 	= $id[0];
    $nroinspeccion = $objFunciones->setCorrelativosVarios(21, $codsuc, "SELECT", 0);
    //echo $nroinspeccion." ola";
    if (empty($codcliente))
	{
		$codcliente = $objFunciones->setCorrelativosVarios(1, $codsuc, "SELECT", 0);
		//die($codcliente);
	}
	else
	{
		
	}
	if (empty($nroinscripcion))
	{
		$nroinscripcion = $objFunciones->setCorrelativosVarios(10, $codsuc, "SELECT", 0);
	}
	else
	{
		$nuevocliente = 0;
	}
	
    //echo $nuevocliente;
    $sql = "INSERT INTO catastro.solicitudinspeccion (nroinspeccion,codemp,codsuc,nroinscripcion,fechaemision,glosa,creador, ";
    $sql .= "codinspector,codconcepto,diasatencion,horareg,codzona,codsector,codmanzanas,lote,sublote,codcliente,propietario, ";
    $sql .= "codcalle,nrocalle,nuevocliente,correo,direccion) VALUES('" . $nroinspeccion . "', 1, '" . $codsuc . "', ";
    $sql .= " '" . $nroinscripcion . "', '" . $fechaemision . "', '" . $glosa . "', '" . $idusuario . "', '" . $codinspector . "', $codconcepto, ";
    $sql .= " '" . $diasatencion . "', '" . $horareg . "', '" . $codzona . "', '" . $codsector . "', '" . $codmanzana . "', '" . $lote . "', ";
    $sql .= " '" . $sublote . "', '" . $codcliente . "', '" . $propietario . "', '" . $codcalle . "', '" . $nrocalle . "', '" . $nuevocliente . "', '" . $correo . "', '" . $direccion . "')";
}
if ($Op == 1) {
    $nuevocliente = $_POST["nuevocliente"];
    $sql = "UPDATE catastro.solicitudinspeccion set nroinscripcion= '" . $nroinscripcion . "',fechaemision= '" . $fechaemision . "',glosa= '" . $glosa . "',codinspector= '" . $codinspector . "', ";
    $sql .= "codconcepto= $codconcepto, creador= '" . $idusuario . "',diasatencion= '" . $diasatencion . "',horareg= '" . $horareg . "',codzona= '" . $codzona . "',codsector= '" . $codsector . "', ";
    $sql .= "codmanzanas= '" . $codmanzana . "',lote= '" . $lote . "',sublote= '" . $sublote . "',codcliente= '" . $codcliente . "',propietario= '" . $propietario . "',codcalle= '" . $codcalle . "', ";
    $sql .= " nrocalle= '" . $nrocalle . "',nuevocliente= '" . $nuevocliente . "',correo= '" . $correo . "',direccion= '" . $direccion . "' ";
    $sql .= "WHERE codemp=1 AND codsuc= '" . $codsuc . "' AND nroinspeccion='" . $nroinspeccion . "'";
}

$result = $conexion->prepare($sql);
$result->execute(array());
//
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    $data['res'] = 2;
    $data['mensaje'] = $mensaje . " ==>" . $sql;
    die(json_encode($data));
}
//INSERTAR DETALLE DE COLATERALES
$Sql = "DELETE FROM catastro.detsolicitudinspeccion 
        WHERE codemp=1 AND codsuc= '" . $codsuc . "' AND nroinspeccion=$nroinspeccion;";
$result = $conexion->prepare($Sql);
$result->execute(array(
        /* "'".$nroinspeccion."'" => $nroinspeccion,
          ":codemp" => 1,
          " '".$codsuc."'" => $codsuc */
));
//
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    $data['res'] = 2;
    $data['mensaje'] = $mensaje . " ==>" . $Sql;
    die(json_encode($data));
}

$NroItems = $_POST['NroItems'];

$Item = 0;
$Igv = 0;
$Red = 0;

//echo $_POST["itemC1"];
//print_r($_POST);
for ($i = 1; $i <= $NroItems; $i++) {

    //if (isset($_POST["itemC".$i])) {

    $codconcepto = $_POST['codconceptoC' . $i];
    $item = $_POST['itemC' . $i];
    $subtotal = str_replace(",", "", $_POST['subtotalC' . $i]);
    $imptotal = str_replace(",", "", $_POST['subtotalC' . $i]);

    $sqlD = "INSERT INTO catastro.detsolicitudinspeccion 
                (  codemp,  codsuc,  nroinspeccion,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal) 
                VALUES (  $codemp, $codsuc, $nroinspeccion, $codconcepto, '" . $item . "', '" . $subtotal . "',  0,  0, '" . $imptotal . "');";

    $resultD = $conexion->prepare($sqlD);
    $resultD->execute(array());
    if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        $data['res'] = 'Error detalle solicitudinspeccion';
        die(json_encode($data));
    }

    $Item = $_POST["itemC" . $i];
    $Igv += $_POST["igvC" . $i];
    $Red += $_POST["redondeoC" . $i];
    //}
}

$Item += 1;

//INSERTAR IGV
if ($Igv > 0) {

    $subtotal = str_replace(",", "", $Igv);
    $sqlD = "INSERT INTO catastro.detsolicitudinspeccion 
                (  codemp,  codsuc,  nroinspeccion,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal) 
                VALUES ( $codemp, $codsuc, $nroinspeccion, 5, '" . $Item . "', '" . $subtotal . "',  0, 0, '" . $subtotal . "');";

    $resultD = $conexion->prepare($sqlD);
    $resultD->execute(array());
    if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $data['res'] = 'Error IGV';
        die(json_encode($data));
    }
}

$Item += 1;

//INSERTAR REDONDEO
$Red = $_POST['redondeo'];
if ($Red <> 0) {
    $CodConcep = 7;

    if ($Red > 0) {
        $CodConcep = 8;
    }

    $redondeo = str_replace(",", "", $Red);

    $sqlD = "INSERT INTO catastro.detsolicitudinspeccion 
                (  codemp,  codsuc,  nroinspeccion,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal) 
            VALUES ( $codemp, $codsuc, $nroinspeccion, $CodConcep, '" . $Item . "', '" . $redondeo . "', 0, 0, '" . $redondeo . "');";

    $resultD = $conexion->prepare($sqlD);
    $resultD->execute(array());
    if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $data['res'] = 'Error REDONDEO';
        die(json_encode($data));
    }
}
//INSERTAR DETALLE DE COLATERALES

$conexion->commit();

$mensaje = "El Registro se ha Grabado Correctamente";

if (empty($_POST["codcliente"]))
{
    $n = $objFunciones->setCorrelativosVarios(1, $codsuc, "UPDATE", $codcliente);
}
if (empty($_POST["nroinscripcion"]))
{
	$n = $objFunciones->setCorrelativosVarios(10, $codsuc, "UPDATE", $nroinscripcion);
}
if ($Op == 0)
{
    $objFunciones->setCorrelativosVarios(21, $codsuc, "UPDATE", $nroinspeccion);
}

echo $res = 1;
?>
