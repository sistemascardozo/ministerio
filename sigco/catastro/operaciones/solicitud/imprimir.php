<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include($_SESSION['path']."objetos/clsReporte.php");

    class clsSolicitud extends clsReporte {

        function Header() {
            global $codsuc, $nrosolicitud, $fechaemision, $horareg, $Sede;

            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', 'B', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = $empresa["razonsocial"];
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SEDE: ".$empresa["descripcion"];

            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."images/logo_empresa.jpg", 8, 2, 12, 11);
            $this->SetXY($x, $y);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 14);
            $tit1 = "SOLICITUD DE SERVICIOS";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetXY(150, 5);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(50, 8, utf8_decode("N° Solicitud:"), 1, 0, 'L');
            $this->SetXY(175, 5);
            $this->SetFont('Arial', '', 10);
            $this->Cell(25, 8, str_pad($nrosolicitud, 8, "0", STR_PAD_LEFT), 0, 1, 'R');
            $this->SetX(150);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(50, 8, utf8_decode("Fecha :"), 1, 0, 'L');
            $this->SetX(175);
            $this->SetFont('Arial', '', 10);
            $this->Cell(25, 8, $this->DecFecha($fechaemision)." ".$horareg, 0, 1, 'R');

            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
        }

        function Contenido($nrosolicitud, $propietario, $documento, $nrodocumento, $direccion, $glosa, $inspector, 
                $insdocumento, $insnrodocumento, $resuelve, $codconcepto, $concepto, $nrocalle, $zona, $codcatastro, 
                $nroinscripcion, $nromed, $ruc, $diasatencion, $codantiguo)
		{
            global $codsuc, $fechaemision, $horareg, $conexion, $Sede;
			
            $empresa = $this->datos_empresa($codsuc);
			
            //OBTENEMOS EL CONCEPTO PRINCIPAL
			
            $Sql = "SELECT d.codconcepto, c.descripcion
                FROM  catastro.detsolicitudinspeccion d
                INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto)
                WHERE d.codemp=1  AND d.codsuc=".$codsuc." AND d.nroinspeccion=".$nrosolicitud." 
                ORDER BY d.item LIMIT 1";
			
            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array());
            $row = $consulta->fetch();
			
            $codconcepto = $row[0];
            $concepto = $row[1];
            //OBTENEMOS EL CONCEPTO PRINCIPAL
            $h = 8;
            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $this->Rect($this->GetX(), $this->GetY(), 50, $h);
            $this->SetFont('Arial', '', 6);
            $this->Cell(15, $h, utf8_decode('Código :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(35, $h, $codconcepto, 0, 0, 'C');
            $this->SetFont('Arial', '', 6);
            $this->Cell(25, $h, utf8_decode('Descripción :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(115, $h, $concepto, 0, 1, 'L');
            $h = 12;
            $this->Rect($this->GetX(), $this->GetY(), 190, $h);

            $h = 6;
            $this->SetFont('Arial', '', 6);
            $this->Cell(15, $h, utf8_decode('Dirección :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(125, $h, utf8_decode($direccion), 0, 0, 'L');
            $this->SetFont('Arial', '', 6);
            $this->Cell(10, $h, utf8_decode('Nro. :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(40, $h, $nrocalle, 0, 1, 'C');

            $this->SetFont('Arial', '', 6);
            $this->Cell(15, $h, utf8_decode('Urb. :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(50, $h, $zona, 0, 0, 'L');
            $this->SetFont('Arial', '', 6);
            $this->Cell(20, $h, utf8_decode('Cod. Catastral :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(65, $h, $codcatastro, 0, 0, 'C');
            $this->SetFont('Arial', '', 6);
            $this->Cell(10, $h, utf8_decode('Teléfono :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, $telefono, 0, 1, 'C');

            $h = 12;
            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $this->Rect($this->GetX(), $this->GetY(), 90, $h);
            $this->Rect($this->GetX(), $this->GetY(), 140, $h);
            $this->SetFont('Arial', '', 6);
            $h = 4;
            $this->Cell(90, $h, utf8_decode('Nombre :'), 0, 0, 'L');
            $this->Cell(50, $h, utf8_decode('N° Inscripción:'), 0, 0, 'L');
            $this->Cell(50, $h, utf8_decode('N° Medidor:'), 0, 1, 'L');
            $this->SetFont('Arial', 'B', 8);
            $h = 8;
            $this->Cell(90, $h, utf8_decode($propietario), 0, 0, 'C');
            $this->Cell(50, $h, $codantiguo, 0, 0, 'C');
            $this->Cell(50, $h, $nromed, 0, 1, 'C');
            $this->Ln(2);
            $h = 2;
            $this->Image($_SESSION['path']."images/tijera.jpeg", $this->GetX() - 5, $this->GetY() - 1, 5, 5);
            for ($i = 5; $i < 100; $i++) {
                $this->Cell(2, $h, ' - ', 0, 0, 'C');
            }
            $this->SetFont('Arial', 'B', 8);
            $this->Ln(12);
            $this->Cell(0, $h, utf8_decode(''), 0, 1, 'R');//RIO
            $this->SetFont('Arial', '', 8);
            $this->Ln(2);
            $h = 12;
            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $h = 8;
            $this->SetFont('Arial', '', 10);
            $this->Cell(150, $h, utf8_decode($empresa["razonsocial"]), 0, 0, 'L');
            $this->Cell(40, $h, '', 0, 1, 'R');
            $this->Ln(8);

            $this->SetFont('Arial', 'B', 14);
            $tit1 = "SOLICITUD DE SERVICIOS";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetX(150);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(50, 8, utf8_decode("N° Solicitud:"), 1, 0, 'L');
            $this->SetX(175);
            $this->SetFont('Arial', '', 10);
            $this->Cell(25, 8, str_pad($nrosolicitud, 8, "0", STR_PAD_LEFT), 0, 1, 'R');
            $this->SetX(150);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(50, 8, utf8_decode("Fecha :"), 1, 0, 'L');
            $this->SetX(175);
            $this->SetFont('Arial', '', 10);
            $this->Cell(25, 8, $this->DecFecha($fechaemision)." ".$horareg, 0, 1, 'R');
            $h = 6;
            $this->Rect($this->GetX(), $this->GetY(), 60, $h);
            $this->SetFont('Arial', '', 6);
            $this->Cell(50, 4, utf8_decode("Servicio Solicitado"), 0, 1, 'L');
            $h = 8;
            $this->Ln(2);
            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $this->Rect($this->GetX(), $this->GetY(), 50, $h);
            $this->SetFont('Arial', '', 6);
            $this->Cell(15, $h, utf8_decode('Código :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(35, $h, $codconcepto, 0, 0, 'C');
            $this->SetFont('Arial', '', 6);
            $this->Cell(25, $h, utf8_decode('Descripción :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(115, $h, $concepto, 0, 1, 'L');
            $h = 6;
            $this->Rect($this->GetX(), $this->GetY(), 60, $h);
            $this->SetFont('Arial', '', 6);
            $this->Cell(50, 4, utf8_decode("Servicio Complementarios"), 0, 1, 'L');
            $h = 30;
            $this->Ln(2);
            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $this->Rect($this->GetX(), $this->GetY(), 50, $h);
            $h = 6;

            $Sql = "SELECT  d.codconcepto,  c.descripcion
                            FROM  catastro.detsolicitudinspeccion d
                            INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc)  AND (d.codconcepto = c.codconcepto)
                            WHERE d.codemp=:codemp  AND d.codsuc=:codsuc AND d.nroinspeccion=:nroinscripcion 
                            ORDER BY d.item ";
            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array(":codemp" => 1, ":codsuc" => $codsuc, ":nroinscripcion" => $nrosolicitud));
            $itemsD = $consulta->fetchAll();
            $c = 0;
            $c2 = 0;
            foreach ($itemsD as $rowD) {
                $c++;
                if ($c > 1) {
                    $c2++;
                    $this->Cell(15, $h, utf8_decode('Código :'), 0, 0, 'L');
                    $this->SetFont('Arial', 'B', 8);
                    $this->Cell(35, $h, $rowD['codconcepto'], 0, 0, 'C');
                    $this->SetFont('Arial', '', 6);
                    $this->Cell(25, $h, utf8_decode('Descripción :'), 0, 0, 'L');
                    $this->SetFont('Arial', 'B', 8);
                    $this->Cell(115, $h, $rowD['descripcion'], 0, 1, 'L');
                    $h = 6;
                    //$this->SetFont('Arial','B',8);
                    //$this->Cell(34,$h,'0',0,1,'R');$this->Cell(34,$h,'0',0,1,'R');$this->Cell(34,$h,'0',0,1,'R');$this->Cell(34,$h,'0',0,1,'R');
                }
            }
            for ($i = 1; $i <= 5 - $c2; $i++) {
                $this->SetFont('Arial', 'B', 8);
                $this->Cell(34, $h, '0', 0, 1, 'R');
            }
            $this->Ln(2);
            $h = 6;
            $this->Rect($this->GetX(), $this->GetY(), 60, $h);
            $this->SetFont('Arial', '', 6);
            $this->Cell(50, 4, utf8_decode("Solicitante"), 0, 1, 'C');
            $h = 12;
            $this->Ln(2);
            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $this->Rect($this->GetX(), $this->GetY(), 155, $h);
            $h = 6;
            $this->SetFont('Arial', '', 6);
            $this->Cell(15, $h, utf8_decode('Dirección :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(140, $h, utf8_decode($direccion), 0, 0, 'L');
            $this->SetFont('Arial', '', 6);
            $this->Cell(10, $h, utf8_decode('Teléfono. :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(40, $h, $telefono, 0, 1, 'C');

            $this->SetFont('Arial', '', 6);
            $this->Cell(15, $h, utf8_decode('Nro. :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(45, $h, $nrocalle, 0, 0, 'C');
            $this->SetFont('Arial', '', 6);
            $this->Cell(15, $h, utf8_decode('Urb. :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(50, $h, $zona, 0, 1, 'L');

            $h = 8;

            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $this->Rect($this->GetX(), $this->GetY(), 90, $h);
            $this->Rect($this->GetX(), $this->GetY(), 140, $h);
            $this->SetFont('Arial', '', 6);
            $this->Cell(20, $h, utf8_decode('Cod. Catastral :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(75, $h, $codcatastro, 0, 0, 'C');
            $this->SetFont('Arial', '', 6);
            $this->Cell(10, $h, utf8_decode('N° Inscripción:'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(35, $h, $codantiguo, 0, 0, 'C');
            $this->SetFont('Arial', '', 6);
            $this->Cell(10, $h, utf8_decode('N° Medidor:'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, $nromed, 0, 1, 'C');

            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $this->Rect($this->GetX(), $this->GetY(), 140, $h);
            $this->SetFont('Arial', '', 6);
            $this->Cell(15, $h, utf8_decode('Nombre :'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(125, $h, utf8_decode($propietario), 0, 0, 'L');
            $this->SetFont('Arial', '', 6);
            $this->Cell(10, $h, utf8_decode('R.U.C.:'), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, $ruc, 0, 1, 'C');


            $this->Ln(2);
            $h = 6;
            $this->Rect($this->GetX(), $this->GetY(), 60, $h);
            $this->SetFont('Arial', '', 6);
            $this->Cell(50, 4, utf8_decode("Observaciones"), 0, 1, 'C');
            $h = 50;
            $this->Ln(2);
            $this->Rect($this->GetX(), $this->GetY(), 190, $h);
            $this->Rect($this->GetX(), $this->GetY(), 155, $h);
            $h = 6;
            $this->SetFont('Arial', '', 6);
            $this->SetX(165);
            $this->Cell(20, $h, utf8_decode('Tiempo de Atención: '), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(40, $h, utf8_decode($diasatencion." (días)"), 0, 0, 'L');
            $this->SetX(10);
            $this->SetFont('Arial', '', 10);
            //$this->Cell(155,$h,utf8_decode($glosa),0,0,'L');
            $this->MultiCell(155, $h, utf8_decode(strtoupper($glosa)), 0, 'J');



             $this->Ln(48); 
//              $this->SetFont('Arial','',8);
//              $this->Cell(10,0,"",0,0,'L');
//              $this->Cell(50,0,"",'T',0,'L');
//              $this->Cell(60,0,"",0,0,'L');
//              $this->Cell(50,0,"",'T',1,'L');
//              $this->Cell(10,$h,"",0,0,'L');
//              $this->Cell(50, $h,utf8_decode($propietario),0,0,'C');
//              $this->Cell(60,0,"",0,0,'L');
//              $this->Cell(50, $h,$inspector,0,1,'C');
//              $this->Cell(10,$h,"",0,0,'L');
//              $this->Cell(50, $h,$documento." - ".$nrodocumento,0,0,'C');
//              $this->Cell(60,0,"",0,0,'L');
//              $this->Cell(50, $h,$insdocumento." - ".$insnrodocumento,0,1,'C');
//             
        }

    }

    $nrosolicitud = $_GET["nrosolicitud"];
    $nuevocliente = $_GET["nuevocliente"];
    $codsuc = $_GET["codsuc"];

    $objReporte = new clsSolicitud();
	
    if ($nuevocliente == 0)
	{
        $sqlcabecera = "SELECT sol.nroinspeccion, UPPER(clie.propietario), UPPER(tipdoc.abreviado), clie.nrodocumento, ";
		$sqlcabecera .= " UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion ), sol.glosa, ";
		$sqlcabecera .= " UPPER(insp.nombres), UPPER(tdoc.abreviado), insp.nrodocumento, sol.resuelve, sol.horareg, sol.fechaemision, ";
		$sqlcabecera .= " sol.diasatencion, '' AS concepto, sol.codconcepto, clie.nrocalle, zo.descripcion AS zona, ";
		$sqlcabecera .= " ".$objReporte->getCodCatastral("clie.").", clie.nroinscripcion, clie.nromed, clie.ruc, clie.codantiguo ";
		$sqlcabecera .= "FROM catastro.solicitudinspeccion sol ";
		$sqlcabecera .= " INNER JOIN catastro.clientes clie ON(sol.codemp = clie.codemp) AND (sol.codsuc = clie.codsuc) AND (sol.nroinscripcion = clie.nroinscripcion) ";
		$sqlcabecera .= " INNER JOIN public.tipodocumento tipdoc ON(clie.codtipodocumento = tipdoc.codtipodocumento) ";
		$sqlcabecera .= " INNER JOIN public.calles cal ON(clie.codemp = cal.codemp) AND (clie.codsuc = cal.codsuc) AND (clie.codcalle = cal.codcalle) AND (clie.codzona = cal.codzona) ";
		$sqlcabecera .= " INNER JOIN public.tiposcalle tipcal ON(cal.codtipocalle = tipcal.codtipocalle) ";
		$sqlcabecera .= " INNER JOIN reglasnegocio.inspectores insp ON(sol.codinspector = insp.codinspector) AND (sol.codsuc = insp.codsuc) ";
		$sqlcabecera .= " INNER JOIN public.tipodocumento tdoc ON(insp.codtipodocumento = tdoc.codtipodocumento) ";
		$sqlcabecera .= " INNER JOIN admin.zonas zo ON(clie.codemp = zo.codemp) AND (clie.codsuc = zo.codsuc) AND (clie.codzona = zo.codzona) ";
		$sqlcabecera .= "WHERE sol.codsuc = ".$codsuc." AND sol.nroinspeccion = ".$nrosolicitud;
    }
	else
	{
		$sqlcabecera = "SELECT sol.nroinspeccion, UPPER(sol.propietario), '', '', ";
		$sqlcabecera .= " UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion), sol.glosa, ";
		$sqlcabecera .= " UPPER(insp.nombres), UPPER(tdoc.abreviado), insp.nrodocumento, sol.resuelve, sol.horareg, sol.fechaemision, ";
		$sqlcabecera .= " sol.diasatencion, '' AS concepto, sol.codconcepto, sol.nrocalle, zo.descripcion AS zona, ";
		$sqlcabecera .= " ".$objReporte->getCodCatastral("sol.").", sol.nroinscripcion, '', '', clie.codantiguo ";
		$sqlcabecera .= "FROM catastro.solicitudinspeccion sol ";
		$sqlcabecera .= " INNER JOIN public.calles cal ON(sol.codemp = cal.codemp) AND (sol.codsuc = cal.codsuc) AND (sol.codcalle = cal.codcalle) AND (sol.codzona = cal.codzona) ";
		$sqlcabecera .= " INNER JOIN public.tiposcalle tipcal ON(cal.codtipocalle = tipcal.codtipocalle) ";
		$sqlcabecera .= " INNER JOIN reglasnegocio.inspectores insp ON(sol.codinspector = insp.codinspector) AND (sol.codsuc = insp.codsuc) ";
		$sqlcabecera .= " INNER JOIN public.tipodocumento tdoc ON(insp.codtipodocumento = tdoc.codtipodocumento) ";
		$sqlcabecera .= " INNER JOIN admin.zonas zo ON(sol.codemp = zo.codemp) AND (sol.codsuc = zo.codsuc) AND (sol.codzona = zo.codzona) ";
		$sqlcabecera .= "INNER JOIN catastro.clientes AS c ON (c.codemp = sol.codemp AND c.codsuc =  sol.codsuc AND c.codzona = sol.codzona AND c.codcalle = sol.codcalle AND c.nroinscripcion= sol.nroinscripcion)";
		$sqlcabecera .= "WHERE sol.codsuc = ".$codsuc." AND sol.nroinspeccion = ".$nrosolicitud;
    }
	
    $consulta = $conexion->prepare($sqlcabecera);
    $consulta->execute(array());
    $row = $consulta->fetch();
	
	if ($row[0] == '')//SI ES EVENTUAL
	{ 
		$sqlcabecera = "SELECT sol.nroinspeccion, UPPER(sol.propietario), '', '', ";
		$sqlcabecera .= " sol.direccion, sol.glosa, ";
		$sqlcabecera .= " UPPER(insp.nombres), UPPER(tdoc.abreviado), insp.nrodocumento, sol.resuelve, sol.horareg, sol.fechaemision, ";
		$sqlcabecera .= " sol.diasatencion, '' AS concepto, sol.codconcepto, sol.nrocalle, '', '', sol.nroinscripcion, '', '' ";
		$sqlcabecera .= "FROM catastro.solicitudinspeccion sol ";
		$sqlcabecera .= " INNER JOIN reglasnegocio.inspectores insp ON(sol.codinspector = insp.codinspector) AND (sol.codsuc = insp.codsuc) ";
		$sqlcabecera .= " INNER JOIN public.tipodocumento tdoc ON(insp.codtipodocumento = tdoc.codtipodocumento) ";
		$sqlcabecera .= "WHERE sol.codsuc = ".$codsuc." AND sol.nroinspeccion = ".$nrosolicitud;
		
        $consulta = $conexion->prepare($sqlcabecera);
        $consulta->execute(array());
        $row = $consulta->fetch();
    }
	
    $horareg = $row['horareg'];
    $fechaemision = $row['fechaemision'];

    $objReporte->AliasNbPages();
    $objReporte->AddPage();

    $objReporte->Contenido($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], 
            $row[6], $row[7], $row[8], $row['resuelve'], $row['codconcepto'], 
            $row['concepto'], $row['nrocalle'], $row['zona'], $row['codcatastro'], 
            $row['nroinscripcion'], $row['nromed'], $row['ruc'], $row['diasatencion'], $row['codantiguo']);
			
    $objReporte->Output();
?>
