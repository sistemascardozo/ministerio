<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsDrop.php");
	
	$objDrop = new clsDrop();
	
	$Op 		= $_GET["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$codemp		= 1;
	$codsuc 	= $_SESSION['IdSucursal'];
	$codsector	= $_POST["codsector"];
	$guardar	= "op=".$Op;
	$disabled	= "";
	
	$sqlRuta = "select descripcion
				from public.rutasmaelecturas
				where codemp=1 and codsuc=? and codsector=? and codrutlecturas=? ";

	$consRuta = $conexion->prepare($sqlRuta);
	$consRuta->execute(array($codsuc,$codsector,$Id));
	$itemRuta = $consRuta->fetch();

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
urldir = '<?php echo $_SESSION['urldir'];?>'

var cont = 0; 
var contador=0; 

function QuitaFilaD(x)
{	
	while (x.tagName.toLowerCase() !='tr')
	{
		if(x.parentElement)
			x=x.parentElement;
		else if(x.parentNode)
			x=x.parentNode;
		else
			return;
	}
	
	var rowNum=x.rowIndex;
	while (x.tagName.toLowerCase() !='table')
	{
		if(x.parentElement)
			x=x.parentElement;
		else if(x.parentNode)
			x=x.parentNode;
		else
			return;
	}
	x.deleteRow(rowNum);
	contador=contador-1
}
function Agregar()
{
	var codmanzana 	= $("#manzanas").val()
	var manzanas	= $("#manzanas option:selected").text();
	
	if(codmanzana==0)
	{
		//alert("Seleccione la Manzana")
		//return false		
	}

	for (i = 1; i <= $("#contador").val(); i++) 
	{ 		
		try
		{
			if(eval($("#codmanzanas"+i).val()==codmanzana))
			{
				Msj($("#manzanas"),"La Manzana se Encuentra Agregada");
				return false
			}
		}catch(exp)
		{
			
		}
	} 
	
	cont++;
	contador++;
	
	$("#tbrutas tbody" ).append("<tr style='background-color:#FFFFFF; padding:4px;'>"+
								"<td align='center'><input type='hidden' name='codmanzanas"+cont+"' id='codmanzanas"+cont+"' value='"+codmanzana+"' />"+codmanzana+"</td>"+
								"<td>"+manzanas+"</td>"+
								"<td align='center' ><input name='nroorden"+cont+"' id='nroorden"+cont+"' type='text' style='text-align:center' value='0' class='inputtext' maxlength='5' size='5' /></td>"+
								"<td align='center'><span class='icono-icon-trash'  onclick='QuitaFilaD(this);' title='Borrar Registros' ></span></td>"+
								"</tr>")
	
	$("#contador").val(cont)
}
function ValidarForm(Op)
{
	for (x = 1; x <= $("#contador").val(); x++) 
	{ 		
			try
			{
				if($("#nroorden"+x).val()=="")
				{
					Msj($("#nroorden"+x),"Ingrese el Orden en : "+x)
					return false
				}
			}catch(exp)
			{

			}
	} 
	GuardarP(Op)
}	
function Cancelar()
{
	location.href='index.php';
}
</script>
<form id="form1" name="form1" method="post" action="guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos" align="center">
      <tbody>
        
        <tr>
            <td colspan="2">
            <fieldset>
               <table border="0px" width="100%">
               	<tr>
            <td width="70" class="TitDetalle">&nbsp;</td>
            <td width="30" class="TitDetalle">&nbsp;</td>
            <td class="CampoDetalle">&nbsp;</td>
        </tr>
                    <tr>
                        <td align="right">Rutas</td>
                        <td align="center">:</td>
                        <td colspan="2" align="left">
                          	<input type="text" name="ruta" id="ruta" style="width:220px" class="inputtext" readonly="readonly" value="<?=$itemRuta["descripcion"]?>" />
                        	<input type="hidden" name="codruta" id="codruta" value="<?=$Id?>" />
                        	<input type="hidden" name="codsector" id="codsector" value="<?=$codsector?>" /></td>
                    </tr>
                    <tr>
                        <td align="right">Manzana</td>
                      <td align="center">:</td>
                        <td width="36%" align="left">
                        	<?php
								$sql = $objDrop->Sentencia("manzanas")." WHERE codemp=1 AND codsuc=? AND codsector=? ORDER BY descripcion ";
								//print_r($sql);
								$consulta = $conexion->prepare($sql);
								$consulta->execute(array($codsuc,$codsector));
								$items = $consulta->fetchAll();
								
								echo "<select id='manzanas' name='manzanas' class='select' style='width:220px' >";
								echo "<option value='0'>--Seleccione la Manzana--</option>";
								foreach($items as $row)
								{
									$sqlcont = "select count(*) from rutaslecturas 
												where codsuc=? and codsector=? AND codrutdistribucion=? and codmanzanas=?";
												
									$conscont = $conexion->prepare($sqlcont);
									$conscont->execute(array($codsuc,$codsector,$Id,$row["codmanzanas"]));
									$itemCont = $conscont->fetch();
									
									if($itemCont[0]==0)
									{
										echo "<option value='".$row["codmanzanas"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
									}
								}
								//$objDrop->drop_manzanas($codsuc,$codsector); 
							?>
                        </td>
                        <td width="49%" align="left"><img src="../../../../../images/iconos/add.png" alt="" style="cursor: pointer" onclick="Agregar()" /></td>
                    </tr> 
                </table>
          </fieldset></td>
	</tr>
        <tr>
            <td class="TitDetalle">&nbsp;</td>
            <td class="CampoDetalle">&nbsp;</td>
	</tr>
        <tr>
            <td colspan="2" align="center">
                 <table border="1px" rules="all" class="ui-widget-content" frame="void" width="99%" id="tbrutas" >
          			<thead class="ui-widget-header" style="font-size: 11px">
                    <tr style="color: white; font-weight: bold">
                        <th width="17%" align="center" s>Codigo</th>
                        <th width="64%" align="center">Manzana</th>
                        <th width="14%" align="center">Nro. Orden</th>
                        <th width="5%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
<?php
						$cont = 0;
						
						$Sql  = "SELECT r.codmanzanas, m.descripcion, r.nroorden ";
						$Sql .= "FROM public.rutaslecturas r ";
						$Sql .= " INNER JOIN public.manzanas m ON(r.codemp = m.codemp) AND (r.codsuc = m.codsuc) ";
						$Sql .= " AND (r.codsector = m.codsector) AND (r.codmanzanas = m.codmanzanas) ";
						$Sql .= "WHERE r.codsuc = ? AND r.codsector = ? AND r.codrutlecturas = ? ";
						$Sql .= "ORDER BY r.nroorden";
						
						$consultaD = $conexion->prepare($Sql);
						$consultaD->execute(array($codsuc,$codsector,$Id));
						$itemsD = $consultaD->fetchAll();
						
						foreach($itemsD as $rowD)
						{
							$cont++;
?>
                    <tr style="background:#FFF">
                      <td align="center" ><?=$rowD["codmanzanas"]?>
                      <input type="hidden" name="codmanzanas<?=$cont?>" id="codmanzanas<?=$cont?>" value="<?=$rowD["codmanzanas"]?>" />
                      </td>
                      <td align="left" style="padding-left:5px;"><?=strtoupper($rowD["descripcion"])?></td>
                      <td align="center">
                        <input name='nroorden<?=$cont?>' id="nroorden<?=$cont?>" type='text' style="text-align:center" value='<?=$rowD["nroorden"]?>' class='inputtext' maxlength='5' size='5' />
                      </td>
                      <td align="center">
                      	<span class='icono-icon-trash'   onclick='QuitaFilaD(this, 1);' title="Borrar Registros" ></span>
                      	
                      </td>
                    </tr>
                    <?php } ?>
                    <script>
						cont 		= <?=$cont?>;
						contador 	= <?=$cont?>;
					</script>
					</tbody>
                </table>
              </td>
	</tr>
	 <tr>
     	<td class="TitDetalle">&nbsp;</td>
	    <td class="CampoDetalle"><input type="hidden" name="contador" id="contador" value="<?=$cont?>" /></td>
	  </tr>
          </tbody>
    </table>
 </form>