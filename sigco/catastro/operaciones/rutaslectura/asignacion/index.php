<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");
	
	$TituloVentana = "ASIGNACIÓN DE RUTAS DE LECTURA";
	
	$Activo = 1;
	
	$Criterio = 'rLecturas';
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsector 	= isset($_GET["codsector"])?$_GET["codsector"]:1;
	$codsuc = $_SESSION['IdSucursal'];
	$FormatoGrilla = array ();
	
	$Sql = "SELECT s.codrutlecturas, s.descripcion, s.orden, e.descripcion, s.estareg, s.codsector ";
	$Sql .= "FROM public.rutasmaelecturas s ";
	$Sql .= " INNER JOIN public.estadoreg e ON (s.estareg = e.id)";
		  
	$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
	$FormatoGrilla[1] = array('1'=>'codrutlecturas', '2'=>'s.descripcion');          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Ruta de Lectura', 'T3'=>'Orden', 'T4'=>'Estado');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center', 'A4'=>'center');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1'=>'60', 'W3'=>'60', 'W4'=>'90');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 700;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = " AND (codemp = 1 AND codsuc = ".$codsuc." AND codsector = ".$codsector.") ORDER BY codrutlecturas ASC ";                                   //Orden de la Consulta
	$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Modificar(this);"',  //Eventos del Botón
              'BtnCI1'=>'5',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
			  
	$FormatoGrilla[10] = array(array('Name' =>'id','Col'=>1),array('Name' =>'sector','Col'=>6));//DATOS ADICIONALES 
	$FormatoGrilla[11]=4;//FILAS VISIBLES
	
	$_SESSION['Formato'.$Criterio] = $FormatoGrilla;
	
	$Previo=' <table width="100%" border="0" >';
	$Previo.='<tr><td valign="middle" align="left" width="65">Sector :</td><td valign="middle" align="left">';
	$Previo.= $objFunciones->drop_sectores2($codsuc, $codsector, "onchange='BuscarB(".$Op.")'");
	$Previo.='</td></tr></table>';
	
	Cabecera($Previo, $FormatoGrilla[7], 650, 600);
	Pie();
	CuerpoInferior();
?>
<script type="text/javascript">
	function BuscarB(Op)
	{
		var Valor     = document.getElementById('Valor').value;
		var codsector   = document.getElementById('codsector').value;
		var Op2 = '';
		
		if (Op!=0)
		{
			Op2 = '&Op=' + Op;
		}
		
		location.href = 'index.php?Valor=' + Valor + '&pagina=' + Pagina + Op2 + '&codsector=' + codsector;
	}

	function Modificar(obj)
	{
		$("#form1").remove();
		
		var Id = $(obj).parent().parent().data('id')
		var sector = $(obj).parent().parent().data('sector')
		
		$("#Modificar").dialog("open");
		$("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
		
		$.ajax({
			url:'mantenimiento.php',
			type:'POST',
			async:true,
			data:'Id=' + Id + '&codsuc=<?=$codsuc?>&codsector=' + sector,
			success:function(data){
				$("#DivModificar").html(data);
			}
		})
	}
	
	$("#BtnNuevoB").remove();
</script>

