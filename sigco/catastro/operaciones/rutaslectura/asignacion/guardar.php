<?php 	
session_name("pnsu");
	if(!session_start()){session_start();} 
	include('../../../../../objetos/clsFunciones.php');
	
	$objFunciones = new clsFunciones();
	
	$conexion->beginTransaction();
	
	$contador 	= $_POST["contador"];
	$codsuc 	= $_SESSION['IdSucursal'];
	$codemp		= 1;
	
	$Error = 0;
	for($i = 1; $i <= $contador; $i++)
	{
		if(isset($_POST["codmanzanas".$i]))
		{
			$sqlV = "SELECT count(*) FROM public.rutaslecturas ";
			$sqlV .= "WHERE codsuc = ".$codsuc." ";
			$sqlV .= " AND codzona = 1 ";
			$sqlV .= " AND codsector = ".$_POST["codsector"]." ";
			$sqlV .= " AND codrutlecturas = ".$_POST["codruta"]." ";
			$sqlV .= " AND codmanzanas = '".$_POST["codmanzanas".$i]."' ";
			
			$result = $conexion->prepare($sqlV);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error select ";
				die($sqlV);
			}

			$itemV = $result->fetch();

			if($itemV[0] == 0)
			{
				$sql = "INSERT INTO public.rutaslecturas(codemp, codzona, codsector, codrutlecturas, codmanzanas, codsuc, nroorden) ";
				$sql .= "VALUES(".$codemp.",1,  ".$_POST["codsector"].", ".$_POST["codruta"].", '".$_POST["codmanzanas".$i]."', ".$codsuc.", ".$_POST["nroorden".$i].")";
			}
			else
			{
				$sql = "UPDATE public.rutaslecturas SET nroorden = ".$_POST["nroorden".$i]." ";
				$sql .= "WHERE codemp = ".$codemp." ";
				$sql .= " AND codzona = 1 ";
				$sql .= " AND codsuc = ".$codsuc." ";			
				$sql .= " AND codsector = ".$_POST["codsector"]." ";
				$sql .= " AND codrutlecturas = ".$_POST["codruta"]." ";
				$sql .= " AND codmanzanas = '".$_POST["codmanzanas".$i]."' ";	
			}

			$result = $conexion->prepare($sql);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error rutaslecturas";
				die($sql);
			}			
		}		
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
