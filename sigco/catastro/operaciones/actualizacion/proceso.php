<?php 
    session_name("pnsu");
    if (!session_start()) {
        session_start();
    }

    include('../../../../objetos/clsFunciones.php');
    
    $conexion->beginTransaction();
    
    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $nroinscripcion= $_POST["nroinscripcion"];
    $valor 	= $_POST["valor"];
    $campo	= $_POST["campo"];
    $tipo	= $_POST["tipo"]; 
    
    $Ver= "DELETE FROM catastro.tempmodificaciones WHERE campo='$campo' AND nroinscripcion=".$nroinscripcion;
    $result = $conexion->prepare($Ver);
    $result->execute(array());
    
    if($campo=='catetar')
    {
        $consul = $conexion->prepare("SELECT uc.catetar as codtarifa, t.nomtar as tarifa,
                uc.porcentaje as porcentaje, uc.principal as estado
                FROM catastro.unidadesusoclientes uc
                INNER JOIN facturacion.tarifas t ON (uc.catetar = t.catetar AND uc.codemp = t.codemp AND uc.codsuc = t.codsuc)
                WHERE uc.codsuc=?  AND t.estado=1 AND uc.nroinscripcion=?");
        $consul->execute(array($codsuc, $nroinscripcion));
        $rows = $consul->fetchAll();
        $cont = 1;

        foreach ($rows as $key) {

            if ($cont == 1) {
                $Porcentaje= number_format($key['porcentaje'], 2);
                $valorold = substr($key['tarifa'], 0, 3);
                $valorold .= "(".$Porcentaje."-".$key['estado'].")";
                $cont++;
            } else {
                $Porcentaje= number_format($key['porcentaje'], 2);
                $valorold .= "-".substr($key['tarifa'], 0, 3);
                $valorold .= "(".$Porcentaje."-".$key['estado'].")";
            }
        }
        
        $Insert= "INSERT INTO catastro.tempmodificaciones( nroinscripcion, campo, valoractual, valorultimo, codsuc)
            VALUES ($nroinscripcion, '$campo', '$valorold', '$valor', $codsuc)";
        $result = $conexion->prepare($Insert);
        $result->execute(array());
    
        
    }else
        {
            if ($tipo == 0) {
                $sql = "SELECT ".$campo." FROM catastro.clientes WHERE codemp=? and codsuc=? and nroinscripcion=?";
            } else {
                $sql = "SELECT ".$campo." FROM catastro.conexiones WHERE codemp=? and codsuc=? and nroinscripcion=?";
            }
            
            $consultaF = $conexion->prepare($sql);
            $consultaF->execute(array($codemp, $codsuc, $nroinscripcion));
            $items = $consultaF->fetch();
            $valorold = $items[0];
            
            $Insert= "INSERT INTO catastro.tempmodificaciones( nroinscripcion, campo, valoractual, valorultimo, codsuc)
                VALUES ($nroinscripcion, '$campo', '$valorold', '$valor', $codsuc)";
            $result = $conexion->prepare($Insert);
            $result->execute(array());
        }
        
    if (!$result) {
        $conexion->rollBack();
        var_dump($result->errorInfo());
        die(2);
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }

?>