<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	// ini_set("display_errors", 1);
	// error_reporting( E_ALL);

	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "ACTUALIZACION DE USUARIOS";

	$Criterio = 'Usuarios';
	$Activo = 1;

	$Op = isset($_GET['Op'])?$_GET['Op']:0;

	if($Op != 5)
	{
		CuerpoSuperior($TituloVentana);
	}

	unset($_SESSION["Consulta"]);

	if($Op == 100)
	{
		$_SESSION["Consulta"] = 1;
	}

	$codsuc = $_SESSION['IdSucursal'];
	//$codsuc =2;
	$FormatoGrilla = array ();

	$Sql = "SELECT c.codantiguo, ".$objFunciones->getCodCatastral("c.").", c.codcliente, ";
	$Sql .= " c.propietario, tc.descripcioncorta || ' ' || cl.descripcion || '  ' || c.nrocalle, ";
	$Sql .= " c.nromed, e.descripcion, 1, c.nroinscripcion ";
	$Sql .= "FROM catastro.clientes c ";
	$Sql .= " INNER JOIN public.calles cl ON(c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona) ";
	$Sql .= " INNER JOIN public.tiposcalle tc ON(cl.codtipocalle = tc.codtipocalle) ";
	$Sql .= " INNER JOIN public.sectores s ON(c.codemp = s.codemp AND c.codsuc = s.codsuc AND c.codzona = s.codzona AND c.codsector = s.codsector) ";
	$Sql .= " INNER JOIN public.estadoservicio e ON(c.codestadoservicio = e.codestadoservicio) ";

	$SqlPag = "SELECT COUNT(*) ";
	$SqlPag .= "FROM catastro.clientes c ";
	$SqlPag .= " INNER JOIN public.calles cl ON(c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona)
            inner join public.tiposcalle tc ON(cl.codtipocalle = tc.codtipocalle)
            inner join public.sectores s ON(c.codemp = s.codemp AND c.codsuc = s.codsuc AND c.codzona = s.codzona AND c.codsector = s.codsector)
            inner join public.estadoservicio e ON(c.codestadoservicio = e.codestadoservicio) ";

	$SqlPag = preg_replace("[\n\r\n\r]", ' ', $SqlPag);

  //$Sql= "SELECT * FROM catastro.view_index_catastroclientes AS w  ";

  $FormatoGrilla[0] = preg_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  //$FormatoGrilla[1] = array('1'=>'c.nroinscripcion', '2'=>'c.codcliente','3'=>'c.propietario', '4'=>'c.codantiguo','5'=>'cl.descripcion','6'=>'tc.descripcioncorta','7'=>'c.nrocalle');
  $FormatoGrilla[1] = array('1'=>'c.nroinscripcion', '2'=>'c.codcliente', '3'=>'c.propietario', '4'=>'c.nrodocumento',
  					'5'=>'c.codantiguo', '6'=>"tc.descripcioncorta || ' ' ||cl.descripcion || ' # ' || c.nrocalle",
                     '7'=>'s.descripcion', '8'=>'('.substr($objFunciones->getCodCatastral("c."), 0, - 15).')', '9'=>'c.nromed', '10'=>'e.descripcion');	//Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Nro. Inscripcion','T2'=>'Cod Catastral','T3'=>'Cod. Cliente','T4'=>'Usuario','T5'=>'Direcci&oacute;n','T6'=>'N° Medidor','T7'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'center', 'A6'=>'center', 'A7'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'70', 'W2'=>'140', 'W3'=>'80');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  AND (c.codemp = 1 AND c.codsuc = ".$codsuc.") ORDER BY codcatastro DESC ";
  //$FormatoGrilla[8] = "  AND  (w.codsuc=".$codsuc." ) ORDER BY w.nroinscripcion desc ";                                //Orden de la Consulta

	if($Op != 5)
	{
		$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'2',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Modificar(this);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
							'BtnId2'=>'BtnFicha',   //Nombre del Boton
              'BtnI2'=>'documento.png',   //Imagen a mostrar
              'Btn2'=>'Ficha Catastral',       //Titulo del Botón
              'BtnF2'=>'onclick="Ficha_Catastral(this);"',  //Eventos del Botón
              'BtnCI2'=>'8',  //Item a Comparar
              'BtnCV2'=>'1'    //Valor de comparación
              );
	}
	else
	{
		$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
		$FormatoGrilla[5] = array('W1'=>'60','W6'=>'80','W11'=>'20','W2'=>'70','W3'=>'70','W10'=>'90');
	}

  $FormatoGrilla[10] = array(array('Name' =>'id','Col'=>9));//DATOS ADICIONALES
  $FormatoGrilla[11]=7;//FILAS VISIBLES
  $FormatoGrilla[100] = $SqlPag;

  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],1000,600);
  Pie();


?>
<script type="text/javascript">
	var urldir = '<?php echo $urldir;?>';

	function ValidarEnter(e, Op)
        {
        switch(e.keyCode)
            {
                case $.ui.keyCode.UP:
                    for (var i = 10; i >= 1; i--)
                        {
                            if ($('tr[tabindex=\'' + i  + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' +i  + '\']')
                                if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                                {
                                  $('tr[tabindex=\'' + i  + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }

                             }
                        };
                        $('#Valor').focus().select();
                break;
                case $.ui.keyCode.DOWN:

                       for (var i = 1; i <= 10; i++)
                        {
                            if ($('tr[tabindex=\'' + i  + '\']') != null )
                              {
                                var obj = $('tr[tabindex=\'' +i  + '\']')
                                if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
                                {
                                  $('tr[tabindex=\'' + i  + '\']').focus();
                                   e.preventDefault();
                                   return false;
                                   break;
                               }

                             }
                        };

                        $('#Valor').focus().select();
                break;
                default:  if (VeriEnter(e)) Buscar(Op);


            }


            //ValidarEnterG(evt, Op)
        }
$("#BtnNuevoB").remove();
function Enviar(obj)
  {
    var Id = $(obj).parent().parent().data('id')
    opener.Recibir(Id);
    window.close();
  }
   function Modificar(obj)
  {
    //$(obj).parent().parent()

    var nroinscripcion = $(obj).parent().parent().data('id')
    $("#Modificar").dialog("open");
    $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
    $.ajax({
       url:'mantenimiento.php',
       type:'POST',
       async:true,
        data: 'Op=1&Id=' + nroinscripcion + '&codsuc=<?=$codsuc?>',
       success:function(data){
        $("#DivModificar").html(data);
       }
    })
  }
	function Ficha_Catastral(obj)
 {

	 var nroinscripcion = $(obj).parent().parent().data('id')
	 AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/catastro/operaciones/actualizacion/ficha_catastral.php?nroinscripcion=" + nroinscripcion + '&codsuc=<?=$codsuc?>', 800, 600);

 }
</script>
<?php
	if($Op != 5)
	{
		CuerpoInferior();
	}
?>
