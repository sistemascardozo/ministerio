<?php

date_default_timezone_set("America/Lima");
session_name("pnsu");

include("../../../../objetos/clsReporte.php");

ini_set("memory_limit", "512M");

// ini_set("display_errors",1);
// error_reporting(E_ALL);

class clsFichaCatastral extends clsReporte {

    function cabecera() {
        global $codsuc, $meses;

        $periodo = $this->DecFechaLiteral();

        $this->SetFont('Arial', 'B', 10);
        $this->SetY(8);
        $h = 4;
        $this->Cell(210, $h + 2, "DATOS BASICOS", 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(210, $h + 2, "", 0, 1, 'C');
        $this->Ln(1);
    }

    // function Contenido($nroinscripion, $codcatastro, $ruc, $facturacion, $recibo, $femision, $propietario, $direccion,
    //     $servicio, $tfact, $tarifa, $nromed, $fechalectanterior, $lectanterior, $consumo, $fechalectultima,
    //     $lectultima, $mensaje, $fvencimientodeudores, $fcorte, $fvencimiento, $docidentidad, $nrodocidentidad,
    //     $mensajenormal, $codantiguo, $zona, $tipoactividad, $catetar, $consumofact, $ruta, $serie, $vma,$nrofacturacion,$horasabastecimiento ) {

    function contenido($nroinscripcion_original, $provincia, $distrito, $sector, $manzana, $local, $sublocal, $ciclo, $nroinscripcion, $ciudad, $direccion, $numero, $complemento, $urbanizacion, $propietario, $documento_identidad, $ruc, $telefono, $correo, $distri_fact, $estado_conex, $tipo_serv_conex, $situacion_conex, $rut_reparto, $cant_unidad_uso, $fecha_eje_agua, $diametro_agua, $fecha_eje_desague, $diametro_desague, $numero_med_insta, $capacidad_med_insta, $diametro_med_insta, $fecha_med_insta, $lectura_med_insta, $ruta_lect_med_insta,
						$numero_med_reti, $capacidad_med_reti, $fecha_med_reti, $lectura_med_reti, $numero_uni_uso, $ambito_uni_uso, $categoria_uni_uso, $subcategoria_uni_uso, $tarifa_uni_uso, $responsable_uni_uso, $personas_uni_uso, $tipouso_uni_uso, $cantidad_uni_uso, $codantiguo, $tipo_propi_complement, $tipo_contruc_complement, $material_contruc_complement, $pisos_complement, $situacion_conex_complement, $tipo_reserv_complement, $piscina_complement, $frentera_complement, $area_lot_complement,
						$area_contruc_complement, $contrato_complement, $cod_traba_complement, $tipo_abas_agua_complement, $sector_agua_complement, $tipo_desague_complement, $sector_desague_complement, $facturacion_espe_complement, $grupo_complement, $subgrupo_complement, $banco_complement, $numero_cuenta_complement, $mate_conex_agua_complement, $local_caja_medidor_agua_complement, $material_caja_agua_complement, $mate_conex_desague_complement, $local_caja_medidor_desague_complement, $material_caja_desague_complement)
	{
        global $conexion, $codsuc;
        global $Dim, $Dim1, $Dim2, $Dim3;

        $h = 10;
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(60, $h, "Ficha de Datos Catastrales", 0, 0, 'C');
        $this->Cell(1, $h, "", 0, 0, 'L');
        $h = 5;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim[0], $h, "Provincia", 0, 0, 'C');
        $this->Cell($Dim[1], $h, "Distrito", 0, 0, 'C');
        $this->Cell($Dim[2], $h, "Sector", 0, 0, 'C');
        $this->Cell($Dim[3], $h, "Manzana", 0, 0, 'C');
        $this->Cell($Dim[4], $h, "Local", 0, 0, 'C');
        $this->Cell($Dim[5], $h, "Sublocal", 0, 0, 'C');
        $this->Cell($Dim[6], $h, "Ciclo", 0, 0, 'C');
        $this->Cell($Dim[7], $h, "Inscripcion", 0, 0, 'C');
        $this->Cell($Dim[8], $h, "Ciudad", 0, 1, 'C');

        $this->SetFont('Arial', '', 6);
        $this->Cell(61, $h, "", 0, 0, 'L');
        $this->Cell($Dim[0], $h, $provincia, 0, 0, 'C');
        $this->Cell($Dim[1], $h, $distrito, 0, 0, 'C');
        $this->Cell($Dim[2], $h, $sector, 0, 0, 'C');
        $this->Cell($Dim[3], $h, $manzana, 0, 0, 'C');
        $this->Cell($Dim[4], $h, $local, 0, 0, 'C');
        $this->Cell($Dim[5], $h, $sublocal, 0, 0, 'C');
        $this->Cell($Dim[6], $h, $ciclo, 0, 0, 'C');
        $this->Cell($Dim[7], $h, $nroinscripcion, 0, 0, 'C');
        $this->Cell($Dim[8], $h, $ciudad, 0, 1, 'C');
        $this->Ln(1);

        // Level 2
        $h = 3;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim1[0], $h, utf8_decode("Dirección"), 0, 0, 'L');
        $this->Cell($Dim1[1], $h, utf8_decode(strtoupper("Normal")), 0, 0, 'C');
        $this->Cell($Dim1[2], $h, utf8_decode("Número"), 0, 0, 'C');
        $this->Cell($Dim1[3], $h, utf8_decode("Complemento"), 0, 0, 'C');
        $this->Cell($Dim1[4], $h, utf8_decode("Urbanización"), 0, 1, 'L');

        $h = 4.5;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim1[0], $h, utf8_decode(strtoupper($direccion)), 0, 0, 'L');
        $this->Cell($Dim1[1], $h, utf8_decode(""), 0, 0, 'C');
        $this->Cell($Dim1[2], $h, utf8_decode($numero), 0, 0, 'C');
        $this->Cell($Dim1[3], $h, utf8_decode($complemento), 0, 0, 'C');
        $this->Cell($Dim1[4], $h, utf8_decode(strtoupper($urbanizacion)), 0, 1, 'L');

        $h = 3;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim2[0], $h, utf8_decode("Nombre del Cliente"), 0, 0, 'L');
        $this->Cell($Dim2[1], $h, utf8_decode("Doc. Ide."), 0, 0, 'L');
        $this->Cell($Dim2[2], $h, utf8_decode("Ruc"), 0, 0, 'L');
        // $this->Cell($Dim2[3], $h, utf8_decode("Telefono"), 0, 0, 'L');
        $this->Cell($Dim2[4], $h, utf8_decode("Correo"), 0, 1, 'L');

        $h = 4.5;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim2[0], $h, utf8_decode(strtoupper($propietario)), 0, 0, 'L');
        $this->Cell($Dim2[1], $h, utf8_decode($documento_identidad), 0, 0, 'L');
        $this->Cell($Dim2[2], $h, utf8_decode($ruc), 0, 0, 'L');
        // $this->Cell($Dim2[3], $h, $telefono, 0, 0, 'L');
        $this->Cell($Dim2[4], $h, $correo, 0, 1, 'L');
        $this->Ln(1);

        // Level 3
        $h = 9;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim3[0], $h, utf8_decode("Distribución de Facturación"), 0, 0, 'L');
        $this->Cell($Dim3[1], $h, utf8_decode("Estado de Conexión"), 0, 0, 'L');
        $this->Cell($Dim3[2], $h, utf8_decode("Tipo de Servicio de Conexión"), 0, 0, 'L');
        $this->Cell($Dim3[3], $h, utf8_decode("Situación de Conexión"), 0, 0, 'L');
        $this->Cell($Dim3[4], $h, utf8_decode("Ruta de Reparto"), 0, 0, 'C');
        $this->Cell($Dim3[5], $h, utf8_decode("Cant. Total Unid. Uso"), 0, 1, 'C');

        $h = 6;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim3[0], $h, utf8_decode($distri_fact), 0, 0, 'L');
        $this->Cell($Dim3[1], $h, utf8_decode($estado_conex), 0, 0, 'L');
        $this->Cell($Dim3[2], $h, utf8_decode($tipo_serv_conex), 0, 0, 'L');
        $this->Cell($Dim3[3], $h, utf8_decode($situacion_conex), 0, 0, 'L');
        $this->Cell($Dim3[4], $h, utf8_decode($rut_reparto), 0, 0, 'C');

        // Calculo de unidad de Uso
        $sql = "SELECT COUNT(*) as cantidad FROM catastro.unidadesusoclientes WHERE codemp = 1 AND codsuc = ? AND nroinscripcion = ?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $nroinscripcion_original));
        $items = $consulta->fetch();


        $this->Cell($Dim3[5], $h, utf8_decode($items['cantidad']), 0, 1, 'C');
        $this->Ln(1);

        global $Dim4;

        // Level 4
        $h = 7;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim4[0], $h, utf8_decode("CONEXIÓN DE AGUA"), 0, 0, 'L');
        $this->Cell($Dim4[1], $h, "", 0, 0, 'L');
        $this->Cell($Dim4[2], $h, utf8_decode("CONEXIÓN DE DESAGUE"), 0, 1, 'L');

        $h = 4;
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($Dim4[3], $h, utf8_decode("Fecha de Ejecución"), 0, 0, 'L');
        $this->Cell($Dim4[3], $h, utf8_decode("Diámetro"), 0, 0, 'L');
        $this->Cell($Dim4[1], $h, "", 0, 0, 'L');

        $this->Cell($Dim4[3], $h, utf8_decode("Fecha de Ejecución"), 0, 0, 'L');
        $this->Cell($Dim4[3], $h, utf8_decode("Diámetro"), 0, 1, 'L');

        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim4[3], $h, utf8_decode($fecha_eje_agua), 0, 0, 'L');
        $this->Cell($Dim4[3], $h, utf8_decode($diametro_agua), 0, 0, 'L');

        $this->Cell($Dim4[1], $h, "", 0, 0, 'L');

        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim4[3], $h, utf8_decode($fecha_eje_desague), 0, 0, 'L');
        $this->Cell($Dim4[3], $h, utf8_decode($diametro_desague), 0, 1, 'L');
        $this->Ln(1);

        // Level 5
        global $Dim5;

        $h = 7;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim5[0], $h, utf8_decode("MEDIDOR INSTALADO"), 0, 0, 'L');
        $this->Cell($Dim5[1], $h, utf8_decode("MEDIDOR RETIRADO"), 0, 1, 'L');

        $h = 4;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim5[2], $h, utf8_decode("Número"), 0, 0, 'L');
        $this->Cell($Dim5[3], $h, utf8_decode("Capacidad"), 0, 0, 'L');
        $this->Cell($Dim5[4], $h, utf8_decode("Diámetro"), 0, 0, 'L');
        $this->Cell($Dim5[5], $h, utf8_decode("Fecha"), 0, 0, 'L');
        $this->Cell($Dim5[6], $h, utf8_decode("Lectura"), 0, 0, 'L');
        $this->Cell($Dim5[7], $h, utf8_decode("Ruta Lect."), 0, 0, 'L');
        $this->Cell($Dim5[8], $h, utf8_decode("Número"), 0, 0, 'L');
        $this->Cell($Dim5[9], $h, utf8_decode("Capacidad"), 0, 0, 'L');
        $this->Cell($Dim5[10], $h, utf8_decode("Fecha"), 0, 0, 'L');
        $this->Cell($Dim5[11], $h, utf8_decode("Lectura"), 0, 1, 'L');

        $this->Cell($Dim5[2], $h, utf8_decode($numero_med_insta), 0, 0, 'L');
        $this->Cell($Dim5[3], $h, utf8_decode($capacidad_med_insta), 0, 0, 'L');
        $this->Cell($Dim5[4], $h, utf8_decode($diametro_med_insta), 0, 0, 'L');
        $this->Cell($Dim5[5], $h, utf8_decode($fecha_med_insta), 0, 0, 'L');
        $this->Cell($Dim5[6], $h, utf8_decode($lectura_med_insta), 0, 0, 'L');
        $this->Cell($Dim5[7], $h, utf8_decode($ruta_lect_med_insta), 0, 0, 'L');

        $VerRet="SELECT MAX(d.coddetmedidor), d.nromed, ca.descripcion AS capacidad,
            to_char(d.fechamov, 'dd/mm/YYYY') AS fecha, d.lecturaultima
            FROM micromedicion.detmedidor AS d
            INNER JOIN public.capacidadmedidor AS ca ON ca.codcapacidadmedidor = d.codcapacidadmedidor
            WHERE d.nroinscripcion=$nroinscripcion_original and d.codtipomovimiento=3 and d.estareg=3
            GROUP BY d.nromed, ca.descripcion, d.fechamov, d.lecturaultima";
        $res = $conexion->prepare($VerRet);
        $res->execute(array());
        $rw = $res->fetch();
        $numero_med_reti= $rw['nromed'];
        $capacidad_med_reti= $rw['capacidad'];
        $fecha_med_reti= $rw['fecha'];
        $lectura_med_reti= $rw['lecturaultima'];

        $this->Cell($Dim5[8], $h, utf8_decode($numero_med_reti), 0, 0, 'L');
        $this->Cell($Dim5[9], $h, utf8_decode($capacidad_med_reti), 0, 0, 'L');
        $this->Cell($Dim5[10], $h, utf8_decode($fecha_med_reti), 0, 0, 'L');
        $this->Cell($Dim5[11], $h, utf8_decode($lectura_med_reti), 0, 1, 'L');



        $this->Ln(1);

        // Level 6
        global $Dim6;

        $h = 7;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim6[0], $h, utf8_decode("UNIDADES DE USO"), 0, 1, 'L');

        $h = 4;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim6[1], $h, utf8_decode("Nro."), 0, 0, 'L');
        $this->Cell($Dim6[2], $h, utf8_decode("Ambito"), 0, 0, 'L');
        $this->Cell($Dim6[3], $h, utf8_decode("Categoria"), 0, 0, 'L');
        $this->Cell($Dim6[4], $h, utf8_decode("Subcategoria"), 0, 0, 'L');
        $this->Cell($Dim6[5], $h, utf8_decode("Tarifa"), 0, 0, 'L');
        $this->Cell($Dim6[6], $h, utf8_decode("Responsable"), 0, 0, 'L');
        $this->Cell($Dim6[7], $h, utf8_decode("Pers."), 0, 0, 'L');
        $this->Cell($Dim6[8], $h, utf8_decode("Tipo de Uso"), 0, 0, 'L');
        $this->Cell($Dim6[9], $h, utf8_decode("Cant."), 0, 1, 'L');

        $h = 4;
        $this->SetFont('Arial', '', 6);
        $numero_uni_uso = $items['cantidad'];
        $this->Cell($Dim6[1], $h, utf8_decode($numero_uni_uso), 0, 0, 'L');

        $this->Cell($Dim6[2], $h, utf8_decode($ambito_uni_uso), 0, 0, 'L');
        $this->Cell($Dim6[3], $h, utf8_decode($categoria_uni_uso), 0, 0, 'L');
        $this->Cell($Dim6[4], $h, utf8_decode($subcategoria_uni_uso), 0, 0, 'L');
        $this->Cell($Dim6[5], $h, utf8_decode($tarifa_uni_uso), 0, 0, 'L');
        $this->Cell($Dim6[6], $h, utf8_decode($responsable_uni_uso), 0, 0, 'L');
        $personas_uni_uso = (isset($personas_uni_uso)) ? $personas_uni_uso : 0;
        $this->Cell($Dim6[7], $h, utf8_decode($personas_uni_uso), 0, 0, 'L');

        $sql_unidades = " SELECT a.descripcion as actividad FROM catastro.unidadesusoclientes as u
            INNER JOIN facturacion.tarifas as t on(u.codemp=t.codemp and u.codsuc=t.codsuc and u.catetar=t.catetar)
            INNER JOIN public.tipoactividad as a on(u.codtipoactividad=a.codtipoactividad)
            WHERE u.codemp = 1 and u.codsuc = ? and u.nroinscripcion = ? LIMIT 1";

        $consulta_unidades = $conexion->prepare($sql_unidades);
        $consulta_unidades->execute(array($codsuc, $nroinscripcion_original));
        $items_unidad = $consulta_unidades->fetch();
        $this->Cell($Dim6[8], $h, utf8_decode($items_unidad['actividad']), 0, 0, 'L');
        $this->Cell($Dim6[9], $h, utf8_decode($cantidad_uni_uso), 0, 1, 'L');
        $this->Ln(1);

        // Level 7
        global $Dim7;

        $h = 8;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim7[0], $h, utf8_decode("DATOS COMPLEMENTARIOS"), 0, 0, 'L');
        $this->Cell($Dim7[1], $h, utf8_decode(""), 0, 0, 'L');
        $this->Cell($Dim7[2], $h, utf8_decode("Codigo Antiguo:"), 0, 0, 'L');
        $this->Cell($Dim7[3], $h, utf8_decode($codantiguo), 0, 1, 'L');

        $this->Ln(1);

        // Level 8
        global $Dim8;

        $h = 9;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim8[0], $h, utf8_decode("Tipo de Propiedad"), 0, 0, 'L');
        $this->Cell($Dim8[1], $h, utf8_decode("Tipo de Construcción"), 0, 0, 'L');
        $this->Cell($Dim8[2], $h, utf8_decode("Material Construcción"), 0, 0, 'L');
        $this->Cell($Dim8[3], $h, utf8_decode("Pisos"), 0, 0, 'L');
        $this->Cell($Dim8[4], $h, utf8_decode("Situación de la Conexión"), 0, 1, 'C');

        $h = 6;
        $this->SetFont('Arial', '', 6);
        if ($tipo_propi_complement == 1): $tipo_propi_complement = "UNIFAMILIAR";
        else: $tipo_propi_complement = "MULTIFAMILIAR";
        endif;
        $this->Cell($Dim8[0], $h, utf8_decode($tipo_propi_complement), 0, 0, 'L');

        $this->Cell($Dim8[1], $h, utf8_decode($tipo_contruc_complement), 0, 0, 'L');
        $this->Cell($Dim8[2], $h, utf8_decode($material_contruc_complement), 0, 0, 'L');
        $this->Cell($Dim8[3], $h, utf8_decode($pisos_complement), 0, 0, 'L');
        $this->Cell($Dim8[4], $h, utf8_decode($situacion_conex_complement), 0, 1, 'C');
        $this->Ln(1);

        // Level 9
        global $Dim9;

        $h = 8;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim9[0], $h, utf8_decode("Tipo de Reservorios"), 0, 0, 'L');
        $this->Cell($Dim9[1], $h, utf8_decode("Piscina"), 0, 0, 'C');
        // $this->Cell($Dim9[2], $h, utf8_decode("Frentera"), 0, 0, 'C');
        $this->Cell($Dim9[3], $h, utf8_decode("Area del Lote"), 0, 0, 'C');
        // $this->Cell($Dim9[4], $h, utf8_decode("Area Constr."), 0, 0, 'C');
        $this->Cell($Dim9[5], $h, utf8_decode("Contrato"), 0, 1, 'C');
        // $this->Cell($Dim9[6], $h, utf8_decode("Cód. Trabajador"), 0, 1, 'C');

        $h = 7;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim9[0], $h, utf8_decode($tipo_reserv_complement), 0, 0, 'L');
        if ($piscina_complement == 0) :
            $piscina_complement = "SIN";
        else:
            $piscina_complement = "SI";
        endif;

        $contrato_complement = str_pad($contrato_complement, 3 , "0", STR_PAD_LEFT);

        $this->Cell($Dim9[1], $h, utf8_decode($piscina_complement), 0, 0, 'C');
        // $this->Cell($Dim9[2], $h, utf8_decode($frentera_complement), 0, 0, 'C');
        $this->Cell($Dim9[3], $h, utf8_decode($area_lot_complement), 0, 0, 'C');
        // $this->Cell($Dim9[4], $h, utf8_decode($area_contruc_complement), 0, 0, 'C');
        $this->Cell($Dim9[5], $h, utf8_decode($contrato_complement), 0, 1, 'C');
        // $this->Cell($Dim9[6], $h, utf8_decode($cod_traba_complement), 0, 1, 'C');
        $this->Ln(1);

        // Level 10
        global $Dim10;

        $h = 8;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim10[0], $h, utf8_decode("Tipo Abastec. Agua"), 0, 0, 'L');
        $this->Cell($Dim10[1], $h, utf8_decode("Sector Agua"), 0, 0, 'C');
        // $this->Cell($Dim10[2], $h, utf8_decode("Tipo de Desague"), 0, 0, 'C');
        $this->Cell($Dim10[3], $h, utf8_decode("Sector de Desague"), 0, 1, 'C');
        // $this->Cell($Dim10[4], $h, utf8_decode("Facturación Especial"), 0, 1, 'L');

        $h = 5;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim10[0], $h, utf8_decode($tipo_abas_agua_complement), 0, 0, 'L');
        $this->Cell($Dim10[1], $h, utf8_decode($sector_agua_complement), 0, 0, 'C');
        // $this->Cell($Dim10[2], $h, utf8_decode($tipo_desague_complement), 0, 0, 'C');
        $this->Cell($Dim10[3], $h, utf8_decode($sector_desague_complement), 0, 1, 'C');
        // $this->Cell($Dim10[4], $h, utf8_decode($facturacion_espe_complement), 0, 1, 'L');

        $h = 7;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim10[5], $h, utf8_decode("Grupo"), 0, 1, 'L');
        // $this->Cell($Dim10[6], $h, utf8_decode("Sub Grupo"), 0, 0, 'L');
        // $this->Cell($Dim10[7], $h, utf8_decode("Banco"), 0, 0, 'L');
        // $this->Cell($Dim10[8], $h, utf8_decode("Número de Cuenta"), 0, 1, 'L');

        $h = 5;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim10[5], $h, utf8_decode($grupo_complement), 0, 1, 'L');
        // $this->Cell($Dim10[6], $h, utf8_decode($subgrupo_complement), 0, 0, 'L');
        // $this->Cell($Dim10[7], $h, utf8_decode($banco_complement), 0, 0, 'L');
        // $this->Cell($Dim10[8], $h, utf8_decode($numero_cuenta_complement), 0, 1, 'L');
        $this->Ln(1);

        // Level 11
        global $Dim11;

        $h = 7;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim11[0], $h, utf8_decode("CONEXION DE AGUA"), 0, 1, 'L');

        $h = 4;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim11[1], $h, utf8_decode("Material de Conexión"), 0, 0, 'L');
        $this->Cell($Dim11[2], $h, utf8_decode("Localización de Caja Medidor"), 0, 0, 'L');
        $this->Cell($Dim11[2], $h, utf8_decode("Material de la Caja"), 0, 1, 'L');

        $this->Cell($Dim11[1], $h, utf8_decode($mate_conex_agua_complement), 0, 0, 'L');
        $this->Cell($Dim11[2], $h, utf8_decode($local_caja_medidor_agua_complement), 0, 0, 'L');
        $this->Cell($Dim11[2], $h, utf8_decode($material_caja_agua_complement), 0, 1, 'L');
        $this->Ln(1);

        // Level 11
        global $Dim12;

        $h = 7;
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($Dim12[0], $h, utf8_decode("CONEXION DE DESAGUE"), 0, 1, 'L');

        $h = 4;
        $this->SetFont('Arial', '', 6);
        $this->Cell($Dim12[1], $h, utf8_decode("Material de Conexión"), 0, 0, 'L');
        $this->Cell($Dim12[2], $h, utf8_decode("Localización de Caja Medidor"), 0, 0, 'L');
        $this->Cell($Dim12[2], $h, utf8_decode("Material de la Caja"), 0, 1, 'L');

        $this->Cell($Dim12[1], $h, utf8_decode($mate_conex_desague_complement), 0, 0, 'L');
        $this->Cell($Dim12[2], $h, utf8_decode($local_caja_medidor_desague_complement), 0, 0, 'L');
        $this->Cell($Dim12[2], $h, utf8_decode($material_caja_desague_complement), 0, 1, 'L');
    }

    function Footer() {

    }

    //  Para los cuadros con bordes redondeados o sin ellos
    function RoundedRect($x, $y, $w, $h, $r, $style = '') {
        $k = $this->k;
        $hp = $this->h;
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op = 'S';
        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m', ($x + $r) * $k, ($hp - $y) * $k));
        $xc = $x + $w - $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - $y) * $k));

        $this->_Arc($xc + $r * $MyArc, $yc - $r, $xc + $r, $yc - $r * $MyArc, $xc + $r, $yc);
        $xc = $x + $w - $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc + $r, $yc + $r * $MyArc, $xc + $r * $MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x + $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - ($y + $h)) * $k));
        $this->_Arc($xc - $r * $MyArc, $yc + $r, $xc - $r, $yc + $r * $MyArc, $xc - $r, $yc);
        $xc = $x + $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1 * $this->k, ($h - $y1) * $this->k, $x2 * $this->k, ($h - $y2) * $this->k, $x3 * $this->k, ($h - $y3) * $this->k));
    }

}

$Dim = array(0 => 15, 1 => 15, 2 => 15, 3 => 15, 4 => 15, 5 => 15, 6 => 15, 7 => 15, 8 => 9, 9 => 15);
$Dim1 = array(0 => 61, 1 => 24, 2 => 20, 3 => 35, 4 => 50);
$Dim2 = array(0 => 50, 1 => 34, 2 => 30, 3 => 41, 4 => 35);
$Dim3 = array(0 => 35, 1 => 30, 2 => 40, 3 => 30, 4 => 25, 5 => 30);
$Dim4 = array(0 => 80, 1 => 30, 2 => 80, 3 => 40);
$Dim5 = array(0 => 95, 1 => 95, 2 => 16, 3 => 16, 4 => 16, 5 => 16, 6 => 16, 7 => 15, 8 => 25, 9 => 25, 10 => 25, 11 => 20);
$Dim6 = array(0 => 190, 1 => 21, 2 => 21, 3 => 21, 4 => 21, 5 => 21, 6 => 21, 7 => 21, 8 => 31, 9 => 12);
$Dim7 = array(0 => 60, 1 => 70, 2 => 25, 3 => 35);
$Dim8 = array(0 => 40, 1 => 40, 2 => 50, 3 => 20, 4 => 40);
$Dim9 = array(0 => 28, 1 => 27, 2 => 27, 3 => 27, 4 => 27, 5 => 27, 6 => 27);
$Dim10 = array(0 => 38, 1 => 38, 2 => 38, 3 => 38, 4 => 38, 5 => 47, 6 => 47, 7 => 48, 8 => 48);
$Dim11 = array(0 => 190, 1 => 64, 2 => 63, 3 => 63);
$Dim12 = array(0 => 190, 1 => 64, 2 => 63, 3 => 63);


$nroinscripcion = $_GET["nroinscripcion"];
$codsuc = $_GET["codsuc"];
$codciclo = 1;

$objReporte = new clsFichaCatastral();

$sql = "SELECT
    c.codzona as codzona , z.descripcion as distrito,
    c.codsector as codsector , s.descripcion AS sector,
    CAST(c.codmanzanas as integer) as codmanzanas, m.descripcion as manzanas,
    c.lote as lote,
    c.sublote as sublote,
    c.nroinscripcion as nroinscripion, c.codantiguo as codantiguo,
    (tc.descripcioncorta ||' '|| cl.descripcion) AS direccion,
    (c.nrocalle) AS nrocalle,
    (u.tipo ||' '|| u.descripcion) as urbanizacion,
    c.propietario as propietario,
    c.nrodocumento as nrodocumento,
    c.nrodocumento as ruc,
    c.catetar AS tarifa,
    tu.descripcion AS estado_conex,
    CASE
            WHEN c.codtiposervicio = 0 THEN 'NN'
            WHEN c.codtiposervicio = 1 THEN 'A/D'
            WHEN c.codtiposervicio = 2 THEN 'A'
            ELSE 'D'
    END AS tiposervicio, ts.descripcion AS tipo_serv_conex,
    e.descripcion AS situacion_conex, e.codestadoservicio,
    c.codrutdistribucion AS rut_reparto,
    cx.fechainsagua AS fecha_eje_agua, da.descripcion AS diametro_agua, cx.fechainsdesague AS fecha_eje_desague, dd.descripcion AS diametro_desague, c.nromed AS numero_med_insta, cm.descripcion AS capacidad_med_insta, dm.descripcion AS diametro_med_insta, cx.fechainsmed AS fecha_med_insta, c.lecturaultima AS lectura_med_insta, c.codrutlecturas AS ruta_lect_med_insta,
    z.descripcion AS ambito_uni_uso,
    ctr.descripcion AS categoria_uni_uso,
    (t.nomtar ||''|| to_char(t.volumenesp,'99G999.99')) AS subcategoria_uni_uso,
    (t.catetar) AS tarifa_uni_uso, (trp.descripcion) AS responsable_uni_uso, (cx.nrohabitantes) AS personas_uni_uso,
    cx.tipopredio AS tipo_propi_complement, tcr.descripcion AS tipo_contruc_complement, tmcr.descripcion AS material_contruc_complement, cx.nropisos AS pisos_complement, e.descripcion AS situacion_conex_complement,
    tal.descripcion AS tipo_reserv_complement, cx.piscina AS piscina_complement,
    tab.descripcion AS tipo_abas_agua_complement,
    cte.descripcion AS grupo_complement,
    tpa.descripcion AS mate_conex_agua_complement, lca.descripcion AS local_caja_medidor_agua_complement, tca.descripcion AS material_caja_agua_complement,
    lcd.descripcion AS local_caja_medidor_desague_complement, tcd.descripcion AS material_caja_desague_complement,
    t.nomtar AS subcategoria,
    c.codtipodocumento AS codtipodocumento,
    c.nromed  AS medidor,
    c.codtipodocumento as codtipodocumento,
    CASE WHEN cx.codestadoconexion = 1 THEN 'AC' ELSE 'IN' END AS estado_conexion,
    ex.nrocontrato AS nrocontrato, so.area AS area,
    c.correo
    FROM catastro.clientes AS c
    JOIN public.calles AS cl ON (c.codemp=cl.codemp AND c.codsuc=cl.codsuc AND c.codcalle=cl.codcalle AND c.codzona=cl.codzona)
    JOIN public.tiposcalle AS tc ON(cl.codtipocalle=tc.codtipocalle)
    JOIN public.tipousuario AS tu ON(c.codtipousuario=tu.codtipousuario)
    JOIN public.tiposervicio AS ts ON(c.codtiposervicio = ts.codtiposervicio)
    JOIN facturacion.tarifas AS t ON(c.codemp=t.codemp AND c.codsuc=t.codsuc AND c.catetar=t.catetar)
    JOIN public.estadoservicio AS e ON(c.codestadoservicio=e.codestadoservicio)
    JOIN public.sectores AS s ON(c.codemp=s.codemp AND c.codsuc=s.codsuc AND c.codsector=s.codsector AND c.codzona=s.codzona)
    JOIN catastro.conexiones AS cx ON(c.codemp=cx.codemp AND c.codsuc=cx.codsuc AND c.nroinscripcion=cx.nroinscripcion)
    LEFT JOIN public.diametrosagua AS da ON(cx.coddiametrosagua = da.coddiametrosagua)
    LEFT JOIN public.diametrosdesague AS dd ON(cx.coddiametrosdesague = dd.coddiametrosdesague)
    LEFT JOIN public.capacidadmedidor AS cm ON(cx.codcapacidadmedidor = cm.codcapacidadmedidor)
    LEFT JOIN public.diametrosmedidor AS dm ON(cx.coddiametrosmedidor = dm.coddiametrosmedidor)
    JOIN facturacion.categoriatarifaria AS ctr ON(t.codcategoriatar = ctr.codcategoriatar)
    LEFT JOIN public.tiporesponsable AS trp ON(cx.codtiporesponsable = trp.codtiporesponsable)
    LEFT JOIN public.tipoconstruccion AS tcr ON(cx.codtipoconstruccion = tcr.codtipoconstruccion)
    LEFT JOIN public.tipomatconstruccion AS tmcr ON(cx.codtipomatconstruccion = tmcr.codtipomatconstruccion)
    LEFT JOIN public.tipoalmacenaje AS tal ON(cx.codtipoalmacenaje = tal.codtipoalmacenaje)
    LEFT JOIN public.tipoabastecimiento AS tab ON(cx.codtipoabastecimiento = tab.codtipoabastecimiento)
    LEFT JOIN public.tipoentidades AS cte ON(cx.codtipoentidades = cte.codtipoentidades)
    LEFT JOIN public.tipomaterialagua AS tpa ON(cx.codtipomaterialagua = tpa.codtipomaterialagua)
    LEFT JOIN public.locacajaagua AS lca ON(cx.codlocacajaagua = lca.codlocacajaagua)
    LEFT JOIN public.tipocajaagua AS tca ON(cx.codtipocajaagua = tca.codtipocajaagua)
    LEFT JOIN public.locacajadesague AS lcd ON(cx.codlocacajadesague = lcd.codlocacajadesague)
    LEFT JOIN public.tipocajadesague AS tcd ON(cx.codtipocajadesague = tcd.codtipocajadesague)
    JOIN public.manzanas AS m ON( c.codemp = m.codemp AND c.codsuc = m.codsuc AND c.codzona = m.codzona AND c.codsector = m.codsector AND c.codmanzanas = m.codmanzanas)
    JOIN admin.zonas AS z ON(c.codzona = z.codzona AND c.codsuc = z.codsuc AND c.codemp = z.codemp)
    JOIN public.rutaslecturas AS rl ON(c.codemp = rl.codemp AND c.codsuc = rl.codsuc AND c.codzona = rl.codzona AND c.codsector = rl.codsector AND c.codrutlecturas = rl.codrutlecturas AND c.codmanzanas = rl.codmanzanas)
    JOIN public.rutasdistribucion AS rd ON(c.codemp = rd.codemp AND c.codsuc = rd.codsuc AND c.codzona = rd.codzona AND c.codsector = rd.codsector AND c.codrutdistribucion = rd.codrutdistribucion AND c.codmanzanas = rd.codmanzanas)
    LEFT JOIN catastro.urbanizaciones AS u ON( c.codsuc = u.codsuc AND c.codzona = u.codzona AND c.codurbanizacion = u.codurbanizacion)
    LEFT JOIN solicitudes.expedientes AS ex ON( c.codemp = ex.codemp AND c.codsuc = ex.codsuc AND c.nroinscripcion = ex.nroinscripcion)
    LEFT JOIN solicitudes.solicitudes AS so ON( c.codemp = so.codemp AND c.codsuc = so.codsuc AND c.nroinscripcion = so.nroinscripcion)
    WHERE c.codemp = 1 AND c.codsuc = " . $codsuc . " AND c.nroinscripcion = " . $nroinscripcion . " AND t.estado = 1 ";

// echo $nroinscripcion;exit;
// echo $codsuc;exit;
// echo $sql;exit;

$consulta = $conexion->query($sql);
$items = $consulta->fetch();

// echo "<pre>"; var_dump($items); echo "</pre>";exit;

$objReporte->AliasNbPages();
$objReporte->AddPage();

// Primera Linea
$objReporte->SetFillColor(255, 255, 255);
$objReporte->RoundedRect(10, 21, 60, 10, 0.5, 'DF');
$objReporte->SetFillColor(255, 255, 255);
$objReporte->RoundedRect(71, 21, 129, 10, 0.5, 'DF');
// $objReporte->Cabecera1(71, 21, 129, 10, 0.5, 'DF');
// Segunda Linea
$objReporte->RoundedRect(10, 32, 190, 15, 0.5, 'DF');

// Tercera Linea
$objReporte->RoundedRect(10, 48, 190, 15, 0.5, 'DF');

// Cuarta Linea
$objReporte->RoundedRect(10, 64, 80, 15, 0.5, 'DF');
$objReporte->RoundedRect(120, 64, 80, 15, 0.5, 'DF');

//Quinta Linea
$objReporte->RoundedRect(10, 80, 190, 15, 0.5, 'DF');

//Sexta Linea
$objReporte->RoundedRect(10, 96, 190, 15, 0.5, 'DF');

//Septima Linea
$objReporte->RoundedRect(10, 112, 60, 8, 0.5, 'DF');
$objReporte->RoundedRect(140, 112, 60, 8, 0.5, 'DF');

//Octava Linea
$objReporte->RoundedRect(10, 121, 190, 15, 0.5, 'DF');

//Novena Linea por partes
$objReporte->RoundedRect(10, 137, 190, 15, 0.5, 'DF');

//Decima Linea
$objReporte->RoundedRect(10, 153, 190, 25, 0.5, 'DF');

//Onceava Linea
$objReporte->RoundedRect(10, 179, 190, 15, 0.5, 'DF');

//Doceava Linea
$objReporte->RoundedRect(10, 195, 190, 15, 0.5, 'DF');

switch ($items['codtipodocumento']) :
    case 1: $items["nrodocumento"] = $items["nrodocumento"];
        $items["ruc"] = "";
        break;
    case 3: $items["ruc"] = $items["nrodocumento"];
        $items["nrodocumento"] = "";
        break;
    default:$items["nrodocumento"] = "";
        $items["ruc"] = "";
        break;
endswitch;

if ($items['codtipodocumento'] == 1):
    $items["nrodocumento"] = $items["nrodocumento"];
endif;

if(empty($items['nrocontrato'])){
  $items['nrocontrato'] = '';
}

if(empty($items['area'])){
  $items['area'] = '0.00';
}


$objReporte->contenido($nroinscripcion, '', $items["codzona"], $items["codsector"], $items["codmanzanas"], $items["lote"], $items["sublote"],
        $codciclo, $items["codantiguo"], '', $items["direccion"], $items["nrocalle"], $complemento, $items['urbanizacion'], $items["propietario"],
        $items["nrodocumento"], $items["ruc"], '', $items["correo"], '', $items["estado_conex"], $items["tipo_serv_conex"], $items["situacion_conex"],
        $items['rut_reparto'], '', $items['fecha_eje_agua'], $items['diametro_agua'], $items['fecha_eje_desague'], $items['diametro_desague'],
        $items['numero_med_insta'], $items['capacidad_med_insta'], $items['diametro_med_insta'], $items['fecha_med_insta'],
        number_format($items['lectura_med_insta'], 0), $items['ruta_lect_med_insta'], $items['numero_med_reti'], $items['capacidad_med_reti'],
        $items['fecha_med_reti'], $items['lectura_med_reti'], $items['numero_uni_uso'], $items['ambito_uni_uso'], $items['categoria_uni_uso'],
        $items['subcategoria_uni_uso'], $items['tarifa_uni_uso'], $items['responsable_uni_uso'], $items['personas_uni_uso'], '',
        $items['cantidad_uni_uso'], '', $items['tipo_propi_complement'], $items['tipo_contruc_complement'], $items['material_contruc_complement'],
        $items['pisos_complement'], $items['situacion_conex_complement'], $items['tipo_reserv_complement'], $items['piscina_complement'], '', $items['area'], '',
        $items['nrocontrato'], '', $items['tipo_abas_agua_complement'], $items['sector'], '', $items['sector'], '', $items['grupo_complement'], '', '', '', $items["mate_conex_agua_complement"],
        $items['local_caja_medidor_agua_complement'], $items['material_caja_agua_complement'], '', $items['local_caja_medidor_desague_complement'],
        $items['material_caja_desague_complement']
);

$objReporte->Output();
?>
