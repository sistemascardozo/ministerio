<?php
	session_name("pnsu");
	if (!session_start()) {session_start();}
	
	include("../../../../objetos/clsDrop.php");
	
	$objMantenimiento = new clsDrop();
	
	$Consulta = isset($_SESSION["Consulta"]) ? $_SESSION["Consulta"] : '0';
	
	$Op = isset($_POST["Op"]) ? $_POST["Op"] : $_GET["Op"];
	
	$Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
	
	if ($Op == 100)
	{
		include("../../../../include/claseindex.php");
		
		$Id = $_GET['NroInscripcion'];
		
		$Consulta = 1;
	}

	$codsuc = $_SESSION['IdSucursal'];
	
	$guardar = "op=" . $Op;
	
	
	unset($_SESSION["oactualizacion"]);

	$sucursal = $objMantenimiento->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);


	$sql = "SELECT c.nroinscripcion, c.codcliente, c.codsector, codmanzanas, c.codcalle, c.lote, c.sublote, c.codestadoservicio, conx.codtiporesponsable,
		c.propietario,c.codtipodocumento,c.nrodocumento,c.nrocalle,conx.altocon,conx.codtipoentidades,c.codtipousuario,c.direcciondistribucion,
		conx.inquilino,c.codtipoactividad,c.codciclo,conx.tipopredio,conx.codtipoconstruccion,conx.nropisos,conx.codtipoabastecimiento,conx.piscina,
		conx.codtipoalmacenaje,c.codtiposervicio,conx.referencia,conx.codtipomaterialagua,conx.codlocacajaagua,conx.coddiametrosagua,conx.codtipopavimentoagua,
		conx.codtipocorte,conx.codestadoconexion,conx.codtipovereda,conx.fechainsagua,conx.codtipocajaagua,conx.codestadocajaagua,conx.codtipotapaagu,
		conx.estadotapaagu,conx.codtipofugasagua,conx.codmarca,conx.coddiametrosmedidor,conx.codestadomedidor,conx.posicionmed,conx.codmodelo,
		conx.tipolectura,conx.aniofabmed,conx.codtipomedidor,conx.fechainsmed,conx.codcapacidadmedidor,c.nromed,c.tipofacturacion,conx.codubicacion,
		conx.coddiametrosdesague,conx.codtipomaterialdesague,conx.codlocacajadesague,conx.fechainsdesague,conx.codtipocajadesague,conx.codestadocajadesague,
		conx.codtipotapadesague,conx.estadotapadesague,conx.fugasatoros,conx.codzonaabas,conx.presionagua,conx.horasabastecimiento,conx.nrolavatorios,
		conx.nrolavaropas,conx.nrowater,conx.nroduchas,conx.nrourinarios,conx.nrogrifos,conx.nrohabitantes,conx.observacionabast,c.lecturaanterior,
		c.lecturaultima,c.consumo,c.fechalecturaanterior,c.fechalecturaultima,tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,/*t.nomtar*/ '' as nomtar ,
		e.descripcion as estser,ts.descripcion as tservicio,c.latitud,c.longitud,c.correo,conx.codtipomatconstruccion,c.nroorden,
		c.lecturapromedio,c.codrutlecturas,c.codzona,conx.conpozo,conx.conpozoimporte,c.codantiguo, c.catetar, c.vma, conx.orden_lect, 
		conx.orden_dist, c.codurbanizacion, c.codrutdistribucion
		FROM catastro.clientes as c
		INNER JOIN catastro.conexiones as conx on(c.codemp=conx.codemp and c.codsuc=conx.codsuc
		and c.nroinscripcion=conx.nroinscripcion)
		INNER JOIN public.calles as cl on(c.codemp= cl.codemp AND c.codsuc= cl.codsuc AND c.codcalle= cl.codcalle AND c.codzona=cl.codzona)
		INNER join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
		INNER join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)
		INNER join public.tiposervicio as ts on(c.codtiposervicio=ts.codtiposervicio)
		WHERE c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";
	//die($sql);
	//echo $codsuc."-".$Id;
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc, $Id));
	$items = $consulta->fetch();
	//var_dump($consulta->errorInfo());
	$guardar = $guardar . "&Id2=" . $Id;

	$direccion = $items["descripcioncorta"] . " " . $items["calle"] . " " . $items["nrocalle"];
	$servicio = $items["tservicio"];
	$facturacion = $tfacturacion[$items["tipofacturacion"]];
	
	$fechinstagua = new DateTime($items["fechainsagua"]);
	$fechainstagua = $fechinstagua->format("d/m/Y");
	
	$fechinstmedidor = new DateTime($items["fechainsmed"]);
	$fechainstmedidor = $fechinstmedidor->format("d/m/Y");
	
	$fechinstdesague = new DateTime($items["fechainsdesague"]);
	$fechainstdesague = $fechinstdesague->format("d/m/Y");
	
	$fechlectanterior = new DateTime($items["fechalecturaanterior"]);
	$fechalectanterior = $fechlectanterior->format("d/m/Y");
	
	$fechlectultima = new DateTime($items["fechalecturaultima"]);
	$fechalectultima = $fechlectultima->format("d/m/Y");
	$conpozo = $items['conpozo'] ? $items['conpozo'] : 0;

	if ($Op == 100)
	{
?>
    <link rel="stylesheet" href="<?php echo $_SESSION['urldir']; ?>css/css_base.css" type="text/css" />
    <script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/jquery-ui-1.9.2.custom.min.js"></script>

    <script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/jquery_sistema.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $_SESSION['urldir']; ?>images/empresa.ico" />

    <!-- Código CheGuimo Inicio -->
    <link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir']; ?>css/interface.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['urldir']; ?>css/interface_menu.css">
    <script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones_interface.js"></script>
    <script>
        var urlDir = '<?php echo $_SESSION['urldir']; ?>';
        var codsuc = <?=$codsuc ?>;
    </script>
<?php
	}
?>

<style type="text/css">

    #tbCampos * {
        font-size: 10px;
    }
</style>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_actualizacion.js" language="JavaScript"></script>
<script type="text/javascript" >
    function buscar_calle()
    {
        object = "calles"
        var codzona = $("#codzona").val()
        var codsector = $("#sector").val()
        if (codzona == 0)
        {
            alert('Seleccione una Zona');
            return false;
        }

        if (codsector == 0)
        {
            alert('Seleccione un Sector');
            return false;
        }
        AbrirPopupBusqueda("../../../../buscar/calles.php?codzona=" + codzona + "&codsector=" + codsector + "&codsuc=" + codsuc, 700, 450)
    }
    
    function recibir_calles(Id, Des)
    {
        $("#calles").val(Id)
        $("#calles").prop('selected', true)
        // $("#calles").val(Des)
    }
    
    function buscar_urbanizacion()
    {
        object = "urbanizacion"
        var codzona = $("#codzona").val()
        if (codzona == 0)
        {
            alert('Seleccione una Zona');
            return false;
        }
        AbrirPopupBusqueda("../../../../buscar/urbanizacion.php?codzona=" + codzona + "&codsuc=" + codsuc, 700, 450)
    }
    
    function recibir_urbanizacion(Id, Des)
    {
        $("#codurbanizacion").val(Id)
        $("#codurbanizacion").prop('selected', true)
    }

    function buscar_actividad()
    {
        AbrirPopupBusqueda("../../../../buscar/actividad.php", 700, 450)
    }
    function recibir_actividad(Id)
    {
        $("#tipoactividad").val(Id)
        $("#tipoactividad").prop('selected', true)
    }

    var codsuc = <?=$codsuc ?>;
    var urldir = '<?php echo $_SESSION['urldir']; ?>';

    VImportePozo(<?=$conpozo ?>);

    $(document).ready(function () {
        $("#tabs").tabs();

        $('form').keypress(function (e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
        });


        $("#DivLecturas").dialog({
            autoOpen: false,
            height: 250,
            width: 520,
            modal: true,
            resizable: true,
            buttons: {
                "Aceptar": function () {
                    if (objIdx == "promediar")
                    {
                        $("#lecturaultima").val($("#lectpromedio").val())
                        $("#consumo").val($("#lectpromedio").val())
                        $("#lecturapromedio").val($("#lectpromedio").val())
                        //calcularconsumo($("#lectpromedio").val())
                    } else {
                        if ($("#obstem").val() == '')
                        {
                            Msj($("#obstem"), "Digite el Motivo de Modificación")
                            return false;
                        }
                        $("#obs").val($("#obstem").val())
                        GuardarP(1)
                    }
                    $(this).dialog("close");
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
            }
        });


    })

    function BloquearTodo()
    {
        if ('<?=$Consulta ?>' == '1')
        {
            $("input").each(function (index)
            {
                $(this).attr("readonly", "readonly");
            })

            $("textarea").each(function (index)
            {
                $(this).attr("readonly", "readonly");
            })

            $("select").each(function (index)
            {
                $(this).attr("disabled", "disabled");
            })

            $("radio").each(function (index)
            {
                $(this).attr("disabled", "disabled");
            })

            //Iconos de Buqueda y otros
            $('#imgBuscarU').hide();
            $('#imgBuscarC').hide();

            $('#flagnuevo').hide();
            $('#lblUsuarioN').hide();

            $('#altoconsumidor').hide();
            $('#lblAltoConsumidor').hide();

            $('#imgAsignarC').hide();
            $('#imgPromediarL').hide();

            //Fecha
            //$('#fechalecturaanterior').empty();

            $('.trUnidUso').hide();
            $('.imgQuitarUU').hide();

            $('#button-maceptar').hide();
        }
    }

    function ValidarForm(Op)
    {
        //alert('');
        if ($("#cCatastro").val() > 0 && <?=$Op ?> != 2)
        {
            /*alert("El Codigo Catastral ya Existe")
            return false*/
            $("#tabs").tabs({selected: 0});
            Msj($("#codantiguo"), "El Codigo Catastral ya Existe")
            return false;
        }
        $("#nropresupuesto").removeClass("ui-state-error");
        
        if ($("#contmod").val() == 0)
        {
            //alert('No se ha Realizado Ninguna Modificacion');
            //return false;
            $("#tabs").tabs({selected: 0});
            Msj($("#codantiguo"), "No se ha Realizado Ninguna Modificacion")
            return false;
            
        }
        $("#contmod").removeClass("ui-state-error");

        if ($("#codzona").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#codzona"), "Seleccione el Zona")
            return false;
        }
        $("#codzona").removeClass("ui-state-error");
        
        if ($("#sector").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#sector"), "Seleccione el Sector")
            return false;
        }
        $("#sector").removeClass("ui-state-error");
        
        if ($("#manzanas").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#manzanas"), "Seleccione el Manzana")
            return false;
        }
        $("#manzanas").removeClass("ui-state-error");
        
        if ($("#lote").val() == "")
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#lote"), "Digite el Nro. de Lote")
            return false;
        }
        $("#lote").removeClass("ui-state-error");
        
        if ($("#sublote").val() == "")
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#sublote"), "Digite el Nro. de Sub. Lote")
            return false;
        }
        $("#sublote").removeClass("ui-state-error");
        
        if ($("#estadoservicio").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#estadoservicio"), "Seleccione el Estado de Servicio")
            return false;
        }
        $("#estadoservicio").removeClass("ui-state-error");
        
        if ($("#propietario").val() == "" && $("#flagnuevotext").val() == 1)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#propietario"), "Digite el Nombre del Propietario")
            return false;
        }
        $("#propietario").removeClass("ui-state-error");
        
        if ($("#tipodocumento").val() == '')
        {
            Msj($("#sector"), "Seleccione el Tipo de Documento")
            return false;
        }
        $("#tipodocumento").removeClass("ui-state-error");
        
        if ($("#tipodocumento").val() != '0')
        {
            if ($("#nrodocumento").val() == "")
            {
                $("#tabs").tabs({selected: 0});
                Msj($("#nrodocumento"), "Digite el Nro. de Documento")
                return false;
            }
            $("#tipodocumento").removeClass("ui-state-error");
        }

        if ($("#calles").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#calles"), "Seleccione la Calle")
            return false;
        }
        $("#calles").removeClass("ui-state-error");
        
        if ($("#nrocalle").val() == "")
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#nrocalle"), "Digite el Nro. de Calle")
            return false;
        }
        $("#nrocalle").removeClass("ui-state-error");
        
        if ($("#ciclo").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#ciclo"), "Seleccione el Ciclo de Facturacion")
            return false;
        }
        $("#ciclo").removeClass("ui-state-error");
        
        if ($("#tiposervicio").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#tiposervicio"), "Seleccione el Tipo de Servicio")
            return false;
        }
        $("#tiposervicio").removeClass("ui-state-error");
        
        if ($('input[name="conpozo"]:checked').val() == 1)
        {
            if (parseFloat($("#conpozoimporte").val()) == 0 || isNaN($("#conpozoimporte").val()))
            {
                $("#tabs").tabs({selected: 1});
                Msj($("#conpozoimporte"), "Ingrese Importe Valido")
                return false;
            }
            $("#conpozoimporte").removeClass("ui-state-error");
        }
        
        if ($("#tipofacturacion").val() == 100)
        {
            $("#tabs").tabs({selected: 3});
            Msj($("#tipofacturacion"), "Seleccione el Tipo de Facturacion")
            return false;
        }
        $("#tipofacturacion").removeClass("ui-state-error");
        
        if ($("#tipofacturacion").val() == 0)
        {
            if ($("#nromedidor").val() == "")
            {
                $("#tabs").tabs({selected: 4});
                Msj($("#nromedidor"), "El Nro. de Medidor no pueder ser NULO")
                return false
            }
            $("#nromedidor").removeClass("ui-state-error");
            
            if ($("#lecturaanterior").val() == "")
            {
                $("#tabs").tabs({selected: 3});
                Msj($("#lecturaanterior"), "Digite la Lectura Anterior")
                return false
            }
            $("#lecturaanterior").removeClass("ui-state-error");
            
            if ($("#lecturaultima").val() == "")
            {
                $("#tabs").tabs({selected: 3});
                Msj($("#lecturaultima"), "Digite la Lectura Ultima")
                return false
            }
            $("#lecturaultima").removeClass("ui-state-error");
            
            if ($("#comsumo").val() == "")
            {
                $("#tabs").tabs({selected: 3});
                Msj($("#comsumo"), "Digite la Lectura Ultima")
                return false
            }
            $("#comsumo").removeClass("ui-state-error");
            
        }
        
        if ($("#zonasbastecimiento").val() == 0)
        {
            $("#tabs").tabs({selected: 5});
            Msj($("#zonasbastecimiento"), "Seleccione la Zona de abastecimiento")
            return false;
        }
        $("#zonasbastecimiento").removeClass("ui-state-error");
        
        if ($("#presionagua").val() == 0)
        {
            $("#tabs").tabs({selected: 5});
            Msj($("#presionagua"), "Seleccione la Presion de Agua")
            return false;
        }
        $("#presionagua").removeClass("ui-state-error");
        
        if (parseInt($("#tbunidades tbody tr").length) == 0)
        {
            $("#tabs").tabs({selected: 6});
            Msj($("#tbunidades"), "Aun no se ha Agregado ninguna categoria tarifaria");
            return false;
        }
        $("#tbunidades").removeClass("ui-state-error");
        
        validar_cont_presupuesto = 0;
        for (var i = 1; i <= $("#cont_unidades").val(); i++)
        {
            try
            {
                if ($("#principal" + i).val() == 0)
                {
                    validar_cont_presupuesto++;
                }
            } catch (exp)
            {

            }
        }

        if ($("#principal" + i).val() == validar_cont_presupuesto)
        {
            $("#tabs").tabs({selected: 6});
            Msj($("#tbunidades"), "Aun No ha Asignado una Categoria Principal");
            return false
        }
        $("#tbunidades").removeClass("ui-state-error");
        
        if ($("#horasabastecimiento").val() == "") {
            $("#horasabastecimiento").val(0)
        }
        if ($("#nrolavatorios").val() == "") {
            $("#nrolavatorios").val(0)
        }
        if ($("#nrolavaropas").val() == "") {
            $("#nrolavaropas").val(0)
        }
        if ($("#nrowater").val() == "") {
            $("#nrowater").val(0)
        }
        if ($("#nroduchas").val() == "") {
            $("#nroduchas").val(0)
        }
        if ($("#nrourinarios").val() == "") {
            $("#nrourinarios").val(0)
        }
        if ($("#nrogrifos").val() == "") {
            $("#nrogrifos").val(0)
        }
        if ($("#nrohabitantes").val() == "") {
            $("#nrohabitantes").val(0)
        }
        //VALIDAR CATEGORIAS
        c = 0;
        $("#tbunidades input.principal").each(function () {
            if ($(this).val() == 1)
                c++;
        });
        if (c == 0)
        {
            $("#tabs").tabs({selected: 6});

            Msj('#tbunidades', 'Seleccione Categoria Principal')
            return false;
        }
        $.ajax({
            url: 'observacion.php',
            type: 'POST',
            async: true,
            data: '',
            success: function (datos) {
                $("#div_lecturas").html(datos)
            }
        })
        objIdx = "guardar";
        vCatastroSave()
        return false
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }
    function vCatastroSave()
    {
        var sector = $("#sector").val();
        var manzanas = $("#manzanas").val();
        var lote = $("#lote").val();

        if (sector != "0" && manzanas != "0" && lote != "")
        {
            $.ajax({
                url: 'vcatastro.php',
                type: 'POST',
                async: true,
                data: 'Op=1&sector=' + sector + '&manzanas=' + manzanas + '&lote=' + lote + '&nroinscripcion=' + $("#nroinscripcion").val(),
                dataType: "json",
                success: function (datos)
                {
                    if (datos.resultado == 1)
                    {
                        $("#tabs").tabs({selected: 0});

                        Msj($("#propietario"), 'Existe un usuario con el mismo Catastro : ' + datos.propietario);

                        return false;
                    }
                    else
                    {
                        $("#DivLecturas").dialog("open");
                    }
                }
            })
        }
    }
    function vCatastro()
    {
        var sector = $("#sector").val();
        var manzanas = $("#manzanas").val();
        var lote = $("#lote").val();

        if (sector != "0" && manzanas != "0" && lote != "")
        {
            $.ajax({
                url: 'vcatastro.php',
                type: 'POST',
                async: true,
                data: 'Op=1&sector=' + sector + '&manzanas=' + manzanas + '&lote=' + lote + '&nroinscripcion=' + $("#nroinscripcion").val(),
                dataType: "json",
                success: function (datos)
                {
                    if (datos.resultado == 1)
                    {
                        $("#tabs").tabs({selected: 0});

                        Msj($("#propietario"), 'Existe un usuario con el mismo Catastro : ' + datos.propietario);

                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            })
        }
    }
</script>
<div align="center">
	<form id="form1" name="form1" method="post" action="guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
		<table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
			<tbody>
				<tr>
					<td colspan="3" class="TitDetalle" >
						<div id="tabs">
							<ul>
                                <li style="font-size:10px"><a href="#tabs-2">Datos del Cliente</a></li>
                                <li style="font-size:10px"><a href="#tabs-3">Datos del Predio</a></li>
                                <li style="font-size:10px"><a href="#tabs-4">Conexion de Agua</a></li>                                
                                <li style="font-size:10px"><a href="#tabs-6">Conexion de Desague</a></li>
                                <li style="font-size:10px"><a href="#tabs-5">Medidor</a></li>
                                <li style="font-size:10px"><a href="#tabs-7">Cal. de Servicio</a></li>
                                <li style="font-size:10px"><a href="#tabs-8">Und. de Uso</a></li>
                            </ul>
                            <div id="tabs-2" style="height:375px">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
                                        <td width="14%" align="right">Nro. Inscripcion</td>
                                        <td width="3%" align="center">:</td>
                                        <td width="33%">
                                            <input type="text" class="inputtext"  id="codantiguo" readonly="readonly" maxlentgth="10" size="10" value="<?=$items['codantiguo'] ?>"/>
                                            <input type="hidden" class="inputtext"  id="nroinscripcion" name="nroinscripcion" readonly="readonly" maxlentgth="10" size="10" value="<?=$Id ?>"/>
                                            <input type="hidden" name="codcliente" id="codcliente" value="<?=isset($items["codcliente"]) ? $items["codcliente"] : 0 ?>" />
                                            <img src="../../../../images/iconos/buscar.png" width="16" height="16" title="Buscar Usuario del Catalogo" style="cursor:pointer" onclick="buscar_usuarios();" id="imgBuscarU" /></td>
                                        <td width="10%" align="right">Sucursal</td>
                                        <td width="3%" align="center">:</td>
                                        <td width="39%">
                                            <input type="text" class="inputtext"  id="sucursal1" name="sucursal" readonly="readonly" maxlentgth="30" size="30" value="<?=$sucursal["descripcion"] ?>"/>
                                            <input type="hidden" name="cCatastro" id="cCatastro" value="0" />
                                            <input type="hidden" name="obs" id="obs" value="" />
										</td>
                                    </tr>
                                    <tr>
                                        <td align="right">Zona</td>
                                        <td align="center">:</td>
                                        <td>
<?php
	echo $objMantenimiento->drop_zonas($codsuc, $items["codzona"], "onchange='cargar_sectores(" . $codsuc . ", this.value, 0, 0); cargar_urbanizacion(" . $codsuc . ", this.value, 0, 0)';");
?>
                                        </td>
                                        <td align="right">Sector</td>
                                        <td align="center">:</td>
                                        <td>
                                            <div id="div_sector">
                                                <select name="sector" id="sector" style="width:220px" class="select">
                                                    <option value="0">.:: Seleccione el Sector ::.</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Manzana</td>
                                        <td align="center">:</td>
                                        <td>
                                            <div id="div_manzanas">
                                                <select name="manzanas" id="manzanas" style="width:220px" class="select">
                                                    <option value="0">.:: Seleccione la Manzana ::.</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td align="right">Habilitación</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div id="div_urbanizacion">
                                                                <select name="sector" id="sector" style="width:220px" class="select">
                                                                    <option value="0">.:: Seleccione el Urbanizacion ::.</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <span style="position:relative" class="MljSoft-icon-buscar" title="Buscar Urbanizacion" onclick="buscar_urbanizacion();" id="imgBuscarC" ></span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Lote</td>
                                        <td align="center">:</td>
                                        <td>
<?php
	$checked = "";
	$disabled = "";
	
	if (isset($items["flag_nuevo"]))
	{
		if ($items["flag_nuevo"] == 1)
		{
			$checked = "checked='checked'";
			$disabled = "disabled='disabled'";
		}
	}
?>
                                            <input type="text" class="inputtext entero"  id="lote" name="lote" maxlentgth="10" size="10" value="<?=$items["lote"] ?>" onchange="vCatastro();" onkeyup="actualizacion(this, 0);" />
                                            <input type="checkbox" name="flagnuevo" <?=$disabled ?> id="flagnuevo" <?=$checked ?> onclick="CambiarEstado(this, 'flagnuevotext');
                                                    quitar_readonly(this, 'propietario');"/>
                                            <label id="lblUsuarioN">Usuario Nuevo</label>
                                            <input type="hidden" name="flagnuevotext" id="flagnuevotext" value="<?=isset($items["flag_nuevo"]) ? $items["flag_nuevo"] : 0 ?>" />
                                        </td>
                                        <td align="right">Sub. Lote</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" class="inputtext"  id="sublote" name="sublote" maxlentgth="10" size="10" value="<?=$items["sublote"] ?>" onkeypress="return permite(event, 'num');" onkeyup="actualizacion(this.value, 0);" />
                                            <!-- &nbsp;Orden&nbsp;:&nbsp; -->
                                            <input type="hidden" class="inputtext"  id="nroorden" name="nroorden" maxlentgth="10" size="11" value="0" onkeypress="return permite(event, 'num');" onkeyup="actualizacion(this, 0)"/>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Est. de Servicio</td>
                                        <td align="center">:</td>
                                        <td><?php $objMantenimiento->drop_estado_servicio($items["codestadoservicio"], "onchange='actualizacion(this,0);'"); ?></td>
                                        <td align="right">C. Usuario</td>
                                        <td align="center">:</td>
                                        <td><?php $objMantenimiento->drop_tipo_responsable($items["codtiporesponsable"], "onchange='actualizacion(this,0);'"); ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Propietario</td>
                                        <td align="center">:</td>
                                        <td colspan="4"><input type="text" class="inputtext"  id="propietario" name="propietario" maxlentgth="200" size="100" value="<?=$items["propietario"] ?>" onkeyup="actualizacion(this, 0)" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Documento</td>
                                        <td align="center">:</td>
                                        <td><?php $objMantenimiento->drop_tipodocumento($items["codtipodocumento"], 'onChange="actualizacion(this,0);"'); ?></td>
                                        <td align="right">Nro. Doc.</td>
                                        <td align="center">:</td>
                                        <td>
<?php
	$checked = "";
	
	if (isset($items["altocon"]))
	{
		if ($items["altocon"] == 1)
		{
			$checked = "checked='checked'";
		}
	}
?>
                                            <input type="text" class="inputtext"  id="nrodocumento" name="nrodocumento" maxlength="11" size="10" value="<?=$items["nrodocumento"] ?>" onkeypress="return permite(event, 'num');" onkeyup="actualizacion(this, 0);"/>
                                            <input type="checkbox" name="altoconsumidor" id="altoconsumidor" onclick="CambiarEstado(this, 'altoconsumidortext');validar_altocon();" <?=$checked ?> />
                                            <label id="lblAltoConsumidor">Alto Consumidor</label>
                                            <input type="hidden" name="altoconsumidortext" id="altoconsumidortext" value="<?=isset($items["altocon"]) ? isset($items["altocon"]) : 0 ?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Calle</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div id="div_calles">
                                                            <select id="calles" name="calles" style="width:100px" class="select">
                                                                <option value="0">.:: Seleccione la Calle ::.</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <span style="position:relative" class="MljSoft-icon-buscar" title="Buscar Calle" onclick="buscar_calle();" id="imgBuscarC" ></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">Nro. Calle</td>
                                        <td align="center">:</td>
                                        <td><input type="text" class="inputtext"  id="nrocalle" name="nrocalle" maxlength="500" size="30" value="<?=$items["nrocalle"] ?>" onkeyup="actualizacion(this, 0);" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Grupo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_entidades($items["codtipoentidades"], 'onChange="actualizacion(this,1);"'); ?>
                                        </td>
                                        <td align="right">T. Usuario</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_usuario($items["codtipousuario"], 'onChange="actualizacion(this,0);"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">R. Distri.</td>
                                        <td align="center">:</td>
                                        <td>
                                            <!--<input type="text" class="inputtext"  id="rutadistribucion" name="rutadistribucion" readonly="readonly" maxlentgth="10" size="10" value=""/>-->
                                            <select id='codrutdistribucion' name='codrutdistribucion' class='select' style='width:130px' onchange='actualizacion(this, 0);' >

                                            </select>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secuencia&nbsp;:&nbsp;
                                            <input type="text" class="inputtext"  id="orden_dist" name="orden_dist" maxlentgth="10" size="4" value="<?=$items["orden_dist"] ?>" onkeypress="return permite(event, 'num');" onkeyup="actualizacion(this, 0)" />
                                            <!--<input type="hidden" name="codrutdistribucion" id="codrutdistribucion" value="0" />-->
                                        </td>
                                        <td align="right">R. Lect.</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id='codrutlectura' name='codrutlectura' class='select' style='width:150px' onchange='actualizacion(this, 0);
                                                    ' > <!--cargar_rutas_lecturas_orden();-->

                                            </select>
                                            <!-- <input type="text" class="inputtext"  id="rutalecturas" name="rutalecturas" readonly="readonly" maxlentgth="10" size="10" value=""/> -->
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secuencia&nbsp;:&nbsp;
                                            <input type="text" class="inputtext"  id="orden_lect" name="orden_lect" maxlentgth="10" size="4" value="<?=$items["orden_lect"] ?>" onkeypress="return permite(event, 'num');" onkeyup="actualizacion(this, 0)" />
                                            <input type="hidden" id="codrutlecturatemp" value="<?=$items["codrutlecturas"] ?>" />
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td align="right">Habilitación</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div id="div_urbanizacion">
                                                                <select name="sector" id="sector" style="width:220px" class="select">
                                                                    <option value="0">.:: Seleccione el Urbanizacion ::.</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <span style="position:relative" class="MljSoft-icon-buscar" title="Buscar Urbanizacion" onclick="buscar_urbanizacion();" id="imgBuscarC" ></span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>-->

                                    <tr>
                                        <td align="right">Dir. Distribucion</td>
                                        <td align="center">:</td>
                                        <td colspan="4">
                                            <input type="text" class="inputtext"  id="dirdistribucion" name="dirdistribucion" maxlentgth="200" size="100" value="<?=$items["direcciondistribucion"] ?>" onkeyup="actualizacion(this, 0)" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Inquilino</td>
                                        <td align="center">:</td>
                                        <td >
                                            <input type="text" class="inputtext"  id="inquilino" name="inquilino" maxlentgth="200" size="50" value="<?=$items["inquilino"] ?>"/>
                                        </td>
                                        <td align="right">Correo</td>
                                        <td align="center">:</td>
                                        <td >
                                            <input type="text" class="inputtext3"  id="correo" name="correo" maxlentgth="200" size="50" value="<?=$items["correo"] ?>" />
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td align="right">Ciclo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_ciclos($codsuc, $items["codciclo"], 'onChange="actualizacion(this,0)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Latitud</td>
                                        <td align="center">:</td>
                                        <td><input type="text" class="inputtext"  id="latitud" name="latitud"  maxlentgth="10" size="10" value="<?=$items["latitud"] ?>" onkeyup="actualizacion(this, 0)"/></td>
                                        <td align="right">Longitud </td>
                                        <td align="center">:</td>
                                        <td><input type="text" class="inputtext"  id="longitud" name="longitud" maxlentgth="10" size="10" value="<?=$items["longitud"] ?>" onkeyup="actualizacion(this, 0)"/>
                                            &nbsp;<input type="button" name="buscar" id="buscar" value="Buscar Referencias Catastrales" class="button" onclick="buscar_gis('<?=$direccion ?>', '<?=$items["nomtar"] ?>', '<?=$items["estser"] ?>', '<?=$servicio ?>', '<?=$facturacion ?>')" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-3" style="height:350px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="15%" align="right">T. Predio</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%">
                                            <select name="tipopredio" id="tipopredio" style="width:220px" class="select" onChange="actualizacion(this, 1)">
                                                <option value="1" <?=$items["tipopredio"] == 1 ? "selected='selected'" : "" ?> >UNIFAMILIAR</option>
                                                <option value="2" <?=$items["tipopredio"] == 2 ? "selected='selected'" : "" ?> >MULTIFAMILIAR</option>
                                            </select>
                                        </td>
                                        <td width="12%" align="right">T. Construccion</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="44%">
                                            <?php $objMantenimiento->drop_tipo_construccion($items["codtipoconstruccion"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Pisos</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" class="inputtext"  id="nropisos" name="nropisos" maxlentgth="10" size="10" value="<?=isset($items["nropisos"]) ? $items["nropisos"] : 1 ?>" onkeyup="actualizacion(this, 1)" />
                                        </td>
                                        <td align="right">Abastecimiento</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_abastecimiento($items["codtipoabastecimiento"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Piscina</td>
                                        <td align="center">:</td>
                                        <td>
<?php
                                            if ($items['piscina'] == 1) {
                                                $ck1 = "checked=''";
                                                $ck2 = "";
                                            } else {
                                                $ck1 = "";
                                                $ck2 = "checked";
                                            }
?>
                                            <div class="buttonset" style="display:inline">
                                                <input type="radio" name="piscina" id="piscina1" value="1" <?php echo $ck1; ?> onChange="actualizacion(this, 1)"/>
                                                <label for="piscina1">Si</label>
                                                <input type="radio" name="piscina" id="piscina0" value="0" <?php echo $ck2; ?> onChange="actualizacion(this, 1)"/>
                                                <label for="piscina0">No</label>
                                            </div>
                                        </td>
                                        <td align="right">Almacenaje</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_almacenaje($items["codtipoalmacenaje"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tipo de Servicio</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tiposervicio($items["codtiposervicio"], "", 'onChange="actualizacion(this,0)"'); ?>
                                        </td>

                                        <td align="right">Tipo de Material</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipomatconstruccion($items["codtipomatconstruccion"], "", 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Pozo</td>
                                        <td align="center">:</td>
                                        <td>
<?php
                                            if ($items['conpozo'] == 1) {
                                                $ck1 = "checked=''";
                                                $ck2 = "";
                                            } else {
                                                $ck1 = "";
                                                $ck2 = "checked";
                                            }
?>
                                            <div class="buttonset" style="display:inline">
                                                <input type="radio" name="conpozo" id="conpozo1" value="1" <?php echo $ck1; ?> onchange="VImportePozo(1);
                                                        actualizacion(this, 1)" />
                                                <label for="conpozo1">Si</label>
                                                <input type="radio" name="conpozo" id="conpozo0" value="0" <?php echo $ck2; ?> onchange="VImportePozo(2);
                                                        actualizacion(this, 1)"/>
                                                <label for="conpozo0">No</label>
                                            </div>
                                        </td>
                                        <td align="right" class="cconpozoimporte">Importe Pozo</td>
                                        <td align="center" class="cconpozoimporte">:</td>
                                        <td class="cconpozoimporte">
                                            <input type="text" class="inputtext"  id="conpozoimporte" name="conpozoimporte" maxlentgth="10" size="10" value="<?=isset($items["conpozoimporte"]) ? $items["conpozoimporte"] : 0 ?>" onkeyup="actualizacion(this, 1)" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">Referencia</td>
                                        <td align="center" valign="top">:</td>
                                        <td colspan="4">
                                            <textarea name="referencia" id="referencia" cols="107" rows="5" class="inputtext" onkeyup="actualizacion(this, 1)"><?=$items["referencia"] ?></textarea>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-4" style="height:350px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="15%" align="right">Mat. Tubo</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%">
                                            <?php $objMantenimiento->drop_tipo_material_tubo_agua($items["codtipomaterialagua"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td width="18%" align="right">L. Caja</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="38%">
                                            <?php $objMantenimiento->drop_localizacion_caja_agua($items["codlocacajaagua"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Diametro Tubo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_diametros_agua($items["coddiametrosagua"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Pavimento</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_pavimento_agua($items["codtipopavimentoagua"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tipo de Corte</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_corte($items["codtipocorte"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Est. Conexion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_conexion($items["codestadoconexion"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Vereda</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_vereda($items["codtipovereda"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Fecha Instalacion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="fechainstagua" id="fechainstagua" class="inputtext" maxlentgth="6" value="<?=$fechainstagua ?>" onkeyup="actualizacion(this, 1)" onchange="actualizacion(this, 1)" style="width:80px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_caja_agua($items["codtipocajaagua"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Est. Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_caja_agua($items["codestadocajaagua"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tapa</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_tapa_agua($items["codtipotapaagu"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Est. Tapa</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="estadotapaagua" name="estadotapaagua" style="width: 220px" class="select" onChange="actualizacion(this, 1)" >
                                                <option value="1" <?=$items["estadotapaagu"] == 1 ? "selected='selected'" : "" ?> >EN BUEN ESTADO</option>
                                                <option value="2" <?=$items["estadotapaagu"] == 2 ? "selected='selected'" : "" ?> >EN MAL ESTADO</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Fugas</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_fugas_agua($items["codtipofugasagua"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-5" style="height:350px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="15%" align="right">Marca</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%">
                                            <?php $objMantenimiento->drop_marca_medidor($items["codmarca"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td width="18%" align="right">Diametro</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="38%">
                                            <?php $objMantenimiento->drop_diametros_medidor($items["coddiametrosmedidor"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Estado</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_medidor($items["codestadomedidor"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Posicion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="posicionmedidor" name="posicionmedidor" style="width: 220px" class="select" onChange="actualizacion(this, 1)">
                                                <option value="1" <?=$items["posicionmed"] == 1 ? "selected='selected'" : "" ?> >CORRECTA</option>
                                                <option value="2" <?=$items["posicionmed"] == 2 ? "selected='selected'" : "" ?> >INCORRECTA</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Modelo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_modelo_medidor($items["codmodelo"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Tipo de Lectura</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="tipolectura" name="tipolectura" style="width: 220px" class="select" onChange="actualizacion(this, 1)">
                                                <option value="1" <?=$items["tipolectura"] == 1 ? "selected='selected'" : "" ?> >CIRCULAR</option>
                                                <option value="2" <?=$items["tipolectura"] == 2 ? "selected='selected'" : "" ?> >DIRECTA</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">A&ntilde;o de Fabricacion</td>
                                        <td align="center">:</td>
                                        <td><input type="text" name="aniofab" id="aniofab" class="inputtext" maxlength="4" size="15" value="<?=$items["aniofabmed"] ?>" onkeypress="return permite(event, 'num');" onkeyup="actualizacion(this, 1)" /></td>
                                        <td align="right">Tipo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_medidor($items["codtipomedidor"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Fecha Instalacion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="fechainstmedidor" id="fechainstmedidor" class="inputtext" maxlentgth="6" value="<?=$fechainstmedidor ?>" onchange="actualizacion(this, 1)" onkeyup="actualizacion(this, 1)" style="width:80px;" />
                                        </td>
                                        <td align="right">Capacidad</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_capacidad_medidor($items["codcapacidadmedidor"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Nro. Medidor</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="nromedidor" id="nromedidor" class="inputtext" size="15" value="<?=trim($items["nromed"]) ?>"  onkeyup="actualizacion(this, 0)" />
                                        </td>
                                        <td align="right">Tipo de Facturacion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="tipofacturacion" name="tipofacturacion" style="width:220px" class="select" onChange="actualizacion(this, 0)">
                                                <option value="100" >--Seleccione el Tipo--</option>
                                                <option value="0" <?=$items["tipofacturacion"] == 0 ? "selected='selected'" : "" ?> >CONSUMO LEIDO</option>
                                                <option value="1" <?=$items["tipofacturacion"] == 1 ? "selected='selected'" : "" ?> >CONSUMO PROMEDIADO</option>
                                                <option value="2" <?=$items["tipofacturacion"] == 2 ? "selected='selected'" : "" ?> >CONSUMO ASIGNADO</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Ub. Llave</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_ubicacion_llave_medidor($items["codubicacion"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Lectura Anterior</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="lecturaanterior" id="lecturaanterior" class="inputtext" maxlentgth="6" size="15" value="<?=intval($items["lecturaanterior"]) ?>" onkeyup="calcularconsumo(document.getElementById('lecturaultima').value);
                                                    actualizacion(this, 0);"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Fecha Lect. Anterior</td>
                                        <td align="center">:</td>
                                        <td><input type="text" name="fechalecturaanterior" id="fechalecturaanterior" onchange="actualizacion(this, 0)" onkeyup="actualizacion(this, 0)" class="inputtext" maxlentgth="6" value="<?=$fechalectanterior ?>" style="width:70px;" /></td>
                                        <td align="right">Lectura Ultima</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="lecturaultima" id="lecturaultima" class="inputtext" maxlentgth="6" size="15" value="<?=intval($items["lecturaultima"]) ?>" onkeyup="calcularconsumo(this.value);
                                                    actualizacion(this, 0);"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Fecha Lect. Ultima</td>
                                        <td align="center">:</td>
                                        <td><input type="text" name="fechalecturaultima" id="fechalecturaultima" class="inputtext" maxlentgth="6" value="<?=$fechalectultima ?>" onchange="actualizacion(this, 1)" onkeyup="actualizacion(this, 1)" style="width:70px;" /></td>
                                        <td align="right">Consumo</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="consumo" id="consumo" class="inputtext" maxlentgth="6" size="15" value="<?=intval($items["consumo"]) ?>" onkeyup="actualizacion(this, 0)"  />
                                            <span class="icono-icon-info" title="Asignar Consumos" onclick="VerTarifas()" id="imgAsignarC"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="right">Promedio</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="lecturapromedio" id="lecturapromedio" class="inputtext" maxlentgth="6" size="15" value="<?=intval($items["lecturapromedio"]) ?>" onkeyup="actualizacion(this, 0)" />
                                            <span class="icono-icon-info" title="Promediar Lecturas" onclick="promediar_lecturas()" id="imgPromediarL"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>
                                        	<span class="CampoDetalle">
                                                <select name="catetar" id="catetar" style="display:none" >
<?php
                                                    $SqlU = "select DISTINCT catetar, nomtar FROM facturacion.tarifas WHERE codsuc=1 ORDER BY catetar";
                                                    $Consulta = $conexion->query($SqlU);

                                                    foreach ($Consulta->fetchAll() as $RowU)
													{
?>
                                                        <option value="<?php echo $RowU[0]; ?>" <?php
                                                        if ($items['catetar'] == $RowU[0]) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>><?php echo $RowU[1]; ?></option>
<?php
													}
?>
                                                </select>
                                            </span>
										</td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-6" style="height:350px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="15%" align="right">Diametro</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%"><?php $objMantenimiento->drop_diametros_desague($items["coddiametrosdesague"], 'onChange="actualizacion(this,1)"'); ?></td>
                                        <td width="18%" align="right">Mat. Tubo</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="38%"><?php $objMantenimiento->drop_tipo_material_tubo_desague($items["codtipomaterialdesague"], 'onChange="actualizacion(this,1)"'); ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Localizacion Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_localizacion_caja_desague($items["codlocacajadesague"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Fecha Instalacion</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="fechainstdesague" id="fechainstdesague" class="inputtext" maxlentgth="6" value="<?=$fechainstdesague ?>" onchange="actualizacion(this, 1)" onkeyup="actualizacion(this, 1)" style="width:80px;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tipo de Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_caja_desague($items["codtipocajadesague"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Estado de la Caja</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_estado_caja_desague($items["codestadocajadesague"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Tipo de Tapa</td>
                                        <td align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tipo_tapa_desague($items["codtipotapadesague"], 'onChange="actualizacion(this,1)"'); ?>
                                        </td>
                                        <td align="right">Estado de Tapa</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="estadotapadesague" name="estadotapadesague" style="width: 220px" class="select"  onChange="actualizacion(this, 1)">
                                                <option value="1" <?=$items["estadotapadesague"] == 1 ? "selected='selected'" : "" ?> >EN BUEN ESTADO</option>
                                                <option value="2" <?=$items["estadotapadesague"] == 2 ? "selected='selected'" : "" ?> >EN MAL ESTADO</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Fugas y Atoros</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id="fugasyatoros" name="fugasyatoros" style="width: 220px" class="select" onChange="actualizacion(this, 1)">
                                                <option value="1" <?=$items["fugasatoros"] == 1 ? "selected='selected'" : "" ?> >NO HAY</option>
                                                <option value="2" <?=$items["fugasatoros"] == 1 ? "selected='selected'" : "" ?> >SI HAY</option>
                                            </select>
                                        </td>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">
<?php
                                            $checked = "";
											
                                            if (isset($items["exoneralac"])) {
                                                if ($items["exoneralac"] == 1) {
                                                    $checked = "checked='checked'";
                                                }
                                            }
?>
                                            <input type="checkbox" name="desagueinactivo" id="desagueinactivo" <?=$checked ?> onclick="CambiarEstado(this, 'desagueinactivotext');
                                                    validar_desagueinactivo();" />
                                        </td>
                                        <td>
                                            Servicio Inactivo
                                            <input type="hidden" name="desagueinactivotext" id="desagueinactivotext" value="<?=isset($items["exoneralac"]) ? $items["exoneralac"] : 0 ?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="right">&nbsp;</td>
                                        <td align="center">
<?php
                                            $checked = "";
											
                                            if (isset($items["vma"])) {
                                                if ($items["vma"] == 1) {
                                                    $checked = "checked='checked'";
                                                }
                                            }
?>
                                            <input type="checkbox" name="vma" id="vma" <?=$checked ?> onclick="CambiarEstadoVMA(this, 'vma');" disabled="disabled" />
                                        </td>
                                        <td>Valores Máximos Admisibles</td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-7" style="height:350px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr valign="top">&nbsp;</tr>
                                    <tr>
                                        <td width="37%" align="right">Zona de abastecimiento</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="23%">
                                            <div id="div_zonas">
                                                <?php $objMantenimiento->drop_zonas_abastecimiento_sector($codsuc, 0) ?>
                                            </div>
                                        </td>
                                        <td width="16%" align="right">Presion del Agua</td>
                                        <td width="2%" align="center">:</td>
                                        <td width="25%">
                                            <select id="presionagua" name="presionagua" style="width: 180px" class="select" onchange="actualizacion(this, 1)">
                                                <option value="0" >--Seleccione el Tipo--</option>
                                                <option value="1" <?=$items["presionagua"] == 1 ? "selected='selected'" : "" ?> >BAJA</option>
                                                <option value="2" <?=$items["presionagua"] == 2 ? "selected='selected'" : "" ?> >NORMAL</option>
                                                <option value="3" <?=$items["presionagua"] == 3 ? "selected='selected'" : "" ?> >ALTA</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="14%" align="right">Horas Abast. </td>
                                                    <td width="4%" align="center">:</td>
                                                    <td width="9%">
<?php
                                                        $abastecimiento = $items["horasabastecimiento"];
														
                                                        if (isset($items["horasabastecimiento"]))
														{
                                                            if ($items["horasabastecimiento"] != 0)
															{
                                                                $abastecimiento = $items["horasabastecimiento"];
                                                            }
                                                        }
?>
                                                        <input type="text" class="inputtext" name="horasabastecimiento" id="horasabastecimiento" maxlenght="10" size="10" value="<?=$abastecimiento ?>" onkeyup="actualizacion(this, 1)" />
													</td>
                                                    <td width="15%" align="right">Nro. Lavatorios</td>
                                                    <td width="4%" align="center">:</td>
                                                    <td width="9%">
<?php
                                                        $nrolavatorios = "";
														
                                                        if (isset($items["nrolavatorios"]))
														{
                                                            if ($items["nrolavatorios"] != 0)
															{
                                                                $nrolavatorios = $items["nrolavatorios"];
                                                            }
                                                        }
?>
                                                        <input type="text" class="inputtext" name="nrolavatorios" id="nrolavatorios" maxlenght="10" size="10" value="<?=$nrolavatorios ?>" onkeyup="actualizacion(this, 1)" />
													</td>
                                                    <td width="18%" align="right">Nro. Lavaropas</td>
                                                    <td width="4%" align="center">:</td>
                                                    <td width="23%">
<?php
                                                        $nrolavaropas = "";
														
                                                        if (isset($items["nrolavaropas"]))
														{
                                                            if ($items["nrolavaropas"] != 0)
															{
                                                                $nrolavaropas = $items["nrolavaropas"];
                                                            }
                                                        }
?>
                                                        <input type="text" class="inputtext" name="nrolavaropas" id="nrolavaropas" maxlenght="10" size="10" value="<?=$nrolavaropas ?>"  onkeyup="actualizacion(this, 1)"/>
													</td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Nro. Water</td>
                                                    <td align="center">:</td>
                                                    <td>
<?php
                                                        $nrowater = "";
														
                                                        if (isset($items["nrowater"]))
														{
                                                            if ($items["nrowater"] != 0)
															{
                                                                $nrowater = $items["nrowater"];
                                                            }
                                                        }
?>
                                                        <input type="text" class="inputtext" name="nrowater" id="nrowater" maxlenght="10" size="10" value="<?=$nrowater ?>" onkeyup="actualizacion(this, 1)" />
													</td>
                                                    <td align="right">Nro. Duchas</td>
                                                    <td align="center">:</td>
                                                    <td>
<?php
                                                        $nroduchas = "";
														
                                                        if (isset($items["nroduchas"]))
														{
                                                            if ($items["nroduchas"] != 0)
															{
                                                                $nroduchas = $items["nroduchas"];
                                                            }
                                                        }
?>
                                                        <input type="text" class="inputtext" name="nroduchas" id="nroduchas" maxlenght="10" size="10" value="<?=$nrowater ?>" onkeyup="actualizacion(this, 1)" />
													</td>
                                                    <td align="right">Nro. Urinarios</td>
                                                    <td align="center">:</td>
                                                    <td>
<?php
                                                        $nrourinarios = "";
														
                                                        if (isset($items["nrourinarios"]))
														{
                                                            if ($items["nrourinarios"] != 0)
															{
                                                                $nrourinarios = $items["nrourinarios"];
                                                            }
                                                        }
?>
                                                        <input type="text" class="inputtext" name="nrourinarios" id="nrourinarios" maxlenght="10" size="10" value="<?=$nrourinarios ?>" onkeyup="actualizacion(this, 1)" />
													</td>
                                                </tr>
                                                <tr>
                                                    <td align="right">Nro. Grifos</td>
                                                    <td align="center">:</td>
                                                    <td>
<?php
                                                        $nrogrifos = "";
														
                                                        if (isset($items["nrogrifos"]))
														{
                                                            if ($items["nrogrifos"] != 0)
															{
                                                                $nrogrifos = $items["nrogrifos"];
                                                            }
                                                        }
?>
                                                        <input type="text" class="inputtext" name="nrogrifos" id="nrogrifos" maxlenght="10" size="10" value="<?=$nrogrifos ?>"  onkeyup="actualizacion(this, 1)"/>
													</td>
                                                    <td align="right">Nro. Habitantes</td>
                                                    <td align="center">:</td>
                                                    <td>
<?php
                                                        $nrohabitantes = "";
														
                                                        if (isset($items["nrohabitantes"]))
														{
                                                            if ($items["nrohabitantes"] != 0)
															{
                                                                $nrohabitantes = $items["nrohabitantes"];
                                                            }
                                                        }
?>
                                                        <input type="text" class="inputtext" name="nrohabitantes" id="nrohabitantes" maxlenght="10" size="10" value="<?=$nrohabitantes ?>" onkeyup="actualizacion(this, 1)" />
													</td>
                                                    <td align="right">&nbsp;</td>
                                                    <td align="center">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
										</td>
                                    </tr>
                                    <tr valign="top">
                                        <td align="right">Referencia</td>
                                        <td align="center">:</td>
                                        <td colspan="4"><textarea name="observacion" id="observacion" cols="90" rows="10" class="inputtext" style="font-size:12px" onkeyup="actualizacion(this, 1)" ><?=$items["observacionabast"] ?></textarea></td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-8" style="height:350px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="trUnidUso">
                                        <td width="8%">&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>
                                        <td width="69%">&nbsp;</td>
                                    </tr>
                                    <tr class="trUnidUso">
                                        <td>&nbsp;</td>
                                        <td width="20%" align="right">Categoria</td>
                                        <td width="3%" align="center">:</td>
                                        <td>
                                            <?php $objMantenimiento->drop_tarifas($codsuc); ?>
                                            <input type="checkbox" name="principal_categoria_check" id="principal_categoria_check" onclick="CambiarEstado(this, 'principal_categoria')"  />
                                            Categoria Principal
                                            <input type="hidden" name="principal_categoria" id="principal_categoria" value="0" />
										</td>
                                    </tr>
                                    <tr class="trUnidUso">
                                        <td>&nbsp;</td>
                                        <td align="right">Actividad</td>
                                        <td align="center">:</td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div id="div_rutasdistribucion_2">
                                                            <?php $objMantenimiento->drop_tipo_actividad($items["codtipoactividad"]); ?>
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <span class="MljSoft-icon-buscar" title="Buscar Actividad" onclick="buscar_actividad();"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="trUnidUso">
                                        <td>&nbsp;</td>
                                        <td align="right">Porcentaje</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="procentaje" id="procentaje" class="inputtext" onkeypress="return permite(event, 'num')" value="100" />
                                            <input type="text" name="categoria_veces" id="categoria_veces" class="inputtext" style="width: 50px;"/>
                                            <input type="checkbox" style="margin-right: 10px; margin-left: 10px;" name="principal_categoria_veces_check" id="principal_categoria_veces_check" checked="checked" onclick="Habilitar(this, 'principal_categoria_veces');"  />
                                            <img id="porimg" src="../../../../images/iconos/add.png" style="cursor: pointer" onclick="agregar_unidades_uso(<?=$codsuc ?>);
                                                    actualizacion(this, 0);" />
                                            <input type="hidden" name="principal_categoria_veces" id="principal_categoria_veces" value="1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <input name="cont_unidades" id="cont_unidades" type="hidden" value="0" />
                                            <input type="hidden" name="total_porct" id="total_porct" value="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div style="overflow:auto; height: 200px">
                                                <table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tbunidades" >
                                                    <thead class="ui-widget-header" style="font-size: 11px">
                                                        <tr>
                                                            <th width="50">Item</th>
                                                            <th width="50">Codigo</th>
                                                            <th width="150">Categoria</th>
                                                            <th>Actividad</th>
                                                            <th width="80">Porcentaje</th>
                                                            <th width="60">Principal</th>
                                                            <th width="40">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
<?php
                                                        $cont_unidades = 0;
                                                        $tot_porcentaje = 0;

                                                        if ($Id != "")
														{
                                                            $sql_unidades = "SELECT u.catetar, t.nomtar, u.porcentaje, u.principal,
                                                                    a.descripcion AS actividad, a.codtipoactividad
                                                                FROM catastro.unidadesusoclientes u
                                                                INNER JOIN facturacion.tarifas t ON(u.codemp = t.codemp AND u.codsuc = t.codsuc AND u.catetar = t.catetar)
                                                                INNER JOIN public.tipoactividad a ON(u.codtipoactividad = a.codtipoactividad)
                                                                WHERE u.codemp = 1 AND u.codsuc = ? AND u.nroinscripcion = ? AND t.estado = 1";

                                                            $consulta_unidades = $conexion->prepare($sql_unidades);
                                                            $consulta_unidades->execute(array($codsuc, $Id));
                                                            $items_unidad = $consulta_unidades->fetchAll();

                                                            foreach ($items_unidad as $rowUnidades) {
                                                                $cont_unidades++;

                                                                $tot_porcentaje += $rowUnidades["porcentaje"];
                                                                ?>
                                                                <tr style='background-color:#FFFFFF; padding:4px'>
                                                                    <td align="center" >
                                                                        <?=$cont_unidades ?>
                                                                    </td>
                                                                    <td align="center" >
                                                                        <input type="hidden" name="ItemD<?=$cont_unidades ?>" id="ItemD<?=$cont_unidades ?>" value="<?=$cont_unidades ?>" />
                                                                        <input type='hidden' id='tarifas<?=$cont_unidades ?>' name='tarifas<?=$cont_unidades ?>' value='<?=$rowUnidades["catetar"] ?>' />
                                                                        <?=$rowUnidades["catetar"] ?>
                                                                    </td>

                                                                    <td style="padding-left:5px;" >
                                                                        <input type='hidden' id='porcentaje<?=$cont_unidades ?>' name='porcentaje<?=$cont_unidades ?>' value='<?=$rowUnidades["porcentaje"] ?>' />
                                                                        <?=strtoupper($rowUnidades["nomtar"]) ?>
                                                                    </td>
                                                                    <td align="center" >
                                                                        <input type="hidden" name="codtipoactividad<?=$cont_unidades ?>" id="codtipoactividad<?=$cont_unidades ?>" value="<?=$rowUnidades["codtipoactividad"] ?>" />
                                                                        <?=$rowUnidades["actividad"] ?>
                                                                    </td>
                                                                    <td align="center" ><?=number_format($rowUnidades["porcentaje"], 1) ?></td>
                                                                    <td align="center" >
                                                                        <input type='hidden' class='principal' id='principal<?=$cont_unidades ?>' name='principal<?=$cont_unidades ?>' value='<?=$rowUnidades["principal"] ?>' />
                                                                        <?php //$rowUnidades["principal"]==1?"SI":"NO" ?>
                                                                        <input type="radio" name="principalUU" id="principalUU<?=$cont_unidades ?>" value="<?=$rowUnidades["codunidad_uso"] ?>") <?php
                                                                        if ($rowUnidades["principal"] == 1) {
                                                                            echo 'checked="checked"';
                                                                        }
                                                                        ?> onChange='Numerar();
                                                                                actualizacion(this, 3);' style="cursor:pointer" />
                                                                    </td>
                                                                    <td align="center" >
                                                                        <img src='../../../../images/iconos/cancel.png' width='16' height='16' title='Borrar Registro' onclick='QuitaFilaD(this, 1,<?=$cont_unidades ?>);' style='cursor:pointer' class="imgQuitarUU"/>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <script>
                                                        var cont_unidad = <?=$cont_unidades ?>;
                                                        var cont_fila_unidad = <?=$cont_unidades ?>;
                                                        var total_porcentaje = <?=$tot_porcentaje ?>;

                                                        $("#cont_unidades").val(<?=$cont_unidades ?>)
                                                        $("#total_porct").val(<?=$tot_porcentaje ?>)

                                                        $("#procentaje").val(100 - <?=$tot_porcentaje ?>)
                                                    </script>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="hidden" name="contmod" id="contmod" value="0" />&nbsp;
                    </td>
                </tr>
            </tbody>

        </table>
        <div id="DivLecturas" title="Actualizacion y Modificacion Catastral"  >
            <div id="div_lecturas"></div>
        </div>
    </form>
</div>
<div id="DivTarifas" title="Tarifas" style="display: none;">
    <div id="DivTarifasAcordion">
        <?php
        $sql = "SELECT * FROM facturacion.tarifas WHERE codsuc = " . $codsuc . " ORDER BY catetar";
        $ConsultaTar = $conexion->query($sql);
        foreach ($ConsultaTar->fetchAll() as $tarifas) {
            ?>
            <h3><?=strtoupper($tarifas["nomtar"]) ?></h3>
            <div style="height: 100px; display: block;">
                <table width="100%" border="0" align="center" cellspacing="0" class="ui-widget" rules="cols">
                    <thead class="ui-widget-header" style="font-size:10px">
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center">Volumen</td>
                            <td align="center">Importe Agua</td>
                            <td align="center">Importe Desague</td>
                            <td align="center">&nbsp;</td>
                        </tr>
                    </thead>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Inicial</td>
                        <td align="right">
                            <label class="inputtext" id="hastarango1" ><?=number_format($tarifas["hastarango1"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmin" ><?=$tarifas["impconsmin"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmindesa"><?=$tarifas["impconsmindesa"] ?></label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango1"], 0) ?>')"></span></td>
                    </tr>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Intermedio </td>
                        <td align="right">
                            <label class="inputtext" id="hastarango2"><?=number_format($tarifas["hastarango2"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmedio"><?=$tarifas["impconsmedio"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsmediodesa"><?=$tarifas["impconsmediodesa"] ?> </label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango2"], 0) ?>')"></span></td>
                    </tr>
                    <tr>
                        <td align="right" class="ui-widget-header" style="font-size:10px">Rango Final </td>
                        <td align="right">
                            <label class="inputtext" id="hastarango3"><?=number_format($tarifas["hastarango3"], 2) ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsexec"><?=$tarifas["impconsexec"] ?></label>
                        </td>
                        <td align="right">
                            <label class="inputtext" id="impconsexecdesa"><?=$tarifas["impconsexecdesa"] ?></label>
                        </td>
                        <td align="center"><span class="icono-icon-add" title="Agregar" onclick="AddTarifa('<?=number_format($tarifas["hastarango3"], 0) ?>')"></span></td>
                    </tr>
                </table>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
<?php
	if ($Id != "")
	{
?>
		cargar_sectores(<?=$codsuc ?>, <?=$items["codzona"] ?>, '<?=$items["codsector"] ?>', 1);
		cargar_manzana(<?=$codsuc ?>, <?=$items["codsector"] ?>, '<?=$items["codmanzanas"] ?>', 1);
		cargar_calles(<?=$items["codsector"] ?>, <?=$codsuc ?>, <?=$items["codzona"] ?>, '<?=$items["codcalle"] ?>');
		cargar_zonas_abastecimiento(<?=$items["codsector"] ?>, '<?=$items["codzonaabas"] ?>', 0);
		//cargar_horarios_zonas(<?=$items["codzona"] ?>, <?=$items["codsector"] ?>);
		cargar_rutas_lecturas('<?=$items["codmanzanas"] ?>', '<?=$items["codsector"] ?>');
		cargar_rutas_distribucion('<?=$items["codmanzanas"] ?>', '<?=$items["codsector"] ?>');
		cargar_urbanizacion(<?=$codsuc ?>, <?=$items["codzona"] ?>, '<?=$items["codurbanizacion"] ?>', 1);
<?php
	}
?>
    $('#categoria_veces').attr('readonly', 'readonly');
    //setTimeout(validar_cod_catastro(),1000)
    setTimeout(BloquearTodo(), 2000);
</script>
