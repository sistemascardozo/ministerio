<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("clsactualizacion.php");
    include('../../../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();
    $codtultimo = 0;

    $codemp = 1;
    $cont = 1;
    $nItems = $_SESSION["oactualizacion"]->item;
    $idusuario = $_SESSION['id_user'];
    $codsuc = $_SESSION['IdSucursal'];
	$zona = $_POST["codzona"];
    $observacion = $_POST["obs"];
    $flagnuevotext = $_POST["flagnuevotext"];
    $nroinscripcion = $_POST["nroinscripcion"];
    $codcliente = $_POST["codcliente"];
    $sector = $_POST["sector"];
    $manzanas = $_POST["manzanas"];
    $lote = $_POST["lote"];
    $sublote = $_POST["sublote"];
    $nroorden = $_POST["nroorden"];

    if ($nroorden == "")
    {
		$nroorden = 0;
		}

    $estadoservicio = $_POST["estadoservicio"];
    $tiporesponsable = $_POST["tiporesponsable"];
    $propietario = strtoupper($_POST["propietario"]);
    $tipodocumento = $_POST["tipodocumento"];
    $altoconsumidortext = $_POST["altoconsumidortext"];
    $nrodocumento = $_POST["nrodocumento"];
    $calles = $_POST["calles"];
    $nrocalle = $_POST["nrocalle"];
    $tipoentidades = $_POST["tipoentidades"];
    $tipousuario = $_POST["tipousuario"];
    $codrutdistribucion = $_POST["codrutdistribucion"];
    $codrutlectura = $_POST["codrutlectura"];
    $dirdistribucion = $_POST["dirdistribucion"];
    $inquilino = $_POST["inquilino"];
    $tipoactividad = $_POST["tipoactividad"];
    $ciclo = $_POST["ciclo"];
    $tipopredio = $_POST["tipopredio"];
    $tipoconstruccion = $_POST["tipoconstruccion"];
    $nropisos = $_POST["nropisos"];
    $tipoabastecimiento = $_POST["tipoabastecimiento"];
    $piscina = $_POST["piscina"];
    $tipoalmacenaje = $_POST["tipoalmacenaje"];
    $tiposervicio = $_POST["tiposervicio"];
    $tipomatconstruccion = $_POST["tipomatconstruccion"];
    $referencia = $_POST["referencia"];
    $tipomaterialagua = $_POST["tipomaterialagua"];
    $locacajaagua = $_POST["locacajaagua"];
    $diametrosagua = $_POST["diametrosagua"];
    $pavimentoagua = $_POST["pavimentoagua"];
    $tipocorte = $_POST["tipocorte"];
    $estadoconexion = $_POST["estadoconexion"];
    $tipovereda = $_POST["tipovereda"];
    $fechainstagua = $objFunciones->CodFecha($_POST["fechainstagua"]);
    $tipocajaagua = $_POST["tipocajaagua"];
    $estadocajaagua = $_POST["estadocajaagua"];
    $tipotapaagua = $_POST["tipotapaagua"];
    $estadotapaagua = $_POST["estadotapaagua"];
    $tipofugasagua = $_POST["tipofugasagua"];
    $marcamedidor = $_POST["marcamedidor"];
    $diametrosmedidor = $_POST["diametrosmedidor"];
    $estadomedidor = $_POST["estadomedidor"];
    $posicionmedidor = $_POST["posicionmedidor"];
    $modelomedidor = $_POST["modelomedidor"];
    $tipolectura = $_POST["tipolectura"];
    $aniofab = $_POST["aniofab"];
    $tipomedidor = $_POST["tipomedidor"];
    $codurbanizacion = $_POST["codurbanizacion"];

    $fechainstmedidor = $objFunciones->CodFecha($_POST["fechainstmedidor"]);

    if ($fechainstmedidor == '')
	{
        $fechainstmedidor = null;
    }

    $capacidadmedidor = $_POST["capacidadmedidor"];
    $nromedidor = trim($_POST["nromedidor"]);
    $tipofacturacion = $_POST["tipofacturacion"];
    $ubicacionllavemedidor = $_POST["ubicacionllavemedidor"];
    $lecturaanterior = $_POST["lecturaanterior"];
    $fechalecturaanterior = $objFunciones->CodFecha($_POST["fechalecturaanterior"]);
    $lecturaultima = $_POST["lecturaultima"];
    $fechalecturaultima = $objFunciones->CodFecha($_POST["fechalecturaultima"]);
    $consumo = $_POST["consumo"];
    $diametrosdesague = $_POST["diametrosdesague"];
    $tipomaterialdesague = $_POST["tipomaterialdesague"];
    $locacajadesague = $_POST["locacajadesague"];
    $fechainstdesague = $objFunciones->CodFecha($_POST["fechainstdesague"]);
    $tipocajadesague = $_POST["tipocajadesague"];
    $estadocajadesague = $_POST["estadocajadesague"];
    $tipotapadesague = $_POST["tipotapadesague"];
    $estadotapadesague = $_POST["estadotapadesague"];
    $desagueinactivotext = $_POST["desagueinactivotext"];
    $zonasbastecimiento = $_POST["zonasbastecimiento"];
    $presionagua = $_POST["presionagua"];
    $horasabastecimiento = $_POST["horasabastecimiento"];
    $nrolavatorios = $_POST["nrolavatorios"];
    $nrolavaropas = $_POST["nrolavaropas"];
    $nrowater = $_POST["nrowater"];
    $nroduchas = $_POST["nroduchas"];
    $nrourinarios = $_POST["nrourinarios"];
    $nrogrifos = $_POST["nrogrifos"];
    $nrohabitantes = $_POST["nrohabitantes"];
    $fugasyatoros = $_POST["fugasyatoros"];
    $observacionabast = $_POST["observacion"];
    $latitud = $_POST["latitud"];
    $longitud = $_POST["longitud"];
    $correo = $_POST["correo"];
    $lecturapromedio = $_POST["lecturapromedio"];
    $conpozo = $_POST["conpozo"] ? $_POST["conpozo"] : 0;
    $conpozoimporte = $_POST["conpozoimporte"] ? $_POST["conpozoimporte"] : 0;

	$OrdenLect = $_POST["orden_lect"];
    $OrdenDist = $_POST["orden_dist"];

    if ($lecturapromedio == '')
        $lecturapromedio = 0;
    $correlativo = 0;
    if (trim($consumo) == "")
        $consumo = 0;
    $consultaM = $conexion->prepare("SELECT max(nromodificaciones) FROM catastro.modificaciones WHERE codsuc=".$codsuc);
    $consultaM->execute();
    if ($rowM = $consultaM->fetch()) {
        $correlativo = $rowM[0];
    }

    $campoold = "";
    $valorold = "";
    $entro = 0;

    if ($flagnuevotext == 1) {
        $codcliente = $objFunciones->SETCorrelativosVarios(1, $codsuc, "SELECT", 0);
        $n = $objFunciones->SETCorrelativosVarios(1, $codsuc, "UPDATE", $codcliente);
    }

    for ($i = 0; $i < $nItems; $i++) {
        $campo = $_SESSION["oactualizacion"]->campo[$i];
        $valor = $_SESSION["oactualizacion"]->valor[$i];
        $tipo  = $_SESSION["oactualizacion"]->tipo[$i];

        if ($campo == "catetar") {

            $consul = $conexion->prepare("SELECT uc.catetar as codtarifa, t.nomtar as tarifa,
                uc.porcentaje as porcentaje, uc.principal as estado
                FROM catastro.unidadesusoclientes uc
                INNER JOIN facturacion.tarifas t ON (uc.catetar = t.catetar AND uc.codemp = t.codemp AND uc.codsuc = t.codsuc)
                WHERE uc.codsuc=? AND uc.nroinscripcion=?");
            $consul->execute(array($codsuc, $nroinscripcion));
            $rows = $consul->fetchAll();
            $cont = 1;

            foreach ($rows as $key) {

                if ($cont == 1) {
                    $valorultimo = substr($key['tarifa'], 0, 3);
                    $valorultimo .= "(".$key['porcentaje']."-".$key['estado'].")";
                    $cont++;
                } else {
                    $valorultimo .= "-".substr($key['tarifa'], 0, 3);
                    $valorultimo .= "(".$key['porcentaje']."-".$key['estado'].")";
                }
            }
            $codtultimo = 1;
            continue;
        }

        //die($tipo);
        if ($tipo == 0) {
            $sql = "SELECT ".$campo." FROM catastro.clientes WHERE codemp=? and codsuc=? and nroinscripcion=?";
        } else {
            $sql = "SELECT ".$campo." FROM catastro.conexiones WHERE codemp=? and codsuc=? and nroinscripcion=?";
        }

        $consultaF = $conexion->prepare($sql);
        $consultaF->execute(array($codemp, $codsuc, $nroinscripcion));
        $items = $consultaF->fetch();
        $campoold = $campo;
        $valorold = $items[0];

        $correlativo++;

        $instModificaciones = "INSERT INTO catastro.modificaciones(codemp,codsuc,nroinscripcion,
            nromodificaciones,campo, valoractual,valorultimo,observacion,creador)
            VALUES( ".$codemp.", ".$codsuc.", '".$nroinscripcion."', ".$correlativo.", '".$campoold."', '".$valorold."', '".$valor."', '".$observacion."', ".$idusuario." )";
        $result = $conexion->query($instModificaciones);
        /*$result->execute(array(":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":nroinscripcion" => $nroinscripcion,
            ":nromodificaciones" => $correlativo,
            ":campo" => $campoold,
            ":valoractual" => $valorold,
            ":valorultimo" => $valor,
            ":observacion" => $observacion,
            ":creador" => $idusuario));*/

        $campo = $_SESSION["oactualizacion"]->campo[$i];
    }

    //Cambio de Categoria y Actividad
    $SqlTA = "SELECT codtipoactividad, catetar FROM catastro.clientes ";
    $SqlTA .= "WHERE codemp=1 and codsuc=".$codsuc." and nroinscripcion=".$nroinscripcion."";

    $resultTA = $conexion->prepare($SqlTA);
    $resultTA->execute(array());
    $itemsTA = $resultTA->fetch();

    $tipoactividadA = $itemsTA[0];
    $catetarA = $itemsTA[1];

    //die();
	$updC = "UPDATE catastro.clientes ";
	$updC .= "SET codcliente = ".$codcliente.", ";
	$updC .= " codzona = ".$zona.", codsector = ".$sector.", codmanzanas = ".$manzanas.", lote = ".$lote.", sublote = ".$sublote.", nroorden = ".$nroorden.", ";
	$updC .= " propietario = '".$propietario."', codcalle = ".$calles.", nrocalle = '".$nrocalle."', direcciondistribucion = '".$dirdistribucion."', ";
	$updC .= " codtipodocumento = ".$tipodocumento.", nrodocumento = '".$nrodocumento."', correo = '".$correo."', ";
	$updC .= " codtiposervicio = ".$tiposervicio.", codestadoservicio = ".$estadoservicio.", codtipousuario = ".$tipousuario.", ";
	$updC .= " codtipoactividad = ".$tipoactividad.", tipofacturacion = ".$tipofacturacion.", codciclo = ".$ciclo.", ";
	$updC .= " nromed = '".$nromedidor."', codestadomedidor = ".$estadomedidor.", ";
	$updC .= " codrutlecturas = ".$codrutlectura.", codrutdistribucion = ".$codrutdistribucion.", ";
	$updC .= " lecturaultima = '".$lecturaultima."', fechalecturaultima = '".$fechalecturaultima."', ";
	$updC .= " lecturaanterior = '".$lecturaanterior."', fechalecturaanterior = '".$fechalecturaanterior."', lecturapromedio = '".$lecturapromedio."', ";
	$updC .= " exoneralac = ".$desagueinactivotext.", consumo = '".$consumo."', latitud = ".$longitud.", longitud = ".$longitud.", ";
	$updC .= " digitado = 0, conpozo = ".$conpozo.", codurbanizacion = ".$codurbanizacion." , conpozoimporte = '".$conpozoimporte."', codusu = ".$idusuario." ";
	$updC .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND nroinscripcion = '".$nroinscripcion."' ";

    $result = $conexion->query($updC);

    /* var_dump($resultC->errorInfo())."<br>";
      die(); */
    $updConx = "UPDATE catastro.conexiones SET
        referencia=:referencia,tipopredio=:tipopredio,codtipoconstruccion=:codtipoconstruccion,nropisos=:nropisos,
        codtipoabastecimiento=:codtipoabastecimiento,piscina=:piscina,codtipoalmacenaje=:codtipoalmacenaje,codtiporesponsable=:codtiporesponsable,
        inquilino=:inquilino,altocon=:altocon,codtipoentidades=:codtipoentidades,coddiametrosagua=:coddiametrosagua,codtipomaterialagua=:codtipomaterialagua,
        codlocacajaagua=:codlocacajaagua,codtipopavimentoagua=:codtipopavimentoagua,codtipocorte=:codtipocorte,codestadoconexion=:codestadoconexion,
        codestadocajaagua=:codestadocajaagua,codubicacion=:codubicacion,codtipovereda=:codtipovereda,fechainsagua=:fechainsagua,codtipofugasagua=:codtipofugasagua,
        codtipocajaagua=:codtipocajaagua,codtipotapaagu=:codtipotapaagu,estadotapaagu=:estadotapaagu,codmarca=:codmarca,coddiametrosmedidor=:coddiametrosmedidor,
        codestadomedidor=:codestadomedidor,posicionmed=:posicionmed,tipolectura=:tipolectura,fechainsmed=:fechainsmed,codmodelo=:codmodelo,aniofabmed=:aniofabmed,
        codtipomedidor=:codtipomedidor,codcapacidadmedidor=:codcapacidadmedidor,coddiametrosdesague=:coddiametrosdesague,codtipomaterialdesague=:codtipomaterialdesague,
        codlocacajadesague=:codlocacajadesague,fechainsdesague=:fechainsdesague,codtipocajadesague=:codtipocajadesague,codestadocajadesague=:codestadocajadesague,
        codtipotapadesague=:codtipotapadesague,estadotapadesague=:estadotapadesague,fugasatoros=:fugasatoros,presionagua=:presionagua,
        horasabastecimiento=:horasabastecimiento,nrohabitantes=:nrohabitantes,observacionabast=:observacionabast,codzonaabas=:codzonaabas,nrolavatorios=:nrolavatorios,
        nrolavaropas=:nrolavaropas,nrowater=:nrowater,nroduchas=:nroduchas,nrourinarios=:nrourinarios,nrogrifos=:nrogrifos,codtipomatconstruccion=:codtipomatconstruccion,
        conpozo=:conpozo,conpozoimporte=:conpozoimporte, codurbanizacion=:codurbanizacion, orden_lect = ".$OrdenLect.", orden_dist = ".$OrdenDist."
        WHERE codemp=:codemp and codsuc=:codsuc and nroinscripcion=:nroinscripcion";

    $result = $conexion->prepare($updConx);
    $result->execute(array(":referencia" => $referencia,
        ":tipopredio" => $tipopredio,
        ":codtipoconstruccion" => $tipoconstruccion,
        ":nropisos" => $nropisos,
        ":codtipoabastecimiento" => $tipoabastecimiento,
        ":piscina" => $piscina,
        ":codtipoalmacenaje" => $tipoalmacenaje,
        ":codtiporesponsable" => $tiporesponsable,
        ":inquilino" => $inquilino,
        ":altocon" => $altoconsumidortext,
        ":codtipoentidades" => $tipoentidades,
        ":coddiametrosagua" => $diametrosagua,
        ":codtipomaterialagua" => $tipomaterialagua,
        ":codlocacajaagua" => $locacajaagua,
        ":codtipopavimentoagua" => $pavimentoagua,
        ":codtipocorte" => $tipocorte,
        ":codestadoconexion" => $estadoconexion,
        ":codestadocajaagua" => $estadocajaagua,
        ":codubicacion" => $ubicacionllavemedidor,
        ":codtipovereda" => $tipovereda,
        ":fechainsagua" => $fechainstagua,
        ":codtipofugasagua" => $tipofugasagua,
        ":codtipocajaagua" => $tipocajaagua,
        ":codtipotapaagu" => $tipotapaagua,
        ":estadotapaagu" => $estadotapaagua,
        ":codmarca" => $marcamedidor,
        ":coddiametrosmedidor" => $diametrosmedidor,
        ":codestadomedidor" => $estadomedidor,
        ":posicionmed" => $posicionmedidor,
        ":tipolectura" => $tipolectura,
        ":fechainsmed" => $fechainstmedidor,
        ":codmodelo" => $modelomedidor,
        ":aniofabmed" => $aniofab,
        ":codtipomedidor" => $tipomedidor,
        ":codcapacidadmedidor" => $capacidadmedidor,
        ":coddiametrosdesague" => $diametrosdesague,
        ":codtipomaterialdesague" => $tipomaterialdesague,
        ":codlocacajadesague" => $locacajadesague,
        ":fechainsdesague" => $fechainstdesague,
        ":codtipocajadesague" => $tipocajadesague,
        ":codestadocajadesague" => $estadocajadesague,
        ":codtipotapadesague" => $tipotapadesague,
        ":estadotapadesague" => $estadotapadesague,
        ":fugasatoros" => $fugasyatoros,
        ":presionagua" => $presionagua,
        ":horasabastecimiento" => $horasabastecimiento,
        ":nrohabitantes" => $nrohabitantes,
        ":observacionabast" => $observacionabast,
        ":codzonaabas" => $zonasbastecimiento,
        ":nrolavatorios" => $nrolavatorios,
        ":nrolavaropas" => $nrolavaropas,
        ":nrowater" => $nrowater,
        ":nroduchas" => $nroduchas,
        ":nrourinarios" => $nrourinarios,
        ":nrogrifos" => $nrogrifos,
        ":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nroinscripcion" => $nroinscripcion,
        ":codtipomatconstruccion" => $tipomatconstruccion,
        ":conpozo" => $conpozo,
        ":conpozoimporte" => $conpozoimporte,
        ":codurbanizacion" => $codurbanizacion
    ));

    $del = "DELETE FROM catastro.unidadesusoclientes WHERE codemp=? and codsuc=? and nroinscripcion=?";
    $result = $conexion->prepare($del);
    $result->execute(array($codemp, $codsuc, $nroinscripcion));
    $count_unidades = $_POST["cont_unidades"];

    $catetar = 0;
    $domestico = 0;
    $social = 0;
    $comercial = 0;
    $estatal = 0;
    $industrial = 0;
    $cont = 1;

    for ($i = 1; $i <= $count_unidades; $i++) {
        if (isSET($_POST["tarifas".$i])) {
            if ($_POST["principal".$i] == 1) {
                $catetar = $_POST["tarifas".$i];
            }

            $tarifasdet= $_POST["tarifas".$i];
            $porcentaje= $_POST["porcentaje".$i];
            $principal = $_POST["principal".$i];
            $item      = $_POST["ItemD".$i];
            $codtipoactividad      = $_POST["codtipoactividad".$i];
            $sqlT = "SELECT t.catetar as codcategoriatar,
                uc.catetar as codtarifa,
                t.nomtar as tarifa
                FROM catastro.unidadesusoclientes uc
                INNER JOIN facturacion.tarifas t ON (uc.catetar = t.catetar AND uc.codemp = t.codemp AND uc.codsuc = t.codsuc)
                WHERE uc.codemp=".$codemp." and uc.codsuc=".$codsuc." and uc.catetar=".$tarifasdet." ";

            $result = $conexion->query($sqlT);
            //$result->execute(array($codemp, $codsuc, $_POST["tarifas".$i] ));
            $itemsT = $result->fetch();

            if ($itemsT["codcategoriatar"] == 1) {
                $domestico++;
            }
            if ($itemsT["codcategoriatar"] == 2) {
                $comercial++;
            }
            if ($itemsT["codcategoriatar"] == 3) {
                $industrial++;
            }
            if ($itemsT["codcategoriatar"] == 4) {
                $estatal++;
            }
            if ($itemsT["codcategoriatar"] == 5) {
                $social++;
            }

            $instUnidades = "INSERT INTO catastro.unidadesusoclientes(codemp, codsuc, nroinscripcion, catetar,
                porcentaje, principal, item,codtipoactividad)
                VALUES(".$codemp.", ".$codsuc.", '".$nroinscripcion."', ".$tarifasdet.", '".$porcentaje."', ".$principal.", ".$i.",".$codtipoactividad.")";

            $result = $conexion->query($instUnidades);
            /*$result->execute(array(":codemp" => $codemp,
                ":codsuc" => $codsuc,
                ":nroinscripcion" => $nroinscripcion,
                ":catetar" => $_POST["tarifas".$i],
                ":porcentaje" => $_POST["porcentaje".$i],
                ":principal" => $_POST["principal".$i],
                ":item" => $_POST["ItemD".$i],
            ));*/

            if ($cont == 1) {
                $valoractual = substr($itemsT['tarifa'], 0, 3);
                $valoractual .= "(".$_POST["porcentaje".$i]."-".$_POST["principal".$i].")";
                $cont++;
            } else {
                $valoractual .= "-".substr($itemsT['tarifa'], 0, 3);
                $valoractual .= "(".$_POST["porcentaje".$i]."-".$_POST["principal".$i].")";
            }
            if ($_POST["ItemD".$i] == 1) {
                $catetar = $_POST["tarifas".$i];
            }
        }
    }

    $updClientes = "UPDATE catastro.clientes SET
        catetar=".$catetar.", domestico=".$domestico.", social=".$social.",
        comercial=".$comercial.", estatal=".$estatal.", industrial=".$industrial."
        WHERE codemp=".$codemp." and codsuc=".$codsuc." and nroinscripcion='".$nroinscripcion."' ";

    $result = $conexion->query($updClientes);
    /*$result->execute(array(":catetar" => $catetar,
        ":domestico" => $domestico,
        ":social" => $social,
        ":comercial" => $comercial,
        ":estatal" => $estatal,
        ":industrial" => $industrial,
        ":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nroinscripcion" => $nroinscripcion)
    );*/


    if ($codtultimo != 0) {
        $correlativo++;
        $instModificaciones = "INSERT INTO catastro.modificaciones(codemp, codsuc, nroinscripcion, nromodificaciones,
            campo,valoractual,valorultimo,observacion,creador)
            VALUES(".$codemp.", ".$codsuc.", '".$nroinscripcion."', ".$correlativo.", 'catetar', '".$valorultimo."', '".$valoractual."',
                '".$observacion."', ".$idusuario.")";
        $result = $conexion->query($instModificaciones);
        /*$result->execute(array(":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":nroinscripcion" => $nroinscripcion,
            ":nromodificaciones" => $correlativo,
            ":campo" => 'catetar',
            ":valoractual" => $valorultimo,
            ":valorultimo" => $valoractual,
            ":observacion" => $observacion,
            ":creador" => $idusuario));*/
    }

    //Cambio de Categoria y Actividad
    if ($tipoactividad != $tipoactividadA) {
        $SqlTA = "UPDATE vma.usuarios ";
        $SqlTA .= "SET cambio_actividad=1, cambio_actividad_antiguo=".$tipoactividadA.", cambio_actividad_nuevo=".$tipoactividad." ";
        $SqlTA .= "WHERE codemp=1 AND codsuc=".$codsuc." AND nroinscripcion=".$nroinscripcion;

        $resultTA = $conexion->query($SqlTA);
        //$resultTA->execute(array());
    }
    if ($catetar != $catetarA) {
        $SqlTA = "UPDATE vma.usuarios ";
        $SqlTA .= "SET cambio_categoria=1 ";
        $SqlTA .= "WHERE codemp=1 AND codsuc=".$codsuc." AND nroinscripcion=".$nroinscripcion;

        $resultTA = $conexion->prepare($SqlTA);
        $resultTA->execute(array());
    }

	//Reordenar la Secuencia de la Ruta
	$Sql = "SELECT con.nroinscripcion ";
	$Sql .= "FROM catastro.clientes cli ";
	$Sql .= " INNER JOIN catastro.conexiones con ON (cli.codemp = con.codemp) AND (cli.codsuc = con.codsuc) AND (cli.nroinscripcion = con.nroinscripcion) ";
	$Sql .= "WHERE cli.codsuc = ".$codsuc." ";
	$Sql .= " AND cli.codzona = ".$zona." ";
	$Sql .= " AND cli.codsector = ".$sector." ";
	$Sql .= " AND cli.codrutlecturas = ".$codrutlectura." ";
	$Sql .= "ORDER BY CAST(con.orden_lect AS INTEGER), CAST(cli.codmanzanas AS INTEGER), cli.lote ";
	
	$consul = $conexion->prepare($Sql);
	$consul->execute(array());
	$rows = $consul->fetchAll();
	
	$Flag = 0;
	
	foreach ($rows as $row)
	{
		if ($row[0] == $nroinscripcion)
		{
			$Flag = 1;
		}
		
		if ($Flag == 1)
		{
			$SqlC = "UPDATE catastro.conexiones ";
			$SqlC .= "SET orden_lect = ".$OrdenLect.", orden_dist = ".$OrdenLect." ";
			$SqlC .= "WHERE codsuc = ".$codsuc." ";
			$SqlC .= " AND nroinscripcion = ".$row[0];

			$resultC = $conexion->prepare($SqlC);
			$resultC->execute(array());
			
			$OrdenLect++;
		}
	}
	
    if (!$result) {
        $conexion->rollBack();
        var_dump($result->errorInfo());
        die(2);
    }
	else
	{
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
	}
?>
