// JavaScript Document
var objIdx				= "";

var cont_unidad			= 0;
var cont_fila_unidad 	= 0;

var total_porcentaje 	= 0;
var procentaje 			= 0;

function Recibir(Id)
{
	if(objIdx=="cliente")
	{
		cargar_datos_usuario(Id)
	}
}
function QuitaFilaD(x,opcion,idx)
{
	while (x.tagName.toLowerCase() !='tr')
	{
		if(x.parentElement)
			x=x.parentElement;
		else if(x.parentNode)
			x=x.parentNode;
		else
			return;
	}

	var rowNum=x.rowIndex;
	while (x.tagName.toLowerCase() !='table')
	{
		if(x.parentElement)
			x=x.parentElement;
		else if(x.parentNode)
			x=x.parentNode;
		else
			return;
	}
    if(opcion==1)
	{
		total_porcentaje = parseFloat(total_porcentaje) - parseFloat($("#porcentaje"+idx).val())
		$("#total_porct").val(total_porcentaje)
		$("#procentaje").val(parseFloat($("#procentaje").val()) + parseFloat($("#porcentaje"+idx).val()))

		cont_fila_unidad = parseFloat(cont_fila_unidad) - 1;
	}
	if(opcion==0)
	{
		cont_fila_presupuesto = parseFloat(cont_fila_presupuesto) - 1;
		if(cont_fila_presupuesto==0){$("#codcliente").val(0)}
	}

	x.deleteRow(rowNum);

	if (opcion==1)
	{
		if($("#cont_unidades").val() > 0){
			var contador = parseInt($("#cont_unidades").val()) - 1;
			cont_unidad = contador;
			$("#cont_unidades").val(contador);
		}
		Numerar();
		Reiniciar();
	}
}
function Numerar()
{
	var contt = 1;
	for (var i = 1; i<= cont_unidad; i++)
	{
		try
		{
			document.getElementById('ItemD' + i).value = contt;
			$('#principalUU' + i).val(contt);
			contt = contt + 1;

			//alert(cont_unidad);

			$('#principal' + i).val(0);
			if ($('#principalUU' + i).is(':checked'))
			{
				var valor = $('#tarifas' + i).val();

				$('#principal' + i).val(1);
				$("#catetar option[value=" + valor + "]").attr("selected",true);
			}
		}
		catch(err)
		{
			//alert('papap');
		}
	}
}

function Reiniciar(){
	var nuevo_indice = 0;
	var id_ant;
	var id_ant;
	var id_ant;
	$("#tbunidades tbody tr").each(function(index, element){

		// $(this).children("td").each(function (index2,element2){
		//
		//
		//
		// }
		// Para el item
		nuevo_indice = parseInt(index) + 1
		$(this).children("td:eq(0)").html(nuevo_indice)

		// Para el codigo
		$(this).children("td:eq(1)").children("input:eq(0)").attr('id','ItemD'+nuevo_indice)
		$(this).children("td:eq(1)").children("input:eq(0)").attr('name','ItemD'+nuevo_indice)
		$(this).children("td:eq(1)").children("input:eq(0)").val(nuevo_indice)

		$(this).children("td:eq(1)").children("input:eq(1)").attr('id','tarifas'+nuevo_indice)
		$(this).children("td:eq(1)").children("input:eq(1)").attr('name','tarifas'+nuevo_indice)

		// Para el categoria
		$(this).children("td:eq(2)").children("input:eq(0)").attr('id','porcentaje'+nuevo_indice)
		$(this).children("td:eq(2)").children("input:eq(0)").attr('name','porcentaje'+nuevo_indice)

		// Para el actividad
		$(this).children("td:eq(3)").children("input:eq(0)").attr('id','codtipoactividad'+nuevo_indice)
		$(this).children("td:eq(3)").children("input:eq(0)").attr('name','codtipoactividad'+nuevo_indice)

		// Para el principal
		$(this).children("td:eq(5)").children("input:eq(0)").attr('id','principal'+nuevo_indice)
		$(this).children("td:eq(5)").children("input:eq(0)").attr('name','principal'+nuevo_indice)

		$(this).children("td:eq(5)").children("input:eq(1)").attr('id','principalUU'+nuevo_indice)

		// Boton Eliminar
		$(this).children("td:eq(6)").children("img").attr('onclick','QuitaFilaD(this, 1,'+nuevo_indice+');')


	});
}


function Habilitar(Check, Obj)
{
    if (Check.checked == true)
    {
				$("#categoria_veces").attr('readonly','readonly').val('')
        $("#"+Obj).val(1)
    } else {
        $("#categoria_veces").removeAttr('readonly')
        $("#"+Obj).val(0)
    }
}

	jQuery(function($)
	{
		redo_select();

		$("#DivTarifas").dialog({
			autoOpen: false,
			modal: true,
			width: 450,
			height:450,
			resizable: false,
			show: "scale",
			hide: "scale",
			close: function() {},
			buttons: {
				"Cerrar": function() {
					$( this ).dialog( "close" );
				}
			}
		});

		$("#DivTarifasAcordion").accordion({heightStyle: "content"});

		$("#DivTarifas").dialog('close');

		//Inicializamos los campos de Fecha
		$("#fechainstagua").mask("99/99/9999");
		$("#fechainstmedidor").mask("99/99/9999");
		$("#fechainstdesague").mask("99/99/9999");
		$("#fechalecturaanterior").mask("99/99/9999");
		$("#fechalecturaultima").mask("99/99/9999");


		$("#fechainstagua").datepicker(
		{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true
			}
		);

		$("#fechainstmedidor").datepicker(
		{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true
		});

		$("#fechainstdesague").datepicker(
		{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true
		});

		$("#fechalecturaanterior").datepicker(
		{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true
		});

		$("#fechalecturaultima").datepicker(
		{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true
		});
	});


function cargar_rutas_lecturas(codmanzanas, codsector)
{
    if(codmanzanas == '')
	{
		codmanzanas = $("#codrutlecturatemp").val();
	}

    //var cod = $("#sector").val();
//
//    if(cod == 0)
//    {
        cod = codsector;
    //}

	//alert(cod);
    $.ajax({
		 url:'../../../../ajax/rutaslecturascmb.php',
		 type:'POST',
		 async:true,
		 data:'codsuc=' + codsuc + '&codsector=' + cod + '&codmanzana=' + codmanzanas + '&codrutlecturas=' + $("#codrutlecturatemp").val(),
		 success:function(datos){
			//var r=datos.split("|")
			if(datos=='')
			{
				alert('No se a Asigando Ruta de Lectura al Sector y Manzana seleccionados')
			}
			else
			{
				$("#codrutlectura").html(datos)
				cargar_rutas_lecturas_orden()
			}

		 }
    })
}
var codsector = 0;
function cargar_rutas_lecturas_orden()
{
	var cod = $("#sector").val();

    if(cod == 0)
    {
        cod = codsector;
    }

	$.ajax({
		 url:'../../../../ajax/rutaslecturasorden.php',
		 type:'POST',
		 async:true,
		 data:'codsuc=' +codsuc + '&codsector=' + cod + '&codmanzana=' + $("#manzanas").val() + '&codrutlecturas=' + $("#codrutlectura").val(),
		 success:function(datos){
			//var r=datos.split("|")
			$("#ordenlecturas").val(datos)
			/*$("#rutalecturas").val(r[2])
			$("#ordenlecturas").val(r[1])
			$("#codrutlectura").val(r[0])*/
			//cargar_rutas_lecturas_orden()
		 }
    })
}


function cargar_rutas_distribucion(codmanzanas, codsector)
{
    if (codmanzanas == '')
        codmanzanas = $("#codrutdistribuciontemp").val()
    //var cod = $("#sector").val();
    var cod= codsector;
    if (cod == 0)
    {
        cod = codsector;
    }
    //alert(cod);
    $.ajax({
        url: '../../../../ajax/rutasdistribucioncmb.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&codsector='+cod+'&codmanzana='+codmanzanas+'&codrutdistribucion='+$("#codrutdistribuciontemp").val(),
        success: function(datos) {
            //var r=datos.split("|")
            if (datos == '')
            {
                alert('No se a Asigando Ruta de Distribucion al Sector y Manzana seleccionados')
            }
            else
            {
                $("#codrutdistribucion").html(datos)
                //cargar_rutas_distribucion_orden()
            }

        }
    })
}

/*
function cargar_rutas_distribucion(obj, codsector)
{
    var cod = $("#sector").val();
        cod= codsector;

    $.ajax({
		 url:'../../../../ajax/rutasdistribucion.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&codsector='+cod+'&codmanzana='+obj,
		 success:function(datos){
			var r = datos.split("|")

			if(r[1]=='')
			{
				alert('No se a Asigando Ruta de Distribucion al Sector y Manzana seleccionados')
			}
			else
			{
				$("#rutadistribucion").val(r[2])
				//$("#ordendistribucion").val(r[1])
				$("#codrutdistribucion").val(r[0])
			}
        }
    })
}
*/

function agregar_unidades_uso(codsuc)
{
	if($("#tarifas").val()==0)
	{
		alert("Seleccione la Tarifa")
		return false;
	}
	if($("#tipoactividad").val()==0)
	{
		Msj($("#tipoactividad"),"Seleccione la Actividad")
		return false;
	}
	if($("#procentaje").val()==0 || $("#procentaje").val()=="" || $("#procentaje").val()<0 || $("#procentaje").val()>100)
	{
		alert("El Porcentaje Ingresado no es Valido")
		return false;
	}
	if($("#total_porct").val()>=100)
	{
		alert("Ya se alcanzo el 100%")
		return false;
	}


	var categoria   = $("#tarifas option:selected").text();
	var idcategoria = $("#tarifas").val();
	total_porcentaje   += parseFloat($("#procentaje").val());
	if(total_porcentaje>100)
	{
		alert("Se sobrepaso el 100%")
		total_porcentaje   -= parseFloat($("#procentaje").val());
		return false;
	}

	// Verificando que N veces categorias no superen el 100%

	if($("#principal_categoria_veces").val() == 0){
		var porcentaje     = parseInt($("#procentaje").val());
		var cantidad_veces = parseInt($("#categoria_veces").val());

		total = porcentaje * cantidad_veces;

		if(total > 100)
		{
			alert("Se sobrepaso el 100%")
			total_porcentaje   -= parseFloat($("#procentaje").val())
			return false;
		}
	}


	var principal="NO";
	if($("#principal_categoria").val()==1)
	{
		principal="SI";
	}

	if($("#principal_categoria").val()==1)
	{
		c=0;
		$("#tbunidades input.principal").each(function(){
			if($(this).val()==1)
				c++;
		});
		if(c==1)
		{
			$("#tabs").tabs({ selected:6 });
			Msj('#tbunidades','Ya se Agrego Categoria Principal',1000,'above','',false)
          	return false;
		}

		if ( $("#tipofacturacion").val() == 2 ) {
			cambiarcombotipofacturacion(codsuc,idcategoria);
		}
	}
	cont_unidad++;
	cont_fila_unidad++;

	principalUU = "";
	if ($("#principal_categoria").val()==1)
	{
		principalUU = "checked='checked'";
	}

	var item         = $('#tbunidades >tbody >tr').length;
	var indice_tabla = parseInt(item);
	$("#total_porct").val(total_porcentaje)
	var total_veces = parseInt($("#categoria_veces").val());

	if($("#principal_categoria_veces").val() == 0){

		cont_unidad = item;
		for (var i = 1; i <= total_veces; i++) {

			indice_tabla++;
			cont_unidad++;
			$( "#tbunidades tbody" ).append(
				"<tr style='background-color:#FFFFFF; padding:4px'>"+
					"<td align='center'>"+indice_tabla+"</td>"+
					"<td align='center'><input type='hidden' name='ItemD" + cont_unidad + "' id='ItemD" + cont_unidad + "' value='" + cont_unidad + "' /><input type='hidden' id='tarifas"+cont_unidad+"' name='tarifas"+cont_unidad+"' value='"+$("#tarifas").val()+"' />"+$("#tarifas").val()+"</td>"+
					"<td>"+categoria+"</td>"+
					"<td align='center'><input type='hidden' name='codtipoactividad" + cont_unidad + "' id='codtipoactividad" + cont_unidad + "' value='" + $("#tipoactividad").val() + "' />"+$("#tipoactividad option:selected").html()+"</td>"+
					"<td align='center' ><input type='hidden' id='porcentaje"+cont_unidad+"' name='porcentaje"+cont_unidad+"' value='"+$("#procentaje").val()+"' />"+$("#procentaje").val()+"</td>"+
					"<td align='center' ><input type='hidden' class='principal' id='principal"+cont_unidad+"' name='principal"+cont_unidad+"' value='"+$("#principal_categoria").val()+"' />"+
					"<input type='radio' name='principalUU' id='principalUU" + cont_unidad + "' value=" + cont_fila_unidad + " " + principalUU + " onChange='Numerar(); actualizacion(this, 3);'  style='cursor:pointer'/></td>"+
					"<td align='center' ><img src='../../../../images/iconos/cancel.png' width='16' height='16' title='Borrar Registro' onclick='QuitaFilaD(this,1,"+cont_unidad+");' style='cursor:pointer'/></td>"+
				"</tr>");
			}
			cont_unidad = indice_tabla;
	}else{

		cont_unidad = item + 1;
		indice_tabla++;
		$("#total_porct").val(total_porcentaje)
		$( "#tbunidades tbody" ).append(
										"<tr style='background-color:#FFFFFF; padding:4px'>"+
											"<td align='center'>"+indice_tabla+"</td>"+
											"<td align='center'><input type='hidden' name='ItemD" + cont_unidad + "' id='ItemD" + cont_unidad + "' value='" + cont_unidad + "' /><input type='hidden' id='tarifas"+cont_unidad+"' name='tarifas"+cont_unidad+"' value='"+$("#tarifas").val()+"' />"+$("#tarifas").val()+"</td>"+
											"<td>"+categoria+"</td>"+
											"<td align='center'><input type='hidden' name='codtipoactividad" + cont_unidad + "' id='codtipoactividad" + cont_unidad + "' value='" + $("#tipoactividad").val() + "' />"+$("#tipoactividad option:selected").html()+"</td>"+
											"<td align='center' ><input type='hidden' id='porcentaje"+cont_unidad+"' name='porcentaje"+cont_unidad+"' value='"+$("#procentaje").val()+"' />"+$("#procentaje").val()+"</td>"+
											"<td align='center' ><input type='hidden' class='principal' id='principal"+cont_unidad+"' name='principal"+cont_unidad+"' value='"+$("#principal_categoria").val()+"' />"+
											"<input type='radio' name='principalUU' id='principalUU" + cont_unidad + "' value=" + cont_fila_unidad + " " + principalUU + " onChange='Numerar(); actualizacion(this, 3);'  style='cursor:pointer'/></td>"+
											"<td align='center' ><img src='../../../../images/iconos/cancel.png' width='16' height='16' title='Borrar Registro' onclick='QuitaFilaD(this,1,"+cont_unidad+");' style='cursor:pointer'/></td>"+
										"</tr>");
	}


	$("#cont_unidades").val(cont_unidad)

	$("#tarifas").val(0)
	document.getElementById("principal_categoria_check").checked=false;

	$("#principal_categoria").val(0)
	$("#procentaje").val(100 - parseFloat($("#total_porct").val()))

	Numerar();
}
function validar_nuevo_usuario(obj)
{
	if(obj.checked==false)
	{
		if($("#flagnuevotext").val()==1)
		{
			alert("No se puede desactivar el control por que la opcion Nuevo Usuario esta Activada")
			obj.checked=true
			$("#sinpresupuestotext").val(1)
			return false
		}
	}
}
function quitar_readonly(obj,input)
{
	if(obj.checked)
	{
		if($("#sinpresupuestotext").val()==0)
		{
			alert("No se puede seleccionar esta opcion por que la opcion con presupuesto esta activada")
			obj.checked=false
			return false;
		}
		$('#'+input).attr('readonly', false);
		$("#codcliente").val(0)
		$('#'+input).val('');
	}else{
		$('#'+input).val('');
		$('#'+input).attr('readonly', true);
	}
}
function buscar_usuarios()
{
	if($("#flagnuevotext").val()==1)
	{
		alert("No se puede buscar un usuario por que la opcion Nuevo Usuario esta Activada")
		return false
	}
	objIdx = "cliente"
	AbrirPopupBusqueda("../actualizacion/?Op=5",800,600)
}
function cargar_datos_usuario(nroinscripcion)
{
	$.ajax({
		 url:'../../../../ajax/clientes.php',
		 type:'POST',
		 async:true,
         dataType: 'json',
		 data:'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion,
		 success:function(datos){
			//var r=datos.split("|")

			$("#codcliente").val(datos.codcliente)
			$("#propietario").val(datos.propietario)
			$("#tipodocumento").val(datos.codtipodocumento)
			$("#nrodocumento").val(datos.nrodocumento)

			actualizacionx(datos.codcliente,"codcliente","0")
			actualizacionx(datos.propietario,"propietario","0")
			actualizacionx(datos.codtipodocumento,"propietario","0")
			actualizacionx(datos.nrodocumento,"nrodocumento","0")
		 }
    })
}
function calcularconsumo(obj)
{
	anterior 	= $("#lecturaanterior").val()
	ultima		= obj

	if(ultima=="")
	{
		ultima=0;
	}
	$("#consumo").val(parseFloat(ultima)-parseFloat(anterior))
}
function actualizacion(obj, tipo)
{
	if(obj.id=="lecturaultima")
	{
		consumo = parseFloat($("#lecturaultima").val())-parseFloat($("#lecturaanterior").val())
		actualizacionx(consumo,"consumo",0)
	}
	if(obj.id=="sector"){campo="codsector";}
	if(obj.id=="manzanas"){campo="codmanzanas";}
	if(obj.id=="lote"){campo="lote";}
	if(obj.id=="sublote"){campo="sublote";}
	if(obj.id=="estadoservicio"){campo="codestadoservicio";}
	if(obj.id=="tiporesponsable"){campo="codtiporesponsable";}
	if(obj.id=="propietario"){campo="propietario";}
	if(obj.id=="tipodocumento"){campo="codtipodocumento";}
	if(obj.id=="nrodocumento"){campo="nrodocumento";}
	if(obj.id=="calles"){campo="calles";}
	if(obj.id=="nrocalle"){campo="nrocalle";}
	if(obj.id=="tipoentidades"){campo="codtipoentidades";}
	if(obj.id=="tipousuario"){campo="codtipousuario";}
	if(obj.id=="dirdistribucion"){campo="direcciondistribucion";}
	if(obj.id=="inquilino"){campo="inquilino";}
	if(obj.id=="tipoactividad"){campo="codtipoactividad";}
	if(obj.id=="ciclo"){campo="codciclo";}
	if(obj.id=="tipopredio"){campo="tipopredio";}
	if(obj.id=="tipoconstruccion"){campo="codtipoconstruccion";}
	if(obj.id=="nropisos"){campo="nropisos";}
	if(obj.id=="tipoabastecimiento"){campo="codtipoabastecimiento";}
	if(obj.id=="piscina1"){campo="piscina";}
	if(obj.id=="piscina2"){campo="piscina";}
	if(obj.id=="tipoalmacenaje"){campo="codtipoalmacenaje";}
	if(obj.id=="tiposervicio"){campo="codtiposervicio";}
	if(obj.id=="referencia"){campo="referencia";}
	if(obj.id=="tipomaterialagua"){campo="codtipomaterialagua";}
	if(obj.id=="locacajaagua"){campo="codlocacajaagua";}
	if(obj.id=="diametrosagua"){campo="coddiametrosagua";}
	if(obj.id=="pavimentoagua"){campo="codtipopavimentoagua";}
	if(obj.id=="tipocorte"){campo="codtipocorte";}
	if(obj.id=="estadoconexion"){campo="codestadoconexion";}
	if(obj.id=="tipovereda"){campo="codtipovereda";}
	if(obj.id=="fechainstagua"){campo="fechainsagua";}
	if(obj.id=="tipocajaagua"){campo="codtipocajaagua";}
	if(obj.id=="estadocajaagua"){campo="codestadocajaagua";}
	if(obj.id=="tipofugasagua"){campo="codtipofugasagua";}
	if(obj.id=="estadotapaagua"){campo="estadotapaagu";}
	if(obj.id=="marcamedidor"){campo="codmarca";}
	if(obj.id=="diametrosmedidor"){campo="coddiametrosmedidor";}
	if(obj.id=="estadomedidor"){campo="codestadomedidor";}
	if(obj.id=="posicionmedidor"){campo="posicionmed";}
	if(obj.id=="modelomedidor"){campo="codmodelo";}
	if(obj.id=="tipolectura"){campo="tipolectura";}
	if(obj.id=="aniofab"){campo="aniofabmed";}
	if(obj.id=="tipomedidor"){campo="codtipomedidor";}
	if(obj.id=="fechainstmedidor"){campo="fechainsmed";}
	if(obj.id=="capacidadmedidor"){campo="codcapacidadmedidor";}
	if(obj.id=="nromedidor"){campo="nromed";}
	if(obj.id=="tipofacturacion"){campo="tipofacturacion";}
	if(obj.id=="ubicacionllavemedidor"){campo="codubicacion";}
	if(obj.id=="lecturaanterior"){campo="lecturaanterior";}
	if(obj.id=="fechalecturaanterior"){campo="fechalecturaanterior";}
	if(obj.id=="lecturaultima"){campo="lecturaultima";}
	if(obj.id=="fechalecturaultima"){campo="fechalecturaultima";}
	if(obj.id=="consumo"){campo="consumo";}
	if(obj.id=="diametrosmedidor"){campo="coddiametrosdesague";}
	if(obj.id=="tipomaterialdesague"){campo="codtipomaterialdesague";}
	if(obj.id=="locacajadesague"){campo="codlocacajadesague";}
	if(obj.id=="fechainstdesague"){campo="fechainsdesague";}
	if(obj.id=="tipocajadesague"){campo="codtipocajadesague";}
	if(obj.id=="estadocajadesague"){campo="codestadocajadesague";}
	if(obj.id=="tipotapadesague"){campo="codtipotapadesague";}
	if(obj.id=="estadotapadesague"){campo="estadotapadesague";}
	if(obj.id=="fugasyatoros"){campo="fugasatoros";}
	if(obj.id=="desagueinactivotext"){campo="exoneralac";}
	if(obj.id=="zonasbastecimiento"){campo="codzona";}
	if(obj.id=="presionagua"){campo="presionagua";}
	if(obj.id=="horasabastecimiento"){campo="horasabastecimiento";}
	if(obj.id=="nrolavatorios"){campo="nrolavatorios";}
	if(obj.id=="nrolavaropas"){campo="nrolavaropas";}
	if(obj.id=="nrowater"){campo="nrowater";}
	if(obj.id=="nroduchas"){campo="nroduchas";}
	if(obj.id=="nrourinarios"){campo="nrourinarios";}
	if(obj.id=="nrogrifos"){campo="nrogrifos";}
	if(obj.id=="nrohabitantes"){campo="nrohabitantes";}
	if(obj.id=="observacion"){campo="observacionabast";}
	if(obj.id=="latitud"){campo="latitud";}
	if(obj.id=="longitud"){campo="longitud";}
	if(obj.id=="correo"){campo="correo";}
	if(obj.id=="tipomatconstruccion"){campo="codtipomatconstruccion";}
	if(obj.id=="nroorden"){campo="nroorden";}
	if(obj.id=="lecturapromedio"){campo="lecturapromedio";}
	if(obj.id=="codrutdistribucion"){campo="codrutdistribucion";}
	if(obj.id=="codrutlectura"){campo="codrutlecturas";}
	if(obj.id=="conpozo1"){campo="conpozo";}
	if(obj.id=="conpozo0"){campo="conpozo";}
	if(obj.id=="conpozoimporte"){campo="conpozoimporte";}
	if(obj.id=="porimg"){campo="catetar";}
	if(obj.id=="codurbanizacion"){campo="codurbanizacion";}

	if(obj.id=="orden_lect"){campo="orden_lect";}
	if(obj.id=="orden_dist"){campo="orden_dist";}

	if(tipo==3){campo="catetar"; tipo=0;}

	$.ajax({
		 url:'proceso.php',
		 type:'POST',
		 async:true,
		 data:'valor='+obj.value+'&campo='+campo+'&tipo='+tipo,
		 success:function(datos){
			$("#contmod").val(1)
		 }
	})
}
function actualizacionx(valor,campo,tipo)
{
	$.ajax({
		 url:'proceso.php',
		 type:'POST',
		 async:true,
		 data:'valor='+valor+'&campo='+campo+'&tipo='+tipo,
		 success:function(datos){
			$("#contmod").val(1)
		 }
	})
}
function promediar_lecturas()
{
	if($("#tipofacturacion").val()!=1)
	{
		alert("El Tipo de Facturacion no permite promediar las Lecturas")
		return false;
	}
	verlecturas();
	objIdx = "promediar";
	$( "#DivLecturas" ).dialog( "open" );
}
function verlecturas()
{
	$.ajax({
		 url:'ver_lecturas.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val(),
		 success:function(datos){
			$("#div_lecturas").html(datos)
		 }
	})
}
function asignarconsumo()
{
	var catetar="";
	var tfacturacion=$("#tipofacturacion").val();

	if(tfacturacion!=2)
	{
		alert('El Tipo de Facturacion no Admite que se asigne el Consumo')
		return
	}

	for (i=1;i<=$("#cont_unidades").val();i++)
	{
		try
		{
			if($("#principal"+i).val()==1)
			{
				catetar=$("#tarifas"+i).val()
			}
		}catch(exp)
		{

		}
	}

	mostrarconsumo(catetar)
}
function mostrarconsumo(catetar)
{
	$.ajax({
		 url:'mostrar_volumen.php',
		 type:'POST',
		 async:true,
		 data:'catetar='+catetar+'&codsuc='+codsuc,
		 success:function(datos){
			$("#lecturaanterior").val(0)
			$("#lecturaultima").val(0)
			$("#consumo").val(datos)
		 }
	})
}
function validar_altocon()
{
	actualizacionx($("#altoconsumidortext").val(),"altocon",1)
}
function validar_desagueinactivo()
{
	actualizacionx($("#desagueinactivotext").val(),"exoneralac",0)
}
function validar_cod_catastro()
{
	$.ajax({
		 url:'../../../../admin/validaciones/validar_catastro.php',
		 type:'POST',
		 async:true,
		 data:'codrutlecturas='+$("#codrutlectura").val()+'&lote='+$("#lote").val(),
		 success:function(datos){
			$("#cCatastro").val(datos)

			//setTimeout(validar_cod_catastro(),2000)
		}
	})
}
function cargar_zonas_abastecimiento(codsector,seleccion,opcion)
{
	$.ajax({
		 url:'../../../../ajax/zonas_abastecimiento_drop.php',
		 type:'POST',
		 async:true,
		 data:'codsector='+codsector+'&codsuc='+codsuc+'&seleccion='+seleccion+'&opcion='+opcion,
		 success:function(datos){
			$("#div_zonas").html(datos)
		 }
	})
}
function cargar_horarios_zonas(codzona,codsector)
{
	$.ajax({
		 url:'../../../../ajax/horarios_abastecimiento.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&codzona='+codzona+'&codsector='+codsector+'&codrutlectura='+$("#codrutlectura").val(),
		 success:function(datos){
			$("#horasabastecimiento").val(datos)
		 }
	})
}
function buscar_gis(direccion,tarifa,estado,servicio,tfacturacion)
{
	if($("#latitud").val()=="" || $("#latitud").val()==0)
	{
		alert("Latitud no valida")
		return
	}
	if($("#longitud").val()=="" || $("#longitud").val()==0)
	{
		alert("Longitud no valida")
		return
	}
	http://www.cheguimo.com/demos/gis/?Lat=&Long=&Abonado=&Direccion=&Tarifa=&Medidor=&UltimaLect=&Estado=
	url="http://www.cheguimo.com/demos/gis/?Lat="+$("#latitud").val()+"&Long="+$("#longitud").val()+
		"&Abonado="+$("#propietario").val()+"&Direccion="+direccion+"&Tarifa="+tarifa+"&Medidor="+$("#nromedidor").val()+
		"&UltimaLect="+$("#lecturaultima").val()+"&Estado="+estado+'&TipS='+servicio+'&TipF='+tfacturacion

	AbrirPopupBusqueda(url,900,500)
}

function VerTarifas()
{
	if($("#tipofacturacion").val()!=2)
	{
		alert("El Tipo de Facturacion no permite promediar asignar Consumos")
		return false;
	}
	$("#DivTarifas").dialog('open');
}
function AddTarifa(consumo)
{
	$("#DivTarifas").dialog('close');
	$("#consumo").val(consumo);
	actualizacion(document.getElementById('consumo'),0)
}
function VImportePozo(op)
{
	if(op==1)
	{
		$(".cconpozoimporte").show();
		$("#conpozoimporte").focus().select();
	}

	else
	{
		$("#conpozoimporte").val(0);
		$(".cconpozoimporte").hide();
	}
}

function cambiarcombotipofacturacion(codsuc,id){

	$.ajax({
		 url:'../../../../ajax/modificartipofacturacion.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&catetar='+id,
		 success:function(datos){
		 	datos = parseInt(datos);
		 	$("#consumo").val(datos);
		 	actulizacioncampos(datos,'consumo',0);
		 }
    })

}

function actulizacioncampos(valor,campo,tipo){
	$.ajax({
		 url:'proceso.php',
		 type:'POST',
		 async:true,
		 data:'valor='+valor+'&campo='+campo+'&tipo='+tipo,
		 success:function(datos){
			$("#contmod").val(1)
		 }
	})
}

function redo_select(){
	//$("#div_rutasdistribucion_2").find('select').attr('id','tipoactividad').attr('name','');
}
