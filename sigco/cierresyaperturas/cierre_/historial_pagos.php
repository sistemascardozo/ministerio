<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../objetos/clsFunciones.php");

$objFunciones = new clsFunciones();

$codsuc = $_POST["codsuc"];
$nroinscripcion = $_POST["nroinscripcion"];

ECHO $sql = "SELECT d.nropago,c.fechareg,t.descripcion, d.detalle,sum(importe) as importe,d.categoria
    FROM cobranza.detpagos as d
    INNER JOIN cobranza.cabpagos as c on(d.codemp=c.codemp AND d.codsuc=c.codsuc 
    AND d.nroinscripcion=c.nroinscripcion AND d.nropago=c.nropago)
    INNER JOIN public.tipodeuda as t on(d.codtipodeuda=t.codtipodeuda)
    WHERE d.codemp=1 AND d.codsuc=$codsuc AND d.nroinscripcion=$nroinscripcion
    GROUP BY d.nropago,c.fechareg,t.descripcion,d.detalle,d.categoria ORDER BY c.fechareg DESC";

$consulta = $conexion->prepare($sql);
//$consulta->execute(array($codsuc, $nroinscripcion));
$consulta->execute(array());
$items = $consulta->fetchAll();
?>
<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetpagos" rules="all" >
    <thead class="ui-widget-header" >
    <td width="5%" align="center">Nro. Pago</td>
    <td width="5%" align="center">Fecha</td>
    <!-- <td width="30%">Tipo de Deuda</td> -->
    <td width="12%" align="center">Detalle</td>
    <!-- <td width="12%" align="center">Categoria</td> -->
    <td width="5%" align="center">Importe</td>
</tr>
<thead>
<tbody>
    <?php
    foreach ($items as $row) {
        ?>
        <tr align="center">
            <td  align="center"><?=$row["nropago"] ?></td>
            <td align="center"><?=$objFunciones->DecFecha($row["fechareg"]) ?></td>
            <!-- <td  align="center"><?=$row["descripcion"] ?></td> -->
            <td align="left"><?=$row["detalle"] ?></td>
            <!-- <td  align="center"><?=$categoria[$row["categoria"]] ?></td> -->
            <td  align="right"><?=number_format($row["importe"], 2) ?></td>
        </tr>
    <?php } ?>
</tbody>
</table>