<?php
    session_name("pnsu");
    if (!session_start()) {
        session_start();
    }

    include("../../../include/main.php");
    include("../../../include/claseindex.php");
    
    $objMantenimiento = new clsDrop();
    
    $codsuc= $_SESSION['IdSucursal'];
    $filtro=$_POST['Filtro'];

    $sql = "SELECT clie.codcliente, clie.propietario AS propietario, sect.descripcion AS sector,";
    $sql .= " tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion, ";
    $sql .= " clie.codemp, clie.codsuc, clie.codsector, clie.codmanzanas, clie.lote, clie.sublote, ";
    $sql .= " clie.nromed, clie.nroinscripcion, clie.codestadoservicio, ";
    $sql .= $objMantenimiento->getCodCatastral("clie.").", clie.codsector, clie.codantiguo, clie.codtiposervicio ";
    $sql .= " FROM catastro.clientes clie ";
    $sql .= " INNER JOIN public.calles cal ON(clie.codemp = cal.codemp) AND (clie.codsuc = cal.codsuc) ";
    $sql .= "  AND (clie.codcalle = cal.codcalle) AND (clie.codzona = cal.codzona) ";
    $sql .= " INNER JOIN public.tiposcalle tipcal ON(cal.codtipocalle = tipcal.codtipocalle) ";
    $sql .= " INNER JOIN public.sectores sect ON(clie.codemp = sect.codemp) AND (clie.codsuc = sect.codsuc) ";
    $sql .= "  AND (clie.codzona = sect.codzona) AND (clie.codsector = sect.codsector) ";
    $sql .= " WHERE clie.codsuc = $codsuc AND clie.codciclo =1 ";
    $sql .= " AND ( CAST(clie.nroinscripcion AS varchar) ILIKE '%$filtro%' ";
    $sql .= " OR CAST(clie.codantiguo AS varchar) ILIKE '%$filtro%' ";
    $sql .= " OR CAST(clie.propietario AS varchar) ILIKE '%$filtro%' ) ";
    $sql .= " ORDER BY clie.codsector, clie.nroorden ";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array());
    $items = $consulta->fetchAll();
                                        
    foreach ($items as $row) {
        $i ++;		
        $cont = $row["nroinscripcion"];
        $NroInscripcion= " '".$row["nroinscripcion"]."' ";
        $CodCatastro= " '".$row["codcatastro"]."' ";
        $Propietario= " '".$row["propietario"]."' ";
        $Direccion= " '".$row["direccion"]."' ";
        $Sector= " '".$row["sector"]."' ";
        $Nromed= " '".$row["nromed"]."' ";
        $Codsector= " '".$row["codsector"]."' ";
        $Codantiguo= " '".$row["codantiguo"]."' ";
        $Codtiposervicio= " '".$row["codtiposervicio"]."' ";
        
        $tr.='<tr id="'.$cont.'" onclick="SeleccionaId(this);" >';
            $tr.='<td ><label class="CodCatastral">'.$row["codcatastro"].'</label>';
            $tr.=' <label class="NroInscripcion">'.$row["nroinscripcion"].'</label></td>';
            $tr.='<td align="center"><label class="Codantiguo">'.strtoupper($row['codantiguo']).'</label></td>';
            $tr.='<td >&nbsp;<label class="Propietario">'.strtoupper($row[1]).'</label></td>';
            $tr.='<td >&nbsp;<label class="Direccion">'.strtoupper($row['direccion']).'</label></td>';
            $tr.='<td align="center"><label class="Sector">'.strtoupper($row['sector']).'</label>';
            $tr.=' <label class="CodSector">'.strtoupper($row['codsector']).'</label></td>';
            $tr.='<td align="center"><label class="NroMedidor">'.strtoupper($row['nromed']).'</label></td>';
            
            $tr.='<td>';
            $tr.='<select id="tiposervicio-'.$row["nroinscripcion"].'" name="tiposervicio" class="select" style="width:120px">
                <option value="A">AGUA</option>
                <option value="A/D">AGUA Y DESAGUE</option>
                <option value="D">DESAGUE</option>';
            $tr.='</select></td>';
            
            $tr.='<td align="center"><span class="icono-icon-add Upd" onclick="agregar_corte('.$cont.' , '.$NroInscripcion.', '.$CodCatastro.', '.$Propietario.', '.$Direccion.', '.$Sector.', '.$Nromed.', '.$Codsector.', '.$Codantiguo.', '.$Codtiposervicio.' )" title="Agregar Usuario para Corte de Servicio" ></span></td>';
            $tr.='<td align="center"><span class="icono-icon-ok" title="Ver Pagos de Usuario" onclick="abrir_popu_pagos('.$NroInscripcion.');" ></span></td>';
            $tr.='<td align="center"><span class="icono-icon-dinero" title="Ver Cuenta Corriente" onclick="CuentaCorriente('.$NroInscripcion.', this)"></span></td>';
        $tr.='</tr>';
        
    } 
    
    echo $tr;
    
?>
    