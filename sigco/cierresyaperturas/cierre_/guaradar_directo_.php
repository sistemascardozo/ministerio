<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include('../../../objetos/clsFunciones.php');
//ini_set("display_errors",1);
$clsFunciones = new clsFunciones();

$contador = $_POST["cont"];
$codsuc = $_POST["codsuc"];
$codciclo = $_POST["codciclo"];
$anio = $_POST["anio"];
$mes = $_POST["mes"];
$nroinscripcion = $_POST["nroinscripcion"];
$nromed = $_POST["nromed"];
$codusu = $_POST["codusu"];
$Op = $_POST["Op"];
$codemp = 1;
$codtipoocurrencia = 1;
$codcliente = 0;
$conexion->beginTransaction();
if ($Op == 1) {
    $sqlD = "DELETE FROM medicion.cierreyapertura WHERE codemp=1 and codsuc=:codsuc and codciclo=:codciclo and
	anio=:anio and mes=:mes and tipooperacion=0 AND nroinscripcion=:nroinscripcion";

    $result = $conexion->prepare($sqlD);
    $result->execute(array(":codsuc" => $codsuc, ":codciclo" => $codciclo, ":anio" => $anio, ":mes" => $mes, ":nroinscripcion" => $nroinscripcion));

    $sqlI = "INSERT INTO medicion.cierreyapertura(codemp,codsuc,codciclo,nroinscripcion,anio,mes,tipooperacion,
        codtipocorte,fecha,codinspector,codmotivocorte,tipopersonal,observacion,codsector,codusu,nromed)
        VALUES(:codemp,:codsuc,:codciclo,:nroinscripcion,:anio,:mes,:tipooperacion,:codtipocorte,:fecha,:codinspector,
        :codmotivocorte,:tipopersonal,:observacion,:codsector,:codusu,:nromed)";
    $result = $conexion->prepare($sqlI);
    $result->execute(array(":codemp" => 1,
        ":codsuc" => $codsuc,
        ":codciclo" => $codciclo,
        ":nroinscripcion" => $nroinscripcion,
        ":anio" => $anio,
        ":mes" => $mes,
        ":tipooperacion" => 0,
        ":codtipocorte" => $_POST["codtipocorte"],
        ":fecha" => $_POST["fechacorte"],
        ":codinspector" => $_POST["codinspector"],
        ":codmotivocorte" => $_POST["codmotivocorte"],
        ":tipopersonal" => $_POST["codtipopersonal"],
        ":observacion" => isset($_POST["observacion"]) ? $_POST["observacion"] : '',
        ":codsector" => $_POST['codsector'],
        ":codusu" => $codusu,
        ":nromed" => $nromed));
    
    /* ESTO ES PROPIO DEL TRABAJO DE PUCALLPA*/
    $sqlU = "UPDATE catastro.clientes set codestadoservicio=2 WHERE codsuc=? and nroinscripcion=?";
    $result = $conexion->prepare($sqlU);
    $result->execute(array($codsuc, $nroinscripcion));
    
    //INSERTAR OCURRENCIA
    //$observacion = "CIERRE DE SERVICIO ";
    //$clsFunciones->InsertOcurrencia($codemp,$codsuc,$codtipoocurrencia,$codcliente,$nroinscripcion,'','',$_POST["fechacorte"],$observacion,$codusu,1);
}
if ($Op == 2) {
    $Sql = "UPDATE medicion.cierreyapertura  SET   observacion = :observacion
        WHERE codemp = :codemp AND  codsuc = :codsuc AND  codciclo = :codciclo AND nroinscripcion = :nroinscripcion
        AND  anio = :anio AND  mes = :mes AND  tipooperacion = :tipooperacion;";

    $result = $conexion->prepare($Sql);
    $result->execute(array(":codemp" => 1,
        ":codsuc" => $codsuc,
        ":codciclo" => $codciclo,
        ":nroinscripcion" => $nroinscripcion,
        ":anio" => $anio,
        ":mes" => $mes,
        ":tipooperacion" => 0,
        ":observacion" => isset($_POST["Obs"]) ? $_POST["Obs"] : ''));
}
if ($Op == 3) {
    
    //ANULACION DE CIERRE DE SERVICIO
    $sqlD = "DELETE FROM medicion.cierreyapertura WHERE codemp=1 and codsuc=:codsuc and codciclo=:codciclo and
	anio=:anio and mes=:mes and tipooperacion=0 AND nroinscripcion=:nroinscripcion";

    $result = $conexion->prepare($sqlD);
    $result->execute(array(":codsuc" => $codsuc, ":codciclo" => $codciclo, ":anio" => $anio, ":mes" => $mes, ":nroinscripcion" => $nroinscripcion));

    $sqlU = "UPDATE catastro.clientes set codestadoservicio=1 WHERE codsuc=? and nroinscripcion=?";
    $result = $conexion->prepare($sqlU);
    $result->execute(array($codsuc, $nroinscripcion));

}
//$conexion->rollBack();
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    echo $res = 2;
} else {
    $conexion->commit();
    $mensaje = "El Registro se ha Grabado Correctamente";
    echo 1;
}

?>

