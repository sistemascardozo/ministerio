<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include('../../../objetos/clsFunciones.php');
//ini_set("display_errors",1);


$contador = $_POST["cont"];
$codsuc = $_POST["codsuc"];
$codciclo = $_POST["codciclo"];
$anio = $_POST["anio"];
$mes = $_POST["mes"];

if ($_POST["codsector"] != "%") {
    $Csec = " AND codsector=".$_POST["codsector"];
} else {
    $Csec = "";
}
$conexion->beginTransaction();
$sqlD = "delete from medicion.cierreyapertura where codemp=1 and codsuc=:codsuc and codciclo=:codciclo and
			 anio=:anio and mes=:mes and tipooperacion=0 ".$Csec;

$result = $conexion->prepare($sqlD);
$result->execute(array(":codsuc" => $codsuc, ":codciclo" => $codciclo, ":anio" => $anio, ":mes" => $mes));

for ($i = 1; $i <= $contador; $i++) {//echo "Holaaa";
    if (isset($_POST["nroinscripcion_corte".$i])) {
        //echo "-->".$_POST["nroinscripcion_corte".$i]."<br>";
        $sqlI = "insert into medicion.cierreyapertura(codemp,codsuc,codciclo,nroinscripcion,anio,mes,tipooperacion,codtipocorte,
					 fecha,codinspector,codmotivocorte,tipopersonal,observacion,codsector) values(:codemp,:codsuc,:codciclo,:nroinscripcion,
					 :anio,:mes,:tipooperacion,:codtipocorte,:fecha,:codinspector,:codmotivocorte,:tipopersonal,:observacion,:codsector)";

        $result = $conexion->prepare($sqlI);
        $result->execute(array(":codemp" => 1,
            ":codsuc" => $codsuc,
            ":codciclo" => $codciclo,
            ":nroinscripcion" => $_POST["nroinscripcion_corte".$i],
            ":anio" => $anio,
            ":mes" => $mes,
            ":tipooperacion" => 0,
            ":codtipocorte" => $_POST["tipo_corte".$i],
            ":fecha" => $_POST["fecha_corte".$i],
            ":codinspector" => $_POST["inpector_corte".$i],
            ":codmotivocorte" => $_POST["motivo_corte".$i],
            ":tipopersonal" => $_POST["tipo_presonal_corte".$i],
            ":observacion" => isset($_POST["observacion_corte".$i]) ? $_POST["observacion_corte".$i] : '',
            ":codsector" => $_POST['codsector'.$i]));

        $sqlU = "update catastro.clientes set codestadoservicio=2 where codsuc=? and nroinscripcion=?";
        $result = $conexion->prepare($sqlU);
        $result->execute(array($codsuc, $_POST["nroinscripcion_corte".$i]));
        //die();
    }
}
//$conexion->rollBack();
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    echo $res = 2;
} else {
    $conexion->commit();
    $mensaje = "El Registro se ha Grabado Correctamente";
    echo 1;
}
?>

