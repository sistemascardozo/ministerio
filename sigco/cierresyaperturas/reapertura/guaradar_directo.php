<?php

    session_name("pnsu");
    if (!session_start()) {
        session_start();
    }

    include('../../../objetos/clsFunciones.php');
    //ini_set("display_errors",1);
    $clsFunciones = new clsFunciones();
    $contador = $_POST["cont"];
    $codsuc = $_POST["codsuc"];
    $codciclo = $_POST["codciclo"];
    $anio = $_POST["anio"];
    $mes = $_POST["mes"];
    $nroinscripcion = $_POST["nroinscripcion"];
    $Op = $_POST["Op"];
    $nromed = $_POST["nromed"];
    $codusu = $_POST["codusu"];
    $tpservicio = $_POST["tpservicio"];
    $codemp = 1;
    $codtipoocurrencia = 1;
    $codcliente = 0;
    $conexion->beginTransaction();
    if ($Op == 1) {
        $sqlD = "DELETE FROM medicion.cierreyapertura WHERE codemp=1 AND codsuc=:codsuc AND codciclo=:codciclo AND
            anio=:anio AND mes=:mes AND tipooperacion=1 AND nroinscripcion=:nroinscripcion";

        $result = $conexion->prepare($sqlD);
        $result->execute(array(":codsuc" => $codsuc, ":codciclo" => $codciclo, ":anio" => $anio, ":mes" => $mes, ":nroinscripcion" => $nroinscripcion));


        $sqlI = "INSERT INTO medicion.cierreyapertura(codemp,codsuc,codciclo,nroinscripcion,anio,mes,tipooperacion,codtipocorte,
            fecha, codinspector, codmotivocorte, tipopersonal, observacion, codsector, codusu, nromed, tiposervicio) 
            VALUES(:codemp,:codsuc,:codciclo,:nroinscripcion, :anio,:mes,:tipooperacion,:codtipocorte,:fecha,
            :codinspector, :codmotivocorte, :tipopersonal, :observacion, :codsector, :codusu, :nromed, :tpservicio)";
        $result = $conexion->prepare($sqlI);
        $result->execute(array(":codemp" => 1,
            ":codsuc" => $codsuc,
            ":codciclo" => $codciclo,
            ":nroinscripcion" => $nroinscripcion,
            ":anio" => $anio,
            ":mes" => $mes,
            ":tipooperacion" => 1,
            ":codtipocorte" => $_POST["codtipocorte"],
            ":fecha" => $_POST["fechacorte"],
            ":codinspector" => $_POST["codinspector"],
            ":codmotivocorte" => $_POST["codmotivocorte"],
            ":tipopersonal" => $_POST["codtipopersonal"],
            ":observacion" => isset($_POST["observacion"]) ? $_POST["observacion"] : '',
            ":codsector" => $_POST['codsector'],
            ":codusu" => $codusu,
            ":nromed" => $nromed,
            ":tpservicio" => $tpservicio
        ));

        $sqlU = "UPDATE catastro.clientes set codestadoservicio=1 WHERE codsuc=? AND nroinscripcion=?";
        $result = $conexion->prepare($sqlU);
        $result->execute(array($codsuc, $nroinscripcion));

        //$observacion = "REAPERTURA DE SERVICIO ";
        //$clsFunciones->InsertOcurrencia($codemp,$codsuc,$codtipoocurrencia,$codcliente,$nroinscripcion,'','',$_POST["fechacorte"],$observacion,$codusu,1);
    }
    
    if ($Op == 2) {
        $Sql = "UPDATE medicion.cierreyapertura  SET   observacion = :observacion
            WHERE codemp = :codemp AND  codsuc = :codsuc AND  codciclo = :codciclo AND nroinscripcion = :nroinscripcion
            AND  anio = :anio AND  mes = :mes AND  tipooperacion = :tipooperacion;";

        $result = $conexion->prepare($Sql);
        $result->execute(array(":codemp" => 1,
            ":codsuc" => $codsuc,
            ":codciclo" => $codciclo,
            ":nroinscripcion" => $nroinscripcion,
            ":anio" => $anio,
            ":mes" => $mes,
            ":tipooperacion" => 1,
            ":observacion" => isset($_POST["Obs"]) ? $_POST["Obs"] : ''));
    }
    if ($Op == 3) {
        
        $sqlD = "DELETE FROM medicion.cierreyapertura WHERE codemp=1 AND codsuc=:codsuc AND codciclo=:codciclo AND
            anio=:anio AND mes=:mes AND tipooperacion=1 AND nroinscripcion=:nroinscripcion ";

        $result = $conexion->prepare($sqlD);
        $result->execute(array(":codsuc" => $codsuc, ":codciclo" => $codciclo, ":anio" => $anio, ":mes" => $mes, ":nroinscripcion" => $nroinscripcion));

        $sqlU = "UPDATE catastro.clientes set codestadoservicio=2 WHERE codsuc=? AND nroinscripcion=?";
        $result = $conexion->prepare($sqlU);
        $result->execute(array($codsuc, $nroinscripcion));

        //$observacion = "ANULACION DE CIERRE DE SERVICIO ";
        //$clsFunciones->InsertOcurrencia($codemp,$codsuc,$codtipoocurrencia,$codcliente,$nroinscripcion,'','',date('Y-m-d'),$observacion,$codusu,1);
    }
    //$conexion->rollBack();
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo 1;
    }

?>

