<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	
	$sql = "select d.nropago,c.fechareg,t.descripcion,d.detalle,sum(importe) as importe,d.categoria
			from cobranza.detpagos as d
			inner join cobranza.cabpagos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
			and d.nroinscripcion=c.nroinscripcion and d.nropago=c.nropago)
			inner join public.tipodeuda as t on(d.codtipodeuda=t.codtipodeuda)
			where d.codemp=1 and d.codsuc=? and d.nroinscripcion=?
			group by d.nropago,c.fechareg,t.descripcion,d.detalle,d.categoria order by c.fechareg desc";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroinscripcion));
	$items = $consulta->fetchAll();
	
?>
<link href="../../../css/estilos.css" rel="stylesheet" type="text/css">
<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetpagos" rules="all" >
<thead class="ui-widget-header" >
	<tr>
    <td width="5%" align="center">Nro. Pago</td>
    <td width="5%" align="center">Fecha</td>
    <!-- <td width="30%">Tipo de Deuda</td> -->
    <td width="12%" align="center">Detalle</td>
    <!-- <td width="12%" align="center">Categoria</td> -->
    <td width="5%" align="center">Importe</td>
  </tr>
</thead>
<tbody>
 <?php
 	foreach($items as $row)
	{	
 ?>
 	<tr align="center">
        <td  align="center"><?=$row["nropago"]?></td>
        <td align="center"><?=$objFunciones->DecFecha($row["fechareg"])?></td>
        <!-- <td  align="center"><?=$row["descripcion"]?></td> -->
        <td align="left"><?=$row["detalle"]?></td>
        <!-- <td  align="center"><?=$categoria[$row["categoria"]]?></td> -->
        <td  align="right"><?=number_format($row["importe"],2)?></td>
      </tr>
 <?php } ?>
 </tbody>
</table>