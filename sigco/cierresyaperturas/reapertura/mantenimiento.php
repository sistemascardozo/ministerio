<?php
    include("../../../include/main.php");
    include("../../../include/claseindex.php");
    include("../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();
    
    $TituloVentana = "REAPERTURA DE SERVICIOS";
    $Activo = 1;
    CuerpoSuperior($TituloVentana);
    $objMantenimiento = new clsDrop();

    $anio = $_GET["anio"];
    $mes = $_GET["mes"];
    $ciclo = $_GET["ciclo"];
    $sector = "%".$_GET["sector"]."%";
    if ($_GET["sector"] != "%") {
        $Csec = " AND clie.codsector=".$_GET["sector"];
    } else {
        $Csec = "";
    }
    $codsuc = $_GET["codsuc"];
    $inspectores = $_GET["inspectores"];
    $fcorte = $_GET["fcorte"] ? $_GET["fcorte"] : date('d/m/Y');
    $tipopersonal = $_GET["tipopersonal"];
    $tipocorte = $_GET["tipocorte"];
    $motivocorte = $_GET["motivocorte"];
    $codusu = $_SESSION['id_user'];
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_cierre.js" language="JavaScript"></script>
<style type="text/css">
    .TipoPersonal, .NroInscripcion,.MotivoCorte,.CodInspector,.TipoCorte,.Obs,.Sectorn,.NroMedidorn,.Direccionn,.CodSector { display: none;}

</style>
<script type="text/javascript" >
    var codsuc = <?=$codsuc ?>;
    var codusu = <?=$codusu ?>;
    $(function () {
        $("#motivocorte").val(<?=$motivocorte ?>);
        $("#DivCuentaCorriente").dialog({
            autoOpen: false,
            height: 600,
            width: 1000,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#DivCuentaCorriente").dialog("close");

                }
            },
            close: function () {
                $(ObjTr).removeClass('ui-widget-header');
            }
        });
    });
    function ValidarForm(Op)
    {
        var id = parseInt($("#tbcierreyapertura tbody tr").length)
        if (id <= 0)
        {
            Msj('#tbcierreyapertura', 'No hay Registros para realizar el cierre del servicio', 1000, 'above', 'error', false)
            return false;
        }
        var tr = ''
        var tipo_corte, nroinscripcion_corte, fecha_corte, inpector_corte, motivo_corte, tipo_presonal_corte, observacion_corte = ''
        for (var i = 1; i <= id; i++)
        {

            nroinscripcion_corte = $("#tbcierreyapertura tbody tr#corte"+i+" label.NroInscripcion").text()
            tipo_corte = 0
            fecha_corte = $("#tbcierreyapertura tbody tr#corte"+i+" label.FechaCorte").text()
            inpector_corte = $("#tbcierreyapertura tbody tr#corte"+i+" label.CodInspector").text()
            motivo_corte = $("#tbcierreyapertura tbody tr#corte"+i+" label.MotivoCorte").text()
            tipo_presonal_corte = $("#tbcierreyapertura tbody tr#corte"+i+" label.TipoPersonal").text()
            observacion_corte = $("#tbcierreyapertura tbody tr#corte"+i+" label.Obs").text()
            codsector = $("#tbcierreyapertura tbody tr#corte"+i+" label.CodSector").text()

            tr += '<input type="hidden" name="nroinscripcion_corte'+i+'"  value="'+nroinscripcion_corte+'"/>';
            tr += '<input type="hidden" name="tipo_corte'+i+'"  value="'+tipo_corte+'"/>';
            tr += '<input type="hidden" name="fecha_corte'+i+'"  value="'+fecha_corte+'"/>';
            tr += '<input type="hidden" name="inpector_corte'+i+'"  value="'+inpector_corte+'"/>';
            tr += '<input type="hidden" name="motivo_corte'+i+'"  value="'+motivo_corte+'"/>';
            tr += '<input type="hidden" name="tipo_presonal_corte'+i+'"  value="'+tipo_presonal_corte+'"/>';
            tr += '<input type="hidden" name="observacion_corte'+i+'"  value="'+observacion_corte+'"/>';
            tr += '<input type="hidden" name="codsector'+i+'"  value="'+codsector+'"/>';

        }
        tr += '<input type="hidden" name="cont" id="cont"  value="'+id+'"/>';
        $("#DivSave").html(tr)
        GuardarP(Op);
        return false
    }
    function GuardarP(Op)
    {

        $.ajax({
            url: 'Guardar.php?Op='+Op,
            type: 'POST',
            async: true,
            data: $('#form1').serialize(), //+'&0form1_idusuario=<?=$IdUsuario ?>&3form1_fechareg=<?=$Fecha ?>',
            success: function (data) {
                // location.href='index.php';	
                // location.reload(true);
                var myUrl = window.location

                myUrl = myUrl+"&inspectores="+$("#inspectores").val();
                myUrl = myUrl+"&motivocorte="+$("#motivocorte").val();
                myUrl = myUrl+"&fcorte="+$("#fcorte").val();
                myUrl = myUrl+"&tipopersonal="+$("#tipopersonal").val();
                myUrl = myUrl+"&tipocorte="+$("#tipocorte").val();
                window.location = myUrl;

            }
        })
    }


    function Cancelar()
    {
        location.href = 'index.php';
    }
    function CargarSector()
    {
        if ($("#codsector").val() == 0)
        {
            Msj($("#codsector"), 'Seleccione Sector')
            return false
        }
        location.href = 'mantenimiento.php?anio=<?=$_GET["anio"] ?>&mes=<?=$_GET["mes"] ?>&ciclo=<?=$_GET["ciclo"] ?>&sector='+$("#codsector").val()+'&codsuc=<?=$_GET["codsuc"] ?>&ciclotext=<?=$_GET["ciclotext"] ?>';

    }
    function CuentaCorriente(nroinscripcion, obj)
    {
        $("#nroinscripcion").val(nroinscripcion)
        ObjTr = $(obj).parent().parent();
        $(ObjTr).addClass('ui-widget-header');
        $("#detalle-CuentaCorriente").empty().html('<span class="icono-icon-loading">&nbsp;&nbsp;ConsultANDo...</span>');
        $("#DivCuentaCorriente").dialog('open');
        //AbrirPopupImpresion("<?php echo $_SESSION['urldir']; ?>sigco/catastro/listados/cuentacorriente/Pdf.php?NroInscripcion="+nroinscripcion,Tam[0],Tam[1])
        $("#nroinscripcion").val(nroinscripcion)
        $.ajax({
            url: '<?php echo $_SESSION['urldir']; ?>sigco/catastro/listados/cuentacorriente/Consulta.php',
            type: 'POST',
            async: true,
            data: 'NroInscripcion='+nroinscripcion,
            success: function (datos)
            {

                $("#detalle-CuentaCorriente").empty().append(datos);


            }
        })
    }
</script>
<div align="center">
    <form id="form1" name="form1">
        <table width="1100" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="2" class="TitDetalle" style="height:4px"></td>
                </tr>
                <tr>
                    <td colspan="2" class="TitDetalle">
                        <table width="90%" border="0" cellspacing="0" cellpadding="0">
                            <tr style="font-size:12px; font-weight:bold">
                                <td width="9%" align="right">Ciclo</td>
                                <td width="2%" align="center">:</td>
                                <td width="18%"><?=$_GET["ciclotext"] ?>
                                    <input type="hidden" name="codsuc" value="<?=$codsuc ?>" />
                                    <input type="hidden" name="codciclo" id="codciclo" value="<?=$ciclo ?>" />
                                    <input type=hidden id=nroinscripcion>
                                </td>
                                <td width="6%" align="right">A&ntilde;o</td>
                                <td width="2%" align="center">:</td>
                                <td width="21%"><?=$anio ?>
                                    <input type="hidden" name="anio" value="<?=$anio ?>" id="anio" /></td>
                                <td width="8%" align="right">Mes</td>
                                <td width="2%" align="center">:</td>
                                <td width="28%"><?=$meses[$mes] ?><div id="DivSave"></div><input id="sector" name="sector" value="<?=$_GET["sector"] ?>" type="hidden">
                                    <input type="hidden" name="mes" value="<?=$mes ?>" id="mes" /></td>
                                <td>Sectror&nbsp;:&nbsp;</td>
                                <td><?php echo $objMantenimiento->drop_sectores2($codsuc, $_GET["sector"], 'onchange="CargarSector()"'); ?></td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="TitDetalle">
                        <fieldset>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr style="padding:4px">
                                    <td width="6%">Inspector</td>
                                    <td width="2%" align="center">:</td>
                                    <td width="25%"><?php $objMantenimiento->drop_inspector($codsuc,$inspectores); ?></td>
                                    <td width="13%" align="right">M. de Reapertura</td>
                                    <td width="2%" align="center">:</td>
                                    <td width="17%"><?php $objMantenimiento->drop_motivo_corte(1); ?></td>
                                    <td width="13%" align="right">Fecha de Reapertura</td>
                                    <td width="2%" align="center">:</td>
                                    <td width="18%">
                                        <input type="text" name="fcorte" id="fcorte" maxlength="15" class="inputtext" value="<?=$fcorte ?>" style="width:80px;" />
                                    </td>
                                </tr>
                                <tr style="padding-left:4px">
                                    <td>Personal</td>
                                    <td align="center">:</td>
                                    <td>
                                        <select name="tipopersonal" id="tipopersonal" style="width:220px" class="SELECT" >
                                            <option value="0">PERSONAL PROPIO</option>
                                            <option value="1">PERSONAL SERVICE</option>
                                        </select>                                        
                                    </td>
                                    <td align="right">Tipo de Corte</td>
                                    <td align="center">:</td>
                                    <td><?php $objMantenimiento->drop_tipo_corte($tipocorte); ?></td>
                                    <td align="right">&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>	
                <tr>
                    <td colspan="2" class="TitDetalle">
                        <div id="clientescierre" style="overflow:auto; height:150px">
                            <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbcierre" rules="all" >
                                <thead class="ui-widget-header" >
                                    <tr  align="center">
                                        <td width="80">Cod. Catastral </td>
                                        <td width="80">Inscrip.</td>
                                        <td>Propietario</td>
                                        <td>Direccion</td>
                                        <td width="75">Sector</td>
                                        <td width="105">Reapert. Serv.</td>
                                        <td width="30">&nbsp;</td>
                                        <td width="30">&nbsp;</td>
                                        <td width="30">&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //Ver el Periodo
                                    $VerF= $objFunciones->datosfacturacion($codsuc, $ciclo);
                                    $NroFact=$VerF['nrofacturacion'] -1;
                                    
                                    $VerP= "SELECT pe.anio, pe.mes FROM facturacion.periodofacturacion AS pe
                                        WHERE pe.nrofacturacion=".$NroFact;
                                    $consulta = $conexion->prepare($VerP);
                                    $consulta->execute(array());
                                    $it = $consulta->fetch();
                                    
                                    $Mes = $it['mes'];
                                    $Anio= $it['anio'];
                                    
                                    $VerCie= "SELECT  cie.nroinscripcion, cie.fecha
                                        FROM medicion.cierreyapertura AS cie
                                        WHERE
                                        cie.anio='$Anio' AND cie.mes='$Mes' AND cie.tipooperacion=0";
                                    $consulta = $conexion->prepare($VerCie);
                                    $consulta->execute(array());
                                    $item = $consulta->fetchAll();
                                    $MesSig= $Mes+1;
                                    $MesSig = str_pad($MesSig, 2, '0', STR_PAD_LEFT);
                                    $Anio= date('Y');
                                    $FechaUlt= $Anio.'-'.$MesSig.'-07';
                                    $cont = 0;
                                    foreach ($item as $rw) {
                                        
                                        $fecha= $rw['fecha'];
                                        //$sql .= "FROM catastro.view_clientes AS clie ";
                                        $sqlPag = "SELECT DISTINCT clie.nroinscripcion, clie.codcliente, clie.propietario AS propietario, sect.descripcion AS sector, 
                                            tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle AS direccion, 
                                            clie.codemp, clie.codsuc, clie.codsector, clie.codmanzanas, clie.lote, clie.sublote, clie.nromed, clie.codestadoservicio, 
                                            to_char(clie.codsuc, '00') || '-' || TRIM(to_char(clie.codzona, '00')) || '-' || TRIM(to_char(clie.codsector, '00')) || '-' || TRIM(to_char(CAST(clie.codmanzanas AS INTEGER), '000')) || '-' || TRIM(to_char(CAST(clie.lote AS INTEGER), '0000')) || '-' || TRIM(clie.sublote) AS codcatastro , 
                                            clie.codsector, clie.codantiguo, clie.nroorden
                                            FROM cobranza.cabpagos AS ca 
                                            INNER JOIN cobranza.detpagos AS de ON de.codemp = ca.codemp AND de.codsuc = ca.codsuc AND de.nroinscripcion = ca.nroinscripcion AND de.nropago = ca.nropago 
                                            INNER JOIN catastro.clientes AS clie ON clie.nroinscripcion = ca.nroinscripcion AND clie.codemp = ca.codemp AND clie.codsuc = ca.codsuc 
                                            INNER JOIN public.calles cal ON(clie.codemp = cal.codemp) AND (clie.codsuc = cal.codsuc) AND (clie.codcalle = cal.codcalle) AND (clie.codzona = cal.codzona) 
                                            INNER JOIN public.tiposcalle tipcal ON(cal.codtipocalle = tipcal.codtipocalle) 
                                            INNER JOIN public.sectores sect ON(clie.codemp = sect.codemp) AND (clie.codsuc = sect.codsuc) AND (clie.codzona = sect.codzona) AND (clie.codsector = sect.codsector) ";
                                        $sqlPag .= " WHERE clie.codsuc = ".$codsuc." AND ca.fechareg BETWEEN '$fecha' AND '$FechaUlt' ";
                                        $sqlPag .= " AND clie.codciclo = ".$ciclo." ".$Csec." AND clie.nroinscripcion= ".$rw['nroinscripcion'];
                                        $sqlPag .= " ORDER BY clie.nroorden";

                                        $conn = $conexion->prepare($sqlPag);
                                        $conn->execute(array());
                                        $row = $conn->fetch();

                                        if ($row['nroinscripcion']!=''){
                                            $cont = $cont+1;
                                            ?>         
                                            <tr id="<?=$cont ?>" onclick="SeleccionaId(this);" style="font-size:10px;">
                                                <td align="center">
                                                    <label class="CodCatastral"><?=$row["codcatastro"] ?></label>
                                                    <label class="NroInscripcion"><?=$row["nroinscripcion"] ?></label>
                                                </td>
                                                <td align="center">
                                                    <label class="Codantiguo"><?=strtoupper($row['codantiguo']) ?></label>
                                                </td>
                                                <td style="padding-left:5px;">
                                                    <label class="Propietario"><?=strtoupper($row[2]) ?></label>
                                                </td>
                                                <td style="padding-left:5px;">
                                                    <label class="Direccion"><?=strtoupper($row['direccion']) ?></label>
                                                </td>
                                                <td align="center">
                                                    <label class="Sector"><?=strtoupper($row['sector']) ?></label>
                                                    <label class="CodSector"><?=strtoupper($row['codsector']) ?></label>
                                                </td>
                                                <td align="center">
                                                    <select id="tiposervicio-<?=$row["nroinscripcion"] ?>" name="tiposervicio" class="select" style="width:100px">
                                                        <option value="A">AGUA</option>
                                                        <option value="A/D">AGUA Y DESAGUE</option>
                                                        <option value="D">DESAGUE</option>
                                                        <option value="MATRIZ A">MATRIZ AGUA</option>
                                                        <option value="MATRIZ D">MATRIZ DESAGUE</option>
                                                        <option value="MATRIZ A/D">MATRIZ AGUA Y DESAGUE</option>
                                                    </select>
                                                </td>
                                                <td align="center">
                                                    <span class="icono-icon-add Upd" onclick="agregar_corte(<?=$cont ?>, '<?=$row["nroinscripcion"] ?>', '<?=$row["codcatastro"] ?>', '<?=str_replace(array('"', "'"), '', strtoupper($row['propietario'])) ?>', '<?=str_replace(array('"', "'"), '', strtoupper($row['direccion'])) ?>', '<?=$row["sector"] ?>', '<?=$row["nromed"] ?>', '<?=$row["codsector"] ?>', '<?=$row["codantiguo"] ?>')"  title="Agregar Usuario para Apertura de Servicio" ></span>
                                                </td>
                                                <td align="center">
                                                    <span class="icono-icon-ok" title="Ver Pagos de Usuario" onclick="abrir_popu_pagos(<?=$row["nroinscripcion"] ?>);"  ></span>
                                                </td>
                                                <td align="center">
                                                    <span class="icono-icon-dinero" title="Ver Cuenta Corriente" onclick="CuentaCorriente(<?=$row['nroinscripcion'] ?>, this)"></span>
                                                </td>
                                            </tr>
                                        <?php 
                                        
                                        } 
                                    } //echo $cont;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="TitDetalle">
                        <fieldset style="padding:4px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td align="center">
                                        Buscar:&nbsp;<input id="Filtro" class="inputtext" value="" maxlength="30" size="30" type="text" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="TitDetalle">
                        <div id="clientescierre" style="overflow:auto; height:150px">
                            <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbcierreyapertura" rules="all" >
                                <thead class="ui-widget-header" >
                                    <tr  align="center">
                                        <td width="11%" height="23">Cod. Catastral </td>
                                        <td width="10%">Inscripcion</td>
                                        <td width="23%">Propietario</td>
                                        <td width="10%">Fecha</td>
                                        <td width="13%">Motivo</td>
                                        <td width="15%">Inspector</td>
                                        <td width="19%">Personal</td>
                                        <td width="3%">&nbsp;</td>
                                        <td width="3%">&nbsp;</td>
                                        <td width="3%">&nbsp;</td>
                                        <td width="3%">&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $cont_1 = 0;

                                    $sqlD = "SELECT cierr.nroinscripcion,clie.propietario, ";
                                    $sqlD .= "cierr.fecha,motcorte.descripcion as motivo,tipcorte.descripcion as tipocorte,inspec.nombres as inspectores, ";
                                    $sqlD .= "case when cierr.tipopersonal=0 then 'PERSONAL PROPIO' else 'PERSONAL SERVICE' end as personal, ";
                                    $sqlD .= "cierr.nroinscripcion,cierr.observacion,cierr.codmotivocorte,cierr.codtipocorte,cierr.codinspector,cierr.tipopersonal,
						 t.descripcioncorta || ' ' || c.descripcion || ' ' || clie.nrocalle as direccion,clie.nromed, ";
                                    $sqlD .= $objMantenimiento->getCodCatastral("clie.").",s.descripcion as sector ,clie.codsector,clie.codantiguo
						FROM medicion.cierreyapertura as cierr ";
                                    $sqlD .= " INNER JOIN catastro.clientes as clie on(cierr.codemp=clie.codemp AND cierr.codsuc=clie.codsuc ";
                                    $sqlD .= "AND cierr.nroinscripcion=clie.nroinscripcion) ";
                                    $sqlD .= "INNER JOIN public.motivocorte as motcorte on(cierr.codmotivocorte=motcorte.codmotivocorte) ";
                                    $sqlD .= "INNER JOIN public.tipocorte as tipcorte on(cierr.codtipocorte=tipcorte.codtipocorte) ";
                                    $sqlD .= "INNER JOIN reglasnegocio.inspectores as inspec on(cierr.codemp=inspec.codemp AND cierr.codsuc=inspec.codsuc AND cierr.codinspector=inspec.codinspector)
                                        INNER JOIN public.calles as c on(clie.codemp=c.codemp AND clie.codsuc=c.codsuc AND clie.codcalle=c.codcalle AND clie.codzona=c.codzona)
                                        INNER JOIN public.tiposcalle as t on(c.codtipocalle=t.codtipocalle)
                                        INNER JOIN public.sectores as s on(clie.codemp=s.codemp AND clie.codsuc=s.codsuc AND clie.codzona=s.codzona AND clie.codsector=s.codsector)";
                                    $sqlD .= " WHERE cierr.codemp=1 AND cierr.codsuc=:codsuc AND cierr.codciclo=:codciclo AND cierr.anio=:anio AND cierr.mes=:mes ";
                                    $sqlD .= " AND cierr.tipooperacion=1  ".$Csec." order by codcatastro ";

                                    $consultaD = $conexion->prepare($sqlD);
                                    $consultaD->execute(array(":codsuc" => $codsuc, ":codciclo" => $ciclo, ":anio" => $anio, ":mes" => $mes));
                                    $itemsD = $consultaD->fetchAll();

                                    foreach ($itemsD as $rowD) {
                                        $cont_1 = $cont_1+1;
                                        ?>
                                        <tr id="corte<?=$cont_1 ?>" onclick="SeleccionaId(this);">
                                            <td align="center">
                                                <label class="TipoPersonal"><?=strtoupper($rowD['tipopersonal']) ?></label>
                                                <label class="MotivoCorte"><?=strtoupper($rowD['codmotivocorte']) ?></label>
                                                <label class="CodInspector"><?=strtoupper($rowD['codinspector']) ?></label>
                                                <label class="CodSector"><?=strtoupper($rowD['codsector']) ?></label>

                                                <label class="TipoCorte"><?=strtoupper($rowD['codtipocorte']) ?></label>

                                                <label class="CodCatastral"><?=$rowD["codcatastro"] ?></label>
                                                <label class="NroInscripcion"><?=$rowD["nroinscripcion"] ?></label>
                                                <label class="Sectorn"><?=$rowD["sector"] ?></label> 
                                                <label class="NroMedidorn"><?=$rowD["nromed"] ?></label> 
                                                <label class="Direccionn"><?=$rowD["direccion"] ?></label> 
                                            </td>
                                            <td align="center">
                                                <label class="Codantiguo"><?=strtoupper($rowD['codantiguo']) ?></label>
                                            </td>
                                            <td>
                                                <label class="Propietario"><?=strtoupper($rowD['propietario']) ?></label>
                                            </td>
                                            <td align="center">
                                                <label class="FechaCorte"><?=$objMantenimiento->DecFecha($rowD["fecha"]) ?></label>
                                            </td>
                                            <td align="center">
                                                <label class="Motivo"><?=strtoupper($rowD["motivo"]) ?></label></td>

                                            <td>
                                                <label class="Inspector"><?=strtoupper($rowD["inspectores"]) ?></label></td>
                                            <td>
                                                <label class="Personal"><?=strtoupper($rowD["personal"]) ?></label></td>
                                            <td>
                                                <label class="Obs"><?=strtoupper($rowD["observacion"]) ?></label>
                                                <a href="javascript:agregar_observacion_corte(<?=$cont_1 ?>)" class="Upd" >
                                                    <span title="Agregar Observacion de Reapertura" class="MljSoft-icon-refresh"></span>
                                                </a>

                                            </td>
                                            <td align="center">
                                                <span title="Ver Pagos de Usuario"  onclick='abrir_popu_pagos(<?=$rowD["nroinscripcion"] ?>);' class="icono-icon-detalle"></span>
                                            </td>
                                            <td align="center">
                                                <a href="javascript:agregar_usuarios(<?=$cont_1 ?>,'<?=$rowD["nroinscripcion"] ?>','<?=$rowD["codcatastro"] ?>','<?=str_replace(array('"', "'"), '', strtoupper($rowD['propietario'])) ?>','<?=str_replace(array('"', "'"), '', strtoupper($rowD['direccion'])) ?>','<?=$rowD["sector"] ?>','<?=$rowD["nromed"] ?>','<?=$rowD["codantiguo"] ?>')" class="Del" >
                                                    <span class="icono-icon-trash" title="Borrar Registro" ></span>
                                                </a>
                                            </td>
                                            <td align="center">
                                                <span class="icono-icon-dinero" title="Ver Cuenta Corriente" onclick="CuentaCorriente(<?=$rowD['nroinscripcion'] ?>, this)"></span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
            <td colspan="2" style="background-image:url(<?php echo $_SESSION['urldir']; ?>images/Pie_Index.png); padding:4px; height:45px">
                <table border="0" width="100%">
                    <tbody><tr>
                            <td align="left">

                            </td>
                            <td align="right">
                                <input type="button" onclick="Cancelar();" value="Cancelar" id="btnCancelar" class="button ui-button ui-widget ui-state-default ui-corner-all" name="btnCancelar" role="button" aria-disabled="false">
                            </td>
                        </tr>
                    </tbody></table>
            </td>
            </tfoot>
        </table>
    </form>
</div>
<div id="DivHistorial" title="Historial de Pagos"  >
    <div id="div_historial"></div>
</div>
<div id="dialog-form-observcion" title="Agregar Observacion de Corte"  >
    <input type="hidden" id="ItemUpd">
    <textarea name="observacion" id="observacion" cols="60" rows="5" class="inputtext"></textarea>
</div>
<div id="DivCuentaCorriente" title="Ver Cuenta Corriente"  >
    <div id="detalle-CuentaCorriente">
    </div>
</div>
<?php CuerpoInferior(); ?>