<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsMantenimiento();
	if($Id!='')
	{
		$sql = "select * from 	public.tipoot where codtipoot=?";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($Id));
		$row = $consulta->fetch();
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if ($("#Nombre").val() == '')
		{
			alert('La Descripcion del Tipo no puede ser NULO');
			$("#Nombre").focus();
			return false;
		}
		if (document.form1.Abreviatura.value == '')
		{
			alert('La Abreviatura del Tipo no puede ser NULO');
			document.form1.Abreviatura.focus();
			return false;
		}
		if (document.form1.descorta.value == '')
		{
			alert('La Descripcion Corta del Tipo no puede ser NULO');
			document.form1.descorta.focus();
			return false;
		}
		if (document.form1.correlativo.value == '')
		{
			alert('El Correlativo del Tipo no puede ser NULO');
			document.form1.correlativo.focus();
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr><td colspan="3"></td></tr>
	<tr>
        <td class="TitDetalle">Id</td>
        <td width="30" align="center">:</td>
        <td class="CampoDetalle">
		<input name="codtipoot" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/></td>
	</tr>
	
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Nombre" maxlength="200" value="<?php echo $row["descripcion"];?>" style="width:400px;"/></td>
	  </tr>
	  <tr>
	  <td class="TitDetalle">Abreviatura</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="abreviatura" type="text" id="Abreviatura" maxlength="200" value="<?php echo $row["abreviatura"];?>" style="width:100px;"/></td>
	  </tr>
	  <tr>
	  <td class="TitDetalle">Descrip. Corta</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descorta" type="text" id="descorta" maxlength="200" value="<?php echo $row["descorta"];?>" style="width:200px;"/></td>
	  </tr>
	  <tr>
	  <td class="TitDetalle">Correlativo</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext entero" name="correlativo" type="text" id="correlativo" maxlength="200" value="<?php echo $row["correlativo"];?>" style="width:100px;"/></td>
	  </tr>
	  <tr>
	  <td class="TitDetalle">Cod. Barra</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="codbarra" type="text" id="codbarra" maxlength="200" value="<?php echo $row["codbarra"];?>" style="width:150px;"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	    <? include("../../../../include/estareg.php"); ?></td>
	  </tr>
	  <tr><td colspan="3"></td></tr>
          </tbody>
    </table>
 </form>
</div>
<script>
  $("#Nombre").focus();
</script>
<?
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php");
?>