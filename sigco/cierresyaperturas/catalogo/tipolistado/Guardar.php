<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codtipolistado		= $_POST["codtipolistado"];
	//$codemp			= 1;
	//$unisuc 		= $_SESSION["IdSucursal"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	$codemp=1;
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("public.tipolistado","0","0");
			$codtipolistado = $id[0];
			
			$sql = "insert into public.tipolistado(codtipolistado,descripcion,estareg) values(:codtipolistado,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipolistado"=>$codtipolistado,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipolistado set descripcion=:descripcion, estareg=:estareg 
			where codtipolistado=:codtipolistado";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipolistado"=>$codtipolistado,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipolistado set estareg=:estareg
				where codtipolistado=:codtipolistado";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipolistado"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
