<?php

include("../../../../objetos/clsReporte.php");
$objMantenimiento = new clsFunciones();

class clsRegistroVentas extends clsReporte {

    function cabecera() {
        global $m, $anio, $ciclotext;

        $this->SetFont('Arial', 'B', 14);

        $tit1 = "REGISTRO DE CORTADOS";
        $this->Cell(277, $h + 2, utf8_decode($tit1), 0, 1, 'C');

        $this->Ln(5);

        $h = 4;
        $this->SetFont('Arial', '', 7);
        $this->Cell(277, $h, $ciclotext, 0, 1, 'C');

        $this->SetX(113.5);
        $this->Cell(10, $h, "MES", 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(30, $h, $m, 0, 0, 'L');
        $this->Cell(10, $h, utf8_decode("AÑO"), 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(20, $h, $anio, 0, 1, 'L');
        $this->Ln(2);
        
    }
    
    function CabDetalleRuta($Sect, $Des) {
        global $anio, $mes, $sectores, $rutas, $Dim;

        $h2 = 6;

        $this->SetFont('Arial', 'B', 10);
        $this->Ln(4);
        $this->Cell(277, $h2, "SECTOR : ".$Sect."                    RUTA : ".$Des, 1, 1, 'L', false);
                
        $this->SetFont('Arial', '', 8);
        $this->SetWidths(array(8, 30, 18, 19, 65, 55, 18, 30, 18, 16));
                $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C","C"));
                $this->Row(array(utf8_decode('N°'),
                    "COD. CATASTRAL",
                    "SEC ",
                    "INSCRIP.",
                    "PROPIETARIO",
                    "DIRECCION",
                    "FECHA",
                    "MOTIVO",
                    "TIP. CORTE",
                    "TP. SERV."
                ));
    }
    
    function agregar_detalle($codsuc, $codciclo, $anio, $mes, $codsector, $codmotivo, $codservices){
        global $conexion, $objMantenimiento;
        
        if ($codservices== 1)
        {$codservices=0;}
        
        $h = 4;

        $count = 0;
        
        $impmes = 0;
        $impigv = 0;
        $imptotal = 0;

        $this->SetTextColor(0, 0, 0);
        $cont_1 = 0;

        $sqlD = " SELECT cierr.nroinscripcion, clie.propietario, clie.codantiguo, cierr.codsector, cierr.fecha, clie.codzona, ";
        $sqlD .= "  motcorte.descripcion as motivo, tipcorte.descripcion as tipocorte, inspec.nombres as inspectores, ";
        $sqlD .= " case when cierr.tipopersonal=0 then 'PERSONAL PROPIO' else 'PERSONAL SERVICE' end as personal, ";
        $sqlD .= " cierr.nroinscripcion,cierr.observacion,cierr.codmotivocorte,cierr.codtipocorte, cierr.codinspector, cierr.tipopersonal,
            t.descripcioncorta || ' ' || c.descripcion || ' ' || clie.nrocalle as direccion, cierr.nromed, ";
        $sqlD .= " ( clie.codsector ||' - '|| clie.codrutlecturas ||' - '|| co.orden_lect) AS secrutsecu, ";
        $sqlD .= $objMantenimiento->getCodCatastral("clie.").", cierr.tiposervicio, clie.codrutlecturas";
        $sqlD .= " FROM medicion.cierreyapertura as cierr ";
        $sqlD .= " INNER JOIN catastro.clientes as clie on(cierr.codemp=clie.codemp AND cierr.codsuc=clie.codsuc AND cierr.codsector=clie.codsector AND cierr.nroinscripcion=clie.nroinscripcion AND cierr.codsector=clie.codsector) ";
        $sqlD .= " INNER JOIN public.motivocorte as motcorte on(cierr.codmotivocorte=motcorte.codmotivocorte) ";
        $sqlD .= " INNER JOIN public.tipocorte as tipcorte on(cierr.codtipocorte=tipcorte.codtipocorte) ";
        $sqlD .= " INNER JOIN reglasnegocio.inspectores as inspec on(cierr.codemp=inspec.codemp AND cierr.codsuc=inspec.codsuc AND cierr.codinspector=inspec.codinspector)
            INNER JOIN public.calles as c on(clie.codemp=c.codemp AND clie.codsuc=c.codsuc AND clie.codcalle=c.codcalle AND clie.codzona=c.codzona)
            INNER JOIN public.tiposcalle as t on(c.codtipocalle=t.codtipocalle)
            INNER JOIN catastro.conexiones AS co ON(clie.codemp=co.codemp AND clie.codsuc=co.codsuc AND clie.nroinscripcion=co.nroinscripcion) ";
        $sqlD .= " WHERE cierr.codemp=1 AND cierr.codsuc=".$codsuc." AND cierr.codciclo=".$codciclo." 
            AND cierr.anio='".$anio."' AND cierr.mes='".$mes."' ";
        if ($codsector != "%")
            $sqlD .= " AND clie.codsector='".$codsector."' ";
        
        if ($codmotivo != 0)
            $sqlD .= " AND cierr.codmotivocorte='".$codmotivo."' ";
        
        if ($codservices != 0 || $codservices != 1)
            $sqlD .= " AND cierr.tipopersonal='".$codservices."' ";
        
        $sqlD .= " AND cierr.tipooperacion=0 ORDER BY cierr.codsector, clie.codrutlecturas, co.orden_dist ASC ";

        $consultaD = $conexion->query($sqlD);
        $itemsD = $consultaD->fetchAll();
        $CodRutaAnt=0;
        //var_dump($consulta->errorInfo());
        
        foreach ($itemsD as $row) {
            $count++;
            $CodRuta  = $row['codrutlecturas'];
            $CodSector= $row['codsector'];
            
            $VerRuta= "SELECT * FROM rutasmaelecturas WHERE codzona='".$row['codzona']."' AND codsector='".$row['codsector']."' AND codrutlecturas='".$row['codrutlecturas']."' ";
            $rwD = $conexion->query($VerRuta)->fetch();
            $Ruta = $rwD['descripcion'];
            //if($CodSectorD!=$row['codsector'])
            //{   
            if ($CodRutaAnt != $CodRuta) {
                if($CodRutaAnt!=0)
                {
                    $this->Ln(4);
                }
                
                //if (count($itemsD) > 0) {
                    $this->CabDetalleRuta($CodSector, $CodRuta);
                //}
            }
            
            $this->SetFont('Arial', '', 8);
            $this->SetWidths(array(8, 30, 18, 19, 65, 55, 18, 30, 18, 16));
            $this->SetAligns(array("C", "L", "C", "C", "L", "L", "C", "L", "C","C"));
            $this->Row(array($count,
                $row['codcatastro'],
                $row['secrutsecu'],
                $row['codantiguo'],
                utf8_decode(strtoupper($row["propietario"])),
                utf8_decode(strtoupper($row["direccion"])),
                $this->DecFecha($row["fecha"]),
                utf8_decode(strtoupper($row["motivo"])),
                utf8_decode(strtoupper($row["tipocorte"])),
                $row['tiposervicio']
            ));
            
            $CodRutaAnt= $row['codrutlecturas'];
            
        }
                
    }

}

$ciclo = $_GET["ciclo"];
$sector = $_GET["sector"];
$codsuc = $_GET["codsuc"];
$anio = $_GET["anio"];
$mes = $_GET["mes"];
$ciclotext = $_GET["ciclotext"];
$codservices = $_GET["codservices"];

$codmotivo = $_GET["codmotivo"];

$m = $meses[$mes];


$objReporte = new clsRegistroVentas();
$objReporte->AliasNbPages();
$objReporte->AddPage("L");

$objReporte->agregar_detalle($codsuc, $ciclo, $anio, $mes, $sector, $codmotivo, $codservices);

$objReporte->Output();

?>