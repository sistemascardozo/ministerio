<?php

include("../../../../objetos/clsReporte.php");
$objMantenimiento = new clsFunciones();

class clsRegistroVentas extends clsReporte {

    function cabecera() {
        global $objMantenimiento, $m, $anio, $ciclotext, $fdesde, $fhasta;

        $this->SetFont('Arial', 'B', 14);

        $tit1 = "REGISTRO DE REAPERTURADOS";
        $this->Cell(0, $h+2, utf8_decode($tit1), 0, 1, 'C');

        $this->Ln(5);

        $h = 4;
        $this->SetFont('Arial', '', 7);
        $this->Cell(170, $h, $ciclotext, 0, 0, 'C');

        $this->SetX(113.5);
        $this->Cell(10, $h, "MES", 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(30, $h, $m, 0, 0, 'L');
        $this->Cell(10, $h, utf8_decode("AÑO"), 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(20, $h, $anio, 0, 1, 'L');

        $this->SetFont('Arial', 'B', 8);
        $this->Cell(92, $h, 'FECHA CONSULTA :', 0, 0, 'R');
        $this->Cell(40, $h, $fdesde." - ".$fhasta, 0, 1, 'R');
        $this->Ln(3);

    }

    function agregar_detalle($codsuc, $codciclo, $anio, $mes, $codsector, $verfecha, $fdesde, $fhasta)
    {
        global $conexion, $objMantenimiento;

        $h = 4;

        $count = 0;

        $impmes = 0;
        $impigv = 0;
        $imptotal = 0;

        $this->SetTextColor(0, 0, 0);
        $cont_1 = 0;
        $AndSect='';

        //echo $codsector;
        if($codsector=='%')
        {
            $AndSect= $AndSect;
        }
        else {
            $AndSect= " AND cie.codsector = '".$codsector."' ";
        }
        
        
        $VerSector="SELECT DISTINCT se.codsector, se.descripcion
            FROM catastro.clientes AS clie
            INNER JOIN medicion.cierreyapertura AS cie ON clie.nroinscripcion = cie.nroinscripcion AND clie.codemp = cie.codemp AND clie.codsuc = cie.codsuc AND clie.codsector = cie.codsector
            INNER JOIN public.sectores AS se ON se.codemp = clie.codemp AND se.codsuc = clie.codsuc AND se.codsector = clie.codsector AND se.codzona = clie.codzona
            WHERE cie.codemp = 1 AND cie.codsuc = $codsuc AND cie.codciclo = 1 AND cie.anio = '".$anio."' AND cie.mes = '".$mes."' ".$AndSect." ";
        if ($verfecha != 0)
        {$VerSector .= " AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ";}

        $VerSector.=" ORDER BY se.codsector ASC ";
        $consultaS = $conexion->query($VerSector);
        $itemsS = $consultaS->fetchAll();

        $array_general = array();

        foreach ($itemsS as $indice_sector => $val) :

          if(!array_key_exists($val['codsector'],$array_general)):
            $array_general[$val['codsector']] = array();
            $array_general[$val['codsector']]['nombre'] = $val['descripcion'];
          endif;

          $CodSectorN = $val['codsector'];
          
          //$this->Ln(3);
          $Sector     = $val['descripcion'];

          $VerRuta = "SELECT DISTINCT ru.codrutlecturas, ru.descripcion
            FROM catastro.clientes AS clie
            INNER JOIN medicion.cierreyapertura AS cie ON clie.nroinscripcion = cie.nroinscripcion AND clie.codemp = cie.codemp AND clie.codsuc = cie.codsuc AND clie.codsector = cie.codsector
            INNER JOIN public.rutasmaelecturas AS ru ON ru.codemp = clie.codemp AND ru.codsuc = clie.codsuc AND ru.codzona = clie.codzona AND ru.codsector = clie.codsector AND ru.codrutlecturas = clie.codrutlecturas
            WHERE cie.codemp = 1 AND cie.codsuc = $codsuc
            AND cie.codciclo = 1 AND ru.estareg=1
            AND cie.anio = '".$anio."' AND cie.mes = '".$mes."'
            AND clie.codsector = " . $CodSectorN;

          if ($verfecha != 0): $VerRuta .= " AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' "; endif;
          $VerRuta.=" ORDER BY ru.codrutlecturas ASC ";
          $consultaR = $conexion->query($VerRuta);
          $itemsR    = $consultaR->fetchAll();

          foreach ($itemsR as $indice_ruta => $valu) :

            if(!array_key_exists($valu['codrutlecturas'], $array_general[$val['codsector']])):
              $array_general[$val['codsector']][$valu['codrutlecturas']] = array();
              $array_general[$val['codsector']][$valu['codrutlecturas']]['nombre'] = $valu['descripcion'];
            endif;


            $CodRuta = $valu['codrutlecturas'];
            $Ruta    = $valu['descripcion'];

            // Para los usuarios
            $sqlD = "SELECT clie.codantiguo,
                cie.nroinscripcion,
                clie.propietario,
                cie.fecha,
                tp.descripcioncorta || ' ' || ca.descripcion || ' ' || clie.nrocalle AS direccion,
                to_char(clie.codsuc, '00') || '-' || TRIM(to_char(clie.codsector, '00')) || '-' || TRIM(to_char(CAST(clie.codmanzanas AS INTEGER), '000')) || '-' || TRIM(to_char(CAST(clie.lote AS INTEGER), '0000')) || '-' || TRIM(clie.sublote) AS codcatastro,
                se.descripcion AS sector,
                cie.tiposervicio
                FROM medicion.cierreyapertura AS cie
                  INNER JOIN catastro.clientes AS clie ON (cie.codemp = clie.codemp AND cie.codsuc = clie.codsuc AND cie.nroinscripcion = clie.nroinscripcion)
                  INNER JOIN public.calles AS ca ON (clie.codemp = ca.codemp AND clie.codsuc = ca.codsuc AND clie.codcalle = ca.codcalle AND clie.codzona = ca.codzona)
                  INNER JOIN public.tiposcalle AS tp ON (ca.codtipocalle = tp.codtipocalle)
                  INNER JOIN public.sectores AS se ON (clie.codemp = se.codemp AND clie.codsuc = se.codsuc AND clie.codsector = se.codsector AND clie.codzona = se.codzona) ";
            $sqlD .= " WHERE cie.codemp=1 AND cie.codsuc=".$codsuc." AND cie.codciclo=".$codciclo." AND cie.anio='".$anio."' AND cie.mes='".$mes."' ";
            if ($codsector == '%')
            { $sqlD .= " AND clie.codsector=".$CodSectorN." AND clie.codrutlecturas=".$CodRuta; }
            else { $sqlD .= " AND clie.codsector='".$CodSectorN."' AND clie.codrutlecturas=".$CodRuta; }

            if ($verfecha != 0)
            {$sqlD .= " AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ";}

            $sqlD .= "AND cie.tipooperacion=1 ORDER BY codcatastro ";

            $consultaD = $conexion->query($sqlD);
            $itemsD = $consultaD->fetchAll();

            foreach ($itemsD as $indice_usuario => $values) :

              if(!array_key_exists($values['nroinscripcion'], $array_general[$val['codsector']][$valu['codrutlecturas']])):
                $array_general[$val['codsector']][$valu['codrutlecturas']][$values['nroinscripcion']] = array();
              endif;

              $array_general[$val['codsector']][$valu['codrutlecturas']][$values['nroinscripcion']] = array(
                'codantiguo' => $values['codantiguo'],
                'nroinscripcion' => $values['nroinscripcion'],
                'propietario' => $values['propietario'],
                  'direccion' => $values['direccion'],
                'fecha' => $values['fecha'],
                'codcatastro' => $values['codcatastro'],
                'sector' => $values['sector'],
                'tiposervicio' => $values['tiposervicio']
              );

            endforeach; // fin cliente

          endforeach; // fin ruta

        endforeach; // fin sector

        foreach ($array_general as $indice_sector => $datos_rutas) :

          $CodSectorN = $indice_sector;
          $Sector     = $datos_rutas['nombre'];
          unset($datos_rutas['nombre']);

          $this->SetFont('Arial', 'B', 9);
          $this->Cell(80, 6, utf8_decode($Sector) , 0, 1, 'L');
          //$this->Ln(1);

          foreach ($datos_rutas as $indice_ruta => $datos_usuarios) :

            $CodRuta = $indice_ruta;
            $Ruta    = $datos_usuarios['nombre'];
            unset($datos_usuarios['nombre']);

            if(empty($datos_usuarios)): break; endif;
            $this->Ln(2);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(196, 6,  utf8_decode($Sector)."    ===>    ".$Ruta , 1, 1, 'L');
            $this->SetFont('Arial', 'B', 6);

            $this->SetWidths(array(8, 21, 16, 58, 50, 13, 16, 14));
            $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C"));
            $this->Row(array("ITEM",
                "COD. CATASTRAL",
                "INSCRIPCION",
                "PROPIETARIO",
                "DIRECCION",
                "FECHA",
                //"MOTIVO",
                "SERV",
                "HORA"
                //"PERSONAL"
            ));

            foreach ($datos_usuarios as $indice_inscripcion => $datos) :

              $count++;

              $ts = $datos['tiposervicio'];
              $this->SetFont('Arial', '', 6);
              $this->SetWidths(array(8, 21, 16, 58, 50, 13, 16, 14));
              $this->SetAligns(array("C", "C", "C", "L", "L", "C", "C", "C"));
              $this->Row(array($count,
                  $datos['codcatastro'],
                  $datos['codantiguo'],
                  utf8_decode(strtoupper($datos["propietario"])),
                  utf8_decode(strtoupper($datos["direccion"])),
                  $this->DecFecha($datos["fecha"]),
                  //utf8_decode(strtoupper($row["motivo"])),
                  utf8_decode(strtoupper($ts)),
                  ''
                  //utf8_decode(strtoupper($row["personal"]))
              ));
              
              $impmes += $datos["impmes"];
              $impigv += $datos["impigv"];
              $imptotal += ($impmes+$impigv);


            endforeach;

          endforeach;

        endforeach;


        // echo "<pre>";
        // var_dump($array_general);
        // echo "</pre>";exit;


    }

}

$ciclo = $_GET["ciclo"];
$sector = $_GET["sector"];
$codsuc = $_GET["codsuc"];
$anio = $_GET["anio"];
$mes  = $_GET["mes"];
//$fecha   = $_GET["fecha"];
$fdesde  = $_GET["fdesde"];
$fhasta  = $_GET["fhasta"];
$verfecha= $_GET["verfecha"];
$ciclotext = $_GET["ciclotext"];

$m = $meses[$mes];


$objReporte = new clsRegistroVentas();
$objReporte->AliasNbPages();
$objReporte->AddPage("");

$objReporte->agregar_detalle($codsuc, $ciclo, $anio, $mes, $sector, $verfecha, $fdesde, $fhasta);

$objReporte->Output();

?>
