<?php

set_time_limit(0);
ini_set('memory_limit', '2048M');
session_name("pnsu");
include("../../../../objetos/clsReporte.php");

class clsCorte extends clsReporte {

    function cabecera() {
        global $anio, $mes, $meses, $sectores, $rutas, $Dim;

        $h = 5;
        $this->SetY(8);
        $this->SetFont('Arial', 'B', 8);
        $tit1 = "";
        $this->SetX(230);
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'L');
        $this->SetFont('Arial', 'B', 12);
        $this->SetY(8);
        $this->Cell(0, $h, "PROGRAMA DE EJECUCION DE CORTE DE", 0, 1, 'C');
        $this->Cell(0, $h, " CONEXION DE AGUA ".$meses[intval($mes)]."-".$anio, 0, 1, 'C');
        $this->Ln(5);

    }

    function CabDetalleSector($Des)
    {
        global $anio, $mes, $sectores, $rutas, $Dim;

        $h = 12;
        $h2 = 6;
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(277, $h2, utf8_decode($Des), 1, 1, 'L', false);
        //$this->Ln(2);
    }

    function CabDetalleRuta($Sect, $Des)
    {
        global $anio, $mes, $sectores, $rutas, $Dim;

        $h = 10;
        $h2 = 5;

        $this->SetFont('Arial', 'B', 10);
        $this->Ln(4);
        $this->Cell(277, $h2, "SECTOR : ".$Sect."                    RUTA : ".$Des, 1, 1, 'L', false);
        //$this->Ln(1);

        $this->SetFont('Arial', '', 6.5);
        //$this->Cell($Dim[1], $h, utf8_decode('Item'), 1, 0, 'C', false);
        $this->Cell($Dim[1], $h, utf8_decode('Codigo Geografico'), 0, 0, 'C', false);
        $this->Cell($Dim[2], $h, utf8_decode('S - R - Sec'), 0, 0, 'C', false);
        $this->Cell($Dim[3], $h, utf8_decode('Inscrito'), 0, 0, 'C', false);
        $this->Cell($Dim[4], $h, utf8_decode('Apellidos y Nombres'), 0, 0, 'C', false);
        $this->Cell($Dim[5], $h,utf8_decode('Direccion'),0,0,'C',false);
        $this->Cell($Dim[6], $h2,utf8_decode('Deuda'),0,0,'C',false);
        $this->Cell($Dim[7], $h2, utf8_decode('Medidor'), 0, 0, 'C', false);
        $this->Cell($Dim[8], $h, utf8_decode('Informe de Trabajo Realizado'), 0, 1, 'C', false);
        //$this->Cell($Dim[8], $h, utf8_decode('Medidor Actual'), 1, 0, 'C', false);
        //$this->Cell($Dim[9], $h, utf8_decode('Medidor Retirado'), 1, 0, 'C', false);
        //$this->Cell($Dim[10], $h, utf8_decode('Lectura'), 1, 0, 'C', false);
        $this->SetXY(195,$this->GetY()-5);
        $this->Cell($Dim[9], $h2, utf8_decode('Meses'), 0, 0, 'C', false);
        $this->Cell($Dim[10], $h2, utf8_decode('N°'), 0, 0, 'C', false);
        $this->Cell($Dim[11], $h2, utf8_decode('Retirado'), 0, 0, 'C', false);
        $this->Cell($Dim[12], $h2, utf8_decode('Lect(m3)'), 0, 1, 'C', false);
    }



    function Contenido($usuario, $direccion, $meses, $imptotal, $codcatastro, $tiposervicio, $tipofacturacion, $nromed, $item, $codantiguo, $nromedidor, $secrutsecu){
        global $anio, $mes, $sectores, $rutas, $Dim;

        //$dibujar_cudrado_dinamico = (int) $this->GetY();

        //$this->SetFillColor(255, 255, 255); //Color de Fondo
        //$this->RoundedRect(245, (48 + $dibujar_cudrado_dinamico), 42, 6, 0, 'DF');  // Para la cabecera

        //$this->SetFillColor(255, 255, 255);
        //$this->RoundedRect(10, (49 + $this->GetY()), 42, 6, 0, 'DF');  // Para la cabecera


        $this->SetFont('Arial', '', 7);
        $this->SetFillColor(255, 255, 255); //Color de Fondo
        $this->SetTextColor(0);
        $h = 12;
        $h2 = 6;

        //$this->Cell($Dim[1], $h2, utf8_decode($item), 1, 0, 'C', true);
        $this->Cell($Dim[1], $h2, utf8_decode($codcatastro), 0, 0, 'C', true);
        $this->Cell($Dim[2], $h2, $secrutsecu, 0, 0, 'C', true);
        $this->Cell($Dim[3], $h2, utf8_decode($codantiguo), 0, 0, 'C', true);
        $this->Cell($Dim[4], $h2, strtoupper($usuario), 0, 0, 'L', true);
        $this->Cell($Dim[5], $h2,strtoupper($direccion), 0,0,'L',true);
        $this->Cell($Dim[6], $h2, $meses, 0, 0, 'C', true);
        $this->Cell($Dim[10], $h2, $nromedidor, 0, 0, 'R', true);

        $py = $this->GetY();
        $px = $this->GetX();

        $this->SetXY($px+3, $py+1);
        $this->Cell(3, 3, "", 1, 0, 'C', true);
        $this->SetXY($px,$py);
        $this->Cell($Dim[11], $h2, "", 0, 0, 'C', false);
        $this->Cell($Dim[12], $h2, "________", 0, 0, 'C', true);
        $this->Cell($Dim[13], $h2, "", 0, 0, 'C', true);
        //$this->Cell($Dim[6], $h2, utf8_decode(strtoupper(trim($tiposervicio))), 1, 0, 'C', true);
        //$this->Cell($Dim[8], $h2, utf8_decode($nromed), 1, 0, 'C', true);
        //$this->Cell($Dim[9], $h2, utf8_decode(''), 1, 0, 'C', true);
        //$this->Cell($Dim[10], $h2, utf8_decode(''), 1, 0, 'C', true);
        //$this->Cell($Dim[11], $h2, $meses, 1, 0, 'C', true);
        $this->Cell($Dim[12], $h2, number_format($imptotal, 2), 0, 1, 'R', true);
        //$this->SetX(60);
        //$this->Cell($Dim[4] + $Dim[5], $h2, strtoupper($usuario), 'B', 1, 'L', true);
        //$this->Ln($h2);
    }

    function ContenidFoot($total, $monto) {
        $h = 6;
        $this->SetFont('Arial', 'B', 8);
        $this->Ln(2);
        $this->Cell(8, $h, "", 0, 0, 'C');
        $this->Cell(23, $h, "", 0, 0, 'L');
        $this->Cell(50, $h, "Total Usuarios", 0, 0, 'R');
        $this->Cell(45, $h, $total, 0, 0, 'L');
        $this->Cell(15, $h, "", 0, 0, 'C');
        $this->Cell(26, $h, "MONTO DEUDA", 0, 0, 'R');
        $this->Cell(25, $h, number_format($monto, 2), 0, 1, 'R');
    }


    //  Para los cuadros con bordes redondeados o sin ellos
    function RoundedRect($x, $y, $w, $h, $r, $style = '') {
      $k = $this->k;
      $hp = $this->h;
      if($style=='F')
        $op='f';
      elseif($style=='FD' || $style=='DF')
        $op='B';
      else
        $op='S';
      $MyArc = 4/3 * (sqrt(2) - 1);
      $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
      $xc = $x+$w-$r ;
      $yc = $y+$r;
      $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

      $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
      $xc = $x+$w-$r ;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
      $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
      $xc = $x+$r ;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
      $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
      $xc = $x+$r ;
      $yc = $y+$r;
      $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
      $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
      $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
     $h = $this->h;
     $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
     $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

}

if (!session_start()) {
    session_start();
}

if ($Mobil == 1):
  $objReporte = new clsCorte();
else:
  $objReporte = new clsCorte('L');
endif;


$Dim = array('1' => 30, '2' => 25, '3' => 20, '4' => 55, '5' => 55, '6' => 10, '7' => 40, '8' => 42, '9' => 10, '10' => 18, '11' => 9, '12' => 12, '13' => 30);

$codsuc = $_SESSION['IdSucursal'];

$nrolistado = $_GET["nrolistado"];

//Mobiles
$Mobil = isset($_GET["Mobil"]) ? $_GET["Mobil"] : 0;

$codemp = 1;
$Select = "SELECT codservices, codtipolistado, codsectores,
    codrutas, codestados, mesini, mesfin, incluir, ordenar, codsucursales, codciclos, codsuc, anio, mes
    FROM  medicion.cablistadocorte
    WHERE nrolistado=".$nrolistado." AND codsuc=".$codsuc;
$Consulta = $conexion->query($Select);
$row = $Consulta->fetch();
$codservices = $row['codservices'];
$codtipolistado = $row['codtipolistado'];
$codsectores = $row['codsectores'];
$codrutas = $row['codrutas'];
$codestados = $row['codestados'];
$inicio = $row['mesini'];
$final = $row['mesfin'];
$incluir = $row['incluir'];
$orden = $row['ordenar'];
$codsucursales = $row['codsucursales'];
$codciclos = $row['codciclos'];
$codsuc = $row['codsuc'];
$anio = $row['anio'];
$mes = $row['mes'];

//$facturacion = $objReporte->datosfacturacion($codsuc,$ciclo);

$objReporte->AliasNbPages();
$objReporte->AddPage("");

if ($orden == 1) {
    $ord = "clie.nroorden";
} else {
    $ord = "clie.nroorden";
}
$ord = " ORDER BY codcatastro ASC ";

$count = 0;
$monto = 0;

$codsucursales = split('[-]', $codsucursales);
$codciclos = trim($codciclos);
$codciclos = split('[=]', $codciclos);

$codsectores = trim($codsectores);
$rs = split('[-]', $codsectores);
$CodSector= $rs[1];

$codestados = trim($codestados);
$codestados = str_replace('=', ',', $codestados);
$AndEst = " clie.codestadoservicio IN( ".$codestados." )";

$VerSect="SELECT s.codzona, s.codsector, s.descripcion FROM public.sectores AS s
    WHERE s.codsuc = 1 AND s.codsector=$CodSector ";
$rs = $conexion->query($VerSect);
$its = $rs->fetch();
$CodZona  = $its[0];
//foreach ($its as $rws)
//{
    $Descripcion= $its['descripcion'];
    $objReporte->CabDetalleSector(utf8_decode($Descripcion));

    $VerRuta="SELECT
        DISTINCT li.codrutlecturas
        FROM
        medicion.listadocorte AS li
        WHERE
        li.codsector=".$CodSector." AND li.nrolistado=".$nrolistado."
        ORDER BY
        li.codrutlecturas ASC "; //AND s.codzona=$CodZona
    $rsrut = $conexion->query($VerRuta);
    $itrut = $rsrut->fetchAll();

    $CodRutaAnt= 0;

    $objReporte->SetFillColor(255, 255, 255);
    $objReporte->RoundedRect(10, 38, 277, 9, 0, 'DF');  // Para la cabecera

    $aumentar = -5;

    foreach ($itrut as $rews)
    {
        $Ruta=$rews['descripcion'];
        $CodRuta= $rews['codrutlecturas'];

        $sql = "SELECT lst.nroinscripcion, clie.propietario,
            tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle AS direccion,
            lst.nromeses, lst.imptotal, ".$objReporte->getCodCatastral("clie.").", clie.codtiposervicio,
            ( clie.codsector ||' - '|| clie.codrutlecturas ||' - '|| co.orden_lect) AS secrutsecu,
            clie.tipofacturacion, clie.nromed, clie.codantiguo,
            clie.codrutlecturas, clie.nromed
            FROM medicion.listadocorte AS lst
            JOIN catastro.clientes AS clie ON(lst.codemp=clie.codemp AND lst.codsuc=clie.codsuc AND lst.nroinscripcion=clie.nroinscripcion)
            JOIN catastro.conexiones AS co ON(clie.codemp=co.codemp AND clie.codsuc=co.codsuc AND clie.nroinscripcion=co.nroinscripcion)
            JOIN public.calles AS cal ON(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona)
            JOIN public.tiposcalle AS tipcal ON(cal.codtipocalle=tipcal.codtipocalle)
            WHERE lst.codsuc=".$codsuc." AND clie.codsector=".$CodSector." AND clie.codrutlecturas=".$CodRuta." AND lst.nrolistado=".$nrolistado."  ".$ord;

        $consulta = $conexion->query($sql);
        $items = $consulta->fetchAll();

        if($CodRutaAnt!=$CodRuta)
        {
            if(count($items)>0)
            {
                $objReporte->CabDetalleRuta($CodSector, $CodRuta);
            }
        }

        $CodRutaAnt= $CodRuta;

        foreach ($items as $row)
        {
            $ts = "";
            $tf = "";
            $nroinsc = $row['nroinscripcion'];
            $count++;

            if ($row["codtiposervicio"] == 1) {
                $ts = "A/D";
            }
            if ($row["codtiposervicio"] == 2) {
                $ts = "A";
            }
            if ($row["codtiposervicio"] == 3) {
                $ts = "D";
            }
            if ($row["tipofacturacion"] == 0) {
                $tf = "L";
            }
            if ($row["tipofacturacion"] == 1) {
                $tf = "P";
            }
            if ($row["tipofacturacion"] == 2) {
                $tf = "A";
            }

            $monto += $row["imptotal"];

            //if($nroinsc!='')
            //{
            if ($Mobil == 1) {
                $output["Cortes"][] = $row;
            } else {

                // $aumentar +=5;
                // $objReporte->SetFillColor(255, 255, 255);
                // $objReporte->RoundedRect(10, (30 + $aumentar), 42, 6, 0, 'DF');  // Para la cabecera
                $objReporte->Contenido(utf8_decode($row["propietario"]), utf8_decode($row["direccion"]), $row["nromeses"], $row["imptotal"], $row["codcatastro"], $ts, $tf, $row["nromed"], $count, isset($row['codantiguo']) ? $row['codantiguo'] : $row['nroinscripcion'], $row['nromed'], $row['secrutsecu']);
            }
            //}
        }
    }
//}

if ($Mobil == 1) {
    $jsonStr = json_encode($output);

    $json = fopen("Cortes.json", "w") or die("Problemas en la creacion");
    fwrite($json, $jsonStr);
    fclose($json);
    header("Content-Disposition: attachment; filename=Cortes.json;");
    header("Content-Type: application/force-download;");
    readfile("Cortes.json");

    //print($jsonStr);
} else {
    $objReporte->ContenidFoot($count, $monto);
    $objReporte->Output();
}
?>
