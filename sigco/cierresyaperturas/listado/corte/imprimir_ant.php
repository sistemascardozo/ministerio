<?php

set_time_limit(0);
ini_set('memory_limit', '2048M');
session_name("pnsu");
include("../../../../objetos/clsReporte.php");

class clsCorte extends clsReporte {

    function cabecera() {
        global $anio, $mes, $meses, $sectores, $rutas, $Dim;

        $h = 5;
        $this->SetY(8);
        $this->SetFont('Arial', 'B', 8);
        $tit1 = "";
        $this->SetX(230);
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'L');
        $this->SetFont('Arial', 'B', 12);
        $this->SetY(8);
        $this->Cell(0, $h, "PROGRAMA DE EJECUCION DE CORTE DE", 0, 1, 'C');
        $this->Cell(0, $h, " CONEXION DE AGUA ".$meses[intval($mes)]."-".$anio, 0, 1, 'C');
        $this->Ln(5);    

    }

    function CabDetalleSector($Des)
    {
        global $anio, $mes, $sectores, $rutas, $Dim;
        
        $h = 12;
        $h2 = 6; 
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(190, $h2, utf8_decode($Des), 1, 1, 'L', false);      
        //$this->Ln(2);
    }
    
    function CabDetalleRuta($Sect, $Des)
    {
        global $anio, $mes, $sectores, $rutas, $Dim;
        
        $h = 8;
        $h2 = 6; 
        
        $this->SetFont('Arial', 'B', 10);
        $this->Ln(4);
        $this->Cell(190, $h2, "SECTOR : ".$Sect."                    RUTA : ".$Des, 1, 1, 'L', false);
        //$this->Ln(1);
        
        $this->SetFont('Arial', '', 6.5);
        $this->Cell($Dim[1], $h, utf8_decode('Item'), 1, 0, 'C', false);
        $this->Cell($Dim[2], $h, utf8_decode('Codigo Catastral'), 1, 0, 'C', false);
        $this->Cell($Dim[3], $h, utf8_decode('Inscripcion'), 1, 0, 'C', false);
        $this->Cell($Dim[4], $h, utf8_decode('Usuario'), 1, 0, 'C', false);
        $this->Cell($Dim[5], $h,utf8_decode('Direccion'),1,0,'C',false);
        $this->Cell($Dim[6], $h, utf8_decode('Serv.'), 1, 0, 'C', false);
        $this->Cell($Dim[7], $h, utf8_decode('Hora'), 1, 0, 'C', false);
        //$this->Cell($Dim[8], $h, utf8_decode('Medidor Actual'), 1, 0, 'C', false);
        //$this->Cell($Dim[9], $h, utf8_decode('Medidor Retirado'), 1, 0, 'C', false);
        //$this->Cell($Dim[10], $h, utf8_decode('Lectura'), 1, 0, 'C', false);
        $this->Cell($Dim[11], $h, utf8_decode('#Meses'), 1, 0, 'C', false);
        $this->Cell($Dim[12], $h, utf8_decode('Deuda'), 1, 1, 'C', false);
    }
    
    
    
    function Contenido($usuario, $direccion, $meses, $imptotal, $codcatastro, $tiposervicio, $tipofacturacion, $nromed, $item, $codantiguo ){
        global $anio, $mes, $sectores, $rutas, $Dim;
        $this->SetFont('Arial', '', 7);
        $this->SetFillColor(255, 255, 255); //Color de Fondo
        $this->SetTextColor(0);
        $h = 12;
        $h2 = 6;        
        
        $this->Cell($Dim[1], $h2, utf8_decode($item), 1, 0, 'C', true);
        $this->Cell($Dim[2], $h2, utf8_decode($codcatastro), 1, 0, 'C', true);
        $this->Cell($Dim[3], $h2, utf8_decode($codantiguo), 1, 0, 'C', true);
        $this->Cell($Dim[4], $h2, strtoupper($usuario), 1, 0, 'L', true);
        $this->Cell($Dim[5], $h2,strtoupper($direccion),1,0,'L',true);
        $this->Cell($Dim[6], $h2, utf8_decode(strtoupper(trim($tiposervicio))), 1, 0, 'C', true);
        $this->Cell($Dim[7], $h2, "", 1, 0, 'C', true);
        //$this->Cell($Dim[8], $h2, utf8_decode($nromed), 1, 0, 'C', true);
        //$this->Cell($Dim[9], $h2, utf8_decode(''), 1, 0, 'C', true);
        //$this->Cell($Dim[10], $h2, utf8_decode(''), 1, 0, 'C', true);
        $this->Cell($Dim[11], $h2, $meses, 1, 0, 'C', true);
        $this->Cell($Dim[12], $h2, number_format($imptotal, 2), 1, 1, 'R', true);
        //$this->SetX(60);
        //$this->Cell($Dim[4] + $Dim[5], $h2, strtoupper($usuario), 'B', 1, 'L', true);
        //$this->Ln($h2);
    }

    function ContenidFoot($total, $monto) {
        $h = 6;
        $this->SetFont('Arial', 'B', 8);
        $this->Ln(2);
        $this->Cell(8, $h, "", 0, 0, 'C');
        $this->Cell(23, $h, "", 0, 0, 'L');
        $this->Cell(50, $h, "Total Usuarios", 0, 0, 'R');
        $this->Cell(45, $h, $total, 0, 0, 'L');
        $this->Cell(15, $h, "", 0, 0, 'C');
        $this->Cell(26, $h, "MONTO DEUDA", 0, 0, 'R');
        $this->Cell(25, $h, number_format($monto, 2), 0, 1, 'R');
    }

}

if (!session_start()) {
    session_start();
}
$objReporte = new clsCorte();

$Dim = array('1' => 8, '2' => 22, '3' => 13, '4' => 52, '5' => 52, '6' => 9, '7' => 12, '8' => 25, '9' => 20, '10' => 20, '11' => 10, '12' => 12);

$codsuc = $_SESSION['IdSucursal'];

$nrolistado = $_GET["nrolistado"];

//Mobiles
$Mobil = isset($_GET["Mobil"]) ? $_GET["Mobil"] : 0;

$codemp = 1;
$Select = "SELECT codservices, codtipolistado, codsectores,
    codrutas, codestados, mesini, mesfin, incluir, ordenar, codsucursales, codciclos, codsuc, anio, mes
    FROM  medicion.cablistadocorte
    WHERE nrolistado=".$nrolistado." AND codsuc=".$codsuc;
$Consulta = $conexion->query($Select);
$row = $Consulta->fetch();
$codservices = $row['codservices'];
$codtipolistado = $row['codtipolistado'];
$codsectores = $row['codsectores'];
$codrutas = $row['codrutas'];
$codestados = $row['codestados'];
$inicio = $row['mesini'];
$final = $row['mesfin'];
$incluir = $row['incluir'];
$orden = $row['ordenar'];
$codsucursales = $row['codsucursales'];
$codciclos = $row['codciclos'];
$codsuc = $row['codsuc'];
$anio = $row['anio'];
$mes = $row['mes'];

//$facturacion = $objReporte->datosfacturacion($codsuc,$ciclo);        

$objReporte->AliasNbPages();
$objReporte->AddPage("");

if ($orden == 1) {
    $ord = "clie.nroorden";
} else {
    $ord = "clie.nroorden";
}
$ord = " ORDER BY codcatastro ASC ";

$count = 0;
$monto = 0;

$codsucursales = split('[-]', $codsucursales);
$codciclos = trim($codciclos);
$codciclos = split('[=]', $codciclos);

$codsectores = trim($codsectores);
$rs = split('[-]', $codsectores);
$CodSector= $rs[1];

$codestados = trim($codestados);
$codestados = str_replace('=', ',', $codestados);
$AndEst = " clie.codestadoservicio IN( ".$codestados." )";

$VerSect="SELECT s.codzona, s.codsector, s.descripcion FROM public.sectores AS s
    WHERE s.codsuc = 1 AND s.codsector=$CodSector ";
$rs = $conexion->query($VerSect);
$its = $rs->fetch();
$CodZona  = $its[0];
//foreach ($its as $rws) 
//{
    $Descripcion= $its['descripcion'];
    $objReporte->CabDetalleSector(utf8_decode($Descripcion));
    
    $VerRuta="SELECT
        DISTINCT li.codrutlecturas
        FROM
        medicion.listadocorte AS li
        WHERE
        li.codsector=".$CodSector." AND li.nrolistado=".$nrolistado."
        ORDER BY
        li.codrutlecturas ASC "; //AND s.codzona=$CodZona
    $rsrut = $conexion->query($VerRuta);
    $itrut = $rsrut->fetchAll();
    
    $CodRutaAnt= 0;
    
    foreach ($itrut as $rews) 
    {
        $Ruta=$rews['descripcion'];
        $CodRuta= $rews['codrutlecturas'];
                        
        $sql = "SELECT lst.nroinscripcion, clie.propietario, 
            tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle AS direccion,
            lst.nromeses, lst.imptotal, ".$objReporte->getCodCatastral("clie.").", clie.codtiposervicio, 
            clie.tipofacturacion, clie.nromed, clie.codantiguo,
            clie.codrutlecturas
            FROM medicion.listadocorte AS lst 
            INNER JOIN catastro.clientes AS clie ON(lst.codemp=clie.codemp AND lst.codsuc=clie.codsuc AND lst.nroinscripcion=clie.nroinscripcion)
            INNER JOIN public.calles AS cal ON(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona)
            INNER JOIN public.tiposcalle AS tipcal ON(cal.codtipocalle=tipcal.codtipocalle)
            WHERE lst.codsuc=".$codsuc." AND clie.codsector=".$CodSector." AND clie.codrutlecturas=".$CodRuta." AND lst.nrolistado=".$nrolistado."  ".$ord;

        $consulta = $conexion->query($sql);
        $items = $consulta->fetchAll();        
        
        if($CodRutaAnt!=$CodRuta)
        {
            if(count($items)>0)
            {      
                $objReporte->CabDetalleRuta($CodSector, $CodRuta); 
            }
        }
        
        $CodRutaAnt= $CodRuta;
        
        foreach ($items as $row) 
        {
            $ts = "";
            $tf = "";
            $nroinsc = $row['nroinscripcion'];
            $count++;

            if ($row["codtiposervicio"] == 1) {
                $ts = "A/D";
            }
            if ($row["codtiposervicio"] == 2) {
                $ts = "A";
            }
            if ($row["codtiposervicio"] == 3) {
                $ts = "D";
            }
            if ($row["tipofacturacion"] == 0) {
                $tf = "L";
            }
            if ($row["tipofacturacion"] == 1) {
                $tf = "P";
            }
            if ($row["tipofacturacion"] == 2) {
                $tf = "A";
            }

            $monto += $row["imptotal"];
            
            //if($nroinsc!='')
            //{
                if ($Mobil == 1) {
                    $output["Cortes"][] = $row;
                } else {
                    $objReporte->Contenido(utf8_decode($row["propietario"]), utf8_decode($row["direccion"]), $row["nromeses"], $row["imptotal"], $row["codcatastro"], $ts, $tf, $row["nromed"], $count, isset($row['codantiguo']) ? $row['codantiguo'] : $row['nroinscripcion']);
                }
            //}
        }
    }
//}

if ($Mobil == 1) {
    $jsonStr = json_encode($output);

    $json = fopen("Cortes.json", "w") or die("Problemas en la creacion");
    fwrite($json, $jsonStr);
    fclose($json);
    header("Content-Disposition: attachment; filename=Cortes.json;");
    header("Content-Type: application/force-download;");
    readfile("Cortes.json");

    //print($jsonStr);
} else {
    $objReporte->ContenidFoot($count, $monto);
    $objReporte->Output();
}
?>