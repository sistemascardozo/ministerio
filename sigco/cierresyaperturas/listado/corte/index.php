<?php
include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$TituloVentana = "LISTADO DE CORTE DE SERVICIOS";
$Activo = 1;
CuerpoSuperior($TituloVentana);

$objDrop = new clsDrop();
$codsuc = $_SESSION['IdSucursal'];
$codemp = 1;
$sucursal = $objDrop->setSucursales(" where codemp=1 and codsuc=?", $codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_corte.js" language="JavaScript"></script>
<script type="text/javascript" src="../../../medicion/operaciones/digitacion/js_digitacion.js" language="JavaScript"></script>
<style type="text/css">
    .multiselect {
        width:25em;
        height:5em;
        border:solid 1px #c0c0c0;
        overflow:auto;
    }

    .multiselect label {
        display:block;
    }

    .multiselect-on {
        color:#ffffff;
        background-color:#000099;
    }
</style>
<script>
    jQuery.fn.multiselect = function () {
        $(this).each(function () {
            var checkboxes = $(this).find("input:checkbox");
            checkboxes.each(function () {
                var checkbox = $(this);
                // Highlight pre-selected checkboxes
                if (checkbox.attr("checked"))
                    checkbox.parent().addClass("multiselect-on");

                // Highlight checkboxes that the user selects
                checkbox.click(function () {
                    if (checkbox.attr("checked"))
                        checkbox.parent().addClass("multiselect-on");
                    else
                        checkbox.parent().removeClass("multiselect-on");
                });
            });
        });
    };
    var Tam = TamVentana();
    var urldir = "<?php echo $_SESSION['urldir']; ?>";
    var codsuc = <?=$codsuc ?>;
    $(document).ready(function () {
        $('#DivTbListados #TbListados').fixedHeaderTable({height: '300', footer: true});
        var theTable = $('#TbListados')
        $("#Filtro2").keyup(function () {
            $.uiTableFilter(theTable, this.value)
        })

        $("#DivSucu").multiselect();

        $("#tabs").tabs();
        $("#DivCuentaCorriente").dialog({
            autoOpen: false,
            height: 600,
            width: 1000,
            modal: true,
            buttons: {
                "Cerrar": function () {
                    $("#DivCuentaCorriente").dialog("close");
                }
            },
            close: function () {
                $(ObjTr).addClass('ui-widget-header');
            }
        });

    });

    var codsuc = <?=$codsuc ?>;

    var c = 0
    function ValidarForm(Op)
    {

        var Temp = ""
        var i = 0
        var Data = ''
        if ($("#nombrelistado").val() == '')
        {
            Msj('#nombrelistado', 'Ingrese Nombre del Listado', 1000, 'right', 'error', false)
            return false
        }
        $('#DivSucu :input').each(function ()
        {
            if ($(this).attr("checked"))
            {
                i++
                Temp += 'suc_' + i + '=' + $(this).val() + '&'
            }
        });
        if (Temp == '')
        {
            Msj('#DivSucu', 'Seleccione Surcursales', 1000, 'right', 'error', false)
            return false
        }
        Data += Temp + 'NroSuc=' + i + '&'
        Temp = ''
        i = 0
        ////////////
        $('#DivCiclo :input').each(function ()
        {
            if ($(this).attr("checked"))
            {
                i++
                Temp += 'ciclo_' + i + '=' + $(this).val() + '&'
            }
        });
        if (Temp == '')
        {
            Msj('#DivCiclo', 'Seleccione Ciclos', 1000, 'right', 'error', false)
            return false;
        }
        Data += Temp + 'NroCic=' + i + '&'
        Temp = ''
        i = 0
        ////////////
        $('#DivSector :input').each(function ()
        {
            if ($(this).attr("checked"))
            {
                i++
                Temp += 'sector_' + i + '=' + $(this).val() + '&'
            }
        });
        if (Temp == '')
        {
            Msj('#DivSector', 'Seleccione Sectores', 1000, 'right', 'error', false)
            return false;
        }
        Data += Temp + 'NroSec=' + i + '&'
        Temp = ''
        i = 0
        ////////////
        $('#DivRuta :input').each(function ()
        {
            if ($(this).attr("checked"))
            {
                i++
                Temp += 'ruta_' + i + '=' + $(this).val() + '&'
            }
        });
        if (Temp == '')
        {
            Msj('#DivRuta', 'Seleccione Rutas', 1000, 'right', 'error', false)
            return false;
        }
        Data += Temp + 'NroRut=' + i + '&'
        Temp = ''
        i = 0
        ////////////
        $('#DivEstado :input').each(function ()
        {
            if ($(this).attr("checked"))
            {
                i++
                Temp += 'estado_' + i + '=' + $(this).val() + '&'
            }
        });
        if (Temp == '')
        {
            Msj('#DivEstado', 'Seleccione Estado', 1000, 'right', 'error', false)
            return false;
        }
        Data += Temp + 'NroEst=' + i + '&'
        if ($("#mesdesde").val() == '')
        {
            Msj('#mesdesde', 'Ingrese Mes Inicial', 1000, 'right', 'error', false)
            return false
        }
        Data += 'mesdesde=' + $("#mesdesde").val() + '&'
        if ($("#meshasta").val() == '')
        {
            Msj('#meshasta', 'Ingrese Mes Final', 1000, 'right', 'error', false)
            return false
        }
        Data += 'meshasta=' + $("#meshasta").val() + '&'
        
        var TipoServ= $("#tiposervicio").val();
        if (TipoServ == 0)
        {
            Msj('#tiposervicio', 'Seleccione Servicio', 1000, 'right', 'error', false)
            return false
        }
        Data += 'tiposervicio='+TipoServ+ '&'
        
        var TpEntidades= $("#tipoentidades").val();
        
        Data += 'tipoentidades='+TpEntidades+ '&'
        ///////DivIncluir
        Temp = ''
        i = 0
        $('#DivIncluir :input').each(function ()
        {
            if ($(this).attr("checked"))
            {
                i++
                Temp += 'incluir_' + i + '=' + $(this).val() + '&'
            }
        });

        Data += Temp + 'NroInc=' + i + '&'
        ///////DivOrdenar
        Temp = ''
        i = 0
        $('#DivOrdenar :input').each(function ()
        {
            if ($(this).attr("checked"))
            {
                i++
                Temp += 'ordenar_' + i + '=' + $(this).val() + '&'
            }
        });

        Data += Temp + 'NroOrd=' + i + '&'
        if ($("#codservices").val() == 0)
        {
            Msj('#codservices', 'Seleccione Services', 1000, 'right', 'error', false)
            return false
        }
        Data += 'codservices=' + $("#codservices").val() + '&'
        Data += 'nrolistado=' + $("#nrolistado").val() + '&'
        Data += 'nombrelistado=' + $("#nombrelistado").val() + '&'
        $("#btnVerificar").hide();
        $("#BtnLoading").show();
        $.ajax({
            url: 'generar.php',
            type: 'POST',
            async: true,
            data: Data,
            success: function (datos)
            {
                $("#btnVerificar").show();
                $("#BtnLoading").hide();
                if (datos != 0 && datos != 'a')
                {
                    //$("#nrolistado").val(datos)
                    Msj('#btnVerificar', 'Listado Generado con Exito', 2000, 'above', '', false)
                    cListado(datos)

                }
                else
                {
                    if (Trim(datos) == 'a')
                        Msj('#btnVerificar', 'No Existen Datos para estos parametros', 2000, 'above', 'error', false)
                    else
                        Msj('#btnVerificar', 'Ocurrio un Error al Generar', 2000, 'above', '', false)

                }

            }
        })

    }
    function Cancelar()
    {
        location.href = '<?php echo $_SESSION['urldir']; ?>/admin/indexB.php'
    }
    function cListado(nrolistado)
    {
        $.ajax({
            url: 'cListado.php',
            type: 'POST',
            async: true,
            data: 'listado=' + nrolistado,
            success: function (datos) {
                $("#cmbnrolistado").html(datos)
                $("#nrolistado").html("<option value='0' >Nuevo Listado</option>" + datos)
                cnombrelistado()
            }
        })
    }

    function generar()
    {
        if ($("#ciclo").val() == 0)
        {
            alert("Seleccione el Ciclo de Facturacion")
            return false
        }
        $("#div_valida").html("<img src='../../../../images/iconos/sincro.gif' />")
        $.ajax({
            url: 'generar.php',
            type: 'POST',
            async: true,
            data: 'codciclo=' + $("#ciclo").val(),
            success: function (datos) {
                valida($("#ciclo").val());
            }
        })
    }
    function chcksucall(obj)
    {
        $("#DivCiclo").html('')
        $("#DivSector").html('')
        $("#DivRuta").html('')
        $("#chkciclos").attr("checked", false);
        $("#chksectores").attr("checked", false);
        $("#chkrutas").attr("checked", false);
        if ($(obj).attr("checked"))
        {

            $('#DivSucu :input').each(function ()
            {

                $(this).attr("checked", "checked");
                $(this).parent().addClass("multiselect-on");
                chcksuc(this)
            });
        }
        else
        {
            $('#DivSucu :input').each(function ()
            {
                $(this).attr("checked", false);
                $(this).parent().removeClass("multiselect-on");

            });
        }
    }
    function chcksuc(obj)
    {

        if ($(obj).attr("checked"))
        {

            cCiclos(obj)
            cSector(obj)
        }
        else
        {
            $("#DivRuta").html('')
            $('#DivCiclo .CicloSuc_' + $(obj).val()).each(function ()
            {
                $(this).remove();

            });
            $('#DivSector .SectorSuc_' + $(obj).val()).each(function ()
            {
                $(this).remove();

            });

        }
    }
    function cCiclos(obj)
    {
        var IdSucursal = $(obj).val();

        $.ajax({
            url: 'cCiclos.php',
            type: 'POST',
            async: true,
            data: 'IdSucursal=' + IdSucursal,
            success: function (datos) {

                $("#DivCiclo").append(datos)
            }
        })

    }
    function chckciclo(obj)
    {
        if ($(obj).attr("checked"))
            $(obj).parent().addClass("multiselect-on");
        else
            $(obj).parent().removeClass("multiselect-on");
    }
    function chckruta(obj)
    {
        if ($(obj).attr("checked"))
            $(obj).parent().addClass("multiselect-on");
        else
            $(obj).parent().removeClass("multiselect-on");
    }
    function chckestado(obj)
    {
        if ($(obj).attr("checked"))
            $(obj).parent().addClass("multiselect-on");
        else
            $(obj).parent().removeClass("multiselect-on");
    }
    function chckcicloall(obj)
    {

        if ($(obj).attr("checked"))
        {

            $('#DivCiclo :input').each(function ()
            {

                $(this).attr("checked", "checked");
                $(this).parent().addClass("multiselect-on");

            });
        }
        else
        {
            $('#DivCiclo :input').each(function ()
            {
                $(this).attr("checked", false);
                $(this).parent().removeClass("multiselect-on");

            });
        }
    }
    function cSector(obj)
    {
        var IdSucursal = $(obj).val();

        $.ajax({
            url: 'cSector.php',
            type: 'POST',
            async: true,
            data: 'IdSucursal=' + IdSucursal,
            success: function (datos) {

                $("#DivSector").append(datos)
            }
        })

    }
    function chcksector(obj)
    {
        if ($(obj).attr("checked"))
        {

            cRuta(obj)
            $(obj).parent().addClass("multiselect-on");
        }
        else
        {
            $(obj).parent().removeClass("multiselect-on");
            $('#DivRuta .RutaSec_' + $(obj).val()).each(function ()
            {
                $(this).remove();

            });


        }
    }
    function chcksectorall(obj)
    {
        $("#DivRuta").html('')
        $("#chkrutas").attr("checked", false);
        if ($(obj).attr("checked"))
        {

            $('#DivSector :input').each(function ()
            {

                $(this).attr("checked", "checked");
                $(this).parent().addClass("multiselect-on");
                chcksector(this)
            });
        }
        else
        {
            $('#DivSector :input').each(function ()
            {
                $(this).attr("checked", false);
                $(this).parent().removeClass("multiselect-on");

            });
        }
    }
    function cRuta(obj)
    {
        var IdSector = $(obj).val();

        $.ajax({
            url: 'cRuta.php',
            type: 'POST',
            async: true,
            data: 'IdSector=' + IdSector,
            success: function (datos) {

                $("#DivRuta").append(datos)
            }
        })

    }
    function chckrutaall(obj)
    {

        if ($(obj).attr("checked"))
        {

            $('#DivRuta :input').each(function ()
            {

                $(this).attr("checked", "checked");
                $(this).parent().addClass("multiselect-on");
                //chcksector(this)
            });
        }
        else
        {
            $('#DivRuta :input').each(function ()
            {
                $(this).attr("checked", false);
                $(this).parent().removeClass("multiselect-on");

            });
        }
    }
    function chckestadoall(obj)
    {

        if ($(obj).attr("checked"))
        {

            $('#DivEstado :input').each(function ()
            {

                $(this).attr("checked", "checked");
                $(this).parent().addClass("multiselect-on");
                //chcksector(this)
            });
        }
        else
        {
            $('#DivEstado :input').each(function ()
            {
                $(this).attr("checked", false);
                $(this).parent().removeClass("multiselect-on");

            });
        }
    }
    function Valida()
    {
        if ($("#cmbnrolistado").val() > 0)
            return true
        else
        {
            Msj('#cmbnrolistado', 'Seleccione Listado', 1000, 'right', 'error', false)
            return false
        }

    }
    function Consultar()
    {

        if (!Valida())
            return false
        $('#DivConsulta').fadeOut(100)
        $('#ImgLoad').fadeIn();
        var nrolistado = $("#cmbnrolistado").val()
        $.ajax({
            url: 'Consultar.php',
            type: 'POST',
            async: true,
            data: 'nrolistado=' + nrolistado,
            success: function (datos)
            {
                $("#TrResultados").show()
                $('#ImgLoad').fadeOut(500, function () {
                    $("#DivConsulta").empty().append(datos);
                    /*$("#TbConsulta tbody tr").mouseover(function(){$(this).addClass('ui-state-active');})
                     $("#TbConsulta tbody tr").mouseout(function(){$(this).removeClass('ui-state-active'); })
                     $(document).on('focusin', '#TbConsulta tbody tr',function(event){$(this).addClass('ui-widget-header');});
                     $(document).on('focusout','#TbConsulta tbody tr', function(event){$(this).removeClass('ui-widget-header');});	
                     */
                    $("#TbConsulta tbody tr").focusin(function () {
                        $(this).siblings().removeClass('ui-widget-header');
                        $(this).addClass('ui-widget-header');
                    })
                    $("#TbConsulta tbody tr").focusout(function () {
                        $(this).removeClass('ui-widget-header');
                    })
                    var nroinput = 0;
                    $('#TbConsulta tbody tr').each(function () {
                        nroinput++;
                        $(this).attr('tabindex', nroinput);

                    });
                    tb = $('#TbConsulta tbody tr');
                    if ($.browser.mozilla)
                        $(tb).keypress(TabIndexIndex);
                    else
                        $(tb).keydown(TabIndexIndex);


                    $('#DivConsulta').fadeIn(500, function () {
                        var theTable = $('#TbConsulta')
                        $("#Filtro").keyup(function () {
                            $.uiTableFilter(theTable, this.value)
                        })
                    })
                });


            }
        })

    }
    function TabIndexIndex(e)
    {
        switch (e.keyCode)
        {
            case $.ui.keyCode.UP:
                for (var i = parseInt($(this).attr('tabindex')) - 1; i > 0; i--)
                {
                    var obj = $('tr[tabindex=\'' + i + '\']')
                    if (obj != null)
                    {
                        if (obj.is(':visible') && $(obj).parents(':hidden').length == 0)
                        {
                            $('tr[tabindex=\'' + i + '\']').focus();
                            e.preventDefault();
                            return false;
                        }

                    }
                }
                $('#Valor').focus().select();

                break;
            case $.ui.keyCode.DOWN:
                for (var i = parseInt($(this).attr('tabindex')) + 1; i <= 10000; i++)
                {
                    var obj = $('tr[tabindex=\'' + i + '\']')
                    if (obj != null)
                    {
                        if (obj.is(':visible') && $(obj).parents(':hidden').length == 0)
                        {
                            $('tr[tabindex=\'' + i + '\']').focus();
                            e.preventDefault();
                            return false;
                        }
                    }
                }
                $('#Valor').focus().select();
                break;




        }

    }
    function Excel()
    {

        if (!Valida())
            return false
        var nrolistado = $("#cmbnrolistado").val()
        var ventana = window.open('Excel.php?nrolistado=' + nrolistado, 'Excel Listado', 'resizable=yes, scrollbars=yes');
        ventana.focus();

    }
    function Pdf()
    {
        if (!Valida())
            return false
        var nrolistado = $("#cmbnrolistado").val()
        var ventana = window.open('imprimir.php?nrolistado=' + nrolistado, 'Imprimir Listado', 'resizable=yes, scrollbars=yes');
        ventana.focus();

    }
    function Mobil()
    {
        if (!Valida())
            return false
        var nrolistado = $("#cmbnrolistado").val()
        var ventana = window.open('imprimir.php?nrolistado=' + nrolistado + '&Mobil=1', 'Imprimir Listado', 'resizable=yes, scrollbars=yes');
        ventana.focus();

    }

    function cnombrelistado()
    {
        $("#nombrelistado").val($("#nrolistado option:selected").html())
    }

    function DeleteCorte(count, codsuc, nrolistado, nroinscripcion, anio, mes)
    {
        var r = confirm("Realmente desea eliminar el registro?")
        if (r == false)
        {
            return false
        }
        $.ajax({
            url: 'deletelistado.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nrolistado=' + nrolistado + '&nroinscripcion=' + nroinscripcion + '&anio=' + anio + '&mes=' + mes,
            success: function (datos) {
                if (datos == 1)
                {
                    $("#TbConsulta tbody tr#" + count).next().focus();
                    $("#TbConsulta tbody tr#" + count).remove();
                }
                //$("#TbConsulta tbody tr#"+(parseInt(count)+1)).focus();
            }
        })
    }
    var ObjTr = ''
    function CuentaCorriente(nroinscripcion, obj)
    {
        ObjTr = $(obj).parent().parent();
        //AbrirPopupImpresion("<?php echo $_SESSION['urldir']; ?>sigco/catastro/listados/cuentacorriente/Pdf.php?NroInscripcion="+nroinscripcion,Tam[0],Tam[1])
        $("#nroinscripcion").val(nroinscripcion)
        $.ajax({
            url: '<?php echo $_SESSION['urldir']; ?>sigco/catastro/listados/cuentacorriente/Consulta.php',
            type: 'POST',
            async: true,
            data: 'NroInscripcion=' + nroinscripcion,
            success: function (datos)
            {

                $("#detalle-CuentaCorriente").empty().append(datos);
                $("#DivCuentaCorriente").dialog('open');

            }
        })
    }
    
    function check_listado(obj, nrolistado)
    {
        var Op = 0;
        if (obj.checked)
            Op = 1;
        $.ajax({
            url: 'activarlistado.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nrolistado=' + nrolistado + '&Op=' + Op,
            success: function (datos) {
                if (Op == 1)
                {
                    Msj(obj, 'Listado Activado');
                    $("#lbl_listado" + nrolistado).html('Activo')
                }
                else
                {
                    Msj(obj, 'Listado Desactivado');
                    $("#lbl_listado" + nrolistado).html('Desactivo')
                }
                cListado(nrolistado)
                //$("#TbConsulta tbody tr#"+(parseInt(count)+1)).focus();
            }
        })
    }
</script>
<div align="center">
    <form id="form1" name="form1">
        <table width="95%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="2" >
                        <input type="hidden" id="nroinscripcion">
                        <div id="tabs">
                            <ul>
                                <li title="Datos Basicos"><a href="#tabs-1">Generar</a></li>
                                <li title="Imprimir"><a href="#tabs-2">Imprimir</a></li>		
                                <li title="Anular Listados"><a href="#tabs-3">Anular</a></li>	
                            </ul>
                            <div id="tabs-1" title="Generar">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <fieldset style="padding:4px">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr><td colspan="5">&nbsp;</td></tr>
                                                    <tr>
                                                        <td>N° Listado</td>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <!-- <input type="text" name="nrolistado" id="nrolistado" readonly="readonly" size="10" maxlength="6" class="inputtext" /> -->
                                                            <select id="nrolistado" name="nrolistado" style="width:150px" onchange="cnombrelistado()">
                                                                <option value='0' >Nuevo Listado</option>
                                                                <?php
                                                                $consulta = $conexion->prepare("SELECT nrolistado,nombrelistado FROM medicion.cablistadocorte WHERE codemp=1 AND codsuc=".$codsuc." AND estareg=1 ORDER BY fechareg DESC");
                                                                $consulta->execute();
                                                                $items = $consulta->fetchAll();
                                                                foreach ($items as $row) {

                                                                    echo "<option value='".$row["nrolistado"]."' >".$row["nombrelistado"]."</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                            <div style="display:inline;float:right">Nombre : </div>
                                                        </td>
                                                        <td align="left" colspan="2">
                                                            <input type="text" name="nombrelistado" id="nombrelistado" size="40" class="inputtext" />
                                                        </td>
                                                    </tr>
                                                    <tr><td colspan="5">&nbsp;</td></tr>
                                                    <tr>
                                                        <td width="10%">Sucursal</td>
                                                        <td width="3%" align="center">:</td>
                                                        <td >
                                                            <!-- <label>
                                                                     <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                                                            </label> -->
                                                            <?php
                                                            $consulta = $conexion->prepare("select * from admin.sucursales WHERE codsuc=".$codsuc." ORDER BY codsuc");
                                                            $consulta->execute();
                                                            $items = $consulta->fetchAll();
                                                            echo "<div class='multiselect' id='DivSucu'>";
                                                            foreach ($items as $row) {
                                                                $selected = "";
                                                                if ($_SESSION['IdSucursal'] == $row["codsuc"]) {
                                                                    $selected = "selected='selected'";
                                                                }
                                                                "<option value='".$row["codsuc"]."' ".$selected." >".$row["descripcion"]."</option>";
                                                                echo "<label><input type='checkbox' id='chcksuc_".$row["codsuc"]."' value='".$row["codsuc"]."' onclick='chcksuc(this)'/>".$row["descripcion"]."</label>";
                                                            }
                                                            echo "</div>";
                                                            ?>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="chksucursales" id="chksucursales" value="checkbox"  onclick="chcksucall(this)" />
                                                        </td>
                                                        <td>Todos las Sucursales </td>
                                                    </tr>
                                                    <tr><td colspan="5">&nbsp;</td></tr>
                                                    <tr>
                                                        <td>Ciclo</td>
                                                        <td align="center">:</td>
                                                        <td width="35%">
                                                            <? /*$objDrop->drop_ciclos($codsuc,0,"onchange='datos_facturacion(this.value);valida(this.value);'"); */?>
                                                            <div class='multiselect' id='DivCiclo'>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="chkciclos" id="chkciclos" value="checkbox"  onclick="chckcicloall(this)" />
                                                        </td>
                                                        <td>Todos los Ciclos </td>
                                                    </tr>
                                                    <tr><td colspan="5">&nbsp;</td></tr>
                                                    <tr>
                                                        <td>Sector</td>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <?php /* echo $objDrop->drop_sectores($codsuc,0,"onchange='cargar_rutas_lecturas(this.value,0);'"); */ ?>
                                                            <div class='multiselect' id='DivSector'>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" onclick="chcksectorall(this)" />
                                                        </td>
                                                        <td>Todos los Sectores </td>
                                                    </tr>
                                                    <tr><td colspan="5">&nbsp;</td></tr>
                                                    <tr>
                                                        <td>Ruta</td>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <div class='multiselect' id='DivRuta'>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox"  onclick="chckrutaall(this);CambiarEstado(this, 'todosrutas');" />
                                                        </td>
                                                        <td>Todas las Rutas</td>
                                                    </tr>
                                                    <tr><td colspan="5">&nbsp;</td></tr>
                                                    <tr>
                                                        <td>Estado</td>
                                                        <td align="center">:</td>
                                                        <td><?/*$objDrop->drop_estado_servicio();*/ ?>
                                                            <div class='multiselect' id='DivEstado'>
                                                            <?php
                                                            $Sql = "SELECT codestadoservicio,descripcion
                                                                FROM   public.estadoservicio 
                                                                WHERE estareg = 1 ORDER BY codestadoservicio";
                                                            $Consulta = $conexion->query($Sql);
                                                            foreach ($Consulta->fetchAll() as $row) {

                                                                echo "<label><input type='checkbox' id='chckestado_".$row[0]."' value='".$row[0]."' onclick='chckestado(this)' />".$row[1]."</label>";
                                                            }
                                                            ?>
                                                            </div>
                                                        </td>
                                                        <td align="center"><input type="checkbox" name="chkestados" id="chkestados" value="checkbox" onclick="chckestadoall(this);
                                                    CambiarEstado(this, 'todosectores');" /></td>
                                                        <td>Todos los Estados de Servicios </td>
                                                    </tr>
                                                    <tr><td colspan="5">&nbsp;</td></tr>
                                                    <tr>
                                                        <td>Meses</td>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <input type="text" name="mesdesde" id="mesdesde" value="2" size="10" maxlength="6" class="inputtext entero" />&nbsp;-&nbsp;
                                                            <input type="text" name="meshasta" id="meshasta" value="999999" size="10" maxlength="6" class="inputtext entero" /></td>
                                                        <td align="center">&nbsp;</td>
                                                        <td>
                                                            <input type="hidden" name="anio" id="anio" value="0" />
                                                            <input type="hidden" name="mes" id="mes" value="0" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">Tipo de Servicio</td>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <select id="tiposervicio" name="tiposervicio" class="select" style="width:220px">
                                                                <option value="4">AGUA / AGUA Y DESAGUE</option>
                                                                <option value="3">DESAGUE</option>
                                                                <option value="2">AGUA</option>
                                                                <option value="1">AGUA Y DESAGUE</option>
                                                            </select>
                                                        </td>
                                                        <td align="center">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Grupo</td>
                                                        <td align="center">:</td>
                                                        <td colspan="">
                                                            <div id="div_grupo">
                                                                <?php $objDrop->drop_tipo_entidades(); ?>
                                                            </div>
                                                        </td>
                                                        <td align="center">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr><td colspan="5"><hr></td></tr>
                                                    <tr>
                                                        <td colspan="5" align="center">
                                                            <div id="DivIncluir">
                                                                <input type="checkbox"  id="I1" value="1"/>
                                                                Incluir Comprobantes en Reclamos
                                                                <input type="checkbox"  id="I2" value="2"/>
                                                                Incluir Cuotas de Convenio
                                                                <input type="checkbox"  id="I3" value="3"/>
                                                                Incluir Fecha de &uacute;ltima Nota Emitida
                                                            </div>
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td align="center">&nbsp;</td>
                                                        <td>

                                                            <input type="hidden" name="todosrutas" id="todosrutas" value="1" />
                                                            <input type="hidden" name="todosectores" id="todosectores" value="2" />
                                                            <input type="hidden" name="orden" id="orden" value="3" />
                                                            <input type="hidden" name="todosestados" id="todosestados" value="4" />

                                                        </td>
                                                        <td align="center">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" align="center">
                                                            <div id="DivOrdenar">
                                                                <input type="radio" name="porrutas" id="porrutas" value="1" checked="checked" onclick="javascript:$('#orden').val('1')" />
                                                                Ordenar por Rutas
                                                                <input type="radio" name="porrutas" id="porrutasmanzanas" value="2" onclick="javascript:$('#orden').val('2')" />
                                                                Ordenar por Rutas y Manzanas
                                                                <input type="radio" name="porrutas" id="porrutascalles" value="3" onclick="javascript:$('#orden').val('3')" />
                                                                Ordenar por Calles
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr><td colspan="5"><hr></td></tr>
                                                    <tr>
                                                        <td width="10%">Services</td>
                                                        <td width="3%" align="center">:</td>
                                                        <td >
                                                            <?php
                                                            $consulta = $conexion->prepare("select * from admin.services WHERE codsuc=".$codsuc." ORDER BY codservices");
                                                            $consulta->execute();
                                                            $items = $consulta->fetchAll();
                                                            echo '<select name="codservices" id="codservices" style="width:220px" class="select">
                                                                                            <option value="0">--Seleccione la Services--</option>';
                                                            foreach ($items as $row) {
                                                                $selected = "";
                                                                if ($_SESSION['codservices'] == $row["codservices"]) {
                                                                    $selected = "selected='selected'";
                                                                }
                                                                echo "<option value='".$row["codservices"]."' ".$selected." >".$row["descripcion"]."</option>";
                                                            }
                                                            echo "</div>";
                                                            ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="4" align="center">
                                                            <input type="button" value="Generar" id="btnVerificar" onclick="ValidarForm();" />
                                                            <input type="button" value="Generando" id="BtnLoading" onmousemove="Sobre(this);" onmouseout="Fuera(this);" class="Botones"/>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-2" title="Imprimir">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2">

                                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                <tr><td colspan="4">&nbsp;</td></tr>
                                                <tr>
                                                    <td width="90" align="left">N° Listado</td>
                                                    <td width="30" align="center">:</td>
                                                    <td align="left">
                                                        <select name="cmbnrolistado" id="cmbnrolistado" style="width:150px">
                                                        <?php
                                                        $consulta = $conexion->prepare("SELECT nrolistado,nombrelistado FROM medicion.cablistadocorte WHERE codemp=1 AND codsuc=".$codsuc." AND estareg=1 ORDER BY fechareg DESC");
                                                        $consulta->execute();
                                                        $items = $consulta->fetchAll();
                                                        foreach ($items as $row) {

                                                            echo "<option value='".$row["nrolistado"]."' >".$row["nombrelistado"]."</option>";
                                                        }
                                                        ?>
                                                        </select>
                                                    </td>
                                                    <td align="left"><table width="300" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="center"><input type="button" value="Guardar" id="BtnAceptar" onclick="Consultar();" onmousemove="Sobre(this);" onmouseout="Fuera(this);"/></td>
                                                                <td align="center"><input id="BtnExcel" type="button" value="Excel" onclick="Excel();" onmousemove="Sobre(this);" onmouseout="Fuera(this);" class="BtnIndex"/></td>
                                                                <td align="center"><input id="BtnPdf" type="button" value="Pdf" onclick="Pdf();" onmousemove="Sobre(this);" onmouseout="Fuera(this);" class="BtnIndex"/></td>
                                                            </tr>
                                                        </table></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="left">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" height="40" align="center">&nbsp;</td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                    <tr id="TrResultados">
                                        <td colspan="2" align="center">
                                            <fieldset>
                                                <legend class="ui-widget-header">Resultados de la Consulta</legend>
                                                <div id="ImgLoad" style="text-align: center"><img src="../../../../images/iconos/avance.gif" /><br/>Cargando ...</div>
                                                <div style="height:auto; overflow:auto; display:none " align="center" id="DivConsulta">
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr> 

                                </table>	
                            </div>
                            <div id="tabs-3" title="Anular">
                                <table>
                                    <tr>
                                        <th scope="col" colspan="2" align="center">LISTADOS PARA CORTE DE SERVICIOS</th>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            Buscar:&nbsp;<input id="Filtro2" class="inputtext" value="" maxlength="30" size="30" type="text" />
                                        </td>
                                    </tr>
                                    <tr>


                                        <td colspan="2">
                                            <div style="overflow:auto; height:400px" id="DivTbListados">	
                                                <table class="ui-widget" border="1" cellspacing="0" id="TbListados" width="100%" rules="rows">
                                                    <thead class="ui-widget-header" style="font-size:10px">
                                                        <tr title="Cabecera">
                                                            <th scope="col" width="100" align="center">ITEM</th>
                                                            <th scope="col" width="100" align="center">FECHA</th>
                                                            <th scope="col" width="200" align="center">PERIODO</th>
                                                            <th scope="col" width="400" align="center">DESCRIPCION</th>
                                                            <th scope="col" width="100" align="center">ESTADO</th>
                                                            <th scope="col" width="50" align="center">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="font-size:10px">
                                                        <?php
                                                        $ctd = 0;
                                                        ///////////////
                                                        $sql = "SELECT nrolistado,fechareg,anio,mes,nombrelistado ,estareg
                                                            FROM medicion.cablistadocorte 
                                                            WHERE codemp=1 AND codsuc=".$codsuc." ORDER BY fechareg DESC";
                                                        //echo $sql."<br>";
                                                        //die($sql);
                                                        $consulta = $conexion->query($sql);
                                                        $items = $consulta->fetchAll();
                                                        foreach ($items as $row) {
                                                            $ctd++;
                                                            ?>
                                                            <tr id="L<?=$count ?>">
                                                                <td align="center"><?=$ctd ?></td>
                                                                <td><?=$objDrop->DecFecha($row["fechareg"]) ?></td>
                                                                <td><?=$meses[$row["mes"]]." - ".$row["anio"] ?> &nbsp;</td>
                                                                <td><?=$row["nombrelistado"] ?></td>
                                                            <?php
                                                            $checked = "checked='checked'";
                                                            $estado = "Activo";
                                                            if ($row['estareg'] == 0) {
                                                                $checked = "";
                                                                $estado = "Inactivo";
                                                            }
                                                            ?>
                                                                <td><label id='lbl_listado<?=$row["nrolistado"] ?>'><?=$estado ?></label></td>
                                                                <td>
                                                                    <input type='checkbox' id='check_listado<?=$row["nrolistado"] ?>' <?=$checked ?> class='input-chk' onclick='check_listado(this,<?=$row["nrolistado"] ?>)'>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>

                                                                <?php
                                                                /* $objReporte->Contenido(utf8_decode($row["propietario"]),$row["direccion"],$row["nromeses"],
                                                                  $row["imptotal"],$row["catastro"],$ts,$tf,$row["nromed"],$count);
                                                                 */
                                                            }
                                                            ////////////
                                                            //}//IF CICLOS
                                                            //}//FOR CICLOS
                                                            //}//FOR SUC
                                                            ?>

                                                    </tbody>
                                                    <tfoot class="ui-widget-header">
                                                        <tr>
                                                            <td colspan="6" align="center">&nbsp;</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>

                                </table>	
                            </div>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </tbody>

        </table>
    </form>
</div>
<script>
    $("#codsector").attr("disabled", true)
    $("#estadoservicio").attr("disabled", true)
    $("#BtnAceptar").attr('value', 'Consultar')
    $("#BtnAceptar").css('background-image', 'url(../../../../css/images/ver.png)')
    $("#TrResultados").hide()
    //$("#btnAceptar").attr('type','button')
</script>
<div id="DivCuentaCorriente" title="Ver Cuenta Corriente"  >
    <div id="detalle-CuentaCorriente">
    </div>
</div>
<?php CuerpoInferior(); ?>