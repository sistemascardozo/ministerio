<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	set_time_limit(0);

	include("../../../../objetos/clsFunciones.php");
	$nrolistado = $_GET['nrolistado'];
	$codsuc		= $_SESSION['IdSucursal'];
	$objFunciones = new clsFunciones();


	header("Content-type: application/vnd.ms-excel; name='excel'");
    header("Content-Disposition: filename=Listado.xls");
    header("Pragma: no-cache");
    header("Expires: 0");

	//////////////
	$codemp		= 1;
	$Select="SELECT codservices,codtipolistado,codsectores,
	codrutas,codestados,mesini,mesfin,incluir,ordenar,codsucursales,codciclos,codsuc
	FROM  medicion.cablistadocorte
	WHERE nrolistado=".$nrolistado." AND codsuc=".$codsuc;
	$Consulta=$conexion->query($Select);
	$row=$Consulta->fetch();
	$codservices=$row['codservices'];
	$codtipolistado=$row['codtipolistado'];
	$codsectores=$row['codsectores'];
	$codrutas=$row['codrutas'];
	$codestados=$row['codestados'];
	$inicio=$row['mesini'];
	$final=$row['mesfin'];
	$incluir=$row['incluir'];
	$orden=$row['ordenar'];
	$codsucursales=$row['codsucursales'];
	$codciclos=$row['codciclos'];
	$codsuc=$row['codsuc'];

	if($orden==1){$ord = "clie.nroorden";}else{$ord = "clie.nroorden";}
	$ord ="ORDER BY  lst.codsuc,lst.codsector,CAST(lst.codrutlecturas AS INTEGER ),CAST(lst.codmanzanas AS INTEGER) ,CAST(lst.lote AS INTEGER)";
	$count=0;
	$monto=0;

	$codsucursales = split('[-]',$codsucursales);
	$codciclos=trim($codciclos);
	$codciclos = split('[=]',$codciclos);
	$codsectores=trim($codsectores);
	$codsectores = split('[=]',$codsectores);
	$codestados=trim($codestados);
	$codestados=str_replace('=',',',$codestados);
	$AndEst = " clie.codestadoservicio IN( ".$codestados." )";

	$Select="SELECT codtipoestado,descripcion
	FROM medicion.tipoestado ORDER BY codtipoestado;";
	$Consulta=$conexion->query($Select);
	$Estados="";
	$at = array(NULL);
	foreach($Consulta->fetchAll() as $row)
	{

		$at[$c]=$row[0].'='.$row[1];
		$c++;


	}//FOR SECTORES
	$Estados ="Estado = ".implode(",", $at);
	////////////////

?>



<table class="ui-widget" border="1" cellspacing="0" id="TbConsulta" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
    <tr title="Cabecera">
    	 <th scope="col" class="ui-widget" colspan="12" align="center">LISTADO PARA CORTE DE SERVICIOS</th>
    </tr>
      <tr title="Cabecera">
        <th scope="col" width="100" align="center" rowspan="2">ITEM</th>
        <th scope="col" width="150" align="center" rowspan="2">COD. GEOGRAFICO</th>
        <th scope="col" width="100" align="center" rowspan="2">S -R - Sec</th>
        <th scope="col" width="100" align="center" rowspan="2">INSCRITO</th>
        <th scope="col" width="400" align="center" rowspan="2">APELLIDOS Y NOMBRES</th>
        <th scope="col" width="400" align="center" rowspan="2">DIRECCION</th>
        <th scope="col" width="100" align="center">DEUDA</th>
        <th scope="col" width="100" align="center" colspan="3">MEDIDOR</th>
        <th scope="col" width="400" align="center" rowspan="2" colspan="2">INFORME DE TRABAJO REALIZADO</th>
			</tr>
			<tr>
				<th width="100" align="center">Meses</th>
				<th width="100" align="center">N°</th>
				<th width="100" align="center">Retirado</th>
				<th width="100" align="center">Lect.(m3)</th>
			</tr>
    </thead>
    <tbody style="font-size:10px">
    <?php
    $ctd=0;
   /* for ($i=0; $i <count($codsucursales) ; $i++)
     {
     	$codsuc=$codsucursales[$i];
     	//SECTORES INCLUIDOS//
     	$AndSec="";
     	$c=0;
     	$at1 = array(NULL);
   		for ($is=0; $is <count($codsectores) ; $is++)
		{

			$codsector = split('[-]',$codsectores[$is]);

			if($codsuc==$codsector[0])
			{
				$sector =$codsector[1];
				$at1[$c]=" clie.codsector= ".$sector."  ";
				$c++;
			}//IF SECTORES

		}//FOR SECTORES
		$AndSec = "( ".implode("OR", $at1)." )";
     	//SECTORES INCLUIDOS//
		for ($ic=0; $ic <count($codciclos) ; $ic++)
		{

			$codciclo = split('[-]',$codciclos[$ic]);
			//print_r($codciclo);
			if($codsuc==$codciclo[0])
			{
				$ciclo =$codciclo[1];
*/
				///////////////

				$sql = "SELECT lst.nroinscripcion, clie.propietario,
						tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle AS direccion,
						lst.nromeses, lst.imptotal, ".$objFunciones->getCodCatastral("clie.").", clie.codtiposervicio,
						( clie.codsector ||' - '|| clie.codrutlecturas ||' - '|| co.orden_lect) AS secrutsecu,
						clie.tipofacturacion, clie.nromed, clie.codantiguo,
						clie.codrutlecturas, clie.nromed
						FROM medicion.listadocorte AS lst
						JOIN catastro.clientes AS clie ON(lst.codemp=clie.codemp AND lst.codsuc=clie.codsuc AND lst.nroinscripcion=clie.nroinscripcion)
						JOIN catastro.conexiones AS co ON(clie.codemp=co.codemp AND clie.codsuc=co.codsuc AND clie.nroinscripcion=co.nroinscripcion)
						JOIN public.calles AS cal ON(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona)
						JOIN public.tiposcalle AS tipcal ON(cal.codtipocalle=tipcal.codtipocalle)
						WHERE lst.codsuc=".$codsuc." AND lst.nrolistado=".$nrolistado."  ".$ord;

			//	echo $sql."<br>";
		$consulta=$conexion->query($sql);
		$items = $consulta->fetchAll();
		foreach($items as $row)
		{
			$ts="";
			$tf="";
			$count++;

			if($row["codtiposervicio"]==1){$ts="Ag/Des";}
			if($row["codtiposervicio"]==2){$ts="Ag.";}
			if($row["codtiposervicio"]==3){$ts="Des.";}

			if($row["tipofacturacion"]==0){$tf="L";}
			if($row["tipofacturacion"]==1){$tf="P";}
			if($row["tipofacturacion"]==2){$tf="A";}

	        $monto += $row["imptotal"];
	        $ctd++;
	         ?>
	         <tr>
	         	<td align="center"><?=$ctd?></td>
	         	<td align="center"><?=$row["codcatastro"]?></td>
	         	<td align="center" style="mso-number-format:'\@'"><?=$row['secrutsecu']?></td>
	         	<td style="mso-number-format:'\@'"><?=$objFunciones->CodUsuario($codsuc,$row['nroinscripcion'])?></td>
	         	<td><?=utf8_decode($row["propietario"])?></td>
	         	<td><?=utf8_decode($row["direccion"])?></td>
	         	<td align="center"><?=$row["nromeses"]?></td>
	         	<td align="right"><?=$row["nromed"]?></td>
	         	<!--<td><?=$row["nromed"]?></td>-->
	         	<!--<td><td><?=$ts?></td>-->
	         	<td align="center">&nbsp;</td>
	         	<td align="center">&nbsp;</td>
	         	<!--<td><?=$tf?></td>-->
	         	<td align="center">&nbsp;&nbsp;</td>
	         	<td align="center"><?=$row["imptotal"]?></td>
	         	<!--<td><?=$row["estadoreg"]?></td>-->
	         </tr>

	    <?php

	    			/*$objReporte->Contenido(utf8_decode($row["propietario"]),$row["direccion"],$row["nromeses"],
	                        $row["imptotal"],$row["catastro"],$ts,$tf,$row["nromed"],$count);
*/
		}
				////////////
	/*
	}//IF CICLOS

		}//FOR CICLOS

     }//FOR SUC
*/
    ?>

    </tbody>
    <tfoot class="ui-widget-header">
    	<tr>
         <td colspan="7" align="center">
         	TOTAL USUARIOS:<?= $ctd?>

         </td>
         <td colspan="2" >Monto Deuda:</td>
         <td align="right" ><?=$monto?></td>
         <td></td><td></td>
         </tr>
        <tr>
        	<td colspan='10'><?=$Estados?></td>
        </tr>
    </tfoot>
</table>
