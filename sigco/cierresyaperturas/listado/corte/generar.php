<?php	
    session_name("pnsu");
    if(!session_start()){session_start();}

    set_time_limit(0);
    //ini_set("display_errors",1);
    include("../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();

    $codemp      = 1;
    $NroSuc      = $_POST['NroSuc'];
    $NroCic      = $_POST["NroCic"];
    $NroSec      = $_POST["NroSec"];
    $NroRut      = $_POST["NroRut"];
    $NroEst      = $_POST["NroEst"];
    $codservices = $_POST["codservices"];
    $mesini      = $_POST["mesdesde"];
    $mesfin      = $_POST["meshasta"];
    $tiposervicio= $_POST['tiposervicio'];
    $tipoentidades= $_POST['tipoentidades'];
    $NroInc     = $_POST["NroInc"];
    $NroOrd      = $_POST["NroOrd"];
    $nrolistado      = $_POST["nrolistado"];
    $nombrelistado      = $_POST["nombrelistado"];
    $codtipolistado=1;//creado
    $observacion='';
    $estareg=1;//
    $fechareg = date('Y-m-d');//$objFunciones->FechaServer();
    $horareg = $objFunciones->HoraServidor();
    $codsuc=$_SESSION['IdSucursal'];
    $codciclo=1;
    $facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
    $anio =$facturacion["anio"];
    $mes =$facturacion["mes"];
    
    if($nrolistado==0)
    {
        $Select=" SELECT nrolistado FROM medicion.cablistadocorte 
        WHERE codemp=".$codemp." AND codsuc=".$codsuc." /*AND anio='".$anio."' AND mes='".$mes."'*/";
        $Consulta=$conexion->query($Select);
        $row=$Consulta->fetch();
        if($row[0]!='')
        {
            $Select=" SELECT max(nrolistado) 
            FROM medicion.cablistadocorte 
            WHERE codemp=".$codemp." AND codsuc=".$codsuc." /*AND anio='".$anio."' AND mes='".$mes."'*/";
            $Consulta=$conexion->query($Select);
            $row=$Consulta->fetch();
            if($row[0]=='')
                    $nrolistado=1;
            else
                    $nrolistado=$row[0]+1;

        }
		else
			$nrolistado=1;//$row[0];
    }

    $codsucursales='';
	
    for ($i=1; $i <=$NroSuc ; $i++) 
    {
            $tem = trim($_POST["suc_".$i]);
			
            if($i == $NroSuc)
				$codsucursales .=$tem;
            else
				$codsucursales .=$tem."-";
    }
    
    $codciclos = '';
	
    for ($i=1; $i <=$NroCic ; $i++) 
    {
            $tem=trim($_POST["ciclo_".$i]);
            if($i==$NroCic)
                    $codciclos .=$tem;
            else
                    $codciclos .=$tem."=";
    }
    
    $codsectores='';
    for ($i=1; $i <=$NroSec ; $i++) 
    {
            $tem=trim($_POST["sector_".$i]);
            if($i==$NroSec)
                    $codsectores .=$tem;
            else
                    $codsectores .=$tem."=";
    }
    
    $codrutas='';
    for ($i=1; $i <=$NroRut ; $i++) 
    {
            $tem=trim($_POST["ruta_".$i]);
            if($i==$NroRut)
                    $codrutas .=$tem;
            else
                    $codrutas .=$tem."=";
    }
    
    $codestados='';
    for ($i=1; $i <=$NroEst ; $i++) 
    {
            $tem=trim($_POST["estado_".$i]);
            if($i==$NroEst)
                    $codestados .=$tem;
            else
                    $codestados .=$tem."=";
    }
    
    $incluir='';
    for ($i=1; $i <=$NroInc ; $i++) 
    {
            $tem=trim($_POST["estado_".$i]);
            if($i==$NroInc)
                    $incluir .=$tem;
            else
                    $incluir .=$tem."=";
    }
    
    $ordenar='';
    for ($i=1; $i <=$NroOrd ; $i++) 
    {
            $tem=trim($_POST["estado_".$i]);
            if($i==$NroOrd)
                    $ordenar .=$tem;
            else
                    $ordenar .=$tem."=";
    }
    $conexion->beginTransaction();

    $Sql = "DELETE FROM medicion.cablistadocorte WHERE nrolistado=".$nrolistado." AND codsuc=".$codsuc;
    $Consulta = $conexion->query($Sql);

    $Sql = "DELETE FROM medicion.listadocorte WHERE nrolistado=".$nrolistado." AND codsuc=".$codsuc;
    $Consulta = $conexion->query($Sql);
	
	$Select = "INSERT INTO medicion.cablistadocorte( nrolistado,codemp,codservices,codtipolistado,
	codsectores,codrutas,codestados,mesini,mesfin,
	incluir,ordenar,fechareg,horareg,observacion,
	estareg,codsucursales,codciclos,anio,mes,codsuc,nombrelistado) 
	VALUES (
  	".$nrolistado.",".$codemp.",".$codservices.",".$codtipolistado.",
  	'".$codsectores."','".$codrutas."','".$codestados."',".$mesini.",".$mesfin.",
  	'".$incluir."','".$ordenar."','".$fechareg."','".$horareg."',
  	'".$observacion."',".$estareg.",'".$codsucursales."','".$codciclos."','".$anio."','".$mes."',".$codsuc.",'".$nombrelistado."');";
	$Consulta=$conexion->query($Select);
	//SECTORES INCLUIDOS//
	$codsucursales = split('[-]',$codsucursales);
	$codsectores=trim($codsectores);
	$codrutas=trim($codrutas);
	$codrutas = split('[=]',$codrutas);
	$codsectores = split('[=]',$codsectores);
    
    for ($i=0; $i < count($codsucursales); $i++) 
    { 
     	$codsuc=$codsucursales[$i];
     	//SECTORES INCLUIDOS//
     	$AndSec="";
     	$c=0;
     	$c1=0;
     	$at1 = array(NULL);
     	$at2 = array(NULL);
     	for ($is=0; $is <count($codsectores) ; $is++) 
        {
            $codsector = split('[-]',$codsectores[$is]);
			
            if($codsuc==$codsector[0]) 
            {	
                    $sector =$codsector[1];
                    $at1[$c]="".$sector."  ";
                    $c++;

            }//IF SECTORES

            //rutas		
            for ($is2=0; $is2 <count($codrutas) ; $is2++) 
            {
                $codruta = split('[-]',$codrutas[$is2]);

                if($codsector[1]==$codruta[0]) 
                {	
                        $ruta =$codruta[1];
                        $at2[$c1]="".$ruta."  ";
                        $c1++;

                }//IF rutas

            }//FOR rutas

        }//FOR SECTORES
		
    }
	
	$CodEst= " ".str_replace("=", ",", $codestados)." ";
	$AndSec = " ".implode(",", $at1)." ";
	$AndRut = " ".implode(",", $at2)." ";
	/*
        if($tiposervicio==4)
        {
            $tiposervicio="1,2";
        }
         * 
         */
        //die();
     	//SECTORES INCLUIDOS//
        //die($AndSec);
	for ($i=1; $i <=$NroSuc ; $i++) 
	{ 
		$codsuc=trim($_POST["suc_".$i]);
		for ($i2=0; $i2 <=$NroCic ; $i2++) 
		{ 
			$codciclo=trim($_POST["ciclo_".$i2]);
			$codciclo = split('[-]',$codciclo); 
			if($codsuc==$codciclo[0]) 
			{
				$codciclo=$codciclo[1];
                
				$sql = "SELECT * FROM medicion.f_corte('".$anio."','".$mes."',".$codsuc.",".$codciclo.",".$nrolistado.",".$mesini.",".$mesfin.",'".$AndSec."','".$AndRut."','".$CodEst."','".$tiposervicio."', '".$tipoentidades."'  )"; 
				$Consulta=$conexion->query($sql); 
	   		}//IF CICLOS

		}//FOR CICLOS
	}//FOR SUCURSAL

	if(!$Consulta)
	{
		$conexion->rollBack();
		$res=0;
	}else{
		
            //VERIFICAR SI AY DATOS
            $Select="SELECT * FROM medicion.listadocorte WHERE nrolistado=".$nrolistado." AND codsuc=".$codsuc;
            $Consulta=$conexion->query($Select);
            $nr= $Consulta->rowCount();
            if($nr==0)
            {
                $conexion->rollBack();
                $res='a';
            }
            else
            {
                $conexion->commit();
                $res=$nrolistado;
            }
	}
	
	echo $res;
	
?>
