// JavaScript Document
function quitar_disabled(obj, input)
{
    if (obj.checked)
    {
        $("#"+input).attr("disabled", true)
    } else {
        $("#"+input).attr("disabled", false)
    }
}
function validar_check()
{
    $("#chkrutas").attr("checked", true)
    $("#todosrutas").val(1)
    $("#rutaslecturas").attr("disabled", true)
}
function cargar_rutas_lecturas(obj, cond)
{
    $.ajax({
        url: '../../../../ajax/rutas_lecturas_drop.php',
        type: 'POST',
        async: true,
        data: 'codsector='+obj+'&codsuc='+codsuc+'&condicion='+cond,
        success: function (datos) {
            $("#div_rutaslecturas").html(datos)

            $("#chkrutas").attr("checked", true)
            $("#todosrutas").val(1)

            $("#rutaslecturas").attr("disabled", true)
        }
    })
}