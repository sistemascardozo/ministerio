<?php
include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$TituloVentana = "CORTADOS";
$Activo = 1;
CuerpoSuperior($TituloVentana);
$codsuc = $_SESSION['IdSucursal'];

$objMantenimiento = new clsDrop();

$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script>
    var codsuc = <?=$codsuc ?>;

    var c = 0
    function ver(h) {
    }
    function ValidarForm(Op)
    {
        var sectores = "";

        if ($("#ciclo").val() == 0)
        {
            alert('Seleccione el Ciclo')
            return false
        }
        if ($("#anio").val() == 0)
        {
            alert('Seleccione el A�o')
            return false
        }
        if ($("#mes").val() == 0)
        {
            alert('Seleccione el mes')
            return false
        }
        
        if ($("#todosectores").val() == 1)
        {
            sectores = "%"
        } else {
            sectores = $("#codsector").val()
            if (sectores == "")
            {
                alert("Seleccione el Sector")
                return false;
            }
        }
        
        if ($("#todomotivos").val() == 1)
        {
            var codmotivo = 0;
        } else {
            codmotivo = $("#motivocorte").val()
            if (codmotivo == 0)
            {
                alert("Seleccione el Motivos del Corte")
                return false;
            }
        }
        
        if ($("#codservices").val() == 0)
        {
            alert('Seleccione Service')
            return false
        }
        var codservices = $("#codservices").val();
        
        url = "Pdf.php?ciclo="+$("#ciclo").val()+"&sector="+sectores+"&codsuc=<?=$codsuc ?>&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclotext="+$("#ciclo option:selected").text()+"&codmotivo="+codmotivo+"&codservices="+codservices;
        AbrirPopupImpresion(url, 800, 600)

        return false
    }
    function Cancelar()
    {
        location.href = '<?php echo $_SESSION['urldir']; ?>/admin/indexB.php'
    }
    function quitar_disabled(obj, input)
    {
        if (obj.checked)
        {
            $("#"+input).attr("disabled", true)
        } else {
            $("#"+input).attr("disabled", false)
        }
    }
    function cargar_anio_drop(obj)
    {
        $.ajax({
            url: '../../../../ajax/anio_drop.php',
            type: 'POST',
            async: true,
            data: 'codsuc=<?=$codsuc ?>&codciclo='+obj+'&condicion=1',
            success: function (datos) {
                $("#div_anio").html(datos)
            }
        })
    }
    function cargar_mes(ciclo, suc, anio)
    {
        $.ajax({
            url: '../../../../ajax/mes_drop.php',
            type: 'POST',
            async: true,
            data: 'codciclo='+ciclo+'&codsuc='+suc+'&anio='+anio,
            success: function (datos) {
                $("#div_meses").html(datos)
            }
        })
    }
    
    function VerMotivos()
    {
        if (!$('#chkmotivo').attr('checked'))
        {            
            //$("#motivocorte").attr('disabled', 'true');
            $("#motivocorte").removeAttr('disabled');
            $("#chkmotivo").removeAttr('checked');
            $("#todomotivos").val(0);
            
        } else {
            //$("#motivocorte").removeAttr('disabled');
            $("#motivocorte").attr('disabled', 'true');
            $("#chkmotivo").attr('checked', 'true');
            $("#todomotivos").val(1);
        }
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="2">
                        <fieldset style="padding:4px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td  colspan="5">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="13%">Sucursal</td>
                                    <td width="3%" align="center">:</td>
                                    <td colspan="3">
                                        <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ciclo</td>
                                    <td align="center">:</td>
                                    <td width="35%">
                                        <?php $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio_drop(this.value);'"); ?>
                                    </td>
                                    <td width="3%" align="center">&nbsp;</td>
                                    <td width="49%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>A&ntilde;o</td>
                                    <td align="center">:</td>
                                    <td>
                                        <div id="div_anio">
                                            <?php $objMantenimiento->drop_anio($codsuc,0); ?>
                                        </div>
                                    </td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>Meses</td>
                                    <td align="center">&nbsp;</td>
                                    <td>
                                        <div id="div_meses">
                                            <?php $objMantenimiento->drop_mes($codsuc,0,0); ?>
                                        </div>
                                    </td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Sector</td>
                                    <td align="center">:</td>
                                    <td>
                                        <?php echo $objMantenimiento->drop_sectores2($codsuc, 0, ""); ?>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="quitar_disabled(this, 'codsector');CambiarEstado(this, 'todosectores')" />
                                    </td>
                                    <td>Todos los Sectores </td>
                                </tr>
                                <tr>
                                    <td>Motivo Corte</td>
                                    <td align="center">:</td>
                                    <td>
                                        <?php $objMantenimiento->drop_motivo_corte(0); ?>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" name="chkmotivo" id="chkmotivo" value="checkbox" checked="checked" onclick="VerMotivos();" />
                                    </td>
                                    <td>Todos los Motivos </td>
                                </tr>
                                <tr>
                                    <td width="10%">Services</td>
                                    <td width="3%" align="center">:</td>
                                    <td >
                                        <?php
                                        $consulta = $conexion->prepare("select * from admin.services WHERE codsuc=".$codsuc." ORDER BY codservices");
                                        $consulta->execute();
                                        $items = $consulta->fetchAll();
                                        echo '<select name="codservices" id="codservices" style="width:220px" class="select">
                                                                        <option value="0">--Seleccione la Services--</option>';
                                        foreach ($items as $row) {
                                            $selected = "";
                                            if ($_SESSION['codservices'] == $row["codservices"]) {
                                                $selected = "selected='selected'";
                                            }
                                            echo "<option value='".$row["codservices"]."' ".$selected." >".$row["descripcion"]."</option>";
                                        }
                                        echo "</div>";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>
                                        <input type="hidden" name="todosectores" id="todosectores" value="1" />
                                        <input type="hidden" name="todomotivos" id="todomotivos" value="1" />
                                    </td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td colspan="5" align="center"><input type="button" onclick="return ValidarForm();" value="Generar" id=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="4" align="left"><div id="div_valida"></div></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>

            </tbody>
        </table>
    </form>
</div>
<script>
    $("#codsector").attr("disabled", true)
</script>
<?php CuerpoInferior(); ?>