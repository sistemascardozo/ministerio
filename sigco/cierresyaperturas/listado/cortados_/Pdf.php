<?php

include("../../../../objetos/clsReporte.php");
$objMantenimiento = new clsFunciones();

class clsRegistroVentas extends clsReporte {

    function cabecera() {
        global $m, $anio, $ciclotext;

        $this->SetFont('Arial', 'B', 14);

        $tit1 = "REGISTRO DE CORTADOS";
        $this->Cell(277, $h + 2, utf8_decode($tit1), 0, 1, 'C');

        $this->Ln(5);

        $h = 4;
        $this->SetFont('Arial', '', 7);
        $this->Cell(277, $h, $ciclotext, 0, 1, 'C');

        $this->SetX(113.5);
        $this->Cell(10, $h, "MES", 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(30, $h, $m, 0, 0, 'L');
        $this->Cell(10, $h, utf8_decode("AÑO"), 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(20, $h, $anio, 0, 1, 'L');

        $this->Ln(2);


        $this->Ln(1);
        $this->SetFont('Arial', '', 6);

        $this->SetWidths(array(8, 22, 17, 50, 45, 15, 30, 18, 25, 28, 18));
        $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"));
        $this->Row(array("ITEM",
            "COD. CATASTRAL",
            "INSCRIPCION",
            "PROPIETARIO",
            "DIRECCION",
            "FECHA",
            "MOTIVO",
            "TIP. CORTE",
            "INSPECTOR",
            "PERSONAL",
            "TP. SERV."
        ));
    }

    function agregar_detalle($codsuc, $codciclo, $anio, $mes, $codsector, $codmotivo, $codservices) 
    {
        global $conexion, $objMantenimiento;
        
        if ($codservices== 1)
        {$codservices=0;}
        
        $h = 4;

        $count = 0;

        $impmes = 0;
        $impigv = 0;
        $imptotal = 0;

        $this->SetTextColor(0, 0, 0);
        $cont_1 = 0;

        $sqlD = "SELECT cierr.nroinscripcion,clie.propietario,clie.codantiguo, ";
        $sqlD .= "cierr.fecha,motcorte.descripcion as motivo,tipcorte.descripcion as tipocorte, inspec.nombres as inspectores, ";
        $sqlD .= "case when cierr.tipopersonal=0 then 'PERSONAL PROPIO' else 'PERSONAL SERVICE' end as personal, ";
        $sqlD .= "cierr.nroinscripcion,cierr.observacion,cierr.codmotivocorte,cierr.codtipocorte, cierr.codinspector,cierr.tipopersonal,
            t.descripcioncorta || ' ' || c.descripcion || ' ' || clie.nrocalle as direccion,cierr.nromed, ";
        $sqlD .= $objMantenimiento->getCodCatastral("clie.").", s.descripcion as sector, cierr.tiposervicio ";
        $sqlD .= "FROM medicion.cierreyapertura as cierr ";
        $sqlD .= "INNER JOIN catastro.clientes as clie on(cierr.codemp=clie.codemp AND cierr.codsuc=clie.codsuc AND cierr.codsector=clie.codsector";
        $sqlD .= " AND cierr.nroinscripcion=clie.nroinscripcion AND cierr.codsector=clie.codsector) ";
        $sqlD .= "INNER JOIN public.motivocorte as motcorte on(cierr.codmotivocorte=motcorte.codmotivocorte) ";
        $sqlD .= "INNER JOIN public.tipocorte as tipcorte on(cierr.codtipocorte=tipcorte.codtipocorte) ";
        $sqlD .= "INNER JOIN reglasnegocio.inspectores as inspec on(cierr.codemp=inspec.codemp AND cierr.codsuc=inspec.codsuc AND cierr.codinspector=inspec.codinspector)
            INNER JOIN public.calles as c on(clie.codemp=c.codemp AND clie.codsuc=c.codsuc AND clie.codcalle=c.codcalle AND clie.codzona=c.codzona)
            INNER JOIN public.tiposcalle as t on(c.codtipocalle=t.codtipocalle)
            INNER JOIN public.sectores as s on(clie.codemp=s.codemp AND clie.codsuc=s.codsuc AND clie.codzona=s.codzona AND clie.codsector=s.codsector) ";
        $sqlD .= " WHERE cierr.codemp=1 AND cierr.codsuc=".$codsuc." AND cierr.codciclo=".$codciclo." 
            AND cierr.anio='".$anio."' AND cierr.mes='".$mes."' ";
        if ($codsector != "%")
            $sqlD .= " AND clie.codsector='".$codsector."' ";
        
        if ($codmotivo != 0)
            $sqlD .= " AND cierr.codmotivocorte='".$codmotivo."' ";
        
        if ($codservices != 0 || $codservices != 1)
            $sqlD .= " AND cierr.tipopersonal='".$codservices."' ";
        
        $sqlD .= " AND cierr.tipooperacion=0 ORDER BY codcatastro ";

        $consultaD = $conexion->query($sqlD);
        $itemsD = $consultaD->fetchAll();
        //var_dump($consulta->errorInfo());
        foreach ($itemsD as $row) {
            $count++;

            $this->SetWidths(array(8, 22, 17, 50, 45, 15, 30, 18, 25, 28, 18));
            $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"));
            $this->Row(array($count,
                $row['codcatastro'],
                $row['codantiguo'],
                utf8_decode(strtoupper($row["propietario"])),
                utf8_decode(strtoupper($row["direccion"])),
                $this->DecFecha($row["fecha"]),
                utf8_decode(strtoupper($row["motivo"])),
                utf8_decode(strtoupper($row["tipocorte"])),
                utf8_decode(strtoupper($row["inspectores"])),
                utf8_decode(strtoupper($row["personal"])),
                $row['tiposervicio']
            ));

            $impmes+=$row["impmes"];
            $impigv+=$row["impigv"];
            $imptotal += ($impmes + $impigv);
        }
    }

}

$ciclo = $_GET["ciclo"];
$sector = $_GET["sector"];
$codsuc = $_GET["codsuc"];
$anio = $_GET["anio"];
$mes = $_GET["mes"];
$ciclotext = $_GET["ciclotext"];
$codservices = $_GET["codservices"];

$codmotivo = $_GET["codmotivo"];

$m = $meses[$mes];


$objReporte = new clsRegistroVentas();
$objReporte->AliasNbPages();
$objReporte->AddPage("L");

$objReporte->agregar_detalle($codsuc, $ciclo, $anio, $mes, $sector, $codmotivo, $codservices);

$objReporte->Output();

?>