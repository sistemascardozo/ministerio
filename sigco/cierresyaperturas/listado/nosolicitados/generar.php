<?php
    
    session_name("pnsu");
    if(!session_start()){session_start();}

    set_time_limit(0);
    //ini_set("display_errors",1);
    include("../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();

    $codemp      = 1;
    $ciclo      = $_POST['ciclo'];
    $codsector  = $_POST["sector"];    
    $fdesde      = $_POST["fdesde"];
    $fhasta      = $_POST["fhasta"];
    $ciclotext = $_POST["ciclotext"];    

    $fechareg = date('Y-m-d');//$objFunciones->FechaServer();
    $horareg = $objFunciones->HoraServidor();
    $codsuc=$_SESSION['IdSucursal'];
    $codciclo=1;
    $facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
    
    $AndSect= '';
    if($codsector=='9999')
    {
        $AndSect= $AndSect; 
    }
    else {
        $AndSect= " AND cie.codsector = '".$codsector."' ";
    }
        
    $VerSector="SELECT DISTINCT se.codsector, se.descripcion
        FROM catastro.clientes AS clie
        INNER JOIN medicion.cierreyapertura AS cie ON clie.nroinscripcion = cie.nroinscripcion AND clie.codemp = cie.codemp AND clie.codsuc = cie.codsuc AND clie.codsector = cie.codsector
        INNER JOIN public.sectores AS se ON se.codemp = clie.codemp AND se.codsuc = clie.codsuc AND se.codsector = clie.codsector AND se.codzona = clie.codzona
        WHERE cie.codemp = 1 AND cie.codciclo = 1 AND cie.tipooperacion=0 AND cie.codsuc = 1 AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ".$AndSect." ";

    $VerSector.=" ORDER BY se.codsector ASC ";
    $consultaS = $conexion->query($VerSector);
    $itemsS = $consultaS->fetchAll();
    $c = 0;
    foreach ($itemsS as $rw0) 
    {
        $CodSectorN= $rw0[0];
        $Sector    = $rw0[1];

        $VerRuta="SELECT
            DISTINCT ru.codrutlecturas,
            ru.descripcion
            FROM
            catastro.clientes AS clie
            INNER JOIN medicion.cierreyapertura AS cie ON clie.nroinscripcion = cie.nroinscripcion AND clie.codemp = cie.codemp AND clie.codsuc = cie.codsuc AND clie.codsector = cie.codsector
            INNER JOIN public.rutasmaelecturas AS ru ON ru.codemp = clie.codemp AND ru.codsuc = clie.codsuc AND ru.codzona = clie.codzona AND ru.codsector = clie.codsector AND ru.codrutlecturas = clie.codrutlecturas
            WHERE
            cie.codemp = 1 AND cie.codsuc = 1 AND cie.codciclo = 1 AND ru.estareg=1 AND cie.tipooperacion=0
            AND clie.codsector = ".$CodSectorN."
            AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ";
        $VerRuta.=" ORDER BY ru.codrutlecturas ASC ";
        $consultaR = $conexion->query($VerRuta);
        $itemsR    = $consultaR->fetchAll();
        
        foreach ($itemsR as $rw1) 
        {
            $CodRuta= $rw1[0];
            $Ruta   = $rw1[1];
                
            $sqlD = "SELECT
                    clie.nroinscripcion, clie.codantiguo,
                    to_char(clie.codsuc, '00') || '-' || TRIM(to_char(clie.codsector, '00')) || '-' || TRIM(to_char(CAST(clie.codmanzanas AS INTEGER), '000')) || '-' || TRIM(to_char(CAST(clie.lote AS INTEGER), '0000')) || '-' || TRIM(clie.sublote) AS codcatastro,
                    clie.propietario,
                    cie.fecha,
                    tp.descripcioncorta || ' ' || ca.descripcion || ' ' || clie.nrocalle AS direccion
                    FROM
                    medicion.cierreyapertura AS cie
                    INNER JOIN catastro.clientes AS clie ON clie.nroinscripcion = cie.nroinscripcion AND clie.codemp = cie.codemp AND clie.codsuc = cie.codsuc AND clie.codsector = cie.codsector
                    INNER JOIN public.calles AS ca ON (clie.codemp = ca.codemp AND clie.codsuc = ca.codsuc AND clie.codcalle = ca.codcalle AND clie.codzona = ca.codzona)
                    INNER JOIN public.tiposcalle AS tp ON (ca.codtipocalle = tp.codtipocalle) ";
                $sqlD .= " WHERE cie.tipooperacion = 0 AND cie.codciclo=".$codciclo." AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ";
                if ($codsector == '9999')
                { $sqlD .= " AND clie.codsector=".$CodSectorN." AND clie.codrutlecturas=".$CodRuta; }
                else { $sqlD .= " AND clie.codsector='".$CodSectorN."' AND clie.codrutlecturas=".$CodRuta; }
               
                $sqlD .= " ORDER BY codcatastro, clie.codrutlecturas ASC ";

                $consultaD = $conexion->query($sqlD);
                $itemsD = $consultaD->fetchAll();

                foreach ($itemsD as $row) 
                {
                    $NroInscripcion= $row['nroinscripcion'];                   

                    $VerPago="SELECT
                        COUNT(DISTINCT ca.nroinscripcion)
                        FROM
                        cobranza.cabpagos AS ca
                        INNER JOIN cobranza.detpagos AS de ON de.codemp = ca.codemp AND de.codsuc = ca.codsuc AND de.nroinscripcion = ca.nroinscripcion AND de.nropago = ca.nropago
                        WHERE
                        ca.nroinscripcion=".$NroInscripcion." 
                        AND ca.fechareg BETWEEN '".$fdesde."' AND '".$fhasta."' ";
                    $conD = $conexion->query($VerPago);
                    $itD = $conD->fetch();
                    $VerCant=$itD[0];

                    if ($VerCant!=0) 
                    {
                        $VerReap="SELECT cie.nroinscripcion,
                            cie.tipooperacion, cie.fecha
                            FROM
                            medicion.cierreyapertura AS cie
                            WHERE
                            cie.nroinscripcion=$NroInscripcion
                            AND cie.tipooperacion=1
                            AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ";
                        $CondRea = $conexion->query($VerReap);
                        $ItemRea = $CondRea->fetch();

                        if($ItemRea==0)
                        {
                            /*$Upd="UPDATE medicion.cierreyapertura
                                SET nosolicitado=1, fechanosolcitado='$fechareg'
                                WHERE nroinscripcion=".$NroInscripcion;
                            $RwD = $conexion->query($Upd);*/
                            
                            $NroFac="SELECT pe.nrofacturacion FROM facturacion.periodofacturacion AS pe
                                WHERE pe.anio='2015' AND pe.mes='12'";
                            
                            $Insert="INSERT INTO medicion.cierreyapertura(
                                codemp, codsuc, codciclo, nroinscripcion, anio, mes, tipooperacion, 
                                codtipocorte, fecha, codinspector, codmotivocorte, tipopersonal, 
                                observacion, codsector, codusu, nromed, tiposervicio, nosolicitado, 
                                fechanosolcitado)
                            VALUES (1, 1, 1, '$NroInscripcion', ?, ?, ?, 
                                ?, ?, ?, ?, ?, 
                                ?, ?, ?, ?, ?, ?, 
                                ?) ";
                            
                        }
                        
                    }
                    
                }
        } 
        
    }
    
    if(!$RwD)
    {
        $conexion->rollBack();
        $res='a';
    }
    else
    {
        $conexion->commit();
        $res=$nrolistado;
    }
        
?>