<?php
    session_name("pnsu");
    if (!session_start()) {
        session_start();
    }
    
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");

    $TituloVentana = "REAPERTURADOS";
    $Activo = 1;
    CuerpoSuperior($TituloVentana);
    $codsuc = $_SESSION['IdSucursal'];
    $FechaHoy= date('d/m/Y');
    $objMantenimiento = new clsDrop();

    $sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript">
    var codsuc = <?=$codsuc ?>;

    var c = 0
    $(function () {
        $("#fdesde, #fhasta").datepicker({
            showOn: 'button',
            direction: 'up',
            buttonImage: '../../../../images/iconos/calendar.png',
            buttonImageOnly: true,
            showOn: 'both',
                    showButtonPanel: true
        });
    });
    
    function VerFecha()
    {
        if (!$('#chkfecha').attr('checked'))
        {
            
            $("#fecha").attr('disabled', 'true');
            $("#chkfecha").removeAttr('checked');
            $("#verfecha").val(0);
            
        } else {
            $("#fecha").removeAttr('disabled');
            $("#chkfecha").attr('checked', 'true');
            $("#verfecha").val(1);
        }
    }
    
    function ProcesarNoSol()
    {
        if ($("#ciclo").val() == 0)
        {
            alert('Seleccione el Ciclo')
            return false
        }
        
        if ($("#fdesde").val() == '')
        {
            alert('Seleccione el fecha')
            return false
        }
        if ($("#fhasta").val() == '')
        {
            alert('Seleccione el fecha')
            return false
        }
        
        if ($("#todossectores").val() == 1)
        {
            var sectores = "9999"
        } else {
            sectores = $("#codsector").val();
            if (sectores == "")
            {
                alert("Seleccione el Sector")
                return false;
            }
        }
        //alert(sectores);
        var Data="ciclo="+$("#ciclo").val()+"&sector="+sectores+"&codsuc=<?=$codsuc ?>&fdesde="+$("#fdesde").val()+"&fhasta="+$("#fhasta").val()+"&ciclotext="+$("#ciclo option:selected").text()
        $.ajax({
            url: 'generar.php',
            type: 'POST',
            async: true,
            data: Data,
            success: function (datos)
            {
                $("#btnVerificar").show();
                $("#BtnLoading").hide();
                if (datos != 0 && datos != 'a')
                {
                    Msj('#btnVerificar', 'Listado Generado con Exito', 2000, 'above', '', false)
                    
                }
                else
                {
                    if (Trim(datos) == 'a')
                        Msj('#btnVerificar', 'Ocurrio un Error al Generar', 2000, 'above', '', false)

                }

            }
        })
    }
    
    function ValidarForm(Op)
    {
        var sectores = "";
        var verfecha= $("#verfecha").val();
        var fecha= $("#fecha").val();
        
        if ($("#ciclo").val() == 0)
        {
            alert('Seleccione el Ciclo')
            return false
        }
        
        if ($("#fdesde").val() == '')
        {
            alert('Seleccione el fecha')
            return false
        }
        if ($("#fhasta").val() == '')
        {
            alert('Seleccione el fecha')
            return false
        }
        
        if ($("#todossectores").val() == 1)
        {
            sectores = "%"
        } else {
            sectores = $("#codsector").val()
            if (sectores == "")
            {
                alert("Seleccione el Sector")
                return false;
            }
        }

        var url = "Pdf.php?ciclo="+$("#ciclo").val()+"&sector="+sectores+"&codsuc=<?=$codsuc ?>&fdesde="+$("#fdesde").val()+"&fhasta="+$("#fhasta").val()+"&ciclotext="+$("#ciclo option:selected").text();
        AbrirPopupImpresion(url, 800, 600)

        return false
    }
    function Cancelar()
    {
        location.href = '<?=$urldir ?>/admin/indexB.php'
    }
    function quitar_disabled(obj, input)
    {
        if (obj.checked)
        {
            $("#"+input).attr("disabled", true)
        } else {
            $("#"+input).attr("disabled", false)
        }
    }
    function cargar_anio_drop(obj)
    {
        $.ajax({
            url: '../../../../ajax/anio_drop.php',
            type: 'POST',
            async: true,
            data: 'codsuc=<?=$codsuc ?>&codciclo='+obj+'&condicion=1',
            success: function (datos) {
                $("#div_anio").html(datos)
            }
        })
    }
    function cargar_mes(ciclo, suc, anio)
    {
        $.ajax({
            url: '../../../../ajax/mes_drop.php',
            type: 'POST',
            async: true,
            data: 'codciclo='+ciclo+'&codsuc='+suc+'&anio='+anio,
            success: function (datos) {
                $("#div_meses").html(datos)
            }
        })
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="70%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="2">
                        <fieldset style="padding:4px">
                            <table width="70%" border="0" cellspacing="0" cellpadding="0" style="margin: 0px auto;">
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" width="12%">Sucursal</td>
                                    <td width="2%" align="center">:</td>
                                    <td colspan="3">
                                        <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" >Ciclo</td>
                                    <td align="center">:</td>
                                    <td width="35%">
                                        <?php $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio_drop(this.value);'"); ?>
                                    </td>
                                    <td width="3%" align="center">&nbsp;</td>
                                    <td width="49%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" >Fecha</td>
                                    <td align="center">:</td>
                                    <td>
                                        <input type="text" name="fdesde" id="fdesde" value="<?=date('d/m/Y') ?>" class="inputtext" style="width:80px;" />
                                        - <input type="text" name="fhasta" id="fhasta" value="<?=date('d/m/Y') ?>" class="inputtext" style="width:80px;" />
                                    </td>
                                    <td align="right" >&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <!--
                                    <td align="center" align="center" width="3%">
                                        <input type="hidden" name="todfechas" id="todfechas" value="1" />
                                        <input type="checkbox"  id="ChckFechaReg" checked="checked" />
                                    </td>
                                    <td>Todas las Fechas</td>
                                    -->
                                </tr>
                                <!--
                                <tr>
                                    <td align="right" >Ciclo</td>
                                    <td align="center">:</td>
                                    <td width="35%">
                                        <?php $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio_drop(this.value);'"); ?>
                                    </td>
                                    <td width="3%" align="center">&nbsp;</td>
                                    <td width="49%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" >A&ntilde;o</td>
                                    <td align="center">:</td>
                                    <td>
                                        <div id="div_anio">
                                            <?php $objMantenimiento->drop_anio($codsuc,0); ?>
                                        </div>
                                    </td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" >Meses</td>
                                    <td align="center">:</td>
                                    <td>
                                        <div id="div_meses">
                                            <?php $objMantenimiento->drop_mes($codsuc,0,0); ?>
                                        </div>
                                    </td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                -->
                                <tr>
                                    <td align="right" >Sector</td>
                                    <td align="center">:</td>
                                    <td align="center" width="36%">
                                        <?php echo $objMantenimiento->drop_sectores2($codsuc, 0, ""); ?>
                                    </td>
                                    <td align="center" align="center" width="3%">
                                        <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="quitar_disabled(this, 'codsector'); CambiarEstado(this, 'todossectores')" />
                                    </td>
                                    <td>Todos los Sectores </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td><input type="hidden" name="todossectores" id="todossectores" value="1" /></td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center"><input type="button" id="btnVerificar"  onclick="return ProcesarNoSol();" value="Procesar No Solicitado" ></td>
                                    <td colspan="2" align="center"><input type="button" onclick="return ValidarForm();" value="Generar" id=""></td>
                                    
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="4" align="left"><div id="div_valida"></div></td>
                                </tr>

                            </table>
                        </fieldset>
                    </td>
                </tr>
            </tbody>

        </table>
    </form>
</div>
<script>
    $("#codsector").attr("disabled", true)
</script>
<?php CuerpoInferior(); ?>