<?php

include("../../../../objetos/clsReporte.php");
$objMantenimiento = new clsFunciones();

class clsRegistroVentas extends clsReporte {

    function cabecera() {
        global $m, $fdesde, $ciclotext;

        $this->SetFont('Arial', 'B', 14);

        $tit1 = "REGISTRO DE REAPERTURADOS";
        $this->Cell(0, $h+2, utf8_decode($tit1), 0, 1, 'C');

        $this->Ln(5);

        $h = 4;
        $this->SetFont('Arial', '', 7);
        $this->Cell(170, $h, $ciclotext, 0, 0, 'C');

        $this->SetX(113.5);
        $this->Cell(10, $h, "MES", 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(30, $h, $m, 0, 0, 'L');
        $this->Cell(10, $h, utf8_decode("AÑO"), 0, 0, 'R');
        $this->Cell(5, $h, ":", 0, 0, 'C');
        $this->Cell(20, $h, $fdesde, 0, 1, 'L');

        $this->Ln(2);

    }

    function agregar_detalle($codsuc, $codciclo, $fdesde, $fhasta, $codsector)
    {
        global $conexion, $objMantenimiento;

        $h = 4;

        $count = 0;

        $impmes = 0;
        $impigv = 0;
        $imptotal = 0;

        $this->SetTextColor(0, 0, 0);
        $cont_1 = 0;
        $AndSect='';
        
        //echo $codsector;
        if($codsector=='%')
        {
            $AndSect= $AndSect; 
        }
        else {
            $AndSect= " AND cie.codsector = '".$codsector."' ";
        }
        
        $DataGeneral = array();

        $VerSector="SELECT DISTINCT se.codsector, se.descripcion
            FROM catastro.clientes AS clie
            INNER JOIN medicion.cierreyapertura AS cie ON clie.nroinscripcion = cie.nroinscripcion AND clie.codemp = cie.codemp AND clie.codsuc = cie.codsuc AND clie.codsector = cie.codsector
            INNER JOIN public.sectores AS se ON se.codemp = clie.codemp AND se.codsuc = clie.codsuc AND se.codsector = clie.codsector AND se.codzona = clie.codzona
            WHERE cie.codemp = 1 AND cie.codciclo = 1 AND cie.tipooperacion=0 AND cie.codsuc = 1 AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ".$AndSect." ";
                    
        $VerSector.=" ORDER BY se.codsector ASC "; 
        $consultaS = $conexion->query($VerSector);
        $itemsS = $consultaS->fetchAll();
        
        foreach ($itemsS as $value) {
            if (!array_key_exists($value['codsector'], $DataGeneral )) {
                $DataGeneral[$value['codsector']]=array();
                $DataGeneral[$value['codsector']]['nombre']= $value['descripcion'];
            }


            $VerRuta="SELECT
                DISTINCT ru.codrutlecturas,
                ru.descripcion
                FROM
                catastro.clientes AS clie
                INNER JOIN medicion.cierreyapertura AS cie ON clie.nroinscripcion = cie.nroinscripcion AND clie.codemp = cie.codemp AND clie.codsuc = cie.codsuc AND clie.codsector = cie.codsector
                INNER JOIN public.rutasmaelecturas AS ru ON ru.codemp = clie.codemp AND ru.codsuc = clie.codsuc AND ru.codzona = clie.codzona AND ru.codsector = clie.codsector AND ru.codrutlecturas = clie.codrutlecturas
                WHERE
                cie.codemp = 1 AND cie.codsuc = 1 AND cie.codciclo = 1 AND ru.estareg=1 AND cie.tipooperacion=0
                AND clie.codsector = ".$value['codsector']."         
                AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ";        
                
            $VerRuta.=" ORDER BY ru.codrutlecturas ASC ";
            $consultaR = $conexion->query($VerRuta);
            $itemsR    = $consultaR->fetchAll();

            foreach ($itemsR as $valueRu) {

                if (!array_key_exists($valueRu['codrutlecturas'], $DataGeneral[$value['codsector']] )) {
                    $DataGeneral[$value['codsector']][$valueRu['codrutlecturas']]=array();
                    $DataGeneral[$value['codsector']][$valueRu['codrutlecturas']]['nombre']= $valueRu['descripcion'];
                }

                $sqlD = "SELECT
                    clie.nroinscripcion, clie.codantiguo,
                    to_char(clie.codsuc, '00') || '-' || TRIM(to_char(clie.codsector, '00')) || '-' || TRIM(to_char(CAST(clie.codmanzanas AS INTEGER), '000')) || '-' || TRIM(to_char(CAST(clie.lote AS INTEGER), '0000')) || '-' || TRIM(clie.sublote) AS codcatastro,
                    clie.propietario,
                    cie.fecha,
                    tp.descripcioncorta || ' ' || ca.descripcion || ' ' || clie.nrocalle AS direccion,
                    clie.codtiposervicio
                    FROM
                    medicion.cierreyapertura AS cie
                    INNER JOIN catastro.clientes AS clie ON clie.nroinscripcion = cie.nroinscripcion AND clie.codemp = cie.codemp AND clie.codsuc = cie.codsuc AND clie.codsector = cie.codsector
                    INNER JOIN public.calles AS ca ON (clie.codemp = ca.codemp AND clie.codsuc = ca.codsuc AND clie.codcalle = ca.codcalle AND clie.codzona = ca.codzona)
                    INNER JOIN public.tiposcalle AS tp ON (ca.codtipocalle = tp.codtipocalle) ";
                $sqlD .= " WHERE cie.tipooperacion = 0 AND cie.codciclo=".$codciclo." AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ";
                if ($codsector == '%')
                { $sqlD .= " AND clie.codsector=".$value['codsector']." AND clie.codrutlecturas=".$valueRu['codrutlecturas']; }
                else { $sqlD .= " AND clie.codsector='".$value['codsector']."' AND clie.codrutlecturas=".$valueRu['codrutlecturas']; }
               
                $sqlD .= " ORDER BY codcatastro, clie.codrutlecturas ASC ";

                $consultaD = $conexion->query($sqlD);
                $itemsD = $consultaD->fetchAll();

                
                foreach ($itemsD as $User) {

                    $VerPago="SELECT
                        COUNT(DISTINCT ca.nroinscripcion)
                        FROM
                        cobranza.cabpagos AS ca
                        INNER JOIN cobranza.detpagos AS de ON de.codemp = ca.codemp AND de.codsuc = ca.codsuc AND de.nroinscripcion = ca.nroinscripcion AND de.nropago = ca.nropago
                        WHERE
                        ca.nroinscripcion=".$User['nroinscripcion']." 
                        AND ca.fechareg BETWEEN '".$fdesde."' AND '".$fhasta."' ";
                    $conD = $conexion->query($VerPago);
                    $itD = $conD->fetch();
                    $VerCant=$itD[0];

                    if ($VerCant!=0) 
                    {
                        
                        $VerReap="SELECT
                            cie.nroinscripcion,
                            cie.tipooperacion,
                            cie.fecha,
                            cie.tiposervicio
                            FROM
                            medicion.cierreyapertura AS cie
                            WHERE
                            cie.nroinscripcion=".$User['nroinscripcion']." 
                            AND cie.tipooperacion=1
                            AND cie.fecha BETWEEN '".$fdesde."' AND '".$fhasta."' ";
                        $CondRea = $conexion->query($VerReap);
                        $ItemRea = $CondRea->fetch();
                        
                        //$ItemRea=0;
                        if($ItemRea==0)
                        {
                            if ($User["codtiposervicio"] == 1) {
                                $User['tiposervicio'] = "A - No Sol.";
                            }
                            if ($User["codtiposervicio"] == 2) {
                                $User['tiposervicio'] = "A - No Sol.";
                            }
                            if ($User["codtiposervicio"] == 3) {
                                $User['tiposervicio'] = "D - No Sol.";
                            }

                            $DataGeneral[$value['codsector']][$valueRu['codrutlecturas']][]=array(

                                'nroinscripcion'=>$User['nroinscripcion'],
                                'codantiguo'=>$User['codantiguo'],
                                'codcatastro'=>$User['codcatastro'],
                                'propietario'=>$User['propietario'],
                                'fecha'=>$User['fecha'],
                                'direccion'=>$User['direccion'],
                                'tiposervicio'=>$User['tiposervicio']

                            );

                        }
                        else
                        {                            
                            
                            
                            $DataGeneral[$value['codsector']][$valueRu['codrutlecturas']][]=array(

                                'nroinscripcion'=>$User['nroinscripcion'],
                                'codantiguo'=>$User['codantiguo'],
                                'codcatastro'=>$User['codcatastro'],
                                'propietario'=>$User['propietario'],
                                'fecha'=>$User['fecha'],
                                'direccion'=>$User['direccion'],
                                'tiposervicio'=>$ItemRea['tiposervicio']
                            );
                        }

                    }

                    
                }

            }

        }

        $count=0;
        foreach ($DataGeneral as $indice_sector => $datosector) {            
            
            $Sector =$datosector['nombre'];
            unset($datosector['nombre']);

            if (count($datosector)==1) {
                break;
            }

            $this->SetFont('Arial', 'B', 9);
            $this->Cell(80, 6, $Sector , 0, 1, 'L');
            $this->Ln(1);

            foreach ($datosector as $indice_ruta => $datosrutas) {

                $Ruta =$datosrutas['nombre'];
                unset($datosrutas['nombre']);

                if (count($datosrutas) == 0 ) {
                    break;
                }

                $this->SetFont('Arial', 'B', 9);
                $this->Cell(196, 5, $Sector."    ===> ".$Ruta , 1, 1, 'L');

                $this->SetFont('Arial', 'B', 6);

                $this->SetWidths(array(8, 21, 16, 58, 50, 13, 16, 14));
                $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C"));
                $this->Row(array("ITEM",
                    "COD. CATASTRAL",
                    "INSCRIPCION",
                    "PROPIETARIO",
                    "DIRECCION",
                    "FECHA",
                    //"MOTIVO",
                    "SERV",
                    "HORA"
                    //"PERSONAL"
                ));

                
                foreach ($datosrutas as $datosusuarios) {
                    
                    $count++;
                    $this->SetFont('Arial', '', 6);
                    $this->SetWidths(array(8, 21, 16, 58, 50, 13, 16, 14));
                    $this->SetAligns(array("C", "C", "C", "L", "L", "C", "C", "C"));
                    $this->Row(array(
                        $count,
                        $datosusuarios['codcatastro'],
                        $datosusuarios['codantiguo'],
                        utf8_decode(strtoupper($datosusuarios["propietario"])),
                        utf8_decode(strtoupper($datosusuarios["direccion"])),
                        $this->DecFecha($datosusuarios["fecha"]),                            
                        $datosusuarios['tiposervicio'],
                        '' 
                    ));
                }
            }
        }

        
        // echo "<pre>";
        // var_dump($DataGeneral);
        // echo "</pre>";exit;
        
    }

}

$ciclo = $_GET["ciclo"];
$sector = $_GET["sector"];
$codsuc = $_GET["codsuc"];
$fdesde = $_GET["fdesde"];
$fhasta  = $_GET["fhasta"];
$ciclotext = $_GET["ciclotext"];

$m = $fhastaes[$fhasta];


$objReporte = new clsRegistroVentas();
$objReporte->AliasNbPages();
$objReporte->AddPage("");

$objReporte->agregar_detalle($codsuc, $ciclo, $fdesde, $fhasta, $sector );

$objReporte->Output();

?>