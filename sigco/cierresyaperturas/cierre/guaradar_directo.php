<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}


include('../../../objetos/clsFunciones.php');
//ini_set("display_errors",1);
$clsFunciones = new clsFunciones();

$IdUserReg= $_SESSION['id_user'];
$Fechareg= date('Y-m-d');
$HoraReg= date('H:i:s');
$contador = $_POST["cont"];
$codsuc = $_SESSION['IdSucursal'];
$codciclo = $_POST["codciclo"];
$anio = $_POST["anio"];
$mes = $_POST["mes"];
$nroinscripcion = $_POST["nroinscripcion"];
$nromed = $_POST["nromed"];
$codusu = $_POST["codusu"];
$Op = $_POST["Op"];
$codtiposervicio = $_POST["codtiposervicio"];
$DireccionPost = $_POST["direccion"];
$Destpservicio = $_POST["Destpservicio"];
$codemp = 1;
$codtipoocurrencia = 1;
$codcliente = 0;

$CodMotivoCorte=$_POST["codmotivocorte"];
$conexion->beginTransaction();

$CodConceptoA = "402";
$CodConceptoD = '502'; 
    
    //SICUANI NO CARGARÁ DIRECTO LOS CARGOS
    
    $VerNroFact="SELECT nrofacturacion, anio, mes FROM facturacion.periodofacturacion WHERE facturacion=0";
    $RowFact= $conexion->query($VerNroFact)->fetch();
    
    $NroFact= $RowFact['nrofacturacion'];
    $AnioF= $RowFact['anio'];
    $MesF = $RowFact['mes'];
    
    if($Destpservicio=='A')
    {
        $CodConcepto= $CodConceptoA;//'806';
        $VerConcepto="SELECT codconcepto, descripcion, importe FROM facturacion.conceptos WHERE codconcepto=".$CodConcepto;
        $rwag= $conexion->query($VerConcepto)->fetch();
        $Concepto= $rwag['descripcion'];
        $Imp= $rwag['importe'];
        $Igv= number_format((0.18* $Imp), 2);
        $Monto=$Imp + $Igv + 0.02;
        
    }
    
    if($Destpservicio=='A/D')
    {
        //$CodConceptoA='806';
        $VerConceptoA="SELECT codconcepto, descripcion, importe FROM facturacion.conceptos WHERE codconcepto=".$CodConceptoA;
        $rwagA= $conexion->query($VerConceptoA)->fetch();
        
        $ConceptoA= $rwaA['descripcion'];
        $Imp= $rwagA['importe'];
        $IgvA= number_format((0.18* $Imp), 2);
        $MontoA= $Imp + $IgvA + 0.02;
        
        
        //$CodConceptoD='10016';
        $VerConceptoD="SELECT codconcepto, descripcion, importe FROM facturacion.conceptos WHERE codconcepto=".$CodConceptoD;
        $rwagD= $conexion->query($VerConceptoD)->fetch();        
        
        $ConceptoD= $rwagD['descripcion'];
        $Imp= $rwagD['importe'];        
        $IgvD= number_format((0.18* $Imp), 2);
        $MontoD= $Imp + $IgvD + 0.02;
        
        $Igv= $IgvD+ $IgvA;
    }
    
    if($Destpservicio=='D')
    {
        $CodConcepto=$CodConceptoD;//'806';
        $VerConcepto="SELECT codconcepto, descripcion, importe FROM facturacion.conceptos WHERE codconcepto=".$CodConcepto;
        $rwag= $conexion->query($VerConcepto)->fetch();        
        
        $Concepto= $rwag['descripcion'];
        $Imp= $rwag['importe'];
        $Igv  = number_format((0.18* $Imp), 2);
        $Monto= $Imp + $Igv + 0.02;
        
    }
    //
    
if ($Op == 1) {
    
    //SICUANI CARGARÁ DIRECTO LOS CARGOS
    
    $VerCliente="SELECT clie.nroinscripcion, clie.propietario,
        tca.descripcioncorta||' '||ca.descripcion||' '||clie.nrocalle,
        clie.codantiguo,
        clie.codtiposervicio
        FROM
        catastro.clientes AS clie
        INNER JOIN public.calles AS ca ON ca.codemp = clie.codemp AND ca.codsuc = clie.codsuc AND ca.codzona = clie.codzona AND ca.codcalle = clie.codcalle
        INNER JOIN public.tiposcalle AS tca ON tca.codtipocalle = ca.codtipocalle
        WHERE clie.nroinscripcion =".$nroinscripcion;
    
    $RowCli= $conexion->query($VerCliente)->fetch();
    $Direccion  = $RowCli[2];
    $Propietario= $RowCli[1];
    $CodAntiguo = $RowCli[3];
    
    $Sql="SELECT max(nrocredito) FROM facturacion.cabvarios WHERE codsuc=".$codsuc;
    //$rowc= $conexion->query($Sql)->fetch();
    $result = $conexion->prepare($Sql);
    $result->execute(array());
    $items = $result->fetch();
    
    $nrocabvarios = $items[0]+1;
    //
    
    //$nrocabvarios = 0;
    /****************************/
    
    $sqlD = "DELETE FROM medicion.cierreyapertura WHERE codemp=1 and codsuc=:codsuc and codciclo=:codciclo and
	anio=:anio and mes=:mes and tipooperacion=0 AND nroinscripcion=:nroinscripcion";

    $result = $conexion->prepare($sqlD);
    $result->execute(array(":codsuc" => $codsuc, ":codciclo" => $codciclo, ":anio" => $anio, ":mes" => $mes, ":nroinscripcion" => $nroinscripcion));

    $sqlI = "INSERT INTO medicion.cierreyapertura(codemp,codsuc,codciclo,nroinscripcion,anio,mes,tipooperacion,
        codtipocorte,fecha,codinspector,codmotivocorte,tipopersonal,observacion,codsector,codusu,
        nromed, nrocredito, tiposervicio)
        VALUES(:codemp,:codsuc,:codciclo,:nroinscripcion,:anio,:mes,:tipooperacion,:codtipocorte,:fecha,:codinspector,
        :codmotivocorte,:tipopersonal,:observacion,:codsector,:codusu,:nromed, :nrocredito, :tiposervicio)";
    $result = $conexion->prepare($sqlI);
    $result->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":codciclo" => $codciclo,
        ":nroinscripcion" => $nroinscripcion,
        ":anio" => $anio,
        ":mes" => $mes,
        ":tipooperacion" => 0,
        ":codtipocorte" => $_POST["codtipocorte"],
        ":fecha" => $_POST["fechacorte"],
        ":codinspector" => $_POST["codinspector"],
        ":codmotivocorte" => $_POST["codmotivocorte"],
        ":tipopersonal" => $_POST["codtipopersonal"],
        ":observacion" => isset($_POST["observacion"]) ? $_POST["observacion"] : '',
        ":codsector" => $_POST['codsector'],
        ":codusu" => $codusu,
        ":nromed" => $nromed,
        ":nrocredito" => $nrocabvarios,
        ":tiposervicio" => $Destpservicio
    ));
    
    /* CAMBIAMOS A ESTADO DE SERVICIO CORTADO */
    $sqlU = "UPDATE catastro.clientes set codestadoservicio=2 WHERE codsuc=? and nroinscripcion=?";
    $result = $conexion->prepare($sqlU);
    $result->execute(array($codsuc, $nroinscripcion));
    

    
    //INSERTAR OCURRENCIA
    $observacion = "CIERRE DE SERVICIO ";
    $clsFunciones->InsertOcurrencia($codemp,$codsuc,$codtipoocurrencia,$codcliente,$nroinscripcion,'','',$_POST["fechacorte"],$observacion,$codusu,1);


    
    /* CORTE A SOLICITUD */
    if($CodMotivoCorte==1)
    {
        $sqlCl = "UPDATE catastro.clientes set codestadoservicio=8 WHERE codsuc=? and nroinscripcion=?";
        $result = $conexion->prepare($sqlCl);
        $result->execute(array($codsuc, $nroinscripcion)); 
    }
    


    /* INGRESO DE CARGOS AUTOMATICAMENTE */ 
    
    //SICUANI CARGARÁ DIRECTO LOS CARGOS
    
    if($Destpservicio=='A/D')
    {
        //INSERTAMOS SU CARGO -- AGUA
            $InsertCargoCabV="INSERT INTO facturacion.cabvarios(
                    codemp, codsuc, nrocredito, nroinscripcion, imptotal, igv, redondeo, 
                    subtotal, codconcepto, nropresupuesto, observacion, estareg, 
                    creador, fechareg, fechaanulacion, horaanulacion, creadoranulacion, 
                    nropago, nroprepago, nroinspeccion, nroprepagoinicial, 
                    sininteres, direccion, propietario, codantiguo, hora, nrocolaterales, 
                    tipo)
                VALUES ( $codemp, $codsuc, '$nrocabvarios', $nroinscripcion, '$MontoA', '$Igv', '0.02', 
                    '$Imp', '$CodConceptoA', 0, '$ConceptoA', 1, 
                    $IdUserReg, '$Fechareg', '$Fechareg', '$HoraReg', 0, 
                    0, 0, 0, 0, 
                    0, '$Direccion', '$Propietario', '$CodAntiguo', '$HoraReg', 1, 
                    0)";
            $result = $conexion->prepare($InsertCargoCabV);
            $result->execute(array());
            
            $MontoA= $MontoA + 0.5;
            $InsertCargoDetV="INSERT INTO facturacion.detvarios(
                codemp, codsuc, nrocredito, nrocuota, totalcuotas, fechavencimiento, 
                subtotal, igv, redondeo, imptotal, estadocuota, tipocuota, nrofacturacion, 
                anio, mes, interes)
                VALUES ( $codemp, $codsuc, '$nrocabvarios', 1, 1, '$Fechareg', 
                '$Imp', '$Igv', '0.02', '$MontoA', 0, 0, '$NroFact', 
                '$Anio', '$Mes', '0.05') ";
            $result = $conexion->prepare($InsertCargoDetV);
            $result->execute(array());
            
            //806
            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES (1, $codsuc, $nrocabvarios, '$CodConceptoA', 1, '$Imp', '0.00', '0.00', '$Imp') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());
            
            //5 IGV
            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES (1, $codsuc, $nrocabvarios, '5', 2, '$Igv', '0.00', '0.00', '$Igv') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());
            
            //8 REDONDEO
            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES (1, $codsuc, $nrocabvarios, '8', 3, '0.02', '0.00', '0.00', '0.02') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());

            // FIN DE INGRESOS DE CARGOS -- AGUA

            $Sql="SELECT max(nrocredito) FROM facturacion.cabvarios WHERE codsuc=".$codsuc;
            //$rowc= $conexion->query($Sql)->fetch();
            $result = $conexion->prepare($Sql);
            $result->execute(array());
            $items = $result->fetch();
            
            $nrocabvariosD = $items[0]+1;

        //INSERTAMOS SU CARGO -- DESAGUE

            $InsertCargoCabV="INSERT INTO facturacion.cabvarios(
                    codemp, codsuc, nrocredito, nroinscripcion, imptotal, igv, redondeo, 
                    subtotal, codconcepto, nropresupuesto, observacion, estareg, 
                    creador, fechareg, fechaanulacion, horaanulacion, creadoranulacion, 
                    nropago, nroprepago, nroinspeccion, nroprepagoinicial, 
                    sininteres, direccion, propietario, codantiguo, hora, nrocolaterales, 
                    tipo)
                VALUES ( $codemp, $codsuc, '$nrocabvariosD', $nroinscripcion, '$MontoD', '$Igv', '0.02', 
                    '$Imp', '$CodConceptoD', 0, '$ConceptoD', 1, 
                    $IdUserReg, '$Fechareg', '$Fechareg', '$HoraReg', 0, 
                    0, 0, 0, 0, 
                    0, '$Direccion', '$Propietario', '$CodAntiguo', '$HoraReg', 1, 
                    0)";
            $result = $conexion->prepare($InsertCargoCabV);
            $result->execute(array());
            
            $MontoD= $MontoD + 0.5;
            $InsertCargoDetV="INSERT INTO facturacion.detvarios(
                codemp, codsuc, nrocredito, nrocuota, totalcuotas, fechavencimiento, 
                subtotal, igv, redondeo, imptotal, estadocuota, tipocuota, nrofacturacion, 
                anio, mes, interes)
                VALUES ( $codemp, $codsuc, '$nrocabvariosD', 1, 1, '$Fechareg', 
                '$Imp', '$Igv', '0.02', '$MontoD', 0, 0, '$NroFact', 
                '$Anio', '$Mes', '0.50') ";
            $result = $conexion->prepare($InsertCargoDetV);
            $result->execute(array());
            
            //806
            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES ( $codemp, $codsuc, $nrocabvariosD, '$CodConceptoD', 1, '$Imp', '0.00', '0.00', '$Imp') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());
            
            //5 IGV
            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES (1, $codsuc, $nrocabvarios, '5', 2, '$Igv', '0.00', '0.00', '$Igv') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());
            
            //8 REDONDEO
            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES (1, $codsuc, $nrocabvarios, '8', 3, '0.02', '0.00', '0.00', '0.02') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());
            
        // FIN DE INGRESOS DE CARGOS -- DESAGUE


    }
    else
        {
            //INSERTAMOS SU CARGO
            $InsertCargoCabV="INSERT INTO facturacion.cabvarios(
                    codemp, codsuc, nrocredito, nroinscripcion, imptotal, igv, redondeo, 
                    subtotal, codconcepto, nropresupuesto, observacion, estareg, 
                    creador, fechareg, fechaanulacion, horaanulacion, creadoranulacion, 
                    nropago, nroprepago, nroinspeccion, nroprepagoinicial, 
                    sininteres, direccion, propietario, codantiguo, hora, nrocolaterales, 
                    tipo)
                VALUES ( $codemp, $codsuc, '$nrocabvarios', $nroinscripcion, '$Monto', '$Igv', '0.02', 
                    '$Imp', '$CodConcepto', 0, '$Concepto', 1, 
                    $IdUserReg, '$Fechareg', '$Fechareg', '$HoraReg', 0, 
                    0, 0, 0, 0, 
                    0, '$Direccion', '$Propietario', '$CodAntiguo', '$HoraReg', 1, 
                    0)";
            $result = $conexion->prepare($InsertCargoCabV);
            $result->execute(array());
            
            $Monto= $Monto+ 0.5;
            $InsertCargoDetV="INSERT INTO facturacion.detvarios(
                codemp, codsuc, nrocredito, nrocuota, totalcuotas, fechavencimiento, 
                subtotal, igv, redondeo, imptotal, estadocuota, tipocuota, nrofacturacion, 
                anio, mes, interes)
                VALUES ( $codemp, $codsuc, '$nrocabvarios', 1, 1, '$Fechareg', 
                '$Imp', '$Igv', '0.02', '$Monto', 0, 0, '$NroFact', 
                '$Anio', '$Mes', '0.50') ";
            $result = $conexion->prepare($InsertCargoDetV);
            $result->execute(array());

            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES ( $codemp, $codsuc, $nrocabvarios, '$CodConcepto', 1, '$Imp', '0.00', '0.00', '$Imp') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());
            
            //5 IGV
            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES (1, $codsuc, $nrocabvarios, '5', 2, '$Igv', '0.00', '0.00', '$Igv') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());
            
            //8 REDONDEO
            $InsertCargoDetCabV="INSERT INTO facturacion.detcabvarios(
            codemp, codsuc, nrocredito, codconcepto, item, subtotal, igv, redondeo, imptotal)
            VALUES (1, $codsuc, $nrocabvarios, '8', 3, '0.02', '0.00', '0.00', '0.02') ";
            $result = $conexion->prepare($InsertCargoDetCabV);
            $result->execute(array());
            
            // FIN DE INGRESOS DE CARGOS
        }
    //
}


if ($Op == 2) {
    
    $Sql = "UPDATE medicion.cierreyapertura  SET   observacion = :observacion
        WHERE codemp = :codemp AND  codsuc = :codsuc AND  codciclo = :codciclo AND nroinscripcion = :nroinscripcion
        AND  anio = :anio AND  mes = :mes AND  tipooperacion = :tipooperacion;";

    $result = $conexion->prepare($Sql);
    $result->execute(array(":codemp" => 1,
        ":codsuc" => $codsuc,
        ":codciclo" => $codciclo,
        ":nroinscripcion" => $nroinscripcion,
        ":anio" => $anio,
        ":mes" => $mes,
        ":tipooperacion" => 0,
        ":observacion" => isset($_POST["Obs"]) ? $_POST["Obs"] : ''));
}
if ($Op == 3) {
    
    //ANULACION DE CIERRE DE SERVICIO
    $sqlD = "DELETE FROM medicion.cierreyapertura WHERE codemp=1 and codsuc=:codsuc and codciclo=:codciclo and
	anio=:anio and mes=:mes and tipooperacion=0 AND nroinscripcion=:nroinscripcion";

    $result = $conexion->prepare($sqlD);
    $result->execute(array(":codsuc" => $codsuc, ":codciclo" => $codciclo, ":anio" => $anio, ":mes" => $mes, ":nroinscripcion" => $nroinscripcion));
    /*
    $sqlU = "UPDATE catastro.clientes set codestadoservicio=1 WHERE codsuc=? and nroinscripcion=?";
    $result = $conexion->prepare($sqlU);
    $result->execute(array($codsuc, $nroinscripcion));
    
    
    $DelDetVarios="DELETE FROM facturacion.detvarios
        WHERE codsuc=1 AND nrofacturacion=$NroFact AND nroinscripcion=$nroinscripcion ";
    $result = $conexion->prepare($DelDetVarios);
    $result->execute(array());
    */
    
}
//$conexion->rollBack();
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    //print_r($result->errorInfo());
    echo $res = 2;
} else {
    $conexion->commit();
    $mensaje = "El Registro se ha Grabado Correctamente";
    echo 1;
}

?>

