$(function () {
    $("#fcorte").datepicker();
    var theTable = $('#tbcierre')
    $(document).ready(function () {

        $('form').keypress(function (e) {
            if (e == 13) {
                return false;
            }
        });

        $('input').keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
        });

    });

    $("#Filtro").keyup(function (e) {
        if (VeriEnter(e))
        {
            val= $("#Filtro").val();
            //alert(val);
            $.ajax({
                url: 'Consulta.php',
                type: 'POST',
                async: true,
                data: 'Filtro='+val,
                success: function(datos) {                
                    $("#tbcierre tbody").html(datos);
                }
            })
            $.uiTableFilter(theTable, this.value)

        }

    })
    
    
    $("#DivHistorial").dialog({
        autoOpen: false,
        height: 350,
        width: 700,
        modal: true,
        resizable: false,
        buttons: {
            "Cerrar": function () {
                $(this).dialog("close");
            }
        },
        close: function () {

        }
    });
    
    $("#dialog-form-observcion").dialog({
        autoOpen: false,
        height: 250,
        width: 500,
        modal: true,
        resizable: false,
        buttons: {
            "Aceptar": function () {
                ObsOk()

                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        },
        close: function () {

        }
    });

});


function Muestra_Consulta(datos)
{
    alert(datos);
    $('#ImgLoad').fadeOut(500, function () {
        $("#DivConsulta").empty().append(datos);
        $('#DivConsulta').fadeIn(500, function () {
            var theTable = $('#TbIndex')
            $("#Filtro").keyup(function () {

                $.uiTableFilter(theTable, this.value)

            })
            //$('#DivConsulta #TbIndex').fixedHeaderTable({ footer: true, cloneHeadToFoot: true, fixedColumn: true });
        })
    })

}
    
function datos_facturacion(codciclo)
{
    $.ajax({
        url: '../../../ajax/periodo_facturacion_facturado.php',
        type: 'POST',
        async: true,
        data: 'codciclo='+codciclo+'&codsuc='+codsuc,
        success: function (datos) {
            var r = datos.split("|")

            $("#anio").val(r[1])
            $("#mes").val(r[3])
        }
    })
}
function quitar_disabled(obj, input)
{
    if (obj.checked)
    {
        $("#"+input).attr("disabled", true)
    } else {
        $("#"+input).attr("disabled", false)
    }
}

function abrir_popu_pagos(nroinscripcion)
{
    historial_pagos(nroinscripcion)
    $("#DivHistorial").dialog("open");
}
function historial_pagos(nroinscripcion)
{
    $.ajax({
        url: 'historial_pagos.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion,
        success: function (datos) {
            $("#div_historial").html(datos)
        }
    })
}
function agregar_corte(index, NroInscripcion, CodCatastral, Propietario, Direccion, Sector, NroMedidor, codsector, Codantiguo, TpServico)
{
    //alert();
    var codmotivocorte = $("#motivocorte").val()
    var motivocorte = $("#motivocorte option:selected").text();

    var codtipocorte = $("#tipocorte").val()
    var tipocorte = $("#tipocorte option:selected").text();

    var codinspector = $("#inspectores").val()
    var inspector = $("#inspectores option:selected").text();

    var codtipopersonal = $("#tipopersonal").val()
    var tipopersonal = $("#tipopersonal option:selected").text();
    var fechacorte = $("#fcorte").val()

    var Destpservicio = $("#tiposervicio-"+NroInscripcion).val();
    
    if (codinspector == 0)
    {
        Msj('#inspectores', 'Seleccione el Inspector', 1000, 'right', '', false)
        return false
    }
    if (codmotivocorte == 0)
    {
        Msj('#motivocorte', 'Seleccione el Motivo de Corte', 1000, 'right', '', false)
        return false
    }
    if (codtipocorte == 0)
    {
        Msj('#tipocorte', 'Seleccione el Tipo de Corte', 1000, 'right', '', false)
        return false
    }
    
    var j = parseInt($("#tbcierreyapertura tbody tr").length)
                
    for(var i1=1; i1<=j; i1++)
    {   
        var Codigo = $("#tbcierreyapertura tbody tr#corte"+i1+" label.NroInscripcion").text();
        
        if (Codigo==NroInscripcion)
        {
            Msj('#tipopersonal', 'Usuario ya fue agregado en este periodo', 30000, 'right', '', false)
            return false;
        }
    }
    
    if (NroInscripcion == undefined)
    {
        var NroInscripcion = $("#tbcierre tbody tr#"+index+" label.NroInscripcion").text()
        var CodCatastral = $("#tbcierre tbody tr#"+index+" label.CodCatastral").text()
        var Propietario = $("#tbcierre tbody tr#"+index+" label.Propietario").text()
        var Sector = $("#tbcierre tbody tr#"+index+" label.Sector").text()
        var NroMedidor = $("#tbcierre tbody tr#"+index+" label.NroMedidor").text()
        var Direccion = $("#tbcierre tbody tr#"+index+" label.Direccion").text()
        var codsector = $("#tbcierre tbody tr#"+index+" label.CodSector").text()
        var Codantiguo = $("#tbcierre tbody tr#"+index+" label.Codantiguo").text()
    }
    
    var i = j+1;
    var tr = ' <tr id="corte'+i+'" onclick="SeleccionaId(this);">';
        tr += '<td align="center">';
        tr += '<label class="TipoPersonal">'+codtipopersonal+'</label>';
        tr += '<label class="CodSector">'+codsector+'</label>';
        tr += '<label class="MotivoCorte">'+codmotivocorte+'</label>';
        tr += '<label class="CodInspector">'+codinspector+'</label>';
        tr += '<label class="TipoCorte">'+codtipocorte+'</label>';
        tr += '<label class="CodCatastral">'+CodCatastral+'</label>';
        tr += '<label class="Sectorn">'+Sector+'</label>';
        tr += '<label class="NroMedidorn">'+NroMedidor+'</label>';
        tr += '<label class="Direccionn">'+Direccion+'</label>';
        tr += '<label class="NroInscripcion">'+NroInscripcion+'</label></td>';
        tr += '<td>';
        tr += '<label class="Codantiguo">'+Codantiguo+'</label></td>';
        tr += '<td>';
        tr += '<label class="Propietario">'+Propietario+'</label></td>';
        tr += '<td align="center">';
        tr += '<label class="FechaCorte">'+fechacorte+'</label></td>';
        tr += '<td align="center">';
        tr += '<label class="Motivo">'+motivocorte+'</label></td>';
        tr += '<td align="center">';
        tr += '<label class="Tipo">'+tipocorte+'</label></td>';
        tr += '<td><label class="Inspector">'+inspector+'</label></td>';
        tr += '<td><label class="Personal">'+tipopersonal+'</label></td>';
        tr += '<td><label class="Obs"></label>';
        tr += '<a href="javascript:agregar_observacion_corte('+i+')" class="Upd" >';
        tr += '<span title="Agregar Observacion de Corte" class="MljSoft-icon-refresh"></span>';
        tr += '</a></td>';
        tr += '<td align="center">';
        tr += '<span title="Ver Pagos de Usuario" onclick="abrir_popu_pagos('+NroInscripcion+');" class="icono-icon-detalle"></span>';
        tr += '</td>';
        tr += '<td align="center">';
        tr += '<a href=\'javascript:agregar_usuarios("'+i+'","'+NroInscripcion+'","'+CodCatastral+'","'+Propietario+'","'+Direccion+'","'+Sector+'","'+NroMedidor+'","'+codsector+'","'+Codantiguo+'")\' class="Del" >';
        tr += '<span class="icono-icon-trash" title="Borrar Registro" ></span>';
        tr += '</a></td>';
    tr += '</tr>';

    $("#tbcierreyapertura tbody").append(tr);
                   
    remove_item_usurio(index)
    agregar_corte_tabla(NroInscripcion, codmotivocorte, codtipocorte, codinspector, codtipopersonal, fechacorte, 1, '', codsector, NroMedidor, TpServico, Direccion, Destpservicio)
}

function agregar_corte_tabla(nroinscripcion, codmotivocorte, codtipocorte, codinspector, codtipopersonal, fechacorte, Op, Obs, Sector, NroMedidor, TpServico, Direccion, Destpservicio)
{
    $.ajax({
        url: 'guaradar_directo.php',
        type: 'POST',
        async: true,
        data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion+'&codmotivocorte='+codmotivocorte+'&codtipocorte='+codtipocorte+'&codinspector='+codinspector+'&codtipopersonal='+codtipopersonal+'&fechacorte='+fechacorte+'&codciclo='+$("#codciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val()+'&codsector='+Sector+'&Op='+Op+'&Obs='+Obs+'&codusu='+codusu+'&nromed='+NroMedidor+'&codtiposervicio='+TpServico+'&direccion='+Direccion+'&Destpservicio='+Destpservicio,
        success: function (datos) 
        {
            if (datos != 1)
                alert('Ocurrio un error al realizar la operacion')
        }
    });

}

function remove_item_corte(index)
{

    var id = parseInt($("#tbcierreyapertura tbody tr").length);
    $("#corte"+index).remove()
    var nextfila = index+1;
    var j;
    for (var i = nextfila; i <= id; i++)
    {
        j = i - 1;//alert(j);
        // $("#TbIndex tbody tr#"+i+" label.Item").text(j);
        $("#tbcierreyapertura tbody tr#corte"+i+" a.Del").attr("href", "javascript:agregar_usuarios("+j+")");
        $("#tbcierreyapertura tbody tr#corte"+i+" a.Upd").attr("href", "javascript:agregar_observacion_corte("+j+")");

        $("#tbcierreyapertura tbody tr#corte"+i).attr("id", 'corte'+j);

    }
}
function remove_item_usurio(index)
{

    var id = parseInt($("#tbcierre tbody tr").length);
    $("#"+index).remove()
    var nextfila = index+1;
    var j;
    /* for(var i=nextfila; i<=id; i++)
     {
     j=i-1;//alert(j);
     // $("#TbIndex tbody tr#"+i+" label.Item").text(j);
     $("tr#"+i+" img.Upd").attr("onclick","agregar_corte("+j+")");
     
     $("tr#"+i).attr("id",j);
     
     }*/

}
function agregar_observacion_corte(index)
{
    var Obs = $("#tbcierreyapertura tbody tr#corte"+index+" label.Obs").text()
    $("#observacion").val(Obs)
    $("#ItemUpd").val(index);

    $("#dialog-form-observcion").dialog("open");
}
function ObsOk()
{
    var index = $("#ItemUpd").val();
    $("#tbcierreyapertura tbody tr#corte"+index+" label.Obs").text($("#observacion").val())
    var nroinscripcion = $("#tbcierreyapertura tbody tr#corte"+index+" label.NroInscripcion").text()
    agregar_corte_tabla(nroinscripcion, 'codmotivocorte', 'codtipocorte', 'codinspector', 'codtipopersonal', 'fechacorte', 2, $("#observacion").val(), '', '')

}
function agregar_usuarios(index, NroInscripcion, CodCatastral, Propietario, Direccion, Sector, NroMedidor, codsector, Codantiguo)
{

    var j = parseInt($("#tbcierre tbody tr").length)
    var i = j+1;
    if (NroInscripcion == undefined)
    {
        var NroInscripcion = $("#tbcierreyapertura tbody tr#corte"+index+" label.NroInscripcion").text()
        var CodCatastral = $("#tbcierreyapertura tbody tr#corte"+index+" label.CodCatastral").text()
        var Propietario = $("#tbcierreyapertura tbody tr#corte"+index+" label.Propietario").text()
        var Direccion = $("#tbcierreyapertura tbody tr#corte"+index+" label.Direccionn").text()
        var Sector = $("#tbcierreyapertura tbody tr#corte"+index+" label.Sectorn").text()
        var NroMedidor = $("#tbcierreyapertura tbody tr#corte"+index+" label.NroMedidorn").text()
        var codsector = $("#tbcierreyapertura tbody tr#corte"+index+" label.CodSector").text()
        var Codantiguo = $("#tbcierreyapertura tbody tr#corte"+index+" label.Codantiguo").text()
    }
    i = NroInscripcion
    var tr = ' <tr id="'+i+'" onclick="SeleccionaId(this);">';
    tr += '<td align="center">';
    tr += '<label class="CodCatastral">'+CodCatastral+'</label>';
    tr += '<label class="CodSector">'+codsector+'</label>';
    tr += '<label class="NroInscripcion">'+NroInscripcion+'</label> </td>';
    tr += '<td><label class="Codantiguo">'+Codantiguo+'</label></td>';
    tr += '<td><label class="Propietario">'+Propietario+'</label></td>';
    tr += '<td><label class="Direccion">'+Direccion+'</label></td>';
    tr += '<td align="center">';
    tr += '<label class="Sector">'+Sector+'</label> </td>';
    tr += '<td align="center">';
    tr += '<label class="NroMedidor">'+NroMedidor+'</label> &nbsp;</td>';
    tr += '<td align="center">';
    tr += ' <span class="icono-icon-add Upd" ';
    tr += ' onclick=\'agregar_corte("'+i+'","'+NroInscripcion+'","'+CodCatastral+'","'+Propietario+'","'+Direccion+'","'+Sector+'","'+NroMedidor+'","'+codsector+'","'+Codantiguo+'")\'  title="Agregar Usuario para Corte de Servicio" ></span>';
    tr += '</td>';
    tr += '<td align="center"> ';
    tr += '<span class="icono-icon-ok" title="Ver Pagos de Usuario" onclick="abrir_popu_pagos('+NroInscripcion+');"  ></span>';
    tr += '</td>';
    tr += '</tr>';

    $("#tbcierre tbody").append(tr)

    remove_item_corte(index)

    agregar_corte_tabla(NroInscripcion, 'codmotivocorte', 'codtipocorte', 'codinspector', 'codtipopersonal', 'fechacorte', 3, '', codsector, NroMedidor)


}
