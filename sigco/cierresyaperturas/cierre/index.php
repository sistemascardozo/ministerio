<?php

include("../../../include/main.php");
include("../../../include/claseindex.php");

$TituloVentana = "CIERRE DE SERVICIO";
$Activo = 1;
CuerpoSuperior($TituloVentana);

$objMantenimiento = new clsDrop();

$Op = $_POST["Op"];
$Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];

$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);
$guardar = "op=".$Op;

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>

<script type="text/javascript" src="js_cierre.js" language="JavaScript"></script>
<script>
    var codsuc = <?=$codsuc ?>;

    var c = 0
    function ValidarForm(Op)
    {
        var ciclos = $("#ciclo").val()
        var rutas = ""
        var sectores = ""

        if (ciclos == 0)
        {
            alert('Seleccione el Ciclo')
            return
        }
        if ($("#todosectores").val() == 1)
        {
            sectores = "%"
        } else {
            sectores = $("#codsector").val()
            if (sectores == 0)
            {
                alert("Seleccione el Sector")
                return false
            }
        }

        url = "mantenimiento.php?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+ciclos +
                "&sector="+sectores+"&codsuc="+codsuc+"&ciclotext="+$("#ciclo option:selected").text()
        location.href = url;

        return false
    }
    function Cancelar()
    {
        location.href = '<?php echo $_SESSION['urldir']; ?>/admin/indexB.php'
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>

                <tr>
                    <td colspan="2">
                        <fieldset style="padding:4px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="11%">Sucursal</td>
                                    <td width="4%" align="center">:</td>
                                    <td width="15%">
                                        <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                                    </td>
                                    <td width="13%" align="right">Ciclo</td>
                                    <td width="4%" align="center">:</td>
                                    <td width="53%">
                                        <?php $objMantenimiento->drop_ciclos($codsuc,0,"onchange='datos_facturacion(this.value);'"); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sector</td>
                                    <td align="center">:</td>
                                    <td colspan="4">
                                        <?php echo $objMantenimiento->drop_sectores2($codsuc); ?>
                                        <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosectores');quitar_disabled(this, 'codsector');" />
                                        Todos los Sectores 
                                    </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td colspan="2">
                                        <input type="hidden" name="todosectores" id="todosectores" value="1" /></td>
                                    <td align="center">&nbsp;</td>
                                    <td><input type="hidden" name="anio" id="anio" />
                                        <input type="hidden" name="mes" id="mes" /></td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="center"><input type="button" onclick="return ValidarForm();" value="Aceptar" id=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </tbody>

        </table>
    </form>
</div>
<script>
    $("#codsector").attr("disabled", true)
</script>
<?php CuerpoInferior(); ?>