<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codmotivocorte = $_POST["codmotivocorte"];
	$tipooperacion	= $_POST["tipooperacion"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$estareg		= $_POST["estareg"];
	

	switch ($Op) 
	{
		case 0:
			$id   				= $objFunciones->setCorrelativos("motivocorte","0","0");
			$codmotivocorte 	= $id[0];
			
			$sql = "insert into public.motivocorte(codmotivocorte,descripcion,estareg,tipooperacion) values(:codmotivocorte,:descripcion,:estareg,:tipooperacion)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codmotivocorte"=>$codmotivocorte,
						   ":tipooperacion"=>$tipooperacion,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));

		break;
		case 1:
			$sql = "update public.motivocorte set descripcion=:descripcion,estareg=:estareg,tipooperacion=:tipooperacion
				where codmotivocorte=:codmotivocorte";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codmotivocorte"=>$codmotivocorte,
						   ":tipooperacion"=>$tipooperacion,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));

		break;
		case 2:case 3:
			$sql = "update public.motivocorte set estareg=:estareg
				where codmotivocorte=:codmotivocorte";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codmotivocorte"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
