<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$nroinscripcion = $_POST["nroinscripcion"];
	$anio			= $_POST["anio"];
	$lecturas		= $_POST["lecturas"];
	$codciclo		= $_POST["codciclo"];
	$codsuc			= $_POST["codsuc"];

	$top ='';
	$limit = '';
	
	if($lecturas != "%")
	{
		$top = '';
		$limit = "limit ".$lecturas;
	}

	$consulta = $conexion->prepare("SELECT * FROM medicion.view_cabecera_tarjeta WHERE nroinscripcion = ? AND codsuc = ? AND codciclo = ?");
	$consulta->execute(array($nroinscripcion,$codsuc,$codciclo));
	$rowcabecera = $consulta->fetch();

?>
<style>
	.text1{
		font-size:11px;
		font-weight:bold;
		color:#000;
	}
	.text2{
		font-size:11px;
		font-weight:bold;
		color:#800000;
	}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
    	<fieldset style="padding:4px">
       	  <table width="800" border="0" cellspacing="0" cellpadding="0">
    	      <tr style="padding:1px">
    	        <td width="150" class="text1">Codigo</td>
    	        <td width="30" align="center" class="text1">:</td>
    	        <td width="400" class="text1"><?=$rowcabecera["nroinscripcion"]?>&nbsp;</td>
    	        <td width="200" align="right" class="text1">Nro. Medidor</td>
    	        <td width="30" align="center" class="text1">:</td>
    	        <td class="text2"><?=strtoupper(trim($rowcabecera["nromed"]))?></td>
            </tr>
    	      <tr style="padding:1px">
    	        <td class="text1">Cod. Catastral</td>
    	        <td align="center" class="text1">:</td>
    	        <td class="text1"><?=$rowcabecera["catastro"]?></td>
    	        <td align="right" class="text1">Diametro de Medidor</td>
    	        <td align="center" class="text1">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["diamedidor"])?></td>
            </tr>
    	      <tr style="padding:1px">
    	        <td class="text1">Usuario</td>
    	        <td align="center">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["propietario"])?></td>
    	        <td align="right" class="text1">Tipo de Medidor</td>
    	        <td align="center">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["tipomed"])?></td>
            </tr>
    	      <tr style="padding:1px">
    	        <td class="text1">Direccion</td>
    	        <td align="center">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["direccion"])?></td>
    	        <td align="right" class="text1">Marca de Medidor</td>
    	        <td align="center">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["marcmedidor"])?></td>
            </tr>
    	      <tr style="padding:1px">
    	        <td class="text1">Categoria</td>
    	        <td align="center" class="text1">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["nomtar"])?></td>
    	        <td align="right" class="text1">Capacidad Medidor</td>
    	        <td align="center" class="text1">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["capacidadmed"])?></td>
            </tr>
    	      <tr style="padding:1px">
    	        <td class="text1">Est. Servicio</td>
    	        <td align="center" class="text1">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["descripcion"])?></td>
    	        <td align="right" class="text1">Tipo de Facturacion</td>
    	        <td align="center" class="text1">:</td>
    	        <td class="text1"><?=strtoupper($tfacturacion[$rowcabecera["tipofacturacion"]])?></td>
            </tr>
    	      <tr style="padding:1px">
    	        <td class="text1">Ruta de Lectura</td>
    	        <td align="center">:</td>
    	        <td class="text1"><?=strtoupper($rowcabecera["codrutlecturas"])?></td>
    	        <td align="right" class="text1">Nro. Orden</td>
    	        <td align="center">:</td>
    	        <td><?=strtoupper($rowcabecera["nroorden"])?></td>
            </tr>
   	      </table>
    	</fieldset>
    </td>
  </tr>
  <tr style="padding-top:3px">
    <td>
    <div id="div_lecturas" style="overflow:auto; height:200px">
        <table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tblecturas" >
           <thead class="ui-widget-header" style="font-size: 11px">
            <tr align="center">
              <th width="20%" >Mes y A&ntilde;o</th>
              <th width="16%" >Fecha de Lectura</th>
              <th width="14%" >Lect. Anterior</th>
              <th width="14%" >Lect. Ultima</th>
              <th width="11%" >Lect. Promedio </th>
              <th width="10%" >Consumo</th>
              <th width="13%" >Consumo Fact.</th>
              <th width="8%" >Importe</th>
            </tr>
          </thead>
          <tbody>
<?php 
				$sqldetalle = "SELECT ".$top." * FROM medicion.view_detalle_tarjeta ";
				$sqldetalle .= "WHERE nroinscripcion = ".$nroinscripcion." AND anio = '".$anio."' AND codsuc = ".$codsuc." AND codciclo = ".$codciclo." ";
				$sqldetalle .= "ORDER BY anio, mes DESC ".$limit; 
				$consultaD = $conexion->prepare($sqldetalle);
				$consultaD->execute(array());
				$items = $consultaD->fetchAll();
			//	var_dump($consultaD->errorInfo());
				foreach($items as $rowdetalle)
				{
			?>
                <tr style="font-size: 11px" align="center">
                  <td align="right"><?=$meses[$rowdetalle["mes"]]." - ".$rowdetalle["anio"]?>&nbsp;</td>
                  <td align="center"><?=$objFunciones->DecFecha($rowdetalle["fechalectultima"])?></td>
                  <td align="right"><?=number_format($rowdetalle["lecturaanterior"],2)?></td>
                  <td align="right"><?=number_format($rowdetalle["lecturaultima"],2)?></td>
                  <td align="right"><?=number_format($rowdetalle["lecturapromedio"],2)?></td>
                  <td align="right"><?=number_format($rowdetalle["consumo"],2)?></td>
                  <td align="right"><?=number_format($rowdetalle["consumofact"],2)?></td>
                  <td align="right"><?=number_format($rowdetalle["importe"],2)?></td>
                 </tr>
            <?php } ?>
          </tbody>
        </table>
     </div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
