<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");

  $TituloVentana = "TARJETA DE LECTURAS";
  $Activo = 1;
  
  CuerpoSuperior($TituloVentana);
  
  $codsuc   = $_SESSION['IdSucursal'];

  $objFunciones   = new clsDrop();

  
  $documento    = 3;
  $sucursal     = $objFunciones->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);

?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_tarjeta.js" language="JavaScript"></script>
<script>
	var codsuc 	= <?=$codsuc?>;
	
	function ValidarForm(Op)
	{
		if($("#nroinscripcion").val()=="")
		{
			Msj($("#nroinscripcion"),'Seleccione un Usario')
			return false
		}
		if($("#anio").val()==0)
		{
			 Msj($("#anio"),'Seleccione a&ntilde;o a consultar')
			return false
		}

		$("#div_consulta").html('<span class="icono-icon-loading"></span>Consultado...')
		$.ajax({
			 url:'consulta.php',
			 type:'POST',
			 async:true,
			 data:'nroinscripcion='+$("#nroinscripcion").val()+'&anio='+$("#anio").val()+'&lecturas='+$("#lecturas").val()+
			 	  '&codsuc='+codsuc+'&codciclo='+$("#ciclo").val(),
			 success:function(datos){
				$("#div_consulta").html(datos)
			 }
		}) 
		return false
	}	
	function Cancelar()
	{
		location.href = '<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}

</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
        <tr><td></td></tr>
    <tr>
        <td width="70">Usuario</td>
        <td width="30" align="center">:</td>
        <td width="204"><label>
          <input type="text" name="nroinscripcion" id="nroinscripcion" class="inputtext" readonly="readonly" style="width:75px;" />
          <img src="../../../../images/iconos/buscar.png" width="16" height="16" style="cursor:pointer" onclick="buscar_usuarios();" /></label></td>
        <td width="148" align="right">A&ntilde;o</td>
        <td width="14" align="center">:</td>
        <td width="372" align="left">
        	<div id="div_anio">
        	<?php
				echo "<select id='anio' name='anio' class='select' style='width:120px' >";
				echo "<option value='0'>".utf8_encode("--Seleccione el A�o--")."</option>";
				echo "</select>";
			?>
            </div>
        </td>
	</tr>
    <tr>
      <td>Propietario</td>
      <td align="center">:</td>
      <td colspan="4">
      	<input type="text" class="inputtext" id="propietario" name="propietario" readonly="readonly" maxlentgth="200" style="width:400px;" />
      </td>
    </tr>
    <tr>
      <td>Lectuas</td>
      <td align="center">:</td>
      <td><label>
        <select name="lecturas" id="lecturas" class="select" >
        	<option value="%">Mostrar Todos los Meses</option>
            <option value="1">Mostrar El Ultimo Mes</option>
            <option value="2">Mostrar Los 2 Ultimos Meses</option>
            <option value="6">Mostrar Los 6 Ultimos Meses</option>
        </select>
      </label></td>
      <td align="right"><input type="hidden" name="ciclo" id="ciclo" value="0" /></td>
      <td align="center">&nbsp;</td>
      <td align="left">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="6" align="center"> <input type="button" onclick="return ValidarForm();" value="Consultar" id=""></td>
    </tr>
    <tr>
      <td colspan="6"></td>
    </tr>
    <tr align="center">
      <td colspan="6">
      	<div id="div_consulta">
        </div>
      </td>
    </tr>
    
	</tbody>
	
    </table>
 </form>
</div>