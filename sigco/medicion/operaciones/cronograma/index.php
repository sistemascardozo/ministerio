<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "CRONOGRAMA DE LECTURAS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	$Fecha = date('d/m/Y');
	$sucursal 		= $objDrop->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<style type="text/css">
.multiselect {
    width:20em;
    height:5em;
    border:solid 1px #c0c0c0;
    overflow:auto;
}
 
.multiselect label {
    display:block;
}
 
.multiselect-on {
    color:#ffffff;
    background-color:#000099;

}
.CodSec,.CodRut {display: none;}
</style>
<script>
	var codsuc = <?=$codsuc?>;
	var c = 0

	$(function() {
		$("#codsector" ).change(function() {
			if($("#ciclo").val()==0)
		{
			Msj($("#ciclo"),'Seleccione el Ciclo')
			return false
		}
		cargar_rutas_distribucion();
		$("#tbsectores tbody").html('');
		$("#chkrutas").attr('checked',false)
		cCronorgrama();
		});

		$("input.fecha").live("click", function() {
        $(this)
            .removeClass("hasDatepicker")
            .datepicker({showOn:"focus"})
            .focus();
		    });

		var dates = $( "#FechaDesde, #FechaHasta" ).datepicker({
			buttonText: 'Selecione Fecha de Busqueda',
			showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: '<?php echo $_SESSION['urldir'];?>images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true,
			numberOfMonths: 2,
			onSelect: function( selectedDate ) {
				var option = this.id == "FechaDesde" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});

	});
	function cargar_rutas_distribucion(codsector)
	{
		var codsector = $("#codsector").val()
		$.ajax({
				 url:'cRuta.php',
				 type:'POST',
				 async:true,
				 data:'IdSector='+codsuc+'-'+codsector,
				 success:function(datos){

					$("#DivRuta").html(datos)
				 }
			}) 
	}
	function chckruta(obj)
	{
		if($(obj).attr("checked"))
			$(obj).parent().addClass("multiselect-on");
		else
			$(obj).parent().removeClass("multiselect-on"); 
	}
	function chckrutaall(obj)
	{
		
		if($(obj).attr("checked"))
		{

			$('#DivRuta :input').each(function()
			 {
			 		
			 		$(this).attr("checked","checked");
			 		$(this).parent().addClass("multiselect-on"); 
			 		//chcksector(this)
			});
		}
		else
		{
			$('#DivRuta :input').each(function()
			 {
			 		$(this).attr("checked",false); 
			 		$(this).parent().removeClass("multiselect-on"); 
			 		
			});
		}
	}
	function AgregarItem()
	{
		var sectores = "";
		
		if($("#ciclo").val()==0)
		{
			Msj($("#ciclo"),'Seleccione el Ciclo')
			return false
		}
		var codsec 	= $("#codsector").val()
		var decSec	= $("#codsector option:selected").text(); 
		if(codsec==0)
		{
			Msj($("#codsector"),"Seleccione el Sector"); return false;
		}
		i=0

			////////////
		$('#DivRuta :input').each(function()
			 {
		 		if($(this).attr("checked"))
		 			{
		 				i++
		 			}
			});
		if(i==0)
		{
			Msj('#DivRuta','Seleccione Rutas',1000,'right','error',false)
			return false;
		}

		var FechaD = $("#FechaDesde").val()
		var FechaH = $("#FechaHasta").val()
		var j=parseInt($("#tbsectores tbody tr").length);
		
	$('#DivRuta :input').each(function() {			
				
			  if($(this).attr("checked"))
			  {
			  	codrut 	= $(this).val()
				decRut	= $(this).data('nombre'); 
				noagrego=true
				for(i=1;i<=j;i++)
				{

					if(codsec==$("#tbsectores tbody tr#"+i+" label.CodSec").text() && codrut==$("#tbsectores tbody tr#"+i+" label.CodRut").text())
					{
						noagrego=false
					}

				}
				if(noagrego)cargar_sectores_periodo(codsec,decSec,codrut,decRut,FechaD,FechaH)


			  }


			});
	

}
function cargar_sectores_periodo(codsec,decSec,codrut,decRut,FechaD,FechaH)
{
	
	var i=parseInt($("#tbsectores tbody tr").length)+1
	var tr='<tr id="'+i+'" onclick="SeleccionaId(this);">';
	tr+='<td align="left" style="padding-left:5px;"><label class="CodSec">'+codsec+'</label>';
	tr+='<label class="DesSec">'+decSec+'</label></td>';
	tr+='<td align="left" style="padding-left:5px;"><label class="CodRut">'+codrut+'</label>';
	tr+='<label class="DesRut">'+decRut+'</label></td>';
	tr+='<td align="center">';
	tr+='<input type="text" class="fecha text inputtext ui-corner-all"  id="FechaD'+i+'" ';
	tr+=' value= "' + FechaD + '" style="width:80px;">-';
	tr+='<input type="text" class="fecha text inputtext ui-corner-all"  id="FechaH'+i+'" ';
	tr+=' value= "' + FechaH + '" style="width:80px;">';
	tr+='</td>';
	tr+='<td align="center">';
	tr+='<span class="icono-icon-trash" title="Borrar Registro" onclick="QuitaItem('+i+')"></span>';
	tr+='</td></tr>';
	$("#tbsectores tbody").append(tr);

}
  function QuitaItem(tr)
  { 
    var id=parseInt($("#tbsectores tbody tr").length);
      $("#tbsectores tbody tr#"+tr).remove();
      var nextfila=tr+1;
      var j;
      var IdMat="";
      for(var i=nextfila; i<=id; i++)
      {
        j=i-1;//alert(j);
        $("#tbsectores tbody tr#"+i+" span.icono-icon-trash").attr("href","javascript:QuitaItem("+j+")");
        $("#FechaD"+i).attr("id","FechaD"+j);
        $("#FechaH"+i).attr("id","FechaH"+j);
        $("#tbsectores tbody tr#"+i).attr("id",j);
        
      }

    }
function quitar_todos()
{
	$("#tbsectores tbody").html('');
	cont=0;
	$("#cont_horario").val(0)

}	
	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
function validar_lecturas(codsuc,codciclo)
{
	$.ajax({
		 url:'../apertura/validar_lecturas.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&codciclo='+codciclo+'&sector='+$("#codsector").val(),
		 success:function(datos){
			$("#con_lecturas").val(datos)
			if($("#con_lecturas").val()==0)
			{
				Msj($("#tbsectores"),'No se Aperturo lecturas')
				$("#btnAp2").hide();
				return false
			}
		 }
	}) 
}
function datos_facturacion(codciclo)
{
	$.ajax({
		 url:'../../../../ajax/periodo_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+codciclo+'&codsuc='+codsuc,
		 success:function(datos){
			var r=datos.split("|")

			$("#anioperdio").val(r[2]+"-"+r[1])
			$("#anio").val(r[1])
			$("#mes").val(r[3])

		 }
	}) 
}
function Guardar()
{
	var id=$("#tbsectores tbody tr").length;
	if(id==0)
	{
		Msj($("#tbsectores"),'No Existen Datos a Guardar')
		return false
	}
	if($("#con_lecturas").val()==0)
	{

		Msj($("#tbsectores"),'No se Aperturo lecturas')
		return false
	}
	var tr=''
	for(var i=1; i<=id; i++)
      { 
        var CodSec=$("#tbsectores tbody tr#"+i+" label.CodSec").text()
        var CodRut=$("#tbsectores tbody tr#"+i+" label.CodRut").text()
        var FechaD = $("#FechaD"+i).val();
        var FechaH = $("#FechaH"+i).val();

        tr+='<input type="hidden" name="x'+i+'_codsector"  value="'+CodSec+'"/>';
        tr+='<input type="hidden" name="x'+i+'_codrutlecturas"  value="'+CodRut+'"/>';
        tr+='<input type="hidden" name="x'+i+'_fechalecanterior"  value="'+FechaD+'"/>';
        tr+='<input type="hidden" name="x'+i+'_fechalecultima"  value="'+FechaH+'"/>';

    }
    tr+='<input type="hidden" name="NroItems" id="Items"  value="'+id+'"/>';
     $("#DivSave").html(tr)
     var Data = $("#form1").serialize();
     $.blockUI({ message: '<div><center><span class="icono-icon-loading"></span>Guardando...Espere un Momento por Favor...</center></div>',
				  css: { 
					border: 'none', 
					padding: '15px', 
					backgroundColor: '#000', 
					'-webkit-border-radius': '10px', 
					'-moz-border-radius': '10px', 
					opacity: .5, 
					color: '#fff' 
						}
				  }); 
			$.ajax({
			 url:'guardar.php',
			 type:'POST',
			 async:true,
			  data: Data,
			 success:function(data){
				 if (parseInt(data)!=0)
					{
						window.parent.OperMensaje("Ocurrio un problema al guardar",2)	
						Msj($("#btnAp2"),'Ocurrio un problema al guardar')
					}
				else
					{
						
						window.parent.OperMensaje("Cronograma Generado Correctamente",1)
						Msj($("#btnAp2"),'Cronograma Generado Correctamente')
					}
					
					$.unblockUI()
			}
			 })

}
function cCronorgrama()
{
	$.ajax({
		 url:'Consulta.php',
		 type:'POST',
		 async:true,
		 data:'codemp=1&codsuc=<?=$codsuc?>&anio='+$("#anio").val()+'&mes='+$("#mes").val()+'&codciclo='+$("#ciclo").val()+'&codsector='+$("#codsector").val(),
		 success:Muestra_Consulta
		})
}
function Muestra_Consulta(datos)
	{	
		$("#tbsectores tbody").html(datos)
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
      <td colspan="2">
        <fieldset style="padding:4px">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="80">Sucursal
              	<div id="DivSave"></div>
              <input type="hidden" name="anio" id="anio" value="" />
                    	<input type="hidden" name="mes" id="mes" value="" />
                    	<input type="hidden" name="codsuc" id="codsuc" value="<?=$codsuc?>" />
                    	<input type="hidden" id="con_lecturas" value="0" />
                    </td>
              <td width="30" align="center">:</td>
              <td ><label>
                <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
                </label></td>
                <td width="80">Ciclo</td>
              <td width="30" align="center">:</td>
              <td width="35%">
                <? $objDrop->drop_ciclos($codsuc, 0, "onchange='datos_facturacion(this.value); validar_lecturas(".$codsuc.",this.value);'"); ?>
                </td>
              </tr>
              <tr>
	              <td>Mes y A&ntilde;o </td>
				    <td align="center">:</td>
				    <td><input type="text" name="anioperdio" id="anioperdio" value="" class="inputtext" readonly="readonly" /></td>
				    
            </tr>
            <tr>
              <td>Sector</td>
              <td align="center">:</td>
              <td>
                <?php echo $objDrop->drop_sectores2($codsuc); ?>

                </td>
            	<td>Ruta 
            		<input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox"  onclick="chckrutaall(this);CambiarEstado(this,'todosrutas');" title="Todas las Rutas"/>
            	</td>
	            <td align="center">:</td>
	            <td>
                 <div class='multiselect' id='DivRuta'></div>
                  </td>
              </tr>
              <tr>
                <td>Periodo Inicial</td>
                 <td align="center">:</td>
                <td>
                 <input type="text" class="text ui-corner-all" id="FechaDesde" maxlength="10" value="<?=$Fecha?>" style="width:85px;" />
                 </td>
                <td >Periodo Final</td>
                 <td align="center">:</td>
                <td>
                 <input type="text" class="text ui-corner-all" id="FechaHasta" size="9" maxlength="10" value="<?=$Fecha?>" style="width:85px;" />
                </td>
			</tr>
              <tr>
                <td colspan="6" align="center">&nbsp;</td>
              </tr>
              <tr>
              <td colspan="6" align="center">
              	<input type="button" onclick="AgregarItem();" value="Agregar" id="btnAp">
              	<input type="button" onclick="Guardar();" value="Guardar" id="btnAp2">
              	</td>
              </tr>
            <tr>
              <td colspan="6" align="center">&nbsp;</td>
            </tr>
        	<tr>
        		<td colspan="6">
        			<div id="sectores_zonas" style="overflow:auto; height:200px;">
                <table border="1"  align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbsectores" rules="all" >
	              <thead>
	              <tr class="ui-widget-header" align="center">
	                <td width="120" >Sector</td>
	                <td >Ruta</td>
	                <td width="170" >Periodo</td>
	                <td width="30" >
	                	<span class="icono-icon-trash" title='Quitar Todos' onclick='quitar_todos()'></span>
	                </td>
	              </tr>
	              </thead>
	              <tbody>
				  </tbody>
	              </table>

                  </div>
        		</td>
        	</tr>
            </table>
          </fieldset>
        </td>
    </tr>

    </tbody>

    </table>
 </form>
</div>
<?php   CuerpoInferior();?>