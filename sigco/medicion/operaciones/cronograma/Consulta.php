<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codemp		= 1;
	$codsuc   = $_POST["codsuc"];
	$codciclo = $_POST["codciclo"];
	$anio     = $_POST["anio"];
	$mes      = $_POST["mes"];
	$sector   = $_POST["codsector"];
	
	$Sql = "SELECT DISTINCT c.codsector, s.descripcion AS sector, c.codrutlecturas, ";
	$Sql .= " (SELECT r.descripcion FROM public.rutasmaelecturas r ";
	$Sql .= "  WHERE r.codsuc = c.codsuc AND r.codsector = c.codsector AND r.codrutlecturas = c.codrutlecturas) AS ruta, ";
	$Sql .= " c.fechalecanterior, c.fechalecultima ";
	$Sql .= "FROM medicion.lecturas_cronograma c ";
	$Sql .= " INNER JOIN public.sectores s ON (c.codemp = s.codemp) AND (c.codsuc = s.codsuc) AND (c.codsector = s.codsector) ";
	$Sql .= "WHERE c.codemp = ? AND c.codsuc = ? AND c.anio = ? AND c.mes = ? AND c.codciclo = ? AND c.codsector = ? ";
	$Sql .= "ORDER BY c.codrutlecturas";
	
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array($codemp, $codsuc, $anio, $mes, $codciclo, $sector));
	$items = $consulta->fetchAll();
	$i=0;
	$tr='';
	foreach($items as $row)
	{
		$i++;
		$tr.='<tr id="'.$i.'" onclick="SeleccionaId(this);">';
		$tr.='<td align="left" style="padding-left:5px;"><label class="CodSec">'.$row['codsector'].'</label>';
		$tr.='<label class="DesSec">'.$row['sector'].'</label></td>';
		$tr.='<td align="left" style="padding-left:5px;"><label class="CodRut">'.$row['codrutlecturas'].'</label>';
		$tr.='<label class="DesRut">'.$row['ruta'].'</label></td>';
		$tr.='<td align="center">';
		$tr.='<input type="text" class="fecha text inputtext ui-corner-all"  id="FechaD'.$i.'" ';
		$tr.=' value= "'.$objFunciones->DecFecha($row['fechalecanterior']).'" style="width:80px;">-';
		$tr.='<input type="text" class="fecha text inputtext ui-corner-all"  id="FechaH'.$i.'" ';
		$tr.=' value= "'.$objFunciones->DecFecha($row['fechalecultima']).'" style="width:80px;">';
		$tr.='</td>';
		$tr.='<td align="center">';
		$tr.='<span class="icono-icon-trash" title="Borrar Registro" onclick="QuitaItem('.$i.')"></span>';
		$tr.='</td></tr>';
	}
	if($tr=='')
	{
		$Select ="SELECT r.codrutlecturas,su.descripcion,s.descripcion as sector,
  				r.descripcion as ruta, s.codsector,s.codsuc
				FROM
  				public.sectores s
			  INNER JOIN public.rutasmaelecturas r ON (s.codsector = r.codsector)
			  AND (s.codemp = r.codemp)
			  AND (s.codsuc = r.codsuc)
			  INNER JOIN admin.sucursales su ON (su.codsuc = s.codsuc)
			  AND (su.codemp = s.codemp)
  			WHERE  s.codsector=".$sector." AND r.codsuc=".$codsuc." AND r.estareg=1
  			ORDER BY r.codrutlecturas";
		 $Consulta=$conexion->query($Select);
		 foreach($Consulta->fetchAll() as $row)
		 {
		 	//OBTENER ULTIMA FECHA
		 	$Sql="SELECT fechalectanterior,fechareg
				from medicion.lecturas 
				where codemp=".$codemp." and codsuc=".$codsuc." 
						and codciclo=".$codciclo." AND
						anio='".$anio."' and mes='".$mes."' and	codsector=".$sector."
				   		AND codrutlecturas='".$row['codrutlecturas']."'
				   		group by  fechalectanterior,fechareg";
			$row2      =  $conexion->query($Sql)->fetch();	
			if($row2['fechalectanterior']!='')
			{
				$fecha = date($row2['fechalectanterior']);
				$nuevafecha = strtotime ( '+31 day' , strtotime ( $fecha ) ) ;
				$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
			}
			else
			{
				$fecha = date($row2['fechareg']);
				$nuevafecha = strtotime ( '-31 day' , strtotime ( $fecha ) ) ;
				$row2['fechalectanterior'] = date ( 'Y-m-j' , $nuevafecha );

				$nuevafecha=$row2['fechareg'];
				
			}
			if($nuevafecha!='')
			{
				$i++;
			 	$tr.='<tr id="'.$i.'" onclick="SeleccionaId(this);">';
				$tr.='<td align="left"><label class="CodSec">'.$row['codsector'].'</label>';
				$tr.='<label class="DesSec">'.$row['sector'].'</label></td>';
				$tr.='<td align="left"><label class="CodRut">'.$row['codrutlecturas'].'</label>';
				$tr.='<label class="DesRut">'.$row['ruta'].'</label></td>';
				$tr.='<td align="center" >';
				$tr.='<input type="text" class="fecha text inputtext ui-corner-all"  id="FechaD'.$i.'" ';
				$tr.=' value= "'.$objFunciones->DecFecha($row2['fechalectanterior']).'">-';
				$tr.='<input type="text" class="fecha text inputtext ui-corner-all"  id="FechaH'.$i.'" ';
				$tr.=' value= "'.$objFunciones->DecFecha($nuevafecha).'">';
				$tr.='</td>';
				$tr.='<td align="center">';
				$tr.='<span class="icono-icon-trash" title="Borrar Registro" onclick="QuitaItem('.$i.')"></span>';
				$tr.='</td></tr>';
			}

		 }
	}
echo $tr;
?>