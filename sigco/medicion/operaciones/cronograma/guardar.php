<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	set_time_limit(0);
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	file_put_contents('progreso.txt','0|Calculando..');
	$codemp		= 1;
	$codsuc   = $_POST["codsuc"];
	$codciclo = $_POST["ciclo"];
	$anio     = $_POST["anio"];
	$mes      = $_POST["mes"];
	$NroItems = $_POST["NroItems"];
	$sector   = $_POST["codsector"];
	
	$conexion->beginTransaction();
	
	//SE VALIDARA Q NO SE ELIMINE SI TIENE LECTURAS ASIGNADASS
	$sql = "DELETE FROM medicion.lecturas_cronograma ";
	$sql .= "WHERE codemp = ? AND codsuc = ? AND anio = ? AND mes = ? AND codciclo = ? AND codsector = ?";
	

	$result = $conexion->prepare($sql);
	$result->execute(array($codemp, $codsuc, $anio, $mes, $codciclo, $sector));
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error DELETE lecturas";
		die();
	}
	//INSERTAR DETALLE
	$NroItems = $_POST["NroItems"];
	
	$Error = 0;
	
	for ($i=1; $i<=$NroItems; $i+= 1)
	{
		$Sql = "INSERT INTO medicion.lecturas_cronograma(codemp, codsuc, anio, mes, codciclo, ";
		$Sql .= " codsector, codrutlecturas, fechalecanterior, fechalecultima) ";
		$Sql .= "VALUES(".$codemp.", ".$codsuc.", '".$anio."', '".$mes."', ".$codciclo.", ";
		$Sql .= " ".$sector.", ".$_POST['x'.$i.'_codrutlecturas'].", '".$objFunciones->CodFecha($_POST['x'.$i.'_fechalecanterior'])."', '".$objFunciones->CodFecha($_POST['x'.$i.'_fechalecultima'])."')";
		
		$result = $conexion->prepare($Sql);
		$result->execute(array());
		
		if($result->errorCode() != '00000')
		{
			$Error = 1;
			
			echo "Error INSERT -><br>".$Sql."<br>";
		}
	}
	//INSERTAR DETALLE
	//CLIENTES ACTUALIZAR
	$Sql = "SELECT * FROM medicion.lecturas_cronograma ";
	$Sql .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." ";
	$Sql .= " AND anio = '".$anio."' AND mes = '".$mes."' AND codciclo = ".$codciclo." AND codsector = ".$sector;
	
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array());
	
	$items = $consulta->fetchAll();
	
	$totalAvance = count($items);
	
	foreach($items as $row)
	{
		
		$Sql="UPDATE  catastro.clientes 
				SET fechalecturaanterior = '".$row['fechalecanterior']."',
				fechalecturaultima='".$row['fechalecultima']."'
		        WHERE codsuc={$codsuc} AND codsector=".$row['codsector']." AND 
		        codrutlecturas='".$row['codrutlecturas']."'";
		$result = $conexion->query($Sql);

		$Sql="UPDATE  medicion.lecturas 
				SET fechalectanterior = '".$row['fechalecanterior']."',
				fechalectultima='".$row['fechalecultima']."'
		        WHERE codsuc={$codsuc} AND codsector=".$row['codsector']." AND 
		        codrutlecturas='".$row['codrutlecturas']."'
		        AND anio='".$anio."' AND mes='".$mes."'";
		$result = $conexion->query($Sql);

		$actualAvance++;
		$porcentajeAvance = round($actualAvance*100/$totalAvance);
	    $textoAvance=$porcentajeAvance."|".$actualAvance." de ".$totalAvance;
	    file_put_contents('progreso.txt',$textoAvance);
	}


	//$conexion->rollBack();
	$textoAvance="100|".$totalAvance." de ".$totalAvance;
	file_put_contents('progreso.txt',$textoAvance);
	
	if($Error == 1)
	{
		$conexion->rollBack();
		echo "1";
	}else{
		$conexion->commit();
		echo "0";
	}

?>