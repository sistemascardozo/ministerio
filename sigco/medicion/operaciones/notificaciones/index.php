<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "NOTIFICACIONES DE CONSUMO";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc 	= $_SESSION['IdSucursal'];

	$objFunciones 	= new clsDrop();
	$documento		= 3;
	$sucursal 		= $objFunciones->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="../digitacion/js_digitacion.js" language="JavaScript"></script>

<script>
	var codsuc = <?=$codsuc?>

	function ValidarForm(Op)
	{
		var rutas 		= ""
		var sectores	= ""
		
		if($("#ciclo").val()==0)
		{
			alert('Seleccione el Ciclo')
			return
		}
		if($("#todosectores").val()==0)
		{
			sectores = $("#codsector").val();
			if(sectores == 0)
			{
				alert("Seleccione el Sector")
				return false
			}		
		}else{
			sectores="%"
		}
		
		if($("#todosrutas").val()==0)
		{
			rutas=$("#rutaslecturas").val();
			if(rutas==0)
			{
				alert("Seleccione la Ruta de Lectura")
				return false
			}		
		}else{
			rutas="%"
		}
		
		url = "imprimir.php?codsuc=<?=$codsuc?>&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&ruta="+rutas+"&sector="+sectores
		AbrirPopupImpresion(url,800,600)
		return false;
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}

	function Generar()
	{
		var sectores = "";
		
		if( $("#ciclo").val()==0)
		{
			Msj($("#ciclo"),'Seleccione Ciclo')
			return
		}
		$("#DivResult").html('<span class="icono-icon-loading"></span>Generando Notificaciones...')
		$.ajax({
			 url:'Guardar.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&anio='+$("#anio").val()+'&mes='+$("#mes").val()+'&ciclo='+$("#ciclo").val(),
			 success:function(datos){
				$("#DivResult").html(datos)
			 }
        }) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="70">Sucursal</td>
				    <td width="30" align="center">:</td>
				    <td width="42%"><label>
				      <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </label></td>
				    <td width="3%" align="center">&nbsp;</td>
				    <td width="41%">&nbsp;</td>
				  </tr>
				  <tr>
				    <td>Ciclo</td>
				    <td align="center">:</td>
				    <td>
                    	<? $objFunciones->drop_ciclos($codsuc,0,"onchange='datos_facturacion(this.value);'"); ?>
                    </td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  
				  <tr>
				    <td>Sector</td>
				    <td align="center">:</td>
				    <td><?php echo $objFunciones->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_lecturas(".$codsuc.", this.value, 1);'"); ?></td>
				    <td align="center"><input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosectores'); validar_sector(this);" /></td>
				    <td>Todos los Sectores</td>
			      </tr>
				  <tr>
				    <td>Ruta</td>
				    <td align="center">:</td>
				    <td>
                    <div id="div_rutaslecturas">
               	  	<select name="rutaslecturas" id="rutaslecturas" style="width:220px" class="select" disabled="disabled">
                      	<option value="0">--Seleccione la Ruta--</option>
                    </select>
                    </div></td>
				    <td align="center"><input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todosrutas');validar_rutas(this);" /></td>
				    <td>Todas las Rutas</td>
			      </tr>
			
				  <tr>
				    <td colspan="5" align="center">
				    	<div id="DivResult">&nbsp;</div>
				    	<input type="hidden" name="todosectores" id="todosectores" value="1" />
			        	<input type="hidden" name="todosrutas" id="todosrutas" value="1" />
			        	<input type="hidden" name="anio" id="anio" value="0" />
                    	<input type="hidden" name="mes" id="mes" value="0" />

				    </td>
			      </tr>
				  <tr>
				    <td colspan="5" align="center"><label>
				      <input type="button" class="button" name="generar" id="generar" value="Generar Notificaciones de Consumo" onclick="Generar()"  />
				       <input type="button" onclick="return ValidarForm();" value="Imprimir" id="">
				    </label></td>
			      </tr>
				<tr><td colspan="5">&nbsp;</td></tr>
				</table>
			</fieldset>
		</td>
	</tr>

	 </tbody>

    </table>
 </form>
</div>
<script>
	$("#codsector").attr("disabled", true)
</script>
<?php
	include("../../../../include/validar_correlativo.php");
?>