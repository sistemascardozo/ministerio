<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
    
	$conexion->beginTransaction();
	
	$objFunciones = new clsFunciones();
	
	$codsuc 		= $_POST["codsuc"];
	$anio 			= $_POST["anio"];
	$mes 			= $_POST["mes"];
	$ciclo 			= $_POST["ciclo"];
	
	$correlativo = $objFunciones->setCorrelativosVarios(3, $codsuc, "SELECT", 0);
	
	$sql = "SELECT l.nroinscripcion, l.nronotificacion ";
	$sql = "FROM medicion.lecturas l 
			INNER JOIN catastro.clientes c ON(l.codemp = c.codemp AND l.codsuc = c.codsuc AND l.nroinscripcion=c.nroinscripcion)
			where l.codsuc=? and l.anio=? and l.mes=? and l.codciclo=? and l.codestlectura=1";
	$result = $conexion->prepare($sql);
	$result->execute(array($codsuc,$anio,$mes,$ciclo));
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error SELECT lecturas";
		die();
	}

	$items = $result->fetchAll();
	foreach($items as $row)
	{
		if($row["nronotificacion"]==0)
		{
			$upd = "update medicion.lecturas set nronotificacion=? where codsuc=? and anio=? and mes=? and codciclo=? and codestlectura=1 
					and nroinscripcion=?";
			
			$result = $conexion->prepare($upd);
			$result->execute(array($correlativo,$codsuc,$anio,$mes,$ciclo,$row["nroinscripcion"]));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update lecturas";
				die();
			}

			$correlativo++;
		}
	}

	$n = $objFunciones->setCorrelativosVarios(3,$codsuc,"UPDATE",$correlativo);

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		echo "<span class='icono-icon-x'></span> No se pudo Realizar la Transferencia de Datos";
	}else{
		$conexion->commit();
		echo "<span class='icono-icon-ok'></span>Notificaciones Generadas Correctamente";
	}
	

	
?>
