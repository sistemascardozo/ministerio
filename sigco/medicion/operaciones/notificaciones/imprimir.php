<?php 
	include("../../../../objetos/clsReporte.php");
        
	$fechac = date("d/m/Y");
				
	class clsNotificaciones extends clsReporte
	{
		function Header()
		{
			global $codsuc;
			
			$empresa = $this->datos_empresa($codsuc);
			
			$this->Rect(5,5,143,200);
			$this->Rect(150,5,142,200);
			
			$x=5;
			$h=5;
			$y=5;
			
			$tit1=$empresa["razonsocial"];
			$tit2="SUCURSAL: ".$empresa["descripcion"];
			$tit3="Ruc :".$empresa["ruc"];
			
			$this->SetXY($x+20,$y+4);
			$this->SetFont('Arial','',6);
			$this->Cell(85,$h-2,utf8_decode($tit1),0,1,'L');
			$this->SetX($x+20);
			$this->Cell(85, $h-2,utf8_decode($tit2),0,1,'L');
			$this->SetX($x+20);
			$this->Cell(90, $h-2,utf8_decode($tit3),0,0,'L');
			$this->Cell(10, $h-2,"Fecha",0,0,'R');
			$this->Cell(5, $h-2,":",0,0,'C');
			$this->Cell(15, $h-2,$this->FechaServer(),0,0,'L');
			
			$this->SetXY($x+165,$y+4);
			$this->SetFont('Arial','',6);
			$this->Cell(85,$h-2,utf8_decode($tit1),0,1,'L');
			$this->SetX($x+165);
			$this->Cell(85, $h-2,utf8_decode($tit2),0,1,'L');
			$this->SetX($x+165);
			$this->Cell(90, $h-2,utf8_decode($tit3),0,0,'L');
			$this->Cell(10, $h-2,"Fecha",0,0,'R');
			$this->Cell(5, $h-2,":",0,0,'C');
			$this->Cell(15, $h-2,$this->FechaServer(),0,0,'L');
			
			$this->SetX($x);
			$this->SetFont('Arial','B',14);
			
			$this->Image("../../../../images/logo_empresa.jpg",6,8,17,16);
			$this->Image("../../../../images/logo_empresa.jpg",151,8,17,16);
                        
			$this->Ln(10);
			$this->Cell(143, $h,"Aviso de Consumo",0,0,'C');
			
			$this->SetX($x+145);
			$this->Cell(142, $h,"Aviso de Consumo",0,1,'C');				
		}
		function Contenido($nronotificacion,$nronotificacion1,$propietario,$propietario1,$direccion,$direccion1,
						  $catetar,$catetar1,$actividad,$actividad1,$fechalectanterior,$fechalectanterior1,$lectanterior,$lectanterior1,
						  $ciclo,$ciclo1,$fechalectultima,$fechalectultima1,$lectultima,$lectultima1,
						  $nromed,$nromed1,$consumo,$consumo1,$nrodocumento,$nrodocumento1,$img1,$img2)
		{

			$x=5;
			$h=5;
			$y=5;
			
			$this->SetX($x);
			$this->SetFont('Arial','B',10);
                        
			if(!empty($img1)){$this->Image($img1,120,70,25,18);}
			
			if(!empty($img2)){$this->Image($img2,264,70,25,18);}
                                                
			$this->Cell(143, $h,utf8_decode("Nro. de Notificacion : ").$nronotificacion,0,0,'C');
			
			$this->Cell(2, $h,"",0,0,'C');
			$this->Cell(142, $h,utf8_decode("Nro. de Notificacion : ").$nronotificacion1,0,1,'C');
						
			$y=$this->GetY();
			$this->Rect($x+1,$y,141,165);
			$this->Rect($x+146,$y,140,165);
			
			$this->Ln(3);
			
			$this->SetFont('Arial','',7);
			
			$this->SetX($x+1);
			$this->Cell(30, $h,"Amigo Usuario Sr(a)",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(106, $h,$propietario,0,0,'L');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Amigo Usuario Sr(a)",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(105, $h,$propietario1,0,1,'L');
			
			$this->SetX($x+1);
			$this->Cell(30, $h,"Direccion",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(106, $h,$direccion,0,0,'L');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Direccion",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(106, $h,$direccion1,0,1,'L');
			
			$this->SetX($x+1);
			$this->Cell(30, $h,"Categoria",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$catetar,0,0,'L');
			$this->Cell(19, $h,"Periodo",1,0,'C');
			$this->Cell(19, $h,"Lecturas",1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Categoria",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$catetar1,0,0,'L');
			$this->Cell(19, $h,"Periodo",1,0,'C');
			$this->Cell(19, $h,"Lecturas",1,0,'C');
			$this->Cell(2, $h,"",0,1,'C');
			
			$this->SetX($x+1);
			$this->Cell(30, $h,"Catastro",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$actividad,0,0,'L');
			$this->Cell(19, $h,$fechalectanterior,1,0,'C');
			$this->Cell(19, $h,number_format($lectanterior,0),1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Catastro",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$actividad1,0,0,'L');
			$this->Cell(19, $h,$fechalectanterior1,1,0,'C');
			$this->Cell(19, $h,number_format($lectanterior1,0),1,0,'C');
			$this->Cell(2, $h,"",0,1,'C');
			
			$this->SetX($x+1);
			$this->Cell(30, $h,"Ciclo Facturacion",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$ciclo,0,0,'L');
			$this->Cell(19, $h,$fechalectultima,1,0,'C');
			$this->Cell(19, $h,number_format($lectultima,0),1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Ciclo Facturacion",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$ciclo1,0,0,'L');
			$this->Cell(19, $h,$fechalectultima1,1,0,'C');
			$this->Cell(19, $h,number_format($lectultima1,0),1,0,'C');
			$this->Cell(2, $h,"",0,1,'C');
			
			$this->SetX($x+1);
			$this->Cell(30, $h,"Medidor Nro.",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$nromed,0,0,'L');
			$this->Cell(19, $h,"Consumo",1,0,'R');
			$this->Cell(19, $h,number_format($consumo,0),1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Medidor Nro",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$nromed1,0,0,'L');
			$this->Cell(19, $h,"Consumo",1,0,'R');
			$this->Cell(19, $h,number_format($consumo1,0),1,0,'C');
			$this->Cell(2, $h,"",0,1,'C');
			
			$this->Ln(1);
			
			$this->SetX($x+1);
			$tit = "CUMPLIMOS EN COMUNICARLE QUE SU CONSUMO DE AGUA POTABLE EN EL ";
			$this->Cell(141, $h,$tit,0,0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,$tit,0,1,'L');
			
			$this->SetX($x+1);
			$tit = "PRESENTE MES A EXPERIMENTADO INCREMENTO CON RELACION A LOS MESES ";
			$this->Cell(141, $h,$tit,0,0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,$tit,0,1,'L');
			
			$this->SetX($x+1);
			$tit  = "ANTERIORES HABIENDO ENCONTRADO LAS SIGUIENTES OBSERVACIONES :";
			$this->Cell(141, $h,$tit,0,0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,$tit,0,1,'L');

            $this->Ln(2);
			$this->SetX($x+1);
			$this->Cell(141, $h,"",'B',0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,"",'B',1,'L');
			
            $this->Ln(4);
            $this->SetX($x+1);
			$this->Cell(30, $h,"Amigo Usuario Sr(a)",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(106, $h,$propietario,0,0,'L');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Amigo Usuario Sr(a)",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(105, $h,$propietario1,0,1,'L');
                        
            $this->SetX($x+1);
			$this->Cell(30, $h,"Direccion",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$direccion,0,0,'L');
            $this->Cell(19, $h,"Periodo",1,0,'C');
			$this->Cell(19, $h,"Lecturas",1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');
                        
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Direccion",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$direccion1,0,0,'L');
            $this->Cell(19, $h,"Periodo",1,0,'C');
			$this->Cell(19, $h,"Lecturas",1,0,'C');
			$this->Cell(2, $h,"",0,1,'C');
                        
            $this->SetX($x+1);
            $this->Cell(30, $h,"Categoria",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$catetar,0,0,'L');
			$this->Cell(19, $h,$fechalectanterior,1,0,'C');
			$this->Cell(19, $h,number_format($lectanterior,0),1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Categoria",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$catetar1,0,0,'L');
            $this->Cell(19, $h,$fechalectanterior1,1,0,'C');
			$this->Cell(19, $h,number_format($lectanterior1,0),1,0,'C');
			$this->Cell(2, $h,"",0,1,'C');
                        
            $this->SetX($x+1);
            $this->Cell(30, $h,"Catastro",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$actividad,0,0,'L');
            $this->Cell(19, $h,$fechalectultima,1,0,'C');
			$this->Cell(19, $h,number_format($lectultima,0),1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');

			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Catastro",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$actividad1,0,0,'L');
            $this->Cell(19, $h,$fechalectultima1,1,0,'C');
			$this->Cell(19, $h,number_format($lectultima1,0),1,0,'C');
			$this->Cell(2, $h,"",0,1,'C');
                        
            $this->SetX($x+1);
			$this->Cell(30, $h,"Medidor Nro.",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$nromed,0,0,'L');
            $this->Cell(19, $h,"Consumo",1,0,'R');
			$this->Cell(19, $h,number_format($consumo,0),1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');
			
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(30, $h,"Medidor Nro",0,0,'L');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(66, $h,$nromed1,0,0,'L');
			$this->Cell(19, $h,"Consumo",1,0,'R');
			$this->Cell(19, $h,number_format($consumo1,0),1,0,'C');
			$this->Cell(2, $h,"",0,0,'C');
                        
            $this->Ln(2);
			$this->SetX($x+1);
			$this->Cell(141, $h,"",'B',0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,"",'B',1,'L');
			
			$this->SetX($x+1);
			$this->Cell(141, $h,"",'B',0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,"",'B',1,'L');
			
			$this->SetX($x+1);
			$this->Cell(141, $h,"",'B',0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,"",'B',1,'L');
			
			$this->SetX($x+1);
			$this->Cell(141, $h,"",'B',0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,"",'B',1,'L');
			
			$this->SetX($x+1);
			$this->Cell(141, $h,"",'B',0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,"",'B',1,'L');
			
            $this->SetX($x+1);
			$this->Cell(141, $h,"",'B',0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,"",'B',1,'L');
			
			$this->SetX($x+1);
			$this->Cell(141, $h,"",'B',0,'L');
			$this->Cell(4, $h,"",0,0,'C');
			$this->Cell(140, $h,"",'B',1,'L');

            $this->Ln(4);
			
			$this->SetX($x+5);
			$this->Cell(50, $h,"",'B',0,'L');
			$this->Cell(15, $h,"",0,0,'L');
			$this->Cell(25, $h,"Fecha de Entrega",0,0,'L');
			$this->Cell(45, $h,"",1,0,'L');
			$this->Cell(2, $h,"",0,0,'L');

			$this->Cell(9, $h,"",0,0,'C');
			$this->Cell(50, $h,"",'B',0,'L');
			$this->Cell(15, $h,"",0,0,'L');
			$this->Cell(25, $h,"Fecha de Entrega",0,0,'L');
			$this->Cell(42, $h,"",1,1,'L');
			
			$this->Cell(50, $h,$propietario,0,0,'C');
			$this->Cell(15, $h,"",0,0,'L');
			$this->Cell(25, $h,"Hora de Entrega",0,0,'L');
			$this->Cell(45, $h,"",1,0,'L');
			$this->Cell(2, $h,"",0,0,'L');
			
			$this->Cell(9, $h,"",0,0,'C');
			$this->Cell(50, $h,$propietario1,0,0,'C');
			$this->Cell(15, $h,"",0,0,'L');
			$this->Cell(25, $h,"Hora de Entrega",0,0,'L');
			$this->Cell(42, $h,"",1,1,'L');
			
			$this->Cell(50, $h,$nrodocumento,0,0,'C');
			$this->Cell(15, $h,"",0,0,'L');
			$this->Cell(25, $h,"Inspector",0,0,'L');
			$this->Cell(45, $h,"",1,0,'L');
			$this->Cell(2, $h,"",0,0,'L');
			
			$this->Cell(9, $h,"",0,0,'C');
			$this->Cell(50, $h,$nrodocumento1,0,0,'C');
			$this->Cell(15, $h,"",0,0,'L');
			$this->Cell(25, $h,"Inspector",0,0,'L');
			$this->Cell(42, $h,"",1,1,'L');
						
			$this->Ln(4);
			$this->SetFont('Arial','',6);
			
			$tit  = "Recomendamos mantener siempre sus servicios en buen estado y en horas de corte de Suministro a la poblacion ";
			$tit .= "tener todos los caños de sus servicios completamente cerrado.";
			$tit .= "El cumplimiento de esta accion le ayudara a cuidar su economia y la empresa tendra agua para mas usuarios";
			$this->SetX($x+1);
			$y=$this->GetY();
			$this->MultiCell(130,$h-2,utf8_decode($tit),0,'J');
			
			$this->SetXY($x+146,$y);
			$this->MultiCell(130,$h-2,utf8_decode($tit),0,'J');
                        
            $this->Ln(3);
			
		}
		function Footer()
		{					 
		}
    }
	
	$codsuc			= $_GET["codsuc"];
	$anio			= $_GET["anio"];
	$mes			= $_GET["mes"];
	$ciclo			= $_GET["ciclo"];

    if($_GET["sector"]!="%") $cSector=" AND clie.codsector=".$_GET["sector"];
	else $cSector="";

	if($_GET["ruta"]!="%") $cRuta=" AND clie.codrutlecturas=".$_GET["ruta"];
	else $cRuta="";

	$objReporte	= new clsNotificaciones('L');
	
	$pagina	= 1;
	$c=1;
	$not1 	= "1";
	$not2 	= "1";
	
	$sql  = "select lect.nronotificacion,upper(clie.propietario),lect.nroinscripcion,clie.codcliente,";
	$sql .= "tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,";
	$sql .= "tipact.descripcion,lect.nromed,lect.lecturaanterior,lect.lecturaultima,lect.consumo,";
	$sql .= "lect.fechalectanterior,lect.fechalectultima,ci.descripcion,clie.nrodocumento,tar.nomtar,".$objReporte->getCodCatastral("clie.");
	$sql .= " from medicion.lecturas as lect ";
	$sql .= "inner join catastro.clientes as clie on(lect.codemp=clie.codemp and lect.codsuc=clie.codsuc and ";
	$sql .= "lect.nroinscripcion=clie.nroinscripcion) ";
	$sql .= "inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona  ) ";
	$sql .= "inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) ";
	$sql .= "inner join public.tipoactividad as tipact on(clie.codtipoactividad=tipact.codtipoactividad) ";
	$sql .= "inner join public.rutaslecturas as rlect on(clie.codemp=rlect.codemp and clie.codsuc=rlect.codsuc and clie.codsector=rlect.codsector ";
	$sql .= "and clie.codrutlecturas=rlect.codrutlecturas and clie.codmanzanas=rlect.codmanzanas) ";
	$sql .= "inner join public.rutasmaelecturas as rtmae on(rlect.codemp=rtmae.codemp and rlect.codsuc=rtmae.codsuc and rlect.codsector=rtmae.codsector ";
	$sql .= "and rlect.codrutlecturas=rtmae.codrutlecturas) ";
	$sql .= "inner join facturacion.ciclos as ci on(lect.codciclo=ci.codciclo) 
			 inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)";
	$sql .= "where lect.codestlectura=1 and lect.codsuc=:codsuc and lect.anio=:anio and lect.mes=:mes ";
	$sql .= "and lect.codciclo=:codciclo ".$cSector.$cRuta;
	$sql .= " order by lect.nronotificacion";
	
	$result = $conexion->prepare($sql);
	$result->execute(array(":codsuc"=>$codsuc,
							 ":anio"=>$anio,
							 ":mes"=>$mes,
							 ":codciclo"=>$ciclo						 
							));
	//$n 		= $result->rowCount();
	$items 	= $result->fetchAll();
	$n = count($items);

	foreach($items as $row)
	{
		
		$tarifas = $row["nomtar"];
		
		if($pagina==1)
		{
			$not1 	= $row[0];
			$pro1	= $row[1];
			$dir1	= $row[4];
			$cat1	= $tarifas;
			$act1	= $row["codcatastro"];
			$fante1	= $objReporte->DecFecha($row[10]);
			$lante1	= $row[7];
			$ciclo1	= $row[12];
			$fulti1	= $objReporte->DecFecha($row[11]);
			$lulti1	= $row[8];
			$nromed1= $row[6];
			$cons1	= $row[9];
			$nrodoc1= $row[13];
            $img1   = $objReporte->generargraficos($row["nroinscripcion"],$codsuc);
		}
		if($pagina==2)
		{
			$not2 	= $row[0];
			$pro2	= $row[1];
			$dir2	= $row[4];
			$cat2	= $tarifas;
			$act2	= $row["codcatastro"];
			$fante2	= $objReporte->DecFecha($row[10]);
			$lante2	= $row[7];
			$ciclo2	= $row[12];
			$fulti2	= $objReporte->DecFecha($row[11]);
			$lulti2	= $row[8];
			$nromed2= $row[6];
			$cons2	= $row[9];
			$nrodoc2= $row[13];
            $img2   = $objReporte->generargraficos($row["nroinscripcion"],$codsuc);
			
			$objReporte->AddPage();
			$objReporte->Contenido($not1,$not2,$pro1,$pro2,$dir1,$dir2,$cat1,$cat2,$act1,$act2,$fante1,$fante2,
			$lante1,$lante2,$ciclo1,$ciclo2,$fulti1,$fulti2,$lulti1,$lulti2,$nromed1,$nromed2,$cons1,$cons2,$nrodoc1,$nrodoc2,$img1,$img2);
			
			$not1 		= "";
			$not2 		= "";
			$pro1		= "";
			$pro2		= "";
			$dir1		= "";
			$dir2		= "";
			$cat1		= "";
			$cat2		= "";
			$act1 		= "";
			$act2		= "";
			$fante1		= "";
			$fante2		= "";
			$lante1		= "";
			$lante2		= "";
			$ciclo1		= "";
			$ciclo2		= "";
			$fulti1 	= "";
			$fulti2		= "";
			$lulti1		= "";
			$lulti2		= "";
			$nromed1	= "";
			$nromed2	= "";
			$cons1		= "";
			$cons2		= "";
			$nrodoc1	= "";
			$img1       = "";
            $img2       = "";
                        
			$pagina=0;
		}
		
		if($c==$n && ($n%2)!=0)
		{
			$objReporte->AddPage();
			$objReporte->Contenido($not1,$not2,$pro1,$pro2,$dir1,$dir2,$cat1,$cat2,$act1,$act2,$fante1,$fante2,
			$lante1,$lante2,$ciclo1,$ciclo2,$fulti1,$fulti2,$lulti1,$lulti2,$nromed1,$nromed2,$cons1,$cons2,$nrodoc1,$nrodoc2,$img1,$img2);
			
			$not1 		= "";
			$not2 		= "";
			$pro1		= "";
			$pro2		= "";
			$dir1		= "";
			$dir2		= "";
			$cat1		= "";
			$cat2		= "";
			$act1 		= "";
			$act2		= "";
			$fante1		= "";
			$fante2		= "";
			$lante1		= "";
			$lante2		= "";
			$ciclo1		= "";
			$ciclo2		= "";
			$fulti1 	= "";
			$fulti2		= "";
			$lulti1		= "";
			$lulti2		= "";
			$nromed1	= "";
			$nromed2	= "";
			$cons1		= "";
			$cons2		= "";
			$nrodoc1	= "";
			$nrodoc2	= "";
			
			$pagina=0;
		}
		
		$pagina=$pagina+1;
		$c=$c+1;

	}
	$objReporte->Output();	
        
        
		
?>