<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	$Sede = $_SESSION['Sede'];
	
	set_time_limit(0);
	
	include("../../../../objetos/clsFunciones.php");

	$objFunciones = new clsFunciones();
	
	file_put_contents('progreso.txt', '0|Calculando...');
	
	$codemp		= 1;
	$codsuc 	= $_POST["codsuc"];
	$codciclo	= $_POST["codciclo"];
	$anio		= $_POST["anio"];
	$mes		= $_POST["mes"];
	
	$cortado	= $_POST["cortado"];
	//$cortado	= $_POST["todoscortados"];
	$fapertura	= $objFunciones->CodFecha($_POST["fapertura"]);
	$digitadas	= $_POST["digitadas"];
	//$digitadas	= $_POST["lectdigitadas"];
	$apertura	= $_POST["apertura"];
	$paramae	= $objFunciones->getParamae("LIMLEC", $codsuc);	
	$idusuario	= $_SESSION['id_user'];
	//$facturacion 	= $objFunciones->datosfacturacion($codsuc);
	
/*	$anio = $facturacion["anio"];
	$mes = $facturacion["mes"];*/

	//$sector		= "%".$_POST["sector"]."%";
	if($_POST["sector"] != "%")
	{
		$cSector = " AND codsector = ".$_POST["sector"];
	}
	else
	{
		$cSector = "";
	}
	
	//die($cSector);
	
	$activo 	= 1;
	$cortadox 	= 1;
	
	$facturacion = $objFunciones->datosfacturacion($codsuc, $codciclo);

	if($cortado == 1){$cortadox = 2;}
	if($digitadas == 1){$digitadas = " AND digitado = 0 ";}else{$digitadas = "";}
	
	$conexion->beginTransaction();
	//SE VALIDARA Q NO SE ELIMINE SI TIENE LECTURAS ASIGNADASS
	$sql = "DELETE FROM medicion.lecturas ";
	$sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND anio = '".$anio."' AND mes = '".$mes."' AND codciclo = ".$codciclo." ".$cSector.$digitadas;
	
	$result = $conexion->prepare($sql);
	$result->execute(array());
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error DELETE lecturas 56 : ".$sql;

		die($mensaje);
	}
	if($apertura != 0)
	{
		/*$nrofacturacion = $facturacion["nrofacturacion"] - 1;
		
		$sqlF = "select anio,mes from facturacion.periodofacturacion where codemp=1 and codsuc=? and nrofacturacion=? and codciclo=?";
		
		$result = $conexion->prepare($sqlF);
		$result->execute(array($codsuc,$nrofacturacion,$codciclo));
		$itemsF = $result->fetch();
	
		$sqlL = "select lecturaultima,fechalectultima,lecturaanterior,fechalectanterior,consumo,lecturapromedio,nroinscripcion 
				from medicion.lecturas 
				where codemp=1 and codsuc=? and codciclo=? and anio=? and mes=? ".$digitadas;
		
		$result = $conexion->prepare($sqlL);
		$result->execute(array($codsuc,$codciclo,$itemsF["anio"],$itemsF["mes"]));
		$itemsL = $result->fetchAll();
		
		foreach($itemsL as $rowL)
		{
			$updC = "update catastro.clientes set lecturaultima=?,fechalecturaultima=?,lecturaanterior=?,fechalecturaanterior=?,consumo=?
					where codemp=1 and codsuc=? and nroinscripcion=? ".$digitadas;
			
			$result = $conexion->prepare($updC);
			$result->execute(array($rowL["lecturaultima"],
									$rowL["fechalectultima"],
									$rowL["lecturaanterior"],
									$rowL["fechalectanterior"],
									$rowL["consumo"],
									$codsuc,
									$rowL["nroinscripcion"]));
		}
		*/
	}
	//ACTUALIZAR FECHA SIN EXCEPCION
	/*$updClientes = "update catastro.clientes 
					set fechalecturaanterior=fechalecturaultima,
					fechalecturaultima=:fechalecturaultima
					where codemp=:codemp and codsuc=:codsuc and codciclo=:codciclo
					";
		
	$result = $conexion->prepare($updClientes);
	$result->execute(array(		":fechalecturaultima"=>$fapertura,
								":codemp"=>$codemp,
								":codsuc"=>$codsuc,
								":codciclo"=>$codciclo));
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error UPDATE CLIENTES";
		die();
	}*/
	//ACTUALIZAR FECHA SIN EXCEPCION		
	$sqlLectura = "SELECT c.nroinscripcion, c.nromed, c.tipofacturacion, c.lecturaultima, c.fechalecturaultima, c.codsector, conx.codestadomedidor, ";
	$sqlLectura .= " c.consumo, c.lecturapromedio, c.lecturaanterior, c.fechalecturaanterior, c.codestadoservicio, c.lecturaestimada, c.codrutlecturas ";
	$sqlLectura .= "FROM catastro.clientes AS c ";
	$sqlLectura .= " INNER JOIN catastro.conexiones AS conx on(c.codemp = conx.codemp AND c.codsuc = conx.codsuc AND c.nroinscripcion = conx.nroinscripcion) ";
	$sqlLectura .= "WHERE c.codemp = 1 AND c.codsuc = ".$codsuc." AND c.codciclo = ".$codciclo." ";
	$sqlLectura .= " AND ((c.tipofacturacion = 0 OR c.tipofacturacion = 1) OR (c.tipofacturacion = 2 AND TRIM(c.nromed) <> '')) ";
	$sqlLectura .= " AND (c.codestadoservicio = ".$activo." OR c.codestadoservicio = ".$cortadox.") ".$cSector.$digitadas;

	$result = $conexion->prepare($sqlLectura);
	$result->execute(array());
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error select clientes 129 : ".$sqlLectura;
		
		die($mensaje);
	}

	$itemsLectura = $result->fetchAll();
	////////////7
	$limit = "limit ".$paramae["valor"];

	//////////////
	$totalAvance = count($itemsLectura);
	$actualAvance = 0;
	
	foreach($itemsLectura as $rowLect)
	{
		$lectPromedio = 0;
		$consPromedio = 0;
		
		if($rowLect["tipofacturacion"] == 0)
		{
			$consumo = 0;
			
			$sqlPromedio  = "SELECT * FROM facturacion.promedio_lectura(".$codsuc.", ".$rowLect["nroinscripcion"].", 6); "; 
   		  	$result = $conexion->prepare($sqlPromedio);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error SELECT consumo 164 : ".$sqlPromedio;
				
				die($mensaje);
			}
			
			$itemsPromedio = $result->fetch();
			
			$lectPromedio = $itemsPromedio[0];

			if($lectPromedio == '')		
			{
				$lectPromedio = 0;
			}
		}
		else
		{
			$lectPromedio = $rowLect["lecturapromedio"];
			$consumo = $rowLect["consumo"];
		}
		//$consumo = $rowLect["consumo"];
		
		
		if($apertura == 0)
		{
			$sql = "DELETE FROM medicion.lecturas ";
			$sql .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." ";
			$sql .= " AND anio = '".$anio."' AND mes = '".$mes."' AND codciclo = ".$codciclo." AND nroinscripcion = ".$rowLect["nroinscripcion"];
				   
			$result = $conexion->query($sql);	
				 
			if(!$result)
			{
				$conexion->rollBack();
				$mensaje = "Error DELETE lecturas 197 : ".$sql;
				
				die($mensaje);
			}
			
			//------------Agregado por Mauro ---13/09/2015-------------//
			//LOS USUARIOS CON LECTURA ULTIMA CERO
			//PASAR SU LECTURA ESTIMADA COMO ANTERIOR
			if($rowLect["lecturaultima"] == '' || $rowLect["lecturaultima"] == 0)
			{
				$rowLect["lecturaultima"] = $rowLect["lecturaestimada"];
			}

			$instLectura = "INSERT INTO medicion.lecturas(codemp, codsuc, anio, mes, codciclo, nroinscripcion, ";
			$instLectura .= " nromed, tipofacturacion, lecturaanterior, fechalectanterior, ";
			$instLectura .= " creador, codsector, codestadomedidor, lecturapromedio, fechareg, fechalectultima, ";
			$instLectura .= " consumo, codestadoservicio, codrutlecturas) ";
			$instLectura .= "VALUES(".$codemp.", ".$codsuc.", '".$anio."', '".$mes."', ".$codciclo.", ".$rowLect["nroinscripcion"].", ";
			$instLectura .= " '".$rowLect["nromed"]."', ".$rowLect["tipofacturacion"].", ".$rowLect["lecturaultima"].", '".$rowLect["fechalecturaultima"]."', ";
			$instLectura .= " ".$idusuario.", ".$rowLect["codsector"].", 0, ".$lectPromedio.", '".$fapertura."', '".$fapertura."', ";
			$instLectura .= " ".$consumo.", ".$rowLect["codestadoservicio"].", ".$rowLect["codrutlecturas"].")";
			
			//echo $instLectura."<br>";
			
			$result = $conexion->prepare($instLectura);
			$result->execute(array());
			
			if($result->errorCode() != '00000')
			{
				$conexion->rollBack();
				$mensaje = "Error INSERT lecturas 226 : ".$instLectura;
				var_dump($result->errorInfo());
				
				die($mensaje);
			}
			
			if($rowLect["tipofacturacion"] == 0)
			{
				$consumoSql = ", lecturaanterior='".$rowLect["lecturaultima"]."', lecturaultima = 0, consumo = '".$consumo."', lecturapromedio = '".$lectPromedio."' ";
			}
			
			$updClientes = "UPDATE catastro.clientes ";
			$updClientes .= "SET fechalecturaanterior='".$rowLect["fechalecturaultima"]."', ";
			$updClientes .= " fechalecturaultima = '".$fapertura."' ".$consumoSql." ";
			$updClientes .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND codciclo = ".$codciclo." AND nroinscripcion = ".$rowLect["nroinscripcion"]." ";
		
			$result = $conexion->prepare($updClientes);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error UPDATE CLIENTES 249 : ".$updClientes;
				
				die($mensaje);
			}
		}
		else
		{
			$sql = "DELETE FROM medicion.lecturas ";
			$sql .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." ";
			$sql .= " AND anio = '".$anio."' AND mes = '".$mes."' AND codciclo = ".$codciclo." AND nroinscripcion = ".$rowLect["nroinscripcion"]." ";
			
			$result = $conexion->query($sql);
				 
			if(!$result)
			{
				$conexion->rollBack();
				$mensaje = "Error DELETE lecturas 266 : ".$sql;
				
				die($mensaje);
			}
			
			$instLectura = "INSERT INTO medicion.lecturas(codemp, codsuc, anio, mes, codciclo, nroinscripcion, ";
			$instLectura .= " nromed, tipofacturacion, lecturaanterior, fechalectanterior, ";
			$instLectura .= " creador, codsector, codestadomedidor, lecturapromedio, fechareg, fechalectultima, ";
			$instLectura .= " consumo, codestadoservicio, codrutlecturas) ";
			$instLectura .= "VALUES(".$codemp.", ".$codsuc.", '".$anio."', '".$mes."', ".$codciclo.", ".$rowLect["nroinscripcion"].", ";
			$instLectura .= " '".$rowLect["nromed"]."', ".$rowLect["tipofacturacion"].", ".$rowLect["lecturaanterior"].", '".$rowLect["fechalecturaultima"]."', ";
			$instLectura .= " ".$idusuario.", ".$rowLect["codsector"].", 0, ".$lectPromedio.", '".$fapertura."', '".$fapertura."', ";
			$instLectura .= " ".$consumo.", ".$rowLect["codestadoservicio"].", ".$rowLect["codrutlecturas"].")";
			
			//echo $instLectura."<br>";
			
			$result = $conexion->prepare($instLectura);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error INSERT lecturas 287 : ".$instLectura;
				
				die($mensaje);
			}
			
			$consumoSql = '';
			
			if($rowLect["tipofacturacion"] == 0)
			{
				$consumoSql = ", lecturaanterior='".$rowLect["lecturaanterior"]."', lecturaultima = 0, consumo = '".$consumo."', lecturapromedio = '".$lectPromedio."' ";
			}
			
			$updClientes = "UPDATE catastro.clientes ";
			$updClientes .= "SET fechalecturaanterior='".$rowLect["fechalecturaanterior"]."', ";
			$updClientes .= " fechalecturaultima = '".$fapertura."' ".$consumoSql." ";
			$updClientes .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND codciclo = ".$codciclo." AND nroinscripcion = ".$rowLect["nroinscripcion"]." ";
		
			$result = $conexion->prepare($updClientes);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error UPDATE CLIENTES 310 : ".$updClientes;
				
				die($mensaje);
			}
		
		}
		//var_dump($resultClientes->errorInfo())."<br>";
		////////////
		$actualAvance++;
		$porcentajeAvance = round($actualAvance * 100 / $totalAvance);
	    $textoAvance = $porcentajeAvance."|".$actualAvance." de ".$totalAvance;
		
	    file_put_contents('progreso.txt', $textoAvance);
	}
	
	//$conexion->rollBack();
	$textoAvance = "100|".$totalAvance." de ".$totalAvance;
	
	file_put_contents('progreso.txt', $textoAvance);
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		echo "<span class='icono-icon-x'></span> No se pudo Realizar la Apertura";
	}else{
		$conexion->commit();
		echo "<span class='icono-icon-ok'></span>Las Lecturas se han Aperturado Correctamente";
	}

?>