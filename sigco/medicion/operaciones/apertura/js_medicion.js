// JavaScript Document
$(function() {
	 $("#dialogo_barras").dialog({
        autoOpen:false,
        modal: true,
        closeOnEscape: false,
        title:"Aperturando Lecturas. Espere por Favor",
        height:130,
        open: function(event, ui) {
            //ocultar el boton de cerrar en el dialogo.
            $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
        }
    });
	 //preparación de la barra de progreso
    $("#barra").progressbar({
        value:0
    });
    
	$( "#fapertura" ).datepicker(
	{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true
	});
});
jQuery(function($)
{ 
	$("#fapertura").mask("99/99/9999");
});
function quitar_disabled(obj, cmb)
{
	if(obj.checked)
	{
		$("#" + cmb).attr("disabled", true)
	}
	else
	{
		$("#" + cmb).attr("disabled", false)
	}
}
function validar_lecturas(codsuc, codciclo)
{
	$.ajax({
		 url:'validar_lecturas.php',
		 type:'POST',
		 async:true,
		 data:'codsuc=' + codsuc + '&codciclo=' + codciclo + '&sector=' + $("#codsector").val(),
		 success:function(datos){
			$("#con_lecturas").val(datos)
		 }
	}) 
}
function datos_facturacion(codciclo)
{
	$.ajax({
		 url:'../../../../ajax/periodo_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+codciclo+'&codsuc='+codsuc,
		 success:function(datos){
			var r=datos.split("|")

			$("#anioperdio").val(r[2]+"-"+r[1])
			$("#anio").val(r[1])
			$("#mes").val(r[3])

		 }
	}) 
}