<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "APERTURA DE LECTURAS";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc 	= $_SESSION['IdSucursal'];

	$objDrop 	= new clsDrop();

	$sucursal = $objDrop->setSucursales(" WHERE codemp=1 AND codsuc=?", $codsuc);

	$Fecha = date('d/m/Y');
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_medicion.js"></script>
<script>
	var c = 0
	var codsuc	= <?=$codsuc?>;
	var Param =''
	
	function ValidarForm(Op)
	{
		if($("#ciclo").val() == 0)
		{
			Msj($("#ciclo"), 'Seleccione Ciclo');
			
			return false;
		}
		
		if($("#todosectores").val() == 0)
		{
			sector = $("#codsector").val();
			
			if(sector == 0)
			{
				Msj($("#codsector"), 'Seleccione Sector');
				
				return false;
			}
		}
		else
		{
			sector = "%";
		}
		
		if($("#con_lecturas").val() != 0)
		{
			Msj($("#codsector"), 'Las Lecturas ya fueron generadas');
			
			//return false;
			
			var r = confirm("Las Lecturas ya fueron generadas. Desea Volver a Generarlas?");
			
			if(r == false)
			{
				return false;
			}
		}
		
		Param = sector;
		
		return true;
	}
	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	
	function Generar(codsector)
	{		
		$("#div_guardar").html('<span class="icono-icon-loading"></span>Aperturando...')
		$("#dialogo_barras").dialog('open');
		
		$.ajax({
			 url:'guardar.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&codciclo=' + $("#ciclo").val() + '&anio=' + $("#anio").val() + '&mes=' + $("#mes").val() + '&sector=' + codsector +
				  '&cortado=' + $("#todoscortados").val() + '&fapertura=' + $("#fapertura").val() + "&digitadas=" + $("#lectdigitadas").val() +
				  "&apertura=" + $("#con_lecturas").val(),
			 success:function(datos){
				$("#dialogo_barras").dialog('close');
				$("#barra").progressbar({value: 0}); //actualizar la barra.
				$("#titulo_barra").html('Calculando...'); //actualizar etiqueta. 
				$("#div_guardar").html(datos);
				
				validar_lecturas(<?=$codsuc?>, $("#ciclo").val());//, $("#anio").val(), $("#mes").val()
			 }
		});
		
		timerBarra = setInterval(actualizarBarra, 1000);
	}
	
	function actualizarBarra()
	{
		//leer el archivo de texto que contiene el procentaje y avance del proceso principal.
		$.ajax({
			url: 'progreso.txt',
			type: 'POST',
			async: true,
			data: '',
			success: function (texto) {
				var partes	= texto.split('|'), 
				porcentaje	= parseInt(partes[0]),
				avance		= partes[1],
				etiqueta	= partes[0] + "% (" + partes[1] + ")";
			
				if (porcentaje == 100) //llegamos al 100% fin de proceso.
				{
					clearInterval(timerBarra); //borrar timer.
				}
		 
				$("#barra").progressbar({value: porcentaje}); //actualizar la barra.
				$("#titulo_barra").html(etiqueta); //actualizar etiqueta.
			}
		});
	}
	
function Backup()
{
	if(ValidarForm())
	{
		$.blockUI({ message: '<div><center><span class="icono-icon-loading"></span>Generando Backup...Espere un Momento por Favor...</center></div>',
			css: { 
				border: 'none', 
				padding: '15px', 
				backgroundColor: '#000', 
				'-webkit-border-radius': '10px', 
				'-moz-border-radius': '10px', 
				opacity: .5, 
				color: '#fff' 
			}
		}); 
		
		$.ajax({
			url:'Backup.php',
			type:'POST',
			async:true,
			data:'codsuc=' + codsuc,
			success:function(data){
				if (parseInt(data)==0)
				{
					window.parent.OperMensaje("Error al Generar el Backup", 2);	
				}
				else
				{
					window.parent.OperMensaje("Backup Generado Correctamente", 1);
					
					Msj($("#btnAp"), 'Backup Generado Correctamente');
					Generar(Param);
					//location.href=data;
				}
					//Generar(Param);
				$.unblockUI();
			}
		})
	}
}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="guardar.php" enctype="multipart/form-data">
 <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">

   <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="80">Sucursal</td>
				    <td width="30" align="center">:</td>
				    <td width="100"><label>
				    	<input type="hidden" name="codsuc" id="codsuc" value="<?=$codsuc?>" />
				      <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </label></td>
				    <td width="80" align="right">Ciclo</td>
				    <td width="30" align="center">:</td>
				    <td width="53%">
                    	<? $objDrop->drop_ciclos($codsuc, 0, "onchange='datos_facturacion(this.value); validar_lecturas(".$codsuc.", this.value);'"); ?>
                    </td>
				  </tr>
				  <tr>
				    <td>Mes y A&ntilde;o </td>
				    <td align="center">:</td>
				    <td><input type="text" name="anioperdio" id="anioperdio" value="" class="inputtext" readonly="readonly" /></td>
				    <td align="right">Sector</td>
				    <td align="center">:</td>
				    <td>
				      	<?php echo $objDrop->drop_sectores2($codsuc); ?>
                        <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosectores'); quitar_disabled(this, 'codsector');" />
			        	Todos los Sectores
                    </td>
			      </tr>
				  <tr>
				    <td>Apertura</td>
				    <td align="center">:</td>
				    <td><input type="text" name="fapertura" id="fapertura" value="<?=date("d/m/Y");?>" class="inputtext" style="width:80px;" /></td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
                  <tr>
				    <td>&nbsp;</td>
				    <td align="center"><input type="checkbox" name="cortados" id="cortados" value="checkbox" onclick="CambiarEstado(this,'todoscortados');" /></td>
				    <td colspan="2">Incluye a Servicios Cortados</td>
				    <td align="center">&nbsp;</td>
				    <td><input type="hidden" name="anio" id="anio" value="" />
			        <input type="hidden" name="mes" id="mes" value="" /></td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">
                    	<input type="checkbox" name="digitados" id="digitados" value="checkbox" onclick="CambiarEstado(this, 'lectdigitadas');" checked="checked" />
                    </td>
				    <td colspan="4">No Borrar las Lecturas que ya Fueron Digitadas</td>
			      </tr>
				  <tr>
				    <td colspan="6" align="center">
				    	<input type="hidden" name="todoscortados" id="todoscortados" value="0" />
				    	<input type="hidden" name="lectdigitadas" id="lectdigitadas" value="1" />
				    	<input type="hidden" name="todosectores" id="todosectores" value="1" />
			        	<input type="hidden" name="con_lecturas" id="con_lecturas" value="0" />
				    	<div id="div_guardar">&nbsp;</div><div id="sql"></div></td>
			      </tr>
				  <tr>
				    <td colspan="6" align="center">
				    	
				     <input type="button" onclick="Backup();" value="Aperturar" id="btnAp"></td>
			      </tr>
			      <tr><td colspan="2">&nbsp;</td> </tr>
				</table>
			</fieldset>
		</td>
	</tr>

    </tbody>
    </table>
    <div id="dialogo_barras">
    <span id="titulo_barra">0 %</span>
    <div id="barra"> </div>
  </div>
 </form>
</div>
<script>
	$("#codzona").attr("disabled", true);
	$("#codsector").attr("disabled", true);
	
	validar_lecturas(<?=$codsuc?>, $("#ciclo").val());
	
	$("#ciclo").change(function() {
 		validar_lecturas(<?=$codsuc?>, $("#ciclo").val());
	});
	$("#codzona").change(function() {
		validar_lecturas(<?=$codsuc?>, $("#ciclo").val());
	});
	$("#codsector").change(function() {
		validar_lecturas(<?=$codsuc?>, $("#ciclo").val());
	});
</script>
<?php
	CuerpoInferior();
?>