<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "DIGITACION DE LECTURAS";
	$Activo = 1;

	CuerpoSuperior($TituloVentana);

	$codsuc 	= $_SESSION['IdSucursal'];
	$idusuario	= $_SESSION['id_user'];

	$objDrop 	= new clsDrop();

	$sucursal = $objDrop->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);

	$Fecha = date('d/m/Y');

	$facturacion = $objDrop->datosfacturacion($codsuc, $_POST["ciclo"]);

    if($_POST["todosectores"] == 1)
	{
		$sectores = "todos los sectores";
	}
	else
	{
		$sectores = $_POST["sector_text"];
	}

	if($_POST["rutaslecturas"] == '%')
	{
		$rutas = "todas las rutas";
	}
	else
	{
		$rutas = $_POST["rutas_text"];
	}

	$sucursal = $objDrop->setSucursales(" WHERE codemp = 1 AND codsuc = ? ", $codsuc);

	if($_POST["orden"] == 1)
	{
		$order = " ORDER BY c.codsuc, c.codsector, CAST(c.codrutlecturas AS INTEGER), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER) ";
	}
	else
	{
		$order = " ORDER BY c.codsuc, c.codsector, CAST(c.codrutlecturas AS INTEGER), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER) ";
	}

	if($_POST["codsector"] != "%")
	{
		$cSector = " AND c.codsector = ".$_POST["codsector"];
	}
	else
	{
		$cSector = "";
	}

	if($_POST["rutaslecturas"] != "%")
	{
		$cRuta = " AND c.codrutlecturas = ".$_POST["rutaslecturas"];
	}
	else
	{
		$cRuta = "";
	}

	$sqlLecturas = "SELECT l.nroinscripcion, c.propietario, tc.descripcioncorta, cl.descripcion AS calle, c.nrocalle, c.nromed, ";
	$sqlLecturas .= " l.lecturaanterior, l.lecturaultima, c.catetar, c.codestadoservicio, l.observacion, l.codestlectura, ";
	$sqlLecturas .= " estlec.descripcion, l.codestadomedidor, ";
	$sqlLecturas .= $objDrop->getCodCatastral("c.").", c.codantiguo, l.consumo, l.lecturapromedio, l.tipofacturacion, l.lecturaestimada, ";
	$sqlLecturas .= " l.bloqueado ";
	$sqlLecturas .= "FROM medicion.lecturas l
					JOIN catastro.clientes AS c ON(l.codemp=c.codemp AND l.codsuc=c.codsuc AND l.nroinscripcion=c.nroinscripcion)
					JOIN public.calles AS cl ON(c.codemp=cl.codemp AND c.codsuc=cl.codsuc AND c.codcalle=cl.codcalle AND c.codzona=cl.codzona)
					JOIN public.tiposcalle AS tc ON(cl.codtipocalle=tc.codtipocalle)
					JOIN public.rutaslecturas AS rl ON (c.codemp=rl.codemp AND c.codsuc=rl.codsuc AND  c.codzona=rl.codzona AND c.codsector = rl.codsector AND c.codrutlecturas=rl.codrutlecturas AND c.codmanzanas=rl.codmanzanas)
					JOIN public.rutasmaelecturas rtml ON(rl.codemp = rtml.codemp AND rl.codsuc = rtml.codsuc AND rl.codzona = rtml.codzona AND rl.codsector = rtml.codsector AND rl.codrutlecturas = rtml.codrutlecturas)
					JOIN public.estadolectura AS estlec ON(l.codestlectura = estlec.codestlectura)
					WHERE l.codemp = 1 AND l.codsuc = ".$codsuc." AND l.codciclo = ".$_POST["ciclo"]."
					AND l.anio = '".$facturacion["anio"]."' AND l.mes = '".$facturacion["mes"]."' AND trim(c.nromed) <> ''" .$cSector.$cRuta.$order;

	$consultaL = $conexion->prepare($sqlLecturas);
	$consultaL->execute(array());

	$itemsLecturas = $consultaL->fetchAll();

	$consultaEL = $conexion->prepare("SELECT codestlectura, UPPER(descripcion) AS descripcion FROM public.estadolectura WHERE estareg=1 ORDER BY codestlectura");
	$consultaEL->execute();
	$itemEL = $consultaEL->fetchAll();

	$consultaM = $conexion->prepare("SELECT * FROM public.estadomedidor WHERE estareg=1 ORDER BY codestadomedidor");
	$consultaM->execute();
	$itemM = $consultaM->fetchAll();


?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_digitacion.js"></script>
<script>
	var c = 0
	var index=0;
	codsuc = '<?=$codsuc?>'
  var id_user = '<?=$idusuario?>'
  var urldir = '<?php echo $_SESSION['urldir'];?>'
  var ObjTr='';
$(document).ready(function(){
  $( "#DivCuentaCorriente" ).dialog({
            autoOpen: false,
            height:600,
            width: 1000,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $( "#DivCuentaCorriente" ).dialog( "close" );
                }
            },
            close: function() {
              $(ObjTr).addClass('ui-widget-header');
            }
        });

});

  function CuentaCorriente(nroinscripcion,obj)
  {
    ObjTr=$(obj).parent().parent();
    //AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/Pdf.php?NroInscripcion="+nroinscripcion,Tam[0],Tam[1])
    $("#nroinscripcion").val(nroinscripcion)
    $.ajax({
       url:'<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/Consulta.php',
       type:'POST',
       async:true,
       data:'NroInscripcion='+nroinscripcion,
       success:function(datos)
       {

             $("#detalle-CuentaCorriente").empty().append(datos);
        $("#DivCuentaCorriente").dialog('open');

       }
      })
  }
</script>
<div align="center">
<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <table width="98%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
   <tr>
        <td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="7%" align="right">Sucursal</td>
            <td width="1%" align="center">:</td>
            <td width="17%"><input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" /></td>
            <td width="6%" align="right">Fecha </td>
            <td width="1%" align="center">:</td>
            <td width="8%"><input type="text" name="fechadigitacion" id="fechadigitacion" value="<?=$_POST["fdigitacion"]?>" class="inputtext" readonly="readonly" style="width:80px;" /></td>
            <td width="6%" align="right">Sectores</td>
            <td width="2%" align="center">:</td>
            <td width="16%"><input type="text" name="sectores" id="sectores" value="<?=$sectores?>" class="inputtext" readonly="readonly" size="30" /></td>
            <td width="5%" align="right">Ruta</td>
            <td width="1%" align="center">:</td>
            <td width="30%">
            	<input type="text" name="ruta" id="ruta" value="<?=$rutas?>" class="inputtext" readonly="readonly" size="30" />
              	<input type="hidden" name="anio" id="anio" value="<?=$facturacion["anio"]?>" />
              	<input type="hidden" name="mes" id="mes" value="<?=$facturacion["mes"]?>" />
              	<input type="hidden" name="ciclo" id="ciclo" value="<?=$_POST["ciclo"]?>" />
                <input type="hidden" id="nroinscripcion">
             </td>
          </tr>
        </table>
        </td>
	</tr>
    <tr>
      <td colspan="2" align="center">
      	<div id="div_lecturas" style="overflow:auto;height:320px">
      	 <table class="ui-widget" border="1" cellspacing="0" id="tbdigitacion" width="95%" rules="all" >
      	    <thead class="ui-widget-header" style="font-size:14px" height="30px">
      	      	<tr>
	      	        <th scope="col" width="20">&nbsp;</th>
	      	        <th width="60" align="center" valign="middle"  scope="col">Cod.Catastral</th>
	      	        <th width="50" align="center" valign="middle"  scope="col">C&oacute;digo</th>
	      	        <th align="center" valign="middle"  scope="col">Cliente</th>
	      	        <th align="center" valign="middle"  scope="col">Direccion</th>
	      	        <th width="50" align="center" valign="middle"  scope="col">Nro. Med </th>
	      	        <th width="55" align="center" valign="middle"  scope="col">Lect. Anterior </th>
	      	        <th width="55" align="center" valign="middle"  scope="col">Lect. Ultima </th>
	      	        <th width="10" align="center" valign="middle"  scope="col">Con.</th>
	      	        <th width="10" align="center" valign="middle"  scope="col">Prom.</th>
                  <th width="10" align="center" valign="middle"  scope="col">Esti.</th>
	      	        <th width="200" align="center" valign="middle"  scope="col">Estado Lectura </th>
	      	        <th width="90" align="center" valign="middle"  scope="col">Estado Medidor </th>
	      	        <th width="40" align="center" valign="middle"  scope="col">&nbsp;</th>
      	      	</tr>
      	    </thead>
      	    <tbody>
      	      <?php
			  	$cont=0;

			  	foreach($itemsLecturas as $rowL)
				{
					$cont++;
					$class='';
					if($rowL['tipofacturacion']==1)
						$class=' bgcolor="#FDF0A1"';
          $BloqueadoImg = '';
          $Bloqueado=1;
          $readonly="readonly='readonly'";
          if($rowL['bloqueado']==0)
          {
            $Bloqueado=0;
            $BloqueadoImg = 'style="display:none;"';
            $readonly="";
          }
			  ?>
              <tr <?=$class?> tabindex="<?=$cont?>">
      	        <td align="center">
      	        	<img src="../../../../images/iconos/ver.png" width="16" height="16" style="cursor:pointer" onclick="agregar_observacion(<?=$cont?>);" title="Agregar Observacion de Digitacion" />
      	        </td>
      	        <td align="center"><?=$rowL["codcatastro"]?>
      	          <input type="hidden" name="observacion<?=$cont?>" id="observacion<?=$cont?>" value="<?=$rowL["observacion"]?>" />
                  <input type="hidden" name="inscripcion<?=$cont?>" id="inscripcion<?=$cont?>" value="<?=$rowL["nroinscripcion"]?>" />
                  <input type="hidden" id="bloqueado<?=$cont?>" value="<?=$rowL["bloqueado"]?>" />
                </td>
                <td align="center" ><?=strtoupper($rowL["codantiguo"])?></td>
      	        <td style="padding-left:5px;" ><?=strtoupper($rowL["propietario"])?></td>
      	        <td style="padding-left:5px;" ><?=strtoupper($rowL["descripcioncorta"]." ".$rowL["calle"]." ".$rowL["nrocalle"])?></td>
      	        <td  align="center"><?=$rowL["nromed"]?></td>
      	        <td  align="center">

      	        	<input type="text" name="lecturaanterior<?=$cont?>" id="lecturaanterior<?=$cont?>" maxlength="10" class="inputtext numeric" value="<?=$rowL["lecturaanterior"]==0?'':number_format($rowL["lecturaanterior"],0);?>" style="text-align:right; width:50px;"  readonly="readonly"/>
      	        </td>
      	        <td  align="center">
                	<input type="text" name="lectultima<?=$cont?>" id="lectultima<?=$cont?>" maxlength="10" class="inputtext numeric focusselect" value="<?=$rowL["lecturaultima"]==0?'':number_format($rowL["lecturaultima"],0);?>" style="text-align:right; width:50px;"
                  <?=$readonly?>
                  onkeyup="calcular_consumo(this.value,<?=$rowL["lecturaanterior"]?>,<?=$cont?>,<?=$rowL["catetar"]?>,<?=$rowL["codestadoservicio"]?>,<?=$rowL["nroinscripcion"]?>,<?=$facturacion["mes"]?>,<?=$Bloqueado?>);CambiarFoco(event,'lectultima<?=$cont+1?>');"
                  onblur="GuardarDigitacionSin(<?=$rowL["nroinscripcion"]?>,<?=$cont?>,<?=$Bloqueado?>);"

                  />
               	</td>
      	        <td  align="center">
                	<!-- <input type="hidden" name="consumo<?=$cont?>" id="consumo<?=$cont?>" value="<?=$rowL["lecturaultima"]-$rowL["lecturaanterior"]?>" />
                    <label id="lblconsumo<?=$cont?>"><?=$rowL["lecturaultima"]!=0?$rowL["lecturaultima"]-$rowL["lecturaanterior"]:""?></label> -->
                    <input type="hidden" id="catetar<?=$cont?>" value="<?=$rowL["catetar"]?>" />
                    <input type="hidden" id="codestadoservicio<?=$cont?>" value="<?=$rowL["codestadoservicio"]?>" />
                    <input type="hidden" name="consumo<?=$cont?>" id="consumo<?=$cont?>" value="<?=number_format($rowL["consumo"],2)?>" />
                    <label id="lblconsumo<?=$cont?>"><?=number_format($rowL["consumo"],0)?></label>
                </td>
                <td align="center">
                    <input type="hidden" name="promedio<?=$cont?>" id="promedio<?=$cont?>" value="<?=number_format($rowL["lecturapromedio"],2)?>" />
                    <label id="lblpromedio<?=$cont?>"><?=number_format($rowL["lecturapromedio"],0)?></label>
                </td>
                 <td align="center">
                    <input type="hidden" name="lecturaestimada<?=$cont?>" id="lecturaestimada<?=$cont?>" value="<?=number_format($rowL["lecturaestimada"],2)?>" />
                    <label id="lbllecturaestimada<?=$cont?>"><?=number_format($rowL["lecturaestimada"],0)?></label>
                </td>
      	        <td align="left" style="padding-left:5px;">
									<select name="estlectura<?=$cont?>" id="estlectura<?=$cont?>" class="select" style="width:150px" onchange="GuardarDigitacionCmb(<?=$rowL["nroinscripcion"]?>,<?=$cont?>)">
                  <?php
									  $rowL["codestlectura"] = isset($rowL["codestlectura"])?$rowL["codestlectura"]:0;
										foreach($itemEL as $rowEL) :
											$selected = "";
											if($rowL["codestlectura"]==$rowEL["codestadomedidor"]) {
												$selected="selected='selected'";
											}
											echo "<option value='".$rowEL["codestlectura"]."' ".$selected." >".$rowEL["descripcion"]."</option>";
										endforeach;
									?>
    	          	</select>
									<span class="MljSoft-icon-buscar" title="Estado Lectura" onclick="Ver_EstadoLectura(this,<?php echo $cont; ?>);"></span>
                  <!-- <input type="hidden" name="estlectura<?=$cont?>" id="estlectura<?=$cont?>" value="<?=isset($rowL["codestlectura"])?$rowL["codestlectura"]:4?>" /> -->
                	<!-- <label id="lblcritica<?=$cont?>"><?=isset($rowL["descripcion"])?$rowL["descripcion"]:"LECTURA NEGATIVA"?></label> -->
                </td>

      	        <td >
      	          <select name="estmedidor<?=$cont?>" id="estmedidor<?=$cont?>" class="select" style="width:90px" onchange="GuardarDigitacionCmb(<?=$rowL["nroinscripcion"]?>,<?=$cont?>)">
                  <?php
				  	foreach($itemM as $rowM)
					{
						$selected="";
						if($rowL["codestadomedidor"]==$rowM["codestadomedidor"])
						{
							$selected="selected='selected'";
						}
						echo "<option value='".$rowM["codestadomedidor"]."' ".$selected." >".$rowM["codestadomedidor"]."-".$rowM["descripcion"]."</option>";
					}
				  ?>
    	          </select>
    	        </td>
    	        <td>
                <span class="icono-icon-info" title="Promediar Consumo" onclick="promediar_lecturas(<?=$cont?>,this);"></span>
                <span class="icono-icon-document" title="Cambio de Valor Medicion" onclick="cambio_lecturaestimada(<?=$cont?>,this);"></span>
                <span class="MljSoft-icon-buscar" title="Historico de Lecturas" onclick="Ver_Historico(<?=$rowL["nroinscripcion"]?>,this);"></span>
                <span id="spanbloq<?=$cont?>" class="MljSoft-icon-bloqueado" <?=$BloqueadoImg?> title="Lectura Bloqueada" onclick="Desbloquear(<?=$rowL["nroinscripcion"]?>,<?=$cont?>);"></span>
                <span class="icono-icon-dinero" title="Ver Cuenta Corriente" onclick="CuentaCorriente(<?=$rowL['nroinscripcion']?>,this)"></span>
              </td>
    	        </tr>
              <?php
				}
			  ?>

  	      	</tbody>

   	    </table>
        </div>
      </td>
    </tr>
    <tr>
    <td align="right">Buscar:</td>
    <td>
    	<input id="Valor" class="buscar" value="" maxlength="30" size="33" type="text" />
    </td>

  </tr>
    <tr>
      <td colspan="2">
      	<input type="hidden" name="contador" id="contador" value="<?=$cont?>" />
      	&nbsp;<div id="div_error"></div>
      </td>
    </tr>
	  </tbody>
	<tfoot>
		<td colspan="2"  style="padding:4px; height:45px">
        	<table width="100%" border="0">
			 <tbody><tr>
			  <td align="left">
			     <input type="button" onclick="ValidarForm();" value="Procesar" id="btnAceptar" class="button" name="btnAceptar" style="display:none">
			  </td>
			  <td align="right">
			     <input type="button" onclick="Cancelar();" value="Cancelar" id="btnCancelar" class="button" name="btnCancelar">
			  </td>
			 </tr>
			</tbody></table>
		</td>
	  </tfoot>
    </table>
 </form>
</div>
<script>
	$("#btnAceptar").val("Procesar")
	$("#sector").attr("disabled",true)
</script>
<div id="DivObs" title="Agregar Observacion de Lectura" style="display:none"  >
  	<textarea name="observacion" id="observacion" class="inputtext" style="width:470px; height:180px;"></textarea>
</div>
<div id="DivPromediar" title="Historico de Lecturas" style="display:none" >
    <div id="DivPromedios"></div>
  </div>
  <div id="DivLecturaEstimada" title="Lectura Estimada" style="display:none" >
  <div id="DivLecEst"></div>
</div>
<div id="DivCuentaCorriente" title="Ver Cuenta Corriente"  >
    <div id="detalle-CuentaCorriente">
    </div>
</div>
  <?php   CuerpoInferior(); ?>
