<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	//ini_set("display_errors",1);
	include("../../../../objetos/clsFunciones.php");
	$objFunciones = new clsFunciones();
	
	$conexion->beginTransaction();
	//print_r($_POST);
	$ciclo			= $_POST["ciclo"];
	$anio			= $_POST["anio"];
	$mes			= $_POST["mes"];
	$contador		= $_POST["contador"];
	
	$codsuc	 		= $_SESSION['IdSucursal'];

	for($i=1;$i<=$contador;$i++)
	{
			$digitado		= "";
			$observacion  	= $_POST["observacion".$i];
			$promedio  	= $_POST["promedio".$i];
			$lecturaanterior  	= $_POST["lecturaanterior".$i];
			if($lecturaanterior=='')
				$lecturaanterior=0;
			if(!empty($_POST["lectultima".$i]))
			{
				$lectUltima = $_POST["lectultima".$i];
			}else{
				$lectUltima = 0;
				$digitado	= ",digitado=1";
			}
			
			$updC = "update medicion.lecturas set lecturaultima=:lecturaultima,
					codestlectura=:codestlectura,consumo=:consumo,observacion=:observacion,
					lecturapromedio=:lecturapromedio,
					codestadomedidor=:codestadomedidor,
					lecturaanterior =:lecturaanterior".$digitado."
					where codsuc=:codsuc and anio=:anio and mes=:mes and codciclo=:codciclo and nroinscripcion=:nroinscripcion";

			$result = $conexion->prepare($updC);
			$result->execute(array(":lecturaultima"=>$lectUltima,
									  ":codestlectura"=>$_POST["estlectura".$i],
									  ":consumo"=>$_POST["consumo".$i],
									  ":observacion"=>$observacion,
									  ":codestadomedidor"=>$_POST["estmedidor".$i],
									  ":codsuc"=>$codsuc,
									  ":anio"=>$anio,
									  ":mes"=>$mes,
									  ":codciclo"=>$ciclo,
									  ":lecturapromedio"=>$promedio,
									  ":lecturaanterior"=>$lecturaanterior,
									  ":nroinscripcion"=>$_POST["inscripcion".$i]));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				 $mensaje = "Error update lecturas";
				die();
			}
			//var_dump($consultaC->errorInfo());
			$updCl = "update catastro.clientes set codestlectura=?,consumo=?,lecturapromedio=? ,lecturaanterior=?,digitado=1 
					 where codsuc=? and nroinscripcion=?";
			$result = $conexion->prepare($updCl);
			$result->execute(array($_POST["estlectura".$i],$_POST["consumo".$i],$promedio,$lecturaanterior,$codsuc,$_POST["inscripcion".$i]));
			//var_dump($consultaCL->errorInfo());
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update clientes";
				die();
			}

			$updCX = "update catastro.conexiones set codestadomedidor=? where codsuc=? and nroinscripcion=?";
			$result = $conexion->prepare($updCX);
			$result->execute(array($_POST["estmedidor".$i],$codsuc,$_POST["inscripcion".$i]));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update conexiones";
				die();
			}
			//var_dump($consultaCX->errorInfo());
	}
	//die('error saco');
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$res=2;
	}else{
		$conexion->commit();
		$res=1;
	}
?>

