<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	$objFunciones = new clsFunciones();
	$conexion->beginTransaction();
	
	$idusuario      = $_POST['id_user'];
	$codsuc         = $_POST['IdSucursal'];
	$ciclo          = $_POST["ciclo"];
	$anio           = $_POST["anio"];
	$mes            = $_POST["mes"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$ultima         = $_POST["ultima"];
	$consumo         = $_POST["consumo"];
	$updL = "update medicion.lecturas set lecturaestimada=?,consumo=?
			where codsuc=? and anio=? and mes=? and codciclo=? and nroinscripcion=?";
	
	$result = $conexion->prepare($updL);
	$result->execute(array(
						  $ultima,
						  $consumo,
						  $codsuc,
						  $anio,
						  $mes,
						  $ciclo,
						  $nroinscripcion));
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error update lecturas";
		die();
	}


	$updC = "UPDATE catastro.clientes set lecturaestimada=:lecturaestimada,consumo=:consumo
			where codsuc=:codsuc and nroinscripcion=:nroinscripcion ";
			
	$result = $conexion->prepare($updC);
	$result->execute(array(":lecturaestimada"=>$ultima,":consumo"=>$consumo,":codsuc"=>$codsuc,":nroinscripcion"=>$nroinscripcion));

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error update clientes";
		die();
	}


	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		
	}else{
		$conexion->commit();
		
	}

?>
