// JavaScript Document
var index = 0;

$(document).ready(function(){

$(document).on('focusin', '.focusselect',function(event){$(this).siblings().removeClass('ui-widget-header');$(this).parent().parent().addClass('ui-widget-header');});
$(document).on('focusout','.focusselect', function(event){$(this).parent().parent().removeClass('ui-widget-header');});	

$("#tbdigitacion tbody tr").focusin(function(){$(this).siblings().removeClass('ui-widget-header');$(this).addClass('ui-widget-header');})
$("#tbdigitacion tbody tr").focusout(function(){$(this).removeClass('ui-widget-header'); })
					

$('#div_lecturas #tbdigitacion').fixedHeaderTable({ height: '300', footer: true });	
	var theTable = $('#tbdigitacion')
  	$("#Valor").keyup(function(){$.uiTableFilter( theTable, this.value)})
	$("#DivObs").dialog({
		autoOpen: false,
		height:300,
		width: 500,
		modal: true,
/*			show: "blind",
		hide: "scale",*/
		buttons: {
			"Aceptar": function() {
				$("#observacion"+index).val($("#observacion").val())
				
				$( this ).dialog( "close" );
			},
			"Cancelar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {

		}
	});
})
$(function() {
	

	 $('form').keypress(function(e){   
		if(e == 13){
		  return false;
		}
	  });

	  $('input').keypress(function(e){
		if(e.which == 13){
		  return false;
		}
	  });
	  //$('#div_lecturas #tbdigitacion').fixedHeaderTable({ height: '300', footer: true });
	  	$( "#DivPromediar" ).dialog({
			autoOpen: false,
			height: 350,
			width: 550,
			modal: true,
			resizable:true,
			buttons: {
				"Aceptar": function() {
					
					$("#promedio"+itemupd).val($("#lectpromedio").val())
					$("#lblpromedio"+itemupd).html($("#lectpromedio").val())

					$("#consumo"+itemupd).val($("#lectpromedio").val())
					$("#lblconsumo"+itemupd).html($("#lectpromedio").val())
					//$("#lectultima"+itemupd).val($("#lectpromedio").val())
					var lecturaestimada = parseFloat(str_replace($("#lecturaanterior"+itemupd).val(), ',', '')) + parseFloat($("#lectpromedio").val());  
					if(isNaN(lecturaestimada)) lecturaestimada=0
					$("#lbllecturaestimada"+itemupd).html(lecturaestimada)
					$("#lecturaestimada"+itemupd).val(lecturaestimada)
					$("#bloqueado"+itemupd).val(1) //BLOQUEMOS POR PROMEDIAR
					GuardarDigitacionSin($("#inscripcion"+itemupd).val(),itemupd,0)
					$( this ).dialog( "close" );
				},
				"Cancelar": function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				$(ObjTr).addClass('ui-widget-header');
			}
		});
		$( "#DivLecturaEstimada" ).dialog({
			autoOpen: false,
			height: 350,
			width: 550,
			modal: true,
			resizable:true,
			buttons: {
				"Aceptar": function() {


					if ($("#lectestimada").val() == '') {
						alert("Introducir una lectura estimada");return;
					};
					/*if ($("#claveestimada").val() == '') {
						alert("Digitar por favor la clave");return;
					};


					if( $("#claveestimada").val() != "yavuelta1009"){
						$("#dathtml").html("Clave no valida o incorrecta").css('color', 'red').fadeIn("slow").fadeOut( 1900);
						return false;
					}*/
					var lectestimada = $("#lectestimada").val()
					
					//var lectanterior=str_replace($("#lecturaanterior"+itemupd).val(), ',', '')
					var lectultima=str_replace($("#lectultima"+itemupd).val(), ',', '')
					$("#lbllecturaestimada"+itemupd).html(lectestimada)
					$("#lecturaestimada"+itemupd).val(lectestimada)
					//var consumo = parseFloat(lectultima)-parseFloat(lectestimada);  
					var consumo = parseFloat(lectultima)-parseFloat(lectestimada); 

					$("#consumo"+itemupd).val(consumo)
					$("#lblconsumo"+itemupd).html(consumo)

					$("#bloqueado"+itemupd).val(1) //BLOQUEMOS POR PROMEDIAR
					//validar_estlectura(catetar,estservicio,lectanterior,obj,cons,idx,numinscripcion,mes)
					validar_estlectura($("#catetar"+itemupd).val(),$("#codestadoservicio"+itemupd).val(),lectestimada,lectestimada,consumo,itemupd,$("#inscripcion"+itemupd).val(),$("#mes").val())

					GuardarLecturaEstimada($("#inscripcion"+itemupd).val(),itemupd,lectestimada,consumo)
					$( this ).dialog( "close" );
				},
				"Cancelar": function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				$(ObjTr).addClass('ui-widget-header');
			}
		});


});

function validar_sector(obj)
{
	$("#rutaslecturas").attr("disabled",true)
	$("#chkrutas").attr("checked",true)
 	$("#todosrutas").val(1)
 
	if(obj.checked)
	{
		$("#codsector").attr("disabled",true)
	}else{
		$("#codsector").attr("disabled",false)
	}
}
function validar_rutas(obj)
{
	if(obj.checked)
	{
		$("#rutaslecturas").attr("disabled",true)
	}else{
		$("#rutaslecturas").attr("disabled",false)
		
	}
}
function cargar_rutas_lecturas(codsuc, obj, cond)
{
	
	$.ajax({
		 url:'../../../../ajax/rutas_lecturas_drop.php',
		 type:'POST',
		 async:true,
		 data:'codsector=' + obj + '&codsuc=' + codsuc + '&condicion=' + cond,
		 success:function(datos){
	
			 $("#div_rutaslecturas").html(datos)

			 $( "#rutaslecturas" ).change(function() {
			  cFechaLectura()
			});

/*			 $("#chkrutas").attr("checked",true)
			 $("#todosrutas").val(1)*/
		 }
	}) 
}
function cFechaLectura()
{
	$.ajax({
		 url:'fecha_lectura.php',
		 type:'POST',
		 async:true,
		 data:'codsector=' + $("#codsector").val() + '&codsuc=' + codsuc + '&ciclo=' + $("#ciclo").val() + '&rutaslecturas=' + $("#rutaslecturas").val(),
		 success:function(datos){
			if(datos!='0')
			{
				$("#fdigitacion").val(datos)
			}
			else
			{
				Msj($("#fdigitacion"),'No se registro Cronograma')
				$("#fdigitacion").val('')
			}
			 

		 }
	}) 
}
function calcular_consumo(obj,lectura,idx,catetar,estservicio,nroinscripcion,mes,bloqueado)
{
	
	//if(lectura==""){lectura=0;}
	
	if(bloqueado==0 || $("#bloqueado"+idx).val()==0 )
	{
		lectultima = str_replace($("#lectultima"+idx).val(), ',', '');
		obj= lectultima
		if(lectultima==""){lectultima=0;}
		lectura	= str_replace($("#lecturaanterior"+idx).val(), ',', '');
		var cons = parseFloat(lectultima) - parseFloat(lectura)
		if(isNaN(cons))
			cons=0;
		$("#lecturaestimada"+idx).val(lectultima);
		$("#lbllecturaestimada"+idx).html(lectultima);
		$("#lblconsumo"+idx).html(cons)
		$("#consumo"+idx).val(cons)
		validar_estlectura(catetar,estservicio,lectura,obj,cons,idx,nroinscripcion,mes)
	}
}

function validar_estlectura(catetar,estservicio,lectanterior,obj,cons,idx,numinscripcion,mes)
{

	$.ajax({
		 url:'../../../../admin/validaciones/validar_estlectura.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&catetar='+catetar+'&estservicio='+estservicio+'&lectanterior='+lectanterior+
			  '&lectultima='+obj+'&consumo='+cons+"&numinscripcion="+numinscripcion+"&mes="+mes,
		 success:function(datos){
			 var r=datos.split("|")
			 $("#estlectura"+idx).val(r[0])
			 $("#lblcritica"+idx).html(r[1])
			 GuardarDigitacion(numinscripcion,r[0],idx)
		 }
	}) 
}
function agregar_observacion(idx)
{
	$("#observacion").val($("#observacion"+idx).val())
	index = idx
	
	$( "#DivObs" ).dialog( "open" );
}
function GuardarDigitacionSin(nroinscripcion,idx,bloqueado)
{
	var url;
		if(bloqueado==0 || $("#bloqueado"+idx).val()==0)	
		{
			lectultima  = $("#lectultima"+idx).val();
			consumo		= $("#consumo"+idx).val();
			obs			= $("#observacion"+idx).val();
			estlectura	= $("#estlectura"+idx).val();
			estmedidor	= $("#estmedidor"+idx).val();
			promedio	= $("#promedio"+idx).val();
			lecturaanterior	= $("#lecturaanterior"+idx).val();
			lecturaestimada	= $("#lecturaestimada"+idx).val();
			bloqueado = $("#bloqueado"+idx).val()

			if(lectultima=='')lectultima=0
			if(lecturaanterior=='')lecturaanterior=0
			if(lecturaestimada=='')lecturaestimada=0

			url = 'ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val();
			url = url + '&nroinscripcion='+nroinscripcion+'&fdigitacion='+$("#fechadigitacion").val()+'&lectultima='+lectultima+'&consumo='+consumo;
			url = url + '&observacion='+obs+'&estlectura='+estlectura+'&estadomedidor='+estmedidor+'&promedio='+promedio+'&lecturaanterior='+lecturaanterior;
			url = url +'&IdSucursal='+codsuc+'&id_user='+id_user+'&lecturaestimada='+lecturaestimada;
			url = url + '&bloqueado='+bloqueado
			$.ajax({
				 url:'guardar_digitacion.php',
				 type:'POST',
				 async:true,
				 data:url,
				 success:function(datos){
					if(bloqueado==1)
					{
						Bloquear(idx)
					}
				 }
			}) 
		}
		
}
function GuardarDigitacionCmb(nroinscripcion,idx)
{
	var url;
					
		lectultima  = $("#lectultima"+idx).val();
		consumo		= $("#consumo"+idx).val();
		obs			= $("#observacion"+idx).val();
		estlectura	= $("#estlectura"+idx).val();
		estmedidor	= $("#estmedidor"+idx).val();
		promedio	= $("#promedio"+idx).val();
		lecturaanterior	= $("#lecturaanterior"+idx).val();
		lecturaestimada	= $("#lecturaestimada"+idx).val();
		if(lectultima=='')lectultima=0
		if(lecturaanterior=='')lecturaanterior=0
		if(lecturaestimada=='')lecturaestimada=0
		bloqueado = $("#bloqueado"+idx).val()
		url = 'ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val();
		url = url + '&nroinscripcion='+nroinscripcion+'&fdigitacion='+$("#fechadigitacion").val()+'&lectultima='+lectultima+'&consumo='+consumo;
		url = url + '&observacion='+obs+'&estlectura='+estlectura+'&estadomedidor='+estmedidor
		url = url +'&promedio='+promedio+'&lecturaanterior='+lecturaanterior;
		url = url +'&IdSucursal='+codsuc+'&id_user='+id_user+'&lecturaestimada='+lecturaestimada;
		url = url + '&bloqueado='+bloqueado
		$.ajax({
			 url:'guardar_digitacion.php',
			 type:'POST',
			 async:true,
			 data:url,
			 success:function(datos){
				
			 }
		}) 
}

function GuardarLecturaEstimada(nroinscripcion,idx,lectultima,consumo)
{

	var url;
		
	url = 'ciclo='+$("#ciclo").val()+'&nroinscripcion='+nroinscripcion+'&ultima='+lectultima+'&anio='+$("#anio").val()+'&mes='+$("#mes").val()+'&IdSucursal='+codsuc+'&id_user='+id_user+'&consumo='+consumo+'&bloqueado='+$("#bloqueado"+idx).val();
	$.ajax({
		 url:'guardar_lecturaestimada.php',
		 type:'POST',
		 async:true,
		 data:url,
		 success:function(datos){
		 	Bloquear(idx)
		 }
	}) 
}
function Bloquear(idx)
{
	$("#spanbloq"+idx).show()
	$("#lectultima"+idx).attr('disabled','disabled')	
	Msj($("#spanbloq"+idx),'Lectura Bloqueada','','left')
	/*$("#lectultima"+idx).unbind("keyup");
	$("#lectultima"+idx).keyup(function() {
		  //alert( "Handler for .keyup() called." );

		  calcular_consumo($(this).val(),$("#lecturaanterior"+idx).val(),idx,$("#catetar"+idx).val(),$("#codestadoservicio"+idx).val(),$("#inscripcion"+idx).val(),$("#mes").val(),1);
		  //CambiarFoco(event,'lectultima<?=$cont+1?>');
		});
*/
}
function Desbloquear(nroinscripcion,idx)
{
	var url;
	if(confirm("Desea Desbloquear la Lectura?"))
	{	
		url = 'ciclo='+$("#ciclo").val()+'&nroinscripcion='+nroinscripcion+'&anio='+$("#anio").val()+'&mes='+$("#mes").val()+'&IdSucursal='+codsuc+'&id_user='+id_user;
		$.ajax({
			 url:'desbloquear_lectura.php',
			 type:'POST',
			 async:true,
			 data:url,
			 success:function(datos)
			 {
			 	 $("#bloqueado"+idx).val(0)
			 	$("#spanbloq"+idx).hide()
				$("#lectultima"+idx).attr('disabled',false)	
				$("#lectultima"+idx).attr('readonly',false)	
				Msj($("#lectultima"+idx),'Lectura Desbloqueada')
				/*$("#lectultima"+idx).unbind("keyup");
				$("#lectultima"+idx).keyup(function() 
				{
			  	  calcular_consumo($(this).val(),$("#lecturaanterior"+idx).val(),idx,$("#catetar"+idx).val(),$("#codestadoservicio"+idx).val(),$("#inscripcion"+idx).val(),$("#mes").val(),0);
			  	});*/
			 }
		})
	}
}
// function GuardarDigitacion(e,nroinscripcion,idx)
// {
// 	var url;
// 	if (!e) e = window.event; 
// 	if(e && e.keyCode == 13)
// 	{				
// 		lectultima  = $("#lectultima"+idx).val();
// 		consumo		= $("#consumo"+idx).val();
// 		obs			= $("#observacion"+idx).val();
// 		estlectura	= $("#estlectura"+idx).val();
// 		estmedidor	= $("#estmedidor"+idx).val();
// 		promedio	= $("#promedio"+idx).val();
// 		lecturaanterior	= $("#lecturaanterior"+idx).val();
// 		if(lectultima=='')lectultima=0
// 		if(lecturaanterior=='')lecturaanterior=0
		
// 		url = 'ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val();
// 		url = url + '&nroinscripcion='+nroinscripcion+'&fdigitacion='+$("#fechadigitacion").val()+'&lectultima='+lectultima+'&consumo='+consumo;
// 		url = url + '&observacion='+obs+'&estlectura='+estlectura+'&estadomedidor='+estmedidor+'&promedio='+promedio+'&lecturaanterior='+lecturaanterior;
// 		url = url +'&IdSucursal='+codsuc+'&id_user='+id_user;
// 		$.ajax({
// 			 url:'guardar_digitacion.php',
// 			 type:'POST',
// 			 async:true,
// 			 data:url,
// 			 success:function(datos){
				
// 			 }
// 		}) 
// 	}
// }
function GuardarDigitacion(nroinscripcion,estlectura,idx)
{

	var url;
	// if (!e) e = window.event; 
	// if(e && e.keyCode == 13)
	// {				
	lectultima      = $("#lectultima"+idx).val();
	consumo         = $("#consumo"+idx).val();
	obs             = $("#observacion"+idx).val();
	estmedidor      = $("#estmedidor"+idx).val();
	promedio        = $("#promedio"+idx).val();
	lecturaanterior = $("#lecturaanterior"+idx).val();
	lecturaestimada	= $("#lecturaestimada"+idx).val();
	if(lectultima=='')lectultima=0
	if(lecturaanterior=='')lecturaanterior=0
	if(lecturaestimada=='')lecturaestimada=0
	bloqueado = $("#bloqueado"+idx).val()	
	url = 'ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val();
	url = url + '&nroinscripcion='+nroinscripcion+'&fdigitacion='+$("#fechadigitacion").val()+'&lectultima='+lectultima+'&consumo='+consumo;
	url = url + '&observacion='+obs+'&estlectura='+estlectura+'&estadomedidor='+estmedidor+'&promedio='+promedio+'&lecturaanterior='+lecturaanterior;
	url = url +'&IdSucursal='+codsuc+'&id_user='+id_user+'&lecturaestimada='+lecturaestimada;
	url = url + '&bloqueado='+bloqueado
	$.ajax({
		 url:'guardar_digitacion.php',
		 type:'POST',
		 async:true,
		 data:url,
		 success:function(datos){
		 }
	}) 
	// }
}
function guardar()
{
	var cad = "";
	var cont=0;
	
	for(i=1;i<=$("#contador").val();i++)
	{
		lectura = $("#lectultima"+i).val()
		if(lectura!="")
		{

			cad  = cad + "inscripcion"+i+"="+$("#inscripcion"+i).val()+'&';
			cad = cad + "lectultima"+i+"="+lectura+'&';
			cad = cad + "consumo"+i+"="+$("#consumo"+i).val()+'&';
			cad = cad + "observacion"+i+"="+$("#observacion"+i).val()+'&';
			cad = cad + "estlectura"+i+"="+$("#estlectura"+i).val()+'&';
			cad = cad + "estmedidor"+i+"="+$("#estmedidor"+i).val()+'&';
			cad = cad + "promedio"+i+"="+$("#promedio"+i).val()+'&';
			cad = cad + "lecturaanterior"+i+"="+$("#lecturaanterior"+i).val()+'&';
			cont++;
		}
	}

	$.ajax({
		 url:'guardar.php',
		 type:'POST',
		 async:true,
		 data:cad+'&ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val()+'&contador='+cont+
		 	  '&fechadigitacion='+$("#fechadigitacion").val(),
		 success:function(datos){
		 	//alert(datos)
			//$("#div_error").html(datos)
			var r=datos.split("|")
			window.parent.OperMensaje(r[1],r[0])
			//location.href='index.php';
		 }
	}) 
}
function datos_facturacion(codciclo)
{
	$.ajax({
		 url:'../../../../ajax/periodo_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+codciclo+'&codsuc='+codsuc,
		 success:function(datos){
			var r=datos.split("|")

			$("#anio").val(r[1])
			$("#mes").val(r[3])
		 }
	}) 
}

function ValidarForm(Op)
	{
		//guardar()
		var miTabla 	= document.getElementById("tbdigitacion");
		var numFilas 	= miTabla.rows.length;
		
		if(numFilas==1)
		{
			alert("No hay Ninguna Item Generada...Genere como minima una para poder continuar")
			return false;
		}
		guardar()
		//GuardarP(Op)
	}
	
	function Cancelar()
	{
		location.href='index.php'
	}
	var itemupd=0
	var ObjTr ='';
	function promediar_lecturas(item,obj)
	{
		itemupd=item
		verlecturas(item);
		objIdx = "promediar";
		$( "#DivPromediar" ).dialog( "open" );
		ObjTr=$(obj).parent().parent();
		//$(ObjTr).addClass('ui-widget-header');
	}
	function cambio_lecturaestimada(item,obj)
	{
		itemupd=item
		verlecturaestimada(item);
		objIdx = "promediar";
		$( "#DivLecturaEstimada" ).dialog( "open" );
		ObjTr=$(obj).parent().parent();
	}
	function verlecturas(item)
	{
		$.ajax({
			 url:'ver_lecturas.php',
			 type:'POST',
			 async:true,
			 data:'codsuc='+codsuc+'&nroinscripcion='+$("#inscripcion"+item).val(),
			 success:function(datos){
				$("#DivPromedios").html(datos)
			 }
		}) 
		}

	function verlecturaestimada(item)
	{
		$.ajax({
			 url:'ver_lecturasestimada.php',
			 type:'POST',
			 async:true,
			 data:'codsuc='+codsuc+'&nroinscripcion='+$("#inscripcion"+item).val(),
			 success:function(datos){
				$("#DivLecEst").html(datos)
			 }
		}) 
	}
	function Ver_Historico(nroinscripcion,obj)
	{	
		
		AbrirPopupImpresion(urldir+"sigco/medicion/listado/historico/Pdf.php?NroInscripcion="+nroinscripcion,Tam[0],Tam[1])
		
	}
	