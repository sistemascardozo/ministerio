<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	
	$paramae = $objFunciones->getParamae("LIMLEC", $codsuc);
	
?>
<style>
	.text1{
		font-size:11px;
		font-weight:bold;
		color:#000;
	}
	.text2{
		font-size:11px;
		font-weight:bold;
		color:#800000;
	}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr style="padding-top:3px">
    <td colspan="3">
    <div id="div_lecturas" style="overflow:auto; height:180px">
        <table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tblecturas" >
           <thead class="ui-widget-header" style="font-size: 11px">
            <tr >
				<th width ="20%" >Mes y A&ntilde;o</th>
				<th width ="10%" >Categoria</th>
				<th width ="16%" >Fecha de Lectura</th>
				<th width ="14%" >Lect. Ultima</th>
				<th width ="11%" >Lect. Anterior</th>
				<th width ="10%" >Consumo</th>
				<th width ="10%" >Lect. Promedio</th>
				<th width ="13%" >Consumo Fact.</th>
            </tr>
           </thead>
           <tbody>
            <?php 
				$count		= 0;
				$promedio 	= 0;

				$limit= "limit ".$paramae["valor"];
				// $sqlLecturas = "select ".$top." anio,mes,fechalectultima,lecturaultima,lecturaanterior,consumo,lecturapromedio,consumofact
				// 				from facturacion.cabfacturacion 
				// 				where codemp=1 and codsuc=? and nroinscripcion=? 
				// 				order by ".$objFunciones->CONVERT('anio','INTEGER').",".$objFunciones->CONVERT('mes','INTEGER')." asc ".$limit; 
				
				$annoactual = date('Y');
				$mesactual  = date('m');
				
				$sqlLecturas = " SELECT lec.anio,lec.mes,lec.fechalectultima,lec.lecturaultima,lec.lecturaanterior,lec.consumo,lec.lecturapromedio,lec.consumo as consumofact,tar.nomtar
				FROM medicion.lecturas lec 
				INNER JOIN catastro.clientes cli ON (lec.codemp = cli.codemp AND lec.codsuc = cli.codsuc AND lec.nroinscripcion = cli.nroinscripcion)
				INNER JOIN facturacion.tarifas tar ON (cli.codemp = tar.codemp AND cli.codsuc = tar.codsuc AND cli.catetar = tar.catetar)
				where lec.codemp=1 and lec.codsuc=? and lec.nroinscripcion=? AND mes <>'" . $mesactual ."' and lec.consumo>0
				order by ".$objFunciones->CONVERT('anio','INTEGER')." desc, ".$objFunciones->CONVERT('mes','INTEGER')." desc ".$limit; 

				$consultaLecturas = $conexion->prepare($sqlLecturas);
				$consultaLecturas->execute(array($codsuc,$nroinscripcion));
				$itemL = $consultaLecturas->fetchAll();

				foreach($itemL as $rowL)
				{
					if($rowL["consumo"]>0)
					{
						$promedio+=$rowL["consumo"];
						$count++;
					}
			?>
                <tr style="font-size: 11px" align="center">
                  <td align="right"><?=$meses[$rowL["mes"]]." - ".$rowL["anio"]?> &nbsp;</td>
                  <td align="right"><?=$rowL["nomtar"]?></td>
                  <td ><?=$objFunciones->DecFecha($rowL["fechalectultima"])?></td>
                  <td align="right"><?=number_format($rowL["lecturaultima"],0)?></td>
                  <td align="right"><?=number_format($rowL["lecturaanterior"],0)?></td>
                  <td align="right"><?=number_format($rowL["consumo"],0)?></td>
                  <td align="right"><?=number_format($rowL["lecturapromedio"],0)?></td>
                  <td align="right"><?=number_format($rowL["consumofact"],0)?></td>
                </tr>
            <?php } ?>
         </tbody>
        </table>
    </div>    </td>
  </tr>
  <tr style="padding:4px" >
    <td width="70">Promedio</td>
    <td width="30" align="center">:</td>
    <td><input type="text" name="lectpromedio" id="lectpromedio" class="inputtext" value="<?=$count!=0?round($promedio/$count,0):0?>" /></td>
  </tr>
</table>
