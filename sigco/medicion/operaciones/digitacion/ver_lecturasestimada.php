<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	
	$paramae = $objFunciones->getParamae("LIMLEC",$codsuc);
	
?>
<style>
	.text1{
		font-size:11px;
		font-weight:bold;
		color:#000;
	}
	.text2{
		font-size:11px;
		font-weight:bold;
		color:#800000;
	}
</style>
<script type="text/javascript" language="JavaScript">
	$(document).ready(function(){
		$('.numeric').numeric();
	});
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr style="padding-top:3px">
	    <td colspan="3">
	    	<div id="div_lecturas" style="overflow:auto; height:180px">
	        	<table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tblecturas" >
	           	<thead class="ui-widget-header" style="font-size: 11px">
	            	<tr >
						<th width ="20%" >Lectura Estimada Actual</th>
	            	</tr>
	           	</thead>
	           	<tbody>
	            <?php 
					
					$annoactual = date('Y');
					$mesactual  = date('m');
					$sqlLecturas = " SELECT ".$top." lec.lecturaestimada
					FROM medicion.lecturas lec 
					where lec.codemp=1 and lec.codsuc=? and lec.nroinscripcion=? AND lec.mes = '" . $mesactual ."' AND lec.anio = '" . $annoactual ."'";

					$consultaLecturas = $conexion->prepare($sqlLecturas);
					$consultaLecturas->execute(array($codsuc,$nroinscripcion));
					$itemL = $consultaLecturas->fetch();
				?>
	                <tr style="font-size: 11px" align="center">
	                  <td align="center"><?=number_format($itemL["lecturaestimada"],0)?></td>
	                </tr>
	         </tbody>
	        	</table>
	    	</div>    
		</td>
  	</tr>
  	<tr>
		<td width="90" align="left">
  			<span>Lectura Estimada</span> 
		</td>
		<td width="30" align="center">:</td>	
		<td>
	  		<input type="text" name="lectestimada" id="lectestimada" class="inputtext numeric" />
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td align="left">
			<span>Digite Clave</span></td>
		<td align="center">:</td>	
		<td>
  			<input type="password" name="claveestimada" id="claveestimada" class="inputtext" />
		</td>
  	</tr>  
  <tr>
  	<td colspan="3">	
  		<span id="dathtml"></span>
	</td>
  </tr>
</table>

