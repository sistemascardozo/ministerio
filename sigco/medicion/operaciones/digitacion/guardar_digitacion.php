<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$conexion->beginTransaction();
	
	$idusuario       = $_POST['id_user'];
	$codsuc          = $_POST['IdSucursal'];
	
	$ciclo           = $_POST["ciclo"];
	$anio            = $_POST["anio"];
	$mes             = $_POST["mes"];
	$nroinscripcion  = $_POST["nroinscripcion"];
	$lectultima      = str_replace(",", "", $_POST["lectultima"]);
	$consumo         = str_replace(",", "",$_POST["consumo"]);
	$observacion     = $_POST["observacion"];
	$estlectura      = $_POST["estlectura"];
	$estadomedidor   = $_POST["estadomedidor"];
	$promedio        = intval(str_replace(",", "",$_POST["promedio"]));
	$lecturaanterior = str_replace(",", "",$_POST["lecturaanterior"]);
	$lecturaestimada = str_replace(",", "",$_POST["lecturaestimada"]);
	$bloqueado       = $_POST["bloqueado"]?$_POST['bloqueado']:0;
	
	if($lectultima==0)  $lecturaestimada = $lecturaanterior + $promedio ;
	
	$updL = "UPDATE medicion.lecturas SET lecturaultima=  ?,
			codestlectura = ?, consumo = ?, observacion = ?, codestadomedidor = ?,
			digitado = 1, creador = ?, lecturapromedio = ?, lecturaanterior = ?, lecturaestimada = ?,
			bloqueado = ?
			WHERE codsuc = ? AND anio = ? AND mes = ? AND codciclo = ? AND nroinscripcion = ?";
	
	$result = $conexion->prepare($updL);
	$result->execute(array($lectultima,
							  $estlectura,
							  $consumo,
							  $observacion,
							  $estadomedidor,
							  $idusuario,
							  $promedio,
							  $lecturaanterior,
							  $lecturaestimada,
							  $bloqueado,
							  $codsuc,
							  $anio,
							  $mes,
							  $ciclo,
							  $nroinscripcion));
if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error update lecturas";
		die();
	}


	$updC = "UPDATE catastro.clientes set lecturaultima=:lecturaultima,
			codestlectura=:codestlectura,consumo=:consumo,lecturapromedio=:lecturapromedio ,lecturaanterior=:lecturaanterior,
			digitado=1,lecturaestimada=:lecturaestimada,codestadomedidor=:estadomedidor
			where codsuc=:codsuc and nroinscripcion=:nroinscripcion ";
			
	$result = $conexion->prepare($updC);
	$result->execute(array(':lecturaultima'=>$lectultima,
							  ':codestlectura'=>$estlectura,
							  ':consumo'=>$consumo,
							  ':lecturapromedio'=>$promedio,
							  ':lecturaanterior'=>$lecturaanterior,
							  ':codsuc'=>$codsuc,
							  ':nroinscripcion'=>$nroinscripcion,
							  ":lecturaestimada"=>$lecturaestimada,
							  ":estadomedidor"=>$estadomedidor
							  ));
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error update clientes";
		die();
	}

	$updConx = "update catastro.conexiones set codestadomedidor=? where codsuc=? and nroinscripcion=?";
	$result = $conexion->prepare($updConx);
	$result->execute(array($estadomedidor,$codsuc,$nroinscripcion));
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error update conexiones";
		die();
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		
	}else{
		$conexion->commit();
		
	}

?>
