<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "DIGITACION DE LECTURAS";
	$Activo = 1;

	CuerpoSuperior($TituloVentana);

	$codsuc 	= $_SESSION['IdSucursal'];

	$objDrop 	= new clsDrop();

	$sucursal = $objDrop->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);

	$Fecha = date('d/m/Y');
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_digitacion.js"></script>
<script>
	var c = 0;
	var index = 0;
	var codsuc = '<?=$codsuc?>';

	function ValidarForm(Op)
	{
		if($("#ciclo").val()==0)
		{
			Msj($("#ciclo"),"Seleccione Ciclo")
			return false
		}
		if($("#todosectores").val()==0)
		{
			if($("#sector").val()==0)
			{
				Msj($("#sector"),"Seleccione Sector")
				return false
			}
		}
		if($("#todosrutas").val()==0)
		{
			/*if($("#rutaslecturas").val()==0)
			{
				alert("Seleccione la Ruta de Lectura")
				return false
			}*/
		}

		if($("#todosgrupos").val()=='1')
		{
			if($("#tipoentidades").val()==0)
			{
				alert("Seleccione otro Grupo")
				return false
			}
		}

		if($("#fdigitacion").val()=='')
		{
			Msj($("#fdigitacion"),"No se registro Cronograma")
			return false
		}
		$("#sector_text").val($("#codsector option:selected").text());
		$("#rutas_text").val($("#rutaslecturas option:selected").text());

		GuardarP(Op)
	}

	function quitar_disabled(obj, input)
	{
		if(obj.checked)
		{
			$("#codsector").attr("disabled", true);
			$("#rutaslecturas").attr("disabled", true);
			$("#" + input).attr("disabled", false);
		}
		else
		{
			$("#codsector").attr("disabled", false);
			$("#rutaslecturas").attr("disabled", false);
			$("#" + input).attr("disabled", true);
		}
	}


</script>
<div align="center">
<form id="form1" name="form1" method="post" action="digitacion.php" enctype="multipart/form-data">
    <table width="700" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="13%">Sucursal</td>
				    <td width="3%" align="center">:</td>
				    <td width="24%"><label>
				      <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </label></td>
				    <td width="11%" align="right">Ciclo</td>
				    <td width="3%" align="center">:</td>
				    <td width="46%">
                    	<? $objDrop->drop_ciclos($codsuc); ?>
                    </td>
				  </tr>


				  <tr>
				    <td>Sector</td>
				    <td align="center">:</td>
				    <td>
                   	 <?php echo $objDrop->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_lecturas(".$codsuc.", this.value, 0);'"); ?></td>
				    <td>&nbsp;</td>
				    <td align="center"><input type="checkbox" name="chksectores" style="visibility:hidden" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todosectores');validar_sector(this);" /></td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
				    <td>Ruta</td>
				    <td align="center">:</td>
				    <td>
                    <div id="div_rutaslecturas">
               	  	<select name="rutaslecturas" id="rutaslecturas" style="width:220px" class="select" >
                      	<option value="0">--Seleccione la Ruta--</option>
                    </select>
                    </div> </td>
				    <td align="center">&nbsp;</td>
				    <td><input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox" style="visibility:hidden" checked="checked" onclick="CambiarEstado(this,'todosrutas');validar_rutas(this);" /></td>
				    <td>&nbsp;</td>
		      </tr>
					<tr>
				    <td>Grupo</td>
				    <td align="center">:</td>
				    <td>
							<div id="div_grupo">
								<? echo $objDrop->drop_tipo_entidades(); ?>
							</div>
           	</td>
						<td>
							&nbsp;&nbsp;
							<input type="checkbox" name="chkgrupos" id="chkgrupos" value="checkbox" onclick="CambiarEstado(this,'todosgrupos');quitar_disabled(this,'tipoentidades');" />
							Aplicar
							<input type="hidden" name="todosgrupos" id="todosgrupos" value="0" />
						</td>
				    <td><input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox" style="visibility:hidden" checked="checked" onclick="CambiarEstado(this,'todosrutas');validar_rutas(this);" /></td>
				    <td>&nbsp;</td>
		      </tr>
				<tr>
				    <td>Fecha </td>
				    <td align="center">:</td>
				    <td><input type="text" readonly="readonly" name="fdigitacion" id="fdigitacion" value="<?=date('d/m/Y')?>" size="15" class="inputtext" /></td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
			      <tr>
			      	<td>Ordenar</td>
			      	<td align="center">:</td>
			      	<td colspan="4">
			      		<input type="hidden" name="orden" id="orden" value="1" />
			        	<input type="hidden" name="todosectores" id="todosectores" value="0" />
                    	<input type="hidden" name="todosrutas" id="todosrutas" value="0" />
                    	<input type="hidden" name="sector_text" id="sector_text" />
			       		<input type="hidden" name="rutas_text" id="rutas_text" />
			      		<div class="buttonset">
					        <input type="radio" name="porrutas" id="porrutas" value="radio" checked="checked"/>
					        <label for="porrutas" onclick="javascript:$('#orden').val('1')">Por Rutas</label>
					        <input type="radio" name="porrutas" id="porrutasmanzanas" value="radio"   />
					        <label for="porrutasmanzanas" onclick="javascript:$('#orden').val('2')">Rutas y Manzanas</label>
					     </div>
			      	</td>
			      </tr>

				  <tr>
				    <td>&nbsp;</td>
				    <td align="center"><input type="checkbox" name="lectfaltantes" id="lectfaltantes" onclick="CambiarEstado(this,'faltantes')" /></td>
				    <td colspan="2">Lecturas Faltantes
                    <input type="hidden" name="faltantes" id="faltantes" value="0" /></td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				   <tr>
				    <td colspan="6" align="center"> <input type="submit" onclick="return ValidarForm();" value="Digitar" id=""></td>
			      </tr>
			       <tr><td colspan="2">&nbsp;</td> </tr>
				</table>
			</fieldset>
		</td>
	</tr>

	  </tbody>
    </table>
 </form>
</div>
<script>
	$("#btnAceptar").val("Procesar")
	$("#tipoentidades").attr("disabled",true)
</script>
  <?php   CuerpoInferior(); ?>
