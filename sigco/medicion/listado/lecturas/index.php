<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "TOMA DE LECTURAS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	$sucursal = $objDrop->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="../../operaciones/digitacion/js_digitacion.js" language="JavaScript"></script>
<script>
jQuery(function($)
	{

		$( "#DivTipos" ).buttonset();

	});
	var codsuc = <?=$codsuc?>;

	function ValidarForm(Op)
	{
		var rutas 		= ""
		var sectores	= ""

		if($("#ciclo").val()==0)
		{
			alert('Seleccione el Ciclo')
			return false
		}
		if($("#todosectores").val()==0)
		{
			sectores = $("#codsector").val()
			if(sectores==0)
			{
				alert("Seleccione el Sector")
				return
			}
		}else{
			sectores="%"
		}
		if($("#todosrutas").val()==0)
		{
			rutas = $("#rutaslecturas").val()
			if(rutas==0)
			{
				alert("Seleccione la Ruta de Lectura")
				return false;
			}
		}else{
			rutas="%"
		}

		grupo = 0
		if($("#todosgrupos").val() == 1)
		{
			grupo  = 1
			grupos = "%"
		}else{

			if($("#tipoentidades").val() != 0)
			{
				grupos = $("#tipoentidades").val()
				grupo  = 1
			}
		}


		if(grupo == 1){

			if(document.getElementById("rptpdf").checked==true)
			{
				url="imprimir_grupo.php";
			}
			if(document.getElementById("rptexcel").checked==true)
			{
				url="excel_grupo.php";
			}
			url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&grupo="+grupos+"&codsuc=<?=$codsuc?>"
			url += "&ruta="+rutas+"&sector="+sectores+"&orden="+$("#orden").val();

			AbrirPopupImpresion(url,800,600);
			return false;
		}

		grupos = $("#tipoentidades").val()

		if(document.getElementById("rptpdf").checked==true)
		{
			url="imprimir.php";
		}
		if(document.getElementById("rptexcel").checked==true)
		{
			url="excel.php";
		}

		url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val();
		url += "&ruta="+rutas+"&sector="+sectores+"&orden="+$("#orden").val()+"&codsuc=<?=$codsuc?>";
		url += "&grupo="+grupos;
		AbrirPopupImpresion(url,800,600)

		return false
	}
	function Cancelar()
	{
		location.href = '<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function cargar_rutas_lecturas(obj,cond)
	{
		$.ajax({
			 url:'../../../../ajax/rutas_lecturas_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+obj+'&codsuc=<?=$codsuc?>&condicion='+cond,
			 success:function(datos){
				$("#div_rutaslecturas").html(datos)
				$("#chkrutas").attr("checked",true)
				$("#todosrutas").val(1)
			 }
		})
	}
	function validar_sectores(obj, idx)
	{
		$("#rutaslecturas").attr("disabled",true)
		$("#chkrutas").attr("checked",true)
		$("#todosrutas").val(1)

		if(obj.checked)
		{
			$("#" + idx).val(1)
			$("#codsector").attr("disabled",true)
		}else{
			$("#" + idx).val(0)
			$("#codsector").attr("disabled",false)
		}
	}
	function validar_rutas(obj,idx)
	{
		if(obj.checked)
		{
			$("#"+idx).val(1)
			$("#rutaslecturas").attr("disabled",true)
		}else{
			$("#"+idx).val(0)
			$("#rutaslecturas").attr("disabled",false)
		}
	}
	function validar_grupos(obj,idx)
	{
		if(obj.checked)
		{
			$("#"+idx).val(1)
			$("#tipoentidades").attr("disabled",true)
		}else{
			$("#"+idx).val(0)
			$("#tipoentidades").attr("disabled",false)
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2">
			<fieldset style="padding:4px">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="8%">Sucursal</td>
				    <td width="3%" align="center">:</td>
				    <td width="33%">
				      <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </td>
				    <td width="6%" align="right">Ciclo</td>
				    <td width="2%" align="center">:</td>
				    <td width="48%">
                    	<? $objDrop->drop_ciclos($codsuc,0,"onchange='datos_facturacion(this.value)'"); ?>
                    </td>
				  </tr>
				  <tr>
				    <td>Sector</td>
				    <td align="center">:</td>
				    <td>
                   	<?php echo $objDrop->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_lecturas(this.value,1);'"); ?></td>
				    <td>&nbsp;</td>
				    <td align="center"><input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosectores'); validar_sectores(this, 'todosectores');" /></td>
				    <td>Todos los Sectores </td>
			      </tr>
				  <tr>
				    <td>Ruta</td>
				    <td align="center">:</td>
				    <td>
                    <div id="div_rutaslecturas">
               	  	<select name="rutaslecturas" id="rutaslecturas" style="width:220px" class="select" disabled="disabled">
                      	<option value="0">--Seleccione la Ruta de Lectura--</option>
                    </select>
                    </div>
                    </td>
				    <td>&nbsp;</td>
				    <td align="center"><input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todosrutas');validar_rutas(this,'todosrutas');" /></td>
				    <td>Todas las Rutas </td>
			      </tr>
				  <tr>
					<tr>
						<td>Grupo</td>
						<td align="center">:</td>
						<td colspan="">
							<div id="div_grupo">
										<? $objDrop->drop_tipo_entidades(); ?>
									</div>
						</td>
					    <td>&nbsp;</td>
							<td align="center"><input type="checkbox" name="chkgrupos" id="chkgrupos" value="checkbox" onclick="CambiarEstado(this,'todosgrupos');validar_grupos(this,'todosgrupos');" /></td>
							<td>Todos Grupos</td>
			      </tr>
					  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td colspan="2">
                        <input type="hidden" name="todosrutas" id="todosrutas" value="1" />
                        <input type="hidden" name="todosectores" id="todosectores" value="1" />
                        <input type="hidden" name="orden" id="orden" value="1" />
												<input type="hidden" name="todosgrupos" id="todosgrupos" value="0" />
                    </td>
				    <td align="center">&nbsp;</td>
				    <td><input type="hidden" name="anio" id="anio" value="0" />
			        <input type="hidden" name="mes" id="mes" value="0" /></td>
			      </tr>
			      <tr>
			      	<td colspan="6">
			      		<div class="buttonset" style="display:inline">
	                           <input type="radio" name="porrutas" id="porrutas" value="radio" checked="checked" onclick="javascript:$('#orden').val('1')" />
	                            <label for="porrutas">Ordenar por Rutas y Secuencia</label>
	                            <input type="radio" name="porrutas" id="porrutasmanzanas" value="radio" onclick="javascript:$('#orden').val('2')" />
	                           <label for="porrutasmanzanas">Ordenar por Rutas y Manzanas</label>

						</div>
			      	</td>
			      </tr>
				 <tr><td>&nbsp;</td></tr>
			      <tr>
        			<td colspan="6">
        				<div id="DivTipos" style="display:inline">
	                           <input type="radio" name="rabresumen" id="rptpdf" value="radio" checked="checked" />
	                            <label for="rptpdf">PDF</label>
	                            <input type="radio" name="rabresumen" id="rptexcel" value="radio2" />
	                           <label for="rptexcel">EXCEL</label>

						</div>
						 <input type="button" onclick="return ValidarForm();" value="Generar" id="">
        			</td>
        		</tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td colspan="2">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				</table>
			</fieldset>
		</td>
	</tr>

	 </tbody>
    </table>
 </form>
</div>
<script>
	$("#codsector").attr("disabled", true)
</script>
<?php CuerpoInferior(); ?>
