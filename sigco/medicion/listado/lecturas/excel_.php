<?php 
	include("../../../../objetos/clsReporte.php");
	include("../../../../objetos/clsReporteExcel.php");
	
	header("Content-type: application/vnd.ms-excel; name='excel'");  
  	header("Content-Disposition: filename=Lecturas.xls"); 
  	header("Pragma: no-cache");  
  	header("Expires: 0"); 
	
  	$objReporte = new clsReporte();
	
  	$codemp		= 1;
	$codsuc 	= $_GET["codsuc"];
	$anio		= $_GET["anio"]; 
	$mes		= $_GET["mes"]; 
	$ciclo		= $_GET["ciclo"];
	$ruta		= $_GET["ruta"];
	$sector		= $_GET["sector"];
	$orden		= $_GET["orden"];
	
	$sect 	= "";
	$rut	= "";

	if($orden == 1)
	{
		$order = " order by c.nroorden  ";
	}
	else
	{
		$order = " order by  c.nroorden ";
	}
	if($_GET["sector"]=='%')
	{
		$order = " order by c.codsector, c.nroorden  ";
	}
	
	$sqlLect = "SELECT DISTINCT l.nroinscripcion, c.propietario, tc.descripcioncorta, cl.descripcion AS calle, c.nrocalle, c.nromed,
				s.descripcion AS sector, rtml.descripcion AS rutas, ".$objReporte->getCodCatastral("c.").", c.codantiguo, t.nomtar
				FROM medicion.lecturas l
				INNER JOIN catastro.clientes as c on(l.codemp=c.codemp and l.codsuc=c.codsuc and l.nroinscripcion=c.nroinscripcion)
				INNER JOIN public.rutaslecturas as rl on(c.codsector=rl.codsector and c.codrutlecturas=rl.codrutlecturas and 
				c.codmanzanas=rl.codmanzanas and c.codemp=rl.codemp and c.codsuc=rl.codsuc)
				INNER JOIN public.rutasmaelecturas rtml on(rl.codrutlecturas=rtml.codrutlecturas and rl.codsector=rtml.codsector
				and rl.codemp=rtml.codemp and rl.codsuc=rtml.codsuc)
				INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
				INNER JOIN public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
				INNER JOIN public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
				INNER JOIN facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
				WHERE l.codemp=? and l.codsuc=? and l.codciclo=? and l.anio=? and l.mes=? ";
	if($sector!="%")
		$sqlLect .= " and c.codsector =".$sector;
	if($ruta!="%")
		$sqlLect .= " and c.codrutlecturas=".$ruta;

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp,
							  $codsuc,
							  $ciclo,
							  $anio,
							  $mes));
	$itemsL = $consultaL->fetchAll();
	?>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php CabeceraExcel(2,7); ?>
	<table class="ui-widget" border="0" cellspacing="0" id="TbTitulo" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:14px">
          
      <tr title="Cabecera">
        <th scope="col" colspan="9" align="center" >
        	LISTADO PARA LA TOMA DE LECTURAS
		</th>
      </tr>
        
        </thead>
        </table>
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
    
    <tr>
        <th width="50" scope="col" class="ui-widget-header">N&deg;</th>
        <th width="200" scope="col" class="ui-widget-header">COD. CATASTRAL</th>
        <th width="200" scope="col" class="ui-widget-header">COD. ANTIGUO</th>
        <th width="50" scope="col" class="ui-widget-header">DIRECCION</th>
        <th width="30" scope="col" class="ui-widget-header">USUARIO</th>
        <th width="50" scope="col" class="ui-widget-header">CAT. </th>
        <th width="50" scope="col" class="ui-widget-header">NRO. MED </th>
        <th width="50" scope="col" class="ui-widget-header">LECTURA</th>
        <th width="50" scope="col" class="ui-widget-header">OBSERVACION</th>
  	</tr>
    </thead>
    <tbody style="font-size:12px">
    <?php
    $count=0;
	foreach($itemsL as $rowL)
	{    $count++;
?>
                              
<tr <?=$title?> <?=$class?> onclick="SeleccionaId(this);" id="<?=$count?>" >
 <td align="left" valign="middle"><? echo $count; ?></td>
    <td align="left" valign="middle" style="mso-number-format:'\@';"><? echo ($rowL["codcatastro"]); ?></td>
    <td align="left" valign="middle" style="mso-number-format:'\@';"><? echo ($rowL['codantiguo']); ?></td>
	<td align="left" valign="middle"><? echo trim($rowL["descripcioncorta"]." ".$rowL["calle"]." ".$rowL["nrocalle"]); ?></td>
	<td align="left" valign="middle"><? echo (trim(strtoupper(utf8_decode($rowL["propietario"])))); ?></td>
	<td align="left" valign="middle"><? echo substr(strtoupper($rowL["nomtar"]),0,3); ?></td>
	<td align="left" valign="middle" style="mso-number-format:'\@';"><? echo ($rowL["nromed"]); ?></td>
	<td align="left" valign="middle"></td>
	<td align="left" valign="middle"></td>

<?php

}
?>
</tbody>

    </tfoot>
</table>