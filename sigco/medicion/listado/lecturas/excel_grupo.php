<?php

// ini_set("display_errors",1);
// error_reporting(E_ALL);

	include("../../../../objetos/clsReporte.php");
	include("../../../../objetos/clsReporteExcel.php");

	header("Content-type: application/vnd.ms-excel; name='excel'");
	header("Content-Disposition: filename=Lecturas_Grupo_Excel.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

  	$objReporte = new clsReporte();

	$codemp		 = 1;
	$codsuc 	 = $_GET["codsuc"];
	$anio		   = $_GET["anio"];
	$mes		   = $_GET["mes"];
	$ciclo		 = $_GET["ciclo"];
	$grupo  = $_GET["grupo"];
	$ruta   = $_GET["ruta"];
	$sector = $_GET["sector"];
	$orden  = $_GET["orden"];

	$sect 	= "";
	$rut	= "";

	// if($orden == 1)
	// {
	// 	$order = " order by c.nroorden  ";
	// }
	// else
	// {
	// 	$order = " order by  c.nroorden ";
	// }
	// if($_GET["sector"]=='%')
	// {
	// 	$order = " order by c.codsector, c.nroorden  ";
	// }

	if($orden == 1)
	{
		//$order = " ORDER BY c.codsector, CAST(c.codrutlecturas AS INTEGER), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER), CAST(c.sublote AS INTEGER)";
		$order = " ORDER BY c.codemp, c.codsuc, c.codsector, c.codrutlecturas, co.orden_dist";
		//$order = " ORDER BY CAST(co.orden_lect AS INTEGER)";

	}else{
		$order = " ORDER BY c.codemp, c.codsuc, c.codsector, c.codrutlecturas, CAST(c.codmanzanas AS INTEGER)";
		//$order = " ORDER BY CAST(co.orden_lect AS INTEGER)";
	}

	$sqlLect  = "SELECT c.codtipoentidades AS grupo, te.descripcion AS entidad, l.nroinscripcion, c.propietario, tc.descripcioncorta, cl.descripcion AS calle, c.nrocalle, ";
	$sqlLect .= " c.nromed, s.descripcion AS sector, rtml.descripcion AS rutas, c.codmanzanas AS manzanas, c.lote, ";
	$sqlLect .= " c.codantiguo, t.nomtar, es.descripcion AS estadoservicio, ";
	$sqlLect .= " ".$objReporte->getCodCatastral("c.").", ";
	$sqlLect .= " c.codsector, CAST(c.codrutlecturas AS INTEGER), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER), ";
	$sqlLect .= " ( c.codsector ||' - '|| c.codrutlecturas ||' - '|| co.orden_lect) AS secrutsecu ";
	$sqlLect .= "FROM medicion.lecturas l ";
	$sqlLect .= " JOIN catastro.clientes c ON (l.codemp = c.codemp) AND (l.codsuc = c.codsuc) AND (l.nroinscripcion = c.nroinscripcion) ";
	$sqlLect .= " JOIN public.rutaslecturas rl ON (c.codemp = rl.codemp) AND (c.codsuc = rl.codsuc) ";
	$sqlLect .= "  AND (c.codsector = rl.codsector) AND (c.codrutlecturas = rl.codrutlecturas) AND (c.codmanzanas = rl.codmanzanas) ";
	$sqlLect .= " JOIN public.rutasmaelecturas rtml ON (rl.codemp = rtml.codemp) AND (rl.codsuc = rtml.codsuc) ";
	$sqlLect .= "  AND (rl.codrutlecturas = rtml.codrutlecturas) AND (rl.codsector = rtml.codsector) ";
	$sqlLect .= " JOIN public.calles cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) ";
	$sqlLect .= "  AND (c.codcalle = cl.codcalle) AND (c.codzona = cl.codzona) ";
	$sqlLect .= " JOIN public.tiposcalle tc ON (cl.codtipocalle = tc.codtipocalle) ";
	$sqlLect .= " JOIN public.sectores s ON (c.codemp = s.codemp) AND (c.codsuc = s.codsuc) AND (c.codsector = s.codsector) ";
	$sqlLect .= " JOIN facturacion.tarifas t ON (c.codemp = t.codemp) AND (c.codsuc = t.codsuc) AND (c.catetar = t.catetar) ";
	$sqlLect .= " JOIN public.estadoservicio es ON (es.codestadoservicio = c.codestadoservicio) ";
	$sqlLect .= " JOIN catastro.conexiones AS co ON(c.codemp=co.codemp AND c.codsuc=co.codsuc AND c.nroinscripcion=co.nroinscripcion) ";
	$sqlLect .= " JOIN tipoentidades AS te ON(c.codtipoentidades = te.codtipoentidades) ";
	$sqlLect .= "WHERE t.estado = 1 AND l.codemp = ".$codemp." AND l.codsuc = ".$codsuc." AND l.codciclo = ".$ciclo." ";
	$sqlLect .= " AND l.anio = '".$anio."' AND l.mes = '".$mes."' AND c.codtipoentidades <> 0 ";
	$sqlLect .= " AND trim(c.nromed) <> '' ";

	if($grupo != '%'){
		$sqlLect .= " AND c.codtipoentidades = " . $grupo;
	};

	if($sector != "%"){
		$sqlLect .= " AND c.codsector = ".$sector;
	}

	if($ruta != "%"){
		$sqlLect .= " AND c.codrutlecturas = ".$ruta;
	}

	$sqlLect .= $order;

	$consultaL = $conexion->query($sqlLect);
	$itemsL = $consultaL->fetchAll();
	?>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php CabeceraExcel(2,7); ?>
	<table class="ui-widget" border="0" cellspacing="0" id="TbTitulo" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:14px">

      <tr title="Cabecera">
        <th scope="col" colspan="9" align="center" >
        	LISTADO PARA LA TOMA DE LECTURAS
		</th>
      </tr>

        </thead>
        </table>
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">

    <tr>
        <th width="80" scope="col" class="ui-widget-header">CODIGO GEOGRAFICO</th>
        <th width="70" scope="col" class="ui-widget-header">S R Sec</th>
        <th width="50" scope="col" class="ui-widget-header">INSCRIPCION</th>
        <th width="200" scope="col" class="ui-widget-header">APELLIDOS Y NOMBRES</th>
        <th width="200" scope="col" class="ui-widget-header">DIRECCION</th>
        <th width="50" scope="col" class="ui-widget-header">N° MEDIDOR</th>
        <th width="50" scope="col" class="ui-widget-header">LECTURA</th>
        <th width="50" scope="col" class="ui-widget-header">TARIFA</th>
        <th width="50" scope="col" class="ui-widget-header">OBSERVACION</th>
  	</tr>
    </thead>
    <tbody style="font-size:12px">
    <?php
		$contador = $contador_medidor = 0;
	foreach($itemsL as $rowL)
	{    $count++;
		$grup = $rowL["grupo"];
		if($grupos != $grup) :
			$nombre_grupos = $rowL['entidad'];

		if(!empty($rowL["nromed"])){
			$contador_medidor++;
		}

?>
<!-- <tr>
	<td colspan="9" >&nbsp;</td>
</tr> -->
<?php
		endif;
?>

<tr <?=$title?> <?=$class?> onclick="SeleccionaId(this);" id="<?=$count?>" >
	<td align="center" valign="middle" style="mso-number-format:'\@';"><? echo $rowL["codcatastro"]; ?></td>
  <td align="center" valign="middle"><? echo $rowL['secrutsecu']; ?></td>
  <td align="center" valign="middle" style="mso-number-format:'\@';"><? echo ($rowL['codantiguo']); ?></td>
	<td align="left" valign="middle"><? echo (trim(strtoupper(utf8_decode($rowL["propietario"])))); ?></td>
	<td align="left" valign="middle"><? echo trim($rowL["descripcioncorta"]." ".$rowL["calle"]." ".$rowL["nrocalle"]); ?></td>
	<td align="center" valign="middle" style="mso-number-format:'\@';"><? echo ($rowL["nromed"]); ?></td>
	<td align="left" valign="middle"></td>
	<td align="center" valign="middle"><? echo substr(strtoupper($rowL["nomtar"]),0,3); ?></td>
	<td align="left" valign="middle"></td>
</tr>
<?php
}
?>
</tbody>
	<tfoot>
		<tr><td colspan="9">&nbsp;</td></tr>
		<tr><td colspan="9">&nbsp;</td></tr>
		<tr><td colspan="9">&nbsp;</td></tr>
		<tr>
			<td colspan="2" align="center">TOTALES</td>
			<td colspan="2" align="center">CANTIDAD</td>
		</tr>
		<tr>
			<td colspan="2" align="left">Usuarios con Medidor</td>
			<td colspan="2" align="right"><?=$contador_medidor?></td>
		</tr>
		<tr>
			<td colspan="2" align="left">TOTAL</td>
			<td colspan="2" align="right"><?=($contador_medidor)?></td>
		</tr>
  </tfoot>
</table>
