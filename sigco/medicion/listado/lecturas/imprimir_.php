<?php
	include("../../../../objetos/clsReporte.php");

	// ini_set("display_errors", 1);
	// error_reporting(E_ALL);

	class clsLecturas extends clsReporte
	{
		function cabecera(){
			global $fechaapertura,$meses,$sectores,$rutas,$Dim;

			$h = 5;
			$this->SetY(6);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);

			//var_dump($this->GetX());
			$this->SetX(60);

			$this->Cell(0, $h,"PADRON DE LECTURA DE MEDIDORES",0,1,'C');
			$this->Ln(15);

			$this->SetFont('Arial','',7);
			$this->Cell(30, $h, "FECHA DE APERTURA",0,0,'R');
			$this->Cell(5, $h, ":",0,0,'C');
			$this->Cell(20, $h, $fechaapertura,0,0,'L');
			$this->Cell(30, $h, utf8_decode(strtoupper($sectores)),0,0,'L');
			$this->Cell(40, $h, utf8_decode(strtoupper($rutas)),0,0,'L');
			$this->Cell(10, $h, "FECHA",0,0,'R');
			$this->Cell(5, $h, ":",0,0,'C');
			$this->Cell(30, $h, "",0,0,'L');
			$this->Cell(10, $h, "LECTOR",0,0,'R');
			$this->Cell(5, $h, ":",0,0,'C');
			$this->Cell(20, $h, "",0,1,'L');

			$this->SetX(5);
			$this->SetFont('Arial','B',7);
			$this->SetWidths(array(30,15,20,60,60,30,20,15,35));
			$this->SetAligns(array("C","C","C","C","C","C","C","C","C","C","C","C"));
			/*
			$this->Row(array(utf8_decode("SECU"),
							 utf8_decode("MANZ"),
							 utf8_decode("LOTE"),
							 utf8_decode("INSCRIPCION"),
							 utf8_decode("USUARIO"),
							 utf8_decode("DIRECCION"),
							 utf8_decode("EST"),
							 utf8_decode("CAT"),
							 utf8_decode("NRO. MED"),
							 utf8_decode("LECT."),
							 utf8_decode("COD. LECT"),
							 utf8_decode("FECHA LECT.")
							//  utf8_decode("OBSERVACION")
						 ));
			*/
			$this->Row(array(utf8_decode("CODIGO GEOGRAFICO"),
							 utf8_decode("S R Sec"),
							 utf8_decode("INSCRIPCION"),
							 utf8_decode("APELLIDOS Y NOMBRES"),
							 utf8_decode("DIRECCION"),
							 utf8_decode("N° MEDIDOR"),
							 utf8_decode("LECTURA"),
							 utf8_decode("TARIFA"),
							 utf8_decode("OBSERVACIONES")
							//  utf8_decode("OBSERVACION")
						 ));
			$this->Ln(1);
			// $this->SetFont('Arial','',9);
		}

		function contenido($nroinscripcion, $codantiguo, $direccion, $propietario, $nromed, $manzana, $lote, $nomtar, $estadoservicio, $secu, $secrutsecu, $codcatastro)
		{
			global $Dim;

			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);

			$h = 5;

			$this->SetFont('Arial','', 8);
			$this->Cell($Dim[1], $h, utf8_decode($codcatastro),0,0,'C',true);
			$this->Cell($Dim[2], $h, utf8_decode($secrutsecu),0,0,'C',true);
			$this->Cell($Dim[3], $h, utf8_decode($codantiguo),0,0,'C',true);
			$this->Cell($Dim[4], $h, strtoupper(trim($propietario)),0,0,'L',true);
			$this->Cell($Dim[5], $h, strtoupper(trim($direccion)),0,0,'L',true);
			$this->Cell($Dim[6], $h, utf8_decode($nromed),0,0,'C',true);
			$this->Cell($Dim[7], $h, utf8_decode("____________"),0,0,'C',true);
			$this->Cell($Dim[8], $h, utf8_decode($nomtar),0,0,'C',true);
			$this->Cell($Dim[9], $h, utf8_decode('________________________'),0,1,'C',true);
		}

		function pie($total_con_medidores){

			global $Dim;

			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);

			$h = 6;

			$this->SetFont('Arial','B',10);
			$this->SetXY($this->GetX(), $this->GetY()+30);
			$this->Cell(120, $h, utf8_decode(""),0,0,'C',true);
			$this->Cell(40, $h, utf8_decode("TOTALES"),0,0,'C',true);
			$this->Cell(3, $h, utf8_decode(""),0,0,'C',true);
			$this->Cell(40, $h, utf8_decode("CANTIDAD"),0,1,'C',true);

			$this->Cell(120, $h, utf8_decode(""),0,0,'C',true);
			$this->Cell(75,.3,"",1,1,'C',true);

			$this->Cell(120, $h, utf8_decode(""),0,0,'C',true);
			$this->Cell(40, $h, utf8_decode("Usuarios con Medidor"),0,0,'L',true);
			$this->Cell(3, $h, utf8_decode(":"),0,0,'C',true);
			$this->Cell(40, $h, ($total_con_medidores),0,1,'C',true);

			$this->Cell(120, $h, utf8_decode(""),0,0,'C',true);
			$this->Cell(75,.3,"",1,1,'C',true);

			$this->SetFont('Arial','B',9);
			$this->Cell(120, $h, utf8_decode(""),0,0,'C',true);
			$this->Cell(43, $h, utf8_decode("TOTAL"),0,0,'L',true);
			$this->Cell(40, $h, ($total_con_medidores+$total_usuarios),0,1,'C',true);

		}


	}

	$Dim = array('1' =>30,'2'=>15,'3'=>20,'4'=>60,'5'=>60,'6'=>30,'7'=>20,'8'=>15,'9'=>35);

	$codemp = 1;
	$anio   = $_GET["anio"];
	$mes    = $_GET["mes"];
	$codsuc = $_GET["codsuc"];
	$ciclo  = $_GET["ciclo"];
	$ruta   = $_GET["ruta"];
	$sector = $_GET["sector"];
	$orden  = $_GET["orden"];
	$grupo  = $_GET["grupo"];

	$sqlfecha   = "SELECT DISTINCT l.fechareg AS fechaapertura FROM medicion.lecturas l ";
	$sqlfecha   .= "WHERE l.codemp = ? AND l.codsuc = ? AND l.codciclo = ? AND l.anio = ? AND l.mes = ?";

	$confecha   = $conexion->prepare($sqlfecha);
	$confecha->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
	$fechaapertura = $confecha->fetchAll();
	$fechaapertura = $fechaapertura[0]['fechaapertura'];

	$objReporte = new clsLecturas("L");

	if($orden == 1)
	{
		//$order = " ORDER BY c.codsector, CAST(c.codrutlecturas AS INTEGER), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER), CAST(c.sublote AS INTEGER)";
		
		/*
		$order = " ORDER BY c.codemp, c.codsuc, c.codsector, c.codrutlecturas, co.orden_dist";
		//$order = " ORDER BY CAST(co.orden_lect AS INTEGER)";
		*/

		$order = " ORDER BY c.codsuc, c.codsector, CAST(c.codmanzanas AS INTEGER ), CAST(c.lote AS INTEGER) ,CAST(c.sublote AS INTEGER)";

	}else{
		$order = " ORDER BY c.codemp, c.codsuc, c.codsector, c.codrutlecturas, CAST(c.codmanzanas AS INTEGER) ";
		//$order = " ORDER BY CAST(co.orden_lect AS INTEGER)";
	}

	// if($_GET["sector"] == '%')
	// {
	// 	//$order = " ORDER BY c.codsector, CAST(c.codrutlecturas AS INTEGER ), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER), CAST(c.sublote AS INTEGER) ";
	// 	$order = " ORDER BY c.codemp, c.codsuc, c.codsector, c.codrutlecturas, co.orden_dist";
	// }

	$sqlLect = "SELECT l.nroinscripcion, c.propietario, tc.descripcioncorta, cl.descripcion AS calle, c.nrocalle, ";
	$sqlLect .= " c.nromed, s.descripcion AS sector, rtml.descripcion AS rutas, c.codmanzanas AS manzanas, c.lote, ";
	$sqlLect .= " c.codantiguo, t.nomtar, es.descripcion AS estadoservicio, ";
	$sqlLect .= " ".$objReporte->getCodCatastral("c.").",";
	$sqlLect .= " c.codsector, CAST(c.codrutlecturas AS INTEGER), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER), ";
	$sqlLect .= " ( c.codsector ||' - '|| c.codrutlecturas ||' - '|| co.orden_lect) AS secrutsecu ";
	$sqlLect .= "FROM medicion.lecturas l ";
	$sqlLect .= " JOIN catastro.clientes c ON (l.codemp = c.codemp) AND (l.codsuc = c.codsuc) AND (l.nroinscripcion = c.nroinscripcion) ";
	$sqlLect .= " JOIN public.rutaslecturas rl ON (c.codemp = rl.codemp) AND (c.codsuc = rl.codsuc) ";
	$sqlLect .= "  AND (c.codsector = rl.codsector) AND (c.codrutlecturas = rl.codrutlecturas) AND (c.codmanzanas = rl.codmanzanas) ";
	$sqlLect .= " JOIN public.rutasmaelecturas rtml ON (rl.codemp = rtml.codemp) AND (rl.codsuc = rtml.codsuc) ";
	$sqlLect .= "  AND (rl.codrutlecturas = rtml.codrutlecturas) AND (rl.codsector = rtml.codsector) ";
	$sqlLect .= " JOIN public.calles cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) ";
	$sqlLect .= "  AND (c.codcalle = cl.codcalle) AND (c.codzona = cl.codzona) ";
	$sqlLect .= " JOIN public.tiposcalle tc ON (cl.codtipocalle = tc.codtipocalle) ";
	$sqlLect .= " JOIN public.sectores s ON (c.codemp = s.codemp) AND (c.codsuc = s.codsuc) AND (c.codsector = s.codsector) ";
	$sqlLect .= " JOIN facturacion.tarifas t ON (c.codemp = t.codemp) AND (c.codsuc = t.codsuc) AND (c.catetar = t.catetar) ";
	$sqlLect .= " JOIN public.estadoservicio es ON (es.codestadoservicio = c.codestadoservicio) ";
	$sqlLect .= " JOIN catastro.conexiones AS co ON(c.codemp=co.codemp AND c.codsuc=co.codsuc AND c.nroinscripcion=co.nroinscripcion)";
	$sqlLect .= "WHERE t.estado = 1 AND l.codemp = ".$codemp." AND l.codsuc = ".$codsuc." AND l.codciclo = ".$ciclo." ";
	$sqlLect .= " AND l.anio = '".$anio."' AND l.mes = '".$mes."' AND trim(c.nromed) <> '' ";
	$sqlLect .= " AND c.codtipoentidades IN(" . $grupo . ")";

	if($sector != "%")
	{
		$sqlLect .= " AND c.codsector = ".$sector;
	}

	if($ruta != "%")
	{
		$sqlLect .= " AND c.codrutlecturas = ".$ruta;
	}

	$sqlLect .= $order;

	// echo $sqlLect;exit;

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array());
	$itemsL = $consultaL->fetchAll();

	$sectores = $itemsL[0]['sector'];
	$rutas    = $itemsL[0]['rutas'];
	$sect     = $itemsL[0]['sector'];
	$rut      = $itemsL[0]['rutas'];

	$contador = $contador_medidor = 0;
	$objReporte->AliasNbPages();
    $objReporte->SetLeftMargin(5);
    $objReporte->SetAutoPageBreak(true,10);
	$objReporte->AddPage('H');

	foreach($itemsL as $rowL)
	{
		$contador++;
		$sectores = $rowL["sector"];
		$rutas    = $rowL["rutas"];
		if($rutas != $rut)
		{
			$objReporte->AddPage('H');
		}

		if(!empty($rowL["nromed"])){
			$contador_medidor++;
		}

		$objReporte->contenido($rowL["nroinscripcion"], $rowL['codantiguo'], $rowL["descripcioncorta"]." ".$rowL["calle"]." ".$rowL["nrocalle"],
							   utf8_decode($rowL["propietario"]), $rowL["nromed"], $rowL["manzanas"], $rowL["lote"],
							   substr(strtoupper($rowL["nomtar"]), 0, 3), substr(strtoupper($rowL['estadoservicio']), 0, 3), $contador, $rowL['secrutsecu'],  $rowL["codcatastro"]);

		$sect  = $sectores;
		$rut   = $rutas;

	}

	$total_usuarios_medidores = $contador_medidor;

	$objReporte->AddPage('H');
	$objReporte->pie($total_usuarios_medidores);

	$objReporte->Output();
?>
