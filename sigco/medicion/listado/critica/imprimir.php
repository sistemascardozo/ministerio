<?php

include("../../../../objetos/clsReporte.php");

$clfunciones = new clsFunciones();

class clsCriticas extends clsReporte {

    function Header() {
        global $codsuc, $meses;
        global $Dim;

        $periodo = $this->DecFechaLiteral();
        //DATOS DEL FORMATO
        $this->SetY(7);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "CRITICA DE LECTURAS";
        $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(277, 5, $periodo["mes"]." - ".$periodo["anio"], 0, 1, 'C');
        $this->Ln(1);
        $this->SetFont('Arial', '', 7);
        $hc = 5;
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
        //DATOS DEL FORMATO
        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoUPPER($empresa["razonsocial"]);
        $tit2 = strtoUPPER($empresa["direccion"]);
        $tit3 = "SUCURSAL: ".strtoUPPER($empresa["descripcion"]);
        $x = 23;
        $y = 5;
        $h = 3;
        $this->Image($_SESSION['path']."images/logo_empresa.jpg", 6, 1, 17, 16);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

        $this->SetY(10);
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');
        $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
        $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
        $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
        $this->SetY(17);
        $this->SetLineWidth(.1);
        //$this->Cell(0, .1, "", 1, 1, 'C', true);
        $this->cabecera();
    }

    function cabecera() {
        global $Dim;

        $this->Ln(5);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);
        $h = 5;

        //$this->SetWidths(array(25,15,40,65,10,23,30,45));
        $this->SetFont('Arial', '', 6.5);
        $this->Cell($Dim[1], $h, utf8_decode('CODIGO'), 1, 0, 'C', false);
        $this->Cell($Dim[12], $h, utf8_decode('S R Sec'), 1, 0, 'C', false);
        $this->Cell($Dim[9], $h, utf8_decode('INSCRIPCION'), 1, 0, 'C', false);
        $this->Cell($Dim[3], $h, utf8_decode('USUARIO'), 1, 0, 'C', false);
        $this->Cell($Dim[2], $h, utf8_decode('DIRECCION'), 1, 0, 'C', false);
        $this->Cell($Dim[5], $h, utf8_decode('NRO. MED'), 1, 0, 'C', false);
        $this->Cell($Dim[8], $h, utf8_decode('PROM.'), 1, 0, 'C', false);
        $this->Cell($Dim[8], $h, utf8_decode('LEC. ANT.'), 1, 0, 'C', false);
        $this->Cell($Dim[7], $h, utf8_decode('LEC. ULT.'), 1, 0, 'C', false);
        $this->Cell($Dim[11], $h, utf8_decode('CONS'), 1, 0, 'C', false);
        $this->Cell($Dim[6], $h, utf8_decode('LEC. CRITICA.'), 1, 0, 'C', false);
        $this->Cell($Dim[4], $h, utf8_decode('TAR.'), 1, 0, 'C', false);
        $this->Cell($Dim[10], $h, utf8_decode('OBSERVACION'), 1, 1, 'C', false);
    }

    function Contenido($catastro, $direccion, $usuario, $categoria, $numeromedida, $lecant, $lectult, $consumo, $estado, $obs, $numeroinscripcion, $consu = 0, $op, $secrutsecu, $estadolect, $lecturapromedio)
    {
        global $Dim;

        $this->SetFillColor(255, 255, 255); //Color de Fondo
        $this->SetTextColor(0);
        $h = 5;
        $this->SetFont('Arial', '', 8);
        if ($op == 0) {
			$this->SetFont('Arial', '', 7);
            $this->Cell($Dim[1], $h, utf8_decode($catastro), 0, 0, 'C', true);
			$this->SetFont('Arial', '', 8);
            $this->Cell($Dim[12], $h, utf8_decode($secrutsecu), 0, 0, 'L', true);
            $this->Cell($Dim[9], $h, utf8_decode($numeroinscripcion), 0, 0, 'C', true);
            $this->Cell($Dim[3], $h, $usuario, 0, 0, 'L', true);
            $this->Cell($Dim[2], $h, utf8_decode($direccion), 0, 0, 'L', true);
            $this->Cell($Dim[5], $h, utf8_decode($numeromedida), 0, 0, 'C', true);
            $this->Cell($Dim[8], $h, number_format($lecturapromedio,0), 0, 0, 'C', true);
            $this->Cell($Dim[8], $h, utf8_decode($lecant), 0, 0, 'C', true);
            $this->Cell($Dim[7], $h, $lecant = (utf8_decode($lectult)) ? $lectult : "____________|", 0, 0, 'C', true);
            $this->Cell($Dim[11], $h, number_format($consu, 0), 0, 0, 'C', true);
            $this->Cell($Dim[6], $h, "_________|", 0, 0, 'C', true);
            $this->Cell($Dim[4], $h, utf8_decode($categoria), 0, 0, 'C', true);
            $this->SetFont('Arial', '', 4);
            $this->Cell($Dim[10], $h, $estadolect, 0, 1, 'L', true);
        } else {
            $this->SetFont('Arial', '', 12);
            $this->SetTextColor(255, 0, 0);
            $this->Cell(286, $h, utf8_decode($catastro), 1, 0, 'C', true);
        }
    }

}

// $Dim = array('1'=>20,'2'=>55,'3'=>60,'4'=>10,'5'=>20,'6'=>22,'7'=>15,'8'=>20,'9'=>30,'10'=>30);
$Dim = array('1' => 22, '2' => 55, '3' => 50, '4' => 10, '5' => 17, '6' => 20, '7' => 14, '8' => 14, '9' => 16, '10' => 30, '11' => 13, '12' => 15);

$codsuc        = $_GET["codsuc"];
$anio          = $_GET["anio"];
$mes           = $_GET["mes"];
$ciclo         = $_GET["ciclo"];
$ruta          = $_GET["ruta"];
$sector        = $_GET["sector"];
$orden         = $_GET["orden"];
$tpmedidor     = $_GET["tpmedidor"];
$estlectura    = $_GET["observacion"];
$rango_consumo = $_GET["rango_consumo"];
$prommax       = $_GET["promedioelevado"];
$prommin       = $_GET["promedioadoelevadomin"];
$valporcmax    = $_GET["valorporcentual"];
$valporcmin    = $_GET["valorporcentualmin"];
$grupos        = $_GET["grupos"];
$sect          = "";
$rut           = "";
$cero          = "";

$prommaxM       = $_GET["promedioelevadom"];
$prommaxMV      = $_GET["promedioelevadomv"];

$LecturaAtipica	= $_GET["LecturaAtipica"];

$topval = (empty($_GET['top'])) ? 6 : $_GET['top'];

	$sql = "SELECT nrofacturacion, nrotarifa ";
	$sql .= "FROM facturacion.periodofacturacion ";
	$sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND codciclo = ".$ciclo." AND anio = '".$anio."' AND mes = '".$mes."' ";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array());
	$row = $consulta->fetch();

	$nrofacturacion = $row['nrofacturacion'];
	$nrotarifa = $row['nrotarifa'];
			
if ($orden == 1) {
    //$ord = " ORDER BY clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER)";
    $ord = " ORDER BY clie.codemp, clie.codsuc, clie.codsector, clie.codrutlecturas, co.orden_dist";
} else {
    //$ord = " ORDER BY clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER)";
    $ord = " ORDER BY  clie.codemp, clie.codsuc, clie.codsector, clie.codrutlecturas, co.orden_dist";
}

if ($_GET["sector"] == '%') {
    //$order = " ORDER BY c.codsector, CAST(c.codrutlecturas AS INTEGER ), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER), CAST(c.sublote AS INTEGER) ";
    $ord = " ORDER BY  clie.codemp, clie.codsuc, clie.codsector, clie.codrutlecturas, co.orden_dist";
}

$estlectura = str_replace("9999,", "", $estlectura);
$estlectura = str_replace("10000,", "", $estlectura);

if ($estlectura == "%" or $estlectura == '') {
    $observacion = "'%".$estlectura."%'";
    $obs = " ";
} else {
    $arraycondicion = explode(",", $estlectura);

    if (in_array("50", $arraycondicion)) {
        $total = count($arraycondicion);

        if ($total == 2) {
            $cero = " AND (lect.lecturaanterior <> lect.lecturaultima OR lect.consumo = 0)";
        } else {
            foreach (array_keys($arraycondicion, '50') as $key) {
                unset($arraycondicion[$key]);
            }

            $estlectura = array();

            foreach ($arraycondicion as $val) {
                if (!empty($val)) {
                    array_push($estlectura, $val);
                }
            }

            array_push($estlectura, 50);

            $estlectura = " ".implode(",", $estlectura).")";
            $obs = " AND lect.codestlectura in ".$estlectura." AND (lect.lecturaanterior <> lect.lecturaultima OR lect.consumo = 0)";
        }
    }

    $observacion = " (".substr($estlectura, 0, strlen($estlectura) - 1).") ";
    $obs = " AND lect.codestlectura IN ".$observacion;
}

$sql = "SELECT DISTINCT clie.nroinscripcion, UPPER(clie.propietario), estser.idestadoservicio, ";
$sql .= " UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,";
$sql .= " lect.nromed, lect.lecturaanterior, lect.lecturaultima, lect.consumo, UPPER(lect.observacion), clie.codsector,";
$sql .= " sect.descripcion as sector, UPPER(rtmae.descripcion), estlect.descripcion AS estalect, lect.codestlectura, lect.codestadomedidor,";
$sql .= " rtmae.codrutlecturas, clie.codemp, clie.codsuc, clie.loteantiguo, lect.lecturapromedio, ".$clfunciones->getCodCatastral("clie.").", t.nomtar, rtmae.descripcion AS rutas,  ";
$sql .= " clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER), ";
$sql .= " ( clie.codsector ||' - '|| clie.codrutlecturas ||' - '|| co.orden_lect) AS secrutsecu, clie.codrutlecturas, co.orden_dist, clie.codantiguo, ";
$sql .= " (t.volumenesp * 2) as asignacion ";
$sql .= "FROM medicion.lecturas as lect ";
$sql .= " INNER JOIN catastro.clientes as clie on(lect.codemp=clie.codemp AND lect.codsuc=clie.codsuc AND lect.nroinscripcion=clie.nroinscripcion) ";
$sql .= " INNER JOIN public.calles as cal on(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona) ";
$sql .= " INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) ";
$sql .= " INNER JOIN public.estadoservicio as estser on(clie.codestadoservicio=estser.codestadoservicio) ";
$sql .= " INNER JOIN public.sectores as sect on(clie.codemp=sect.codemp AND clie.codsuc=sect.codsuc AND clie.codsector=sect.codsector) ";
$sql .= " INNER JOIN public.rutaslecturas as rlect on(clie.codemp=rlect.codemp AND clie.codsuc=rlect.codsuc AND clie.codsector=rlect.codsector AND clie.codrutlecturas=rlect.codrutlecturas AND clie.codmanzanas=rlect.codmanzanas) ";
$sql .= " INNER JOIN public.rutasmaelecturas as rtmae on(rlect.codemp=rtmae.codemp AND rlect.codsuc=rtmae.codsuc AND rlect.codsector=rtmae.codsector AND rlect.codrutlecturas=rtmae.codrutlecturas) ";
$sql .= " INNER JOIN public.estadolectura as estlect on(lect.codestlectura= estlect.codestlectura) ";
$sql .= " INNER JOIN facturacion.tarifas as t on(clie.codemp=t.codemp AND clie.codsuc=t.codsuc AND clie.catetar=t.catetar) ";
$sql .= " INNER JOIN catastro.conexiones AS co ON(clie.codemp=co.codemp AND clie.codsuc=co.codsuc AND clie.nroinscripcion=co.nroinscripcion)";
$sql .= "WHERE clie.codestadoservicio = 1 ";
$sql .= " AND trim(clie.nromed) <> '' ";
$sql .= " AND lect.codsuc = $codsuc ";
$sql .= " AND lect.codciclo = $ciclo ";
$sql .= " AND lect.anio = '$anio' ";
$sql .= " AND lect.mes = '$mes' ";
$sql .= " AND t.nrotarifa = ".$nrotarifa." ";

if($grupos == "0"):

  if ($sector != "%"):
      $sql .= " AND clie.codsector=".$sector;
  endif;

  if ($ruta != "%"):
      $sql .= " AND clie.codrutlecturas=".$ruta;
  endif;

else:

  if($grupos == "%"):
    $sql .= ' AND clie.codtipoentidades NOT IN (0) ';
  else:
    $sql .= ' AND clie.codtipoentidades = ' . $grupos;
  endif;

endif;

if ($tpmedidor != "%")
    $sql .= " AND lect.codestadomedidor=".$tpmedidor;
if ($rango_consumo != "%") {
    $range = explode(",", $rango_consumo);
    $sql .= " AND lect.consumo >= ".$range[0]." AND lect.consumo <= ".$range[1];
}

	if ($LecturaAtipica == 1)
	{
		$sql .= " AND ((lect.consumo > (lect.lecturapromedio * 2)) AND (lect.consumo >= (t.volumenesp * 2)))";
	}

$sql .= $obs.$cero;
//$sql .= "  ORDER BY  clie.codsuc,clie.codsector,CAST(clie.codrutlecturas AS INTEGER ),CAST(clie.codmanzanas AS INTEGER) ,CAST(clie.lote AS INTEGER) ";
$sql .= $ord;

//echo $sql; exit;

$consulta = $conexion->prepare($sql);
$consulta->execute(array());
/*
$consulta->execute(array(":codsuc" => $codsuc,
    ":codciclo" => $ciclo,
    ":anio" => $anio,
    ":mes" => $mes
));
*/
$items = $consulta->fetchAll();
$objReporte = new clsCriticas('L');
$contador = 0;
$objReporte->AliasNbPages();
$objReporte->SetLeftMargin(5);
$objReporte->SetAutoPageBreak(true, 10);
$objReporte->AddPage('H');

$sectores = $items[0]['sector'];
$rutas = $items[0]['rutas'];
$sect = $items[0]['sector'];
$rut = $items[0]['rutas'];

$nomes = date('m');


if (count($items) > 0)
{
    foreach ($items as $row)
	{
        if ($prommax == 1 or $prommin == 1 or $prommaxM == 1)
		{
            $lectuprom = $clfunciones->obtenerpromedio($codsuc, $row['nroinscripcion'], $topval, $nomes);
		}

        $sectores = $row["sector"];
        $rutas = $row["rutas"];

        if($grupos == '0')
		{
          if ($rutas != $rut)
		  {
              //$objReporte->AddPage('H');
		  }
		}

        if ($prommax == 1 or $prommin == 1)
		{
            $lectuprom = $lectuprom + (($lectuprom * $valporcmax) / 100);

            if (($row['consumo'] > $lectuprom) or ($row['consumo'] < $lectuprom))
			{
                $numeroinscripcion = $row['codantiguo'];
                $row['codcatastro'] = substr($row['codcatastro'], 4);
                $objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoUPPER($row["nomtar"]), 0, 3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]), 0, 25), "", $numeroinscripcion, $row['consumo'], 0, $row['secrutsecu'], $row['estalect'], $row['lecturapromedio']);
                $contador++;
			}
		}
        else
		{
			if ($prommaxM == 1)
			{
				$lectupromp = $lectuprom + $prommaxMV;
				
				if ($row['consumo'] > $lectupromp)
				{
					//die((string)$lectupromp);
					
					$numeroinscripcion = $row['codantiguo'];
					$row['codcatastro'] = substr($row['codcatastro'], 4);
					$objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoUPPER($row["nomtar"]), 0, 3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]), 0, 25), "", $numeroinscripcion, $row['consumo'], 0, $row['secrutsecu'], $row['estalect'], $row['lecturapromedio']);
					$contador++;
				}
			}
			else
			{
				$numeroinscripcion = $row['codantiguo'];
				$row['codcatastro'] = substr($row['codcatastro'], 4);
				$objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoUPPER($row["nomtar"]), 0, 3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]), 0, 25), "", $numeroinscripcion, $row['consumo'], 0, $row['secrutsecu'], $row['estalect'], $row['lecturapromedio']);
				$contador++;
			}
		}

		
        $sect = $sectores;
        $rut = $rutas;
        $estlec = $row[13];
	}
}
else
{
    $objReporte->contenido("No existe datos para la consulta establecida", 0, 0, 0, 0, 0, 0, 0, 0, "", 0, 0, 1);
}

$objReporte->Output();
?>
