<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsReporte.php");
include("../../../../objetos/clsReporteExcel.php");
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=PadronUsuarios.xls");
header("Pragma: no-cache");
header("Expires: 0");

$clsFunciones = new clsFunciones();
$objReporte = new clsReporte();
//$clfunciones = new clsFunciones();
//$objReporte = new clsCriticas('L');



/*$codsuc = $_GET["codsuc"];
$codsector = $_GET["sector"];
$tiposervicio = $_GET["tiposervicio"];
$estadoservicio = $_GET["estadoservicio"];
$ciclo = $_GET["ciclo"];
$altocon = $_GET["altocon"];
$Desde = $_GET["Desde"];
$Hasta = $_GET["Hasta"];
$conpozo = $_GET["conpozo"];*/
//$codsector          = $_GET["sector"];
$manzanas           = $_GET["manzanas"];
//$codsuc             = $_GET["codsuc"];
$tiposervicio       = $_GET["tiposervicio"];
$estadoservicio     = $_GET["estadoservicio"];
//$ciclo              = $_GET["ciclo"];
$estadomedidor      = $_GET["estadomedidor"];
$altocon            = $_GET["altocon"];
$Desde              = $_GET["Desde"];
$Hasta              = $_GET["Hasta"];
//$rutai              = $_GET["rutai"];
//$rutaf              = $_GET["rutaf"];
$conpozo            = $_GET["conpozo"];
$periodo            = $clsFunciones->DecFechaLiteral();

$Dim = array('1' => 22, '2' => 55, '3' => 50, '4' => 10, '5' => 17, '6' => 20, '7' => 14, '8' => 14, '9' => 16, '10' => 30, '11' => 13, '12' => 15);

$codsuc        = $_GET["codsuc"];
$anio          = $_GET["anio"];
$mes           = $_GET["mes"];
$ciclo         = $_GET["ciclo"];
$ruta          = $_GET["ruta"];
$sector        = $_GET["sector"];
$orden         = $_GET["orden"];
$tpmedidor     = $_GET["tpmedidor"];
$estlectura    = $_GET["observacion"];
$rango_consumo = $_GET["rango_consumo"];
$prommax       = $_GET["promedioelevado"];
$prommin       = $_GET["promedioadoelevadomin"];
$valporcmax    = $_GET["valorporcentual"];
$valporcmin    = $_GET["valorporcentualmin"];
$grupos        = $_GET["grupos"];
$sect          = "";
$rut           = "";
$cero          = "";
$prommaxM       = $_GET["promedioelevadom"];
$prommaxMV      = $_GET["promedioelevadomv"];
        
        $topval = (empty($_GET['top'])) ? 6 : $_GET['top'];

        if ($orden == 1) 
        {
            //$ord = " ORDER BY clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER)";
            $ord = " ORDER BY clie.codemp, clie.codsuc, clie.codsector, clie.codrutlecturas, co.orden_dist";
        } 
        else 
        {
            //$ord = " ORDER BY clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER)";
            $ord = " ORDER BY  clie.codemp, clie.codsuc, clie.codsector, clie.codrutlecturas, co.orden_dist";
        }

        if ($_GET["sector"] == '%') 
        {
            //$order = " ORDER BY c.codsector, CAST(c.codrutlecturas AS INTEGER ), CAST(c.codmanzanas AS INTEGER), CAST(c.lote AS INTEGER), CAST(c.sublote AS INTEGER) ";
            $ord = " ORDER BY  clie.codemp, clie.codsuc, clie.codsector, clie.codrutlecturas, co.orden_dist";
        }

        $estlectura = str_replace("9999,", "", $estlectura);
        
        if ($estlectura == "%" or $estlectura == '') {
            $observacion = "'%".$estlectura."%'";
            $obs = " ";
        } 
        else
        {
            $arraycondicion = explode(",", $estlectura);

            if (in_array("50", $arraycondicion))
            {
                $total = count($arraycondicion);

                if ($total == 2) 
                {
                    $cero = " AND (lect.lecturaanterior <> lect.lecturaultima OR lect.consumo = 0)";
                } 
                else 
                {
                    foreach (array_keys($arraycondicion, '50') as $key) 
                    {
                        unset($arraycondicion[$key]);
                    }

                    $estlectura = array();

                    foreach ($arraycondicion as $val) 
                    {
                        if (!empty($val)) 
                        {
                            array_push($estlectura, $val);
                        }
                    }

                    array_push($estlectura, 50);

                    $estlectura = " ".implode(",", $estlectura).")";
                    $obs = " AND lect.codestlectura in ".$estlectura." AND (lect.lecturaanterior <> lect.lecturaultima OR lect.consumo = 0)";
                }
            }

            $observacion = " (".substr($estlectura, 0, strlen($estlectura) - 1).") ";
            $obs = " AND lect.codestlectura IN ".$observacion;
        }


        
        $sql = "SELECT DISTINCT clie.nroinscripcion, UPPER(clie.propietario), estser.idestadoservicio, ";
        $sql .= "UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,";
        $sql .= " lect.nromed,lect.lecturaanterior,lect.lecturaultima,lect.consumo,UPPER(lect.observacion),clie.codsector,";
        $sql .= " sect.descripcion as sector, UPPER(rtmae.descripcion), estlect.descripcion AS estalect, lect.codestlectura, lect.codestadomedidor,";
        $sql .= " rtmae.codrutlecturas, clie.codemp, clie.codsuc, clie.loteantiguo, lect.lecturapromedio, ".$objReporte->getCodCatastral("clie.").", t.nomtar, rtmae.descripcion AS rutas,  ";
        $sql .= " clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER), ";
        $sql .= " ( clie.codsector ||' - '|| clie.codrutlecturas ||' - '|| co.orden_lect) AS secrutsecu, clie.codrutlecturas, co.orden_dist, clie.codantiguo";
        $sql .= " FROM medicion.lecturas as lect ";
        $sql .= " INNER JOIN catastro.clientes as clie on(lect.codemp=clie.codemp AND lect.codsuc=clie.codsuc AND lect.nroinscripcion=clie.nroinscripcion) ";
        $sql .= " INNER JOIN public.calles as cal on(clie.codemp=cal.codemp AND clie.codsuc=cal.codsuc AND clie.codcalle=cal.codcalle AND clie.codzona=cal.codzona) ";
        $sql .= " INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) ";
        $sql .= " INNER JOIN public.estadoservicio as estser on(clie.codestadoservicio=estser.codestadoservicio) ";
        $sql .= " INNER JOIN public.sectores as sect on(clie.codemp=sect.codemp AND clie.codsuc=sect.codsuc AND clie.codsector=sect.codsector) ";
        $sql .= " INNER JOIN public.rutaslecturas as rlect on(clie.codemp=rlect.codemp AND clie.codsuc=rlect.codsuc AND clie.codsector=rlect.codsector AND clie.codrutlecturas=rlect.codrutlecturas AND clie.codmanzanas=rlect.codmanzanas) ";
        $sql .= " INNER JOIN public.rutasmaelecturas as rtmae on(rlect.codemp=rtmae.codemp AND rlect.codsuc=rtmae.codsuc AND rlect.codsector=rtmae.codsector AND rlect.codrutlecturas=rtmae.codrutlecturas) ";
        $sql .= " INNER JOIN public.estadolectura as estlect on(lect.codestlectura= estlect.codestlectura) ";
        $sql .= " INNER JOIN facturacion.tarifas as t on(clie.codemp=t.codemp AND clie.codsuc=t.codsuc AND clie.catetar=t.catetar) ";
        $sql .= " INNER JOIN catastro.conexiones AS co ON(clie.codemp=co.codemp AND clie.codsuc=co.codsuc AND clie.nroinscripcion=co.nroinscripcion)";
        $sql .= " WHERE t.estado=1 AND clie.codestadoservicio=1 AND trim(clie.nromed)<>'' AND lect.codsuc=$codsuc AND lect.codciclo=$ciclo AND lect.anio='$anio' AND lect.mes='$mes' ";
        //$ord = " ORDER BY  clie.codsector, CAST(clie.codmanzanas as integer)";
        $ord = "ORDER BY clie.codemp, clie.codsuc, clie.codsector, clie.codrutlecturas, co.orden_dist";
        //echo $sql.$ord;exit;
        //die($sql);

        if($grupos == "0")
        {
            if ($sector != "%")
            {
                $sql .= " AND clie.codsector=".$sector;
            }

            if ($ruta != "%")
            {
                $sql .= " AND clie.codrutlecturas=".$ruta;
            }
        }
        else
        {
            if($grupos == "%")
            {
                $sql .= ' AND clie.codtipoentidades NOT IN (0) ';
            }
            else
            {
                $sql .= ' AND clie.codtipoentidades = ' . $grupos;
            }
        }

        if ($tpmedidor != "%")
        {
            $sql .= " AND lect.codestadomedidor=".$tpmedidor;
        }

        if ($rango_consumo != "%") 
        {
            $range = explode(",", $rango_consumo);
            $sql .= " AND lect.consumo >= ".$range[0]." AND lect.consumo <= ".$range[1];
        }

        $sql .= $obs.$cero;
        //$sql .= "  ORDER BY  clie.codsuc,clie.codsector,CAST(clie.codrutlecturas AS INTEGER ),CAST(clie.codmanzanas AS INTEGER) ,CAST(clie.lote AS INTEGER) ";
        $sql .= $ord;
        //echo $sql; exit;


        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());

        //$count = 0;
        //$consulta = $conexion->prepare($sql);
        //$consulta->execute(array($codsuc));

        $items = $consulta->fetchAll();
        //$objReporte = new clsCriticas('L');
        $contador = 0;
        //$objReporte->AliasNbPages();
        //$objReporte->SetLeftMargin(5);
        //$objReporte->SetAutoPageBreak(true, 10);
        //$objReporte->AddPage('H');
        //$distrito=$items[0]['distrito'];
        //$sector=$items[0]['sector'];
        //$manzana=$items[0]['manzanas'];

        $sectores = $items[0]['sector'];
        $rutas = $items[0]['rutas'];
        $sect = $items[0]['sector'];
        $rut = $items[0]['rutas'];
        $nomes = date('m');

CabeceraExcel(2, 14); 
?>
        <tr>
            <table class="ui-widget" border="1" cellspacing="1" id="TbTitulo" width="100%" rules="rows">
            <thead class="ui-widget-header" style="font-size:14px">
            <tr title="Cabecera">
            <th scope="col" colspan="14" align="center" >CRITICA DE LECTURAS</th>
            </tr>
            <tr>
            <th colspan="14"><?=$periodo["mes"]." - ".$periodo["anio"] ?> </th>
            </tr> 
            </thead>
            <tbody style="font-size:12px">
        
<?php

       //echo count($items);
        if (count($items) > 0)
        {
?>
                
            <tr>
            <th width="50" scope="col" class="ui-widget-header">CODIDO</th>
            <th width="50" scope="col" class="ui-widget-header">S R Sec</th>
            <th width="50" scope="col" class="ui-widget-header">NRO. INSC.</th>
            <th width="50" scope="col" class="ui-widget-header">USUARIO</th>
            <th width="50" scope="col" class="ui-widget-header">DIRECCION</th>
            <th width="50" scope="col" class="ui-widget-header">NRO. MED</th>
            <th width="50" scope="col" class="ui-widget-header">PROM.</th>
            <th width="50" scope="col" class="ui-widget-header">LEC. ANT.</th>
            <th width="50" scope="col" class="ui-widget-header">LEC. ULT.</th>
            <th width="50" scope="col" class="ui-widget-header">CONS</th>
            <th width="50" scope="col" class="ui-widget-header">LEC. CRITICA</th>
            <th width="50" scope="col" class="ui-widget-header">TAR</th>
            <th width="50" scope="col" class="ui-widget-header" colspan="2">OBSERVACION</th>         
            </tr>
<?php
            foreach ($items as $row)
            {
                if ($prommax == 1 OR $prommin == 1)
                {
                        //$lectuprom = $clfunciones->obtenerpromedio($codsuc, $row['nroinscripcion'], $topval, $nomes);
/*?>
                <tr>
                    <th colspan="4"><?=$codsuc?></th>
                    <th colspan="4"><?=$nroinscripcion?></th>
                    <th colspan="8"><?=$topval?></th>
                    <th colspan="4"><?=$nomes?></th>
                </tr> 
<?php **/
                }

                $sectores = $row["sector"];
                $rutas = $row["rutas"];

                if($grupos == '0' )
                {
                    if ($rutas != $rut)
                    {
                        //$objReporte->AddPage('H');
                    }
                }

                if ($prommax == 1 OR $prommin == 1)
                {
                    $lectuprom = $lectuprom + (($lectuprom * $valporcmax) / 100);

                    if (($row['consumo'] > $lectuprom) OR ( $row['consumo'] < $lectuprom))
                    {
                        $numeroinscripcion = $row['codantiguo'];
                        $row['codcatastro'] = substr($row['codcatastro'], 4);
                            
                            //$objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoUPPER($row["nomtar"]), 0, 3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]), 0, 25), "", $numeroinscripcion, $row['consumo'], 0, $row['secrutsecu'], $row['estalect'], $row['lecturapromedio']);
?>
                        <tr>
                            <td align="center" valign="middle"><?=($row['codcatastro']); ?></td>
                            <td align="center" valign="middle"><?=($row['secrutsecu']); ?></td>
                            <td align="center" valign="middle"><?=($row["nroinscripcion"]); ?></td>
                            <td align="left" valign="middle"><?=(utf8_decode($row[1])); ?></td>
                            <td align="left" valign="middle"><?=($row[3]); ?></td>
                            <td align="center" valign="middle"><?=($row[4]); ?></td>
                            <td align="center" valign="middle"><?=intval($row['lecturapromedio']); ?></td>
                            <td align="center" valign="middle"><?=intval($row['lecturaanterior']); ?></td>
                            <td align="center" valign="middle"><?=intval($row['lecturaultima']); ?></td>
                            <td align="center" valign="middle"><?=intval($row['consumo']); ?></td>
                            <td align="center" valign="middle">____________</td>
                            <td align="center" valign="middle"><?=substr(strtoUPPER($row["nomtar"]), 0, 3); ?></td> 
                            <td align="center" valign="middle" colspan="2"><?=substr(trim($row[12]), 0, 25); ?></td> 
                        </tr>
<?php
                        $contador++;
                    }
                    else
                    {
                        if ($prommaxM == 1)
                        {
                           $lectupromp = $lectuprom + $prommaxMV;
                           
                           if ($row['consumo'] > $lectupromp)
                           {
                                $numeroinscripcion = $row['codantiguo'];
                                $row['codcatastro'] = substr($row['codcatastro'], 4);
                                //$objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoUPPER($row["nomtar"]), 0, 3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]), 0, 25), "", $numeroinscripcion, $row['consumo'], 0, $row['secrutsecu'], $row['estalect'], $row['lecturapromedio']);
?>
                                <tr>
                                    <td align="center" valign="middle"><?=($row['codcatastro']); ?></td>
                                    <td align="center" valign="middle"><?=($row['secrutsecu']); ?></td>
                                    <td align="center" valign="middle"><?=($row["nroinscripcion"]); ?></td>
                                    <td align="left" valign="middle"><?=(utf8_decode($row[1])); ?></td>
                                    <td align="left" valign="middle"><?=($row[3]); ?></td>
                                    <td align="center" valign="middle"><?=($row[4]); ?></td>
                                    <td align="center" valign="middle"><?=intval($row['lecturapromedio']); ?></td>
                                    <td align="center" valign="middle"><?=intval($row['lecturaanterior']); ?></td>
                                    <td align="center" valign="middle"><?=intval($row['lecturaultima']); ?></td>
                                    <td align="center" valign="middle"><?=intval($row['consumo']); ?></td>
                                    <td align="center" valign="middle">____________</td>
                                    <td align="center" valign="middle"><?=substr(strtoUPPER($row["nomtar"]), 0, 3); ?></td> 
                                    <td align="center" valign="middle" colspan="2"><?=substr(trim($row[12]), 0, 25); ?></td> 
                                </tr>
<?php
                                $contador++;
                           } 
                        }
                        else
                        {
                            $numeroinscripcion = $row['codantiguo'];
                            $row['codcatastro'] = substr($row['codcatastro'], 4);
                            //$objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoUPPER($row["nomtar"]), 0, 3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]), 0, 25), "", $numeroinscripcion, $row['consumo'], 0, $row['secrutsecu'], $row['estalect'], $row['lecturapromedio']);
?>
                                <tr>
                                    <td align="center" valign="middle"><?=($row['codcatastro']); ?></td>
                                    <td align="center" valign="middle"><?=($row['secrutsecu']); ?></td>
                                    <td align="center" valign="middle"><?=($row["nroinscripcion"]); ?></td>
                                    <td align="left" valign="middle"><?=(utf8_decode($row[1])); ?></td>
                                    <td align="left" valign="middle"><?=($row[3]); ?></td>
                                    <td align="center" valign="middle"><?=($row[4]); ?></td>
                                    <td align="center" valign="middle"><?=intval($row['lecturapromedio']); ?></td>
                                    <td align="center" valign="middle"><?=intval($row['lecturaanterior']); ?></td>
                                    <td align="center" valign="middle"><?=intval($row['lecturaultima']); ?></td>
                                    <td align="center" valign="middle"><?=intval($row['consumo']); ?></td>
                                    <td align="center" valign="middle">____________</td>
                                    <td align="center" valign="middle"><?=substr(strtoUPPER($row["nomtar"]), 0, 3); ?></td> 
                                    <td align="center" valign="middle" colspan="2"><?=substr(trim($row[12]), 0, 25); ?></td> 
                                </tr>
<?php
                            $contador++;
                        }

                    }
                    $sect = $sectores;
                    $rut = $rutas;
                    $estlec = $row[13];
                }
            }
        }
?>
    </tbody>
</table>