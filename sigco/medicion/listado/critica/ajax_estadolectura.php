<?php
include("../../../../config.php");

$opcion = $_POST["opcion"];

$sql = "select * from estadolectura where estareg=1 order by codestlectura";
$consulta = $conexion->prepare($sql);
$consulta->execute();
$items = $consulta->fetchAll();
?>
<script>
    function cargarCodigo(obj, codigo, idx)
    {
        if (obj.checked)
        {
            $("#codigo" + idx).val(codigo)
            $("#op" + idx).val(1)
        } else {
            $("#codigo" + idx).val(0)
            $("#op" + idx).val(0)
        }
    }
</script>
<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetpagos" rules="all" >
    <tr class="ui-widget-header" >
        <td width="50" align="center">Codigo</td>
        <td>Estado de Lectura</td>
        <td width="70">&nbsp;</td>
    </tr>
    <?php
    $count = 0;
    if ($opcion == 0) {
        foreach ($items as $row) {
            $count++;

            $click = "onclick='cargarCodigo(this,".$row["codestlectura"].",".$count.")";
            $click.=";cargarValor(this,".$row["codestlectura"].")'";

            echo "<tr>
                    <td align='center'>
                    <input type='hidden' id='codigo".$count."' name='codigo".$count."' value='0' />
                    <input type='hidden' id='op".$count."' name='op".$count."' value='0' />
                    ".$row["codestlectura"]."</td>
                    <td style='padding-left:5px;'>".$row["descripcion"]."</td>
                    <td align='center'>
                        <input type='checkbox' id='chkseleccionar".$count."' name='chkseleccionar".$count."' ".$click." />
                    </td>
                  </tr>";
        }
		
		$count++;
		
		$click = "onclick='cargarCodigo(this, 9999, ".$count.")";
		$click.=";cargarValor(this, 9999)'";
			
		echo "<tr>
				<td align='center'>
				<input type='hidden' id='codigo".$count."' name='codigo".$count."' value='0' />
				<input type='hidden' id='op".$count."' name='op".$count."' value='0' />
				9999</td>
				<td style='padding-left:5px;'>CONSUMO EXCESIVO x M3 MAYORES A </td>
				<td align='center'>
					<input type='checkbox' id='chkseleccionar".$count."' name='chkseleccionar".$count."' ".$click." />
				</td>
			  </tr>";

    } else {
        echo "<tr>
                <td style='padding:4px' align='center' colspan='3'>VER TODAS LAS OBSERVACIONES</td>
              </tr>";
    }
    ?>
</table>
<input type="hidden" name="contador" id="contador" value="<?= $count ?>" />
