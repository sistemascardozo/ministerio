<?php
    session_name("pnsu");
    if(!session_start()){session_start();}

    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
    

    $TituloVentana = "CRITICAS DE LECTURAS";
    $Activo = 1;
    $codtipomedidor = 0;
    CuerpoSuperior($TituloVentana);
    $codsuc = $_SESSION['IdSucursal'];

    $objDrop = new clsDrop();

    $sucursal = $objDrop->setSucursales(" WHERE codemp=1 and codsuc=?", $codsuc);

    $Fecha = date('d/m/Y');

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="../../operaciones/digitacion/js_digitacion.js" language="JavaScript"></script>
<script>
    var c = 0
    var codsuc = <?=$codsuc ?>

    jQuery(function ($)
    {

        $("#DivTipos").buttonset();

    });

    function ValidarForm(Op)
    {
        var ciclos = $("#ciclo").val()
        var rutas = ""
        var sectores = ""
        var observacion = ""

        if ($("#ciclo").val() == 0)
        {
            alert('Seleccione el Ciclo')
            $("#codsector").focus()
            return
        }
        if ($("#codsector").val() == 0 && document.getElementById("chksectores").checked == false)
        {
            alert('Seleccione el Sector')
            $("#codsector").focus()
            return
        }
        if ($("#todosrutas").val() == 1)
        {
            rutas = "%"
        } else {
            rutas = $("#rutaslecturas").val()
            if (rutas == 0)
            {
                alert("Seleccione la Ruta de Lectura")
                return false
            }
        }
        if ($("#todosectores").val() == 1)
        {
            sectores = "%"
        } else {
            sectores = $("#codsector").val()
            if (sectores == 0)
            {
                alert("Seleccione el Sector")
                return false
            }
        }
        if ($("#todosgrupos").val() == 1)
        {
            grupos = "%"
        } else {
            grupos = $("#tipoentidades").val()
        }

        if ($("#todotipomedidor").val() == 1)
        {
            tipomedidor = "%"
        } else {
            tipomedidor = $("#codestadomedidor").val()
            if (tipomedidor == 10000)
            {
                alert("Seleccione el Tipo de Medidor")
                return false
            }
        }
        if ($("#active_rangoconsumo").val() == 0) {
            rango_consumo = "%";
        } else {
            consumo_ini = $("#div_rangoconsumo input#consumo_ini").val();
            consumo_fin = $("#div_rangoconsumo input#consumo_fin").val();
            if (isNaN(consumo_ini) || isNaN(consumo_fin)) {
                alert("Indique un Rango de Consumo valido.");
                return false
            } else if (consumo_ini > consumo_fin) {
                alert("El valor del campo Desde debe ser menor o igual que el valor del campo Hasta");
                return false
            }
            rango_consumo = consumo_ini + "," + consumo_fin;
        }

        if ($("#todasobservaciones").val() == 1)
        {
            observacion = "%"
        } else {
            for (i = 1; i <= $("#contador").val(); i++)
            {
                if ($("#op" + i).val() == 1)
                {
                    observacion += $("#codigo" + i).val() + ",";
                }
            }
        }

        if ($("#valorporcentualmax").val() > 100)
        {
            alert('El valor porcentual no debe exeder del 100%')
            $("#valorporcentualmax").focus()
            return
        } else {
            var valorporcentualmax = $("#valorporcentualmax").val()
            if (isNaN(valorporcentualmax)) {
                valorporcentualmax = 0;
            }
        }
        if ($("#valorporcentualmin").val() > 100)
        {
            alert('El valor porcentual no debe exeder del 100%')
            $("#valorporcentualmin").focus()
            return
        } else {
            var valorporcentualmin = $("#valorporcentualmin").val()
            if (isNaN(valorporcentualmin)) {
                valorporcentualmin = 0;
            }
        }

        promedioadoelevado = $("#promedioelevado").val();
        promedioadoelevadomin = $("#promedioelevadomin").val();
		
		promedioadoelevadoM = $("#promedioelevadoM").val();
		
		valorporcentualmaxM = 0;
		
		LecturaAtipica = $("#active_atipicas").val();

		
        if (document.getElementById("criticalocal").checked == true)
        {
            url = "imprimir.php?";
        }
        if (document.getElementById("criticacampo").checked == true)
        {
            url = "imprimir_campo.php?";
        }
        if (document.getElementById("rptexcel").checked == true)
        {
            url = "excel.php?";
        }

        url +="codsuc=<?=$codsuc ?>&anio=" + $("#anio").val() + "&mes=" + $("#mes").val() + "&ciclo=" + $("#ciclo").val();
        url +="&ruta=" + rutas + "&sector=" + sectores + '&tpmedidor=' + tipomedidor;
        url +="&orden=" + $("#orden").val() + "&rango_consumo=" + rango_consumo + '&observacion=' + observacion + '&promedioelevado=' + promedioadoelevado + '&promedioadoelevadomin=' + promedioadoelevadomin + '&valorporcentual=' + valorporcentualmax + '&valorporcentualmin=' + valorporcentualmin;
        url +="&grupos="+grupos + '&promedioelevadom=' + promedioadoelevadoM + '&promedioelevadomv=' + valorporcentualmaxM + '&LecturaAtipica=' + LecturaAtipica;
        AbrirPopupImpresion(url, 800, 600)

        return false;
    }
    function Cancelar()
    {
        location.href = '<?=$urldir ?>/admin/indexB.php'
    }
    function cargarobservaciones()
    {
        $.ajax({
            url: 'ajax_estadolectura.php',
            type: 'POST',
            async: true,
            data: 'opcion=' + $("#todasobservaciones").val(),
            success: function (datos) {
                $("#div_observaciones").html(datos)
            }
        })
    }

    function cargarValor(id, idestlectura) {
        if (idestlectura == 1) {
            if (id.checked == true) {
                document.getElementById("promedioelevado").value = 1
                $(id).after('<input class="entero" type="text" name="valorporcentualmax" id="valorporcentualmax" style="width:50">');
                $("#valorporcentualmax").css("display", "block");
                $("#valorporcentualmax").val("60");

            } else {
                document.getElementById("promedioelevado").value = 0
                $("#valorporcentualmax").css("display", "none");
                $("#valorporcentualmax").val("0");
                $("#valorporcentualmax").remove();
            }
        }
        if (idestlectura == 8) {
            if (id.checked == true) {
                document.getElementById("promedioelevadomin").value = 1
                $(id).after('<input class="entero" type="text" name="valorporcentualmin" id="valorporcentualmin" style="width:50">');
                $("#valorporcentualmin").css("display", "block");
                $("#valorporcentualmin").val("40");

            } else {
                document.getElementById("promedioelevadomin").value = 0
                $("#valorporcentualmin").css("display", "none");
                $("#valorporcentualmin").val("0");
                $("#valorporcentualmin").remove();
            }
        }
		
		if (idestlectura == 9999) {
            if (id.checked == true) {
                document.getElementById("promedioelevadoM").value = 1
                $(id).after('<input class="entero" type="text" name="valorporcentualmaxM" id="valorporcentualmaxM" style="width:50">');
                $("#valorporcentualmaxM").css("display", "block");
                $("#valorporcentualmaxM").val("60");

            } else {
                document.getElementById("promedioelevadoM").value = 0
                $("#valorporcentualmax").css("display", "none");
                $("#valorporcentualmax").val("0");
                $("#valorporcentualmax").remove();
            }
        }
    }

    function CambObservaciones(obj)
    {
        if (obj.checked == true)
        {
            document.getElementById("todasobservaciones").value = 1
        } else {
            document.getElementById("todasobservaciones").value = 0
        }
        cargarobservaciones()
    }

    function validar_rangoconsumo(obj) {
        if (obj.checked)
        {
            $("#div_rangoconsumo").show();
        } else {
            $("#div_rangoconsumo").hide().find('input').val('');

        }
    }

    function validar_estadomedidor(obj)
    {
        if (obj.checked)
        {
            $("#codestadomedidor").attr("disabled", true)
        } else {
            $("#codestadomedidor").attr("disabled", false)

        }
    }

    function validar_grupo(obj)
    {
        if (obj.checked)
        {
            $("#tipoentidades").attr("disabled", true)
        } else {
            $("#tipoentidades").attr("disabled", false)

        }
    }


</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
        <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <fieldset style="padding:4px">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="110">Sucursal</td>
                                    <td width="20" align="center">:</td>
                                    <td colspan="3">
                                        <label>
                                            <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ciclo </td>
                                    <td align="center">:</td>
                                    <td><?php $objDrop->drop_ciclos($codsuc,0,"onchange='datos_facturacion(this.value)'"); ?></td>
                                    <td width="20" align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Mes :</td>
                                    <td align="center">:</td>
                                    <td>
                                        <select id="mes" name="mes" class="select" style="width:220px">
                                            <option value="1">Enero</option>
                                            <option value="2">Febrero</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Abril</option>
                                            <option value="5">Mayo</option>
                                            <option value="6">Junio</option>
                                            <option value="7">Julio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Setiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>
                                        </select>
                                    </td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Sector</td>
                                    <td align="center">:</td>
                                    <td><?php echo $objDrop->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_lecturas(" . $codsuc . ",this.value,1);'"); ?></td>
                                    <td align="center"><input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosectores');
                                            validar_sector(this);" /></td>
                                    <td>Todos los Sectores </td>
                                </tr>
                                <tr>
                                    <td>Ruta</td>
                                    <td align="center">:</td>
                                    <td>
                                        <div id="div_rutaslecturas">
                                            <select name="rutaslecturas" id="rutaslecturas" style="width:220px" class="select" disabled="disabled">
                                                <option value="0">--Seleccione la Ruta de Lectura--</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td align="center"><input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosrutas');
                                            validar_rutas(this);" /></td>
                                    <td>Todas las Rutas</td>
                                </tr>
                                <tr>
                                    <td>Grupo</td>
                                    <td align="center">:</td>
                                    <td colspan="">
                        							<div id="div_grupo">
                        										<? $objDrop->drop_tipo_entidades(); ?>
                        									</div>
                        						</td>
                                    <td align="center"><input type="checkbox" name="chkgrupos" id="chkgrupos" value="checkbox" onclick="CambiarEstado(this, 'todosgrupos'); validar_grupo(this)" /></td>
                                    <td>Todos los Grupos</td>
                                </tr>
                                <tr>
                                    <td>Estado de Medidor</td>
                                    <td align="center">:</td>
                                    <td><?php echo $objDrop->drop_tipomedidor($codtipomedidor, 10000, ""); ?></td>

                                    <td align="center"><input type="checkbox" name="chkestmed" id="chkestmed" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todotipomedidor');
                                            validar_estadomedidor(this);" /></td>
                                    <td>Todas los estados de Medidor </td>
                                </tr>
                                <tr>
                                    <td>Consumo</td>
                                    <td align="center">:</td>
                                    <td>
                                        <input type="checkbox" name="chkconsumo" id="chkconsumo" value="checkbox" onclick="CambiarEstado(this, 'active_rangoconsumo');
                                                validar_rangoconsumo(this);" />
                                        Por Rango de Consumo?
                                        <input type="hidden" id="active_rangoconsumo" name="active_rangoconsumo" value="0">
                                    </td>
                                    <td colspan="2">
                                        <div id="div_rangoconsumo" style="display:none">
                                            Desde: <input type="text" id="consumo_ini" size="8">
                                            Hasta: <input type="text" id="consumo_fin" size="8">
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                  <td>Lecturas Atípicas</td>
                                  <td align="center">:</td>
                                  <td><input type="checkbox" name="chkatipicas" id="chkatipicas" value="checkbox" onclick="CambiarEstado(this, 'active_atipicas');" />
                                  Lecturas Atípicas
                                        <input type="hidden" id="active_atipicas" name="active_atipicas" value="0"></td>
                                  <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Observacion</td>
                                    <td align="center">:</td>
                                    <td colspan="1">
                                        <input type="checkbox" name="chkobservaciones" id="chkobservaciones" value="checkbox" checked="checked" onclick="CambObservaciones(this)" />
                                        Todas las Observaciones
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3" style="padding-top: 4px">
                                        <div id="div_observaciones">
                                            &nbsp;
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3" style="padding-top: 4px">
                                        <div id="div_observaciones">
                                            &nbsp;
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Imprimir en Pantalla</td>
                                    <td align="center">:</td>
                                    <td colspan="3">
                                        <div class="buttonset" style="display:inline">
                                            <input type="radio" name="rtcritica" id="criticalocal" value="radio" checked="checked" />
                                            <label for="criticalocal">Gabinete</label>
                                            <input type="radio" name="rtcritica" id="rptexcel" value="radio" />
                                            <label for="rptexcel">Excel</label>
                                            <input type="radio" name="rtcritica" id="criticacampo" value="radio" />
                                            <label for="criticacampo">Campo</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="center">
                                        <input type="button" onclick="return ValidarForm();" value="Generar" id="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3" style="padding-top: 4px">
                                        <div id="div_observaciones">
                                            &nbsp;
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>
                                        <input type="hidden" name="todosectores" id="todosectores" value="1" />
                                        <input type="hidden" name="orden" id="orden" value="1" />
                                        <input type="hidden" name="todasobservaciones" id="todasobservaciones" value="1" />
                                        <input type="hidden" name="promedioelevado" id="promedioelevado" value="0" />
                                        <input type="hidden" name="promedioelevadomin" id="promedioelevadomin" value="0" />
                                        <input type="hidden" name="todosrutas" id="todosrutas" value="1" />
                                        <input type="hidden" name="todosgrupos" id="todosgrupos" value="0" />
                                        <input type="hidden" name="todotipomedidor" id="todotipomedidor" value="1" />
                                        
                                        <input type="hidden" name="promedioelevadoM" id="promedioelevadoM" value="0" />
                                    </td>
                                    <td align="center">&nbsp;</td>
                                    <td>
                                        <input type="hidden" name="anio" id="anio" value="0" />
                                        <!--<input type="hidden" name="mes" id="mes" value="0" />-->
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    $("#codsector").attr("disabled", true)
    $("#chkconsumo").attr("checked", false);
    $("#valorporcentual").val("60");
    cargarobservaciones();
</script>
<?php CuerpoInferior(); ?>
