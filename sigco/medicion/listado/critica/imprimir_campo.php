<?php

include("../../../../objetos/clsReporte.php");
$clfunciones = new clsFunciones();

class clsCriticas extends clsReporte {

    function Header() {

        global $codsuc, $meses;
        global $Dim;

        $periodo = $this->DecFechaLiteral();
        //DATOS DEL FORMATO
        $this->SetY(7);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "CRITICA DE LECTURAS";
        $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(277, 5, $periodo["mes"]." - ".$periodo["anio"], 0, 1, 'C');
        $this->Ln(1);
        $this->SetFont('Arial', '', 7);
        $hc = 5;
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
        //DATOS DEL FORMATO
        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoupper($empresa["razonsocial"]);
        $tit2 = strtoupper($empresa["direccion"]);
        $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
        $x = 23;
        $y = 5;
        $h = 3;
        $this->Image($_SESSION['path']."images/logo_empresa.jpg", 6, 1, 17, 16);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

        $this->SetY(10);
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');
        $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
        $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
        $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
        $this->SetY(17);
        $this->SetLineWidth(.1);
        //$this->Cell(0, .1, "", 1, 1, 'C', true);
        $this->cabecera();
    }

    function cabecera() {
        global $Dim;

        $this->Ln(5);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);
        $h = 5;

        //$this->SetWidths(array(25,15,40,65,10,23,30,45));
        $this->SetFont('Arial', '', 7);
        $this->Cell($Dim[1], $h, utf8_decode('CODIGO'), 1, 0, 'L', false);
        $this->Cell($Dim[9], $h, utf8_decode('INSCRIPCION'), 1, 0, 'L', false);
        $this->Cell($Dim[2], $h, utf8_decode('DIRECCION'), 1, 0, 'C', false);
        $this->Cell($Dim[3], $h, utf8_decode('USUARIO'), 1, 0, 'C', false);
        $this->Cell($Dim[4], $h, utf8_decode('CAT'), 1, 0, 'C', false);
        $this->Cell($Dim[5], $h, utf8_decode('NRO. MED'), 1, 0, 'C', false);
        $this->Cell($Dim[8], $h, utf8_decode('LEC. ANT.'), 1, 0, 'C', false);
        $this->Cell($Dim[7], $h, utf8_decode('LEC. ULT.'), 1, 0, 'C', false);
        $this->Cell($Dim[6], $h, utf8_decode('LEC. CRITICA.'), 1, 0, 'C', false);
        $this->Cell($Dim[10], $h, utf8_decode('OBSERVACION'), 1, 1, 'C', false);
    }

    function Contenido($catastro, $direccion, $usuario, $categoria, $numeromedida, $lecant, $lectult, $consumo, $estado, $obs, $numeroinscripcion, $op) {
        global $Dim;

        $this->SetFillColor(255, 255, 255); //Color de Fondo
        $this->SetTextColor(0);
        $h = 3;
        $this->SetFont('Arial', '', 10);
        if ($op == 0) {
            $this->Cell($Dim[1], $h, utf8_decode($catastro), 0, 0, 'L', true);
            $this->Cell($Dim[9], $h, utf8_decode($numeroinscripcion), 0, 0, 'L', true);
            $this->Cell($Dim[2], $h, $direccion, 0, 0, 'L', true);
            $this->Cell($Dim[3], $h, $usuario, 0, 0, 'L', true);
            $this->Cell($Dim[4], $h, utf8_decode($categoria), 0, 0, 'C', true);
            $this->Cell($Dim[5], $h, utf8_decode($numeromedida), 0, 0, 'C', true);
            $this->Cell($Dim[8], $h, utf8_decode($lecant), 0, 0, 'C', true);
            $this->Cell($Dim[7], $h, $lecant = (utf8_decode($lectult)) ? $lectult : "____________|", 0, 0, 'C', true);
            $this->Cell($Dim[6], $h, "_________|", 0, 0, 'C', true);
            // $this->Cell($Dim[9],$h,utf8_decode($estado),0,0,'C',true);
            $this->Cell($Dim[10], $h, $lecant = (utf8_decode($obs)) ? $obs : "______________", 0, 1, 'L', true);
        } else {
            $this->SetFont('Arial', '', 12);
            $this->SetTextColor(255, 0, 0);
            $this->Cell(286, $h, utf8_decode($catastro), 1, 0, 'C', true);
        }
    }

}

// $Dim = array('1'=>20,'2'=>55,'3'=>60,'4'=>10,'5'=>20,'6'=>22,'7'=>15,'8'=>20,'9'=>30,'10'=>30);
$Dim = array('1' => 25, '2' => 55, '3' => 70, '4' => 10, '5' => 20, '6' => 22, '7' => 15, '8' => 20, '9' => 20, '10' => 30);

$codsuc = $_GET["codsuc"];
$anio = $_GET["anio"];
$mes = $_GET["mes"];
$ciclo = $_GET["ciclo"];
$ruta = $_GET["ruta"];
$sector = $_GET["sector"];
$orden = $_GET["orden"];
$tpmedidor = $_GET["tpmedidor"];
$estlectura = $_GET["observacion"];
$rango_consumo = $_GET["rango_consumo"];
$prommax = $_GET["promedioelevado"];
$prommin = $_GET["promedioadoelevadomin"];
$valporcmax = $_GET["valorporcentual"];
$valporcmin = $_GET["valorporcentualmin"];
$grupos        = $_GET["grupos"];
$sect = "";
$rut = "";
$cero = "";


$topval = (empty($_GET['top'])) ? 6 : $_GET['top'];



if ($orden == 1) {
    $ord = "ORDER BY clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER)";
} else {
    $ord = "ORDER BY clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER)";
}
if ($estlectura == "%") {
    $observacion = "'%".$estlectura."%'";
    $obs = " ";
} else {

    $arraycondicion = explode(",", $estlectura);
    if (in_array("50", $arraycondicion)) :

        $total = count($arraycondicion);
        if ($total == 2) :
            $cero = " AND lect.lecturaanterior <> lect.lecturaultima  AND lect.consumo = 0";
        else:
            foreach (array_keys($arraycondicion, '50') as $key) :
                unset($arraycondicion[$key]);
            endforeach;

            $estlectura = array();

            foreach ($arraycondicion as $val) :
                if (!empty($val)):
                    array_push($estlectura, $val);
                endif;
            endforeach;

            array_push($estlectura, 50);
            $estlectura = "(".implode(",", $estlectura).")";
            $obs = " AND lect.codestlectura in ".$estlectura." AND lect.lecturaanterior <> lect.lecturaultima  AND lect.consumo = 0";
        endif;
    else:
        $observacion = "(".substr($estlectura, 0, strlen($estlectura) - 1).") ";
        $obs = " AND lect.codestlectura in ".$observacion;
    endif;
}

$sql = "SELECT DISTINCT clie.nroinscripcion,upper(clie.propietario),estser.idestadoservicio,";
$sql .= "upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,";
$sql .= "lect.nromed,lect.lecturaanterior,lect.lecturaultima,lect.consumo,upper(lect.observacion),clie.codsector,";
$sql .= "sect.descripcion as sector,upper(rtmae.descripcion),estlect.descripcion,lect.codestlectura,lect.codestadomedidor,";
$sql .= "rtmae.codrutlecturas,clie.codemp,clie.codsuc,clie.loteantiguo,lect.lecturapromedio, ".$clfunciones->getCodCatastral("clie.").", t.nomtar, rtmae.descripcion AS rutas,  ";
$sql .= " clie.codsector, CAST(clie.codrutlecturas AS INTEGER), CAST(clie.codmanzanas AS INTEGER), CAST(clie.lote AS INTEGER), clie.codantiguo";
$sql .= "FROM medicion.lecturas as lect ";
$sql .= "INNER JOIN catastro.clientes as clie on(lect.codemp=clie.codemp and lect.codsuc=clie.codsuc and ";
$sql .= "lect.nroinscripcion=clie.nroinscripcion) ";
$sql .= "INNER JOIN public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona) ";
$sql .= "INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) ";
$sql .= "INNER JOIN public.estadoservicio as estser on(clie.codestadoservicio=estser.codestadoservicio) ";
$sql .= "INNER JOIN public.sectores as sect on(clie.codemp=sect.codemp and clie.codsuc=sect.codsuc and clie.codsector=sect.codsector) ";
$sql .= "INNER JOIN public.rutaslecturas as rlect on(clie.codemp=rlect.codemp and clie.codsuc=rlect.codsuc and clie.codsector=rlect.codsector ";
$sql .= "and clie.codrutlecturas=rlect.codrutlecturas and clie.codmanzanas=rlect.codmanzanas) ";
$sql .= "INNER JOIN public.rutasmaelecturas as rtmae on(rlect.codemp=rtmae.codemp and rlect.codsuc=rtmae.codsuc and rlect.codsector=rtmae.codsector ";
$sql .= "and rlect.codrutlecturas=rtmae.codrutlecturas) ";
$sql .= "INNER JOIN public.estadolectura as estlect on(lect.codestlectura=estlect.codestlectura)
            INNER JOIN facturacion.tarifas as t on(clie.codemp=t.codemp and clie.codsuc=t.codsuc and clie.catetar=t.catetar) ";

$sql .= "WHERE clie.codestadoservicio=1 AND lect.codestlectura<>0 AND trim(clie.nromed)<>'' AND lect.codsuc=:codsuc and lect.codciclo=:codciclo and lect.anio=:anio and lect.mes=:mes ";

if($grupos == "0"):

  if ($sector != "%"):
      $sql .= " AND clie.codsector=".$sector;
  endif;

  if ($ruta != "%"):
      $sql .= " AND clie.codrutlecturas=".$ruta;
  endif;

else:

  if($grupos == "%"):
    $sql .= ' AND clie.codtipoentidades NOT IN (0) ';
  else:
    $sql .= ' AND clie.codtipoentidades = ' . $grupos;
  endif;

endif;


if ($tpmedidor != "%")
    $sql .= " and lect.codestadomedidor=".$tpmedidor;
if ($rango_consumo != "%") {
    $range = explode(",", $rango_consumo);
    $sql .= " and lect.consumo >= ".$range[0]." and lect.consumo <= ".$range[1];
}
$sql .= $obs.$cero;
$sql .= "  ORDER BY  clie.codsuc,clie.codsector,CAST(clie.codrutlecturas AS INTEGER ),CAST(clie.codmanzanas AS INTEGER) ,CAST(clie.lote AS INTEGER) ";

$consulta = $conexion->prepare($sql);
$consulta->execute(array(":codsuc" => $codsuc,
    ":codciclo" => $ciclo,
    ":anio" => $anio,
    ":mes" => $mes
));

$items = $consulta->fetchAll();
$objReporte = new clsCriticas('L');
$contador = 0;
$objReporte->AliasNbPages();
$objReporte->SetLeftMargin(5);
$objReporte->SetAutoPageBreak(true, 10);
$objReporte->AddPage('H');

$sectores = $items[0]['sector'];
$rutas = $items[0]['rutas'];
$sect = $items[0]['sector'];
$rut = $items[0]['rutas'];

$nomes = date('m');


if (count($items) > 0) :

    foreach ($items as $row):

        if ($prommax == 1 OR $prommin == 1):
            $lectuprom = $clfunciones->obtenerpromedio($codsuc, $row['nroinscripcion'], $topval, $nomes);
        endif;

        $sectores = $row["sector"];
        $rutas = $row["rutas"];

        if ($rutas != $rut):
            $objReporte->AddPage('H');
        endif;

        if ($prommax == 1 OR $prommin == 1):

            $lectuprom = $lectuprom + (($lectuprom * $valporcmax) / 100);

            if (($row['consumo'] > $lectuprom) OR ( $row['consumo'] < $lectuprom)):
                //$numeroinscripcion = $clfunciones->CodUsuario($codsuc, $row['nroinscripcion']);
                $numeroinscripcion = $row['codantiguo'];
                $row['codcatastro'] = substr($row['codcatastro'], 4);
                $objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoupper($row["nomtar"]), 0, 3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]), 0, 25), "", $numeroinscripcion, 0);
                $contador++;
            endif;
        // else{
        //     if($prommin == 1){
        //         $lectuprom = $lectuprom - (($lectuprom*$valporcmin)/100);
        //         if ($row['consumo'] < $lectuprom) {
        //             $numeroinscripcion = $clfunciones->CodUsuario($codsuc,$row['nroinscripcion']);
        //             $row['codcatastro'] = substr($row['codcatastro'],4);
        //             $objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoupper($row["nomtar"]),0,3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]),0,25), "", $numeroinscripcion,0);
        //             $contador++;
        //         }
        //     }
        //     else{
        //         $numeroinscripcion = $clfunciones->CodUsuario($codsuc,$row['nroinscripcion']);
        //         $row['codcatastro'] = substr($row['codcatastro'],4);
        //         $objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoupper($row["nomtar"]),0,3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]),0,25), "", $numeroinscripcion,0);
        //                 $contador++;
        //     }
        // }

        else :
            // if($prommin == 1):
            //     $lectuprom = $lectuprom - (($lectuprom*$valporcmin)/100);
            //     if ($row['consumo'] < $lectuprom) :
            //         $numeroinscripcion = $clfunciones->CodUsuario($codsuc,$row['nroinscripcion']);
            //         $row['codcatastro'] = substr($row['codcatastro'],4);
            //         $objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoupper($row["nomtar"]),0,3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]),0,25), "", $numeroinscripcion,0);
            //         $contador++;
            //     endif;
            // else:
            //$numeroinscripcion = $clfunciones->CodUsuario($codsuc, $row['nroinscripcion']);
            $numeroinscripcion = $row['codantiguo'];
            $row['codcatastro'] = substr($row['codcatastro'], 4);
            $objReporte->contenido($row["codcatastro"], $row[3], utf8_decode($row[1]), substr(strtoupper($row["nomtar"]), 0, 3), $row[4], intval($row[5]), intval($row[6]), intval($row[7]), substr(trim($row[12]), 0, 25), "", $numeroinscripcion, 0);
            $contador++;
        endif;
        $sect = $sectores;
        $rut = $rutas;
        $estlec = $row[13];

    endforeach;
else :
    $objReporte->contenido("No existe datos para la consulta establecida", 0, 0, 0, 0, 0, 0, 0, 0, "", 0, 1);
endif;
$objReporte->Output();
?>
