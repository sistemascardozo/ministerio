<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsReporte.php");
	
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $NroInscripcion, $codsuc, $objreporte, $conexion, $Dim;
			
			$h=4;
			$this->SetFont('Arial','B',12);
			$this->Cell(0, $h+1,utf8_decode("HISTÓRICO DE LECTURAS"),0,1,'C');

			$Sql ="select clie.nroinscripcion,".$objreporte->getCodCatastral("clie.").", 
			clie.propietario,td.abreviado,clie.nrodocumento,
			tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
			es.descripcion,tar.nomtar,clie.codantiguo, clie.tipofacturacion
			from catastro.clientes as clie 
			inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
			inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
			inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
			inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
			WHERE clie.codemp=1 AND clie.codsuc=".$codsuc." AND clie.nroinscripcion=".$NroInscripcion."";
			$Consulta = $conexion->query($Sql);
			$row = $Consulta->fetch();

			switch ($row['tipofacturacion']) 
		    {
		        case 0:
		        $TipoFac='CONSUMO LEIDO';
		        break;
		        case 1:
		        $TipoFac='CONSUMO PROMEDIADO';
		        break;
		        case 2:
		        $TipoFac='CONSUMO ASIGNADO';
		        break;
		    }

			$this->SetLeftMargin(30);
			$this->Ln(5);

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h, utf8_decode("N° de Inscripcion"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h, $row['codantiguo'],0,0,'L');

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h, utf8_decode("Código Catastral"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h, $row[1],0,1,'L');
			/*-------------*/
		

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h, utf8_decode($row['abreviado']),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h, $row['nrodocumento'],0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h, utf8_decode("Cliente"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h, utf8_decode($row['propietario']), 0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Dirección"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h,strtoupper($row['direccion']),0,0,'L');

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Tipo de Facturacion"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$TipoFac,0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Estado"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h,$row['descripcion'],0,0,'L');

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,'Categoria',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$row['nomtar'],0,1,'L');
			
			$this->Ln(5);
			$this->SetX(10);
			$this->SetFont('Arial','B',8);
	 	 	$this->Cell($Dim[1],5,utf8_decode('N°'),1,0,'C',false);
	 	 	$this->Cell($Dim[2],5,utf8_decode('Periodo'),1,0,'C',false);
			$this->Cell($Dim[3],5,utf8_decode('N° Medidor'),1,0,'C',false);
			$this->Cell($Dim[4],5,utf8_decode('Fecha Lectura'),1,0,'C',false);
			$this->Cell($Dim[5],5,utf8_decode('Lectura'),1,0,'C',false);
			$this->Cell($Dim[6],5,utf8_decode('Cons.'),1,0,'C',false);
			$this->Cell($Dim[6],5,utf8_decode('Esti.'),1,0,'C',false);
			$this->Cell($Dim[6],5,utf8_decode('Prom.'),1,0,'C',false);
			$this->Cell($Dim[7],5,utf8_decode('Tipo Lectura'),1,0,'C',false);
			$this->Cell($Dim[8],5,utf8_decode('Est.Med.'),1,1,'C',false);
	
				 	 	 	 	 	
			
			
		}
        
		function Detalle()
		{
			global $NroInscripcion,$codsuc,$objreporte,$conexion,$Dim,$meses;
			$Condicion="WHERE nroinscripcion =".$NroInscripcion." AND codsuc=".$codsuc;
			$Orden=" ORDER BY fechalectultima DESC ";				
		   $Sql = "SELECT l.codemp, l.anio,  l.mes,l.nromed,l.fechalectultima,  l.lecturaultima,
			  			l.consumo,  l.tipofacturacion,l.lecturapromedio,l.lecturaestimada,
			  			e.descripcion as estadomedidor,l.codestadomedidor
					FROM  medicion.lecturas l 
					INNER JOIN public.estadomedidor e ON (l.codestadomedidor=e.codestadomedidor)
					  ".$Condicion.$Orden;   

					  	$Consulta =$conexion->query($Sql);
			$c=0;
			$Deuda=0;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);
			$this->SetLeftMargin(10);
			$this->SetFont('Arial','',8);
			foreach($Consulta->fetchAll() as $row)
			{	
				$c++;
				$TipoFac='';
				switch ($row['tipofacturacion']) 
					{
					    case 0:
					       $TipoFac='CONSUMO LEIDO';
					    break;
					    case 1:
					       $TipoFac='CONSUMO PROMEDIADO';
					    break;
					    case 2:
					        $TipoFac='CONSUMO ASIGNADO';
					    break;
					    
					}
				if($row['codestadomedidor']==0) $row['estadomedidor']='ACTIVO';
				$this->SetX(10);
				$this->Cell($Dim[1],5,$c,1,0,'C',true);
		 	 	$this->Cell($Dim[2],5,$meses[$row['mes']]." - ".$row['anio'],1,0,'C',true);
				$this->Cell($Dim[3],5,$row['nromed'],1,0,'C',true);
				$this->Cell($Dim[4],5,$objreporte->DecFecha($row['fechalectultima']),1,0,'C',true);
				$this->Cell($Dim[5],5,intval($row['lecturaultima']),1,0,'C',true);
				$this->Cell($Dim[6],5,intval($row['consumo']),1,0,'R',true);
				$this->Cell($Dim[6],5,intval($row['lecturaestimada']),1,0,'R',true);
				$this->Cell($Dim[6],5,intval($row['lecturapromedio']),1,0,'R',true);
				
				$this->Cell($Dim[7],5,utf8_decode($TipoFac),1,0,'L',true);
				$this->Cell($Dim[8],5,$row['estadomedidor'],1,1,'L',true);

			}
			//$this->SetFont('Arial','B',10);
			//$this->Cell(array_sum($Dim)-$Dim[9],5,'Deuda Total:',0,0,'R',true);
			//$this->Cell($Dim[9],5,number_format($Deuda,2),0,1,'R',true);
			
		}
    
    }
    
	define('Cubico', chr(179));
	
	$codsuc	= $_SESSION["IdSucursal"];
	$NroInscripcion = $_GET["NroInscripcion"];
	$Dim = array('1'=>8,'2'=>27,'3'=>20,'4'=>20,'5'=>15,'6'=>14,'7'=>30,'8'=>30);
    $x = 20;
    $h=4;
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("P");
	$objreporte->Detalle("P");
	
	
	

	
	$objreporte->Output();	
	
?>