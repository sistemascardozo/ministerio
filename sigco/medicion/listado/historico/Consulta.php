<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsDrop.php");
    
    $objMantenimiento = new clsDrop();
    $Presicion = $_SESSION["Presicion"];
    $codsuc = $_SESSION["IdSucursal"];
    $NroInscripcion = $_POST["NroInscripcion"];
    
    $Sql = "SELECT clie.nroinscripcion,".$objMantenimiento->getCodCatastral("clie.").", 
        clie.propietario,td.abreviado,clie.nrodocumento,
        tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
        es.descripcion,tar.nomtar,clie.codantiguo,clie.tipofacturacion
        FROM catastro.clientes as clie 
        inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
        inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
        inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
        inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
        inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
        WHERE clie.codemp=1 AND clie.codsuc=".$codsuc." AND clie.nroinscripcion=".$NroInscripcion."";
    $Consulta = $conexion->query($Sql);
    $row = $Consulta->fetch();
    switch ($row['tipofacturacion']) 
    {
        case 0:
        $TipoFac='CONSUMO LEIDO';
        break;
        case 1:
        $TipoFac='CONSUMO PROMEDIADO';
        break;
        case 2:
        $TipoFac='CONSUMO ASIGNADO';
        break;
    }
?>

<table class="ui-widget" border="0" cellspacing="0" width="100%" rules="rows" align="center">
    <thead style="font-size:12px;">	
        <tr>
            <td><span class="Negrita">N&deg; de Inscripcion:&nbsp;</span>&nbsp;<?= $row['codantiguo'] ?></td>
            <td><span class="Negrita">C&oacute;digo Catastral:</span> <?= $row[1] ?></td>
        </tr>
        <tr>
          
            <td><span class="Negrita"><?= $row['abreviado'] ?>:</span> <?= $row['nrodocumento'] ?></td>
        </tr>
        <tr>
            <td colspan="2" align="left"><span class="Negrita">Cliente:</span> <?= $row['propietario'] ?></td>

        </tr>
        <tr>
            <td align="left"><span class="Negrita">Direcci&oacute;n :</span><?= strtoupper($row['direccion']) ?></td>
            <td align="left"><span class="Negrita">Tipo de Facturaci&oacute;n :</span><?= $TipoFac ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">Estado:</span> <?= $row['descripcion'] ?></td>
            <td><span class="Negrita">Categoria:</span> <?= $row['nomtar'] ?></td>
        </tr>
    </thead>
</table>
<table class="ui-widget" border="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
        <tr>
            <th width="10" scope="col">Item</th>
            <th width="100" scope="col">Periodo</th>
            <th width="100" scope="col">N&deg; Medidor</th>
            <th width="50" scope="col">Fecha Lectura</th>
            <th width="50" scope="col">Lectura</th>
            <th width="50" scope="col">Consumo</th>
            <th width="50" scope="col">Esti.</th>
            <th width="50" scope="col">Prom.</th>
            <th width="100" scope="col">Tipo Lectura</th>
            <th width="100" scope="col">Estado Medidor</th>
            <th scope="col" width="10" align="center">&nbsp;</th>

        </tr>
    </thead>
    <tbody style="font-size:12px">
        <?php
            $Condicion="WHERE nroinscripcion =".$NroInscripcion." AND codsuc=".$codsuc;
            $Orden=" ORDER BY fechalectultima DESC ";

            $Sql = "SELECT l.codemp, l.anio,  l.mes,l.nromed,l.fechalectultima,  l.lecturaultima,
                        l.consumo,  l.tipofacturacion,l.lecturapromedio,l.lecturaestimada,
                        e.descripcion as estadomedidor,l.codestadomedidor
                    FROM  medicion.lecturas l 
                    INNER JOIN public.estadomedidor e ON (l.codestadomedidor=e.codestadomedidor)
                      ".$Condicion.$Orden;   

            $Consulta =$conexion->query($Sql);
            $c=0;
            $Deuda=0;
            foreach($Consulta->fetchAll() as $row)
            {	
                $c++;

                $TipoFac='';
                switch ($row['tipofacturacion']) 
                {
                case 0:
                $TipoFac='CONSUMO LEIDO';
                break;
                case 1:
                $TipoFac='CONSUMO PROMEDIADO';
                break;
                case 2:
                $TipoFac='CONSUMO ASIGNADO';
                break;

                }
                if($row['codestadomedidor']==0) $row['estadomedidor']='ACTIVO';
        ?>

        <tr <?= $title ?> <?= $class ?> onclick="SeleccionaId(this);" id="<?= $c ?>" >
            <td align="center"><?= $c ?></td>
            <td align="left" valign="middle"><?= $meses[$row['mes']]." - ".$row['anio'] ?></td>
            <td align="center" valign="middle"><?= $row['nromed'] ?></td>
            <td align="center" valign="middle"><?= $objMantenimiento->DecFecha($row['fechalectultima']) ?></td>
            <td align="center" valign="middle"><?= intval($row['lecturaultima']) ?></td>
            <td align="center" valign="middle"><?= intval($row['consumo']) ?></td>
            
            <td align="center" valign="middle"><?= intval($row['lecturaestimada']) ?></td>
            <td align="center" valign="middle"><?= intval($row['lecturapromedio']) ?></td>
            <td align="left" valign="middle"><?= $TipoFac ?></td>
            <td align="center" valign="middle"><?= $row['estadomedidor'] ?></td>
            <td>&nbsp;</td>
        </tr>

        <?php
        }
        ?>
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td colspan="11" align="right" >&nbsp;</td>
        </tr>
    </tfoot>
</table>