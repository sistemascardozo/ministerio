<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;

	$objMantenimiento 	= new clsDrop();

	if($Id!='')
	{
		$consulta   = $conexion->prepare($objMantenimiento->Sentencia("categoriatarifaria")." where codcategoriatar=?");
		$consulta->execute(array($Id));
		$row 		= $consulta->fetch();		
		
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if($("#tipocategoriatarifaria").val()==0)
		{
			alert("Seleccione el Tipo de Categoria Tarifaria")
			return false			
		}
		if ($("#Nombre").val() == '')
		{
			alert('La Descripcion del Concepto no puede ser NULO');
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
   </script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td colspan="4" class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
        <td class="TitDetalle">Id</td>
        <td align="center" class="TitDetalle">:</td>
        <td colspan="4" class="CampoDetalle">
		<input name="codcategoriatar" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/></td>
	</tr>
	
	<tr>
	  <td class="TitDetalle">Tipo Categ.</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td colspan="4" class="CampoDetalle">
      	<?php $objMantenimiento->drop_tipo_categoria_tarifaria($row["codtipcattar"]); ?>
      </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td colspan="4" class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Nombre" maxlength="200" value="<?=$row["descripcion"]?>" style="width:400px;"/></td>
	  </tr>
	
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td colspan="4" class="CampoDetalle">
	  	<? include("../../../../include/estareg.php"); ?> 
      </td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td colspan="4" class="CampoDetalle">&nbsp;</td>
	   </tr>
	 
          </tbody>
	 
	</table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>