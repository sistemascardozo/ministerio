<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codtipcattar 	= $_POST["codtipcattar"];
	$descripcion	= $_POST["descripcion"];
	$estareg		= $_POST["estareg"];
	

	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("tipocategoriatarifaria","0","0");
			$codtipcattar = $id[0];
			
			$sql = "insert into public.tipocategoriatarifaria(codtipcattar,descripcion,estareg) values(:codtipcattar,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipcattar"=>$codtipcattar,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipocategoriatarifaria set descripcion=:descripcion, estareg=:estareg 
			where codtipcattar=:codtipcattar";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipcattar"=>$codtipcattar,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipocategoriatarifaria set estareg=:estareg
				where codtipcattar=:codtipcattar";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipcattar"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
