<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	 
	include("../../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsMantenimiento();

	if($Id != '')
	{
		$Select  = "SELECT * FROM public.tipoocurrencia where codtipoocurrencia = ".$Id;
		$row=$conexion->query($Select)->fetch();
		$Estado = $row['estado'];
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if ($("#descripcion").val() == '')
		{
			alert('La Descripcion del Tipo Estado Servicio del Medidor no puede ser NULO');
			$("#descripcion").focus();
			return false;
		}
		
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
	<table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr><td>&nbsp;</td>
			  <td width="30">&nbsp;</td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
				<td class="TitDetalle">Id</td>
				<td align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<input name="codtipoocurrencia" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
				</td>
			</tr>      
			<tr>
				<td class="TitDetalle">Descripcion</td>
				<td align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="descripcion" type="text" id="descripcion" maxlength="200" value="<?php echo $row["descripcion"];?>" style="width:400px;"/>
				</td>
			</tr>
			<tr>
				<td class="TitDetalle">Estado</td>
				<td align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<?php include("../../../../include/estareg.php"); ?>
				</td>
			</tr>
			<tr>
				<td class="TitDetalle">&nbsp;</td>
				<td class="TitDetalle">&nbsp;</td>
				<td class="CampoDetalle">&nbsp;</td>
			</tr>

		</tbody>

	</table>
 </form>
</div>
<script>
 $("#descripcion").focus();
</script>
<?php
	$est = isset($inspectores["estareg"])?$inspectores["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>