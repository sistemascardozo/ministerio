<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codtipoconcepto 	= $_POST["codtipoconcepto"];
	$descripcion		= strtoupper($_POST["descripcion"]);
	$estareg			= $_POST["estareg"];
        
	
	switch ($Op) 
	{
		case 0:
			$id   				= $objFunciones->setCorrelativos("tipoconceptos","0","0");
			$codtipoconcepto = $id[0];
			
			$sql = "insert into public.tipoconceptos(codtipoconcepto,descripcion,estareg) values(:codtipoconcepto,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoconcepto"=>$codtipoconcepto,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update public.tipoconceptos set descripcion=:descripcion, estareg=:estareg 
			where codtipoconcepto=:codtipoconcepto";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoconcepto"=>$codtipoconcepto,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update public.tipoconceptos set estareg=:estareg
				where codtipoconcepto=:codtipoconcepto";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codtipoconcepto"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
