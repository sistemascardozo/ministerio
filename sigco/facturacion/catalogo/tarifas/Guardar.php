<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	$codemp = 1;
	$codsuc 		= $_SESSION['IdSucursal'];
	$idusuario		= $_SESSION['id_user'];
	
	$nrotarifa 			= $_POST["nrotarifa"];
	
	$catetar 			= $_POST["catetar"];
	$categoriatarifaria	= $_POST["categoriatarifaria"];
	$nomtar				= strtoupper($_POST["nomtar"]);
	$volumenmin			= $_POST["volumenmin"];
	$volumenesp			= $_POST["volumenesp"];
	$impcargofijo1		= $_POST["impcargofijo1"];
	$impcargofijo2		= $_POST["impcargofijo2"];
	$impcargofijo3		= $_POST["impcargofijo3"];
	$hastarango1		= $_POST["hastarango1"];
	$impconsmin			= $_POST["impconsmin"];
	$impconsmindesa		= $_POST["impconsmindesa"];
	$hastarango2		= $_POST["hastarango2"];
	$impconsmedio		= $_POST["impconsmedio"];
	$impconsmediodesa	= $_POST["impconsmediodesa"];
	$hastarango3		= $_POST["hastarango3"];
	$impconsexec		= $_POST["impconsexec"];
	$impconsexecdesa	= $_POST["impconsexecdesa"];
	$estareg			= $_POST["estareg"];
	$diametrosagua	= $_POST["diametrosagua"];
	
	switch ($Op) 
	{
		case 0:
			$id   		= $objFunciones->setCorrelativos("tarifas",$codsuc,"0");
			$catetar 	= $id[0];
		
			$sql = "INSERT INTO facturacion.tarifas(codemp,codsuc,catetar,codcategoriatar,nomtar,volumenmin,volumenesp,impconsmin,
				impconsmedio,impconsexec,hastarango1,hastarango2,hastarango3,estareg,impcargofijo1,impconsmindesa,impconsexecdesa,
				impconsmediodesa,codusu,impcargofijo2,impcargofijo3,coddiametrosagua, nrotarifa) 
				values(:codemp,:codsuc,:catetar,:codcategoriatar,:nomtar,:volumenmin,:volumenesp,:impconsmin,
				:impconsmedio,:impconsexec,:hastarango1,:hastarango2,:hastarango3,:estareg,:impcargofijo,:impconsmindesa,
				:impconsexecdesa,:impconsmediodesa,:codusu,:impcargofijo2,:impcargofijo3,:coddiametrosagua, :nrotarifa)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":catetar"=>$catetar,
						   ":codcategoriatar"=>$categoriatarifaria,
						   ":nomtar"=>$nomtar,
						   ":volumenmin"=>$volumenmin,
						   ":volumenesp"=>$volumenesp,
						   ":impconsmin"=>$impconsmin,
						   ":impconsmedio"=>$impconsmedio,
						   ":impconsexec"=>$impconsexec,
						   ":hastarango1"=>$hastarango1,
						   ":hastarango2"=>$hastarango2,
						   ":hastarango3"=>$hastarango3,
						   ":estareg"=>$estareg,
						   ":impcargofijo"=>$impcargofijo1,
						   ":impconsmindesa"=>$impconsmindesa,
						   ":impconsexecdesa"=>$impconsexecdesa,
						   ":impconsmediodesa"=>$impconsmediodesa,
						   ":codusu"=>$idusuario,
						   ":impcargofijo2"=>$impcargofijo2,
						   ":impcargofijo3"=>$impcargofijo3,
							":coddiametrosagua"=>$diametrosagua,
							":nrotarifa"=>$nrotarifa
						   ));
		break;
		case 1:
			$sql = "update facturacion.tarifas set codcategoriatar=:codcategoriatar,nomtar=:nomtar,volumenmin=:volumenmin,volumenesp=:volumenesp
				,impconsmin=:impconsmin,impconsmedio=:impconsmedio,impconsexec=:impconsexec,hastarango1=:hastarango1,hastarango2=:hastarango2,
				hastarango3=:hastarango3,estareg=:estareg,impcargofijo1=:impcargofijo,impconsmindesa=:impconsmindesa,impconsexecdesa=:impconsexecdesa,
				impconsmediodesa=:impconsmediodesa,codusu=:codusu,impcargofijo2=:impcargofijo2,impcargofijo3=:impcargofijo3,coddiametrosagua=:coddiametrosagua
				where codemp=:codemp and codsuc=:codsuc and catetar=:catetar AND nrotarifa = ".$nrotarifa;
				$result = $conexion->prepare($sql);
				$result->execute(array(":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":catetar"=>$catetar,
						   ":codcategoriatar"=>$categoriatarifaria,
						   ":nomtar"=>$nomtar,
						   ":volumenmin"=>$volumenmin,
						   ":volumenesp"=>$volumenesp,
						   ":impconsmin"=>$impconsmin,
						   ":impconsmedio"=>$impconsmedio,
						   ":impconsexec"=>$impconsexec,
						   ":hastarango1"=>$hastarango1,
						   ":hastarango2"=>$hastarango2,
						   ":hastarango3"=>$hastarango3,
						   ":estareg"=>$estareg,
						   ":impcargofijo"=>$impcargofijo1,
						   ":impconsmindesa"=>$impconsmindesa,
						   ":impconsexecdesa"=>$impconsexecdesa,
						   ":impconsmediodesa"=>$impconsmediodesa,
						   ":codusu"=>$idusuario,
						   ":impcargofijo2"=>$impcargofijo2,
						   ":impcargofijo3"=>$impcargofijo3,
							":coddiametrosagua"=>$diametrosagua
						   ));
		break;
		case 2:case 3:
			$sql = "update facturacion.tarifas set estareg=:estareg
				where codemp=:codemp and codsuc=:codsuc and catetar=:catetar AND nrotarifa = ".$nrotarifa;
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":catetar"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
