<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$NroTarifa	= isset($_POST["nrotarifa"])?$_POST["nrotarifa"]:'0';
	
	$codsuc 	= $_SESSION['IdSucursal'];
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsDrop();

	if($Id!='')
	{
		$sql = $objMantenimiento->Sentencia("tarifas")." WHERE codemp=? AND codsuc=? AND catetar=? AND nrotarifa =".$NroTarifa;
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array(1,$codsuc,$Id));
		$tarifas	= $consulta->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if ($("#codcategoriatar").val() == '')
		{
			alert('Seleccione la Categoria Tarifaria');
			return false;
		}
		if ($("#nomtar").val() == '')
		{
			alert('La Descripcion de la Tarifaria no puede ser NULO');
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    <tbody>
	  <tr>
	  	<td width="140" class="TitDetalle">&nbsp;</td>
	  	<td class="CampoDetalle">&nbsp;</td>
	  	<td class="CampoDetalle">&nbsp;</td>
	  	</tr>
	  <tr>
	    <td class="TitDetalle">Nro</td>
	    <td align="center" >:</td>
	    <td colspan="2" class="CampoDetalle"><input name="nrotarifa" type="text" id="nrotarifa" size="2" maxlength="2" readonly="readonly" value="<? echo $NroTarifa; ?>" class="inputtext"/></td>
	    </tr>
	  <tr>
        <td class="TitDetalle">Id</td>
        <td width="30" align="center" >:</td>
        <td colspan="2" class="CampoDetalle">
            <input name="catetar" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
		</td>
	</tr>
	<tr>
	  <td class="TitDetalle">Categoria</td>
	  <td align="center" >:</td>
	  <td colspan="2" class="CampoDetalle">
      	<? $objMantenimiento->drop_categoria_tarifarias($tarifas["codcategoriatar"]); ?>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Nombre Tarifa</td>
	  <td align="center" >:</td>
	  <td colspan="2" class="CampoDetalle">
	    <input class="inputtext" name="nomtar" type="text" id="nomtar" maxlength="200" value="<?=$tarifas["nomtar"]?>" style="width:400px;" /></td>
	</tr>
	 <tr>
	  <td class="TitDetalle">C&oacute;digo Diametro</td>
	  <td align="center" >:</td>
	  <td colspan="2" class="CampoDetalle">
	  	<? $objMantenimiento->drop_diametros_agua($tarifas["coddiametrosagua"],''); ?>
	  </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Volumen M&iacute;nimo</td>
	  <td align="center" >:</td>
	  <td colspan="2" class="CampoDetalle"><input class="inputtext" style="text-align:right;width:110px;" name="volumenmin" type="text" id="volumenmin" maxlength="20" value="<?=isset($tarifas["volumenmin"])?$tarifas["volumenmin"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'volumenesp');return permite(event,'num');"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Volumen Mes Promedio</td>
	  <td align="center" >:&nbsp;</td>
	  <td colspan="2" class="CampoDetalle"><input class="inputtext" style="text-align:right;width:110px;" name="volumenesp" type="text" id="volumenesp" maxlength="20" value="<?=isset($tarifas["volumenesp"])?$tarifas["volumenesp"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impcargofijo');return permite(event,'num');"/></td>
	  </tr>
	<tr>
	  <td colspan="4" class="TitDetalle">
	    <table border="1" align="center" cellspacing="0" class="ui-widget"  width="500" id="tbtarifas" rules="all" >

	      <tr class="ui-widget-header">
	        <td>&nbsp;</td>
	        <td width="115" align="center">Volumen</td>
	        <td width="115" align="center">Importe Agua</td>
	        <td width="115" align="center">Importe Desague</td>
	        </tr>
	      <tr style="padding:4px">
	        <td align="right" class="ui-widget-header" style="padding-right:5px;">Rango Inicial</td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="hastarango1" type="text" id="hastarango1" maxlength="20" value="<?=isset($tarifas["hastarango1"])?$tarifas["hastarango1"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsmin');return permite(event,'num');"/>
	          </td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="impconsmin" type="text" id="impconsmin" maxlength="20" value="<?=isset($tarifas["impconsmin"])?$tarifas["impconsmin"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsmindesa');return permite(event,'num');"/>
	          </td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="impconsmindesa" type="text" id="impconsmindesa" maxlength="20" value="<?=isset($tarifas["impconsmindesa"])?$tarifas["impconsmindesa"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'hastarango2');return permite(event,'num');"/>
	          </td>
	        </tr>
	      <tr style="padding:4pxs">
	        <td align="right" class="ui-widget-header" style="padding-right:5px;">Rango Intermedio </td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="hastarango2" type="text" id="hastarango2" maxlength="20" value="<?=isset($tarifas["hastarango2"])?$tarifas["hastarango2"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsmedio');return permite(event,'num');"/>
	          </td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="impconsmedio" type="text" id="impconsmedio" maxlength="20"  value="<?=isset($tarifas["impconsmedio"])?$tarifas["impconsmedio"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsmediodesa');return permite(event,'num');"/>
	          </td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="impconsmediodesa" type="text" id="impconsmediodesa" maxlength="20" value="<?=isset($tarifas["impconsmediodesa"])?$tarifas["impconsmediodesa"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'hastarango3');return permite(event,'num');"/>
	          </td>
	        </tr>
	      <tr style="padding:4px">
	        <td align="right" class="ui-widget-header" style="padding-right:5px;">Rango Final </td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="hastarango3" type="text" id="hastarango3" maxlength="20" value="<?=isset($tarifas["hastarango3"])?$tarifas["hastarango3"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsexec');return permite(event,'num');"/>
	          </td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="impconsexec" type="text" id="impconsexec" maxlength="20" value="<?=isset($tarifas["impconsexec"])?$tarifas["impconsexec"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsexecdesa');return permite(event,'num');"/>
	          </td>
	        <td style="background-color: white" align="center">
	          <input class="inputtext" style="text-align:right;width:110px;" name="impconsexecdesa" type="text" id="impconsexecdesa" maxlength="20" value="<?=isset($tarifas["impconsexecdesa"])?$tarifas["impconsexecdesa"]:number_format(0,6)?>" onkeypress="return permite(event,'num');"/>
	          </td>
	        </tr>
	      </table>
	    </td>
	  </tr>	
	<tr>
	  <td colspan="4" class="TitDetalle" height="5px"></td>
	</tr>
	<tr>
	  <td colspan="4" class="TitDetalle">
      <table border="1" align="center" cellspacing="0" class="ui-widget"  width="270" id="tbtarifas2" rules="all" >
	    <tr class="ui-widget-header">
	      <td colspan="2" align="center">Importe de Cargo Fijo</td>
	      </tr>
	    <tr style="padding:4px">
	      <td align="right" class="ui-widget-header" style="padding-right:5px;">Rango Inicial</td>
	      <td width="115" align="center" style="background-color: white"><input class="inputtext" style="text-align:right;width:110px;" name="impcargofijo1" type="text" id="impcargofijo1" maxlength="20" value="<?=isset($tarifas["impcargofijo1"])?$tarifas["impcargofijo1"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsmin');return permite(event,'num');"/></td>
	      </tr>
	    <tr style="padding:4pxs">
	      <td align="right" class="ui-widget-header" style="padding-right:5px;">Rango Intermedio </td>
	      <td style="background-color: white" align="center"><input class="inputtext" style="text-align:right;width:110px;" name="impcargofijo2" type="text" id="impcargofijo2" maxlength="20" value="<?=isset($tarifas["impcargofijo2"])?$tarifas["impcargofijo2"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsmedio');return permite(event,'num');"/></td>
	      </tr>
	    <tr style="padding:4px">
	      <td align="right" class="ui-widget-header" style="padding-right:5px;">Rango Final </td>
	      <td style="background-color: white" align="center"><input class="inputtext" style="text-align:right;width:110px;" name="impcargofijo3" type="text" id="impcargofijo3" maxlength="20" value="<?=isset($tarifas["impcargofijo3"])?$tarifas["impcargofijo3"]:number_format(0,6)?>" onkeypress="CambiarFoco(event,'impconsexec');return permite(event,'num');"/></td>
	      </tr>
	    </table></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center">:</td>
	  <td colspan="2" class="CampoDetalle"><? include("../../../../include/estareg.php"); ?></td>
	  </tr>
	<tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td colspan="3" class="CampoDetalle">&nbsp;</td>
	</tr>
   </tbody>

  </table>
 </form>
</div>
<?php
	$est = isset($tarifas["estareg"])?$tarifas["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>