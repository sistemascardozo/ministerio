<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codactividadconcepto 	= $_POST["codactividadconcepto"];
	$descripcion	= $_POST["descripcion"];
	$estareg		= $_POST["estareg"];
	
	
	switch ($Op) 
	{
		case 0:
			$id = $objFunciones->setCorrelativos("actividadconcepto", "0", "0");
			$codactividadconcepto = $id[0];
			
			$sql = "insert into facturacion.actividadconcepto(codactividadconcepto,descripcion,estareg) values(:codactividadconcepto,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codactividadconcepto"=>$codactividadconcepto,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update facturacion.actividadconcepto set descripcion=:descripcion, estareg=:estareg 
			where codactividadconcepto=:codactividadconcepto";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codactividadconcepto"=>$codactividadconcepto,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update facturacion.actividadconcepto set estareg=:estareg
				where codactividadconcepto=:codactividadconcepto";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codactividadconcepto"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
