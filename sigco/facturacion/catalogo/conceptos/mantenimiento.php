<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc		= $_SESSION['IdSucursal'];
	
	$objMantenimiento 	= new clsDrop();
	$sefactura=1;
	if($Id!='')
	{
		$consulta   = $conexion->prepare("select * from facturacion.conceptos where codconcepto=? and codsuc=?");
		$consulta->execute(array($Id,$codsuc));
		$row 		= $consulta->fetch();		
		$sefactura=$row['sefactura'];
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
$(document).ready(function(){
	
 	$("#sefactura<?=$sefactura?>").attr("checked", true);
 	$(".buttonset").buttonset('refresh');
 	$( "#tabs" ).tabs();
 	});
	function ValidarForm(Op)
	{
		if($("#validarP").val()>0)
		{
			$("#tabs").tabs({selected: 0});
			alert("Ya se ha ingersado un concepto con el valor de conexion y presupuesto")
			return false
		}
		if($("#tipoconceptos").val()==0)
		{
			$("#tabs").tabs({selected: 0});
			alert("Seleccione el Tipo de Concepto")
			return false
		}
		if($("#Nombre").val()==0)
		{
			$("#tabs").tabs({selected: 0});
			alert('La Descripcion del Concepto no puede ser NULO');
			return false;
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function ver_presupuesto(obj)
	{
		if(obj.checked)
		{
			$("#fila_presupuesto").css("visibility","visible");
			validar_presupuesto(1)
		}else{
			$("#validarP").val(0)
			$("#fila_presupuesto").css("visibility","hidden");
		}
	}
	function ver_categoria_interes(obj)
	{
		if(obj.value==3)
		{
			$("#fila_categoria_interes").css("visibility","visible");
		}else{
			$("#categoria_intereses").val(0)
			$("#fila_categoria_interes").css("visibility","hidden");
		}
		if(obj.value==7)
		{
			$("#fila_categoria_redondeo").css("visibility","visible");
		}else{
			$("#categoria_redondeo").val(0)
			$("#fila_categoria_redondeo").css("visibility","hidden");
		}
	}
	function validar_presupuesto(val)
	{
		$.ajax({
			 url:'validar_conceptos.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&tipopresupuesto='+val,
			 success:function(datos){		 	
				$("#validarP").val(datos)
			 }
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
	<table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
				<td colspan="5">
					<div id="tabs">
						<ul>
                            <li style="font-size:10px"><a href="#tabs-1">Datos Generales</a></li>
                            <li style="font-size:10px"><a href="#tabs-2">Cuentas Contables</a></li>
						</ul>
						<div id="tabs-1" >
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
                                    <td class="TitDetalle">C&oacute;digo</td>
                                    <td width="30" align="center" class="TitDetalle">:</td>
                                    <td class="CampoDetalle">
										<input name="codconcepto" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
									</td>
                                </tr>
                                <tr>
                                    <td class="TitDetalle">Tipo de Concepto</td>
                                    <td align="center" class="TitDetalle">:</td>
                                    <td class="CampoDetalle"><? $objMantenimiento->drop_tipo_conceptos($row["codtipoconcepto"]); ?></td>
                                </tr>
                                <tr>
                                    <td class="TitDetalle">Descripcion</td>
                                    <td align="center" class="TitDetalle">:</td>
                                    <td lass="CampoDetalle">
										<input class="inputtext" name="descripcion" type="text" id="Nombre" maxlength="200" value="<?=$row["descripcion"]?>" style="width:400px;"/></td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle">Actividad</td>
                                        <td align="center" class="TitDetalle">:</td>
                                        <td class="CampoDetalle"><? $objMantenimiento->drop_actividad_conceptos($row["codactividadconcepto"]); ?></td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle">Especificaci&oacute;n</td>
                                        <td align="center" class="TitDetalle">:</td>
                                        <td class="CampoDetalle">
											<input class="inputtext" name="especificacion" type="text" id="especificacion" maxlength="200" value="<?=$row["especificacion"]?>" style="width:400px;"/>
										</td>
							  </tr>
									<!--<tr>
                                        <td class="TitDetalle"><span class="CampoDetalle">Codigo Avalon</span></td>
                                        <td align="center" class="TitDetalle">:</td>
                                        <td class="CampoDetalle"><input class="inputtext" name="codantiguo" type="text" id="codantiguo" size="25" maxlength="25" value="<?=$row["codantiguo"]?>" onkeypress="return permite(event,'num');"/></td>
									</tr>-->
                              <tr>
						  <td class="TitDetalle">Signo</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><label>
						    <select name="signo" id="signo" class="select">
						    	<option value="+" <?=$d="";$row["signo"]=='+'?$d="selected='selected'":$d="";echo $d?> >+</option> 
					            <option value="-" <?=$d="";$row["signo"]=='-'?$d="selected='selected'":$d="";echo $d?> >-</option> 
					        </select>
						  </label></td>
						  </tr>
						<tr>
						  <td class="TitDetalle">Importe</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="importe" type="text" id="importe" size="25" maxlength="25"  value="<?=isset($row["importe"])?number_format($row["importe"],2):'0.00'?>" onkeypress="return permite(event,'num');"/></td>
						  </tr>
						   <tr>
						  <td class="TitDetalle">Se Factura?</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle">
						  	 <div class="buttonset" style="display:inline">
					          <input type="radio" id="sefactura1" name="sefactura" value='1' />
					          <label for="sefactura1"   >SI</label>
					          <input type="radio" id="sefactura0" name="sefactura" value='0' />
					          <label for="sefactura0"  >NO</label>
					        </div>
						  </td>
						  </tr>
						  <tr>
						  <td class="TitDetalle">Orden Recibo</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ordenrecibo" type="text" id="ordenrecibo" size="25" maxlength="25"  value="<?=$row["ordenrecibo"]?>" onkeypress="return permite(event,'num');"/></td>
						  </tr>
						   <tr>
						  <td class="TitDetalle">D&iacute;as Atenci&oacute;n</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="diasatencion" type="text" id="diasatencion" size="25" maxlength="25"  value="<?=$row["diasatencion"]?>" onkeypress="return permite(event,'num');"/></td>
						  </tr>
						<tr>
						  <td class="TitDetalle">&nbsp;</td>
						  <td align="center" class="TitDetalle">&nbsp;</td>
						  <td class="CampoDetalle">
					        <?php
								$chekced	= "";
								$visibled 	= "style='visibility:hidden'";
								
								if(isset($row["presupuesto"]))
								{
									if($row["presupuesto"]==1)
									{
										$checked 	= "checked='checked'";
										$visibled 	=  "style='visibility:visible'";
									}
								}
							?>
						    <input type="checkbox" name="presupuesto_chhk" id="presupuesto_chk" <?=$checked?> onclick="CambiarEstado(this,'presupuestotxt');ver_presupuesto(this);" />
						    Requiere Presupuesto de Conexion.
						    <input type="hidden" name="presupuestotxt" id="presupuestotxt" value="<?=isset($row["presupuesto"])?$row["presupuesto"]:0?>" /></td>
						  </tr>
						<tr>
						  <td class="TitDetalle">&nbsp;</td>
						  <td align="center" class="TitDetalle">&nbsp;</td>
						  <td class="CampoDetalle"><label>
					        <?php
								$checked="";
								if(isset($row["afecto_contable"]))
								{
									if($row["afecto_contable"]==1)
									{
										$checked="checked='checked'";
									}
								}
							?>
						    <input type="checkbox" name="afecto_contable" id="afecto_contable" onclick="CambiarEstado(this,'no_afecto');" <?=$checked?> />
						    No Afecto a Contabilidad
						    <input type="hidden" name="no_afecto" id="no_afecto" value="<?=isset($row["afecto_contable"])?$row["afecto_contable"]:0?>" />
						  </label></td>
						  </tr>

						  <tr>
						  <td class="TitDetalle">&nbsp;</td>
						  <td align="center" class="TitDetalle">&nbsp;</td>
						  <td class="CampoDetalle"><label>
					        <?php
								$checked="";
								if(isset($row["afecto_igv"]))
								{
									if($row["afecto_igv"]==1)
									{
										$checked="checked='checked'";
									}
								}
							?>
						    <input type="checkbox" name="chk_afecto_igv" id="chk_afecto_igv" onclick="CambiarEstado(this,'afecto_igv');" <?=$checked?> />
						    Afecto I.G.V.
						    <input type="hidden" name="afecto_igv" id="afecto_igv" value="<?=isset($row["afecto_igv"])?$row["afecto_igv"]:0?>" />
						  </label></td>
						  </tr>
						<tr id="fila_presupuesto" <?=$visibled?> >
						  <td class="TitDetalle">Tipo Presupuesto</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><select name="tipopresupuesto" id="tipopresupuesto" style="width:80px" class="select" onchange="validar_presupuesto(this.value);" >
						    <option value="1" <?=$d="";$row["tipopresupuesto"]==1?$d="selected='selected'":$d="";echo $d?> >AGUA</option>
						    <option value="2" <?=$d="";$row["tipopresupuesto"]==2?$d="selected='selected'":$d="";echo $d?> >DESAGUE</option>
						    </select></td>
						</tr>
					    <?php
								$chekced	= "";
								$visibled 	= "style='visibility:hidden'";
								
								if(isset($row["categoria"]))
								{
									if($row["categoria"]==3)
									{
										$visibled 	=  "style='visibility:visible'";
									}
								}
							?>
						<tr>
						  <td class="TitDetalle">Categoria</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle">
					      <select name="categoria" id="categoria" style="width:120px" class="select" onchange="ver_categoria_interes(this);" >
						    <option value="1" <?=$d="";$row["categoria"]==1?$d="selected='selected'":$d="";echo $d?> >AGUA</option>
						    <option value="2" <?=$d="";$row["categoria"]==2?$d="selected='selected'":$d="";echo $d?> >DESAGUE</option>
					        <option value="3" <?=$d="";$row["categoria"]==3?$d="selected='selected'":$d="";echo $d?> >INTERES</option>
					        <option value="4" <?=$d="";$row["categoria"]==4?$d="selected='selected'":$d="";echo $d?> >IGV</option>
					        <option value="5" <?=$d="";$row["categoria"]==5?$d="selected='selected'":$d="";echo $d?> >CARGO FIJO</option>
					        <option value="6" <?=$d="";$row["categoria"]==6?$d="selected='selected'":$d="";echo $d?> >CREDITO</option>
					        <option value="7" <?=$d="";$row["categoria"]==7?$d="selected='selected'":$d="";echo $d?> >REDONDEO</option>
					        <option value="8" <?=$d="";$row["categoria"]==8?$d="selected='selected'":$d="";echo $d?> >FONAVI</option>
					        <option value="9" <?=$d="";$row["categoria"]==9?$d="selected='selected'":$d="";echo $d?> >VMA</option>
					        <option value="10" <?=$d="";$row["categoria"]==10?$d="selected='selected'":$d="";echo $d?> >COLATERAL</option>
						  </select>
					      </td>
						  </tr>
						<tr id="fila_categoria_interes" <?=$visibled?> >
						  <td class="TitDetalle">Categoria de Intereses</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle">
					      <select name="categoria_intereses" id="categoria_intereses" style="width:120px" class="select" >
					      	<option value="0" <?=$d="";$row["categoria_intereses"]==0?$d="selected='selected'":$d="";echo $d?> >NORMAL</option>
						    <option value="1" <?=$d="";$row["categoria_intereses"]==1?$d="selected='selected'":$d="";echo $d?> >AGUA</option>
						    <option value="2" <?=$d="";$row["categoria_intereses"]==2?$d="selected='selected'":$d="";echo $d?> >DESAGUE</option>
						    <option value="3" <?=$d="";$row["categoria_intereses"]==3?$d="selected='selected'":$d="";echo $d?> >IGV</option>
						    <option value="4" <?=$d="";$row["categoria_intereses"]==4?$d="selected='selected'":$d="";echo $d?> >CARGO FIJO</option>
						    <option value="5" <?=$d="";$row["categoria_intereses"]==5?$d="selected='selected'":$d="";echo $d?> >CREDITO</option>
						    <option value="6" <?=$d="";$row["categoria_intereses"]==6?$d="selected='selected'":$d="";echo $d?> >VMA</option>
						    <option value="7" <?=$d="";$row["categoria_intereses"]==7?$d="selected='selected'":$d="";echo $d?> >MORATORIO</option>
							<!-- CREDITO DE COLATERAL (5)-->
					      </select>
					      </td>
						</tr>
					    <?php
								$chekced	= "";
								$visibled 	= "style='visibility:hidden'";
								
								if(isset($row["categoria"]))
								{
									if($row["categoria"]==7)
									{
										$visibled 	=  "style='visibility:visible'";
									}
								}
							?>
						<tr id="fila_categoria_redondeo" <?=$visibled?> >
						  <td class="TitDetalle">Categoria de Redondeo</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle">
					      <select name="categoria_redondeo" id="categoria_redondeo" style="width:120px" class="select" >
						    <option value="0" <?=$d="";$row["categoria_redondeo"]==0?$d="selected='selected'":$d="";echo $d?> >NORMAL</option>
						    <option value="1" <?=$d="";$row["categoria_redondeo"]==1?$d="selected='selected'":$d="";echo $d?> >REDONDEO ACTUAL</option>
						    <option value="2" <?=$d="";$row["categoria_redondeo"]==2?$d="selected='selected'":$d="";echo $d?> >REDONDEO ANTERIOR</option>
						  </select>
					      </td>
						  </tr>
						<tr>
						  <td class="TitDetalle">Estado</td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle">
						  	<? include("../../../../include/estareg.php"); ?>
					      </td>
						  </tr>
						 <tr>
						   <td class="TitDetalle">&nbsp;</td>
						   <td class="TitDetalle">&nbsp;</td>
						   <td class="CampoDetalle">&nbsp;</td>
						  </tr>
          			</table>
          		</div>
          		<div id="tabs-2" >
			      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			      		<tr>
						  <td class="TitDetalle">Cta. Debe</td>
						  <td width="30" align="center" class="TitDetalle">:</td>
						  <td width="150" class="CampoDetalle"><input class="inputtext" name="ctadebe" type="text" id="ctadebe" size="25" maxlength="25" value="<?=$row["ctadebe"]?>" onkeypress="return permite(event,'num');" /></td>
						  <td width="110" class="CampoDetalle" align="right">&nbsp;</td>
						  </tr>
						<tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Haber</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctahaber" type="text" id="ctahaber" size="25" maxlength="25" value="<?=$row["ctahaber"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"><input type="hidden" name="validarP" id="validarP" value="0" /></td>
						  </tr>
						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Debe Rebajas</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctadeberebajas" type="text" id="ctadeberebajas" size="25" maxlength="25" value="<?=$row["ctadeberebajas"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>
						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Haber Rebajas</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctahaberrebajas" type="text" id="ctahaberrebajas" size="25" maxlength="25" value="<?=$row["ctahaberrebajas"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>

						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Debe Provision</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctadebeprov" type="text" id="ctadebeprov" size="25" maxlength="25" value="<?=$row["ctadebeprov"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>
						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Haber Provision</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctahaberprov" type="text" id="ctahaberprov" size="25" maxlength="25" value="<?=$row["ctahaberprov"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>

						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Debe Provision 2</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctadebeprov2" type="text" id="ctadebeprov2" size="25" maxlength="25" value="<?=$row["ctadebeprov2"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>
						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Haber Provision 2</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctahaberprov2" type="text" id="ctahaberprov2" size="25" maxlength="25" value="<?=$row["ctahaberprov2"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>

						   <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Debe Provision Reg. Cob.</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctadebeprovcob" type="text" id="ctadebeprovcob" size="25" maxlength="25" value="<?=$row["ctadebeprovcob"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>
						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Haber Provision Reg. Cob</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctahaberprovcob" type="text" id="ctahaberprovcob" size="25" maxlength="25" value="<?=$row["ctahaberprovcob"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>

						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Debe Quiebre</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctadebequiebres" type="text" id="ctadebequiebres" size="25" maxlength="25" value="<?=$row["ctadebequiebres"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>
						  <tr>
						  <td class="TitDetalle"><span class="CampoDetalle">Cta. Haber Quiebre</span></td>
						  <td align="center" class="TitDetalle">:</td>
						  <td class="CampoDetalle"><input class="inputtext" name="ctahaberquiebres" type="text" id="ctahaberquiebres" size="25" maxlength="25" value="<?=$row["ctahaberquiebres"]?>" onkeypress="return permite(event,'num');"/></td>
						  <td class="CampoDetalle" align="right"></td>
						  </tr>

						  
			      	</table>
			    </div>
          	</div>
          </td>
      </tr>

          </tbody>
	 
    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>