<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();} 
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp               = 1;
	$codsuc               = $_SESSION['IdSucursal'];
	
	$codconcepto          = $_POST["codconcepto"];
	$tipoconceptos        = $_POST["tipoconceptos"];
	$descripcion          = strtoupper($_POST["descripcion"]);
	$ctadebe              = $_POST["ctadebe"];
	$ctahaber             = $_POST["ctahaber"];
	$signo                = $_POST["signo"];
	$importe              = $_POST["importe"];
	$estareg              = $_POST["estareg"];
	$presupuestotxt       = $_POST["presupuestotxt"];
	$idusuario            = $_SESSION['id_user'];
	$tipopresupuesto      = $_POST["tipopresupuesto"];
	$categoria            = $_POST["categoria"];
	$no_afecto            = $_POST["no_afecto"];
	$categoria_intereses  = $_POST["categoria_intereses"];
	$categoria_redondeo   = $_POST["categoria_redondeo"];
	$afecto_igv           = $_POST["afecto_igv"];
	$ordenrecibo          = $_POST["ordenrecibo"];
	$codactividadconcepto = $_POST["codactividadconcepto"];
	$especificacion       = $_POST["especificacion"];
	$diasatencion         = $_POST["diasatencion"];
	$sefactura            = $_POST["sefactura"];
	$codantiguo           = $_POST["codantiguo"];
	$ctadeberebajas       = $_POST["ctadeberebajas"];
	$ctahaberrebajas      = $_POST["ctahaberrebajas"];
	$ctadebequiebres      = $_POST["ctadebequiebres"];
	$ctahaberquiebres     = $_POST["ctahaberquiebres"];
	$ctadebeprov          = $_POST["ctadebeprov"];
	$ctahaberprov         = $_POST["ctahaberprov"];
	$ctadebeprov2          = $_POST["ctadebeprov2"];
	$ctahaberprov2         = $_POST["ctahaberprov2"];

	$ctadebeprovcob          = $_POST["ctadebeprovcob"];
	$ctahaberprovcob         = $_POST["ctahaberprovcob"];
	if($ordenrecibo=="")$ordenrecibo=1;
	if($diasatencion=="")$diasatencion=0;

	if($sefactura=="")$sefactura=1;

	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("conceptos",$codsuc,"0");
			$codconcepto	= $id[0];
			
			$sql  = "insert into facturacion.conceptos(codconcepto,descripcion,codtipoconcepto,ctadebe,ctahaber,signo,importe,estareg,creador,codemp,";
			$sql .= "codsuc,presupuesto,tipopresupuesto,categoria,afecto_contable,
					categoria_intereses,categoria_redondeo,afecto_igv,ordenrecibo,
					codactividadconcepto,especificacion,diasatencion,sefactura,codantiguo,
					ctadeberebajas,ctahaberrebajas,ctadebequiebres,ctahaberquiebres,
					ctadebeprov,ctahaberprov,ctadebeprov2,ctahaberprov2,ctadebeprovcob,ctahaberprovcob) 
					 values(:codconcepto,:descripcion,:codtipoconcepto,:ctadebe,:ctahaber,:signo,:importe,"; 
			$sql .= ":estareg,:creador,:codemp,:codsuc,:presupuesto,:tipopresupuesto,:categoria,
					:afecto_contable,:categoria_intereses,:categoria_redondeo,:afecto_igv,:ordenrecibo,
					:codactividadconcepto,:especificacion,:diasatencion,:sefactura,:codantiguo,
					:ctadeberebajas,:ctahaberrebajas,:ctadebequiebres,:ctahaberquiebres,
					:ctadebeprov,:ctahaberprov,:ctadebeprov2,:ctahaberprov2,:ctadebeprovcob,:ctahaberprovcob)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codconcepto"=>$codconcepto,
						   ":descripcion"=>$descripcion,
						   ":codtipoconcepto"=>$tipoconceptos,
						   ":ctadebe"=>$ctadebe,
						   ":ctahaber"=>$ctahaber,
						   ":signo"=>$signo,
						   ":importe"=>$importe,
						   ":estareg"=>$estareg,
						   ":creador"=>$idusuario,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":presupuesto"=>$presupuestotxt,
						   ":tipopresupuesto"=>$tipopresupuesto,
						   ":categoria"=>$categoria,
						   ":afecto_contable"=>$no_afecto,
						   ":categoria_intereses"=>$categoria_intereses,
						   ":categoria_redondeo"=>$categoria_redondeo,
						   ":afecto_igv"=>$afecto_igv,
						   ":ordenrecibo"=>$ordenrecibo,
						   ":codactividadconcepto"=>$codactividadconcepto,
							":especificacion"=>$especificacion,
							":diasatencion"=>$diasatencion,
							":sefactura"=>$sefactura,
							":codantiguo"=>$codantiguo,
							":ctadeberebajas"=>$ctadeberebajas,
							":ctahaberrebajas"=>$ctahaberrebajas,
							":ctadebequiebres"=>$ctadebequiebres,
							":ctahaberquiebres"=>$ctahaberquiebres,
							":ctadebeprov"=>$ctadebeprov,
							":ctahaberprov"=>$ctahaberprov,
							":ctadebeprov2"=>$ctadebeprov2,
							":ctahaberprov2"=>$ctahaberprov2,
							":ctadebeprovcob"=>$ctadebeprovcob,
							":ctahaberprovcob"=>$ctahaberprovcob

						   ));
		
		break;
		case 1:
			$sql  = "update facturacion.conceptos set descripcion=:descripcion,codtipoconcepto=:codtipoconcepto,ctadebe=:ctadebe,ctahaber=:ctahaber,signo=:signo,";
			$sql .= "importe=:importe,estareg=:estareg,creador=:creador,presupuesto=:presupuesto,tipopresupuesto=:tipopresupuesto,categoria=:categoria
				 ,afecto_contable=:afecto_contable,categoria_intereses=:categoria_intereses,categoria_redondeo=:categoria_redondeo,afecto_igv=:afecto_igv,
				 ordenrecibo=:ordenrecibo,codactividadconcepto=:codactividadconcepto,
				 especificacion=:especificacion,diasatencion=:diasatencion,sefactura=:sefactura,
				 codantiguo=:codantiguo,ctadeberebajas=:ctadeberebajas,ctahaberrebajas=:ctahaberrebajas,
				 ctadebequiebres=:ctadebequiebres,ctahaberquiebres=:ctahaberquiebres,
				 ctadebeprov=:ctadebeprov,ctahaberprov=:ctahaberprov,
				 ctadebeprov2=:ctadebeprov2,ctahaberprov2=:ctahaberprov2,
				 ctadebeprovcob=:ctadebeprovcob,ctahaberprovcob=:ctahaberprovcob
				 where codemp=:codemp and codsuc=:codsuc and codconcepto=:codconcepto";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codconcepto"=>$codconcepto,
						   ":descripcion"=>$descripcion,
						   ":codtipoconcepto"=>$tipoconceptos,
						   ":ctadebe"=>$ctadebe,
						   ":ctahaber"=>$ctahaber,
						   ":signo"=>$signo,
						   ":importe"=>$importe,
						   ":estareg"=>$estareg,
						   ":creador"=>$idusuario,
						   ":codemp"=>$codemp,
						   ":codsuc"=>$codsuc,
						   ":presupuesto"=>$presupuestotxt,
						   ":tipopresupuesto"=>$tipopresupuesto,
						   ":categoria"=>$categoria,
						   ":afecto_contable"=>$no_afecto,
						   ":categoria_intereses"=>$categoria_intereses,
						   ":categoria_redondeo"=>$categoria_redondeo,
						   ":afecto_igv"=>$afecto_igv,
						   ":ordenrecibo"=>$ordenrecibo,
						   ":codactividadconcepto"=>$codactividadconcepto,
							":especificacion"=>$especificacion,
							":diasatencion"=>$diasatencion,
							":sefactura"=>$sefactura,
							":codantiguo"=>$codantiguo,
							":ctadeberebajas"=>$ctadeberebajas,
							":ctahaberrebajas"=>$ctahaberrebajas,
							":ctadebequiebres"=>$ctadebequiebres,
							":ctahaberquiebres"=>$ctahaberquiebres,
							":ctadebeprov"=>$ctadebeprov,
							":ctahaberprov"=>$ctahaberprov,
							":ctadebeprov2"=>$ctadebeprov2,
							":ctahaberprov2"=>$ctahaberprov2,
							":ctadebeprovcob"=>$ctadebeprovcob,
							":ctahaberprovcob"=>$ctahaberprovcob

						   ));
		break;
		case 2:case 3:
			$sql = "update facturacion.conceptos set estareg=:estareg
				where codemp=:codemp and codsuc=:codsuc and codconcepto=:codconcepto";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codemp"=>$codemp,":codsuc"=>$codsuc,":codconcepto"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
