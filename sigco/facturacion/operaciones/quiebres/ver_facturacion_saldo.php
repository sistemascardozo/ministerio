<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../config.php");
	
	$codsuc = $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$count 	= 0;
	$total	= 0;
	
	$sqlfacturacion  = "SELECT det.codemp,det.codsuc,cab.codciclo,cab.nrofacturacion,";
	$sqlfacturacion .= "upper(doc.abreviado) AS abreviado,cab.nrodocumento,cab.anio,cab.mes,";
	$sqlfacturacion .= "upper(deuda.descripcion) AS deuda,CASE WHEN det.categoria = 0 THEN 'FACTURACION' ";
	$sqlfacturacion .= "ELSE CASE WHEN det.categoria = 1 THEN 'SALDO' ELSE 'SALDO REFINANCIADO' END ";
	$sqlfacturacion .= "END AS categoria,det.codtipodeuda,det.nroinscripcion,det.categoria AS cat,";
	$sqlfacturacion .= "sum(det.importe -(det.imppagado + det.importerebajado)) AS imptotal ";
	$sqlfacturacion .= "FROM facturacion.detfacturacion det ";
	$sqlfacturacion .= "JOIN facturacion.cabfacturacion cab ON det.codemp=cab.codemp AND det.codsuc=cab.codsuc ";
	$sqlfacturacion .= "AND det.nrofacturacion=cab.nrofacturacion AND det.nroinscripcion=cab.nroinscripcion and det.codciclo=cab.codciclo ";
	$sqlfacturacion .= "JOIN reglasnegocio.documentos doc ON cab.coddocumento=doc.coddocumento AND cab.codsuc=doc.codsuc ";
	$sqlfacturacion .= "JOIN public.tipodeuda deuda ON det.codtipodeuda=deuda.codtipodeuda ";
	$sqlfacturacion .= "WHERE det.nroinscripcion=? and det.codemp=1 and det.codsuc=? AND det.codtipodeuda<>11 AND det.codtipodeuda<>3 /*AND (det.codtipodeuda=10 OR det.codconcepto=101)*/ ";
	$sqlfacturacion .= " GROUP BY det.codtipodeuda,cab.nrofacturacion,doc.abreviado,cab.nrodocumento,cab.anio,";
	$sqlfacturacion .= "cab.mes,deuda.descripcion,det.categoria,det.nroinscripcion,det.nrofacturacion,det.codemp,";
	$sqlfacturacion .= "det.codsuc,cab.codciclo order by det.nrofacturacion";
			
	$consultaF = $conexion->prepare($sqlfacturacion);
	$consultaF->execute(array($nroinscripcion,$codsuc));
	$items = $consultaF->fetchAll();

	$tr="";
	foreach($items as $rowF)
	{
            if($rowF["imptotal"]>0)
            {
                $count++;
                $total+=$rowF["imptotal"];

                $tr .= "<tr style='background-color:#FFF'>";
                $tr .= "<td align='center'>
                    ".$rowF["nrofacturacion"]."</td>";

                $tr .= "<td align='center'>".$rowF["abreviado"]."</td>";
                $tr .= "<td align='center'>".$rowF["nrodocumento"]."</td>";
                $tr .= "<td align='center'>".$meses[$rowF["mes"]]."-".$rowF["anio"]."</td>";
                $tr .= "<td align='center'>".$rowF["deuda"]."</td>";
                $tr .= "<td align='center'>".$rowF["categoria"]."</td>";
                $tr .= "<td align='right'>".number_format($rowF["imptotal"],2)."</td>";
                $tr .= "<td align='center'><input  type='checkbox' name='check_deuda".$count."' checked='checked' class='input-chk' onclick='check_deuda(this)'>";
                $tr .= "<input type='hidden' value='".$rowF["codtipodeuda"]."' class='input-codtipodeuda' name='codtipodeuda[]'/>
                    <input type='hidden' value='".$rowF["imptotal"]."' class='input-deuda' name='' />
                    <input type='hidden' name='codciclodet[]' id='codciclo".$count."' value='".$rowF["codciclo"]."' class='input-codciclo' />
                    <input type='hidden' name='facturaciondet[]' id='facturacion".$count."' value='".$rowF["nrofacturacion"]."' class='input-facturacion'/></td>";
                $tr .= "</tr>";
            }
	}

	echo $tr."|".$count."|".$total;

?>