<?php
	include("../../../../objetos/clsReporte.php");
	
	class clsCreditos extends clsReporte
	{
		function cabecera()
		{
			global $nroquiebre;
         
			$this->Ln();
			$this->SetFont('Arial','B',10);
			$tit1 	= "QUIEBRE ";
			$tit2 	= "Nro. : ".$nroquiebre;
			
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			$this->Cell(190,5,utf8_decode($tit2),0,1,'C');
		}
		function Contenido($nroquiebre,$codsuc)
		{			
			global $conexion,$meses,$NombreEmpresa;
			$codemp=1;
			$h = 4;
			$s = 2;
			
			$sql  = "select upper(clie.propietario),cab.fechaemision,cab.totalquiebre,
							'0.00' as igv,'0.00' as redondeo,
						cab.totalquiebre as imptotal,";
			$sql .= "cab.glosa,cab.creador,upper(doc.abreviado),clie.nrodocumento,";
			$sql .= "upper(cab.glosa),".$this->getCodCatastral("clie.").",clie.nroinscripcion,
					upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
					zo.descripcion as zona ";
			$sql .= "from facturacion.cabquiebres as cab ";
			$sql .= "inner join catastro.clientes as clie on(cab.codemp=clie.codemp and ";
			$sql .= "cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion) ";
			$sql .= "inner join public.tipodocumento as doc on(clie.codtipodocumento=doc.codtipodocumento)
					 inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
					 inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
					 inner join admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)";
 
			$sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nroquiebre=?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$nroquiebre));
			$row = $consulta->fetch();
	
			$this->Ln(5);
			$this->SetFont('Arial','',7);
			$y=$this->GetY();
			$this->Rect($x+10,$y,190,10);
			$this->Cell(47.5,$h,("FECHA DE QUIEBRE"),0,0,'C');
			$this->Cell(47.5,$h,("C�DIGO CATASTRAL"),0,0,'C');
			$this->Cell(47.5,$h,("C�DIGO DE INSCRIPCI�N"),0,0,'C');
			$this->Cell(47.5,$h,(""),0,1,'C');
			$this->SetFont('Arial','B',9);
			$this->Cell(47.5,$h,$this->DecFecha($row[1]),0,0,'C');
			$this->Cell(47.5,$h,$row['codcatastro'],0,0,'C');
			$this->Cell(47.5,$h,$this->CodUsuario($codsuc,$row['nroinscripcion']),0,0,'C');
			$this->Cell(47.5,$h,"",0,1,'C');


			$this->Ln(5);
			$h = 6;
			$nroprepago=$row['nroprepago'];
			$this->SetFont('Arial','',6);
			$this->Cell(25,$h,"ITEM",1,0,'C');
			$this->Cell(30,$h,"FACTURACI�N",1,0,'C');
			$this->Cell(30,$h,"TIPO RECIBO",1,0,'C');
			$this->Cell(30,$h,"SERIE NUMERO",1,0,'C');
			$this->Cell(25,$h,"MONTO",1,0,'C');
			$this->Cell(25,$h,"EMISI�N",1,0,'C');
			$this->Cell(0,$h,"VENCIMIENTO",1,1,'C');
			$this->SetFont('Arial','',8);
			$this->SetFont('Arial','',8);
			$i=0;
			$TotalRef=0;
			$Sql="SELECT f.serie,f.nrodocumento, f.nrofacturacion,p.fechaemision,f.fechavencimiento,
				SUM(o.importe) as importe
				FROM facturacion.cabfacturacion f
				  INNER JOIN facturacion.origen_quiebre o ON (f.codemp = o.codemp)
				  AND (f.codsuc = o.codsuc)
				  AND (f.codciclo = o.codciclo)
				  AND (f.nrofacturacion = o.nrofacturacion)
				  AND (f.nroinscripcion = o.nroinscripcion)
				   INNER JOIN facturacion.periodofacturacion p ON (f.codemp = p.codemp)
				  AND (f.codsuc = p.codsuc)
				  AND (f.nrofacturacion = p.nrofacturacion)
				  AND (f.codciclo = p.codciclo)
				  where o.codemp=1 and o.codsuc=? and o.nroquiebre=?
				  GROUP BY f.serie,f.nrodocumento, f.nrofacturacion, p.fechaemision,f.fechavencimiento ";
			
			$consulta2 = $conexion->prepare($Sql);
			$consulta2->execute(array($codsuc,$nroquiebre));
			$itemD = $consulta2->fetchAll();
			
			foreach($itemD as $rowD)
			{
				$i++;
				
				$TotalRef+= $rowD['importe'];
				$recibon = str_pad($rowD['serie'],3,"0",STR_PAD_LEFT)." - ".str_pad($rowD['nrodocumento'],7,"0",STR_PAD_LEFT);
				$this->Cell(25,$h,$i,1,0,'C');
				$this->Cell(30,$h,"PENSIONES",1,0,'C');
				$this->Cell(30,$h,"RECIBO",1,0,'C');
				$this->Cell(30,$h,$recibon,1,0,'C');
				$this->Cell(25,$h,number_format($rowD['importe'],2),1,0,'R');
				$this->Cell(25,$h,$this->DecFecha($rowD['fechaemision']),1,0,'C');
				$this->Cell(0,$h,$this->DecFecha($rowD['fechavencimiento']),1,1,'C');
			}
			//RECIBOS
			$this->SetFont('Arial','B',8);
			$this->Cell(115,$h,"Deuda Quebrada S/. ",1,0,'C');
			$this->Cell(25,$h,number_format($TotalRef,2),1,1,'R');

			
			
		

		}
    }

	$nroquiebre	= $_GET["id"];
	$codsuc		= $_GET["codsuc"];
	$NombreEmpresa = $_SESSION['NombreEmpresa'];
    $objReporte	=	new clsCreditos();
	$objReporte->AliasNbPages();
    $objReporte->AddPage();
	$objReporte->Contenido($nroquiebre,$codsuc);
	$objReporte->Output();	
	
	
?>