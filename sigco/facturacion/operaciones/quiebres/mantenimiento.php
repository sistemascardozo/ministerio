<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

include("../../../../objetos/clsDrop.php");

$Op = $_GET["Op"];
$Id = isset($_GET["Id"]) ? $_GET["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=".$Op;

$fecha = date("d/m/Y");

$objMantenimiento = new clsDrop();
$sininteres = 0;
$paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);
$sql = "SELECT nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
    FROM facturacion.periodofacturacion
    WHERE codemp=1 and codsuc=? and codciclo=1 and facturacion=0";
$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc));
$row = $consulta->fetch();
$TasaInteres = $row['tasapromintant'] / 100;
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>

<style type="text/css">
    .CodConcepto { display: none;}
</style>
<script>
    var cont = 0;
    var object = "";
    var codsuc = <?=$codsuc ?>;
    var nrofact = 0;

    var nitem = 0;
    var TasaInteres = <?=$TasaInteres ?>;
    var TasaInteresO = <?=$TasaInteres ?>;
    var urldir = "<?php echo $_SESSION['urldir'];?>";
    function ValidarForm(Op)
    {
        if ($("#nroinscripcion").val() == "")
        {
            $("#tabs").tabs({SELECTed: 0});
            alert("Ingrese el nro. de inscripcion del usuario")
            return false
        }
        if ($("#glosarefinanciamiento").val() == "")
        {           
            $("#glosarefinanciamiento").val('QUIEBRE')
        }
        
        var nrocuotas = $("#nrocuotas").val();
        if (parseFloat($("#cuotainicial").val()) > 0)
            nrocuotas++
        $("#nrocuotas").val(nrocuotas)
          
        //enviar solo fcturaciones chekeadas
        $('#tbfacturacion tbody input.input-chk').each(function ()
        {
            if (!$(this).attr("checked"))           {

                $(this).siblings('.input-facturacion').attr("name", '')
                $(this).siblings('.input-codtipodeuda').attr("name", '')
                $(this).siblings('.input-codciclo').attr("name", '')
            }
        });


        GuardarP(Op)
        //$("#Nuevo").dialog("close");
    }
    
    function cleartbRefinanciamiento() {
        var nro = $("#NroConceptos").val();
        if (nro > 0) {
            QuitaRow();
            clearcuotas();
            $("#cuotamensual").val('0.00')
            $("#cuotainicial").val('0.00').removeAttr('readonly');
            $("#nrocuotas").val('')
        }

    }
    
    var totalrecibos = 0;
    function check_deuda(obj) {
        //cleartbRefinanciamiento();
        //alert('Hola');
        var tr = $(obj).parents("tr:eq(0)");
        var importe = parseFloat(tr.find("input.input-deuda").val()).toFixed(2);
        var val = parseFloat($("#totalquiebre").val());
        var newval;
        if (obj.checked) {
            newval = val + parseFloat(importe);
        }
        else {
            newval = val - importe;
        }
        totalrecibos = newval;
        var importetotalcol = str_replace($("#importetotal").val(), ',', '');
        newval = parseFloat(newval) + parseFloat(importetotalcol);
        $("#totalquiebre").val(newval);
        $("#totalquiebrelbl").html(newval.toFixed(2))
    }
</script>
<script src="js_refinanciamiento.js" type="text/javascript"></script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos de Quiebre</a></li>
                                
                            </ul>
                            <div id="tabs-1" style="height: 390px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="80" >Usuario</td>
                                        <td width="17" align="center">:</td>
                                        <td colspan="7" >
                                            <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" value="<?php echo $row["codantiguo"] ?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:70px;"/>
                                            <input type="hidden" name="nroinscripcion" readonly="readonly" id="nroinscripcion" size="15" maxlength="15" class="inputtext" value="" />
                                            <span class="MljSoft-icon-buscar" title="Buscar Clientes" onclick="buscar_usuario();"></span>
                                            <input type="text" name="propietario" id="propietario" class="inputtext" maxlength="60" readonly="readonly" style="width:400px;" />
                                            <label>
                                                <input type="hidden" name="tipo" id="tipo" value="1" />
                                                <input type="hidden" name="anio" id="anio" />
                                                <input type="hidden" name="mes" id="mes" />
                                                <input type="hidden" name="ciclo" id="ciclo" value="0" />
                                                <input type="hidden" name="NroConceptos" id="NroConceptos" value="0" />
                                                <input type="hidden" name="count" id="NroConceptos" value="0" />
                                                <input type="hidden" name="totalquiebre" id="totalquiebre" value="0.00" />
                                                <input type="hidden" name="importetotal" id="importetotal" value="0.00" />
                                                
                                                <div id="DivSave"></div>
                                            </label>
                                        </td>
                                    </tr>	
                                    <tr>
                                        <td valign="top">Glosa</td>
                                        <td align="center" valign="top">:</td>
                                        <td colspan="7" >
                                            <textarea name="glosarefinanciamiento" id="glosarefinanciamiento" cols="100" rows="3" class="inputtext" style="font-size:11px"></textarea>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td colspan="9" class="TitDetalle">
                                            <input type="hidden" id="in_tipodeuda" value="" />
                                            <div style="height:280px; width:100%; overflow:scroll" id="div_facturacion">
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbfacturacion" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="11%" >Nro. Facturacion</th>
                                                            <th width="13%" >Doc.</th>
                                                            <th width="11%" >Nro. Doc.</th>
                                                            <th width="12%" >A&ntilde;o y Mes</th>
                                                            <th width="24%" >Tipo de Deuda</th>
                                                            <th width="11%" >Categoria</th>
                                                            <th width="10%" >Importe</th>
                                                            <th width="10%" >&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot class="ui-widget-header" >
                                                        <tr>
                                                            <td colspan="6">Total</td>
                                                            <td align="right"><label id="totalquiebrelbl">0.00</label></td>
                                                            <td width="10%" >&nbsp;</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                    </td>
                </tr>


            </tbody>
        </table>
    </form>
</div>
