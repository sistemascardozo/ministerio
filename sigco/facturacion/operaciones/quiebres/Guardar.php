<?php
include("clsproceso.php");
include("../../../../objetos/clsFunciones.php");

$objFunciones = new clsFunciones();

$codemp = 1;
$codsuc = $_SESSION["IdSucursal"];

$facturacion = $objFunciones->datosfacturacion($codsuc, $_POST["ciclo"]);
$glosa = $_POST["glosarefinanciamiento"];
$totalquiebre = $_POST["totalquiebre"];
$tipo = $_POST["tipo"];
$nrocuotas = $_POST["nrocuotas"];
$nroinscripcion = $_POST["nroinscripcion"];
$fechareg = $objFunciones->CodFecha($objFunciones->FechaServer());
$estadorefinanciamiento = 1;
$idusuario = $_SESSION["id_user"];
$count = $_POST["count"];
$interes = $_POST["interescredito"] ? $_POST["interescredito"] : 0;
$sininteres = $_POST["sininteres"] ? $_POST["sininteres"] : 0;
$Cuotas = $_SESSION["orefinanciamiento"];
$anio = $facturacion["anio"];
$mes = $facturacion["mes"];

$nroquiebre = $objFunciones->setCorrelativosVarios(22, $codsuc, "SELECT", 0);

$conexion->beginTransaction();

//* Aqgregar un tipo -->Quiebre y el Incobrable
$sqlL = "INSERT INTO facturacion.cabquiebres(nroquiebre, codemp, codsuc, tipo, nroinscripcion, fechaemision, 
    estareg, totalquiebre, creador,glosa , hora,anio,mes) 
    VALUES(:nroquiebre, :codemp, :codsuc, :tipo, :nroinscripcion, :fechaemision, :estareg, 
    :totalquiebre,  :creador, :glosa, :hora,:anio,:mes)";

$consultaL = $conexion->prepare($sqlL);
$consultaL->execute(array(":nroquiebre" => $nroquiebre,
    ":codemp" => $codemp,
    ":codsuc" => $codsuc,
    ":tipo" => $tipo,
    ":nroinscripcion" => $nroinscripcion,
    ":fechaemision" => $fechareg,
    ":estareg" => $estadorefinanciamiento,
    ":totalquiebre" => $totalquiebre,    
    ":creador" => $idusuario,
    ":glosa" => $glosa,   
    ":hora" => date('H:i'),
    ":anio"=>$anio,
    ":mes"=>$mes));

    if ($consultaL->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
$Facturaciones = array();
foreach($_POST['facturaciondet'] as $i => $nrofact)
{
    // if (!array_key_exists($nrofact, $Facturaciones))
    // {
        $Facturaciones[$nrofact] = array("Id"=>$nrofact);
        $codciclo= $_POST["codciclodet"][$i];
        $codtipodeuda= $_POST["codtipodeuda"][$i];
       $Sql  = "SELECT *,(importe -(imppagado + importerebajado)) as importetotal ";
        $Sql .= "FROM facturacion.detfacturacion  ";
        $Sql .= "WHERE codemp=1 and codsuc=? AND nroinscripcion=? 
                and nrofacturacion=?  
                AND codciclo=? 
                AND codtipodeuda=?/*and codtipodeuda=10 OR codconcepto=101*/";
        $consultaF = $conexion->prepare($Sql);
        $consultaF->execute(array($codsuc,$nroinscripcion,$nrofact,$codciclo,$codtipodeuda));

        $items = $consultaF->fetchAll();
        foreach($items as $row)
        {

            $upd = "UPDATE facturacion.detfacturacion set codtipodeuda=11
                        WHERE codemp=".$codemp." and codsuc=".$codsuc." AND nrofacturacion=".$nrofact." 
                        and nroinscripcion='".$nroinscripcion."'
                          AND codciclo=".$row['codciclo']."
                          AND codtipodeuda=".$codtipodeuda." /*and codtipodeuda=10 OR codconcepto=101*/";
            $consultaU = $conexion->query($upd);
           if ($consultaU->errorCode() != '00000') 
           {
                $conexion->rollBack();
                $mensaje = "Error detfacturacion";
                die(2);
            }
            
            
            //* Aqgregar un tipo -->Quiebre y el Incobrable
            $sqlO = "INSERT INTO facturacion.origen_quiebre(
                    codemp,codsuc,nroquiebre,tipo,codciclo,nrofacturacion,nroinscripcion,
                    codconcepto,nrocredito,nrocuota,item,importe,estadofacturacion,
                    codtipodeuda,categoria,nrorefinanciamiento)
                VALUES(".$codemp.",".$codsuc.",".$nroquiebre.",".$tipo.",".$codciclo.",".$nrofact.",
                    '".$nroinscripcion."',".$row['codconcepto'].",
                ".$row['nrocredito'].",".$row['nrocuota'].",".$row['item'].",
                ".$row['importetotal'].",".$row['estadofacturacion'].",
                ".$row['codtipodeuda'].",".$row['categoria'].",".$row['nrorefinanciamiento'].")";
            //die($sqlO);
            $consultaO = $conexion->query($sqlO);
            
            if($consultaO->errorCode()!='00000')
            {
                $conexion->rollBack();
                $mensaje = "Error INSERT origen_quiebre";
                $data['res']       = 2; 
                die(json_encode($data));
            }
        }
    //}
}
if($consultaO->errorCode()!='00000')
    {
        $conexion->rollBack();
        $mensaje = "Error INSERT origen_quiebre";
        $data['res']       = 2; 
        die(json_encode($data));
    }
else
{
    $conexion->commit();
    $objFunciones->setCorrelativosVarios(22, $codsuc, "UPDATE", $nroquiebre);
    $mensaje = "El Registro se ha Grabado Correctamente";
    $res = 1;
    $data['res'] = $res;
    $data['nroquiebre'] = $nroquiebre;
    echo json_encode($data);
}

?>
