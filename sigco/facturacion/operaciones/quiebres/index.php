<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
	
    $TituloVentana = "QUIEBRES";
    $Activo = 1;
	
    CuerpoSuperior($TituloVentana);
	
    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
    $codsuc = $_SESSION['IdSucursal'];
    $FormatoGrilla = array();
    $documento = 11;
    $Criterio = 'QUIEBRES';
    unset($_SESSION["oquiebres"]);
	
    $Sql = "SELECT cab.nroquiebre,clie.codantiguo,clie.propietario,".$objFunciones->FormatFecha('cab.fechaemision').",
        e.descripcion, u.login,".$objFunciones->Convert("cab.totalquiebre", "NUMERIC (18,2)").",cab.nrocuotas,cab.cuotames,1,
        cab.nroinscripcion
        FROM facturacion.cabquiebres as cab
        INNER JOIN catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion)
        INNER JOIN seguridad.usuarios as u ON (cab.codemp=u.codemp and cab.creador=u.codusu)
        INNER JOIN public.estadoreg as e ON (e.id=cab.estareg )";
    $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
    $FormatoGrilla[1] = array('1' => 'cab.nroquiebre', '2' => 'clie.propietario', '3' => 'clie.codantiguo');          //Campos por los cuales se hará la búsqueda
    $FormatoGrilla[2] = $Op;                                                            //Operacion
    $FormatoGrilla[3] = array('T1' => 'Nro. Quiebre', 'T2' => 'Nro.Inscripcion', 'T3' => 'Propietario', 'T4' => 'Fecha Emision',
        'T5' => 'Estado', 'T6' => 'Usuario', 'T7' => 'Total', 'T8' => 'Cuotas', 'T9' => 'Cuota Mensual');   //Títulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'center', 'A3' => 'left', 'A4' => 'center', 'A7' => 'right', 'A6' => 'center');                        //Alineación por Columna
    $FormatoGrilla[5] = array('W1' => '60', 'W2' => '100', 'W3' => '400', 'W4' => '95', 'W5' => '50', 'W6' => '95', 'W7' => '95', 'W8' => '95');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
    $FormatoGrilla[7] = 1200;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = "  and (cab.codemp=1 and cab.codsuc=".$codsuc.") ORDER BY cab.nroquiebre desc ";                                   //Orden de la Consulta
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '1', //Número de Botones a agregar
        'BtnId1' => 'BtnGenerar', //y aparece este boton
        'BtnI1' => 'documento.png',
        'Btn1' => 'Imprimir Quiebre',
        'BtnF1' => 'onclick="imprimir_contrato(this.id)"',
        'BtnCI1' => '10',
        'BtnCV1' => '1',

    );
    $FormatoGrilla[10] = array(array('Name' => 'ref', 'Col' => 13), array('Name' => 'id', 'Col' => 1),
        array('Name' => 'estado', 'Col' => 13), array('Name' => 'nroprepago', 'Col' => 11), array('Name' => 'nroprepagoinicial', 'Col' => 12),
        array('Name' => 'nroinscripcion', 'Col' => 13), array('Name' => 'nroquiebres', 'Col' => 1)); //DATOS ADICIONALES
    $FormatoGrilla[11] = 7; //FILAS VISIBLES            
    $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 950, 600);
    Pie();
?>
<script type="text/javascript">
    function imprimir_ficha(id)
    {
        AbrirPopupImpresion("imprimir.php?id=" + id + "&codsuc=<?=$codsuc ?>", 800, 600)
    }
    function imprimir_contrato(id)
    {
        AbrirPopupImpresion("imprimirc.php?id=" + id + "&codsuc=<?=$codsuc ?>", 800, 600)
    }
    function imprimir_c(obj)
    {
        var nropago = $(obj).parent().parent().data('nroprepago')
        var nroprepagoinicial = $(obj).parent().parent().data('nroprepagoinicial')
        var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
        var femision = $(obj).parent().parent().data('femision')
        if (nropago != 0)
        {
            var url = '../../../cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php'
            url += '?codpago=' + nropago + '&nroinscripcion=' + nroinscripcion + '&fechapago=' + femision + '&codsuc=<?=$codsuc ?>';
            var ventana = window.open(url, 'Buscar', 'width=800, height=600, resizable=yes, scrollbars=yes, status=yes,location=yes');
            ventana.focus();
        }
    }
    function imprimir_i(obj)
    {
        var nropago = $(obj).parent().parent().data('nroprepago')
        var nropago = $(obj).parent().parent().data('nroprepagoinicial')
        var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
        var femision = $(obj).parent().parent().data('femision')
        if (nropago != 0)
        {
            var url = '../../../cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php'
            url += '?codpago=' + nropago + '&nroinscripcion=' + nroinscripcion + '&fechapago=' + femision + '&codsuc=<?=$codsuc ?>';
            var ventana = window.open(url, 'Buscar', 'width=800, height=600, resizable=yes, scrollbars=yes, status=yes,location=yes');
            ventana.focus();
        }
    }
    function Pagos(obj)
    {

        var nroquiebres = $(obj).parent().parent().data('nroquiebres')
        $("#Modificar").dialog({title: 'Pago Cuotas'});

        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function () {
            GuardarOk();
        });
        $("#button-maceptar").children('span.ui-button-text').text('Pago Cuotas')
        $.ajax({
            url: 'pagos/index.php',
            type: 'POST',
            async: true,
            data: 'Id=' + nroquiebres + '&codsuc=<?=$codsuc ?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function GuardarOk()
    {

        var val = parseFloat($("#PagoAdelantado").val()).toFixed(2);

        if (val <= 0)
        {
            Msj($("#tbdetalle"), 'Seleccione Cuota a Cancelar')
            return false
        }


        $.ajax({
            url: 'pagos/Guardar.php?Op=0',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: $('#form1').serialize(),
            success: function (data)
            {

                if (data.res == 1)
                {

                    if (confirm("Desea Imprimir el Recibo?"))
                        AbrirPopupImpresion(urldir + "sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php?codpago=" + data.nroprepagocre + "&nroinscripcion=" + $("#nroinscripcion").val() + '&fechapago=' + $("#femision").val() + '&codsuc=' + codsuc, 800, 600)

                    OperMensaje(data.res)
                    $("#Mensajes").html(data.res);
                    $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                    $("#Modificar").dialog("close");

                    // Buscar(Op);
                }

            }
        })
    }
</script>

<?php CuerpoInferior(); ?>