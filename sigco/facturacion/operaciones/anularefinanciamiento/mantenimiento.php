<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsDrop.php");

$Op = $_POST["Op"];
$nrorefinanciamiento = isset($_POST["Id"]) ? $_POST["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=".$Op;
$formapago = 1;
$fecha = date("d/m/Y");

$objMantenimiento = new clsDrop();
$sininteres = 0;
$paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);

$sql = "SELECT upper(clie.propietario) as propietario,cab.fechaemision,cab.totalrefinanciado,";
$sql .= "''as concepto,cab.creador,upper(doc.abreviado),clie.nrodocumento,";
$sql .= "cab.glosa,clie.nroinscripcion,
    upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
    zo.descripcion as zona,cab.nroprepago,clie.codantiguo,cab.nroprepagoinicial ";
$sql .= "from facturacion.cabrefinanciamiento as cab ";
$sql .= "inner join catastro.clientes as clie on(cab.codemp=clie.codemp and ";
$sql .= "cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion) ";

$sql .= "inner join public.tipodocumento as doc on(clie.codtipodocumento=doc.codtipodocumento)
    inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
    inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
    inner join admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)";
$sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrorefinanciamiento=?";

$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc, $nrorefinanciamiento));
$row = $consulta->fetch();

$TasaInteres = $row['interes'] / 100;
?>

<style type="text/css">
    .CodConcepto { display: none;}
</style>
<script>
    var cont = 0;
    var object = "";
    var codsuc = <?=$codsuc ?>;
    var nrofact = 0;

    var nitem = 0;
    var TasaInteres = <?=$TasaInteres ?>;
    var TasaInteresO = <?=$TasaInteres ?>;
    var urldir = "<?php echo $_SESSION['urldir']; ?>";
    function ValidarForm(Op)
    {
        if ($("#nroinscripcion").val() == "")
        {
            $("#tabs").tabs({selected: 0});
            //alert("Ingrese el nro. de inscripcion del usuario")
            Msj($("#nroinscripcion"), "Ingrese el nro. de inscripcion del usuario ")
            return false
        }
        $("#nroinscripcion").removeClass("ui-state-error");
        
        if ($("#glosarefinanciamiento").val() == "")
        {            
            $("#glosarefinanciamiento").val('REFINANCIAMIENTO')
        }

        if ($("#nrocuotas").val() == 0 || $("#nrocuotas").val() == "")
        {
            $("#tabs").tabs({selected: 2});
            //alert("No hay Ningun Refinanciamiento Generado...Genere como minima una para poder continuar")
            Msj($("#nrocuotas"), "No hay Ningun Refinanciamiento Generado...Genere como minima una para poder continuar ")
            return false
        }
        $("#nrocuotas").removeClass("ui-state-error");
        
        var nrocuotas = $("#nrocuotas").val();
        if (parseFloat($("#cuotainicial").val()) > 0)
            nrocuotas++
        $("#nrocuotas").val(nrocuotas)
        ///CALCULAR IMPUT DE COLATERALES
        var id = parseInt($("#TbIndex tbody tr").length)
        var tr = '';
        var CodConcepto, SubTotal, IGV, Redondeo, Colateral = '';
        for (var i = 1; i <= id; i++)
        {

            CodConcepto = $("#TbIndex tbody tr#" + i + " label.CodConcepto").text()
            SubTotal = $("#TbIndex tbody tr#" + i + " label.SubTotal").text()
            IGV = $("#TbIndex tbody tr#" + i + " label.IGV").text()
            Redondeo = $("#TbIndex tbody tr#" + i + " label.Redondeo").text()
            Importe = $("#TbIndex tbody tr#" + i + " label.Importe").text()
            Colateral = $("#TbIndex tbody tr#" + i + " label.Colateral").text()
            SubTotal = str_replace(SubTotal, ',', '');
            IGV = str_replace(IGV, ',', '');
            Redondeo = str_replace(Redondeo, ',', '');
            Importe = str_replace(Importe, ',', '');
            tr += '<input type="hidden" name="itemC' + i + '"  value="' + i + '"/>';
            tr += '<input type="hidden" name="codconceptoC' + i + '"  value="' + CodConcepto + '"/>';
            tr += '<input type="hidden" name="subtotalC' + i + '"  value="' + SubTotal + '"/>';
            tr += '<input type="hidden" name="igvC' + i + '"  value="' + IGV + '"/>';
            tr += '<input type="hidden" name="redondeoC' + i + '"  value="' + Redondeo + '"/>';
            tr += '<input type="hidden" name="imptotalC' + i + '"  value="' + Importe + '"/>';
            tr += '<input type="hidden" name="Colateral' + i + '"  value="' + Colateral + '"/>';
        }
        tr += '<input type="hidden" name="NroItems" id="Items"  value="' + id + '"/>';
        $("#DivSave").html(tr)

        ///CALCULAR IMPUT DE COLATERALES        
        //enviar solo fcturaciones chekeadas
        $('#tbfacturacion tbody input.input-chk').each(function ()
        {
            if (!$(this).attr("checked"))
            {

                $(this).siblings('.input-facturacion').attr("name", '')
            }
        });


        GuardarP(Op)
        //$("#Nuevo").dialog("close");
    }
    function agregar_detalle()
    {
        ncuota = Trim($("#nrocuota").val())
        var totrefin = 0;
        var ultcuota = 0;
        var diferencia = 0;
        var impmesx = 0;
        var cuotas = ncuota
        var nrofacturacion = nrofact;

        if (ncuota == "" || ncuota == 0)
        {
            alert("El Nro. de la Cuota no es Valido!!!")
            document.getElementById("nrocuota").focus();
            return
        }

        QuitaRow();

        nitem = $("#nrocuota").val()
        var cmes = $("#cuotames").val()

        var cuotaini = $("#cuotaini").val()
        var tmp_cuotaini = cuotaini;
        var cuotames = cmes

        var afecto_igv = $("#afecto_igv").val()
        var sumasubtotal = 0;
        var saldo = parseFloat($("#totrefinanciamiento").val())
        var inicio = 0;
        var count = 0;
        if (cuotaini > 0)
        {
            nrofacturacion = parseFloat(nrofacturacion) - 1;//SI TIENE CUOTA INICIAL RESTAR UNA FACTURACION
            if (afecto_igv == 1)//PRIMERA CUOTA IRA SIN IGV
                vigv = 0	//vigv 	= Round(CalculaIgv(cuotainicial,igv),2);
            else
                vigv = 0
            imp = parseFloat(cuotaini) + parseFloat(vigv)
            redondeo = CalcularRedondeo(imp)
            imp = parseFloat(imp) + parseFloat(redondeo)
            cuotas = parseFloat(cuotas) + 1;

            sumasubtotal = parseFloat(sumasubtotal) + parseFloat(cuotaini)

            //insertadetalle(mes,anio,cuotaini,vigv,redondeo,imp,1,nrofacturacion,0,1,cuotas,saldo,0,1)
            insertar_cuotas(nrofacturacion, cuotaini, 1, 'Pendiente', vigv, redondeo, imp, saldo, cuotaini, 0)
            //mes				= parseFloat(mes)+1;
            nrofacturacion = parseFloat(nrofacturacion) + 1;
            inicio = 1;
            count = 1;

            /*if(mes>12)
             {
             mes=1;
             anio=parseFloat(anio) + 1;
             }*/
        }
        saldo = saldo - cuotaini;
        sumasubtotal = 0

        for (var i = 1; i <= ncuota; i++)
        {
            var interes = saldo * TasaInteres;
            interes = parseFloat(interes).toFixed(1)
            inicio = parseFloat(inicio) + 1;
            if (afecto_igv == 1)
                vigv = CalculaIgv(cuotames, igv);
            else
                vigv = 0
            cuota = cuotames - vigv - parseFloat(interes)
            if (i == ncuota)
            {
                imp = parseFloat(saldo, 2).toFixed(2) + parseFloat(vigv)
            }
            else
            {
                imp = parseFloat(cuota) + parseFloat(vigv)
            }

            redondeo = CalcularRedondeo(imp)
            imp = parseFloat(imp) + parseFloat(redondeo) + parseFloat(interes)

            sumasubtotal = parseFloat(sumasubtotal) + imp//parseFloat($("#cuotamensual").val())


            insertar_cuotas(nrofacturacion, imp, inicio, 'Pendiente', vigv, redondeo, imp, saldo, cuota, interes)
            saldo -= cuota;
            count = parseFloat(count) + 1;
            //mes 			= parseFloat(mes) + 1;
            nrofacturacion = parseFloat(nrofacturacion) + 1;
            /*if(mes>12)
             {
             mes=1;
             anio=parseFloat(anio) + 1;
             }*/
        }

        //$("#count").val(count)


        ///////////////////////77
        $("#cuotamensual").val(parseFloat(cmes).toFixed(2))
        $("#nrocuotas").val(ncuota)
        //$("#cuotainicial").attr('readonly','readonly');

        guardarcuotas();
        //close_frmpop();
    }

    function cleartbRefinanciamiento() {
        var nro = $("#NroConceptos").val();
        if (nro > 0) {
            QuitaRow();
            clearcuotas();
            $("#cuotamensual").val('0.00')
            $("#cuotainicial").val('0.00').removeAttr('readonly');
            $("#nrocuotas").val('')
        }

    }
    var totalrecibos = 0;
    function check_deuda(obj)
    {

        var tr = $(obj).parents("tr:eq(0)");
        var importe = parseFloat(tr.find("input.input-deuda").val()).toFixed(2);
        var val = parseFloat($("#PagoAdelantado").val());
        var newval;
        $(".input-paso").val(0)
        if (obj.checked)
        {
            newval = /* val +*/ parseFloat(importe);
            tr.find("input.input-paso").val(1)
        }
        else
        {
            newval = /*val -*/ importe;

            tr.find("input.input-paso").val(0)
        }
        $("#PagoAdelantado").val(newval);
        $("#LblPagoAdelantado").html(newval.toFixed(2));
    }
    function ImprimirC(nropago, femision)
    {

        if (nropago != 0)
        {
            var url = '../../../cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php'
            url += '?codpago=' + nropago + '&nroinscripcion=<?=$row['nroinscripcion'] ?>&fechapago=' + femision + '&codsuc=<?=$codsuc ?>';
            var ventana = window.open(url, 'Buscar', 'width=800, height=600, resizable=yes, scrollbars=yes, status=yes,location=yes');
            ventana.focus();
        }
    }
</script>
<script src="js_refinanciamiento.js" type="text/javascript"></script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos de Refinanciamiento</a></li>
                                <!-- <li style="font-size:10px"><a href="#tabs-2">Colaterales</a></li> -->
                                <li style="font-size:10px"><a href="#tabs-3">Cronograma</a></li>

                            </ul>
                            <div id="tabs-1" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td width="80" >Usuario</td>
                                        <td width="17" align="center">:</td>
                                        <td colspan="7" >
                                            <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" readonly="readonly" value="<?php echo $row["codantiguo"] ?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:75px;"/>
                                            <input type="hidden" name="nroinscripcion" readonly="readonly" id="nroinscripcion" size="15" maxlength="15" class="inputtext" value="<?php echo $row["nroinscripcion"] ?>" />

                                            <input type="text" name="propietario" id="propietario" class="inputtext" size="60" maxlength="60" readonly="readonly" value="<?php echo $row["propietario"] ?>"/>
                                            <label>
                                                <input type="hidden" name="anio" id="anio" />
                                                <input type="hidden" name="mes" id="mes" />
                                                <input type="hidden" name="ciclo" id="ciclo" value="0" />
                                                <input type="hidden" name="NroConceptos" id="NroConceptos" value="0" />
                                                <input type="hidden" id="nrorefinanciamiento" name="nrorefinanciamiento"  value="<?=$nrorefinanciamiento ?>"/>
                                                <input type="hidden" id="codconcepto" name="codconcepto"  value="992"/>
                                                <input type="hidden" id="Colateral" name="Colateral"  value="PAGO DE CONVENIO"/>
                                                <div id="DivSave"></div>
                                            </label></td>
                                    </tr>	
                                    <tr>
                                        <td valign="top">Glosa</td>
                                        <td align="center" valign="top">:</td>
                                        <td colspan="7" ><textarea readonly="readonly" name="glosarefinanciamiento" id="glosarefinanciamiento" cols="100" rows="3" class="inputtext" style="font-size:11px"><?php echo $row["glosa"] ?></textarea></td>
                                    </tr>	

                                    <tr>
                                        <td colspan="9" class="TitDetalle">
                                            <input type="hidden" id="in_tipodeuda" value="" />
                                            <div style="height:220px; width:100%; overflow:scroll" id="div_facturacion">
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbfacturacion" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="11%" >Item</th>
                                                            <th width="13%" >Facturacion</th>
                                                            <th width="13%" >Tipo Recibo</th>
                                                            <th width="11%" >Nro. Doc.</th>
                                                            <th width="11%" >Monto</th>
                                                            <th width="11%" >Emisión</th>
                                                            <th width="10%" >Vencimiento</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $Sql = "SELECT f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg 
                                                            FROM facturacion.cabfacturacion f
                                                            INNER JOIN facturacion.origen_refinanciamiento o ON (f.codemp = o.codemp)
                                                            AND (f.codsuc = o.codsuc)
                                                            AND (f.codciclo = o.codciclo)
                                                            AND (f.nrofacturacion = o.nrofacturacion)
                                                            AND (f.nroinscripcion = o.nroinscripcion)
                                                            where o.codemp=1 and o.codsuc=? and o.nrorefinanciamiento=?
                                                            GROUP BY f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg
                                                            ORDER BY  f.nrofacturacion";

                                                        $consulta2 = $conexion->prepare($Sql);
                                                        $consulta2->execute(array($codsuc, $nrorefinanciamiento));
                                                        $itemD = $consulta2->fetchAll();

                                                        foreach ($itemD as $rowD) {
                                                            $i++;
                                                            $Sql = "select sum(importe - (importerebajado))
                                                                from facturacion.detfacturacion
                                                                where codemp=1 and codsuc=? and nroinscripcion=? AND nrofacturacion=?";

                                                            $result = $conexion->prepare($Sql);
                                                            $result->execute(array($codsuc, $row['nroinscripcion'], $rowD['nrofacturacion']));
                                                            $row2 = $result->fetch();
                                                            $TotalRef+= $row2[0];
                                                            $recibon = str_pad($rowD['serie'], 3, "0", STR_PAD_LEFT)." - ".str_pad($rowD['nrodocumento'], 7, "0", STR_PAD_LEFT);
                                                            ?>
                                                            <tr align="center">
                                                                <td><?=$i ?></td>
                                                                <td><?=$rowD['nrofacturacion'] ?>-PENSIONES</td>
                                                                <td>RECIBO</td>
                                                                <td><?=$recibon ?></td>
                                                                <td align="right"><?=number_format($row2[0], 2) ?></td>
                                                                <td><?=$objMantenimiento->DecFecha($rowD['fechareg']) ?></td>
                                                                <td><?=$objMantenimiento->DecFecha($rowD['fechareg']) ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-3" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">	
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                        <td width="90" >&nbsp;</td>
                                        <td width="104" align="right">&nbsp;</td>
                                        <td width="17" align="center">&nbsp;</td>
                                        <td width="90" >&nbsp;</td>
                                        <td width="99" >&nbsp;</td>
                                        <td width="21" >&nbsp;</td>
                                        <td width="274" >&nbsp;</td>
                                    </tr>
                                    <tr style="height:5px">
                                        <td colspan="9" class="TitDetalle">
                                            <div style="height:220px; overflow:scroll" id="div_refinanciamiento">
                                                <table id="tabla-cronograma" border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="10%" >Cuota</th>
                                                            <th width="12%" >Mes</th>
                                                            <th width="11%" >Año</th> 
                                                            <th width="8%" >Vencimiento</th>
                                                            <th width="8%" >Cuota</th>
                                                            <th width="11%" >Estado</th>
                                                    <!-- <th width="10%" >Fec.Pago</th> -->
                                                     <!-- <th width="1%" align="center" scope="col">&nbsp;</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sqldetalle = "SELECT  d.nrocuota,d.totalcuotas,d.anio,d.mes,
                                                            sum(importe) as importe,estadocuota,d.fechapago, a.nroprepago 
                                                            from facturacion.detrefinanciamiento d
                                                            LEFT JOIN facturacion.cabrefinanciamientosadelantos a ON (d.codemp = a.codemp)
                                                                    AND (d.codsuc = a.codsuc)  AND (d.nrorefinanciamiento = a.nrorefinanciamiento)
                                                                    AND (d.nrocuota = a.nrocuota)
                                                            where d.codemp=1 and d.codsuc=? and d.nrorefinanciamiento=?
                                                            group by d.nrocuota,d.totalcuotas,d.anio,d.mes,d.estadocuota,d.fechapago, a.nroprepago 
                                                            order by ".$objMantenimiento->Convert('anio', 'INTEGER')."  ,".$objMantenimiento->Convert('mes', 'INTEGER')."";

                                                        $consultaD = $conexion->prepare($sqldetalle);
                                                        $consultaD->execute(array($codsuc, $nrorefinanciamiento));
                                                        $itemD = $consultaD->fetchAll();
                                                        $Saldo = 0;
                                                        $TotalDeuda = 0;
                                                        foreach ($itemD as $rowD) {
                                                            $Dias = cal_days_in_month(CAL_GREGORIAN, $rowD["mes"], $rowD["anio"]);
                                                            if (intval($rowD["mes"]) < 10)
                                                                $rowD["mes"] = "0".$rowD["mes"];
                                                            if (intval($Dias) < 10)
                                                                $Dias = "0".$Dias;
                                                            $FechaP = "";
                                                            $Acuenta = "";
                                                            $FechaA = "";
                                                            $TotalDeuda+=$rowD["importe"];
                                                            $Saldo+=$rowD["importe"];
                                                            switch ($rowD["estadocuota"]) {
                                                                case '0': $Estado = "<span style='color:red'>PENDIENTE</span>";
                                                                    break;
                                                                case '1': $Estado = "<span style='color:red'>FACTURADO</span>";
                                                                    break;
                                                                case '2': $Estado = "<span style='color:red'>ANULADO</span>";
                                                                    $Saldo -= $TotalDeuda;
                                                                    break;
                                                                case '3': $Estado = "<span style='color:green'>CANCELADO</span>";
                                                                    $Saldo -= $TotalDeuda;
                                                                    break;
                                                                case '4': $Estado = "<span style='color:yellow'>QUEBRADO</span>";
                                                                    break;
                                                            }
                                                            ?>
                                                            <tr align="center">
                                                                <td><?=$rowD["nrocuota"]."/".$rowD["totalcuotas"] ?>
                                                                    <input type='hidden' value='<?=$rowD["importe"] ?>' class='input-deuda' />
                                                                </td>
                                                                <td><?=$meses[intval($rowD["mes"])] ?></td>
                                                                <td><?=$rowD["anio"] ?></td>
                                                                <td><?=$Dias."/".$rowD["mes"]."/".$rowD["anio"] ?></td>
                                                                <td align="right"><?=number_format($rowD["importe"], 2) ?></td>
                                                                <td>
                                                                    <input type="hidden" id="estado_cuota" name="estado_cuota" value="<?=$rowD["estadocuota"]; ?>">
                                                                    <input type="hidden" id="cuota" name="cuota" value="<?=$rowD["nrocuota"]; ?>">
                                                                    <?=$Estado ?>
                                                                </td>									
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <tfoot class="ui-widget-header" >
                                                        <tr align="center">
                                                            <td colspan="4"> Total</td>

                                                            <td align="right" ><label id="LblPagoAdelantado"><?=number_format($Saldo, 2) ?></label>
                                                                <input id="PagoAdelantado" name="imptotal" value="<?=$TotalDeuda ?>" type="hidden">
                                                                <input name="TotalCuotasP" value="<?=$total ?>" type="hidden">
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>


            </tbody>
        </table>
    </form>
</div>