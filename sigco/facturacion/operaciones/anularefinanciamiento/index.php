<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  $TituloVentana = "ANULACION DE REFINANCIAMIENTO";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  $FormatoGrilla = array ();
  $documento  = 11;
  $Criterio='Refinanciamiento';
  unset($_SESSION["orefinanciamiento"]);
  $Sql = "SELECT cab.nrorefinanciamiento,clie.codantiguo,clie.propietario,".$objFunciones->FormatFecha('cab.fechaemision').",
            e.descripcion,u.login,".$objFunciones->Convert("cab.totalrefinanciado", "NUMERIC (18,2)").",cab.nrocuotas,cab.cuotames,1,
            cab.nroprepago,cab.nroprepagoinicial,cab.nroinscripcion, cab.estareg
            FROM facturacion.cabrefinanciamiento as cab
            INNER JOIN catastro.clientes as clie ON(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and
            cab.nroinscripcion=clie.nroinscripcion)
            INNER JOIN seguridad.usuarios as u ON (cab.codemp=u.codemp and cab.creador=u.codusu)
            INNER JOIN public.estadorefinanciamiento as e ON (cab.codestadorefinanciamiento=e.codestadorefinanciamiento )";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'cab.nrorefinanciamiento', '2'=>'clie.propietario','3'=>'clie.codantiguo');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Nro. Refianciamiento','T2'=>'Nro.Inscripcion', 'T3'=>'Propietario', 'T4'=>'Fecha Emision',
                            'T5'=>'Estado','T6'=>'Usuario', 'T7'=>'Total', 'T8'=>'Cuotas', 'T9'=>'Cuota Mensual');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center','A2'=>'center', 'A3'=>'left','A4'=>'center','A7'=>'right','A6'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60','W2'=>'100','W3'=>'400','W4'=>'95','W5'=>'50','W6'=>'95','W7'=>'95','W8'=>'95');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1200;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  and (cab.codemp=1 and cab.codsuc=".$codsuc.") ORDER BY cab.nrorefinanciamiento desc ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnRestablecer', //y aparece este boton
              'BtnI1'=>'eliminar.png', 
              'Btn1'=>'Anular Refinanciamiento', 
              'BtnF1'=>'onclick="Anular(this)"', 
              'BtnCI1'=>'14', //Item a Comparar
              'BtnCV1'=>'1',  //Valor de comparación
              );
  $FormatoGrilla[10] = array(array('Name' =>'id','Col'=>1),
  array('Name' =>'estado','Col'=>14),array('Name' =>'nroprepago','Col'=>11),array('Name' =>'nroprepagoinicial','Col'=>12),
  array('Name' =>'nroinscripcion','Col'=>13));//DATOS ADICIONALES
  $FormatoGrilla[11]=7;//FILAS VISIBLES            
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],950,600);
  Pie();
  
?>
<script type="text/javascript">
	function Anular(obj)
    {
        var Id     = $(obj).parent().parent().attr('id');
        var Estado = $(obj).parent().parent().attr('data-estado');
		
		Id = Id.substr(Id.indexOf('-') + 1);
		
        if (Estado == 0)
        {
            alert('No se puede Anular el refinanciamiento. El estado que se encuentra, no permite anularlo')
            return false
        }


        $("#Modificar").dialog({title: 'Anulacion de Refinanciamiento'});

        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function() {
            GuardarOk();
        });
        $("#button-maceptar").children('span.ui-button-text').text('Confirmar Anulacion')
        $.ajax({
            url: 'mantenimiento.php',
            type: 'POST',
            async: true,
            data: 'Id=' + Id + '&codsuc=<?= $codsuc ?>',
            success: function(data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function GuardarOk()
    {

        verificar_cuotapagagas();

        $.ajax({
            url: 'Guardar.php?Op=0',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: $('#form1').serialize(),
            success: function(data) 
            {
              if(data.res == 1)
              {                       
                OperMensaje(data.res)
                $("#Mensajes").html(data.res);
                $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                $("#Modificar").dialog("close");
                   
              }

            }
        })
    }

    function verificar_cuotapagagas(){
      var elemen;
      var cuot;
      $('table#tabla-cronograma tbody tr').each(function() {
          
          elemen = parseInt($(this).children('td:eq(5)').children('#estado_cuota').val());
          cuot   = parseInt($(this).children('td:eq(5)').children('#cuota').val());
          if(elemen == 3 && cuot != 1){
            
            Msj($("#tabs"),"No se podra anular por que existe un pago realizado");
            return false;
          }

      });
    }
	
	$('#BtnNuevoB').hide();
</script>

<?php CuerpoInferior(); ?>