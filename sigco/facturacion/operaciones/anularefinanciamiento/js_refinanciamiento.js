// JavaScript Document
function buscar_usuario()
{
	object="codcliente";
	AbrirPopupBusqueda('../../../catastro/operaciones/actualizacion/?Op=5',1100,500);
}
function Recibir(id)
{
	if(object=="codcliente")
	{
		datos_usuarios(id)
		verfacuracionsaldo(id)	
	}
	if(object=="codconcepto")
	{
		datos_conceptos(id)
	}
}
function datos_conceptos(id)
{
	$.ajax({
		 url:'../../../../ajax/mostrar/conceptos.php',
		 type:'POST',
		 async:true,
		 data:'conceptos='+id+'&codsuc='+codsuc,
		  dataType: 'json',
		 success:function(datos){
			if(datos.codconcepto!=null)
			{
				var igv=0;
				$("#CodConcepto").val(datos.codconcepto)
				$("#Colateral").val(datos.descripcion)
				$("#subtotal").val(parseFloat(datos.importe).toFixed(2))

				$("#afecto_igv").val(datos.afecto_igv)
				vigv=0
				if(datos.afecto_igv==1)
				{
					vigv = CalculaIgv(datos.importe,$("#IgvP").val())

					$("#igv").val(vigv)
				}
				var redondeo	= CalcularRedondeo((parseFloat(datos.importe)+parseFloat(vigv)))
				$("#redondeo").val(redondeo.toFixed(2))
				var importe = parseFloat(datos.importe)+parseFloat(vigv)+parseFloat(redondeo)
				$("#importe").val(importe.toFixed(2))
				$("#cuotamensual").val( importe- parseFloat($("#cuotainicial").val()))
				$("#subtotal").focus().select();
			}
			else
			{
				Msj($("#CodConcepto"),'No Existe Colateral')
			}
		 }
	}) 
}
function datos_usuarios(id)
{
	QuitaRow();
	
	$.ajax({
		 url:'../../../../ajax/clientes.php',
		 type:'POST',
		 async:true,
         dataType: 'json',
		 data:'nroinscripcion='+id+'&codsuc='+codsuc,
		 success:function(datos){
			//var r=datos.split("|")
			
			$("#nroinscripcion").val(id)
			$("#propietario").val(datos.propietario)
			$("#codantiguo").val(datos.codantiguo)
			$("#ciclo").val(datos.codciclo)
			datos_facturacion(datos.codciclo)
		 }
	}) 
}
function MostrarDatosUsuario(datos)
{
	
	$("#nroinscripcion").val(datos.nroinscripcion)
	$("#propietario").val(datos.propietario)
	$("#ciclo").val(datos.codciclo)
	datos_facturacion(datos.codciclo)
	verfacuracionsaldo(datos.nroinscripcion)
	
}
function datos_facturacion(codciclo)
{
	$.ajax({
		 url:'../../../../ajax/periodo_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+codciclo+'&codsuc='+codsuc,
		 success:function(datos){
			var r=datos.split("|")
			
			nrofact = r[0];
			$("#anio").val(r[1])
			$("#mes").val(r[3])
		 }
	}) 
}
function verfacuracionsaldo(id)
{
	$.ajax({
		 url:'ver_facturacion_saldo.php',
		 type:'POST',
		 async:true,
		 data:'nroinscripcion='+id+'&codsuc='+codsuc,
		 success:function(datos){
			 var r=datos.split("|")

			 //QuitaRow()
			//$("#tbfacturacion tbody").empty()
			$("#tbfacturacion tbody" ).empty().append(r[0])
			totalrecibos=r[2];
			$("#totrefinanciamiento").val(r[2])
			$("#count").val(r[1])
			CalcularTotal();
		 }
	}) 
}
function QuitaRow()
{
		var miTabla 	= document.getElementById("tbfacturacion");
		miTabla.deleteRow(0);
		/*$( "#tbdetalle tbody" ).append("<tr style='color: white; font-weight: bold; font-size: 12px' align='center'>"+
									   "<th width='11%' >Nro. Facturacion</th>"+
									   "<th width='13%' >Doc.</th>"+
									   "<th width='11%' >Nro. Doc.</th>"+
									   "<th width='12%' >A&ntilde;o y Mes</th>"+
									   "<th width='24%' >Tipo de Deuda</th>"+
									   "<th width='11%' >Categoria</th>"+
									   "<th width='10%' >Importe</th>"+
									   "</tr>")
		//$("#count").val(0)
*/
}
$(document).ready(function(){
	
	//$( "#dialog:ui-dialog" ).dialog( "destroy" );
$( "#tabs" ).tabs();
cSerieNunC();
$("#codantiguo").focus();
	$( "#DivRef" ).dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
/*			show: "blind",
		hide: "scale",*/
		buttons: {
			"Aceptar": function() {
				agregar_detalle();
			},
			"Cancelar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {

		}
	});
})
function ver_montos()
{
	var Facturaciones='';
	 $('#tbfacturacion tbody input.input-chk').each(function()
       {
        if($(this).attr("checked"))
          {
            
            Facturaciones +=$(this).siblings('.input-facturacion').val()+','
          }
      });


	if($("#nroinscripcion").val()=="")
	{
		$("#tabs").tabs({selected: 0});
		Msj($("#nroinscripcion"),"Seleccione el Usuario")
		return 
	}
	var intipodeuda = getTipoDeuda();
	if(intipodeuda==''){
		$("#tabs").tabs({selected: 0});
		alert("Seleccione al menos una Deuda")
		return 	
	}
	
	if(isNaN($("#cuotainicial").val())){
		$("#tabs").tabs({selected: 2});
		alert("Ingrese Cuota Inicial valida")
		return 		
	}else if(parseFloat($("#cuotainicial").val()) > parseFloat($("#totrefinanciamiento").val())){
		$("#tabs").tabs({selected: 2});
		alert("La Cuota Inicial no debe superar al Monto Total de la Deuda")
		$("#cuotainicial").focus()
		return
	}

	generarcuotas($("#nroinscripcion").val(), intipodeuda,Facturaciones)	
	
}
function getTipoDeuda(){
	var intd = [], i = 0;
	$("#tbfacturacion tbody tr input[type=checkbox]" ).each(function(){
		var tr = $(this).parents("tr:eq(0)");
		var tipodeuda = tr.find("input.input-codtipodeuda").val();

		if(this.checked){
			intd[i] = tipodeuda;
			i++;
		}
	});
	//$("#count").val(i);
	return intd.join(',');
}
function generarcuotas(nroinscripcion, intipodeuda,Facturaciones)
{
	$.ajax({
		 url:'refinanciamiento.php',
		 type:'POST',
		 async:true,
		 data:'nroinscripcion='+nroinscripcion+'&codsuc='+codsuc+'&intipodeuda='+intipodeuda+"&cuotaini="+$("#cuotainicial").val()+'&Facturaciones='+Facturaciones,
		 success:function(datos){
				$("#div_ref").html(datos)
				AgregarColaterales()
				
	 	 }
	}) 
}
function AgregarColaterales()
{
	var id=parseInt($("#TbIndex tbody tr").length)
	var j=parseInt($("#tbconceptodeuda tbody tr").length)
	var tr='';
	var CodConcepto,SubTotal,IGV,Redondeo,Colateral='';
	var tr=''
	for(var i=1; i<=id; i++)
	{ 
		j++;
		CodConcepto =$("#TbIndex tbody tr#"+i+" label.CodConcepto").text()
		SubTotal    =$("#TbIndex tbody tr#"+i+" label.SubTotal").text()
		IGV         =$("#TbIndex tbody tr#"+i+" label.IGV").text()
		Redondeo    =$("#TbIndex tbody tr#"+i+" label.Redondeo").text()
		Importe     =$("#TbIndex tbody tr#"+i+" label.Importe").text()
		Colateral   =$("#TbIndex tbody tr#"+i+" label.Colateral").text()
		SubTotal = str_replace(SubTotal, ',','') ;
		IGV      = str_replace(IGV, ',', '') ;
		Redondeo = str_replace(Redondeo, ',','') ;
		Importe  = str_replace(Importe, ',','') ;
		tr+= '<tr style="background-color:#FFF">';
		tr+= '<td align="center">';
        tr+= '<input type="hidden" name="concepto'+j+'" id="concepto'+j+'" value="'+CodConcepto+'" />';
        tr+= ''+CodConcepto+'';
      	tr+= '</td>';
      	tr+= '<td align="left">'+Colateral+'</td>';
      	tr+= '<td align="right">';
        tr+= '<input type="hidden" name="montomesconcepto'+j+'" id="montomesconcepto'+j+'" value="'+Importe+'" />';
        tr+= ''+parseFloat(Importe).toFixed(2)+'';
      	tr+= '</td>';
      	tr+= '<td align="right">';
        tr+= '  <input type="hidden" name="montoultmes'+j+'" id="montoultmes'+j+'" value="0.00" />';
        tr+= '<input type="hidden" name="montomes'+j+'" id="montomes'+j+'" value="0.00" />';
        tr+= ' <label id="montoconcepto'+j+'">0.00</label>';
      	tr+= '</td>';
    	tr+= '</tr>';
	}
	$("#contador").val(parseInt($("#contador").val())+ id)
	var totalsaldo = $("#totalsaldo").val()
	var TotalColaterales = str_replace($("#importetotal").val(), ',','') ; 
	totalsaldo = parseFloat(totalsaldo) + parseFloat(TotalColaterales);
	$("#totalsaldo").val(totalsaldo);
	$("#Lbltotalsaldo").html(totalsaldo.toFixed(2))
	var totalafinanciar = $("#totalafinanciar").val()
	totalafinanciar = parseFloat(totalafinanciar)+parseFloat(TotalColaterales);
	//totalafinanciar=totalafinanciar//SUMAR INTERES
	
	$("#totalafinanciar").val(totalafinanciar)
	$("#Lbltotalafinanciar").html(totalafinanciar.toFixed(2))
	$("#tbconceptodeuda tbody ").append(tr);
	$( "#DivRef" ).dialog( "open" );
}
 function guardarcuotas()
{
	var count=$("#contador").val()
	var data=""
	
	for(i=1;i<=count;i++)
	{
		data += "concepto"+i+"="+$("#concepto"+i).val()+"&montoultmes"+i+"="+$("#montoultmes"+i).val()+
				"&montoconcepto"+i+"="+Round($("#montomes"+i).val(),2)+"&estado=0&"
	}
	$("#NroConceptos").val(count)
	var nrocuotas=$("#nrocuotas").val();
	if(parseFloat($("#cuotainicial").val())>0)
		nrocuotas++
	data+='count='+count
	data+='&NroCuotas='+nrocuotas
	data+='&cuotaini='+$("#cuotainicial").val();
	$.ajax({
		 url:'proceso.php',
		 type:'POST',
		 async:true,
		 data:data,
		 success:function(datos){

			$("#contador").val(datos)
			$( "#DivRef" ).dialog('close');

		}
	})
}
function clearcuotas(){
	$.ajax({
		 url:'proceso.php',
		 type:'POST',
		 async:true,
		 data:"count=0&NroCuotas=0",
		 success:function(datos){

			$("#NroConceptos").val(datos)
		}
	})	
}
function insertar_cuotas(nrofacturacion,importe,idx, estado,igv,redondeo,imp,saldo,subtotal,interes)
{
	$("#tbrefinanciamiento").append("<tr style='background-color:#FFFFFF'>"+
										  "<td align='center'>"+
										  "<input type='hidden' id='nrofactura"+idx+"' name='nrofactura"+idx+"' value='"+nrofacturacion+"' />"+nrofacturacion+"</td>"+
										  "<td align='center'>REFINANCIAMIENTO</td>"+
										  "<td align='center'>FACTURACION</td>"+
										  "<td align='right'><input type='hidden' name='saldo"+idx+ "' id='saldo"+idx+ "' value='" +saldo+ "' />" +parseFloat(saldo).toFixed(2) + "</td>"+
								   		   "<td align='right'><input type='hidden' name='stotal"+idx+"' id='stotal"+idx+"' value='"+subtotal+"' /><label id='stotalx"+idx+"'>"+parseFloat(subtotal).toFixed(2)+"</label></td>"+
								   			"<td align='right'><input type='hidden' name='redondeo"+idx+"' id='redondeo"+idx+"' value='"+redondeo+"' /><label id='redondeox"+idx+"' style='display:none'>"+parseFloat(redondeo).toFixed(2)+"</label>"+
								   			"<input type='hidden' name='interes"+idx+"' id='interes"+idx+"' value='"+interes+"' /><label id='interes"+idx+"'>"+parseFloat(interes).toFixed(2)+"</label></td>"+										  
								   			"<td align='right'><input type='hidden' name='igv"+idx+"' id='igv"+idx+"' value='"+igv+"' /><label id='igvx"+idx+"'>"+parseFloat(igv).toFixed(2)+"</label></td>"+
										  "<td align='right'>"+
										  "<input type='hidden' id='importemes"+idx+"' name='importemes"+idx+"' value='"+importe+"' />"+
										  "<label id='impmes"+idx+"' name='impmes"+idx+"' value='"+importe+"' >"+parseFloat(importe).toFixed(2)+"</label></td>"+
										  "<td align='center'>"+estado+"</td>"+
										  "</tr>")

}
function QuitaRow(){
	$("#tbrefinanciamiento tbody tr").remove();
}
/*
function QuitaRow_()
{
		var miTabla 	= document.getElementById("tbrefinanciamiento");
		var numFilas 	= miTabla.rows.length;
		
		for(i=1;i<=numFilas;i++)
		{
			try
			{
				miTabla.deleteRow(i);
			}catch(exp)
			{
				
			}
		}
}*/

/////
function ValidarEnterCre(e,op)
  {
    if (VeriEnter(e) )
      { 
	      switch(op)
	      {
	        case 1: 
	          AgregarItem();
	        break;
	        case 2:
	        datos_conceptos($("#CodConcepto").val())
	        break;
	        
	      
	      }
      e.returnValue = false;
    }
  }
  function AgregarItem()
  {
	var CodConcepto =$("#CodConcepto").val();
	var Colateral   =$("#Colateral").val();
	var subtotal    =$("#subtotal").val();
	var igv         =$("#igv").val();
	var redondeo    =$("#redondeo").val();
	var importe     =$("#importe").val();
   
   
    var j=parseInt($("#TbIndex tbody tr").length)

    for(var i1=1; i1<=j; i1++)
    { 
        Codigo = $("#TbIndex tbody tr#"+i1+" label.CodConcepto").text() ;
        
        if (Codigo==CodConcepto)
        {
            Msj('#Colateral','El Colateral '+ Colateral +' ya esta Agregado')
            return false;
        }
    }

    if(CodConcepto!="" && CodConcepto!=""  )
    {
       	if (subtotal=="" || parseFloat(subtotal)==0)
        { 
              Msj("#subtotal",'Digite un Precio v&aacute;lido')
              return false;
        }
    	$("#subtotal").removeClass( "ui-state-error" );
     	subtotal=number_format(subtotal,2)
     	igv=number_format(igv,2)
     	redondeo=number_format(redondeo,2)
     	importe=number_format(importe,2)
      	var i=j+1;
		var tr='<tr id="'+i+'" onclick="SeleccionaId(this);">';
		tr+='<td align="center" ><label class="Item">'+i+'</label></td>';
		tr+='<td align="left"><label class="CodConcepto">'+CodConcepto+'</label>';
		tr+='<label class="Colateral">'+Colateral+'</label></td>';
		tr+='<td align="right"><label class="SubTotal">'+subtotal+'</label></td>';
		tr+='<td align="right"><label class="IGV">'+igv+'</label></td>';
		tr+='<td align="right"><label class="Redondeo">'+redondeo+'</label></td>';
		tr+='<td align="right"><label class="Importe">'+importe+'</label></td>';
		tr+='<td align="center" ><a href="javascript:QuitaItemc('+i+')" class="Del" >';
		tr+='<span class="icono-icon-trash" title="Quitar Colateral"></span> ';
		tr+='</a></td></tr>';
		//alert(Precio)

		$("#TbIndex tbody").append(tr);
		$("#CodConcepto").val("");
		$("#Colateral").val("");
		$("#subtotal").val("0.00");
		$("#igv").val('0.00')
		$("#redondeo").val('0.00');
		$("#importe").val('0.00');
		$("#CodConcepto").focus();
		

		CalcularTotal()
      
    }
    else
    {
       if(Trim(CodConcepto)=="")
        {
          Msj('#CodConcepto','Seleccione un Colateral')
          return false;
        }
        else
        {
          Msj('#subtotal','Digite una  Precio v&aacute;lido')
          return false;
        }
       
    }
    
  }
  function CalcularTotal()
  { 

    var j=parseInt($("#TbIndex tbody tr").length);
	var importetotal  =0;
	var subtotaltotal =0;
	var igvtotal      =0;
	var redondeototal =0;
	var t 			  =0;
    for(var i=1; i<=j; i++)
    { 
      t = $("#TbIndex tbody tr#"+i+" label.SubTotal").text()
      t = str_replace(t, ',', '') ;
      subtotaltotal+= parseFloat(t);
      t = $("#TbIndex tbody tr#"+i+" label.IGV").text()
      t = str_replace(t, ',', '') ;
      igvtotal+= parseFloat(t);
      t = $("#TbIndex tbody tr#"+i+" label.Redondeo").text()
      t = str_replace(t, ',', '') ;
      redondeototal+= parseFloat(t);
      t = $("#TbIndex tbody tr#"+i+" label.Importe").text()
      t = str_replace(t, ',', '') ;
      importetotal+= parseFloat(t);



    }
    
    $('#subtotaltotal').val(number_format(subtotaltotal,2));
    $('#igvtotal').val(number_format(igvtotal,2));
    $('#redondeototal').val(number_format(redondeototal,2));
    $('#importetotal').val(number_format(importetotal,2));
   // $("#TotalCalculo").html(number_format(importetotal,2))
   var TotalCalculo = parseFloat(importetotal) + parseFloat(totalrecibos);
   $("#totrefinanciamiento").val(parseFloat(TotalCalculo).toFixed(2));

  }
  function QuitaItemc(tr)
  { 
    var id=parseInt($("#TbIndex tbody tr").length);
    
    
      $("#TbIndex tbody tr#"+tr).remove();
      var nextfila=tr+1;
      var j;
      var IdMat="";
      for(var i=nextfila; i<=id; i++)
      {
        j=i-1;//alert(j);
        $("#TbIndex tbody tr#"+i+" label.Item").text(j);
        $("#TbIndex tbody tr#"+i+" a.Del").attr("href","javascript:QuitaItemc("+j+")");
        $("#TbIndex tbody tr#"+i).attr("id",j);
        
      }
      CalcularTotal()  
      $("#tbrefinanciamiento tbody").html('')

    }
    function CalcularImporte()
	{
		


		/////
		vigv=0;
		if($("#afecto_igv").val()==1)
		{
			vigv = CalculaIgv($("#subtotal").val(),$("#IgvP").val())
			$("#igv").val(vigv)
		}
		importe = parseFloat($("#subtotal").val())+parseFloat(vigv)+parseFloat($("#redondeo").val())
		if(isNaN(importe))
			importe=0
		$("#importe").val(importe.toFixed(2))
		


	}
function buscar_conceptos()
{
	
	/*if($("#nroinscripcion").val()=="")
	{
		Msj($("#nroinscripcion"),"Seleccione el Usuario al que se le va a crear el Credito")
		return false
	}*/
	object="codconcepto";
	AbrirPopupBusqueda('../../catalogo/conceptos/?Op=5&Tipo=2',1000,500);
}
function habilitar_sininteres()
{
	if($("#chksininteres").attr('checked'))
	{
		TasaInteres=0
		$("#sininteres").val(1)
	}
	else
	{
		TasaInteres=TasaInteresO
		$("#sininteres").val(0)
	}
	$("#tbrefinanciamiento tbody").html('')
}
function cSerieNunC()
{
	var TipoDoc = $("#documentoC").val()
	
		$.ajax({
		 url:urldir+'sigco/cobranza/operaciones/cancelacion/include/cSerieNun.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&TipoDoc='+TipoDoc,
		 success:function(datos){
		 	var r=datos.split("|")
		 	$("#seriedocC").val(r[0])
		 	$("#numerodocC").val(r[1])
		 }
    	}) 	
	
}
function GuardarP(Op)
        {	//alert(Op)
            $.ajax({
                url: 'guardar.php?Op=' + Op,
                type: 'POST',
                async: true,
                data: $('#form1').serialize(), 
                dataType: 'json',
                success: function(data) 
                {	
                	//if($("#cuotainicialflag").val()==1)
                    //	imprimirrecibo(data.nropago,$("#nroinscripcion").val(),$("#femision").val())
                    if(data.nroprepagocre!=0)
						{
	                 		if(confirm("Desea Imprimir el Recibo?"))
			                    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php?codpago="+data.nroprepagocre+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#femision").val()+'&codsuc='+codsuc,800,600)
			            }
		                if(data.nroprepagocreInicial!=0)
		                {
		                	if(confirm("Desea Imprimir la Nota de Pago?"))
		                    AbrirPopupImpresion(urldir+"sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php?codpago="+data.nroprepagocreInicial+"&nroinscripcion="+$("#nroinscripcion").val()+'&fechapago='+$("#femision").val()+'&codsuc='+codsuc,800,600)
		                }
		                
                    if(data.res==1)
                    {
                    	if(parseInt($("#TbIndex tbody tr").length)>0)
                    	{
		                    if(confirm('Desea Generar Solicitud?'))
							{
								$.ajax({
				                 url:urldir+'sigco/cobranza/operaciones/cancelacion/consulta/GenerarSolicitud.php',
				                type: 'POST',
				                async: true,
				                data: 'tipo=2&pago=0&codsuc='+codsuc+'&nroinscripcion='+$("#nroinscripcion").val()+'&nrorefinanciamiento='+data.nrorefinanciamiento, 
				                dataType: 'json',
				                success: function(datos) 
				                {	
									if(datos.res==1)                         
				                    	AbrirPopupImpresion(urldir+"sigco/catastro/operaciones/solicitud/imprimir.php?nrosolicitud="+datos.nroinspeccion+'&codsuc='+codsuc+'&nuevocliente=0',800,600)
				                   
				                    

				                }
				            	});
							}
						}
						
		                
		                     	


	                    OperMensaje(data.res)
	                    $("#Mensajes").html(data.res);
	                    $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
	                    $("#Nuevo").dialog("close");
	                    $("#DivInicial").dialog("close");
     					 Buscar(Op);
	                }


                }
            })
        }