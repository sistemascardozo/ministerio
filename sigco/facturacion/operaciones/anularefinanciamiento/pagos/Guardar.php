<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	

	$Op  	= $_GET["op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp			= 1;
	$codsuc			= $_SESSION['IdSucursal'];
	$nroinscripcion	= $_POST["nroinscripcion"];
	$imptotal		= $_POST["imptotal"];
	$igv			= $_POST["igv"];
	$redondeo		= $_POST["redondeo"];
	$subtotal		= $_POST["subtotal"];
	$codconcepto	= $_POST['codconceptoC1'];//0;//$_POST["codconcepto"];
	$nropresupuesto	= $_POST["codexpediente"]?$_POST["codexpediente"]:0;
	$observacion	= $_POST["observacion"];
	$codusu			= $_SESSION['id_user'];
	$femision		= $objFunciones->CodFecha($_POST["femision"]); 
	$count			= $_POST["count"];
	$interes		= $_POST["interescredito"]?$_POST["interescredito"]:0;
	$nroprepago			= $_POST["nprepago"]?$_POST["nprepago"]:0;
	$sininteres			= $_POST["sininteres"]?$_POST["sininteres"]:0;
	$nrorefinanciamiento = $_POST['nrorefinanciamiento'];
	//if($_POST['cuotainicialflag']==1)
	//	$nropago = $objFunciones->setCorrelativosVarios(5, $codsuc, "SELECT", 0);
	$nropago =0;
	$nroprepagocre = $objFunciones->setCorrelativosVarios(6,$codsuc,"SELECT",0);
	$creoprepago=false;
	$cuotacancelada = $_POST['check_deudax'];
	$conexion->beginTransaction();
	//OBTENER EL INTERRES DE LA CUOTA
	$sql  = "select upper(clie.propietario),cab.fechaemision,cab.totalrefinanciado,
							'0.00' as igv,'0.00' as redondeo,
						cab.totalrefinanciado as imptotal,";
	$sql .= "cab.glosa,cab.creador,upper(doc.abreviado),clie.nrodocumento,";
	$sql .= "upper(cab.glosa),'',clie.nroinscripcion,
			upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
			zo.descripcion as zona,cab.nroprepago,cab.nroprepagoinicial,cab.sininteres,cab.interes,cab.cuotames ";
	$sql .= "from facturacion.cabrefinanciamiento as cab ";
	$sql .= "inner join catastro.clientes as clie on(cab.codemp=clie.codemp and ";
	$sql .= "cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion) ";
	
	$sql .= "inner join public.tipodocumento as doc on(clie.codtipodocumento=doc.codtipodocumento)
			 inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
			 inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			 inner join admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)";

	$sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrorefinanciamiento=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nrorefinanciamiento));
	$row = $consulta->fetch();
	$TotalRef=0;
	//COLATERAL
	$Sql="SELECT SUM(imptotal) 
		FROM facturacion.detcabrefinanciamiento 
		  where codemp=1 and codsuc=? and nrorefinanciamiento=?";
	$consulta2 = $conexion->prepare($Sql);
	$consulta2->execute(array($codsuc,$nrorefinanciamiento));
	$itemD = $consulta2->fetchAll();
	
	foreach($itemD as $rowD)
	{
		if($rowD[0]!='')
		{
			$TotalRef+= $rowD[0];
		}
	}
	//RECIBOS
	$Sql="SELECT f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg 
		FROM facturacion.cabfacturacion f
		  INNER JOIN facturacion.origen_refinanciamiento o ON (f.codemp = o.codemp)
		  AND (f.codsuc = o.codsuc)
		  AND (f.codciclo = o.codciclo)
		  AND (f.nrofacturacion = o.nrofacturacion)
		  AND (f.nroinscripcion = o.nroinscripcion)
		  where o.codemp=1 and o.codsuc=? and o.nrorefinanciamiento=?
		  GROUP BY f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg ";
	
	$consulta2 = $conexion->prepare($Sql);
	$consulta2->execute(array($codsuc,$nrorefinanciamiento));
	$itemD = $consulta2->fetchAll();
	
	foreach($itemD as $rowD)
	{
		$i++;
		 $Sql="select sum(importe - (importerebajado))
				 from facturacion.detfacturacion
			where codemp=1 and codsuc=? and nroinscripcion=? AND nrofacturacion=?";
			
		$result = $conexion->prepare($Sql);
		$result->execute(array($codsuc,$row['nroinscripcion'],$rowD['nrofacturacion']));
		$row2 = $result->fetch();
		$TotalRef+= $row2[0];
	}

	$sqldetalle  = "select nrocuota,totalcuotas,'' as fechavencimiento,SUM(importe) AS imptotal,
							'0.00' as igv,
							'0.00' as redondeo,SUM(importe),";
	$sqldetalle .= "0 as tipocuota,nrofacturacion,mes,anio,'0.00' as interes ";
	$sqldetalle .= "from facturacion.detrefinanciamiento ";
	$sqldetalle .= "where codemp=1 and codsuc=? and nrorefinanciamiento=? 
					group by nrocuota,totalcuotas,anio,mes,estadocuota,nrofacturacion
					order by nrocuota";
	
	$consultaD = $conexion->prepare($sqldetalle);
	$consultaD->execute(array($codsuc,$nrorefinanciamiento));
	$itemD = $consultaD->fetchAll();
	$Saldo =  $TotalRef;
	$cuotames=$row['cuotames'];
	$vigv=0;
	$ai = array();
	foreach($itemD as $rowD)
	{
		
		if($row['nroprepagoinicial']!=0 && $rowD["nrocuota"]==1) //CON INICIAL
		{
			$Amortizacion= $rowD["imptotal"];
			$interes = 0;
			$importe = $rowD["imptotal"];
		}
		else
		{
			
			if($row['sininteres']==0)
			{
				$interes =  round($Saldo*$row['interes']/100,1);
			}
			else
			$interes = 0;

		

			$Amortizacion= $cuotames-$vigv-$interes;
			

			if($rowD["nrocuota"]==$nrocuota)
			{
				$importe		 = $Saldo + $vigv;
			}
			else
			{
				$importe		 = $Amortizacion + $vigv;
			}
			$importe = $Amortizacion + $interes;

		}
		$ai[$rowD["nrocuota"]]=$interes;
		 $Saldo  -=  $Amortizacion;
		 $tinteres+=$rowD["interes"];
		 $tigv+=$rowD["igv"];
		 $totalcuotas+=$rowD["imptotal"];
	}
	//OBTENER EL INTERRES DE LA CUOTA

		//INSERTAR UN PREPAGO POR EL MONTO TOTAL
		$sql = "SELECT c.codcliente,c.propietario,c.codtipodocumento,c.nrodocumento,c.codciclo,tc.descripcioncorta,
            cl.descripcion as calle,c.nrocalle,c.codciclo,td.abreviado,c.codsector,
            c.codcalle
            FROM catastro.clientes as c
            INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
            inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
            where c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

        $result = $conexion->prepare($sql);
        $result->execute(array($codsuc, $nroinscripcion));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error SELECT Catastro";
            $data['res']    =2;
            die(json_encode($data)) ;
        }
        $item = $result->fetch();
        $propietario = strtoupper($item["propietario"]);
        $direccion = strtoupper($item["descripcioncorta"] . " " . $item["calle"] . " " . $item["nrocalle"]);
        $docidentidad = strtoupper($item["nrodocumento"]);

		$creoprepago=true;
		$condpago = $_POST["condpago"];
	    $documento = 16;//nota de pago
	    $formapago = 1;
	    $fechareg = date('Y-m-d');
	    $default = array('1' =>9,'2'=>4,'3'=>2);//CAR DEFECTO
        $car = $_POST["carx"];
        if($car=='') $car = $default[$codsuc];
	    $nrocaja = $_SESSION["nocaja"];
	    $idusuario = $_SESSION['id_user'];
	     $hora = $objFunciones->HoraServidor();
	     $eventualtext=0;
	    $nrofacturacion=0;
	    $npresupuesto=0;
	
	     $glosa = 'PAGO ADELANTADO DE CUOTA '.$cuotacancelada;//$_POST["glosa"];
		$sqlC = "insert into cobranza.cabprepagos(codemp,codsuc,nroinscripcion,nroprepago,car,codformapago,creador,fechareg,subtotal,hora,
		        codciclo,condpago,igv,redondeo,imptotal,eventual,propietario,direccion,documento,glosa,nroinspeccion,estado,
		        nrocredito,origen) 
		        values(:codemp,:codsuc,:nroinscripcion,:nroprepago,:car,:codformapago,:creador,:fechareg,
		        :subtotal,:hora,:codciclo,:condpago,:igv,:redondeo,:imptotal,:eventual,:propietario,:direccion,:documento,
		        :glosa,:nroinspeccion,:estado,:nrocredito,:origen)";
		$resultC = $conexion->prepare($sqlC);
		$resultC->execute(array(":codemp"=>$codemp,
		                        ":codsuc"=>$codsuc,
		                        ":nroprepago"=>$nroprepagocre,
		                        ":nroinscripcion"=>$nroinscripcion,
		                        ":car"=>$car,
		                        ":codformapago"=>$formapago,
		                        ":creador"=>$idusuario,
		                        ":fechareg"=>$fechareg,
		                        ":subtotal"=>floatval(str_replace(",", "", $_POST['imptotal'])),
		                        ":hora"=>$hora,
		                        ":codciclo"=>1,
		                        ":condpago"=>$condpago,
		                        ":igv"=>0,
		                        ":redondeo"=>0,//str_replace(",", "", $_POST['redondeo']),
		                        ":imptotal"=>str_replace(",", "", $_POST['imptotal']),
		                        ":eventual"=>$eventualtext,
		                        ":propietario"=>$propietario,
		                        ":direccion"=>$direccion,
		                        ":documento"=>$docidentidad,
		                        ":glosa"=>$glosa,
		                        ":nroinspeccion"=>0,
		                        ":estado"=>1,
		                        ":nrocredito"=>$nrorefinanciamiento,
		                        ":origen"=>4));
		if($resultC->errorCode()!='00000')
		{
		    $conexion->rollBack();
		    $mensaje = "Error update cabpagos";
		    $data['res']    =2;
		    die(json_encode($data)) ;
		}
		
		$sqlD = "insert into cobranza.detprepagos(codemp,codsuc,nroinscripcion,nroprepago,codconcepto,importe,detalle,tipopago,
		    nrofacturacion,coddocumento,nrodocumento,serie,codtipodeuda,anio,mes,categoria,codcategoria,nropresupuesto) 
		    values(:codemp,:codsuc,:nroinscripcion,:nroprepago,:codconcepto,:importe,:detalle,:tipopago,
		    :nrofacturacion,:coddocumento,:nrodocumento,:serie,:codtipodeuda,:anio,:mes,:categoria,:codcategoria,:nropresupuesto)";
		//$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL
		$DocSeries= $objFunciones->GetSeries($codsuc,$documento);
		$seriedoc = $DocSeries["serie"];
		$numerodoc = $DocSeries["correlativo"];
		$fechas = $objFunciones->DecFechaLiteral();
	    $anio = $fechas["anio"];
	    $mes = $fechas["mes_num"];
	    $facturacion = 0;
		$resultD = $conexion->prepare($sqlD);
		$resultD->execute(array(":codemp"=>$codemp,
		                        ":codsuc"=>$codsuc,
		                        ":nroinscripcion"=>$nroinscripcion,
		                        ":nroprepago"=>$nroprepagocre,
		                        ":codconcepto"=>$_POST['codconcepto'],
		                        ":importe"=>str_replace(",", "", $_POST['imptotal'])/*-$ai[$cuotacancelada]*/,
		                        ":detalle"=>$_POST['Colateral'],
		                        ":tipopago"=>0,
		                        ":nrofacturacion"=>$nrofacturacion,
		                        ":coddocumento"=>$documento,//4
		                        ":nrodocumento"=>$numerodoc,//0
		                        ":serie"=>$seriedoc,//001
		                        ":codtipodeuda"=>5,//COBRANZA DIRECTAMENTE EN CAJA
		                        ":anio"=>$anio,
		                        ":mes"=>$mes,
		                        ":categoria"=>3,
		                        ":codcategoria"=>4,
		                        ":nropresupuesto" => $npresupuesto));



		if($resultD->errorCode()!='00000')
		{
		    $conexion->rollBack();
		    $mensaje = "Error update cabpagos";
		    $data['res']    =2;
		    die(json_encode($data)) ;
		}
		//DETALLE DE INTERES
		/*$sqlD = "insert into cobranza.detprepagos(codemp,codsuc,nroinscripcion,nroprepago,codconcepto,importe,detalle,tipopago,
		    nrofacturacion,coddocumento,nrodocumento,serie,codtipodeuda,anio,mes,categoria,codcategoria,nropresupuesto) 
		    values(:codemp,:codsuc,:nroinscripcion,:nroprepago,:codconcepto,:importe,:detalle,:tipopago,
		    :nrofacturacion,:coddocumento,:nrodocumento,:serie,:codtipodeuda,:anio,:mes,:categoria,:codcategoria,:nropresupuesto)";
		//$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL
		$codconcepto=9;//CONCEPTO DE INTERES
		$resultD = $conexion->prepare($sqlD);
		$resultD->execute(array(":codemp"=>$codemp,
		                        ":codsuc"=>$codsuc,
		                        ":nroinscripcion"=>$nroinscripcion,
		                        ":nroprepago"=>$nroprepagocre,
		                        ":codconcepto"=>$codconcepto,
		                        ":importe"=>$ai[$cuotacancelada],
		                        ":detalle"=>'INTERESES',
		                        ":tipopago"=>0,
		                        ":nrofacturacion"=>$nrofacturacion,
		                        ":coddocumento"=>$documento,//4
		                        ":nrodocumento"=>$numerodoc,//0
		                        ":serie"=>$seriedoc,//001
		                        ":codtipodeuda"=>5,//COBRANZA DIRECTAMENTE EN CAJA
		                        ":anio"=>$anio,
		                        ":mes"=>$mes,
		                        ":categoria"=>3,
		                        ":codcategoria"=>4,
		                        ":nropresupuesto" => $npresupuesto));



		if($resultD->errorCode()!='00000')
		{
		    $conexion->rollBack();
		    $mensaje = "Error update cabpagos";
		    $data['res']    =2;
		    die(json_encode($data)) ;
		}*/
		//DETALLE DE INTERES
		//ACTUALIZAR CORRELATIVO
		 $sql = "UPDATE  reglasnegocio.correlativos  
		    SET  correlativo = correlativo+1
		    WHERE codemp=1 AND  codsuc=" . $codsuc . " AND nrocorrelativo=".$documento;
		    $result = $conexion->query($sql);
		    if (!$result) 
		    {
		        $conexion->rollBack();
		        $mensaje = "619 Error UPDATE correlativos";
		        $data['res']    =2;
		        $data['mensaje']    =$mensaje;
		        die(json_encode($data));
		    }
		
	

	//CUOTAS CANCELADAS
	    $sqldetalle  = "select nrocuota,anio,mes,sum(importe) as imptotal, estadocuota ";
		$sqldetalle .= "from facturacion.detrefinanciamiento ";
		$sqldetalle .= "where codemp=1 and codsuc=? and nrorefinanciamiento=?
						group by nrocuota,anio,mes,estadocuota
						order by nrocuota";
		$consultaD = $conexion->prepare($sqldetalle);
		$consultaD->execute(array($codsuc,$nrorefinanciamiento));
		$itemD = $consultaD->fetchAll();
		foreach($itemD as $rowD)
		{
			if($rowD["estadocuota"]==0)
			{

				if($_POST['paso'.$rowD['nrocuota']]==1)	
				{
					//OBTENER PREPAGO SI YA SE GENERO Y ANULARLO
					$Sql="SELECT * FROM facturacion.cabrefinanciamientosadelantos 
							WHERE  codemp=:codemp AND codsuc=:codsuc AND nrorefinanciamiento=:nrorefinanciamiento and nrocuota=:nrocuota";
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrorefinanciamiento"=>$nrorefinanciamiento,
					                        ":nrocuota"=>$rowD['nrocuota']
					                        
					                        ));	
					$rowX = $resultD->fetch();	
					if($rowX[0]!='')
					{
						$Sql="update cobranza.cabprepagos set estado=0 
						     WHERE codemp=1 AND  codsuc=".$codsuc." 
						     and nroprepago=".$rowX['nroprepago']." AND nroinscripcion=".$nroinscripcion;
						$conexion->query($Sql);
					}
					//OBTENER PREPAGO SI YA SE GENERO Y ANULARLO
					
					$Sql="DELETE FROM facturacion.cabrefinanciamientosadelantos 
							WHERE  codemp=:codemp AND codsuc=:codsuc AND nrorefinanciamiento=:nrorefinanciamiento and nrocuota=:nrocuota";
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrorefinanciamiento"=>$nrorefinanciamiento,
					                        ":nrocuota"=>$rowD['nrocuota']
					                        
					                        ));	


					
					$Sql="INSERT INTO 
						  facturacion.cabrefinanciamientosadelantos
						(
						  codemp,
						  codsuc,
						  nrorefinanciamiento,
						  nrocuota,
						  imptotal,
						  fechareg,
						  nroprepago
						  
						) 
						VALUES (
						  :codemp,
						  :codsuc,
						  :nrorefinanciamiento,
						  :nrocuota,
						  :imptotal,
						  :fechareg,
						  :nroprepago
						  
						); ";
				
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrorefinanciamiento"=>$nrorefinanciamiento,
					                        ":nrocuota"=>$rowD['nrocuota'],
					                        ":imptotal"=>$rowD['imptotal'],
					                        ":fechareg"=>$fechareg,
					                        ":nroprepago"=>$nroprepagocre
					                        ));



					if($resultD->errorCode()!='00000')
					{
					    $conexion->rollBack();
					    $mensaje = "Error update cabpagos";
					    $data['res']    =2;
					    die(json_encode($data)) ;
					}
					//´poner a facturado la cuota
					/*$sqldetalle  = "UPDATE facturacion.detrefinanciamiento SET estadocuota=1";
						$sqldetalle .= "where codemp=1 and codsuc=? and nrorefinanciamiento=? AND nrocuota=?";
						$consultaD = $conexion->prepare($sqldetalle);
						$consultaD->execute(array($codsuc,$nrorefinanciamiento,$rowD['nrocuota']));
						$itemD = $consultaD->fetchAll();*/
					//´poner a facturado la cuota

				}
			}
			
		}
	//CUOTAS CANCELADAS
	
	//die();
	if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       $res = 2;
    } else {
        $conexion->commit();
        		$objFunciones->setCorrelativosVarios(6, $codsuc, "UPDATE", $nroprepagocre);
        	
        
        
        	
        $mensaje = "El Registro se ha Grabado Correctamente";
        $res = 1;
    }
	
	$data['res']    =  $res;
	$data['nropago']              = 0;// $nropago;
	$data['nrocredito']           = $nrocredito;
	$data['nroprepagocre']        = $nroprepagocre;
	echo json_encode($data);

?>
