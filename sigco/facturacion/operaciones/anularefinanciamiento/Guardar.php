<?php 


	//ini_set("display_errors",1);
	//error_reporting(E_ALL);

	session_name("pnsu");
	if(!session_start()){session_start();} 
	
	include("../../../../objetos/clsFunciones.php");
	include("../../../../objetos/clsSeguridad.php");
	

	$Op                  = $_GET["op"];
	
	$objFunciones        = new clsFunciones();
	$objSeguridad        = new clsSeguridad();
	
	$codemp              = 1;
	$codsuc              = $_SESSION['IdSucursal'];
	$nroinscripcion      = $_POST["nroinscripcion"];
	
	$observacion         = $_POST["observacion"];
	
	$codusu              = $_SESSION['id_user'];
	$fecha               = $objFunciones->CodFecha($objFunciones->FechaServer());
	$hora                = $objFunciones->HoraServidor();
	
	$femision            = $objFunciones->CodFecha($_POST["femision"]);  

	$nroprepago          = $_POST["nprepago"]?$_POST["nprepago"]:0;
	$nrorefinanciamiento = $_POST['nrorefinanciamiento'];
	
	$creoprepago         = false;
	$conexion->beginTransaction();

	// Cambiar los estados correspondientes anulacion
	// estareg y codestadorefinanciamiento con valores 0 y 5 respectivamente

	$sql = "UPDATE  facturacion.cabrefinanciamiento  
		    SET  codestadorefinanciamiento = 5, estareg = 2
		    WHERE codemp = 1 AND  codsuc = ".$codsuc." AND nrorefinanciamiento=".$nrorefinanciamiento;
    $result = $conexion->query($sql);
    if (!$result) 
    {
		$conexion->rollBack();
		$mensaje         = "Error en el estado de cabrefinanciamiento";
		$data['res']     = 2;
		$data['mensaje'] = $mensaje;
		die(json_encode($data));
    }

    // Pasamos el estado de la cuota del detalle de financiamiento a anulado que es 2

    $sql = "UPDATE  facturacion.detrefinanciamiento  
		    SET  estadocuota = 2
		    WHERE codemp = 1 AND  codsuc = ".$codsuc." AND nrorefinanciamiento=".$nrorefinanciamiento;
    $result = $conexion->query($sql);
    if (!$result) 
    {
		$conexion->rollBack();
		$mensaje         = "Error en el estado de detrefinanciamiento";
		$data['res']     = 2;
		$data['mensaje'] = $mensaje;
		die(json_encode($data));
    }

    //Obteniendo el nrofacturacion actual

	$Sql1 = " SELECT (MAX(nrofacturacion) - 1) as actual
				FROM facturacion.periodofacturacion per
		 		WHERE per.codsuc = ?";
	
	$consulta3 = $conexion->prepare($Sql1);
	$consulta3->execute(array($codsuc));
	$itemD1     = $consulta3->fetch();	
	
	//Origen de Refinanciamiento

	$Sql = " SELECT oref.* 
				FROM facturacion.origen_refinanciamiento oref
		 		WHERE oref.codemp=1 AND oref.codsuc = ? 
		 		AND oref.nrorefinanciamiento = ? AND oref.nroinscripcion = ? ORDER BY nrofacturacion ASC";
	
	$consulta2 = $conexion->prepare($Sql);
	$consulta2->execute(array($codsuc,$nrorefinanciamiento,$nroinscripcion));
	$itemD     = $consulta2->fetchAll();


	foreach($itemD as $rowD)
	{

		if($itemD1['actual'] == $rowD['nrofacturacion']): $estad = 0;	else: $estad = 1; endif;

			$sql = "UPDATE  facturacion.detfacturacion  
		    SET  categoria = ".$estad."
		    WHERE codemp = 1 AND  codsuc = {$codsuc} 
		    AND nrofacturacion = ". $rowD['nrofacturacion']." 
		    AND nroinscripcion = ".$nroinscripcion;
			$result = $conexion->query($sql);

			if (!$result) :
		    
				$conexion->rollBack();
				$mensaje         = "Error en el cambio de categoria de detfacturacion";
				$data['res']     = 2;
				$data['mensaje'] = $mensaje;
				die(json_encode($data));
		    
		    endif;
	}

	// Modificacion de la cabrefinanciamiento con respecto a las fechas 
	$sql = "UPDATE  facturacion.cabrefinanciamiento  
		    SET  codusu_modificador = ".$codusu.", fecha_modificacion = '".$fecha 
		   ."', hora_modificacion = '".$hora."'
		    WHERE codemp = 1 AND  codsuc = ".$codsuc." AND nrorefinanciamiento=".$nrorefinanciamiento;
			$result = $conexion->query($sql);

			if (!$result) :
		    
				$conexion->rollBack();
				$mensaje         = "Error en el cambio de fechas de cabfacturacion";
				$data['res']     = 2;
				$data['mensaje'] = $mensaje;
				die(json_encode($data));
		    
    		endif;

    // Insercion para el seguimiento del usuario que realiza la modificacion o actualizacion

	$IdControl = $nrorefinanciamiento;
	
	$SqlControl = "INSERT INTO admin.control (codsuc, tabla, id, operacion) ";
	$SqlControl .= "VALUES(".$_SESSION['IdSucursal'].", 'facturacion{}cabrefinanciamiento', '".$IdControl."', 1)";
	$resultControl = $conexion->prepare($SqlControl);
	$resultControl->execute(array());

	$objSeguridad->seguimiento(1,$codsuc,5,$codusu,"SE HA ANULADO EL REFINANCIAMIENTO CON NUMERO ".$nrorefinanciamiento,148);


	if ($resultControl->errorCode() != '00000') :
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       	$res = 2;
    else :
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        $res = 1;
    
    endif;
	
	$data['res']    =  $res;
	$data['nropago']              = 0;// $nropago;
	$data['nrocredito']           = $nrocredito;
	$data['nroprepagocre']        = $nroprepagocre;
	echo json_encode($data);

?>
