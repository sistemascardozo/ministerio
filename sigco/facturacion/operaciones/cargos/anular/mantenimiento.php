<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../objetos/clsDrop.php");

$Op = $_GET["Op"];
$Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=" . $Op;

$objMantenimiento = new clsDrop();

$paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);

$Select = "SELECT cab.nroinscripcion,clie.propietario,tipcal.descripcioncorta || cal.descripcion || ' #' || clie.nrocalle,";
$Select .= "conc.descripcion,cab.fechareg,cab.nropresupuesto,cab.observacion,cab.nropago ";
$Select .= "FROM facturacion.cabvarios as cab ";
$Select .= "INNER JOIN catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and ";
$Select .= "cab.nroinscripcion=clie.nroinscripcion) ";
$Select .= "INNER JOIN public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )";
$Select .= "INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) ";
$Select .= "INNER JOIN facturacion.conceptos as conc on(cab.codemp=conc.codemp and cab.codsuc=conc.codsuc and cab.codconcepto=conc.codconcepto) ";
$Select .= "WHERE cab.codemp=1 and cab.codsuc=? and cab.nrocredito=?";

$consulta = $conexion->prepare($Select);
$consulta->execute(array($codsuc, $Id));
$row = $consulta->fetch();
//echo $row['nropago'];
$sqlD = "SELECT count(*) FROM facturacion.detvarios WHERE tipocuota=0 and estadocuota=1 and nrocredito=? and codsuc=?";
$consultaD = $conexion->prepare($sqlD);
$consultaD->execute(array($Id, $codsuc));
$items = $consultaD->fetch();
$num_detalle = $items[0];
//////
$nropec = 0;
$msj = '';
if ($row['nropago'] != 0) {
    $msj = 'Tambien se anulara el pago de la cuota inicial';
    $sql = "SELECT nropec FROM cobranza.cabpagos WHERE nropago=? and codemp=1 and codsuc=?";
    $consulta2 = $conexion->prepare($sql);
    $consulta2->execute(array($row['nropago'], $codsuc));
    $row2 = $consulta->fetch();
    $nropec = $row2[0];
}
//$facturacion 	= $objMantenimiento->datosfacturacion($codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>

<script>
    $(document).ready(function () {
        /*
<?php
if ($num_detalle > 0) {
    ?>
             $("#button-maceptar").unbind("click");
             $("#button-maceptar").click(function () {
             NoAnular();
             });
<?php }
?>
<?php
if ($nropec > 0 || $nropec != '') {
    ?>
             $("#button-maceptar").unbind("click");
             $("#button-maceptar").click(function () {
             NoAnular();
             });
<?php }
?>*/

    });
    function NoAnular()
    {
        alert('No se puede Anular el Cargo')
        return false
    }
    function ValidarForm(Op)
    {
        if (confirm('Desea Anular el Cargo de Facturacion?. <?=$msj ?>') == false)
        {
            return false;
        }
        GuardarP(Op)
    }
    function Cancelar()
    {
        location.href = 'index.php?Op=1';
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
        <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="6" align="center" >&nbsp;</td>
                </tr>
                <tr>
                    <td >Nro. Cargo </td>
                    <td align="center">:</td>
                    <td >
                        <input type="text" name="nrocredito" id="nrocredito" class="inputtext" size="12" maxlength="12" value="<?=$Id ?>" readonly="readonly" />
                        <input name="nropago" id="nropago" value="<?=$row['nropago'] ?>">
                    </td>
                    <td align="left" colspan="3">
                        <?php
                        if ($num_detalle > 0) {
                            ?>
                            <div id="div_estadoservicio" style="color:#FF0000; font-size:15px; font-weight:bold; padding:1px; display:inline">
                                El Cargo Presenta Cuotas Canceladas
                            </div>
                        <?php }
                        ?>

                    </td>

                </tr>
                <tr>
                    <td width="95" >Codigo</td>
                    <td width="19" align="center">:</td>
                    <td width="372" >
                        <input type="text" name="nroinscripcion" id="nroinscripcion" class="inputtext" size="12" maxlength="12" readonly="readonly" value="<?=$row[0] ?>" />
                    </td>
                    <td width="114" align="right" >Fecha Emision</td>
                    <td width="18" align="center">:</td>
                    <td width="278" >
                            <input type="text" name="femision" id="femision" size="15" maxlength="15" class="inputtext" readonly="readonly" value="<?=$objMantenimiento->DecFecha($row["fechareg"]) ?>" />
                    </td>
                </tr>
                <tr>
                    <td >Nombres</td>
                    <td align="center">:</td>
                    <td ><label>
                            <input type="text" name="nombres" id="nombres" class="inputtext" size="65" maxlength="65" readonly="readonly" value="<?=$row[1] ?>" />
                        </label></td>
                    <td align="right">Nro. de Presup </td>
                    <td align="center">:</td>
                    <td ><input type="text" name="npresupuesto" id="npresupuesto" size="15" maxlength="15" class="inputtext" readonly="readonly" value="<?=$row[5] ?>" />
                        <label></label></td>
                </tr>
                <tr>
                    <td >Direccion</td>
                    <td align="center">:</td>
                    <td ><input type="text" name="direccion" id="direccion" class="inputtext" size="65" maxlength="65" readonly="readonly" value="<?=$row[2] ?>" /></td>
                    <td align="left" colspan="3">
                        <?php
                        if ($nropec > 0) {
                            ?>
                            <div id="div_estadoservicio" style="color:#FF0000; font-size:15px; font-weight:bold; padding:1px; display:inline">
                                Pago de Cuota Inicial, ya Cuenta con Nro PEC
                            </div>
                        <?php }
                        ?>

                    </td>
                </tr>
                <tr>
                    <td >Colateral</td>
                    <td align="center">:</td>
                    <td ><input type="text" name="colateral" id="colateral" class="inputtext" size="65" maxlength="65" readonly="readonly" value="<?=$row[3] ?>" /></td>
                    <td align="right">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td >&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" class="TitDetalle">
                        <div style="height:170px; overflow:scroll">
                            <!--
                            <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetalle" class="myTable01" rules="all" >
                                <thead class="ui-widget-header" >
                                    <tr align="center">
                                        <th width="6%" >Item</th>
                                        <th width="8%" >Nro. Cuota </th>
                                        <th width="14%" >Fecha de Vencimiento</th>
                                        <th width="9%" >Sub. Total </th>
                                        <th width="7%" >Igv</th>
                                        <th width="9%" >Redondeo</th>
                                        <th width="9%" >Importe</th>
                                        <th width="13%" >Categoria</th>
                                        <th width="13%" >Nro. Facturacion </th>
                                        <th width="12%" >Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                            $count = 0;
                            $tip = "";
                            $est = "";

                            $sqldetalle = "SELECT nrocuota,totalcuotas,fechavencimiento,subtotal,igv,redondeo,imptotal,tipocuota,";
                            $sqldetalle .= "nrofacturacion,estadocuota FROM facturacion.detvarios ";
                            $sqldetalle .= "WHERE codemp=1 and codsuc=? and nrocredito=?";
                            $sqldetalle .= " ORDER BY nrocuota asc";

                            $consultaD = $conexion->prepare($sqldetalle);
                            $consultaD->execute(array($codsuc, $Id));
                            $items = $consultaD->fetchAll();

                            foreach ($items as $rowdetalle) {
                                $count+=1;

                                if ($rowdetalle[7] == 0) {
                                    $tip = "CUOTA NORMAL";
                                } else {
                                    $tip = "CUOTA INICIAL";
                                }
                                switch ($rowdetalle['estadocuota']) {
                                    case 0:$est = "PENDIENTE";
                                        break;
                                    case 1:$est = "FACTURADO";
                                        break;
                                    case 2:$est = "ANULADO";
                                        break;
                                    case 3:$est = "CANCELADO";
                                        break;
                                    case 4:$est = "QUEBRADO";
                                        break;
                                }
                                ?>
                                            <tr style="background-color:#FFFFFF; font-size:11px" >
                                                <th align="center" ><?=$count ?></th>
                                                <th align="center" ><?=$rowdetalle[0] . "/" . $rowdetalle[1] ?></th>
                                                <th align="center" ><?=$objMantenimiento->DecFecha($rowdetalle[2]) ?></th>
                                                <th align="right" ><?=number_format($rowdetalle[3], 2) ?></th>
                                                <th align="right" ><?=number_format($rowdetalle[4], 2) ?></th>
                                                <th align="right" ><?=number_format($rowdetalle[5], 2) ?></th>
                                                <th align="right" ><?=number_format($rowdetalle[6], 2) ?></th>
                                                <th align="center" ><?=$tip ?></th>
                                                <th align="center" ><?=$rowdetalle[8] ?></th>
                                                <th align="center" ><?=$est ?></th>
                                            </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            -->

                            <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetalle" class="myTable01" rules="all" >
                                <thead class="ui-widget-header" >
                                    <tr align="center">
                                        <th width="8%" >Nro. Cuota </th>
                                        <th width="10%" >Fecha de Vencimiento</th>
                                        <th width="8%" >Saldo</th>
                                        <th width="8%" >Amortizaci&oacute;n</th>
                                        <th width="8%" >Interes</th>
                                        <th width="5%" >Igv</th>
                                        <th width="9%" >Importe</th>
                                        <th width="10%" >Estado</th>
                                        <!--<th width="1%" align="center" scope="col">&nbsp;</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqldetalle = "SELECT nrocuota,totalcuotas,fechavencimiento,subtotal,igv,redondeo,imptotal,";
                                    $sqldetalle .= "tipocuota,nrofacturacion,mes,anio,interes,estadocuota ";
                                    $sqldetalle .= "FROM facturacion.detvarios ";
                                    $sqldetalle .= "WHERE codemp=1 and codsuc=? and nrocredito=? ORDER BY nrocuota";
                                    $consultaD = $conexion->prepare($sqldetalle);
                                    $consultaD->execute(array($codsuc, $Id));
                                    $itemD = $consultaD->fetchAll();
                                    $Saldo = $row[5];

                                    $tinteres = 0;
                                    $tigv = 0;
                                    $totalcuotas = 0;
                                    $TotalDeuda = 0;
                                    $TotalDeudaInteres = 0;
                                    $TotalDeudaIgv = 0;
                                    $total = 0;
                                    foreach ($itemD as $rowD) {
                                        if ($rowD["tipocuota"] == 1) {
                                            $tipo = "CUOTA INICIAL";
                                        } else {
                                            $tipo = "CUOTA NORMAL";
                                        }
                                        switch ($rowD["estadocuota"]) {
                                            case '0': $Estado = '<p style="color:red;font-weight: bold;">PENDIENTE</p>';
                                                break;
                                            case '1': $Estado = '<p style="color:green;font-weight: bold;">FACTURADO</p>';
                                                break;
                                            case '2': $Estado = '<p style="color:green;font-weight: bold;">ANULADO</p>';
                                                break;
                                            case '3': $Estado = '<p style="color:green;font-weight: bold;">CANCELADO</p>';
                                                break;
                                            case '4': $Estado = "QUEBRADO";
                                                break;
                                        }
                                        ?>
                                        <tr>
                                            <td><?=$rowD["nrocuota"] ?></td>
                                            <td><?=$meses[$rowD["mes"]] . "-" . $rowD["anio"] ?>
                                                <input type='hidden' value='<?=$rowD["imptotal"] ?>' class='input-deuda' />
                                                <input type='hidden' value='<?=$rowD["interes"] ?>' class='input-deudainteres' />
                                                <input type='hidden' value='<?=$rowD["igv"] ?>' class='input-deudaigv' />
                                            </td>
                                            <td align="right"><?=number_format($Saldo, 2) ?></td>
                                            <td align="right"><?=number_format($rowD["subtotal"], 2) ?></td>
                                            <td align="right"><?=number_format($rowD["interes"], 2) ?></td>
                                            <td align="right"><?=number_format($rowD["igv"], 2) ?></td>
                                            <td align="right"><?=number_format($rowD["imptotal"], 2) ?></td>
                                            <td align="center"><?=$Estado ?></td>
                                            <!--<th width="5" align="center" scope="col">
                                            <?php
                                            switch ($rowD["estadocuota"]) {
                                                case '0':
                                                    $TotalDeuda+=$rowD["imptotal"];
                                                    $TotalDeudaInteres+=$rowD["interes"];
                                                    $TotalDeudaIgv+=$rowD["igv"];
                                                    $total++;
                                                    ?>
                                                                <input type='hidden' value='1' class='input-paso'  name="paso<?=$rowD["nrocuota"] ?>" />
                                                                <input type='checkbox' name='check_deuda<?=$rowD["nrocuota"] ?>' checked='checked' class='input-chk' onclick='check_deuda(this)'>
                                                    <?php
                                                    break;
                                                default:
                                                    ?>
                                                                <input type='hidden' value='0' class='input-paso'  name="paso<?=$rowD["nrocuota"] ?>" />
                                                <?php
                                            }
                                            ?>	

                                            </th>-->
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot class="ui-widget-header" >
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                        <td> Total</td>
                                        <td align="right"><label id="LblPagoAdelantadoInteres"><?=number_format($TotalDeudaInteres, 2) ?></label>
                                            <input id="PagoAdelantadoInteres" name="interespagado" value="<?=$TotalDeudaInteres ?>" type="hidden">

                                        </td>
                                        <td align="right"><label id="LblPagoAdelantadoIgv"><?=number_format($TotalDeudaIgv, 2) ?></label>
                                            <input id="PagoAdelantadoIgv" name="igv" value="<?=$TotalDeudaIgv ?>" type="hidden">

                                        </td>
                                        <td align="right"><label id="LblPagoAdelantado"><?=number_format($TotalDeuda, 2) ?></label>
                                            <input id="PagoAdelantado" name="imptotal" value="<?=$TotalDeuda ?>" type="hidden">
                                            <input name="TotalCuotasP" value="<?=$total ?>" type="hidden">
                                        </td>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>


                    </td>
                </tr>
                <tr>
                    <td valign="top" >Observacion</td>
                    <td align="center" valign="top">:</td>
                    <td colspan="4">
                        <textarea name="observacion" id="observacion" class="inputtext" style="font-size:11px" cols="110" rows="5" readonly="readonly"><?=$row[6] ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" class="TitDetalle"><input type="hidden" name="count" id="count" value="0" />&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>