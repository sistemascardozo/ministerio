<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$nroinscripcion = $_POST["nroinscripcion"];
	$codsuc			    = $_POST["codsuc"];
  $intipodeuda    = $_POST["intipodeuda"];
  $cuotaini       = $_POST["cuotaini"];
  if($intipodeuda!=''){
    $intipodeuda = " and d.codtipodeuda in($intipodeuda)";
  }
	$facturaciones = " (".substr($_POST["Facturaciones"],0,strlen($_POST["Facturaciones"])-1).") ";
 $facturaciones     = " and d.nrofacturacion in ".$facturaciones;

	$objFunciones = new clsFunciones();
	
	$sqlS  		= "select d.codconcepto,c.descripcion,
				  sum(d.importe - (d.importerebajado + d.imppagado)) as imptotal
				  from facturacion.detfacturacion as d
				  inner join facturacion.conceptos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto)
				  where d.nroinscripcion=? and d.codsuc=? /*and d.categoria=1*/ and d.estadofacturacion<>2".$intipodeuda.$facturaciones."
				  group by d.codconcepto,c.descripcion
				  order by d.codconcepto";
	
	$consultaS = $conexion->prepare($sqlS);
	$consultaS->execute(array($nroinscripcion,$codsuc));
	$itemS = $consultaS->fetchAll();
	
	
?>
<script>
	function cuotamensualtotal(obj)
	{            
		ncuota = Trim(obj.value);
		
		if(ncuota=="")
		{
			ncuota = 1;
		}
		else if(isNaN(ncuota) || ncuota <= 0)
		{
			ncuota = 1;
			obj.value = ncuota; 
		}
            
		var totafinanciar = $("#totalafinanciar").val();
           
		var interestotal = parseFloat(totafinanciar) * (Math.pow((1 + TasaInteres), ncuota) -1);
          
		var totsaldo 	 = $("#totalsaldo").val() //total a refinanciar
            //var cuotamensual = Round(parseFloat(totafinanciar) / parseFloat(ncuota),2)
		if(TasaInteres != 0)
		{
			var cuotamensual = totafinanciar * (Math.pow((1 + TasaInteres), ncuota) * (TasaInteres)) / (Math.pow((1 + TasaInteres), ncuota) -1);
		}
		else
		{
			var cuotamensual = parseFloat(totafinanciar)/parseFloat(ncuota);
		}
            ///
            /*
              if(TasaInteres!=0)
                var cuotames    = montoref*(Math.pow((1+TasaInteres),nrocuota)*(TasaInteres))/(Math.pow((1+TasaInteres),nrocuota)-1)
              else
                var cuotames = parseFloat(montoref)/parseFloat(nrocuota)
                        */
            ///

            var totcuotas   = Round(parseFloat(ncuota) * parseFloat(cuotamensual), 2)//monto real a pagar
            var ultmes      = Round(parseFloat(totafinanciar) - parseFloat(totcuotas), 2)
            
            var x           = 0
            var sumultmes   = 0

            var cuotaini    = parseFloat($("#cuotaini").val());

            $("#cuotames").val(parseFloat(cuotamensual).toFixed(1))
            $("#totcuotas").val(totcuotas)
            var SumaTotalI=0
            var SumaTotalF=0

            for(i=1;i<=$("#contador").val();i++)//NUMERO DE CONCEPTOS
            {
                    impconcepto 	= $("#montomesconcepto"+i).val()

                    cuotaconcepto	= Round(parseFloat(impconcepto) * (parseFloat(cuotamensual) /  parseFloat(totsaldo)), 2)
                    cuotaultmes     = 0
					
                    if(ultmes!=0 )
                    {
                        x = parseFloat(cuotamensual) + parseFloat(ultmes)

                        cuotaultmes  = Round(parseFloat(impconcepto) * parseFloat(x) /  parseFloat(totsaldo), 2)
                    }
                    else
                    {
                      cuotaultmes  = Round(parseFloat(impconcepto) * parseFloat(cuotamensual) /  parseFloat(totsaldo), 2)
                    }
					
                    $("#montoconcepto" + i).html(parseFloat(cuotaconcepto).toFixed(2));
					$("#montomes" + i).val(parseFloat(cuotaconcepto).toFixed(2));
                    $("#montoultmes" + i).val(parseFloat(cuotaconcepto).toFixed(2));
                    //alert(cuotaconcepto+'-'+cuotaconcepto+'-'+cuotaultmes)
                    sumultmes = Round(parseFloat(sumultmes) + parseFloat(cuotaultmes),2)
                    
                    SumaTotalI += parseFloat(cuotaconcepto)
                    SumaTotalF += parseFloat(cuotaultmes)

            }
            //alert(SumaTotalI)
            //alert(SumaTotalF)
            var totalsumatoria= SumaTotalI;
            SumaTotalI = SumaTotalI*parseFloat(parseInt(ncuota)-1)
            var TotalF = SumaTotalI+SumaTotalF
            
            var Dif      = parseFloat(totafinanciar) - parseFloat(TotalF);
            Round(parseFloat(Dif), 2)
            
            var UltimoC = $("#montoultmes1").val()
            
           //alert($("#montoultmes1").val())
            UltimoC=parseFloat(UltimoC)+(Dif)
            
            $("#montoultmes1").val(Round(parseFloat(UltimoC), 2))
            //alert($("#montoultmes1").val())
            $("#ultmes").val(sumultmes)
            ///
            var cuotamensual= $("#cuotames").val()
           // alert(parseFloat(cuotamensual).toFixed(2)+'<--->'+parseFloat(totalsumatoria).toFixed(2))
            if(parseFloat(cuotamensual).toFixed(2) != parseFloat(totalsumatoria).toFixed(2))
            {
              var dif = parseFloat(cuotamensual).toFixed(2)-parseFloat(totalsumatoria).toFixed(2);
			  
              dif = parseFloat(dif).toFixed(2)
              
              $("#montoconcepto1").html(parseFloat($("#montoconcepto1").html()) + parseFloat(dif))
              $("#montoultmes1").val(parseFloat($("#montomes1").val()) + parseFloat(dif))
              $("#montomes1").val(parseFloat($("#montomes1").val()) + parseFloat(dif))


            }
                
	}
</script>
<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbconceptodeuda" rules="all" class="myTable">
  <thead>
    <tr class="ui-widget-header">
      <th width="8%" >Concepto</th>
      <th width="47%" >Importe</th>
      <th width="20%" >Monto Mensual </th>
      <th width="25%" >Cuota Mensual </th>
    </tr>
  </thead>
  <tbody>
    <?php
		$count	= 0;
		$total 	= 0;
		
		foreach($itemS as $rowS)
		{
			$count ++;
			$total += $rowS["imptotal"];
   ?>
    <tr style="background-color:#FFF">
      <td align="center">
          <input type="hidden" name="concepto<?=$count?>" id="concepto<?=$count?>" value="<?=$rowS["codconcepto"]?>" />
          <?=$rowS["codconcepto"]?>
      </td>
      <td align="left"><?=strtoupper($rowS["descripcion"])?></td>
      <td align="right">
          <input type="hidden" name="montomesconcepto<?=$count?>" id="montomesconcepto<?=$count?>" value="<?=number_format($rowS["imptotal"],2)?>" />
          <?=number_format($rowS["imptotal"],2)?>
      </td>
      <td align="right">
          <input type="hidden" name="montoultmes<?=$count?>" id="montoultmes<?=$count?>" value="0.00" />
          <input type="hidden" name="montomes<?=$count?>" id="montomes<?=$count?>" value="0.00" />
          <label id="montoconcepto<?=$count?>">0.00</label>
      </td>
    </tr>
    <?php 
		}
    $totalafinanciar = $total - $cuotaini;
	?>
	</tbody>
  <tfoot class="ui-widget-header">
    <tr>
      <td align="center">&nbsp;</td>
      <td align="right">Totales:</td>
      <td align="right"><input type="hidden" name="totalsaldo" id="totalsaldo" value="<?=$total?>" />
       <label id="Lbltotalsaldo"><?=number_format($total,2)?></label> </td>
      <td align="right">&nbsp;
        <input type="hidden" name="contador" id="contador" value="<?=$count?>" />
        <input type="hidden" name="totcuotas" id="totcuotas" value="0" />
        <input type="hidden" name="ultmes" id="ultmes" value="0" />
      </td>
    </tr>
    
    <tr>
      <td align="center">&nbsp;</td>
      <td align="right">C. Inicial:</td>
      <td align="right"><input type="hidden" id="cuotaini" value="<?=$cuotaini?>" />
        <?=number_format($cuotaini,2)?></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td align="center">&nbsp;</td>
      <td align="right">Total a Financiar:</td>
      <td align="right"><input type="hidden" name="totalafinanciar" id="totalafinanciar" value="<?=$totalafinanciar?>" />
        <label id="Lbltotalafinanciar"><?=number_format($totalafinanciar,2)?></label></td>
      <td align="right">&nbsp;</td>
    </tr>
  </tfoot>
</table>
<div style="height:10px">&nbsp;<div>
<div style="padding:4px; border:1px #000000 dashed; width:100%">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td width="15%">Nro. Cuotas </td>
		<td width="2%" align="center">:</td>
		<td width="18%"><label>
		  <input type="text" name="nrocuota" id="nrocuota" class="inputtext" value="" onKeyUp="cuotamensualtotal(this);" />
		</label></td>
	    <td width="14%">&nbsp;</td>
	    <td width="17%" align="right">C. Mensual </td>
	    <td width="2%" align="center">:</td>
	    <td width="32%"><input type="text" name="cuotames" id="cuotames" readonly="readonly" class="inputtext" value="0.00" /></td>
	  </tr>
	</table>
</div>
<div style="height:10px">&nbsp;<div>
<script>$("#nrocuota").focus();</script>