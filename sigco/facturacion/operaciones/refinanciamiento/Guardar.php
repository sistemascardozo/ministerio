<?php
	include("clsproceso.php");
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codemp = 1;
	$codsuc = $_SESSION["IdSucursal"];

	$facturacion	= $objFunciones->datosfacturacion($codsuc, $_POST["ciclo"]);
	$glosa			= $_POST["glosarefinanciamiento"];
	$totalrefinanciado = $_POST["totrefinanciamiento"];
	$cuotames		= $_POST["cuotamensual"];
	$nrocuotas		= $_POST["nrocuotas"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$fechareg		= $objFunciones->CodFecha($objFunciones->FechaServer());
	$estadorefinanciamiento = 1;
	$idusuario		= $_SESSION["id_user"];
	$count			= $_POST["count"];
	$interes		= $_POST["interescredito"] ? $_POST["interescredito"] : 0;
	$sininteres		= $_POST["sininteres"] ? $_POST["sininteres"] : 0;
	//$contador               = $_SESSION["orefinanciamiento"]->item;
	$Cuotas			= $_SESSION["orefinanciamiento"];
	
	$anio	= $facturacion["anio"];
	$mes	= $facturacion["mes"];

/* -_- */
	$DiaAc	= date('j');
	$MesAc	= date('n');
	$AnioAc	= date('Y');
	
	$SelFactActual = "SELECT nrofacturacion FROM facturacion.periodofacturacion WHERE anio='$AnioAc' AND mes='$MesAc' ";
	
	$consultaNF = $conexion->prepare($SelFactActual);
	$consultaNF->execute(array());
	$item = $consultaNF->fetch();
	
	$NroFactActual = $item['nrofacturacion'];

	$nrorefinanciamiento = $objFunciones->setCorrelativosVarios(11, $codsuc, "SELECT", 0);
	$nroprepagocre = $objFunciones->setCorrelativosVarios(6, $codsuc, "SELECT", 0);
	
	$creoprepago = false;
	$nroprepagocreInicial = $nroprepagocre + 1;
	
	$conexion->beginTransaction();
	
	$sqlL = "INSERT INTO facturacion.cabrefinanciamiento(codemp, codsuc, nrorefinanciamiento, nroinscripcion, fechaemision, codestadorefinanciamiento, ";
	$sqlL .= " totalrefinanciado, cuotames, nrocuotas, creador, glosa, interes, sininteres, hora) ";
	$sqlL .= "VALUES(".$codemp.", ".$codsuc.", ".$nrorefinanciamiento.", ".$nroinscripcion.", '".$fechareg."', ".$estadorefinanciamiento.", ";
	$sqlL .= " '".$totalrefinanciado."', ".$cuotames.", ".$nrocuotas.", ".$idusuario.", '".$glosa."', '".$interes."', ".$sininteres.", '".date('H:i')."')";

	$consultaL = $conexion->prepare($sqlL);
	$consultaL->execute(array());
	
	if ($consultaL->errorCode() != '00000')
	{
		$conexion->rollBack();
		$mensaje = "Error reclamos";
		
		die(2);
	}
	
	$Fac = array();
	
	for ($i = 1; $i <= $count; $i++)
	{
		if (isset($_POST["facturacion".$i]))
		{
			$upd = "UPDATE facturacion.detfacturacion ";
			$upd .= "SET categoria = 2 ";
			$upd .= "WHERE nrofacturacion = ".$_POST["facturacion".$i]." ";
			$upd .= " AND nroinscripcion = ".$nroinscripcion." ";
			$upd .= " AND codemp = ".$codemp." AND codsuc = ".$codsuc;

			$consultaU = $conexion->prepare($upd);
			$consultaU->execute(array());

			if ($consultaU->errorCode() != '00000')
			{
				$conexion->rollBack();
				$mensaje = "Error reclamos";
				
				die(2);
        	}
			
			$sqlO = "INSERT INTO facturacion.origen_refinanciamiento ";
			$sqlO .= " (codemp, codsuc, nrorefinanciamiento, codciclo, ";
			$sqlO .= " nrofacturacion, nroinscripcion) ";
			$sqlO .= "VALUES(".$codemp.", ".$codsuc.", ".$nrorefinanciamiento.", ".$_POST["codciclo".$i].", ";
			$sqlO .= " ".$_POST["facturacion".$i].", ".$nroinscripcion.")";

			if (!array_key_exists($_POST["facturacion".$i], $Fac))
			{
				$Fac[$_POST["facturacion".$i]] = $_POST["facturacion".$i];
				
				$consultaO = $conexion->prepare($sqlO);
				$consultaO->execute(array());
				
				if ($consultaO->errorCode() != '00000')
				{
					$conexion->rollBack();
					$mensaje = "Error reclamos";
					
					die(2);
				}
			}
		}
	}

/*---------------------------------------------------------*/

if($DiaAc > 6)
{
    //var_dump($NroFactActual);
    //var_dump($DiaAc);
    //exit;
    $mes = date('n');
    $nrofacturacion = $NroFactActual;

    $nrofact = ($nrofacturacion - 1) + $nrocuotas;
    $contador = $_POST["NroConceptos"];
    $cuotaini = floatval($_POST['cuotainicial']);
	
    if ($cuotaini > 0)
	{
        $nrofacturacion = $nrofacturacion - 1;
        $nrofacturacionO = $nrofacturacion;
    }
    //print_r($Cuotas);
	
    for ($i = 1; $i <= $nrocuotas; $i++) {

        if ($cuotaini > 0)
		{
            $m_contador = 1;
            $cuotaini = 0;
        }
		else
		{
            $m_contador = $contador;
		}

        for ($j = 1; $j <= $m_contador; $j++)
		{
            if ($nrofact == $nrofacturacion)
			{
                //$monto=$_SESSION["orefinanciamiento"]->montoultmes[$j];
                $monto = $Cuotas[$i][$j]["Monto"];
				
                if ($monto == 0.00 || $monto == 0)
				{
                    //$monto= $_SESSION["orefinanciamiento"]->montoconcepto[$j];
                    $monto = $Cuotas[$i][$j]["Monto"];
                }
            }
			else
			{
                //$monto= $_SESSION["orefinanciamiento"]->montoconcepto[$j];
                $monto = $Cuotas[$i][$j]["Monto"];
            }

            $estado = $Cuotas[$i][$j]["Estado"];
			
            if ($monto <> 0.00)
			{

                $Dias = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
                $mesv = $mes;
				
                if (intval($mes) < 10)
				{
                    $mesv = "0".$mes;
				}
				
                if (intval($Dias) < 10)
				{
                    $Dias = "0".$Dias;
				}
				
                $sqlD = "INSERT INTO facturacion.detrefinanciamiento(nrorefinanciamiento,nrocuota,totalcuotas,codconcepto,
                    importe,estadocuota,nrofacturacion,codemp,codsuc,anio,mes,fechavencimiento, item) 
                    VALUES(:nrorefinanciamiento,:nrocuota,:totalcuotas,:codconcepto,:importe,:estadocuota,
                    :nrofacturacion,:codemp,:codsuc,:anio,:mes,:fechavencimiento, :item)";

                $consultaD = $conexion->prepare($sqlD);
                $consultaD->execute(array(":nrorefinanciamiento" => $nrorefinanciamiento,
                    ":nrocuota" => $i,
                    ":totalcuotas" => $nrocuotas,
                    //":codconcepto"=>$_SESSION["orefinanciamiento"]->codconcepto[$j],
                    ":codconcepto" => $Cuotas[$i][$j]["concepto"],
                    ":importe" => $monto,
                    ":estadocuota" => $estado,
                    ":nrofacturacion" => $nrofacturacion,
                    ":codemp" => $codemp,
                    ":codsuc" => $codsuc,
                    ":anio" => $anio,
                    ":mes" => $mes,
                    ":fechavencimiento" => $anio."-".$mesv."-".$Dias,
					":item" => $j));
				
                if ($consultaD->errorCode() != '00000')
				{
                    $conexion->rollBack();
                    $mensaje = "Error reclamos";
                    die(2);
                }
            }
        }

        $mes++;
        if ($mes > 12) {
            $mes = 1;
            $anio++;
        }

        $nrofacturacion = $nrofacturacionO + $i;
    }
}
else
{
    
    $nrofacturacion = $facturacion["nrofacturacion"];

    $nrofact = ($nrofacturacion - 1) + $nrocuotas;
    $contador = $_POST["NroConceptos"];
    $cuotaini = floatval($_POST['cuotainicial']);
	
    if ($cuotaini > 0) {
        $nrofacturacion = $nrofacturacion - 1;
        $nrofacturacionO = $nrofacturacion;
    }
    //print_r($Cuotas);
    for ($i = 1; $i <= $nrocuotas; $i++) {

        if ($cuotaini > 0) {
            $m_contador = 1;
            $cuotaini = 0;
        } else
            $m_contador = $contador;

        for ($j = 1; $j <= $m_contador; $j++) {
            if ($nrofact == $nrofacturacion) {
                //$monto=$_SESSION["orefinanciamiento"]->montoultmes[$j];
                $monto = $Cuotas[$i][$j]["Monto"];
                if ($monto == 0.00 || $monto == 0) {
                    //$monto= $_SESSION["orefinanciamiento"]->montoconcepto[$j];
                    $monto = $Cuotas[$i][$j]["Monto"];
                }
            } else {
                //$monto= $_SESSION["orefinanciamiento"]->montoconcepto[$j];
                $monto = $Cuotas[$i][$j]["Monto"];
            }

            $estado = $Cuotas[$i][$j]["Estado"];
            if ($monto <> 0.00) {

                $Dias = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
                $mesv = $mes;
                if (intval($mes) < 10)
                    $mesv = "0".$mes;
                if (intval($Dias) < 10)
                    $Dias = "0".$Dias;

                $sqlD = "INSERT INTO facturacion.detrefinanciamiento(nrorefinanciamiento,nrocuota,totalcuotas,codconcepto,
                    importe,estadocuota,nrofacturacion,codemp,codsuc,anio,mes,fechavencimiento, item) 
                    VALUES(:nrorefinanciamiento,:nrocuota,:totalcuotas,:codconcepto,:importe,:estadocuota,
                    :nrofacturacion,:codemp,:codsuc,:anio,:mes,:fechavencimiento, :item)";

                $consultaD = $conexion->prepare($sqlD);
                $consultaD->execute(array(":nrorefinanciamiento" => $nrorefinanciamiento,
                    ":nrocuota" => $i,
                    ":totalcuotas" => $nrocuotas,
                    //":codconcepto"=>$_SESSION["orefinanciamiento"]->codconcepto[$j],
                    ":codconcepto" => $Cuotas[$i][$j]["concepto"],
                    ":importe" => $monto,
                    ":estadocuota" => $estado,
                    ":nrofacturacion" => $nrofacturacion,
                    ":codemp" => $codemp,
                    ":codsuc" => $codsuc,
                    ":anio" => $anio,
                    ":mes" => $mes,
                    ":fechavencimiento" => $anio."-".$mesv."-".$Dias,
					":item" => $j));
					
                if ($consultaD->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "Error reclamos";
                    die(2);
                }
            }
        }

        $mes++;
        if ($mes > 12) {
            $mes = 1;
            $anio++;
        }

        $nrofacturacion = $nrofacturacionO + $i;
    }
    
}

/*------------------------*/


$cuotaini = floatval($_POST['cuotainicial']);

//INSERTAR DETALLE DE COLATERALES
$NroItems = $_POST['NroItems'];
for ($i = 1; $i <= $NroItems; $i++) {
    if (isset($_POST["itemC".$i])) {
        $sqlD = "INSERT INTO facturacion.detcabrefinanciamiento
			(  codemp,  codsuc,  nrorefinanciamiento,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal)
			VALUES (  :codemp,  :codsuc,  :nrorefinanciamiento,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";

        $resultD = $conexion->prepare($sqlD);
        $resultD->execute(array(":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":nrorefinanciamiento" => $nrorefinanciamiento,
            ":codconcepto" => $_POST['codconceptoC'.$i],
            ":item" => $_POST["itemC".$i],
            ":subtotal" => str_replace(",", "", $_POST["subtotalC".$i]),
            ":igv" => str_replace(",", "", $_POST["igvC".$i]),
            ":redondeo" => str_replace(",", "", $_POST["redondeoC".$i]),
            ":imptotal" => str_replace(",", "", $_POST["imptotalC".$i]),
        ));
        if ($resultD->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error reclamos";
            $data['res'] = 2;
			
            die(json_encode($data));
        }
    }
}

//INSERTAR DETALLE DE COLATERALES
//SIEMPRE INSERTA PREPAGO POR EL TOTAL DE COLATERALES
if ($NroItems > 0) {
    //INSERTAR UN PREPAGO POR EL MONTO TOTAL
    $sql = "SELECT c.codcliente,c.propietario,c.codtipodocumento,c.nrodocumento,c.codciclo,tc.descripcioncorta,
            cl.descripcion as calle,c.nrocalle,c.codciclo,td.abreviado,c.codsector,
            c.codcalle
            FROM catastro.clientes as c
            INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
            inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
            where c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

    $result = $conexion->prepare($sql);
    $result->execute(array($codsuc, $nroinscripcion));
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error SELECT Catastro";
        $data['res'] = 2;
        die(json_encode($data));
    }
	
    $item = $result->fetch();
    $propietario = strtoupper($item["propietario"]);
    $direccion = strtoupper($item["descripcioncorta"]." ".$item["calle"]." ".$item["nrocalle"]);
    $docidentidad = strtoupper($item["nrodocumento"]);

    $creoprepago = true;
    $condpago = 1;
    $documento = $_POST["documentoC"];
    $formapago = 1;
    //$fechareg = $femision;
    $default = array('6' => 9); //CAR DEFECTO
    
	$car = $_POST["carx"];
	
    if ($car == '')
        $car = $default[$codsuc];
		
    $nrocaja = $_SESSION["nocaja"];
    $idusuario = $_SESSION['id_user'];
    $hora = $objFunciones->HoraServidor();
    $eventualtext = 0;
    $nrofacturacion = 0;
    $npresupuesto = 0;
    $glosa = $_POST["glosa"];
	
    $sqlC = "INSERT INTO cobranza.cabprepagos(codemp,codsuc,nroinscripcion,nroprepago,car,codformapago,creador,fechareg,subtotal,hora,
		        codciclo,condpago,igv,redondeo,imptotal,eventual,propietario,direccion,documento,glosa,nroinspeccion,estado,
		        nrocredito,origen)
		        VALUES(:codemp,:codsuc,:nroinscripcion,:nroprepago,:car,:codformapago,:creador,:fechareg,
		        :subtotal,:hora,:codciclo,:condpago,:igv,:redondeo,:imptotal,:eventual,:propietario,:direccion,:documento,
		        :glosa,:nroinspeccion,:estado,:nrocredito,:origen)";
    $resultC = $conexion->prepare($sqlC);
    $resultC->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nroprepago" => $nroprepagocre,
        ":nroinscripcion" => $nroinscripcion,
        ":car" => $car,
        ":codformapago" => $formapago,
        ":creador" => $idusuario,
        ":fechareg" => $fechareg,
        ":subtotal" => str_replace(",", "", $_POST['subtotal']),
        ":hora" => $hora,
        ":codciclo" => 1,
        ":condpago" => $condpago,
        ":igv" => str_replace(",", "", $_POST['igv']),
        ":redondeo" => str_replace(",", "", $_POST['redondeo']),
        ":imptotal" => str_replace(",", "", $_POST['imptotal']),
        ":eventual" => $eventualtext,
        ":propietario" => $propietario,
        ":direccion" => $direccion,
        ":documento" => $docidentidad,
        ":glosa" => $glosa,
        ":nroinspeccion" => 0,
        ":estado" => 4,
        ":nrocredito" => $nrorefinanciamiento,
        ":origen" => 2));
    if ($resultC->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error UPDATE cabpagos";
        $data['res'] = 2;
        die(json_encode($data));
    }
	
    $NroItems = $_POST['NroItems'];
	
	$Item = 0;
	
    for ($i = 1; $i <= $NroItems; $i++)
	{
        if (isset($_POST["itemC".$i]))
		{
			$DocSeries = $objFunciones->GetSeries($codsuc, $documento);
            $seriedoc = $DocSeries["serie"];
            $numerodoc = $DocSeries["correlativo"];
            $fechas = $objFunciones->DecFechaLiteral();
            $anio = $fechas["anio"];
            $mes = $fechas["mes_num"];
            $facturacion = 0;
			
			$Item++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, ";
			$sqlD .= " coddocumento, nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto, item) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$_POST['codconceptoC'.$i].", ";
			$sqlD .= " '".str_replace(",", "", $_POST["imptotalC".$i])."', '".$_POST['Colateral'.$i]."', 0, ".$nrofacturacion.", ";
			$sqlD .= " ".$documento.", ".$numerodoc.", '".$seriedoc."', 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.", ".$Item.")";
            //$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL

            $resultD = $conexion->prepare($sqlD);
            $resultD->execute(array());

            if ($resultD->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error UPDATE cabpagos";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }
	
	
		
    //ACTUALIZAR CORRELATIVO
    $sql = "UPDATE  reglasnegocio.correlativos
		    SET  correlativo = correlativo+1
		    WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocorrelativo=".$documento;
    $result = $conexion->query($sql);
    if (!$result) {
        $conexion->rollBack();
        $mensaje = "619 Error UPDATE correlativos";
        $data['res'] = 2;
        $data['mensaje'] = $mensaje;
        die(json_encode($data));
    }
    //INSERTAR UN PREPAGO POR EL MONTO TOTAL
    //ACTUALIZAR NRO PAGO
    if (!$creoprepago)
        $nroprepagocre = 0;
    $sql = "UPDATE  facturacion.cabrefinanciamiento
			SET  nroprepago=".$nroprepagocre."
			WHERE codemp=1 AND  codsuc=".$codsuc." AND nrorefinanciamiento=".$nrorefinanciamiento."";
    $result = $conexion->query($sql);
    if (!$result) {
        $conexion->rollBack();
        $mensaje = "Error UPDATE correlativos";
        $data['res'] = 2;
        die(json_encode($data));
    }
}
//INSERTAR UN PREPAGO POR EL MONTO TOTAL
//////////////NOTA DE PAGO DE CUOTA INICIAL
//die("-->".$_POST['cuotainicialflag']."<--");
if ($cuotaini > 0) {

    //INSERTA DIRECTAMENTE EN CAJA EL PAGO

    $sql = "SELECT c.codcliente,c.propietario,c.codtipodocumento,c.nrodocumento,c.codciclo,tc.descripcioncorta,
            cl.descripcion as calle,c.nrocalle,c.codciclo,td.abreviado,c.codsector,
            c.codcalle
            FROM catastro.clientes as c
            INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
            inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
            where c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

    $result = $conexion->prepare($sql);
    $result->execute(array($codsuc, $nroinscripcion));
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error SELECT Catastro";
        $data['res'] = 2;
        die(json_encode($data));
    }
    $item = $result->fetch();
    $propietario = strtoupper($item["propietario"]);
    $direccion = strtoupper($item["descripcioncorta"]." ".$item["calle"]." ".$item["nrocalle"]);
    $docidentidad = strtoupper($item["nrodocumento"]);

    $condpago = 1;
    $documento = 16; //NOTA DE PAGO//$_POST["documentox"];
    $formapago = 1; //$_POST["formapagox"];
    //$fechareg = $femision;
    $default = array('1' => 9, '2' => 4, '3' => 2); //CAR DEFECTO
    $car = $_POST["carx"];
    if ($car == '')
        $car = $default[$codsuc];
    $nrocaja = $_SESSION["nocaja"];
    $idusuario = $_SESSION['id_user'];

    $imptotal = $_POST["importemes1"];

    $efectivo = $_POST["efectivo_x"];
    $hora = $objFunciones->HoraServidor();
    $igv = $_POST["igv1"];
    $redondeo = $_POST["redondeo1"];
    $subtotal = $_POST["stotal1"]; //PRIMERA CUOTA
    //$nroprepago = 0;
    $fechas = $objFunciones->DecFechaLiteral();
    $anio = $fechas["anio"];
    $mes = $fechas["mes_num"];
    $facturacion = 0;

    $glosa = $_POST["glosa"];
    if ($glosa == '')
        $glosa = 'PAGO DE CUOTA INICIAL, REFINANCIAMIENTO NRO. '.$nrorefinanciamiento;
    $eventualtext = 0;
    $nrofacturacion = 0;
    $npresupuesto = 0;


    /* $sqlC = "INSERT into cobranza.cabpagos(codemp,codsuc,nroinscripcion,nropago,car,nrocaja,codformapago,creador,imptotal,imppagado,hora,
      condpago,igv,redondeo,subtotal,propietario,direccion,nrodocumento,eventual,nroprepago,fechareg,anio,mes,glosa,nrocredito)
      VALUES(:codemp,:codsuc,:nroinscripcion,:nropago,:car,:nrocaja,:codformapago,:creador,:imptotal,:imppagado,:hora,
      :condpago,:igv,:redondeo,:subtotal,:propietario,:direccion,:nrodocumento,:eventual,:nroprepago,:fechareg,:anio,:mes,:glosa,:nrocredito);";

      $result = $conexion->prepare($sqlC);
      $result->execute(array(":codemp" => $codemp,
      ":codsuc" => $codsuc,
      ":nroinscripcion" => $nroinscripcion,
      ":nropago" => $nropago,
      ":car" => $car,
      ":nrocaja" => $nrocaja,
      ":codformapago" => $formapago,
      ":creador" => $idusuario,
      ":imptotal" => str_replace(",", "", $imptotal),
      ":imppagado" => str_replace(",", "", $efectivo),
      ":hora" => $hora,
      ":condpago" => $condpago,
      ":igv" => str_replace(",", "", $igv),
      ":redondeo" => str_replace(",", "", $redondeo),
      ":subtotal" => str_replace(",", "", $subtotal),
      ":propietario" => $propietario,
      ":direccion" => $direccion,
      ":nrodocumento" => $docidentidad,
      ":eventual" => $eventualtext,
      ":nroprepago" => $nroprepago,
      ":fechareg" => $fechareg,
      ":anio" => $anio,
      ":mes" => $mes,
      ":glosa" => $glosa,
      ":nrocredito" => $nrocredito,
      )); */

    if ($creoprepago)
        $nroprepagocreInicial = $nroprepagocreInicial;
    else
        $nroprepagocreInicial = $nroprepagocre;
    $sqlC = "INSERT INTO cobranza.cabprepagos(codemp,codsuc,nroinscripcion,nroprepago,car,codformapago,creador,fechareg,subtotal,hora,
        codciclo,condpago,igv,redondeo,imptotal,eventual,propietario,direccion,documento,glosa,nroinspeccion,estado,
        nrocredito,origen)
        VALUES(:codemp,:codsuc,:nroinscripcion,:nroprepago,:car,:codformapago,:creador,:fechareg,
        :subtotal,:hora,:codciclo,:condpago,:igv,:redondeo,:imptotal,:eventual,:propietario,:direccion,:documento,
        :glosa,:nroinspeccion,:estado,:nrocredito,:origen)";
    $resultC = $conexion->prepare($sqlC);
    $resultC->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nroprepago" => $nroprepagocreInicial,
        ":nroinscripcion" => $nroinscripcion,
        ":car" => $car,
        ":codformapago" => $formapago,
        ":creador" => $idusuario,
        ":fechareg" => $fechareg,
        ":subtotal" => str_replace(",", "", $subtotal),
        ":hora" => $hora,
        ":codciclo" => 1,
        ":condpago" => $condpago,
        ":igv" => str_replace(",", "", $igv),
        ":redondeo" => str_replace(",", "", $redondeo),
        ":imptotal" => str_replace(",", "", $imptotal),
        ":eventual" => $eventualtext,
        ":propietario" => $propietario,
        ":direccion" => $direccion,
        ":documento" => $docidentidad,
        ":glosa" => $glosa,
        ":nroinspeccion" => 0,
        ":estado" => 1,
        ":nrocredito" => $nrorefinanciamiento,
        ":origen" => 2));

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error INSERT CobranzaCabPagos";
        $data['res'] = 2;
        die(json_encode($data));
    }
    //OBTENER CORRELATIVOS ACTUALES
    $DocSeries = $objFunciones->GetSeries($codsuc, $documento);
    $seriedoc = $DocSeries["serie"];
    $numerodoc = $DocSeries["correlativo"];
    //OBTENER CORRELATIVOS ACTUALES
    /* 	$insdetpagos = "INSERT into cobranza.detpagos(codemp,codsuc,nrofacturacion,nroinscripcion,nropago,codconcepto,importe,codtipodeuda,";
      $insdetpagos .= "tipopago,codcategoria,coddocumento,serie,nrodocumento,categoria,detalle,anio,mes,nropresupuesto)
      VALUES(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:nropago,:codconcepto,:importe,:codtipodeuda,
      :tipopago,:codcategoria,:coddocumento,:serie,:nrodocumento,:categoria,:detalle,:anio,:mes,:nropresupuesto)";

      $result = $conexion->prepare($insdetpagos);
      $result->execute(array(":codemp" => $codemp,
      ":codsuc" => $codsuc,
      ":nrofacturacion" => $nrofacturacion,
      ":nroinscripcion" => $nroinscripcion,
      ":nropago" => $nropago,
      ":codconcepto" => $_POST["codconcepto"],
      ":importe" => str_replace(",", "", $imptotal),
      ":codtipodeuda" => 5,//$_POST["codtipodeuda".$i],
      ":tipopago" => 0,
      ":codcategoria" => 4,//$_POST["codcategoria".$i],
      ":coddocumento" => $documento,
      ":serie" => $seriedoc,
      ":nrodocumento" => $numerodoc,
      ":categoria" => 3,
      ":detalle" => 'PAGO POR '.$_POST["colateral"],
      ":anio" => $anio,
      ":mes" => $mes,
      ":nropresupuesto" => $npresupuesto));
     */
	$Itemm = 0;
	
	$SubTotal = $imptotal - $igv;
    $codconcepto = 10; //CONCEPTO DE CUOTA INICIAL
	
    $sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, importe, detalle, tipopago, ";
	$sqlD .= " nrofacturacion, coddocumento, nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
	$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocreInicial.", ".$Itemm.", ".$codconcepto.", '".str_replace(",", "", $SubTotal)."', ";
	$sqlD .= " 'PAGO POR ".$_POST["colateral"]."', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', 5, ";
	$sqlD .= " '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
	
    $resultD = $conexion->prepare($sqlD);
    $resultD->execute(array());


    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error INSERT detpagos";

        $data['res'] = 2;
        die(json_encode($data));
    }
	
	//IGV
		if ($igv > 0)
		{
			$codconcepto = 5; //IGV
			
			$Itemm++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocreInicial.", ".$Itemm.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $igv)."', 'I.G.V.', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		//REDONDEO
		if ($redondeo < 0)
		{
			$codconcepto = 7; //DIFERENCIA REDONDEO NEGATIVO
			
			$Itemm++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocreInicial.", ".$Itemm.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $redondeo)."', 'DIFERENCIA REDONDEO NEGATIVO', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		if ($redondeo < 0)
		{
			$codconcepto = 8; //DIFERENCIA REDONDEO POSITIVO
			
			$Itemm++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocreInicial.", ".$Itemm.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $redondeo)."', 'DIFERENCIA REDONDEO POSITIVO', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		
		
    //ACTUALIZAR CORRELATIVO
    $sql = "UPDATE  reglasnegocio.correlativos SET  correlativo = correlativo+1
        WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocorrelativo=".$documento."";
    $result = $conexion->query($sql);
    if (!$result) {
        $conexion->rollBack();
        $mensaje = "Error UPDATE correlativos";
        $data['res'] = 2;
        die(json_encode($data));
    }
    //ACTUALIZAR NRO PAGO

    $sql = "UPDATE  facturacion.cabrefinanciamiento SET nroprepagoinicial=".$nroprepagocreInicial."
        WHERE codemp=1 AND  codsuc=".$codsuc." AND nrorefinanciamiento=".$nrorefinanciamiento."";
    $result = $conexion->query($sql);
    if (!$result) {
        $conexion->rollBack();
        $mensaje = "Error UPDATE correlativos";
        $data['res'] = 2;
        die(json_encode($data));
    }
    //ACTUALIZAR CUOTA INICIAL
    /* $sql = "UPDATE  facturacion.detcreditos
      SET  estadocuota = 3
      WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocredito=".$nrocredito." AND nrocuota=1 AND tipocuota=1";
      $result = $conexion->query($sql);
      if (!$result) {
      $conexion->rollBack();
      $mensaje = "Error UPDATE detcreditos";
      $data['res']    =2;
      die(json_encode($data)) ;
      } */
}

$total_cargos = ($_POST['total_cargos']);

// Para cargar cuotas anteriores
if (!empty($total_cargos)):

    $sql = "  SELECT c.nrocredito
              FROM facturacion.cabvarios AS c
                JOIN facturacion.detvarios d ON(c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nrocredito = d.nrocredito)
              WHERE c.codemp = 1 AND c.codsuc = ? AND c.nroinscripcion = ? AND d.estadocuota = 0 ";

    $result = $conexion->prepare($sql);
    $result->execute(array($codsuc, $nroinscripcion));
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error SELECT Catastro";
        $data['res'] = 2;
        die(json_encode($data));
    }
    $item = $result->fetchAll();

    foreach ($item as $row) :
        $sql = "UPDATE facturacion.cabvarios
                SET  estareg = 3, nrorefinanciamiento = ".$nrorefinanciamiento."
                WHERE codemp = 1 AND  codsuc=".$codsuc." AND nrocredito =".$row['nrocredito']."
                 AND nroinscripcion = ".$nroinscripcion." ";
        $result = $conexion->query($sql);
        if (!$result) {
            $conexion->rollBack();
            $mensaje = "Error UPDATE cabvarios con refinanciamiento";
            $data['res'] = 2;
            die(json_encode($data));
        }

        $sql = "UPDATE facturacion.detvarios
                SET  estadocuota = 5
                WHERE codemp = 1 AND  codsuc=".$codsuc." AND nrocredito =".$row['nrocredito']."";
        $result = $conexion->query($sql);
        if (!$result) {
            $conexion->rollBack();
            $mensaje = "Error UPDATE detvarios con refinanciamiento";
            $data['res'] = 2;
            die(json_encode($data));
        }

    endforeach;

endif;

///////////////PAGO DE CUOTA INICIAL

if ($consultaD->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    echo $res = 2;
} else {

    $conexion->commit();
    $objFunciones->setCorrelativosVarios(11, $codsuc, "UPDATE", $nrorefinanciamiento);
    if ($creoprepago) {
        $objFunciones->setCorrelativosVarios(6, $codsuc, "UPDATE", $nroprepagocre);
        if ($cuotaini > 0)
            $objFunciones->setCorrelativosVarios(6, $codsuc, "UPDATE", $nroprepagocreInicial);
        else
            $nroprepagocreInicial = 0;
    }
    else {
        if ($cuotaini > 0)
            $objFunciones->setCorrelativosVarios(6, $codsuc, "UPDATE", $nroprepagocre);
        else
            $nroprepagocreInicial = 0;
        $nroprepagocre = 0;
    }

    $mensaje = "El Registro se ha Grabado Correctamente";
    $res = 1;

    $data['res'] = $res;
    $data['nropago'] = 0; // $nropago;
    $data['nrorefinanciamiento'] = $nrorefinanciamiento;
    $data['nroprepagocre'] = $nroprepagocre;
    $data['nroprepagocreInicial'] = $nroprepagocreInicial;
    echo json_encode($data);
}
?>
