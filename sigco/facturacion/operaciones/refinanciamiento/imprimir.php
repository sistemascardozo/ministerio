<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsReporte.php");
	
	class clsRefinanciamiento extends clsReporte
	{
		function cabecera()
		{
			global $nrorefinanciamiento;
			$this->SetY(20);
			$this->SetFont('Arial','B',10);
			$tit1 	= "CRONOGRAMA DE FRACCIONAMIENTO DE DEUDA";
			$tit2 	= "Nro. DE REFINANCIAMIENTO : ".$nrorefinanciamiento;
			
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			$this->Cell(190,5,utf8_decode($tit2),0,1,'C');
		}
		function Contenido($nrorefinanciamiento,$codsuc,$NombreEmpresa)
		{			
			global $conexion,$meses;
			
			$h = 4;
			$s = 2;
			
			$sql  = "select cl.propietario,t.abreviado,cl.nrodocumento,c.fechaemision,c.glosa,c.totalrefinanciado,c.cuotames,c.nrocuotas,
					cl.codcliente,c.nroinscripcion,tc.descripcioncorta || ' ' || ca.descripcion || ' ' || cl.nrocalle as direccion
					from facturacion.cabrefinanciamiento as c
					inner join catastro.clientes as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.nroinscripcion=cl.nroinscripcion)
					inner join public.tipodocumento as t on(cl.codtipodocumento=t.codtipodocumento)
					
					inner join public.calles as ca on(cl.codemp=ca.codemp and cl.codsuc=ca.codsuc and cl.codcalle=ca.codcalle and cl.codzona=ca.codzona )
					inner join public.tiposcalle as tc on(ca.codtipocalle=tc.codtipocalle)
					inner join public.sectores as s on(cl.codemp=s.codemp and cl.codsuc=s.codsuc and cl.codsector=s.codsector)
					
					where c.codemp=1 and c.codsuc=? and c.nrorefinanciamiento=?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$nrorefinanciamiento));
			$row = $consulta->fetch();
			//var_dump($consulta->errorInfo());
			$this->Cell(0,0.1,"",1,0,'R');
			$this->Ln(5);
			$this->SetFont('Arial','',7);
			
			$this->Cell(25,$h,"Usuario",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(120,$h,$row["codcliente"]." ".utf8_decode(strtoupper($row["propietario"])),0,0,'L');
			
			$this->Cell(25,$h,"Fecha de Emision",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(0,$h,$this->DecFecha($row["fechaemision"]),0,1,'L');

			$this->Cell(25,$h,"Documento",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(120,$h,$row["abreviado"]." - ".$row["nrodocumento"],0,0,'L');
			
			$this->Cell(25,$h,"Nro Inscripci�n",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(0,$h,($row["nroinscripcion"]),0,1,'L');
			
			$this->Cell(25,$h,"Direcci�n",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(120,$h,$row["direccion"],0,1,'L');
			

			$this->Ln(1);
			$this->Cell(25,$h,"Observaci�n",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->SetX(38);
			$this->MultiCell(130,$h,strtoupper(utf8_decode($row["glosa"])),'0','J');
			
			$this->Cell(0,0.1,"",1,0,'R');

			$this->Ln(1);
			$this->Cell(25,$h,"Total Refinanciado",0,0,'R');
			$this->Cell(3,$h,":",0,0,'C');
			$this->Cell(30,$h,number_format($row["totalrefinanciado"],2),0,0,'L');
			
			$this->Cell(20,$h,"Cuota Mensual",0,0,'R');
			$this->Cell(3,$h,":",0,0,'C');
			$this->Cell(10,$h,number_format($row["cuotames"],2),0,0,'L');
			
			$this->Cell(20,$h,"Nro. Cuotas",0,0,'R');
			$this->Cell(3,$h,":",0,0,'C');
			$this->Cell(20,$h,number_format($row["nrocuotas"],0),0,1,'L');
			
			$this->Cell(0,0.1,"",1,1,'R');
			$this->Ln(1);

	
			$this->SetWidths(array(20,20,10,30,20,20,20,15,15,20));
			$this->SetAligns(array("C","C","C","C","C","C","C","C","C","C"));
			$this->Row(array("Nro. Cuota",
							 "Mes","A�o","Fecha de Vencimiento","Fecha de Pago",
							 "A Cuenta","Fecha A Cta",
							 "Cuota","Saldo","Estado"));
			
			$sqldetalle  = "select nrocuota,totalcuotas,anio,mes,sum(importe) as importe,
								estadocuota
							from facturacion.detrefinanciamiento
							where codemp=1 and codsuc=? and nrorefinanciamiento=?
							group by nrocuota,totalcuotas,anio,mes,estadocuota
							order by ".$this->Convert('anio','INTEGER')."  ,".$this->Convert('mes','INTEGER')."";
			
			$consultaD = $conexion->prepare($sqldetalle);
			$consultaD->execute(array($codsuc,$nrorefinanciamiento));
			$itemD = $consultaD->fetchAll();
			$Saldo=0;
			foreach($itemD as $rowD)
			{
				$Dias= cal_days_in_month(CAL_GREGORIAN,$rowD["mes"],$rowD["anio"]); 
				if(intval($rowD["mes"])<10) $rowD["mes"]="0".$rowD["mes"];
				if(intval($Dias)<10) $Dias="0".$Dias;
				$FechaP="";
				$Acuenta="";
				$FechaA="";
				$Saldo+=$rowD["importe"];
				switch ($rowD["estadocuota"]) {
					case '0': $Estado="PENDIENTE";break;
					case '1': $Estado="FACTURADO";break;
					case '2': $Estado="ANULADO";break;
					case '3': $Estado="CANCELADO";break;
					
				}
				$this->SetWidths(array(20,20,10,30,20,20,20,15,15,20));
				$this->SetAligns(array("C","C","C","C","C","C","C","R","R","C"));
				$this->Row(array($rowD["nrocuota"]."/".$rowD["totalcuotas"],
								 $meses[intval($rowD["mes"])],
								 $rowD["anio"],
								 $Dias."/".$rowD["mes"]."/".$rowD["anio"],
								 $FechaP,$Acuenta,$FechaA,
								 number_format($rowD["importe"],2),number_format($Saldo,2),
								 $Estado));
			}
			$this->Cell(140,$h,"Total:",0,0,'R');
			$this->Cell(15,$h,number_format($Saldo,2),0,0,'R');


			$this->Ln(30);
			$this->SetX(40);
			$this->Cell(50,0.1,"",1,0,'C');
			$this->SetX(130);
			$this->Cell(50,0.1,"",1,1,'C');
			$this->SetX(40);
			$this->Cell(50,$h,"USUARIO",0,0,'C');
			$this->SetX(130);
			$this->Cell(50,$h,"LA EMPRESA",0,1,'C');
			$this->SetX(40);
			$this->Cell(50,$h,"Sr.(a): ".utf8_decode(strtoupper($row["propietario"])),0,0,'L');
			$this->SetX(130);
			$this->Cell(50,$h,$NombreEmpresa,0,1,'C');
			$this->SetX(40);
			$this->Cell(50,$h,$row["abreviado"]." : ".$row["nrodocumento"],0,1,'L');

			$this->Ln(30);
			$texto="*El incumplimiento de pago de cualquier cuota en la fecha estipulada en el presente documento, obligara a la empresa a rescindir el CONTRATO y por ende al cobro de la deuda Total; adem�s se clausurar� o levantar� la conexi�n de agua y desague y se aperturara la cobranza de los adeudos via Judicial.";
			$this->SetX(15);
			$this->MultiCell(0,$h,(($texto)),'0','J');
			$this->Ln(5);

			$texto="*En los casos que el usuario se habilite clandestinamente el servicio clausurado, la empresa proceder a presentar la denuncia penal ante el poder judicial por el delito contra el patrimonio, en aplicaci�n del articulo 185 del Codigo Penal.";
			$this->SetX(15);
			$this->MultiCell(0,$h,(($texto)),'0','J');
			$this->Ln(5);

			$texto="*El Usuario acepta las conciciones de Refinanciamiento, como es el pago de los Intereses TAMN sobre el Total de la Deuda Refinanciada,Intereses que seran cargados Adicional a la Cuota Mensual en los Recibos posteriores hasta agotar la Deuda .";
			$this->SetX(15);
			$this->MultiCell(0,$h,(($texto)),'0','J');
			$this->Ln(5);

					
		}
    }

	$nrorefinanciamiento	= $_GET["id"];
	$codsuc					= $_GET["codsuc"];
	$NombreEmpresa = $_SESSION['NombreEmpresa'];
    $objReporte	=	new clsRefinanciamiento();
	$objReporte->AliasNbPages();
    $objReporte->AddPage();
	$objReporte->Contenido($nrorefinanciamiento,$codsuc,$NombreEmpresa);
	$objReporte->Output();	
	
	
?>