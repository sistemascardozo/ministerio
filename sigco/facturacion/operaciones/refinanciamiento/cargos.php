<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../objetos/clsFunciones.php");

	$nroinscripcion = $_POST["nroinscripcion"];
	$codsuc			    = $_POST["codsuc"];

	$sql           = "	SELECT  c.codconcepto AS codconcepto, co.descripcion AS concepto,
															SUM(d.imptotal) AS importe
											FROM facturacion.cabvarios AS c
												JOIN facturacion.detvarios d ON(c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nrocredito = d.nrocredito)
												JOIN facturacion.conceptos co ON(c.codemp = co.codemp AND c.codsuc = co.codsuc AND c.codconcepto = co.codconcepto)
											WHERE c.codemp = 1 AND c.codsuc = ? AND c.nroinscripcion = ? AND d.estadocuota = 0
											GROUP BY c.codconcepto, co.descripcion";

	$consulta      = $conexion->prepare($sql);
	$consulta->execute(array($codsuc, $nroinscripcion));
	$row           = $consulta->fetchAll();


?>
<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbcargos" rules="all" class="myTable">
  <thead>
    <tr class="ui-widget-header">
      <th width="8%" >Item</th>
      <th width="60%" >Concepto</th>
      <th width="25%" >Importe</th>
      <!-- <th width="10%" >Cargar</th> -->
    </tr>
  </thead>
  <tbody>
    <?php $count = 1; $total = 0;?>
		<?php foreach ($row as $key => $value) : ?>
			<tr>
				<td align="center"><?php echo $count ?><input type="hidden" name="codconcepto" id="codconcepto" value="<?php echo $value['codconcepto']; ?>" data="<?php echo $value['concepto']; ?>" ></td>
				<td><?php echo $value['concepto']; ?></td>
				<td align="right"><?php echo number_format($value['importe'],2); ?></td>
				<!-- <td align="center"><input type="checkbox" style="margin: 5px;" id="chkscargos" onclick="cargar_cargos(this);" /></td> -->
			</tr>
			<?php $count++; ?>
			<?php $total +=$value['importe']; ?>
		<?php endforeach; ?>
  </tbody>
	<tfoot>
		<tr>
			<td colspan="2" align="right"><span>Total a Cargar</span></td>
			<td align="right">
				<input readonly="readonly" id="total_cargar_cargos" name="total_cargar_cargos" value="<?php echo number_format($total,2) ?>" type="text" style="width: 70px; margin: 0px 0px 0px 50px; text-align: right;" />
			</td>
			<!-- <td align="center"><input type="checkbox" style="margin: 5px;" id="chkstcargos" onclick="todos_cargos(this);" /></td> -->
		</tr>
	</tfoot>
</table>
