<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../objetos/clsReporte.php");

	class clsCreditos extends clsReporte
	{
		function cabecera()
		{
			global $nrorefinanciamiento;

			$this->Ln();
			$this->SetFont('Arial','B',10);
			$tit1 	= "CONVENIO PARA PAGO FRACCIONADO ";
			$tit2 	= "Nro. : ".$nrorefinanciamiento;

			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');
			$this->Cell(190,5,utf8_decode($tit2),0,1,'C');
		}
		function Contenido($nrorefinanciamiento,$codsuc)
		{
			global $conexion,$meses,$NombreEmpresa;
			$codemp=1;
			$h = 4;
			$s = 2;

			$sql  = "select upper(clie.propietario),cab.fechaemision,cab.totalrefinanciado,
							'0.00' as igv,'0.00' as redondeo,
						cab.totalrefinanciado as imptotal,";
			$sql .= "cab.glosa,cab.creador,upper(doc.abreviado),clie.nrodocumento,";
			$sql .= "upper(cab.glosa),".$this->getCodCatastral("clie.").",clie.nroinscripcion,
					upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
					zo.descripcion as zona,cab.nroprepago,cab.nroprepagoinicial,cab.sininteres,cab.interes,cab.cuotames ";
			$sql .= "from facturacion.cabrefinanciamiento as cab ";
			$sql .= "inner join catastro.clientes as clie on(cab.codemp=clie.codemp and ";
			$sql .= "cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion) ";

			$sql .= "inner join public.tipodocumento as doc on(clie.codtipodocumento=doc.codtipodocumento)
					 inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
					 inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
					 inner join admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)";

			$sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrorefinanciamiento=?";

			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$nrorefinanciamiento));
			$row = $consulta->fetch();

			$this->Ln(5);
			$this->SetFont('Arial','',7);
			$y=$this->GetY();
			$this->Rect($x+10,$y,190,10);
			$this->Cell(47.5,$h,("FECHA DE CONVENIO"),0,0,'C');
			$this->Cell(47.5,$h,utf8_decode("CÓDIGO CATASTRAL"),0,0,'C');
			$this->Cell(47.5,$h,utf8_decode("CÓDIGO DE INSCRIPCIÓN"),0,0,'C');
			$this->Cell(47.5,$h,utf8_decode("N° DE CONTRATO"),0,1,'C');
			$this->SetFont('Arial','B',9);
			$this->Cell(47.5,$h,$this->DecFecha($row[1]),0,0,'C');
			$this->Cell(47.5,$h,$row['codcatastro'],0,0,'C');
			$this->Cell(47.5,$h,$row['nroinscripcion'],0,0,'C');
			$this->Cell(47.5,$h,"0",0,1,'C');

			$this->Ln(8);
			$this->SetFont('Arial','',10);
			$text="Celebrado entre la empresa Prestadora de Servicios, representada por su responsable comercial, que en lo sucesivo";
			$this->MultiCell(0,$h,(($text)),0,'J');
			$text=utf8_decode("se denominará la 'EMPRESA' y El Sr. (la Sra.) : ");
			$this->Cell(75,$h,$text,0,0,'L');
			$this->SetFont('Arial','B',10);
			$this->Cell(70,$h,(strtoupper($row[0])),0,0,'L');
			$this->SetFont('Arial','',10);
			$text="identificado (a) con ";
			$this->Cell(0,$h,$text,0,1,'L');
			$this->Cell(10,$h,$row[8]." : ",0,0,'L');
			$this->SetFont('Arial','B',10);
			$this->Cell(20,$h,$row[9],0,0,'L');
			$this->SetFont('Arial','',10);
			$text="domiciliado (a) en: ";
			$this->Cell(30,$h,$text,0,0,'L');
			$this->SetFont('Arial','B',10);
			$this->Cell(70,$h,$row['direccion'],0,0,'L');
			$this->SetFont('Arial','',10);
			$text= utf8_decode("Urbanización ");
			$this->Cell(0,$h,$text,0,1,'R');
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,$row['zona'],0,0,'L');
			$this->SetFont('Arial','',10);
			$text=", que en adelante se denominara el 'CLIENTE'. ";
			$this->Cell(0,$h,$text,0,1,'L');
			$this->Ln(3);
			$text="Por la deuda que asciende a :";
			$this->Cell(0,$h,$text,0,1,'L');
			$this->Ln(5);
			$h = 6;
			$nroprepago=$row['nroprepago'];
			if($nroprepago!="" && $nroprepago!="0")
			{
				$Sql="SELECT d.abreviado , p.serie ,  p.nrodocumento,SUM(p.importe) as importe
					FROM reglasnegocio.documentos d
					INNER JOIN cobranza.detprepagos p ON (d.coddocumento = p.coddocumento) AND (d.codsuc = p.codsuc)
				WHERE p.codemp=? AND  p.codsuc=? AND p.nroinscripcion=? AND p.nroprepago=?
				GROUP BY d.abreviado , p.serie ,  p.nrodocumento";
				//	print_r(array($codemp,$codsuc,$nroinscripcion,$codpago));
				$result = $conexion->prepare($Sql);
				$result->execute(array($codemp,$codsuc,$row['nroinscripcion'],$nroprepago));
				$row2 = $result->fetch();
				$documento = $row2[0];
				$recibon = $row2[1]."-".str_pad($row2[2],8,"0",STR_PAD_LEFT);
				$importe = number_format($row2['importe'],2);

				$Sql="SELECT fechareg
					FROM cobranza.cabprepagos
				WHERE codemp=? AND  codsuc=? AND nroinscripcion=? AND nroprepago=?";
				//	print_r(array($codemp,$codsuc,$nroinscripcion,$codpago));
				$result = $conexion->prepare($Sql);
				$result->execute(array($codemp,$codsuc,$row['nroinscripcion'],$nroprepago));
				$row2 = $result->fetch();
				$fechaprepago = $row2[0];

			}
			$this->SetFont('Arial','',6);
			$this->Cell(25,$h,"ITEM",1,0,'C');
			$this->Cell(30,$h,utf8_decode("FACTURACIÓN"),1,0,'C');
			$this->Cell(30,$h,"TIPO RECIBO",1,0,'C');
			$this->Cell(30,$h,"SERIE NUMERO",1,0,'C');
			$this->Cell(25,$h,"MONTO",1,0,'C');
			$this->Cell(25,$h,utf8_decode("EMISIÓN"),1,0,'C');
			$this->Cell(0,$h,"VENCIMIENTO",1,1,'C');
			$this->SetFont('Arial','',8);
			$this->SetFont('Arial','',8);
			$i=0;
			$TotalRef=0;
			//COLATERAL
			$Sql="SELECT SUM(imptotal)
				FROM facturacion.detcabrefinanciamiento
				  where codemp=1 and codsuc=? and nrorefinanciamiento=?";
			$consulta2 = $conexion->prepare($Sql);
			$consulta2->execute(array($codsuc,$nrorefinanciamiento));
			$itemD = $consulta2->fetchAll();

			foreach($itemD as $rowD)
			{
				if($rowD[0]!='')
				{
					$i++;
					$TotalRef+= $rowD[0];
					$this->Cell(25,$h,$i,1,0,'C');
					$this->Cell(30,$h,"COLATERALES",1,0,'C');
					$this->Cell(30,$h,"RECIBO",1,0,'C');
					$this->Cell(30,$h,$recibon,1,0,'C');
					$this->Cell(25,$h,number_format($rowD[0],2),1,0,'R');
					$this->Cell(25,$h,$this->DecFecha($fechaprepago),1,0,'C');
					$this->Cell(0,$h,$this->DecFecha($fechaprepago),1,1,'C');
				}
			}
			//RECIBOS
			$Sql="SELECT f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg
				FROM facturacion.cabfacturacion f
				  INNER JOIN facturacion.origen_refinanciamiento o ON (f.codemp = o.codemp)
				  AND (f.codsuc = o.codsuc)
				  AND (f.codciclo = o.codciclo)
				  AND (f.nrofacturacion = o.nrofacturacion)
				  AND (f.nroinscripcion = o.nroinscripcion)
				  where o.codemp=1 and o.codsuc=? and o.nrorefinanciamiento=?
				  GROUP BY f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg ";

			$consulta2 = $conexion->prepare($Sql);
			$consulta2->execute(array($codsuc,$nrorefinanciamiento));
			$itemD = $consulta2->fetchAll();

			foreach($itemD as $rowD)
			{
				$i++;
				 $Sql="select sum(importe - (importerebajado))
						 from facturacion.detfacturacion
					where codemp=1 and codsuc=? and nroinscripcion=? AND nrofacturacion=?";

				$result = $conexion->prepare($Sql);
				$result->execute(array($codsuc,$row['nroinscripcion'],$rowD['nrofacturacion']));
				$row2 = $result->fetch();
				$TotalRef+= $row2[0];
				$recibon = str_pad($rowD['serie'],3,"0",STR_PAD_LEFT)." - ".str_pad($rowD['nrodocumento'],7,"0",STR_PAD_LEFT);
				$this->Cell(25,$h,$i,1,0,'C');
				$this->Cell(30,$h,"PENSIONES",1,0,'C');
				$this->Cell(30,$h,"RECIBO",1,0,'C');
				$this->Cell(30,$h,$recibon,1,0,'C');
				$this->Cell(25,$h,number_format($row2[0],2),1,0,'R');
				$this->Cell(25,$h,$this->DecFecha($rowD['fechareg']),1,0,'C');
				$this->Cell(0,$h,$this->DecFecha($rowD['fechareg']),1,1,'C');
			}
			//RECIBOS
			$this->SetFont('Arial','B',8);
			$this->Cell(115,$h,"Deuda Negociable S/. ",1,0,'C');
			$this->Cell(25,$h,number_format($TotalRef,2),1,1,'R');

			$sqldetalle  = "select MAX(nrocuota) ";
			$sqldetalle .= "from facturacion.detrefinanciamiento ";
			$sqldetalle .= "where codemp=1 and codsuc=? and nrorefinanciamiento=?";
			$consultaD = $conexion->prepare($sqldetalle);
			$consultaD->execute(array($codsuc,$nrorefinanciamiento));
			$row2 = $consultaD->fetch();
			$nrocuota = $row2[0];
			$this->SetFont('Arial','',10);
			$h = 4;
			$this->Ln(8);
			$text="la misma que ha sido fraccionada en: ";
			$this->Cell(60,$h,$text,0,0,'L');
			$this->SetFont('Arial','B',10);
			$this->Cell(5,$h,$nrocuota,0,0,'L');
			$this->SetFont('Arial','',10);
			$text=" Cuotas. ";
			$this->Cell(0,$h,$text,0,1,'L');

			$this->Ln(5);

			$this->SetWidths(array(20,45,25,25,25,25,25,25,20));
			$this->SetAligns(array("C","L","C","C","C","C","C","C","C"));
			$this->Row(array("Nro. Cuota",
							 utf8_decode("Vencimiento Mes-Año"),
							 "Saldo",
							 utf8_decode("Amorización"),
							 "Interes",
							 "Igv",
							 "Cuota"
							 ));

			$sqldetalle  = "SELECT nrocuota, totalcuotas, '' AS fechavencimiento, ";
			$sqldetalle .= " SUM(importe) AS imptotal, '0.00' AS igv, '0.00' AS redondeo, SUM(importe), ";
			$sqldetalle .= " 0 AS tipocuota, nrofacturacion, mes, anio, '0.00' AS interes ";
			$sqldetalle .= "FROM facturacion.detrefinanciamiento ";
			$sqldetalle .= "WHERE codemp = 1 AND codsuc=? and nrorefinanciamiento=?
							group by nrocuota, totalcuotas, anio, mes, estadocuota, nrofacturacion
							order by nrocuota";

			$consultaD = $conexion->prepare($sqldetalle);
			$consultaD->execute(array($codsuc,$nrorefinanciamiento));
			$itemD = $consultaD->fetchAll();
			$Saldo = $row[5];
			$this->SetAligns(array("C","L","R","R","R","R","R","C","C"));
			$tinteres=0;
			$tigv=0;
			$totalcuotas=0;
			$this->SetFont('Arial','',8);
			$Saldo =  $TotalRef;
			$cuotames=$row['cuotames'];
			$vigv=0;
			
			foreach($itemD as $rowD)
			{				
				if($row['nroprepagoinicial']!=0 && $rowD["nrocuota"]==1) //CON INICIAL
				{
					$Amortizacion = $rowD["imptotal"];
					$interes = 0;
					$vigv = 0;
					$importe = $rowD["imptotal"];
				}
				else
				{

					if($row['sininteres']==0)
					{
						$interes =  round($Saldo * $row['interes']/100, 2);
						
						$vigv = round($interes * 0.18, 2);
					}
					else
					{
						$interes = 0;
						
						$vigv = 0;
					}


					$Amortizacion = $cuotames - $vigv - $interes;


					//if($rowD["nrocuota"] == $nrocuota)
//					{
//						$importe		 = $Saldo + $vigv;
//					}
//					else
//					{
//						$importe		 = $Amortizacion + $vigv;
//					}
					
					$importe = $Amortizacion + $interes + $vigv;

				}


				$this->Row(array($rowD["nrocuota"],
								 $meses[$rowD["mes"]]."-".$rowD["anio"],
								 number_format($Saldo, 2),
								 number_format($Amortizacion,2),
								 number_format($interes,2),
								 number_format($vigv, 2),
								 number_format($importe,2)
								));
				//

				///
				 $Saldo  -=  $Amortizacion;
				 $tinteres += $interes;
				 $tigv += $vigv;
				 $totalcuotas += $importe;
			}
			$this->Cell(90,$h,"TOTALES =>",1,0,'C');
			$this->Cell(25,$h,number_format($row[5] - $tinteres - $tigv,2),1,0,'R');
			$this->Cell(25,$h,number_format($tinteres,2),1,0,'R');
			$this->Cell(25,$h,number_format($tigv,2),1,0,'R');
			$this->Cell(25,$h,number_format($totalcuotas,2),1,1,'R');
			$this->Ln(8);
			$this->SetFont('Arial','',11);
			$text=utf8_decode("El CLIENTE incurrirá en mora al sólo incumpliento de una de las cuotas estipuladas en la clausula anterior, sin necesidad de requerimiento judicial o extrajudicial alguno, pudiendo la EMPRESA proceder a su cobro por la vía judicial.");
			$this->MultiCell(0,$h,(($text)),0,'J');
			$this->SetFont('Arial','',10);
			$this->Ln(30);
			$this->SetX(40);
			$this->Cell(50,0.1,"",1,0,'C');
			$this->SetX(125);
			$this->Cell(60,0.1,"",1,1,'C');
			$this->SetX(40);
			$this->Cell(50,$h,$NombreEmpresa,0,0,'C');
			$this->SetX(130);
			$this->Cell(50,$h,"Sr.(a): ".strtoupper($row[0]),0,0,'C');
			$this->SetX(40);



		}
    }

	$nrorefinanciamiento	= $_GET["id"];
	$codsuc		= $_GET["codsuc"];
	$NombreEmpresa = $_SESSION['NombreEmpresa'];
    $objReporte	=	new clsCreditos();
	$objReporte->AliasNbPages();
    $objReporte->AddPage();
	$objReporte->Contenido($nrorefinanciamiento,$codsuc);
	$objReporte->Output();


?>
