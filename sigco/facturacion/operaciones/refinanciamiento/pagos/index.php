<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../objetos/clsDrop.php");

$Op = $_POST["Op"];
$nrorefinanciamiento = isset($_POST["Id"]) ? $_POST["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=" . $Op;
$formapago = 1;
$fecha = date("d/m/Y");

$objMantenimiento = new clsDrop();
$sininteres = 0;
$paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);

$sql = "SELECT UPPER(clie.propietario) as propietario,cab.fechaemision,cab.totalrefinanciado,";
$sql .= "''as concepto,cab.creador,UPPER(doc.abreviado),clie.nrodocumento,";
$sql .= "cab.glosa,clie.nroinscripcion, UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
        zo.descripcion as zona,cab.nroprepago,clie.codantiguo,cab.nroprepagoinicial ";
$sql .= "FROM facturacion.cabrefinanciamiento as cab ";
$sql .= "INNER JOIN catastro.clientes as clie on(cab.codemp=clie.codemp and ";
$sql .= "cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion) ";
$sql .= "INNER JOIN public.tipodocumento as doc on(clie.codtipodocumento=doc.codtipodocumento)
        INNER JOIN public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
        INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
        INNER JOIN admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)";
$sql .= "WHERE cab.codemp=1 and cab.codsuc=? and cab.nrorefinanciamiento=?";

$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc, $nrorefinanciamiento));
$row = $consulta->fetch();


$TasaInteres = $row['interes'] / 100;
?>

<style type="text/css">
    .CodConcepto { display: none;}
</style>
<script>
    var cont = 0;
    var object = "";
    var codsuc = <?=$codsuc ?>;
    var nrofact = 0;

    var nitem = 0;
    var TasaInteres = <?=$TasaInteres ?>;
    var TasaInteresO = <?=$TasaInteres ?>;
    var urldir = "<?php echo $_SESSION['urldir']; ?>";
    function ValidarForm(Op)
    {
        if ($("#nroinscripcion").val() == "")
        {
            $("#tabs").tabs({SELECTed: 0});
            alert("Ingrese el nro. de inscripcion del usuario")
            return false
        }
        if ($("#glosarefinanciamiento").val() == "")
        {
            /*$("#tabs").tabs({SELECTed: 0});
             alert("La glosa del Refinanciamiento no puede ser NULO")
             $("#glosarefinanciamiento").focus();
             return false*/
            $("#glosarefinanciamiento").val('REFINANCIAMIENTO')
        }

        if ($("#nrocuotas").val() == 0 || $("#nrocuotas").val() == "")
        {
            $("#tabs").tabs({SELECTed: 2});
            alert("No hay Ningun Refinanciamiento Generado...Genere como minima una para poder continuar")
            return false
        }
        var nrocuotas = $("#nrocuotas").val();
        if (parseFloat($("#cuotainicial").val()) > 0)
            nrocuotas++
        $("#nrocuotas").val(nrocuotas)
        ///CALCULAR IMPUT DE COLATERALES
        var id = parseInt($("#TbIndex tbody tr").length)
        var tr = '';
        var CodConcepto, SubTotal, IGV, Redondeo, Colateral = '';
        for (var i = 1; i <= id; i++)
        {

            CodConcepto = $("#TbIndex tbody tr#" + i + " label.CodConcepto").text()
            SubTotal = $("#TbIndex tbody tr#" + i + " label.SubTotal").text()
            IGV = $("#TbIndex tbody tr#" + i + " label.IGV").text()
            Redondeo = $("#TbIndex tbody tr#" + i + " label.Redondeo").text()
            Importe = $("#TbIndex tbody tr#" + i + " label.Importe").text()
            Colateral = $("#TbIndex tbody tr#" + i + " label.Colateral").text()
            SubTotal = str_replace(SubTotal, ',', '');
            IGV = str_replace(IGV, ',', '');
            Redondeo = str_replace(Redondeo, ',', '');
            Importe = str_replace(Importe, ',', '');
            tr += '<input type="hidden" name="itemC' + i + '"  value="' + i + '"/>';
            tr += '<input type="hidden" name="codconceptoC' + i + '"  value="' + CodConcepto + '"/>';
            tr += '<input type="hidden" name="subtotalC' + i + '"  value="' + SubTotal + '"/>';
            tr += '<input type="hidden" name="igvC' + i + '"  value="' + IGV + '"/>';
            tr += '<input type="hidden" name="redondeoC' + i + '"  value="' + Redondeo + '"/>';
            tr += '<input type="hidden" name="imptotalC' + i + '"  value="' + Importe + '"/>';
            tr += '<input type="hidden" name="Colateral' + i + '"  value="' + Colateral + '"/>';
        }
        tr += '<input type="hidden" name="NroItems" id="Items"  value="' + id + '"/>';
        $("#DivSave").html(tr)

        ///CALCULAR IMPUT DE COLATERALES        
        //enviar solo fcturaciones chekeadas
        $('#tbfacturacion tbody input.input-chk').each(function ()
        {
            if (!$(this).attr("checked"))
            {

                $(this).siblings('.input-facturacion').attr("name", '')
            }
        });


        GuardarP(Op)
        //$("#Nuevo").dialog("close");
    }
    function agregar_detalle()
    {
        ncuota = Trim($("#nrocuota").val())
        var totrefin = 0;
        var ultcuota = 0;
        var diferencia = 0;
        var impmesx = 0;
        var cuotas = ncuota
        var nrofacturacion = nrofact;

        if (ncuota == "" || ncuota == 0)
        {
            alert("El Nro. de la Cuota no es Valido!!!")
            document.getElementById("nrocuota").focus();
            return
        }

        QuitaRow();

        nitem = $("#nrocuota").val()
        var cmes = $("#cuotames").val()

        var cuotaini = $("#cuotaini").val()
        var tmp_cuotaini = cuotaini;
        var cuotames = cmes



        /*if(cuotaini>0) {
         ncuota++;			
         }
         for(i=1;i<=ncuota;i++)
         {
         if(tmp_cuotaini>0){
         //totrefin = parseFloat(totrefin) + parseFloat(cuotaini)
         insertar_cuotas(nrofacturacion,cuotaini,i,'Facturado')
         tmp_cuotaini = 0;
         }
         else{
         totrefin = parseFloat(totrefin) + parseFloat(cmes)
         insertar_cuotas(nrofacturacion,cmes,i,'Pendiente')		
         }
         nrofacturacion++;
         }
         
         totrefin 	= Round(totrefin,2)
         totsaldo	= Round($("#totalafinanciar").val(),2)
         
         
         if(totrefin!=totsaldo)
         {
         diferencia 	= Round(parseFloat(totsaldo) - parseFloat(totrefin),2)
         
         impmesx		= parseFloat($("#importemes"+ncuota).val());								 
         ultcuota	= Round(impmesx + diferencia,2)
         
         $("#importemes"+ncuota).val(ultcuota)
         $("#impmes"+ncuota).html(parseFloat(ultcuota).toFixed(2))
         }
         */
        var afecto_igv = $("#afecto_igv").val()
        var sumasubtotal = 0;
        var saldo = parseFloat($("#totrefinanciamiento").val())
        var inicio = 0;
        var count = 0;
        if (cuotaini > 0)
        {
            nrofacturacion = parseFloat(nrofacturacion) - 1;//SI TIENE CUOTA INICIAL RESTAR UNA FACTURACION
            if (afecto_igv == 1)//PRIMERA CUOTA IRA SIN IGV
                vigv = 0	//vigv 	= Round(CalculaIgv(cuotainicial,igv),2);
            else
                vigv = 0
            imp = parseFloat(cuotaini) + parseFloat(vigv)
            redondeo = CalcularRedondeo(imp)
            imp = parseFloat(imp) + parseFloat(redondeo)
            cuotas = parseFloat(cuotas) + 1;

            sumasubtotal = parseFloat(sumasubtotal) + parseFloat(cuotaini)

            //insertadetalle(mes,anio,cuotaini,vigv,redondeo,imp,1,nrofacturacion,0,1,cuotas,saldo,0,1)
            insertar_cuotas(nrofacturacion, cuotaini, 1, 'Pendiente', vigv, redondeo, imp, saldo, cuotaini, 0)
            //mes				= parseFloat(mes)+1;
            nrofacturacion = parseFloat(nrofacturacion) + 1;
            inicio = 1;
            count = 1;

            /*if(mes>12)
             {
             mes=1;
             anio=parseFloat(anio) + 1;
             }*/
        }
        saldo = saldo - cuotaini;
        sumasubtotal = 0

        for (var i = 1; i <= ncuota; i++)
        {
            var interes = saldo * TasaInteres;
            interes = parseFloat(interes).toFixed(1)
            inicio = parseFloat(inicio) + 1;
            if (afecto_igv == 1)
                vigv = CalculaIgv(cuotames, igv);
            else
                vigv = 0
            cuota = cuotames - vigv - parseFloat(interes)
            if (i == ncuota)
            {
                imp = parseFloat(saldo, 2).toFixed(2) + parseFloat(vigv)
            }
            else
            {
                imp = parseFloat(cuota) + parseFloat(vigv)
            }

            redondeo = CalcularRedondeo(imp)
            imp = parseFloat(imp) + parseFloat(redondeo) + parseFloat(interes)

            sumasubtotal = parseFloat(sumasubtotal) + imp//parseFloat($("#cuotamensual").val())

            //insertadetalle(mes,anio,cuota,vigv,redondeo,imp,0,nrofacturacion,0,inicio,cuotas,saldo,interes,0)
            //insertar_cuotas(nrofacturacion,,i,'Facturado',vigv,redondeo,imp,saldo,cuotaini,0)



            insertar_cuotas(nrofacturacion, imp, inicio, 'Pendiente', vigv, redondeo, imp, saldo, cuota, interes)
            saldo -= cuota;
            count = parseFloat(count) + 1;
            //mes 			= parseFloat(mes) + 1;
            nrofacturacion = parseFloat(nrofacturacion) + 1;
            /*if(mes>12)
             {
             mes=1;
             anio=parseFloat(anio) + 1;
             }*/
        }

        //$("#count").val(count)


        ///////////////////////77
        $("#cuotamensual").val(parseFloat(cmes).toFixed(2))
        $("#nrocuotas").val(ncuota)
        //$("#cuotainicial").attr('readonly','readonly');

        guardarcuotas();
        //close_frmpop();
    }

    function cleartbRefinanciamiento() {
        var nro = $("#NroConceptos").val();
        if (nro > 0) {
            QuitaRow();
            clearcuotas();
            $("#cuotamensual").val('0.00')
            $("#cuotainicial").val('0.00').removeAttr('readonly');
            $("#nrocuotas").val('')
        }

    }
    var totalrecibos = 0;
    function check_deuda(obj)
    {

        var tr = $(obj).parents("tr:eq(0)");
        var importe = parseFloat(tr.find("input.input-deuda").val()).toFixed(2);
        var val = parseFloat($("#PagoAdelantado").val());
        var newval;
        $(".input-paso").val(0)
        if (obj.checked)
        {
            newval = /* val +*/ parseFloat(importe);
            tr.find("input.input-paso").val(1)
        }
        else
        {
            newval = /*val -*/ importe;

            tr.find("input.input-paso").val(0)
        }
        $("#PagoAdelantado").val(newval);
        $("#LblPagoAdelantado").html(newval.toFixed(2));
    }
    function ImprimirC(nropago, femision)
    {

        if (nropago != 0)
        {
            var url = '../../../cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php'
            url += '?codpago=' + nropago + '&nroinscripcion=<?=$row['nroinscripcion'] ?>&fechapago=' + femision + '&codsuc=<?=$codsuc ?>';
            var ventana = window.open(url, 'Buscar', 'width=800, height=600, resizable=yes, scrollbars=yes, status=yes,location=yes');
            ventana.focus();
        }
    }
</script>
<script src="js_refinanciamiento.js" type="text/javascript"></script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos de Refinanciamiento</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Colaterales</a></li>
                                <li style="font-size:10px"><a href="#tabs-3">Cronograma</a></li>

                            </ul>
                            <div id="tabs-1" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td width="80" >Usuario</td>
                                        <td width="17" align="center">:</td>
                                        <td colspan="7" >
                                            <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" readonly="readonly" value="<?php echo $row["codantiguo"] ?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:75px;"/>
                                            <input type="hidden" name="nroinscripcion" readonly="readonly" id="nroinscripcion" size="15" maxlength="15" class="inputtext" value="<?php echo $row["nroinscripcion"] ?>" />

                                            <input type="text" name="propietario" id="propietario" class="inputtext" size="60" maxlength="60" readonly="readonly" value="<?php echo $row["propietario"] ?>"/>
                                            
                                                <input type="hidden" name="anio" id="anio" />
                                                <input type="hidden" name="mes" id="mes" />
                                                <input type="hidden" name="ciclo" id="ciclo" value="0" />
                                                <input type="hidden" name="NroConceptos" id="NroConceptos" value="0" />
                                                <input type="hidden" id="nrorefinanciamiento" name="nrorefinanciamiento"  value="<?=$nrorefinanciamiento ?>"/>
                                                <input type="hidden" id="codconcepto" name="codconcepto"  value="992"/>
                                                <input type="hidden" id="Colateral" name="Colateral"  value="PAGO DE CONVENIO"/>
                                                <div id="DivSave"></div>
                                            </td>
                                    </tr>	
                                    <tr>
                                        <td valign="top">Glosa</td>
                                        <td align="center" valign="top">:</td>
                                        <td colspan="7" >
                                            <textarea name="glosarefinanciamiento" id="glosarefinanciamiento" cols="100" rows="3" class="inputtext" style="font-size:11px"><?php echo $row["glosa"] ?></textarea>
                                        </td>
                                    </tr>	

                                    <tr>
                                        <td colspan="9" class="TitDetalle">
                                            <input type="hidden" id="in_tipodeuda" value="" />
                                            <div style="height:220px; width:100%; overflow:scroll" id="div_facturacion">
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbfacturacion" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="60" >Item</th>
                                                            <th >Facturacion</th>
                                                            <th >Tipo Recibo</th>
                                                            <th width="90" >Nro. Doc.</th>
                                                            <th width="80" >Monto</th>
                                                            <th width="80" >Emisión</th>
                                                            <th width="80" >Vencimiento</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $Sql = "SELECT f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg 
                                                            FROM facturacion.cabfacturacion f
                                                            INNER JOIN facturacion.origen_refinanciamiento o ON (f.codemp = o.codemp)
                                                            AND (f.codsuc = o.codsuc)
                                                            AND (f.codciclo = o.codciclo)
                                                            AND (f.nrofacturacion = o.nrofacturacion)
                                                            AND (f.nroinscripcion = o.nroinscripcion)
                                                            WHERE o.codemp=1 and o.codsuc=? and o.nrorefinanciamiento=?
                                                            GROUP BY f.serie,f.nrodocumento, f.nrofacturacion, f.fechareg
                                                            ORDER BY  f.nrofacturacion";

                                                        $consulta2 = $conexion->prepare($Sql);
                                                        $consulta2->execute(array($codsuc, $nrorefinanciamiento));
                                                        $itemD = $consulta2->fetchAll();

                                                        foreach ($itemD as $rowD) {
                                                            $i++;
                                                            $Sql = "SELECT sum(importe - (importerebajado)) FROM facturacion.detfacturacion
								WHERE codemp=1 and codsuc=? and nroinscripcion=? AND nrofacturacion=?";

                                                            $result = $conexion->prepare($Sql);
                                                            $result->execute(array($codsuc, $row['nroinscripcion'], $rowD['nrofacturacion']));
                                                            $row2 = $result->fetch();
                                                            $TotalRef+= $row2[0];
                                                            $recibon = str_pad($rowD['serie'], 3, "0", STR_PAD_LEFT) . " - " . str_pad($rowD['nrodocumento'], 7, "0", STR_PAD_LEFT);
                                                            ?>
                                                            <tr>
                                                                <td align="center"><?=$i ?></td>
                                                                <td style="padding-left:5px;"><?=$rowD['nrofacturacion'] ?>-PENSIONES</td>
                                                                <td style="padding-left:5px;">RECIBO</td>
                                                                <td align="center"><?=$recibon ?></td>
                                                                <td align="right" style="padding-right:5px;"><?=number_format($row2[0], 2) ?></td>
                                                                <td align="center"><?=$objMantenimiento->DecFecha($rowD['fechareg']) ?></td>
                                                                <td align="center"><?=$objMantenimiento->DecFecha($rowD['fechareg']) ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-2" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td colspan="2" >
                                                        <div style="height:auto; overflow:auto" align="center" id="DivDetalle">
                                                            <table width="800" border="0" align="center" cellspacing="0" class="ui-widget" id="TbIndex">
                                                                <thead class="ui-widget-header" >
                                                                    <tr >
                                                                        <th width="43" align="center" scope="col">Item</th>
                                                                        <th align="center" scope="col">Colateral</th>
                                                                        <th width="80" align="center" scope="col">Subtotal</th>
                                                                        <th width="80" align="center" scope="col">IGV</th>
                                                                        <th width="80" align="center" scope="col">Redondeo</th>
                                                                        <th width="80" align="center" scope="col">Importe</th>

                                                                    </tr>

                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    $tr = '';

                                                                    $Sql = "SELECT d.codconcepto,  c.descripcion,  d.item,  d.subtotal,  d.igv, d.redondeo,  d.imptotal
                                                                        FROM  facturacion.detcabrefinanciamiento d
                                                                        INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc)  AND (d.codconcepto = c.codconcepto)
                                                                        WHERE d.codemp=? AND d.codsuc=? AND  d.nrorefinanciamiento=? 
                                                                        ORDER BY  d.item";
                                                                    $consulta = $conexion->prepare($Sql);
                                                                    $consulta->execute(array(1, $codsuc, $nrorefinanciamiento));
                                                                    $itemsD = $consulta->fetchAll();
                                                                    $subtotaltotal = 0;
                                                                    $igvtotal = 0;
                                                                    $redondeototal = 0;
                                                                    $importetotal = 0;
                                                                    foreach ($itemsD as $rowD) {
                                                                        $subtotaltotal +=$rowD['subtotal'];
                                                                        $igvtotal +=$rowD['igv'];
                                                                        $redondeototal +=$rowD['redondeo'];
                                                                        $importetotal +=$rowD['imptotal'];

                                                                        $tr.='<tr id="' . $rowD['item'] . '" onclick="SeleccionaId(this);">';
                                                                        $tr.='<td align="center" ><label class="Item">' . $rowD['item'] . '</label></td>';
                                                                        $tr.='<td align="left"><label class="CodConcepto">' . $rowD['codconcepto'] . '</label>';
                                                                        $tr.='<label class="Colateral">' . $rowD['descripcion'] . '</label></td>';
                                                                        $tr.='<td align="right"><label class="SubTotal">' . number_format($rowD['subtotal'], 2) . '</label></td>';
                                                                        $tr.='<td align="right"><label class="IGV">' . number_format($rowD['igv'], 2) . '</label></td>';
                                                                        $tr.='<td align="right"><label class="Redondeo">' . number_format($rowD['redondeo'], 2) . '</label></td>';
                                                                        $tr.='<td align="right"><label class="Importe">' . number_format($rowD['imptotal'], 2) . '</label></td>';
                                                                        $tr.='<td align="center" ></td></tr>';
                                                                    }

                                                                    echo $tr;
                                                                    ?>

                                                                </tbody>
                                                                <tfoot class="ui-widget-header">

                                                                    <tr>
                                                                        <td colspan="2" align="right" >Total :</td>
                                                                        <td align="right" >
                                                                            <input type="text" id="subtotaltotal" value="<?=number_format($subtotaltotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  /> 
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="igvtotal" value="<?=number_format($igvtotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  /> 
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="redondeototal" value="<?=number_format($redondeototal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  /> 
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="importetotal" value="<?=number_format($importetotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  /> 
                                                                        </td>
                                                                    </tr>
                                                                </tfoot>

                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-3" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">	
                                    <tr>
                                        <td>

                                        </td>
                                        <td align="center">&nbsp;</td>
                                        <td width="90" >&nbsp;</td>
                                        <td width="104" align="right">&nbsp;</td>
                                        <td width="17" align="center">&nbsp;</td>
                                        <td width="90" >&nbsp;</td>
                                        <td width="99" >&nbsp;</td>
                                        <td width="21" >&nbsp;</td>
                                        <td width="274" >&nbsp;</td>
                                    </tr>
                                    <tr style="height:5px">
                                        <td colspan="9" class="TitDetalle">
                                            <div style="height:220px; overflow:scroll" id="div_refinanciamiento">
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbrefinanciamiento" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="70" >Cuota</th>
                                                            <th width="" >Mes</th>
                                                            <th width="" >Año</th> 
                                                            <th width="80" >Vencimiento</th>
                                                            <th width="" >Cuota</th>
                                                            <th width="90" >Estado</th>
                                                            <th width="80" >Fec.Pago</th>
                                                            <th width="60" align="center" scope="col">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sqldetalle = "SELECT d.nrocuota, d.totalcuotas, d.anio, d.mes,
                                                            sum(importe) AS importe, estadocuota, d.fechapago, a.nroprepago 
                                                            FROM facturacion.detrefinanciamiento d
                                                            LEFT JOIN facturacion.cabrefinanciamientosadelantos a ON (d.codemp = a.codemp)
                                                                    AND (d.codsuc = a.codsuc)  AND (d.nrorefinanciamiento = a.nrorefinanciamiento)
                                                                    AND (d.nrocuota = a.nrocuota)
                                                            WHERE d.codemp=1 and d.codsuc=? and d.nrorefinanciamiento=?
                                                            group by d.nrocuota,d.totalcuotas,d.anio,d.mes,d.estadocuota,d.fechapago, a.nroprepago 
                                                            ORDER BY " . $objMantenimiento->Convert('anio', 'INTEGER') . "  ," . $objMantenimiento->Convert('mes', 'INTEGER') . "";

                                                        $consultaD = $conexion->prepare($sqldetalle);
                                                        $consultaD->execute(array($codsuc, $nrorefinanciamiento));
                                                        $itemD = $consultaD->fetchAll();
                                                        $Saldo = 0;
                                                        $TotalDeuda = 0;
                                                        foreach ($itemD as $rowD) {
                                                            $Dias = cal_days_in_month(CAL_GREGORIAN, $rowD["mes"], $rowD["anio"]);
                                                            if (intval($rowD["mes"]) < 10)
                                                                $rowD["mes"] = "0" . $rowD["mes"];
                                                            if (intval($Dias) < 10)
                                                                $Dias = "0" . $Dias;
                                                            $FechaP = "";
                                                            $Acuenta = "";
                                                            $FechaA = "";
                                                            $Saldo+=$rowD["importe"];
                                                            switch ($rowD["estadocuota"]) {
                                                                case '0': $Estado = "<span style='color:red'>PENDIENTE</span>";
                                                                    break;
                                                                case '1': $Estado = "<span style='color:red'>FACTURADO</span>";
                                                                    break;
                                                                case '2': $Estado = "<span style='color:red'>ANULADO</span>";
                                                                    break;
                                                                case '3': $Estado = "<span style='color:green'>CANCELADO</span>";
                                                                    break;
                                                                case '4': $Estado = "<span style='color:yellow'>QUEBRADO</span>";
                                                                    break;
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td align="center"><?=$rowD["nrocuota"] . "/" . $rowD["totalcuotas"] ?>
                                                                    <input type='hidden' value='<?=$rowD["importe"] ?>' class='input-deuda' /></td>

                                                                <td style="padding-left:5px;"><?=$meses[intval($rowD["mes"])] ?></td>
                                                                <td align="center"><?=$rowD["anio"] ?></td>
                                                                <td align="center"><?=$Dias . "/" . $rowD["mes"] . "/" . $rowD["anio"] ?></td>
                                                                <td align="right" style="padding-right:5px;"><?=number_format($rowD["importe"], 2) ?></td>
                                                                <td align="center"><?=$Estado ?></td>
                                                                <td align="center"><?=$objMantenimiento->DecFecha($rowD['fechapago']) ?></td>
                                                                <th align="left" scope="col" style="padding-left:5px;">
                                                                    <?php
                                                                    switch ($rowD["estadocuota"]) {
                                                                        case '0':
                                                                            //$TotalDeuda+=$rowD["imptotal"];
                                                                            //$TotalDeudaInteres+=$rowD["interes"];
                                                                            //$TotalDeudaIgv+=$rowD["igv"];
                                                                            //$total++;
                                                                            ?>

                                                                            <input type='hidden' value='0' class='input-paso'  name="paso<?=$rowD["nrocuota"] ?>" />
                                                                            <input type='radio' name='check_deudax'  value="<?=$rowD["nrocuota"] ?>" class='input-chk' onclick='check_deuda(this)'>
                                                                            <?php
                                                                            break;
                                                                        default:
                                                                            ?>
                                                                            <input type='hidden' value='0' class='input-paso'  name="paso<?=$rowD["nrocuota"] ?>" />
                                                                        <?php
                                                                    }
                                                                    if ($rowD['nroprepago'] != '') {
                                                                        ?>
                                                                        <span class="MljSoft-icon-print" title="Impimir Nota de Pago" onclick="ImprimirC(<?=$rowD['nroprepago'] ?>, '<?=$rowD['fechapago'] ?>')"></span>
                                                                        <?php
                                                                        }
                                                                        if($rowD['nrocuota']==1 )
                                                                        {
                                                                        ?>
                                                                        <span class="MljSoft-icon-print" title="Impimir Nota de Pago" onclick="ImprimirC(<?=$row['nroprepagoinicial'] ?>,<?=$row['fechaemision'] ?>)"></span>
                                                                        <?php
                                                                        }
                                                                        ?>	

                                                                    </th>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                        <tfoot class="ui-widget-header" >
                                                            <tr>
                                                                <td colspan="4">&nbsp;</td>
                                                                <td> Total</td>

                                                                <td align="right"><label id="LblPagoAdelantado"><?=number_format($TotalDeuda, 2) ?></label>
                                                                    <input id="PagoAdelantado" name="imptotal" value="<?=$TotalDeuda ?>" type="hidden">
                                                                    <input name="TotalCuotasP" value="<?=$total ?>" type="hidden">
                                                            </td>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                                
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>


            </tbody>
        </table>
    </form>
</div>