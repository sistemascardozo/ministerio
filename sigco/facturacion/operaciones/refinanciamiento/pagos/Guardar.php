<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	

	$Op  	= $_GET["op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp			= 1;
	$codsuc			= $_SESSION['IdSucursal'];
	$nroinscripcion	= $_POST["nroinscripcion"];
	$imptotal		= $_POST["imptotal"];
	$igv			= $_POST["igv"];
	$redondeo		= $_POST["redondeo"];
	$subtotal		= $_POST["subtotal"];
	$codconcepto	= $_POST['codconceptoC1'];//0;//$_POST["codconcepto"];
	$nropresupuesto	= $_POST["codexpediente"]?$_POST["codexpediente"]:0;
	$observacion	= $_POST["observacion"];
	$codusu			= $_SESSION['id_user'];
	$femision		= $objFunciones->CodFecha($_POST["femision"]); 
	$count			= $_POST["count"];
	$interes		= $_POST["interescredito"]?$_POST["interescredito"]:0;
	$nroprepago			= $_POST["nprepago"]?$_POST["nprepago"]:0;
	$sininteres			= $_POST["sininteres"]?$_POST["sininteres"]:0;
	$nrorefinanciamiento = $_POST['nrorefinanciamiento'];
	//if($_POST['cuotainicialflag']==1)
	//	$nropago = $objFunciones->setCorrelativosVarios(5, $codsuc, "SELECT", 0);
	$nropago =0;
	$nroprepagocre = $objFunciones->setCorrelativosVarios(6,$codsuc,"SELECT",0);
	$creoprepago=false;
	$cuotacancelada = $_POST['check_deudax'];
	$conexion->beginTransaction();
	
	//OBTENER EL INTERRES DE LA CUOTA
	$sql  = "SELECT UPPER(clie.propietario), cab.fechaemision, cab.totalrefinanciado, ";
	$sql .= " '0.00' AS igv, '0.00' AS redondeo, cab.totalrefinanciado AS imptotal,";
	$sql .= " cab.glosa, cab.creador, UPPER(doc.abreviado), clie.nrodocumento,";
	$sql .= " UPPER(cab.glosa), '', clie.nroinscripcion, ";
	$sql .= " UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) AS direccion, ";
	$sql .= " zo.descripcion AS zona, cab.nroprepago, cab.nroprepagoinicial, cab.sininteres, cab.interes, cab.cuotames ";
	$sql .= "FROM facturacion.cabrefinanciamiento cab ";
	$sql .= " INNER JOIN catastro.clientes clie ON(cab.codemp = clie.codemp AND cab.codsuc = clie.codsuc AND cab.nroinscripcion = clie.nroinscripcion) ";
	$sql .= " INNER JOIN public.tipodocumento doc ON(clie.codtipodocumento = doc.codtipodocumento) ";
	$sql .= " INNER JOIN public.calles cal ON(clie.codemp = cal.codemp AND clie.codsuc = cal.codsuc AND clie.codcalle = cal.codcalle AND clie.codzona = cal.codzona) ";
	$sql .= " INNER JOIN public.tiposcalle tipcal ON(cal.codtipocalle = tipcal.codtipocalle) ";
	$sql .= " INNER JOIN admin.zonas zo ON(clie.codemp = zo.codemp AND clie.codsuc = zo.codsuc AND clie.codzona = zo.codzona)";
	$sql .= "WHERE cab.codemp = 1 AND cab.codsuc = ? AND cab.nrorefinanciamiento = ?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nrorefinanciamiento));
	$row = $consulta->fetch();
	
	$TotalRef=0;
	//COLATERAL
	$Sql = "SELECT SUM(imptotal) ";
	$Sql .= "FROM facturacion.detcabrefinanciamiento ";
	$Sql .= "WHERE codemp = 1 ";
	$Sql .= " AND codsuc = ? ";
	$Sql .= " AND nrorefinanciamiento = ?";
	
	$consulta2 = $conexion->prepare($Sql);
	$consulta2->execute(array($codsuc,$nrorefinanciamiento));
	$itemD = $consulta2->fetchAll();
	
	foreach($itemD as $rowD)
	{
		if($rowD[0]!='')
		{
			$TotalRef+= $rowD[0];
		}
	}
	
	//RECIBOS
	$Sql = "SELECT f.serie, f.nrodocumento, f.nrofacturacion, f.fechareg ";
	$Sql .= "FROM facturacion.cabfacturacion f ";
	$Sql .= " INNER JOIN facturacion.origen_refinanciamiento o ON (f.codemp = o.codemp) AND (f.codsuc = o.codsuc) AND (f.codciclo = o.codciclo) ";
	$Sql .= "  AND (f.nrofacturacion = o.nrofacturacion) AND (f.nroinscripcion = o.nroinscripcion) ";
	$Sql .= "WHERE o.codemp = 1 ";
	$Sql .= " AND o.codsuc = ? ";
	$Sql .= " AND o.nrorefinanciamiento = ? ";
	$Sql .= "GROUP BY f.serie, f.nrodocumento, f.nrofacturacion, f.fechareg ";
	
	$consulta2 = $conexion->prepare($Sql);
	$consulta2->execute(array($codsuc,$nrorefinanciamiento));
	$itemD = $consulta2->fetchAll();
	
	foreach($itemD as $rowD)
	{
		$i++;
		
		$Sql = "SELECT SUM(importe - (importerebajado)) ";
		$Sql .= "FROM facturacion.detfacturacion ";
		$Sql .= "WHERE codemp = 1 ";
		$Sql .= " AND codsuc = ? ";
		$Sql .= " AND nroinscripcion=? AND nrofacturacion=?";
			
		$result = $conexion->prepare($Sql);
		$result->execute(array($codsuc,$row['nroinscripcion'],$rowD['nrofacturacion']));
		$row2 = $result->fetch();
		
		$TotalRef += $row2[0];
	}

	$sqldetalle  = "SELECT nrocuota, totalcuotas, '' AS fechavencimiento, ";
	$sqldetalle .= " SUM(importe) AS imptotal, '0.00' as igv, '0.00' AS redondeo, SUM(importe),";
	$sqldetalle .= " 0 as tipocuota, nrofacturacion, mes, anio, '0.00' AS interes ";
	$sqldetalle .= "FROM facturacion.detrefinanciamiento ";
	$sqldetalle .= "WHERE codemp = 1 ";
	$sqldetalle .= " AND codsuc = ? ";
	$sqldetalle .= " AND nrorefinanciamiento = ? ";
	$sqldetalle .= "GROUP BY nrocuota, totalcuotas, anio, mes, estadocuota, nrofacturacion ";
	$sqldetalle .= "ORDER BY nrocuota";
	
	$consultaD = $conexion->prepare($sqldetalle);
	$consultaD->execute(array($codsuc, $nrorefinanciamiento));
	$itemD = $consultaD->fetchAll();
	
	$Saldo =  $TotalRef;
	$cuotames = $row['cuotames'];
	$vigv = 0;
	$ai = array();
	
	foreach($itemD as $rowD)
	{
		
		if($row['nroprepagoinicial'] != 0 && $rowD["nrocuota"] == 1) //CON INICIAL
		{
			$Amortizacion = $rowD["imptotal"];
			$interes = 0;
			$vigv = 0;
			$importe = $rowD["imptotal"];
		}
		else
		{
			
			if($row['sininteres'] == 0)
			{
				$interes =  round($Saldo * $row['interes']/100,1);
				
				$vigv = round($interes * 0.18, 2);
			}
			else
			{
				$interes = 0;
				
				$vigv = 0;
			}

		

			$Amortizacion = $cuotames - $vigv - $interes;
			

//			if($rowD["nrocuota"] == $nrocuota)
//			{
//				$importe		 = $Saldo + $vigv;
//			}
//			else
//			{
				$importe		 = $Amortizacion + $vigv + $interes;
			//}
			
			$importe = $Amortizacion + $interes + $vigv;

		}
		
		$ai[$rowD["nrocuota"]] = $interes;
		
		$Saldo  -=  $Amortizacion;
		$tinteres += $interes;
		$tigv += $vigv;
		$totalcuotas += $importe;
	}
	//OBTENER EL INTERRES DE LA CUOTA

		//INSERTAR UN PREPAGO POR EL MONTO TOTAL
		$sql = "SELECT c.codcliente, c.propietario, c.codtipodocumento, c.nrodocumento, c.codciclo, tc.descripcioncorta, ";
		$sql .= " cl.descripcion AS calle, c.nrocalle, c.codciclo, td.abreviado, c.codsector, c.codcalle ";
		$sql .= "FROM catastro.clientes c ";
		$sql .= " INNER JOIN public.calles cl ON(c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona) ";
		$sql .= " INNER JOIN public.tiposcalle tc ON(cl.codtipocalle = tc.codtipocalle) ";
		$sql .= " INNER JOIN public.tipodocumento td ON(c.codtipodocumento = td.codtipodocumento) ";
		$sql .= "WHERE c.codemp = 1 ";
		$sql .= " AND c.codsuc = ? ";
		$sql .= " AND c.nroinscripcion = ? ";

        $result = $conexion->prepare($sql);
        $result->execute(array($codsuc, $nroinscripcion));
		
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error SELECT Catastro";
            $data['res']    =2;
            die(json_encode($data)) ;
        }
		
        $item = $result->fetch();
		
        $propietario = strtoupper($item["propietario"]);
        $direccion = strtoupper($item["descripcioncorta"] . " " . $item["calle"] . " " . $item["nrocalle"]);
        $docidentidad = strtoupper($item["nrodocumento"]);

		$creoprepago = true;
		$condpago = isset($_POST["condpago"])?$_POST["condpago"]:0;
	    $documento = 16;//nota de pago
	    $formapago = 1;
	    $fechareg = date('Y-m-d');
	    $default = array('6' =>9);//CAR DEFECTO
        $car = $_POST["carx"];
		
        if($car=='') $car = $default[$codsuc];
		
		$nrocaja = $_SESSION["nocaja"];
	    $idusuario = $_SESSION['id_user'];
		$hora = $objFunciones->HoraServidor();
		$eventualtext = 0;
	    $nrofacturacion = 0;
	    $npresupuesto = 0;
		
		$SubTT = $_POST['imptotal'] - $tigv;
		
		
		$glosa = 'PAGO ADELANTADO DE CUOTA '.$cuotacancelada;//$_POST["glosa"];
		
		$sqlC = "INSERT INTO cobranza.cabprepagos(codemp, codsuc, nroprepago, nroinscripcion, car, codformapago, creador, fechareg, ";
		$sqlC .= " subtotal, hora, codciclo, condpago, igv, redondeo, imptotal, ";
		$sqlC .= " eventual, propietario, direccion, documento, glosa, nroinspeccion, estado, nrocredito, origen) ";
		$sqlC .= "VALUES(".$codemp.", ".$codsuc.", ".$nroprepagocre.", ".$nroinscripcion.", ".$car.", ".$formapago.", ".$idusuario.", '".$fechareg."', ";
		$sqlC .= " '".floatval(str_replace(",", "", $SubTT))."', '".$hora."', 1, ".$condpago.", ".$tigv.", 0, '".str_replace(",", "", $_POST['imptotal'])."', ";
		$sqlC .= " ".$eventualtext.", '".$propietario."', '".$direccion."', '".$docidentidad."', '".$glosa."', 0, 1, ".$nrorefinanciamiento.", 4)";
				
		$resultC = $conexion->prepare($sqlC);
		$resultC->execute(array());
		
		if($resultC->errorCode()!='00000')
		{
		    $conexion->rollBack();
		    $mensaje = "Error update cabpagos";
		    $data['res'] = 2;
			
			echo $sqlC;
			
		    die(json_encode($data)) ;
		}
		
		$DocSeries	= $objFunciones->GetSeries($codsuc,$documento);
		$seriedoc	= $DocSeries["serie"];
		$numerodoc	= $DocSeries["correlativo"];
		$fechas		= $objFunciones->DecFechaLiteral();
	    $anio		= $fechas["anio"];
	    $mes		= $fechas["mes_num"];
		
		$Itemm = 1;
		
		$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, codconcepto, ";
		$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, ";
		$sqlD .= " nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto, item) ";
		$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$_POST['codconcepto'].", ";
		$sqlD .= " '".str_replace(",", "", $SubTT)."', '".$_POST['Colateral']."', 0, ".$nrofacturacion.", ".$documento.", ";
		$sqlD .= " ".$numerodoc.", '".$seriedoc."', 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.", ".$Itemm.")";
		
		//$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL
	    $facturacion = 0;
		$resultD = $conexion->prepare($sqlD);
		$resultD->execute(array());



		if($resultD->errorCode()!='00000')
		{
		    $conexion->rollBack();
		    $mensaje = "Error update cabpagos";
		    $data['res']    =2;
			
			echo $sqlD;
			
		    die(json_encode($data)) ;
		}
		
		if ($tigv > 0)
		{
			$codconcepto = 5; //IGV
			
			$Itemm++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$Itemm.", ".$_POST['codconcepto'].", ";
			$sqlD .= " '".str_replace(",", "", $tigv)."', 'I.G.V.', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		
		//DETALLE DE INTERES
		/*$sqlD = "insert into cobranza.detprepagos(codemp,codsuc,nroinscripcion,nroprepago,codconcepto,importe,detalle,tipopago,
		    nrofacturacion,coddocumento,nrodocumento,serie,codtipodeuda,anio,mes,categoria,codcategoria,nropresupuesto) 
		    values(:codemp,:codsuc,:nroinscripcion,:nroprepago,:codconcepto,:importe,:detalle,:tipopago,
		    :nrofacturacion,:coddocumento,:nrodocumento,:serie,:codtipodeuda,:anio,:mes,:categoria,:codcategoria,:nropresupuesto)";
		//$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL
		$codconcepto=9;//CONCEPTO DE INTERES
		$resultD = $conexion->prepare($sqlD);
		$resultD->execute(array(":codemp"=>$codemp,
		                        ":codsuc"=>$codsuc,
		                        ":nroinscripcion"=>$nroinscripcion,
		                        ":nroprepago"=>$nroprepagocre,
		                        ":codconcepto"=>$codconcepto,
		                        ":importe"=>$ai[$cuotacancelada],
		                        ":detalle"=>'INTERESES',
		                        ":tipopago"=>0,
		                        ":nrofacturacion"=>$nrofacturacion,
		                        ":coddocumento"=>$documento,//4
		                        ":nrodocumento"=>$numerodoc,//0
		                        ":serie"=>$seriedoc,//001
		                        ":codtipodeuda"=>5,//COBRANZA DIRECTAMENTE EN CAJA
		                        ":anio"=>$anio,
		                        ":mes"=>$mes,
		                        ":categoria"=>3,
		                        ":codcategoria"=>4,
		                        ":nropresupuesto" => $npresupuesto));



		if($resultD->errorCode()!='00000')
		{
		    $conexion->rollBack();
		    $mensaje = "Error update cabpagos";
		    $data['res']    =2;
		    die(json_encode($data)) ;
		}*/
		//DETALLE DE INTERES
		//ACTUALIZAR CORRELATIVO
		 $sql = "UPDATE  reglasnegocio.correlativos  
		    SET  correlativo = correlativo+1
		    WHERE codemp=1 AND  codsuc=" . $codsuc . " AND nrocorrelativo=".$documento;
		    $result = $conexion->query($sql);
		    if (!$result) 
		    {
		        $conexion->rollBack();
		        $mensaje = "619 Error UPDATE correlativos";
		        $data['res']    =2;
		        $data['mensaje']    =$mensaje;
		        die(json_encode($data));
		    }
		
	

	//CUOTAS CANCELADAS
	    $sqldetalle  = "select nrocuota,anio,mes,sum(importe) as imptotal, estadocuota ";
		$sqldetalle .= "from facturacion.detrefinanciamiento ";
		$sqldetalle .= "where codemp=1 and codsuc=? and nrorefinanciamiento=?
						group by nrocuota,anio,mes,estadocuota
						order by nrocuota";
		$consultaD = $conexion->prepare($sqldetalle);
		$consultaD->execute(array($codsuc,$nrorefinanciamiento));
		$itemD = $consultaD->fetchAll();
		foreach($itemD as $rowD)
		{
			if($rowD["estadocuota"]==0)
			{

				if($_POST['paso'.$rowD['nrocuota']]==1)	
				{
					//OBTENER PREPAGO SI YA SE GENERO Y ANULARLO
					$Sql="SELECT * FROM facturacion.cabrefinanciamientosadelantos 
							WHERE  codemp=:codemp AND codsuc=:codsuc AND nrorefinanciamiento=:nrorefinanciamiento and nrocuota=:nrocuota";
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrorefinanciamiento"=>$nrorefinanciamiento,
					                        ":nrocuota"=>$rowD['nrocuota']
					                        
					                        ));	
					$rowX = $resultD->fetch();	
					if($rowX[0]!='')
					{
						$Sql="update cobranza.cabprepagos set estado=0 
						     WHERE codemp=1 AND  codsuc=".$codsuc." 
						     and nroprepago=".$rowX['nroprepago']." AND nroinscripcion=".$nroinscripcion;
						$conexion->query($Sql);
					}
					//OBTENER PREPAGO SI YA SE GENERO Y ANULARLO
					
					$Sql="DELETE FROM facturacion.cabrefinanciamientosadelantos 
							WHERE  codemp=:codemp AND codsuc=:codsuc AND nrorefinanciamiento=:nrorefinanciamiento and nrocuota=:nrocuota";
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrorefinanciamiento"=>$nrorefinanciamiento,
					                        ":nrocuota"=>$rowD['nrocuota']
					                        
					                        ));	


					
					$Sql="INSERT INTO 
						  facturacion.cabrefinanciamientosadelantos
						(
						  codemp,
						  codsuc,
						  nrorefinanciamiento,
						  nrocuota,
						  imptotal,
						  fechareg,
						  nroprepago
						  
						) 
						VALUES (
						  :codemp,
						  :codsuc,
						  :nrorefinanciamiento,
						  :nrocuota,
						  :imptotal,
						  :fechareg,
						  :nroprepago
						  
						); ";
				
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrorefinanciamiento"=>$nrorefinanciamiento,
					                        ":nrocuota"=>$rowD['nrocuota'],
					                        ":imptotal"=>$rowD['imptotal'],
					                        ":fechareg"=>$fechareg,
					                        ":nroprepago"=>$nroprepagocre
					                        ));



					if($resultD->errorCode()!='00000')
					{
					    $conexion->rollBack();
					    $mensaje = "Error update cabpagos";
					    $data['res']    =2;
					    die(json_encode($data)) ;
					}
					//´poner a facturado la cuota
					/*$sqldetalle  = "UPDATE facturacion.detrefinanciamiento SET estadocuota=1";
						$sqldetalle .= "where codemp=1 and codsuc=? and nrorefinanciamiento=? AND nrocuota=?";
						$consultaD = $conexion->prepare($sqldetalle);
						$consultaD->execute(array($codsuc,$nrorefinanciamiento,$rowD['nrocuota']));
						$itemD = $consultaD->fetchAll();*/
					//´poner a facturado la cuota

				}
			}
			
		}
	//CUOTAS CANCELADAS
	
	//die();
	if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       $res = 2;
    } else {
        $conexion->commit();
        		$objFunciones->setCorrelativosVarios(6,$codsuc,"UPDATE",$nroprepagocre);
        	
        
        
        	
        $mensaje = "El Registro se ha Grabado Correctamente";
        $res = 1;
    }
	
	$data['res']    =  $res;
	$data['nropago']              = 0;// $nropago;
	$data['nrocredito']           = $nrocredito;
	$data['nroprepagocre']        = $nroprepagocre;
	echo json_encode($data);

?>
