<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsDrop.php");

$Op = $_GET["Op"];
$Id = isset($_GET["Id"]) ? $_GET["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=".$Op;

$fecha = date("d/m/Y");

$objMantenimiento = new clsDrop();
$sininteres = 0;
$paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);
$sql = "SELECT nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
    FROM facturacion.periodofacturacion
    WHERE codemp=1 and codsuc=? and codciclo=1 and facturacion=0";
$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc));
$row = $consulta->fetch();
$TasaInteres = $row['tasapromintant'] / 100;
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>

<style type="text/css">
    .CodConcepto { display: none;}
</style>
<script>
    var cont = 0;
    var object = "";
    var codsuc = <?=$codsuc ?>;
    var nrofact = 0;

    var nitem = 0;
    var TasaInteres = <?=$TasaInteres ?>;
    var TasaInteresO = <?=$TasaInteres ?>;
    var urldir = "<?php echo $_SESSION['urldir']; ?>";

    var igv = <?=$paramae['valor'] ?>;

    function ValidarForm(Op)
    {
        if ($("#nroinscripcion").val() == "")
        {
            $("#tabs").tabs({selected: 0});            
            Msj($("#nroinscripcion"), "Ingrese el nro. de inscripcion del usuario")
            return false
        }
        $("#nroinscripcion").removeClass("ui-state-error");
        
        if ($("#glosarefinanciamiento").val() == "")
        {
            $("#glosarefinanciamiento").val('REFINANCIAMIENTO')
        }

        if ($("#nrocuotas").val() == 0 || $("#nrocuotas").val() == "")
        {
            $("#tabs").tabs({selected: 2});
            //alert("No hay Ningun Refinanciamiento Generado...Genere como minima una para poder continuar")
            Msj($("#nrocuotas"), "No hay Ningun Refinanciamiento Generado...Genere como minima una para poder continuar")
            return false
        }
        $("#nrocuotas").removeClass("ui-state-error");
        
        var nrocuotas = $("#nrocuotas").val();
        if (parseFloat($("#cuotainicial").val()) > 0)
            nrocuotas++
        $("#nrocuotas").val(nrocuotas)
        ///CALCULAR IMPUT DE COLATERALES
        var id = parseInt($("#TbIndex tbody tr").length)
        var tr = '';
        var CodConcepto, SubTotal, IGV, Redondeo, Colateral = '';
        for (var i = 1; i <= id; i++)
        {
            CodConcepto = $("#TbIndex tbody tr#" + i + " label.CodConcepto").text()
            SubTotal = $("#TbIndex tbody tr#" + i + " label.SubTotal").text()
            IGV = $("#TbIndex tbody tr#" + i + " label.IGV").text()
            Redondeo = $("#TbIndex tbody tr#" + i + " label.Redondeo").text()
            Importe = $("#TbIndex tbody tr#" + i + " label.Importe").text()
            Colateral = $("#TbIndex tbody tr#" + i + " label.Colateral").text()
            SubTotal = str_replace(SubTotal, ',', '');
            IGV = str_replace(IGV, ',', '');
            Redondeo = str_replace(Redondeo, ',', '');
            Importe = str_replace(Importe, ',', '');
            tr += '<input type="hidden" name="itemC' + i + '"  value="' + i + '"/>';
            tr += '<input type="hidden" name="codconceptoC' + i + '"  value="' + CodConcepto + '"/>';
            tr += '<input type="hidden" name="subtotalC' + i + '"  value="' + SubTotal + '"/>';
            tr += '<input type="hidden" name="igvC' + i + '"  value="' + IGV + '"/>';
            tr += '<input type="hidden" name="redondeoC' + i + '"  value="' + Redondeo + '"/>';
            tr += '<input type="hidden" name="imptotalC' + i + '"  value="' + Importe + '"/>';
            tr += '<input type="hidden" name="Colateral' + i + '"  value="' + Colateral + '"/>';
        }
        tr += '<input type="hidden" name="NroItems" id="Items"  value="' + id + '"/>';
        $("#DivSave").html(tr)

        ///CALCULAR IMPUT DE COLATERALES
        //enviar solo fcturaciones chekeadas
        $('#tbfacturacion tbody input.input-chk').each(function ()
        {
            if (!$(this).attr("checked"))
            {
                $(this).siblings('.input-facturacion').attr("name", '')
            }
        });

        GuardarP(Op)
        //$("#Nuevo").dialog("close");
    }
    function agregar_detalle()
    {
        ncuota = Trim($("#nrocuota").val())
        var totrefin = 0;
        var ultcuota = 0;
        var diferencia = 0;
        var impmesx = 0;
        var cuotas = ncuota
        var nrofacturacion = nrofact;

        if (ncuota == "" || ncuota == 0)
        {
            alert("El Nro. de la Cuota no es Valido!!!")
            document.getElementById("nrocuota").focus();
            return
        }

        QuitaRow();

        nitem = $("#nrocuota").val()
        var cmes = $("#cuotames").val()

        var cuotaini = $("#cuotaini").val()
        var tmp_cuotaini = cuotaini;
        var cuotames = cmes

        var afecto_igv = $("#afecto_igv").val()
        var sumasubtotal = 0;
        var saldo = parseFloat($("#totrefinanciamiento").val())
        var inicio = 0;
        var count = 0;

        if (cuotaini > 0)
        {
            nrofacturacion = parseFloat(nrofacturacion) - 1;//SI TIENE CUOTA INICIAL RESTAR UNA FACTURACION

            cuota = cuotaini; // 1.18

            igvv = cuotaini - cuota;

            if (afecto_igv == 1)//PRIMERA CUOTA IRA SIN IGV
            {
                vigv = Round(igvv, 2);
            }
            else
            {
                vigv = 0
            }

            imp = parseFloat(cuotaini)
            redondeo = CalcularRedondeo(imp)
            imp = parseFloat(imp) + parseFloat(redondeo)
            cuotas = parseFloat(cuotas) + 1;

            sumasubtotal = parseFloat(sumasubtotal) + parseFloat(cuotaini)

            //insertadetalle(mes,anio,cuotaini,vigv,redondeo,imp,1,nrofacturacion,0,1,cuotas,saldo,0,1)
            insertar_cuotas(nrofacturacion, cuotaini, 1, 'Pendiente', vigv, redondeo, imp, saldo, cuota, 0)
            //mes				= parseFloat(mes)+1;
            nrofacturacion = parseFloat(nrofacturacion) + 1;
            inicio = 1;
            count = 1;

        }
        saldo = saldo - cuotaini;
        sumasubtotal = 0

        for (var i = 1; i <= ncuota; i++)
        {
            var interes = saldo * TasaInteres;
            //alert(interes);
            interes = parseFloat(interes).toFixed(2)
            inicio = parseFloat(inicio) + 1;

            if (afecto_igv == 1)
            {
                vigv = CalculaIgv(interes, igv);
            }
            else
            {
                vigv = 0;
            }

            cuota = cuotames - vigv - parseFloat(interes);

            imp = parseFloat(cuota) + parseFloat(vigv) + parseFloat(interes);
            redondeo = CalcularRedondeo(imp);

            imp = parseFloat(imp) + parseFloat(redondeo);

            sumasubtotal = parseFloat(sumasubtotal) + imp//parseFloat($("#cuotamensual").val())

            insertar_cuotas(nrofacturacion, imp, inicio, 'Pendiente', vigv, redondeo, imp, saldo, cuota, interes)
            saldo -= cuota;
            count = parseFloat(count) + 1;
            //mes 			= parseFloat(mes) + 1;
            nrofacturacion = parseFloat(nrofacturacion) + 1;
            
        }

        ///////////////////////77
        $("#cuotamensual").val(parseFloat(cmes).toFixed(2))
        $("#nrocuotas").val(ncuota)
        //$("#cuotainicial").attr('readonly','readonly');

        guardarcuotas();
        //close_frmpop();
    }

    function agregar_cargos()
    {
        var total_rec = parseFloat($("#total_cargar_cargos").val())
        if (total_rec <= 0.00) {
            alert("No existe cargos que agregar")
            $("#DivCargos").dialog("close")
            return false
        }

        eliminar_cargos_cargados();

        var total_importe = 0
        var stotal_importe = 0
        var subtotal_grid = parseFloat(0)
        var total_grid = 0
        var codconcepto = ''
        var nombre_concepto = ''
        var saldo = parseFloat($("#totrefinanciamiento").val())
        var tr;

        var tabla = parseInt($("#TbIndex tbody tr").length);

        $("#tbcargos tbody tr").each(function (idx, elem) {
            tabla += 1;
            contador = idx;
            codconcepto = $(elem).children('td:eq(0)').children('input').val();
            nombre_concepto = $(elem).children('td:eq(0)').children('input').attr('data');
            total_importe = parseFloat($(elem).children('td:eq(2)').html());

            tr = '';
            tr = '<tr id="' + tabla + '" onclick="SeleccionaId(this);" data="exterior">';
            tr += '<td align="center"><label class="Item">' + tabla + '</label></td>';
            tr += '<td align="left"><label class="CodConcepto">' + codconcepto + '</label><label class="Colateral">' + nombre_concepto + '</label></td>';
            tr += '<td align="right"><label class="SubTotal">' + total_importe + '</label></td>';
            tr += '<td align="right"><label class="IGV">0.00</label></td>';
            tr += '<td align="right"><label class="Redondeo">0.00</label></td>';
            tr += '<td align="right"><label class="Importe">' + total_importe + '</label></td>';
            tr += '<td align="center"><a marked="' + tabla + '" href="javascript:QuitaItemc(' + tabla + ')" class="Del"><span class="icono-icon-trash" title="Quitar Colateral"></span> </a></td>';
            tr += "</tr>";

            $("#TbIndex tbody").append(tr);
            stotal_importe += total_importe;
        });

        contador++;

        $("#total_cargos").val(contador)

        stotal_importe = Round(stotal_importe, 2);

        subtotal_grid = parseFloat($("#subtotaltotal").val());
        subtotal_grid += parseFloat(stotal_importe);
        $("#subtotaltotal").val(subtotal_grid);

        total_grid = parseFloat($("#importetotal").val());
        total_grid += parseFloat(stotal_importe);
        $("#importetotal").val(total_grid);

        saldo += parseFloat(Round(total_grid, 2))
        $("#totrefinanciamiento").val(Round(saldo, 2))

        $("#DivCargos").dialog("close")


    }

    function eliminar_cargos_cargados()
    {
        var exteriror = ''
        var importe = importet = 0;
        var subtotal = parseFloat($("#subtotaltotal").val())
        var total = parseFloat($("#importetotal").val())
        $("#TbIndex tbody tr").each(function (idx, elem) {

            exteriror = $(elem).attr('data')

            if (exteriror == 'exterior') {
                importe = parseFloat($(elem).children("td:eq(2)").children("label").html());
                importet = parseFloat($(elem).children("td:eq(5)").children("label").html());
                $(elem).remove();
                subtotal -= importe;
                total -= importet;
            }

        });

        $("#subtotaltotal").val(Round(subtotal, 2))
        $("#importetotal").val(Round(subtotal, 2))

        // Reinicia ID
        $("#TbIndex tbody tr").each(function (idx, elem) {
            id = idx + 1;
            $(elem).attr("id", id);
            $(elem).children("td:eq(0)").children("label").html(id);
            $(elem).children("td:eq(6)").children("a").attr("marked", id);
            $(elem).children("td:eq(6)").children("a").attr("href", "javascript:QuitaItemc(" + id + ")");
        });

    }


    function cargar_credito()
    {
        var nrocredito = 0;
        var cont = 0;
        $("div#datos_creditos_cargos_creditos").html('')
        $("#tbcargos tbody tr").each(function (index, element) {
            cont++;
            nrocredito = parseInt($(element).attr('data'));
            $("div#datos_creditos_cargos_creditos").append("<imput type='hidden' name='credito" + cont + "' id='credito" + cont + "' value='" + nrocredito + "'>");
        });

        $("div#datos_creditos_cargos_creditos").append("<imput type='hidden' name='total_credito_cargo' id='total_credito_cargo' value='" + cont + "'>");

    }

    function cleartbRefinanciamiento() {
        var nro = $("#NroConceptos").val();
        if (nro > 0) {
            QuitaRow();
            clearcuotas();
            $("#cuotamensual").val('0.00')
            $("#cuotainicial").val('0.00').removeAttr('readonly');
            $("#nrocuotas").val('')
        }

    }
    var totalrecibos = 0;
    function check_deuda(obj) {
        cleartbRefinanciamiento();
        var tr = $(obj).parents("tr:eq(0)");
        var importe = parseFloat(tr.find("input.input-deuda").val()).toFixed(2);
        var val = parseFloat($("#totrefinanciamiento").val());
        var newval;
        if (obj.checked) {
            newval = val + parseFloat(importe);
        }
        else {
            newval = val - importe;
        }
        totalrecibos = newval;
        var importetotalcol = str_replace($("#importetotal").val(), ',', '');
        newval = parseFloat(newval) + parseFloat(importetotalcol);

        var redondeo = parseFloat(CalcularRedondeo(newval));
        var total_redondeado = parseFloat(newval) + parseFloat(redondeo);
        $("#totrefinanciamiento").val(total_redondeado.toFixed(2));
    }
</script>
<script src="js_refinanciamiento.js" type="text/javascript"></script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos de Refinanciamiento</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Colaterales</a></li>
                                <li style="font-size:10px"><a href="#tabs-3">Cronograma</a></li>

                            </ul>
                            <div id="tabs-1" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td width="80" >Usuario</td>
                                        <td width="17" align="center">:</td>
                                        <td colspan="7" >
                                            <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" size="15" maxlength="15" value="<?php echo $row["codantiguo"] ?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)"/>
                                            <input type="hidden" name="nroinscripcion" readonly="readonly" id="nroinscripcion" size="15" maxlength="15" class="inputtext" value="" />
                                            <span class="MljSoft-icon-buscar" title="Buscar Clientes" onclick="buscar_usuario();"></span>
                                            <input type="text" name="propietario" id="propietario" class="inputtext" size="60" maxlength="60" readonly="readonly" />
                                            <label>
                                                <input type="hidden" name="anio" id="anio" />
                                                <input type="hidden" name="mes" id="mes" />
                                                <input type="hidden" name="ciclo" id="ciclo" value="0" />
                                                <input type="hidden" name="NroConceptos" id="NroConceptos" value="0" />
                                                <input type="hidden" id="IgvP" value="<?=$paramae["valor"] ?>" />
                                                <div id="DivSave"></div>
                                            </label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">Glosa</td>
                                        <td align="center" valign="top">:</td>
                                        <td colspan="7" >
                                            <textarea name="glosarefinanciamiento" id="glosarefinanciamiento" cols="100" rows="3" class="inputtext" style="font-size:11px"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Documento</td>
                                        <td align="center">:</td>
                                        <td>
                                            <SELECT id='documentoC' name='documentoC' class='SELECT' style='width:250px' onChange="cSerieNunC(this)" >
                                                <?php
                                                $Sql = "SELECT * FROM reglasnegocio.documentos
                                                    WHERE estareg=1 and tipodocumento=0 AND codsuc=".$codsuc;
                                                $consultad = $conexion->prepare($Sql);
                                                $consultad->execute();
                                                $itemsd = $consultad->fetchAll();
                                                foreach ($itemsd as $rowd) {
                                                    $SELECTed = "";
                                                    if ($rowd["coddocumento"] == 13) {
                                                        $SELECTed = "SELECTed='SELECTed'";
                                                    }
                                                    echo "<option value='".$rowd["coddocumento"]."' ".$SELECTed." >".strtoupper($rowd["descripcion"])."</option>";
                                                }
                                                ?>
                                            </SELECT>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Serie</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="seriedocC" id="seriedocC" class="inputtext" size="3" maxlength="45"  readonly="readonly" value=""/>

                                            -<input type="text" name="numerodocC" id="numerodocC" class="inputtext" size="7" maxlength="45"  readonly="readonly" value=""/>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" class="TitDetalle">
                                            <input type="hidden" id="in_tipodeuda" value="" />
                                            <div style="height:220px; width:100%; overflow:scroll" id="div_facturacion">
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbfacturacion" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="11%" >Nro. Facturacion</th>
                                                            <th width="13%" >Doc.</th>
                                                            <th width="11%" >Nro. Doc.</th>
                                                            <th width="12%" >A&ntilde;o y Mes</th>
                                                            <th width="24%" >Tipo de Deuda</th>
                                                            <th width="11%" >Categoria</th>
                                                            <th width="10%" >Importe</th>
                                                            <th width="10%" >&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-2" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr class="TrAddItem">
                                                    <td colspan="2"  >
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" rules="all" >
                                                            <tr>
                                                                <td align="center" style="border-bottom-style: solid; border-bottom-width: 0px;">
                                                                    <input type="text" class="entero text inputtext ui-corner-all" title="C&oacute;digo del Colateral " id="CodConcepto" style="width:80px" onkeypress="ValidarEnterCre(event, 2);" />
                                                                    <span class="MljSoft-icon-buscar" title="Buscar Colateral" onclick="buscar_conceptos();"></span>
                                                                    <input type="text" class="text inputtext ui-corner-all" style="width:350px" title="Descripci&oacute;n del Colateral" id="Colateral"  />

                                                                    <input type="text" id="subtotal" title="Sub Total" class="numeric inputtext ui-corner-all text" style="width:80px" onkeypress="ValidarEnterCre(event, 1);" value="0.00" onkeyup="CalcularImporte();"/>
                                                                    <input type="text" id="igv"  class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                    <input type="text" id="redondeo" class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                    <input type="text" id="importe" class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                    <span class="icono-icon-add" title="Agregar Producto" onclick="AgregarItem()"></span>
                                                                                                                                      <!-- <span title="Obtener Cargos">Obtener Cargos</span> -->
                                                                    &nbsp;
                                                                    <span class="icono-icon-add-red" title="Cargos" onclick="AgregarCargo()"></span>
                                                                    <input type="hidden" name="afecto_igv" id="afecto_igv" value="1" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" >
                                                        <div style="height:auto; overflow:auto" align="center" id="DivDetalle">
                                                            <table width="800" border="0" align="center" cellspacing="0" class="ui-widget" id="TbIndex">
                                                                <thead class="ui-widget-header" >
                                                                    <tr >
                                                                        <th width="43" align="center" scope="col">Item</th>
                                                                        <th align="center" scope="col">Colateral</th>
                                                                        <th width="80" align="center" scope="col">Subtotal</th>
                                                                        <th width="80" align="center" scope="col">IGV</th>
                                                                        <th width="80" align="center" scope="col">Redondeo</th>
                                                                        <th width="80" align="center" scope="col">Importe</th>
                                                                        <th width="5" align="center" scope="col">&nbsp;</th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody>


                                                                </tbody>
                                                                <tfoot class="ui-widget-header">

                                                                    <tr>
                                                                        <td colspan="2" align="right" >Total :</td>
                                                                        <td align="right" >
                                                                            <input type="text" id="subtotaltotal" name="subtotal" value="<?=number_format($Total, $Presicion) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="igvtotal" name="igv" value="<?=number_format($Total, $Presicion) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="redondeototal" name="redondeo" value="<?=number_format($Total, $Presicion) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="importetotal" name="imptotal" value="<?=number_format($Total, $Presicion) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                        </td>
                                                                        <td >&nbsp;</td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-3" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr style="height:5px">
                                        <td colspan="9" class="TitDetalle"></td>
                                    </tr>
                                    <tr style="padding:4px">
                                        <td align="right">Total</td>
                                        <td align="center">:</td>
                                        <td >
                                            <input type="text" name="totrefinanciamiento" readonly="readonly" id="totrefinanciamiento" size="15" maxlength="15" class="inputtext" value="0.00" />
                                        </td>
                                        <td align="right">Cuota Mensual </td>
                                        <td align="center">:</td>
                                        <td ><input type="text" name="cuotamensual" id="cuotamensual" size="15" readonly="readonly" maxlength="15" class="inputtext" value="0.00" /></td>
                                        <td align="right" >Nro. Cuotas </td>
                                        <td align="center" >:</td>
                                        <td >
                                            <input type="text" name="nrocuotas" id="nrocuotas" size="15" maxlength="15" class="inputtext" value="" readonly="readonly" />
                                            &nbsp;&nbsp;<img src="../../../../images/iconos/add.png" width="16" height="16" alt="Generar Cuotas para el Refinanciamiento" style="cursor:pointer" onclick="ver_montos();" />&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>

                                        <td align="right">Cuota Inicial </td>
                                        <td align="center">:</td>
                                        <td ><input type="text" name="cuotainicial" id="cuotainicial" size="15" maxlength="15" class="inputtext focusSELECT" value="0.00" /></td>
                                        <td align="right">Interes </td>
                                        <td align="center">:</td>
                                        <td >
                                            <label ><?=number_format($row['tasapromintant'], 2) ?></label>%
                                            <input type="hidden" id="interescredito" name="interescredito" value="<?=$row['tasapromintant'] ?>"/>
                                        </td>
                                        <td align="right">
                                            <label>
                                                <?php
                                                $checked = "";
                                                if ($sininteres == 1)
                                                    $checked = "checked='checked'";
                                                ?>
                                                <input type="checkbox"  id="chksininteres" onclick="habilitar_sininteres();" <?=$checked ?> /> Sin Interes
                                                <input type="hidden" id="sininteres" name="sininteres" value="<?=$sininteres ?>">
                                            </label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="count" id="count" value="0" />
                                        </td>
                                        <td align="center">&nbsp;</td>
                                        <td width="90" >&nbsp;</td>
                                        <td width="104" align="right">&nbsp;</td>
                                        <td width="17" align="center">&nbsp;</td>
                                        <td width="90" >&nbsp;</td>
                                        <td width="99" >&nbsp;</td>
                                        <td width="21" >&nbsp;</td>
                                        <td width="274" >&nbsp;</td>
                                    </tr>
                                    <tr style="height:5px">
                                        <td colspan="9" class="TitDetalle">
                                            <div style="height:220px; overflow:scroll" id="div_refinanciamiento">
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable" width="100%" id="tbrefinanciamiento" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="10%" >Nro. Facturacion</th>
                                                            <th width="12%" >Tipo de Deuda</th>
                                                            <th width="11%" >Categoria</th>
                                                            <th width="8%" >Saldo</th>
                                                            <th width="8%" >Amortizaci&oacute;n</th>
                                                            <th width="8%" >Interes</th>
                                                            <th width="5%" >Igv</th>
                                                            <th width="10%" >Importe</th>
                                                            <th width="11%" >Estado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>


            </tbody>
        </table>
        <div id="datos_creditos_cargos_creditos">
            <input id="total_cargos" name="total_cargos" value="0" type="hidden" />
        </div>
    </form>
</div>
<div id="DivRef" title="Generar Cuotas de Refinanciamiento"  >
    <div id="div_ref" style="height:330px"></div>
</div>

<div id="DivCargos" title="Actualizacion y Modificacion Catastral"  >
    <div id="div_cargos"></div>
</div>
