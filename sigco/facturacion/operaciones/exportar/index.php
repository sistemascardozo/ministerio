<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "EXPORTAR ASIENTOS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	
	$objMantenimiento 	= new clsDrop();
	$codsuc = $_SESSION['IdSucursal'];
	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
	
	

?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var c = 0
	var codsuc 	= <?=$codsuc?>;
	var urldir 	= "<?php echo $_SESSION['urldir'];?>";
	$(function() {
	//$("#BtnAceptar").hide()
	});

	function cargar_anio(codciclo,condicion)
	{
	    $.ajax({
			 url:urldir + "ajax/anio_drop.php",
			 type:'POST',
			 async:true,
			 data:'codsuc='+codsuc+'&codciclo='+codciclo+'&condicion='+condicion,
			 success:function(datos){
				$("#div_anio").html(datos)
			 }
	    }) 

	}
	function cargar_mes(codciclo,suc,anio)
	{
	    $.ajax({
			 url:urldir + "ajax/mes_drop.php",
			 type:'POST',
			 async:true,
			 data:'codsuc='+suc+'&codciclo='+codciclo+'&anio='+anio,
			 success:function(datos){
				$("#div_meses").html(datos)
			 }
	    }) 

	}

	function Generar()
	{
		var Data=Validar()
		if(Data!=false)
		{
			
			$('#DivConsulta').fadeOut(500) 
			$('#ImgLoad').fadeIn();
			$("#DivResultado").empty();
			$.ajax({
			 url:'exportar.php',
			 type:'POST',
			 async:true,
			 data:Data,
			 success:function(datos)
			 {
			 	$('#ImgLoad').fadeOut(500,function(){
			                  
					$('#DivConsulta').fadeIn(500,function(){$("#DivResultado").empty().append(datos);  }) 
				})
			 	
			 }
			})
		}
	}		
	function Validar()
	{
		var Data=''
		if($("#ciclo").val()==0)
		{
			Msj($("#ciclo"),"Seleccione el Ciclo")
			return false
		}

		if($("#anio").val()==0)
		{
			Msj($("#anio"),'Seleccione el Año')
			return false
		}
		if($("#mes").val()==0)
		{
			Msj($("#mes"),'Seleccione el Mes')
			return false
		}
		return Data +'&codsuc=<?=$codsuc?>'+'&ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val()
	}
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function Consultar()
	{
		var Data=Validar()
		if(Data!=false)
		{
			$('#DivConsulta').fadeOut(500) 
			$('#ImgLoad').fadeIn();
			
			$("#DivResultado").empty();
			$.ajax({
			 url:'Consulta.php',
			 type:'POST',
			 async:true,
			 data:Data,
			 success:function(datos)
			 {
			 	$('#ImgLoad').fadeOut(500,function(){
			                  
					$('#DivConsulta').fadeIn(500,function(){$("#DivResultado").empty().append(datos);  }) 
				})
			 }
			})
		}
	}
	function Pdf()
	{
		var Data=Validar()
		if(Data!=false)
		{
			AbrirPopupImpresion("imprimir.php?" + Data,800,600)

		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	
	<tr>
       <td colspan="2">&nbsp;</td>
	</tr>
    <tr>
        <td colspan="2" align="center">
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="80">Sucursal</td>
				    <td width="30" align="center">:</td>
				    <td>
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </td>
				    <td width="80" align="right">Ciclo</td>
				    <td width="30" align="center">:</td>
				    <td>
                    	<? $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio(this.value,1);'"); ?>
                    </td>
				  </tr>
				  <tr>
				    <td>A&ntilde;o</td>
				    <td align="center">:</td>
				    <td>
                    	<div id="div_anio">
                        	<? $objMantenimiento->drop_anio($codsuc,0); ?>
                        </div>
                    </td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
				    <td>Mes</td>
				    <td align="center">:</td>
				    <td>
                    	<div id="div_meses">
                        	<? $objMantenimiento->drop_mes($codsuc,0,0); ?>
                        </div>                    
                    </td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
				    <td colspan="6" align="center">
				    <input type="button" value="Generar" id="BtnAceptar" onclick="Generar();" />
				    &nbsp;
				    <input type="button" value="Registro Contablizacion" id="BtnPdf" onclick="Pdf();" />
				</td>
			      </tr>
				</table>
		</td>
	</tr>
    <tr>
 	<td colspan="2" >
 		<div id="ImgLoad" style="text-align:center;display:none">
	        <span class="icono-icon-loading"></span>
	        Cargando ...
	    </div>
	    <div id="DivConsulta" style="height:auto; overflow:auto;display:none ">
       	<fieldset>
        <legend class="ui-state-default ui-corner-all" >Resultados de la Consulta</legend>
			<div style="height:auto; overflow:auto;" align="center" id="DivResultado">
			</div>
        </fieldset>
        </td>
        </tr>
        <tr>
	  
	  <td colspan="2" class="CampoDetalle">&nbsp;</td>
	  </tr>
	 </tbody>
	
    </table>
 </form>
</div>

<?php   CuerpoInferior();?>