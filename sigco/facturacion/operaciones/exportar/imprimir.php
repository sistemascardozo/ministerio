<?php
	set_time_limit(0);
	include("../../../../objetos/clsReporte.php");

	class clsRegistroVentas extends clsReporte {
		function cabecera() {

			global $m,$anio,$ciclotext,$Dim;

			$this->SetFont('Arial','B',14);
			$this->SetY(15);
			$tit1 = "REGISTRO DE CONTABILIZACION";
			$this->Cell(0,$h+2,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);

			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(0,$h,$ciclotext,0,1,'C');

			$this->SetX(63.5);
			$this->Cell(10, $h,"MES",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$m,0,0,'L');
			$this->Cell(10, $h,utf8_decode("AÑO"),0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$anio,0,1,'L');

			$this->Ln(2);
			$this->Ln(1);
			$this->SetFont('Arial','',6);

			$h=3;
			$this->Cell($Dim[1],$h,"Nro",'LT',0,'C');
			$this->Cell($Dim[2],$h,"Nro",'T',0,'C');
			$this->Cell($Dim[3],$h,"Cuenta",'T',0,'C');
			$this->Cell($Dim[4],$h,"",'T',0,'C');
			$this->Cell($Dim[5]+$Dim[6],$h,"IMPORTE",'TB',0,'C');
			$this->Cell($Dim[7],$h,"",'RT',1,'C');

			$this->Cell($Dim[1],$h,"Voucher",'LB',0,'C');
			$this->Cell($Dim[2],$h,"Linea",'B',0,'C');
			$this->Cell($Dim[3],$h,"Contable",'B',0,'C');
			$this->Cell($Dim[4],$h,"GLOSA",'B',0,'C');
			$this->Cell($Dim[5],$h,"CARGO",'B',0,'C');
			$this->Cell($Dim[6],$h,"ABONO",'B',0,'C');
			$this->Cell($Dim[7],$h,"DIFERENCIA",'BR',1,'C');
			$h=4;


		}

		function agregar_detalle($codsuc,$ciclo,$anio,$mes,$Csec) {

			global $conexion,$Dim,$meses;
			//if(intval($mes)<10) $mes="0".$mes;

			$h         = 4;
			$ultimodia = $this->obtener_ultimo_dia_mes($mes,$anio);
			$primerdia = $this->obtener_primer_dia_mes($mes,$anio);
			$count     = 0;
			$borde     = 0;
			$impmes    = 0;
			$impigv    = 0;
			$imptotal  = 0;
			$TotalG    = 0;

			$this->SetTextColor(0,0,0);
			//DATOS DE LA FACTURACION
			$sql = "select nrofacturacion,fechaemision
					from facturacion.periodofacturacion
					where codemp=1 and codsuc=? and codciclo=? AND anio=? AND mes=?";
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$ciclo,$anio,intval($mes)));
			$row = $consulta->fetch();
			$nrofacturacion=$row['nrofacturacion'];
			$fechaemision=$row['fechaemision'];

	    //ORDEN LCL, 1 REG, TEC,1REG, TEL, DETALLE 2103280
			//if(intval($mes)<10) $mes="0".$mes;
			$aniomin = substr($anio,2, 2);
			$count=0;
			$TotalRestar=0;

			$Sql="	SELECT
										cli.codzona as codzona,
										z.descripcion as zona,
										CAST(sum(CASE WHEN d.importe <= 0 AND c.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)) as cantidad
									FROM facturacion.detctacorriente d
										JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto)
										JOIN catastro.clientes cli ON (d.codemp = cli.codemp) AND (d.codsuc = cli.codsuc) AND (d.nroinscripcion = cli.nroinscripcion)
										JOIN admin.zonas z ON (cli.codzona = z.codzona) AND (cli.codemp = z.codemp) AND (cli.codsuc = z.codsuc)
									WHERE d.codsuc=".$codsuc." and d.nrofacturacion=".$nrofacturacion."
										AND ((d.nrocredito = 0 and d.nrorefinanciamiento = 0) OR d.codconcepto = 9 OR  d.codtipodeuda in (3, 4, 7,9))
										AND d.tipo=0 AND d.tipoestructura=0 AND d.periodo='".$anio.$mes."'
										AND d.codconcepto<>10012 and d.codtipodeuda not in (3, 4, 7)
									GROUP BY cli.codzona, z.descripcion
									ORDER BY cli.codzona";

			$row = $conexion->query($Sql)->fetchAll();

			foreach ($row as $indice => $valores) :


				$Total = $valores['cantidad'] - $TotalRestar;
				//$LDOC1= "0101".$aniomin.$mes."01";
				$LDOC1 = "0".$valores['codzona']."01".$aniomin.str_pad($mes, 2, "0", STR_PAD_LEFT)."0".$valores['codzona'];
	

				$Glosa = "FACT - ".substr($meses[intval($mes)], 0, 3)." CORONEL PORTILLO," . $aniomin." Distrito : " . $valores['zona'];

				/*$Sql="INSERT INTO dicfactu
				            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
				            	CTOD,CTOH,DOC1,TIVO,CODDD,
				            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
								( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
									".$Total.",".$Total.",'".$LDOC1."',41,'33',
									' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
				odbc_exec($conexion2,$Sql);*/
				$this->SetFont('Arial', 'B', 6);
				$this->SetFillColor(255, 255, 255); //Color de Fondo

				$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
				$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
				$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
				$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
				$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
				$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
				$this->SetFont('Arial','',6);
				$TotalG+=$Total;

				//DETALLE DE FACTURACION
				$Sql="	SELECT CAST(sum(CASE WHEN d.importe <= 0 AND c.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
									c.descripcion, c.ctadebe,  c.ctahaber,c.codconcepto
								FROM  facturacion.detctacorriente d
									JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto)
									JOIN catastro.clientes cli ON (d.codemp = cli.codemp) AND (d.codsuc = cli.codsuc) AND (d.nroinscripcion = cli.nroinscripcion)
								WHERE d.codsuc=".$codsuc." and d.nrofacturacion=".$nrofacturacion."
									AND ((d.nrocredito = 0 and d.nrorefinanciamiento = 0) OR d.codtipodeuda=9 OR d.codconcepto = 9 OR d.codtipodeuda in (3, 4, 7,9))
									AND d.codconcepto<>10012 and d.codtipodeuda not in (3, 4, 7) AND cli.codzona = " . $valores['codzona'] . "
									AND d.tipo=0 AND d.tipoestructura=0 AND d.periodo='".$anio.$mes."'
								GROUP BY  c.descripcion,c.ctadebe,c.ctahaber,c.codantiguo,c.codconcepto
								ORDER BY c.codantiguo  ASC";

				$Consulta = $conexion->query($Sql);
				$cont = 0;
				$TotalDebe = 0;
				foreach($Consulta->fetchAll() as $row)
				{
					$cont++;
					$concepto= $row[1];
					$restar=isset($Conceptos2[$row['codconcepto']]["Importe"])?$Conceptos2[$row['codconcepto']]["Importe"]:0;
					$TotalConcepto=$row[0]-floatval($restar);
					/*$Sql="INSERT INTO dilfactu
			        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
						( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaber']."',
							' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
							'H',41,40,' ','33',' ',' ',' ',
							' ',' ',' ',' ',' ',' ')";
					odbc_exec($conexion2,$Sql);*/
					$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
					$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
					$this->Cell($Dim[3],$h,$row['ctahaber'],$borde,0,'C',true);
					$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
					$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
					$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
					$this->Cell($Dim[6],$h,'',$borde,1,'R',true);

					$cont++;
					/*$Sql="INSERT INTO dilfactu
			        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
						( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebe']."',
							' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
							'D',41,40,' ','33',' ',' ',' ',
							' ',' ',' ',' ',' ',' ')";
					odbc_exec($conexion2,$Sql);*/
					$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
					$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
					$this->Cell($Dim[3],$h,$row['ctadebe'],$borde,0,'C',true);
					$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
					$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
					$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
					$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
				}

			endforeach;



			//DETALLE DE FACTURACION
			/////////////////////////////////////////
			$Sql="SELECT  CAST(SUM(CASE WHEN d.imprebajado <= 0 AND co.signo='-' THEN d.imprebajado*(-1)  ELSE d.imprebajado END) AS NUMERIC (18,2))
			FROM   facturacion.cabrebajas c
			INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp)
			AND (c.codsuc = d.codsuc)  AND (c.nrorebaja = d.nrorebaja)
			INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
			  AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
					  WHERE c.codsuc=".$codsuc." AND c.estareg=1 AND c.anio='".$anio."'
					  AND c.mes='".intval($mes)."'";
			$row = $conexion->query($Sql)->fetch();
			$Total = $row[0];
			$LDOC1= "0102".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
			$Glosa="NOTA  ABONO -".substr($meses[intval($mes)],0,3)." ".$aniomin." ";
			/*$Sql="INSERT INTO dicfactu
			            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
			            	CTOD,CTOH,DOC1,TIVO,CODDD,
			            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
							( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
								".$Total.",".$Total.",'".$LDOC1."',41,'33',
								' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
			odbc_exec($conexion2,$Sql);*/
			$this->SetFont('Arial','B',6);
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
			$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
			$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
			$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
			$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
			$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
			$this->SetFont('Arial','',6);
			$TotalG+=$Total;
			//REBAJAS
			//DETALLE DE REBAJAS
			$Sql="SELECT CAST(sum(CASE WHEN d.imprebajado <= 0 AND co.signo='-' THEN d.imprebajado*(-1)  ELSE d.imprebajado END) AS NUMERIC (18,2)),
			co.descripcion, co.ctahaberrebajas,  co.ctadeberebajas
			FROM facturacion.cabrebajas c
			INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp)
			AND (c.codsuc = d.codsuc)  AND (c.nrorebaja = d.nrorebaja)
			 INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
			  AND (d.codsuc = co.codsuc)
			  AND (d.codconcepto = co.codconcepto)
					  WHERE c.codsuc=".$codsuc." AND c.estareg=1 AND c.anio='".$anio."'
					  AND c.mes='".intval($mes)."'
			  GROUP BY co.descripcion, co.ctahaberrebajas,  co.ctadeberebajas,co.codantiguo
			  ORDER BY co.codantiguo  ASC";
			$Consulta = $conexion->query($Sql);
			$cont=0;
			$TotalDebe=0;
			foreach($Consulta->fetchAll() as $row)
			{
				$cont++;
				$concepto= $row[1];
				$TotalConcepto=$row[0];
				if($TotalConcepto>0)
				{
					/*$Sql="INSERT INTO dilfactu
			        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
						( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberrebajas']."',
							' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
							'H',41,40,' ','33',' ',' ',' ',
							' ',' ',' ',' ',' ',' ')";
					odbc_exec($conexion2,$Sql);*/
					$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
					$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
					$this->Cell($Dim[3],$h,$row['ctahaberrebajas'],$borde,0,'C',true);
					$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
					$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
					$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
					$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
					$cont++;
					/*$Sql="INSERT INTO dilfactu
			        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
						( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadeberebajas']."',
							' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
							'D',41,40,' ','33',' ',' ',' ',
							' ',' ',' ',' ',' ',' ')";
					odbc_exec($conexion2,$Sql);*/
					$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
					$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
					$this->Cell($Dim[3],$h,$row['ctadeberebajas'],$borde,0,'C',true);
					$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
					$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
					$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
					$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
				}
			}
			//CABECERA DE CARGOS O INTERESES
			//CREDITOS
			$Total = 0;
			
			$Sql = "SELECT CAST(SUM(d.interes) AS NUMERIC (18, 2)) ";
			$Sql .= "FROM facturacion.cabcreditos c ";
			$Sql .= " INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrocredito = d.nrocredito) ";
			$Sql .= "WHERE c.codsuc = ".$codsuc." ";
			$Sql .= " AND c.estareg = 1 ";
			$Sql .= " AND c.sininteres = 0 ";
			$Sql .= " AND c.interes > 0 ";
			$Sql .= " AND c.nroprepago > 0 ";
			$Sql .= " AND c.nroprepagoinicial > 0 ";
			$Sql .= " AND extract(year from c.fechareg) = '".$anio."' ";
			$Sql .= " AND extract(month from c.fechareg) = '".intval($mes)."' ";
			
			$row = $conexion->query($Sql)->fetch();
			
			$Total = $row[0];
			
			
			//FINANCIAMIENTOS
			$Sql = "SELECT CAST(SUM(d.importe) - c.totalrefinanciado AS  NUMERIC (18, 2)), c.nroinscripcion ";
			$Sql .= "FROM facturacion.cabrefinanciamiento c ";
			$Sql .= " INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorefinanciamiento = d.nrorefinanciamiento) ";
			$Sql .= "WHERE d.codemp = 1 ";
			$Sql .= " AND d.codsuc = ".$codsuc." ";
			$Sql .= " AND c.estareg = 1 ";
			$Sql .= " AND c.sininteres = 0 ";
			$Sql .= " AND c.nroprepagoinicial > 0 ";
			$Sql .= " AND extract(year from c.fechaemision) = '".$anio."' ";
			$Sql .= " AND extract(month from c.fechaemision) = '".intval($mes)."' ";
			$Sql .= " AND c.codestadorefinanciamiento <> 5 ";
			$Sql .= "GROUP BY c.nroinscripcion, c.totalrefinanciado ";
			//die($Sql);
			$Consulta = $conexion->query($Sql);
			
			foreach($Consulta->fetchAll() as $row)
			{
				$Total += $row[0];
			}

			//FINANCIAMIENTOS
			$LDOC1= "0103".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
			$Glosa="NOTA CARGO-".substr($meses[intval($mes)],0,3)." ".$aniomin." ";
			/*$Sql="INSERT INTO dicfactu
			            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
			            	CTOD,CTOH,DOC1,TIVO,CODDD,
			            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
							( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
								".$Total.",".$Total.",'".$LDOC1."',41,'33',
								' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
			odbc_exec($conexion2,$Sql);*/
			$this->SetFont('Arial','B',6);
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
			$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
			$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
			$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
			$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
			$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
			$this->SetFont('Arial','',6);
			$TotalG+=$Total;
			//CABECERA DE CARGOS O INTERESES
			//DETALLE DE CARGOS O INTERESES
			$Sql="SELECT co.descripcion, co.ctahaber,  co.ctadebe
			FROM  facturacion.conceptos co
			  WHERE co.codsuc=".$codsuc."  AND co.codconcepto=9";

			$Consulta = $conexion->query($Sql);
			$cont=0;
			$TotalDebe=0;
			foreach($Consulta->fetchAll() as $row)
			{
				$cont=1;
				$concepto= 'INTERESES POR DEUDA';
				$TotalConcepto=$Total;
				/*$Sql="INSERT INTO dilfactu
		        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
		        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
		        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
		        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
					( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaber']."',
						' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
						'H',41,40,' ','33',' ',' ',' ',
						' ',' ',' ',' ',' ',' ')";
				odbc_exec($conexion2,$Sql);*/
				$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
				$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
				$this->Cell($Dim[3],$h,$row['ctahaber'],$borde,0,'C',true);
				$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
				$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
				$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
				$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
				$cont++;
				/*$Sql="INSERT INTO dilfactu
		        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
		        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
		        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
		        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
					( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebe']."',
						' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
						'D',41,40,' ','33',' ',' ',' ',
						' ',' ',' ',' ',' ',' ')";
				odbc_exec($conexion2,$Sql);*/
				$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
				$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
				$this->Cell($Dim[3],$h,$row['ctadebe'],$borde,0,'C',true);
				$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
				$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
				$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
				$this->Cell($Dim[6],$h,'',$borde,1,'R',true);

			}
			//DETALLE DE CARGOS O INTERESES

			//CABECERA DE REG CONVENIO
			//PENSIONES
			$Total=0;
			$Sql="SELECT  o.codsuc,  o.nrorefinanciamiento,  o.codciclo,  o.nrofacturacion,  o.nroinscripcion
				FROM  facturacion.cabrefinanciamiento c
			  INNER JOIN facturacion.origen_refinanciamiento o ON (c.codemp = o.codemp)
			  AND (c.codsuc = o.codsuc)
			  AND (c.nrorefinanciamiento = o.nrorefinanciamiento)
			  AND (c.nroinscripcion = o.nroinscripcion)
			  INNER JOIN facturacion.cabfacturacion ON (o.codsuc = facturacion.cabfacturacion.codsuc)
			  AND (o.codemp = facturacion.cabfacturacion.codemp)
			  AND (o.codsuc = facturacion.cabfacturacion.codsuc)
			  AND (o.codciclo = facturacion.cabfacturacion.codciclo)
			  AND (o.nrofacturacion = facturacion.cabfacturacion.nrofacturacion)
			  AND (o.nroinscripcion = facturacion.cabfacturacion.nroinscripcion)
				where c.codemp=1 and c.codsuc=".$codsuc." AND c.estareg=1
				AND  extract(year from c.fechaemision) ='".$anio."'
				AND extract(month from c.fechaemision)  ='".intval($mes)."'
				GROUP BY o.codsuc,o.nrorefinanciamiento, o.codciclo,  o.nrofacturacion,  o.nroinscripcion
			  ORDER BY o.nrorefinanciamiento,o.nrofacturacion";
			$Consulta = $conexion->query($Sql);
			$Conceptos = array();
			foreach($Consulta->fetchAll() as $row)
			{
				//
				$Sql="SELECT CAST(sum(CASE WHEN d.importe <= 0 AND c.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
				c.descripcion, c.ctadebe,  c.ctahaber,c.codconcepto,c.signo
				FROM  facturacion.detfacturacion d
				INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)
				AND (d.codsuc = c.codsuc)  AND (d.codconcepto = c.codconcepto)
				WHERE d.codsuc=".$codsuc." and d.nrofacturacion=".$row['nrofacturacion']."
					AND d.codciclo=".$row['codciclo']." and d.nroinscripcion=".$row['nroinscripcion']."
				  GROUP BY  c.descripcion,c.ctadebe,c.ctahaber,c.codantiguo,c.codconcepto, c.signo
				  ORDER BY c.codantiguo  ASC";
				$Consulta2 = $conexion->query($Sql);

				foreach($Consulta2->fetchAll() as $row2)
				{
					if (!array_key_exists($row2['codconcepto'], $Conceptos))
					{
						$Conceptos[$row2['codconcepto']] = array(
																"Id"=>$row2['codconcepto'],
																"Importe"=>$row2[0],
																"Descripcion"=>$row2[1],
																"CtaDebe"=>$row2['ctadebe'],
																"Signo"=>$row2['signo'],
																"CtaHaber"=>$row2['ctahaber']);
					}
					else
					{
						$Conceptos[$row2['codconcepto']] = array(
																	"Id"=>$row2['codconcepto'],
																	"Descripcion"=>$row2[1],
																	"CtaDebe"=>$row2['ctadebe'],
																	"CtaHaber"=>$row2['ctahaber'],
																	"Signo"=>$row2['signo'],
																	"Importe"=>$Conceptos[$row2['codconcepto']]["Importe"] + $row2[0]);
					}
					if($row2['signo']=='+')
						$Total+=$row2[0];
				}
				//
			}
			//print_r($Conceptos);
			//die($Total);
			//PENSIONES
			//COLATERALES

			$Sql="SELECT CAST(sum(CASE WHEN d.imptotal <= 0 AND co.signo='-' THEN d.imptotal*(-1)  ELSE d.imptotal END) AS NUMERIC (18,2)),
				co.descripcion, co.ctadebe,  co.ctahaber,co.codconcepto,co.signo
				FROM    facturacion.cabrefinanciamiento c
		  		INNER JOIN facturacion.detcabrefinanciamiento d ON (c.codemp = d.codemp)
		  		AND (c.codsuc = d.codsuc) AND (c.nrorefinanciamiento = d.nrorefinanciamiento)
		  		INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
		  		AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
				where c.codemp=1 and c.codsuc=".$codsuc." AND c.estareg=1
				AND  extract(year from c.fechaemision) ='".$anio."'
				AND extract(month from c.fechaemision)  ='".intval($mes)."'
				  GROUP BY  co.descripcion,co.ctadebe,co.ctahaber,co.codantiguo,co.codconcepto, co.signo
				  ORDER BY co.codantiguo  ASC";
				$Consulta2 = $conexion->query($Sql);
				foreach($Consulta2->fetchAll() as $row2)
				{
					if (!array_key_exists($row2['codconcepto'], $Conceptos))
					{
						$Conceptos[$row2['codconcepto']] = array(
																"Id"=>$row2['codconcepto'],
																"Importe"=>$row2[0],
																"Descripcion"=>$row2[1],
																"CtaDebe"=>$row2['ctadebe'],
																"Signo"=>$row2['signo'],
																"CtaHaber"=>$row2['ctahaber']);
					}
					else
					{
						$Conceptos[$row2['codconcepto']] = array(
																	"Id"=>$row2['codconcepto'],
																	"Descripcion"=>$row2[1],
																	"CtaDebe"=>$row2['ctadebe'],
																	"CtaHaber"=>$row2['ctahaber'],
																	"Signo"=>$row2['signo'],
																	"Importe"=>$Conceptos[$row2['codconcepto']]["Importe"] + $row2[0]);
					}
					if($row2['signo']=='+')
						$Total+=$row2[0];
				}
			$Total = $Total +$TotalRestar;
			$LDOC1= "0104".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
			$Glosa="REGULARIZACION POR CONVENIO";
			/*$Sql="INSERT INTO dicfactu
			            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
			            	CTOD,CTOH,DOC1,TIVO,CODDD,
			            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
							( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
								".$Total.",".$Total.",'".$LDOC1."',41,'33',
								'00000903',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
			odbc_exec($conexion2,$Sql);*/
			$this->SetFont('Arial','B',6);
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
			$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
			$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
			$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
			$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
			$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
			$this->SetFont('Arial','',6);
			$TotalG+=$Total;
			//COLATERALES
			//CABECERA DE CONVENIO
			//DETALLE DE CONVENIO
			sort($Conceptos);
			//print_r($Conceptos);
			$cont=0;
			$TotalDebe=0;
			for($i=0;$i<count($Conceptos);$i++)
			{
				$cont++;
				$concepto= utf8_decode($Conceptos[$i]["Descripcion"]);
				$restar=isset($Conceptos2[$Conceptos[$i]["Id"]]["Importe"])?$Conceptos2[$Conceptos[$i]["Id"]]["Importe"]:0;
				$TotalConcepto=$Conceptos[$i]["Importe"];
				$TotalConcepto=$TotalConcepto+floatval($restar);
				if($TotalConcepto>0)
				{
					if($Conceptos[$i]['Signo']=='+')
					{//echo '==>'.$Conceptos[$i]['Signo'];
						/*$Sql="INSERT INTO dilfactu
				        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
				        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
				        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
				        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
							( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$Conceptos[$i]["CtaDebe"]."',
								' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
								'H',41,40,' ','33',' ',' ',' ',
								' ',' ',' ',' ',' ',' ')";*/
						$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
						$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
						$this->Cell($Dim[3],$h,$Conceptos[$i]["CtaDebe"],$borde,0,'C',true);
						$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
						$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
						$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
						$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
					}
					else
					{
						/*$TotalDebe+=$TotalConcepto;
						$Sql="INSERT INTO dilfactu
				        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
				        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
				        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
				        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
							( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$Conceptos[$i]["CtaHaber"]."',
								' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
								'D',41,40,' ','33',' ',' ',' ',
								' ',' ',' ',' ',' ',' ')";*/
						$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
						$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
						$this->Cell($Dim[3],$h,$Conceptos[$i]["CtaHaber"],$borde,0,'C',true);
						$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
						$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
						$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
						$this->Cell($Dim[6],$h,'',$borde,1,'R',true);

					}
					//odbc_exec($conexion2,$Sql);
				}
			}
			$cont++;
			$TotalConcepto=$Total-$TotalDebe;
			$concepto='TOTAL FACTURA';
			/*$Sql="INSERT INTO dilfactu
		    	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
		    		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
		    		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
		    		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','121105101',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'D',41,40,' ','33',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);*/
			$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
			$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
			$this->Cell($Dim[3],$h,'121105101',$borde,0,'C',true);
			$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
			$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
			$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
			$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
			//DETALLE DE CONVENIO
			//CABECERA DE BOLETAS Y FACTURAS
			//CABPREPAGOS
			$Sql="SELECT  CAST(SUM(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2))
					FROM  cobranza.cabprepagos c
		  			INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
		  			AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
		  			AND (c.nroinscripcion = d.nroinscripcion)
		  			INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
			  AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
		  			WHERE c.codsuc=".$codsuc." AND c.estado<>0 AND c.estado<>2
		  			AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."' AND d.coddocumento IN (13,14) AND co.codconcepto<>10005";
			$row = $conexion->query($Sql)->fetch();
			$Total = $row[0];
			//PAGOS
			$Sql="SELECT  CAST(SUM(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2))
					FROM  cobranza.cabpagos c
		  			INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
		  			AND (c.codsuc = d.codsuc)  AND (c.nropago = d.nropago)
		  			AND (c.nroinscripcion = d.nroinscripcion)
		  			INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
			  AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
		  			WHERE c.codsuc=".$codsuc." AND c.anulado=0 AND c.car <> 9
		  			AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."' AND d.coddocumento IN (13,14) AND co.codconcepto<>10005";
			$row = $conexion->query($Sql)->fetch();
			$Total += $row[0];
			$LDOC1= "0105".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
			$Glosa="COLATERALES DE ".substr($meses[intval($mes)],0,3)." ".$aniomin." ";
			/*$Sql="INSERT INTO dicfactu
			            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
			            	CTOD,CTOH,DOC1,TIVO,CODDD,
			            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
							( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
								".$Total.",".$Total.",'".$LDOC1."',41,'33',
								'00000903',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
			odbc_exec($conexion2,$Sql);*/
			$this->SetFont('Arial','B',6);
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
			$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
			$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
			$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
			$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
			$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
			$this->SetFont('Arial','',6);
			$TotalG+=$Total;
			//CABECERA DE BOLETAS Y FACTURAS
			//DETALLE DE BOLETAS Y FACTURAS
			$Sql="(SELECT
				  CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
					co.descripcion, co.ctahaber,  co.ctadebe,co.codantiguo,co.codconcepto,1
				FROM  cobranza.cabprepagos c
				  INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
				  AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
				  AND (c.nroinscripcion = d.nroinscripcion)
				   INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
					  AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
				  WHERE c.codsuc=".$codsuc." AND c.estado<>0 AND c.estado<>2
				  AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."'
				   AND d.coddocumento IN (13,14) AND co.codconcepto<>10005
				  GROUP BY co.descripcion, co.ctahaber,  co.ctadebe,co.codantiguo,co.codconcepto
					  ORDER BY co.codantiguo  ASC)
				UNION
				(
				 SELECT
				   CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
					co.descripcion, co.ctahaber,  co.ctadebe,co.codantiguo,co.codconcepto,2
				FROM
				  cobranza.cabpagos c
				  INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
				  AND (c.codsuc = d.codsuc)
				  AND (c.nropago = d.nropago)
				  AND (c.nroinscripcion = d.nroinscripcion)
				    INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
					  AND (d.codsuc = co.codsuc)
					  AND (d.codconcepto = co.codconcepto)
				  WHERE c.codsuc=".$codsuc." AND c.anulado=0 AND c.car <> 9
				  AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."'
				  AND d.coddocumento IN (13,14) AND co.codconcepto<>10005
				   GROUP BY co.descripcion, co.ctahaber,  co.ctadebe,co.codantiguo,co.codconcepto
					  ORDER BY co.codantiguo  ASC)
				 ORDER BY codantiguo, descripcion, ctahaber,  ctadebe";
			$Consulta = $conexion->query($Sql);
			$Conceptos = array();
			foreach($Consulta->fetchAll() as $row)
			{
				if (!array_key_exists($row['codconcepto'], $Conceptos))
				{
					$Conceptos[$row['codconcepto']] = array(
															"Id"=>$row['codconcepto'],
															"Importe"=>$row[0],
															"Descripcion"=>$row[1],
															"CtaDebe"=>$row['ctadebe'],
															"CtaHaber"=>$row['ctahaber']);
				}
				else
				{
					$Conceptos[$row['codconcepto']] = array(
																"Id"=>$row['codconcepto'],
																"Descripcion"=>$row[1],
																"CtaDebe"=>$row['ctadebe'],
																"CtaHaber"=>$row['ctahaber'],
																"Importe"=>$Conceptos[$row['codconcepto']]["Importe"] + $row[0]);
				}
			}


			sort($Conceptos);
			//print_r($Conceptos);
			$cont=0;
			$TotalDebe=0;
			for($i=0;$i<count($Conceptos);$i++)
			{
				$cont++;
				$concepto= $Conceptos[$i]["Descripcion"];
				$TotalConcepto=$Conceptos[$i]["Importe"];
				if($TotalConcepto>0)
				{
					/*$Sql="INSERT INTO dilfactu
			        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
						( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$Conceptos[$i]["CtaHaber"]."',
							' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
							'H',41,40,' ','33',' ',' ',' ',
							' ',' ',' ',' ',' ',' ')";
					odbc_exec($conexion2,$Sql);*/
					$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
					$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
					$this->Cell($Dim[3],$h,$Conceptos[$i]["CtaHaber"],$borde,0,'C',true);
					$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
					$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
					$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
					$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
					$cont++;
					/*$Sql="INSERT INTO dilfactu
			        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
						( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$Conceptos[$i]["CtaDebe"]."',
							' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
							'D',41,40,' ','33',' ',' ',' ',
							' ',' ',' ',' ',' ',' ')";
					odbc_exec($conexion2,$Sql);*/
					$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
					$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
					$this->Cell($Dim[3],$h,$Conceptos[$i]["CtaDebe"],$borde,0,'C',true);
					$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
					$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
					$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
					$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
				}
			}
			//DETALLE DE BOLETAS Y FACTURAS
			//PROVISIONES 1 Y 2
			//CABECERA DE PROVISION1
			// $Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2))
			// 	FROM
			// 	  facturacion.provisiones p
			// 	  INNER JOIN facturacion.provisiones_detalle pd ON (p.codemp = pd.codemp)
			// 	  AND (p.codsuc = pd.codsuc)
			// 	  AND (p.codprovision = pd.codprovision)
			// 	  AND (p.tipo = pd.tipo)
			// 	  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
			//   AND (pd.codsuc = c.codsuc)
			//   AND (pd.codconcepto = c.codconcepto)
			// 	  WHERE p.codsuc=".$codsuc." and  p.estadoprovision=2 AND p.tipo=1
			// 		AND p.anio='".$anio."' AND p.mes='".$mes."'";
			// $row = $conexion->query($Sql)->fetch();
			// $Total = isset($row[0])?$row[0]:0;
			// if(floatval($Total)>0)
			// {
			// 	$LDOC1= "0106".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
			// 	//BORRAR DATOS
			// 	$Glosa="PROVISION ASIENTO 1 ";
			//
			// 	/*$Sql="INSERT INTO dicfactu
			// 	            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
			// 	            	CTOD,CTOH,DOC1,TIVO,CODDD,
			// 	            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
			// 					( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
			// 						".$Total.",".$Total.",'".$LDOC1."',41,'33',
			// 						' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
			// 	odbc_exec($conexion2,$Sql);*/
			// 	$this->SetFont('Arial','B',6);
			// 	$this->SetFillColor(255,255,255); //Color de Fondo
			// 	$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
			// 	$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
			// 	$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
			// 	$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
			// 	$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
			// 	$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
			// 	$this->SetFont('Arial','',6);
			// 	//$TotalG+=$Total;
			// 	//CABECERA DE PROVISION1
			// 	//DETALLE DE PROVISION1
			// 	$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2)),
			// 			c.descripcion, c.ctadebeprov,  c.ctahaberprov,c.codconcepto
			// 		FROM
			// 		  facturacion.provisiones p
			// 		  INNER JOIN facturacion.provisiones_detalle pd ON (p.codemp = pd.codemp)
			// 		  AND (p.codsuc = pd.codsuc)
			// 		  AND (p.codprovision = pd.codprovision)
			// 		  AND (p.tipo = pd.tipo)
			// 		  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
			// 	  AND (pd.codsuc = c.codsuc)
			// 	  AND (pd.codconcepto = c.codconcepto)
			// 		  WHERE p.codsuc=".$codsuc." and  p.estadoprovision=2 AND p.tipo=1
			// 			AND p.anio='".$anio."' AND p.mes='".$mes."'
			// 			GROUP BY  c.descripcion,c.ctadebeprov,c.ctahaberprov,c.codantiguo,c.codconcepto
			// 	 		 ORDER BY c.codantiguo  ASC ";
			// 	$Consulta = $conexion->query($Sql);
			// 	$cont=0;
			// 	$TotalDebe=0;
			// 	foreach($Consulta->fetchAll() as $row)
			// 	{
			// 		$cont++;
			// 		$concepto= utf8_decode($row[1]);
			// 		$TotalConcepto=$row[0];
			// 		/*$Sql="INSERT INTO dilfactu
			//         	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			//         		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			//         		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			//         		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			// 			( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberprov']."',
			// 				' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
			// 				'H',41,40,' ','33',' ',' ',' ',
			// 				' ',' ',' ',' ',' ',' ')";
			// 		odbc_exec($conexion2,$Sql);
			// 		*/
			// 		$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
			// 		$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
			// 		$this->Cell($Dim[3],$h,$row["ctahaberprov"],$borde,0,'C',true);
			// 		$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
			// 		$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
			// 		$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
			// 		$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
			// 		$cont++;
			// 		/*$Sql="INSERT INTO dilfactu
			//         	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			//         		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			//         		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			//         		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			// 			( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebeprov']."',
			// 				' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
			// 				'D',41,40,' ','33',' ',' ',' ',
			// 				' ',' ',' ',' ',' ',' ')";
			// 		odbc_exec($conexion2,$Sql);*/
			// 		$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
			// 		$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
			// 		$this->Cell($Dim[3],$h,$row["ctadebeprov"],$borde,0,'C',true);
			// 		$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
			// 		$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
			// 		$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
			// 		$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
			// 	}
			// 	//DETALLE DE PROVISION 1
			// 	//CABECERA DE PROVISION2
			// 	$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2))
			// 		FROM
			// 		  facturacion.provisiones p
			// 		  INNER JOIN facturacion.provisiones_detalle pd ON (p.codemp = pd.codemp)
			// 		  AND (p.codsuc = pd.codsuc)
			// 		  AND (p.codprovision = pd.codprovision)
			// 		  AND (p.tipo = pd.tipo)
			// 		  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
			// 	  AND (pd.codsuc = c.codsuc)
			// 	  AND (pd.codconcepto = c.codconcepto)
			// 		  WHERE p.codsuc=".$codsuc." and  p.estadoprovision=2 AND p.tipo=1
			// 			AND p.anio='".$anio."' AND p.mes='".$mes."' ";
			// 	$row = $conexion->query($Sql)->fetch();
			// 	$Total = $row[0];
			// 	$LDOC1= "0107".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
			// 	//BORRAR DATOS
			// 	$Glosa="PROVISION ASIENTO 2 ";
			//
			// 	/*$Sql="INSERT INTO dicfactu
			// 	            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
			// 	            	CTOD,CTOH,DOC1,TIVO,CODDD,
			// 	            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
			// 					( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
			// 						".$Total.",".$Total.",'".$LDOC1."',41,'33',
			// 						' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
			// 	odbc_exec($conexion2,$Sql);*/
			// 	$this->SetFont('Arial','B',6);
			// 	$this->SetFillColor(255,255,255); //Color de Fondo
			// 	$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
			// 	$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
			// 	$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
			// 	$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
			// 	$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
			// 	$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
			// 	$this->SetFont('Arial','',6);
			// 	//$TotalG+=$Total;
			// 	//CABECERA DE PROVISION1
			// 	//DETALLE DE PROVISION1
			// 	$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2)),
			// 			c.descripcion, c.ctadebeprov2,  c.ctahaberprov2,c.codconcepto
			// 		FROM
			// 		  facturacion.provisiones p
			// 		  INNER JOIN facturacion.provisiones_detalle pd ON (p.codemp = pd.codemp)
			// 		  AND (p.codsuc = pd.codsuc)
			// 		  AND (p.codprovision = pd.codprovision)
			// 		  AND (p.tipo = pd.tipo)
			// 		  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
			// 	  		AND (pd.codsuc = c.codsuc)
			// 	  		AND (pd.codconcepto = c.codconcepto)
			// 		  WHERE p.codsuc=".$codsuc." and  p.estadoprovision=2 AND p.tipo=1
			// 			AND p.anio='".$anio."' AND p.mes='".$mes."'
			// 			GROUP BY  c.descripcion,c.ctadebeprov2,c.ctahaberprov2,c.codantiguo,c.codconcepto
			// 	 		 ORDER BY c.codantiguo  ASC ";
			// 	$Consulta = $conexion->query($Sql);
			// 	$cont=0;
			// 	$TotalDebe=0;
			// 	foreach($Consulta->fetchAll() as $row)
			// 	{
			// 		$cont++;
			// 		$concepto= utf8_decode($row[1]);
			// 		$TotalConcepto=$row[0];
			// 		/*$Sql="INSERT INTO dilfactu
			//         	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			//         		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			//         		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			//         		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			// 			( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberprov2']."',
			// 				' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
			// 				'H',41,40,' ','33',' ',' ',' ',
			// 				' ',' ',' ',' ',' ',' ')";
			// 		odbc_exec($conexion2,$Sql);*/
			// 		$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
			// 		$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
			// 		$this->Cell($Dim[3],$h,$row["ctahaberprov2"],$borde,0,'C',true);
			// 		$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
			// 		$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
			// 		$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
			// 		$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
			// 		$cont++;
			// 		/*$Sql="INSERT INTO dilfactu
			//         	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			//         		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			//         		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			//         		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			// 			( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebeprov2']."',
			// 				' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
			// 				'D',41,40,' ','33',' ',' ',' ',
			// 				' ',' ',' ',' ',' ',' ')";
			// 		odbc_exec($conexion2,$Sql);*/
			// 		$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
			// 		$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
			// 		$this->Cell($Dim[3],$h,$row["ctadebeprov2"],$borde,0,'C',true);
			// 		$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
			// 		$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
			// 		$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
			// 		$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
			// 	}
			// }

			//DETALLE DE PROVISION 1
			//PROVISIONES 1 Y 2
			//QUIEBRES
			//CABECERA


			$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2))
				FROM
				  facturacion.cabquiebres p
				  INNER JOIN facturacion.origen_quiebre pd ON (p.codemp = pd.codemp)
				  AND (p.codsuc = pd.codsuc)
				  AND (p.nroquiebre = pd.nroquiebre)
				  AND (p.tipo = pd.tipo)
				  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
					  AND (pd.codsuc = c.codsuc)
					  AND (pd.codconcepto = c.codconcepto)
				  WHERE p.codsuc=".$codsuc."  AND p.tipo=1 AND p.estareg =1
					AND p.anio='".$anio."' AND p.mes='".$mes."'";
			$row = $conexion->query($Sql)->fetch();
			$Total = isset($row[0])?$row[0]:0;
			if(floatval($Total)>0)
			{
				$LDOC1= "0108".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
				//BORRAR DATOS
				$Glosa="QUIEBRES";

				/*$Sql="INSERT INTO dicfactu
				            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
				            	CTOD,CTOH,DOC1,TIVO,CODDD,
				            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
								( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
									".$Total.",".$Total.",'".$LDOC1."',41,'33',
									' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
				odbc_exec($conexion2,$Sql);*/
				$this->SetFont('Arial','B',6);
				$this->SetFillColor(255,255,255); //Color de Fondo
				$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
				$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
				$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
				$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
				$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
				$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
				$this->SetFont('Arial','',6);
				//CABECERA
				//DETALLE
				$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2)),
						c.descripcion, c.ctadebequiebres,  c.ctahaberquiebres,c.codconcepto
					FROM
					  facturacion.cabquiebres p
					  INNER JOIN facturacion.origen_quiebre pd ON (p.codemp = pd.codemp)
					  AND (p.codsuc = pd.codsuc)
					  AND (p.nroquiebre = pd.nroquiebre)
					  AND (p.tipo = pd.tipo)
					  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
						  AND (pd.codsuc = c.codsuc)
						  AND (pd.codconcepto = c.codconcepto)
					  WHERE p.codsuc=".$codsuc." AND p.tipo=1
						AND p.anio='".$anio."' AND p.mes='".$mes."' AND p.estareg =1
						GROUP BY  c.descripcion,c.ctadebequiebres,c.ctahaberquiebres,c.codantiguo,c.codconcepto
				 		 ORDER BY c.codantiguo  ASC ";
				$Consulta = $conexion->query($Sql);
				$cont=0;
				$TotalDebe=0;
				foreach($Consulta->fetchAll() as $row)
				{
					$cont++;
					$concepto= utf8_decode($row[1]);
					$TotalConcepto=$row[0];
					/*$Sql="INSERT INTO dilfactu
			        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
						( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberquiebres']."',
							' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
							'H',41,40,' ','33',' ',' ',' ',
							' ',' ',' ',' ',' ',' ')";
					odbc_exec($conexion2,$Sql);*/
					$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
					$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
					$this->Cell($Dim[3],$h,$row["ctahaberquiebres"],$borde,0,'C',true);
					$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
					$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
					$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
					$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
					$cont++;
					/*$Sql="INSERT INTO dilfactu
			        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
						( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebequiebres']."',
							' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
							'D',41,40,' ','33',' ',' ',' ',
							' ',' ',' ',' ',' ',' ')";
					odbc_exec($conexion2,$Sql);*/
					$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
					$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
					$this->Cell($Dim[3],$h,$row["ctadebequiebres"],$borde,0,'C',true);
					$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
					$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
					$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
					$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
				}
			}
			//DETALLE DE PROVISION 1
			//PROVISIONES 1 Y 2
			//QUIEBRES
			//PAGO DE PROVISIONES
			// $Sql="SELECT  CAST(SUM(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2))
			// 		FROM  cobranza.cabpagos c
		  // 			INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
		  // 			AND (c.codsuc = d.codsuc)  AND (c.nropago = d.nropago)
		  // 			AND (c.nroinscripcion = d.nroinscripcion)
		  // 			INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
			//   		AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
		  // 			WHERE c.codsuc=".$codsuc." AND c.anulado=0
		  // 			AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."'
		  // 			AND d.codtipodeuda IN (10) ";
			// $row = $conexion->query($Sql)->fetch();
			// $Total = $row[0];
			// if(floatval($Total)>0)
			// 	$LDOC1= "0109".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
			// if(floatval($Total)>0)
			// {
			// 	$Glosa="REGUL. COBRANZA DE PROVISIONES DE ".substr($meses[intval($mes)],0,3)." ".$aniomin." ";
			// 	/*$Sql="INSERT INTO dicfactu
			// 	            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
			// 	            	CTOD,CTOH,DOC1,TIVO,CODDD,
			// 	            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
			// 					( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
			// 						".$Total.",".$Total.",'".$LDOC1."',41,'33',
			// 						'00000903',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
			// 	odbc_exec($conexion2,$Sql);*/
			// 	$this->SetFont('Arial','B',6);
			// 	$this->SetFillColor(255,255,255); //Color de Fondo
			// 	$this->Cell($Dim[1],$h,$LDOC1,$borde,0,'C',true);
			// 	$this->Cell($Dim[2],$h,'',$borde,0,'C',true);
			// 	$this->Cell($Dim[3]+$Dim[4],$h,$Glosa,$borde,0,'L',true);
			// 	$this->Cell($Dim[5],$h,number_format($Total,2),$borde,0,'L',true);
			// 	$this->Cell($Dim[6],$h,number_format($Total,2),$borde,0,'R',true);
			// 	$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
			// 	$this->SetFont('Arial','',6);
			// 	//CABECERA DE
			// 	//DETALLE D
			// 	$Sql="
			// 		 SELECT
			// 		   CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
			// 			co.descripcion, co.ctahaberprovcob,  co.ctadebeprovcob,co.codantiguo,co.codconcepto
			// 		FROM
			// 		  cobranza.cabpagos c
			// 		  INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
			// 		  AND (c.codsuc = d.codsuc)
			// 		  AND (c.nropago = d.nropago)
			// 		  AND (c.nroinscripcion = d.nroinscripcion)
			// 		    INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
			// 			  AND (d.codsuc = co.codsuc)
			// 			  AND (d.codconcepto = co.codconcepto)
			// 		  WHERE c.codsuc=".$codsuc." AND c.anulado=0
			// 		  AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."'
			// 		  AND d.codtipodeuda IN (10)
			// 		   GROUP BY co.descripcion, co.ctahaberprovcob,  co.ctadebeprovcob,co.codantiguo,co.codconcepto
			// 			  ORDER BY co.codantiguo  ASC";
			// 		$Consulta = $conexion->query($Sql);
			// 		$cont=0;
			// 		$TotalDebe=0;
			// 		foreach($Consulta->fetchAll() as $row)
			// 		{
			// 			$cont++;
			// 			$concepto= utf8_decode($row[1]);
			// 			$TotalConcepto=$row[0];
			// 			/*$Sql="INSERT INTO dilfactu
			// 	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			// 	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			// 	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			// 	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			// 				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberprovcob']."',
			// 					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
			// 					'H',41,40,' ','33',' ',' ',' ',
			// 					' ',' ',' ',' ',' ',' ')";
			// 			odbc_exec($conexion2,$Sql);*/
			// 			$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
			// 			$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
			// 			$this->Cell($Dim[3],$h,$row["ctahaberprovcob"],$borde,0,'C',true);
			// 			$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
			// 			$this->Cell($Dim[5],$h,'',$borde,0,'L',true);
			// 			$this->Cell($Dim[6],$h,number_format($TotalConcepto,2),$borde,0,'R',true);
			// 			$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
			// 			$cont++;
			// 			/*$Sql="INSERT INTO dilfactu
			// 	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
			// 	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
			// 	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
			// 	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			// 				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebeprovcob']."',
			// 					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
			// 					'D',41,40,' ','33',' ',' ',' ',
			// 					' ',' ',' ',' ',' ',' ')";
			// 			odbc_exec($conexion2,$Sql);*/
			// 			$this->Cell($Dim[1],$h,'',$borde,0,'C',true);
			// 			$this->Cell($Dim[2],$h,str_pad($cont,4,"0",STR_PAD_LEFT),$borde,0,'C',true);
			// 			$this->Cell($Dim[3],$h,$row["ctadebeprovcob"],$borde,0,'C',true);
			// 			$this->Cell($Dim[4],$h,$concepto,$borde,0,'L',true);
			// 			$this->Cell($Dim[5],$h,number_format($TotalConcepto,2),$borde,0,'L',true);
			// 			$this->Cell($Dim[6],$h,'',$borde,0,'R',true);
			// 			$this->Cell($Dim[6],$h,'',$borde,1,'R',true);
			// 		}
			// 	}

			//DETALLE
			//PAGO DE PROVISIONES

			$this->SetFont('Arial','B',6);

			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->Cell(0,0.01,'',1,1,'C',true);
			$this->Cell($Dim[1]+$Dim[2]+$Dim[3]+$Dim[4],$h,'Total General',$borde,0,'C',true);
			$this->Cell($Dim[5],$h,number_format($TotalG,2),$borde,0,'L',true);
			$this->Cell($Dim[6],$h,number_format($TotalG,2),$borde,0,'R',true);
			$this->Cell($Dim[6],$h,number_format(0,2),$borde,1,'R',true);
			$this->SetFont('Arial','',6);
			$this->Cell(0,0.01,'',1,1,'C',true);

		}

		function FechaDoc($cod,$serie,$numero) {

			global $conexion,$codsuc;
			$Sql ="SELECT fechareg FROM facturacion.cabfacturacion
					WHERE codsuc=".$codsuc." AND coddocumento=".$cod."
					AND serie='".$serie."' AND nrodocumento='".$numero."'";
			$c = $conexion->query($Sql)->fetch();
			return $this->DecFecha($c[0]);
		}

		function EquivalenteDoc($cod,$tipo) {

			if($tipo==1) :
				switch ($cod)
				{
					case 4: return "14";break;
					case 13: return "03";break;
					case 14: return "01";break;
					case 17: return "07";break;
				}
			else :
				switch ($cod)
				{
					case 1: return "1";break;
					case 3: return "6";break;
					default: return "0";break;
				}
			endif;
				/*
				COMPROBANTES
				-----------------------------
				14  Recibos de Pensiones
				01  Facturas
				03  Boletas de Ventas
				07  Notas de Abono/Credito
				08  Notas de cargo/Debito


				DOCUMENTOS
				-----------------------------
				1  DNI
				6  RUC
				0  Otros*/
			}

	}

	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc		= $_GET["codsuc"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclotext	= $_GET["ciclotext"];
	if($_GET["sector"]!="%"){$Csec=" AND cl.codsector=".$_GET["sector"];}else{$Csec="";}
	$m = $meses[$mes];
	$Dim = array('1'=>20,'2'=>20,'3'=>20,'4'=>65,'5'=>25,'6'=>20,'7'=>20,'8'=>20,'9'=>20);
/*	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$caja		= $_GET["caja"];
	$cars		= $_GET["cars"];
	$cajeros 	= $_SESSION['nombre'];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$totagua 		= 0;
	$totdesague		= 0;
	$totcargofijo	= 0;
	$totinteres		= 0;
	$totredondeo	= 0;
	$tototros		= 0;
	$totigv			= 0;
	$totalT			= 0;*/

	$objReporte	=	new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage("");

	$objReporte->agregar_detalle($codsuc,$ciclo,$anio,$mes,$Csec);


	$objReporte->Output();

?>
