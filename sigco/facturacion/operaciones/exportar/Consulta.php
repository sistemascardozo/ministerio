<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsDrop.php");

$objMantenimiento = new clsDrop();

$codsuc = $_SESSION["IdSucursal"];
$Desde = $objMantenimiento->CodFecha($_POST["Desde"]);
$Hasta = $objMantenimiento->CodFecha($_POST["Hasta"]);

$Condicion = " AND cab.fechareg BETWEEN CAST('".$Desde."' AS DATE) AND CAST('".$Hasta."' AS DATE) ";

$Sql = "SELECT cab.car,car.descripcion
			FROM cobranza.cabpagos AS cab
			INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc 
			AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago
			INNER JOIN cobranza.car AS car ON cab.codemp = car.codemp AND cab.codsuc = car.codsuc 
			AND cab.car = car.car
			WHERE cab.codemp = 1 AND cab.codsuc = ".$codsuc.$Condicion." AND cab.nropec = 0
			ORDER BY cab.fechareg,cab.car,car.descripcion";
//$ConsultaC = $conexion->query($Sql);
//foreach($ConsultaC->fetchAll() as $rowC)
//{
?>
<div>

    <?php
    $Sql = "SELECT cab.nropec FROM cobranza.cabpagos AS cab 
        WHERE cab.codemp = 1 AND cab.codsuc = ".$codsuc.$Condicion." AND
        cab.nropec = 0 AND cab.car=".$rowC[0]."

        ORDER BY  cab.nropec ASC ";
    //$ConsultaP = $conexion->query($Sql);
    //foreach($ConsultaP->fetchAll() as $rowP)
    //{
    ?>
                                        
    <table class="ui-widget" border="0" cellspacing="0" width="100%" rules="rows" align="center">
        <thead style="font-size:12px;">	
            <tr>
                <th width="40">&nbsp;</th>
                <td  width="80">&nbsp;</td>
                <td align="center">
                    <table border="1" cellspacing="0" width="100%" rules="rows" align="left">
                        <tr>
                            <td width="100%" valign="top">
                                <table class="ui-widget" border="0" cellspacing="0" width="100%" rules="rows" align="left">
                                    <thead class="ui-widget-header" style="font-size:10px">
                                        <tr>
                                            <th colspan="5">Detalle de Ingresos</th>
                                        </tr>
                                        <tr>
                                            <th width="20" scope="col">C&oacute;digo</th>
                                            <th width="20" scope="col">Concepto</th>
                                            <th width="20" scope="col">N&deg; Cuenta</th>
                                            <th width="20" scope="col">Debe</th>
                                            <th width="20" scope="col">Haber</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $TotalPec = 0; //02039634,
                                        $TotalD = 0;
                                        $TotalH = 0;
                                        $Sql = "SELECT con.ctadebe,sum(CASE WHEN det.importe <= 0 AND con.signo='-' THEN det.importe*(-1)  ELSE det.importe END) AS Totales,
                                            con.descripcion,con.ordenrecibo,con.signo, con.codantiguo
                                            FROM cobranza.cabpagos AS cab 
                                            INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago 
                                            INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto 
                                            WHERE cab.codemp = 1 AND cab.codsuc = ".$codsuc.$Condicion." AND
                                                            /*cab.car=".$rowC[0]." AND cab.nropec =".$rowP[0]." AND*/  cab.anulado=0
                                            GROUP BY con.ctadebe,con.descripcion,con.ordenrecibo ,con.signo,codantiguo
                                            ORDER BY con.codantiguo  ASC ";
                                        $ConsultaT = $conexion->query($Sql);
                                        foreach ($ConsultaT->fetchAll() as $rowT) {
                                            $TotalPec+=$rowT[1];
                                            $TotalCuenta = $rowT[1];
                                            $D = $TotalCuenta;
                                            $H = $TotalCuenta;
                                            if ($rowT['signo'] == '-') {
                                                $flag = 1;
                                                $tipocta = 'D';
                                                $TotalD +=$TotalCuenta;
                                                $H = 0;
                                            } else {
                                                $D = 0;
                                                $TotalH +=$TotalCuenta;
                                            }
                                            // if($D<0) $D = $D*(-1);
                                            //if($H<0) $D = $H*(-1);
                                            ?>
                                            <tr>
                                                <td width="20"><?=$rowT['codantiguo'] ?></td>
                                                <td width="20"><?=$rowT[2] ?></td>
                                                <td width="20"><?=$rowT[0] ?></td>
                                                <td width="20" align="right"><?=number_format($D, 2) ?></td>
                                                <td width="20" align="right"><?=number_format($H, 2) ?></td>
                                            </tr>
                                        <?php
                                    }
                                    //TOTAL A CAJA
                                    $Sql = "SELECT sum( det.importe) AS Totales
                                        FROM cobranza.cabpagos AS cab 
                                        INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago 
                                        INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto 
                                        WHERE cab.codsuc = ".$codsuc.$Condicion." AND  cab.anulado=0 ";
                                    $ConsultaT = $conexion->query($Sql);
                                    foreach ($ConsultaT->fetchAll() as $rowT) {
                                        //$TotalPec+=$rowT[1];
                                        $NroCuentaConcepto = '101111101'; //$rowT[0];
                                        $TotalCuenta = $rowT[0];
                                        $cont++;
                                        $concepto = 'CAJA';
                                        $codconcepto = '';
                                        $TotalD +=$TotalCuenta;
                                        ?>
                                            <tr>
                                                <td width="20">&nbsp;</td>
                                                <td width="20"><?=$concepto ?></td>
                                                <td width="20"><?=$NroCuentaConcepto ?></td>
                                                <td width="20" align="right"><?=number_format($TotalCuenta, 2) ?></td>
                                                <td width="20" align="right"><?=number_format(0, 2) ?></td>
                                            </tr>
                                            <?php


                                            }

                                            ?>
                                        <tfoot class="ui-widget-header" style="font-size:10px">
                                            <tr>
                                                <td align="right" colspan="3">Total:</td>
                                                <td align="right" ><?=number_format($TotalD, 2) ?></td>
                                                <td align="right" ><?=number_format($TotalH, 2) ?></td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="4">DIF:</td>

                                                <td align="right" ><?=number_format($TotalD - $TotalH, 2) ?></td>
                                            </tr>
                                        </tfoot>
                                        </tbody>
                                    </table>
                                </td>
                               
                            </tr>
                    </td>
                </tr>
            </thead>
        </table>
    <?php
    //}
    ?>
    </div>
        <?php
        //}

        /*
          SELECT con.codconcepto,det.nropago,det.importe
          FROM cobranza.cabpagos AS cab INNER JOIN cobranza.detpagos AS det ON det.codemp = cab.codemp
          AND det.codsuc = cab.codsuc AND det.nroinscripcion = cab.nroinscripcion AND det.nropago = cab.nropago INNER JOIN facturacion.conceptos AS con ON det.codemp = con.codemp
          AND det.codsuc = con.codsuc AND det.codconcepto = con.codconcepto WHERE cab.codemp = 1 AND cab.codsuc = 1 AND cab.fechareg BETWEEN CAST ('2014-11-12' AS DATE) AND CAST ( '2014-11-30' AS DATE ) AND  cab.anulado=0
          AND con.signo='-' and det.importe>0 group by con.codconcepto,det.nropago,det.importe --ORDER BY con.codantiguo ASC

          select * from facturacion.detfacturacion where codsuc=3 and codconcepto=8 and importe<0
          update facturacion.detfacturacion set codconcepto=7 where codsuc=3 and codconcepto=8 and importe<0
         */
        ?>
	