<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../objetos/clsDrop.php");

	$objMantenimiento = new clsDrop();

	$codsuc		= $_POST["codsuc"];
	$anio		= $_POST["anio"];
	$ciclo		= $_POST["ciclo"];
	$mes		= intval($_POST["mes"]);
	$Loca 		= "0".$codsuc;

	$conexion2 = odbc_connect("ODBC_AVALON_FACT", "", "") or die ("No se pudo conectar con DBF");

	ereg( "([0-9]{2,4})/([0-9]{1,4})/([0-9]{1,4})",$_POST["Desde"], $mifecha);
    $Fecha = $mifecha[2].substr($mifecha[3],2,2);

	$factoct = '240';
	
	$RutaDisco = trim('D:\E-SIINCOWEB\AVALON\FACTU\ ');
	
    $NombreCab = "twc".str_pad($mes, 2, "0", STR_PAD_LEFT).substr($anio, 2).".dbf"; //"dicfactu.dbf";
	$NombreDet = "twl".str_pad($mes, 2, "0", STR_PAD_LEFT).substr($anio, 2).".dbf"; //"dilfactu.dbf";
	
    $dicfactu = trim($RutaDisco).$NombreCab;
	$dilfactu = trim($RutaDisco).$NombreDet;
	
	/*if(file_exists($dicfactu))
	{
		unlink($dicfactu);
	}
	$dilfactu = trim($RutaDisco)."dilfactu.dbf";
	if(file_exists($dilfactu))
	{
		unlink($dilfactu);
	}*/

    //
    function DeleteReg($LDOC1)
    {
    	//echo $LDOC1."</br>";
    	global $conexion2;
    	$Sql="DELETE FROM dicfactu WHERE LDOC1 in ('".$LDOC1."')";
		odbc_exec($conexion2,$Sql);
		$Sql="DELETE FROM dilfactu WHERE LDOC1 in ('".$LDOC1."')";
		odbc_exec($conexion2,$Sql);
    }
    if(!file_exists($dicfactu))
    {
	    $archivo = "dicfactu.dbf";
	    if (!copy($archivo, $dicfactu))
	    {
	   		echo "Error al copiar $archivo...\n";
		}
	}
	if(!file_exists($dilfactu))
	{
		$archivo = "dilfactu.dbf";
	    if (!copy($archivo, $dilfactu))
	    {
	   		echo "Error al copiar $archivo...\n";
		}
	}

	//DATOS DE LA FACTURACION
	$sql = "select nrofacturacion,fechaemision
			from facturacion.periodofacturacion
			where codemp=1 and codsuc=? and codciclo=? AND anio=? AND mes=?";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$ciclo,$anio,$mes));
	$row = $consulta->fetch();
	$nrofacturacion=$row['nrofacturacion'];
	$fechaemision=$row['fechaemision'];

    //ORDEN LCL, 1 REG, TEC,1REG, TEL, DETALLE 2103280
	// if(intval($mes)<10) $mes="0".$mes;
	$aniomin = substr($anio,2, 2);
	$count=0;
	//QUITAR
	$TotalRestar=0;

	//CABECERA DE FACTURACION
     $Sql="	SELECT
										cli.codzona as codzona,
										z.descripcion as zona,
										CAST(sum(CASE WHEN d.importe <= 0 AND c.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)) as cantidad
									FROM facturacion.detctacorriente d
										JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto)
										JOIN catastro.clientes cli ON (d.codemp = cli.codemp) AND (d.codsuc = cli.codsuc) AND (d.nroinscripcion = cli.nroinscripcion)
										JOIN admin.zonas z ON (cli.codzona = z.codzona) AND (cli.codemp = z.codemp) AND (cli.codsuc = z.codsuc)
									WHERE d.codsuc=".$codsuc." and d.nrofacturacion=".$nrofacturacion."
										AND ((d.nrocredito = 0 and d.nrorefinanciamiento = 0) OR d.codconcepto = 9 OR  d.codtipodeuda in (3, 4, 7,9))
										AND d.tipo=0 AND d.tipoestructura=0 AND d.periodo='".$anio.$mes."'
										AND d.codconcepto<>10012 and d.codtipodeuda not in (3, 4, 7)
									GROUP BY cli.codzona, z.descripcion
									ORDER BY cli.codzona";

	$row = $conexion->query($Sql)->fetchAll();

	foreach ($row as $indice => $valores) :
	
	$Total = $row[0] - $TotalRestar;
	$LDOC1 = "0101".$aniomin.str_pad($mes, 2, "0", STR_PAD_LEFT)."01";
	
	//BORRAR DATOS
	DeleteReg($LDOC1);

	$Glosa = "FACT - ".substr($meses[intval($mes)], 0, 3)." CORONEL PORTILLO," . $aniomin." Distrito : " . $valores['zona'];

	$Sql = "INSERT INTO ".$NombreCab."
	            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
	            	CTOD,CTOH,DOC1,TIVO,CODDD,
	            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
					( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
						".$Total.",".$Total.",'".$LDOC1."',31,'64',
						' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
	odbc_exec($conexion2,$Sql);
	//CABECERA DE FACTURACION
	//DETALLE DE FACTURACION
	$Sql="SELECT CAST(sum(CASE WHEN d.importe <= 0 AND c.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
		c.descripcion, c.ctadebe,  c.ctahaber,c.codconcepto
		FROM  facturacion.detctacorriente d
		  INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)
		  AND (d.codsuc = c.codsuc)
		  AND (d.codconcepto = c.codconcepto)
		  WHERE d.codsuc=".$codsuc." and d.nrofacturacion=".$nrofacturacion."
						and ((d.nrocredito = 0 and d.nrorefinanciamiento = 0) OR d.codconcepto = 9 OR d.codtipodeuda in (3, 4, 7,9))
						AND d.codconcepto<>10012 and d.codtipodeuda not in (3, 4, 7)
		AND d.tipo=0 AND d.tipoestructura=0 AND d.periodo='".$anio.$mes."' AND cli.codzona = " . $valores['codzona'] . " 
		  GROUP BY  c.descripcion,c.ctadebe,c.ctahaber,c.codantiguo,c.codconcepto
		  ORDER BY c.codantiguo  ASC";


	$Consulta = $conexion->query($Sql);
	$cont=0;
	$TotalDebe=0;
	foreach($Consulta->fetchAll() as $row)
	{
		$cont++;
		$concepto= $row[1];
		$restar=isset($Conceptos2[$row['codconcepto']]["Importe"])?$Conceptos2[$row['codconcepto']]["Importe"]:0;
		$TotalConcepto=$row[0]-floatval($restar);
		$Sql="INSERT INTO ".$NombreDet."
        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaber']."',
				' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
				'H',31,40,' ','64',' ',' ',' ',
				' ',' ',' ',' ',' ',' ')";
		odbc_exec($conexion2,$Sql);
		$cont++;
		$Sql="INSERT INTO ".$NombreDet."
        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebe']."',
				' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
				'D',31,40,' ','64',' ',' ',' ',
				' ',' ',' ',' ',' ',' ')";
		odbc_exec($conexion2,$Sql);
	}

	endforeach;


	//DETALLE DE FACTURACION
	//REBAJAS
	$Sql="SELECT CAST(SUM(CASE WHEN d.imprebajado <= 0 AND co.signo='-' THEN d.imprebajado*(-1)  ELSE d.imprebajado END) AS NUMERIC (18,2))
			FROM   facturacion.cabrebajas c
			INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp)
			AND (c.codsuc = d.codsuc)  AND (c.nrorebaja = d.nrorebaja)
			INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
	  AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
			  WHERE c.codsuc=".$codsuc." AND c.estareg=1 AND c.anio='".$anio."'
			  AND c.mes='".intval($mes)."'";
	$row = $conexion->query($Sql)->fetch();
	$Total = $row[0];
	$LDOC1= "0102".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
	//BORRAR DATOS
	DeleteReg($LDOC1);

	$Glosa = "NOTA ABONO - ".substr($meses[intval($mes)],0,3)." ".$aniomin." ";

	$Sql = "INSERT INTO ".$NombreCab."
	            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
	            	CTOD,CTOH,DOC1,TIVO,CODDD,
	            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
					( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
						".$Total.",".$Total.",'".$LDOC1."',31,'64',
						' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
	odbc_exec($conexion2,$Sql);
	//REBAJAS
	//DETALLE DE REBAJAS
	$Sql="SELECT CAST(sum(CASE WHEN d.imprebajado <= 0 AND co.signo='-' THEN d.imprebajado*(-1)  ELSE d.imprebajado END) AS NUMERIC (18,2)),
	co.descripcion, co.ctahaberrebajas,  co.ctadeberebajas
	FROM facturacion.cabrebajas c
	INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp)
	AND (c.codsuc = d.codsuc)  AND (c.nrorebaja = d.nrorebaja)
	 INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
	  AND (d.codsuc = co.codsuc)
	  AND (d.codconcepto = co.codconcepto)
			  WHERE c.codsuc=".$codsuc." AND c.estareg=1 AND c.anio='".$anio."'
			  AND c.mes='".intval($mes)."'
	  GROUP BY co.descripcion, co.ctahaberrebajas,  co.ctadeberebajas,co.codantiguo
	  ORDER BY co.codantiguo  ASC";
	$Consulta = $conexion->query($Sql);
	$cont=0;
	$TotalDebe=0;
	foreach($Consulta->fetchAll() as $row)
	{
		$cont++;
		$concepto= $row[1];
		$TotalConcepto=$row[0];
		if($TotalConcepto>0)
		{
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberrebajas']."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'H',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
			$cont++;
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadeberebajas']."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'D',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
		}
	}
	//DETALLE DE REBAJAS
	//CABECERA DE CARGOS O INTERESES
	//CREDITOS
	$Sql="SELECT CAST(SUM(d.interes) AS NUMERIC (18,2))
			FROM facturacion.cabcreditos c
			INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp)
			AND (c.codsuc = d.codsuc)  AND (c.nrocredito = d.nrocredito)
			  WHERE c.codsuc=".$codsuc." AND c.estareg=1 AND c.sininteres=0 AND c.nroprepagoinicial>0
			  AND  extract(year from c.fechareg) ='".$anio."'
			  AND extract(month from c.fechareg)  ='".intval($mes)."'";
	$row = $conexion->query($Sql)->fetch();
	$Total = $row[0];
	//FINANCIAMIENTOS
	$Sql = "SELECT CAST(SUM(d.importe)-c.totalrefinanciado AS NUMERIC (18, 2)),c.nroinscripcion
			FROM facturacion.cabrefinanciamiento c
		  INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp)
		  AND (c.codsuc = d.codsuc)
		  AND (c.nrorefinanciamiento = d.nrorefinanciamiento)
		where d.codemp=1 and d.codsuc=".$codsuc." AND c.estareg=1 AND c.sininteres=0 AND c.nroprepagoinicial>0
				AND  extract(year from c.fechaemision) ='".$anio."'
				AND extract(month from c.fechaemision)  ='".intval($mes)."' AND c.codestadorefinanciamiento<>5
		  GROUP BY c.totalrefinanciado,c.nroinscripcion";
	$Consulta = $conexion->query($Sql);
	foreach($Consulta->fetchAll() as $row)
	{
		$Total += $row[0];
	}

	//FINANCIAMIENTOS
	$LDOC1= "0103".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";

	DeleteReg($LDOC1);

	$Glosa = "NOTA CARGO - ".substr($meses[intval($mes)],0,3)." ".$aniomin." ";

	$Sql="INSERT INTO ".$NombreCab."
	            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
	            	CTOD,CTOH,DOC1,TIVO,CODDD,
	            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
					( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
						".$Total.",".$Total.",'".$LDOC1."',31,'64',
						' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
	odbc_exec($conexion2,$Sql);
	//CABECERA DE CARGOS O INTERESES
	//DETALLE DE CARGOS O INTERESES
	$Sql="SELECT co.descripcion, co.ctahaber,  co.ctadebe
	FROM  facturacion.conceptos co
	  WHERE co.codsuc=".$codsuc."  AND co.codconcepto=9";

	$Consulta = $conexion->query($Sql);
	$cont=0;
	$TotalDebe=0;
	foreach($Consulta->fetchAll() as $row)
	{
		$cont=1;
		$concepto = 'INTERESES POR DEUDA';
		$TotalConcepto = $Total;
		
		$Sql = "INSERT INTO ".$NombreDet."
        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaber']."',
				' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
				'H',31,40,' ','64',' ',' ',' ',
				' ',' ',' ',' ',' ',' ')";
		odbc_exec($conexion2,$Sql);
		
		$cont++;
		$Sql="INSERT INTO ".$NombreDet."
        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
			( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebe']."',
				' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
				'D',31,40,' ','64',' ',' ',' ',
				' ',' ',' ',' ',' ',' ')";
		odbc_exec($conexion2,$Sql);

	}
	//DETALLE DE CARGOS O INTERESES
	//CABECERA DE REG CONVENIO
	//PENSIONES
	$Total=0;
	$Sql="SELECT o.codsuc,  o.nrorefinanciamiento,  o.codciclo,  o.nrofacturacion,  o.nroinscripcion
		FROM  facturacion.cabrefinanciamiento c
	  INNER JOIN facturacion.origen_refinanciamiento o ON (c.codemp = o.codemp)
	  AND (c.codsuc = o.codsuc)
	  AND (c.nrorefinanciamiento = o.nrorefinanciamiento)
	  AND (c.nroinscripcion = o.nroinscripcion)
	  INNER JOIN facturacion.cabfacturacion ON (o.codsuc = facturacion.cabfacturacion.codsuc)
	  AND (o.codemp = facturacion.cabfacturacion.codemp)
	  AND (o.codsuc = facturacion.cabfacturacion.codsuc)
	  AND (o.codciclo = facturacion.cabfacturacion.codciclo)
	  AND (o.nrofacturacion = facturacion.cabfacturacion.nrofacturacion)
	  AND (o.nroinscripcion = facturacion.cabfacturacion.nroinscripcion)
		where c.codemp=1 and c.codsuc=".$codsuc." AND c.estareg=1
		AND  extract(year from c.fechaemision) ='".$anio."'
		AND extract(month from c.fechaemision)  ='".intval($mes)."'
		GROUP BY o.codsuc,o.nrorefinanciamiento, o.codciclo,  o.nrofacturacion,  o.nroinscripcion
	  ORDER BY o.nrorefinanciamiento,o.nrofacturacion";
	$Consulta = $conexion->query($Sql);
	$Conceptos = array();
	foreach($Consulta->fetchAll() as $row)
	{
		//
		$Sql="SELECT CAST(sum(CASE WHEN d.importe <= 0 AND c.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
		c.descripcion, c.ctadebe,  c.ctahaber,c.codconcepto,c.signo
		FROM  facturacion.detfacturacion d
		INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)
		AND (d.codsuc = c.codsuc)  AND (d.codconcepto = c.codconcepto)
		WHERE d.codsuc=".$codsuc." and d.nrofacturacion=".$row['nrofacturacion']."
			AND d.codciclo=".$row['codciclo']." and d.nroinscripcion=".$row['nroinscripcion']."
		  GROUP BY  c.descripcion,c.ctadebe,c.ctahaber,c.codantiguo,c.codconcepto, c.signo
		  ORDER BY c.codantiguo  ASC";
		$Consulta2 = $conexion->query($Sql);

		foreach($Consulta2->fetchAll() as $row2)
		{
			if (!array_key_exists($row2['codconcepto'], $Conceptos))
			{
				$Conceptos[$row2['codconcepto']] = array(
														"Id"=>$row2['codconcepto'],
														"Importe"=>$row2[0],
														"Descripcion"=>$row2[1],
														"CtaDebe"=>$row2['ctadebe'],
														"Signo"=>$row2['signo'],
														"CtaHaber"=>$row2['ctahaber']);
			}
			else
			{
				$Conceptos[$row2['codconcepto']] = array(
															"Id"=>$row2['codconcepto'],
															"Descripcion"=>$row2[1],
															"CtaDebe"=>$row2['ctadebe'],
															"CtaHaber"=>$row2['ctahaber'],
															"Signo"=>$row2['signo'],
															"Importe"=>$Conceptos[$row2['codconcepto']]["Importe"] + $row2[0]);
			}
			if($row2['signo']=='+')
				$Total+=$row2[0];
		}
		//
	}
	//print_r($Conceptos);
	//die($Total);
	//PENSIONES
	//COLATERALES

	$Sql="SELECT CAST(sum(CASE WHEN d.imptotal <= 0 AND co.signo='-' THEN d.imptotal*(-1)  ELSE d.imptotal END) AS NUMERIC (18,2)),
		co.descripcion, co.ctadebe,  co.ctahaber,co.codconcepto,co.signo
		FROM    facturacion.cabrefinanciamiento c
  		INNER JOIN facturacion.detcabrefinanciamiento d ON (c.codemp = d.codemp)
  		AND (c.codsuc = d.codsuc) AND (c.nrorefinanciamiento = d.nrorefinanciamiento)
  		INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
  		AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
		where c.codemp=1 and c.codsuc=".$codsuc." AND c.estareg=1
		AND  extract(year from c.fechaemision) ='".$anio."'
		AND extract(month from c.fechaemision)  ='".intval($mes)."'
		  GROUP BY  co.descripcion,co.ctadebe,co.ctahaber,co.codantiguo,co.codconcepto, co.signo
		  ORDER BY co.codantiguo  ASC";
		$Consulta2 = $conexion->query($Sql);
		foreach($Consulta2->fetchAll() as $row2)
		{
			if (!array_key_exists($row2['codconcepto'], $Conceptos))
			{
				$Conceptos[$row2['codconcepto']] = array(
														"Id"=>$row2['codconcepto'],
														"Importe"=>$row2[0],
														"Descripcion"=>$row2[1],
														"CtaDebe"=>$row2['ctadebe'],
														"Signo"=>$row2['signo'],
														"CtaHaber"=>$row2['ctahaber']);
			}
			else
			{
				$Conceptos[$row2['codconcepto']] = array(
															"Id"=>$row2['codconcepto'],
															"Descripcion"=>$row2[1],
															"CtaDebe"=>$row2['ctadebe'],
															"CtaHaber"=>$row2['ctahaber'],
															"Signo"=>$row2['signo'],
															"Importe"=>$Conceptos[$row2['codconcepto']]["Importe"] + $row2[0]);
			}
			if($row2['signo']=='+')
				$Total+=$row2[0];
		}
	$Total = $Total +$TotalRestar;
	$LDOC1= "0104".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
	DeleteReg($LDOC1);
	$Glosa = "REGULARIZACION POR CONVENIO";
	$Sql="INSERT INTO ".$NombreCab."
	            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
	            	CTOD,CTOH,DOC1,TIVO,CODDD,
	            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
					( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
						".$Total.",".$Total.",'".$LDOC1."',31,'64',
						'00000903',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
	odbc_exec($conexion2,$Sql);
	//COLATERALES
	//CABECERA DE CONVENIO
	//DETALLE DE CONVENIO
	sort($Conceptos);
	//print_r($Conceptos);
	$cont=0;
	$TotalDebe=0;
	for($i=0;$i<count($Conceptos);$i++)
	{
		$cont++;
		$concepto= $Conceptos[$i]["Descripcion"];

		$restar=isset($Conceptos2[$Conceptos[$i]["Id"]]["Importe"])?$Conceptos2[$Conceptos[$i]["Id"]]["Importe"]:0;
		$TotalConcepto=$Conceptos[$i]["Importe"];
		$TotalConcepto=$TotalConcepto+floatval($restar);
		if($TotalConcepto>0)
		{
			if($Conceptos[$i]['Signo']=='+')
			{//echo '==>'.$Conceptos[$i]['Signo'];
				$Sql="INSERT INTO ".$NombreDet."
		        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
		        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
		        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
		        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
					( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$Conceptos[$i]["CtaDebe"]."',
						' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
						'H',31,40,' ','64',' ',' ',' ',
						' ',' ',' ',' ',' ',' ')";
			}
			else
			{
				$TotalDebe+=$TotalConcepto;
				$Sql="INSERT INTO ".$NombreDet."
		        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
		        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
		        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
		        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
					( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$Conceptos[$i]["CtaHaber"]."',
						' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
						'D',31,40,' ','64',' ',' ',' ',
						' ',' ',' ',' ',' ',' ')";

			}
			odbc_exec($conexion2,$Sql);
		}
	}
	$cont++;
	$TotalConcepto=$Total-$TotalDebe;
	$concepto='TOTAL FACTURA';
	$Sql="INSERT INTO ".$NombreDet."
    	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
    		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
    		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
    		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
		( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','121105101',
			' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
			'D',31,40,' ','64',' ',' ',' ',
			' ',' ',' ',' ',' ',' ')";
	odbc_exec($conexion2,$Sql);
	//DETALLE DE CONVENIO

	//CABECERA DE BOLETAS Y FACTURAS
	//CABPREPAGOS
	$Sql="SELECT CAST(SUM(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2))
			FROM  cobranza.cabprepagos c
  			INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
  			AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
  			AND (c.nroinscripcion = d.nroinscripcion)
  			INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
	  AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
  			WHERE c.codsuc=".$codsuc." AND c.estado<>0 AND c.estado<>2
  			AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."' AND d.coddocumento IN (13,14) AND co.codconcepto<>10005";
	$row = $conexion->query($Sql)->fetch();
	$Total = $row[0];
	//PAGOS
	$Sql="SELECT CAST(SUM(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2))
			FROM  cobranza.cabpagos c
  			INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
  			AND (c.codsuc = d.codsuc)  AND (c.nropago = d.nropago)
  			AND (c.nroinscripcion = d.nroinscripcion)
  			INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
	  AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
  			WHERE c.codsuc=".$codsuc." AND c.anulado=0 AND c.car <> 9
  			AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."' AND d.coddocumento IN (13,14) AND co.codconcepto<>10005";
	$row = $conexion->query($Sql)->fetch();
	$Total += $row[0];
	$LDOC1= "0105".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
	
	DeleteReg($LDOC1);
	
	$Glosa="COLATERALES DE ".substr($meses[intval($mes)],0,3)." ".$aniomin." ";
	
	$Sql="INSERT INTO ".$NombreCab."
	            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
	            	CTOD,CTOH,DOC1,TIVO,CODDD,
	            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
					( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
						".$Total.",".$Total.",'".$LDOC1."',31,'64',
						'00000903',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
	odbc_exec($conexion2,$Sql);
	//CABECERA DE BOLETAS Y FACTURAS
	//DETALLE DE BOLETAS Y FACTURAS
	$Sql="(SELECT
		  CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
			co.descripcion, co.ctahaber,  co.ctadebe,co.codantiguo,co.codconcepto,1
		FROM  cobranza.cabprepagos c
		  INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
		  AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
		  AND (c.nroinscripcion = d.nroinscripcion)
		   INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
			  AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
		  WHERE c.codsuc=".$codsuc." AND c.estado<>0 AND c.estado<>2
		  AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."'
		   AND d.coddocumento IN (13,14) AND co.codconcepto<>10005
		  GROUP BY co.descripcion, co.ctahaber,  co.ctadebe,co.codantiguo,co.codconcepto
			  ORDER BY co.codantiguo  ASC
		)
		UNION
		(
		 SELECT
		   CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
			co.descripcion, co.ctahaber,  co.ctadebe,co.codantiguo,co.codconcepto,2
		FROM
		  cobranza.cabpagos c
		  INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
		  AND (c.codsuc = d.codsuc)
		  AND (c.nropago = d.nropago)
		  AND (c.nroinscripcion = d.nroinscripcion)
		    INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
			  AND (d.codsuc = co.codsuc)
			  AND (d.codconcepto = co.codconcepto)
		  WHERE c.codsuc=".$codsuc." AND c.anulado=0 AND c.car <> 9
		  AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."'
		  AND d.coddocumento IN (13,14) AND co.codconcepto<>10005
		   GROUP BY co.descripcion, co.ctahaber,  co.ctadebe,co.codantiguo,co.codconcepto
			  ORDER BY co.codantiguo  ASC)
		 ORDER BY codantiguo, descripcion, ctahaber,  ctadebe";
	$Consulta = $conexion->query($Sql);
	$Conceptos = array();
	foreach($Consulta->fetchAll() as $row)
	{
		if (!array_key_exists($row['codconcepto'], $Conceptos))
		{
			$Conceptos[$row['codconcepto']] = array(
													"Id"=>$row['codconcepto'],
													"Importe"=>$row[0],
													"Descripcion"=>$row[1],
													"CtaDebe"=>$row['ctadebe'],
													"CtaHaber"=>$row['ctahaber']);
		}
		else
		{
			$Conceptos[$row['codconcepto']] = array(
														"Id"=>$row['codconcepto'],
														"Descripcion"=>$row[1],
														"CtaDebe"=>$row['ctadebe'],
														"CtaHaber"=>$row['ctahaber'],
														"Importe"=>$Conceptos[$row['codconcepto']]["Importe"] + $row[0]);
		}
	}


	sort($Conceptos);
	//print_r($Conceptos);
	$cont=0;
	$TotalDebe=0;
	for($i=0;$i<count($Conceptos);$i++)
	{
		$cont++;
		$concepto= $Conceptos[$i]["Descripcion"];
		$TotalConcepto=$Conceptos[$i]["Importe"];
		if($TotalConcepto>0)
		{
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$Conceptos[$i]["CtaHaber"]."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'H',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
			$cont++;
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$Conceptos[$i]["CtaDebe"]."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'D',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
		}
	}
	//DETALLE DE BOLETAS Y FACTURAS
	//PROVISIONES 1 Y 2
	//CABECERA DE PROVISION1
	$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2))
		FROM
		  facturacion.provisiones p
		  INNER JOIN facturacion.provisiones_detalle pd ON (p.codemp = pd.codemp)
		  AND (p.codsuc = pd.codsuc)
		  AND (p.codprovision = pd.codprovision)
		  AND (p.tipo = pd.tipo)
		  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
			  AND (pd.codsuc = c.codsuc)
			  AND (pd.codconcepto = c.codconcepto)
		  WHERE p.codsuc=".$codsuc." and  p.estadoprovision=2 AND p.tipo=1
			AND p.anio='".$anio."' AND p.mes='".$mes."' ";

	$row = $conexion->query($Sql)->fetch();
	$Total = isset($row[0])?$row[0]:0;
	$LDOC1= "0106".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
		//BORRAR DATOS
	DeleteReg($LDOC1);
	if(floatval($Total)>0)
	{

		$Glosa="PROVISION ASIENTO 1 ";

		$Sql="INSERT INTO ".$NombreCab."
		            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
		            	CTOD,CTOH,DOC1,TIVO,CODDD,
		            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
						( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
							".$Total.",".$Total.",'".$LDOC1."',31,'64',
							' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
		odbc_exec($conexion2,$Sql);
		//CABECERA DE PROVISION1
		//DETALLE DE PROVISION1
		$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2)),
				c.descripcion, c.ctadebeprov,  c.ctahaberprov,c.codconcepto
			FROM
			  facturacion.provisiones p
			  INNER JOIN facturacion.provisiones_detalle pd ON (p.codemp = pd.codemp)
			  AND (p.codsuc = pd.codsuc)
			  AND (p.codprovision = pd.codprovision)
			  AND (p.tipo = pd.tipo)
			  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
				  AND (pd.codsuc = c.codsuc)
				  AND (pd.codconcepto = c.codconcepto)
			  WHERE p.codsuc=".$codsuc." and  p.estadoprovision=2 AND p.tipo=1
				AND p.anio='".$anio."' AND p.mes='".$mes."'
				GROUP BY  c.descripcion,c.ctadebeprov,c.ctahaberprov,c.codantiguo,c.codconcepto
		 		 ORDER BY c.codantiguo  ASC ";
		$Consulta = $conexion->query($Sql);
		$cont=0;
		$TotalDebe=0;
		foreach($Consulta->fetchAll() as $row)
		{
			$cont++;
			$concepto= $row[1];
			$TotalConcepto=$row[0];
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberprov']."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'H',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
			$cont++;
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebeprov']."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'D',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
		}
		//DETALLE DE PROVISION 1
		//CABECERA DE PROVISION2
		$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2))
			FROM
			  facturacion.provisiones p
			  INNER JOIN facturacion.provisiones_detalle pd ON (p.codemp = pd.codemp)
			  AND (p.codsuc = pd.codsuc)
			  AND (p.codprovision = pd.codprovision)
			  AND (p.tipo = pd.tipo)
			  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
				  AND (pd.codsuc = c.codsuc)
				  AND (pd.codconcepto = c.codconcepto)
			  WHERE p.codsuc=".$codsuc." and  p.estadoprovision=2 AND p.tipo=1
				AND p.anio='".$anio."' AND p.mes='".$mes."'";
		$row = $conexion->query($Sql)->fetch();
		$Total = $row[0];
		$LDOC1= "0107".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
		//BORRAR DATOS
		DeleteReg($LDOC1);
		
		$Glosa="PROVISION ASIENTO 2 ";

		$Sql="INSERT INTO ".$NombreCab."
		            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
		            	CTOD,CTOH,DOC1,TIVO,CODDD,
		            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
						( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
							".$Total.",".$Total.",'".$LDOC1."',31,'64',
							' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
		odbc_exec($conexion2,$Sql);
		//CABECERA DE PROVISION1
		//DETALLE DE PROVISION1
		$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2)),
				c.descripcion, c.ctadebeprov2,  c.ctahaberprov2,c.codconcepto
			FROM
			  facturacion.provisiones p
			  INNER JOIN facturacion.provisiones_detalle pd ON (p.codemp = pd.codemp)
			  AND (p.codsuc = pd.codsuc)
			  AND (p.codprovision = pd.codprovision)
			  AND (p.tipo = pd.tipo)
			  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
				  AND (pd.codsuc = c.codsuc)
				  AND (pd.codconcepto = c.codconcepto)
			  WHERE p.codsuc=".$codsuc." and  p.estadoprovision=2 AND p.tipo=1
				AND p.anio='".$anio."' AND p.mes='".$mes."'
				GROUP BY  c.descripcion,c.ctadebeprov2,c.ctahaberprov2,c.codantiguo,c.codconcepto
		 		 ORDER BY c.codantiguo  ASC ";
		$Consulta = $conexion->query($Sql);
		$cont=0;
		$TotalDebe=0;
		foreach($Consulta->fetchAll() as $row)
		{
			$cont++;
			$concepto= $row[1];
			$TotalConcepto=$row[0];
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberprov2']."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'H',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
			$cont++;
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebeprov2']."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'D',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
		}
	}
	//DETALLE DE PROVISION 1
	//PROVISIONES 1 Y 2
	//QUIEBRES
	//CABECERA
	$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2))
		FROM
		  facturacion.cabquiebres p
		  INNER JOIN facturacion.origen_quiebre pd ON (p.codemp = pd.codemp)
		  AND (p.codsuc = pd.codsuc)
		  AND (p.nroquiebre = pd.nroquiebre)
		  AND (p.tipo = pd.tipo)
		  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
			  AND (pd.codsuc = c.codsuc)
			  AND (pd.codconcepto = c.codconcepto)
		  WHERE p.codsuc=".$codsuc."  AND p.tipo=1 AND p.estareg =1
			AND p.anio='".$anio."' AND p.mes='".$mes."'";
	$row = $conexion->query($Sql)->fetch();
	$Total = isset($row[0])?$row[0]:0;
	$LDOC1= "0108".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
		//BORRAR DATOS
	DeleteReg($LDOC1);
	if(floatval($Total)>0)
	{

		$Glosa = "QUIEBRES";

		$Sql="INSERT INTO ".$NombreCab."
		            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
		            	CTOD,CTOH,DOC1,TIVO,CODDD,
		            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
						( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
							".$Total.",".$Total.",'".$LDOC1."',31,'64',
							' ',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
		odbc_exec($conexion2,$Sql);
		//CABECERA
		//DETALLE
		$Sql="SELECT CAST(sum(CASE WHEN pd.importe <= 0 AND c.signo='-' THEN pd.importe*(-1)  ELSE pd.importe END) AS NUMERIC (18,2)),
				c.descripcion, c.ctadebequiebres,  c.ctahaberquiebres,c.codconcepto
			FROM
			  facturacion.cabquiebres p
			  INNER JOIN facturacion.origen_quiebre pd ON (p.codemp = pd.codemp)
			  AND (p.codsuc = pd.codsuc)
			  AND (p.nroquiebre = pd.nroquiebre)
			  AND (p.tipo = pd.tipo)
			  INNER JOIN facturacion.conceptos c ON (pd.codemp = c.codemp)
				  AND (pd.codsuc = c.codsuc)
				  AND (pd.codconcepto = c.codconcepto)
			  WHERE p.codsuc=".$codsuc." AND p.tipo=1
				AND p.anio='".$anio."' AND p.mes='".$mes."' AND p.estareg =1
				GROUP BY  c.descripcion,c.ctadebequiebres,c.ctahaberquiebres,c.codantiguo,c.codconcepto
		 		 ORDER BY c.codantiguo  ASC ";
		$Consulta = $conexion->query($Sql);
		$cont=0;
		$TotalDebe=0;
		foreach($Consulta->fetchAll() as $row)
		{
			$cont++;
			$concepto= utf8_decode($row[1]);
			$TotalConcepto=$row[0];
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberquiebres']."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'H',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
			$cont++;
			$Sql="INSERT INTO ".$NombreDet."
	        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
	        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
	        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
	        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES

				( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebequiebres']."',
					' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
					'D',31,40,' ','64',' ',' ',' ',
					' ',' ',' ',' ',' ',' ')";
			odbc_exec($conexion2,$Sql);
		}
	}
	//DETALLE DE PROVISION 1
	//PROVISIONES 1 Y 2
	//QUIEBRES
	//PAGO DE PROVISIONES
	$Sql="SELECT  CAST(SUM(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2))
			FROM  cobranza.cabpagos c
  			INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
  			AND (c.codsuc = d.codsuc)  AND (c.nropago = d.nropago)
  			AND (c.nroinscripcion = d.nroinscripcion)
  			INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
	  		AND (d.codsuc = co.codsuc)	  AND (d.codconcepto = co.codconcepto)
  			WHERE c.codsuc=".$codsuc." AND c.anulado=0
  			AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."'
  			AND d.codtipodeuda IN (10) ";
	$row = $conexion->query($Sql)->fetch();
	$Total = $row[0];
	$LDOC1= "0109".$aniomin.str_pad($mes,2, "0", STR_PAD_LEFT)."01";
	DeleteReg($LDOC1);
	if(floatval($Total)>0)
	{
		$Glosa="REGUL. COBRANZA DE PROVISIONES DE ".substr($meses[intval($mes)],0,3)." ".$aniomin." ";
		$Sql="INSERT INTO dicfactu
		            	(OFICOD,OFIAGECOD,LDOC1,FECT,ORIG,GLOS,CTOT,
		            	CTOD,CTOH,DOC1,TIVO,CODDD,
		            	NDOCU,CODR,ECHQ,ASI,NOMB,CTAB,ZONA,LOCA,FECI,FECHALNK) VALUES
						( 1,1,'".$LDOC1."',{".$mes."/01/".$aniomin."},40,'".$Glosa."',0,
							".$Total.",".$Total.",'".$LDOC1."',31,'64',
							'00000903',' ',' ',0,' ',' ','10','".$Loca."',{".$mes."/01/".$aniomin."},{".$mes."/01/".$aniomin."})";//FECI FECHALNK
		odbc_exec($conexion2,$Sql);
		//CABECERA DE
		//DETALLE D
		$Sql="
			 SELECT
			   CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)),
				co.descripcion, co.ctahaberprovcob,  co.ctadebeprovcob,co.codantiguo,co.codconcepto
			FROM
			  cobranza.cabpagos c
			  INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
			  AND (c.codsuc = d.codsuc)
			  AND (c.nropago = d.nropago)
			  AND (c.nroinscripcion = d.nroinscripcion)
			    INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
				  AND (d.codsuc = co.codsuc)
				  AND (d.codconcepto = co.codconcepto)
			  WHERE c.codsuc=".$codsuc." AND c.anulado=0
			  AND c.fechareg BETWEEN '".$anio."-".$mes."-01' AND '".$anio."-".$mes."-".date ('t', mktime (0,0,0, $mes, 1, $anio))."'
			  AND d.codtipodeuda IN (10)
			   GROUP BY co.descripcion, co.ctahaberprovcob,  co.ctadebeprovcob,co.codantiguo,co.codconcepto
				  ORDER BY co.codantiguo  ASC";
			$Consulta = $conexion->query($Sql);
			$cont=0;
			$TotalDebe=0;
			foreach($Consulta->fetchAll() as $row)
			{
				$cont++;
				$concepto= utf8_decode($row[1]);
				$TotalConcepto = $row[0];
				
				$Sql = "INSERT INTO ".$NombreDet."
		        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
		        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
		        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
		        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
					( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctahaberprovcob']."',
						' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/30/".$aniomin."},'".$concepto."',".$TotalConcepto.",
						'H',31,40,' ','64',' ',' ',' ',
						' ',' ',' ',' ',' ',' ')";
				odbc_exec($conexion2,$Sql);
				
				$cont++;
				$Sql = "INSERT INTO ".$NombreDet."
		        	(OFICOD,OFIAGECOD,LDOC1,LLIN,NCCOS,LCCOS,LCTA,
		        		LUBIC,LANAL,NDOCU,LFECH,LFECT,LGLOS,LIMPO,
		        		LFLAGDH,LTIVO,LORIG,LID,LCODDD,ILINE,ECHEQ,LCOND,
		        		CODDD,LRESP,PRORU,LCOMP,LTRAB,LPPTO) VALUES
					( 1,1,'".$LDOC1."','".str_pad($cont,4,"0",STR_PAD_LEFT)."',' ',' ','".$row['ctadebeprovcob']."',
						' ',' ',' ',{".$mes."/01/".$aniomin."},{".$mes."/".date ('t', mktime (0,0,0, $mes, 1, $anio))."/".$aniomin."},'".$concepto."',".$TotalConcepto.",
						'D',31,40,' ','64',' ',' ',' ',
						' ',' ',' ',' ',' ',' ')";
				odbc_exec($conexion2,$Sql);
			}
		}
	//DETALLE
	//PAGO DE PROVISIONES

		$dicfactu = "tablas/".$NombreCab;
		if(file_exists($dicfactu))
		{
			unlink($dicfactu);
		}
		$dilfactu = "tablas/".$NombreDet;
		if(file_exists($dilfactu))
		{
			unlink($dilfactu);
		}

	    //
	    $archivo = trim($RutaDisco).$NombreCab;
	    if (!copy($archivo, "tablas/".$NombreCab))
	    {
	   		echo "Error al copiar $archivo...\n";
		}
		$archivo = trim($RutaDisco).$NombreDet;
	    if (!copy($archivo, "tablas/".$NombreDet))
	    {
	   		echo "Error al copiar $archivo...\n";
		}
		
		echo "Los Asientos Contables de Facturacion<br>";
		echo "<br>";
		echo "<a href='tablas/".$NombreCab."' target='_blank'>".$NombreCab."</a><br>";
		echo "<br>";
		echo "<a href='tablas/".$NombreDet."' target='_blank'>".$NombreDet."</a><br>";
		echo "<br>";
		echo "Se han Generado Correctamente";

		odbc_close ( $conexion2 );
/*	$sql = "select co.ctadebe,c.car,c.fechareg,c.nropec,
			case when sum(d.importe) is null then 0 else sum(d.importe) end as importe
			from cabpagos as c
			inner join detpagos as d on(c.codemp=d.codemp and c.codsuc=d.codsuc
			and c.nropago=d.nropago and c.nroinscripcion=d.nroinscripcion)
			inner join conceptos as co on(d.codconcepto=co.codconcepto)
			where c.fechareg='".$fecha."' and c.nropec<>0
			group by co.ctadebe,d.codconcepto,c.car,c.fechareg,c.nropec
			order by d.codconcepto";
	$consulta=pg_query($sql);
	while($row=pg_fetch_array($consulta))
	{


	}*/

?>