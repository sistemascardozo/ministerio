<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	set_time_limit(0);
	//ini_set("display_errors",1);
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();

	
	$codemp      = 1;
	$codsuc      = $_POST['codsuc'];
	$codusu      = $_POST["codusu"];
	$dias      = $_POST["dias"];
	if($_POST["fecha"]!='')
		$fecha      = $objFunciones->CodFecha($_POST["fecha"]);
	else $fecha = date('Y-m-d');
	$nrolistado      = $_POST["nrolistado"];
	$nombrelistado      = $_POST["nombrelistado"];
	$tipo=1;//
	$resolucion=$_POST["resolucion"];
	$estareg=1;//
	$estadoprovision=1;//
	$fechareg = date('Y-m-d');//$objFunciones->FechaServer();
	$horareg = $objFunciones->HoraServidor();
	$codciclo=$_POST["ciclo"];
	$facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
	$anio =$facturacion["anio"];
	$mes =$facturacion["mes"];
	$factactual = $facturacion['nrofacturacion'];

	//regularizar
	$anio = $_POST["anio"];//'2015';
	$mes = $_POST["mes"];//'1';
	$sql = "select nrofacturacion
			from facturacion.periodofacturacion
			where codemp = 1 AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$codciclo,$anio,$mes));
	$row = $consulta->fetch();
	$factactual = $row["nrofacturacion"];//243;
	//regularizar
	//
	//die("==>".$factactual);
	if($nrolistado==0)
	{
		$Select=" SELECT codprovision 
		FROM facturacion.provisiones 
		WHERE codemp=".$codemp." AND codsuc=".$codsuc." AND tipo=".$tipo;
		$Consulta=$conexion->query($Select);
		$row=$Consulta->fetch();
		if($row[0]!='')
			{
				$Select=" SELECT max(codprovision) 
				FROM facturacion.provisiones 
				WHERE codemp=".$codemp." AND codsuc=".$codsuc." AND tipo=".$tipo;
				$Consulta=$conexion->query($Select);
				$row=$Consulta->fetch();
				if($row[0]=='')
					$nrolistado=1;
				else
					$nrolistado=$row[0]+1;

			}
		else
			$nrolistado=1;//$row[0];
	}

	$conexion->beginTransaction();

	$Sql = "DELETE FROM facturacion.provisiones_detalle WHERE codprovision=".$nrolistado." AND codsuc=".$codsuc." AND tipo=".$tipo;

	$Consulta = $conexion->query($Sql);
	
	$Sql="DELETE FROM facturacion.provisiones WHERE codprovision=".$nrolistado." AND codsuc=".$codsuc." AND tipo=".$tipo;
	$Consulta=$conexion->query($Sql);
	
	$Select="INSERT INTO facturacion.provisiones(
				codemp, codsuc, codprovision, tipo, nombre, dias, fechalimite, fechareg,
				hora, codusu, estareg, estadoprovision, resolucion, anio, mes) 
			VALUES (".$codemp.",".$codsuc.",".$nrolistado.",".$tipo.",'".$nombrelistado."',
				".$dias.",'".$fecha."','".$fechareg."','".$horareg."','".$codusu."',
				'".$estareg."','".$estadoprovision."','".$resolucion."','".$anio."','".$mes."');";
	$result=$conexion->query($Select);
	
    $Sql="SELECT c.nrofacturacion,c.nroinscripcion,c.fechavencimiento,
			extract(day from ('".$fecha."'::timestamp - c.fechavencimiento ::timestamp)) as dias,
			CAST(sum(d.importe -(d.imppagado + d.importerebajado)) AS NUMERIC(18,2)) as importe
			FROM
			  facturacion.cabfacturacion c
			  INNER JOIN facturacion.detfacturacion d ON (c.codemp = d.codemp)
			  AND (c.codsuc = d.codsuc)
			  AND (c.nrofacturacion = d.nrofacturacion)
			  AND (c.nroinscripcion = d.nroinscripcion)
			  AND (c.codciclo = d.codciclo)
			WHERE c.codsuc=".$codsuc." and d.estadofacturacion in (1,3) AND categoria IN (0,1)
			and (c.fechavencimiento+ CAST('".$dias." days' AS INTERVAL))<'".$fecha."'
			AND codtipodeuda NOT IN (10,11) AND d.codconcepto NOT IN (101,7,8)
			GROUP BY c.nroinscripcion,c.fechavencimiento,c.nrofacturacion
			ORDER BY c.fechavencimiento";
	 $Sql="SELECT c.nrofacturacion,c.nroinscripcion,c.fechavencimiento,
			extract(day from ('".$fecha."'::timestamp - c.fechavencimiento ::timestamp)) as dias,
			CAST(sum(d.importe -(d.imppagado + d.importerebajado)) AS NUMERIC(18,2)) as importe,
			d.codconcepto, d.nrocredito, d.nrocuota,d.item, d.estadofacturacion, d.codtipodeuda,
			 d.categoria, d.nrorefinanciamiento
			FROM
			  facturacion.cabfacturacion c
			  INNER JOIN facturacion.detfacturacion d ON (c.codemp = d.codemp)
			  AND (c.codsuc = d.codsuc)
			  AND (c.nrofacturacion = d.nrofacturacion)
			  AND (c.nroinscripcion = d.nroinscripcion)
			  AND (c.codciclo = d.codciclo)
			WHERE c.codsuc=".$codsuc."
			AND c.nrofacturacion <(".$factactual."-(".$dias."-1))
			 and d.estadofacturacion in (1,3) AND categoria IN (0,1)
			AND codtipodeuda NOT IN (10,11) AND d.codconcepto NOT IN (101)
			GROUP BY c.nroinscripcion,c.fechavencimiento,c.nrofacturacion,
			d.codconcepto, d.nrocredito, d.nrocuota,d.item, d.estadofacturacion, d.codtipodeuda,
			 d.categoria, d.nrorefinanciamiento
			ORDER BY c.nrofacturacion";
	$Consulta = $conexion->query($Sql);
	foreach($Consulta->fetchAll() as $row)				
	{

		$Sql="INSERT INTO facturacion.provisiones_detalle(
			codemp, codsuc, codprovision, tipo, codciclo, nrofacturacion,
			nroinscripcion, codconcepto, nrocredito, nrocuota, item, importe,
			dias, estadofacturacion, codtipodeuda, categoria, nrorefinanciamiento) 
		VALUES (
			".$codemp.",".$codsuc.",".$nrolistado.",".$tipo.",".$codciclo.",
			".$row['nrofacturacion'].",".$row['nroinscripcion'].",".$row['codconcepto'].",
			".$row['nrocredito'].",".$row['nrocuota'].",".$row['item'].",
			".$row['importe'].",".$row['dias'].",".$row['estadofacturacion'].",
			".$row['codtipodeuda'].",".$row['categoria'].",".$row['nrorefinanciamiento'].");";
		$result=$conexion->query($Sql);
		if(!$result)
		{
			die($Sql);

		}
	}
				

	if(!$result)
	{
		$conexion->rollBack();
		$res=0;
	}else{
		
		//VERIFICAR SI AY DATOS
		$Select="SELECT * FROM facturacion.provisiones_detalle WHERE codprovision=".$nrolistado." AND codsuc=".$codsuc." AND tipo=".$tipo;
		$Consulta=$conexion->query($Select);
		$nr= $Consulta->rowCount();
		if($nr==0)
		{
			$conexion->rollBack();
			$res='a';
		}
		else
		{
			$conexion->commit();
			$res=$nrolistado;
		}
	}
	
	echo $res;
	
?>
