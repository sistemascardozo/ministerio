<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsDrop.php");

$objMantenimiento = new clsDrop();

$Presicion = $_SESSION["Presicion"];
$codsuc = $_SESSION["IdSucursal"];
$NroInscripcion = $_POST["NroInscripcion"];
$codprovision = $_POST["codprovision"];
$Tipo = 1;

$Sql = "select clie.nroinscripcion,".$objMantenimiento->getCodCatastral("clie.").", 
            clie.propietario,td.abreviado,clie.nrodocumento,
            tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
            es.descripcion,tar.nomtar,clie.codantiguo,ts.descripcion as tiposervicio,
            clie.tipofacturacion,clie.nromed,clie.consumo,clie.codestadoservicio,co.fechainsmed
            from catastro.clientes as clie 
            INNER JOIN catastro.conexiones co ON (clie.codemp = co.codemp)
             AND (clie.codsuc = co.codsuc)  AND (clie.nroinscripcion = co.nroinscripcion)
            inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
            inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
            inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
            inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
            inner join public.tiposervicio as ts on(ts.codtiposervicio=clie.codtiposervicio)
            inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
            WHERE clie.codemp=1 AND clie.codsuc=".$codsuc." AND clie.nroinscripcion=".$NroInscripcion."";
$Consulta = $conexion->query($Sql);
$row = $Consulta->fetch();


switch ($row['tipofacturacion']) {
    case 0: $TipoFacturacion = 'CONSUMO LEIDO';
        break;
    case 1: $TipoFacturacion = 'CONSUMO PROMEDIADO';
        break;
    case 2: $TipoFacturacion = 'CONSUMO ASIGNADO';
        break;
}

$sql = "SELECT uso.catetar as catetar, tar.nomtar AS nombre, uso.porcentaje as porcentaje 
            FROM catastro.unidadesusoclientes as uso
            INNER JOIN facturacion.tarifas tar ON (uso.codemp = tar.codemp AND uso.codsuc = tar.codsuc AND uso.catetar = tar.catetar) 
            WHERE uso.codemp=1 AND uso.codsuc=".$codsuc." AND uso.nroinscripcion=".$NroInscripcion."";
$Consulta = $conexion->query($sql);
$raw = $Consulta->fetchAll();
$categorias = '';
foreach ($raw as $kew) :
    $categorias .= substr($kew['nombre'], 0, 3)."(".number_format($kew['porcentaje'])."%)-";
endforeach;
$categorias = substr($categorias, 0, -1);

$Medidor = 'No';
if (trim($row['nromed']) != "")
    $Medidor = 'Si ( N° '.$row['nromed'].')';
?>
<script type="text/javascript">
    function abrir_detalle_facturacion2(nrofacturacion, categoria) {
        cargar_detalle_facturacion2(nrofacturacion, categoria);
        $("#dialog-form-detalle-facturacion").dialog("open");
    }

    function cargar_detalle_facturacion2(nrofacturacion, categoria)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_facturacion_corriente.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nroinscripcion=' + $("#nroinscripcion").val() +
                    '&nrofacturacion=' + nrofacturacion + '&categoria=' + categoria,
            success: function(datos) {
                $("#detalle-facturacion").html(datos)
            }
        })
    }

    function abrir_detalle_pagos(codpagos) {
        cargar_detalle_pagos(codpagos);
        $("#dialog-form-detalle-pagos").dialog("open");
    }

    function cargar_detalle_pagos(codpagos)
    {
        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_pagos.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nroinscripcion=' + $("#nroinscripcion").val() +
                    '&codpagos=' + codpagos,
            success: function(datos) {
                $("#detalle-pagos").html(datos)
            }
        })
    }

    function abrir_detalle_colaterales(codcolateral)
    {
        cargar_detalle_colaterales(codcolateral);
        $("#dialog-form-detalle-colaterales").dialog("open");
    }

    function cargar_detalle_colaterales(codcolateral)
    {

        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_colaterales.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nroinscripcion=' + $("#nroinscripcion").val() +
                    '&codcolateral=' + codcolateral,
            success: function(datos) {
                $("#detalle-colaterales").html(datos)
            }
        })
    }

    function abrir_detalle_creditos(nrocredito) {
        cargar_detalle_creditos(nrocredito);
        $("#dialog-form-detalle-creditos").dialog("open");
    }

    function cargar_detalle_creditos(nrocredito) {

        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_creditos.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nrocredito=' + nrocredito,
            success: function(datos) {
                $("#detalle-creditos").html(datos)
            }
        })
    }
    function abrir_detalle_refinanciamiento(nrocredito) {
        cargar_detalle_refinanciamiento(nrocredito);
        $("#dialog-form-detalle-refinanciamiento").dialog("open");
    }

    function cargar_detalle_refinanciamiento(nrocredito) {

        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_cargar_detalle_refinanciamiento.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nrocredito=' + nrocredito + '&nroinscripcion=<?=$NroInscripcion ?>',
            success: function(datos) {
                $("#detalle-refinanciamiento").html(datos)
            }
        })
    }
    function abrir_detalle_varios(nrocredito) {
        cargar_detalle_varios(nrocredito);
        $("#dialog-form-detalle-varios").dialog("open");
    }

    function cargar_detalle_varios(nrocredito) {

        $.ajax({
            url: urldir + 'sigco/catastro/listados/cuentacorriente/include/from_detalle_varios.php',
            type: 'POST',
            async: true,
            data: 'codsuc=' + codsuc + '&nrocredito=' + nrocredito,
            success: function(datos) {
                $("#detalle-varios").html(datos)
            }
        })
    }

    $(function() {
        $("#dialog-form-detalle-facturacion").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $("#dialog-form-detalle-facturacion").dialog("close");
                }
            },
            close: function() {

            }
        });

        $("#dialog-form-detalle-pagos").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $("#dialog-form-detalle-pagos").dialog("close");
                }
            },
            close: function() {

            }
        });

        $("#dialog-form-detalle-colaterales").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $("#dialog-form-detalle-colaterales").dialog("close");
                }
            },
            close: function() {

            }
        });

        $("#dialog-form-detalle-creditos").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $("#dialog-form-detalle-creditos").dialog("close");
                }
            },
            close: function() {

            }
        });
        $("#dialog-form-detalle-refinanciamiento").dialog({
            autoOpen: false,
            height: 400,
            width: 550,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $("#dialog-form-detalle-refinanciamiento").dialog("close");
                }
            },
            close: function() {

            }
        });
        $("#dialog-form-detalle-varios").dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $("#dialog-form-detalle-varios").dialog("close");
                }
            },
            close: function() {

            }
        });

    });
</script>
<table class="ui-widget" border="1" cellspacing="0" width="100%"  rules="rows" align="center">
    <thead style="font-size:12px;"> 
        <tr>
            <td><span class="Negrita">N&deg; de Inscripcion:&nbsp;</span><?=$row['codantiguo'] ?></td>
            <td><span class="Negrita">Cod. Catastral:&nbsp;</span> <?=$row[1] ?></td>
            <td>

            </td>
        </tr>
        <tr>
            <td align="left"><span class="Negrita">Cliente:&nbsp;</span> <?=$row['propietario'] ?></td>
            <td><span class="Negrita">Tipo Servicio:&nbsp;</span> <?=$row['tiposervicio'] ?></td>

        </tr>
        <tr>
            <td align="left"><span class="Negrita">Direcci&oacute;n :&nbsp;</span><?=strtoupper($row['direccion']) ?></td>
            <td><span class="Negrita">Categoria:</span> <?=$categorias ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">Estado:&nbsp;</span> <?=$row['descripcion'] ?></td>
            <td><span class="Negrita">Fecha Activaci&oacute;n:&nbsp;</span> 
                <?php
                if ($row['codestadoservicio'] == 1) {
                    $Sql = "(select MAX(cierr.fecha) AS FECHA 
                                from medicion.cierreyapertura as cierr 
                                where cierr.codemp=1 and cierr.codsuc={$codsuc}  
                                AND tipooperacion=1  AND cierr.nroinscripcion = ".$NroInscripcion."
                                )UNION
                                (                           
                                select MAX(m.fechacontrol) AS FECHA 
                                from catastro.modificaciones as m
                                where m.codsuc={$codsuc}
                                AND trim(m.campo)='codestadoservicio'
                                AND m.valorultimo='1' AND m.nroinscripcion= ".$NroInscripcion."
                                )UNION
                                (                           
                                    select m.fechainsagua AS FECHA 
                                    from catastro.conexiones as m
                                    where m.codemp=1 and m.codsuc={$codsuc}
                                    AND m.nroinscripcion= ".$NroInscripcion."
                                )UNION
                                (                           
                                    select m.fechainsdesague AS FECHA 
                                    from catastro.conexiones as m
                                    where m.codemp=1 and m.codsuc={$codsuc}
                                    AND m.nroinscripcion= ".$NroInscripcion."
                                )
                                ORDER BY  FECHA DESC";
                    //  echo $Sql;
                    $ConsultaF = $conexion->query($Sql);
                    $rowF = $ConsultaF->fetchAll();
                    if ($rowF[0][0] != '')
                        echo $objMantenimiento->CodFecha($rowF[0][0]);
                    else
                        echo $objMantenimiento->CodFecha($rowF[1][0]);
                }
                ?></td>

        </tr>
        <tr>
            <td width="40%"><span class="Negrita">Tipo Facturaci&oacute;n:&nbsp;</span> <?=$TipoFacturacion ?></td>
            <td width="20%"><span class="Negrita">Medidor:&nbsp;</span><?=$Medidor ?></td>
            <td width="40%">
                <?php
                if (trim($row['nromed']) != "") {
                    ?>
                    <span class="Negrita">Fecha Instalaci&oacute;n:&nbsp;</span><?=$objMantenimiento->CodFecha($row['fechainsmed']); ?>
                    <?php
                    }
                    ?>
                </td>

            </tr>
        </thead>
    </table>
    <table class="ui-widget" bORDER="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
        <thead class="ui-widget-header" style="font-size:10px">
            <tr>
                <th width="100" scope="col">Item</th>
                <th width="50" scope="col">Fecha</th>
                <th width="50" scope="col">Hora</th>
                <th width="200" scope="col">Mes Facturaci&oacute;n</th>
                <th width="50" scope="col">Serie</th>
                <th width="50" scope="col">N&uacute;mero</th>
                <th width="300" scope="col">Operaci&oacute;n</th>
                <th width="100" scope="col">Cat</th>
                <th width="100" scope="col" data="estado_lectura">Co</th>
                <th width="100" scope="col">Lectura</th>
                <th width="100" scope="col">Consu</th>
                <th width="100" scope="col">Cargo</th>
                <th width="100" scope="col">Abono</th>
                <th width="100" scope="col">Saldo</th>
                <th width="100" scope="col">Estado Recibo</th>
                <th scope="col" width="10" align="center">Acciones</th>

            </tr>
        </thead>
        <tbody style="font-size:12px">

            <?php
            $Condicion1 = " WHERE f.nroinscripcion =".$NroInscripcion." AND pd.codprovision=".$codprovision." AND pd.tipo=".$Tipo;

			$Sql = " SELECT 
				f.fechareg, 
				tar.nomtar as categoria, 
				f.serie,
				f.nrodocumento,
				'00:00:00' as hora,
				".$objMantenimiento->Convert('f.anio', 'INTEGER')." as anio, 
				".$objMantenimiento->Convert('f.mes', 'INTEGER')." AS mes, 
				sum(df.importe) as cargo,
				'0.00' as abono,
				'0' as movimiento, 
				f.enreclamo as reclamo, 
				f.lecturaultima as lectura, 
				f.consumofact as consumo,
				df.nrofacturacion as facturacion,
				df.categoria as cat,
				0 as codtipodeuda,
				'0' as nropago,
				f.estadofacturacion as estado,
				0 as nrocredito,
				1 as orden
				FROM facturacion.cabfacturacion f 
				INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) 
				AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
				INNER JOIN facturacion.tarifas as tar ON (f.codemp=tar.codemp and f.codsuc=tar.codsuc and f.catetar=tar.catetar)
				INNER JOIN facturacion.provisiones_detalle pd ON (f.codemp = pd.codemp)
					AND (f.codsuc = pd.codsuc)
					AND (f.codciclo = pd.codciclo)
					AND (f.nrofacturacion = pd.nrofacturacion)
					AND (f.nroinscripcion = pd.nroinscripcion)
				".$Condicion1." AND df.nrocredito=0 and df.nrorefinanciamiento=0 
				GROUP BY f.estadofacturacion, df.categoria, df.nrofacturacion, 
				f.enreclamo, tar.nomtar,f.lecturaultima, f.consumofact, f.fechareg,
				f.serie,f.nrodocumento,f.anio, f.mes, f.nrofacturacion 
				ORDER BY f.nrofacturacion ";

            $Consulta = $conexion->query($Sql);
            $datos = $Consulta->fetchAll();
            $c = 0;
            $Deuda = 0;
            // var_dump($datos);exit;
            ?>

            <?php if (count($datos) > 0): ?>

                <?php foreach ($datos as $row): ?>
                    <?php
                    $c++;
                    $Operacion = '';
                    switch ($row['movimiento']) {
                        case 0:
                            $Operacion = "FACT. DE PENSIONES";
                            $Anio = $row['anio'];
                            $Mes = $row['mes'];
                            $AnioT = $row['anio'];
                            $MesT = $row['mes'];
                            $Deuda += $row['cargo'];
                            $style = "style=color:red;";
                            $onClickDetalle = "abrir_detalle_facturacion2(".$row["facturacion"].",".$row["cat"].")";
                            $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Facturacion' ></span>";

                            switch ($row['estado']) {
                                case 1:$row['estado'] = 'PEN';
                                    break;
                                case 2:$row['estado'] = 'PAG';
                                    break;
                                case 3:$row['estado'] = 'AMO';
                                    break;
                                case 4:$row['estado'] = 'ANU';
                                    $Deuda-=$row['cargo'];
                                    break;
                                default:$row['estado'] = 'PEN';
                                    break;
                            };

                            break;
                        case 1:
                            if ($row['anio'] == $AnioT && $row['mes'] == $MesT)
                                $Operacion = "PAGO RECIBO DE PENSIONES";
                            else
                                $Operacion = "PAGO DEUDA ACUMULADA";

                            $Anio = $row['anio'];
                            $Mes = $row['mes'];
                            $Deuda -= $row['abono'];
                            $style = "style=color:green;";
                            $onClickDetalle = "abrir_detalle_pagos(".$row["nropago"].")";
                            $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Pagos' ></span>";
                            $row['estado'] = '';
                            break;

                        case 2:

                            switch ($row['codtipodeuda']) {
                                case 3:
                                    $Operacion = $row['categoria']."(REFINANCIAMIENTO)";
                                    break;
                                case 4:
                                    $Operacion = $row['categoria']."(CREDITOS)";
                                    break;
                                case 4:
                                    $Operacion = $row['categoria']."(CREDITOS)";
                                    break;
                                default:
                                    $Operacion = $row['categoria'];
                                    break;
                            }

                            $Anio = $row['anio'];
                            $Mes = $row['mes'];
                            $style = "style=color:#FF6347;";
                            $onclick = '';
                            $onClickDetalle = "abrir_detalle_colaterales(".$row["nropago"].")";
                            $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Colaterales' ></span>";
                            $row['estado'] = '';
                            $Deuda -= $row['abono'];
                            break;
                        case 3:
                            $Operacion = "FACT. DE CREDITOS";

                            $Anio = $row['anio'];
                            $Mes = $row['mes'];
                            $style = "style=color:#00008B;";
                            $onClickDetalle = "abrir_detalle_creditos(".$row["nrocredito"].")";
                            $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Creditos'></span>";

                            switch ($row['estado']) {
                                case 0:$row['estado'] = 'ANU';
                                    break;
                                case 1:$row['estado'] = 'PEN';
                                    $Deuda += $row['cargo'];
                                    break;
                                case 2:$row['estado'] = 'CAN';
                                    $Deuda += $row['cargo'];
                                    break;
                                case 3:$row['estado'] = 'CAN';
                                    break;
                                case 4:$row['estado'] = 'QUE';
                                    break;
                            };

                            break;
                        case 4:
                            $Operacion = "FACT. POR REFINANCIAMIENTO";
                            $Anio = $row['anio'];
                            $Mes = $row['mes'];
                            $style = "style=color:#20B2AA;";
                            $onClickDetalle = "abrir_detalle_refinanciamiento(".$row["nrocredito"].")";
                            $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Creditos'></span>";
                            switch ($row['estado']) {
                                case 0:$row['estado'] = 'PEN';
                                    $Deuda += $row['abono'];
                                    break;
                                case 1:$row['estado'] = 'FACT';
                                    $Deuda += $row['abono'];
                                    break;
                                case 2:$row['estado'] = 'ANU';
                                    break;
                                case 3:$row['estado'] = 'CAN';
                                    break;
                                default:$row['estado'] = 'PEN';
                                    $Deuda += $row['abono'];
                                    break;
                            };
                            // $Deuda -=  $row['abono'];
                            break;
                        case 5:
                            $Operacion = "REBAJAS";
                            $Anio = $row['anio'];
                            $Mes = $row['mes'];
                            $style = "style=color:#E28409;";
                            $onclick = '';
                            switch ($row['estado']) {
                                case 0:$row['estado'] = 'ANU';
                                    break;
                                case 1:$row['estado'] = 'REB';
                                    $Deuda -= $row['abono'];
                                    break;
                                default:$row['estado'] = 'REB';
                                    $Deuda -= $row['abono'];
                                    break;
                            };
                            break;
                        case 6:
                            $Operacion = "FACT DE CARGOS";

                            $Anio = $row['anio'];
                            $Mes = $row['mes'];
                            $style = "style=color:red;";
                            $onClickDetalle = "abrir_detalle_varios(".$row["nrocredito"].")";
                            $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Creditos'></span>";
                            switch ($row['estado']) {
                                case 1:$row['estado'] = 'ING';
                                    break;
                                case 0:$row['estado'] = 'ANU';
                                    break;
                                default:$row['estado'] = 'ING';
                                    break;
                            };
                            $Deuda += $row['cargo'];
                            break;
                        case 7:
                            switch ($row['codtipodeuda']) {
                                case 3:
                                    $Operacion = $row['categoria']."(REFINANCIAMIENTO)";
                                    break;
                                case 4:
                                    $Operacion = $row['categoria']."(CREDITOS)";
                                    break;
                                case 4:
                                    $Operacion = $row['categoria']."(CREDITOS)";
                                    break;
                                default:
                                    $Operacion = $row['categoria'];
                                    break;
                            }

                            $Anio = $row['anio'];
                            $Mes = $row['mes'];
                            $style = "style=color:red;";
                            $onclick = '';
                            $onClickDetalle = "abrir_detalle_colaterales(".$row["nropago"].")";
                            $onclick = "<span class='icono-icon-detalle' onclick='".$onClickDetalle."' title='Ver Detalle de Colaterales' ></span>";
                            $row['estado'] = '';
                            $Deuda += $row['cargo'];
                            break;
                        default:;
                            break;
                    }
                    ?>

                    <tr <?=$title ?> <?=$class ?> onclick="SeleccionaId(this);" id="<?=$c ?>" <?=$style ?> >
                        <td align="center"><?=$c ?></td>
                        <td align="center" valign="middle"><?=$objMantenimiento->DecFecha($row['fechareg']) ?></td>
                        <td align="center" valign="middle"><?=substr($row['hora'], 0, 5) ?></td>
                        <td align="left" valign="middle"><?=substr($meses[$row['mes']], 0, 3)." - ".$row['anio'] ?></td>
                        <td align="center" valign="middle"><?=$row['serie'] ?></td>
                        <td align="center" valign="middle"><?=$row['nrodocumento'] = ($row['nrodocumento'] == 0) ? '&nbsp;' : $row['nrodocumento']; ?></td>
                        <td align="left" valign="middle"><?=$Operacion ?></td>
                        <td align="left" valign="middle"><?=substr($row['categoria'], 0, 3) ?></td>
                        <td align="left" valign="middle">&nbsp;</td>
                        <td align="left" valign="middle"><?=substr($row['lectura'], 0, -3) ?></td>
                        <td align="left" valign="middle"><?=substr($row['consumo'], 0, -3) ?></td>
                        <td align="right" valign="middle"><?=$row['cargo'] = ($row['cargo'] == 0) ? '&nbsp;' : number_format($row['cargo'], 2); ?></td>
                        <td align="right" valign="middle"><?=$row['abono'] = ($row['abono'] == 0) ? '&nbsp;' : number_format($row['abono'], 2); ?></td>
                        <td align="right" valign="middle"><?=number_format($Deuda, 2) ?></td>

                        <td><?=$row['estado']; ?></td>
                        <td>
                            <?=$onclick; ?>
                            <span class="icono-icon-trash" title="Borrar Registro"
                                  onclick="DeleteCorteDet(<?=$c ?>,<?=$codsuc ?>,<?=$codprovision ?>,<?=$NroInscripcion ?>, <?=$row['facturacion'] ?>,<?=$row['cargo'] ?>)"></span>
                        </td>
                    </tr>
        <?php endforeach; ?>
    <?php else: ?>
                <tr>
                    <td colspan="16" align="center" style="color:red;">NO EXISTE NINGUN MOVIMIENTO PARA ESTE CLIENTE</td>
                </tr>
    <?php endif; ?>
        </tbody>
        <tfoot class="ui-widget-header" style="font-size:10px">
            <tr>
                <td colspan="13" align="right" >Deuda Total:</td>
                <td align="right" ><label id="DeudaUsu"><?=number_format($Deuda, 2) ?></label></td>
            <td scope="col" colspan="2" align="center">&nbsp;</td>
        </tr>
    </tfoot>
</table>
<div id="dialog-form-detalle-facturacion" title="Ver Detalle de Facturacion"  >
    <div id="detalle-facturacion">
    </div>
</div>
<div id="dialog-form-detalle-pagos" title="Ver Detalle de Pagos"  >
    <div id="detalle-pagos">
    </div>
</div>

<div id="dialog-form-detalle-colaterales" title="Ver Detalle de Colaterales"  >
    <div id="detalle-colaterales">
    </div>
</div>
<div id="dialog-form-detalle-creditos" title="Ver Detalle de Creditos"  >
    <div id="detalle-creditos">
    </div>
</div>
<div id="dialog-form-detalle-refinanciamiento" title="Ver Detalle de Financiamiento"  >
    <div id="detalle-refinanciamiento">
    </div>
</div>
<div id="dialog-form-detalle-varios" title="Ver Detalle de Creditos Varios"  >
    <div id="detalle-varios">
    </div>
</div>