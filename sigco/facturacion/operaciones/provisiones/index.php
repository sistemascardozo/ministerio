<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "PROVISION DE RECIBOS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$objDrop 	= new clsDrop();
	$codsuc 	= $_SESSION['IdSucursal'];
	$codusu 	= $_SESSION['id_user'];
	$codemp=1;
	$sucursal 		= $objDrop->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
	$fechaserver  = $objDrop->FechaServer();
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_corte.js" language="JavaScript"></script>
<script type="text/javascript" src="../../../medicion/operaciones/digitacion/js_digitacion.js" language="JavaScript"></script>
<script>

var Tam = TamVentana();
var urldir 	= "<?php echo $_SESSION['urldir'];?>" ;
var codsuc 	= <?=$codsuc?>;

$(document).ready(function(){
$('#DivTbListados #TbListados').fixedHeaderTable({ height: '300', footer: true });
var theTable = $('#TbListados')
				  	$("#Filtro2").keyup(function() {
						$.uiTableFilter( theTable, this.value)
					  })


$("#tabs").tabs();
$( "#DivCuentaCorriente" ).dialog({
            autoOpen: false,
            height:600,
            width: 1000,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $( "#DivCuentaCorriente" ).dialog( "close" );
                }
            },
            close: function() {
            	$(ObjTr).addClass('ui-widget-header');
            }
        });

$( "#DivDetalle" ).dialog({
            autoOpen: false,
            height:600,
            width: 1000,
            modal: true,
            buttons: {
                "Cerrar": function() {
                    $( "#DivDetalle" ).dialog( "close" );
                }
            },
            close: function() {
            	$(ObjTr).addClass('ui-widget-header');
            }
        });

});

	var codsuc = <?=$codsuc?>;
	
	var c = 0
	function ValidarForm(Op)
	{

		var Temp=""
		var i=0
		var Data=''
		if($("#nombrelistado").val()=='')
		{
			Msj('#nombrelistado','Ingrese Nombre de la Provision',1000,'right','error',false)
			return false
		}
		if($("#dias").val()=='')
		{
			Msj('#dias','Ingrese Numero de Dias',1000,'right','error',false)
			return false
		}
		/*if($("#fecha").val()=='')
		{
			Msj('#dias','Ingrese Fecha Limite',1000,'right','error',false)
			return false
		}*/
		if($("#resolucion").val()=='')
		{
			Msj('#dias','Ingrese la Resolucion',1000,'right','error',false)
			return false
		}
		Data+='nrolistado='+$("#nrolistado").val()+'&'
		Data+='nombrelistado='+$("#nombrelistado").val()+'&'
		Data+='dias='+$("#dias").val()+'&'
		Data+='resolucion='+$("#resolucion").val()+'&'
		Data+='anio='+$("#anio").val()+'&'
		Data+='mes='+$("#mes").val()+'&'
		Data+='ciclo='+$("#ciclo").val()+'&'
		Data+='&codsuc=<?=$codsuc?>&codusu=<?=$codusu?>'
		$("#btnVerificar").hide();
		$("#BtnLoading").show();
		$.ajax({
				 url:'generar.php',
				 type:'POST',
				 async:true,
				 data:Data,
				 success:function(datos)
				 {
				 	$("#btnVerificar").show();
					$("#BtnLoading").hide();
					if(datos!=0 && datos!='a')
					{
						//$("#nrolistado").val(datos)
						Msj('#btnVerificar','Provision Generado con Exito',2000,'above','',false)
						cListado(datos)
						
					}	
					else
					{
						if(Trim(datos)=='a')
							Msj('#btnVerificar','No Existen Datos para estos parametros',2000,'above','error',false)
						else
							Msj('#btnVerificar','Ocurrio un Error al Generar',2000,'above','',false)
						
					}
					
				 }
			}) 
		
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function cListado(nrolistado)
	{
		$.ajax({
				 url:'cListado.php',
				 type:'POST',
				 async:true,
				 data:'listado='+nrolistado,
				 success:function(datos){
					$("#cmbnrolistado").html(datos)
					$("#nrolistado").html("<option value='0' >Nuevo Listado</option>"+datos)
					cnombrelistado()
				 }
			}) 
	}

	function generar()
	{
		if($("#ciclo").val()==0)
		{
			alert("Seleccione el Ciclo de Facturacion")
			return false
		}
		$("#div_valida").html("<img src='../../../../images/iconos/sincro.gif' />")
		$.ajax({
				 url:'generar.php',
				 type:'POST',
				 async:true,
				 data:'codciclo='+$("#ciclo").val(),
				 success:function(datos){
					valida($("#ciclo").val());
				 }
			}) 
	}

	
	function Valida()
	{
		if($("#cmbnrolistado").val() > 0)
		return true
	else
	{
				Msj('#cmbnrolistado','Seleccione Listado',1000,'right','error',false)
				return false
			}
		
	}
	function Consultar() 
	{

		if(!Valida())
			return false
		$('#DivConsulta').fadeOut(100) 
			$('#ImgLoad').fadeIn();
		var nrolistado = $("#cmbnrolistado").val()
		$.ajax({
				 url:'Consultar.php',
				 type:'POST',
				 async:true,
				 data:'nrolistado='+nrolistado,
				 success:function(datos)
				 {$("#TrResultados").show()
					$('#ImgLoad').fadeOut(500,function(){
			       	$("#DivConsulta").empty().append(datos);         
			       	/*$("#TbConsulta tbody tr").mouseover(function(){$(this).addClass('ui-state-active');})
					$("#TbConsulta tbody tr").mouseout(function(){$(this).removeClass('ui-state-active'); })
					$(document).on('focusin', '#TbConsulta tbody tr',function(event){$(this).addClass('ui-widget-header');});
					$(document).on('focusout','#TbConsulta tbody tr', function(event){$(this).removeClass('ui-widget-header');});	
					*/
					$("#TbConsulta tbody tr").focusin(function(){$(this).siblings().removeClass('ui-widget-header');$(this).addClass('ui-widget-header');})
					$("#TbConsulta tbody tr").focusout(function(){$(this).removeClass('ui-widget-header'); })
					var nroinput=0;
					$('#TbConsulta tbody tr').each(function(){nroinput++;$(this).attr('tabindex',nroinput);
						
					});
					tb = $('#TbConsulta tbody tr');
					if ($.browser.mozilla) $(tb).keypress(TabIndexIndex);
    				else $(tb).keydown(TabIndexIndex);


					$('#DivConsulta').fadeIn(500,function(){var theTable = $('#TbConsulta')
				  	$("#Filtro").keyup(function() {
						$.uiTableFilter( theTable, this.value)
					  })}) 
					});


				 }
			}) 

	}
	function TabIndexIndex(e)
	{
    	switch(e.keyCode)
		{	
			case $.ui.keyCode.UP: 
		       for (var i = parseInt($(this).attr('tabindex'))-1; i > 0; i--) 
				{
					var obj = $('tr[tabindex=\'' +i+ '\']')
					if (obj != null )
			          {
			          	if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
			          	{
			              $('tr[tabindex=\'' + i  + '\']').focus();
			               e.preventDefault();
			               return false;
			           }
			           
			         }
				};
				$('#Valor').focus().select();

			break;
			case $.ui.keyCode.DOWN:
	           for (var i = parseInt($(this).attr('tabindex'))+1; i <= 10000; i++) 
				{
					var obj = $('tr[tabindex=\'' +i+ '\']')
					if (obj != null )
			          {
			          	if(obj.is (':visible') && $(obj).parents (':hidden').length==0)
			          	{
			              $('tr[tabindex=\'' + i  + '\']').focus();
			               e.preventDefault();
			               return false;
			           }
			           
			         }
				};
				$('#Valor').focus().select();
			break;
			
			
			
			
		}
     
}
function Excel2()
	{
	
		if(!Valida())
			return false
		var nrolistado = $("#cmbnrolistado").val()
		var ventana=window.open('ExcelDetallado.php?nrolistado=' + nrolistado, 'Excel Listado', 'resizable=yes, scrollbars=yes');
		ventana.focus();
	
	}
	function Excel()
	{
	
		if(!Valida())
			return false
		var nrolistado = $("#cmbnrolistado").val()
		var ventana=window.open('Excel.php?nrolistado=' + nrolistado, 'Excel Listado', 'resizable=yes, scrollbars=yes');
		ventana.focus();
	
	}
	function Pdf()
	{
		if(!Valida())
		return false
		var nrolistado = $("#cmbnrolistado").val()
		var ventana=window.open('imprimir.php?nrolistado=' + nrolistado, 'Imprimir Listado', 'resizable=yes, scrollbars=yes');
		ventana.focus();
		
	}
	function ExcelP(nrolistado)
	{
		var ventana=window.open('Excel.php?nrolistado=' + nrolistado, 'Excel Listado', 'resizable=yes, scrollbars=yes');
		ventana.focus();
	
	}
	function PdfP(nrolistado)
	{
	
		
		var ventana=window.open('imprimir.php?nrolistado=' + nrolistado, 'Imprimir Listado', 'resizable=yes, scrollbars=yes');
		ventana.focus();
		
	}	
	function ExcelP2(nrolistado)
	{
	
		var ventana=window.open('ExcelDetallado.php?nrolistado=' + nrolistado, 'Excel Listado', 'resizable=yes, scrollbars=yes');
		ventana.focus();
	
	}
	function cnombrelistado()
	{
		$("#nombrelistado").val($("#nrolistado option:selected").html())
		$("#resolucion").val($("#nrolistado option:selected").data('res'));
		$("#fecha").val($("#nrolistado option:selected").data('fecha'));
		$("#dias").val($("#nrolistado option:selected").data('dia'));
	}

	function DeleteCorteDet(count,codsuc,nrolistado,nroinscripcion,facturacion,deuda)
	{
		var r=confirm("Realmente desea eliminar el registro?")
		if(r==false)
		{
			return false	
		}
		$.ajax({
				 url:'deletefacturacion.php',
				 type:'POST',
				 async:true,
				 data:'codsuc='+codsuc+'&nrolistado='+nrolistado+'&nroinscripcion='+nroinscripcion+'&facturacion='+facturacion,
				 success:function(datos){
					if(datos==1)
					{
						deudaact= $("#DeudaUsu").html()
						deudaact = str_replace(deudaact, ',', '');
						deudaact = deudaact-deuda;
						$("#DeudaUsu").html(parseFloat(deudaact).toFixed(2))
						$("#TbIndex tbody tr#"+count).remove();
						
					}
					//$("#TbConsulta tbody tr#"+(parseInt(count)+1)).focus();
				 }
			}) 
	}
	function DeleteCorte(count,codsuc,nrolistado,nroinscripcion,anio,mes)
	{
		var r=confirm("Realmente desea eliminar el registro?")
		if(r==false)
		{
			return false	
		}
		$.ajax({
				 url:'deletelistado.php',
				 type:'POST',
				 async:true,
				 data:'codsuc='+codsuc+'&nrolistado='+nrolistado+'&nroinscripcion='+nroinscripcion+'&anio='+anio+'&mes='+mes,
				 success:function(datos){
					if(datos==1)
					{
						 $("#TbConsulta tbody tr#"+count).next().focus();
						 $("#TbConsulta tbody tr#"+count).remove();
					}
					//$("#TbConsulta tbody tr#"+(parseInt(count)+1)).focus();
				 }
			}) 
	}
	var ObjTr=''
	function CuentaCorriente(nroinscripcion,obj)
	{
		ObjTr=$(obj).parent().parent();
		//AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/Pdf.php?NroInscripcion="+nroinscripcion,Tam[0],Tam[1])
		$("#nroinscripcion").val(nroinscripcion)
		$.ajax({
			 url:'<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/Consulta.php',
			 type:'POST',
			 async:true,
			 data:'NroInscripcion='+nroinscripcion,
			 success:function(datos)
			 {
			 	
			       $("#detalle-CuentaCorriente").empty().append(datos);             
				$("#DivCuentaCorriente").dialog('open');
				
			 }
			})
	}
	function DetalleRecibo(nroinscripcion,obj,codprovision)
	{
		ObjTr=$(obj).parent().parent();
		//AbrirPopupImpresion("<?php echo $_SESSION['urldir'];?>sigco/catastro/listados/cuentacorriente/Pdf.php?NroInscripcion="+nroinscripcion,Tam[0],Tam[1])
		$("#nroinscripcion").val(nroinscripcion)
		$.ajax({
			 url:'detalle.php',
			 type:'POST',
			 async:true,
			 data:'NroInscripcion='+nroinscripcion+'&codprovision='+codprovision,
			 success:function(datos)
			 {
			 	
			       $("#detalle-Recibos").empty().append(datos);             
				$("#DivDetalle").dialog('open');
				
			 }
			})
	}
	function check_listado(obj,nrolistado)
	{
		var Op=0;
		if(obj.checked)
			Op=1;
		$.ajax({
				 url:'activarlistado.php',
				 type:'POST',
				 async:true,
				 data:'codsuc='+codsuc+'&nrolistado='+nrolistado+'&Op='+Op,
				 success:function(datos){
					if(Op==1)
					{
						Msj(obj,'Listado Activado');
						$("#lbl_listado"+nrolistado).html('Activo')
					}
					else
					{
						Msj(obj,'Listado Desactivado');
						$("#lbl_listado"+nrolistado).html('Desactivo')
					}
					cListado(nrolistado)
					//$("#TbConsulta tbody tr#"+(parseInt(count)+1)).focus();
				 }
			}) 
	}
	function Provisionar()
	{
		if(!Valida())
			return false
		var nrolistado= $("#cmbnrolistado").val()
		
		var r=confirm("Realmente desea Provionar el listado: "+ $("#cmbnrolistado option:selected").text())
		if(r==false)
		{
			return false	
		}
		$.blockUI({ message: '<div><center><span class="icono-icon-loading"></span>Provisionado...Espere un Momento por Favor...</center></div>',
				  css: { 
					border: 'none', 
					padding: '15px', 
					backgroundColor: '#000', 
					'-webkit-border-radius': '10px', 
					'-moz-border-radius': '10px', 
					opacity: .5, 
					color: '#fff' 
						}
				  }); 

		$.ajax({
				 url:'Provisionar.php',
				 type:'POST',
				 async:true,
				 data:'nrolistado='+nrolistado+'&codsuc=<?=$codsuc?>&codusu=<?=$codusu?>',
				 success:function(datos)
				 {
				 	if (parseInt(datos)!=0)
					{
						window.parent.OperMensaje("Ocurrio un problema al guardar",2)	
						Msj($("#BtnP"),'Ocurrio un problema al guardar')
					}
				else
					{
						
						window.parent.OperMensaje("Provision Generado Correctamente",1)
						Msj($("#BtnP"),'Provision Generado Correctamente')
						$("#DivConsulta").empty();
						cListado(0);
					}
					
					$.unblockUI()

				 }
			}) 
	}
	function AnularProvision(nrolistado)
	{

		
		var r=confirm("Realmente desea Anular la Provision")
		if(r==false)
		{
			return false	
		}
		var clave = prompt("Ingrese Clave de Seguridad");
		if(clave!='admin')
		{
			alert('Clave Incorrecta')
			return false
		}
		$.blockUI({ message: '<div><center><span class="icono-icon-loading"></span>Provisionado...Espere un Momento por Favor...</center></div>',
				  css: { 
					border: 'none', 
					padding: '15px', 
					backgroundColor: '#000', 
					'-webkit-border-radius': '10px', 
					'-moz-border-radius': '10px', 
					opacity: .5, 
					color: '#fff' 
						}
				  }); 

		$.ajax({
				 url:'AnularProvision.php',
				 type:'POST',
				 async:true,
				 data:'nrolistado='+nrolistado+'&codsuc=<?=$codsuc?>&codusu=<?=$codusu?>',
				 success:function(datos)
				 {
				 	if (parseInt(datos)!=0)
					{
						window.parent.OperMensaje("Ocurrio un problema al guardar",2)	
						Msj($("#Filtro2"),'Ocurrio un problema al guardar')
					}
				else
					{
						
						window.parent.OperMensaje("Provision Generado Correctamente",1)
						Msj($("#Filtro2"),'Provision Generado Correctamente')
						$("#DivConsulta").empty();
						cListado(0);
					}
					
					$.unblockUI()

				 }
			}) 
	}
function datos_facturacion(codciclo)
{
	$.ajax({
		 url:'../../../../ajax/periodo_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+codciclo+'&codsuc='+codsuc,
		 success:function(datos){
			var r=datos.split("|")

			$("#anio").val(r[1])
			$("#mes").val(r[3])
		 }
	}) 
}
</script>
<div align="center">
<form id="form1" name="form1">
	<table width="1000" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
				<td colspan="2" >
					<input type="hidden" id="nroinscripcion">
					<div id="tabs">
						<ul>
                            <li title="Datos Basicos" style="display:none;"><a href="#tabs-1">Generar</a></li>
                            <li title="Imprimir" style="display:none;"><a href="#tabs-2">Imprimir</a></li>		
                            <li title="Historico de Listados"><a href="#tabs-3">Historico</a></li>	
						</ul>
						<div id="tabs-1" title="Generar" style="display:none;">
							<table width="100%" style="display:none;">
								<tr>
									<td colspan="2">
										<table width="95%" border="0" cellspacing="0" cellpadding="0">
                                            <tr><td colspan="5">&nbsp;</td></tr>
                                            <tr>
                                                <td width="90">Sucursal</td>
                                                <td width="30" align="center">:</td>
                                                <td colspan="1">
													<input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
												</td>
												<td width="80" align="right">Ciclo:</td>
                                                <td width="30" align="center">:</td>
												<td>
													<? $objDrop->drop_ciclos($codsuc,0,"onchange='datos_facturacion(this.value);'"); ?>
                                                    Anio : <input type="text" name="anio" id="anio" class="inputtext entero" size="5" maxlength="4" value="" />
                                                    Mes : <input type="text" name="mes" id="mes" class="inputtext entero" size="5" maxlength="2" value="" />
												</td>
                                            </tr>
                                            <tr>
                                                <td>Provisión</td>
                                                <td align="center">:</td>
                                                <td>
					    	<!-- <input type="text" name="nrolistado" id="nrolistado" readonly="readonly" size="10" maxlength="6" class="inputtext" /> -->
													<select id="nrolistado" name="nrolistado" style="width:150px" onchange="cnombrelistado()">
														<option value='0' >Nueva Provision</option>
<?php
					    	$consulta = $conexion->prepare("SELECT * FROM facturacion.provisiones WHERE codemp=1 AND codsuc=".$codsuc." AND tipo=1 AND estareg=1 AND estadoprovision=1 ORDER BY fechareg DESC");
								$consulta->execute();
								$items = $consulta->fetchAll();
								foreach($items as $row)
								{
																		
									echo "<option value='".$row["codprovision"]."' data-res='".$row["resolucion"]."' data-fecha='".$objDrop->DecFecha($row["fechalimite"])."' data-dia='".$row["dias"]."' >".$row["nombre"]."</option>";
									
								}
										
							?>
													</select>
                                                </td>
                                                <td>
													<div style="display:inline;float:right">Nombre : </div>
												</td>
												<td align="left" colspan="2">
													<input type="text" name="nombrelistado" id="nombrelistado" size="40" class="inputtext" />
												</td>
                                            </tr>
                                            <tr>
                                                <td>Facturaciones</td>
                                                <td align="center">:</td>
                                                <td><input type="text" name="dias" id="dias" class="inputtext entero" size="10" maxlength="10" value="6" /></td>
                                                <td align="right" style="display:none">Fecha</td>
                                                <td align="center" style="display:none">:</td>
                                                <td style="display:none"><input type="text" name="fecha" id="fecha"  class="inputtext fecha" size="15" maxlength="10" value="<?=$fechaserver?>" /></td>
                                            </tr>
                                            <tr>
                                                <td valign="top">Resolución</td>
                                                <td align="center" valign="top">:</td>
                                                <td colspan="4" ><textarea name="resolucion" id="resolucion" cols="69" rows="2" class="inputtext" style="font-size:11px"></textarea></td>
                                            </tr>	    
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="4" align="center">
                                                    <input type="button" value="Generar" id="btnVerificar" onclick="ValidarForm();" />
                                                    <input type="button" value="Generando" id="BtnLoading" onmousemove="Sobre(this);" onmouseout="Fuera(this);" class="Botones"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
                        </div>
                        <div id="tabs-2" title="Imprimir">
							<table>
								<tr>
									<td colspan="2">
										<table width="95%" border="0" cellspacing="0" cellpadding="0">
                                            <tr><td colspan="5">&nbsp;</td></tr>
                                            <tr>
												<td colspan="5" align="left">Provision :
													<select id="cmbnrolistado" style="width:150px">
					    	<?php
					    		$consulta = $conexion->prepare("SELECT * FROM facturacion.provisiones WHERE codemp=1 AND codsuc=".$codsuc." AND tipo=1 AND estareg=1 AND estadoprovision=1 ORDER BY fechareg DESC");
								$consulta->execute();
								$items = $consulta->fetchAll();
								foreach($items as $row)
								{
?>				
														<option value='<?=$row["codprovision"]?>' data-res='<?=$row["resolucion"]?>' data-fecha='<?=$objDrop->DecFecha($row["fechalimite"])?>' data-dia='<?=$row["dias"]?>' ><?=$row["nombre"]?></option>
<?php
								}
										
							?>
													</select>
												</td>
                                            </tr>
                                            <tr>
												<td colspan="5" height="40" align="center">
													<table width="500" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td align="center">
																<input type="button" value="Guardar" id="BtnAceptar" onclick="Consultar();" onmousemove="Sobre(this);" onmouseout="Fuera(this);"/>
                                                            </td>
                                                            <td align="center"><input id="BtnExcel" type="button" value="Excel" onclick="Excel();" onmousemove="Sobre(this);" onmouseout="Fuera(this);" class="BtnIndex"/></td>
                                                            <td align="center"><input id="BtnExcel2" type="button" value="Excel" onclick="Excel2();" onmousemove="Sobre(this);" onmouseout="Fuera(this);" class="BtnIndex"/></td>
                                                            <td align="center">
																<input id="BtnPdf" type="button" value="Pdf" onclick="Pdf();" onmousemove="Sobre(this);" onmouseout="Fuera(this);" class="BtnIndex"/>
                                                            </td>
                                                            <td align="center">
																<input id="BtnP" type="button" value="Provisionar" onclick="Provisionar();" onmousemove="Sobre(this);" onmouseout="Fuera(this);" class="BtnIndex"/>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
                                </tr>
                                <tr id="TrResultados">
									<td colspan="2" align="center">
										<fieldset>
											<legend class="ui-widget-header">Resultados de la Consulta</legend>
                                            <div id="ImgLoad" style="text-align: center"><img src="../../../../images/avance.gif" /><br/>Cargando ...</div>
                                            <div style="height:auto; overflow:auto; display:none " align="center" id="DivConsulta"></div>
										</fieldset>
									</td>
								</tr> 
							</table>	
                        </div>
                        <div id="tabs-3" title="Anular">
							<table>
								<tr>
									<th scope="col" colspan="2" align="center">LISTADOS PARA PROVISION</th>
                                </tr>
                                <tr>
									<td align="center" colspan="2">
										Buscar:&nbsp;<input id="Filtro2" class="inputtext" value="" maxlength="30" size="30" type="text" />
									</td>
                                </tr>
                                <tr>
									<td colspan="2">
										<div style="overflow:auto; height:400px" id="DivTbListados">	
											<table class="ui-widget" border="1" cellspacing="0" id="TbListados" width="100%" rules="rows">
												<thead class="ui-widget-header" style="font-size:10px">
													<tr title="Cabecera">
                                                        <th scope="col" width="100" align="center">ITEM</th>
                                                        <th scope="col" width="100" align="center">FECHA</th>
                                                        <th scope="col" width="200" align="center">PERIODO</th>
                                                        <th scope="col" width="400" align="center">DESCRIPCION</th>
                                                        <th scope="col" width="100" align="center">ESTADO</th>
                                                        <th scope="col" width="100" align="center">&nbsp;</th>
													</tr>
												</thead>
												<tbody style="font-size:10px">
<?php
			    $ctd=0;
							///////////////
				$sql  = "SELECT codprovision,fechareg,anio,mes,nombre,estareg,estadoprovision
						FROM facturacion.provisiones 
						WHERE codemp=1 AND codsuc=".$codsuc." AND tipo=1 ORDER BY fechareg DESC";
							//echo $sql."<br>";
							//die($sql);
					$consulta=$conexion->query($sql);
					$items = $consulta->fetchAll();	
					foreach($items as $row)
					{
						$ctd++;
?>
													<tr id="L<?=$count?>">
                                                        <td align="center"><?=$ctd?></td>
                                                        <td><?=$objDrop->DecFecha($row["fechareg"])?></td>
                                                        <td><?=$meses[$row["mes"]]." - ".$row["anio"]?> &nbsp;</td>
                                                        <td><?=$row["nombre"]?></td>
<?php
				         		$checked="checked='checked'";
				         		$estado="Registrado";
				         		if($row['estadoprovision']==2)
				         		{
				         			$checked="";
				         			$estado="Provisionado";
				         		}
				         		else
				         		{
				         			if($row['estareg']==1)
				         			{
				         				$checked="checked='checked'";
				         				$estado="Registrado";
				         			}
				         			else
				         			{
				         				$checked="";
				         				$estado="Desactivado";
				         			}
				         		}
				         		?>
                                                        <td><label id='lbl_listado<?=$row["codprovision"]?>'><?=$estado?></label></td>
                                                        <td>
<?php 
				         			if($row['estadoprovision']==1)
				         			{
?>
															<input type='checkbox' id='check_listado<?=$row["codprovision"]?>' <?=$checked?> class='input-chk' onclick='check_listado(this,<?=$row["codprovision"]?>)'>
<?php
								}
?>
                                                            <span class="icono-icon-ok" title="a Excel Detallado" onclick="ExcelP2(<?=$row['codprovision']?>)"></span>&nbsp;
                                                            <span class="icono-icon-ok" title="a Excel" onclick="ExcelP(<?=$row['codprovision']?>)"></span>&nbsp;
                                                            <span class="icono-icon-ok" title="a Pdf" onclick="PdfP(<?=$row['codprovision']?>)"></span>&nbsp;
								<?php if($row['estadoprovision']==2) { ?>
															<span class="icono-icon-trash" style="display:none;" title="Anular Provision" onclick="AnularProvision(<?=$row['codprovision']?>)"></span>&nbsp;
								<?php } ?>
														</td>
													</tr>

<?php

				    			/*$objReporte->Contenido(utf8_decode($row["propietario"]),$row["direccion"],$row["nromeses"],
				                        $row["imptotal"],$row["catastro"],$ts,$tf,$row["nromed"],$count);
			*/
					}
							////////////
					//}//IF CICLOS

					//}//FOR CICLOS

			     //}//FOR SUC

?>
												</tbody>
												<tfoot class="ui-widget-header">
													<tr><td colspan="6" align="center">&nbsp;</td>
												</tfoot>
											</table>
										</div>
									</td>
								</tr>
							</table>	
						</div>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</tr>
		</tbody>
	</table>
</form>
</div>
<script>
	$("#sector").attr("disabled", true)
	$("#estadoservicio").attr("disabled", true)
	$("#BtnAceptar").attr('value', 'Consultar')
	
$("#BtnAceptar").css('background-image', 'url(../../../../css/images/ver.png)')
$("#TrResultados").hide()
	//$("#btnAceptar").attr('type','button')
</script>
<div id="DivCuentaCorriente" title="Ver Cuenta Corriente"  >
    <div id="detalle-CuentaCorriente">
    </div>
</div>
<div id="DivDetalle" title="Detalle de Recibos a Provisionar"  >
    <div id="detalle-Recibos">
    </div>
</div>
<?php CuerpoInferior();?>
