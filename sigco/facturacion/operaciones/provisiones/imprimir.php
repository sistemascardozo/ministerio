<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsCorte extends clsReporte
	{
		function cabecera()
		{
			global $anio,$mes,$meses,$sectores,$rutas,$Dim;
			
			$h = 5;
			$this->SetY(8);
			$this->SetFont('Arial','B',12);
			$this->Cell(0, $h,"PROVISION",0,1,'C');
			$this->Cell(0, $h,$meses[intval($mes)]."-".$anio,0,1,'C');
			$this->Ln(5);
			
			$this->SetFont('Arial','',6);
			
			$this->Cell($Dim[1],$h,utf8_decode('Item'),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode('Codigo Catastral'),1,0,'C',false);
			$this->Cell($Dim[3],$h,utf8_decode('Inscripcion'),1,0,'C',false);
			$this->Cell($Dim[4],$h,utf8_decode('Nombre'),1,0,'C',false);
			$this->Cell($Dim[5],$h,utf8_decode('Direccion'),1,0,'C',false);
			$this->Cell($Dim[6],$h,utf8_decode('Tipo Ser.'),1,0,'C',false);
			$this->Cell($Dim[7],$h,utf8_decode('#Meses Deuda'),1,0,'C',false);
			$this->Cell($Dim[8],$h,utf8_decode('Valor Deuda'),1,1,'C',false);

			$this->SetFont('Arial','',6);
			
			
		}
		function Contenido($usuario,$direccion,$meses,$imptotal,$codcatastro,
                                   $tiposervicio,$item,$codantiguo)
		{
			global $anio,$mes,$sectores,$rutas,$Dim;
			$this->SetFont('Arial','',8);
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);
			$h=5;
			$this->Cell($Dim[1],$h,utf8_decode($item),1,0,'C',true);
			$this->Cell($Dim[2],$h,utf8_decode($codcatastro),1,0,'C',true);
			$this->Cell($Dim[3],$h,utf8_decode($codantiguo),1,0,'C',true);
			$this->Cell($Dim[4],$h,strtoupper($usuario),1,0,'L',true);
			$this->Cell($Dim[5],$h,strtoupper($direccion),1,0,'L',true);
			$this->Cell($Dim[6],$h,utf8_decode(strtoupper(trim($tiposervicio))),1,0,'C',true);
			$this->Cell($Dim[7],$h,$meses,1,0,'C',true);
			$this->Cell($Dim[8],$h,number_format($imptotal,2),1,1,'R',true);
			
		}
		function ContenidFoot($total,$monto)
		{
			$h=6;
			$this->SetFont('Arial','B',8);
			$this->Ln(2);
			$this->Cell(8, $h,"",0,0,'C');
			$this->Cell(23, $h,"",0,0,'L');
			$this->Cell(50, $h,"Total Usuarios",0,0,'R');
			$this->Cell(45, $h,$total,0,0,'L');
			$this->Cell(15, $h,"",0,0,'C');		
			$this->Cell(26, $h,"MONTO DEUDA",0,0,'R');
			$this->Cell(25, $h,number_format($monto,2),0,1,'R');
		}
    }
	if(!session_start()){session_start();}
	$objReporte	=	new clsCorte();
	$Dim = array('1' =>8,'2'=>20,'3'=>20,'4'=>80,'5'=>90,'6'=>15,'7'=>25,'8'=>25,'9'=>20,'10'=>27,'11'=>15,'12'=>20);
	
	/*$ciclo		= $_GET["ciclo"];
	$ruta		= "%".$_GET["ruta"]."%";
	$sector		= "%".$_GET["sector"]."%";
	$orden 		= $_GET["orden"];
	$inicio		= $_GET["inicio"];
	$final		= $_GET["final"];
	$estado		= "%".$_GET["estado"]."%";*/
	$codsuc		= $_SESSION['IdSucursal'];
	$nrolistado		= $_GET["nrolistado"];
	$tipo=1;
	$codemp		= 1;
	$Select		= "SELECT anio, mes
	FROM  facturacion.provisiones
	WHERE codprovision=".$nrolistado." AND codsuc=".$codsuc." AND tipo=".$tipo;
	$Consulta=$conexion->query($Select);
	$row=$Consulta->fetch();
	
	$anio=$row['anio'];
	$mes=$row['mes'];

	$objReporte->AliasNbPages();
    $objReporte->AddPage("H");
				///////////////
			$sql  = "select clie.nroinscripcion,clie.propietario,clie.codtiposervicio,
				tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle as direccion,
				".$objReporte->getCodCatastral("clie.").",clie.codantiguo,
				SUM(p.importe) as imptotal,count(*) as nromeses
				from catastro.clientes as clie 
				INNER JOIN facturacion.provisiones_detalle p ON (clie.codemp = p.codemp)
  				AND (clie.codsuc = p.codsuc)  AND (clie.nroinscripcion = p.nroinscripcion)
  				inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
				inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
				where  p.codsuc=".$codsuc." AND p.codprovision=".$nrolistado." AND p.tipo=".$tipo."
				GROUP BY clie.nroinscripcion,clie.propietario,clie.codtiposervicio,
				tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle,
				clie.codantiguo,clie.codsuc,clie.codsector,clie.codmanzanas,clie.lote,clie.codrutlecturas ";
				$ord = " ORDER BY  clie.codsuc,clie.codsector,CAST(clie.codrutlecturas AS INTEGER ),CAST(clie.codmanzanas AS INTEGER) ,CAST(clie.lote AS INTEGER)";
		$consulta=$conexion->query($sql.$ord);
		$items = $consulta->fetchAll();

		foreach($items as $row)
		{
			$ts="";
			$tf="";
			$count++;
			
			if($row["codtiposervicio"]==1){$ts="Ag/Des";}
			if($row["codtiposervicio"]==2){$ts="Ag.";}
			if($row["codtiposervicio"]==3){$ts="Des.";}
			
	        $monto += $row["imptotal"];
	        
			$objReporte->Contenido(utf8_decode($row["propietario"]), utf8_decode($row["direccion"]), $row["nromeses"],
	        $row["imptotal"], $row["codcatastro"], $ts,$count, isset($row['codantiguo'])?$row['codantiguo']:$row['nroinscripcion']);
			
		}

	
		$objReporte->ContenidFoot($count,$monto);
		$objReporte->Output();
	
	
?>