<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	set_time_limit(0);
	
	include("../../../../objetos/clsFunciones.php");
	
	$nrolistado = $_POST['nrolistado'];
	$tipo=1;
	$objFunciones = new clsFunciones();
	$codsuc=$_SESSION['IdSucursal'];
	//////////////
	$codemp		= 1;
	$Select		= "SELECT anio, mes
	FROM  facturacion.provisiones
	WHERE codprovision=".$nrolistado." AND codsuc=".$codsuc." AND tipo=".$tipo;
	$Consulta=$conexion->query($Select);
	$row=$Consulta->fetch();
	
	$anio=$row['anio'];
	$mes=$row['mes'];
	
?>
<table class="ui-widget" border="1" cellspacing="0" id="TbConsulta" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
    <tr>
	 	<td align="center" colspan="11">
	   Buscar:&nbsp;<input id="Filtro" class="inputtext" value="" maxlength="30" size="30" type="text" />
	   </td>
	 </tr>
    <tr>
    	 <th scope="col" colspan="9" align="center">LISTADO PARA PROVISION</th>
    </tr>
      <tr title="Cabecera">
        <th scope="col" width="100" align="center">ITEM</th>
        <th scope="col" width="100" align="center">COD. CATASTRAL</th>
        <th scope="col" width="100" align="center">Inscripcion</th>
        <th scope="col" width="400" align="center">USUARIO</th>
        <th scope="col" width="400" align="center">DIRECCION</th>
        <th scope="col" width="100" align="center">TS</th>
        <th scope="col" width="100" align="center">MESES</th>
        <th scope="col" width="100" align="center">IMPORTE</th>
        <th scope="col" width="10" align="center">&nbsp;</th>
      </tr>
    </thead>
    <tbody style="font-size:10px">
    <?php
    $ctd=0;
   /* for ($i=0; $i <count($codsucursales) ; $i++) 
     { */
     	/*$codsuc=$codsucursales[$i];
     	//SECTORES INCLUIDOS//
     	$AndSec="";
     	$c=0;
     	$at1 = array(NULL);
   		for ($is=0; $is <count($codsectores) ; $is++) 
		{ 

			$codsector = split('[-]',$codsectores[$is]);
			
			if($codsuc==$codsector[0]) 
			{	
				$sector =$codsector[1];
				$at1[$c]=" clie.codsector= ".$sector."  ";
				$c++;
			}//IF SECTORES

		}//FOR SECTORES
		$AndSec = "( ".implode("OR", $at1)." )";
     	//SECTORES INCLUIDOS//
		/*for ($ic=0; $ic <count($codciclos) ; $ic++) 
		{ 

			$codciclo = split('[-]',$codciclos[$ic]);
			//print_r($codciclo);
			if($codsuc==$codciclo[0]) 
			{
				$ciclo =$codciclo[1];*/

				///////////////
				$sql  = "select clie.nroinscripcion,clie.propietario,clie.codtiposervicio,
				tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle as direccion,
				".$objFunciones->getCodCatastral("clie.").",clie.codantiguo,
				SUM(p.importe) as imptotal,count(*) as nromeses
				from catastro.clientes as clie 
				INNER JOIN facturacion.provisiones_detalle p ON (clie.codemp = p.codemp)
  				AND (clie.codsuc = p.codsuc)  AND (clie.nroinscripcion = p.nroinscripcion)
  				inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
				inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
				where  p.codsuc=".$codsuc." AND p.codprovision=".$nrolistado." AND p.tipo=".$tipo."
				GROUP BY clie.nroinscripcion,clie.propietario,clie.codtiposervicio,
				tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle,
				clie.codantiguo,clie.codsuc,clie.codsector,clie.codmanzanas,clie.lote,clie.codrutlecturas ";
				$ord = " ORDER BY  clie.codsuc,clie.codsector,CAST(clie.codrutlecturas AS INTEGER ),CAST(clie.codmanzanas AS INTEGER) ,CAST(clie.lote AS INTEGER)";
				//echo $sql."<br>";
				//die($sql);
		$consulta=$conexion->query($sql.$ord);
		$items = $consulta->fetchAll();	
		foreach($items as $row)
		{
			$ts="";
			$tf="";
			$count++;
			
			if($row["codtiposervicio"]==1){$ts="Ag/Des";}
			if($row["codtiposervicio"]==2){$ts="Ag.";}
			if($row["codtiposervicio"]==3){$ts="Des.";}
			
	        $monto += $row["imptotal"];
	        $ctd++;
	         ?>
	         <tr id="<?=$count?>">
	         	<td align="center"><?=$ctd?></td>
	         	<td><?=$row["codcatastro"]?></td>
	         	<td><?=$row["codantiguo"]?></td>
	         	<td><?=$row["propietario"]?></td>
	         	<td><?=$row["direccion"]?></td>
	         	<td><?=$ts?></td>
	         	<td><?=$row["nromeses"]?></td>
	         	<td align="right"><?=number_format($row["imptotal"],2)?></td>
	         	<td>
	         		<span class="icono-icon-trash" title="Borrar Registro"
				         	onclick="DeleteCorte(<?=$count?>,<?=$codsuc?>,<?=$nrolistado?>,<?=$row['nroinscripcion']?>, <?=$anio?>,<?=$mes?>)"></span>
				         	<span class="icono-icon-detalle" title="Detalle de Recibos"
				         	onclick="DetalleRecibo(<?=$row['nroinscripcion']?>,this,<?=$nrolistado?>)"></span>
				         	<span class="icono-icon-dinero" title="Ver Cuenta Corriente"
				         	onclick="CuentaCorriente(<?=$row['nroinscripcion']?>,this)"></span>
	         </td>
	         </tr>

	    <?php


		}

     //}//FOR SUC

    ?>

    </tbody>
    <tfoot class="ui-widget-header">
    	<tr>
         <td colspan="5" align="center">
         	TOTAL USUARIOS:<?= $ctd?>

         </td>
         <td colspan="2" >Monto Deuda:</td>
         <td align="right" ><?=number_format($monto,2)?></td>
         <td scope="col" width="10" align="center">&nbsp;</td>
         
    </tfoot>
</table>
