<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

set_time_limit(0);

include("../../../../objetos/clsFunciones.php");

$nrolistado = $_GET['nrolistado'];
$codsuc = $_SESSION['IdSucursal'];

$objFunciones = new clsFunciones();

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=Listado.xls");
header("Pragma: no-cache");
header("Expires: 0");
//////////////
$codemp = 1;
$tipo = 1;

$Select = "SELECT anio, mes
	FROM  facturacion.provisiones
	WHERE codprovision=".$nrolistado." AND codsuc=".$codsuc." AND tipo=".$tipo;

$Consulta = $conexion->query($Select);
$row = $Consulta->fetch();

$anio = $row['anio'];
$mes = $row['mes'];
?>



<table class="ui-widget" border="1" cellspacing="0" id="TbConsulta" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
        <tr title="Cabecera">
            <th scope="col" class="ui-widget" colspan="8" align="center">PROVISION <?=$meses[intval($mes)]."-".$anio ?></th>
        </tr>
        <tr title="Cabecera">
            <th scope="col" width="100" align="center">ITEM</th>
            <th scope="col" width="100" align="center">COD. CATASTRAL</th>
            <th scope="col" width="100" align="center">CODIGO</th>
            <th scope="col" width="400" align="center">USUARIO</th>
            <th scope="col" width="400" align="center">DIRECCION</th>
            <th scope="col" width="100" align="center">TS</th>
            <th scope="col" width="100" align="center">MESES</th>
            <th scope="col" width="100" align="center">IMPORTE</th>
        </tr>
    </thead>
    <tbody style="font-size:10px">
        <?php
        $ctd = 0;
        $sql = "SELECT clie.nroinscripcion,clie.propietario,clie.codtiposervicio,
            tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle as direccion,
            ".$objFunciones->getCodCatastral("clie.").",clie.codantiguo,
            SUM(p.importe) as imptotal,count(*) as nromeses
            FROM catastro.clientes as clie 
            INNER JOIN facturacion.provisiones_detalle p ON (clie.codemp = p.codemp)
            AND (clie.codsuc = p.codsuc)  AND (clie.nroinscripcion = p.nroinscripcion)
            INNER JOIN public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
            INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
            WHERE  p.codsuc=".$codsuc." AND p.codprovision=".$nrolistado." AND p.tipo=".$tipo."
            GROUP BY clie.nroinscripcion,clie.propietario,clie.codtiposervicio,
            tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle,
            clie.codantiguo,clie.codsuc,clie.codsector,clie.codmanzanas,clie.lote,clie.codrutlecturas ";
        $ord = " ORDER BY  clie.codsuc,clie.codsector,CAST(clie.codrutlecturas AS INTEGER ),CAST(clie.codmanzanas AS INTEGER) ,CAST(clie.lote AS INTEGER)";
        $consulta = $conexion->query($sql.$ord);
        $items = $consulta->fetchAll();
        foreach ($items as $row) {
            $ts = "";
            $tf = "";
            $count++;

            if ($row["codtiposervicio"] == 1) {
                $ts = "Ag/Des";
            }
            if ($row["codtiposervicio"] == 2) {
                $ts = "Ag.";
            }
            if ($row["codtiposervicio"] == 3) {
                $ts = "Des.";
            }

            $monto += $row["imptotal"];
            $ctd++;
            ?>
            <tr>
                <td><?=$ctd ?></td>
                <td><?=$row["codcatastro"] ?></td>
                <td style="mso-number-format:'\@'"><?=$objFunciones->CodUsuario($codsuc, $row['nroinscripcion']) ?></td>
                <td><?=utf8_decode($row["propietario"]) ?></td>
                <td><?=utf8_decode($row["direccion"]) ?></td>
                <td><?=$ts ?></td>
                <td><?=$row["nromeses"] ?></td>
                <td align="right"><?=$row["imptotal"] ?></td>

            </tr>

            <?php
        }
        ?>

    </tbody>
    <tfoot class="ui-widget-header">
        <tr>
            <td colspan="5" align="center">
                TOTAL USUARIOS:<?=$ctd ?>

            </td>
            <td colspan="2" >Monto Deuda:</td>
            <td align="right" ><?=number_format($monto, 2) ?></td>
        </tr>
    </tfoot>
</table>
