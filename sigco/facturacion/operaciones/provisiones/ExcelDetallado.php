<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	set_time_limit(0);
	
	include("../../../../objetos/clsReporte.php");
	include("../../../../objetos/clsReporteExcel.php");
	
	$nrolistado = $_GET['nrolistado'];
	$codsuc		= $_SESSION['IdSucursal'];
	$objFunciones = new clsFunciones();
	$objReporte   = new clsReporte();
	
	header("Content-type: application/vnd.ms-excel; name='excel'");  
    header("Content-Disposition: filename=Listado.xls");  
    header("Pragma: no-cache");  
    header("Expires: 0"); 
	//////////////
	$codemp		= 1;
	$tipo		= 1;
	$Select		= "SELECT anio, mes
	FROM  facturacion.provisiones
	WHERE codprovision=".$nrolistado." AND codsuc=".$codsuc." AND tipo=".$tipo;
	$Consulta=$conexion->query($Select);
	$row=$Consulta->fetch();
	
	$anio = $row['anio'];
	$mes = $row['mes'];
	
	CabeceraExcel(2, 5);
	//OBTENER LOS CONCEPTOS
	$Sql = "SELECT 
			  co.codconcepto,
			  co.descripcion
			FROM
			  facturacion.provisiones_detalle dp
			  INNER JOIN facturacion.conceptos co ON (dp.codemp = co.codemp)
			  AND (dp.codsuc = co.codsuc)
			  AND (dp.codconcepto = co.codconcepto)
			  where  dp.codsuc=".$codsuc." AND dp.codprovision=".$nrolistado." 
			  		AND dp.tipo=".$tipo."
			  GROUP BY  co.codconcepto,
			  co.descripcion
				ORDER BY co.codconcepto";

	$consultacol = $conexion->query($Sql);
	$alcas = $consultacol->fetchAll();
	$nr= $consultacol->rowCount();
	$nroconceptos=0;
?>
<table class="ui-widget" border="1" cellspacing="0" id="TbConsulta" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
    <tr title="Cabecera">
    	 <th scope="col" class="ui-widget" colspan="<?=(9+$nr)?>" align="center">PROVISION  DE COBRANZA DUDOSA <?=$meses[intval($mes)]."-".$anio?></th>
    </tr>
      <tr title="Cabecera">
        <th scope="col" width="100" align="center" class="ui-widget-header">ITEM</th>
        <!-- <th scope="col" width="100" align="center" class="ui-widget-header">COD. CATASTRAL</th> -->
        <th scope="col" width="100" align="center" class="ui-widget-header">N&deg; INSCRIPCION</th>
        <th scope="col" width="400" align="center" class="ui-widget-header">USUARIO</th>
        <th scope="col" width="400" align="center" class="ui-widget-header">DIRECCION</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">F.Emisi&oacute;n</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">Fecha Venc.</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">Serie</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">N&deg;</th>
         <?php
	        	$SelFac ='';
	        	foreach($alcas as $row)
				{
					$nroconceptos++;
					$concepto= $row["descripcion"];
					$SelFac.="SUM(CASE WHEN p.codconcepto=".$row["codconcepto"]." THEN p.importe ELSE 0  END) AS c".$row["codconcepto"].",";
					?>
					<th width="50" scope="col" class="ui-widget-header">C<?=$row["codconcepto"]?></th>
					<?
				}
				


			?>

		<th scope="col" width="100" align="center" class="ui-widget-header">Total</th>	
      </tr>
    </thead>
    <tbody style="font-size:10px">
<?php
	$ctd = 0;
	$sql  = "select clie.nroinscripcion,clie.propietario,clie.codtiposervicio,
				tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle as direccion,
				".$objFunciones->getCodCatastral("clie.").",clie.codantiguo,
				pf.fechaemision,cf.fechavencimiento,cf.serie,cf.nrodocumento,
				".$SelFac." SUM(p.importe) AS c0
				from catastro.clientes as clie 
				INNER JOIN facturacion.provisiones_detalle p ON (clie.codemp = p.codemp)
  				AND (clie.codsuc = p.codsuc)  AND (clie.nroinscripcion = p.nroinscripcion)
  				inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
				inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
				  INNER JOIN facturacion.periodofacturacion pf ON (pf.codemp = p.codemp)
				  AND (pf.codsuc = p.codsuc)
				  AND (pf.codciclo = p.codciclo)
				  AND (pf.nrofacturacion = p.nrofacturacion)
				  INNER JOIN facturacion.cabfacturacion cf ON (p.codemp = cf.codemp)
				  AND (p.codsuc = cf.codsuc)
				  AND (p.codciclo = cf.codciclo)
				  AND (p.nrofacturacion = cf.nrofacturacion)
				  AND (p.nroinscripcion = cf.nroinscripcion)
				  AND (cf.codemp = pf.codemp)
				  AND (cf.codsuc = pf.codsuc)
				  AND (cf.nrofacturacion = pf.nrofacturacion)
				  AND (cf.codciclo = pf.codciclo)
			  INNER JOIN facturacion.conceptos co ON (p.codemp = co.codemp)
			  AND (p.codsuc = co.codsuc)
			  AND (p.codconcepto = co.codconcepto)
				where  p.codsuc=".$codsuc." AND p.codprovision=".$nrolistado." AND p.tipo=".$tipo."
				GROUP BY clie.nroinscripcion,clie.propietario,clie.codtiposervicio,
				tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle,
				clie.codantiguo,clie.codsuc,clie.codsector,clie.codmanzanas,clie.lote,
				clie.codrutlecturas,pf.fechaemision,cf.fechavencimiento,cf.serie,cf.nrodocumento ";
				$ord = " ORDER BY  clie.propietario";
		$consulta=$conexion->query($sql.$ord);
		$items = $consulta->fetchAll();	
		$tConceptos = array();
		foreach($items as $row)
		{
			$count++;
	        $monto += $row["imptotal"];
	        $ctd++;
	        $tusuario=0;
	         ?>
	         <tr>
	         	<td><?=$ctd?></td>
	         	<td style="mso-number-format:'\@'"><?=$objFunciones->CodUsuario($codsuc,$row['nroinscripcion'])?></td>
	         	<td><?=utf8_decode($row["propietario"])?></td>
	         	<td><?=utf8_decode($row["direccion"])?></td>
	         	<td><?=$objFunciones->DecFecha($row["fechaemision"])?></td>
	         	<td><?=$objFunciones->DecFecha($row["fechavencimiento"])?></td>
	         	<td ><?=$row["serie"]?></td>
	         	<td ><?=$row["nrodocumento"]?></td>
	         	<?php foreach($alcas as $rowc):
			        $c='c'.$rowc['codconcepto'];
			        $tusuario +=$row[$c];
			        $TotalC+=$row[$c];
		
			         $tConceptos[$c]=$tConceptos[$c]+$row[$c];
			        ?>
			        <td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($row[$c],2)?></td>			 			
			 		<?php endforeach;?>
			 		<td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($tusuario,2)?></td>	
	         </tr>

	    <?php

	    			

		}

    ?>

    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
        <td colspan="<?=(8)?>" align="center" class="ui-widget-header">Usuario Registrados: <?=$contador?></td>
        <?php
         foreach($alcas as $rowc):
         	 $c='c'.$rowc['codconcepto'];
        ?>
    	<td width="50" scope="col" align="right"  class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($tConceptos[$c],2)?></td>			 			
    	<?php endforeach;?>
    	<td width="50" scope="col" align="right" class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($TotalC,2)?></td>			 			
        </tr>
    </tfoot>
</table>
<table><tr><tr><td>&nbsp;</td></tr></tr></table>
<table>
	 <thead class="ui-widget-header" style="font-size:10px">
    	<tr title="Cabecera">
    	 	<th scope="col" class="ui-widget" colspan="4" align="center">LEYENDA</th>
    	</tr>
      <tr title="Cabecera">
        <th scope="col" width="100" align="center" class="ui-widget-header">ITEM</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">COD</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">DESCRIPCION</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">TOTAL</th>
      </tr>
      <tbody>
      	<?php
      	$i=0;
         foreach($alcas as $rowc):
         	$i++;
         	 $c='c'.$rowc['codconcepto'];
        ?>
        <tr>
        	<td><?=$i?></td>
        	<td><?=$c?></td>
           	<td><?=$rowc['descripcion']?></td>			 			
           	<td scope="col" align="right"style="mso-number-format:'\#\,\#\#0\.00';">
           		<?=number_format($tConceptos[$c],2)?>
           	</td>			 			

        </tr>
    	<?php endforeach;?>
      </tbody>
      <tfoot class="ui-widget-header" style="font-size:10px">
      	<tr>
      		<td class="ui-widget-header" colspan=3>&nbsp;</td>
      		<td align="right" class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($TotalC,2)?></td>			 			
      	</tr>
      </tfoot>

</table>
