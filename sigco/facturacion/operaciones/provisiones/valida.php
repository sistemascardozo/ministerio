<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc		= $_SESSION['IdSucursal'];
	$codciclo 	= $_POST["codciclo"]; 
	
	$facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
	
	$sql = "select count(*) from medicion.listadocorte where anio=? and mes=? and codciclo=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($facturacion["anio"],$facturacion["mes"],$codciclo));
	$items = $consulta->fetch();

	if($items[0]==0)
	{
		$img="<img src='../../../../images/iconos/error.png' />&nbsp;<label style='color:black; font-size:18; font-weight:bold'>
		El Listado aun no se ha generado...si desea generarlo presione <label style='cursor:pointer;color:red' 
		onClick='generar();'>AQUI</label></label>
		<input type='hidden' name='generado' id='generado' value='0' />";
	}else{
		$img="<img src='../../../../images/iconos/Ok.png' />&nbsp;<label style='color:black; font-size:18; font-weight:bold'>
		El Listado se ha encuentra Generado...si desea volver a generarlo presione <label style='cursor:pointer;color:red'
		onClick='generar();'>AQUI</label></label>
		<input type='hidden' name='generado' id='generado' value='1' />";
	}
	
	echo $img;
?>
