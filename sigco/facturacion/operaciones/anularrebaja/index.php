<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	$Criterio = "Rebajas";
	$TituloVentana = "ANULAR REBAJAS";
	
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	$FormatoGrilla = array ();
	$documento  = 9;
	$Criterio='Rebajas';
	
	$Sql = "SELECT 
			cab.nrorebaja,
			cab.nroreclamo,
			clie.propietario,
			".$objFunciones->FormatFecha('cab.fechaemision').",
			cab.glosa,
			est.descripcion,
			est.codestrebaja
			FROM facturacion.cabrebajas as cab
			INNER JOIN catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion)
			INNER JOIN public.estadorebaja as est on(cab.codestrebaja=est.codestrebaja)";
	
	$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
	$FormatoGrilla[1] = array('1'=>'cab.nrorebaja', '2'=>'clie.propietario','3'=>"to_char(cab.codsuc,'00')||''||TRIM(to_char(cab.nroinscripcion,'000000')) ",'4'=>'est.descripcion');          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1'=>'Nro. Rebaja', 'T2'=>'Nro. Reclamo', 'T3'=>'Propietario',
							'T4'=>'Fecha Emision', 'T5'=>'Observacion', 'T6'=>'Estado');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1'=>'90','W2'=>'90','W3'=>'380','W4'=>'90','W6'=>'50','W7'=>'1');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = "  and (cab.codemp=1 and cab.codsuc=".$codsuc.") ORDER BY cab.nrorebaja desc ";                                   //Orden de la Consulta
	$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
				'NB' => '1', //Número de Botones a agregar
				'BtnId1' => 'BtnModificar', //Nombre del Boton
				'BtnI1' => 'eliminar.png', //Imagen a mostrar
				'Btn1' => 'Anular Rebajas', //Titulo del Botón
				'BtnF1' => 'onclick="Anular(this);"', //Eventos del Botón
				'BtnCI1' => '7', //Item a Comparar
				'BtnCV1' => '1'    //Valor de comparación
			  );
	$FormatoGrilla[10] = array(array('Name' => 'estado', 'Col' => 8)); //DATOS ADICIONALES  
	
	$_SESSION['Formato'.$Criterio] = $FormatoGrilla;
	Cabecera('', $FormatoGrilla[7],1100,600);
	Pie();

?>
<script type="text/javascript">
	$('#BtnNuevoB').hide();
	
	function Anular(obj)
    {
        var Id = $(obj).parent().parent().attr('id')
        //alert(Id);
        var Estado = $(obj).parent().parent().attr('data-estado')
        if (Estado == 2)
        {
            alert('No se puede Anular la rebaja ... Puede que se encuentre Anulado')
            return false
        }


        $("#Modificar").dialog({title: 'Anular Rebaja'});
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");

        $("#button-maceptar").children('span.ui-button-text').text('Anular Rebaja')
        $.ajax({
            url: 'mantenimiento.php',
            type: 'POST',
            async: true,
            data: 'Id='+Id+'&codsuc=<?=$codsuc ?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
</script>
<?php
	CuerpoInferior();
?>
