<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	include("../../../../objetos/clsSeguridad.php");
	
	$conexion->beginTransaction();
	
	$objFunciones = new clsFunciones();
	$objSeguridad = new clsSeguridad();
	
	$codemp         = 1;
	$Op             = $_GET["op"];
	$codsuc         = $_SESSION['IdSucursal'];
	$nroinscripcion = $_POST["nroinscripcion"];
	$nrorebaja      = $_POST["nrorebaja"];
	$nroreclamo     = $_POST["nroreclamo"];
	$contador       = $_POST["contador"];
	
	$fecha = $objFunciones->FechaServer();
	$hora = $objFunciones->HoraServidor();
	$idusuario = $_SESSION["id_user"];
	
	$upd = "UPDATE facturacion.cabrebajas ";
	$upd .= "SET codestrebaja = 0 ";
	$upd .= "WHERE codemp = 1 ";
	$upd .= " AND codsuc = ".$codsuc." ";
	$upd .= " AND nrorebaja = ".$nrorebaja;
	
	$resultC = $conexion->prepare($upd);
	$resultC->execute(array());
	
	if ($resultC->errorCode() != '00000')
	{
		$conexion->rollBack();
		
		$mensaje = "Error en rebajas";
		
		die(2);
	}

    // Obtencion del numero de reclamo
	if($nroreclamo != 0 )
	{
		$sqlR = "SELECT nroreclamo FROM reclamos.reclamos ";
		$sqlR .= "WHERE codsuc = ".$codsuc." ";
		$sqlR .= " AND correlativo = ".$nroreclamo." ";
		
		$consultaM = $conexion->prepare($sqlR);
		$consultaM->execute(array());
		
		if ($consultaM->errorCode() != '00000')
		{
			$conexion->rollBack();
			
			$mensaje = "Error en seleccion de reclamos";
			
			die(2);
		}
		
		$itemsM = $consultaM->fetch();
	}


	for($i = 1; $i <= $contador; $i++)
	{
		if(isset($_POST["nrofacturacion_".$i]))
		{
			$sqlT = "SELECT codconcepto, imprebajado, nrofacturacion, codciclo ";
			$sqlT .= "FROM facturacion.detrebajas ";
			$sqlT .= "WHERE codemp = 1 ";
			$sqlT .= " AND codsuc = ".$codsuc." ";
			$sqlT .= " AND nrorebaja = ".$nrorebaja." ";
			$sqlT .= " AND nrofacturacion = ".$_POST["nrofacturacion_".$i]." ";
			$sqlT .= "GROUP BY codconcepto, imprebajado, nrofacturacion, codciclo ";
			$sqlT .= "ORDER BY imprebajado DESC";
			
			$result = $conexion->prepare($sqlT);
			$result->execute(array());
			
			if ($result->errorCode() != '00000')
			{
				$conexion->rollBack();
				
				$mensaje = "Error en rebajas";
				
				die(2);
			}
			
			$itemsT = $result->fetchAll();

			$ciclocod = $itemsT[0]['codciclo'];
			
			foreach ($itemsT as $key)
			{
				$sqlT = "UPDATE facturacion.detfacturacion ";
				$sqlT .= "SET importerebajado = 0 ";
				$sqlT .= "WHERE codemp = 1 ";
				$sqlT .= " AND codsuc = ".$codsuc." ";
				$sqlT .= " AND nrofacturacion = ".$key['nrofacturacion']." ";
				$sqlT .= " AND nroinscripcion = ".$nroinscripcion." ";
				$sqlT .= " AND codconcepto = ".$key['codconcepto'];
				
				$result = $conexion->prepare($sqlT);
				$result->execute(array());
				
				if ($result->errorCode() != '00000')
				{
					$conexion->rollBack();
					
					$mensaje = "Error en facturacion";
					
					die(2);
				}

				if($nroreclamo != 0)
				{
					$sqlT1 = "UPDATE reclamos.mesesreclamados ";
					$sqlT1 .= "SET imprebajado = 0 ";
					$sqlT1 .= "WHERE codemp = 1 ";
					$sqlT1 .= " AND codsuc = ".$codsuc." ";
					$sqlT1 .= " AND nrofacturacion = ".$key['nrofacturacion']." ";
					$sqlT1 .= " AND nroreclamo = ".$itemsM['nroreclamo']." ";
					$sqlT1 .= " AND codconcepto = ".$key['codconcepto'];
					
					$result1 = $conexion->prepare($sqlT1);
					$result1->execute(array());
					
					if ($result1->errorCode() != '00000')
					{
						$conexion->rollBack();
						
						$mensaje = "Error en meses reclamados";
						
						die(2);
					}
				}
			}

			$SqlImpM = "SELECT SUM(importe - importerebajado) AS suma ";
			$SqlImpM .= "FROM facturacion.detfacturacion ";
			$SqlImpM .= "WHERE codemp = 1 ";
			$SqlImpM .= " AND codsuc = ".$codsuc." ";
			$SqlImpM .= " AND nrofacturacion = ".$_POST["nrofacturacion_".$i]." ";
			$SqlImpM .= " AND nroinscripcion = ".$nroinscripcion." ";
			
			$consultaImpM = $conexion->prepare($SqlImpM);
			$consultaImpM->execute(array());
			
			if ($consultaImpM->errorCode() != '00000') 
			{
				$conexion->rollBack();
				
				$mensaje = "Error reclamos";
				
				die(2);
			}
			
			$itemImpM = $consultaImpM->fetch();
			
			$sqlT = "UPDATE facturacion.cabfacturacion ";
			$sqlT .= "SET impmes = '".$itemImpM['suma']."' ";
			$sqlT .= "WHERE codemp = 1 ";
			$sqlT .= " AND codsuc = ".$codsuc." ";
			$sqlT .= " AND codciclo = ".$ciclocod." ";
			$sqlT .= " AND nrofacturacion = ".$_POST["nrofacturacion_".$i]." ";
			$sqlT .= " AND nroinscripcion = ".$nroinscripcion;
			
			$result = $conexion->prepare($sqlT);
			$result->execute(array());
			
			if ($result->errorCode() != '00000')
			{
				$conexion->rollBack();
				
				$mensaje = "Error en cabecera facturacion";
				
				die(2);
			}
			
			if($nroreclamo != 0)
			{
				$sqlT = "UPDATE facturacion.cabfacturacion ";
				$sqlT .= "SET enreclamo = 1 ";
				$sqlT .= "WHERE codemp = 1 ";
				$sqlT .= " AND codsuc = ".$codsuc." ";
				$sqlT .= " AND codciclo = ".$ciclocod." ";
				$sqlT .= " AND nrofacturacion = ".$_POST["nrofacturacion_".$i]." ";
				$sqlT .= " AND nroinscripcion = ".$nroinscripcion;
				
				$result = $conexion->prepare($sqlT);
				$result->execute(array());
				
				if ($result->errorCode() != '00000')
				{
					$conexion->rollBack();
					
					$mensaje = "Error en cabecera facturacion";
					
					die(2);
				}

				// Actualizando el detalle de reclamos
				$sqlT2 = "UPDATE reclamos.reclamos ";
				$sqlT2 .= "SET codestadoreclamo = 4 ";
				$sqlT2 .= "WHERE codsuc = ".$codsuc." ";
				$sqlT2 .= " AND correlativo = ".$nroreclamo." ";
				
				$result2 = $conexion->prepare($sqlT2);
				$result2->execute(array());
				
				if ($result2->errorCode() != '00000')
				{
					$conexion->rollBack();
					
					$mensaje = "Error en reclamos";
					
					die(2);
				}

				// Actualizando el detalle de reclamos
				$sqlT2 = "UPDATE reclamos.detalle_reclamos ";
				$sqlT2 .= "SET codestadoreclamo = 4 ";
				$sqlT2 .= "WHERE codsuc = ".$codsuc." ";
				$sqlT2 .= " AND nroreclamo = ".$itemsM['nroreclamo'];
				
				$result = $conexion->prepare($sqlT2);
				$result->execute(array());
			}
		}  
	}

    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
