<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../config.php");
	
	$nrorebaja      = $_POST["nrorebaja"];
	$codsuc         = $_POST["codsuc"];
	$nrofacturacion = $_POST["nrofacturacion"];
	$index          = $_POST["index"];
	$Todo           = $_POST["Todo"]?$_POST['Todo']:0;
	$style          ='style:"display:none;"';

	$count=0;
	
	$sql = "select d.codconcepto,c.descripcion,
          sum(d.imporiginal) as original, 
          sum(d.imprebajado) as rebajado 
          from facturacion.detrebajas as d 
          inner join facturacion.conceptos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto) 
          where d.codsuc=? and d.nrorebaja=? 
          and d.nrofacturacion=?
          group by d.codconcepto,c.descripcion order by d.codconcepto";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nrorebaja,$nrofacturacion));
	$items = $consulta->fetchAll();	
	$total = 0;
?>
<div style="height:300px; overflow:auto">
<table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbdetallefacturacion" rules="all">
	<thead class="ui-widget-header" >
      <tr>
        <th width="85%" >Concepto</th>
        <th width="15%" >Importe Original</th>
        <th width="15%" >Importe Rebajado</th>
        <th width="15%" >&nbsp;</th>
     </tr>
   </thead>
   <tbody>
     <?php
      $total  = 0;
      $totalr = 0;
	 	foreach($items as $row)
		{
			$count++;
			
      $total  += $row["original"];
      $totalr += $row["rebajado"];
	 ?>
     <tr style="background:#FFF; color:#000;">
      <td align="left" >
        <input type="hidden" name="codconceptoy<?=$count?>" id="codconceptoy<?=$count?>" value="<?=$row["codconcepto"]?>" />
      	<?=strtoupper($row["descripcion"])?>
      </td>
      <td align="right" >
        <input type="hidden" name="impx<?=$count?>" id="impx<?=$count?>" value="<?=$row["original"]?>" />
        <?=number_format($row["original"], 2)?>
      </td>
      <td align="right" >
	      <input type="hidden" name="impx<?=$count?>" id="impx<?=$count?>" value="<?=$row["rebajado"]?>" />
	      <?=number_format($row["rebajado"], 2)?>
      </td>
     </tr>
     <?php } ?>
</tbody>
     <tfoot class="ui-widget-header">
     <tr style="background:#FFF; color:#000;">
        <td align="right" >Total Facturacion ==></td>
        <td align="right" >
			       <?=number_format($total,2)?>
            <input name="imptotalh" type="hidden" id="imptotalh" value="<?=$total?>" />
        </td>
        <td align="right" >
          <?=number_format($totalr,2)?>
            <input name="imptotalhr" type="hidden" id="imptotalhr" value="<?=$totalr?>" />
        </td>
     </tr>
</tfoot>
</table>
</div>
<div style="height:10px">
</div>
<script type="text/javascript">
function ingresarimporte()
{
  if($("#checkboximp").attr('checked'))
    cargar_from_detalle_facturacion_2()
  else
     cargar_from_detalle_facturacion('<?=$nroinscripcion?>', '<?=$nrofacturacion?>', '<?=$categoria?>', '<?=$codtipodeuda?>', '<?=$index?>')
}
function cargar_from_detalle_facturacion_2()
    {
        ItemUpd = <?=$index?>;
        $.ajax({
            url: '../ingresos/ajax/from_detalle_facturacion.php',
            type: 'POST',
            async: true,
            data: "nroinscripcion=<?=$nroinscripcion?>&codsuc=<?=$codsuc?>&nrofacturacion=<?=$nrofacturacion?>&categoria=<?=$categoria?>&codtipodeuda=<?=$codtipodeuda?>&index=<?=$index?>&Todo=1",
            success: function (datos)
            {
                $("#div_detalle_facturacion").html(datos)
                $("#DivImporteRebaja").show();
            }
        })
    }
    function recalcularmontos()
    {
      var total=0;
      var id=parseInt($("#tbdetallefacturacion tbody tr").length)
      for(var i=1; i<=id; i++)
      { 
        
        t =$("#impamort"+i).val()
        total+= parseFloat(t);
        
      }
      var imptotalh   =  parseFloat($("#imptotalh").val())
      total = imptotalh - parseFloat(total)
     
      $("#mrebajar").val(parseFloat(total).toFixed(2));
    }
</script>