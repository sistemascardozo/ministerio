<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");

    $Op      = $_GET["Op"];
    $Id      = isset($_POST["Id"]) ? $_POST["Id"] : '';
    $Rws= split('[-]', $Id);
    $Id = $Rws[1];

    $codsuc  = $_SESSION['IdSucursal'];
    $guardar = "op=".$Op;

    $objMantenimiento = new clsDrop();

    // Cabecera de la Rebaja

    $Select = " SELECT cab.nroinscripcion,
    clie.propietario,
    cab.fechareg,
    (cab.serie ||'-'||cab.nrodocumento) as comprobante,
    (cab.nroreclamo) as nroreclamo,
    cab.nrorebaja 
    FROM facturacion.cabrebajas as cab 
    INNER JOIN catastro.clientes as clie on(cab.codemp=clie.codemp AND cab.codsuc=clie.codsuc AND cab.nroinscripcion=clie.nroinscripcion) 
    WHERE cab.codemp=1 AND cab.codsuc=? AND cab.nrorebaja=?";

    $consulta = $conexion->prepare($Select);
    $consulta->execute(array($codsuc, $Id));
    $row = $consulta->fetch();

    if ($row['nrorebaja'] != 0) {
        $sql = "SELECT SUM(cab.imprebajado) as cantidad
                FROM facturacion.detrebajas as cab 
                WHERE cab.codemp=1 and cab.codsuc=? and cab.nrorebaja=?";
        $consulta2 = $conexion->prepare($sql);
        $consulta2->execute(array($codsuc, $row['nrorebaja']));
        $row2      = $consulta2->fetch();
        $cantidadrebaja = $row2['cantidad'];
    }

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>

<script>
    var codsuc = <?=$codsuc?>;
    $(document).ready(function ()
    {
        $("#DivRebajar").dialog({
            autoOpen: false,
            height: 450,
            width: 650,
            modal: true,
            resizable: false,
            buttons: {
                "Aceptar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            }
        });
    });

function cargar_from_detalle_facturacion(nroinscripcion, nrofacturacion, categoria, codtipodeuda, idx, nrorebaja)
    {
        ItemUpd = idx;
        $.ajax({
            url: '../anularrebaja/ajax/from_detalle_facturacion.php',
            type: 'POST',
            async: true,
            data: 'nrorebaja=' + nrorebaja +'&nroinscripcion=' + nroinscripcion + '&codsuc=' + codsuc + '&nrofacturacion=' + nrofacturacion +
                    '&categoria=' + categoria + '&codtipodeuda=' + codtipodeuda + '&index=' + idx,
            success: function (datos)
            {
                $("#div_detalle_facturacion").html(datos)

                $("#mrebajar").val($("#tbrebajas tbody tr#tr_" + ItemUpd + " label.ImpRebajado").text()).keyup()
                $("#consumocalcular").val($("#tbrebajas tbody tr#tr_" + ItemUpd + " label.consumoreclamo").text());
                // CalcularImporteConsumo();
                $("#DivRebajar").dialog("open");
                $("#DivImporteRebaja").show();
            }
        })
    }
    function NoAnular()
    {
        alert('No se puede Anular la Rebaja')
        return false
    }
    function ValidarForm(Op)
    {
        var c   = 0;
        var dat = 0;
        if($("#observacion").val()=='')
        {
            Msj($("#observacion"),"Digite en la observacion, motivo de la anulacion")
            return false;
        }

        $("#tbdetalle tbody tr").each(function(){

            dat = $(this).children('td:last').children('#estfacturacion').val();
            if(dat == 2){c++};
        });

        if(c>0)
        {
            Msj('#tbdetalle','Existe facturacion pagadas');
            return false;
        }
        GuardarP(Op)
    }
    function Cancelar()
    {
        location.href = 'index.php?Op=1';
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">

        <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            
            <tbody>
                <tr>
                    <td colspan="9" align="center" >&nbsp;</td>
                </tr>
                <tr>
                    <td width="80" >Nro. Rebaja </td>
                    <td width="30" align="center">:</td>
                    <td >
                        <input type="text" name="nrorebaja" id="nrorebaja" class="inputtext" size="12" maxlength="12" value="<?= $Id ?>" readonly="readonly" />
                  </td>
                    <td align="left" colspan="6">
                        <?php
                        if ($cantidadrebaja > 0) {
                            ?>
                            <div id="div_estadoservicio" style="color:#FF0000; font-size:15px; font-weight:bold; padding:1px; display:inline">
                                La Facturacion Presenta Rebajas
                            </div>
                            <?php }
                        ?>
                    </td>
              </tr>
                <tr>
                    <td width="95" >Codigo</td>
                    <td width="19" align="center">:</td>
                  <td width="372" ><label>
                            <input type="text" name="nroinscripcion" id="nroinscripcion" class="inputtext" size="12" maxlength="12" readonly="readonly" value="<?= $row[0] ?>" />
                        </label></td>
                    <td width="90" align="right" >Fecha Emision</td>
                    <td width="30" align="center">:</td>
                    <td colspan="5" ><label>
                            <input type="text" name="femision" id="femision" size="15" maxlength="15" class="inputtext" readonly="readonly" value="<?= $objMantenimiento->DecFecha($row["fechareg"]) ?>" />
                        </label>
                  </td>
                </tr>
                <tr>
                    <td >Nombres</td>
                    <td align="center">:</td>
                    <td ><label>
                            <input type="text" name="nombres" id="nombres" class="inputtext" maxlength="65" readonly="readonly" value="<?=$row[1]?>" style="width:400px;" />
                        </label></td>
                    <?php if($row['nroreclamo'] != 0):?>
                    <td align="right">Nro. de Reclamo </td>
                    <td align="center">:</td>
                    <td ><input type="text" name="nroreclamo" id="nroreclamo" size="15" maxlength="15" class="inputtext" readonly="readonly" value="<?= $row['nroreclamo'] ?>" />
                        <label></label>
                    </td>
                    <?php else: ?>
                    <td align="right">Nro. de Reclamo </td>
                    <td align="center">:</td>
                    <td >
                        <label style="color:#FF0000; font-size:12px;">Sin Reclamo</label>
                    </td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <td colspan="9">
                        <table border="1" align="center" cellspacing="0" class="ui-widget myTable01"  width="100%" id="tbdetalle" rules="all" >
                        <thead class="ui-widget-header" >
                            <tr align="center">
                                <th width="11%" >Nro. Doc.</th>
                                <th width="12%" >A&ntilde;o y Mes</th>
                                <th width="11%" >Categoria</th>
                                <th width="12%" >Imp. Original</th>
                                <th width="20%" >Imp. Rebajado</th>
                                <th width="15%" >Estado de Fact.</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php
                                $contador = 0;
                                if ($row['nrorebaja'] != "") {
                                    $sqlmeses = "SELECT cab.nrofacturacion,
                                    upper(doc.abreviado) as documento,
                                    cab.nrodocumento as nrodocumento,
                                    cab.anio as anio,
                                    cab.mes as mes,
                                    upper(tipd.descripcion),
                                    cab.categoria as categoria,
                                    cab.codtipodeuda as codtipodeuda,
                                    SUM(cab.imprebajado) as rebajado,
                                    SUM(cab.imporiginal) as original,
                                    cab.codciclo as codciclo,
                                    cab.nrorebaja as nrorebaja,
                                    caf.estadofacturacion as estadofacturacion,
                                    caf.enreclamo as enreclamo
                                    FROM facturacion.detrebajas as cab
                                    INNER JOIN reglasnegocio.documentos as doc on(cab.coddocumento=doc.coddocumento) AND (cab.codsuc=doc.codsuc)
                                    INNER JOIN public.tipodeuda as tipd on(cab.codtipodeuda=tipd.codtipodeuda)
                                    INNER JOIN facturacion.cabrebajas as cre ON (cre.nrorebaja = cab.nrorebaja AND cre.codemp = cab.codemp AND cre.codsuc = cab.codsuc)
                                    INNER JOIN facturacion.cabfacturacion caf ON (caf.codemp = cab.codemp AND caf.codsuc = cab.codsuc AND caf.nrofacturacion = cab.nrofacturacion AND caf.nroinscripcion = cre.nroinscripcion)
                                    WHERE cab.nrorebaja = ? and cab.codemp=1 and cab.codsuc=?
                                    GROUP BY cab.nrorebaja, cab.codtipodeuda,cab.nrofacturacion,
                                    doc.abreviado,cab.nrodocumento,
                                    cab.anio,cab.mes,cab.categoria,tipd.descripcion,
                                    cab.codciclo,caf.estadofacturacion, caf.enreclamo
                                    ORDER BY cab.nrofacturacion";
                                    $consulta_meses = $conexion->prepare($sqlmeses);
                                    $consulta_meses->execute(array($row['nrorebaja'], $codsuc));
                                    $items_meses = $consulta_meses->fetchAll();

                                    foreach ($items_meses as $row_meses) {

                                        $contador++;
                                        $cat = "";
                                        if ($row_meses['categoria'] == 0) {
                                            $cat = "FACTURACION";
                                        }
                                        if ($row_meses['categoria'] == 1) {
                                            $cat = "SALDO";
                                        }
                                        if ($row_meses['categoria'] == 2) {
                                            $cat = "SALDO REFINANCIADO";
                                        }
                                        if ($row_meses['categoria'] == 3) {
                                            $cat = "PRESTACION DE SERVICIOS EN CAJA";
                                        }

                                        $estadofacturacion = "";
                                        if ($row_meses['estadofacturacion'] == 1) {
                                            $estadofacturacion = "<span style='color:#100A52'>PENDIENTE</span>";
                                        }
                                        if ($row_meses['estadofacturacion'] == 2) {
                                            $estadofacturacion = "<span style='color:#0A5212'>PAGADO</span>";
                                        }
                                        if ($row_meses['estadofacturacion'] == 3) {
                                            $estadofacturacion = "<span style='color:#FCD90D'>AMORTIZADO</span>";
                                        }
                                        if ($row_meses['estadofacturacion'] == 4) {
                                            $estadofacturacion = "<span style='color:#FF0000'>ANULADO</span>";
                                        }
                                    ?>
                                    <tr style='background-color:#FFFFFF' align="center" id='tr_<?=$contador ?>'>
                                        <td align="center" ><?php echo $row_meses[1] ?>:<?php echo $row_meses['seriedocumento'] ?>-<?php echo $row_meses[2] ?></td>
                                        <td align="center" ><?php echo $row_meses[3] . "-" . $meses[$row_meses[4]] ?></td>
                                        <td align="center" ><?php echo $cat ?></td>
                                        <td align="right" ><?=number_format($row_meses[9], 2) ?></td>
                                        <td align="right" ><label class="ImpRebajado"><?=number_format($row_meses[8], 2) ?></label></td>
                                        <td align="center">
                                            <?=$estadofacturacion?>
                                            <input type="hidden" name='estfacturacion' id='estfacturacion' value="<?=$row_meses['estadofacturacion']?>">
                                            <input type="hidden" name='nrofacturacion_<?=$contador ?>' id='nrofacturacion_<?=$contador ?>' value="<?=$row_meses['nrofacturacion']?>">
                                            <input type="hidden" name='enreclamo_<?=$contador ?>' id='enreclamo_<?=$contador ?>' value="<?=$row_meses['nrofacturacion']?>">
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                }
                                ?>
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="9" align="center" >&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" >Observacion</td>
                    <td align="center" valign="top">:</td>
                    <td colspan="7"><label>
                            <textarea name="observacion" id="observacion" class="inputtext" style="font-size:11px" cols="110" rows="5"></textarea>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="9" class="TitDetalle">
                        <input type="hidden" name="count" id="count" value="0" />
                        <input type="hidden" name="contador" id="contador" value="<?=$contador?>" />
                    &nbsp;</td>
                </tr>
            </tbody>

        </table>
    </form>
</div>
<div id="DivRebajar" title="Ver Detalle de Facturacion para Realizar la Rebaja"  >
    <div id="div_detalle_facturacion"></div>
</div>