// JavaScript Document
$(document).ready(function () {

    $("#fechafacturacion").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                        showButtonPanel: true
            });

    $("#dialogo_barras").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        title: "Realizando el Proceso de facturacion. Espere por Favor",
        height: 130,
        open: function (event, ui) {
            //ocultar el boton de cerrar en el dialogo.
            $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
        }
    });

    $("#DivUsuarios").dialog({
        autoOpen: false,
        height: 500,
        width: 600,
        modal: true,
        resizable: false,
        buttons: {
            "Cancelar": function () {
                $(this).dialog("close");
            },
            "Excel": function () {
                var ventana = window.open('Excel.php?codsuc=' + codsuc, 'Excel Listado', 'resizable=yes, scrollbars=yes');
                ventana.focus();
            }
        },
        close: function () {

        }
    });

    //preparación de la barra de progreso
    $("#barra").progressbar({
        value: 0
    });


    $("#DivPorcentaje").dialog({
        autoOpen: false,
        height: 500,
        width: 400,
        modal: true,
        resizable: false,
        buttons: {
            "Aceptar": function () {
                grabar_porcentaje();
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        },
        close: function () {

        }
    });
})
function datos_facturacion(codciclo)
{
    $.ajax({
        url: '../../../../ajax/periodo_facturacion.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + codciclo + '&codsuc=' + codsuc,
        success: function (datos) {
            var r = datos.split("|")
            ActivarButton($('#btnAp'));
            $("#anio").val(r[1])
            $("#mes").val(r[2])
            $("#mes_num").val(r[3])
            $("#nrofacturacion").val(r[0])

            $("#control_cierre_cobranza").val(r[4]);
            $("#control_provisiones").val(r[5]);
            $("#control_porcentaje_mes").val(r[6]);
            if (r[4] == 1)
            {

                ActivarButton($('#cierrecobranza'));
                ActivarButton($('#cierrelecturas'));

            }
            if (r[5] == 1)
            {
                ActivarButton($('#porcinteres'));
            }

            if (parseFloat(r[6]) > 0 && r[4] == 1 && r[5] == 1)
            {
                ActivarButton($('#calculofacturacion'));
            }
        }
    })
}
function ActivarButton(obj)
{
    $(obj).attr('disabled', true);
    $(obj).attr('disabled', false);
    $(obj).removeClass("ui-button-disabled ui-state-disabled");
}
function Backup()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }


    $.blockUI({message: '<div><center><span class="icono-icon-loading"></span>Generando Backup...Espere un Momento por Favor...</center></div>',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    $.ajax({
        url: 'Backup.php',
        type: 'POST',
        async: true,
        data: 'codsuc=' + codsuc,
        success: function (data) {
            if (parseInt(data) == 0)
            {
                window.parent.OperMensaje("Error al Generar el Backup", 2);


            }
            else
            {

                window.parent.OperMensaje("Backup Generado Correctamente", 1);
				
                Msj($("#btnAp"), 'Backup Generado Correctamente')

            }
			
            ActivarButton($('#cierrecobranza'));

            $.unblockUI()
        }


    })

}

function cierre_cobranza()
{

    if ($("#control_cierre_cobranza").val() == 1)
    {
        Msj($("#cierrecobranza"), "El Cierre de Cobranza ya se ha Realizado...Proceda al Siguiente Paso")
        return false
    }

    var r = confirm('Desea Realizar el Cierre de Cobranza?')
    if (r == false)
    {
        return 	true
    }
    window.parent.blokear_pantalla("Se Esta Realizando el Cierre de Cobranza Espere por Favor")

    $.ajax({
        url: 'calculo/cobranza.php',
        type: 'POST',
        async: true,
        dataType: 'json',
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc + '&fechafacturacion=' + $("#fechafacturacion").val(),
        success: function (datos)
        {
            if (datos.res == 0)
            {
                $("#Div_Usuarios").html(datos.tr)
                $("#control_cierre_cobranza").val(datos.res)
                $("#DivUsuarios").dialog('open');
            }
            else
                ActivarButton($('#cierrelecturas'));
            $("#control_cierre_cobranza").val(datos.res)
            window.parent.establecer_texto(datos.img, datos.mensaje)


        },
        error: function (jqXHR, exception) {
            Msj($("#cierrecobranza"), 'Ocurrio un problema');
            desbloquear_pantalla();
        }
    })

}
function provisionar()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }

    if ($("#control_provisiones").val() == 1)
    {
        Msj($("#cierrelecturas"), "La Provision ya se ha Realizado...Proceda al Siguiente Paso")
        return false
    }

    var r = confirm('Desea Realizar la Provision?')
    if (r == false)
    {
        return 	true
    }

    window.parent.blokear_pantalla("Se Esta Realizando la Provision. Espere por Favor!!")

    $.ajax({
        url: 'calculo/provision.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc,
        success: function (datos) {
            var r = datos.split("|")

            window.parent.establecer_texto(r[0], r[1])

            $("#control_provisiones").val(r[2])
            ActivarButton($('#porcinteres'));
        }
    })

}
function cargar_porcentaje()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }
    if ($("#control_cierre_cobranza").val() == 0)
    {
        Msj($("#porcinteres"), "Para Grabar el Porcentaje de Interes es Necesario que realize el Cierre de Cobranza")
        return false
    }
    if ($("#control_provisiones").val() == 0)
    {
        Msj($("#porcinteres"), "Para Grabar el Porcentaje de Interes es Necesario que realize el Cierre de Lecturas")
        return false
    }

    cargar_pantalla_porcentaje($("#ciclo").val())
    $("#DivPorcentaje").dialog("open");
}

function cargar_pantalla_porcentaje(codciclo)
{
    $.ajax({
        url: 'calculo/porcentaje.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + codciclo + '&codsuc=' + codsuc,
        success: function (datos) {
            $("#div_procentaje").html(datos)
        }
    })
}
function grabar_porcentaje()
{
    if ($("#porcentaje").val() < 0 || $("#porcentaje").val() == "")
    {
        Msj($("#porcentaje"), "El Valor del Porcentaje no es Valido!!!")
        return
    }

    $.ajax({
        url: 'calculo/guardar_porcentaje.php',
        type: 'POST',
        async: true,
        data: 'nrofacturacion=' + $("#nrofacturacion").val() + '&codsuc=' + codsuc + '&codciclo=' + $("#ciclo").val() + '&porcentaje=' + $("#porcentaje").val(),
        success: function (datos) {
            var r = datos.split("|")

            $("#control_porcentaje_mes").val(r[2])
            $("#DivPorcentaje").dialog("close");
            
			Msj($("#porcinteres"), r[1]);
			
            ActivarButton($('#calculofacturacion'));
        }
    })
}
function generar_facturacion()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }

    if ($("#control_cierre_cobranza").val() == 0)
    {
        Msj($("#cierrecobranza"), "Para Generar el Proceso de Facturacion realize el Cierre de Cobranza")
        return false
    }
    if ($("#control_provisiones").val() == 0)
    {
        Msj($("#cierrelecturas"), "Para Generar el Proceso de Facturacion realize el Cierre de Lecturas")
        return false
    }
    if ($("#control_porcentaje_mes").val() < 0)
    {
        Msj($("#porcinteres"), "El Porcentaje de Interes Ingresado no es Valido!!!")
        return false
    }

    var r = confirm('Desea Realizar el Proceso de Facturacion')
    if (r == false)
    {
        return 	true
    }

    //window.parent.blokear_pantalla("Se Esta Realizando el Proceso de facturacion. Espere por Favor!!")
    $("#dialogo_barras").dialog('open');

    //clearInterval(session);
    $.ajax({
        url: 'calculo/facturacion.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc + '&porcentaje=' + $("#control_porcentaje_mes").val() +
                '&nrofacturacion=' + $("#nrofacturacion").val() + '&factura_alcantarillado=' + $("#factura_alcantarillado").val() +
                '&mes=' + $("#mes_num").val() + '&anio=' + $("#anio").val() + '&fechafacturacion=' + $("#fechafacturacion").val(),
        success: function (datos) {
            var r = datos.split("|");

            $("#dialogo_barras").dialog('close');

            window.parent.blokear_pantalla("");
            window.parent.establecer_texto(r[0], r[1]);

            $("#barra").progressbar({value: 0}); //actualizar la barra.
            $("#titulo_barra").html('Calculando...'); //actualizar etiqueta.

            ActivarButton($('#saldos'));
            //clearInterval(timerBarra); 
            //session = setInterval(session,1000);
        }
    });

    timerBarra = setInterval(actualizarBarra, 1000);
}

function actualizarBarra()
{
    //leer el archivo de texto que contiene el procentaje y avance del proceso principal.
    $.ajax({
        url: 'progreso.txt',
        type: 'POST',
        async: true,
        data: '',
        success: function (texto) {
            var partes	= texto.split('|'),
			porcentaje	= parseInt(partes[0]),
			avance		= partes[1],
			etiqueta	= partes[0] + "% (" + partes[1] + ")";
//alert(texto);
            if (porcentaje == 100) //llegamos al 100% fin de proceso.
            {
                clearInterval(timerBarra); //borrar timer.
            }

            $("#barra").progressbar({value: porcentaje}); //actualizar la barra.
            $("#titulo_barra").html(etiqueta); //actualizar etiqueta.
        },
        error: function (error, textStatus, errorThrown) {
            clearInterval(timerBarra);
            timerBarra = setInterval(actualizarBarra, 1000);
        }
    })

    // //leer el archivo de texto que contiene el procentaje y avance del proceso principal.
    // $.get('progreso.php',function(texto){ 
    //     var partes        = texto.split('|'), 
    //         porcentaje    = parseInt(partes[0]),
    //         avance        = partes[1],
    //         etiqueta      = partes[0] + "% (" + partes[1] + ")";

    //    // if (porcentaje == 100) //llegamos al 100% fin de proceso.
    //      //   clearInterval(timerBarra); //borrar timer.

    //     $("#barra").progressbar({value: porcentaje}); //actualizar la barra.
    //     $("#titulo_barra").html(etiqueta); //actualizar etiqueta.
    // });
}
function actualizar_saldos()
{
    if ($("#ciclo").val() == 0)
    {
        Msj($("#ciclo"), "Seleccione el Ciclo")
        return false
    }

    var r = confirm('Desea Realizar el Proceso de Generación de Históricos')
    if (r == false)
    {
        return 	true
    }

    window.parent.blokear_pantalla("Se Esta Generando los datos Hist&oacute;ricos. Espere por Favor!!!")

    $.ajax({
        url: 'calculo/actualizasaldo.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc,
        success: function (datos) {
            var r = datos.split("|")

            window.parent.establecer_texto(r[0], r[1])

        }
    })
}
