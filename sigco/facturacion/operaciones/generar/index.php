<?php
include("../../../../include/main.php");
include("../../../../include/claseindex.php");

$TituloVentana = "PROCESAR FACTURACION";
$Activo = 1;
CuerpoSuperior($TituloVentana);
$codsuc = $_SESSION['IdSucursal'];
$objMantenimiento = new clsDrop();

$codsuc = $_SESSION['IdSucursal'];

$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);

$empresa = $objMantenimiento->datos_empresa($codsuc);
$fechaserver = $objMantenimiento->FechaServer();
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_facturacion.js" language="JavaScript"></script>
<style type="text/css">
    .button {cursor:pointer;}
</style>
<script>
    var codsuc = <?=$codsuc ?>;
    $(document).ready(function () {
        $(".button").button();

    });
    function ValidarForm(Op)
    {
        GuardarP(Op);
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="780" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>

                <tr>
                    <td colspan="5" class="TitDetalle" style="padding:4px">
                        <fieldset>
                            <legend class="ui-state-default ui-corner-all">Datos de la Empresa</legend>
                            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="63%" rowspan="5" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr valign="top">
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td align="center">&nbsp;</td>
                                                <td colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr valign="top">
                                                <td width="10">&nbsp;</td>
                                                <td width="80">Empresa</td>
                                                <td width="30" align="center">:</td>
                                                <td colspan="4">
                                                    <input name="empresa" id="empresa" readonly="readonly" class="inputtext" value="<?=$empresa["razonsocial"] ?>" style="width:240px;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Ciclo</td>
                                                <td align="center">:</td>
                                                <td colspan="4">
                                                    <?php $objMantenimiento->drop_ciclos($codsuc, 0, "onchange='datos_facturacion(this.value);'"); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>A&ntilde;o</td>
                                                <td align="center">:</td>
                                                <td width="12%"><input type="text" name="anio" id="anio" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:50px;" /></td>
                                                <td width="11%" align="right">Mes</td>
                                                <td width="5%" align="center">:</td>
                                                <td width="50%"><input type="text" name="mes" id="mes" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:150px;" />
                                                    <input type="hidden" name="mes_num" id="mes_num" value="0" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Facturacion</td>
                                                <td align="center">:</td>
                                                <td><input type="text" name="nrofacturacion" id="nrofacturacion" readonly="readonly" class="inputtext" size="10" maxlength="10" value="" style="width:50px;" /></td>
                                                <td align="right">Fecha</td>
                                                <td align="center">:</td>
                                                <td><input type="text" name="fechafacturacion" id="fechafacturacion"  class="inputtext" maxlength="10" value="<?=$fechaserver ?>" style="width:80px;" /></td>
                                            </tr>
                                            <tr style="padding:4px">
                                                <td colspan="7" align="left"><label></label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">&nbsp;</td>
                    <td colspan="4" class="CampoDetalle"><input type="hidden" name="control_cierre_cobranza" id="control_cierre_cobranza" value="0" />
                        <input type="hidden" name="control_provisiones" id="control_provisiones" value="0" />

                        <input type="hidden" name="control_porcentaje_mes" id="control_porcentaje_mes" value="0" />
                        <input type="hidden" name="factura_alcantarillado" id="factura_alcantarillado" value="<?=$empresa["facturaalcantarillado"] ?>" />
                        <input type="hidden" name="control_facturacion" id="control_facturacion" value="0" /></td>
                </tr>
                <tr style="padding:4px">
                    <td colspan="5" class="TitDetalle"><table width="100%" border="0" style="border:1px #000000 dashed" cellspacing="0" cellpadding="0">
                            <tr style="padding:4px">
                                <td align="center">
                                    <div style="margin:5px;">
                                        <input type="button" id="btnAp" class="button" value="(1ro) Copia de Seguridad" style="width:240px; text-align:left" onclick="Backup();"  disabled="disabled"/>
                                    </div>
                                </td>
                                <td align="center">
                                    <div style="margin:5px;">
                                        <input type="button" id="cierrecobranza" class="button" value="(2do) Cierre de Cobranza y Lecturas" style="width:240px; text-align:left" onclick="cierre_cobranza();" disabled="disabled"  />
                                    </div>
                                </td>

                                <td align="center"><input type="button" id="cierrelecturas" value="(3ro) Provisionar Deudas" class="button" style="width:240px;text-align:left" onclick="provisionar();" disabled="disabled" /></td>


                            </tr>

                            <tr style="padding:4px">
                                <td align="center"><input type="button"  id="porcinteres" class="button" value="(4to) Grabar Porcentaje de Interes" style="width:240px;text-align:left" onclick="cargar_porcentaje();" disabled="disabled"/></td>
                                <td align="center"> <input type="button" id="calculofacturacion" class="button" value="(5to) Calculo de Facturacion" style="width:240px;text-align:left" onclick="generar_facturacion();"  disabled="disabled"/></td>

                                <td align="center"><input type="button" name="saldos" id="saldos" value="(6to) Generaci&oacute;n de Hist&oacute;ricos" class="button" style="width:240px;text-align:left" onclick="actualizar_saldos();" disabled="disabled"></td>


                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td width="107" class="TitDetalle">&nbsp;</td>
                    <td width="573" colspan="4" class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div id="DivUsuarios" title="Usuarios con Creditos sin Catastrar"  >
    <div id="Div_Usuarios"></div>
</div>
<div id="DivPorcentaje" title="Grabar Porcenjate de Interes"  >
    <div id="div_procentaje"></div>
</div>
<div id="dialogo_barras">
    <span id="titulo_barra">0 %</span>
    <div id="barra"> </div>
</div>
<?php CuerpoInferior(); ?>