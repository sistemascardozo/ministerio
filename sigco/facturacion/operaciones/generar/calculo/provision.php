<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	set_time_limit(0);
	//ini_set("display_errors",1);
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();

	$codemp	= 1;
	$codsuc	= $_POST['codsuc'];
	$codusu	= $_SESSION['id_user'];
	$dias	= 6;//$_POST["dias"];
	
	if($_POST["fecha"]!='')
	{
		$fecha = $objFunciones->CodFecha($_POST["fecha"]);
	}
	else
	{
		$fecha = date('Y-m-d');
	}
	
	$nrolistado = 0;
	
	$tipo = 1;//
	
	$estareg = 1;//
	$estadoprovision = 1;//
	$fechareg = date('Y-m-d');//$objFunciones->FechaServer();
	
	$horareg = $objFunciones->HoraServidor();
	
	$codciclo = $_POST["codciclo"];
	
	$facturacion = $objFunciones->datosfacturacion($codsuc, $codciclo);
	
	$anio = $facturacion["anio"];
	$mes = intval($facturacion["mes"]);
	$factactual = $facturacion['nrofacturacion'];
	
	$nrotarifa	= $facturacion["nrotarifa"];
	
	$nombrelistado = 'PROVISION DE COB. DUDOSA '.$meses[$mes].' '.$anio;//$_POST["nombrelistado"];
	$resolucion = $nombrelistado;
	//regularizar
	//$anio =$_POST["anio"];//'2015';
	//$mes =$_POST["mes"];//'1';
	
	$sql = "SELECT nrofacturacion ";
	$sql .= "FROM facturacion.periodofacturacion ";
	$sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND codciclo = ".$codciclo." ";
	$sql .= " AND anio = '".$anio."' ";
	$sql .= " AND mes = '".$mes."'";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array());
	$row = $consulta->fetch();
	
	$factactual = $row["nrofacturacion"];//243;
	
	//regularizar
	if($nrolistado == 0)
	{
		$Select = "SELECT codprovision ";
		$Select .= "FROM facturacion.provisiones ";
		$Select .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." ";
		$Select .= " AND tipo = ".$tipo;
		
		$Consulta = $conexion->query($Select);
		$row = $Consulta->fetch();
		
		if($row[0] != '')
		{
			$Select = "SELECT MAX(codprovision) ";
			$Select .= "FROM facturacion.provisiones ";
			$Select .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." ";
			$Select .= " AND tipo = ".$tipo;
			
			$Consulta = $conexion->query($Select);
			$row = $Consulta->fetch();
			
			if($row[0] == '')
			{
				$nrolistado = 1;
			}
			else
			{
				$nrolistado = $row[0] + 1;
			}
		}
		else
		{
			$nrolistado = 1;//$row[0];
		}
	}

	$conexion->beginTransaction();
	
	$Sql = "DELETE FROM facturacion.provisiones_detalle ";
	$Sql .= "WHERE codprovision = ".$nrolistado." AND codsuc = ".$codsuc." ";
	$Sql .= " AND tipo = ".$tipo;

	$Consulta = $conexion->query($Sql);
	
	$Sql = "DELETE FROM facturacion.provisiones ";
	$Sql .= "WHERE codprovision = ".$nrolistado." AND codsuc = ".$codsuc." ";
	$Sql .= " AND tipo = ".$tipo;
	
	$Consulta = $conexion->query($Sql);
	
	
	$Select = "INSERT INTO facturacion.provisiones";
	$Select .= " (codemp, codsuc, codprovision, tipo, nombre, dias, fechalimite, fechareg, ";
	$Select .= "  hora, codusu, estareg, estadoprovision, resolucion, anio, mes) ";
	$Select .= "VALUES(".$codemp.", ".$codsuc.", ".$nrolistado.", ".$tipo.", '".$nombrelistado."', ".$dias.", '".$fecha."', '".$fechareg."', ";
	$Select .= " '".$horareg."', '".$codusu."', '".$estareg."', '".$estadoprovision."', '".$resolucion."', '".$anio."', '".$mes."');";
	
	$result = $conexion->query($Select);
	
	
//	$Sql = "SELECT c.nrofacturacion, c.nroinscripcion, c.fechavencimiento, ";
//	$Sql .= " EXTRACT(day FROM ('".$fecha."'::timestamp - c.fechavencimiento ::timestamp)) AS dias, ";
//	$Sql .= " CAST(SUM(d.importe -(d.imppagado + d.importerebajado)) AS NUMERIC(18, 2)) AS importe ";
//	$Sql .= "FROM facturacion.cabfacturacion c ";
//	$Sql .= " INNER JOIN facturacion.detfacturacion d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) ";
//	$Sql .= "  AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.codciclo = d.codciclo) ";
//	$Sql .= "WHERE c.codsuc = ".$codsuc." AND d.estadofacturacion IN(1, 3) AND categoria IN(0, 1) ";
//	$Sql .= " AND (c.fechavencimiento + CAST('".$dias." days' AS INTERVAL)) < '".$fecha."' ";
//	$Sql .= " AND codtipodeuda NOT IN(10, 11) AND d.codconcepto NOT IN(101, 7, 8) ";
//	$Sql .= "GROUP BY c.nroinscripcion, c.fechavencimiento, c.nrofacturacion ";
//	$Sql .= "ORDER BY c.fechavencimiento";
	
	$Sql = "SELECT c.nrofacturacion, c.nroinscripcion, c.fechavencimiento, ";
	$Sql .= " EXTRACT(DAY FROM (timestamp '".$fecha."' - c.fechavencimiento)) AS dias, ";
	$Sql .= " CAST(SUM(d.importe -(d.imppagado + d.importerebajado)) AS NUMERIC(18, 2)) AS importe, ";
	$Sql .= " d.codconcepto, d.nrocredito, d.nrocuota, d.item, d.estadofacturacion, d.codtipodeuda, d.categoria, d.nrorefinanciamiento ";
	$Sql .= "FROM facturacion.cabfacturacion c ";
	$Sql .= " INNER JOIN facturacion.detfacturacion d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) ";
	$Sql .= "  AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.codciclo = d.codciclo) ";
	$Sql .= "WHERE c.codsuc = ".$codsuc." ";
	$Sql .= " AND (c.fechavencimiento + CAST('".$dias." days' AS INTERVAL)) < '".$fecha."' ";
	$Sql .= " AND d.estadofacturacion IN(1, 3) AND categoria IN(0, 1) ";
	$Sql .= " AND codtipodeuda NOT IN(10, 11) AND d.codconcepto NOT IN(101) ";
	
	//$Sql .= " AND c.nroinscripcion = 13 ";

	$Sql .= "GROUP BY c.nroinscripcion, c.fechavencimiento, c.nrofacturacion, d.codconcepto, d.nrocredito, d.nrocuota,d.item, ";
	$Sql .= " d.estadofacturacion, d.codtipodeuda, d.categoria, d.nrorefinanciamiento ";
	$Sql .= "ORDER BY c.nrofacturacion";
	
	//die($Sql);

	$Consulta = $conexion->query($Sql);
	
	foreach($Consulta->fetchAll() as $row)				
	{
		$Sql = "INSERT INTO facturacion.provisiones_detalle";
		$Sql .= " (codemp, codsuc, codprovision, tipo, codciclo, nrofacturacion, ";
		$Sql .= " nroinscripcion, codconcepto, nrocredito, nrocuota, item, importe, ";
		$Sql .= " dias, estadofacturacion, codtipodeuda, categoria, nrorefinanciamiento) ";
		$Sql .= "VALUES(".$codemp.", ".$codsuc.", ".$nrolistado.", ".$tipo.", ".$codciclo.", ".$row['nrofacturacion'].", ";
		$Sql .= " ".$row['nroinscripcion'].", ".$row['codconcepto'].", ".$row['nrocredito'].", ".$row['nrocuota'].", ".$row['item'].", ".$row['importe'].", ";
		$Sql .= " ".$row['dias'].", ".$row['estadofacturacion'].", ".$row['codtipodeuda'].", ".$row['categoria'].", ".$row['nrorefinanciamiento'].");";
		
		$Consulta = $conexion->query($Sql);
		
		if(!$Consulta)
		{
			die($Sql);
		}
		
		//$Select = "UPDATE catastro.clientes ";
		//$Select .= "SET codestadoservicio = 7 ";
		//$Select .= "WHERE codsuc = ".$codsuc." ";
		//$Select .= " AND nroinscripcion = ".$row['nroinscripcion']." ";
		
		//$Consulta = $conexion->query($Select);
	}
				
	if(!$Consulta)
	{
		$conexion->rollBack();
		$res = 0;
	}
	else
	{
		//VERIFICAR SI AY DATOS
		$Select = "SELECT * FROM facturacion.provisiones_detalle ";
		$Select .= "WHERE codprovision = ".$nrolistado." AND codsuc = ".$codsuc." ";
		$Select .= " AND tipo = ".$tipo;
		
		$Consulta = $conexion->query($Select);
		
		$nr = $Consulta->rowCount();

		//echo $nr;
		//echo $nrolistado;
		
		if($nr == 0 and $nrolistado > 1)
		{
			//---Comerntado por el Ing. Cardozo Mauro---05-03-2018---//

			/*
			$conexion->rollBack();
			$img 		= "<img src='".$_SESSION['urldir']."images/iconos/error.png' width='31' height='31' />";
			$mensaje 	= "Error al Provisionar";
			$res		= 0;
			*/

			//---Agregado por el Ing. Cardozo Mauro---03-05-2018-------//
			//---Si no encuentra usuarios para provisionar entonces solo se agregara en la tabla provision (cabecera)
			//---Y no en el detalle, tampoco se hara el update a detfacturacion------//

				$tipo = 1;
				
				$Select = "UPDATE facturacion.provisiones ";
				$Select .= "SET codusupro = ".$codusu.", estadoprovision = 2, fechapro = '".date('Y-m-d')."' ";
				$Select .= "WHERE codsuc = ".$codsuc." ";
				$Select .= " AND codprovision = ".$nrolistado." ";
				$Select .= " AND tipo = ".$tipo;
				
				$Consulta = $conexion->query($Select);

				//CONFIRMAR PROVISION
				$updP = "UPDATE facturacion.periodofacturacion ";
				$updP .= "SET saldo = 1, lecturas = 1 ";
				$updP .= "WHERE codsuc = ".$codsuc." AND codciclo = ".$codciclo." ";
				$updP .= " AND anio = '".$facturacion["anio"]."' AND mes = '".$facturacion["mes"]."'";
				
				$consultaP = $conexion->prepare($updP);
				$consultaP->execute(array());
		
				$img 		 = "<img src='".$_SESSION['urldir']."images/iconos/Ok.png' width='31' height='31' />";
				$mensaje 	 = "La Provision se ha Realizado Correctamente";
				$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
				$res		 = 1;

				$conexion->commit();
			//---Fin Agregado-----------------------------//
		}
		else
		{
			//CONFIRMAR PROVISION
			$tipo = 1;
			
			$Select = "UPDATE facturacion.provisiones ";
			$Select .= "SET codusupro = ".$codusu.", estadoprovision = 2, fechapro = '".date('Y-m-d')."' ";
			$Select .= "WHERE codsuc = ".$codsuc." ";
			$Select .= " AND codprovision = ".$nrolistado." ";
			$Select .= " AND tipo = ".$tipo;
			
			$Consulta = $conexion->query($Select);
			
			$sql = "SELECT * FROM facturacion.provisiones_detalle ";
			$sql .= "WHERE codsuc = ".$codsuc." ";
			$sql .= " AND codprovision = ".$nrolistado." ";
			$sql .= " AND tipo = ".$tipo;
			
			$consulta = $conexion->query($sql.$ord);
			$items = $consulta->fetchAll();	
			
			foreach($items as $row)
			{
				//YA NO anular los fonavis y redondeos
		        /*$Sql = "UPDATE facturacion.detfacturacion set estadofacturacion=4
		            WHERE codemp=".$codemp." AND codsuc=".$codsuc." AND codciclo=".$row['codciclo']."
		            AND nrofacturacion=".$row['nrofacturacion']." 
		            and nroinscripcion='".$row['nroinscripcion']."'
		            and estadofacturacion in (1,3) AND categoria IN (0,1)
					AND codtipodeuda NOT IN (10,11) AND codconcepto IN (101,7,8)"  ;
					$Consulta = $conexion->query($Sql);*/
				
				$Sql = "UPDATE facturacion.detfacturacion ";
				$Sql .= "SET codtipodeuda = 10 ";
				$Sql .= "WHERE codemp = ".$codemp." AND codsuc = ".$codsuc." AND codciclo = ".$row['codciclo']." ";
				$Sql .= " AND nrofacturacion = ".$row['nrofacturacion']." ";
				$Sql .= " AND nroinscripcion = '".$row['nroinscripcion']."' ";
				$Sql .= " AND codconcepto = '".$row['codconcepto']."' ";
				$Sql .= " AND nrocredito = '".$row['nrocredito']."' ";
				$Sql .= " AND nrocuota = '".$row['nrocuota']."' ";
				$Sql .= " AND item = '".$row['item']."' ";
				
		        $Consulta = $conexion->query($Sql);
			}
			


			//CONFIRMAR PROVISION
			$updP = "UPDATE facturacion.periodofacturacion ";
			$updP .= "SET saldo = 1, lecturas = 1 ";
			$updP .= "WHERE codsuc = ".$codsuc." AND codciclo = ".$codciclo." ";
			$updP .= " AND anio = '".$facturacion["anio"]."' AND mes = '".$facturacion["mes"]."'";
			
			$consultaP = $conexion->prepare($updP);
			$consultaP->execute(array());
	
			$img 		 = "<img src='".$_SESSION['urldir']."images/iconos/Ok.png' width='31' height='31' />";
			$mensaje 	 = "La Provision se ha Realizado Correctamente";
			$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
			$res		 = 1;

			$conexion->commit();
			//$res=$nrolistado;
		}
	}
	
	echo $img."|".$mensaje."|".$res;
	
?>
