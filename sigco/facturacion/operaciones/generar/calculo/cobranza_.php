<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	set_time_limit(0);
	
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	$codemp = 1;
	$codsuc 		= $_POST["codsuc"];
	$codciclo		= $_POST["codciclo"];
	$fdescarga = $objFunciones->CodFecha($_POST["fechafacturacion"]);
	$conexion->beginTransaction();
	$facturacion = $objFunciones->datosfacturacion($codsuc, $codciclo);
	
	$sql = "SELECT nrofacturacion, anio, mes, saldo, lecturas, tasainteres, tasapromintant ";
	$sql .= "FROM facturacion.periodofacturacion ";
	$sql .= " WHERE codemp = 1 AND codsuc = ? AND codciclo = 1 AND facturacion = 0";
			
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc));
	$row = $consulta->fetch();
	
	$TasaInteres = $row['tasapromintant'] / 100;
	$anio = $row['anio'];
	$mes = $row['mes'];
	$nrofacturacionactual = $row['nrofacturacion'];
    $fechavencimiento = FechaVecimiento($mes, $anio);
    $horafac = date('H:i');
	
	//VALIDAR SI EXISTE CREDITOS DE USUARIOS QUE NO ESTEN EN CLIENTES
	$Sql = "SELECT nrocredito, codantiguo, propietario, fechareg ";
	$Sql .= "FROM facturacion.cabcreditos ";
	$Sql .= "WHERE codsuc = ".$codsuc." ";
	$Sql .= " AND estareg = 1 ";
	$Sql .= " AND (nroinscripcion) NOT IN (SELECT nroinscripcion FROM catastro.clientes WHERE codsuc = ".$codsuc.") ";
	$Sql .= "ORDER BY fechareg";
	
	$Consulta = $conexion->query($Sql);
	
	$i=0;
	$tr.='<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetallefacturacion" rules="all" class="myTable">';
	$tr.='<thead class="ui-widget-header" style="font-size:12px">';
	$tr.='     <tr align="center">
	<th width="5%" >Item</th>
        <th width="15%" >Credito</th>
        <th width="15%" >Fecha</th>
        <th width="15%" >Inscripcion</th>
        <th width="40%" >Usuario</th>

     </tr>
 </thead><tbody>';
 
	foreach($Consulta->fetchAll() as $row)
	{
		$i++;
		$tr.="<tr>";
		$tr.="<td>".$i."</td>";
		$tr.="<td>".$row['nrocredito']."</td>";
		$tr.="<td>".$objFunciones->DecFecha($row['fechareg'])."</td>";
		$tr.="<td>".$row['codantiguo']."</td>";
		$tr.="<td>".$row['propietario']."</td>";
		$tr.="</tr>";
	}
	$tr.="</tbody></table>";
	if($i>0)
	{
		$img		= "<img src='".$_SESSION['urldir']."images/iconos/error.png' width='31' height='31' />";
		$mensaje 	= "No se de Realizó el Cierre de Cobranza";
		$mensaje	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$data['img'] = '';
		$data['mensaje'] = $mensaje;
		$data['tr'] = $tr;
		$data['res'] = 0;
		//die(json_encode($data));
	}
		
	//VALIDAR SI EXISTE CREDITOS DE USUARIOS QUE NO ESTEN EN CLIENTES
	//FILTAR PAGOS ADELANTADOS DEL MES
	$Sql = "SELECT d.codemp, d.codsuc, d.nrofacturacion, d.nroinscripcion, d.nropago, d.codconcepto, d.importe, c.creador, c.fechareg ";
	$Sql .= "FROM  cobranza.cabpagos c ";
	$Sql .= " INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp) ";
	$Sql .= "  AND (c.codsuc = d.codsuc)  AND (c.nroinscripcion = d.nroinscripcion) AND (c.nropago = d.nropago) ";
	$Sql .= "WHERE c.codsuc = ".$codsuc." ";
	$Sql .= " AND d.codconcepto = 10005 ";
	$Sql .= " AND d.anio = '".$facturacion["anio"]."' ";
	$Sql .= " AND d.mes = '".$facturacion["mes"]."' ";
	$Sql .= " AND c.anulado = 0 ";
	
	$Consulta = $conexion->query($Sql);
	
	$i = 0;
	
	foreach($Consulta->fetchAll() as $row)
	{
		$sqlD = "INSERT INTO cobranza.pagosvarios 
				(
				  codemp,
				  codsuc,
				  nrofacturacion,
				  nroinscripcion,
				  nropago,
				  codconcepto,
				  creador,
				  fechareg,
				  importe
				) 
				VALUES (
				  :codemp,
				  :codsuc,
				  :nrofacturacion,
				  :nroinscripcion,
				  :nropago,
				  :codconcepto,
				  :creador,
				  :fechareg,
				  :importe
				);";
					            
	            $resultD = $conexion->prepare($sqlD);
	            $resultD->execute(array(":codemp"=>$row['codemp'],
	                                    ":codsuc"=>$row['codsuc'],
	                                    ":nrofacturacion"=>$facturacion["nrofacturacion"],
	                                    ":nroinscripcion"=>$row['nroinscripcion'],
	                                    ":nropago"=>$row['nropago'],
	                                    ":codconcepto"=>$row['codconcepto'],
	                                    ":creador"=>$row['creador'],
	                                    ":fechareg"=>$row['fechareg'],
	                                    ":importe"=>$row['importe']
	                                    ));
	            if ($resultD->errorCode() != '00000') 
	            {
	                $conexion->rollBack();
	                $mensaje = "Error reclamos";
	                $data['res']    =2;
	                die(json_encode($data)) ;
	            }
	}
	//FILTAR PAGOS ADELANTADOS DEL MES
	// //REVISAR PAGOS VENCIDOS
	// $Sql="SELECT c.nroinscripcion,ci.propietario,ci.nrodocumento,ci.codciclo,ci.codantiguo,
	// 			 cl.descripcion as calle,ci.nrocalle,
	// 			MAX(d.nrofacturacion) as nrofacturacion,f.fechavencimiento
	// 		FROM  cobranza.cabpagos c
	// 		  INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
	// 		  AND (c.codsuc = d.codsuc)
	// 		  AND (c.nroinscripcion = d.nroinscripcion)
	// 		  AND (c.nropago = d.nropago)
	// 		  INNER JOIN facturacion.cabfacturacion f ON (f.codemp = d.codemp)  AND (f.codsuc = d.codsuc)  AND (f.nrofacturacion = d.nrofacturacion)  AND (f.nroinscripcion = d.nroinscripcion)
	// 		  --INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp)  AND (f.codsuc = df.codsuc)  AND (f.nrofacturacion = df.nrofacturacion)  AND (f.nroinscripcion = df.nroinscripcion)
	// 		  INNER JOIN catastro.clientes ci ON (ci.codemp = c.codemp) AND (ci.codsuc = c.codsuc)
	// 		   AND (ci.nroinscripcion = c.nroinscripcion)
	// 		   INNER JOIN public.calles as cl on(ci.codemp=cl.codemp and ci.codsuc=cl.codsuc and ci.codcalle=cl.codcalle and ci.codzona=cl.codzona)
	// 			inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
	// 		  WHERE c.codsuc=".$codsuc." and c.anulado=0 AND d.nrofacturacion<>0 
	// 		  and c.fechareg>f.fechavencimiento AND  c.fechareg between   '2015-01-01' and '2015-01-31'
	// 		  group by c.nroinscripcion,ci.propietario,ci.nrodocumento,ci.codciclo,ci.codantiguo,
	// 		  cl.descripcion,ci.nrocalle,f.fechavencimiento ";
	// $Consulta = $conexion->query($Sql);
	// $i=0;
	// $codconcepto=10013;//CONCEPTO INTERES
	// $Sql="select max(nrocredito)  from facturacion.cabvarios where codsuc=".$codsuc;
	// $rowc= $conexion->query($Sql)->fetch();
	// $nrocredito = $rowc[0];
	// foreach($Consulta->fetchAll() as $row)
	// {
	// 	$nroinscripcion=$row['nroinscripcion'];
	// 	$fechavencimientorecibo=$row['fechavencimiento'];
	// 	$Sql="SELECT SUM(d.importe)
	// 			FROM  cobranza.cabpagos c
	// 		  INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
	// 		  AND (c.codsuc = d.codsuc)
	// 		  AND (c.nroinscripcion = d.nroinscripcion)
	// 		  AND (c.nropago = d.nropago)
	// 		  WHERE c.codsuc=".$codsuc." and c.anulado=0 AND d.nrofacturacion<>0 
	// 		  and c.fechareg>'".$fechavencimientorecibo."' AND c.nroinscripcion=".$nroinscripcion."
	// 		  AND d.nrofacturacion=".$row['nrofacturacion'];
	// 	$row2 = $conexion->query($Sql)->fetch();
		
	// 	$dias = dias_transcurridos($fechavencimientorecibo,$fdescarga);
	// 	$importe=$row2[0];
	// 	$importe =round($importe*$dias*($TasaInteres/30),1);
	// 	if($importe>0)
	// 	{
	// 		$propietario = strtoupper($row["propietario"]);
	// 		$direccion = strtoupper($row["descripcioncorta"] . " " . $row["calle"] . " " . $row["nrocalle"]);
	// 		$codciclo    = $row['codciclo'];
	// 		$codantiguo  = $row["codantiguo"];
 // 	     	$nrocredito++;
			
	// 		$sqlC 	= "insert into facturacion.cabvarios(codemp,codsuc,nrocredito,nroinscripcion,imptotal,igv,redondeo,subtotal,codconcepto,
	// 						  nropresupuesto,observacion,creador,fechareg,nroprepago,codantiguo,
	// 						  propietario,direccion,hora) values(:codemp,:codsuc,:nrocredito,:nroinscripcion,:imptotal,:igv,:redondeo,
	// 						  :subtotal,:codconcepto,:nropresupuesto,:observacion,:creador,:fechareg,:nroprepago,:codantiguo,:propietario,:direccion,:hora)";
			
	// 		$resultC = $conexion->prepare($sqlC);
	// 		$resultC->execute(array(":codemp"=>$codemp,
	// 								":codsuc"=>$codsuc,
	// 								":nrocredito"=>$nrocredito,
	// 								":nroinscripcion"=>$nroinscripcion,
	// 								":imptotal"=>str_replace(",","",$importe),
	// 								":igv"=>0,
	// 								":redondeo"=>0,
	// 								":subtotal"=>str_replace(",","",$importe),
	// 								":codconcepto"=>$codconcepto,
	// 								":nropresupuesto"=>0,
	// 								":observacion"=>'INTERES POR '.$dias.'DIAS (S).',
	// 								":creador"=>1,
	// 								":fechareg"=>$fdescarga,
	// 								":nroprepago"=>0,
	// 								":codantiguo"=>$codantiguo,
	// 								":propietario"=>$propietario,
	// 								":direccion"=>$direccion,
	// 								":hora"=>$horafac));
	// 		if ($resultC->errorCode() != '00000') 
	// 	    {
	// 	        $conexion->rollBack();
	// 	        $mensaje = "Error reclamos";
	// 	        $data['res']    =2;
	// 	        die(json_encode($data));
	// 	    }
			
	// 		$sqlD = "insert into facturacion.detvarios(codemp,codsuc,nrocredito,nrocuota,totalcuotas,fechavencimiento,subtotal,igv,
	// 				redondeo,imptotal,nrofacturacion,tipocuota,anio,mes,interes,estadocuota) values(:codemp,:codsuc,:nrocredito,:nrocuota,:totalcuotas,:fechavencimiento,:subtotal,
	// 				:igv,:redondeo,:imptotal,:nrofacturacion,:tipocuota,:anio,:mes,:interes,:estadocuota)";
			
	// 		$resultD = $conexion->prepare($sqlD);
	// 		$resultD->execute(array(":codemp"=>$codemp,
	// 								":codsuc"=>$codsuc,
	// 								":nrocredito"=>$nrocredito,
	// 								":nrocuota"=>1,
	// 								":totalcuotas"=>1,
	// 								":fechavencimiento"=>$objFunciones->CodFecha($fechavencimiento),
	// 								":subtotal"=>str_replace(",","",$importe),
	// 								":igv"=>0,
	// 								":redondeo"=>0,
	// 								":interes"=>0,
	// 								":imptotal"=>str_replace(",","",$importe),
	// 								":nrofacturacion"=>$nrofacturacionactual,
	// 								":tipocuota"=>0,
	// 								":estadocuota"=>0,
	// 								":anio"=>$anio,
	// 								":mes"=>$mes));

	// 		if ($resultD->errorCode() != '00000') {
	// 	        $conexion->rollBack();
	// 	        $mensaje = "Error al Grabar Registro";
	// 	        echo $res = 2;
	// 	    } 
	// 	      $sqlD = "INSERT INTO facturacion.detcabvarios
	// 	            (  codemp,  codsuc,  nrocredito,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal) 
	// 	            VALUES (  :codemp,  :codsuc,  :nrocredito,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, :imptotal);";
		            
	// 	            $resultD = $conexion->prepare($sqlD);
	// 	            $resultD->execute(array(":codemp"=>$codemp,
	// 	                                    ":codsuc"=>$codsuc,
	// 	                                    ":nrocredito"=>$nrocredito,
	// 	                                    ":codconcepto"=>$codconcepto,
	// 	                                    ":item"=>1,
	// 	                                    ":subtotal"=>str_replace(",","",$importe),
	// 	                                    ":igv"=>0,
	// 	                                    ":redondeo"=>0,
	// 	                                    ":imptotal"=>$importe,
	// 	                                    ));
	// 	            if ($resultD->errorCode() != '00000') 
	// 	            {
	// 	                $conexion->rollBack();
	// 	                $mensaje = "Error reclamos";
	// 	                $data['res']    =2;
	// 	                die(json_encode($data)) ;
	// 	            }

		   
	// 	}
	// }
	
	// //REVISAR PAGOS VENCIDOS
	$upd = "UPDATE facturacion.detfacturacion ";
	$upd .= "SET categoria = 1 ";
	$upd .= "WHERE estadofacturacion IN(1, 3) ";
	$upd .= " AND categoria <> 2 ";
	$upd .= " AND categoria = 0 ";
	$upd .= " AND codsuc = ? ";
	$upd .= " AND codciclo = ?";
	
	$consulta 	= $conexion->prepare($upd);
	$consulta->execute(array($codsuc,$codciclo));	
	
	/*var_dump($result->errorInfo());
		die();*/
	$updP = "UPDATE facturacion.periodofacturacion ";
	$updP .= "SET saldo = 1 ";
	$updP .= "WHERE codsuc = ? ";
	$updP .= " AND codciclo = ? ";
	$updP .= " AND anio = ? ";
	$updP .= " AND mes = ?";
	
	$consultaP 	= $conexion->prepare($updP);
	$consultaP->execute(array($codsuc, $codciclo, $facturacion["anio"], $facturacion["mes"]));
	
	//ANULAR PREPAGOS SIN CANCELAR
	$updP = "UPDATE cobranza.cabprepagos ";
	$updP .= "SET estado = 0 ";
	$updP .= "WHERE codsuc = ? ";
	$updP .= " AND estado = 1;";
	
	$consultaP 	= $conexion->prepare($updP);
	$consultaP->execute(array($codsuc));
	
	//ACTUALIZACION DE LECTURAS
	// $facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
	
	// $upd 		= "update facturacion.periodofacturacion set lecturas=1 where codsuc=? and codciclo=? and anio=? and mes=?";
	// $consulta 	= $conexion->prepare($upd);
	// $consulta->execute(array($codsuc,$codciclo,$facturacion["anio"],$facturacion["mes"]));
	//ACTUALIZACION DE LECTURAS
	if(!$consulta || !$consultaP)
	{
		$conexion->rollBack();
		
		$img 		= "<img src='".$_SESSION['urldir']."images/iconos/error.png' width='31' height='31' />";
		$mensaje 	= "Error al Tratar de Realizar el Cierre de Cobranza";
		$res		= 0;
	}else{
		$conexion->commit();
		
		$img 		 = "<img src='".$_SESSION['urldir']."images/iconos/Ok.png' width='31' height='31' />";
		$mensaje 	 = "El Cierre de Cobranza se ha Realizado Correctamente";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
	}
	$data['img']     = $img;
	$data['mensaje'] = $mensaje;
	$data['res']     = $res;
	echo json_encode($data);
	//echo $img."|".$mensaje."|".$res;
	
	function dias_transcurridos($fecha_i,$fecha_f)
	{
		$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
		$dias 	= abs($dias); $dias = floor($dias);		
		return $dias;
	}
	function FechaVecimiento($mes, $anio) {
        $dia = UltimoDia($mes);

        if (strlen($mes) == 1) {
            $mes = "0" . $mes;
        }
        return $dia . "/" . $mes . "/" . $anio;
    }

    function UltimoDia($Mes) {
        $NroDias = 0;

        switch ($Mes) {
            case 1:$NroDias = 31;
                break;
            case 2:$NroDias = 28;
                break;
            case 3:$NroDias = 31;
                break;
            case 4:$NroDias = 30;
                break;
            case 5:$NroDias = 31;
                break;
            case 6:$NroDias = 30;
                break;
            case 7:$NroDias = 31;
                break;
            case 8:$NroDias = 31;
                break;
            case 9:$NroDias = 30;
                break;
            case 10:$NroDias = 31;
                break;
            case 11:$NroDias = 30;
                break;
            case 12:$NroDias = 31;
                break;
        }

        return $NroDias;
    }
?>
