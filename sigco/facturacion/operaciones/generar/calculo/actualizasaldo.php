<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	set_time_limit(0);
	
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$conexion->beginTransaction();
	
	$codsuc 	= $_POST["codsuc"];
	$codciclo 	= $_POST["codciclo"];
	
	$facturacion = $objFunciones->datosfacturacion($codsuc, $codciclo);
	
	$Sql = "SELECT count(*) FROM facturacion.cabfacturacion ";
	$Sql .= "WHERE codsuc = ".$codsuc." ";
	$Sql .= " AND codciclo = ".$codciclo." ";
	$Sql .= " AND nrofacturacion = ".intval($facturacion["nrofacturacion"] - 1);
	
	$control_facturacion = $conexion->prepare($Sql);
	$control_facturacion->execute(array());
	
	$items_facturacion = $control_facturacion->fetch();
	
	$Sql = "SELECT anio || '' || mes FROM facturacion.periodofacturacion ";
	$Sql .= "WHERE nrofacturacion = ".intval(intval($facturacion["nrofacturacion"]) - 1)." ";
	$Sql .= " AND codsuc = ".$codsuc;
	
	$Consula = $conexion->query($Sql);
	$row = $Consula->fetch();
	
	$Periodo = $row[0];
	
	if($items_facturacion[0] <= 0 and $facturacion["nrofacturacion"] > 1)
	{
		$img 		 = "<img src='".$_SESSION['urldir']."images/iconos/error.png' width='31' height='31' />";
		$mensaje 	 = "Aun no se ha Realizado la Facturacion";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
		
		echo $img."|".$mensaje."|".$res;
		
		die();
	}
	
	$consulta_validar = $conexion->prepare("SELECT COUNT(*) FROM facturacion.cabctacorriente WHERE anio=? and mes=? and codsuc=? and codciclo=?");
	$consulta_validar->execute(array($facturacion["anio"],$facturacion["mes"],$codsuc,$codciclo));
	$items_validar = $consulta_validar->fetch();
	
	if($items_validar[0] <= 0)
	{
		$SqlS = "SELECT * FROM facturacion.f_ctacorriente_saldo(".$codsuc.", ".$codciclo.", '".$facturacion["anio"]."', '".$facturacion["mes"]."', ".($facturacion["nrofacturacion"] - 1).", '".$Periodo."')";
		
		$consulta_saldo = $conexion->prepare($SqlS);
		$consulta_saldo->execute(array());
									   
		$items_saldo = $consulta_saldo->fetch();
		
		$SqlF = "SELECT * FROM facturacion.f_ctacorriente_facturacion";
		$SqlF .= " (".$codsuc.", ".$codciclo.", '".$facturacion["anio"]."', '".$facturacion["mes"]."', ".intval($facturacion["nrofacturacion"] - 1).", '".$Periodo."')";
		
		//echo $SqlF;
		
		$consulta_facturacion = $conexion->prepare($SqlF);
		$consulta_facturacion->execute(array());
											 
		$items_facturacion = $consulta_facturacion->fetch();

		$consulta_reclamos = $conexion->prepare("SELECT * FROM facturacion.f_historico_reclamos(".$codsuc.", '".$facturacion["anio"]."', '".$facturacion["mes"]."', '".$Periodo."')");
		$consulta_reclamos->execute(array());
											 
		$items_reclamos = $consulta_reclamos->fetch();
		
		if(!$items_saldo || !$items_facturacion || !$items_reclamos)
		{
			$conexion->rollBack();
			
			$img 		= "<img src='".$_SESSION['urldir']."images/iconos/error.png' width='31' height='31' />";
			$mensaje 	= "Error al Tratar de Actualizar los Saldo";
			$res		= 0;
		}
		else
		{
			$conexion->commit();
			
			$img 		 = "<img src='".$_SESSION['urldir']."images/iconos/Ok.png' width='31' height='31' />";
			$mensaje 	 = "Los Hist&oacute;ricos se han Actualizado Correctamente";
			$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
			$res		 = 1;
		}
	}
	else
	{
		$img 		 = "<img src='".$_SESSION['urldir']."images/iconos/error.png' width='31' height='31' />";
		$mensaje 	 = "Los Saldos Ya Fueron Actualizados no es Posible volver a generalos";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
	}
	
	echo $img."|".$mensaje."|".$res;
?>