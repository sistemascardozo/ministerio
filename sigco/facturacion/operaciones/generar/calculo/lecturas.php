<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	
	$codsuc 	= $_POST["codsuc"];
	$codciclo 	= $_POST["codciclo"];
	
	$conexion->beginTransaction();
	
	$objFunciones = new clsFunciones();
	
	$facturacion = $objFunciones->datosfacturacion($codsuc,$codciclo);
	
	$upd 		= "UPDATE facturacion.periodofacturacion SET lecturas = 1 WHERE codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
	$consulta 	= $conexion->prepare($upd);
	$consulta->execute(array($codsuc, $codciclo, $facturacion["anio"], $facturacion["mes"]));
	
	if(!$consulta)
	{
		$conexion->rollBack();
		
		$img 		= "<img src='".$urldir."images/iconos/error.png' width='31' height='31' />";
		$mensaje 	= "Error al Tratar de Realizar el Cierre de Lecturas";
		$res		= 0;
	}
	else
	{
		$conexion->commit();
		
		$img 		 = "<img src='".$urldir."images/iconos/Ok.png' width='31' height='31' />";
		$mensaje 	 = "El Cierre de Lecturas se ha Realizado Correctamente";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
	}
	
	echo $img."|".$mensaje."|".$res;
?>