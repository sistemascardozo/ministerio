<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../objetos/clsFunciones.php");

$objFunciones = new clsFunciones();

set_time_limit(0);

file_put_contents('../progreso.txt', '0|Calculando..');


$codsuc         = $_POST["codsuc"];
$codciclo       = $_POST["codciclo"];
$porcentaje     = $_POST["porcentaje"];
$idusuario      = $_SESSION['id_user'];
$nrofacturacion = $_POST["nrofacturacion"];
$calcalc        = $_POST["factura_alcantarillado"];
$anio           = $_POST["anio"];
$mes            = $_POST["mes"];
$codemp         = 1;

$SqlF = "SELECT nrotarifa ";
$SqlF .= "FROM facturacion.periodofacturacion ";
$SqlF .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND facturacion = 0 AND codciclo = ".$codciclo.";";

$consulta = $conexion->prepare($SqlF);
$consulta->execute(array());

$nrotarifa = $consulta->fetch();

$nrotarifa = $nrotarifa[0];

$fechafacturacion = $objFunciones->CodFecha($_POST['fechafacturacion']);

$EstMedProm = array();
$EstLecProm = array();

$Sql = "SELECT codestlectura FROM public.estadolectura WHERE estareg = 1 AND promediar = 1";

foreach ($conexion->query($Sql)->fetchAll() as $row) {
    $EstLecProm[$row[0]] = $row[0];
}

$Sql = "SELECT codestadomedidor FROM public.estadomedidor WHERE promediar = 1"; //estareg = 1 AND

foreach ($conexion->query($Sql)->fetchAll() as $row) {
    $EstMedProm[$row[0]] = $row[0];
}

//CONCEPTOS VMA
$conceptovma = 10007;


//ACTUALIZAR PARA EVITAR UN ERROR DE DIGITACION//
$Sql = "UPDATE catastro.clientes SET consumo = 0 WHERE consumo = 'NaN'";
$conexion->query($Sql);

$Sql = "UPDATE catastro.clientes SET lecturaestimada = 0 WHERE lecturaestimada = 'NaN'";
$conexion->query($Sql);

$Sql = "UPDATE catastro.clientes SET lecturaanterior = 0 WHERE lecturaanterior IS null";
$conexion->query($Sql);

$Sql = "UPDATE medicion.lecturas SET consumo = 0 WHERE consumo = 'NaN'";
$conexion->query($Sql);


$conexion->beginTransaction();

$Sql = "UPDATE catastro.clientes SET digitado = 0";
$conexion->query($Sql);


/* Recupera los Datos de los documentos para la facturacion */
$sqldoc = "SELECT coddocumento, serie, correlativo ";
$sqldoc .= "FROM reglasnegocio.correlativos ";
$sqldoc .= "WHERE coddocumento = 4 AND codemp = 1 AND codsuc = ".$codsuc;

$consDoc = $conexion->prepare($sqldoc);
$consDoc->execute(array());
$itemsDoc = $consDoc->fetch();

$documento = $itemsDoc["coddocumento"];
$seriedoc = $itemsDoc["serie"];
$correlativo = $itemsDoc["correlativo"];

/* Obtiene el Porcentaje de Interes del Mes para la Deuda */
$porcentaje = $porcentaje / 100;

$porcalc = $objFunciones->getParamae("PORALC", $codsuc);
$Pigv = $objFunciones->getParamae("IMPIGV", $codsuc);
$Pigv = $Pigv["valor"];


/* Inicio del Proceso de Calculo de Facturacion */
$sqlC = "SELECT * FROM facturacion.view_usuariofacturar ";
$sqlC .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND codciclo = ".$codciclo." ";
$sqlC .= " AND codtipousuario = 1 "; //Usuario Activo
$sqlC .= " AND estareg = 1 ";
// $sqlC .= " AND nroinscripcion IN(1374, 1128) ";
//$sqlC .= " AND codestadoservicio <> 7 ";	//No existe Estado de Servicio 7
//$sqlC .= " AND nroinscripcion < 300 "; //111471, 109002, 13887
//$sqlC .= " AND codsector = 2 ";
$sqlC .= "ORDER by codsuc, codzona, codsector, codrutlecturas, orden_dist ";

// die($sqlC);

$consultaC = $conexion->prepare($sqlC);
$consultaC->execute(array());
$itemsC = $consultaC->fetchAll();

$totalAvance = count($itemsC);
//$TotalRegistros = $totalAvance;

$actualAvance = 0;
$horafac = date('H:i');
$tipofacturacionfact = 0;

$DigRed = 2;

foreach ($itemsC as $rowC) {
    $codsector = $rowC['codsector'];
    $codmanzanas = $rowC['codmanzanas'];
    $lote = $rowC['lote'];
    $sublote = $rowC['sublote'];
    $codrutlecturas = $rowC['codrutlecturas'];
    $codrutdistribucion = $rowC['codrutdistribucion'];
    $codtipoabastecimiento = $rowC['codtipoabastecimiento'];

    $ItemDetalle = 0;
    $conscategoria = 0;
    $impagua = 0;
    $impdesague = 0;
    $impigv = 0;

    $tipofacturacionfact = $rowC['tipofacturacion'];

    //Obtenemos el Nro de Unidades de Uso y lo multiplicamos al consumo
    //$NroUU = 1;
//
//		if ($rowC['tipofacturacion'] == 2)
//		{
//			$NroUU = $rowC["domestico"] + $rowC["social"] + $rowC["comercial"] + $rowC["estatal"] + $rowC["industrial"];
//		}

    $CosumoUU = $rowC["consumo"];

    //Se Obtiene el Consumo Facturado
    //echo $rowC['lecturapromedio'];
    $ConsumoFact = $objFunciones->ConsumoFac($rowC["lecturapromedio"], $rowC['tipofacturacion'], $CosumoUU, intval($rowC['codestlectura']), $rowC['codestadomedidor']);

//echo "Consumo-1 : ".$ConsumoFact."<br>";

    $Flag = true;  //CALCULAR IMPORTE DE ACTIVOS Y AGUA DE CORTADOS CON DEUDA DE 2 MESE
    $Imprimir = 1;  //SI SE VA IMPRIMIR EL RECIBO SI(1)

    $consumo_facturado = $CosumoUU;

    if ($rowC['codtipoentidades'] != 85) { //Entidad 85 = NO FACTURAR
        switch ($rowC["codestadoservicio"]) {
            case 1:  //ACTIVOS
                $correlativotem = $correlativo;
                $correlativo++;

                break;

            case 2:  //CORTADOS

                if ($rowC['tipofacturacion'] != 2):

                    //CALCULAR MESES DE DEUDA
                    $Sql = "SELECT * FROM facturacion.deuda_facturacion(".$codsuc.", ".$rowC["nroinscripcion"].")";
                    $rowDeuda = $conexion->query($Sql)->fetch();

                    $MesesDeuda = $rowDeuda['mesesdeuda'];

                    if ($MesesDeuda > 2) {  //DEUDAS MAYORES A 2 MESES
                        $Flag = false; //No se Factura
                        $Imprimir = 0;  //No se Imprime
                        $correlativotem = 0;  //No lleva Correlativo
                    } else {
                        //Si Meses de Deuda son 2 y son Consecutivos y
                        //Periodo de Deuda es Igual al Periodo Anterior
                        if ($MesesDeuda == 2 && $rowDeuda['consecutivo'] == 1 && $rowDeuda['maxfacdeuda'] == ($nrofacturacion - 1)) {
                            $correlativotem = $correlativo;
                            $correlativo++;
                        } else {
                            if ($MesesDeuda == 0) { //Si no posee Deuda
                                //if ($ConsumoFact > 0) //Si Tiene Consumo
                                //								{
                                //									$correlativotem = $correlativo;
                                //									$correlativo++;
                                //								}
                                //								else
                                //								{
                                $Flag = false; //No se Factura
                                $Imprimir = 0;  //No se Imprime
                                $correlativotem = 0;  //No lleva Correlativo
                                //								}
                            } else {
                                //Si Debe 1 Mes
                                //Periodo de Deuda es Igual al Periodo Anterior
                                if ($MesesDeuda == 1 && $rowDeuda['maxfacdeuda'] == ($nrofacturacion - 1)) {
                                    $correlativotem = $correlativo;
                                    $correlativo++;
                                } else {
                                    $Flag = false; //No se Factura
                                    $Imprimir = 0;  //No se Imprime
                                    $correlativotem = 0;  //No lleva Correlativo
                                }
                            }

                            //Cortado Con Cargo
                            //$sqlcreditos = "SELECT COUNT(det.nrocuota) ";
                            //$sqlcreditos .= "FROM facturacion.detvarios det ";
                            //$sqlcreditos .= " INNER JOIN facturacion.cabvarios cab ON(det.codemp = cab.codemp) AND (det.codsuc = cab.codsuc) ";
                            //$sqlcreditos .= " AND (det.nrocredito = cab.nrocredito) ";
                            //$sqlcreditos .= "WHERE det.codemp = 1 AND det.codsuc = ".$codsuc." ";
                            //$sqlcreditos .= " AND det.estadocuota = 0 ";	//0: Pendiente
                            //$sqlcreditos .= " AND cab.estareg = 1 ";		//1: Ingresado
                            //$sqlcreditos .= " AND cab.nroinscripcion = ".$rowC["nroinscripcion"]." ";
                            //$conCargo = $conexion->prepare($sqlcreditos);
                            //$conCargo->execute(array());
                            //$conCargoC = $conCargo->fetch();
                            //if ($conCargoC[0] > 0)
                            //{
                            //	$Flag 			= true;	//No se Factura
                            //	$Imprimir 		= 1;		//No se Imprime
                            //	if ($correlativotem == 0)
                            //	{
                            //		$correlativotem = $correlativo;
                            //		$correlativo++;
                            //	}
                            //}
                        }
                    }

                else:
                    $Flag = false; //No se Factura
                    $Imprimir = 0;  //No se Imprime
                    $correlativotem = 0;  //No lleva Correlativo
                endif;

                break;

            case 3: case 4: case 5: case 0: case 6: case 7: case 8: //3:CLAUSURADO 4:LEVANTADA 5:NOTIFICADA 6:EN PROCESO
                $Flag = false; //No se Factura
                $Imprimir = 0;  //No se Imprime
                $correlativotem = 0;  //No lleva Correlativo

                break;

            //case 7:
            //$Sql = "SELECT * FROM facturacion.deuda_facturacion(".$codsuc.", ".$rowC["nroinscripcion"].")";
            //$rowDeuda = $conexion->query($Sql)->fetch();
            //$MesesDeuda = $rowDeuda['mesesdeuda'];
            //if($rowDeuda['maxfacdeuda'] == ($nrofacturacion - 1))
            //{
            //if ($ConsumoFact > 0) //Si Tiene Consumo
            //{
            //$correlativotem = $correlativo;
            //$correlativo++;
            //}
            //else
            //{
            //	$Flag 			= false;	//No se Factura
            //	$Imprimir 		= 0;		//No se Imprime
            //	$correlativotem = 0;		//No lleva Correlativo
            //}
            //}
            //break;

            default:
                $Flag = false; //No se Factura
                $Imprimir = 0;  //No se Imprime
                $correlativotem = 0;  //No lleva Correlativo
        }
    }
    else {
        $Flag = false; //No se Factura
        $Imprimir = 0;  //No se Imprime
        $correlativotem = 0;  //No lleva Correlativo
    }

    //SI SE FACTURA SE OBTIENE EL CONSUMO FACTURADO
    if ($Flag) {
        $Sql = "SELECT * FROM facturacion.f_getconsfacturado(:codsuc, :catetar, :consumo, :tipofacturacion)";
        $consFact = $conexion->prepare($Sql);
        $consFact->execute(array(":codsuc" => $codsuc,
            ":catetar" => $rowC["cairo_pattern_get_radial_circles(pattern)"],
            ":consumo" => intval($ConsumoFact),
            ":tipofacturacion" => $tipofacturacionfact));

        $consumo_facturado = $consFact->fetch();
        $consumo_facturado = $consumo_facturado[0];

        if ($consumo_facturado == '') {
            $consumo_facturado = 0;
        }
    }

    $SqlVF = "SELECT COUNT(*) FROM facturacion.cabfacturacion ";
    $SqlVF .= "WHERE codemp = 1 ";
    $SqlVF .= " AND codsuc = ".$codsuc." ";
    $SqlVF .= " AND codciclo = ".$codciclo." ";
    $SqlVF .= " AND nrofacturacion = ".$nrofacturacion." ";
    $SqlVF .= " AND nroinscripcion = ".$rowC["nroinscripcion"]." ";

    $rowVF = $conexion->query($SqlVF)->fetch();

    $ExisteF = $rowVF[0];

    if ($ExisteF == 0) {
        $TotalParaIgv = 0;  //Acumulador para el IGV
        //Insertamos la Cabecera de La Facturación
        $SqlCF = "INSERT INTO facturacion.cabfacturacion";
        $SqlCF .= " (codemp, codsuc, codzona, codciclo, nrofacturacion, nroinscripcion, propietario, ";
        $SqlCF .= " coddocumento, serie, nrodocumento, ";
        $SqlCF .= " catetar, codestlectura, lecturaultima, fechalectultima, ";
        $SqlCF .= " lecturaanterior, fechalectanterior, lecturapromedio, lecturaestimada, ";
        $SqlCF .= " consumo, consumofact, codtiposervicio, tipofacturacion, codestadoservicio, ";
        $SqlCF .= " anio, mes, creador, fechareg, hora, ";
        $SqlCF .= " nromed, codestadomedidor, fecharevmedidor, ";
        $SqlCF .= " horasabastecimiento, observacionabast, tipofacturacionfact, ";
        $SqlCF .= " exoneraalc, imprimir, codtipousuario, ";
        $SqlCF .= " codurbanizacion, codsector, codmanzanas, lote, sublote, codrutlecturas, ";
        $SqlCF .= " codrutdistribucion, codtipoabastecimiento  ) ";
        $SqlCF .= "VALUES(1, ".$codsuc.", ".$rowC["codzona"].", ".$codciclo.", ".$nrofacturacion.", ".$rowC["nroinscripcion"].", '".$rowC["propietario"]."', ";
        $SqlCF .= " ".$documento.", '".$seriedoc."', ".$correlativotem.", ";
        $SqlCF .= " ".$rowC["catetar"].", ".$rowC['codestlectura'].", ".$rowC["lecturaultima"].", '".$rowC["fechalecturaultima"]."', ";
        $SqlCF .= " ".$rowC["lecturaanterior"].", '".$rowC["fechalecturaanterior"]."', ".$rowC["lecturapromedio"].", ".$rowC["lecturaestimada"].",";
        $SqlCF .= " ".$rowC["consumo"].", ".$consumo_facturado.", ".$rowC["codtiposervicio"].", ".$rowC["tipofacturacion"].", ".$rowC["codestadoservicio"].", ";
        $SqlCF .= " '".$anio."', '".$mes."', ".$idusuario.", '".$fechafacturacion."', '".$horafac."', ";
        $SqlCF .= " '".$rowC['nromed']."', ".$rowC['codestadomedidor'].", '".$rowC["fecharevmedidor"]."', ";
        $SqlCF .= " '".$rowC['horasabastecimiento']."', '".$rowC['observacionabast']."', ".$tipofacturacionfact.", ";
        $SqlCF .= " ".$rowC["exoneralac"].", ".$Imprimir.", ".$rowC['codtipousuario'].", ".$rowC['codurbanizacion'].", ";
        $SqlCF .= " ".$codsector.", ".$codmanzanas.", ".$lote.", ".$sublote.", ";
        $SqlCF .= " ".$codrutlecturas.", ".$codrutdistribucion.", ".$codtipoabastecimiento." )";

        $consCab = $conexion->prepare($SqlCF);
        $consCab->execute();

        if ($consCab->errorCode() != '00000') {
            $conexion->rollBack();

            $mensaje = "306: Error INSERT CabFacturacion : ".$rowC["nroinscripcion"]."<br>".$SqlCF;

            die($mensaje);
        }


        //Obtenemos las Unidades de Uso
        $Sql = "SELECT catetar, porcentaje, principal ";
        $Sql .= "FROM catastro.unidadesusoclientes ";
        $Sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
        $Sql .= " AND nroinscripcion = ".$rowC["nroinscripcion"];

        $itemsU = $conexion->query($Sql)->fetchAll();

        $num_total_unidades = count($itemsU);

        if ($num_total_unidades == 1) { //Si posee 1 Unidad de Uso
            $itemsU = $itemsU[0];

            //Obtenemos el Importe a Facturar según la Tarifa
            $items = $objFunciones->ImporteTarifa($codsuc, $itemsU["catetar"], $tipofacturacionfact, $ConsumoFact, $nrofacturacion);

            $impagua = empty($items[0]) ? 0 : $items[0];
            $impdes = empty($items[1]) ? 0 : $items[1];

            $impdesague = $objFunciones->redondeado($impdes, $DigRed);

            $insU = "INSERT INTO facturacion.unidadesusofacturadas ";
            $insU .= " (codemp, codsuc, nrofacturacion, codciclo, nroinscripcion, ";
            $insU .= " item, codunidaduso, catetar, porcentaje, principal, ";
            $insU .= " consumo, importe, importealc) ";
            $insU .= "VALUES(1, ".$codsuc.", ".$nrofacturacion.", ".$codciclo.", ".$rowC["nroinscripcion"].", ";
            $insU .= " 1, 1, ".$itemsU["catetar"].", 100, 1, ";
            $insU .= " ".$ConsumoFact.", '".str_replace(",", "", round($impagua, 1))."', '".str_replace(",", "", round($impdesague, 1))."')";

            $consU = $conexion->prepare($insU);
            $consU->execute();

            if ($consU->errorCode() != '00000') {
                $conexion->rollBack();

                $mensaje = "350: Error UnidadesUsoFacturadas : ".$rowC["nroinscripcion"]."<br>".$SqlCF."<br>".$insU;

                die($mensaje);
            }
        } else { //Si posee más de 1 Unidad de Uso
            $cCateg = 0;

            foreach ($itemsU as $rowU) {
                $cCateg++;

                //Se Obtiene Consumo Según el Porcentaje
                $conscategoria = $objFunciones->redondeado($ConsumoFact * ($rowU["porcentaje"] / 100), $DigRed);

                //Obtenemos el Importe a Facturar según la Tarifa
                $itemsI = $objFunciones->ImporteTarifa($codsuc, $rowU["catetar"], $tipofacturacionfact, $conscategoria, $nrofacturacion);

                $impagua += $itemsI[0];
                $impdes = $objFunciones->redondeado($itemsI[1], $DigRed);

                $impdesague += floatval(str_replace(",", "", $impdes));

                $insU = "INSERT INTO facturacion.unidadesusofacturadas ";
                $insU .= " (codemp, codsuc, nrofacturacion, codciclo, nroinscripcion, ";
                $insU .= " item, codunidaduso, catetar, porcentaje, principal, ";
                $insU .= " consumo, importe, importealc) ";
                $insU .= "VALUES(1, ".$codsuc.", ".$nrofacturacion.", ".$codciclo.", ".$rowC["nroinscripcion"].", ";
                $insU .= " ".$cCateg.", 1, ".$rowU["catetar"].", ".$rowU["porcentaje"].", ".$rowU["principal"].", ";
                $insU .= " ".$conscategoria.", '".str_replace(",", "", round($itemsI[0], 1))."', '".str_replace(",", "", round($impdes, 1))."')";

                $consU = $conexion->prepare($insU);
                $consU->execute(array());

                if ($consU->errorCode() != '00000') {
                    $conexion->rollBack();

                    $mensaje = "389: Error unidadesusofacturadas ".$rowC["nroinscripcion"]."<br>".$insU;

                    die($mensaje);
                }
            }
        }

        //SI SE FACTURA SE INSERTAN LOS CONCEPTOS
        if ($Flag) {
            //Cargo Fijo
            $sqlT = "SELECT impcargofijo1 ";
            $sqlT .= "FROM facturacion.tarifas ";
            $sqlT .= "WHERE codemp = 1 ";
            $sqlT .= " AND codsuc = ".$codsuc." ";
            $sqlT .= " AND catetar = ".$rowC["catetar"]." ";
            $sqlT .= " AND estado = 1 ";

            $consultaT = $conexion->prepare($sqlT);
            $consultaT->execute(array());
            $itemsT = $consultaT->fetch();

            if ($itemsT["impcargofijo1"] > 0) { //Si Cargo Fijo > 0
                $SqlCF = "SELECT codconcepto, descripcion ";
                $SqlCF .= "FROM facturacion.conceptos ";
                $SqlCF .= "WHERE codsuc = ".$codsuc." ";
                $SqlCF .= " AND categoria = 5 ";   //5: Cargo Fijo
                $SqlCF .= " AND codtipoconcepto = 1 ";  //1: Facturacion 2:Cobranza
                $SqlCF .= " AND sefactura = 1";

                $Consulta = $conexion->prepare($SqlCF);
                $Consulta->execute(array());

                foreach ($Consulta->fetchAll() as $row) {
                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($itemsT["impcargofijo1"], 2));

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);

                    $TotalParaIgv += floatval($ImporteConcepto);
                }
            }

            //AGUA Y DESAGUA
            if ($rowC["codtiposervicio"] == 1) {
                if (floatval($impagua) > 0) { //INSERTAR CONCEPTOS DE AGUA
                    $SqlA = "SELECT codconcepto, descripcion ";
                    $SqlA .= "FROM facturacion.conceptos ";
                    $SqlA .= "WHERE codsuc = ".$codsuc." ";
                    $SqlA .= " AND categoria = 1 ";   //1: AGUA
                    $SqlA .= " AND codtipoconcepto = 1 "; //1: Facturacion
                    $SqlA .= " AND sefactura = 1";

                    $Consulta = $conexion->prepare($SqlA);
                    $Consulta->execute(array());

                    foreach ($Consulta->fetchAll() as $row) {
                        $ItemDetalle++;

                        $ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($impagua, $DigRed));

                        //Insertamos el Detalle de la Facturacion
                        $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);

                        $TotalParaIgv += floatval($ImporteConcepto);
                    }
                }

                $impdesague = $objFunciones->redondeado($impdesague, $DigRed);

                if (floatval($impdesague) > 0) { //INSERTAR CONCEPTOS DE DESAGUE
                    $SqlD = "SELECT codconcepto, descripcion ";
                    $SqlD .= "FROM facturacion.conceptos ";
                    $SqlD .= "WHERE codsuc = ".$codsuc." ";
                    $SqlD .= " AND categoria = 2 ";   //2: DESAGUE
                    $SqlD .= " AND codtipoconcepto = 1 "; //1: Facturacion
                    $SqlD .= " AND sefactura = 1";

                    $Consulta = $conexion->prepare($SqlD);
                    $Consulta->execute(array());

                    foreach ($Consulta->fetchAll() as $row) {
                        $ItemDetalle++;

                        $ImporteConcepto = str_replace(",", "", $impdesague);

                        //Insertamos el Detalle de la Facturacion
                        $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);

                        $TotalParaIgv += floatval($ImporteConcepto);
                    }
                }

                //$impigv = $objFunciones->redondeado(($impagua + $impdesague) * ($igv / 100), 2);
            }

            //SOLO AGUA
            if ($rowC["codtiposervicio"] == 2) {
                $impdesague = 0;

                if (floatval($impagua) > 0) { //INSERTAR CONCEPTOS DE AGUA
                    $SqlA = "SELECT codconcepto, descripcion ";
                    $SqlA .= "FROM facturacion.conceptos ";
                    $SqlA .= "WHERE codsuc = ".$codsuc." ";
                    $SqlA .= " AND categoria = 1 ";   //1: AGUA
                    $SqlA .= " AND codtipoconcepto = 1 "; //1: Facturacion
                    $SqlA .= " AND sefactura = 1";

                    $Consulta = $conexion->prepare($SqlA);
                    $Consulta->execute(array());

                    foreach ($Consulta->fetchAll() as $row) {
                        $ItemDetalle++;

                        $ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($impagua, $DigRed));

                        //Insertamos el Detalle de la Facturacion
                        $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);

                        $TotalParaIgv += floatval($ImporteConcepto);
                    }
                }

                //$impigv = $objFunciones->redondeado(($impagua) * ($igv / 100), 2);
            }

            //SOLO DESAGUE
            if ($rowC["codtiposervicio"] == 3) { //Desague
                $impagua = 0;

                $impdesague = $objFunciones->redondeado($impdesague, $DigRed);

                if (floatval($impdesague) > 0) { //INSERTAR CONCEPTOS DE DESAGUE
                    $SqlD = "SELECT codconcepto, descripcion ";
                    $SqlD .= "FROM facturacion.conceptos ";
                    $SqlD .= "WHERE codsuc = ".$codsuc." ";
                    $SqlD .= " AND categoria = 2 ";   //2: DESAGUE
                    $SqlD .= " AND codtipoconcepto = 1 "; //1: Facturacion
                    $SqlD .= " AND sefactura = 1";

                    $Consulta = $conexion->prepare($SqlD);
                    $Consulta->execute(array());

                    foreach ($Consulta->fetchAll() as $row) {
                        $ItemDetalle++;

                        $ImporteConcepto = str_replace(",", "", $impdesague);

                        //Insertamos el Detalle de la Facturacion
                        $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);

                        $TotalParaIgv += floatval($ImporteConcepto);
                    }
                }

                //$impigv = $objFunciones->redondeado(($impdesague) * ($igv / 100), 2);
            }

//Conceptos que se Facturan
            $SqlD = "SELECT codconcepto, descripcion, importe, afecto_igv ";
            $SqlD .= "FROM facturacion.conceptos ";
            $SqlD .= "WHERE codsuc = ".$codsuc." ";
            $SqlD .= " AND categoria = 10 ";   //10: COLATERAL
            $SqlD .= " AND codtipoconcepto = 1 "; //1: Facturacion
            $SqlD .= " AND sefactura = 1";

            $Consulta = $conexion->prepare($SqlD);
            $Consulta->execute(array());

            foreach ($Consulta->fetchAll() as $row) {
                $ItemDetalle++;

                $ImporteConcepto = str_replace(",", "", $row["importe"]);

                //Insertamos el Detalle de la Facturacion
                $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);

                if ($row["afecto_igv"] == 1) {
                    $TotalParaIgv += floatval($ImporteConcepto);
                }
            }


//Fin Conceptos que se Facturan
            //VMA
            //if ($rowC['vma'] == 1)
            //			{
            //				$SqlV = "SELECT porcentaje, factor, descripcion ";
            //				$SqlV .= "FROM vma.unidades_uso ";
            //				$SqlV .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
            //				$SqlV .= " AND nroinscripcion = ".$rowC["nroinscripcion"]." ";
            //				$SqlV .= " AND factor > 0;";
            //
            //				$consultamva = $conexion->prepare($SqlV);
            //				$consultamva->execute(array());
            //				$itemsvma = $consultamva->fetchAll();
            //
            //
            //				$SqlCV = "SELECT descripcion ";
            //				$SqlCV .= "FROM facturacion.conceptos ";
            //				$SqlCV .= "WHERE codsuc = ".$codsuc." ";
            //				$SqlCV .= " AND codconcepto = ".$conceptovma;
            //
            //				$consultacpt = $conexion->prepare($SqlCV);
            //				$consultacpt->execute(array());
            //				$itemcpt = $consultacpt->fetch();
            //
            //				$descripcioncpt = $itemcpt['descripcion'];
            //
            //				foreach ($itemsvma as $rowvma) {
            //					$importe = $impdesague * $rowvma['porcentaje'] / 100;
            //					$importe = $importe * $rowvma['factor'] / 100;
            //
            //					$descripcioncpt = $descripcioncpt." (".$rowvma['descripcion'].") F=".intval($rowvma['factor'])."%";
            //
            //					//importe de desague,multiplar por el procenta de unida de uso
            //					//luego multiplicar por el facor
            //					$ItemDetalle++;
            //
            //					$ImporteConcepto = round($importe, 2);
            //
            //					//Insertamos el Detalle de la Facturacion
            //					$objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $conceptovma, $ImporteConcepto, 8, $codciclo, $ItemDetalle, $descripcioncpt);
            //
            //					$TotalParaIgv = floatval($TotalParaIgv) + floatval($ImporteConcepto);
            //				}
            //			}
            //////////VERIFICAR INTERES POR PENSIONES
            $impdeudaagua = 0;
            $impdeudaalc = 0;
            $intereses = 0;
            $impdeudaigv = 0;
            $impdeudacargo = 0;
            $impdeudacredito = 0;
            $impdeudavma = 0;
            $tipoconcepto = 1;

            //Interes de Deuda
            $SqlID = "SELECT * FROM facturacion.f_importedeuda(".$rowC["nroinscripcion"].", ".$codsuc.")";

            $consulta_deuda = $conexion->prepare($SqlID);
            $consulta_deuda->execute(array());

            $itd = $consulta_deuda->fetch();
            //die($itd["intagua"]);
            $agua = (($itd["agua"] ? $itd["agua"] : 0) + ($itd["intagua"] ? $itd["intagua"] : 0)) * $porcentaje;
            $desague = (($itd["desague"] ? $itd["desague"] : 0) + ($itd["intdesague"] ? $itd["intdesague"] : 0)) * $porcentaje;
            $igv = (($itd["importeigv"] ? $itd["importeigv"] : 0) + ($itd["intigv"] ? $itd["intigv"] : 0)) * $porcentaje;
            $cargo_fijo = (($itd["importecargo"] ? $itd["importecargo"] : 0) + ($itd["intcargo"] ? $itd["intcargo"] : 0)) * $porcentaje;
            $credito = (($itd["importecredito"] ? $itd["importecredito"] : 0) + ($itd["interescredito"] ? $itd["interescredito"] : 0)) * $porcentaje;
            $impdeudavma = (($itd["importevma"] ? $itd["importevma"] : 0) + ($itd["intvma"] ? $itd["intvma"] : 0)) * $porcentaje;

            $agua = floatval(str_replace(",", "", $objFunciones->redondeado($agua, $DigRed)));
            $desague = floatval(str_replace(",", "", $objFunciones->redondeado($desague, $DigRed)));
            $igv = floatval(str_replace(",", "", $objFunciones->redondeado($igv, $DigRed)));
            $cargo_fijo = floatval(str_replace(",", "", $objFunciones->redondeado($cargo_fijo, $DigRed)));
            $credito = floatval(str_replace(",", "", $objFunciones->redondeado($credito, $DigRed)));
            $impdeudavma = floatval(str_replace(",", "", $objFunciones->redondeado($impdeudavma, $DigRed)));

            //Insertamos los Conceptos de Intereses en el Detalle de la Facturación
            //Agua
            if (floatval($agua) <> 0) {
                $SqlIA = "SELECT codconcepto, descripcion ";
                $SqlIA .= "FROM facturacion.conceptos ";
                $SqlIA .= "WHERE codsuc = ".$codsuc." ";
                $SqlIA .= " AND categoria = 3 ";   //3: Interes
                $SqlIA .= " AND categoria_intereses = 1 "; //1: Interes Agua
                $SqlIA .= " AND codtipoconcepto = 1 ";  //1: Facturación
                $SqlIA .= " AND sefactura = 1";

                $Consulta = $conexion->prepare($SqlIA);
                $Consulta->execute(array());

                foreach ($Consulta->fetchAll() as $row) {
                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $agua);

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);
                }
            }

            //Desague
            if (floatval($desague) > 0) {
                $SqlID = "SELECT codconcepto, descripcion ";
                $SqlID .= "FROM facturacion.conceptos ";
                $SqlID .= "WHERE codsuc = ".$codsuc." ";
                $SqlID .= " AND categoria = 3 ";   //3: Interes
                $SqlID .= " AND categoria_intereses = 2 "; //2: Interes Desague
                $SqlID .= " AND codtipoconcepto = 1 ";  //1: Facturación
                $SqlID .= " AND sefactura = 1";

                $Consulta = $conexion->prepare($SqlID);
                $Consulta->execute(array());

                foreach ($Consulta->fetchAll() as $row) {
                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $desague);

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);
                }
            }

            //IGV
            if (floatval($igv) <> 0) {
                $SqlII = "SELECT codconcepto, descripcion ";
                $SqlII .= "FROM facturacion.conceptos ";
                $SqlII .= "WHERE codsuc = ".$codsuc." ";
                $SqlII .= " AND categoria = 3 ";   //3: Interes
                $SqlII .= " AND categoria_intereses = 3 "; //3: Interes IGV
                $SqlII .= " AND codtipoconcepto = 1 ";  //1: Facturación
                $SqlII .= " AND sefactura = 1";

                $Consulta = $conexion->prepare($SqlII);
                $Consulta->execute(array());

                foreach ($Consulta->fetchAll() as $row) {
                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $igv);

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);
                }
            }

            //Cargo Fijo
            if (floatval($cargo_fijo) > 0) {
                $SqlICF = "SELECT codconcepto, descripcion ";
                $SqlICF .= "FROM facturacion.conceptos ";
                $SqlICF .= "WHERE codsuc = ".$codsuc." ";
                $SqlICF .= " AND categoria = 3 ";   //3: Interes
                $SqlICF .= " AND categoria_intereses = 4 "; //4: Cargo Fijo
                $SqlICF .= " AND codtipoconcepto = 1 ";  //1: Facturación
                $SqlICF .= " AND sefactura = 1";

                $Consulta = $conexion->prepare($SqlICF);
                $Consulta->execute(array());

                foreach ($Consulta->fetchAll() as $row) {
                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $cargo_fijo);

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);
                }
            }

            //Crédito
            if (floatval($credito) > 0) {
                $SqlIC = "SELECT codconcepto, descripcion ";
                $SqlIC .= "FROM facturacion.conceptos ";
                $SqlIC .= "WHERE codsuc = ".$codsuc." ";
                $SqlIC .= " AND categoria = 3 ";   //3: Interes
                $SqlIC .= " AND categoria_intereses = 5 "; //5: Crédito
                $SqlIC .= " AND codtipoconcepto = 1 ";  //1: Facturación
                $SqlIC .= " AND sefactura = 1";

                $Consulta = $conexion->prepare($SqlIC);
                $Consulta->execute(array());

                foreach ($Consulta->fetchAll() as $row) {
                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $credito);

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);
                }
            } else {
                $credito = 0;
            }
        }

        $ImprimeC = 0;

        //CARGOS
        if ($rowC["codestadoservicio"] == 1 or $Flag) {
            $ImprimeC = 1;

            $impcargos = 0;

            $sqlcreditos = "SELECT det.nrocuota, det.totalcuotas, cab.codconcepto, ";
            $sqlcreditos .= " det.imptotal, det.nrocredito, det.subtotal, det.igv, det.redondeo, det.interes, ";
            $sqlcreditos .= " det.nrofacturacion, cab.nrocolaterales ";
            $sqlcreditos .= "FROM facturacion.detvarios det ";
            $sqlcreditos .= " INNER JOIN facturacion.cabvarios cab ON(det.codemp = cab.codemp) AND (det.codsuc = cab.codsuc) ";
            $sqlcreditos .= " AND (det.nrocredito = cab.nrocredito) ";
            $sqlcreditos .= "WHERE det.codemp = 1 AND det.codsuc = ".$codsuc." ";
            $sqlcreditos .= " AND det.estadocuota = 0 "; //0: Pendiente
            $sqlcreditos .= " AND cab.estareg = 1 ";  //1: Ingresado
            $sqlcreditos .= " AND cab.nroinscripcion = ".$rowC["nroinscripcion"]." ";
            $sqlcreditos .= "ORDER BY det.nrocredito, det.nrocuota  ";

            $consulta_creditos = $conexion->prepare($sqlcreditos);
            $consulta_creditos->execute(array());

            $SubtotalCreditos = 0;
            $SubTotalRed = 0;

            foreach ($consulta_creditos->fetchAll() as $row_creditos) {
                $codtipodeuda = 9;  //9: Cargos

                $dCuota = "";
                $codconceptoCre = $row_creditos['codconcepto'];

                $SqlC = "SELECT descripcion, categoria ";
                $SqlC .= "FROM facturacion.conceptos ";
                $SqlC .= "WHERE codsuc = ".$codsuc." ";
                $SqlC .= " AND codconcepto = ".$codconceptoCre;

                $consultacpt = $conexion->prepare($SqlC);
                $consultacpt->execute(array());
                $itemcpt = $consultacpt->fetch();

                $desconceptoCre = $itemcpt['descripcion'];

                //Sub Total
                if (floatval($row_creditos['subtotal']) <> 0) {
                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($row_creditos['subtotal'], $DigRed));

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $codconceptoCre, $ImporteConcepto, $codtipodeuda, $codciclo, $ItemDetalle, $desconceptoCre.$dCuota, $row_creditos['nrocredito'], $row_creditos['nrocuota']);

                    if ($itemcpt['categoria'] == 3) {
                        $impcargos += $ImporteConcepto;
                    } else {
                        $TotalParaIgv += floatval($ImporteConcepto);
                    }
                }
                //echo "IMPONIBLE : ".$codconceptoCre."<br>";
                //IGV
                //if (floatval($row_creditos['igv']) <> 0)
                //				{
                //					$ItemDetalle++;
                //
                //					$ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($row_creditos['igv'], $DigRed));
                //
                //					//Insertamos el Detalle de la Facturacion
                //					$objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], 5, $ImporteConcepto, $codtipodeuda, $codciclo, $ItemDetalle, "I.G.V.", $row_creditos['nrocredito'], $row_creditos['nrocuota']);
                //
                //					$impcargos += $ImporteConcepto;
                //				}
                //Interes
                if (floatval($row_creditos['interes']) <> 0) {
                    $SqlIC = "SELECT descripcion ";
                    $SqlIC .= "FROM facturacion.conceptos ";
                    $SqlIC .= "WHERE codsuc = ".$codsuc." ";
                    $SqlIC .= " AND codconcepto = 9"; //9: Interes Crédito

                    $consultacpt = $conexion->prepare($SqlIC);
                    $consultacpt->execute(array());
                    $itemcpt = $consultacpt->fetch();
                    $desconceptoCre = $itemcpt['descripcion'];

                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($row_creditos['interes'], $DigRed));

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], 9, $ImporteConcepto, $codtipodeuda, $codciclo, $ItemDetalle, $desconceptoCre, $row_creditos['nrocredito'], $row_creditos['nrocuota']);

                    $impcargos += $ImporteConcepto;
                }

                //Actualiza el estado en la Tabla de Cargos
                $updcreditos = "UPDATE facturacion.detvarios ";
                $updcreditos .= " SET estadocuota = 1 ";        //1: Facturado
                $updcreditos .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
                $updcreditos .= " AND nrocredito = ".$row_creditos['nrocredito']." "; //Nro de Crédito
                $updcreditos .= " AND nrocuota = ".$row_creditos['nrocuota'];   //Nro de Cuota
                $updcreditos .= " AND estadocuota = 0";         //0: Pendiente

                $update_creditos = $conexion->prepare($updcreditos);
                $update_creditos->execute(array());

                if ($update_creditos->errorCode() != '00000') {
                    $conexion->rollBack();

                    $mensaje = "976: Error UPDATE detvarios : ".$rowC["nroinscripcion"]."<br>".$updcreditos;

                    die($mensaje);
                }

                $updcreditos = "UPDATE facturacion.cabvarios ";
                $updcreditos .= " SET estareg = 2 ";         //2: Facturado
                $updcreditos .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
                $updcreditos .= " AND nrocredito = ".$row_creditos['nrocredito']." "; //Nro de Crédito

                $update_creditos = $conexion->prepare($updcreditos);
                $update_creditos->execute(array());

                if ($update_creditos->errorCode() != '00000') {
                    $conexion->rollBack();

                    $mensaje = "940: Error UPDATE cabvarios : ".$rowC["nroinscripcion"]."<br>".$updcreditos;

                    die($mensaje);
                }
            }
        }


        //CREDITOS - Cuotas
        $impcreditos = 0;

        $Sql = "SELECT det.nrocuota, det.totalcuotas, cab.codconcepto, ";
        $Sql .= " det.imptotal, det.nrocredito, det.subtotal, det.igv, det.redondeo, det.interes, ";
        $Sql .= " det.nrofacturacion, cab.nrocolaterales ";
        $Sql .= "FROM facturacion.detcreditos det ";
        $Sql .= " INNER JOIN facturacion.cabcreditos cab ON(det.codemp = cab.codemp) AND (det.codsuc = cab.codsuc) ";
        $Sql .= " AND (det.nrocredito = cab.nrocredito) ";
        $Sql .= "WHERE det.codemp = 1 AND det.codsuc = ".$codsuc." ";
        $Sql .= " AND cab.nroinscripcion = ".$rowC["nroinscripcion"]." ";
        $Sql .= " AND cab.estareg = 1 ";        //1: Ingresado
        $Sql .= " AND det.estadocuota = 0 ";       //0: Pendiente
        $Sql .= " AND det.nrofacturacion <> 0 ";
        $Sql .= " AND fechavencimiento <= '".$fechafacturacion."' "; //Cuota Vencida
        $Sql .= "ORDER BY det.nrocredito, nrocuota";

        $consulta_creditos = $conexion->prepare($Sql);
        $consulta_creditos->execute(array());

        foreach ($consulta_creditos->fetchAll() as $row_creditos) {
            $ImprimeC = 2;

            $codtipodeuda = 4; //4: Créditos

            $SubtotalCreditos += $row_creditos['subtotal'];
            $SubTotalRed += $row_creditos['redondeo'];

            //Sub Total - Concepto
            if (floatval($row_creditos['subtotal']) <> 0) {
                $dCuota = " Cuota(".$row_creditos['nrocuota']."/".$row_creditos['totalcuotas'].")";

                $codconceptoCre = '992';     //992: Convenio Empresa

                $desconceptoCre = utf8_decode('CONVENIO N° '.$row_creditos['nrocredito']);

                $ItemDetalle++;

                $ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($row_creditos['subtotal'], $DigRed));

                //Insertamos el Detalle de la Facturacion
                $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $codconceptoCre, $ImporteConcepto, $codtipodeuda, $codciclo, $ItemDetalle, $desconceptoCre.$dCuota, $row_creditos['nrocredito'], $row_creditos['nrocuota']);

                $impcreditos += $ImporteConcepto;
            }

            //IGV
            //if (floatval($row_creditos['igv']) <> 0)
            //			{
            //				$ItemDetalle++;
            //
            //				$desconceptoCre = "IGV";
            //
            //				$ImporteConcepto = str_replace(",", "", round($row_creditos['igv'], 2));
            //
            //				//Insertamos el Detalle de la Facturacion
            //				$objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], 5, $ImporteConcepto, $codtipodeuda, $codciclo, $ItemDetalle, $desconceptoCre, $row_creditos['nrocredito'], $row_creditos['nrocuota']);
            //
            //				$impcreditos += $ItemDetalle;
            //			}
            //Interes
            if (floatval($row_creditos['interes']) <> 0) {
                $SqlI = "SELECT descripcion ";
                $SqlI .= "FROM facturacion.conceptos ";
                $SqlI .= "WHERE codsuc = ".$codsuc." ";
                $SqlI .= " AND codconcepto = 9";  //9: Interes Crédito

                $consultacpt = $conexion->prepare($SqlI);
                $consultacpt->execute(array());
                $itemcpt = $consultacpt->fetch();

                $desconceptoCre = $itemcpt['descripcion'];

                $ItemDetalle++;

                $ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($row_creditos['interes'], $DigRed));

                //Insertamos el Detalle de la Facturacion
                $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], 9, $ImporteConcepto, $codtipodeuda, $codciclo, $ItemDetalle, $desconceptoCre, $row_creditos['nrocredito'], $row_creditos['nrocuota']);

                $impcreditos += $ImporteConcepto;
            }

            ////Redondeo
            //			if (floatval($row_creditos['redondeo']) <> 0)
            //			{
            //				$codredondeo = 8;  //8: Redondeo Positivo
            //
            //				if (floatval($row_creditos['redondeo']) < 0)
            //				{
            //					$codredondeo = 7; //7; Redondeo Negativo
            //				}
            //
            //				$SqlR = "SELECT descripcion ";
            //				$SqlR .= "FROM facturacion.conceptos ";
            //				$SqlR .= "WHERE codsuc = ".$codsuc." ";
            //				$SqlR .= " AND codconcepto = ".$codredondeo;
            //
            //				$consultacpt = $conexion->prepare($SqlR);
            //				$consultacpt->execute(array());
            //				$itemcpt = $consultacpt->fetch();
            //
            //				$desconceptoCre = $itemcpt['descripcion'];
            //
            //				$ItemDetalle++;
            //
            //				$ImporteConcepto = str_replace(",", "", round($row_creditos['redondeo'], 2));
            //
            //				//Insertamos el Detalle de la Facturacion
            //				$objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $codredondeo, $ImporteConcepto, $codtipodeuda, $codciclo, $ItemDetalle, $desconceptoCre, $row_creditos['nrocredito'], $row_creditos['nrocuota']);
            //
            //				$impcreditos += $ItemDetalle;
            //			}
            //Actualiza el estado en la Tabla de creditos
            $updcreditos = "UPDATE facturacion.detcreditos ";
            $updcreditos .= " SET estadocuota = 1 ";
            $updcreditos .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
            $updcreditos .= " AND nrocredito = ".$row_creditos['nrocredito']." "; //Nro de Crédito
            $updcreditos .= " AND nrocuota = ".$row_creditos['nrocuota']." ";  //Nro de Cuota
            $updcreditos .= " AND estadocuota = 0";         //0: Pendoente

            $update_creditos = $conexion->prepare($updcreditos);
            $update_creditos->execute(array());

            if ($update_creditos->errorCode() != '00000') {
                $conexion->rollBack();

                $mensaje = "1151: Error detfacturacion : ".$rowC["nroinscripcion"]."<br>".$updcreditos;
                die($mensaje);
            }
        }

        //REFINANCIAMIENTO - Cuotas
        $imprefinanciamiento = 0;

        $Sql = "SELECT SUM(d.importe) AS importe, d.nrorefinanciamiento, d.nrocuota, d.totalcuotas ";
        $Sql .= "FROM facturacion.detrefinanciamiento d ";
        $Sql .= " INNER JOIN facturacion.cabrefinanciamiento c ON(d.codemp = c.codemp) AND (d.codsuc = c.codsuc) ";
        $Sql .= "  AND (d.nrorefinanciamiento = c.nrorefinanciamiento) ";
        $Sql .= " INNER JOIN facturacion.conceptos co ON(d.codemp = co.codemp) AND (d.codsuc=co.codsuc) AND (d.codconcepto = co.codconcepto) ";
        $Sql .= "WHERE c.codsuc = ".$codsuc." ";
        $Sql .= " AND d.estadocuota = 0 ";         //0: Pendiente
        $Sql .= " AND c.nroinscripcion = ".$rowC["nroinscripcion"]." ";
        $Sql .= " AND d.fechavencimiento <= '".$fechafacturacion."' ";  //Cuota Vencida
        $Sql .= "GROUP BY d.nrorefinanciamiento, d.nrocuota, d.totalcuotas ";
        $Sql .= "ORDER BY d.nrorefinanciamiento, d.nrocuota";

        $consulta_refinanciamiento = $conexion->prepare($Sql);
        $consulta_refinanciamiento->execute(array());

        $i = 0;
        $impref = 0;

        foreach ($consulta_refinanciamiento->fetchAll() as $row_refinanciamiento) {
            $ImprimeC = 2;

            $codtipodeuda = 3;  //3: Refinanciamiento

            $codconceptoRef = '';

            $impref += $row_refinanciamiento['importe'];

            $dCuota = " Cuota(".$row_refinanciamiento['nrocuota']."/".$row_refinanciamiento['totalcuotas'].")";

            $codconceptoRef = '992'; //Convenio Empresa
            $desconceptoCre = 'CONVENIO EMPRESA';

            $ItemDetalle++;

            $ImporteConcepto = str_replace(",", "", $objFunciones->redondeado($row_refinanciamiento['importe'], $DigRed));

            //Insertamos el Detalle de la Facturacion
            $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $codconceptoRef, $ImporteConcepto, $codtipodeuda, $codciclo, $ItemDetalle, $desconceptoCre.$dCuota, 0, $row_refinanciamiento['nrocuota'], $row_refinanciamiento['nrorefinanciamiento']);

            //Actualiza el estado en la Tabla de Refeinanciamiento
            $updaterefinanciamiento = "UPDATE facturacion.detrefinanciamiento";
            $updaterefinanciamiento .= " SET estadocuota = 1 ";
            $updaterefinanciamiento .= "WHERE codsuc = ".$codsuc." ";
            $updaterefinanciamiento .= " AND estadocuota = 1 ";   //1: Facturado
            $updaterefinanciamiento .= " AND nrorefinanciamiento = ".$row_refinanciamiento['nrorefinanciamiento']." ";
            $updaterefinanciamiento .= " AND nrocuota = ".$row_refinanciamiento['nrocuota'];

            $update_refinanciamiento = $conexion->prepare($updaterefinanciamiento);
            $update_refinanciamiento->execute(array());

            if ($update_refinanciamiento->errorCode() != '00000') {
                $conexion->rollBack();

                $mensaje = "1214: Error UPDATE DetRefinanciamiento : ".$rowC["nroinscripcion"]."<br>".$updaterefinanciamiento;

                die($mensaje);
            }
        }

        //IGV TOTAL
        if ($TotalParaIgv > 0) {
            $impigv = $objFunciones->redondeado(floatval($TotalParaIgv) * (floatval($Pigv) / 100), $DigRed);

            if ($impigv > 0) {
                $SqlI = "SELECT codconcepto, descripcion ";
                $SqlI .= "FROM facturacion.conceptos ";
                $SqlI .= "WHERE codsuc = ".$codsuc." ";
                $SqlI .= " AND categoria = 4 ";   //4: IGV
                $SqlI .= " AND codtipoconcepto = 1 "; //1: Facturación
                $SqlI .= " AND sefactura = 1";

                $Consulta = $conexion->prepare($SqlI);
                $Consulta->execute(array());

                foreach ($Consulta->fetchAll() as $row) {
                    $ItemDetalle++;

                    $ImporteConcepto = str_replace(",", "", $impigv);

                    //Insertamos el Detalle de la Facturacion
                    $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);
                }
            }
        }

        if ($ImprimeC > 0) {
            if ($correlativotem == 0) {
                $correlativotem = $correlativo;
                $correlativo++;
            }

            $Immm = 0;

            if ($ImprimeC == 2) {
                $Immm = 1;
            }

            $SqlC = "UPDATE facturacion.cabfacturacion";
            $SqlC .= " SET imprimir = ".$Immm.", ";
            $SqlC .= " coddocumento = ".$documento.", serie = '".$seriedoc."', nrodocumento = ".$correlativotem." ";
            $SqlC .= "WHERE codsuc = ".$codsuc." AND codciclo = ".$codciclo." ";
            $SqlC .= " AND nrofacturacion = ".$nrofacturacion." AND nroinscripcion = ".$rowC["nroinscripcion"];

            $update_cabFact = $conexion->prepare($SqlC);
            $update_cabFact->execute(array());

            if ($update_cabFact->errorCode() != '00000') {
                $conexion->rollBack();

                $mensaje = "1248: Error UPDATE CabFacturacion : ".$rowC["nroinscripcion"]."<br>".$SqlC;

                die($mensaje);
            }
        }

        //VERIFICAR SI EL USUARIO TIENE PAGOS DOBLES
        $totfacturacion = $objFunciones->redondeado($TotalParaIgv, $DigRed) + $objFunciones->redondeado($impigv, $DigRed) + //Impobible e IGV
            $objFunciones->redondeado($agua, $DigRed) + $objFunciones->redondeado($desague, $DigRed) + $objFunciones->redondeado($cargo_fijo, $DigRed) + //Intereses
            $objFunciones->redondeado($igv, $DigRed) + $objFunciones->redondeado($credito, $DigRed) + $objFunciones->redondeado($impdeudavma, $DigRed) + //Intereses
            $objFunciones->redondeado($impcreditos, $DigRed) + $objFunciones->redondeado($impref, $DigRed) //Cargos, Créditos y Refinanciamientos
                + $objFunciones->redondeado($impcargos, 2) + $objFunciones->redondeado($ImporteConcepto, 2);

        //die($TotalParaIgv." : ".$impigv);

        $tot1 = $objFunciones->redondeado($totfacturacion, 2);
        $tot2 = $objFunciones->redondeado($totfacturacion, 1);

        $conceptodoble = 10005;  //10005: Pago Adelantado (Caja)

        $totalpagosdobles = 0;

        $Sql = "SELECT SUM(importe) ";
        $Sql .= "FROM cobranza.pagosvarios ";
        $Sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
        // $Sql .= " AND nrofacturacion = ".$nrofacturacion." ";
        $Sql .= " AND codconcepto = ".$conceptodoble." ";
        $Sql .= " AND nroinscripcion = ".$rowC["nroinscripcion"]." AND estado = 1 ";

        $result = $conexion->prepare($Sql);
        $result->execute(array());
        $rowpgdb = $result->fetch();

        if ($rowpgdb[0] != '') {
            $conceptodoble = 10012;  //10012: Pago Adelantado
            //SI EL IMPORTE ADELANTADO ES MENOR AL FACTURADO
            $SqlCD = "SELECT descripcion ";
            $SqlCD .= "FROM facturacion.conceptos ";
            $SqlCD .= "WHERE codsuc = ".$codsuc." ";
            $SqlCD .= " AND codconcepto = ".$conceptodoble;

            $consultacpt = $conexion->prepare($SqlCD);
            $consultacpt->execute(array());
            $itemcpt = $consultacpt->fetch();

            $descripcioncpt = $itemcpt['descripcion'];

            $ItemDetalle++;

            $saldo = floatval($tot1) - floatval($objFunciones->redondeado($rowpgdb[0], $DigRed));

            if (floatval($saldo) >= 0) {
                $ImporteConcepto = $objFunciones->redondeado($rowpgdb[0], $DigRed) * -1;

                //Insertamos el Detalle de la Facturacion
                $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $conceptodoble, $ImporteConcepto, 1, $codciclo, $ItemDetalle, $descripcioncpt);

                $totalpagosdobles = $objFunciones->redondeado($rowpgdb[0], $DigRed);

                $sqlD  = "UPDATE cobranza.pagosvarios SET ";
                $sqlD .= " estado = 2 ";
                $sqlD .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
                $sqlD .= " AND nroinscripcion = ".$rowC["nroinscripcion"]." ";
                $sqlD .= " AND estado =  1 ";

                $resultD = $conexion->prepare($sqlD);
                $resultD->execute(array());

                if ($resultD->errorCode() != '00000') {
                    $conexion->rollBack();

                    $mensaje = "1266: Error UPDATE PagosVarios<br>".$sqlD;

                    die($mensaje);
                }

            } else {
                $ImporteConcepto = $objFunciones->redondeado($tot2, $DigRed) * -1;

                //Insertamos el Detalle de la Facturacion
                $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $conceptodoble, $ImporteConcepto, 1, $codciclo, $ItemDetalle, $descripcioncpt);

                $sqlD  = "UPDATE cobranza.pagosvarios SET ";
                $sqlD .= " estado = 2 ";
                $sqlD .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
                $sqlD .= " AND nroinscripcion = ".$rowC["nroinscripcion"]." ";
                $sqlD .= " AND estado =  1 ";

                $resultD = $conexion->prepare($sqlD);
                $resultD->execute(array());

                if ($resultD->errorCode() != '00000') {
                    $conexion->rollBack();

                    $mensaje = "1266: Error UPDATE PagosVarios<br>".$sqlD;

                    die($mensaje);
                }

                $totalpagosdobles = $objFunciones->redondeado($tot2, $DigRed);
                $Importe = floatval($objFunciones->redondeado($rowpgdb[0], $DigRed)) - floatval($tot2);

                //INGRESAMOS OTRO REGISTRO CON LA DIFERENCIA
                $conceptodoble = 10005;
                $sqlD = "INSERT INTO cobranza.pagosvarios ";
                $sqlD .= " (codemp, codsuc, nrofacturacion, nroinscripcion, ";
                $sqlD .= " nropago, codconcepto, importe, creador, fechareg, estado) ";
                $sqlD .= "VALUES(1, ".$codsuc.", ".($nrofacturacion + 1).", ".$rowC['nroinscripcion'].", ";
                $sqlD .= " 0, ".$conceptodoble.", '$Importe', 1, '".date('Y-m-d')."', 1);";

                $resultD = $conexion->prepare($sqlD);
                $resultD->execute(array());

                if ($resultD->errorCode() != '00000') {
                    $conexion->rollBack();

                    $mensaje = "1330: Error INSERT PagosVarios<br>".$sqlD;
                    die($mensaje);
                }
            }
        }

        //REDONDEO
        $redctual = $objFunciones->redondeado($tot2 - $tot1, 2);

        if ($redctual == -0.05) {
            $redctual = 0.05;
        }

        if (floatval($redctual) <> 0) {
            $redpositivo = 1;  //1: Redondeo Positivo

            if (floatval($redctual) < 0) {
                $redpositivo = 2; //2: Redondeo Negativo
            }

            $Sqlr = "SELECT codconcepto, descripcion ";
            $Sqlr .= "FROM facturacion.conceptos ";
            $Sqlr .= "WHERE codsuc = ".$codsuc." ";
            $Sqlr .= " AND categoria = 7 ";   //7: Redondeo
            $Sqlr .= " AND codtipoconcepto = 1 "; //1: Facturación
            $Sqlr .= " AND sefactura = 1 ";
            $Sqlr .= " AND categoria_redondeo = ".$redpositivo;

            $Consulta = $conexion->prepare($Sqlr);
            $Consulta->execute(array());

            foreach ($Consulta->fetchAll() as $row) {
                $ItemDetalle++;

                $ImporteConcepto = str_replace(",", "", $redctual);

                //Insertamos el Detalle de la Facturacion
                $objFunciones->InsertDetFac($codemp, $codsuc, $nrofacturacion, $rowC["nroinscripcion"], $row["codconcepto"], $ImporteConcepto, 1, $codciclo, $ItemDetalle, $row["descripcion"]);
            }
        }

        //Actualiza la tabla CLIENTES con el valor del REDONDEO
        $SqlCR = "UPDATE catastro.clientes ";
        $SqlCR .= " SET redondeo = '".str_replace(",", "", $objFunciones->redondeado($redctual, 2))."' ";
        $SqlCR .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
        $SqlCR .= " AND nroinscripcion = ".$rowC["nroinscripcion"];

        $updcliente = $conexion->prepare($SqlCR);
        $updcliente->execute(array());

        if ($updcliente->errorCode() != '00000') {
            $conexion->rollBack();

            $mensaje = "1320: Error UPDATE Clientes : ".$rowC["nroinscripcion"]."<br>".$SqlCR;

            die($mensaje);
        }
    }

    //REINCIAMOS VARIABLES
    $TotalParaIgv = 0;
    $impigv = 0;
    $impcreditos = 0;
    $agua = 0;
    $desague = 0;
    $igv = 0;
    $cargo_fijo = 0;
    $credito = 0;
    $impcargos = 0;

    //MODIFICAMOS ARCHIVO PROGRESO
    $actualAvance++;

    $porcentajeAvance = round($actualAvance * 100 / $totalAvance);
    $textoAvance = $porcentajeAvance."|".$actualAvance." de ".$totalAvance;

    file_put_contents('../progreso.txt', $textoAvance);
}


$textoAvance = "100|".$totalAvance." de ".$totalAvance;

file_put_contents('../progreso.txt', $textoAvance);

//Actualiza la tabla CORRELATIVOS
$SqlCor = "UPDATE reglasnegocio.correlativos ";
$SqlCor .= " SET correlativo = ".$correlativo." ";
$SqlCor .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
$SqlCor .= " AND coddocumento = 4 ";  //4: Recibo por Servicio Públicos

$upddocumentoS = $conexion->prepare($SqlCor);
$upddocumentoS->execute(array());

if ($upddocumentoS->errorCode() != '00000') {
    $conexion->rollBack();

    $mensaje = "1430: Error UPDATE Correlativos <br>".$SqlCor;

    die($mensaje);
}

//Actualizamos el PERIODO, FACTURACION e insertamos el nuevo PERIODO
$SqlP = "UPDATE facturacion.periodofacturacion ";
$SqlP .= " SET facturacion = 1 ";
$SqlP .= "WHERE codemp = 1 AND codsuc = ".$codsuc." ";
$SqlP .= " AND codciclo = ".$codciclo." ";
$SqlP .= " AND nrofacturacion = ".$nrofacturacion;

$updperiodo = $conexion->prepare($SqlP);
$updperiodo->execute(array());

if ($updperiodo->errorCode() != '00000') {
    $conexion->rollBack();

    $mensaje = "1449: Error UPDATE PeriodoFacturacion<br>".$SqlP;

    die($mensaje);
}

$newnrofact = $nrofacturacion + 1;
$mes = $mes + 1;

if ($mes > 12) {
    $mes = 1;
    $anio += 1;
}

$porcentaje = $porcentaje * 100;

//Insertamos el Nuevo Periodo de Facturacion
$insperiodo = "INSERT INTO facturacion.periodofacturacion ";
$insperiodo .= "(codemp, codsuc, nrofacturacion, anio, mes, creador, codciclo, tasapromintant, nrotarifa) ";
$insperiodo .= "VALUES(1, ".$codsuc.", ".$newnrofact.", '".$anio."', '".$mes."', ".$idusuario.", ".$codciclo.", '".$porcentaje."', ".$nrotarifa.")";

$insertar_periodo = $conexion->prepare($insperiodo);
$insertar_periodo->execute(array());

if ($insertar_periodo->errorCode() != '00000') {
    $conexion->rollBack();

    $mensaje = "1477: Error INSERT PeriodoFacturacion <br>".$insperiodo;

    die($mensaje);
}

//Actualizamos el Nro de Facturación en DetCreditos
$Sql = "UPDATE facturacion.detcreditos ";
$Sql .= "SET nrofacturacion = ".$newnrofact." ";
$Sql .= "WHERE codsuc = ".$codsuc." AND anio = '".$anio."' AND mes = '".$mes."'";

$updperiodo = $conexion->prepare($Sql);
$updperiodo->execute(array());

if ($updperiodo->errorCode() != '00000') {
    $conexion->rollBack();

    $mensaje = "1494: Error UPDATE detcreditos <br>".$sql;

    die($mensaje);
}

if (!$consCab) {
    $conexion->rollBack();

    $img = "<img src='".$_SESSION['urldir']."images/iconos/error.png' width='31' height='31' />";
    $mensaje = "Error al Realizar la Facturacion";

    $res = 0;
} else {
    //$conexion->rollBack();
    $conexion->commit();

    $img = "<img src='".$_SESSION['urldir']."images/iconos/Ok.png' width='31' height='31' />";
    $mensaje = "La Facturacion se ha Realizado Correctamente";
    $mensaje .= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";

    $res = 1;
}

echo $img."|".$mensaje."|".$res;
?>
