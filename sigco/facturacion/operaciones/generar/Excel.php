<?php
	set_time_limit(0);
	
	include("../../../../objetos/clsReporte.php");
	include("../../../../objetos/clsReporteExcel.php");
	
	$codsuc		= $_SESSION['IdSucursal'];
	
	$urldir = $_SESSION['urldir'];
	
	$clsFunciones = new clsFunciones();
	$objReporte = new clsReporte();
	
	header("Content-type: application/vnd.ms-excel; name='excel'");  
    header("Content-Disposition: filename=Usuarios.xls");  
    header("Pragma: no-cache");  
    header("Expires: 0"); 
	//////////////
	CabeceraExcel(3, 2);
	
	$codemp	= 1;
	
	$Sql = "SELECT nrocredito, codantiguo, propietario, fechareg 
			FROM facturacion.cabcreditos 
			WHERE codsuc=".$codsuc." AND estareg=1
			AND (nroinscripcion) 
			NOT IN (SELECT nroinscripcion FROM catastro.clientes WHERE codsuc=".$codsuc.")
			ORDER BY fechareg";
			
	$Consulta = $conexion->query($Sql);
	$i=0;
	$tr.='<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbdetallefacturacion" rules="all" class="myTable">';
	$tr.='<thead class="ui-widget-header" style="font-size:12px">';
	$tr.='     <tr align="center" class="ui-widget-header">
	<th width="5%" class="ui-widget-header" colspan="5">Usuarios con Creditos sin Catastrar</th>
        

     </tr>
     <tr align="center" class="ui-widget-header">
	<th width="5%" class="ui-widget-header">Item</th>
        <th width="15%" class="ui-widget-header">Credito</th>
        <th width="15%" class="ui-widget-header">Fecha</th>
        <th width="15%" class="ui-widget-header">Inscripcion</th>
        <th width="40%" class="ui-widget-header">Usuario</th>

     </tr>
 </thead><tbody>';
	foreach($Consulta->fetchAll() as $row)
	{
		$i++;
		$tr.="<tr>";
		$tr.="<td>".$i."</td>";
		$tr.="<td>".$row['nrocredito']."</td>";
		$tr.="<td>".$clsFunciones->DecFecha($row['fechareg'])."</td>";
		$tr.="<td>".$row['codantiguo']."</td>";
		$tr.="<td>".$row['propietario']."</td>";
		$tr.="</tr>";
	}
	$tr.="</tbody></table>";
	echo $tr;
	?>