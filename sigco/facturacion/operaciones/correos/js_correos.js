// JavaScript Document
function generar_recibos()
{
	if($("#ciclo").val()==0)
	{
		alert("Seleccione el Ciclo")
		return false
	}

	window.parent.blokear_pantalla("Los Recibos de Facturacion se Estan Generando")

	$.ajax({
		 url:'generar/impresion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+$("#ciclo").val()+'&codsuc='+codsuc+'&nrofacturacion='+$("#nrofacturacion").val(),
		 success:function(datos){
			var r=datos.split("|")

			window.parent.establecer_texto(r[0],r[1])
		 }
	}) 

}
function enviar_recibos()
{
	if($("#ciclo").val()==0)
	{
		alert("Seleccione el Ciclo")
		return false
	}

	window.parent.blokear_pantalla("Se Estan Enviando los Recibos")

	$.ajax({
		 url:'generar/envios.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+$("#ciclo").val()+'&codsuc='+codsuc+'&nrofacturacion='+$("#nrofacturacion").val(),
		 success:function(datos){
			var r=datos.split("|")

			window.parent.establecer_texto(r[0],r[1])
		 }
	}) 

}