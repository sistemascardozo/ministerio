<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../config.php");
	
	include("../../../../../objetos/Correo.php");
	
	$codciclo 		= $_POST["codciclo"];
	$codsuc			= $_POST["codsuc"];
	$nrofacturacion	= ($_POST["nrofacturacion"]-1);
	
	$sqlF = "select anio,mes from 
					facturacion.periodofacturacion where nrofacturacion=? and codciclo=? and codsuc=?";
	$consultaF = $conexion->prepare($sqlF);
	$consultaF->execute(array($nrofacturacion,$codciclo,$codsuc));
	$itemsF = $consultaF->fetch();
	
	$sucursal 	= substr("000".$codsuc,strlen("000".$codsuc)-3,3);
	$ciclo		= substr("000".$codciclo,strlen("000".$codciclo)-3,3);
	$sql = "select * from catastro.clientes where codsuc=? and codciclo=? and correo is not null AND trim(correo)<>''";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$codciclo));
	$items = $consulta->fetchAll();

	foreach($items as $row)
	{
		if(isset($row["correo"]))
		{
			if(filter_var($row["correo"], FILTER_VALIDATE_EMAIL))
	 		{
				//Destinatarios = Destinatarios+ Email+'/'+ Nombres+'|'
				$Destinatarios = $row["correo"].'/'.$row["propietario"];
				$Destinatarios = substr($Destinatarios, 0, -1);
				
				$EmailConsul 	= "facturacion@emapacopsa.com.pe";
		 		$ClaveEmail 	= "13579";

				$Asunto			= "Recibo ".$meses[$itemsF["mes"]]." - ".$itemsF["anio"];
				$Contenido		= "Estimado Cliente : ".$row["propietario"]." adjunto se remite copia del recibo de agua potable y/o alcantarillado correspondiente al mes de ".$meses[$itemsF["mes"]]." - ".$itemsF["anio"];
				$Contenido		.= "<br> <br> Atentamente: EMAPACOP S.A.";
				//$Contenido.="<br> <br>Este Mensaje es solo referencial y podra ser modificado de acuerdo a requerimiento del cliente";
				$NombreEmail	= "Seda Huanuco S.A.";
				
				$archivo = $sucursal.$ciclo.$row["nroinscripcion"]."-".$itemsF["mes"].$itemsF["anio"].".pdf";
				
				if(file_exists("../recibos/".$archivo))
				{
					try {
						EnviarCorreo($Destinatarios, $Asunto, $Contenido, $EmailConsul, $ClaveEmail, $NombreEmail, $archivo);
					} 
					catch (Exception  $e){
						echo "";
					}
				}
				else
				{
					echo "el archivo no existe ".$archivo;
					die();
				}	 		
			}
		}
	}
	
	$img 		 = "<img src='".$urldir."images/Ok.png' width='31' height='31' />";
	$mensaje 	 = "Los Recibos se han Enviado Correctamente";
	$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
			
	echo $img."|".$mensaje."|".$res;
?>