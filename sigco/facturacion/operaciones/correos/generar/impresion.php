<?php 
	include("../../../../../objetos/clsReporte.php");
	include("../../../../../admin/clases/num2letra.php");
	
	require_once  'Image/Barcode.php';
	ini_set("memory_limit","512M");
	
	class clsImpresion extends clsReporte
	{
		function Header()
		{
			
					
		}
		
		function Footer()
		{		
			 
		}
		function generargraficos($mesesconsumo,$meslit,$nroinscripcion)
		{
			
				$count=0;
				
				$carpetaimg = $_SERVER['DOCUMENT_ROOT']."/siinco/sigco/facturacion/operaciones/impresion/graficos/";
				chmod($carpetaimg,7777);
				
				$img = $carpetaimg."consumo_".$nroinscripcion.".png";

				if(file_exists($img)){unlink($img);}

				$datosy=array_merge($mesesconsumo);
		
				// Creamos el grafico
				$grafico = new Graph(500,250);
				$grafico->SetScale('textlin');
		
				 $grafico->xaxis->SetTickLabels(array_merge($meslit));
				// Ajustamos los margenes del grafico-----    (left,right,top,bottom)
				$grafico->SetMargin(40,30,30,40);
		
				// Creamos barras de datos a partir del array de datos
				$bplot = new BarPlot($datosy);
		
				// Configuramos color de las barras 
				$bplot->SetFillColor('#479CC9');
		
				// Queremos mostrar el valor numerico de la barra
				$bplot->value->Show();
		
				//A?adimos barra de datos al grafico
				$grafico->Add($bplot);
				
				// Configuracion de los titulos
				$grafico->title->Set('Evolucion de Consumo');
				$grafico->yaxis->title->Set('Consumo');
				$grafico->xaxis->title->Set('Meses Consumido');
		
				$grafico->title->SetFont(FF_FONT1,FS_BOLD);
				$grafico->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
				$grafico->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
		
				// Se muestra el grafico
				$grafico->Stroke($img);	
		
				return $img;
	
		}
		 function Contenido($x, $y, $nroinscripion, $catastro, $aniomes, $nrorecibo, $femision, $propietario, $direccion, $servicio, $tipo_facturacion, $unduso, $tarifa, $nromed, $fechalectanterior, $lectanterior, $consumo, $fechalectultima, $lectultima, $importeagua, $importedesague, $importeintereses, $importecargofijo, $importecredito, $importerefinanciamiento, $nromeses, $impdeuda, $valorigv, $impmesigv, $impmesredondeo, $mensaje, $fvencimiento, $fvencimientodeudores, $fcorte, $consumoGraf, $mesesGraf, $mes, $docidentidad, $nrodocidentidad, $mensajenormal, $codantiguo, $lecturapromedio, $codsuc, $horasabastecimiento, $nrofacturacion,$impColateral,$fechavencimiento3,$estadomedidor,$consumofact) {

            global $tfacturacion, $meses, $conexion;
            switch ($codsuc) {
                case 1:
                    $zona = "HUANUCO";
                    $serie = '201'; break;

                case 2:
                    $zona = "TINGO MARIA";
                    $serie = '101'; break;
                case 3:
                    $zona = "AUCAYACU";
                    $serie = '301'; break;
            }
             $this->Image("Plantilla.jpg",0,1,172,255);
            //ECHO $nroinscripion;
            $verVma = "SELECT vma FROM catastro.clientes WHERE nroinscripcion=? AND codsuc=? ";
            $consulta = $conexion->prepare($verVma);
            $consulta->execute(array($nroinscripion, $codsuc));
            $itemsD = $consulta->fetch();
            $vma = $itemsD[0];
            
            $grafico = new Graph();
            $grafico = Image_Barcode::draw($codantiguo, 'Code128', 'jpg', false);
            imagejpeg($grafico, "barras/".$nroinscripion.'.jpg');

            $AlturaBarra = 230;
            if ($x != 3) {

                $this->Image("barras/".$nroinscripion.'.jpg', 270, $AlturaBarra, 70, 15);
                $this->SetX($x);
                $this->SetY($y);
            } else {
                $this->Image("barras/".$nroinscripion.'.jpg', 90, $AlturaBarra, 70, 15);
            }
            $this->SetFont('Arial', '', 10);

            if ($vma == 0) {

                if ($tipo_facturacion == 0) {
                    $img = $this->generargraficos($consumoGraf, $mesesGraf, $nroinscripion);
                    if ($x != 3) {
                        $this->Image($img, 290, 70, 40, 30);
                    } else {
                        $this->Image($img, 110, 70, 40, 30);
                    }
                }
            }

            //$this->SetX($x);
            $this->SetFont('Arial', '', 11);

            //$y = $this->GetY();
            //$this->Rect($x + 80, $y - 8, 70, 20);

            $this->SetXY($x + 55, $y + 3);
            $this->Cell(25, 5, "", 0, 0, 'L');
            $this->SetFont('Arial', '', 9);
            $this->Cell(30, 5, utf8_decode(strtoupper($propietario)), 0, 1, 'R');
            $this->Ln(1);
            $this->SetX($x + 85);
            $this->SetFont('Arial', 'B', 14);
            $this->Cell(30, 5, $catastro, 0, 1, 'L');

            $this->SetX($x + 70);
            $this->Cell(0, 0, " ", 0, 1, 'C');
            $this->SetXY($x + 120, 6);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(30, 5, $codantiguo, 0, 1, 'C');
            $this->SetFont('Arial', '', 10);
            $this->SetXY(115, 16);
            $this->Cell(25, 5, "", 0, 0, 'L');
            $this->SetXY($x + 119, 18);
            $this->Cell(30, 5, $serie."-".$nrorecibo, 0, 1, 'C');

            $h = 5;
            $al = 3;
            $this->Ln(8);
            $this->SetFont('Arial', '', 6);
            $this->Cell(30, $al, " ", 0, 0, 'C');
            $this->Cell(30, $al, " ", 0, 0, 'C');
            $this->Cell(30, $al, utf8_decode(" "), 0, 0, 'C');
            $this->Cell(30, $al, " ", 0, 0, 'C');
            $this->Cell(2.5, $al, utf8_decode(" "), 0, 0, 'C');
            $this->Cell(5.5, $al, " ", 0, 1, 'C');

            if (intval($nromeses) > 0) {

                if ($nromeses >= 2) {
                    $fechavencimiento = $fechavencimiento3;
                } else {
                    $fechavencimiento = $fvencimientodeudores;
                    $fechacorte = $fcorte;
                    $fv = $fvencimientodeudores;
                }
            } else {

                $fechavencimiento = $fvencimiento;
                $fechacorte = "";
                $fv = $fvencimiento;
            }

            $this->SetX($x + 2);
            $a = 18;
            $a1 = 30;
            $a2 = 25;
            $a3 = 20;
            $this->SetFont('Arial', '', 9);
            $this->Cell($a, $h, "Mensual", 0, 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell($a1, $h, $aniomes, 0, 0, 'C');
            $this->SetFont('Arial', '', 8.5);
            $this->Cell($a3, $h, $femision, 0, 0, 'C');
            $this->SetFont('Arial', 'B', 11);
            $this->Cell($a2, $h, $fechavencimiento, 0, 0, 'C');
            $this->SetFont('Arial', '', 8);
           // $Tar = substr($tarifa, 0, 3);
            //$CodCt = str_pad($unduso, 3, "00", STR_PAD_LEFT);
            $this->Cell($a3, $h, substr($tarifa,0,strlen($tarifa)-1), 0, 0, 'C');
            $this->SetFont('Arial', '', 8);
            $ruta = $this->getRutas($codsuc, $nroinscripion);
            $this->Cell($a1, $h, $ruta, 0, 1, 'C');
            $this->Ln(1);

            $this->Cell(10, $h, " ", 0, 0, 'C');
            $this->Cell(8, $h, utf8_decode(" "), 0, 1, 'C');

            $this->SetX($x);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(3, $h, " ", 0, 0, 'C');
            $this->Cell(75, $h, utf8_decode(strtoupper($propietario)), 0, 0, 'L');
            $this->Cell(8, $h, utf8_decode($direccion), 0, 1, 'L');
            $this->Ln(5);

            $this->Cell(4.5, 0.4, "", 0, 0, 'C');
            $this->Cell(4.5, 0.4, "", 0, 0, 'C');
            $this->Cell(2, 0.4, "", 0, 0, 'C');
            $this->Cell(3.5, 0.4, "", 0, 0, 'C');
            $this->Cell(3.5, 0.4, utf8_decode(""), 0, 1, 'C');

            if ($tipo_facturacion == 0) {
                $t = "L";
            }
            if ($tipo_facturacion == 1) {
                $t = "P";
            }
            if ($tipo_facturacion == 2) {
                $t = "A";
            }

            $this->SetX($x + 4);
            $this->SetFont('Arial', '', 9);
            $this->Cell(0.5, 0.5, " ", 0, 0, 'C');
            $this->Cell(30, 5, utf8_decode($zona), 0, 0, 'C');

            $this->Cell(40, 5, utf8_decode(''), 0, 0, 'C');
            //$Tar = substr($tarifa, 0, 3);
            //$CodCt = str_pad($unduso, 3, "00", STR_PAD_LEFT);
            $this->Cell(25, 5, $unduso, 0, 0, 'C');
            $this->Cell(35, 5, $fechalectanterior." - ".$fechalectultima, 0, 0, 'C');
            $this->Cell(25, 5, $t, 0, 1, 'C');
            $this->Ln(5);

            /**/
            $this->Cell(30, 5, "", 0, 0, 'C');
            $this->Cell(40, 5, "", 0, 0, 'C');
            $this->Cell(70, 5, "", 0, 1, 'C');
            //-----------> $y = $this->GetY();

            $this->Cell(30, 5, " ", 0, 0, 'C');
            $this->Cell(20, 5, " ", 0, 0, 'C');
            $this->Cell(20, 5, " ", 0, 1, 'C');

            /* TABLAS MVA */
            
            if ($vma == 1) {
                $this->SetFont('Arial', '', 8);
                $this->SetXY($x + 75, $y+68);
				
				$Tabla1= TablaParametros($codsuc, $nroinscripion, $CodUnidadUso);
				$ConUU = count($Tabla1);
					

                $ConDes = "SELECT d.nroinscripcion,d.codconcepto,c.descripcion,d.nrocredito,
                        sum(d.importe-(importerebajado+imppagado)) as monto, c.categoria
                        FROM facturacion.detfacturacion as d
                        INNER JOIN facturacion.conceptos as c on (d.codconcepto=c.codconcepto AND d.codemp=c.codemp AND d.codsuc=c.codsuc)
                        WHERE d.estadofacturacion = 1 AND d.nroinscripcion='".$nroinscripion."'
                        AND d.codsuc=".$codsuc." AND d.categoria=0 AND d.codconcepto=2
                        GROUP BY d.nroinscripcion,d.codconcepto,c.descripcion,d.nrocredito,c.categoria
                        ORDER BY d.codconcepto ";
                $res = $conexion->query($ConDes)->fetch();
                $monto = $res["monto"];
                
                //$this->SetXY($x+30,70);
                $this->Cell(70, 4, "Detalle Importe Facturado Desague ", 1, 1, 'C');
                $this->SetX($x + 75);
                $this->Cell(30, 4, "Total (".$ConUU." Unid. Uso) ", 1, 0, 'C');
                $this->Cell(20, 4, "100% ", 1, 0, 'C');
                $this->Cell(20, 4, number_format($monto, 2), 1, 1, 'R');                
                $cont_unidades  = 0;
                $tot_porcentaje = 0;
                
                $sql_unidades = "SELECT codsuc, nroinscripcion, codunidad_uso, descripcion, porcentaje, factor ";
                $sql_unidades .= "FROM vma.unidades_uso ";
                $sql_unidades .= "WHERE codsuc=".$codsuc." AND nroinscripcion= ".$nroinscripion;            
                $consulta_unidades = $conexion->query($sql_unidades);
                //$consulta_unidades->execute(array($codsuc, $nroinscripcion));
                $items_unidad = $consulta_unidades->fetchAll();            

                foreach($items_unidad as $rowUnidades)
                {                   
                    $cont_unidades++;                    
                    $tot_porcentaje += $rowUnidades["porcentaje"];
                    $porc= $rowUnidades["porcentaje"];
                    $ImpPorc= ($porc*$monto)/100;

                    $this->SetX($x + 75);
                    $this->Cell(30, 4, "Unid. Uso ".$rowUnidades['descripcion'], 1, 0, 'C');
                    $this->Cell(20, 4, number_format($rowUnidades["porcentaje"], 2), 1, 0, 'C');
                    $this->Cell(20, 4, number_format($ImpPorc,2), 1, 1, 'R');
                }
                
            }
            
            $lectultima = intval($lectultima);
            $lectanterior = intval($lectanterior);

            $this->SetXY($x + 4, $y+ 70);
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, 4, $nromed, 0, 0, 'C');
            $this->Cell(20, 4, $tipo_facturacion == 0 ? $lectultima."m".Cubico : "", 0, 0, 'C');
            $this->Cell(20, 4, $tipo_facturacion == 0 ? $lectanterior."m".Cubico : "", 0, 1, 'C');
            $this->Ln(2);

            if ($tipo_facturacion == 0) 
            {
                $t = "L";
                $ConsumoT = $consumo;
                $estadomedidor = utf8_decode($estadomedidor);
            }
            if ($tipo_facturacion == 1) 
            {
                $t = "P";
                $ConsumoT = intval($lecturapromedio);
                $estadomedidor='PROMEDIADO';
            }
            if ($tipo_facturacion == 2) 
            {
                $t = "A";
                $ConsumoT = $consumo;
                $estadomedidor='DIRECTO';
            }
            $this->Cell(30, 7, " ", 0, 0, 'C');
            $this->Cell(20, 7, " ", 0, 0, 'C');
            $this->Cell(20, 7, " ", 0, 1, 'C');
            //$consumofact = $ConsumoT;
            $this->SetX($x + 4);
            $this->Cell(30, 5, $estadomedidor, 0, 0, 'C');
            $this->Cell(20, 5, $ConsumoT != '' ? intval($ConsumoT)."m".Cubico : "", 0, 0, 'C');
            $this->Cell(20, 5, $consumofact != '' ? intval($consumofact)."m".Cubico : "", 0, 1, 'C');

            $this->Ln(9);
            $this->Cell(70, 5, " ", 0, 0, 'C');
            $this->Cell(70, 5, " ", 0, 1, 'C');

            //$y = $this->GetY();
            //$this->Rect($x + 3, $y + 4, 145, 57);
            /////////////////77
            $sqlDet = "SELECT d.codconcepto,d.concepto,c.categoria,c.ordenrecibo,
                    sum(d.importe) as monto
                    FROM facturacion.detfacturacion as d
                    INNER JOIN facturacion.conceptos as c on (d.codconcepto=c.codconcepto and d.codemp=c.codemp and d.codsuc=c.codsuc)
                    WHERE d.nroinscripcion=? and d.codsuc=? and d.nrofacturacion=?
                    GROUP BY d.codconcepto,d.concepto,c.categoria,c.ordenrecibo
                    ORDER BY c.ordenrecibo; ";
            $consultaD = $conexion->prepare($sqlDet);
            $consultaD->execute(array($nroinscripion, $codsuc, $nrofacturacion));
            $itemsD = $consultaD->fetchAll();
            $this->SetX($x + 81);
            $this->SetFont('Arial', '', 8);
            $ac = 40;
            $ac1 = 12;
            $ac2 = 5;
            $ac3 = 15;
            $an = 7;
            $interesMes = 0;
            $totalvma=0;//$vma
            
            foreach ($itemsD as $rowD) {

                if ($rowD["categoria"] != 3 && $rowD["categoria"] != 4 && $rowD["categoria"] != 7) {
                    $CodC = str_pad($rowD["codconcepto"], 5, "0", STR_PAD_LEFT);
                    $this->SetX($x + 75);
                    $this->Cell($ac1, 4, $CodC, 0, 0, 'L');
                    $this->Cell($ac, 4,utf8_decode(substr($rowD["concepto"],0,30).""), 0, 0, 'L');
                    $this->Cell($ac2, $an, "", 0, 0, 'C');
                    $this->Cell($ac3, 4, number_format($rowD["monto"], 2), 0, 1, 'R');
                }
                if ($rowD["categoria"] == 3) {
                    $interesMes += $rowD["monto"];
                }
                
                if ($rowD["categoria"] == 9) 
                {
                    $totalvma += $rowD["monto"];
                }
            }
            if ($interesMes > 0) {
                $this->SetX($x + 75);
                $this->Cell($ac1, 5, '', 0, 0, 'C');
                $this->Cell($ac, 5, "INTERESES", 0, 0, 'L');
                $this->Cell($ac2, $an, "", 0, 0, 'C');
                $this->Cell($ac3, 5, number_format($interesMes, 2), 0, 1, 'R');
            }
            //////////////////7
            /* $this->SetX($x + 80);
              $this->SetFont('Arial', '', 8);
              $ac = 35;
              $ac1 = 15;
              $ac2 = 5;
              $ac3 = 15;
              $an= 7;
              if ($importeagua > 0) {
              $this->SetX($x + 75);
              $this->Cell($ac1, 5, '00001', 0, 0, 'C');
              $this->Cell($ac, 5, "SERVICIO DE AGUA", 0, 0, 'L');
              $this->Cell($ac2, $an, "", 0, 0, 'C');
              $this->Cell($ac3, 5, number_format($importeagua, 2), 0, 1, 'R');
              }
              if ($importedesague > 0) {
              $this->SetX($x + 75);
              $this->Cell($ac1, 5, '00002', 0, 0, 'C');
              $this->Cell($ac, 5, "SERVICIO DESAGUE", 0, 0, 'L');
              $this->Cell($ac2, $an, "", 0, 0, 'C');
              $this->Cell($ac3, 5, number_format($importedesague, 2), 0, 1, 'R');
              }
              if ($importeintereses > 0) {
              $this->SetX($x + 75);
              $this->Cell($ac1, 5, '', 0, 0, 'C');
              $this->Cell($ac, 5, "INTERESES Y MORA", 0, 0, 'L');
              $this->Cell($ac2, $an, "", 0, 0, 'C');
              $this->Cell($ac3, 5, number_format($importeintereses, 2), 0, 1, 'R');
              }
              if ($importecargofijo > 0) {
              $this->SetX($x + 75);
              $this->Cell($ac1, 5, '00006', 0, 0, 'C');
              $this->Cell($ac, 5, "CARGO FIJO", 0, 0, 'L');
              $this->Cell($ac2, $an, "", 0, 0, 'C');
              $this->Cell($ac3, 5, number_format($importecargofijo, 2), 0, 1, 'R');
              }
              if ($importecredito > 0) {
              $this->SetX($x + 75);
              $this->Cell($ac1, 5, '', 0, 0, 'C');
              $this->Cell($ac, 5, "CUOTA DE CREDITO", 0, 0, 'L');
              $this->Cell($ac2, 5, "", 0, 0, 'C');
              $this->Cell($ac3, 5, number_format($importecredito, 2), 0, 1, 'R');
              }
              if ($importerefinanciamiento > 0) {
              $this->SetX($x + 75);
              $this->Cell($ac1, 5, '', 0, 0, 'C');
              $this->Cell($ac, 5, "CUOTA DE REFINANCIAMIENTO", 0, 0, 'L');
              $this->Cell($ac2, $an, "", 0, 0, 'C');
              $this->Cell($ac3, 5, number_format($importerefinanciamiento, 2), 0, 1, 'R');
              }
             */
            $this->SetFont('Arial', '', 10);

            $this->SetY(136);

            $subtotal = $importeagua + $importedesague + $importeintereses + $importecargofijo + $importecredito + $importerefinanciamiento +$totalvma+$impColateral;
            $w = 110;
            $this->SetX($x + 3);
            $this->Cell($w, 5, "SUBTOTAL", 0, 0, 'R');
            //$this->Cell(30, 5,"",0,0,'C');
            $this->Cell(35, 5, number_format($subtotal, 2), 0, 1, 'R');

            $this->SetX($x + 3);
            $this->Cell($w, 5, "Igv ".$valorigv."%", 0, 0, 'R');
            //$this->Cell(30, 5,"",0,0,'C');
            $this->Cell(35, 5, number_format($impmesigv, 2), 0, 1, 'R');

            $this->SetX($x + 3);
            $this->Cell($w, 5, "Redondeo", 0, 0, 'R');
            //$this->Cell(30, 5,"",0,0,'C');
            $this->Cell(35, 5, number_format($impmesredondeo, 2), 0, 1, 'R');

            if ($nromeses > 0) {
                $this->SetX($x + 3);
                $this->Cell($w, 5, "Deuda (".$nromeses.") Mes(es)", 0, 0, 'R');
                //$this->Cell(30, 5,"",0,0,'C');
                $this->Cell(35, 5, number_format($impdeuda, 2), 0, 1, 'R');
            }

            $totalfacturacion = $subtotal + $impmesigv + $impmesredondeo + $impdeuda;

            $this->SetFont('Arial', 'B', 14);
            $this->SetXY($x + 3, 158);

            $this->Cell(105, 5, "", 0, 0, 'R');
            //$this->Cell(30, 5,"",0,0,'C');
            $totalfacturacion = number_format($totalfacturacion, 2);
            $TotalF = str_pad($totalfacturacion, 10, "*", STR_PAD_LEFT);
            $this->Cell(40, 5, $TotalF, 0, 1, 'R');

            $this->Ln(6);
            $this->SetFont('Arial', 'B', 10);
            
            if($vma!= 1)
                {
                /**/
                if ($nromeses > 0) {
                    if ($x != 5) {
                        $this->Image("../../../../../images/logo_corte.jpg", $x + 15, 115, 20, 10);
                        $this->Image("../../../../../images/logo_triste.jpg", $x + 35, 115, 20, 20);
                    } else {
                        $this->Image("../../../../../images/logo_corte.jpg", 15, 115, 20, 10);
                        $this->Image("../../../../../images/logo_triste.jpg", 35, 115, 20, 20);
                    }
                    if ($nromeses >= 2) {
                        $fechavencimiento = "FECHA DE VENCIMIENTO : VENCIDA";
                        $fechacorte = "FECHA DE CORTE : VENCIDA";
                        $fv = 'VENCIDA';
                    } else {
                        $fechavencimiento = "FECHA DE VENCIMIENTO : ".$fvencimientodeudores;
                        $fechacorte = "FECHA DE CORTE : ".$fcorte;
                        $fv = $fvencimientodeudores;
                    }
                } else {
                    $fechavencimiento = "FECHA DE VENCIMIENTO : ".$fvencimiento;
                    $fechacorte = "";
                    $fv = $fvencimiento;

                    if ($x != 5) {
                        $this->Image("../../../../../images/logo_feliz.jpg", $x + 35, 125, 20, 20);
                    } else {
                        $this->Image("../../../../../images/logo_feliz.jpg", 35, 125, 20, 20);
                    }
                }            
                /**/
            }else
                {
                    $ub=120;
                    $this->SetXY($x + 1, $y + $ub);
                    
                    $CodUnidadUso= 0;
                    //$Tabla1= TablaParametros($codsuc, $nroinscripion, $CodUnidadUso);
					//$ConUU = count($Tabla1);

                    $this->SetFont('Arial', 'B', 6);
                    //$this->SetY(12);
                    $this->Cell(24, 4, "Parametros de Augas", 'LTR', 1, 'C');
                    $this->SetX($x+1);
                    $this->Cell(24, 4, "Residuales", 'LBR', 0, 'C');
                    $this->SetXY($x+25, $y+ $ub);
                    $this->Cell(10, 8, "DBO5", 1, 0, 'C');
                    $this->Cell(10, 8, "DQO", 1, 0, 'C');
                    $this->Cell(10, 8, "SST", 1, 0, 'C');
                    $this->Cell(7, 8, "AyG", 1, 0, 'C');
                    $this->SetXY($x+62,$y+ $ub);
                    $this->Cell(9, 8, "Factor", 'LTR', 1, 'C');
                    
                    $this->SetX($x+1);
                    $this->Cell(24, 4, "VMA (mg/L)", 1, 0, 'L');                    
                    $this->Cell(10, 4, "500", 1, 0, 'C');
                    $this->Cell(10, 4, "1,000", 1, 0, 'C');
                    $this->Cell(10, 4, "500", 1, 0, 'C');
                    $this->Cell(7, 4, "100", 1, 0, 'C');
                    $this->SetXY($x+62,$y+ $ub+8);
                    $this->Cell(9, 4, "Total", 'LBR', 1, 'C');

                    $this->SetFont('Arial', '', 6);
                    for ($i=0; $i < $ConUU; $i++)
                    {   
                        $this->SetX($x+1);
                        $ii= $Tabla1[$i+1][4][1][2];
                        $ii1= $Tabla1[$i+1][4][2][2];
                        $ii2= $Tabla1[$i+1][4][3][2];
                        $ii3= $Tabla1[$i+1][4][4][2];
                        $timp= $ii + $ii1 + $ii2 + $ii3;
						
						$UnidadDes = $Tabla1[$i+1][1];
						if ($ConUU==1)
						{
							$UnidadDes = '';
						}
						
                        $this->Cell(24, 4, utf8_decode("Valor de Análisis ").$UnidadDes, 1, 0, 'L');
                        $this->Cell(10, 4, number_format($Tabla1[$i+1][4][1][1], 1), 1, 0, 'C');
                        $this->Cell(10, 4, number_format($Tabla1[$i+1][4][2][1], 1), 1, 0, 'C');
                        $this->Cell(10, 4, number_format($Tabla1[$i+1][4][3][1], 1), 1, 0, 'C');
                        $this->Cell(7, 4, number_format($Tabla1[$i+1][4][4][1], 1), 1, 0, 'C');
                        $this->Cell(9, 4, "", 'LTR', 1, 'C');
                        
                        $this->SetX($x+1);
                        $this->Cell(24, 4, utf8_decode("Factores Individuales "), 1, 0, 'L');
                        $this->Cell(10, 4, number_format($Tabla1[$i+1][4][1][2], 0)." %", 1, 0, 'C');
                        $this->Cell(10, 4, number_format($Tabla1[$i+1][4][2][2], 0)." %", 1, 0, 'C');
                        $this->Cell(10, 4, number_format($Tabla1[$i+1][4][3][2], 0)." %", 1, 0, 'C');
                        $this->Cell(7, 4, number_format($Tabla1[$i+1][4][4][2], 0)." %", 1, 0, 'C');
                        $this->Cell(9, 4, $timp." %", 'LBR', 1, 'C');
                    }
                }
            
            $this->SetFont('Arial', 'B', 7);
            $this->SetXY($x + 3, 160);
            $NunLetras = CantidadEnLetra($totalfacturacion);
            if ($this->GetStringWidth($NunLetras) > 100) {
                $this->SetFont('Arial', 'B', 12);
                $this->MultiCell(72, 4, $NunLetras." NUEVOS SOLES", 0, 'J');
            } else
                $this->MultiCell(72, 4, $NunLetras." NUEVOS SOLES", 0, 'J');


            $this->SetFont('Arial', 'B', 11);
            $this->SetXY($x + 3, 175);
            $this->MultiCell(145, 5, utf8_decode(strtoupper($mensaje)), 0, 'L');
            $this->Ln(2);
            $this->SetX($x + 3);
            $this->SetFont('Arial', '', 10);
            $this->Cell(100, 5, "Horas de Suministro: ".trim(utf8_decode($horasabastecimiento)), 0, 1, 'L');
            $this->SetFont('Arial', '', 8.5);
            $this->SetXY($x + 80, 200);
            $this->Cell(100, 5, trim(utf8_decode($propietario)), 0, 1, 'L');

            $this->Ln(2);
            //$this->SetY(160);
            $this->SetFont('Arial', '', 6);
            $this->SetXY($x + 8.5, 222);
            $this->Cell(3.6, 0.4, "", 0, 0, 'C');
            $this->Cell(3.6, 0.4, utf8_decode(""), 0, 0, 'C');
            $this->Cell(3.6, 0.4, "", 0, 0, 'C');
            $this->Cell(3.6, 0.4, utf8_decode(""), 0, 0, 'C');
            $this->Cell(3.6, 0.4, utf8_decode(""), 0, 1, 'C');

            if (intval($nromeses) > 0) {
                if ($nromeses >= 2) {
                    $fechavencimiento = $fechavencimiento3;
                } else {
                    $fechavencimiento = $fvencimientodeudores;
                    $fechacorte = $fcorte;
                    $fv = $fvencimientodeudores;
                }
            } else {

                $fechavencimiento = $fvencimiento;
                $fechacorte = "";
                $fv = $fvencimiento;
            }

            $this->SetX($x + 4);
            $this->SetFont('Arial', '', 9);
            $this->Cell(30, 0.4, $aniomes, 0, 0, 'C');
            $this->Cell(30, 0.4, $femision, 0, 0, 'C');
            $this->Cell(25, 0.4, $fechavencimiento, 0, 0, 'C');
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(35, 0.4, $serie."-".$nrorecibo, 0, 0, 'C');
            $this->SetFont('Arial', 'B', 18);
            $this->Cell(35, 0.4, $TotalF, 0, 1, 'C');
            $this->Ln(4);


            $this->SetFont('Arial', '', 8);
            /* $this->Cell(3.6, 0.4, utf8_decode("INSCRIPCIÓN"), 0, 0, 'C');
              $this->Cell(3.6, 0.4, utf8_decode("CÓDIGO"), 0, 1, 'C'); */
            $this->Cell(3.4, 3, utf8_decode(""), 0, 0, 'C');
            $this->Cell(3.8, 3, utf8_decode(""), 0, 1, 'C');
            $this->SetFont('Arial', '', 8.5);
            //$this->Cell(3.4, 0.4, $nroinscripion." - ".$codantiguo, 0, 0, 'C');
            $this->SetX($x + 4);
            $this->SetFont('Arial', 'B', 14);
            $this->Cell(30, 5, $codantiguo, 0, 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell(30, 5, $catastro, 0, 1, 'C');
            //$this->Ln(0.2);
            $this->SetXY($x + 25, 238);
            $recibon = $serie."-".str_pad($nrorecibo, 8, "0", STR_PAD_LEFT);
            $this->SetFont('Arial', 'B', 18);
            $this->Cell(60, 0.5, "*".$recibon."*", 0, 1, 'C');
        }

    }
	
	$mesesconsumoA = array();
	$mesesconsumoB = array();
	
	$mesesliteralA = array();
	$mesesliteralB = array();
	define('Cubico', chr(179));
	$codciclo 		= $_POST["codciclo"];
	$codsuc			= $_POST["codsuc"];
	$nrofacturacion	= $_POST["nrofacturacion"];
	
	$nrofacturacion = ($nrofacturacion-1);
	
	$sucursal 	= substr("000".$codsuc, strlen("000".$codsuc) - 3, 3);
	$ciclo		= substr("000".$codciclo, strlen("000".$codciclo) - 3, 3);
	
	$objreporte	= new clsImpresion();
	
	$facturacion = $objreporte->datosfacturacion($codsuc,$codciclo);
	$paramae	 = $objreporte->getParamae("IMPIGV",$codsuc);
	
	$nrofacturacion = ($facturacion["nrofacturacion"] - 1);
	
	$sql = "SELECT imp.* FROM facturacion.impresion_recibos imp ";
	$sql .= "INNER JOIN catastro.clientes as clie ON(imp.codemp=clie.codemp AND imp.codsuc=clie.codsuc AND imp.nroinscripcion=clie.nroinscripcion) ";
	$sql .= "INNER JOIN catastro.conexiones as cone ON (cone.codemp=clie.codemp AND cone.codsuc=clie.codsuc AND cone.nroinscripcion=clie.nroinscripcion) ";
    $sql .= "WHERE imp.codsuc=".$codsuc." and imp.codciclo=".$codciclo."
     and imp.nrofacturacion=".$nrofacturacion." AND trim(clie.correo)<>'' AND clie.correo is not null

      ".$sqls." ".$order." ";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":codsuc"=>$codsuc,
							 ":codciclo"=>$codciclo,
							 ":nrofacturacion"=>$nrofacturacion	));
	$items = $consulta->fetchAll();
	
	foreach($items as $row)
	{
		$mesesconsumoA = array();
        $mesesconsumoB = array();

        $mesesliteralA = array();
        $mesesliteralB = array();
        $mesesconsumoA = explode("|", substr($row["consumografico"], 0, strlen($row["consumografico"]) - 1));
        $mesesliteralA = explode("|", substr($row["mesesgrafico"], 0, strlen($row["mesesgrafico"]) - 1));

        $count = 0;
        for ($i = 0; $i < count($mesesconsumoA); $i++) {
            $mesesconsumoB[$count] = $mesesconsumoA[$i];
            $mesesliteralB[$count] = $meses[$mesesliteralA[$i]];

            $count++;
        }

        
		//////
	
		$totmes = $row["impagua"] + $row["impdesague"] + $row["impinteres"] + $row["impcargofijo"] + $row["impcredito"] + $row["imprefinanciamiento"];
		if($totmes!=0)
		{
			$aniomes = substr($meses[$row["mes"]],0,3)."-".$row["anio"];
		
		
			//$objreporte		= new clsImpresion();
			$objreporte = new clsImpresion('L', 'mm', array(260, 170));
			$archivo = "../recibos/".$sucursal.$ciclo.$row["nroinscripcion"]."-".$row["mes"].$row["anio"].".pdf";
			if(file_exists($archivo)){unlink($archivo);}
			$objreporte->AddPage();
			$objreporte->Contenido(3, 2, $row["nroinscripcion"], $row["codcatastro"], $aniomes, $row["nrorecibo"], $objreporte->DecFecha($row["fechaemision"]), $row["propietario"], $row["direccion"], $row["servicios"], $row["tipofacturacion"], $row["unidades"], $row["tarifa"], $row["nromedidor"], $objreporte->DecFecha($row["fechalecturaanterior"]), $row["lecturaanterior"], $row["consumom3"], $objreporte->DecFecha($row["fechalecturaultima"]), $row["lecturaultima"], $row["impagua"], $row["impdesague"], $row["impinteres"], $row["impcargofijo"], $row["impcredito"], $row["imprefinanciamiento"], $row["nromeses"], $row["impdeuda"], $paramae["valor"], $row["impigv"], $row["impredondeo"], $row["mensajegeneral"], $objreporte->DecFecha($row["fechavencimientonormal"]), $objreporte->DecFecha($row["fechavencimientodeudores"]), $objreporte->DecFecha($row["fechacorte"]), $mesesconsumoB, $mesesliteralB, $row["mes"], $row["docidentidad"], $row["nrodocidentidad"], $row['mensajenormal'], $row['codantiguo'], $row['lecturapromedio'], $codsuc, $row['horasabastecimiento'], $nrofacturacion,$row['impcolateral'],$objreporte->DecFecha($row['fechavencimiento3']),$row['estadomedidor'],$row['consumofact']);
		   $objreporte->Output($archivo,'F');	
			
		}
	}
	

	$img 		 = "<img src='".$urldir."images/Ok.png' width='31' height='31' />";
	$mensaje 	 = "Los Recibos se han Generado Correctamente";
	$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
			
	echo $img."|".$mensaje."|".$res;
	
?>