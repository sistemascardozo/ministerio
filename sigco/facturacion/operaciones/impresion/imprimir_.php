<?php
date_default_timezone_set("America/Lima");

session_name("pnsu");
if (!session_start()) {session_start();}

global $Sede;

include($_SESSION['path']."/objetos/clsReporte.php");
include($_SESSION['path']."/admin/clases/num2letra.php");
include($_SESSION['path']."/vma/include/funciones.php");
//    include($_SESSION['path']."/include/Image/Barcode.php");
include("../../../../include/Image/Barcode.php");

ini_set("memory_limit", "512M");

class clsImpresion extends clsReporte {

    function Header() {

    }

    function Contenido($x, $y, $ruc, $nroinscripion, $catastro, $aniomes, $nrorecibo, $femision, $propietario, $direccion, $servicio, $tipo_facturacion, $unduso, $tarifa, $cantidad_tarifa, $nromed, $fechalectanterior, $lectanterior, $consumo, $fechalectultima, $lectultima, $importeagua, $importedesague, $importeintereses, $importecargofijo, $importecredito, $importerefinanciamiento, $nromeses, $impdeuda, $valorigv, $impmesigv, $impmesredondeo, $mensaje, $fvencimiento, $fvencimientodeudores, $fcorte, $consumoGraf, $mesesGraf, $mes, $docidentidad, $nrodocidentidad, $mensajenormal, $codantiguo, $lecturapromedio, $codsuc, $horasabastecimiento, $nrofacturacion, $impColateral, $fechavencimiento3, $estadomedidor, $consumofact, $zona, $urbanizacion, $codtiposervicio, $tiposervicio, $catetar, $MensajeDeudas) {

        global $schema, $tfacturacion, $meses, $conexion;

        $fechavencimiento3 = 'VENCIDA';

        $serie = '001';

        //ECHO $nroinscripion;
        $verVma = "SELECT vma FROM catastro.clientes WHERE nroinscripcion = ? AND codsuc = ? ";

        $consulta = $conexion->prepare($verVma);
        $consulta->execute(array($nroinscripion, $codsuc));
        $itemsD = $consulta->fetch();
        $vma = $itemsD[0];

        $grafico = new Graph();
        $grafico = Image_Barcode::draw($codantiguo, 'code128', 'jpg', false);
        imagejpeg($grafico, "barras/".$nroinscripion.'.jpg');

        $AlturaBarra = 228;

        if ($x != 0) {

            $this->Image("barras/".$nroinscripion.'.jpg', 190, $AlturaBarra, 140, 15);
            $this->SetX($x);
            $this->SetY($y);
        } else {
            $this->Image("barras/".$nroinscripion.'.jpg', 10, $AlturaBarra, 140, 15);
        }

        $this->SetFont('Arial', '', 10);

        if ($vma == 0) {

            if ($tipo_facturacion == 0) {
                $img = $this->generargraficos1($consumoGraf, $mesesGraf, $nroinscripion);
                if ($x != 0) {
                    $this->Image($img, 280, 30, 60, 34);
                } else {
                    $this->Image($img, 100, 30, 60, 34);
                }
            }
        }

        $this->SetFont('Arial', '', 11);

        $y = 3;
        $x = (empty($x)) ? $x+5 : $x+2;
        $this->SetXY($x, $y);

        $this->Cell(20, 6, utf8_decode("Código"), 0, 0, 'L');
        $this->SetFont('Arial', '', 9);
        $xt1 = 35;
        $al = 'C';

        if ($x != 0): $al = 'R';
        endif;
        $this->SetFont('Arial', 'B', 14);
        $this->Cell(56, 6, $catastro, 0, 1, 'R');

		$DeudaMayor3 = '';

        if (intval($nromeses) > 0)
		{
            if ($nromeses >= 2)
			{
                $fechavencimiento = $fechavencimiento3;

				$DeudaMayor3 = 'VENCIDO';
            }
			else
			{
                $fechavencimiento = $fvencimientodeudores;
                $fechacorte = $fcorte;
                $fv = $fvencimientodeudores;
            }
		}
        else
		{
            $fechavencimiento = $fvencimiento;
            $fechacorte = "";
            $fv = $fvencimiento;
		}

        $this->SetXY($x, $y+6);
        $this->SetFont('Arial', '', 11);
        $this->Cell(20, 6, utf8_decode("Inscripción"), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 14);
        $this->Cell(56, 6, $codantiguo, 0, 1, 'R');

        $this->Ln(2);
        $h = 0;
        $al = 3;
        $this->SetFont('Arial', '', 6);
        $this->Cell(30, $al, " ", 0, 0, 'C');
        $this->Cell(30, $al, " ", 0, 0, 'C');
        $this->Cell(30, $al, utf8_decode(" "), 0, 0, 'C');
        $this->Cell(30, $al, " ", 0, 0, 'C');
        $this->Cell(2.5, $al, utf8_decode(" "), 0, 0, 'C');
        $this->Cell(5.5, $al, " ", 0, 0, 'C');

        $this->SetFont('Arial', 'B', 9);
        $this->Cell(3, $h+1, "", 0, 0, 'C');

        $xt2 = $x;
        if ($x != 0): $xt2 = $x;
        endif;
        $this->SetX($xt2);
        //$this->SetFont('Arial', 'B', 10);
        //$this->Cell(25, $h+5, "Nombre", 0, 0, 'L');

        $this->SetFont('Arial', 'B', 12);
        //$this->SetX($xt2+30);
        $this->Cell(75, $h + 5, strtoupper(utf8_decode($propietario)), 0, 1, 'L');
        $this->SetX($xt2);
        //$this->SetFont('Arial', 'B', 10);
        //$this->Cell(25, $h+5, "Direccion", 0, 0, 'L');

        $this->SetFont('Arial', '', 10);
        //$this->SetX($xt2+30);
        $this->Cell(75, $h+5, utf8_decode($direccion), 0, 1, 'L');

        $this->SetX($xt2);
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(45, $h+5, $urbanizacion, 0, 0, 'L'); // URB URBANA

        $this->SetX($xt2+60);
        $this->Cell(16, $h+5, "U. USO ".$cantidad_tarifa, 0, 1, 'L');
        $this->Ln(1);

        $xt = $x;
        $al = 'C';

        if ($x == 0)
		{
            $xt = -2;
            $al = 'L';
        };

        $this->SetX($xt+2);

        $a = 18;
        $a1 = 30;
        $a2 = 25;
        $a3 = 22;
        $a4 = 20;

        //$xtruc = $x;
        $xtruc = $this->GetX();
        $xtruc = (empty($xtruc)) ? $xtruc : $xtruc - 2;

        $DocId = "D.N.I.";

        if (strlen(trim($nrodocidentidad)) > 8)
		{
            $DocId = "R.U.C.";
        }

        $this->Ln(4);

        $this->SetX($xtruc);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(9, $h+3, $DocId, 0, 0, 'L');

        $this->SetFont('Arial', '', 9);
        $this->Cell($a3, $h+3, $nrodocidentidad, 0, 0, $al);

        $this->SetX($xt2+$a3+15);
        $this->SetFont('Arial', '', 10);
        $this->Cell(10, $h+3, "Ruta", 0, 0, 'L');

        $ruta = $this->getRutas($codsuc, $nroinscripion);
        $this->Cell($a4, $h+3, $ruta, 0, 0, 'L');

        /*
          $this->SetX($xt2+$a3+$a4+30);
          $this->SetFont('Arial', '', 10);
          $this->Cell(12, $h+3, "Tarifa", 0, 0, 'L');

          $this->SetFont('Arial', 'B', 10);
          $this->Cell($a3, $h+3, substr($tarifa,0,strlen($tarifa)-1), 0, 1, 'L');

         */

        $this->SetXY($x+5, $y+40);
        $this->Cell(30, 6, "Periodo/Ciclo", 0, 0, 'C');

        $this->SetXY($x+45, $y+40);
        $this->SetFont('Arial', '', 10);
        $this->Cell(30, 6, utf8_decode("Número de Recibo"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetXY($x+5, $y+45);
        $this->Cell(30, 6, $aniomes, 0, 0, 'C');

        $this->SetXY($x+45, $y+45);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(30, 6, $serie."-".str_pad($nrorecibo, 8, '0', STR_PAD_LEFT), 0, 1, 'C');
        $this->Ln(1);

        $xtruc = (empty($xtruc)) ? $xtruc : $xtruc - 2;

        $this->SetX($xtruc+2);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(43, 5.5, "SERVICIO PRESTADO", 0, 0, 'L');
        $this->SetFont('Arial', '', 8);
        $this->Cell(46, 6, utf8_decode(strtoupper($tiposervicio)), 0, 1, 'C');

        //$this->Ln(1.5);
		if ($tipo_facturacion == 0) {
            $t = "L";
            $ConsumoT = $consumo;
            $estadomedidor = utf8_decode($estadomedidor);
        }
        if ($tipo_facturacion == 1) {
            $t = "P";
            $ConsumoT = intval($lecturapromedio);
            $estadomedidor = 'PROMEDIADO';
        }
        if ($tipo_facturacion == 2) {
            $t = "A";
            $ConsumoT = $consumo;
            $estadomedidor = 'DIRECTO';
            $consumofact = $ConsumoT;
        }

        $this->SetX($xt2);
        $this->SetFont('Arial', '', 10);
        $this->Cell(40, $h+5, utf8_decode("N° de Medidor"), 0, 0, 'L');
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(40, $h+5, $nromed, 0, 1, 'R');

        $this->SetX($xt2);
        $this->SetFont('Arial', '', 9);
        $this->Cell(35, $h+5, utf8_decode("Categoria Tarifaria"), 0, 0, 'L');
        $this->Cell(45, $h+5, substr($tarifa, 0, strlen($tarifa) - 1)." (".$t.")", 0, 0, 'R');

        $this->Cell(1, $h+5, "", 0, 0, 'C');
        $this->Cell(17, $h+5, utf8_decode(""), 0, 0, 'C');
        $this->Cell(20, $h+5, utf8_decode("LECTURAS"), 0, 0, 'C');
        $this->Cell(18, $h+5, utf8_decode("Cod"), 0, 0, 'C');
        $this->Cell(22, $h+5, utf8_decode("Periodo"), 0, 1, 'C');

        $lectultima = intval($lectultima);
        $lectanterior = intval($lectanterior);
        $diferido = $lectultima - $lectanterior;

        $sql = "SELECT ta.* FROM facturacion.tarifas ta ";
		$sql .= "WHERE ta.codemp = 1 ";
		$sql .= " AND ta.codsuc = ".$codsuc." ";
		$sql .= " AND ta.catetar = ".$catetar." ";
		$sql .= " AND ta.estado = 1";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetch();

        $costo = '';

        switch ($codtiposervicio) :

            //Agua y Desague
            case 1:

                $costo = number_format($items['impconsmin'], 4)."(0-".number_format($items['hastarango1'], 0)."m3)";
                $items['impconsexec'] = (float) $items['impconsexec'];
                $costo .= (empty($items['impconsexec'])) ? "-".number_format($items['impconsmedio'], 4)."(".((int) $items['hastarango1']+1)."- a mas)" : "-".number_format($items['impconsmedio'], 4)."(".((int) $items['hastarango1']+1)."-".number_format($items['hastarango2'], 0).") -".number_format($items['impconsexec'], 4)."(".((int) $items['hastarango3'])."- a mas)";

                break;

            // Agua
            case 2:
                $costo = number_format($items['impconsmin'], 4)."(0-".number_format($items['hastarango1'], 0)."m3)";
                $items['impconsexec'] = (float) $items['impconsexec'];
                $costo .= (empty($items['impconsexec'])) ? "-".number_format($items['impconsmedio'], 4)."(".((int) $items['hastarango1']+1)."- a mas)" : "-".number_format($items['impconsmedio'], 4)."(".((int) $items['hastarango1']+1)."-".number_format($items['hastarango2'], 0).") -".number_format($items['impconsexec'], 4)."(".((int) $items['hastarango3'])."- a mas)";
                break;

            // Desague
            case 3:
                $costo = number_format($items['impconsmindesa'], 4)."(0-".number_format($items['hastarango1'], 0)."m3)";
                $items['impconsexecdesa'] = (float) $items['impconsexecdesa'];
                $costo .= (empty($items['impconsexecdesa'])) ? "-".number_format($items['impconsmediodesa'], 4)."(".((int) $items['hastarango1']+1)."- a mas)" : "-".number_format($items['impconsmediodesa'], 4)."(".((int) $items['hastarango1']+1)."-".number_format($items['hastarango2'], 0).") -".number_format($items['impconsexecdesa'], 4)."(".((int) $items['hastarango3'])."- a mas)";
                break;

        endswitch;

        $this->SetFont('Arial', '', 8);
        $this->SetX($xt2);
        $this->Cell(80, $h+5, utf8_decode("COSTO X m3 ".utf8_decode($costo)), 0, 0, 'L');

        $this->Cell(1, $h+5, "", 0, 0, 'C');
        $this->Cell(17, $h+5, utf8_decode("Actual"), 0, 0, 'l');
        $this->Cell(20, $h+5, utf8_decode($lectultima), 0, 0, 'C');
        $this->Cell(18, $h+5, utf8_decode(""), 0, 0, 'C');
        $this->Cell(22, $h+5, utf8_decode($fechalectultima), 0, 1, 'C');

        $this->SetX($xt2);
        $this->Cell(80, $h+5, utf8_decode("HORARIO SUMINISTRO ".$horasabastecimiento." Horas"), 0, 0, 'L');


        $this->Cell(1, $h+5, "", 0, 0, 'C');
        $this->Cell(17, $h+5, utf8_decode("Anterior"), 0, 0, 'l');
        $this->Cell(20, $h+5, utf8_decode($lectanterior), 0, 0, 'C');
        $this->Cell(18, $h+5, utf8_decode(""), 0, 0, 'C');
        $this->Cell(22, $h+5, utf8_decode($fechalectanterior), 0, 1, 'C');

        $sql = "SELECT nrocontrato FROM catastro.clientes ";
        $sql .= "WHERE codsuc = ".$codsuc." ";
        $sql .= " AND nroinscripcion = ".$nroinscripion." ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetch();

        $this->SetX($xt2);
        $this->Cell(80, $h+5, utf8_decode("CONTRATO N° ").$items[0], 0, 0, 'L');


        $this->Cell(1, $h+5, "", 0, 0, 'C');
        $this->Cell(17, $h+5, utf8_decode("Consumo"), 0, 0, 'L');
        $this->Cell(20, $h+5, utf8_decode((int) $consumo." m3"), 0, 0, 'C');
        $this->Cell(18, $h+5, utf8_decode(""), 0, 0, 'C');
        $this->Cell(22, $h+5, utf8_decode(""), 0, 1, 'C');

        $this->Ln(4);

        $xt1 = 5;

        if ($x != 0): $xt1 = 2;
        endif;

        $this->SetX($x+$xt1);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(90, 5, "DESCRIPCION DE CONCEPTOS", 0, 0, 'C');
        $this->Cell(42, 5, "NO IMPONIBLE", 0, 0, 'R');
        $this->Cell(20, 5, "IMPORTE", 0, 1, 'R');

        if ($tipo_facturacion == 0) {
            $t = "L";
            $ConsumoT = $consumo;
            $estadomedidor = utf8_decode($estadomedidor);
        }
        if ($tipo_facturacion == 1) {
            $t = "P";
            $ConsumoT = intval($lecturapromedio);
            $estadomedidor = 'PROMEDIADO';
        }
        if ($tipo_facturacion == 2) {
            $t = "A";
            $ConsumoT = $consumo;
            $estadomedidor = 'DIRECTO';
            $consumofact = $ConsumoT;
        }

        //$this->Cell(70, 5, " ", 0, 0, 'C');
        $this->Cell(70, 5, " ", 0, 1, 'C');

        $sqlDet = "SELECT d.codconcepto, d.concepto, c.categoria, c.ordenrecibo, d.nrocredito, d.nrorefinanciamiento, ";
        $sqlDet .= " SUM(d.importe) AS monto, d.codtipodeuda, c.afecto_igv ";
        $sqlDet .= "FROM facturacion.detfacturacion d ";
        $sqlDet .= " INNER JOIN facturacion.conceptos c ON(d.codconcepto = c.codconcepto) AND (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) ";
        $sqlDet .= "WHERE d.codsuc = ".$codsuc." ";
        $sqlDet .= " AND d.nroinscripcion = ".$nroinscripion." ";
        $sqlDet .= " AND d.nrofacturacion = ".$nrofacturacion." ";
        $sqlDet .= "GROUP BY d.codconcepto, d.concepto, c.categoria, c.ordenrecibo, d.nrocredito, d.nrorefinanciamiento, d.codtipodeuda, c.afecto_igv ";
        $sqlDet .= "ORDER BY d.codconcepto, c.ordenrecibo; ";

        $consultaD = $conexion->query($sqlDet);

        $itemsD = $consultaD->fetchAll();

        $this->SetX($x + 81);
        $this->SetFont('Arial', '', 8);

        $ac = 110;
        $ac1 = 12;
        $ac2 = 5;
        $ac3 = 19;
        $an = 7;
        $interesMes = 0;
        $totalvma = 0; //$vma
        $subtotal = 0;
        $xt = 15;

        $Cuotas = 0;

        if ($x != 0){
            $xt = 12;
        }

        foreach ($itemsD as $rowD){

          if ($rowD["categoria"] != 3 && $rowD["categoria"] != 4 && $rowD["categoria"] != 7){

            $CodC = str_pad($rowD["codconcepto"], 5, "0", STR_PAD_LEFT);
            $this->SetX($x + $xt);
            $this->Cell($ac1, 4, $CodC, 0, 0, 'L');

            if (($rowD["nrocredito"] <> 0 || $rowD["nrorefinanciamiento"] <> 0) && $rowD["codtipodeuda"] != 9){
              $this->Cell($ac - 20, 4, utf8_decode(substr($rowD["concepto"], 0, 30).""), 0, 0, 'L');
              $Cuotas += $rowD["monto"];
            }else{
              $this->Cell($ac, 4, utf8_decode(substr($rowD["concepto"], 0, 30).""), 0, 0, 'L');
              $subtotal += $rowD["monto"];
            }

            // if ($rowD["categoria"] == 10 && $rowD["afecto_igv"] == 0){
            //   $this->SetY($this->GetY() - 20);
            // }

            // Cambios por el mes unico de Enero
            // Se debe modificar para el siguiente mes de Febrero

            if($rowD["codconcepto"]  == 1 AND $nromeses > 0){
              $this->Cell($ac3, 4, number_format($rowD["monto"], 2), 0, 1, 'R');
              $this->Ln();
              $eny = $this->GetY();
            }else{
              $this->Cell($ac3, 4, number_format($rowD["monto"], 2), 0, 1, 'R');
            }

          }

          if ($rowD["categoria"] == 3){
            $interesMes += $rowD["monto"];
          }
          if ($rowD["categoria"] == 9){
            $totalvma += $rowD["monto"];
          }

        }

        if ($interesMes > 0)
		    {
            $this->SetX($x + $xt);

            $this->Cell($ac1, 5, '', 0, 0, 'C');
            $this->Cell($ac - 20, 5, "MORA(S)", 0, 0, 'L');
            $this->Cell($ac3, 5, number_format($interesMes, 2), 0, 1, 'R');
        }

        if ($nromeses > 0)
		    {
            $this->SetXY($x + $xt + 12, $eny-5);
            $this->Cell($ac - 20, 5, "Mes (es) pendientes de Deuda (".$nromeses.")", 0, 0, 'L');
            $this->Cell($ac3, 5, number_format($impdeuda, 2), 0, 1, 'R');
        }

        $this->SetFont('Arial', '', 10);

        //$subtotal += $interesMes;

        $this->SetY(140);
        $w = 105;

        if ($x != 0) {
            $w = 102;
        }

        $this->SetX($x+3);
        $this->Cell($w, 5, "SUBTOTAL", 0, 0, 'R');

        if ($nromeses > 0 or $interesMes > 0 or $Cuotas > 0 or $impdeuda > 0)
		{
            $this->Cell(30, 5, number_format($impdeuda + $interesMes + $Cuotas, 2)." +", 0, 0, 'R');
		}
		else
		{
            $this->Cell(30, 5, "", 0, 0, 'R');
		}

        $this->Cell(16, 5, number_format($subtotal, 2), 0, 1, 'R');

        $this->SetX($x+3);
        $this->Cell($w, 5, "Igv ".$valorigv."%", 0, 0, 'R');
        $this->Cell(46, 5, number_format($impmesigv, 2), 0, 1, 'R');

        $this->SetX($x+3);
        $this->Cell($w, 5, "Redondeo", 0, 0, 'R');
        $this->Cell(46, 5, number_format($impmesredondeo, 2), 0, 1, 'R');

        // if ($nromeses > 0) {
        //     $this->SetX($x+3);
        //     $this->Cell($w, 5, "Numero de Mes (es) de Deuda (".$nromeses.")", 0, 0, 'R');
        //     $this->Cell(35, 5, number_format($impdeuda, 2), 0, 1, 'R');
        // }

        $totalfacturacion = $subtotal+$impmesigv+$impmesredondeo+$impdeuda+$interesMes+$Cuotas;

        $this->SetFont('Arial', 'B', 12);
        $xt = 5;

        if ($x != 0) {
            $xt = 2;
        }

        $this->SetXY($x+$xt, 160);

        $this->Cell(115, 5, "Total a Pagar", 0, 0, 'R');
        $totalfacturacion = number_format($totalfacturacion, 2);
        $TotalF = str_pad($totalfacturacion, 8, "*", STR_PAD_LEFT);
        $TotalF = "S./ ".$TotalF;
        $this->SetFont('Arial', 'B', 16);
        $this->SetFillColor(195, 195, 195);
        $this->Cell(35, 5, $TotalF, 0, 1, 'R', true);

        $this->SetFont('Arial', 'B', 10);

        if ($vma != 1) {
            if ($nromeses > 0) {
                if ($x != 5) {
                    $this->Image("../../../../images/logo_corte.jpg", $x+70, 150, 12, 5);
                    $this->Image("../../../../images/logo_triste.jpg", $x+65, 125, 20, 20);
                } else {
                    $this->Image("../../../../images/logo_corte.jpg", $x+70, 150, 12, 5);
                    $this->Image("../../../../images/logo_triste.jpg", $x+65, 125, 20, 20);
                }
                if ($nromeses >= 2) {
                    $fechavencimiento = "ULTIMO DIA DE PAGO : ".$fvencimientodeudores;
                    $fechavencimientoval = $fvencimientodeudores;
                    $fechacorte = "FECHA DE CORTE : ".$fcorte;
                    $fv = $fvencimientodeudores;

					if ($nromeses >= 2)
					{
						$fechavencimiento = "ULTIMO DIA DE PAGO : VENCIDO";
						$fechavencimientoval = "VENCIDO";
					}
                } else {
                    $fechavencimiento = "ULTIMO DIA DE PAGO : ".$fvencimientodeudores;
                    $fechavencimientoval = $fvencimientodeudores;
                    $fechacorte = "FECHA DE CORTE : ".$fcorte;
                    $fv = $fvencimientodeudores;
                }
            } else {
                $fechavencimiento = "ULTIMO DIA DE PAGO : ".$fvencimiento;
                $fechavencimientoval = $fvencimiento;
                $fechacorte = "";
                $fv = $fvencimiento;

                if ($x != 5) {
                    $this->Image("../../../../images/logo_feliz.jpg", $x+60, 130, 20, 20);
                } else {
                    $this->Image("../../../../images/logo_feliz.jpg", $x+60, 130, 20, 20);
                }
            }
        } else {
            $ub = 120;
            $this->SetXY($x+1, $y+$ub);

            $CodUnidadUso = 0;
            $this->SetFont('Arial', 'B', 6);
            $this->Cell(24, 4, "Parametros de Augas", 'LTR', 1, 'C');
            $this->SetX($x+1);
            $this->Cell(24, 4, "Residuales", 'LBR', 0, 'C');
            $this->SetXY($x+25, $y+$ub);
            $this->Cell(10, 8, "DBO5", 1, 0, 'C');
            $this->Cell(10, 8, "DQO", 1, 0, 'C');
            $this->Cell(10, 8, "SST", 1, 0, 'C');
            $this->Cell(7, 8, "AyG", 1, 0, 'C');
            $this->SetXY($x+62, $y+$ub);
            $this->Cell(9, 8, "Factor", 'LTR', 1, 'C');

            $this->SetX($x+1);
            $this->Cell(24, 4, "VMA (mg/L)", 1, 0, 'L');
            $this->Cell(10, 4, "500", 1, 0, 'C');
            $this->Cell(10, 4, "1,000", 1, 0, 'C');
            $this->Cell(10, 4, "500", 1, 0, 'C');
            $this->Cell(7, 4, "100", 1, 0, 'C');
            $this->SetXY($x+62, $y+$ub+8);
            $this->Cell(9, 4, "Total", 'LBR', 1, 'C');

            $this->SetFont('Arial', '', 6);
            for ($i = 0; $i < $ConUU; $i++) {
                $this->SetX($x+1);
                $ii = $Tabla1[$i+1][4][1][2];
                $ii1 = $Tabla1[$i+1][4][2][2];
                $ii2 = $Tabla1[$i+1][4][3][2];
                $ii3 = $Tabla1[$i+1][4][4][2];
                $timp = $ii+$ii1+$ii2+$ii3;

                $UnidadDes = $Tabla1[$i+1][1];
                if ($ConUU == 1) {
                    $UnidadDes = '';
                }

                $this->Cell(24, 4, utf8_decode("Valor de Análisis ").$UnidadDes, 1, 0, 'L');
                $this->Cell(10, 4, number_format($Tabla1[$i+1][4][1][1], 1), 1, 0, 'C');
                $this->Cell(10, 4, number_format($Tabla1[$i+1][4][2][1], 1), 1, 0, 'C');
                $this->Cell(10, 4, number_format($Tabla1[$i+1][4][3][1], 1), 1, 0, 'C');
                $this->Cell(7, 4, number_format($Tabla1[$i+1][4][4][1], 1), 1, 0, 'C');
                $this->Cell(9, 4, "", 'LTR', 1, 'C');

                $this->SetX($x+1);
                $this->Cell(24, 4, utf8_decode("Factores Individuales "), 1, 0, 'L');
                $this->Cell(10, 4, number_format($Tabla1[$i+1][4][1][2], 0)." %", 1, 0, 'C');
                $this->Cell(10, 4, number_format($Tabla1[$i+1][4][2][2], 0)." %", 1, 0, 'C');
                $this->Cell(10, 4, number_format($Tabla1[$i+1][4][3][2], 0)." %", 1, 0, 'C');
                $this->Cell(7, 4, number_format($Tabla1[$i+1][4][4][2], 0)." %", 1, 0, 'C');
                $this->Cell(9, 4, $timp." %", 'LBR', 1, 'C');
            }
        }

        $this->SetFont('Arial', 'B', 10);
        $this->SetXY($x + 1, 160);

        $NunLetras = CantidadEnLetra($totalfacturacion);

        if (intval($this->GetStringWidth($NunLetras)) > 84)
		{
            $this->SetFont('Arial', 'B', 8);
            $this->MultiCell(85, 4, trim($NunLetras)." SOLES", 0, 'J');
		}
        else
		{
            //$this->SetFont('Arial', 'B', 9);
            $this->MultiCell(85, 4, trim($NunLetras)." SOLES", 0, 'J');
            $NunLetras = $NunLetras." SOLES";
		}

        // EL mensaje personalizado
        $this->SetFont('Arial', 'B', 8);
        $this->SetXY($x + 5, 135);


		if ($nromeses == 0)
		{
        	$this->MultiCell(60, 5, utf8_decode($mensajenormal), 0, 'C');
		}
		else
		{
			$this->MultiCell(60, 5, utf8_decode($MensajeDeudas), 0, 'C');
		}

        if ($x != 0) {
            $xm = -4;
        }
        $this->SetXY($x+6+$xm, 172);
        $this->SetFont('Arial', 'B', 8);
        $this->MultiCell(152, 5, utf8_decode($mensaje), 0, 'C');
        //$this->Ln(1);
        // if($x != 0){$xm = -4;}
        $this->SetXY($x+8, 187);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(70, 6, "FECHA DE EMISION   ".trim($femision), 0, 0, 'L');
        $this->Cell(75, 6, $fechavencimiento, 0, 1, 'R');
        $this->SetFont('Arial', '', 8.5);

        // $this->SetXY($x+80, 192);
        // $this->Cell(100, 5, '*** V.I.P. ***', 0, 1, 'L');
        $xt01 = 69;
        if ($x != 0) {
            $xt01 = 65;
        }
        $this->SetXY($x + $xt01, 203);
        $this->Cell(40, 5, utf8_decode(trim($propietario)), 0, 0, 'R');
        $this->Cell(30, 5, trim($catastro), 0, 0, 'R');
        $this->Cell(20, 5, trim($codantiguo), 0, 1, 'R');
        $this->Ln(2);

        $this->SetFont('Arial', '', 6);
        $this->SetXY($x+8.5, 221);
        $this->Cell(3.6, 0.4, "", 0, 0, 'C');
        $this->Cell(3.6, 0.4, utf8_decode(""), 0, 0, 'C');
        $this->Cell(3.6, 0.4, "", 0, 0, 'C');
        $this->Cell(3.6, 0.4, utf8_decode(""), 0, 0, 'C');
        $this->Cell(3.6, 0.4, utf8_decode(""), 0, 1, 'C');

        // Ultimo cuadro
        if (intval($nromeses) > 0) {
            if ($nromeses >= 2) {
                $fechavencimiento = $fechavencimiento3;

				$DeudaMayor3 = 'VENCIDO';
            } else {
                $fechavencimiento = $fvencimientodeudores;
                $fechacorte = $fcorte;
                $fv = $fvencimientodeudores;
            }
        } else {

            $fechavencimiento = $fvencimiento;
            $fechacorte = "";
            $fv = $fvencimiento;
        }

        $xt = 4;
        $al = 'C';
        if ($x != 0) {
            $xt = 2;
            $al = 'L';
        }
        $this->SetXY($x+$xt, 211);
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(40, 5, "FECHA DE EMISION", 0, 0, 'C');
        $this->Cell(50, 5, "ULTIMO DIA DE PAGO", 0, 0, 'C');
        $this->Cell(25, 5, $aniomes, 0, 0, 'C');
        $this->Cell(33, 5, $serie."-".str_pad($nrorecibo, 8, '0', STR_PAD_LEFT), 0, 1, 'C');

        $xt = 4;
        $al = 'C';
        if ($x != 0) {
            $xt = 2;
            $al = 'L';
        }
        $this->SetXY($x+$xt, 216);
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(40, 8, $femision, 0, 0, 'C');
        $this->Cell(50, 8, $fechavencimientoval, 0, 0, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(25, 8, "TOTAL A PAGAR", 0, 0, 'C');
        $this->SetFont('Arial', 'B', 14);
        //$this->SetFillColor(195, 195, 195);
        $this->Cell(33, 7, $TotalF, 0, 1, 'C', true);

        $xt = 4;
        if ($x != 0):
			$xt = 10;
            $al = 'L';
        endif;
        $this->SetFont('Arial', '', 11);
        $this->SetX($x + $xt);
        $this->Cell(120, 5, $NunLetras, 0, 1, 'C');

		//Con Promedio
		if ($consumofact <> $consumo && $ConRebaja <> 1)
		{
			$this->SetXY(81 + $xt2, 81);

			$this->SetFont('Arial', 'B', 9);
			$this->SetFillColor(195, 195, 195);

			$this->Cell(74, 7, "Consumo Promediado ".intval($consumofact)." m3", 1, 0, 'C', true);
			//$this->Cell(2, 0.4, utf8_decode("Consumo Fact."), 0, 0, 'L');
//				$this->Cell(2, 0.4, utf8_decode((int)$consumofact." m3"), 0, 0, 'C');
//				$this->Cell(2, 0.4, utf8_decode(""), 0, 0, 'C');
//				$this->Cell(1.7, 0.4, utf8_decode(""), 0, 1, 'C');

			$this->SetXY(95 + $xt2, 70.5);

			//$this->SetFillColor(255, 255, 255);

			//$this->Cell(20, 10, "Sin Lectura", 0, 0, 'C', true);
		}

		$this->SetFont('Arial', '', 8.5);
    }

    function Footer() {

    }

    //----Funcion para generar el Grafico
    function generargraficos1($mesesconsumo, $meslit, $nroinscripcion) {

        $count = 0;

        $carpetaimg = $_SESSION['path']."/sigco/facturacion/operaciones/impresion/graficos/";
        chmod($carpetaimg, 7777);

        $img = $carpetaimg."consumo_".$nroinscripcion.".png";

        if (file_exists($img)) {
            unlink($img);
        }

        $datosy = array_merge($mesesconsumo);

        // Creamos el grafico
        $grafico = new Graph(500, 250);
        $grafico->SetScale('textlin');

        $grafico->xaxis->SetTickLabels(array_merge($meslit));
        // Ajustamos los margenes del grafico-----    (left,right,top,bottom)
        $grafico->SetMargin(40, 30, 40, 40);

        // Creamos barras de datos a partir del array de datos
        $bplot = new BarPlot($datosy);

        // Configuramos color de las barras
        $bplot->SetFillColor('#479CC9');

        // Configuramos color de las barras
        $bplot->SetWidth(0.1);

        // Queremos mostrar el valor numerico de la barra
        $bplot->value->Show();

        //A?adimos barra de datos al grafico
        $grafico->Add($bplot);

        // Configuracion de los titulos
        $grafico->title->Set('Evolucion de Consumo');
        $grafico->yaxis->title->Set('Consumo');
        $grafico->xaxis->title->Set('Meses Consumido');

        $grafico->title->SetFont(FF_FONT1, FS_BOLD);
        $grafico->yaxis->title->SetFont(FF_FONT1, FS_BOLD);
        $grafico->xaxis->title->SetFont(FF_FONT1, FS_BOLD);


        // Se muestra el grafico
        $grafico->Stroke($img);

        return $img;
    }

    //  Para los cuadros con bordes redondeados o sin ellos
    function RoundedRect($x, $y, $w, $h, $r, $style = '') {
        $k = $this->k;
        $hp = $this->h;
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op = 'S';
        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m', ($x+$r) * $k, ($hp - $y) * $k));
        $xc = $x+$w - $r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - $y) * $k));

        $this->_Arc($xc+$r * $MyArc, $yc - $r, $xc+$r, $yc - $r * $MyArc, $xc+$r, $yc);
        $xc = $x+$w - $r;
        $yc = $y+$h - $r;
        $this->_out(sprintf('%.2F %.2F l', ($x+$w) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc+$r, $yc+$r * $MyArc, $xc+$r * $MyArc, $yc+$r, $xc, $yc+$r);
        $xc = $x+$r;
        $yc = $y+$h - $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - ($y+$h)) * $k));
        $this->_Arc($xc - $r * $MyArc, $yc+$r, $xc - $r, $yc+$r * $MyArc, $xc - $r, $yc);
        $xc = $x+$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1 * $this->k, ($h - $y1) * $this->k, $x2 * $this->k, ($h - $y2) * $this->k, $x3 * $this->k, ($h - $y3) * $this->k));
    }

}

$mesesconsumoA = array();
$mesesconsumoB = array();

$mesesliteralA = array();
$mesesliteralB = array();

$codciclo = $_GET["codciclo"];
$codsector = $_GET["codsector"];
$codruta_ini = $_GET["codruta_ini"]; //not used
$codruta_fin = $_GET["codruta_fin"]; //not used
$codsuc = $_SESSION['IdSucursal']; //$_GET["codsuc"];
$orden = 0; //$_GET["orden"];
$codinicio = $_GET["codinicio"]; //10;//$_GET["codinicio"];
$codfinal = $_GET["codfinal"]; //90;//$_GET["codfinal"];
$todosgrupos = $_GET["todosgrupos"]; //90;//$_GET["codfinal"];
$tipoentidades = $_GET["tipoentidades"];
$cantidad_meses = $_GET["cantidad_meses"];

define('Cubico', chr(179));

switch ($cantidad_meses):
    case 1: $sqlcant = ' ';
        break;
    case 2: $sqlcant = ' AND imp.nromeses = 0';
        break;
    case 3: $sqlcant = ' AND imp.nromeses >= 1';
        break;
endswitch;

if ($orden == 0) :
    $order = "ORDER BY clie.codemp, clie.codsuc, clie.codsector, clie.codrutlecturas, cone.orden_dist";

else:
    $order = "ORDER BY imp.direccion ";
endif;

if ($todosgrupos == 1) {
    $sqls = " AND imp.nrorecibo <> 0  AND cone.codtipoentidades <> 0 AND imp.nrorecibo BETWEEN ".$codinicio." AND ".$codfinal." ";
    $order = "ORDER BY cone.codtipoentidades, imp.nrorecibo  ";
} else {
    if ($tipoentidades <> 0) {
        $sqls .= " AND cone.codtipoentidades = ".$tipoentidades." AND imp.nrorecibo <> 0 AND imp.nrorecibo BETWEEN ".$codinicio." AND ".$codfinal." ";
    } else {
        $sqls = " AND imp.codsector = ".$codsector." AND imp.nrorecibo <> 0 AND imp.nrorecibo BETWEEN ".$codinicio." AND ".$codfinal." ";
        $sqls .= " AND CAST(clie.codrutlecturas AS INTEGER) >= ".$codruta_ini." AND CAST(clie.codrutlecturas AS INTEGER) <= ".$codruta_fin." ";
        $sqls .= " AND cone.codtipoentidades = ".$tipoentidades;
    }
}

$objreporte = new clsImpresion('L', 'mm', array(245, 345));
$objreporte->SetAutoPageBreak(true, 1);
$pag = 1;

$facturacion = $objreporte->datosfacturacion($codsuc, $codciclo);
$paramae = $objreporte->getParamae("IMPIGV", $codsuc);

$nrofacturacion = ($facturacion["nrofacturacion"] - 1);

$sql = "  SELECT imp.*, zo.descripcion as zona, clie.ruc, clie.codtiposervicio AS codtiposervicio, ts.descripcion AS tiposervicio, clie.catetar AS catetar
              FROM facturacion.impresion_recibos imp
                JOIN catastro.clientes as clie ON(imp.codemp=clie.codemp AND imp.codsuc=clie.codsuc AND imp.nroinscripcion=clie.nroinscripcion)
                JOIN catastro.conexiones as cone ON (cone.codemp=clie.codemp AND cone.codsuc=clie.codsuc AND cone.nroinscripcion=clie.nroinscripcion)
                JOIN public.tiposervicio as ts ON (ts.codtiposervicio = clie.codtiposervicio)
                JOIN admin.zonas zo ON clie.codemp = zo.codemp AND clie.codsuc = zo.codsuc AND clie.codzona = zo.codzona
              WHERE imp.codsuc=".$codsuc." AND imp.codciclo=".$codciclo." ".$sqlcant."
                AND imp.nrofacturacion=".$nrofacturacion." ".$sqls." ".$order."";

$consulta = $conexion->prepare($sql);
$consulta->execute(array());
$items = $consulta->fetchAll();

foreach ($items as $row) {

    $mesesconsumoA = array();
    $mesesconsumoB = array();

    $mesesliteralA = array();
    $mesesliteralB = array();
    $mesesconsumoA = explode("|", substr($row["consumografico"], 0, strlen($row["consumografico"]) - 1));
    $mesesliteralA = explode("|", substr($row["mesesgrafico"], 0, strlen($row["mesesgrafico"]) - 1));

    $count = 0;
    for ($i = 0; $i < count($mesesconsumoA); $i++) :
        $mesesconsumoB[$count] = $mesesconsumoA[$i];
        $mesesliteralB[$count] = $meses[$mesesliteralA[$i]];
        $count++;
    endfor;

    $totmes = $row["impagua"]+$row["impdesague"]+$row["impinteres"]+$row["impcargofijo"]+$row["impcredito"]+$row["imprefinanciamiento"];
    if ($totmes != 0) {
        $aniomes = substr($meses[$row["mes"]], 0, 3)."-".$row["anio"];
        if ($pag == 1) {
            $pag = 2;
            $objreporte->AddPage();
            $objreporte->SetFillColor(255, 255, 255);
            $objreporte->RoundedRect(5, 3, 80, 12, 0, 'DF');  // Para la cabecera 1° cuadrante
            $objreporte->RoundedRect(5, 42, 80, 13, 0, 'DF');  // Para la cabecera 2° cuadrante

            $objreporte->RoundedRect(5, 60, 80, 28, 0, 'DF');  // Para los lectura periodos 1°
            $objreporte->RoundedRect(86, 65, 74, 23, 0, 'DF');  // Para los lectura periodos 2°

            $objreporte->RoundedRect(5, 95, 155, 72, 2.5, 'DF');  // Para los conceptos
            $objreporte->RoundedRect(5, 170, 155, 24, 2.5, 'DF'); // Para el mensaje
            $objreporte->Line(5, 186, 160, 186); // Cuadro para bauche caja

            $objreporte->RoundedRect(5, 208, 155, 15, 2.5, 'DF'); // Cuadro para bauche caja
            $objreporte->Line(45, 208, 45, 223); // 1° linea Cuadro para bauche caja
            $objreporte->Line(90, 208, 90, 223); // 2° linea Cuadro para bauche caja
            $objreporte->Line(90, 216, 160, 216); // 3° linea horizontal Cuadro para bauche caja
            // var_dump($row['unidades']);exit;

            $objreporte->Contenido(0, 2, $row["ruc"], $row["nroinscripcion"], $row["codcatastro"], $aniomes, $row["nrorecibo"], $objreporte->DecFecha($row["fechaemision"]), $row["propietario"], $row["direccion"], $row["servicios"], $row["tipofacturacion"], $row["unidades"], $row['tarifa'], $row['unidades'], $row["nromedidor"], $objreporte->DecFecha1($row["fechalecturaanterior"]), $row["lecturaanterior"], $row["consumom3"], $objreporte->DecFecha1($row["fechalecturaultima"]), $row["lecturaultima"], $row["impagua"], $row["impdesague"], $row["impinteres"], $row["impcargofijo"], $row["impcredito"], $row["imprefinanciamiento"], $row["nromeses"], $row["impdeuda"], $paramae["valor"], $row["impigv"], $row["impredondeo"], $row["mensajegeneral"], $objreporte->DecFecha($row["fechavencimientonormal"]), $objreporte->DecFecha($row["fechavencimientodeudores"]), $objreporte->DecFecha($row["fechacorte"]), $mesesconsumoB, $mesesliteralB, $row["mes"], $row["docidentidad"], $row["nrodocidentidad"], $row['mensajenormal'], $row['codantiguo'], $row['lecturapromedio'], $codsuc, $row['horasabastecimiento'], $nrofacturacion, $row['impcolateral'], $objreporte->DecFecha($row['fechavencimiento3']), $row['estadomedidor'], $row['consumofact'], $row["zona"], $row['urbanizacion'], $row['codtiposervicio'], $row["tiposervicio"], $row['catetar'], $row['mensajedeudores']);
        } else {
            $pag = 1;
            $objreporte->SetFillColor(255, 255, 255);
            $objreporte->RoundedRect(185, 3, 80, 12, 0, 'DF');  // Para la cabecera 1° cuadrante
            $objreporte->RoundedRect(185, 42, 80, 13, 0, 'DF');  //Para la cabecera 2° cuadrante

            $objreporte->RoundedRect(185, 60, 80, 28, 0, 'DF');  // Para los lectura periodos 1°
            $objreporte->RoundedRect(266, 65, 74, 23, 0, 'DF');  // Para los lectura periodos 2°

            $objreporte->RoundedRect(185, 95, 155, 72, 2.5, 'DF'); // Para los conceptos
            $objreporte->RoundedRect(185, 170, 155, 24, 2.5, 'DF'); // Para el mensaje
            $objreporte->Line(185, 186, 340, 186); // Cuadro para bauche caja

            $objreporte->RoundedRect(184, 208, 155, 15, 2.5, 'DF'); // Cuadro para bauche caja
            $objreporte->Line(230, 208, 230, 223); // 1° linea Cuadro para bauche caja
            $objreporte->Line(273, 208, 273, 223); // 2° linea Cuadro para bauche caja
            $objreporte->Line(273, 216, 339, 216); // 3° linea horizontal Cuadro para bauche caja

            $objreporte->Contenido(183, 2, $row["ruc"], $row["nroinscripcion"], $row["codcatastro"], $aniomes, $row["nrorecibo"], $objreporte->DecFecha($row["fechaemision"]), $row["propietario"], $row["direccion"], $row["servicios"], $row["tipofacturacion"], $row["unidades"], $row['tarifa'], $row['unidades'], $row["nromedidor"], $objreporte->DecFecha1($row["fechalecturaanterior"]), $row["lecturaanterior"], $row["consumom3"], $objreporte->DecFecha1($row["fechalecturaultima"]), $row["lecturaultima"], $row["impagua"], $row["impdesague"], $row["impinteres"], $row["impcargofijo"], $row["impcredito"], $row["imprefinanciamiento"], $row["nromeses"], $row["impdeuda"], $paramae["valor"], $row["impigv"], $row["impredondeo"], $row["mensajegeneral"], $objreporte->DecFecha($row["fechavencimientonormal"]), $objreporte->DecFecha($row["fechavencimientodeudores"]), $objreporte->DecFecha($row["fechacorte"]), $mesesconsumoB, $mesesliteralB, $row["mes"], $row["docidentidad"], $row["nrodocidentidad"], $row['mensajenormal'], $row['codantiguo'], $row['lecturapromedio'], $codsuc, $row['horasabastecimiento'], $nrofacturacion, $row['impcolateral'], $objreporte->DecFecha($row['fechavencimiento3']), $row['estadomedidor'], $row['consumofact'], $row["zona"], $row['urbanizacion'], $row['codtiposervicio'], $row["tiposervicio"], $row['catetar'], $row['mensajedeudores']);
        }
    }
}

$objreporte->Output();
array_map('unlink', glob("barras/*.jpg"));
array_map('unlink', glob("graficos/*.png"));
?>
