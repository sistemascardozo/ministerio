<?php
include("../../../../objetos/clsReporte.php");

class clsImpresionCargo extends clsReporte {

    function Header() {
        global $codsuc;
        //DATOS DEL FORMATO
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
        $this->Ln(1);
        $this->SetFont('Arial', '', 8);
        $hc = 5;
        $tit2 = strtoupper("");
        $this->Cell(190, $hc, $tit2, 0, 1, 'C');
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
        //DATOS DEL FORMATO
        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoupper($empresa["razonsocial"]);
        $tit2 = 'JR. DAMASO BERAUN Nº 545, HUANUCO';
        $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
        $x = 23;
        $y = 5;
        $h = 3;
        $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

        $this->SetY(10);
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');
        $this->Cell(0, 3, utf8_decode("Página :   ".$this->PageNo().' de {nb}     '), 0, 1, 'R');
        $this->Cell(0, 3, "Fecha : ".$FechaActual."   ", 0, 1, 'R');
        $this->Cell(0, 3, "Hora : ".$Hora." ", 0, 1, 'R');
        $this->SetY(19);
        $this->SetLineWidth(.1);
        $this->Cell(0, .1, "", 1, 1, 'C', true);

        $this->Ln(4);
        $this->cabecera();
    }

    function Firmas() {

        $this->CheckPageBreak(40);
        $this->SetY($this->GetY() + 40);
        $this->SetFont('Arial', 'B', 7);
        $this->Cell(40, 4, "", 0, 0, 'L');
        $this->Cell(35, 4, "EPS EMPSSAPAL S.A.", 'T', 0, 'C');
        $this->Cell(60, 4, "", 0, 0, 'L');
        $this->Cell(35, 4, "Recibi Conforme", 'T', 1, 'C');
    }

    function cabecera() {
        global $codsuc, $meses, $aniomes, $grupo_nombre;

        $this->SetFont('Arial', 'B', 10);

        $direcccion_grupo = "---";
        $responsable = utf8_decode("PROMOCIÓN Y VENTAS");

        $h = 4;
        $this->Cell(190, $h + 2, "CARGO DE RECIBOS", 0, 1, 'C');
        $this->SetLineWidth(.2);
        $this->Line(85, 27.8, 125, 27.8);
        $this->SetFont('Arial', 'B', 7);
        $this->Cell(20, $h + 2, "GRUPO", 0, 0, 'L');
        $this->Cell(5, $h + 2, ":", 0, 0, 'L');
        $this->Cell(100, $h + 2, $grupo_nombre, 0, 0, 'L');

        $this->Cell(20, $h + 2, "Mes Fact.", 0, 0, 'L');
        $this->Cell(5, $h + 2, ":", 0, 0, 'L');
        $this->Cell(40, $h + 2, $aniomes, 0, 1, 'L');

        $this->Cell(20, $h + 2, "Direcc.", 0, 0, 'L');
        $this->Cell(5, $h + 2, ":", 0, 0, 'L');
        $this->Cell(100, $h + 2, $direcccion_grupo, 0, 0, 'L');

        $this->Cell(20, $h + 2, "Respon.", 0, 0, 'L');
        $this->Cell(5, $h + 2, ":", 0, 0, 'L');
        $this->Cell(40, $h + 2, $responsable, 0, 1, 'L');


        $this->Ln(3);

        $this->SetFont('Arial', 'B', 6);
        $this->SetWidths(array(20, 15, 100, 30, 25));
        //$this->SetAligns(array("C","C","C","C","C"));
        $this->SetAligns(array("L", "C", "L", "L", "R"));
        $this->Row(array("Codigo",
            "Inscripcion",
            "Nombre - Direccion",
            "Documento",
            "Importe"));
    }

    function Contenido($codcatastro, $nroinscripcion, $usuario, $direccion, $documento, $nrorecibo, $importe) {
        $h = 6;
        $this->SetFont('Arial', '', 7);

        $this->SetWidths(array(20, 15, 100, 30, 25));
        $this->SetAligns(array("L", "C", "L", "L", "R"));
        $this->Row(array($codcatastro,
            $nroinscripcion,
            $usuario." - ".$direccion,
            $documento." - ".$nrorecibo,
            number_format($importe, 2)));
    }

}

	$codciclo		= $_GET["codciclo"];
	$codsector		= $_GET["codsector"];
	$codruta_ini	= $_GET["codruta_ini"];
	$codruta_fin	= $_GET["codruta_fin"];
	$codsuc			= $_GET["codsuc"];
	$orden			= $_GET["orden"];
	$codinicio		= $_GET["codinicio"]; //10;//$_GET["codinicio"];
	$codfinal		= $_GET["codfinal"]; //90;//$_GET["codfinal"];
	$grupo			= $_GET["grupo"]; //0;//$_GET["grupo"];

	$objReporte = new clsImpresionCargo();

	$facturacion	= $objReporte->datosfacturacion($codsuc, $codciclo);
	$paramae		= $objReporte->getParamae("IMPIGV", $codsuc);
	$nrofacturacion	= ($facturacion["nrofacturacion"] - 1);
	$imes			= $facturacion["mes"];
	$ianio			= $facturacion["anio"];
	
	if($facturacion["mes"] == 1)
	{
		$imes = 12;
		$ianio--;
	}
	else
	{
		$imes--;
	}
	
	$aniomes = $meses[$imes]."-".$ianio;

	$total = 0;
	$count = 0;
	$txt = 'RECIBO';

	$documento = substr($txt, 0, 3);

	$objReporte->AliasNbPages();

	$todogrupos = $_GET["todosgrupos"];
	$where = '';
	
	if ($todogrupos == 1)
	{
		//        $sqls = " AND conex.codtipoentidades = ".$grupo;
		//$sqls = " AND clie.codrutdistribucion>=".$codruta_ini;
		// $sqls .= " AND clie.codrutdistribucion<=".$codruta_fin;
		// $sqls .= " AND conex.codtipoentidades <>0 ";
		//$sqls .= "AND clie.codsector= ".$codsector;
		$where = " AND c.codtipoentidades <> 0 AND r.nrorecibo BETWEEN ".$codinicio." AND ".$codfinal." ";
	}
	else
	{
		if($grupo <> 0) 
		{
			$where .= " AND c.codtipoentidades = ".$grupo." AND r.nrorecibo <> 0 AND r.nrorecibo BETWEEN ".$codinicio." AND ".$codfinal." ";
			
			$sqls = " AND conex.codtipoentidades = ".$grupo." AND r.nrorecibo <> 0 AND r.nrorecibo BETWEEN ".$codinicio." AND ".$codfinal." ";
		}
		else
		{
			$sqls = " AND r.codsector = ".$codsector." AND r.nrorecibo <> 0 AND r.nrorecibo BETWEEN ".$codinicio." AND ".$codfinal." ";
			$sqls .= " AND CAST(clie.codrutlecturas AS INTEGER) >= ".$codruta_ini." AND CAST(clie.codrutlecturas AS INTEGER) <= ".$codruta_fin." ";
			$sqls .= " AND conex.codtipoentidades = ".$grupo;
		}
		//$sqls .= " AND CAST(clie.codmanzanas AS INTEGER) >= ".$codruta_ini;
//		$sqls .= " AND CAST(clie.codmanzanas AS INTEGER) <= ".$codruta_fin;
//		$sqls = "AND clie.codsector= ".$codsector;
		//$where = ' AND codtipoentidades='.$grupo;
	}

	$Sql = "SELECT e.codtipoentidades, e.descripcion, e.estareg ";
	$Sql .= "FROM catastro.conexiones c ";
	$Sql .= " INNER JOIN public.tipoentidades e ON (c.codtipoentidades = e.codtipoentidades) ";
	$Sql .= " INNER JOIN facturacion.impresion_recibos r ON (r.codemp = c.codemp AND r.codsuc = c.codsuc AND r.nroinscripcion = c.nroinscripcion) ";
	$Sql .= "WHERE r.codsuc = ".$codsuc." ";
	$Sql .= " AND r.nrofacturacion = ".$nrofacturacion." ";
	$Sql .= " AND e.estareg = 1 ";
	$Sql .= " AND e.codtipoentidades <> 0 ";
	$Sql .= $where." ";
	$Sql .= "GROUP BY e.codtipoentidades, e.descripcion, e.estareg ";
	$Sql .= "ORDER BY e.codtipoentidades";
	
	$consulta2 = $conexion->prepare($Sql);
	$consulta2->execute();
	$items2 = $consulta2->fetchAll();

	foreach ($items2 as $row2)
	{
		$sql_name = "SELECT descripcion FROM tipoentidades WHERE codtipoentidades = ".$row2[0];
		
		$consulta3 = $conexion->prepare($sql_name);
		$consulta3->execute(array());
		$result = $consulta3->fetchAll();
		
		$grupo_nombre = $result[0]['descripcion'];

		$total = 0;
		$count = 0;
		$txt = 'RECIBO';
		
		$objReporte->AddPage();
		
		$sql = "SELECT r.codcatastro, clie.codantiguo, clie.propietario, ";
		$sql .= " (tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) AS direccion, ";
		$sql .= " '".$documento."' documento, r.nrorecibo, (r.totalfacturado+r.impdeuda) AS importe, r.nroinscripcion ";
		$sql .= "FROM facturacion.impresion_recibos r ";
		$sql .= " INNER JOIN catastro.clientes clie ON (r.codemp = clie.codemp AND r.codsuc = clie.codsuc AND r.nroinscripcion = clie.nroinscripcion) ";
		$sql .= " INNER JOIN public.calles cal ON (clie.codemp = cal.codemp AND clie.codsuc = cal.codsuc AND clie.codcalle = cal.codcalle AND clie.codzona = cal.codzona) ";
		$sql .= " INNER JOIN public.tiposcalle tipcal ON (cal.codtipocalle = tipcal.codtipocalle) ";
		$sql .= " INNER JOIN catastro.conexiones conex ON (clie.codemp = conex.codemp AND clie.codsuc = conex.codsuc AND clie.nroinscripcion = conex.nroinscripcion) ";
		$sql .= "WHERE r.nrofacturacion = ".$nrofacturacion." ";
		$sql .= " AND r.codsuc = ".$codsuc." ";
		$sql .= " AND clie.estareg = 1 ";
		$sql .= " AND r.codciclo = ".$codciclo." ";
		$sql .= $sqls." ";
		$sql .= "ORDER BY r.nrorecibo ";
    ///*AND r.nroorden between :codinicial and :codfinal*/

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array());
	
    $items = $consulta->fetchAll();

    foreach ($items as $row) {
        $count++;
        $total += $row["importe"];
        /*
        $nroInsp= $row["nroinscripcion"];        
        $VerImp="SELECT SUM(d.importe) AS importe FROM facturacion.detfacturacion AS d
            WHERE d.nrofacturacion='".$nrofacturacion."' and d.codsuc=".$codsuc." and d.nroinscripcion= ".$nroInsp;
        $rs = $conexion->query($VerImp)->fetch();      
        $total += $rs["importe"];    
        */
        
        $objReporte->Contenido($row["codcatastro"], $row["codantiguo"], utf8_decode($row["propietario"]), 
            utf8_decode(strtoupper($row["direccion"])), $row["documento"], $row["nrorecibo"], $row["importe"]);
    }
    if ($count != 1)
        $txt.='S';
    $objReporte->SetFont('', 'b', 8);
    $objReporte->SetWidths(array(20, 15, 100, 30, 25));
    $objReporte->SetAligns(array("C", "C", "R", "R", "R"));
    $objReporte->Row(array('', '', 'TOTALES: ', $count."  ".$txt, 'S/.'.number_format($total, 2)));
    $objReporte->Firmas();
}
$objReporte->Output();
?>