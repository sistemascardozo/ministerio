<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codciclo          = $_POST["codciclo"];
	$codsuc            = $_POST["codsuc"];
	$femision          = $objFunciones->CodFecha($_POST["femision"]);
	$fvencnormal       = $objFunciones->CodFecha($_POST["fvencnormal"]);
	$mensajenormal     = $_POST["mensajenormal"];
	$fvencdeudores     = $objFunciones->CodFecha($_POST["fvencdeudores"]);
	$mensajedeudores   = $_POST["mensajedeudores"];
	$mensajegeneral    = $_POST["mensajegeneral"];
	$fcorte            = $objFunciones->CodFecha($_POST["fcorte"]);
	$fechavencimiento3 = $objFunciones->CodFecha($_POST["fechavencimiento3"]);
	
	$conexion->beginTransaction();
	
	$facturacion = $objFunciones->datosfacturacion($codsuc, $codciclo);
	
	$nrofacturacion = $facturacion["nrofacturacion"] - 1;
	//die($nrofacturacion);
	$updPeriodo = "UPDATE facturacion.periodofacturacion ";
	$updPeriodo .= " SET fechaemision = '".$femision."', ";
	$updPeriodo .= " fechavencnormal = '".$fvencnormal."', ";
	$updPeriodo .= " mensajenormal = '".$mensajenormal."', ";
	$updPeriodo .= " fechavencdeudores = '".$fvencdeudores."', ";
	$updPeriodo .= " mensajedeudores = '".$mensajedeudores."', ";
	$updPeriodo .= " mensajegeneral = '".$mensajegeneral."', ";
	$updPeriodo .= " fechacorte = '".$fcorte."', ";
	$updPeriodo .= " fechavencimiento3 = '".$fechavencimiento3."' ";
	$updPeriodo .= "WHERE codsuc = ".$codsuc." ";
	$updPeriodo .= " AND codciclo = ".$codciclo." ";
	$updPeriodo .= " AND nrofacturacion = ".$nrofacturacion;
	
	$consulta_Periodo = $conexion->prepare($updPeriodo);
	$consulta_Periodo->execute(array());
	
	if ($consulta_Periodo->errorCode() != '00000')
	{
		$conexion->rollBack();
		$res=2;
		$mensaje = "44: Error Cuando se ha Registrado los Datos";
		die($res."|".$mensaje);
	}
	
	$Sql = "DELETE FROM facturacion.impresion_recibos_fechas ";
	$Sql .= "WHERE codemp = :codemp AND codsuc=:codsuc AND codciclo=:codciclo
			AND  nrofacturacion=:nrofacturacion";
	$consulta_Periodo = $conexion->prepare($Sql);
	$consulta_Periodo->execute(array(":codemp"=>1,
									  ":codsuc"=>$codsuc,
									 ":codciclo"=>$codciclo,
									 ":nrofacturacion"=>$nrofacturacion
									));		
	$sql = "SELECT DISTINCT codsector, descripcion ";
	$sql .= "FROM public.sectores WHERE estareg = 1 AND codemp = 1 AND codsuc = ".$codsuc." ";
	$sql .= "ORDER BY codsector ASC";
	
	$ConsultaTar =$conexion->query($sql);
	
	foreach($ConsultaTar->fetchAll() as $tarifas)
	{
		$Sql="INSERT INTO 
			  facturacion.impresion_recibos_fechas
			(
			  codemp,
			  codsuc,
			  codciclo,
			  nrofacturacion,
			  codsector,
			  fechadeudores,
			  fechacorte
			) 
			VALUES (
			  :codemp,
			  :codsuc,
			  :codciclo,
			  :nrofacturacion,
			  :codsector,
			  :fechadeudores,
			  :fechacorte
			);";

			$consulta_Periodo = $conexion->prepare($Sql);
			$consulta_Periodo->execute(array(":codemp"=>1,
									  ":codsuc"=>$codsuc,
									 ":codciclo"=>$codciclo,
									 ":nrofacturacion"=>$nrofacturacion,
									 ":codsector"=>$tarifas['codsector'],
									 ":fechadeudores"=>$objFunciones->CodFecha($_POST['fechadeudores_'.$tarifas['codsector']]),
									 ":fechacorte"=>$objFunciones->CodFecha($_POST['fechacorte_'.$tarifas['codsector']])
									 ));
			if ($consulta_Periodo->errorCode() != '00000')
			{
				$conexion->rollBack();
				$res=2;
				$mensaje = "97: Error Cuando se ha Registrado los Datos";
				die($res."|".$mensaje);
			}

	}

	$conexion->commit();
	
	$res=1;
	$mensaje = "Los Datos se han Registrado Correctamente";

	echo $res."|".$mensaje;
?>