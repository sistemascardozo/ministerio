<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$codemp = 1;
	$TituloVentana = "IMPRESION DE RECIBOS";

	$Activo = 1;
	CuerpoSuperior($TituloVentana);

	$codsuc   = $_SESSION['IdSucursal'];
	$objDrop   = new clsDrop();

	$codsuc = $_SESSION['IdSucursal'];
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_impresion.js" language="JavaScript"></script>
<script>
	var codsuc 	= <?=$codsuc?>;
	var urldir	= "<?php echo $_SESSION['urldir'];?>";

	function ValidarForm(Op)
	{
		GuardarP(Op);
	}
	function Cancelar()
	{
		location.href='index.php';
	}
	function Inicial(idx)
	{
		$("#codinicial").val($("#codcatas"+idx).val())
	}
	function Final(idx)
	{
		$("#codfinal").val($("#codcatas"+idx).val())
	}
	function quitar_disabled(obj,input)
	{
		if(obj.checked)
		{
			$("#"+input).attr('disabled', true);
		}else{
			$("#"+input).attr('disabled', false);
		}
	}

</script>

<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="1000" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td colspan="5" class="TitDetalle">
	    <div id="tabs">
	      <ul>
	        <li><a href="#tabs-3">Datos para la Impresion de Recibos</a></li>
	        <li><a href="#tabs-2">Lista de Clientes por Recibos</a></li>
	        </ul>
	      <div id="tabs-3" >
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="5%">Ciclo</td>
	            <td width="2%" align="center">:</td>
	            <td width="24%"><? $objDrop->drop_ciclos($codsuc,0,"onchange='datos_facturacion(this.value);cFechas(this.value)'"); ?></td>
	            <td width="16%" align="right">A&ntilde;o Facturado</td>
	            <td width="3%" align="center">:</td>
	            <td width="12%"><input type="text" name="anio" id="anio" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:50px;" /></td>
	            <td width="14%" align="right">Mes Facturado</td>
	            <td width="2%" align="center">:</td>
	            <td width="22%"><input type="text" name="mes" id="mes" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:150px;" /></td>
	            </tr>
	          <tr>
	            <td colspan="9">
	              <fieldset style="padding:4px;">
	                <legend class="ui-state-default ui-corner-all">Seleccione las Fechas de Emision, de Corte y de Vencimiento para Usuarios Normales y Deudores</legend>
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                    <td width="20%">Emision</td>
	                    <td width="3%" align="center">:</td>
	                    <td width="17%"><input type="text" name="fechaemision" id="fechaemision" maxlength="10" value="<?=date("d/m/Y")?>" readonly="readonly" style="width:80px;" /></td>
	                    <td width="21%" align="right">Vencimiento Normal</td>
	                    <td width="3%" align="center">:</td>
	                    <td width="36%"><input type="text" name="fechanormal" id="fechanormal" maxlength="10" value="<?=date("d/m/Y")?>" style="width:80px;" /></td>
	                    </tr>
	                  <tr>
	                    <td>Vencimiento y Corte Deudores por Sectores</td>
	                    <td align="center">:</td>
	                    <td>
	                    	<span class="icono-icon-info" title="Asignar Fechas por Sectores" onclick="VerSectores()"></span>
	                    	<input type="hidden" name="fechadeudores" id="fechadeudores" maxlength="10" size="13" value="<?=date("d/m/Y")?>" />
	                    </td>
	                    <td align="right">Vencimiento 3 meses a más</td>
	                    <td align="center">:</td>
	                    <td>
	                    	<input type="text" name="fechavencimiento3" id="fechavencimiento3" maxlength="10" value="<?=date("d/m/Y")?>" style="width:80px;" />
	                    	<input type="hidden" name="fechacorte" id="fechacorte" maxlength="10" value="<?=date("d/m/Y")?>" style="width:80px;" />
	                        <input type="hidden" name="control_generado" id="control_generado" value="0" />

	                    </td>
	                    </tr>
	                  </table>
	                </fieldset>
	              </td>
	            </tr>
	      	          <tr>
	            <td colspan="9">
	              <fieldset style="padding:4px">
	                <legend class="ui-state-default ui-corner-all">Mensaje a Usuarios</legend>
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                  <tr valign="top">
	                    <td width="21%">A Clientes Normales</td>
	                    <td width="3%" align="center">:</td>
	                    <td width="76%"><textarea name="mensajenormal" id="mensajenormal" cols="80" rows="3" style="font-size:11px"></textarea></td>
	                    </tr>
	                  <tr valign="top">
	                    <td colspan="3" height="3px"></td>
	                    </tr>
	                  <tr valign="top">
	                    <td>A Clientes Deudores</td>
	                    <td align="center">:</td>
	                    <td><textarea name="mensajedeudores" id="mensajedeudores" cols="80" rows="3" style="font-size:11px"></textarea></td>
	                    </tr>
	                  <tr valign="top">
	                    <td colspan="3" height="3px"></td>
	                    </tr>
	                  <tr valign="top">
	                    <td>A Todos los Usuarios</td>
	                    <td align="center">:</td>
	                    <td><textarea name="mensajegeneral" id="mensajegeneral" cols="80" rows="3" style="font-size:11px"></textarea></td>
	                    </tr>
	                  </table>
	                </fieldset></td>
	            </tr>

	          <tr>
	            <td>&nbsp;</td>
	            <td align="center">&nbsp;</td>
	            <td colspan="7" align="right">
	              <input type="button" name="guardar" id="guardar" value="Guardar Datos para la Impresion de Recibos" class="button" onclick="grabar_mensaje();" />
	              &nbsp;
	              <input type="button" name="generar" id="generar" value="Generar Recibos de Facturacion" class="button" onclick="generar_recibos_impresion();" />
	              </td>
	            </tr>
	          <tr>
	            <td>&nbsp;</td>
	            <td align="center">&nbsp;</td>
	            <td colspan="7">&nbsp;</td>
	            </tr>
	          </table>
	        </div>
	      <div id="tabs-2" style="height:370px">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="90">Sector</td>
	            <td width="30" align="center">:</td>
	            <td><?php echo $objDrop->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_distribucion(this.value);'"); ?></td>
	            <td align="center" colspan="4">
	              <input type="radio" name="radio" id="rutas" value="rutas" checked="checked" onclick="javascript:$('#orden').val(0)" />
	              &nbsp;
	              <label for="rutas">Ordenar por Orden</label>
	              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	              <input type="radio" name="radio" id="manzana" value="rutas" onclick="javascript:$('#orden').val(1)" />
	              &nbsp;
	              <label for="manzana">Ordenar por Calles</label>
	            </td>
	            </tr>
	          <tr>
	            <td>Ruta Inicial</td>
	            <td align="center">:</td>
	            <td>
                	<div id="div_rutasdistribucion">
                    	<? $objDrop->drop_rutas_distribucion($codsuc,0); ?>
                    </div>
                </td>
                <td>Ruta Final</td>
	            <td width="30" align="center">:</td>
	            <td colspan="2">
	            	<div id="div_rutasdistribucion_2">
                    	<? $objDrop->drop_rutas_distribucion($codsuc,0); ?>
                    </div>
	            </td>
	          </tr>
						<tr>
	            <td>Meses Deuda</td>
	            <td align="center">:</td>
	            <td>
                	<select id="mesesdeuda" name="mesedeuda">
										<option value="1">TODOS LOS MESES</option>
										<option value="2">1 MES DE DEUDA</option>
										<option value="3">2 MESES DE DEUDA A MAS</option>
                	</select>
              </td>
              <td align="center" colspan="4">
	              <input type="radio" name="hoja" id="2hojas" value="1" checked="checked"/>
	              &nbsp;2 Recibos x Hoja&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	              <input type="radio" name="hoja" id="1hoja" value="0"  />
	              &nbsp;
	              <label for="manzana">1 Recibo x Hoja</label></td>
	          </tr>
	          <tr>
	          	<td>Grupo</td>
	          	<td align="center">:</td>
	            <td colspan="">
	            	<div id="div_grupo">
                    	<? $objDrop->drop_tipo_entidades(); ?>
                    </div>
	            </td>
	            <td>
					<input type="checkbox" name="chkgrupos" id="chkgrupos" value="checkbox" onclick="CambiarEstado(this,'todosgrupos');quitar_disabled(this,'tipoentidades');" />
			        	Todos
                	<input type="hidden" name="todosgrupos" id="todosgrupos" value="0" />
			        </td>
	            <td colspan="4">
	            	<br>
	          		<input type="hidden" name="orden" id="orden" value="0" />
	            	<input type="button" name="cargar" id="cargar" value="Mostrar Usuarios" class="button" onclick="cargar_usuarios();" />
	            	&nbsp;
	            	<input type="button" name="imprimir" id="imprimir" value="Imprimir Recibos de Facturacion" class="button" onclick="imprimir_recibos_facturacion();" />
	              	&nbsp;
	              	<input type="button" name="imprimir_cargo" id="imprimir_cargo" value="Cargo de Recibos" class="button" onclick="imprimir_cargo_recibos();" />

	            </td>
	          </tr>

	          <tr>
	            <td colspan="7" align="center">
	            <input type="hidden" name="cont" id="cont" value="0" />
	              <div id="div_usuarios" style="overflow:auto; height:200px"  width="100%">
                  	<table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="ListaMenu" rules="all" >
					    <thead>
					    	<tr class="ui-widget-header">

						  	<td width="40" align="center" >Item</td>
						    <!-- <td width="11%" >Nro Recibo </td> -->
						    <td width="140" align="center" >Nro. Catastral </td>
						    <td width="90" align="center" >Nro. Documento </td>
						    <td width="80" align="center" >Nro Inscripcion</td>
						    <td align="center">Propietario</td>
						    <td align="center">Direccion</td>
						    <td width="80" align="center" >Cod. Inicial</td>
						    <td width="80" align="center" >Cod. Final</td>
						  </tr>
						</thead>
						  <tbody>
						  	</tbody>
							</table>
	              </div>
	              </td>
	            </tr>
	          <tr>
	            <td colspan="6" align="center">&nbsp;</td>
	            </tr>
	          <tr>
	            <td colspan="7" align="left">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="12%">Cod. Inicial</td>
                    <td width="3%" align="center">:</td>
                    <td width="19%">
                       <input type="text" name="codinicial" id="codinicial" class="inputtext" readonly="readonly" />
                    </td>
                    <td width="11%" align="right">Cod. Final</td>
                    <td width="3%" align="center">:</td>
                    <td width="52%">
                    	<input type="text" name="codfinal" id="codfinal" class="inputtext" readonly="readonly" />
                    </td>
                  </tr>
                </table>
                </td>
	            </tr>
	          <tr>
	            <td colspan="7" align="center">&nbsp;</td>
	            </tr>
	          <tr>
	            <td colspan="7" height="3px"></td>
	            </tr>
	          <tr>
	            <td colspan="7" align="right">

	            </tr>
	          <tr>
	            <td colspan="7" align="center">&nbsp;</td>
	            </tr>
	          </table>
	        </div>
	      </div>
	    </td>
	  </tr>
	</tbody>
	  <tfoot>
	</table>

 </form>
 <form id="form2">
	<div id="DivFecSe" title="Fechas por Sectores" style="display: none;">
	<div id="DivFecSeAcordion">
		<?php
		$sql = "SELECT DISTINCT codsector, descripcion
				from public.sectores WHERE estareg=1 AND codemp=1 AND codsuc=".$codsuc." ORDER BY codsector ASC";

		$ConsultaTar =$conexion->query($sql);
		foreach($ConsultaTar->fetchAll() as $tarifas)
		{
		?>
	  <h3><?=strtoupper($tarifas["descripcion"])?></h3>
	  <div style="height: 100px; display: block;">
	    <table width="100%" border="0" align="center" cellspacing="0" class="ui-widget">
		     <tr>
		         <td align="center"></td>
		        <td align="center"></td>
		        </tr>
		      </thead>
		       <tr>
    				<td align="right">Vencimiento Deudores</td>
    				<td>:</td>
                    <td>
                    	<input type="text" class="fechaxsector" name="fechadeudores_<?=$tarifas['codsector']?>" id="fechadeudores_<?=$tarifas['codsector']?>" maxlength="10" size="13" value="<?=date("d/m/Y")?>" />
                    </td>
                </tr>
                <tr>
                	<td align="right">Corte a Deudores</td>
                	<td>:</td>
                    <td>
                    	<input type="text" class="fechaxsector" name="fechacorte_<?=$tarifas['codsector']?>" id="fechacorte_<?=$tarifas['codsector']?>" maxlength="10" size="13" value="<?=date("d/m/Y")?>" />
	                 </td>
	            </tr>

		      </table>
	  </div>
	  <?php
	  	}
	  ?>
	</div>
</div>
	</form>
</div>
<div id="dialogo_barras">
	<span id="titulo_barra">0 %</span>
	<div id="barra"> </div>
</div>
<div id="DialogSectores" title="Seleccione Sectores">
	<div  id='DivSectores'>
<?php
	$Sql = "SELECT DISTINCT s.codsector, s.descripcion ";
	$Sql .= "FROM public.sectores s ";
	$Sql .= "WHERE s.codemp = ".$codemp." AND s.codsuc = ".$codsuc." AND s.estareg = 1 ";
	$Sql .= "ORDER BY s.codsector";

	foreach($conexion->query($Sql)->fetchAll() as $rowS) {
?>
		<label >
			<input data-text="<?=$rowS["descripcion"]?>" checked="checked" type='checkbox'  id='chcksector_<?=$rowS["codsector"]?>' value='<?=$rowS["codsector"]?>'/><?=$rowS["descripcion"]?>
		</label><br>

	<?php } ?>
    </div>
</div>
<?php /**/ CuerpoInferior();?>
