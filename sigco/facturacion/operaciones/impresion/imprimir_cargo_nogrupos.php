<?php
date_default_timezone_set("America/Lima");
session_name("pnsu");
if (!session_start()) {
    session_start();
}

global $Sede;

include($_SESSION['path']."/objetos/clsReporte.php");
ini_set("memory_limit", "512M");

class clsImpresionCargo extends clsReporte {

  function Header() {
      global $codsuc;
      //DATOS DEL FORMATO
      $this->SetY(5);
      $this->SetFont('Arial', 'B', 10);
      $tit1 = "";
      $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
      $this->Ln(1);
      $this->SetFont('Arial', '', 8);
      $hc = 5;
      $tit2 = strtoupper("");
      $this->Cell(190, $hc, $tit2, 0, 1, 'C');
      $this->SetY(5);
      $this->SetFont('Arial', 'B', 10);
      $tit1 = "";
      $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
      //DATOS DEL FORMATO
      $empresa = $this->datos_empresa($codsuc);
      $this->SetFont('Arial', '', 6);
      $this->SetTextColor(0, 0, 0);
      $tit1 = strtoupper($empresa["razonsocial"]);
      $tit2 = 'JR. DAMASO BERAUN Nº 545, HUANUCO';
      $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
      $x = 23;
      $y = 5;
      $h = 3;
      $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
      $this->SetXY($x, $y);
      $this->SetFont('Arial', '', 6);
      $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
      $this->SetX($x);
      $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
      $this->SetX($x);
      $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

      $this->SetY(10);
      $FechaActual = date('d/m/Y');
      $Hora = date('h:i:s a');
      $this->Cell(0, 3, utf8_decode("Página :   ".$this->PageNo().' de {nb}     '), 0, 1, 'R');
      $this->Cell(0, 3, "Fecha : ".$FechaActual."   ", 0, 1, 'R');
      $this->Cell(0, 3, "Hora : ".$Hora." ", 0, 1, 'R');
      $this->SetY(19);
      $this->SetLineWidth(.1);
      $this->Cell(0, .1, "", 1, 1, 'C', true);

      $this->Ln(4);
      $this->cabecera();
  }

  function Firmas() {

      $this->CheckPageBreak(40);
      $this->SetY($this->GetY() + 40);
      $this->SetFont('Arial', 'B', 7);
      $this->Cell(40, 4, "", 0, 0, 'L');
      $this->Cell(35, 4, "EPS EMPSSAPAL S.A.", 'T', 0, 'C');
      $this->Cell(60, 4, "", 0, 0, 'L');
      $this->Cell(35, 4, "Recibi Conforme", 'T', 1, 'C');
  }

  function cabecera() {
      global $codsuc, $meses, $aniomes, $grupo_nombre;

      $this->SetFont('Arial', 'B', 10);

      $direcccion_grupo = "---";
      $responsable = utf8_decode("PROMOCIÓN Y VENTAS");

      $h = 4;
      $this->Cell(190, $h + 2, "CARGO DE RECIBOS", 0, 1, 'C');
      $this->SetLineWidth(.2);
      $this->Line(85, 27.8, 125, 27.8);
      $this->SetFont('Arial', 'B', 7);
      $this->Cell(30, $h + 2, "Mes Fact.", 0, 0, 'L');
      $this->Cell(5, $h + 2, ":", 0, 0, 'L');
      $this->Cell(60, $h + 2, $aniomes, 0, 0, 'L');

      $this->Cell(30, $h + 2, "Respon.", 0, 0, 'L');
      $this->Cell(5, $h + 2, ":", 0, 0, 'L');
      $this->Cell(60, $h + 2, $responsable, 0, 1, 'L');

      $this->Ln(3);

      $this->SetFont('Arial', 'B', 6);
      $this->SetWidths(array(10, 30, 25, 70, 70, 25, 25, 30));
      $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C"));
      $this->Row(array("Item",
          "Nro Catastral",
          "Nro Inscripcion",
          "Nombres y Apellidos",
          "Direccion",
          "Nro Documento",
          "Importe",
          "Firma"
        ));
  }

  function Contenido($count, $codcatastro, $inscripcion, $propietario, $direccion, $nrodocumento, $importe) {

      $h = 6;
      $this->SetFont('Arial', '', 6);

      $this->Cell(10, $h, $count, 0, 0, 'C');
      $this->Cell(30, $h, $codcatastro, 0, 0, 'C');
      $this->Cell(25, $h, $inscripcion, 0, 0, 'C');
      $this->Cell(70, $h, utf8_decode($propietario), 0, 0, 'L');
      $this->Cell(70, $h, utf8_decode($direccion), 0, 0, 'L');
      $this->Cell(25, $h, $nrodocumento, 0, 0, 'C');
      $this->Cell(25, $h, number_format($importe,2), 0, 0, 'R');
      $this->Cell(30, $h, "______________", 0, 1, 'C');

  }

}

$codsector      = $_GET["codsector"];
$codruta_ini    = $_GET["codruta_ini"];
$codruta_fin    = $_GET["codruta_fin"];
$codrubro       = $_GET["codrubro"];
$codsuc         = $_GET["codsuc"];
$codciclo       = $_GET["codciclo"];
$tipoentidades  = empty($_GET["tipoentidades"])?0:$_GET["tipoentidades"];
$todogrupos     = $_GET["todosgrupos"];
$cantidad_meses = $_GET["cantidad_meses"];
$orden = $_GET["orden"];

$objreporte = new clsImpresionCargo("L");
$objreporte->AddPage();

$facturacion	   = $objreporte->datosfacturacion($codsuc, $codciclo);
$paramae		     = $objreporte->getParamae("IMPIGV", $codsuc);
$nrofacturacion	 = ($facturacion["nrofacturacion"] - 1);
$imes			       = $facturacion["mes"];
$ianio			     = $facturacion["anio"];

if($facturacion["mes"] == 1)
{
  $imes = 12;
  $ianio--;
}
else
{
  $imes--;
}

$aniomes = $meses[$imes]."-".$ianio;

$total = 0;
$count = 0;
$txt = 'RECIBO';

$documento = substr($txt, 0, 3);

// $objreporte->AliasNbPages();

if($orden == 1){
  $order = "ORDER BY tipcal.descripcioncorta, cal.descripcion, clie.nrocalle ";
}else{
  $order = "ORDER BY c.nrodocumento";
}

$todogrupos = $_GET["todosgrupos"];
$where = '';

if ($todogrupos == 1){
  $sqls .= " AND cone.codtipoentidades <> 0 ";
} else {
    if($tipoentidades <> 0){
        $sqls .= " AND cone.codtipoentidades = ".$tipoentidades;
    }
    else{
        $sqls = " AND CAST(clie.codrutlecturas AS INTEGER) >= ".$codruta_ini;
        $sqls .= " AND CAST(clie.codrutlecturas AS INTEGER) <= ".$codruta_fin;
        $sqls .= " AND cone.codtipoentidades = ".$tipoentidades;
        $sqls .= "AND clie.codsector = ".$codsector;
    }
}

switch ($cantidad_meses):
  case 1:	$sqlcant = ' ' ;break;
  case 2: $sqlcant = ' AND imp.nromeses = 0' ;break;
  case 3:	$sqlcant = ' AND imp.nromeses >= 1' ;break;
endswitch;

$count = 0;

$sql = "SELECT DISTINCT c.nroinscripcion,clie.propietario,c.nrodocumento,tipcal.descripcioncorta,cal.descripcion as calle,clie.nrocalle, ";
$sql .= $objreporte->getCodCatastral("clie.").",clie.nroorden ,clie.codantiguo, (imp.totalfacturado + c.impdeuda) AS importe ";
$sql .= "FROM facturacion.cabfacturacion as c ";
$sql .= " JOIN catastro.clientes as clie ON(c.codemp=clie.codemp and c.codsuc=clie.codsuc and c.nroinscripcion=clie.nroinscripcion) ";
$sql .= " JOIN public.calles as cal ON(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona) ";
$sql .= " JOIN public.tiposcalle as tipcal ON(cal.codtipocalle=tipcal.codtipocalle) ";
$sql .= " JOIN catastro.conexiones as cone ON (cone.codemp=clie.codemp AND cone.codsuc=clie.codsuc AND cone.nroinscripcion=clie.nroinscripcion) ";
$sql .= " JOIN facturacion.impresion_recibos as imp ON(imp.codemp=clie.codemp AND imp.codsuc=clie.codsuc AND imp.nroinscripcion=clie.nroinscripcion) ";
$sql .= "WHERE c.nrofacturacion = ".$nrofacturacion." AND c.codsuc = ".$codsuc." AND c.nrodocumento > 0 ".$sqls." ";
$sql .= " AND clie.estareg = 1 AND c.codciclo = ".$codciclo. " " . $sqlcant .  " " . $order . " ";

// die($sql); exit;

$consulta = $conexion->query($sql);
$items = $consulta->fetchAll();
$count = 0;


  foreach ($items as $row) {

    $count++;
    $importe   = $row["importe"];
    $direccion = $row["descripcioncorta"].' '.$row["calle"].' #'.$row["nrocalle"];

    $objreporte->Contenido($count, $row["codcatastro"], $row["codantiguo"], $row["propietario"], $direccion, $row["nrodocumento"], $importe);

  }

$objreporte->Output();
