<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 	= $_POST["codsuc"];
	$codciclo 	= $_POST["codciclo"];
	
	$facturacion = $objFunciones->datosfacturacion($codsuc, $codciclo);
	
	$nrofacturacion = $facturacion["nrofacturacion"] - 1;
	
	
	$sql = "SELECT anio, mes, fechaemision, fechavencnormal, fechavencdeudores, fechacorte, mensajenormal, ";
	$sql .= " mensajedeudores, mensajegeneral, fechavencimiento3 ";
	$sql .= "FROM facturacion.periodofacturacion ";
	$sql .= "WHERE codsuc = ".$codsuc." ";
	$sql .= " AND codciclo = ".$codciclo." ";
	$sql .= " AND nrofacturacion = ".$nrofacturacion." ";
	$sql .= "ORDER BY anio, mes";
			
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array());
	
	$items = $consulta->fetch();
	
	$fechEmision  = new DateTime($items["fechaemision"]);
	$fechaEmision = $fechEmision->format("d/m/Y");
	
	$fechVencimiento  = new DateTime($items["fechavencnormal"]);
	$fechaVencimiento = $fechVencimiento->format("d/m/Y");
	
	$fechDeudores  = new DateTime($items["fechavencdeudores"]);
	$fechaDeudores = $fechDeudores->format("d/m/Y");
	
	$fechCorte 	= new DateTime($items["fechacorte"]);
	$fechaCorte = $fechCorte->format("d/m/Y");
	
	$fechavencimiento3 	= new DateTime($items["fechavencimiento3"]);
	$fechavencimiento3 = $fechavencimiento3->format("d/m/Y");

	$mensajenormal= $items["mensajenormal"];
	$mensajedeudores=$items["mensajedeudores"];
	$mensajegeneral=$items["mensajegeneral"];
	
	//if($items["mensajenormal"]=="")
//	{
//		$sql = "select anio,mes,fechaemision,fechavencnormal,fechavencdeudores,fechacorte,mensajenormal,mensajedeudores,mensajegeneral
//			from facturacion.periodofacturacion
//			where codsuc=? and codciclo=? and nrofacturacion=?";
//			
//		$consulta = $conexion->prepare($sql);
//		$consulta->execute(array($codsuc, $codciclo, (intval($nrofacturacion)-1)));
//		$items2 = $consulta->fetch();
//		$mensajenormal= $items2["mensajenormal"];
//		$mensajedeudores=$items2["mensajedeudores"];
//		$mensajegeneral=$items2["mensajegeneral"];
//	}
	
	$sqlI = "select count(*) from facturacion.impresion_recibos where codemp=1 and codsuc=? and codciclo=? and nrofacturacion=?";
	
	$consultaI = $conexion->prepare($sqlI);
	$consultaI->execute(array($codsuc,$codciclo,$nrofacturacion));
	$itemsI = $consultaI->fetch();
	
	$ret  = $items["anio"]."|".$items["mes"]."|".$meses[$items["mes"]]."|".$fechaEmision."|".$fechaVencimiento."|".$fechaDeudores."|".$fechaCorte;
	$ret .= "|".$mensajenormal."|".$mensajedeudores."|".$mensajegeneral."|".$itemsI[0]."|".$fechavencimiento3;
	
	echo $ret;
?>