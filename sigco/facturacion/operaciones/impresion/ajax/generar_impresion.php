<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	
	set_time_limit(0);
	ini_set("display_errors",1);
	//ini_set("memory_limit","64M");
	set_time_limit(0);
	file_put_contents('../progreso.txt', '0|Calculando..');
	
	$objFunciones = new clsFunciones();
	
	$codemp = 1;
	
	$codciclo 	= $_POST["codciclo"];
	$codsuc 	= $_POST["codsuc"];
	$generado	= $_POST["generado"];
	$sector		= $_POST["sector"];
	$sector		= substr($sector, 0, strlen($sector) - 1);
	
	$facturacion 	= $objFunciones->datosfacturacion($codsuc, $codciclo);
	$paramae		= $objFunciones->getParamae("LIMLEC", $codsuc);
	$nrofacturacion = ($facturacion["nrofacturacion"] - 1);
	
	$conexion->beginTransaction();
	
	//NUEVO CODIGO
	$del = $conexion->prepare("DELETE FROM facturacion.impresion_recibos WHERE codsuc = ".$codsuc." AND codciclo = ".$codciclo." AND codsector IN (".$sector.")");
	$del->execute(array());

	$Sql = "SELECT fechaemision, fechavencnormal, fechavencdeudores, fechacorte, mensajenormal, mensajedeudores, mensajegeneral, fechavencimiento3 ";
	$Sql .= "FROM facturacion.periodofacturacion ";
	$Sql .= "WHERE codemp = 1 AND codsuc = ? AND codciclo = ? AND nrofacturacion = ?";
	
	$consultacpt = $conexion->prepare($Sql);
	$consultacpt->execute(array($codsuc, $codciclo, $nrofacturacion));
	$item = $consultacpt->fetch();
	
	$fechaemi	= $item['fechaemision'];
	$fechavencimientonorm = $item['fechavencnormal'];
	$mensajenor	= $item['mensajenormal'];
	$mensajedeu	= $item['mensajedeudores'];
	$mensajegen = $item['mensajegeneral'];
	$fechavencimiento3 = $item['fechavencimiento3'];
	
	
	//BUCLE DE SECTORES
	$Sql = "SELECT DISTINCT s.codsector, s.descripcion, fechadeudores, fechacorte ";
	$Sql .= "FROM public.sectores s ";
	$Sql .= " INNER JOIN facturacion.impresion_recibos_fechas f ON(s.codemp = f.codemp) AND (s.codsuc = f.codsuc) AND (s.codsector = f.codsector) ";
	$Sql .= "WHERE s.codemp = ".$codemp." AND s.codsuc = ".$codsuc." ";
	$Sql .= " AND codciclo = ".$codciclo." AND nrofacturacion = ".$nrofacturacion." ";
	$Sql .= " AND s.estareg = 1 AND s.codsector IN(".$sector.") ";
	$Sql .= "ORDER BY s.codsector ";
		 
	foreach($conexion->query($Sql)->fetchAll() as $rowS)	 
	{
		$fechavencimientodeud = $rowS['fechadeudores'];
		$fechcorte = $rowS['fechacorte'];
		$Sector .= "<li><span style='font-weight:bold;'>".$rowS['descripcion'].":</span>";
		
		$Sql = "SELECT clie.codemp, clie.codsuc, c.codciclo, c.nrofacturacion, c.nroinscripcion, clie.codrutlecturas, clie.lote, ";
		$Sql .= " c.anio, c.mes, c.nrodocumento, c.propietario, t.descripcion AS tiposervicio, c.tipofacturacionfact AS tipofacturacion, ";
		$Sql .= " clie.domestico, clie.social, clie.comercial, clie.estatal, clie.industrial, clie.nromed, c.fechalectanterior, c.lecturaanterior, ";
		$Sql .= " c.consumo, c.fechalectultima, c.lecturaultima, clie.codsector, clie.codmanzanas, clie.codrutdistribucion, ";
		$Sql .= " rut.nroorden AS orden_manzana, rutmae.orden AS orden_ruta, tipcal.descripcioncorta, cal.descripcion AS calle, clie.nrocalle, ";
		$Sql .= " td.abreviado AS docidentidad, clie.nrodocumento AS nrodocidentidad, clie.nroorden as nroorden, clie.codantiguo, clie.lecturapromedio, ";
		$Sql .= " CASE WHEN estmed.codestadomedidor = 0 THEN 'DIF. LECTURAS' ELSE estmed.descripcion END as estadomedidor, ";
		$Sql .= " tipcal.descripcioncorta || ' ' || cal.descripcion || ' ' || clie.nrocalle AS direccion, ";
		$Sql .= " TRIM(urb.tipo || ' ' || urb.descripcion) AS urbanizacion, ";
		$Sql .= " c.codsuc || '-1-' || clie.codsector || '-' || clie.codmanzanas || '-' || clie.lote || '-' || clie.sublote AS codcatastro, ";
		$Sql .= " clie.domestico + clie.social + clie.comercial + clie.estatal + clie.industrial AS totalcat, ";
		$Sql .= " c.consumofact, pf.fechavencnormal AS fechavennormal, f.fechadeudores AS fechavendeudor, pf.fechavencimiento3 AS fechaven3meses, ";
		$Sql .= " c.observacionabast AS horasabastecimiento ";
		$Sql .= "FROM facturacion.cabfacturacion c ";
		$Sql .= " INNER JOIN catastro.clientes clie ON(c.codemp = clie.codemp) AND (c.codsuc = clie.codsuc) AND (c.nroinscripcion = clie.nroinscripcion) ";
		$Sql .= " INNER JOIN catastro.conexiones con ON(c.codemp = con.codemp) AND (c.codsuc = con.codsuc) AND (c.nroinscripcion = con.nroinscripcion) ";
		$Sql .= " INNER JOIN public.tiposervicio t ON(clie.codtiposervicio = t.codtiposervicio) ";
		$Sql .= " INNER JOIN public.rutasdistribucion rut ON(clie.codemp = rut.codemp) AND (clie.codsuc = rut.codsuc) AND (clie.codzona = rut.codzona) AND (clie.codsector = rut.codsector) AND (clie.codrutdistribucion = rut.codrutdistribucion) AND (clie.codmanzanas = rut.codmanzanas) ";
		$Sql .= " INNER JOIN public.rutasmaedistribucion rutmae ON(rut.codemp = rutmae.codemp) AND (rut.codsuc = rutmae.codsuc) AND (rut.codzona = rutmae.codzona) AND (rut.codsector = rutmae.codsector) AND (rut.codrutdistribucion = rutmae.codrutdistribucion) ";
		$Sql .= " INNER JOIN public.calles cal ON(clie.codemp = cal.codemp) AND (clie.codsuc = cal.codsuc) AND (clie.codcalle = cal.codcalle) AND (clie.codzona = cal.codzona) ";
		$Sql .= " INNER JOIN public.tiposcalle tipcal ON(cal.codtipocalle = tipcal.codtipocalle) ";
		$Sql .= " INNER JOIN public.estadomedidor estmed ON(c.codestadomedidor = estmed.codestadomedidor) ";
		$Sql .= " INNER JOIN tipodocumento td ON(clie.codtipodocumento = td.codtipodocumento) ";
		$Sql .= " INNER JOIN facturacion.periodofacturacion pf ON(c.codemp = pf.codemp) AND (c.codsuc = pf.codsuc) AND (c.nrofacturacion = pf.nrofacturacion) AND (c.codciclo = pf.codciclo) ";
		$Sql .= " INNER JOIN facturacion.impresion_recibos_fechas f ON(c.codemp = f.codemp) AND (c.codsuc = f.codsuc) AND (c.codciclo = f.codciclo) AND (c.nrofacturacion = f.nrofacturacion) ";
		
		$Sql .= " LEFT JOIN catastro.urbanizaciones urb ON(clie.codsuc = urb.codsuc AND clie.codzona = urb.codzona AND clie.codurbanizacion = urb.codurbanizacion)";
		
//		$Sql .= " INNER JOIN catastro.urbanizaciones urb ON(clie.codurbanizacion = urb.codurbanizacion) ";
		
		$Sql .= "WHERE c.codemp = 1 AND c.codsuc = ".$codsuc." AND c.codciclo = ".$codciclo." ";
		$Sql .= " AND c.nrofacturacion = ".$nrofacturacion." ";
		$Sql .= " AND f.codsector = clie.codsector ";
		$Sql .= " AND clie.codsector = ".$rowS['codsector']." ";
		$Sql .= " AND c.nrodocumento <> 0 ";
		//$Sql .= " AND c.impmes = 0 ";
		//$Sql .= "ORDER BY c.nrodocumento ";
		
 		$consultaC = $conexion->prepare($Sql);
		$consultaC->execute();
		$itemsC = $consultaC->fetchAll();
		
		$totalAvance = count($itemsC);
		$actualAvance = 0;
		
		foreach($itemsC as $row)
		{
			$impmesagua    = 0;
			$impmesdsague  = 0;
			$impmesinteres = 0;
			$impmesigv     = 0;
			$impcargo      = 0;
			$impcred       = 0;
			$impref        = 0;
			$impred        = 0;
			$impfon        = 0;
			$nromes        = 0;
			$impColateral  = 0;
			$impdeu        = 0;
			$impvma2       = 0;
			$mesesgraf     = '';
			$consumograf   = '';
			$dir           = '';
			
			$consulta_deuda = $conexion->prepare("SELECT * FROM facturacion.f_recibos(?, ?, ?, ?, ?, ?, ?)");
			$consulta_deuda->execute(array($codsuc, $nrofacturacion, $row["nroinscripcion"], $paramae["valor"], $row["fechavennormal"], $row["fechavendeudor"], $row["fechaven3meses"]));
			$rowD = $consulta_deuda->fetch();
			
			$impmesagua    = $rowD["agua"]?$rowD["agua"]:0;
			$impmesdsague  = $rowD["desague"]?$rowD["desague"]:0;
			$impmesinteres = $rowD["interes"]?$rowD["interes"]:0;
			$impmesigv     = $rowD["igv"]?$rowD["igv"]:0;
			$impcargo      = $rowD["cargo"]?$rowD["cargo"]:0;
			$impColateral  = $rowD["colateral"]?$rowD["colateral"]:0;
			$impred        = $rowD["redondeo"]?$rowD["redondeo"]:0;
			$impfon        = $rowD["fonavi"]?$rowD["fonavi"]:0;
			$impvma2       = $rowD["vma"]?$rowD["vma"]:0;
			$impcred       = $rowD["credito"]?$rowD["credito"]:0;
			$impref        = $rowD["refinanciamiento"]?$rowD["refinanciamiento"]:0;	
			$totalfacturado        = $rowD["totalfacturado"]?$rowD["totalfacturado"]:0;	
			$nromes = $rowD['mesesdeuda'];
			$impdeu = $rowD['deuda'];
			$mesesgraf =$rowD['mesesgraf'];
			$consumograf =$rowD['consumograf'];
			$codca 	=  $row['codcatastro'];
			$dir	 	= $row['direccion'];
			
			$urbanizacion = $row['urbanizacion'];
			
			$tarifasfact = $rowD['tarifasfact'];
			$und = $rowD['ntarifasfact'];
			$nroorden=$row['nroorden']?$row['nroorden']:0;
			
			$Sql = "INSERT INTO facturacion.impresion_recibos ";
			$Sql .= " (codemp, codsuc, codciclo, nrofacturacion, nroinscripcion, codcatastro, anio, mes, ";
			$Sql .= " nrorecibo, fechaemision, propietario, direccion, servicios, tipofacturacion, ";
			$Sql .= " unidades, tarifa, nromedidor, fechalecturaanterior, lecturaanterior, ";
			$Sql .= " consumom3, fechalecturaultima, lecturaultima, mesesgrafico, consumografico, ";
			$Sql .= " impagua, impdesague, impinteres, impigv, ";
			$Sql .= " impcargofijo, impcredito, imprefinanciamiento, impredondeo, impfonavi, ";
			$Sql .= " nromeses, impdeuda, fechavencimientonormal, fechavencimientodeudores, fechacorte, ";
			$Sql .= " mensajenormal, mensajedeudores, mensajegeneral, ";
			$Sql .= " codsector, codmanzanas, codrutdistribucion, orden_rutas, ";
			$Sql .= " orden_manzana, docidentidad, nrodocidentidad, nroorden, ";
			$Sql .= " codantiguo, lecturapromedio, impvma, impcolateral, ";
			$Sql .= " fechavencimiento3, estadomedidor, consumofact, totalfacturado, horasabastecimiento, urbanizacion) ";
			$Sql .= "VALUES(1, ".$codsuc.", ".$codciclo.", ".$nrofacturacion.", ".$row['nroinscripcion'].", '".$codca."', '".$row['anio']."', '".$row['mes']."', ";
			$Sql .= " '".$row['nrodocumento']."', '".$fechaemi."', '".trim($row['propietario'])."', '".trim($dir)."', '".$row['tiposervicio']."', '".$row['tipofacturacion']."', ";
			$Sql .= " '".$und."', '".$tarifasfact."', '".$row['nromed']."', '".$row['fechalectanterior']."', '".$row['lecturaanterior']."', ";
			$Sql .= " '".$row['consumo']."', '".$row['fechalectultima']."', '".$row['lecturaultima']."', '".$mesesgraf."', '".$consumograf."', ";
			$Sql .= " '".round($impmesagua, 2)."', '".round($impmesdsague, 2)."', '".round($impmesinteres, 2)."', '".round($impmesigv, 2)."', ";
			$Sql .= " '".round($impcargo, 2)."', '".round($impcred, 2)."', '".round($impref, 2)."', '".round($impred, 2)."', '".round($impfon, 2)."', ";
			$Sql .= " '".$nromes."', '".$impdeu."', '".$fechavencimientonorm."', '".$fechavencimientodeud."', '".$fechcorte."', ";
			$Sql .= " '".$mensajenor."', '".$mensajedeu."', '".$mensajegen."', ";
			$Sql .= " '".$row['codsector']."', '".$row['codmanzanas']."', '".$row['codrutdistribucion']."', '".$row['orden_ruta']."', ";
			$Sql .= " '".$row['orden_manzana']."', '".$row['docidentidad']."', '".$row['nrodocidentidad']."', '".$nroorden."', ";
			$Sql .= " '".$row['codantiguo']."', '".$row['lecturapromedio']."', '".round($impvma2, 2)."', '".round($impColateral, 2)."', ";
			$Sql .= " '".$fechavencimiento3."', '".$row['estadomedidor']."', '".$row['consumofact']."', '".$totalfacturado."', '".$row['horasabastecimiento']."', ";
			$Sql .= " '".$urbanizacion."')";                                              
			
			$sql_generar = $conexion->query($Sql);
			
			if (!$sql_generar)
			{
			 	//$conexion->rollBack();
			 	$res = 2;
			 	$mensaje = "Error Cuando se ha Registrado los Datos";
				
				var_dump($conexion->errorInfo())."<br>";
				
			 	die($Sql."<br>".$row['nroinscripcion']);
			}
			
			$actualAvance++;
			$porcentajeAvance = round($actualAvance * 100/ $totalAvance);
			$textoAvance = $porcentajeAvance."|".$actualAvance." de ".$totalAvance."|".$Sector."<span class='icono-icon-loading'></span>";
			
			file_put_contents('../progreso.txt',$textoAvance);
		}
		
		$Sector .='<span class="icono-icon-ok" title="Recibos Generados Correctamente"></span>'.number_format($totalAvance).' Recibos Generados';
	}

   

	//NUEVO CODIGO
//select nroinscripcion,count(*) from catastro.unidadesusoclientes where codsuc=1 group by nroinscripcion having count(*)>3
	$textoAvance="100|".$totalAvance." de ".$totalAvance."|".$Sector;
	file_put_contents('../progreso.txt',$textoAvance);
	 //ACTUALIZAR FECHA POR SECTORES
	//var_dump($sql_generar->errorInfo());
	if(!$sql_generar)
	{
		$conexion->rollBack();
		
		$img 		= "<img src='".$_SESSION['urldir']."images/iconos/error.png' width='31' height='31' />";
		$mensaje 	= "Error al Tratar de Generar los Recibos";
		$res		= 0;
	}else{
		$conexion->commit();
		
		$img 		 = "<img src='".$_SESSION['urldir']."images/icoos/Ok.png' width='31' height='31' />";
		$mensaje 	 = "Los Recibos se han Generado Correctamente";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
	}
	
	echo $img."|".$mensaje."|".$res;
	
?>
