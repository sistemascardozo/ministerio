


$(function() {

	 $("#dialogo_barras").dialog({
        autoOpen:false,
        modal: true,
        closeOnEscape: false,
        title:"Generando Recibos. Espere por Favor",
        height:250,
        open: function(event, ui) {
            //ocultar el boton de cerrar en el dialogo.
            $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
        },
        buttons: [
                {
                    id: "button-cerrar",
                    text: "Aceptar",
                    click: function() {
                    	$("#dialogo_barras").dialog('close');
                    	$("#barra").progressbar({value: 0}); //actualizar la barra.
            			$("#titulo_barra").html('Calculando...'); //actualizar etiqueta.
            			clearInterval(timerBarra);


                    }
                }
            ]
    });
	 $("#button-cerrar").hide();
	$( "#tabs" ).tabs();
	$("#DivFecSe").dialog({
              autoOpen: false,
              modal: true,
              width: 450,
              height:450,
              resizable: false,
              show: "scale",
              hide: "scale",
              close: function() {

                },
              buttons: {
              "Cerrar": function() {
               $( this ).dialog( "close" );   }

            }
               //
             });
	$("#DivFecSeAcordion").accordion({
      heightStyle: "content"
    });
	$("#DivFecSe").dialog('close');
	$("#fechaemision,#fechanormal,#fechadeudores,#fechacorte").mask("99/99/9999");
	//$('#div_usuarios #ListaMenu').fixedHeaderTable({ height:'300', footer: true });
	$( "#fechaemision" ).datepicker(
	{
		showOn: 'button',
		direction: 'up',
		buttonImage: '../../../../images/iconos/calendar.png',
		buttonImageOnly: true,
		showOn: 'both',
		showButtonPanel: true
	});

	$("#DialogSectores").dialog({
              autoOpen: false,
              modal: true,
              width: 250,
              height:250,
              resizable: false,
              show: "scale",
              hide: "scale",
              close: function() {

                },
              buttons: {
              "Generar": function() {
              	var x=0;
              	tempsectores='';
              	var etiqueta='';
              	$('#DivSectores :input').each(function()
		       {
		        if($(this).attr("checked"))
		          {
		            x++
		            tempsectores += $(this).val() + ",";
		            etiqueta += "<li><span style='font-weight:bold;'>"+$(this).data('text')+":</span><span class='span_"+$(this).val()+"'></span></li>  ";
		          }
		      });
              	if(x==0)
              	{
              		Msj('#DivSectores','Seleccione Sectores')
              		return false;
              	}
              	//$("#titulo_barra").html(etiqueta);
              	generarOk();
               $( this ).dialog( "close" );   },
               "Cerrar": function() {
               $( this ).dialog( "close" );   }

            }
               //
             });

});
var tempsectores='';
$(function() {
	$("#fechanormal, #fechavencimiento3" ).datepicker(
	{
		showOn: 'button',
		direction: 'up',
		buttonImage: '../../../../images/iconos/calendar.png',
		buttonImageOnly: true,
		showOn: 'both',
		showButtonPanel: true
	});
});
$(function() {
	$( "#fechadeudores2" ).datepicker(
	{
		showOn: 'button',
		direction: 'up',
		buttonImage: '../../../../images/iconos/calendar.png',
		buttonImageOnly: true,
		showOn: 'both',
		showButtonPanel: true
	});
});
$(function() {
	$( "#fechacorte2" ).datepicker(
	{
		showOn: 'button',
		direction: 'up',
		buttonImage: '../../../../images/iconos/calendar.png',
		buttonImageOnly: true,
		showOn: 'both',
		showButtonPanel: true
	});
});
function VerSectores()
{
	$("#DivFecSe").dialog('open');
}
function datos_facturacion(codciclo)
{
	$.ajax({
		 url:'ajax/datos_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+codciclo+'&codsuc='+codsuc,
		 success:function(datos){
			var r=datos.split("|")

			$("#anio").val(r[0]);
			$("#mes").val(r[2]);
			$("#fechaemision").val(r[3])
			$("#fechanormal").val(r[4])
			$("#fechadeudores").val(r[5])
			$("#fechacorte").val(r[6])

			$("#mensajenormal").val(r[7])
			$("#mensajedeudores").val(r[8])
			$("#mensajegeneral").val(r[9])
			$("#control_generado").val(r[10])
			$("#fechavencimiento3").val(r[11])
		 }
	})
}
function cFechas(codciclo)
{
	$.ajax({
		 url:'ajax/datos_fechas.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+codciclo+'&codsuc='+codsuc,
		 success:function(datos){
			if(datos!='')
			{
				$("#DivFecSeAcordion").html('')
				$('#DivFecSeAcordion').append(datos).accordion('destroy').accordion({heightStyle: "content" });
				$('.fecha').not('.hasDatePicker').datepicker();
			}

		 }
	})
}
function grabar_mensaje()
{
	var femision 		= $("#fechaemision").val()
	var fvencnormal 	= $("#fechanormal").val()
	var fcorte			= $("#fechacorte").val()
	var fvencdeudores 	= $("#fechadeudores").val()

	var mensajenormal 	= $("#mensajenormal").val()
	var mensajedeudores	= $("#mensajedeudores").val()
	var mensajegeneral	= $("#mensajegeneral").val()
	var fechavencimiento3	= $("#fechavencimiento3").val()
	var Data = $(".fecha").serialize();

	$.ajax({
		 url:'Guardar.php',
		 type:'POST',
		 async:true,
		 data:Data+'&codciclo='+$("#ciclo").val()+'&codsuc='+codsuc+'&femision='+femision+'&fvencnormal='+fvencnormal+'&mensajenormal='+mensajenormal+
		 	  '&fvencdeudores='+fvencdeudores+'&mensajedeudores='+mensajedeudores+'&mensajegeneral='+mensajegeneral+'&fcorte='+fcorte+'&fechavencimiento3='+fechavencimiento3,
		 success:function(datos){
			var r=datos.split("|")
			Msj($("#guardar"),r[1])
		 }
	})
}
function generar_recibos_impresion()
{
	var generar=0;
	if($("#ciclo").val()==0)
	{
		Msj($("#ciclo"),"Seleccione el Ciclo")
		return false
	}

	// if($("#control_generado").val()>0)
	// {
	// 	var res=confirm("Los Recibos se han Generado.�Desea Volver a Generarlos?")
	// 	if(res==false)
	// 	{
	// 		return false
	// 	}else{
	// 		generar=1;
	// 	}
	// }
	$("#DialogSectores").dialog('open');


}
function generarOk()
{
	$("#dialogo_barras").dialog('open');
	$("#barra").show();
	$.ajax({
		 url:'ajax/generar_impresion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+$("#ciclo").val()+'&codsuc='+codsuc+'&generado='+generar+'&sector='+tempsectores,
		 success:function(datos){
			var r=datos.split("|")
			$("#button-cerrar").show();
			$("#barra").hide();
			if(r[2]==0)
			{
				window.parent.blokear_pantalla("")
				window.parent.establecer_texto(r[0],r[1])
			}
			$("#control_generado").val(r[2])
			$("#barra").hide();
			clearInterval(timerBarra);
			$.get('progreso.txt',function(texto){
		        var partes        = texto.split('|'),
		            porcentaje    = parseInt(partes[0]),
		            avance        = partes[1],
		            etiqueta      = partes[2]+" </li>";

		        $("#barra").progressbar({value: porcentaje}); //actualizar la barra.
		        $("#titulo_barra").html(etiqueta); //actualizar etiqueta.
		    });
		 }
	});
	timerBarra = setInterval(actualizarBarra,1000);
}
function actualizarBarra()
{
    //leer el archivo de texto que contiene el procentaje y avance del proceso principal.
    $.get('progreso.txt',function(texto){
        var partes        = texto.split('|'),
            porcentaje    = parseInt(partes[0]),
            avance        = partes[1],
            etiqueta      = partes[2]+' ' + partes[0] + "% (" + partes[1] + ") </li>";

        //if (porcentaje == 100) //llegamos al 100% fin de proceso.
          //  clearInterval(timerBarra); //borrar timer.

        $("#barra").progressbar({value: porcentaje}); //actualizar la barra.
        $("#titulo_barra").html(etiqueta); //actualizar etiqueta.
    });
}
function cargar_rutas_distribucion(codsector)
{
	$.ajax({
		 url:urldir+'ajax/rutas_distribucion_drop.php',
		 type:'POST',
		 async:true,
		 data:'codsector='+codsector+'&codsuc='+codsuc,
		 success:function(datos){
			$("#div_rutasdistribucion,#div_rutasdistribucion_2").html(datos)
			redo_select();
		 }
	})
}

function validar_ciclosectorrutas(){
	if($("#ciclo").val()==0)
	{
		alert("Seleccione el Ciclo de Facturacion")
		return
	}
	if($("#sector").val()==0)
	{
		alert("Seleccione el Sector")
		return
	}
	if($("#rutasdistribucion").val()==0)
	{
		alert("Seleccione la Ruta de Distribucion Inicial")
		return
	}

	if($("#rutasdistribucion_fin").val()==0)
	{
		alert("Seleccione la Ruta de Distribucion Final")
		return
	}

	if(parseInt($("#rutasdistribucion").val()) > parseInt($("#rutasdistribucion_fin").val()) )
	{
		alert("El codigo de la Ruta Inicial no debe ser mayor que el de la Ruta Final")
		return
	}
	return true;
}
function cargar_usuarios()
{

	if(!validar_ciclosectorrutas()) return
	$("#ListaMenu tbody").html('<span class="icono-icon-loading">&nbsp;&nbsp;Consultando...</span>')
	var tipoentidades = $("#tipoentidades").val();
	var todo = $("#todosgrupos").val();
	$.ajax({
		 url:'ajax/mostrar_usuarios.php',
		 type:'POST',
		 async:true,
		 data:'codsector='+$("#codsector").val()+'&codruta_ini='+$("#rutasdistribucion").val()+'&codruta_fin='+$("#rutasdistribucion_fin").val()+'&codsuc='+codsuc+'&codciclo='+$("#ciclo").val()+'&tipoentidades='+tipoentidades+'&todosgrupos='+todo+'&cantidad_meses='+$("#mesesdeuda").val(),
		 success:function(datos){
			$("#ListaMenu tbody").html(datos)
			$("#cont").val(1)
		 }
	})
}
function imprimir_recibos_facturacion()
{
	var url = ""

	if($("#cont").val()==0)
	{
		alert("No hay Ningun Usuario Mostrado para Imprimir los Recibos")
		return
	}
	if($("#control_generado").val()==0)
	{
		alert("Aun no se han Generado los Recibos Pertenecientes a esta Facturacion")
		return
	}

	if(!validar_ciclosectorrutas()) return

	if($("#codinicial").val()==0)
	{
		alert("Seleccione un Cod Inicial")
		return
	}

	if($("#codfinal").val()==0)
	{
		alert("Seleccione un Cod Final")
		return
	}

	url  = "?codciclo="+$("#ciclo").val()+"&codsector="+$("#codsector").val()+"&codruta_ini="+$("#rutasdistribucion").val()+"&codruta_fin="+$("#rutasdistribucion_fin").val()
	url += "&codsuc="+codsuc+"&orden="+$("#orden").val()+"&codinicio="+$("#codinicial").val()+"&codfinal="+$("#codfinal").val()+"&todosgrupos="+$("#todosgrupos").val()+"&tipoentidades="+$("#tipoentidades").val()
	url += '&cantidad_meses='+$("#mesesdeuda").val()
	//AbrirPopupImpresion("imprimir_unico.php?ruta="+ruta+"&nrofacturacion=<?=$nrofacturacion?>&codinicial="+$("#codinicial").val()+"&codfinal="+$("#codfinal").val(),800,600)
	AbrirPopupImpresion("imprimir.php"+url,800,600)
}

function imprimir_cargo_recibos(){

	if($("#cont").val()==0)
	{
		alert("No hay Ningun Usuario Mostrado para Imprimir los Recibos")
		return
	}
	if($("#control_generado").val()==0)
	{
		alert("Aun no se han Generado los Recibos Pertenecientes a esta Facturacion")
		return
	}

	if(!validar_ciclosectorrutas()) return
	if($("#tipoentidades").val()==0)
	{
		//alert("Seleccione un Grupo")
		//return
	}
	var todo = $("#todosgrupos").val();
	url  = "?codciclo="+$("#ciclo").val()+"&codsector="+$("#codsector").val()+"&codruta_ini="+$("#rutasdistribucion").val()+"&codruta_fin="+$("#rutasdistribucion_fin").val();
	url += "&codsuc="+codsuc+"&orden="+$("#orden").val()+"&codinicio="+$("#codinicial").val()+"&codfinal="+$("#codfinal").val()+"&grupo="+$("#tipoentidades").val()+'&todosgrupos='+todo

	AbrirPopupImpresion("imprimir_cargo.php"+url,800,600)

}

function redo_select(){
	$("#div_rutasdistribucion_2").find('select').attr('id','rutasdistribucion_fin').attr('name','rutasdistribucion_fin');
}

$(document).ready(function(){
	redo_select();
});
