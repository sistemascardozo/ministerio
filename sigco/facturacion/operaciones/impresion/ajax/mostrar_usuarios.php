<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../objetos/clsFunciones.php");

	$objFunciones = new clsFunciones();

	//header ('Content-type: text/html; charset=utf-8');

	$codsector = $_POST["codsector"];
	$codruta_ini = $_POST["codruta_ini"];
	$codruta_fin = $_POST["codruta_fin"];
	$codrubro = $_POST["codrubro"];
	$codsuc = $_POST["codsuc"];
	$codciclo = $_POST["codciclo"];
	$tipoentidades = $_POST["tipoentidades"];
	$todogrupos = $_POST["todosgrupos"];
	$cantidad_meses = $_POST["cantidad_meses"];
	$orden = 0;

	// var_dump($_POST);exit;

	$facturacion = $objFunciones->datosfacturacion($codsuc, $codciclo);

	$nrofacturacion = ($facturacion["nrofacturacion"] - 1);



	if($orden == 1){
		$order = "ORDER BY tipcal.descripcioncorta, cal.descripcion, clie.nrocalle ";
	}else{
		$order = "ORDER BY c.nrodocumento";
	}

if ($todogrupos == 1)
{
    //$sqls = " AND cone.codtipoentidades = ".$tipoentidades;
   //  $sqls = " AND clie.codrutdistribucion>=".$codruta_ini;
   // $sqls .= " AND clie.codrutdistribucion<=".$codruta_fin;
    $sqls .= " AND cone.codtipoentidades <> 0 ";
    //$sqls .= "AND clie.codsector= ".$codsector;

}
else
{
    if($tipoentidades <> 0)
    {

        $sqls .= " AND cone.codtipoentidades = ".$tipoentidades;

    }
    else
    {
        $sqls = " AND CAST(clie.codrutlecturas AS INTEGER) >= ".$codruta_ini;
        $sqls .= " AND CAST(clie.codrutlecturas AS INTEGER) <= ".$codruta_fin;
        $sqls .= " AND cone.codtipoentidades = ".$tipoentidades;
        $sqls .= "AND clie.codsector = ".$codsector;
    }
}

switch ($cantidad_meses):
	case 1:	$sqlcant = ' ' ;break;
	case 2: $sqlcant = ' AND imp.nromeses = 0' ;break;
	case 3:	$sqlcant = ' AND imp.nromeses >= 1' ;break;
endswitch;

$count = 0;


$sql = "SELECT DISTINCT c.nroinscripcion,clie.propietario,c.nrodocumento,tipcal.descripcioncorta,cal.descripcion as calle,clie.nrocalle,";
$sql .= $objFunciones->getCodCatastral("clie.").",clie.nroorden ,clie.codantiguo
    FROM facturacion.cabfacturacion as c
     JOIN catastro.clientes as clie ON(c.codemp=clie.codemp and c.codsuc=clie.codsuc and c.nroinscripcion=clie.nroinscripcion)
     JOIN public.calles as cal ON(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
     JOIN public.tiposcalle as tipcal ON(cal.codtipocalle=tipcal.codtipocalle)
     JOIN catastro.conexiones as cone ON (cone.codemp=clie.codemp AND cone.codsuc=clie.codsuc AND cone.nroinscripcion=clie.nroinscripcion)
		 JOIN facturacion.impresion_recibos as imp ON(imp.codemp=clie.codemp AND imp.codsuc=clie.codsuc AND imp.nroinscripcion=clie.nroinscripcion)
		WHERE c.nrofacturacion = ".$nrofacturacion." AND c.codsuc = ".$codsuc." AND c.nrodocumento > 0 ".$sqls."
    	AND clie.estareg = 1 AND c.codciclo = ".$codciclo. " " . $sqlcant .  " " . $order . " ";

$consulta = $conexion->prepare($sql);
$consulta->execute(array());
$items = $consulta->fetchAll();

foreach ($items as $row) {
    $count++;
    ?>
    <tr>
        <td align="center"><?=$count ?>
            <td align="center"><?=$row["codcatastro"] ?></td>
        <td align="center"><?=$row["nrodocumento"] ?>
            <input type="hidden" name="codcatas<?=$count ?>" id="codcatas<?=$count ?>" value="<?=$row["nrodocumento"] ?>" /></td>

        <td align="center"><?=$row["codantiguo"] ?></td>
        <td align="left" style="padding-left:5px;" ><?=$row["propietario"] ?></td>
        <td align="left" style="padding-left:5px;" ><?=$row["descripcioncorta"].' '.$row["calle"].' #'.$row["nrocalle"] ?></td>
        <td align="center">
            <span class="icono-icon-detalle" title="Seleccione el Codigo de Catastro Inicial" onclick="Inicial(<?=$count ?>)"></span>
        </td>
        <td align="center">
            <span class="icono-icon-detalle" title="Seleccione el Codigo de Catastro Final" onclick="Final(<?=$count ?>)"></span>
        </td>
    </tr>
<?php } ?>
