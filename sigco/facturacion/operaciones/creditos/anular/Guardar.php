<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../../objetos/clsFunciones.php");
    include("../../../../../objetos/clsseguridad.php");

    $conexion->beginTransaction();

    $Op = $_GET["op"];

    $objFunciones = new clsFunciones();
    $objSeguridad = new clsSeguridad();

    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $nrocredito = $_POST["nrocredito"];
    $npresupuesto = $_POST["npresupuesto"]?$_POST["npresupuesto"] : 0;
    
    $fecha = $objFunciones->FechaServer();
    $hora = $objFunciones->HoraServidor();
    $idusuario = $_SESSION["id_user"];

    $upd = "UPDATE facturacion.cabcreditos set estareg=0,horaanulacion=?,fechaanulacion=?,creadoranulacion=?";
    $upd .= " WHERE codemp=1 AND codsuc=? AND nrocredito=?";
    $resultC = $conexion->prepare($upd);
    $resultC->execute(array($hora, $fecha, $idusuario, $codsuc, $nrocredito));
    if ($resultC->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }

    $updpresupuesto = "UPDATE solicitudes.cabpresupuesto set concredito=0
    WHERE codemp=1 AND codsuc=? AND nropresupuesto=?";
    $resultP = $conexion->prepare($updpresupuesto);
    $resultP->execute(array($codsuc, $npresupuesto));
    if ($resultP->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }

    $objSeguridad->seguimiento(1, $codsuc, 5, $idusuario, "SE HA ANULADO EL CREDITO CON NUMERO ".$nrocredito, 145);
    ///////////ANULAR COBRANZA//////////
    $nropago = $_POST['nropago'];
    if ($nropago != 0) {
        $motivo = 'SE ANULO EL CREDITO';
        $upd = "UPDATE cobranza.cabpagos set anulado=1,motivoanulacion=? WHERE codemp=1 AND codsuc=? ";
        $upd .= " AND nropago=?";

        $result = $conexion->prepare($upd);
        $result->execute(array($motivo, $codsuc, $nropago));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error UPDATE cabpagos";
            die(2);
        }

        $sqlpagos = "SELECT codconcepto,importe,tipopago,nroinscripcion,nrofacturacion,nropresupuesto 
            FROM cobranza.detpagos 
            WHERE codemp=1 AND codsuc=?";
        $sqlpagos .= " AND nropago=?";

        $result = $conexion->prepare($sqlpagos);
        $result->execute(array($codsuc, $nropago));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error SELECT detpagos";
            die(2);
        }
        $itemsP = $result->fetchAll();

        foreach ($itemsP as $rowP) {
            if ($rowP["tipopago"] == 0) {
                $updfact = "UPDATE facturacion.detfacturacion set 
                    imppagado=imppagado-:imppagado,estadofacturacion=1 WHERE ";
                $updfact .= "codemp=1 AND codsuc=:codsuc AND nrofacturacion=:nrofacturacion";
                $updfact .= " AND nroinscripcion=:nroinscripcion AND codconcepto=:codconcepto";
            } else {
                $updfact = "UPDATE facturacion.detfacturacion set imppagado=imppagado-:imppagado,estadofacturacion=1,";
                $updfact .= "importeacta=importeacta-:imppagado WHERE codemp=1 AND codsuc=:codsuc AND ";
                $updfact .= "nroinscripcion=:nroinscripcion AND codconcepto=:codconcepto AND nrofacturacion=:nrofacturacion";
            }
                        
            $result = $conexion->prepare($updfact);
            $result->execute(array(":codsuc" => $codsuc,
                ":nrofacturacion" => $rowP["nrofacturacion"],
                ":nroinscripcion" => $rowP["nroinscripcion"],
                ":codconcepto" => $rowP["codconcepto"],
            ));
            
            if ($result->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error UPDATE detfacturacion";
                die(2);
            }           
                                             
            
        }
        
    }
    
        $UpdDet="UPDATE facturacion.detcreditos SET estadocuota=2 WHERE codemp=1 and codsuc=? and nrocredito=? AND estadocuota=0 ";
        $result = $conexion->prepare($UpdDet);
        $result->execute(array($codsuc, $nrocredito));
        //$conD = $conD->fetch();                
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error UPDATE detcreditos";
            die(2);
        }   
    ///////////ANULAR COBRANZA//////////

    $conexion->commit();
    echo $res = 1;
?>
