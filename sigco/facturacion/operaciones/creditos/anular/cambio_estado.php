<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../../objetos/clsFunciones.php");
	
    $conexion->beginTransaction();

    $codsuc         = $_SESSION['IdSucursal'];
    $codiclo        = 1;
    $codemp         = 1;
    $nrocredito     = $_POST['nrocredito'];
    $nroinscripcion = $_POST['nroinscripcion'];
    $estadocuota    = $_POST['estadocuota'];
    $nrocuota       = $_POST['nrocuota'];
    $importe        = $_POST['importe'];

    // Para obtener el periodo de faturacion

    $sqlLect = "select (max(nrofacturacion) - 1) as nrofacturacion
                FROM facturacion.periodofacturacion
                WHERE codemp = ? AND codsuc = ? AND codciclo = ?";
    $consultaL      = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$codiclo));
    $nrofact        = $consultaL->fetch();
    $nrofacturacion = $nrofact['nrofacturacion'];

    if ($estadocuota == 1):

        $sql  = "UPDATE facturacion.detfacturacion SET estadofacturacion = 4";
        $sql .= " WHERE codemp=1 AND codsuc=? AND nrocuota=? AND nrocredito=? AND nrofacturacion=? AND nroinscripcion=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $nrocuota, $nrocredito,$nrofacturacion,$nroinscripcion));
        
        if ($consulta->errorCode() != '00000'):
            $conexion->rollBack();
            $mensaje = "Error en detalle-facturacion";
            $res = 2;
            echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
        endif;

        $sql  = "UPDATE facturacion.cabfacturacion SET impmes = impmes - " . $importe;
        $sql .= " WHERE codemp=1 AND codsuc=? AND nrofacturacion=? AND nroinscripcion=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc,$nrofacturacion,$nroinscripcion));

        if ($consulta->errorCode() != '00000'):
            $conexion->rollBack();
            $mensaje = "Error en cabecera-facturacion";
            $res = 2;
            echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
        endif;

        // Actualizacion de Cuenta Corriente

        $sql  = "UPDATE facturacion.detctacorriente SET estadofacturacion = 4";
        $sql .= " WHERE codemp=1 AND codsuc=? AND nrocuota=? AND nrocredito=? AND nrofacturacion=? AND nroinscripcion=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $nrocuota, $nrocredito,$nrofacturacion,$nroinscripcion));
        
        if ($consulta->errorCode() != '00000'):
            $conexion->rollBack();
            $mensaje = "Error en detalle-ctacorriente";
            $res = 2;
            echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
        endif;

        $sql  = "UPDATE facturacion.cabctacorriente SET impmes = impmes - " . $importe;
        $sql .= " WHERE codemp=1 AND codsuc=? AND nrofacturacion=? AND nroinscripcion=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc,$nrofacturacion,$nroinscripcion));

        if ($consulta->errorCode() != '00000'):
            $conexion->rollBack();
            $mensaje = "Error en cabecera-ctacorriente";
            $res = 2;
            echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
        endif;

        // Fin

        $estadocuota = 2;
        $mensaje     = '<p style="color:green;font-weight: bold;">ANULADO</p>';
          
    else :

        $sql  = "UPDATE facturacion.detfacturacion SET estadofacturacion = 1";
        $sql .= " WHERE codemp=1 AND codsuc=? AND nrocuota=? AND nrocredito=? AND nrofacturacion=? AND nroinscripcion=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $nrocuota, $nrocredito,$nrofacturacion,$nroinscripcion));
        
        if ($consulta->errorCode() != '00000'):
            $conexion->rollBack();
            $mensaje = "Error en detalle-facturacion";
            $res = 2;
            echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
        endif;

        $sql  = "UPDATE facturacion.cabfacturacion SET impmes = impmes + " . $importe;
        $sql .= " WHERE codemp=1 AND codsuc=? AND nrofacturacion=? AND nroinscripcion=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc,$nrofacturacion,$nroinscripcion));

        if ($consulta->errorCode() != '00000'):
            $conexion->rollBack();
            $mensaje = "Error en cabecera-facturacion";
            $res = 2;
            echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
        endif;

        // Actualizacion de Cuenta Corriente

        $sql  = "UPDATE facturacion.detctacorriente SET estadofacturacion = 1 ";
        $sql .= " WHERE codemp=1 AND codsuc=? AND nrocuota=? AND nrocredito=? AND nrofacturacion=? AND nroinscripcion=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $nrocuota, $nrocredito,$nrofacturacion,$nroinscripcion));
        
        if ($consulta->errorCode() != '00000'):
            $conexion->rollBack();
            $mensaje = "Error en detalle-ctacorriente";
            $res = 2;
            echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
        endif;

        $sql  = "UPDATE facturacion.cabctacorriente SET impmes = impmes + " . $importe;
        $sql .= " WHERE codemp=1 AND codsuc=? AND nrofacturacion=? AND nroinscripcion=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc,$nrofacturacion,$nroinscripcion));

        if ($consulta->errorCode() != '00000'):
            $conexion->rollBack();
            $mensaje = "Error en cabecera-ctacorriente";
            $res = 2;
            echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
        endif;
        // Fin

        $estadocuota = 1; 
        $mensaje = '<p style="color:green;font-weight: bold;">FACTURADO</p>';
    
    endif;

    // Modificaciones echas por un usurio

    $IdControl = $nrocredito;

    $SqlControl = "INSERT INTO admin.control (codsuc, tabla, id, operacion) ";
    $SqlControl .= "VALUES(".$_SESSION['IdSucursal'].", 'facturacion{}cabcreditos', '".$IdControl."', 1)";
    $resultControl = $conexion->prepare($SqlControl);
    $resultControl->execute(array());

    // Modificaciones echas por un usurio

    $IdControl = $nrofacturacion."|".$nroinscripcion;

    $SqlControl = "INSERT INTO admin.control (codsuc, tabla, id, operacion) ";
    $SqlControl .= "VALUES(".$_SESSION['IdSucursal'].", 'facturacion{}cabfacturacion', '".$IdControl."', 1)";
    $resultControl = $conexion->prepare($SqlControl);
    $resultControl->execute(array());

    $sql  = "UPDATE facturacion.detcreditos SET estadocuota = ?";
    $sql .= " WHERE codsuc=? AND nrocuota=? AND nrocredito=? AND codemp=1";
    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($estadocuota, $codsuc, $nrocuota, $nrocredito));

    if ($consulta->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        $res = 2;
        echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
    } 
    else {
        $conexion->commit();
        $res = 1;
        echo json_encode(array("res"=>$res,"mensaje"=>$mensaje,"nrocuota"=>$nrocuota,"estadocuota"=>$estadocuota));
    }
?>