<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../include/main.php");
include("../../../../../include/claseindex.php");
$TituloVentana = "ANULACIÓN CREDITOS";
$Activo = 1;
CuerpoSuperior($TituloVentana);
$Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
$codsuc = $_SESSION['IdSucursal'];
$FormatoGrilla = array();
$documento = 8;
$Criterio = 'aCreditos';
$Sql = "SELECT c.nrocredito,c.codantiguo,c.propietario," . $objFunciones->FormatFecha('c.fechareg').",
    CASE WHEN c.estareg = 0 THEN 'ANULADO' ELSE 'PENDIENTE' END,
    ".$objFunciones->Convert("c.imptotal", "NUMERIC (18,2)").",
    u.login,1,
    c.nroprepagoinicial,c.nroinscripcion,
    CASE WHEN c.nroprepagoinicial = 0 THEN 0 ELSE 1     END,
    CASE WHEN c.nroprepago = 0 THEN 0 ELSE 1 END,c.nroprepago,c.nroinspeccion,
    CASE WHEN c.nroinspeccion = 0 THEN 0 ELSE 1 END,c.nroprepagoinicial,
    CASE WHEN c.nroprepagoinicial = 0 THEN 0 ELSE 1 END,c.estareg
    FROM facturacion.cabcreditos as c
    INNER JOIN seguridad.usuarios as u ON (c.codemp=u.codemp and c.creador=u.codusu)";

$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
$FormatoGrilla[1] = array('1' => 'c.nrocredito', '2' => 'c.nroinscripcion', '3' => 'c.propietario', '4' => 'c.codantiguo');          //Campos por los cuales se hará la búsqueda
$FormatoGrilla[2] = $Op;                                                            //Operacion
$FormatoGrilla[3] = array('T1' => 'Nro. Credito', 'T2' => 'Nro.Inscripcion', 'T3' => 'Propietario', 'T4' => 'Fecha Emision', 'T5' => 'Estado',
    'T6' => 'Total', 'T7' => 'Usuario');   //Títulos de la Cabecera
$FormatoGrilla[4] = array('A1' => 'center', 'A3' => 'left', 'A4' => 'center', 'A6' => 'right', 'A7' => 'right', 'A8' => 'right');                        //Alineación por Columna
$FormatoGrilla[5] = array('W1' => '90', 'W4' => '90', 'W6' => '70', 'W7' => '60', 'W7' => '70', 'W8' => '50');                                 //Ancho de las Columnas
$FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
$FormatoGrilla[7] = 1100;                                                           //Ancho de la Tabla
$FormatoGrilla[8] = "  and (c.codemp=1 and c.codsuc=" . $codsuc . ") ORDER BY c.nrocredito desc ";                                   //Orden de la Consulta
$FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
    'NB' => '1', //Número de Botones a agregar
    'BtnId1' => 'BtnModificar', //Nombre del Boton
    'BtnI1' => 'eliminar.png', //Imagen a mostrar
    'Btn1' => 'Anular Credito', //Titulo del Botón
    'BtnF1' => 'onclick="Anular(this);"', //Eventos del Botón
    'BtnCI1' => '18', //Item a Comparar
    'BtnCV1' => '1'    //Valor de comparación
);
$FormatoGrilla[11] = 7; //FILAS VISIBLES   
$FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 1), array('Name' => 'estado', 'Col' => 18)); //DATOS ADICIONALES             
$_SESSION['Formato' . $Criterio] = $FormatoGrilla;
Cabecera('', $FormatoGrilla[7], 1000, 600);
Pie();
?>
<script type="text/javascript">
    function Anular(obj)
    {
        var Id = $(obj).parent().parent().data('id')
        var Estado = $(obj).parent().parent().data('estado')
        if (Estado == 0)
        {
            alert('No se puede Anular el Credito...Puede que se encuentre Anulado')
            return false
        }


        $("#Modificar").dialog({title: 'Anular Credito'});
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");

        $("#button-maceptar").children('span.ui-button-text').text('Anular Credito')
        $.ajax({
            url: 'mantenimiento.php',
            type: 'POST',
            async: true,
            data: 'Id='+Id+'&codsuc=<?=$codsuc ?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function Imprimir(Id)
    {
        AbrirPopupImpresion("imprimir.php?Id="+Id+"&codsuc=<?=$codsuc ?>", 800, 600)
    }
    $("#BtnNuevoB").remove()
</script>

<?php CuerpoInferior(); ?>