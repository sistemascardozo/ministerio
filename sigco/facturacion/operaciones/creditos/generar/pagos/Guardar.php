<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();} 
	
	include("../../../../../../objetos/clsFunciones.php");
	
	
	
	$Op  	= $_GET["op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp         = 1;
	$codsuc         = $_SESSION['IdSucursal'];
	$nroinscripcion = $_POST["nroinscripcion"];
	$imptotal       = $_POST["imptotal"];
	$igv            = $_POST["igv"];
	$redondeo       = isset($_POST["redondeo"])?$_POST["redondeo"]:0;
	$subtotal       = $_POST["subtotal"];
	$codconcepto    = $_POST['codconceptoC1'];//0;//$_POST["codconcepto"];
	$nropresupuesto = $_POST["codexpediente"]?$_POST["codexpediente"]:0;
	$observacion    = $_POST["observacion"];
	$codusu         = $_SESSION['id_user'];
	$femision       = $objFunciones->CodFecha($_POST["femision"]); 
	$count          = $_POST["count"];
	$interes        = $_POST["interescredito"]?$_POST["interescredito"]:0;
	$nroprepago     = $_POST["nprepago"]?$_POST["nprepago"]:0;
	$sininteres     = $_POST["sininteres"]?$_POST["sininteres"]:0;
	$nrocredito     = $_POST['nrocredito'];
	$cuotacancelada = $_POST['check_deudax'];
	//if($_POST['cuotainicialflag']==1)
	//	$nropago = $objFunciones->setCorrelativosVarios(5, $codsuc, "SELECT", 0);
	$nropago =0;
	$nroprepagocre = $objFunciones->setCorrelativosVarios(6,$codsuc,"SELECT",0);
	$creoprepago=false;
	$conexion->beginTransaction();



		//INSERTAR UN PREPAGO POR EL MONTO TOTAL
		$sql = "SELECT c.codcliente,c.propietario,c.codtipodocumento,c.nrodocumento,c.codciclo,tc.descripcioncorta,
            cl.descripcion as calle,c.nrocalle,c.codciclo,td.abreviado,c.codsector,
            c.codcalle
            FROM catastro.clientes as c
            INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
            inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
            where c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

        $result = $conexion->prepare($sql);
        $result->execute(array($codsuc, $nroinscripcion));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error SELECT Catastro";
            $data['res']    =2;
            die(json_encode($data)) ;
        }
        $item = $result->fetch();
        $propietario = strtoupper($item["propietario"]);
        $direccion = strtoupper($item["descripcioncorta"]." ".$item["calle"]." ".$item["nrocalle"]);
        $docidentidad = strtoupper($item["nrodocumento"]);
        if($item["propietario"]=='')
        {
            $propietario = strtoupper($_POST["nombres"]);
            $direccion = strtoupper($_POST["direccion"]);
            $docidentidad = strtoupper($_POST["docidentidad"]);
        }
		$creoprepago=true;
		$condpago = $_POST["condpago"];
	    $documento = 16;//nota de pago
	    $formapago = 1;
	    $fechareg = date('Y-m-d');
	    $default = array('6' =>9, '2'=>4, '3'=>2);//CAR DEFECTO
        $car = $_POST["carx"];
        if($car=='') $car = $default[$codsuc];
	    $nrocaja = $_SESSION["nocaja"];
	    $idusuario = $_SESSION['id_user'];
	     $hora = $objFunciones->HoraServidor();
	     $eventualtext=0;
	    $nrofacturacion=0;
	    $npresupuesto=0;
	     $glosa = 'PAGO ADELANTADO DE CUOTA '.$cuotacancelada ;//$_POST["glosa"];
		 
		$sqlC = "INSERT INTO cobranza.cabprepagos(codemp, codsuc, nroinscripcion, nroprepago, car, codformapago, creador, fechareg, ";
		$sqlC .= " subtotal, hora, codciclo, condpago, igv, redondeo, ";
		$sqlC .= " imptotal, eventual, propietario, direccion, documento, glosa, nroinspeccion, estado, nrocredito,origen) ";
		$sqlC .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$car.", ".$formapago.", ".$idusuario.", '".$fechareg."', ";
		$sqlC .= " '".str_replace(",", "", $_POST['subtotal'])."', '".$hora."', 1, ".$condpago.", '". $_POST['igv']."', '".$redondeo."', ";
		$sqlC .= " '".str_replace(",", "", $_POST['imptotal'])."', ".$eventualtext.", '".$propietario."', '".$direccion."', '".$docidentidad."', ";
		$sqlC .= " '".$glosa."', 0, 1, ".$nrocredito.", 3)";
		
		$resultC = $conexion->prepare($sqlC);
		$resultC->execute(array());
		
		if($resultC->errorCode()!='00000')
		{
		    $conexion->rollBack();
		    $mensaje = "Error update cabpagos";
		    $data['res']    =2;
		    die(json_encode($data)) ;
		}
		
		$DocSeries= $objFunciones->GetSeries($codsuc,$documento);
		$seriedoc = $DocSeries["serie"];
		$numerodoc = $DocSeries["correlativo"];
		$fechas = $objFunciones->DecFechaLiteral();
	    $anio = $fechas["anio"];
	    $mes = $fechas["mes_num"];
	    $facturacion = 0;
		
		$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
		$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, ";
		$sqlD .= " nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
		$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", 1, ".$_POST['codconcepto'].", ";
		$sqlD .= " '".str_replace(",", "", $_POST['subtotal'])."', '".$_POST['Colateral']."', 0, ".$nrofacturacion.", ".$documento.", ";
		$sqlD .= " ".$numerodoc.", '".$seriedoc."', 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
		//$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL

		$resultD = $conexion->prepare($sqlD);
		$resultD->execute(array());

		if($resultD->errorCode()!='00000')
		{
		    $conexion->rollBack();
		    $mensaje = "Error update cabpagos";
		    $data['res']    =2;
		    die(json_encode($data)) ;
		}
		//IGV
		if ($_POST['igv'] > 0)
		{
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, ";
			$sqlD .= " nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", 2, 5, ";
			$sqlD .= " '".str_replace(",", "", $_POST['igv'])."', 'I.G.V.', 0, ".$nrofacturacion.", ".$documento.", ";
			$sqlD .= " ".$numerodoc.", '".$seriedoc."', 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			//$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL
	
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
			if($resultD->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update cabpagos";
				$data['res']    =2;
				die(json_encode($data)) ;
			}
		}
		//REDONDEO
		if ($redondeo < 0)
		{
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, ";
			$sqlD .= " nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", 2, 7, ";
			$sqlD .= " '".$redondeo."', 'DIFERENCIA REDONDEO NEGATIVO', 0, ".$nrofacturacion.", ".$documento.", ";
			$sqlD .= " ".$numerodoc.", '".$seriedoc."', 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			//$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL
	
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
			if($resultD->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update cabpagos";
				$data['res']    =2;
				die(json_encode($data)) ;
			}
		}
		if ($_POST['redondeo'] > 0)
		{
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, ";
			$sqlD .= " nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", 2, 8, ";
			$sqlD .= " '".$redondeo."', 'DIFERENCIA REDONDEO POSITIVO', 0, ".$nrofacturacion.", ".$documento.", ";
			$sqlD .= " ".$numerodoc.", '".$seriedoc."', 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			//$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL
	
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
			if($resultD->errorCode()!='00000')
			{
				$conexion->rollBack();
				$mensaje = "Error update cabpagos";
				$data['res']    =2;
				die(json_encode($data)) ;
			}
		}
		
		//ACTUALIZAR CORRELATIVO
		 $sql = "UPDATE  reglasnegocio.correlativos  
		    SET  correlativo = correlativo+1
		    WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocorrelativo=".$documento;
		    $result = $conexion->query($sql);
		    if (!$result) 
		    {
		        $conexion->rollBack();
		        $mensaje = "619 Error UPDATE correlativos";
		        $data['res']    =2;
		        $data['mensaje']    =$mensaje;
		        die(json_encode($data));
		    }
	

	//CUOTAS CANCELADAS
	    $sqldetalle  = "select nrocuota,totalcuotas,fechavencimiento,subtotal,igv,redondeo,imptotal,";
		$sqldetalle .= "tipocuota,nrofacturacion,mes,anio,interes,estadocuota ";
		$sqldetalle .= "from facturacion.detcreditos ";
		$sqldetalle .= "where codemp=1 and codsuc=? and nrocredito=? order by nrocuota";
		$consultaD = $conexion->prepare($sqldetalle);
		$consultaD->execute(array($codsuc,$nrocredito));
		$itemD = $consultaD->fetchAll();
		foreach($itemD as $rowD)
		{
			if($rowD["estadocuota"]==0)
			{
				if($_POST['paso'.$rowD['nrocuota']]==1)	
				{
					//OBTENER PREPAGO SI YA SE GENERO Y ANULARLO
					$Sql="SELECT * FROM facturacion.cabcreditosadelantos 
							WHERE  codemp=:codemp AND codsuc=:codsuc AND nrocredito=:nrocredito and nrocuota=:nrocuota";
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrocredito"=>$nrocredito,
					                        ":nrocuota"=>$rowD['nrocuota']
					                        
					                        ));
					$rowX = $resultD->fetch();	
					if($rowX[0]!='')
					{
						$Sql="update cobranza.cabprepagos set estado=0 
						     WHERE codemp=1 AND  codsuc=".$codsuc." and nroprepago=".$rowX['nroprepago'];
						$conexion->query($Sql);
					}
					//OBTENER PREPAGO SI YA SE GENERO Y ANULARLO
					$Sql="DELETE FROM facturacion.cabcreditosadelantos 
							WHERE  codemp=:codemp AND codsuc=:codsuc AND nrocredito=:nrocredito and nrocuota=:nrocuota";
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrocredito"=>$nrocredito,
					                        ":nrocuota"=>$rowD['nrocuota']
					                        
					                        ));		
					$Sql="INSERT INTO 
						  facturacion.cabcreditosadelantos
						(
						  codemp,
						  codsuc,
						  nrocredito,
						  nrocuota,
						  imptotal,
						  fechareg,
						  nroprepago
						  
						) 
						VALUES (
						  :codemp,
						  :codsuc,
						  :nrocredito,
						  :nrocuota,
						  :imptotal,
						  :fechareg,
						  :nroprepago
						  
						); ";
				
					$resultD = $conexion->prepare($Sql);
					$resultD->execute(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrocredito"=>$nrocredito,
					                        ":nrocuota"=>$rowD['nrocuota'],
					                        ":imptotal"=>$rowD['imptotal'],
					                        ":fechareg"=>$fechareg,
					                        ":nroprepago"=>$nroprepagocre
					                        ));

					/*print_r(array(":codemp"=>$codemp,
					                        ":codsuc"=>$codsuc,
					                        ":nrocredito"=>$nrocredito,
					                        ":nrocuota"=>$rowD['nrocuota'],
					                        ":imptotal"=>$rowD['imptotal'],
					                        ":fechareg"=>$fechareg,
					                        ":nroprepago"=>$nroprepagocre
					                        ));*/

					if($resultD->errorCode()!='00000')
					{
					    $conexion->rollBack();
					    $mensaje = "Error update cabpagos";
					    $data['res']    =2;
					    die(json_encode($data)) ;
					}
					//´poner a facturado la cuota
					/*$sqldetalle  = "UPDATE facturacion.detcreditos SET estadocuota=1";
						$sqldetalle .= "where codemp=1 and codsuc=? and nrocredito=? AND nrocuota=?";
						$consultaD = $conexion->prepare($sqldetalle);
						$consultaD->execute(array($codsuc,$nrocredito,$rowD['nrocuota']));
						$itemD = $consultaD->fetchAll();*/
					//´poner a facturado la cuota

				}
			}
			
		}
	//CUOTAS CANCELADAS
	
	//die();
	if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       $res = 2;
    } else {
        $conexion->commit();
        		$objFunciones->setCorrelativosVarios(6,$codsuc,"UPDATE",$nroprepagocre);
        	
        
        
        	
        $mensaje = "El Registro se ha Grabado Correctamente";
        $res = 1;
    }
	
	$data['res']    =  $res;
	$data['nropago']              = 0;// $nropago;
	$data['nrocredito']           = $nrocredito;
	$data['nroprepagocre']        = $nroprepagocre;
	echo json_encode($data);

?>
