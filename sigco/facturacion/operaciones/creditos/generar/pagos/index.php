<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../../objetos/clsDrop.php");

$Op = $_POST["Op"];
$nrocredito = isset($_POST["Id"]) ? $_POST["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=".$Op;
$formapago = 1;
$fecha = date("d/m/Y");

$objMantenimiento = new clsDrop();
$sininteres = 0;
$paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);

$sql = "SELECT UPPER(clie.propietario) as propietario,cab.fechareg,cab.subtotal,cab.igv,cab.redondeo,cab.imptotal,";
$sql .= "UPPER(conc.descripcion) as concepto,cab.observacion,cab.creador,UPPER(doc.abreviado),clie.nrodocumento,";
$sql .= "UPPER(cab.observacion),clie.nroinscripcion,
    UPPER(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
    zo.descripcion as zona,cab.nroprepago,clie.codantiguo,cab.codconcepto,cab.nroprepagoinicial ";
$sql .= "FROM facturacion.cabcreditos as cab ";
$sql .= "INNER JOIN catastro.clientes as clie on(cab.codemp=clie.codemp and ";
$sql .= "cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion) ";
$sql .= "INNER JOIN facturacion.conceptos as conc on(cab.codconcepto=conc.codconcepto AND cab.codemp=conc.codemp and cab.codsuc=conc.codsuc  ) ";
$sql .= "INNER JOIN public.tipodocumento as doc on(clie.codtipodocumento=doc.codtipodocumento)
    INNER JOIN public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
    INNER JOIN public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
    INNER JOIN admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)";
$sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrocredito=?";

$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc, $nrocredito));
$row = $consulta->fetch();
if ($row[0] == '') {
    $sql = "SELECT UPPER(cab.propietario) as propietario,cab.fechareg,cab.subtotal,cab.igv,cab.redondeo,cab.imptotal,";
    $sql .= "cab.observacion,cab.creador,";
    $sql .= "UPPER(cab.observacion),cab.nroinscripcion, cab.direccion,
						cab.nroprepago,cab.codconcepto,cab.codantiguo,cab.nroprepagoinicial ";
    $sql .= "FROM facturacion.cabcreditos as cab ";
    $sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrocredito=?";
    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codsuc, $nrocredito));
    $row = $consulta->fetch();
}

$TasaInteres = $row['interes'] / 100;
?>
<style type="text/css">
    .CodConcepto { display: none;}
</style>
<script>

    var cont = 0;
    var nDest = 0;
    var nDestC = 0;
    var object = "";
    var codsuc = <?=$codsuc ?>;
    var fechaserver = "<?=$fecha ?>";
    var nrofact = 0;
    var anio = 0;
    var mes = 0;
    var igv = <?=$paramae["valor"] ?>;
    var urldir = "<?php echo $_SESSION['urldir']; ?>";
    var TasaInteres = <?=$TasaInteres ?>;
    var TasaInteresO = <?=$TasaInteres ?>;

    function Cancelar()
    {
        location.href = 'index.php?Op=1';
    }

    /////

    function check_deuda(obj) {
        var tr = $(obj).parents("tr:eq(0)");
        var importe = parseFloat(tr.find("input.input-deuda").val()).toFixed(2);
        var importeinteres = parseFloat(tr.find("input.input-deudainteres").val()).toFixed(2);
        var importeigv = parseFloat(tr.find("input.input-deudaigv").val()).toFixed(2);

        var val = parseFloat($("#PagoAdelantado").val());
        var valinteres = parseFloat($("#PagoAdelantadoInteres").val());
        var valigv = parseFloat($("#PagoAdelantadoIgv").val());


        var newval;
        var newvalinteres;
        var newvaligv;
        $(".input-paso").val(0)
        if (obj.checked) {
            newval = /*val +*/ parseFloat(importe);
            newvalinteres = /*valinteres +*/ parseFloat(importeinteres);
            newvaligv = /*valigv +*/ parseFloat(importeigv);

            tr.find("input.input-paso").val(1)
        }
        else {
            newval = /*val -*/ importe;
            newvalinteres = /*valinteres -*/ parseFloat(importeinteres);
            newvaligv = /*valigv -*/ parseFloat(importeigv);
            tr.find("input.input-paso").val(0)
        }
        $("#subtotal").val(newval - newvaligv);
        $("#PagoAdelantado").val(newval);
        $("#LblPagoAdelantado").html(newval.toFixed(2));

        $("#PagoAdelantadoInteres").val(newvalinteres);
        $("#LblPagoAdelantadoInteres").html(newvalinteres.toFixed(2));

        $("#PagoAdelantadoIgv").val(newvaligv);
        $("#LblPagoAdelantadoIgv").html(newvaligv.toFixed(2));

    }
    function ImprimirC(nropago, femision)
    {

        if (nropago != 0)
        {
            var url = '../../../../cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php'
            url += '?codpago=' + nropago + '&nroinscripcion=<?=$row['nroinscripcion'] ?>&fechapago=' + femision + '&codsuc=<?=$codsuc ?>';
            var ventana = window.open(url, 'Buscar', 'width=800, height=600, resizable=yes, scrollbars=yes, status=yes,location=yes');
            ventana.focus();
        }
    }
</script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script src="js_creditos.js" type="text/javascript"></script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar; ?>" enctype="multipart/form-data">
        <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos del Cr&eacute;dito</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Colaterales</a></li>
                                <li style="font-size:10px"><a href="#tabs-3">Cronograma</a></li>

                            </ul>
                            <div id="tabs-1" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">


                                    <tr>
                                        <td width="95" >N&deg; Inscripci&oacute;n</td>
                                        <td width="19" align="center">:</td>
                                        <td width="372" ><label>
                                                <input type="text"  id="codantiguo" class="inputtext" size="12" maxlength="12" readonly="readonly" value="<?=$row['codantiguo'] ?>"/>
                                                <input type="hidden" name="nroinscripcion" id="nroinscripcion" value="<?=$row['nroinscripcion'] ?>">
                                                 <!-- <span class="MljSoft-icon-buscar" title="Buscar Clientes" onclick="buscar_usuario();"></span>  -->
                                                <input type="hidden" name="anio" id="anio" />
                                                <input type="hidden" name="mes" id="mes" />
                                                <input type="hidden" name="nrof" id="nrof" />
                                                <input type="hidden" id="IgvP" value="<?=$paramae["valor"] ?>" />
                                                <input type="hidden" id="cuotainicialflag" name="cuotainicialflag" value="0" />
                                                <input type="hidden" name="condpago" id="condpago" value="0">
                                                <input type="hidden" id="formapagox" name="formapagox" />
                                                <input type="hidden" id="documentox" name="documentox" />
                                                <input type="hidden" id="carx" name="carx" />
                                                <input type="hidden" id="tpagarx" name="tpagarx" />
                                                <input type="hidden" id="efectivo_x" name="efectivo_x" />
                                                <input type="hidden" id="vuelto_x" name="vuelto_x" />
                                                <input type="hidden" id="numerodocx" name="numerodocx" />
                                                <input type="hidden" id="seriedocx" name="seriedocx" />
                                                <input type="hidden" id="codcliente" name="codcliente" />
                                                <input type="hidden" id="CodCredito" name="CodCredito"  value="<?=$Id ?>"/>
                                                <input type="hidden" id="nrocredito" name="nrocredito"  value="<?=$nrocredito ?>"/>
                                                <input type="hidden" id="codconcepto" name="codconcepto"  value="<?=$row['codconcepto'] ?>"/>
                                                <input type="hidden" id="Colateral" name="Colateral"  value="<?=$row['concepto'] ?>"/>
                                                <div id="DivSave"></div>
                                            </label></td>
                                        <td width="114" align="right" ></td>
                                        <td width="18" align="center"></td>
                                        <td width="278" >

                                            </label></td>
                                    </tr>
                                    <tr>
                                        <td >Nombres</td>
                                        <td align="center">:</td>
                                        <td ><label>
                                                <input type="text" name="nombres" id="nombres" class="inputtext" maxlength="200" readonly="readonly" value="<?=$row['propietario'] ?>" style="width:350px;"/>
                                            </label></td>
                                        <td align="right"></td>
                                        <td align="center"></td>
                                        <td >

                                        </td>
                                    </tr>
                                    <tr>
                                        <td >Direccion</td>
                                        <td align="center">:</td>
                                        <td ><input type="text" name="direccion" id="direccion" class="inputtext" maxlength="200" readonly="readonly" value="<?=$row['direccion'] ?>" style="width:350px;"/></td>
                                        <td align="right" style="display:none">Tipo de Presup </td>
                                        <td align="center" style="display:none">:</td>
                                        <td style="display:none"></td>
                                    </tr>


                                    <tr>
                                        <td valign="top" >Observacion</td>
                                        <td align="center" valign="top">:</td>
                                        <td colspan="4"><label>
                                                <textarea name="observacion" id="observacion" class="inputtext" style="font-size:11px; width:600px;" rows="5"></textarea>
                                            </label></td>
                                    </tr>
                                    <tr style="height:5px">
                                        <td colspan="6" class="TitDetalle"><input type="hidden" name="count" id="count" value="0" /></td>
                                    </tr>

                                </table>
                            </div>
                            <div id="tabs-2" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="100%">

                                                <tr>
                                                    <td colspan="2" >
                                                        <div style="height:auto; overflow:auto" align="center" id="DivDetalle">
                                                            <table width="800" border="0" align="center" cellspacing="0" class="ui-widget" id="TbIndex">
                                                                <thead class="ui-widget-header" >
                                                                    <tr >
                                                                        <th width="43" align="center" scope="col">Item</th>
                                                                        <th align="center" scope="col">Colateral</th>
                                                                        <th width="80" align="center" scope="col">Subtotal</th>
                                                                        <th width="80" align="center" scope="col">IGV</th>
                                                                        <th width="80" align="center" scope="col">Redondeo</th>
                                                                        <th width="80" align="center" scope="col">Importe</th>
                                                                        <th width="5" align="center" scope="col">&nbsp;</th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    $tr = '';

                                                                    $Sql = "SELECT d.codconcepto,  c.descripcion,  d.item,  d.subtotal,  d.igv, d.redondeo,  d.imptotal
                                                                        FROM  facturacion.detcabcreditos d
                                                                        INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp)  AND (d.codsuc = c.codsuc)  AND (d.codconcepto = c.codconcepto)
                                                                        WHERE d.codemp=? AND d.codsuc=? AND  d.nrocredito=? 
                                                                        ORDER BY  d.item";
                                                                    $consulta = $conexion->prepare($Sql);
                                                                    $consulta->execute(array(1, $codsuc, $nrocredito));
                                                                    $itemsD = $consulta->fetchAll();
                                                                    $subtotaltotal = 0;
                                                                    $igvtotal = 0;
                                                                    $redondeototal = 0;
                                                                    $importetotal = 0;
                                                                    foreach ($itemsD as $rowD) {
                                                                        $subtotaltotal +=$rowD['subtotal'];
                                                                        $igvtotal +=$rowD['igv'];
                                                                        $redondeototal +=$rowD['redondeo'];
                                                                        $importetotal +=$rowD['imptotal'];

                                                                        $tr.='<tr id="'.$rowD['item'].'" onclick="SeleccionaId(this);">';
                                                                        $tr.='<td align="center" ><label class="Item">'.$rowD['item'].'</label></td>';
                                                                        $tr.='<td align="left"><label class="CodConcepto">'.$rowD['codconcepto'].'</label>';
                                                                        $tr.='<label class="Colateral">'.$rowD['descripcion'].'</label></td>';
                                                                        $tr.='<td align="right"><label class="SubTotal">'.number_format($rowD['subtotal'], 2).'</label></td>';
                                                                        $tr.='<td align="right"><label class="IGV">'.number_format($rowD['igv'], 2).'</label></td>';
                                                                        $tr.='<td align="right"><label class="Redondeo">'.number_format($rowD['redondeo'], 2).'</label></td>';
                                                                        $tr.='<td align="right"><label class="Importe">'.number_format($rowD['imptotal'], 2).'</label></td>';
                                                                        $tr.='<td align="center" ></td></tr>';
                                                                    }

                                                                    echo $tr;
                                                                    ?>

                                                                </tbody>
                                                                <tfoot class="ui-widget-header">

                                                                    <tr>
                                                                        <td colspan="2" align="right" >Total :</td>
                                                                        <td align="right" >
                                                                            <input type="text" id="subtotaltotal" value="<?=number_format($subtotaltotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  /> 
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="igvtotal" value="<?=number_format($igvtotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  /> 
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="redondeototal" value="<?=number_format($redondeototal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  /> 
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="importetotal" value="<?=number_format($importetotal, 2) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  /> 
                                                                        </td>
                                                                        <td >&nbsp;</td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-3" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="6" class="TitDetalle">
                                            <div style="height:270px; overflow:scroll">
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable01"  width="100%" id="tbdetalle" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="8%" >Nro. Cuota </th>
                                                            <th width="10%" >Fecha de Vencimiento</th>
                                                            <th width="8%" >Saldo</th>
                                                            <th width="8%" >Amortizaci&oacute;n</th>
                                                            <th width="8%" >Interes</th>
                                                            <th width="5%" >Igv</th>
                                                            <th width="9%" >Importe</th>
                                                            <th width="10%" >Estado</th>
                                                            <th width="10%" >Fec.Pago</th>
                                                            <th width="1%" align="center" scope="col">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sqldetalle = "SELECT d.nrocuota,d.totalcuotas,d.fechavencimiento,
                                                            d.subtotal,d.igv,d.redondeo,d.imptotal,d.tipocuota,d.nrofacturacion,
                                                            d.mes,d.anio,d.interes,d.estadocuota,d.fechapago, a.nroprepago 
                                                            FROM  facturacion.detcreditos d
                                                            LEFT JOIN facturacion.cabcreditosadelantos a ON (d.codemp = a.codemp)
                                                            AND (d.codsuc = a.codsuc)  AND (d.nrocredito = a.nrocredito)
                                                            AND (d.nrocuota = a.nrocuota)
                                                            where d.codemp=1 and d.codsuc=? and d.nrocredito=?
                                                            order by d.nrocuota";
                                                        $consultaD = $conexion->prepare($sqldetalle);
                                                        $consultaD->execute(array($codsuc, $nrocredito));
                                                        $itemD = $consultaD->fetchAll();
                                                        $Saldo = $row[5];

                                                        $tinteres = 0;
                                                        $tigv = 0;
                                                        $totalcuotas = 0;
                                                        $TotalDeuda = 0;
                                                        $TotalDeudaInteres = 0;
                                                        $TotalDeudaIgv = 0;
                                                        $total = 0;
                                                        foreach ($itemD as $rowD) {
                                                            if ($rowD["tipocuota"] == 1) {
                                                                $tipo = "CUOTA INICIAL";
                                                            } else {
                                                                $tipo = "CUOTA NORMAL";
                                                            }
                                                            switch ($rowD["estadocuota"]) {
                                                                case '0': $Estado = "<span style='color:red'>PENDIENTE</span>";
                                                                    break;
                                                                case '1': $Estado = "<span style='color:red'>FACTURADO</span>";
                                                                    break;
                                                                case '2': $Estado = "<span style='color:red'>ANULADO</span>";
                                                                    break;
                                                                case '3': $Estado = "<span style='color:green'>CANCELADO</span>";
                                                                    break;
                                                                case '4': $Estado = "<span style='color:yellow'>QUEBRADO</span>";
                                                                    break;
                                                                case '5': $Estado = "<span style='color:green'>NO MIGRADO</span>";
                                                                    break;
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td align="center"><?=$rowD["nrocuota"] ?></td>
                                                                <td><?=$meses[$rowD["mes"]]."-".$rowD["anio"] ?>
                                                                    <input type='hidden' value='<?=$rowD["imptotal"] ?>' class='input-deuda' />
                                                                    <input type='hidden' value='<?=$rowD["interes"] ?>' class='input-deudainteres' />
                                                                    <input type='hidden' value='<?=$rowD["igv"] ?>' class='input-deudaigv' />

                                                                </td>
                                                                <td align="right"><?=number_format($Saldo, 2) ?></td>
                                                                <td align="right"><?=number_format($rowD["subtotal"], 2) ?></td>
                                                                <td align="right"><?=number_format($rowD["interes"], 2) ?></td>
                                                                <td align="right"><?=number_format($rowD["igv"], 2) ?></td>
                                                                <td align="right"><?=number_format($rowD["imptotal"], 2) ?></td>
                                                                <td align="center"><?=$Estado ?></td>
                                                                <td align="right"><?=$objMantenimiento->DecFecha($rowD['fechapago']) ?></td>
                                                                <th width="5" align="center" scope="col">
                                                                    <?php
                                                                    $Saldo -= $rowD["imptotal"];
                                                                    switch ($rowD["estadocuota"]) {
                                                                        case '0':
                                                                            //$TotalDeuda+=$rowD["imptotal"];
                                                                            //$TotalDeudaInteres+=$rowD["interes"];
                                                                            //$TotalDeudaIgv+=$rowD["igv"];
                                                                            //$total++;
                                                                            ?>
                                                                            <input type='hidden' value='0' class='input-paso'  name="paso<?=$rowD["nrocuota"] ?>" />
                                                                            <input type='radio' name='check_deudax'  value="<?=$rowD["nrocuota"] ?>"  class='input-chk' onclick='check_deuda(this)'>
                                                                            <?php
                                                                            break;
                                                                        default:
                                                                            ?>
                                                                            <input type='hidden' value='0' class='input-paso'  name="paso<?=$rowD["nrocuota"] ?>" />
                                                                            <?php
                                                                        }
                                                                        if ($rowD['nroprepago'] != '') {
                                                                            ?>
                                                                        <span class="MljSoft-icon-print" title="Impimir Nota de Pago" onclick="ImprimirC(<?=$rowD['nroprepago'] ?>, '<?=$rowD['fechapago'] ?>')"></span>
                                                                        <?php
                                                                        }
                                                                        if($rowD['tipocuota']==1 )
                                                                        {
                                                                        ?>
                                                                        <span class="MljSoft-icon-print" title="Impimir Nota de Pago" onclick="ImprimirC(<?=$row['nroprepagoinicial'] ?>,<?=$row['fechareg'] ?>)"></span>
                                                                        <?php
                                                                        }
                                                                        ?>	

                                                                    </th>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                        <tfoot class="ui-widget-header" >
                                                            <tr>
                                                                <td colspan="3">&nbsp;</td>
                                                                <td> Total</td>
                                                                <td align="right"><label id="LblPagoAdelantadoInteres"><?=number_format($TotalDeudaInteres, 2) ?></label>
                                                                    <input id="PagoAdelantadoInteres" name="interespagado" value="<?=$TotalDeudaInteres ?>" type="hidden">

                                                                </td>
                                                                <td align="right"><label id="LblPagoAdelantadoIgv"><?=number_format($TotalDeudaIgv, 2) ?></label>
                                                                    <input id="PagoAdelantadoIgv" name="igv" value="<?=$TotalDeudaIgv ?>" type="hidden">

                                                                </td>
                                                                <td align="right"><label id="LblPagoAdelantado"><?=number_format($TotalDeuda, 2) ?></label>
                                                                    <input id="subtotal" name="subtotal" value="<? echo $TotalDeuda - $TotalDeudaInteres;?>" type="hidden">
                                                                    <input id="PagoAdelantado" name="imptotal" value="<?=$TotalDeuda ?>" type="hidden">
                                                                    <input name="TotalCuotasP" value="<?=$total ?>" type="hidden">
                                                            </td>
                                                            <td colspan="3">&nbsp;</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
        <div id="DivInicial" title="Comprobante Cuota Inicial" style="display: none; font-size:14; font-weight:bold">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ui-widget-content">
                <tr>
                    <td align="right">Forma</td>
                    <td align="center">:</td>
                    <td>

                        <?php $objMantenimiento->drop_forma_pago($formapago,""," where estareg=1 and tipodocumento=0"); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right">Car</td>
                    <td align="center">:</td>
                    <td >
                        <?php $objMantenimiento->drop_car($codsuc); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right">Documento</td>
                    <td align="center">:</td>
                    <td>
                        <?php $objMantenimiento->drop_documentos(13,'onChange="cSerieNun(this)"'," where estareg=1 and tipodocumento=0 codsuc=".$codsuc); ?>

                    </td>
                </tr>
                <tr>
                    <td align="right">Serie</td>
                    <td align="center">:</td>
                    <td>
                        <input type="text" name="seriedoc" id="seriedoc" class="inputtext" size="3" maxlength="45"  readonly="readonly" value=""/>

                        -<input type="text" name="numerodoc" id="numerodoc" class="inputtext" size="7" maxlength="45"  readonly="readonly" value=""/>

                    </td>
                </tr>
                <tr>
                    <td align="right">Glosa</td>
                    <td align="center">:</td>
                    <td >
                        <span class="icono-icon-detalle" title="Glosa" onclick="VerGlosa()"></span>
                    </td>
                </tr>
                <tr><td colspan="3"><td>&nbsp;</td></td></tr>
                <tr>
                    <td style="font-size:12px; font-weight:bold">Total a Pagar</td>
                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
                    <td><label>
                            <input type="text" name="tpagar" id="tpagar" readonly="readonly" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right; background-color:#CCC" value="0.00" />
                        </label></td>
                </tr>
                <tr>
                    <td style="font-size:12px; font-weight:bold">Efectivo</td>
                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
                    <td><input type="text" name="efectivo" id="efectivo" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right" onkeypress="ValidarEfect(event);
                    return permite(event, 'num');" onkeyup="calcular_vuelto(this);" value="0.00" /></td>
                </tr>
                <tr>
                    <td style="font-size:12px; font-weight:bold">Vuelto</td>
                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
                    <td><input type="text" name="vueltox" id="vueltox" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right; background-color:#CCC" readonly="readonly" value="0.00" /></td>
                </tr>
            </table>
        </div>
        <div id="DivGlosa" title="Observaciones"  >
            <table width="100%" border="0">
                <tr>
                    <td> Glosa:</td>
                    <td>
                        <textarea class="inputtext ui-corner-all" id="GlosaTemp" title="Glosa" cols="50" rows="5" ></textarea>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>