<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsReporte.php");
	
	class clsCreditos extends clsReporte
	{
		function cabecera()
		{
			global $nrocredito;
         
			$this->Ln(5);
			$this->SetFont('Arial','B',10);
			$tit1 	= "CONTRATO CONVENIO PARA PAGO A PLAZOS ";
			$tit2 	= "Nro. : ".$nrocredito;
			
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			$this->Cell(190,5,utf8_decode($tit2),0,1,'C');
		}
		function Contenido($nrocredito,$codsuc)
		{			
			global $conexion,$meses,$NombreEmpresa;
			$codemp=1;
			$h = 4;
			$s = 2;
			
			$sql  = "select upper(clie.propietario),cab.fechareg,cab.subtotal,cab.igv,cab.redondeo,cab.imptotal,";
			$sql .= "upper(conc.descripcion),cab.observacion,cab.creador,upper(doc.abreviado),clie.nrodocumento,";
			$sql .= "upper(cab.observacion),".$this->getCodCatastral("clie.").",clie.nroinscripcion,
					upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,
					zo.descripcion as zona,cab.nroprepago ";
			$sql .= "from facturacion.cabcreditos as cab ";
			$sql .= "inner join catastro.clientes as clie on(cab.codemp=clie.codemp and ";
			$sql .= "cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion) ";
			$sql .= "inner join facturacion.conceptos as conc on(cab.codconcepto=conc.codconcepto AND cab.codemp=conc.codemp and cab.codsuc=conc.codsuc  ) ";
			$sql .= "inner join public.tipodocumento as doc on(clie.codtipodocumento=doc.codtipodocumento)
					 inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
					 inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
					 inner join admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)";
 
			 $sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrocredito=?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$nrocredito));
			$row = $consulta->fetch();
			if($row[0]=='')
			{
				$sql  = "select upper(cab.propietario),cab.fechareg,cab.subtotal,cab.igv,cab.redondeo,cab.imptotal,";
				$sql .= "upper(conc.descripcion),cab.observacion,cab.creador,exp.documento,";
				$sql .= "upper(cab.observacion),exp.nroinscripcion,cab.direccion as direccion,'' as zona,cab.nroprepago ";
				$sql .= "from facturacion.cabcreditos as cab 
						inner join solicitudes.expedientes as exp on (exp.codemp=cab.codemp and exp.codsuc=cab.codsuc and exp.nroinscripcion=cab.nroinscripcion and exp.codexpediente=cab.nropresupuesto )";
				$sql .= "inner join facturacion.conceptos as conc on(cab.codconcepto=conc.codconcepto AND cab.codemp=conc.codemp and cab.codsuc=conc.codsuc ) ";
				
	 
				$sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrocredito=?";
				
				$consulta = $conexion->prepare($sql);
				$consulta->execute(array($codsuc,$nrocredito));
				$row = $consulta->fetch();
			}
			$this->Ln(5);
			$this->SetFont('Arial','',7);
			$y=$this->GetY();
			$this->Rect($x+10,$y,190,10);
			$this->Cell(47.5,$h,("FECHA DE CONVENIO"),0,0,'C');
			$this->Cell(47.5,$h,("C�DIGO CATASTRAL"),0,0,'C');
			$this->Cell(47.5,$h,("C�DIGO DE INSCRIPCI�N"),0,0,'C');
			$this->Cell(47.5,$h,("N� DE CONTRATO"),0,1,'C');
			$this->SetFont('Arial','B',9);
			$this->Cell(47.5,$h,$this->DecFecha($row[1]),0,0,'C');
			$this->Cell(47.5,$h,$row['codcatastro'],0,0,'C');
			$this->Cell(47.5,$h,$row['nroinscripcion'],0,0,'C');
			$this->Cell(47.5,$h,"0",0,1,'C');

			$this->Ln(8);
			$this->SetFont('Arial','',10);
			$text="Celebrado entre la empresa Prestadora de Servicios, representada por su responsable comercial, que en lo sucesivo";
			$this->MultiCell(0,$h,(($text)),0,'J');
			$text="se denominar� la 'EMPRESA' y El Sr. (la Sra.) : ";
			$this->Cell(75,$h,$text,0,0,'L');
			$this->SetFont('Arial','B',10);
			$this->Cell(70,$h,(strtoupper($row[0])),0,0,'L');
			$this->SetFont('Arial','',10);
			$text="identificado (a) con ";
			$this->Cell(0,$h,$text,0,1,'L');
			$this->Cell(10,$h,$row[9]." : ",0,0,'L');
			$this->SetFont('Arial','B',10);
			$this->Cell(20,$h,$row[10],0,0,'L');
			$this->SetFont('Arial','',10);
			$text="domiciliado (a) en: ";
			$this->Cell(30,$h,$text,0,0,'L');
			$this->SetFont('Arial','B',10);
			$this->Cell(70,$h,$row['direccion'],0,0,'L');
			$this->SetFont('Arial','',10);
			$text="Urbanizaci�n ";
			$this->Cell(0,$h,$text,0,1,'R');
			$this->SetFont('Arial','B',10);
			$this->Cell(30,$h,$row['zona'],0,0,'L');
			$this->SetFont('Arial','',10);
			$text=", que en adelante se denominara el 'CLIENTE'. ";
			$this->Cell(0,$h,$text,0,1,'L');
			$this->Ln(3);
			$text="Por la deuda que asciende a :";
			$this->Cell(0,$h,$text,0,1,'L');
			$this->Ln(5);
			$h = 6;
			$nroprepago=$row['nroprepago'];
			if($nroprepago!="" && $nroprepago!="0")
			{
				$Sql="SELECT d.abreviado , p.serie ,  p.nrodocumento,SUM(p.importe) as importe
					FROM reglasnegocio.documentos d
					INNER JOIN cobranza.detprepagos p ON (d.coddocumento = p.coddocumento) AND (d.codsuc = p.codsuc)
				WHERE p.codemp=? AND  p.codsuc=? AND p.nroinscripcion=? AND p.nroprepago=?
				GROUP BY d.abreviado , p.serie ,  p.nrodocumento";
				//	print_r(array($codemp,$codsuc,$nroinscripcion,$codpago));
				$result = $conexion->prepare($Sql);
				$result->execute(array($codemp,$codsuc,$row['nroinscripcion'],$nroprepago));
				$row2 = $result->fetch();
				$documento = $row2[0];
				$recibon = $row2[1]."-".str_pad($row2[2],8,"0",STR_PAD_LEFT);
				$importe = number_format($row2['importe'],2);

				$Sql="SELECT fechareg
					FROM cobranza.cabprepagos 
				WHERE codemp=? AND  codsuc=? AND nroinscripcion=? AND nroprepago=?";
				//	print_r(array($codemp,$codsuc,$nroinscripcion,$codpago));
				$result = $conexion->prepare($Sql);
				$result->execute(array($codemp,$codsuc,$row['nroinscripcion'],$nroprepago));
				$row2 = $result->fetch();
				$fechaprepago = $row2[0];

			}
			$this->SetFont('Arial','',6);
			$this->Cell(25,$h,"ITEM",1,0,'C');
			$this->Cell(30,$h,"FACTURACI�N",1,0,'C');
			$this->Cell(30,$h,"TIPO RECIBO",1,0,'C');
			$this->Cell(30,$h,"SERIE NUMERO",1,0,'C');
			$this->Cell(25,$h,"MONTO",1,0,'C');
			$this->Cell(25,$h,"EMISI�N",1,0,'C');
			$this->Cell(0,$h,"VENCIMIENTO",1,1,'C');
			$this->SetFont('Arial','',8);
			$this->Cell(25,$h,"1",1,0,'C');
			$this->Cell(30,$h,"COLATERALES",1,0,'C');
			$this->Cell(30,$h,$documento,1,0,'C');
			$this->Cell(30,$h,$recibon,1,0,'C');
			$this->Cell(25,$h,$importe,1,0,'C');
			$this->Cell(25,$h,$this->DecFecha($fechaprepago),1,0,'C');
			$this->Cell(0,$h,$this->DecFecha($fechaprepago),1,0,'C');

			$sqldetalle  = "select MAX(nrocuota) ";
			$sqldetalle .= "from facturacion.detcreditos ";
			$sqldetalle .= "where codemp=1 and codsuc=? and nrocredito=?";
			$consultaD = $conexion->prepare($sqldetalle);
			$consultaD->execute(array($codsuc,$nrocredito));
			$row2 = $consultaD->fetch();
			$nrocuota = $row2[0];
			$this->SetFont('Arial','',10);
			$h = 4;
			$this->Ln(8);
			$text="la misma que ha sido fraccionada en: ";
			$this->Cell(60,$h,$text,0,0,'L');
			$this->SetFont('Arial','B',10);
			$this->Cell(5,$h,$nrocuota,0,0,'L');
			$this->SetFont('Arial','',10);
			$text=" Cuotas. ";
			$this->Cell(0,$h,$text,0,1,'L');

			$this->Ln(5);

			$this->SetWidths(array(20,45,25,25,25,25,25,25,20));
			$this->SetAligns(array("C","L","C","C","C","C","C","C","C"));
			$this->Row(array("Nro. Cuota",
							 "Vencimiento Mes-A�o",
							 "Saldo",
							 "Amorizaci�n",
							 "Interes",
							 "Igv",
							 "Cuota"
							 ));
			
			$sqldetalle  = "select nrocuota,totalcuotas,fechavencimiento,subtotal,igv,redondeo,imptotal,";
			$sqldetalle .= "tipocuota,nrofacturacion,mes,anio,interes ";
			$sqldetalle .= "from facturacion.detcreditos ";
			$sqldetalle .= "where codemp=1 and codsuc=? and nrocredito=? order by nrocuota";
			
			$consultaD = $conexion->prepare($sqldetalle);
			$consultaD->execute(array($codsuc,$nrocredito));
			$itemD = $consultaD->fetchAll();
			$Saldo = $row[5];
			$this->SetAligns(array("C","L","R","R","R","R","R","C","C"));
			$Amortiz=0;
			$tinteres=0;
			$tigv=0;
			$totalcuotas=0;
			$this->SetFont('Arial','',8);
			foreach($itemD as $rowD)
			{
				if($rowD["tipocuota"]==1){$tipo="CUOTA INICIAL";}else{$tipo="CUOTA NORMAL";}
				
			
			
				$this->Row(array($rowD["nrocuota"],
								 $meses[$rowD["mes"]]."-".$rowD["anio"],
								 number_format($Saldo,2),
								 number_format($rowD["subtotal"],2),
								 number_format($rowD["interes"],2),
								 number_format($rowD["igv"],2),
								 number_format($rowD["imptotal"],2)
								));
				 $Saldo  -=  $rowD["imptotal"];
				 $Amortiz += $rowD["subtotal"];
				 $tinteres+=$rowD["interes"];
				 $tigv+=$rowD["igv"];
				 $totalcuotas+=$rowD["imptotal"];
			}
			$this->Cell(90,$h,"TOTALES =>",1,0,'C');
			$this->Cell(25,$h,number_format($Amortiz,2),1,0,'R');	
			$this->Cell(25,$h,number_format($tinteres,2),1,0,'R');	
			$this->Cell(25,$h,number_format($tigv,2),1,0,'R');	
			$this->Cell(25,$h,number_format($totalcuotas,2),1,1,'R');	
			$this->Ln(8);
			$this->SetFont('Arial','',11);
			$text="El CLIENTE incurrir� en mora al s�lo incumpliento de una de las cuotas estipuladas en la clausula anterior, sin necesidad de requerimiento judicial o extrajudicial alguno, pudiendo la EMPRESA proceder a su cobro por la v�a judicial.";
			$this->MultiCell(0,$h,(($text)),0,'J');
			$this->SetFont('Arial','',10);
			$this->Ln(30);
			$this->SetX(40);
			$this->Cell(50,0.1,"",1,0,'C');
			$this->SetX(125);
			$this->Cell(60,0.1,"",1,1,'C');
			$this->SetX(40);
			$this->Cell(50,$h,$NombreEmpresa,0,0,'C');
			$this->SetX(130);
			$this->Cell(50,$h,"Sr.(a): ".strtoupper($row[0]),0,0,'C');
			$this->SetX(40);
			
		

		}
    }

	$nrocredito	= $_GET["Id"];
	$codsuc		= $_GET["codsuc"];
	$NombreEmpresa = $_SESSION['NombreEmpresa'];
    $objReporte	=	new clsCreditos();
	$objReporte->AliasNbPages();
    $objReporte->AddPage();
	$objReporte->Contenido($nrocredito,$codsuc);
	$objReporte->Output();	
	
	
?>