<?php 
	include("../../../../../objetos/clsReporte.php");
	
	class clsCreditos extends clsReporte
	{
		function cabecera()
		{
			global $nrocredito;
         
			$this->Ln(5);
			$this->SetFont('Arial','B',10);
			$tit1 	= "CRONOGRAMA DE CREDITO";
			$tit2 	= "Nro. DE CREDITO : ".$nrocredito;
			
			$this->Cell(190,5,utf8_decode($tit1),0,1,'C');	
			$this->Cell(190,5,utf8_decode($tit2),0,1,'C');
		}
		function Contenido($nrocredito,$codsuc)
		{			
			global $conexion,$meses;
			
			$h = 4;
			$s = 2;
			
			$sql  = "select upper(clie.propietario),cab.fechareg,cab.subtotal,cab.igv,cab.redondeo,cab.imptotal,";
			$sql .= "upper(conc.descripcion),cab.observacion,cab.creador,upper(doc.abreviado),clie.nrodocumento,";
			$sql .= "upper(cab.observacion) ";
			$sql .= "from facturacion.cabcreditos as cab ";
			$sql .= "inner join catastro.clientes as clie on(cab.codemp=clie.codemp and ";
			$sql .= "cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion) ";
			$sql .= "inner join facturacion.conceptos as conc on(cab.codconcepto=conc.codconcepto) ";
			$sql .= "inner join public.tipodocumento as doc on(clie.codtipodocumento=doc.codtipodocumento)";
			$sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrocredito=?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc,$nrocredito));
			$row = $consulta->fetch();
			if($row[0]=='')
			{
				$sql  = "select upper(cab.propietario),cab.fechareg,cab.subtotal,cab.igv,cab.redondeo,cab.imptotal,";
				$sql .= "upper(conc.descripcion),cab.observacion,cab.creador,'','',";
				$sql .= "upper(cab.observacion) ";
				$sql .= "from facturacion.cabcreditos as cab ";
				$sql .= "inner join facturacion.conceptos as conc on(cab.codconcepto=conc.codconcepto) ";
				
				$sql .= "where cab.codemp=1 and cab.codsuc=? and cab.nrocredito=?";
				$consulta = $conexion->prepare($sql);
				$consulta->execute(array($codsuc,$nrocredito));
				$row = $consulta->fetch();
			}
			$this->Ln(5);
			$this->SetFont('Arial','',7);
			
			$this->Cell(25,$h,"Propietario",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,utf8_decode(strtoupper($row[0])),0,1,'L');
			
			$this->Cell(25,$h,"Documento",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,$row[9]." - ".$row[10],0,1,'L');
			
			$this->Cell(25,$h,"Fecha de Emision",0,0,'R');
			$this->Cell(3,$h,":",0,0,'R');
			$this->Cell(70,$h,$this->DecFecha($row[1]),0,1,'L');
			
			$this->Ln(1);
			$this->SetX(35);
			$this->MultiCell(130,$h,utf8_decode($row[11]),'0','J');
			
			$this->Ln(1);
			$this->Cell(25,$h,"Sub. Total",0,0,'R');
			$this->Cell(3,$h,":",0,0,'C');
			$this->Cell(20,$h,number_format($row[2],2),0,0,'R');
			
			$this->Cell(20,$h,"Igv",0,0,'R');
			$this->Cell(3,$h,":",0,0,'C');
			$this->Cell(20,$h,number_format($row[3],2),0,0,'R');
			
			$this->Cell(20,$h,"Redondeo",0,0,'R');
			$this->Cell(3,$h,":",0,0,'C');
			$this->Cell(20,$h,number_format($row[4],2),0,0,'R');
			
			$this->Cell(20,$h,"Imp. Total",0,0,'R');
			$this->Cell(3,$h,":",0,0,'C');
			$this->Cell(20,$h,number_format($row[5],2),0,1,'R');
			
			$this->Ln(1);

			$this->SetWidths(array(18,28,18,20,18,18,18,25,20));
			$this->SetAligns(array("C","L","C","C","C","C","C","C","C"));
			$this->Row(array("Nro. Cuota",
							 "Vencimiento Mes-A�o",
							 "Saldo",
							 "Amorizaci�n",
							 "Interes",
							 "Igv",
							 "Cuota",
							 "Categoria",
							 "Nro. Fact."));
			
			$sqldetalle  = "select nrocuota,totalcuotas,fechavencimiento,subtotal,igv,redondeo,imptotal,";
			$sqldetalle .= "tipocuota,nrofacturacion,mes,anio,interes,estadocuota ";
			$sqldetalle .= "from facturacion.detcreditos ";
			$sqldetalle .= "where codemp=1 and codsuc=? and nrocredito=? order by nrocuota";
			
			$consultaD = $conexion->prepare($sqldetalle);
			$consultaD->execute(array($codsuc,$nrocredito));
			$itemD = $consultaD->fetchAll();
			$Saldo = $row[5];
			$this->SetAligns(array("C","L","R","R","R","R","R","C","C"));
			$tinteres=0;
			$tigv=0;
			$totalcuotas=0;
			foreach($itemD as $rowD)
			{
				if($rowD["tipocuota"]==1){$tipo="CUOTA INICIAL";}else{$tipo="CUOTA NORMAL";}
				switch ($rowD["estadocuota"]) 
				{
					case '0': $Estado="PENDIENTE";break;
					case '1': $Estado="FACTURADO";break;
					case '2': $Estado="ANULADO";break;
					case '3': $Estado="CANCELADO";break;
					case '4': $Estado="QUEBRADO";break;
					
				}
				$this->Row(array($rowD["nrocuota"],
								 $meses[$rowD["mes"]]."-".$rowD["anio"],
								 number_format($Saldo,2),
								 number_format($rowD["subtotal"],2),
								 number_format($rowD["interes"],2),
								 number_format($rowD["igv"],2),
								 number_format($rowD["imptotal"],2),
								 $tipo,
								 $Estado));
				 $Saldo  -=  $rowD["subtotal"];
				 $tinteres+=$rowD["interes"];
				 $tigv+=$rowD["igv"];
				 $totalcuotas+=$rowD["imptotal"];
			}
			$this->Cell(64,$h,"TOTALES =>",1,0,'C');
			$this->Cell(20,$h,number_format($row[5],2),1,0,'R');	
			$this->Cell(18,$h,number_format($tinteres,2),1,0,'R');	
			$this->Cell(18,$h,number_format($tigv,2),1,0,'R');	
			$this->Cell(18,$h,number_format($totalcuotas,2),1,1,'R');	
		}
    }

	$nrocredito	= $_GET["Id"];
	$codsuc		= $_GET["codsuc"];

    $objReporte	=	new clsCreditos();
	$objReporte->AliasNbPages();
    $objReporte->AddPage();
	$objReporte->Contenido($nrocredito, $codsuc);
	$objReporte->Output();	
	
	
?>