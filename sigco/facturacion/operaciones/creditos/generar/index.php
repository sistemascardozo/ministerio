<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../include/main.php");
include("../../../../../include/claseindex.php");

$TituloVentana = "GENERACIÓN DE CREDITOS";

$Activo = 1;

CuerpoSuperior($TituloVentana);

$Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
$codsuc = $_SESSION['IdSucursal'];
$FormatoGrilla = array();
$documento = 8;

$Criterio = 'Creditos';

$Sql = "SELECT c.nrocredito,c.codantiguo,c.propietario,".$objFunciones->FormatFecha('c.fechareg').",
    CASE WHEN c.estareg = 0 THEN 'ANULADO' ELSE 'PENDIENTE' END,
    ".$objFunciones->Convert("c.imptotal", "NUMERIC (18,2)").",
    u.login,1,
    c.nroprepagoinicial,c.nroinscripcion,
    CASE WHEN c.nroprepagoinicial = 0 THEN 0 ELSE 1     END,
    CASE WHEN c.nroprepago = 0 THEN 0 ELSE 1 END,c.nroprepago,c.nroinspeccion,
    CASE WHEN c.nroinspeccion = 0 THEN 0 ELSE 1 END,c.nroprepagoinicial,
    CASE WHEN c.nroprepagoinicial = 0 THEN 0 ELSE 1 END,c.estareg
    FROM facturacion.cabcreditos as c
    INNER JOIN seguridad.usuarios as u ON (c.codemp=u.codemp and c.creador=u.codusu)";

$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
$FormatoGrilla[1] = array('1' => 'c.nrocredito', '2' => 'c.nroinscripcion', '3' => 'c.propietario', '4' => 'c.codantiguo');          //Campos por los cuales se hará la búsqueda
$FormatoGrilla[2] = $Op;                                                            //Operacion
$FormatoGrilla[3] = array('T1' => 'Nro. Credito', 'T2' => 'Nro.Inscripcion', 'T3' => 'Propietario', 'T4' => 'Fecha Emision', 'T5' => 'Estado',
    'T6' => 'Total', 'T7' => 'Usuario');   //Títulos de la Cabecera
$FormatoGrilla[4] = array('A1' => 'center', 'A3' => 'left', 'A4' => 'center', 'A6' => 'right', 'A7' => 'right', 'A8' => 'right');                        //Alineación por Columna
$FormatoGrilla[5] = array('W1' => '90', 'W4' => '90', 'W6' => '70', 'W7' => '60', 'W7' => '70', 'W8' => '150');                                 //Ancho de las Columnas
$FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
$FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
$FormatoGrilla[8] = "  and (c.codemp=1 and c.codsuc=".$codsuc.") ORDER BY c.nrocredito desc ";                                   //Orden de la Consulta
$FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
    'NB' => '7', //Número de Botones a agregar
    'BtnId1' => 'BtnModificar', //Nombre del Boton
    'BtnI1' => 'imprimir.png', //Imagen a mostrar
    'Btn1' => 'Imprimir Cronograma', //Titulo del Botón
    'BtnF1' => 'onclick="Imprimir(this.id);"', //Eventos del Botón
    'BtnCI1' => '18', //Item a Comparar
    'BtnCV1' => '1', //Valor de comparación
    'BtnId2' => 'BtnModificar', //Nombre del Boton
    'BtnI2' => 'imprimir.png', //Imagen a mostrar
    'Btn2' => 'Imprimir Nota de Pago', //Titulo del Botón
    'BtnF2' => 'onclick="ImprimirC(this);"', //Eventos del Botón
    'BtnCI2' => '18', //Item a Comparar
    'BtnCV2' => '1', //Valor de comparación
    'BtnId3' => 'BtnGenerar', //y aparece este boton
    'BtnI3' => 'documento.png',
    'Btn3' => 'Imprimir Contrato',
    'BtnF3' => 'onclick="imprimir_contrato(this.id)"',
    'BtnCI3' => '18',
    'BtnCV3' => '1',
    'BtnId4' => 'BtnModificar', //Nombre del Boton
    'BtnI4' => 'imprimir.png', //Imagen a mostrar
    'Btn4' => 'Imprimir Comprobante Total', //Titulo del Botón
    'BtnF4' => 'onclick="ImprimirCT(this);"', //Eventos del Botón
    'BtnCI4' => '12', //Item a Comparar
    'BtnCV4' => '1', //Valor de comparación
    'BtnId5' => 'BtnModificar', //Nombre del Boton
    'BtnI5' => 'imprimir.png', //Imagen a mostrar
    'Btn5' => 'Imprimir Solicitud de Servicio', //Titulo del Botón
    'BtnF5' => 'onclick="ImprimirS(this);"', //Eventos del Botón
    'BtnCI5' => '15', //Item a Comparar
    'BtnCV5' => '1', //Valor de comparación
    'BtnI6' => 'Dinero.png', //Imagen a mostrar
    'Btn6' => 'Pago de Cuotas', //Titulo del Botón
    'BtnF6' => 'onclick="Pagos(this);"', //Eventos del Botón
    'BtnCI6' => '18', //Item a Comparar
    'BtnCV6' => '1', //Valor de comparación
    'BtnId7' => 'BtnGenerar', //y aparece este boton
    'BtnI7' => 'imprimir.png',
    'Btn7' => 'Imprimir Inicial',
    'BtnF7' => 'onclick="imprimir_i(this)"',
    'BtnCI7' => '17',
    'BtnCV7' => '1'
);

$FormatoGrilla[10] = array(array('Name' => 'nroprepago', 'Col' => 13),
    array('Name' => 'nropago', 'Col' => 9), array('Name' => 'femision', 'Col' => 1), array('Name' => 'nroinscripcion', 'Col' => 10),
    array('Name' => 'nroinspeccion', 'Col' => 14), array('Name' => 'nrocredito', 'Col' => 1),
    array('Name' => 'nroprepagoinicial', 'Col' => 16)); //DATOS ADICIONALES
$FormatoGrilla[11] = 7; //FILAS VISIBLES            
$_SESSION['Formato'.$Criterio] = $FormatoGrilla;

Cabecera('', $FormatoGrilla[7], 1000, 600);
Pie();
?>

<script type="text/javascript">
    function imprimir_contrato(Id)
    {
        Id = Id.substr(Id.indexOf('-') + 1);

        AbrirPopupImpresion("imprimirc.php?Id=" + Id + "&codsuc=<?=$codsuc ?>", 800, 600);
    }
    function Imprimir(Id)
    {
        Id = Id.substr(Id.indexOf('-') + 1);

        AbrirPopupImpresion("imprimir.php?Id=" + Id + "&codsuc=<?=$codsuc ?>", 800, 600);
    }
    function ImprimirC(obj)
    {
        var nropago = $(obj).parent().parent().data('nropago')
        var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
        var femision = $(obj).parent().parent().data('femision')

        //lert(nropago);
        //if(nropago!=0)
        //{
        var url = '../../../../cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php'
        url += '?codpago=' + nropago + '&nroinscripcion=' + nroinscripcion + '&fechapago=' + femision + '&codsuc=<?=$codsuc ?>';

        var ventana = window.open(url, 'Buscar', 'width=800, height=600, resizable=yes, scrollbars=yes, status=yes,location=yes');
        ventana.focus();
        //}
    }
    function ImprimirCT(obj)
    {
        var nropago = $(obj).parent().parent().data('nroprepago')
        var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
        var femision = $(obj).parent().parent().data('femision')
        if (nropago != 0)
        {
            var url = '../../../../cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php'
            url += '?codpago=' + nropago + '&nroinscripcion=' + nroinscripcion + '&fechapago=' + femision + '&codsuc=<?=$codsuc ?>';
            var ventana = window.open(url, 'Buscar', 'width=800, height=600, resizable=yes, scrollbars=yes, status=yes,location=yes');
            ventana.focus();
        }
    }
    function ImprimirS(obj)
    {
        var nroinspeccion = $(obj).parent().parent().data('nroinspeccion')
        if (nroinspeccion != 0)
        {
            AbrirPopupImpresion("../../../../catastro/operaciones/solicitud/imprimir.php?nrosolicitud=" + nroinspeccion + '&codsuc=<?=$codsuc ?>&nuevocliente=0', 800, 600)

        }
    }
    function imprimir_i(obj)
    {
        // var nropago = $(obj).parent().parent().data('nroprepago')
        var nropago = $(obj).parent().parent().data('nroprepagoinicial')
        var nroinscripcion = $(obj).parent().parent().data('nroinscripcion')
        var femision = $(obj).parent().parent().data('femision')
        if (nropago != 0)
        {
            var url = '../../../../cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php'
            url += '?codpago=' + nropago + '&nroinscripcion=' + nroinscripcion + '&fechapago=' + femision + '&codsuc=<?=$codsuc ?>';
            var ventana = window.open(url, 'Buscar', 'width=800, height=600, resizable=yes, scrollbars=yes, status=yes,location=yes');
            ventana.focus();
        }
    }
    
    function Pagos(obj)
    {
        var nrocredito = $(obj).parent().parent().data('nrocredito')
        $("#Modificar").dialog({title: 'Pago Cuotas'});
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $("#button-maceptar").unbind("click");
        $("#button-maceptar").click(function () {
            GuardarOk();
        });
        $("#button-maceptar").children('span.ui-button-text').text('Pago Cuotas')
        $.ajax({
            url: 'pagos/index.php',
            type: 'POST',
            async: true,
            data: 'Id=' + nrocredito + '&codsuc=<?=$codsuc ?>',
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })
    }
    function GuardarOk()
    {

        var val = parseFloat($("#PagoAdelantado").val()).toFixed(2);

        if (val <= 0)
        {
            Msj($("#tbdetalle"), 'Seleccione Cuota a Cancelar')
            return false
        }


        $.ajax({
            url: 'pagos/Guardar.php?Op=0',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: $('#form1').serialize(),
            success: function (data)
            {

                if (data.res == 1)
                {

                    if (confirm("Desea Imprimir el Recibo?"))
                        AbrirPopupImpresion(urldir + "sigco/cobranza/operaciones/cancelacion/consulta/imprimir_boleta_prepago.php?codpago=" + data.nroprepagocre + "&nroinscripcion=" + $("#nroinscripcion").val() + '&fechapago=' + $("#femision").val() + '&codsuc=' + codsuc, 800, 600)

                    OperMensaje(data.res)
                    $("#Mensajes").html(data.res);
                    $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                    $("#Modificar").dialog("close");

                    // Buscar(Op);
                }

            }
        })
    }


    function ValidarEnter(e, Op)
    {
        switch (e.keyCode)
        {
            case $.ui.keyCode.UP:
                for (var i = 10; i >= 1; i--)
                {
                    if ($('tr[tabindex=\'' + i + '\']') != null)
                    {
                        var obj = $('tr[tabindex=\'' + i + '\']')
                        if (obj.is(':visible') && $(obj).parents(':hidden').length == 0)
                        {
                            $('tr[tabindex=\'' + i + '\']').focus();
                            e.preventDefault();
                            return false;
                            break;
                        }

                    }
                }
                ;
                $('#Valor').focus().select();
                break;
            case $.ui.keyCode.DOWN:

                for (var i = 1; i <= 10; i++)
                {
                    if ($('tr[tabindex=\'' + i + '\']') != null)
                    {
                        var obj = $('tr[tabindex=\'' + i + '\']')
                        if (obj.is(':visible') && $(obj).parents(':hidden').length == 0)
                        {
                            $('tr[tabindex=\'' + i + '\']').focus();
                            e.preventDefault();
                            return false;
                            break;
                        }

                    }
                }

                $('#Valor').focus().select();
                break;
            default:
                if (VeriEnter(e))
                    Buscar(Op);


        }


        //ValidarEnterG(evt, Op)
    }
</script>
<?php CuerpoInferior(); ?>
