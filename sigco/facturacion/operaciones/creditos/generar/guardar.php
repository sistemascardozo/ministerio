<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../../objetos/clsFunciones.php");

    $Op = $_GET["op"];

    $objFunciones = new clsFunciones();

    $codemp = 1;
    $codsuc = $_SESSION['IdSucursal'];
    $nroinscripcion = $_POST["nroinscripcion"];
    $codantiguo = $_POST["codantiguo"];
    $nombres = $_POST["nombres"];
    $nombrecalle = $_POST["direccion"];
    
    $imptotal = $_POST["imptotal"];
    $imptotal = str_replace(",", "", $imptotal);
    
    $igv = $_POST["igv"];
    $igv = str_replace(",", "", $igv);
    
    $redondeo = $_POST["redondeo"];
    $redondeo = str_replace(",", "", $redondeo);
    
    $subtotal = $_POST["subtotal"];
    $subtotal = str_replace(",", "", $subtotal);
    
    $codconcepto    = $_POST['codconceptoC1']; //0;//$_POST["codconcepto"];
    $nropresupuesto = $_POST["codexpediente"] ? $_POST["codexpediente"] : 0;
    $observacion    = $_POST["observacion"];
    $codusu     = $_SESSION['id_user'];
    $femision   = $objFunciones->CodFecha($_POST["femision"]);
    $count   = $_POST["count"];
    $interes = $_POST["interescredito"] ? $_POST["interescredito"] : 0;
    $nroprepago = $_POST["nprepago"] ? $_POST["nprepago"] : 0;
    $sininteres = $_POST["sininteres"] ? $_POST["sininteres"] : 0;
    $nrocredito = $objFunciones->setCorrelativosVarios(8, $codsuc, "SELECT", 0);
    //if($_POST['cuotainicialflag'] ==1)
    //	$nropago                    = $objFunciones->setCorrelativosVarios(5, $codsuc, "SELECT", 0);
    $nropago = 0;
    $nroprepagocre = $objFunciones->setCorrelativosVarios(6, $codsuc, "SELECT", 0);
    $creoprepago = false;
    $nroprepagocreInicial = $nroprepagocre + 1;
    $conexion->beginTransaction();
    
    /*
    $sqlC = "INSERT INTO facturacion.cabcreditos(codemp,codsuc,nrocredito,nroinscripcion,imptotal,igv,redondeo,subtotal,codconcepto,
    nropresupuesto,observacion,creador,fechareg,nroprepago,interes,sininteres,direccion,propietario,codantiguo) ";
    $sqlC .= " values(:codemp,:codsuc,:nrocredito,:nroinscripcion,:imptotal,:igv,:redondeo,
    :subtotal,:codconcepto,:nropresupuesto,:observacion,:creador,:fechareg,:nroprepago,:interes,:sininteres,:direccion,:propietario,:codantiguo)";
    */
    
    $sqlC = "INSERT INTO facturacion.cabcreditos(codemp, codsuc, nrocredito, nroinscripcion, imptotal, igv, redondeo, subtotal, codconcepto, ";
	$sqlC .= " nropresupuesto, observacion, creador, fechareg, nroprepago, interes, sininteres, direccion, propietario, codantiguo) ";
	$sqlC .= "VALUES(".$codemp.", ".$codsuc.", ".$nrocredito.", '".$nroinscripcion."', '".$imptotal."', '".$igv."', '".$redondeo."',
    '".$subtotal."', ".$codconcepto.", ".$nropresupuesto.", '".$observacion."', ".$codusu.", '".$femision."', ".$nroprepago.",
    '".$interes."', ".$sininteres.", '".$nombrecalle."', '".$nombres."', '".$codantiguo."') ";
    
    $resultC = $conexion->query($sqlC);
    /*
    $resultC = $conexion->prepare($sqlC);
    $resultC->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nrocredito" => $nrocredito,
        ":nroinscripcion" => $nroinscripcion,
        ":imptotal" => str_replace(",", "", $imptotal),
        ":igv" => str_replace(",", "", $igv),
        ":redondeo" => str_replace(",", "", $redondeo),
        ":subtotal" => str_replace(",", "", $subtotal),
        ":codconcepto" => $codconcepto,
        ":nropresupuesto" => $nropresupuesto,
        ":observacion" => $observacion,
        ":creador" => $codusu,
        ":fechareg" => $femision,
        ":nroprepago" => $nroprepago,
        ":interes" => $interes,
        ":sininteres" => $sininteres,
        ":direccion" => $nombrecalle,
        ":propietario" => $nombres,
        ":codantiguo" => $codantiguo));*/
    
    if ($resultC->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        $data['res'] = 2;
        die(json_encode($data));
    }
    
    for ($i = 1; $i <= $count; $i++)
	{
        if (isset($_POST["item".$i]))
		{
            $nrocuota = $_POST["item".$i];
            $totalc   = $_POST["totcuotas".$i];
            $fechave= $objFunciones->CodFecha($_POST["fvencimiento".$i]);
            $subt   = str_replace(",", "", $_POST["stotal".$i]);
            $igv    = str_replace(",", "", $_POST["igv".$i]);
            $red    = str_replace(",", "", $_POST["redondeo".$i]);
            $int    = $_POST["interes".$i];
            $impt   = str_replace(",", "", $_POST["imptotal".$i]);
            $nfacturacion = $_POST["nfacturacion".$i];
            $tipc   = $_POST["tipocuota".$i];
            $estado = $_POST["estado".$i];
            $anio   = $_POST["anio".$i];
            $mes    = $_POST["mes".$i];
            /*
            $sqlD = "INSERT INTO facturacion.detcreditos(codemp,codsuc,nrocredito,nrocuota,totalcuotas,fechavencimiento,subtotal,igv,
                redondeo,imptotal,nrofacturacion,tipocuota,anio,mes,interes,estadocuota) ";
            $sqlD .= " VALUES(:codemp,:codsuc,:nrocredito,:nrocuota,:totalcuotas,:fechavencimiento,:subtotal,
                :igv,:redondeo,:imptotal,:nrofacturacion,:tipocuota,:anio,:mes,:interes,:estadocuota)";
             * 
             */
            
            $sqlD = "INSERT INTO facturacion.detcreditos(codemp,codsuc,nrocredito,nrocuota,totalcuotas,
                fechavencimiento,subtotal,igv, redondeo,imptotal,nrofacturacion,tipocuota,anio,mes,interes,
                estadocuota) ";
            $sqlD .= " VALUES(".$codemp.", ".$codsuc.", ".$nrocredito.", ".$nrocuota.", ".$totalc.",
                '".$fechave."', '".$subt."', '".$igv."', '".$red."', '".$impt."', ".$nfacturacion.", ".$tipc.",
                ".$anio.", ".$mes.", '".$int."', ".$estado." )";
            $resultD = $conexion->query($sqlD);
            /*
            $resultD = $conexion->prepare($sqlD);            
            $resultD->execute(array(":codemp" => $codemp,
                ":codsuc" => $codsuc,
                ":nrocredito" => $nrocredito,
                ":nrocuota" => $_POST["item".$i],
                ":totalcuotas" => $_POST["totcuotas".$i],
                ":fechavencimiento" => $objFunciones->CodFecha($_POST["fvencimiento".$i]),
                ":subtotal" => str_replace(",", "", $_POST["stotal".$i]),
                ":igv" => str_replace(",", "", $_POST["igv".$i]),
                ":redondeo" => str_replace(",", "", $_POST["redondeo".$i]),
                ":interes" => $_POST["interes".$i],
                ":imptotal" => str_replace(",", "", $_POST["imptotal".$i]),
                ":nrofacturacion" => $_POST["nfacturacion".$i],
                ":tipocuota" => $_POST["tipocuota".$i],
                ":estadocuota" => $_POST["estado".$i],
                ":anio" => $_POST["anio".$i],
                ":mes" => $_POST["mes".$i]) );*/
            
            if ($resultD->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }
    //INSERTAR DETALLE DE COLATERALES
    $NroItems = $_POST['NroItems'];
    for ($i = 1; $i <= $NroItems; $i++) {
        if (isset($_POST["itemC".$i])) {
            $sqlD = "INSERT INTO facturacion.detcabcreditos 
            (  codemp,  codsuc,  nrocredito,  codconcepto,  item,  subtotal,  igv,  redondeo,  imptotal) 
            VALUES (  :codemp,  :codsuc,  :nrocredito,  :codconcepto,  :item,  :subtotal,  :igv,  :redondeo, 
            :imptotal);";

            $resultD = $conexion->prepare($sqlD);
            $resultD->execute(array(":codemp" => $codemp,
                ":codsuc" => $codsuc,
                ":nrocredito" => $nrocredito,
                ":codconcepto" => $_POST['codconceptoC'.$i],
                ":item" => $_POST["itemC".$i],
                ":subtotal" => str_replace(",", "", $_POST["subtotalC".$i]),
                ":igv" => str_replace(",", "", $_POST["igvC".$i]),
                ":redondeo" => str_replace(",", "", $_POST["redondeoC".$i]),
                ":imptotal" => str_replace(",", "", $_POST["imptotalC".$i]),
            ));
            if ($resultD->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }
    //INSERTAR DETALLE DE COLATERALES
	
    //SIEMPRE INSERTA PREPAGO POR EL TOTAL
    //CREAR UN PREPAGO CON EL TOTAL
    if ($nroprepago == 0) {
        //INSERTAR UN PREPAGO POR EL MONTO TOTAL
        $sql = "SELECT c.codcliente,c.propietario,c.codtipodocumento,c.nrodocumento,c.codciclo,tc.descripcioncorta,
                cl.descripcion as calle,c.nrocalle,c.codciclo,td.abreviado,c.codsector,
                c.codcalle
                FROM catastro.clientes as c
                INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
                inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
                inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
                where c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

        $result = $conexion->prepare($sql);
        $result->execute(array($codsuc, $nroinscripcion));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error SELECT Catastro";
            $data['res'] = 2;
            die(json_encode($data));
        }
        
        $item = $result->fetch();
        $propietario = strtoupper($item["propietario"]);
        $direccion = strtoupper($item["descripcioncorta"]." ".$item["calle"]." ".$item["nrocalle"]);
        $docidentidad = strtoupper($item["nrodocumento"]);
        
        if((trim($propietario))=='')
        { $propietario= $nombres; }
        
        if((trim($direccion))=='')
        { $direccion= $nombrecalle; }
        //$propietario = (empty(trim($propietario))) ? $nombres : $propietario;
        //$direccion = (empty(trim($direccion1))) ? $nombrecalle : $direccion1;

        $creoprepago = true;
        $condpago = $_POST["condpago"];
        $documento = $_POST["documentoC"];
        $formapago = 1;
        $fechareg = $femision;
        $default = array('1' => 9, '2' => 4, '3' => 2); //CAR DEFECTO
        $car = $_POST["carx"];
        if ($car == '')
            $car = $default[$codsuc];
        $nrocaja = $_SESSION["nocaja"];
        $idusuario = $_SESSION['id_user'];
        $hora = $objFunciones->HoraServidor();
        $eventualtext = 0;
        $nrofacturacion = 0;
        $npresupuesto = 0;
        $glosa = $_POST["glosa"];
        $sqlC = "INSERT INTO cobranza.cabprepagos(codemp,codsuc,nroinscripcion,nroprepago,car,codformapago,creador,fechareg,subtotal,hora,
            codciclo,condpago,igv,redondeo,imptotal,eventual,propietario,direccion,documento,glosa,nroinspeccion,estado,
            nrocredito,origen) 
            values(:codemp,:codsuc,:nroinscripcion,:nroprepago,:car,:codformapago,:creador,:fechareg,
            :subtotal,:hora,:codciclo,:condpago,:igv,:redondeo,:imptotal,:eventual,:propietario,:direccion,:documento,
            :glosa,:nroinspeccion,:estado,:nrocredito,:origen)";
        $resultC = $conexion->prepare($sqlC);
        $resultC->execute(array(":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":nroprepago" => $nroprepagocre,
            ":nroinscripcion" => $nroinscripcion,
            ":car" => $car,
            ":codformapago" => $formapago,
            ":creador" => $idusuario,
            ":fechareg" => $fechareg,
            ":subtotal" => str_replace(",", "", $_POST['subtotal']),
            ":hora" => $hora,
            ":codciclo" => 1,
            ":condpago" => $condpago,
            ":igv" => str_replace(",", "", $_POST['igv']),
            ":redondeo" => str_replace(",", "", $_POST['redondeo']),
            ":imptotal" => str_replace(",", "", $_POST['imptotal']),
            ":eventual" => $eventualtext,
            ":propietario" => $propietario,
            ":direccion" => $direccion,
            ":documento" => $docidentidad,
            ":glosa" => $glosa,
            ":nroinspeccion" => 0,
            ":estado" => 3,
            ":nrocredito" => $nrocredito,
            ":origen" => 1));
        if ($resultC->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error update cabpagos";
            $data['res'] = 2;
            die(json_encode($data));
        }
        
        $NroItems = $_POST['NroItems'];
        
		$Connn = 0;
		
        for ($i = 1; $i <= $NroItems; $i++)
		{
            if (isset($_POST["itemC".$i]))
			{
				$DocSeries = $objFunciones->GetSeries($codsuc, $documento);
                $seriedoc = $DocSeries["serie"];
                $numerodoc = $DocSeries["correlativo"];
                $fechas = $objFunciones->DecFechaLiteral();
                $anio = $fechas["anio"];
                $mes = $fechas["mes_num"];
                $facturacion = 0;
				
				$Connn++;
				
                $sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
				$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
				$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$Connn.", ".$_POST['codconceptoC'.$i].", ";
				$sqlD .= " '".str_replace(",", "", $_POST["subtotalC".$i])."', '".$_POST['Colateral'.$i]."', 0, '".$nrofacturacion."', ";
				$sqlD .= " ".$documento.", ".$numerodoc.", '".$seriedoc."', 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
				
                //$documento =13;//DOCUMENTO BOLETA POR EL MONTO TOTAL
                $resultD = $conexion->prepare($sqlD);
                $resultD->execute(array());

                if ($resultD->errorCode() != '00000') {
                    $conexion->rollBack();
                    $mensaje = "Error update cabpagos";
                    $data['res'] = 2;
                    die(json_encode($data));
                }
            }
        }
		//IGV
		if ($_POST['igv'] > 0)
		{
			$codconcepto = 5; //IGV
			
			$Connn++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$Connn.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $_POST['igv'])."', 'I.G.V.', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		$redondeo = $_POST['redondeo'];
		//REDONDEO
		if ($redondeo < 0)
		{
			$codconcepto = 7; //DIFERENCIA REDONDEO NEGATIVO
			
			$Connn++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$Connn.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $redondeo)."', 'DIFERENCIA REDONDEO NEGATIVO', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		if ($redondeo > 0)
		{
			$codconcepto = 8; //DIFERENCIA REDONDEO POSITIVO
			
			$Connn++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocre.", ".$Connn.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $redondeo)."', 'DIFERENCIA REDONDEO POSITIVO', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		
        //ACTUALIZAR CORRELATIVO
        $sql = "UPDATE  reglasnegocio.correlativos  
            SET  correlativo = correlativo + 1
            WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocorrelativo=".$documento;
        $result = $conexion->query($sql);
        if (!$result) {
            $conexion->rollBack();
            $mensaje = "619 Error UPDATE correlativos";
            $data['res'] = 2;
            $data['mensaje'] = $mensaje;
            die(json_encode($data));
        }
        //INSERTAR UN PREPAGO POR EL MONTO TOTAL
        //ACTUALIZAR NRO PAGO
        if (!$creoprepago)
            $nroprepagocre = 0;
        $sql = "UPDATE  facturacion.cabcreditos 
            SET  nroprepago=".$nroprepagocre."
            WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocredito=".$nrocredito."";
        $result = $conexion->query($sql);
        if (!$result) {
            $conexion->rollBack();
            $mensaje = "Error UPDATE correlativos";
            $data['res'] = 2;
            die(json_encode($data));
        }
    }
    //INSERTAR UN PREPAGO POR EL MONTO TOTAL
    //SIEMPRE INSERTA PREPAGO POR EL TOTAL
    ///////////////PAGO DE CUOTA INICIAL
    //die("-->".$_POST['cuotainicialflag']."<--");
    if ($_POST['cuotainicialflag'] == 1) {

        //INSERTA DIRECTAMENTE EN CAJA EL PAGO

        $sql = "SELECT c.codcliente,c.propietario,c.codtipodocumento,c.nrodocumento,c.codciclo,tc.descripcioncorta,
            cl.descripcion as calle,c.nrocalle,c.codciclo,td.abreviado,c.codsector,
            c.codcalle
            FROM catastro.clientes as c
            INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
            inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
            inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
            where c.codemp=1 and c.codsuc=? and c.nroinscripcion=?";

        $result = $conexion->prepare($sql);
        $result->execute(array($codsuc, $nroinscripcion));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error SELECT Catastro";
            $data['res'] = 2;
            die(json_encode($data));
        }
        
        $item = $result->fetch();
        $propietario = strtoupper($item["propietario"]);
        $direccion = strtoupper($item["descripcioncorta"]." ".$item["calle"]." ".$item["nrocalle"]);
        $docidentidad = strtoupper($item["nrodocumento"]);
        
        if((trim($propietario))=='')
        { $propietario= $nombres; }
        
        if((trim($direccion))=='')
        { $direccion= $nombrecalle; }
        //$propietario = (empty(trim($propietario))) ? $nombres : $propietario;
        //$direccion = (empty(trim($direccion))) ? $nombrecalle : $direccion;

        $condpago = $_POST["condpago"];
        $documento = 16; //NOTA DE PAGO//$_POST["documentox"];
        $formapago = 1; //$_POST["formapagox"];
        $fechareg = $femision;
        $default = array('1' => 9, '2' => 4, '3' => 2); //CAR DEFECTO
        $car = $_POST["carx"];
        if ($car == '')
            $car = $default[$codsuc];
        $nrocaja = $_SESSION["nocaja"];
        $idusuario = $_SESSION['id_user'];

        $imptotal = $_POST["imptotal1"];

        $efectivo = $_POST["efectivo_x"];
        $hora = $objFunciones->HoraServidor();
        $igv = $_POST["igv1"];
        $redondeo = $_POST["redondeo1"];
        $subtotal = $_POST["stotal1"]; //PRIMERA CUOTA
        //$nroprepago = 0;
        $fechas = $objFunciones->DecFechaLiteral();
        $anio = $fechas["anio"];
        $mes = $fechas["mes_num"];
        $facturacion = 0;

        $glosa = $_POST["glosa"];
        if ($glosa == '')
            $glosa = 'PAGO DE CUOTA INICIAL, CONVENIO NRO. '.$nrocredito;
        $eventualtext = 0;
        $nrofacturacion = 0;
        $npresupuesto = 0;


        /* $sqlC = "INSERT into cobranza.cabpagos(codemp,codsuc,nroinscripcion,nropago,car,nrocaja,codformapago,creador,imptotal,imppagado,hora,
          condpago,igv,redondeo,subtotal,propietario,direccion,nrodocumento,eventual,nroprepago,fechareg,anio,mes,glosa,nrocredito)
          values(:codemp,:codsuc,:nroinscripcion,:nropago,:car,:nrocaja,:codformapago,:creador,:imptotal,:imppagado,:hora,
          :condpago,:igv,:redondeo,:subtotal,:propietario,:direccion,:nrodocumento,:eventual,:nroprepago,:fechareg,:anio,:mes,:glosa,:nrocredito);";

          $result = $conexion->prepare($sqlC);
          $result->execute(array(":codemp" => $codemp,
          ":codsuc" => $codsuc,
          ":nroinscripcion" => $nroinscripcion,
          ":nropago" => $nropago,
          ":car" => $car,
          ":nrocaja" => $nrocaja,
          ":codformapago" => $formapago,
          ":creador" => $idusuario,
          ":imptotal" => str_replace(",", "", $imptotal),
          ":imppagado" => str_replace(",", "", $efectivo),
          ":hora" => $hora,
          ":condpago" => $condpago,
          ":igv" => str_replace(",", "", $igv),
          ":redondeo" => str_replace(",", "", $redondeo),
          ":subtotal" => str_replace(",", "", $subtotal),
          ":propietario" => $propietario,
          ":direccion" => $direccion,
          ":nrodocumento" => $docidentidad,
          ":eventual" => $eventualtext,
          ":nroprepago" => $nroprepago,
          ":fechareg" => $fechareg,
          ":anio" => $anio,
          ":mes" => $mes,
          ":glosa" => $glosa,
          ":nrocredito" => $nrocredito,
          )); */

        if ($creoprepago)
            $nroprepagocreInicial = $nroprepagocreInicial;
        else
            $nroprepagocreInicial = $nroprepagocre;
        $sqlC = "INSERT INTO cobranza.cabprepagos(codemp,codsuc,nroinscripcion,nroprepago,car,codformapago,creador,fechareg,subtotal,hora,
            codciclo,condpago,igv,redondeo,imptotal,eventual,propietario,direccion,documento,glosa,nroinspeccion,estado,
            nrocredito,origen) 
            values(:codemp,:codsuc,:nroinscripcion,:nroprepago,:car,:codformapago,:creador,:fechareg,
            :subtotal,:hora,:codciclo,:condpago,:igv,:redondeo,:imptotal,:eventual,:propietario,:direccion,:documento,
            :glosa,:nroinspeccion,:estado,:nrocredito,:origen)";
        $resultC = $conexion->prepare($sqlC);
        $resultC->execute(array(":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":nroprepago" => $nroprepagocreInicial,
            ":nroinscripcion" => $nroinscripcion,
            ":car" => $car,
            ":codformapago" => $formapago,
            ":creador" => $idusuario,
            ":fechareg" => $fechareg,
            ":subtotal" => str_replace(",", "", $subtotal),
            ":hora" => $hora,
            ":codciclo" => 1,
            ":condpago" => $condpago,
            ":igv" => str_replace(",", "", $igv),
            ":redondeo" => str_replace(",", "", $redondeo),
            ":imptotal" => str_replace(",", "", $imptotal),
            ":eventual" => $eventualtext,
            ":propietario" => $propietario,
            ":direccion" => $direccion,
            ":documento" => $docidentidad,
            ":glosa" => $glosa,
            ":nroinspeccion" => 0,
            ":estado" => 1,
            ":nrocredito" => $nrocredito,
            ":origen" => 1));

        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT CobranzaCabPagos";
            $data['res'] = 2;
            die(json_encode($data));
        }
		
        //OBTENER CORRELATIVOS ACTUALES
        $DocSeries = $objFunciones->GetSeries($codsuc, $documento);
        $seriedoc = $DocSeries["serie"];
        $numerodoc = $DocSeries["correlativo"];
        //OBTENER CORRELATIVOS ACTUALES
		
		$Itemm = 1;
		
        $codconcepto = 10; //CONCEPTO DE CUOTA INICIAL
		
        $sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
		$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
		$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
		$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocreInicial.", ".$Itemm.", ".$codconcepto.", ";
		$sqlD .= " '".str_replace(",", "", $subtotal)."', 'INICIAL DE CONVENIO', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
		$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
		
        $resultD = $conexion->prepare($sqlD);
        $resultD->execute(array());


        if ($resultD->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error INSERT detpagos";

            $data['res'] = 2;
            die(json_encode($data));
        }
		
		//IGV
		if ($igv > 0)
		{
			$codconcepto = 5; //IGV
			
			$Itemm++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocreInicial.", ".$Itemm.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $igv)."', 'I.G.V.', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		//REDONDEO
		if ($redondeo < 0)
		{
			$codconcepto = 7; //DIFERENCIA REDONDEO NEGATIVO
			
			$Itemm++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocreInicial.", ".$Itemm.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $redondeo)."', 'DIFERENCIA REDONDEO NEGATIVO', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		if ($redondeo > 0)
		{
			$codconcepto = 8; //DIFERENCIA REDONDEO POSITIVO
			
			$Itemm++;
			
			$sqlD = "INSERT INTO cobranza.detprepagos(codemp, codsuc, nroinscripcion, nroprepago, item, codconcepto, ";
			$sqlD .= " importe, detalle, tipopago, nrofacturacion, coddocumento, nrodocumento, serie, ";
			$sqlD .= " codtipodeuda, anio, mes, categoria, codcategoria, nropresupuesto) ";
			$sqlD .= "VALUES(".$codemp.", ".$codsuc.", ".$nroinscripcion.", ".$nroprepagocreInicial.", ".$Itemm.", ".$codconcepto.", ";
			$sqlD .= " '".str_replace(",", "", $redondeo)."', 'DIFERENCIA REDONDEO POSITIVO', 0, ".$nrofacturacion.", ".$documento.", ".$numerodoc.", '".$seriedoc."', ";
			$sqlD .= " 5, '".$anio."', '".$mes."', 3, 4, ".$npresupuesto.")";
			
			$resultD = $conexion->prepare($sqlD);
			$resultD->execute(array());
	
	
			if ($resultD->errorCode() != '00000') {
				$conexion->rollBack();
				$mensaje = "Error INSERT detpagos";
	
				$data['res'] = 2;
				die(json_encode($data));
			}
		}
		
        //ACTUALIZAR CORRELATIVO
        $Sql = "UPDATE reglasnegocio.correlativos ";
		$Sql .= " SET correlativo = correlativo + 1 ";
		$Sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrocorrelativo = ".$documento;
		
        $resultU = $conexion->query($Sql);
		
        if (!$resultU)
		{
            $conexion->rollBack();
            $mensaje = "Error UPDATE correlativos";
            $data['res'] = 2;
			
            die(json_encode($data));
        }
		
        //ACTUALIZAR NRO PAGO

        $sql = "UPDATE facturacion.cabcreditos 
            SET nroprepagoinicial=".$nroprepagocreInicial."
            WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocredito=".$nrocredito." ";
        $result = $conexion->query($sql);
        if (!$result) {
            $conexion->rollBack();
            $mensaje = "Error UPDATE correlativos";
            $data['res'] = 2;
            die(json_encode($data));
        }
        //ACTUALIZAR CUOTA INICIAL
        /* $sql = "UPDATE  facturacion.detcreditos 
          SET  estadocuota = 3
          WHERE codemp=1 AND  codsuc=".$codsuc." AND nrocredito=".$nrocredito." AND nrocuota=1 AND tipocuota=1";
          $result = $conexion->query($sql);
          if (!$result) {
          $conexion->rollBack();
          $mensaje = "Error UPDATE detcreditos";
          $data['res']    =2;
          die(json_encode($data)) ;
          } */
    }
    ///////////////PAGO DE CUOTA INICIAL
    ///////////ACUTALIZAR PREPAGO
    if ($nroprepago > 0) {
        $Sql = "UPDATE cobranza.cabprepagos  
            SET estado = 2,  nrocredito = :nrocredito
            WHERE codemp = :codemp AND codsuc = :codsuc AND nroinscripcion = :nroinscripcion AND nroprepago = :nroprepago";
        $resultC = $conexion->prepare($Sql);
        $resultC->execute(array(":nrocredito" => $nrocredito,
            ":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":nroinscripcion" => $nroinscripcion,
            ":nroprepago" => $nroprepago
        ));
        if ($resultC->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error reclamos";
            $data['res'] = 2;
            die(json_encode($data));
        }
    }
    //ACTUALIZAR EXPEDIENTE
    if ($nropresupuesto > 0) {


        $Sql = "UPDATE  solicitudes.expedientes  
                SET  concredito = 1,estadoexpediente=6
            WHERE codemp = :codemp AND codsuc = :codsuc AND  codexpediente = :codexpediente ";

        $result = $conexion->prepare($Sql);
        $result->execute(array(
            ":codemp" => $codemp,
            ":codsuc" => $codsuc,
            ":codexpediente" => $nropresupuesto
        ));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error update Expediente";
            $data['res'] = 2;
            die(json_encode($data));
        }

        $Sql = "SELECT nropresupuesto
            FROM  solicitudes.expedientesdetalle c
                WHERE c.codemp=? AND c.codsuc=? AND  c.codexpediente=? ";
        $consulta = $conexion->prepare($Sql);
        $consulta->execute(array($codemp, $codsuc, $nropresupuesto));
        $itemsD = $consulta->fetchAll();
        foreach ($itemsD as $rowD) {
            $Sql = "UPDATE solicitudes.cabpresupuesto  
                SET concredito = 1,estadopresupuesto=6
                WHERE codemp = :codemp AND codsuc = :codsuc AND nropresupuesto = :nropresupuesto";
            $resultC = $conexion->prepare($Sql);
            $resultC->execute(array(":codemp" => $codemp,
                ":codsuc" => $codsuc,
                ":nropresupuesto" => $rowD['nropresupuesto']
            ));
            if ($resultC->errorCode() != '00000') {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                $data['res'] = 2;
                die(json_encode($data));
            }
        }
    }


    ///////////ACUTALIZAR PREPAGO
    //die();
    if ($resultD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        $res = 2;
    } else {
        $conexion->commit();
        $objFunciones->setCorrelativosVarios(8, $codsuc, "UPDATE", $nrocredito);

        if ($creoprepago) {
            $objFunciones->setCorrelativosVarios(6, $codsuc, "UPDATE", $nroprepagocre);
            if ($_POST['cuotainicialflag'] == 1)
                $objFunciones->setCorrelativosVarios(6, $codsuc, "UPDATE", $nroprepagocreInicial);
            else
                $nroprepagocreInicial = 0;
        }
        else {
            if ($_POST['cuotainicialflag'] == 1)
                $objFunciones->setCorrelativosVarios(6, $codsuc, "UPDATE", $nroprepagocre);
            else
                $nroprepagocreInicial = 0;
        }


        $mensaje = "El Registro se ha Grabado Correctamente";
        $res = 1;
    }

    $data['res'] = $res;
    $data['nropago'] = 0; // $nropago;
    $data['nrocredito'] = $nrocredito;
    $data['nroprepagocre'] = $nroprepagocre;
    $data['nroprepagocreInicial'] = $nroprepagocreInicial;
    echo json_encode($data);

?>
