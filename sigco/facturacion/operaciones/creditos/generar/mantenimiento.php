<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../../objetos/clsDrop.php");

$Op = $_GET["Op"];
$Id = isset($_GET["Id"]) ? $_GET["Id"] : '';
$codsuc = $_SESSION['IdSucursal'];
$guardar = "op=".$Op;
$formapago = 1;
$fecha = date("d/m/Y");

$objMantenimiento = new clsDrop();
$sininteres = 0;
//$facturacion 	= $objMantenimiento->datosfacturacion($codsuc);
$paramae = $objMantenimiento->getParamae("IMPIGV", $codsuc);

$sql = "SELECT nrofacturacion,anio,mes,saldo,lecturas,tasainteres,tasapromintant
    FROM facturacion.periodofacturacion
    WHERE codemp=1 and codsuc=? and codciclo=1 and facturacion=0";

$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc));
$row = $consulta->fetch();
$TasaInteres = $row['tasapromintant'] / 100;
?>
<style type="text/css">
    .CodConcepto { display: none;}
</style>
<script>

    var cont = 0;
    var nDest = 0;
    var nDestC = 0;
    var object = "";
    var codsuc = <?=$codsuc ?>;
    var fechaserver = "<?=$fecha ?>";
    var nrofact = 0;
    var anio = 0;
    var mes = 0;
    var igv = <?=$paramae["valor"] ?>;
    var urldir = "<?php echo $_SESSION['urldir']; ?>";
    var TasaInteres = <?=$TasaInteres ?>;
    var TasaInteresO = <?=$TasaInteres ?>;
    function ValidarForm(Op)
    {
        if ($("#nroinscripcion").val() == "" || $("#nroinscripcion").val() == 0)
        {
            $("#tabs").tabs({selected: 0});
            Msj($("#nroinscripcion"), "Ingrese el Codigo del Cliente")
            $("#nroinscripcion").focus();
            return
        }
        $("#nroinscripcion").removeClass("ui-state-error");
        /*if($("#codconcepto").val()=="" || $("#codconcepto").val()==0)
         {
         $("#tabs").tabs({selected: 0});
         Msj($("#codconcepto"),"Ingrese el Concepto Colateral")
         return
         }*/
        if ($("#count").val() == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#tbdetalle"), "No hay Ninguna Cuota Generada...Genere como minima una para poder continuar")
            return
        }

        if ($("#codexpediente").val() == "") {
            $("#codexpediente").val(0)
        }
        var inicial = false
        var imptotal = 0;
        var estadocuota = 0;
        for (i = 1; i <= $("#count").val(); i++)
        {
            if ($("#categoria" + i).val() == 1)
            {
                inicial = true
                imptotal = $("#imptotal" + i).val()
                estadocuota = $("#estado" + i).val()
            }

        }
        ///CALCULAR IMPUT DE COLATERALES
        var id = parseInt($("#TbIndex tbody tr").length)
        var tr = '';
        var CodConcepto, SubTotal, IGV, Redondeo, Colateral = '';
        for (var i = 1; i <= id; i++)
        {

            CodConcepto = $("#TbIndex tbody tr#" + i + " label.CodConcepto").text()
            SubTotal = $("#TbIndex tbody tr#" + i + " label.SubTotal").text()
            IGV = $("#TbIndex tbody tr#" + i + " label.IGV").text()
            Redondeo = $("#TbIndex tbody tr#" + i + " label.Redondeo").text()
            Importe = $("#TbIndex tbody tr#" + i + " label.Importe").text()
            Colateral = $("#TbIndex tbody tr#" + i + " label.Colateral").text()
            SubTotal = str_replace(SubTotal, ',', '');
            IGV = str_replace(IGV, ',', '');
            Redondeo = str_replace(Redondeo, ',', '');
            Importe = str_replace(Importe, ',', '');
            tr += '<input type="hidden" name="itemC' + i + '"  value="' + i + '"/>';
            tr += '<input type="hidden" name="codconceptoC' + i + '"  value="' + CodConcepto + '"/>';
            tr += '<input type="hidden" name="subtotalC' + i + '"  value="' + SubTotal + '"/>';
            tr += '<input type="hidden" name="igvC' + i + '"  value="' + IGV + '"/>';
            tr += '<input type="hidden" name="redondeoC' + i + '"  value="' + Redondeo + '"/>';
            tr += '<input type="hidden" name="imptotalC' + i + '"  value="' + Importe + '"/>';
            tr += '<input type="hidden" name="Colateral' + i + '"  value="' + Colateral + '"/>';
        }
        tr += '<input type="hidden" name="NroItems" id="Items"  value="' + id + '"/>';
        $("#DivSave").html(tr)
        ///CALCULAR IMPUT DE COLATERALES
        $("#cuotainicialflag").val(0)
        if (inicial && estadocuota != 3)//DIFERENTE DE CANCELADO
        {
            //validar_usuario_caja(imptotal);
            $("#cuotainicialflag").val(1)
            $("#tpagar").val(parseFloat(imptotal))
            $("#efectivo").val(parseFloat(imptotal))
            GuardarP(Op)

        }
        else
            GuardarP(Op)
    }
    function Cancelar()
    {
        location.href = 'index.php?Op=1';
    }
    function CambiarPresupuesto(Check)
    {
        if (Check.checked == true)
        {
            $("#codexpediente").val(0)
            $("#prepago").val(0)
            $("#nprepago").val()
            $("#chkprepago").attr('checked', false)
            /*document.getElementById('subtotal').readOnly 		= true
             document.getElementById('cuotainicial').readOnly 	= true
             document.getElementById('nrocuotas').readOnly 		= true
             document.getElementById('cuotamensual').readOnly 	= true*/
        } else {
            $("#codexpediente").val(0)
            /*document.getElementById('subtotal').readOnly 		= false
             document.getElementById('cuotainicial').readOnly 	= false
             document.getElementById('nrocuotas').readOnly 		= false
             document.getElementById('cuotamensual').readOnly 	= false*/
            TasaInteres = TasaInteresO
            QuitaRow();

            $("#subtotal").val(0.00)
            $("#cuotainicial").val(0.00)
            $("#nrocuotas").val(0.00)
            $("#cuotamensual").val(0.00)
            $("#colateral").val("")
            $("#codconcepto").val(0)
            $("#importe").val(0)

            $("#nroexpediente").val("")
            $("#tipopresupuesto").val("")

        }
    }
    function CambiarPrepago(Check)
    {
        if (Check.checked == true)
        {
            $("#prepago").val(1)

            document.getElementById('subtotal').readOnly = true
            $("#codexpediente").val(0)
            $("#nroexpediente").val()
            $("#chkpresupuesto").attr('checked', false)


        }
        else {
            $("#prepago").val(0)
            document.getElementById('subtotal').readOnly = false
            TasaInteres = TasaInteresO

            QuitaRow();

            $("#subtotal").val(0.00)
            $("#cuotainicial").val(0.00)
            $("#nrocuotas").val(0.00)
            $("#cuotamensual").val(0.00)
            $("#colateral").val("")
            $("#codconcepto").val(0)
            $("#importe").val(0)

            $("#nprepago").val("")
            $("#tipopresupuesto").val("")

        }

    }
    /////
    function ValidarEnterCre(e, op)
    {
        if (VeriEnter(e))
        {
            switch (op)
            {
                case 1:
                    AgregarItem();
                    break;
                case 2:
                    datos_conceptos($("#CodConcepto").val())
                    break;


            }
            e.returnValue = false;
        }
    }
    function AgregarItem()
    {
        var CodConcepto = $("#CodConcepto").val();
        var Colateral = $("#Colateral").val();
        var subtotal = $("#subtotal").val();
        var igv = $("#igv").val();
        var redondeo = $("#redondeo").val();
        var importe = $("#importe").val();


        var j = parseInt($("#TbIndex tbody tr").length)

        for (var i1 = 1; i1 <= j; i1++)
        {
            Codigo = $("#TbIndex tbody tr#" + i1 + " label.CodConcepto").text();

            if (Codigo == CodConcepto)
            {
                Msj('#Colateral', 'El Colateral ' + Colateral + ' ya esta Agregado')
                return false;
            }
        }

        if (CodConcepto != "" && CodConcepto != "")
        {
            if (subtotal == "" || parseFloat(subtotal) == 0)
            {
                Msj("#subtotal", 'Digite un Precio v&aacute;lido')
                return false;
            }

            $("#subtotal").removeClass("ui-state-error");
            //
//			subtotal = number_format(subtotal, 2)
//			
//			var redondeo_manual = parseFloat(CalcularRedondeo(number_format(subtotal, 2)));
//			
//			subtotal = parseFloat(subtotal) + redondeo_manual
//     	igv=number_format(igv,2)
//     	redondeo=number_format(redondeo,2)
//     	importe=number_format(importe,2)
//			importe = parseFloat(importe) + parseFloat(redondeo_manual)
            var i = j + 1;
            var tr = '<tr id="' + i + '" onclick="SeleccionaId(this);">';
            tr += '<td align="center" ><label class="Item">' + i + '</label></td>';
            tr += '<td align="left"><label class="CodConcepto">' + CodConcepto + '</label>';
            tr += '<label class="Colateral">' + Colateral + '</label></td>';
            tr += '<td align="right"><label class="SubTotal">' + subtotal + '</label></td>';
            tr += '<td align="right"><label class="IGV">' + igv + '</label></td>';
            tr += '<td align="right"><label class="Redondeo">' + redondeo + '</label></td>';
            tr += '<td align="right"><label class="Importe">' + importe + '</label></td>';
            tr += '<td align="center" ><a href="javascript:QuitaItemc(' + i + ')" class="Del" >';
            tr += '<span class="icono-icon-trash" title="Quitar Colateral"></span> ';
            tr += '</a></td></tr>';
            //alert(Precio)

            $("#TbIndex tbody").append(tr);
            $("#CodConcepto").val("");
            $("#Colateral").val("");
            $("#subtotal").val("0.00");
            $("#igv").val('0.00')
            $("#redondeo").val('0.00');
            $("#importe").val('0.00');
            $("#CodConcepto").focus();


            CalcularTotal()

        }
        else
        {
            if (Trim(CodConcepto) == "")
            {
                Msj('#CodConcepto', 'Seleccione un Colateral')
                return false;
            }
            else
            {
                Msj('#subtotal', 'Digite una  Precio v&aacute;lido')
                return false;
            }

        }

    }
    function CalcularTotal()
    {

        var j = parseInt($("#TbIndex tbody tr").length);
        var importetotal = 0;
        var subtotaltotal = 0;
        var igvtotal = 0;
        var redondeototal = 0;
        var t = 0;
        for (var i = 1; i <= j; i++)
        {
            t = $("#TbIndex tbody tr#" + i + " label.SubTotal").text()
            t = str_replace(t, ',', '');
            subtotaltotal += parseFloat(t);
            t = $("#TbIndex tbody tr#" + i + " label.IGV").text()
            t = str_replace(t, ',', '');
            igvtotal += parseFloat(t);
            t = $("#TbIndex tbody tr#" + i + " label.Redondeo").text()
            t = str_replace(t, ',', '');
            redondeototal += parseFloat(t);
            t = $("#TbIndex tbody tr#" + i + " label.Importe").text()
            t = str_replace(t, ',', '');
            importetotal += parseFloat(t);



        }

        $('#subtotaltotal').val(number_format(subtotaltotal, 2));
        $('#igvtotal').val(number_format(igvtotal, 2));
        $('#redondeototal').val(number_format(redondeototal, 2));
        $('#importetotal').val(number_format(importetotal, 2));
        $("#TotalCalculo").html(number_format(importetotal, 2))
    }
    function QuitaItemc(tr)
    {
        var id = parseInt($("#TbIndex tbody tr").length);


        $("#TbIndex tbody tr#" + tr).remove();
        var nextfila = tr + 1;
        var j;
        var IdMat = "";
        for (var i = nextfila; i <= id; i++)
        {
            j = i - 1;//alert(j);
            $("#TbIndex tbody tr#" + i + " label.Item").text(j);
            $("#TbIndex tbody tr#" + i + " a.Del").attr("href", "javascript:QuitaItemc(" + j + ")");
            $("#TbIndex tbody tr#" + i).attr("id", j);

        }
        CalcularTotal()

    }
</script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir']; ?>js/funciones.js" language="JavaScript"></script>
<script src="js_creditos.js" type="text/javascript"></script>
<div align="center">
    <form id="form1" name="form1" method="post" action="guardar.php?<?php echo $Guardar; ?>" enctype="multipart/form-data">
        <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos del Cr&eacute;dito</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Colaterales</a></li>
                                <li style="font-size:10px"><a href="#tabs-3">Cronograma</a></li>
                            </ul>
                            <div id="tabs-1" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="6" align="right" >Fecha Emision :
                                            <input type="text" name="femision" id="femision" maxlength="15" class="inputtext" readonly="readonly" value="<?=$fecha ?>" style="width:80px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="95" >N&deg; Inscripci&oacute;n</td>
                                        <td width="30" align="center">:</td>
                                        <td width="360" >
                                            <input type="hidden" name="nroinscripcion" id="nroinscripcion" class="inputtext" size="12" maxlength="12" readonly="readonly" />
                                            <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" value="<?php echo $row["codantiguo"] ?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:75px;"/>

                                            <span class="MljSoft-icon-buscar" title="Buscar Clientes" onclick="buscar_usuario();"></span>
                                            <input type="hidden" name="anio" id="anio" />
                                            <input type="hidden" name="mes" id="mes" />
                                            <input type="hidden" name="nrof" id="nrof" />
                                            <input type="hidden" id="IgvP" value="<?=$paramae["valor"] ?>" />
                                            <input type="hidden" id="cuotainicialflag" name="cuotainicialflag" value="0" />
                                            <input type="hidden" name="condpago" id="condpago" value="0">
                                            <input type="hidden" id="formapagox" name="formapagox" />
                                            <input type="hidden" id="documentox" name="documentox" />
                                            <input type="hidden" id="carx" name="carx" />
                                            <input type="hidden" id="tpagarx" name="tpagarx" />
                                            <input type="hidden" id="efectivo_x" name="efectivo_x" />
                                            <input type="hidden" id="vuelto_x" name="vuelto_x" />
                                            <input type="hidden" id="numerodocx" name="numerodocx" />
                                            <input type="hidden" id="seriedocx" name="seriedocx" />
                                            <input type="hidden" id="codcliente" name="codcliente" />
                                            <div id="DivSave"></div>
                                        </td>
                                        <td width="114" align="right" >Nro Pre Pago</td>
                                        <td width="18" align="center">:</td>
                                        <td width="278" >
                                            <input type="text" name="nprepago" id="nprepago" maxlength="15" class="inputtext" readonly="readonly" value="" style="width:80px;" />
                                            <span class="MljSoft-icon-buscar" title="Buscar Pre-Pagos" onclick="buscar_prepagos();"></span>

                                            <label>
                                                <input type="checkbox" name="chkprepago"  id="chkprepago" onclick="CambiarPrepago(this)"/>
                                                <input type="hidden" name="prepago" id="prepago" value="0" />
                                                Con Pre Pago </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >Nombres</td>
                                        <td align="center">:</td>
                                        <td >
                                            <input type="text" name="nombres" id="nombres" class="inputtext" maxlength="200" readonly="readonly" style="width:350px;" />
                                        </td>
                                        <td align="right">Nro. de Expediente </td>
                                        <td align="center">:</td>
                                        <td >
                                            <input type="text" id="nroexpediente" maxlength="15" class="inputtext" readonly="readonly" value="" style="width:80px;" />

                                            <span class="MljSoft-icon-buscar" title="Buscar Presupuesto" onclick="buscar_presupuesto();"></span>
                                            <label>
                                                <input type="checkbox" name="chkpresupuesto"  id="chkpresupuesto" onclick="CambiarPresupuesto(this)"/>
                                                <input type="hidden" name="codexpediente" id="codexpediente" value="0" />
                                                Con Expediente </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >Direccion</td>
                                        <td align="center">:</td>
                                        <td ><input type="text" name="direccion" id="direccion" class="inputtext" maxlength="200" readonly="readonly" style="width:350px;" /></td>
                                        <td align="right" style="display:none">Tipo de Presup </td>
                                        <td align="center" style="display:none">:</td>
                                        <td style="display:none"><input type="text" name="tipopresupuesto" id="tipopresupuesto" size="20" maxlength="20" class="inputtext" readonly="readonly" value="" /></td>
                                    </tr>
                                    <tr>
                                        <td>Documento</td>
                                        <td align="center">:</td>
                                        <td>
                                            <select id='documentoC' name='documentoC' class='select' style='width:250px' onChange="cSerieNunC(this)" >
                                                <?php
                                                $Sql = "SELECT * FROM reglasnegocio.documentos
						           	WHERE estareg = 1 AND tipodocumento = 0 AND codsuc = ".$codsuc." AND (coddocumento = 13 OR coddocumento=  14)";

                                                $consultad = $conexion->prepare($Sql);
                                                $consultad->execute();
                                                $itemsd = $consultad->fetchAll();
                                                foreach ($itemsd as $rowd) {
                                                    $selected = "";
                                                    if ($rowd["coddocumento"] == 13) {
                                                        $selected = "selected='selected'";
                                                    }
                                                    echo "<option value='".$rowd["coddocumento"]."' ".$selected." >".strtoupper($rowd["descripcion"])."</option>";
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Serie</td>
                                        <td align="center">:</td>
                                        <td>
                                            <input type="text" name="seriedocC" id="seriedocC" class="inputtext" size="3" maxlength="45"  readonly="readonly" value=""/>
                                            -
                                            <input type="text" name="numerodocC" id="numerodocC" class="inputtext" size="7" maxlength="45"  readonly="readonly" value=""/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >Observacion</td>
                                        <td align="center" valign="top">:</td>
                                        <td colspan="4">
                                            <textarea name="observacion" id="observacion" class="inputtext" style="font-size:11px; width:600px;" rows="5"></textarea>
                                        </td>
                                    </tr>
                                    <tr style="height:5px">
                                        <td colspan="6" class="TitDetalle"><input type="hidden" name="count" id="count" value="0" /></td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-2" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr class="TrAddItem">
                                                    <td colspan="2"  >
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" rules="all" >
                                                            <tr>
                                                                <td align="center">
                                                                    <input type="text" class="entero text inputtext ui-corner-all" title="C&oacute;digo del Colateral " id="CodConcepto" style="width:80px" onkeypress="ValidarEnterCre(event, 2);" />
                                                                    <span class="MljSoft-icon-buscar" title="Buscar Colateral" onclick="buscar_conceptos();"></span>
                                                                    <input type="text" class="text inputtext ui-corner-all" style="width:350px" title="Descripci&oacute;n del Colateral" id="Colateral"  />

                                                                    <input type="text" id="subtotal" title="Sub Total" class="numeric inputtext ui-corner-all text" style="width:80px" onkeypress="ValidarEnterCre(event, 1);" value="0.00" onkeyup="CalcularImporte();"/>
                                                                    <input type="text" id="igv"  class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                    <input type="text" id="redondeo" class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                    <input type="text" id="importe" class="numeric inputtext ui-corner-all text" readonly="readonly" value="0.00" style="width:80px"/>
                                                                    <span class="icono-icon-add" title="Agregar Producto" onclick="AgregarItem()"></span>
                                                                    <input type="hidden" name="afecto_igv" id="afecto_igv" value="0" />

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" >
                                                        <div style="height:auto; overflow:auto" align="center" id="DivDetalle">
                                                            <table width="800" border="0" align="center" cellspacing="0" class="ui-widget" id="TbIndex">
                                                                <thead class="ui-widget-header" >
                                                                    <tr >
                                                                        <th width="43" align="center" scope="col">Item</th>
                                                                        <th align="center" scope="col">Colateral</th>
                                                                        <th width="80" align="center" scope="col">Subtotal</th>
                                                                        <th width="80" align="center" scope="col">IGV</th>
                                                                        <th width="80" align="center" scope="col">Redondeo</th>
                                                                        <th width="80" align="center" scope="col">Importe</th>
                                                                        <th width="5" align="center" scope="col">&nbsp;</th>
                                                                    </tr>

                                                                </thead>
                                                                <tbody>


                                                                </tbody>
                                                                <tfoot class="ui-widget-header">

                                                                    <tr>
                                                                        <td colspan="2" align="right" >Total :</td>
                                                                        <td align="right" >
                                                                            <input type="text" id="subtotaltotal" name="subtotal" value="<?=number_format($Total, $Presicion) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="igvtotal" name="igv" value="<?=number_format($Total, $Presicion) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="redondeototal" name="redondeo" value="<?=number_format($Total, $Presicion) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                        </td>
                                                                        <td align="right" >
                                                                            <input type="text" id="importetotal" name="imptotal" value="<?=number_format($Total, $Presicion) ?>" class="text inputtext ui-corner-all" readonly="readonly" style="text-align:right;width:60px"  />
                                                                        </td>
                                                                        <td >&nbsp;</td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="tabs-3" style="height:380px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="6">
                                            <fieldset style="padding:4px">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="8%">Total</td>
                                                        <td width="2%" align="center">:</td>
                                                        <td colspan="7">
                                                            <label id="TotalCalculo"></label>

                                                                                  <!-- <input type="text" name="colateral" id="colateral" size="80" maxlength="80" class="inputtext" readonly="readonly" onclick="buscar_conceptos();"/>

                                                                                  <input type="hidden" name="codconcepto" id="codconcepto" value="0" /> -->

                                                            <input type="hidden" name="glosa" id="Glosa" value="" />
                                                            </label></td>
                                                        <td colspan="4"><label>
                                                                <?php
                                                                $checked = "";
                                                                if ($sininteres == 1)
                                                                    $checked = "checked='checked'";
                                                                ?>
                                                                <input type="checkbox"  id="chksininteres" onclick="habilitar_sininteres();" <?=$checked ?> /> Sin Interes
                                                                <input type="hidden" id="sininteres" name="sininteres" value="<?=$sininteres ?>">
                                                            </label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>C. Inicial </td>
                                                        <td align="center">:</td>
                                                        <td width="10%"><label>
                                                                <input type="text" name="cuotainicial" id="cuotainicial" size="15" maxlength="15" class="inputtext focusselect" onkeypress="return permite(event, 'num');" value="0.00" onkeyup="CalcularCuota();" />
                                                            </label></td>
                                                        <td width="8%" align="right">N&ordm; Cuotas </td>
                                                        <td width="2%" align="center">:</td>
                                                        <td width="11%"><input type="text" name="nrocuotas" id="nrocuotas" size="15" maxlength="15" class="inputtext focusselect" onkeypress="return permite(event, 'num');" onkeyup="CalcularCuota();" /></td>
                                                        <td width="10%" align="right">Cuota Mens </td>
                                                        <td width="2%" align="center">:</td>
                                                        <td width="11%" ><input type="text" name="cuotamensual" id="cuotamensual" size="15" maxlength="15" class="inputtext" onkeypress="return permite(event, 'num');" value="0.00" onkeyup="calcularnrocuota(this);" />
                                                        </td>
                                                        <td width="10%" align="right">
                                                            Interes</td>
                                                        <td width="2%" align="center">:</td>
                                                        <td width="10%" align="left">
                                                            <label id="Lblinteres"><?=number_format($row['tasapromintant'], 2) ?></label>%
                                                            <input type="hidden" id="interescredito" name="interescredito" value="<?=$row['tasapromintant'] ?>"/>
                                                        </td>
                                                        <td width="14%" align="left"><img src="../../../../../images/iconos/add.png" width="16" height="16" style="cursor:pointer" title="Generar Cuotas para el Credito" onclick="agregar_cuotas();" /></td>
                                                    </tr>
                                                </table>
                                            </fieldset></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="TitDetalle">
                                            <div style="height:270px; overflow:scroll">
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable01"  width="100%" id="tbdetalle" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="8%" >Nro. Cuota </th>
                                                            <th width="10%" >Fecha de Vencimiento</th>
                                                            <th width="8%" >Saldo</th>
                                                            <th width="8%" >Amortizaci&oacute;n</th>
                                                            <th width="8%" >Interes</th>
                                                            <th width="5%" >Igv</th>
                                                            <th width="9%" >Importe</th>
                                                            <th width="12%" >Categoria</th>
                                                            <th width="10%" >Nro. Facturacion </th>
                                                            <th width="10%" >Estado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
        <div id="DivInicial" title="Comprobante Cuota Inicial" style="display: none; font-size:14; font-weight:bold">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ui-widget-content">
                <tr>
                    <td align="right">Forma</td>
                    <td align="center">:</td>
                    <td>

                        <? $objMantenimiento->drop_forma_pago($formapago,""," where estareg=1 and tipodocumento=0"); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right">Car</td>
                    <td align="center">:</td>
                    <td >
                        <? $objMantenimiento->drop_car($codsuc); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right">Documento</td>
                    <td align="center">:</td>
                    <td>
                        <? $objMantenimiento->drop_documentos(13,'onChange="cSerieNun(this)"'," where estareg=1 and tipodocumento=0 codsuc=".$codsuc); ?>

                    </td>
                </tr>
                <tr>
                    <td align="right">Serie</td>
                    <td align="center">:</td>
                    <td>
                        <input type="text" name="seriedoc" id="seriedoc" class="inputtext" size="3" maxlength="45"  readonly="readonly" value=""/>

                        -<input type="text" name="numerodoc" id="numerodoc" class="inputtext" size="7" maxlength="45"  readonly="readonly" value=""/>

                    </td>
                </tr>
                <tr>
                    <td align="right">Glosa</td>
                    <td align="center">:</td>
                    <td >
                        <span class="icono-icon-detalle" title="Glosa" onclick="VerGlosa()"></span>
                    </td>
                </tr>
                <tr><td colspan="3"><td>&nbsp;</td></td></tr>
                <tr>
                    <td style="font-size:12px; font-weight:bold">Total a Pagar</td>
                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
                    <td><label>
                            <input type="text" name="tpagar" id="tpagar" readonly="readonly" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right; background-color:#CCC" value="0.00" />
                        </label></td>
                </tr>
                <tr>
                    <td style="font-size:12px; font-weight:bold">Efectivo</td>
                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
                    <td><input type="text" name="efectivo" id="efectivo" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right" onkeypress="ValidarEfect(event);
                    return permite(event, 'num');" onkeyup="calcular_vuelto(this);" value="0.00" /></td>
                </tr>
                <tr>
                    <td style="font-size:12px; font-weight:bold">Vuelto</td>
                    <td align="center" style="font-size:18px; font-weight:bold">:</td>
                    <td><input type="text" name="vueltox" id="vueltox" size="15" maxlength="15" style="font-size:18px; font-weight:bold; color:#F00; text-align:right; background-color:#CCC" readonly="readonly" value="0.00" /></td>
                </tr>
            </table>
        </div>
        <div id="DivGlosa" title="Observaciones"  >
            <table width="100%" border="0">
                <tr>
                    <td> Glosa:</td>
                    <td>
                        <textarea class="inputtext ui-corner-all" id="GlosaTemp" title="Glosa" cols="50" rows="5" ></textarea>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>
