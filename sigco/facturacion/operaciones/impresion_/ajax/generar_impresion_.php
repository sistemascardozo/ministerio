<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	
	set_time_limit(0);
	ini_set("display_errors", 1);
	ini_set("memory_limit", "64M");
	
	$objFunciones = new clsFunciones();
	
	$codciclo 	= $_POST["codciclo"];
	$codsuc 	= $_POST["codsuc"];
	$generado	= $_POST["generado"];
	
	
	
	$facturacion 	= $objFunciones->datosfacturacion($codsuc,$codciclo);
	$paramae		= $objFunciones->getParamae("LIMLEC",$codsuc);
	$nrofacturacion = ($facturacion["nrofacturacion"]-1);
	
	//if($generado==1)
	//{
		/*$del = $conexion->prepare("delete from facturacion.impresion_recibos where codsuc=? and codciclo=? and nrofacturacion=?");
		$del->execute(array($codsuc,$codciclo,$nrofacturacion));*/
		
	//}

	$conexion->beginTransaction();

	$del = $conexion->prepare("delete from facturacion.impresion_recibos where codsuc=? and codciclo=?");
	$del->execute(array($codsuc,$codciclo));
	$sql_generar = $conexion->prepare("select * from facturacion.f_generar_recibos_facturacion(?,?,?,?)");
	$sql_generar->execute(array($codsuc,$codciclo,$nrofacturacion,$paramae["valor"]));
	
	//ACTUALIZAR FECHA POR SECTORES
	 $Sql = "SELECT codsector,fechadeudores,fechacorte  
				from facturacion.impresion_recibos_fechas
				WHERE codemp=:codemp AND codsuc=:codsuc AND codciclo=:codciclo
			AND  nrofacturacion=:nrofacturacion";
	$consultaf = $conexion->prepare($Sql);
	$consultaf->execute(array(":codemp"=>1,
									  ":codsuc"=>$codsuc,
									 ":codciclo"=>$codciclo,
									 ":nrofacturacion"=>$nrofacturacion
									));	
	foreach($consultaf->fetchAll() as $fechas)
	{

		
	 	$Sql="UPDATE facturacion.impresion_recibos
	 		SET fechavencimientodeudores=:fechavencdeudores, fechacorte=:fechacorte
	 		WHERE  codemp=:codemp AND codsuc=:codsuc AND codciclo=:codciclo
			AND  nrofacturacion=:nrofacturacion AND codsector=:codsector";
			$consulta_Periodo = $conexion->prepare($Sql);
			$consulta_Periodo->execute(array(":codemp"=>1,
									  ":codsuc"=>$codsuc,
									 ":codciclo"=>$codciclo,
									 ":nrofacturacion"=>$nrofacturacion,
									 ":codsector"=>$fechas['codsector'],
									 ":fechavencdeudores"=>$fechas['fechadeudores'],
									 ":fechacorte"=>$fechas['fechacorte']
									 ));
			if ($consulta_Periodo->errorCode() != '00000')
			{
				$conexion->rollBack();
				$res=2;
				$mensaje = "Error Cuando se ha Registrado los Datos";
				die($res."|".$mensaje);
			}
	}
	 //ACTUALIZAR FECHA POR SECTORES
	//var_dump($sql_generar->errorInfo());
	if(!$sql_generar)
	{
		$conexion->rollBack();
		
		$img 		= "<img src='".$urldir."images/error.png' width='31' height='31' />";
		$mensaje 	= "Error al Tratar de Generar los Recibos";
		$res		= 0;
	}else{
		$conexion->commit();
		
		$img 		 = "<img src='".$urldir."images/Ok.png' width='31' height='31' />";
		$mensaje 	 = "Los Recibos se han Generado Correctamente";
		$mensaje 	.= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
		$res		 = 1;
	}
	
	echo $img."|".$mensaje."|".$res;
	
?>