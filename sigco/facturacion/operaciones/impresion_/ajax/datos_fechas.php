<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 	= $_POST["codsuc"];
	$codciclo 	= $_POST["codciclo"];
	
	$facturacion = $objFunciones->datosfacturacion($codsuc, $codciclo);
	
	$nrofacturacion = ($facturacion["nrofacturacion"] - 1);
	
	$sql = "SELECT DISTINCT s.codsector, s.descripcion, f.fechadeudores, f.fechacorte ";
	$sql .= "FROM public.sectores s ";
	$sql .= " INNER JOIN facturacion.impresion_recibos_fechas f ON(f.codemp = s.codemp) AND (f.codsuc = s.codsuc) AND (f.codsector = s.codsector) ";
	$sql .= "WHERE s.codemp = 1 AND s.estareg = 1 AND s.codsuc = ".$codsuc." ";
	$sql .= " AND f.nrofacturacion = ".$nrofacturacion." ";
	$sql .= "ORDER BY s.codsector ASC";
	
	$ConsultaTar =$conexion->query($sql);
	foreach($ConsultaTar->fetchAll() as $tarifas)
	{
?>
	  <h3><?=strtoupper($tarifas["descripcion"])?></h3>
	  <div style="height: 100px; display: block;">
	    <table width="100%" border="0" align="center" cellspacing="0" class="ui-widget">
		     <tr>
		         <td align="center"></td>
		        <td align="center"></td>
		        </tr>
		      </thead>
		       <tr>
    				<td align="right">Vencimiento Deudores</td>
    				<td>:</td>
                    <td>
                    	<input type="text" class="fecha" name="fechadeudores_<?=$tarifas['codsector']?>" id="fechadeudores_<?=$tarifas['codsector']?>" maxlength="10" size="13" value="<?=$objFunciones->DecFecha($tarifas['fechadeudores'])?>" />
                    </td>
                </tr>
                <tr>
                	<td align="right">Corte a Deudores</td>
                	<td>:</td>
                    <td>
                    	<input type="text" class="fecha" name="fechacorte_<?=$tarifas['codsector']?>" id="fechacorte_<?=$tarifas['codsector']?>" maxlength="10" size="13" value="<?=$objFunciones->DecFecha($tarifas['fechacorte'])?>" />
	                 </td>
	            </tr>
		      
		      </table>
	  </div>
	  <?php
	  	}
?>