<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp			= 1;
	$codocurrencia 	= $_POST["codocurrencia"]?$_POST["codocurrencia"]:$_POST["1form1_id"];
	$sucursal		= $_SESSION["IdSucursal"];

	
	$codtipoocurrencia	= $_POST["codtipoocurrencia"];
	$codcliente		= $_POST["codcliente"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$nromed     	= $_POST["nromed"];
	$marcamed		= $_POST["marcamed"];
	$fechamov       = $objFunciones->CodFecha($_POST["fechamov"]);    
	$observacion 	= strtoupper($_POST["observacion"]);
	$idusuario		= $_SESSION['id_user'];
	$estareg		= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("ocurrencias",$sucursal,"0");
			$codocurrencia 	= $id[0];

			$sql = "INSERT INTO facturacion.ocurrencia(codocurrencia,codemp,codsuc,codtipoocurrencia,
					codcliente,nroinscripcion,nromed,marcamed,fechamov,observacion,codusu,estareg) 
				VALUES(:codocurrencia,:codemp,:codsuc,:codtipoocurrencia,:codcliente,
					:nroinscripcion,:nromed,:marcamed,:fechamov,:observacion,:codusu,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codocurrencia"=>$codocurrencia,":codemp"=>$codemp,":codsuc"=>$sucursal,
				":codtipoocurrencia"=>$codtipoocurrencia, ":codcliente"=>$codcliente,":nroinscripcion"=>$nroinscripcion,
				":nromed"=>$nromed,":marcamed"=>$marcamed,":fechamov"=>$fechamov,
				":observacion"=>$observacion, ":codusu"=>$idusuario, ":estareg"=>$estareg ));
		break;
		case 1:
			$sql = "UPDATE facturacion.ocurrencia SET
			fechamov= :fechamov,nromed=:nromed,marcamed=:marcamed,
			codtipoocurrencia=:codtipoocurrencia,
			observacion=:observacion,
			estareg=:estareg
			WHERE codemp=:codemp and codsuc=:codsuc AND codocurrencia=:codocurrencia 
			AND nroinscripcion=:nroinscripcion AND codcliente=:codcliente ";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codocurrencia"=>$codocurrencia,":codemp"=>$codemp,":codsuc"=>$sucursal,
				":codtipoocurrencia"=>$codtipoocurrencia, ":codcliente"=>$codcliente,":nroinscripcion"=>$nroinscripcion,
				":nromed"=>$nromed,":marcamed"=>$marcamed,":fechamov"=>$fechamov,
				":observacion"=>$observacion, ":estareg"=>$estareg ));
		break;
		case 2:case 3:
			$sql = "UPDATE facturacion.ocurrencia set estareg=:estareg
				WHERE codemp=:codemp and codsuc=:codsuc AND codocurrencia=:codocurrencia";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codocurrencia"=>$codocurrencia,":estareg"=>$estareg,":codemp"=>$codemp,":codsuc"=>$sucursal));
		break;
	
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
