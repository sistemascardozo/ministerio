<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	$TituloVentana = "OCURRENCIAS";
	$Activo=1;
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	
	if($Op!=5) CuerpoSuperior($TituloVentana);
	
	$codsuc = $_SESSION['IdSucursal'];
	
	$FormatoGrilla = array();
	
	$Sql = "SELECT
		oc.codocurrencia,
		cl.propietario,
		tp.descripcion,
		cl.nromed,
		".$objFunciones->FormatFecha('oc.fechamov').",
		oc.observacion,
		es.descripcion,
		oc.estareg
		FROM
		facturacion.ocurrencia AS oc
		INNER JOIN public.estadoreg AS es ON es.id = oc.estareg
		INNER JOIN catastro.clientes AS cl ON (cl.nroinscripcion = oc.nroinscripcion AND cl.codcliente = oc.codcliente AND cl.codemp = oc.codemp AND cl.codsuc = oc.codsuc) 
		INNER JOIN public.tipoocurrencia AS tp ON tp.codtipoocurrencia = oc.codtipoocurrencia";
 
	$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql);                                                           //Sentencia SQL
	$FormatoGrilla[1] = array('1'=>'oc.codocurrencia', '2'=>'cl.propietario','3'=>'cl.nromed' );          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Nombre y Apellidos', 'T3'=>'Tipo Ocurrencia', 'T4'=>'Nro. Medidor', 'T5'=>'Fecha','T6'=>'Observacion','T7'=>'Estado');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'left', 'A5'=>'left');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1'=>'50');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = " AND (oc.codemp=1 and oc.codsuc=".$codsuc." ) ORDER BY oc.fechamov ASC ";                                   //Orden de la Consulta
	
	if($Op!=5)
		$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'8', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'8', 
              'BtnCV3'=>'0');
	else
    {
		$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'8',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
      
   }
       
	$FormatoGrilla[10] = array(array('Name' =>'id', 'Col'=>1));//DATOS ADICIONALES            
	$_SESSION['Formato'] = $FormatoGrilla;
	
	Cabecera('', $FormatoGrilla[7],700,450);
	Pie();
	
	if($Op!=5) CuerpoInferior();
?>
<script type="text/javascript">
function Enviar(obj)
  {
    var Id = $(obj).parent().parent().data('id')
    opener.Recibir(Id);
    window.close();
  }
  if(<?=$Op?>==5)
    $("#")
</script>