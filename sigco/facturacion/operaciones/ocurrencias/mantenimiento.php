<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");
	
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	
	$guardar	= "op=".$Op;
	$fechamov   = date('d/m/Y');

	$objMantenimiento 	= new clsDrop();
	$codsuc 		= $_SESSION["IdSucursal"];
	
	if($Id!='')
	{
		$sql = "SELECT
			oc.codsuc,
			oc.codocurrencia,
			oc.codcliente,
			oc.nroinscripcion,
			oc.nromed,
			oc.marcamed,
			oc.codtipoocurrencia,
			oc.fechamov,
			oc.codusu,
			oc.observacion,
			oc.estareg,
			cl.propietario

			FROM
			facturacion.ocurrencia AS oc
			INNER JOIN catastro.clientes AS cl ON cl.nroinscripcion = oc.nroinscripcion AND cl.codcliente = oc.codcliente AND cl.codemp = oc.codemp AND cl.codsuc = oc.codsuc
			LEFT JOIN catastro.conexiones AS co ON (co.codemp = cl.codemp AND co.codsuc = cl.codsuc AND co.nroinscripcion = cl.nroinscripcion)
			INNER JOIN public.marcamedidor AS ma ON ma.codmarca = co.codmarca
			WHERE oc.codocurrencia=? AND oc.codsuc= ? ";
		$consulta = $conexion->prepare($sql);
        $consulta->execute(array($Id,$codsuc));
        $tpocurrencia = $consulta->fetch();

        $fechamov= $objMantenimiento->DecFecha($tpocurrencia['fechamov']);
        //$tiopoc= $tpocurrencia['codtipoocurrencia'];

		$guardar		= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var urldir = '<?php echo $_SESSION['urldir'];?>';
    var codsuc 	= '<?=$codsuc?>';

	$(document).ready(function() {
		$("#fechamov").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
				showButtonPanel: true
            }
        );

	});
	
	function buscar_usuarios()
    {
        object = "usuario"
        AbrirPopupBusqueda('<?php echo $_SESSION['urldir'];?>sigco/catastro/operaciones/actualizacion/index_med.php?Op=5', 1150, 450)
    }
    
    function Recibir(id)
    {
        if (object == "usuario")
        {
            $("#nroinscripcion").val(id)
            cargar_datos_usuario(id)
        }

    }
    
    function cargar_datos_usuario(nroinscripcion)
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>ajax/clientes_instalacion.php',
            type: 'POST',
            async: true,
            data: 'codsuc='+codsuc+'&nroinscripcion='+nroinscripcion,
            dataType: "json",
            success: function(datos) {
                $("#codcliente").val(datos.codcliente)
                $("#Cliente").val(datos.propietario)
                $("#codantiguo").val(datos.codantiguo)                
                $("#nromed").val(datos.nromed)
                $("#marcamed").val(datos.marcamed)                
                $("#nroinscripcion").val(datos.nroinscripcion)
                //alert(datos.consumo)
            }
        });
    }
    function MostrarDatosUsuario(datos)
	{
		$("#nroinscripcion").val(datos.nroinscripcion)
		$("#Cliente").val(datos.propietario)
		
		
	}
	function ValidarForm(Op)
	{
	
		if($("#descripcion").val()=="")
		{
			Msj($("#descripcion"),"Ingrese Nombre y Apellido del Inspector");
			return false
		}
		if($("#tipodocumento").val()=="")
		{
			Msj($("#tipodocumento"),"Seleccione el Tipo de Documento");
			return false
		}
		if($("#nrodocumento").val()=="")
		{
			Msj($("#nrodocumento"),"Ingrese el Nro. Documento");
			return false
		}

		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
	<table width="660" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
			 	<td width="90" class="TitDetalle">&nbsp;</td>
			 	<td width="30" class="TitDetalle">&nbsp;</td>
				<td class="CampoDetalle">&nbsp;</td>
			</tr>
			<tr>
		    	<td class="TitDetalle">Id</td>
		    	<td align="center" class="TitDetalle">:</td>
		    	<td class="CampoDetalle">
					<input name="codocurrencia" type="text" id="Id" size="4" maxlength="2" value="<?=$Id?>" readonly="readonly" class="inputtext"/>
		     	</td>
			</tr>
			<tr>
                <td class="TitDetalle">Fecha</td>
                <td align="center" class="TitDetalle">:</td>
                <td>
                    <input type="text" name="fechamov" id="fechamov" class="inputtext" maxlentgth="6" value="<?=$fechamov?>" style="width:80px;" />
                </td>
            </tr>
            <tr>
				<td class="TitDetalle">Inscripci&oacute;n</td>
				<td align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<input type="hidden" name="nroinscripcion" id="nroinscripcion" size="10" class="inputtext entero"  value="<?php echo $row["nroinscripcion"]?>" />
			        <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" value="<?php echo $row["codantiguo"]?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:70px;"/>
			           <span class="MljSoft-icon-buscar" title="Buscar Personas" onclick="buscar_usuarios();"></span>
				<input type="hidden" name="codcliente" id="codcliente" size="10" class="inputtext" readonly="readonly" value="<?=$tpocurrencia["codcliente"]?>"/>
                    <input class="inputtext" name="propietario" type="text" id="Cliente" maxlength="200"value="<?=$tpocurrencia["propietario"]?>" placeholder="Propietario" style="width:400px;" />
					
				</td>
			</tr>			
			<tr>
				<td class="TitDetalle">Tipo</td>
				<td align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<select name="codtipoocurrencia" id="codtipoocurrencia" style="width:200px">
				        <?php
				            $Sql = "SELECT * FROM public.tipoocurrencia AS tp WHERE tp.estareg = 1 ORDER BY codtipoocurrencia";
				            $Consulta = $conexion->query($Sql);
				            foreach ($Consulta->fetchAll() as $rows) {
				                $Sel = "";
				                if ($tpocurrencia["codtipoocurrencia"] == $rows["codtipoocurrencia"])
				                    $Sel = 'selected="selected"';
				                ?>
				                <option value="<?php echo $rows["codtipoocurrencia"]; ?>" <?=$Sel ?>  ><?=$rows['codtipoocurrencia']?>-<?php echo strtoupper(utf8_decode($rows["descripcion"])); ?></option>
				        <?php } ?>
				    </select>
				</td>
			</tr>
			
			<tr>
				<td class="TitDetalle">Nro. Medidor</td>
				<td align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="nromed" type="text" id="nromed" size="15" maxlength="15" value="<?=$tpocurrencia["nromed"]?>" readonly="readonly"/>
				</td>
			</tr>
			<tr>
				<td class="TitDetalle">Marca Medidor</td>
				<td align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<input class="inputtext" name="marcamed" type="text" id="marcamed" size="15" maxlength="15" value="<?=$tpocurrencia["marcamed"]?>" readonly="readonly"/>
				</td>
			</tr>
			<tr>
                <td class="TitDetalle" valign="top">Observacion</td>
                <td align="center" valign="top" class="TitDetalle">:</td>
                <td colspan="3" valign="top">
                	<textarea name="observacion" id="observacion" class="text ui-widget-content ui-corner-all" style="width:490px;height:100px" ><?=$tpocurrencia["observacion"]; ?></textarea>
                </td>
            </tr> 
			<tr>
				<td class="TitDetalle">Estado</td>
				<td align="center" class="TitDetalle">:</td>
				<td class="CampoDetalle">
					<?php include("../../../../include/estareg.php"); ?>
				</td>
			</tr>
			<tr>
				<td class="TitDetalle">&nbsp;</td>
				<td class="TitDetalle">&nbsp;</td>
				<td class="CampoDetalle">&nbsp;</td>
			</tr>
   </tbody>
  </table>
 </form>
</div>
<script>
 $("#descripcion").focus();
</script>
<?php
	$est = isset($tpocurrencia["estareg"])?$tpocurrencia["estareg"]:1;	
	include("../../../../admin/validaciones/estareg.php"); 
?>