<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
    
    include("../../../../../objetos/clsFunciones.php");
	
    $objFunciones = new clsFunciones();
    
    set_time_limit(0);
    file_put_contents('../progreso.txt','0|Calculando..');
    $codsuc         = $_POST["codsuc"];
    $codciclo       = $_POST["codciclo"];
    $porcentaje     = $_POST["porcentaje"];
    $idusuario      = $_SESSION['id_user'];
    $nrofacturacion = $_POST["nrofacturacion"];
    $calcalc        = $_POST["factura_alcantarillado"]; 
    $anio           = $_POST["anio"];
    $mes            = $_POST["mes"];

    $EstMedProm = array();
    $EstLecProm = array();
    
    $Sql="SELECT codestlectura FROM public.estadolectura WHERE estareg=1 AND promediar=1";
    foreach($conexion->query($Sql)->fetchAll() as $row)
    {
        $EstLecProm[$row[0]]=$row[0];
    }
    $Sql="SELECT codestadomedidor FROM public.estadomedidor WHERE estareg=1 AND promediar=1";
    foreach($conexion->query($Sql)->fetchAll() as $row)
    {
        $EstMedProm[$row[0]]=$row[0];
    }
    //ACTUALIZAR PARA EVITAR UN ERROR DE DIGITACION//
	$Sql = "UPDATE catastro.clientes  SET consumo = 0 WHERE consumo = 'NaN'";
	$conexion->query($Sql);
	
	$Sql = "UPDATE catastro.clientes  SET lecturaestimada = 0 WHERE lecturaestimada = 'NaN'";
	$conexion->query($Sql);
	
	$Sql = "UPDATE medicion.lecturas  SET consumo = 0 WHERE consumo = 'NaN'";
	$conexion->query($Sql);

    $conexion->beginTransaction();
    $del = "delete FROM facturacion.detprefacturacion WHERE codciclo=:codciclo and nrofacturacion=:nrofacturacion and codsuc=:codsuc";
    $consDel = $conexion->prepare($del);
    $consDel->execute(array(":codciclo"=>$codciclo,":nrofacturacion"=>$nrofacturacion,":codsuc"=>$codsuc));

    $del_c = "delete FROM facturacion.cabprefacturacion WHERE codciclo=:codciclo and nrofacturacion=:nrofacturacion and codsuc=:codsuc";
    $consDelc = $conexion->prepare($del_c);
    $consDelc->execute(array(":codciclo"=>$codciclo,":nrofacturacion"=>$nrofacturacion,":codsuc"=>$codsuc));
        

	$Sql="UPDATE catastro.clientes  SET digitado = 0";

    //$conexion->query($Sql);
    /*Recupera los Datos de los documentos para la facturacion*/
    $sqldoc  = "SELECT coddocumento,serie,correlativo ";
    $sqldoc .= "FROM reglasnegocio.correlativos ";
    $sqldoc .= "WHERE coddocumento=4 and codemp=1 and codsuc=?";
    
    $consDoc = $conexion->prepare($sqldoc);
    $consDoc->execute(array($codsuc));
    $itemsDoc = $consDoc->fetch();
    
    $documento      = $itemsDoc["coddocumento"];
    $seriedoc       = $itemsDoc["serie"];
    $correlativo    = $itemsDoc["correlativo"];
    
    /*Obtiene el Porcentaje de Interes del Mes para la Deuda*/
    $porcentaje = $porcentaje/100;
    
    $porcalc    = $objFunciones->getParamae("PORALC",$codsuc);
    $Pigv       = $objFunciones->getParamae("IMPIGV",$codsuc);
    $Pigv       =$Pigv["valor"];
    
        /*Inicio del Proceso de Calculo de Facturacion*/
        $sqlC   = "SELECT * FROM facturacion.view_usuariofacturar 
               WHERE codemp=1 and codsuc=? and codestadoservicio<>7 and codtipousuario=1 
                       and estareg=1 and codciclo=? 
                ORDER by codsuc,codsector,CAST(codrutlecturas AS INTEGER ),CAST(codmanzanas AS INTEGER) ,CAST(lote AS INTEGER) ";
    
        
        $consultaC = $conexion->prepare($sqlC);
        $consultaC->execute(array($codsuc,$codciclo));
        $itemsC = $consultaC->fetchAll();
        
        $totalAvance = count($itemsC);
        $actualAvance=0;
        foreach($itemsC as $rowC)
        {
            $ItemDetalle=0;
            $conscategoria  = 0;
            $impagua        = 0;
            $impdesague     = 0;
            $impigv         = 0;
            $ConsumoFact = $objFunciones->ConsumoFac($rowC["lecturapromedio"],$rowC['tipofacturacion'],$rowC["consumo"],$rowC['codestlectura'],$rowC['codestadomedidor']);
            $Flag = true;//CALCULAR IMPORTE DE ACTIVOS Y AGUA DE CORTADOS CON DEUDA DE 2 MESE
            $Imprimir = 1;//SI SE VA IMPRIMIR EL RECIBO SI(1)
            $consumo_facturado=$rowC["consumo"];
            if($rowC['codtipoentidades']!=85)
            {
                
                
                switch ($rowC["codestadoservicio"]) 
                {
                    case 1:
                        $correlativotem = $correlativo;
                        $correlativo++; 
                    break;
                    case 2: //CORTADOS
                        
                    if($rowC['tipofacturacion'] != 2 ):
                        //CALCULAR MESES QUE DEBE
                        $Sql="select * from facturacion.deuda_facturacion(".$codsuc.",".$rowC["nroinscripcion"].")";
                        $rowDeuda= $conexion->query($Sql)->fetch();
                        $MesesDeuda =$rowDeuda['mesesdeuda'];
                        
                        if($MesesDeuda>2)
                        {
                            $Flag = false;
                            $Imprimir = 0;
                            $correlativotem=0;
                        }
                        else
                        {
                            if($MesesDeuda==2 && $rowDeuda['consecutivo']==1 && $rowDeuda['maxfacdeuda']==($nrofacturacion-1) )
                            {
                                $correlativotem = $correlativo;
                                $correlativo++;
                            }
                            else
                            {
                                if($MesesDeuda==0) 
                                {

                                    $Flag = false;
                                    $Imprimir = 0;
                                    $correlativotem=0;
                                }
                                else
                                {
                                    if($MesesDeuda==1 && $rowDeuda['maxfacdeuda']==($nrofacturacion-1) )
                                    {
                                        $correlativotem = $correlativo;
                                        $correlativo++;
                                    }
                                    else
                                    {
                                        $Flag = false;
                                        $Imprimir = 0;
                                        $correlativotem=0;
                                    }
                                }
                            }                            
                        }

                    else:
                        $Flag = false; //No se Factura
                        $Imprimir = 0;  //No se Imprime
                        $correlativotem = 0;  //No lleva Correlativo
                    endif;

                    break;
                    case 3: case 4: case 5: case 0: case 6: 
                        $correlativotem=0; //SIN NUMERO DE RECIBO
                        $Flag = false; //NO FACTURA AGUA,ETC
                        $Imprimir = 0; //NO IMPRIMIR
                    break;
                    default:
                        $correlativotem=0; //SIN NUMERO DE RECIBO
                        $Flag = false; //NO FACTURA AGUA,ETC
                        $Imprimir = 0; //NO IMPRIMIR

                }
            }
            else
            {
                $correlativotem=0; //SIN NUMERO DE RECIBO
                $Flag = false; //NO FACTURA AGUA,ETC
                $Imprimir = 0; //NO IMPRIMIR
            }
            
            if($Flag)
            {
                $Sql    = "select * from facturacion.f_getconsfacturado(:codsuc,:catetar,:consumo,:tipofacturacion)";
                $consFact   = $conexion->prepare($Sql);
                $consFact->execute(array(":codsuc"=>$codsuc,
                                 ":catetar"=>$rowC["catetar"],
                                 ":consumo"=>intval($ConsumoFact),
                                 ":tipofacturacion"=>$tipofacturacionfact
                                 ));
                $consumo_facturado = $consFact->fetch();
                $consumo_facturado=$consumo_facturado[0];
            }

            $TotalParaIgv=0;
            //CABECERA
            $Sql     = "insert into facturacion.cabprefacturacion(codemp,codsuc,codciclo,nrofacturacion,nroinscripcion,coddocumento,nrodocumento,
                            catetar,lecturaultima,fechalectultima,lecturaanterior,fechalectanterior,consumo,consumofact,tipofacturacion,codestadoservicio,
                            creador,anio,mes,propietario,serie,codtiposervicio,fecharevmedidor,lecturapromedio,exoneraalc,imprimir,codzona,hora,fechareg,
                            codestlectura,codestadomedidor,tipofacturacionfact,lecturaestimada)
                            values(:codemp,:codsuc,:codciclo,:nrofacturacion,:nroinscripcion,:coddocumento,:nrodocumento,:catetar,:lecturaultima,:fechalectultima,
                            :lecturaanterior,:fechalectanterior,:consumo,:consumofact,:tipofacturacion,:codestadoservicio,:creador,:anio,:mes,:propietario,
                            :serie,:codtiposervicio,:fecharevmedidor,:lecturapromedio,:exoneraalc,:imprimir,:codzona,:hora,:fechafacturacion,
                        :codestlectura,:codestadomedidor,:tipofacturacionfact,:lecturaestimada)";
                            
            $consCab = $conexion->prepare($Sql);
            $consCab->execute(array(":codemp"=>1,
                                    ":codsuc"=>$codsuc,
                                    ":codciclo"=>$codciclo,
                                    ":nrofacturacion"=>$nrofacturacion,
                                    ":nroinscripcion"=>$rowC["nroinscripcion"],
                                    ":coddocumento"=>$documento,
                                    ":nrodocumento"=>$correlativotem,
                                    ":catetar"=>$rowC["catetar"],
                                    ":lecturaultima"=>$rowC["lecturaultima"],
                                    ":fechalectultima"=>$rowC["fechalecturaultima"],
                                    ":lecturaanterior"=>$rowC["lecturaanterior"],
                                    ":fechalectanterior"=>$rowC["fechalecturaanterior"],
                                    ":consumo"=>$rowC["consumo"],
                                    ":consumofact"=>$consumo_facturado,
                                    ":tipofacturacion"=>$rowC["tipofacturacion"],
                                    ":codestadoservicio"=>$rowC["codestadoservicio"],
                                    ":creador"=>$idusuario,
                                    ":anio"=>$anio,
                                    ":mes"=>$mes,
                                    ":propietario"=>$rowC["propietario"],
                                    ":serie"=>$seriedoc,
                                    ":codtiposervicio"=>$rowC["codtiposervicio"],
                                    ":fecharevmedidor"=>$rowC["fecharevmedidor"],
                                    ":lecturapromedio"=>$rowC["lecturapromedio"],
                                    ":exoneraalc"=>$rowC["exoneralac"],
                                    ":imprimir"=>$Imprimir,
                                    ":codzona"=>$rowC["codzona"],
                                    ":hora"=>$horafac,
                                    ":fechafacturacion"=>$fechafacturacion,
                                    ":codestlectura"=> $rowC['codestlectura'],
                                    ":codestadomedidor"=>$rowC['codestadomedidor'],
                                    ":tipofacturacionfact"=>$tipofacturacionfact,
                                    ":lecturaestimada"=>$rowC['lecturaestimada']
                                    ));

            if($consCab->errorCode()!='00000')
            {
                $conexion->rollBack();
                $mensaje = "Error INSERT cabfacturacion ".$rowC["nroinscripcion"].$sql;
                die($mensaje);
            }

            if($Flag)
                {
                   $Sql = "SELECT catetar,porcentaje,principal 
                            FROM catastro.unidadesusoclientes 
                            where codemp=1 and codsuc=".$codsuc." and 
                            nroinscripcion=".$rowC["nroinscripcion"];
                    $itemsU = $conexion->query($Sql)->fetchAll();
                    $num_total_unidades= count($itemsU);
                    if($num_total_unidades==1)              
                    {
                        $itemsU = $itemsU[0];
                        $items = $objFunciones->ImporteTarifa($codsuc, $itemsU["catetar"], $tipofacturacionfact, $ConsumoFact, $nrofacturacion);

                        $impagua    = empty($items[0])?0:$items[0];
                        $impdesague = empty($items[1])?0:$items[1];
                        
                        /*if($calcalc==1)
                        {
                            $impdes = round($items[0]*($porcalc/100),2);
                        }else{*/
                            $impdesague = round($impdesague,2);
                        //}
                
                        $insU = "INSERT into facturacion.unidadesusoprefacturadas
                                (codemp,codsuc,nrofacturacion,nroinscripcion,codunidaduso,
                                catetar,porcentaje,principal,consumo,importe,importealc,codciclo)
                                values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codunidaduso,
                                :catetar,:porcentaje,:principal,:consumo,:importe,:importealc,:codciclo)";
                        
                        $consU = $conexion->prepare($insU);
                        $consU->execute(array(  ":codemp"=>1,
                                                ":codsuc"=>$codsuc,
                                                ":nrofacturacion"=>$nrofacturacion,
                                                ":nroinscripcion"=>$rowC["nroinscripcion"],
                                                ":codunidaduso"=>1,
                                                ":catetar"=>$itemsU["catetar"],
                                                ":porcentaje"=>100,
                                                ":principal"=>1,
                                                ":consumo"=>$ConsumoFact,
                                                ":importe"=>str_replace(",","",round($impagua,1)),
                                                ":importealc"=>str_replace(",","",round($impdesague,1)),
                                                ":codciclo"=>$codciclo));
                        if($consU->errorCode()!='00000')
                        {
                            $conexion->rollBack();
                            $mensaje = "Error unidadesusofacturadas ".$rowC["nroinscripcion"].$sql;
                            die($mensaje);
                        }

                            
                    }else
                    {
                        //---cuando tienes mas de 1 unidades de uso------

                        foreach($itemsU as $rowU)
                        {

                            $conscategoria = round($ConsumoFact * ($rowU["porcentaje"] / 100), 2);
							
                            $itemsI = $objFunciones->ImporteTarifa($codsuc, $rowU["catetar"], $tipofacturacionfact, $conscategoria, $nrofacturacion);
                            $impagua    += $itemsI[0];
                                                
                            /*if($calcalc==1)
                            {
                                $impdes = round($itemsI[0]*($porcalc/100),2);
                            }else{*/
                                $impdes = round($itemsI[1],2);
                            //}
                            
                            $impdesague += floatval(str_replace(",","",$impdes));//$impdes;

                            $insU = "insert into facturacion.unidadesusoprefacturadas(codemp,codsuc,nrofacturacion,nroinscripcion,codunidaduso,catetar,porcentaje,
                                     principal,consumo,importe,importealc,codciclo)
                                     values(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codunidaduso,:catetar,:porcentaje,:principal,:consumo,
                                     :importe,:importealc,:codciclo)";
                        
                            $consU = $conexion->prepare($insU);
                            $consU->execute(array(  ":codemp"=>1,
                                                    ":codsuc"=>$codsuc,
                                                    ":nrofacturacion"=>$nrofacturacion,
                                                    ":nroinscripcion"=>$rowC["nroinscripcion"],
                                                    ":codunidaduso"=>1,
                                                    ":catetar"=>$rowU["catetar"],
                                                    ":porcentaje"=>$rowU["porcentaje"],
                                                    ":principal"=>$rowU["principal"],
                                                    ":consumo"=>$conscategoria,
                                                    ":importe"=>str_replace(",","",round($itemsI[0],2)),
                                                    ":importealc"=>str_replace(",","",round($impdes,2)),
                                                    ":codciclo"=>$codciclo));
                            if($consU->errorCode()!='00000')
                            {
                                $conexion->rollBack();
                                $mensaje = "Error unidadesusofacturadas ".$rowC["nroinscripcion"].$sql;
                                die($mensaje);
                            }

                        }
                    }
                
                    /*Valida e inserta el concepto de cargo fijo siempre y cuando sea mayor a cero*/
                    
                    $sqlT = "SELECT impcargofijo1 
                             FROM facturacion.tarifas 
                             WHERE  codemp=1 and codsuc=? and catetar=?";
                    
                    $consultaT = $conexion->prepare($sqlT);
                    $consultaT->execute(array($codsuc,$rowC["catetar"]));
                    $itemsT = $consultaT->fetch();
                    
                        
                    if($itemsT["impcargofijo1"]>0)
                    {
                        $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos where codsuc=? and categoria=5 AND codtipoconcepto=1 AND sefactura=1");
                        $Consulta->execute(array($codsuc));
                        foreach($Consulta->fetchAll() as $row)
                        {   
                            $ItemDetalle++;
                            $ImporteConcepto= str_replace(",","",round($itemsT["impcargofijo1"],2));
                            $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                            $TotalParaIgv=floatval($TotalParaIgv)+floatval($ImporteConcepto);
                                
                        }
                    }

                    /*Valida el Tipo de Servicio*/
                    if($rowC["codtiposervicio"]==1) //Agua y Desague
                    {
                        /*Calculo del Agua*/
                        //INSERTAR SI IMPORTE ES MAYOR CERO
                        if(floatval($impagua)>0)
                        {

                            $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos where codsuc=? and categoria=1 AND codtipoconcepto=1 /*AND codconcepto<>73*/  AND sefactura=1");
                            $Consulta->execute(array($codsuc));
                            foreach($Consulta->fetchAll() as $row)
                            {

                                $ItemDetalle++;
                                $ImporteConcepto= str_replace(",","",round($impagua,2));
                                $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                                $TotalParaIgv=floatval($TotalParaIgv)+floatval($ImporteConcepto);
                                //echo $TotalParaIgv.'<br>';
                            }
                        }
                        /*Calculo del Desague*/
                        /*if($calcalc==1)
                        {
                            $impdesague = round($impagua*($porcalc/100),2);
                        }else{*/
                            $impdesague = round($impdesague,2);
                        //}
                        /*if($rowC["nroinscripcion"]=='699')
                        {
                            
                            echo "<br>Agua-->".round($impagua,2);
                            echo "<br>Desague-->".round($impdesague,2);
                            

                        }
                        */

                        //INSERTAR SI IMPORTE ES MAYOR CERO
                        if(floatval($impdesague)>0)
                        {
                            $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos where codsuc=? and categoria=2 AND codtipoconcepto=1  AND sefactura=1");
                            $Consulta->execute(array($codsuc));
                            foreach($Consulta->fetchAll() as $row)
                            {
                                echo "";
                                $ItemDetalle++;
                                $ImporteConcepto= str_replace(",","",round($impdesague,2));
                                $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                                $TotalParaIgv=floatval($TotalParaIgv)+floatval($ImporteConcepto);

                            }
                        }
                        $impigv = round(($impagua+$impdesague)*($igv/100),2);
                        
                    }
                    
                    if($rowC["codtiposervicio"]==2) //Agua
                    {
                        $impdesague=0;
                        
                        /*Calculo del Agua*/
                        if(floatval($impagua)>0)
                        {
                            $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos where codsuc=? and categoria=1 AND codtipoconcepto=1 /*AND codconcepto<>73*/  AND sefactura=1");
                            $Consulta->execute(array($codsuc));
                            foreach($Consulta->fetchAll() as $row)
                            {

                                $ItemDetalle++;
                                $ImporteConcepto= str_replace(",","",round($impagua,2));
                                $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                                $TotalParaIgv=floatval($TotalParaIgv)+floatval($ImporteConcepto);
                            }
                        }
                        
                        $impigv = round(($impagua)*($igv/100),2);
                    }
                    if($rowC["codtiposervicio"]==3) //Desague
                    {
                        $impagua=0;
                        /*Calculo del Desague*/
                        /*if($calcalc==1)
                        {
                            $impdesague = round($impagua*($porcalc/100),2);
                        }else{*/
                            $impdesague = round($impdesague,2);
                        //}
                        if(floatval($impdesague)>0)
                        {
                            $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos where codsuc=? and categoria=2 AND codtipoconcepto=1 AND sefactura=1");
                            $Consulta->execute(array($codsuc));
                            foreach($Consulta->fetchAll() as $row)
                            {
                                $ItemDetalle++;
                                $ImporteConcepto= str_replace(",","",round($impdesague,2));
                                $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                                $TotalParaIgv=floatval($TotalParaIgv)+floatval($ImporteConcepto);

                            }
                        }
                        $impigv = round(($impdesague)*($igv/100),2);
                    }

                    //VERIFICAR SI EL USUARIO TIENE POZO
                    if($rowC['conpozo']==1)
                    {
                        $conceptopozo=302;
                        $consultacpt = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                            where codsuc=? and codconcepto=".$conceptopozo."");
                        $consultacpt->execute(array($codsuc));
                        $itemcpt = $consultacpt->fetch();
                        $descripcioncpt=$itemcpt['descripcion'];

                        $ItemDetalle++;
                        $ImporteConcepto= str_replace(",","",round($rowC["conpozoimporte"],2));
                        $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$conceptopozo,$ImporteConcepto,1,$codciclo,$ItemDetalle,$descripcioncpt);
                        $TotalParaIgv=floatval($TotalParaIgv)+floatval($ImporteConcepto);

                    }
                    //VERIFICAR SI EL USUARIO TIENE POZO
                    //VERIFICAR SI TIENE VMA
                    if($rowC['vma']==1)
                    {
                        $Sql="SELECT  codemp,  codsuc,  nroinscripcion,  codunidad_uso,  porcentaje,  factor,  
                                        descripcion,  estareg
                               FROM   vma.unidades_uso 
                               WHERE codemp=1 AND  codsuc=? and nroinscripcion=? AND factor>0;";
                        $consultamva = $conexion->prepare($Sql);
                        $consultamva->execute(array($codsuc,$rowC["nroinscripcion"]));
                        $itemsvma = $consultamva->fetchAll();
                        $conceptovma=10007;
                        $consultacpt = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                            where codsuc=? and codconcepto=".$conceptovma."");
                        $consultacpt->execute(array($codsuc));
                        $itemcpt = $consultacpt->fetch();
                        $descripcioncpt=$itemcpt['descripcion'];
                        foreach($itemsvma as $rowvma)
                        {
                            $descripcioncpt=$itemcpt['descripcion'];
                            $importe = $impdesague*$rowvma['porcentaje']/100;
                            $importe = $importe*$rowvma['factor']/100;
                            $descripcioncpt = $descripcioncpt." (".$rowvma['descripcion'].") F=".intval($rowvma['factor'])."%";
                            //importe de desague,multiplar por el procenta de unida de uso
                            //luego multiplicar por el facor
                            $ItemDetalle++;
                            $ImporteConcepto= round($importe,2);
                            $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$conceptovma,$ImporteConcepto,8,$codciclo,$ItemDetalle,$descripcioncpt);
                            $TotalParaIgv=floatval($TotalParaIgv)+floatval($ImporteConcepto);

                        }

                    }
                    //VERIFICAR SI TIENE VMA
                    //$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impdesague,2)));
                    /*Inserta el Concepto de igv siempre y cuando sea mayor a cero(0)*/
                    if($TotalParaIgv>0)
                    {

                        $impigv = round(floatval($TotalParaIgv)*(floatval($Pigv)/100),2);
                        if($impigv>0)
                        {
                            $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos where codsuc=? and categoria=4 AND codtipoconcepto=1 AND sefactura=1");
                            $Consulta->execute(array($codsuc));
                            foreach($Consulta->fetchAll() as $row)
                            {
                                $ItemDetalle++;
                                $ImporteConcepto= str_replace(",","",round($impigv,2));
                                $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                                $TotalParaIgv=floatval($TotalParaIgv)+floatval($ImporteConcepto);

                            }
                        }
                        
                    }
                    //////////VERIFICAR INTERES POR PENSIONES
                    $impdeudaagua       = 0;
                    $impdeudaalc        = 0;
                    $intereses          = 0;
                    $impdeudaigv        = 0;
                    $impdeudacargo      = 0;
                    $impdeudacredito    = 0;
                    $impdeudavma        = 0;
                    $tipoconcepto       = 1;

                    $consulta_deuda = $conexion->prepare("select * from facturacion.f_importedeuda(?,?)");
                    $consulta_deuda->execute(array($rowC["nroinscripcion"],$codsuc));
                    

                    $itd = $consulta_deuda->fetch();
                    //print_r($itd);
                    $agua       = (($itd["agua"]?$itd["agua"]:0) + ($itd["intagua"]?$itd["intagua"]:0))*$porcentaje;
                    $desague    = (($itd["desague"]?$itd["desague"]:0)+($itd["intdesague"]?$itd["intdesague"]:0))*$porcentaje;
                    $igv        = (($itd["importeigv"]?$itd["importeigv"]:0)+($itd["intigv"]?$itd["intigv"]:0))*$porcentaje;
                    $cargo_fijo = (($itd["importecargo"]?$itd["importecargo"]:0)+($itd["intcargo"]?$itd["intcargo"]:0))*$porcentaje;
                    $credito    = (($itd["importecredito"]?$itd["importecredito"]:0)+($itd["interescredito"]?$itd["interescredito"]:0))*$porcentaje;
                    $impdeudavma= (($itd["importevma"]?$itd["importevma"]:0) + ($itd["intvma"]?$itd["intvma"]:0))*$porcentaje;

                    

                    $agua       = floatval(str_replace(",","",round($agua,2)));
                    $desague    = floatval(str_replace(",","",round($desague,2)));
                    $igv        = floatval(str_replace(",","",round($igv,2)));
                    $cargo_fijo = floatval(str_replace(",","",round($cargo_fijo,2)));
                    $impdeudavma= floatval(str_replace(",","",round($impdeudavma,2)));
                    

                    /*insertamos los datos de los intereses en la tabla detfacturacion*/
                    if(floatval($agua)<>0)
                    {
                        $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                        where codsuc=? and categoria=3 and categoria_intereses=1 AND codtipoconcepto=1 AND sefactura=1");
                        $Consulta->execute(array($codsuc));
                        foreach($Consulta->fetchAll() as $row)
                        {
                            $ItemDetalle++;
                            $ImporteConcepto= str_replace(",","",round($agua,2));
                            $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                        }
                    }
                    if(floatval($desague)>0)
                    {
                        $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                        where codsuc=? and categoria=3 and categoria_intereses=2 AND codtipoconcepto=1 AND sefactura=1");
                        $Consulta->execute(array($codsuc));
                        foreach($Consulta->fetchAll() as $row)
                        {
                            $ItemDetalle++;
                            $ImporteConcepto= str_replace(",","",round($desague,2));
                            $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                        }
                    }
                    if(floatval($igv)<>0)
                    {
                        $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                        where codsuc=? and categoria=3 and categoria_intereses=3 AND codtipoconcepto=1 AND sefactura=1");
                        $Consulta->execute(array($codsuc));
                        foreach($Consulta->fetchAll() as $row)
                        {
                            $ItemDetalle++;
                            $ImporteConcepto= str_replace(",","",round($igv,2));
                            $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                        }
                    }
                    if(floatval($cargo_fijo)>0)
                    {
                        $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                        where codsuc=? and categoria=3 and categoria_intereses=4 AND codtipoconcepto=1 AND sefactura=1");
                        $Consulta->execute(array($codsuc));
                        foreach($Consulta->fetchAll() as $row)
                        {
                            $ItemDetalle++;
                            $ImporteConcepto= str_replace(",","",round($cargo_fijo,2));
                            $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                        }
                    }
                    if(floatval($credito)>0)
                    {
                        $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                        where codsuc=? and categoria=3 and categoria_intereses=5 AND codtipoconcepto=1 AND sefactura=1");
                        $Consulta->execute(array($codsuc));
                        foreach($Consulta->fetchAll() as $row)
                        {
                            $ItemDetalle++;
                            $ImporteConcepto= str_replace(",","",round($credito,2));
                            $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                        }
                        $credito    = floatval(str_replace(",","",round($credito,2)));//
                    }
                    else
                        $credito=0; 
                    if(floatval($impdeudavma)>0)
                    {
                        $Consulta = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                        where codsuc=? and categoria=3 and categoria_intereses=6 AND codtipoconcepto=1 AND sefactura=1");
                        $Consulta->execute(array($codsuc));
                        foreach($Consulta->fetchAll() as $row)
                        {
                            $ItemDetalle++;
                            $ImporteConcepto= str_replace(",","",round($impdeudavma,2));
                            $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                        }
                        $impdeudavma    = floatval(str_replace(",","",round($impdeudavma,2)));//
                    }
                    else
                        $impdeudavma=0; 



                }   
                
            //--Verifica si tiene cargos el usuario que esta facturando--
            $impcargos = 0;
            $sqlcreditos  = "SELECT det.nrocuota,det.totalcuotas,cab.codconcepto,
                            det.imptotal,det.nrocredito,det.subtotal,det.igv,det.redondeo,det.interes,
                            det.nrofacturacion,cab.nrocolaterales
                            from facturacion.detvarios as det 
                            inner join facturacion.cabvarios as cab on(det.codemp=cab.codemp and det.codsuc=cab.codsuc and 
                            det.nrocredito=cab.nrocredito) 
                            where det.codemp=1 and det.codsuc=:codsuc 
                            and cab.nroinscripcion=:nroinscripcion  
                            and cab.estareg=1 and estadocuota=0
                            ORDER BY det.nrocredito, nrocuota  ";
            
            $consulta_creditos = $conexion->prepare($sqlcreditos);
            $consulta_creditos->execute(array(":codsuc"=>$codsuc,":nroinscripcion"=>$rowC["nroinscripcion"]));
            $SubtotalCreditos=0;
            $SubTotalIgvCreditos=0;
            $SubTotalRed = 0;
            $temp=0;
            foreach($consulta_creditos->fetchAll() as $row_creditos)
            {           
                $codtipodeuda=9;
                
                //$SubtotalCreditos += $row_creditos['subtotal'];
                //$SubTotalRed += $row_creditos['redondeo'];
                /*Inserta el credito en la facturacion SUB TOTAL*/
                $dCuota = "";
                $codconceptoCre= $row_creditos['codconcepto'];
                $consultacpt = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                where codsuc=? and codconcepto=".$codconceptoCre."");
                $consultacpt->execute(array($codsuc));
                $itemcpt = $consultacpt->fetch();
                $desconceptoCre=$itemcpt['descripcion'];

                    
                if(floatval($row_creditos['subtotal'])<>0)
                {
                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($row_creditos['subtotal'],2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$codconceptoCre,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre.$dCuota,$row_creditos['nrocredito'],$row_creditos['nrocuota']);
                    $impcargos += $ImporteConcepto;
                }

                /*Inserta el credito en la facturacion IGV*/
                if(floatval($row_creditos['igv'])<>0)
                {
                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($row_creditos['igv'],2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],5,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre,$row_creditos['nrocredito'],$row_creditos['nrocuota']);
                    $impcargos += $ImporteConcepto;
                }
                /*Inserta el credito en la facturacion INTERES*/
                if(floatval($row_creditos['interes'])<>0)
                {
                    $consultacpt = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                    where codsuc=? and codconcepto=9");
                    $consultacpt->execute(array($codsuc));
                    $itemcpt = $consultacpt->fetch();
                    $desconceptoCre=$itemcpt['descripcion'];

                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($row_creditos['interes'],2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],9,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre,$row_creditos['nrocredito'],$row_creditos['nrocuota']);
                    $impcargos += $ImporteConcepto;

                }
                /*Inserta el credito en la facturacion REDONDEO*/
                if(floatval($row_creditos['redondeo'])<>0)
                {
                    $codredondeo=8;
                    if(floatval($row_creditos['redondeo'])<0)
                        $codredondeo=7;

                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($row_creditos['redondeo'],2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$codredondeo,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre,$row_creditos['nrocredito'],$row_creditos['nrocuota']);
                    $impcargos += $ImporteConcepto;
                }
                
                    
                
            }

            //--Verifica si tiene credito el usuario que esta facturando--
            $impcreditos = 0;
            $Sql  = "SELECT det.nrocuota,det.totalcuotas,cab.codconcepto,
                            det.imptotal,det.nrocredito,det.subtotal,det.igv,det.redondeo,det.interes,
                            det.nrofacturacion,cab.nrocolaterales
                            from facturacion.detcreditos as det 
                            inner join facturacion.cabcreditos as cab on(det.codemp=cab.codemp and det.codsuc=cab.codsuc and 
                            det.nrocredito=cab.nrocredito) 
                            where det.codemp=1 and det.codsuc=:codsuc 
                            and cab.nroinscripcion=:nroinscripcion  
                            and cab.estareg=1 and estadocuota=0
                            AND det.nrofacturacion<>0 
                            AND fechavencimiento<=:fechavencimiento
                            ORDER BY det.nrocredito, nrocuota  ";
            $consulta_creditos = $conexion->prepare($Sql);
            $consulta_creditos->execute(array(":codsuc"=>$codsuc,":nroinscripcion"=>$rowC["nroinscripcion"],":fechavencimiento"=>$fechafacturacion));
            $SubtotalCreditos=0;
            $SubTotalIgvCreditos=0;
            $SubTotalRed = 0;
            $temp=0;
            foreach( $consulta_creditos->fetchAll() as $row_creditos)
            {           

                $codtipodeuda=4;
                
                $SubtotalCreditos += $row_creditos['subtotal'];
                $SubTotalRed += $row_creditos['redondeo'];
                /*Inserta el credito en la facturacion SUB TOTAL*/
        
                $dCuota = " Cuota(".$row_creditos['nrocuota']."/".$row_creditos['totalcuotas'].")";
                $codconceptoCre='992';
                $desconceptoCre=utf8_decode('CONVENIO N°'.$row_creditos['nrocredito']);
                if($row_creditos['codconcepto']==101)
                {
                    $codconceptoCre= $row_creditos['codconcepto'];
                    $consultacpt = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                    where codsuc=? and codconcepto=".$codconceptoCre."");
                    $consultacpt->execute(array($codsuc));
                    $itemcpt = $consultacpt->fetch();
                    $desconceptoCre=$itemcpt['descripcion'];
                    $codtipodeuda=6;
                }
                    
                if(floatval($row_creditos['subtotal'])<>0)
                {
                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($row_creditos['subtotal'],2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$codconceptoCre,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre.$dCuota,$row_creditos['nrocredito'],$row_creditos['nrocuota']);
                    $impcreditos += $ItemDetalle;
                }

                /*Inserta el credito en la facturacion IGV*/
                if(floatval($row_creditos['igv'])<>0)
                {
                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($row_creditos['igv'],2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],5,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre,$row_creditos['nrocredito'],$row_creditos['nrocuota']);
                    $impcreditos += $ItemDetalle;
                }
                /*Inserta el credito en la facturacion INTERES*/
                if(floatval($row_creditos['interes'])<>0)
                {
                    $consultacpt = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                    where codsuc=? and codconcepto=9");
                    $consultacpt->execute(array($codsuc));
                    $itemcpt = $consultacpt->fetch();
                    $desconceptoCre=$itemcpt['descripcion'];

                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($row_creditos['interes'],2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],9,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre,$row_creditos['nrocredito'],$row_creditos['nrocuota']);
                    $impcreditos += $ItemDetalle;

                }
                /*Inserta el credito en la facturacion REDONDEO*/
                if(floatval($row_creditos['redondeo'])<>0)
                {
                    $codredondeo=8;
                    if(floatval($row_creditos['redondeo'])<0)
                        $codredondeo=7;

                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($row_creditos['redondeo'],2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$codredondeo,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre,$row_creditos['nrocredito'],$row_creditos['nrocuota']);
                    $impcreditos += $ItemDetalle;
                }
                
                   
                
            }
            
            //--Verificamos si el Usuario tiene Refinanciamiento y lo Agregamos en la Facturacion--
            $imprefinanciamiento=0;
            
            $Sql="select SUM(d.importe) as importe,d.nrorefinanciamiento,d.nrocuota,d.totalcuotas
                                  from facturacion.detrefinanciamiento as d 
                                  inner join facturacion.cabrefinanciamiento as c on(d.nrorefinanciamiento=c.nrorefinanciamiento and d.codemp=c.codemp and d.codsuc=c.codsuc)
                                  inner join facturacion.conceptos as co on(d.codconcepto=co.codconcepto and d.codemp=co.codemp and d.codsuc=co.codsuc)
                                  where c.nroinscripcion=?  and c.codsuc=? and d.estadocuota=0  AND fechavencimiento<=?
                                  group by d.nrorefinanciamiento,d.nrocuota,d.totalcuotas
                                  order by d.nrorefinanciamiento,d.nrocuota";
                                  
            $consulta_refinanciamiento = $conexion->prepare($Sql);
            $consulta_refinanciamiento->execute(array($rowC["nroinscripcion"],$codsuc,$fechafacturacion));
            

            $i=0;
            $impref=0;
            foreach($consulta_refinanciamiento->fetchAll() as $row_refinanciamiento)
            {

                /*Inserta los conceptos en la facturacion*/
                $codtipodeuda=3;
                $codconceptoRef='';
                $impref+=$row_refinanciamiento['importe'];
                
                $dCuota = " Cuota(".$row_refinanciamiento['nrocuota']."/".$row_refinanciamiento['totalcuotas'].")";
                $codconceptoRef='992';
                $desconceptoCre='CONVENIO EMPRESA';

                $ItemDetalle++;
                $ImporteConcepto= str_replace(",","",round($row_refinanciamiento['importe'],2));
                $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$codconceptoRef,$ImporteConcepto,$codtipodeuda,$codciclo,$ItemDetalle,$desconceptoCre.$dCuota,0,$row_refinanciamiento['nrocuota'],$row_refinanciamiento['nrorefinanciamiento']);
                    



                            
              
                
            }
            
            //die("==>".$i);
            //--Verifica si el usuario tiene deuda anterior para calcular el credito--
            //VERIFICAR SI EL USUARIO TIENE PAGOS DOBLES
            $totfacturacion = round($TotalParaIgv,2)+
                              round($impigv,2)+round($impcreditos,2)+round($agua,2)+round($desague,2)+round($igv,2)+
                              round($cargo_fijo,2)+round($credito,2)+round($impref,2)+round($impcargos,2) + round($impdeudavma,2);      
            
            $tot1   = round($totfacturacion,2);
            $tot2   = round($totfacturacion,1);

            $conceptodoble=10005;
            $totalpagosdobles=0;
            $Sql="SELECT SUM(importe) 
                    FROM cobranza.pagosvarios 
                    WHERE codemp=1 and codsuc=? and nroinscripcion=? AND nrofacturacion=? AND codconcepto=?";
            $result = $conexion->prepare($Sql);
            $result->execute(array($codsuc,$rowC["nroinscripcion"],$nrofacturacion,$conceptodoble));
            $rowpgdb = $result->fetch();        
            if($rowpgdb[0]!='')
            {
                $conceptodoble=10012;
                //SI EL IMPORTE ADELANTADO ES MENOR AL FACTURADO
                $consultacpt = $conexion->prepare("select codconcepto,descripcion from facturacion.conceptos 
                                                    where codsuc=? and codconcepto=".$conceptodoble."");
                $consultacpt->execute(array($codsuc));
                $itemcpt = $consultacpt->fetch();
                $descripcioncpt=$itemcpt['descripcion'];
                $ItemDetalle++;
                $saldo=floatval($tot1)-floatval(round($rowpgdb[0],2));
                if(floatval($saldo)>=0)
                {
                    
                    $ImporteConcepto= round($rowpgdb[0],2)*-1;
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$conceptodoble,$ImporteConcepto,1,$codciclo,$ItemDetalle,$descripcioncpt);
                    $totalpagosdobles=round($rowpgdb[0],2);
                }
                else
                {

                    $ImporteConcepto= round($tot2,2)*-1;
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$conceptodoble,$ImporteConcepto,1,$codciclo,$ItemDetalle,$descripcioncpt);
                    $totalpagosdobles=round($tot2,2);
                    
                }
            }
            //VERIFICAR SI EL USUARIO TIENE PAGOS DOBLEA
            
            //REINICIAR VARIABLES   
            
            
            /*suma los totales del usuario facturado*/
            
            /*$totfacturacion = round($itemsT["impcargofijo1"],2)+round($impagua,2)+round($impdesague,2)+
                              round($impigv,2)+round($impcreditos,2)+round($agua,2)+round($desague,2)+round($igv,2)+
                              round($cargo_fijo,2)+round($credito,2);*/
            


                    
            
                    
            $totfacturacion = round($TotalParaIgv,2)+
                              round($impigv,2)+round($impcreditos,2)+round($agua,2)+round($desague,2)+round($igv,2)+
                              round($cargo_fijo,2)+round($credito,2)+round($impref,2)+round($impcargos,2)+ round($impdeudavma,2);       


                /*  echo "impcargofijo1-->".round($itemsT["impcargofijo1"],2);
                        echo "<br>impagua-->".round($impagua,2);
                        echo "<br>impdesague-->".round($impdesague,2);
                        echo "<br>TOTAL-->".$TotalParaIgv;
                        echo "<br>IGV-->".$impigv;
                        echo "<br>IGV-->".$Pigv;
                        echo "<br>impcreditos-->".round($impcreditos,2);
                        echo "<br>agua-->".round($agua,2);
                        echo "<br>desague-->".round($desague,2);
                        echo "<br>igv-->".round($igv,2);
                        echo "<br>cargo_fijo-->".round($cargo_fijo,2);
                        echo "<br>credito-->".round($credito,2);
                        echo "<br>impref-->".round($impref,2);
                        echo "<br>impcargos-->".round($impcargos,2);
                        echo "<br>impdeudavma-->".round($impdeudavma,2);
                        echo "<br>totfacturacion-->".round($totfacturacion,2);
                        die("Fin");
            */
            $tot1   = round($totfacturacion,2);
            $tot2   = round($totfacturacion,1);
            
            $redctual = round($tot2-$tot1,2);
            //die('==>total='.$totfacturacion.",redctual=".$redctual);

            /*if($rowC["nroinscripcion"]=='6535')
                {
                    echo '--'.$redctual.'--';
                    die($totfacturacion.' = '.round($TotalParaIgv,2).'+'.round($impigv,2).'+'.round($impcreditos,2).'+'.round($agua,2).'+'.round($desague,2).'+'.round($igv,2).'+'.round($cargo_fijo,2).'+'.round($credito,2) );

                }*/

            /*Inserta el redondeo actual si es mayor q cero*/
            if(floatval($redctual)<>0)
            {
                $redpositivo=1;
                if(floatval($redctual)<0) $redpositivo=2;
            
                $Sqlr="SELECT codconcepto,descripcion from facturacion.conceptos 
                        where codsuc=? and categoria=7 and codtipoconcepto=1 
                        AND sefactura=1 AND categoria_redondeo=".$redpositivo;
                $Consulta = $conexion->prepare($Sqlr);
                $Consulta->execute(array($codsuc));
                foreach($Consulta->fetchAll() as $row)
                {
                    $ItemDetalle++;
                    $ImporteConcepto= str_replace(",","",round($redctual,2));
                    $objFunciones->InsertDetPreFac($codemp,$codsuc,$nrofacturacion,$rowC["nroinscripcion"],$row["codconcepto"],$ImporteConcepto,1,$codciclo,$ItemDetalle,$row["descripcion"]);
                    

                }

            }
            
    

            $TotalParaIgv=0;
            $impigv=0;
            $impcreditos=0;
            $agua=0;
            $desague=0;
            $igv=0;
            $cargo_fijo=0;
            $credito=0;
            $impcargos=0;
            ////////////
            $actualAvance++;
            $porcentajeAvance = round($actualAvance*100/$totalAvance);
            $textoAvance=$porcentajeAvance."|".$actualAvance." de ".$totalAvance;
            file_put_contents('../progreso.txt',$textoAvance);
                
        }
        $textoAvance="100|".$totalAvance." de ".$totalAvance;
        file_put_contents('../progreso.txt',$textoAvance);
        

        
        
        
        
        
        if(!$consCab)
        {
            $conexion->rollBack();
            
            $img        = "<img src='".$urldir."images/error.png' width='31' height='31' />";
            $mensaje    = "Error al Realizar la Pre Facturacion";
            $res        = 0;
        }else{
            $conexion->commit();
            
            $img         = "<img src='".$urldir."images/Ok.png' width='31' height='31' />";
            $mensaje     = "La Pre Facturacion se ha Realizado Correctamente";
            $mensaje    .= "<br><input class='button' type='button' name='cerrar_blokeo' id='cerrar_blokeo' value='Aceptar' onclick='desbloquear_pantalla();' />";
            $res         = 1;
        }
        
        echo $img."|".$mensaje."|".$res;
    
?>