// JavaScript Document
// JavaScript Document
$(document).ready(function () {

    $("#dialogo_barras").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: false,
        title: "Realizando el Proceso de Pre Facturacion. Espere por Favor",
        height: 130,
        open: function (event, ui) {
            //ocultar el boton de cerrar en el dialogo.
            $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
        }
    });
    //preparación de la barra de progreso
    $("#barra").progressbar({
        value: 0
    });


})
function datos_facturacion(codciclo)
{
    $.ajax({
        url: '../../../../ajax/periodo_facturacion.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + codciclo + '&codsuc=' + codsuc,
        success: function (datos) {
            var r = datos.split("|")

            $("#anio").val(r[1])
            $("#mes").val(r[2])
            $("#mes_num").val(r[3])
            $("#nrofacturacion").val(r[0])
            var int = parseFloat(r[7])
            $("#interes").val(int.toFixed(2))
            $("#control_cierre_cobranza").val(r[4])
            $("#control_cierre_lecturas").val(r[5])
            $("#control_porcentaje_mes").val(r[6])
        }
    });
}

function cargar_prefacturacion_form()
{
    if ($("#ciclo").val() == 0)
    {
        alert("Seleccione el Ciclo")
        return false
    }
    window.parent.cargar_prefacturacion($("#nrofacturacion").val())
}


function generar_facturacion()
{
    if ($("#ciclo").val() == 0)
    {
        alert("Seleccione el Ciclo");
		
        return false;
    }
    if ($("#interes").val() == 0 || $("#interes").val() == "0.00" || $("#interes").val() == "")
    {
        Msj($("#interes"), "El Monto del Interes no es Valido");
		
        return false;
    }

    var r = confirm('Desea Realizar el Proceso de Pre-Facturacion');
	
    if (r == false)
    {
        return true;
    }

    //window.parent.blokear_pantalla("Se Esta Realizando el Proceso de Pre-facturacion. Espere por Favor!!")
    $("#dialogo_barras").dialog('open');

    $.ajax({
        url: 'calculo/facturacion.php',
        type: 'POST',
        async: true,
        data: 'codciclo=' + $("#ciclo").val() + '&codsuc=' + codsuc + '&porcentaje=' + $("#interes").val() +
                '&nrofacturacion=' + $("#nrofacturacion").val() + '&factura_alcantarillado=' + $("#factura_alcantarillado").val() +
                '&mes=' + $("#mes_num").val() + '&anio=' + $("#anio").val(),
        success: function (datos) {
            var r = datos.split("|");
			
            $("#dialogo_barras").dialog('close');
			
            window.parent.blokear_pantalla("");
            window.parent.establecer_texto(r[0], r[1]);
			
            $("#barra").progressbar({value: 0}); //actualizar la barra.
            $("#titulo_barra").html('Calculando...'); //actualizar etiqueta. 
        }
    });
    timerBarra = setInterval(actualizarBarra, 6000);
}
var porcentajeA = 0;

function actualizarBarra()
{
	//leer el archivo de texto que contiene el procentaje y avance del proceso principal.
	$.ajax({
        url: 'progreso.txt',
        type: 'POST',
        async: true,
        data: '',
        success: function (texto) {
            var partes        = texto.split('|'), 
            porcentaje    = parseInt(partes[0]),
            avance        = partes[1],
            etiqueta      = partes[0] + "% (" + partes[1] + ")";
 		
			if (porcentaje == 100) //llegamos al 100% fin de proceso.
			{
				clearInterval(timerBarra); //borrar timer.
			}
	 
			$("#barra").progressbar({value: porcentaje}); //actualizar la barra.
			$("#titulo_barra").html(etiqueta); //actualizar etiqueta.
        }
    });
}
function cargar_prefacturacion(nrofacturacion)
{
    cargar_datos_prefacturacion_from(nrofacturacion);
    $("#Divdialog-form-prefacturacion").dialog("open");
}

function cargar_datos_prefacturacion_from(nrofact)
{
    //alert(nrofact)
    $.ajax({
        url: 'ajax/datos_prefacturacion.php',
        type: 'POST',
        async: true,
        data: 'nrofacturacion=' + nrofact,
        success: function (datos) {
            $("#div_prefacturacion").html(datos)
        }
    })

    /*$.ajax({
     url:'<?=$urldir?>sigco/facturacion/operaciones/prefacturacion/ajax/datos_prefacturacion.php',
     type:'POST',
     async:true,
     //data:'nrofacturacion='+nrofact,
     data:'',
     success:function(datos){           
     $("#div_prefacturacion").html(datos)
     }
     }) */
}