<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../../objetos/clsDrop.php");
	
    $codsuc = $_SESSION['IdSucursal'];
    $nrofacturacion = $_POST["nrofacturacion"];
	
    $objDrop = new clsDrop();
?>
<script>
    $(document).ready(function () {
        $('#div_monto_facturacion #ListaMenu').fixedHeaderTable({height: '420', footer: true});
        var theTable = $('#ListaMenu')
        $("#Valor").keyup(function () {
            $.uiTableFilter(theTable, this.value)
        })
    });

    function cargar_rutas_distribucion(codsector)
    {
        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>ajax/rutas_distribucion_drop.php',
            type: 'POST',
            async: true,
            data: 'codsector=' + codsector + '&codsuc=<?=$codsuc?>',
            success: function (datos) {
                $("#div_rutasdistribucion").html(datos)
            }
        })
    }

    function cargar_montos_facturacion()
    {
        if ($("#ciclo").val() == 0)
        {
            alert("Seleccione el Ciclo")
            return
        }
        if ($("#sector").val() == 0)
        {
            alert("Seleccione el Sector")
            return
        }
        if ($("#rutasdistribucion").val() == 0)
        {
            alert("Seleccione la Ruta de Distribucion")
            return
        }

        $("#ListaMenu tbody").html("<span class='icono-icon-loading'>&nbsp;&nbsp;Consultando</span>")

        $.ajax({
            url: '<?php echo $_SESSION['urldir'];?>sigco/facturacion/operaciones/prefacturacion/ajax/montos_prefacturacion.php',
            type: 'POST',
            async: true,
            data: 'codciclo=' + $("#ciclo").val() + '&codsector=' + $("#sector").val() + '&codruta=' + $("#rutasdistribucion").val() +
                    '&codsuc=<?=$codsuc ?>&nrofacturacion=<?=$nrofacturacion ?>',
            success: function (datos) {
                $("#ListaMenu tbody").html(datos)
                $('#div_monto_facturacion #ListaMenu').fixedHeaderTable({height: '420', footer: true});
                $(document).on('mouseover', "#ListaMenu tbody tr", function (event) {
                    $(this).addClass('ui-state-active');
                });
                $(document).on('mouseout', "#ListaMenu tbody tr", function (event) {
                    $(this).removeClass('ui-state-active');
                });

            }
        })
    }
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="80">Ciclo</td>
        <td width="30" align="center">:</td>
        <td><?php $objDrop->drop_ciclos($codsuc); ?></td>
        <td>
            <button onclick="cargar_montos_facturacion();" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                <span class="ui-button-text">Mostrar Datos de la Prefacturacion</span>
            </button>

            <button onclick="javascript:$('#Divdialog-form-prefacturacion').dialog('close');" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                <span class="ui-button-text">Cerrar</span>
            </button>
        </td>
    </tr>
    <tr>
        <td>Sector</td>
        <td align="center">:</td>
        <td><?php echo $objDrop->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_distribucion(this.value);'"); ?></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Ruta</td>
        <td align="center">:</td>
        <td>
            <div id="div_rutasdistribucion">
                <?php $objDrop->drop_rutas_distribucion($codsuc, 0); ?>
            </div>        
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Buscar</td>
        <td align="center">:</td>
        <td>
            <input id="Valor" class="buscar" value="" maxlength="30" size="33" type="text" />
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" height="5px"></td>
    </tr>
    <tr>
        <td colspan="4" align="center">
            <div style="overflow:auto; height:450px" id="div_monto_facturacion">
                <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="ListaMenu" rules="all" >
                    <thead>
                        <tr align="center"  class="ui-widget-header">
                            <td width="10%">Codigo</td>
                            <td width="20%">Propietario</td>
                            <td width="6%">Est. Ser.</td>
                            <td width="4%">Categoria</td>
                            <td width="5%">Consumo</td>
                            <td width="6%">Agua</td>
                            <td width="6%">Desague</td>
                            <td width="6%">Cargo Fijo</td>
                            <td width="6%">Otros Con.</td>
                            <td width="6%">Int. Y Mora</td>                            
                            <td width="6%">Redondeo</td>
                            <!-- <td width="7%" style="display:none">Deuda</td> -->
                            <td width="8%">Total</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="4" height="5px"></td>
    </tr>
</table>
