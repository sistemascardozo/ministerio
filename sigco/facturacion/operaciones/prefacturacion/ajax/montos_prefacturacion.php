<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();

    $codciclo = $_POST["codciclo"];
    $sector = $_POST["codsector"];
    $ruta = $_POST["codruta"];
    $codsuc = $_SESSION['IdSucursal'];
    $nrofacturacion = $_POST["nrofacturacion"];

    $tr = "";

    $sqlC = "SELECT c.nroinscripcion,".$objFunciones->getCodCatastral("cl.").",cl.propietario,
        c.consumofact,c.impdeuda,tar.nomtar, es.descripcion, cl.codestadoservicio
    FROM facturacion.cabprefacturacion as c
    INNER JOIN catastro.clientes as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.nroinscripcion=cl.nroinscripcion)
    INNER JOIN public.estadoservicio AS es ON es.codestadoservicio = cl.codestadoservicio
    INNER JOIN facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and c.catetar=tar.catetar)
    WHERE c.codciclo=:codciclo and cl.codsector=:codsector and cl.codrutdistribucion=:codrutdistribucion and c.codsuc=:codsuc and nrofacturacion=:nrofacturacion order by codcatastro asc";

    $consultaC = $conexion->prepare($sqlC);
    $consultaC->execute(array(":codciclo" => $codciclo, ":codsector" => $sector, ":codrutdistribucion" => $ruta,
        ":codsuc" => $codsuc, ":nrofacturacion" => $nrofacturacion));
    /*print_r(array(":codciclo" => $codciclo, ":codsector" => $sector, ":codrutdistribucion" => $ruta,
        ":codsuc" => $codsuc, ":nrofacturacion" => $nrofacturacion));*/
    $itemsC = $consultaC->fetchAll();
            
    foreach ($itemsC as $rowC) {
        $mesagua = 0;
        $mesdesague = 0;
        $mescargo = 0;
        $mesotros = 0;
        $mesinteres = 0;
        $mesigv = 0;
        $mesred = 0;
        $deuda = 0;
        $total = 0;

        $sqlD = "SELECT sum(d.importe-(d.importeacta+d.importerebajado)) as importe,c.categoria
            FROM facturacion.detprefacturacion as d
            INNER JOIN facturacion.conceptos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto)
            WHERE d.codciclo=? and d.nroinscripcion=? and d.codsuc=? and d.nrofacturacion=? and d.categoria=0 and estadofacturacion=1
            GROUP BY c.categoria";
        //echo $sqlD."<br>";
        $consultaD = $conexion->prepare($sqlD);
        $consultaD->execute(array($codciclo, $rowC["nroinscripcion"], $codsuc, $nrofacturacion));
        $itemsD = $consultaD->fetchAll();    
        
        $des= substr($rowC['descripcion'], 0, 3);
            
        foreach ($itemsD as $rowD) {
            if ($rowD["categoria"] == 1) {
                $mesagua += $rowD["importe"];
            }
            if ($rowD["categoria"] == 2) {
                $mesdesague += $rowD["importe"];
            }
            if ($rowD["categoria"] == 3) {
                $mesinteres += $rowD["importe"];
            }
            if ($rowD["categoria"] == 4) {
                $mesigv += $rowD["importe"];
            }
            if ($rowD["categoria"] == 5) {
                $mescargo += $rowD["importe"];
            }
            if ($rowD["categoria"] == 6) {
                $mesotros += $rowD["importe"];
            }
            if ($rowD["categoria"] == 7) {
                $mesred += $rowD["importe"];
            }
        }

        $deuda = $rowC["impdeuda"];
        $total = $mesagua + $mesdesague + $mescargo + $mesotros + $mesinteres + $mesigv + $mesred + $deuda;

        $tr .= "<tr style='font-size:11px !important'>";
        $tr .= "<td width='10%' align='center'>".$rowC["codcatastro"]."</td>";
        $tr .= "<td width='20%'>".$rowC["propietario"]."</td>";
        switch ($rowC['codestadoservicio']) {
            case 1:
                $tr .= "<td width='6%' align='center' ><p style='color:green;font-weight: bold;'>".$des."&nbsp;</p></td>"; break;
            case 2:
                $tr .= "<td width='6%' align='center' ><p style='color:red;font-weight: bold;'>".$des."&nbsp;</p></td>"; break;

        }
        
        $tr .= "<td width='4%' align='center'>".substr($rowC["nomtar"], 0, 3)."</td>";
        $tr .= "<td width='5%' align='right'>".number_format($rowC["consumofact"],0)."&nbsp;</td>";
        $tr .= "<td width='6%' align='right' >".number_format($mesagua, 2)."&nbsp;</td>";
        $tr .= "<td width='6%' align='right' >".number_format($mesdesague, 2)."&nbsp;</td>";
        $tr .= "<td width='6%' align='right' >".number_format($mescargo, 2)."&nbsp;</td>";
        $tr .= "<td width='6%' align='right' >".number_format($mesotros, 2)."&nbsp;</td>";
        $tr .= "<td width='6%' align='right' >".number_format($mesinteres, 2)."&nbsp;</td>";
        
        $tr .= "<td width='6%' align='right' >".number_format($mesred, 2)."&nbsp;</td>";
        //$tr .= "<td width='7%' align='right' style='display:none'>".number_format($deuda,2)."</td>";
        $tr .= "<td width='8%' align='right' >".number_format($total, 2)."&nbsp;</td>";
        $tr .= "</tr>";
    }

    echo $tr;
?>
