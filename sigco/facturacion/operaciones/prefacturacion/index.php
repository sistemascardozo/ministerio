<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	$TituloVentana = "GENERAR PRE-FACTURACION";
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);
	
	$codsuc = $_SESSION['IdSucursal'];
	$objMantenimiento = new clsDrop();
	$codsuc = $_SESSION['IdSucursal'];
	$sucursal = $objMantenimiento->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);
	
	$empresa = $objMantenimiento->datos_empresa($codsuc);
	$fechaserver = $objMantenimiento->FechaServer();
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_facturacion.js" language="JavaScript"></script>
<script>
    var codsuc = <?=$codsuc ?>;
    $(document).ready(function () {
        $(".button").button();

        $("#Divdialog-form-prefacturacion").dialog({
            autoOpen: false,
            height: 580,
            width: 1300,
            modal: true,
            resizable: false,
            /*buttons: {
             "Mostrar Datos de la Prefacturacion": function() {
             cargar_montos_facturacion();
             },
             "Cerrar": function() { 
             $("#Divdialog-form-prefacturacion").dialog( "close" );
             }
             },*/
            close: function () {

            }
        });


    });

    function ValidarForm(Op)
    {
        GuardarP(Op);
    }

    function Cancelar()
    {
        location.href = 'index.php';
    }

</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
	<table width="780" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
				<td colspan="5" class="TitDetalle" style="padding:4px">
					<fieldset>
						<legend  class="ui-state-default ui-corner-all">Datos de la Empresa</legend>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="63%" rowspan="9" valign="top">
                                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr valign="top">
										  <td width="10">&nbsp;</td>
                                            <td width="70">Empresa</td>
                                            <td width="30" align="center">:</td>
                                            <td colspan="4">
                                            	<textarea name="empresa" id="empresa" cols="50" rows="5" readonly="readonly" class="inputtext"><?=$empresa["razonsocial"] ?></textarea>
											</td>
                                        </tr>
                                        <tr>
                                          <td>&nbsp;</td>
                                            <td>Ciclo</td>
                                            <td align="center">:</td>
                                            <td colspan="4">
												<?php $objMantenimiento->drop_ciclos($codsuc, 0, "onchange='datos_facturacion(this.value);'"); ?>
											</td>
                                        </tr>
                                        <tr>
                                          <td>&nbsp;</td>
                                            <td>A&ntilde;o</td>
                                            <td align="center">:</td>
                                            <td width=""><input type="text" name="anio" id="anio" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:50px;" /></td>
                                            <td width="" align="right">Mes</td>
                                            <td width="30" align="center">:</td>
                                            <td>
                                            	<input type="text" name="mes" id="mes" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:150px;"  />
												<input type="hidden" name="mes_num" id="mes_num" value="0" />
											</td>
                                        </tr>
                                        <tr>
                                          <td>&nbsp;</td>
                                            <td>Facturacion</td>
                                            <td align="center">:</td>
                                            <td><input type="text" name="nrofacturacion" id="nrofacturacion" readonly="readonly" class="inputtext" maxlength="10" value="" style="width:50px" /></td>
                                            <td align="right">Fecha</td>
                                            <td align="center">:</td>
                                            <td><input type="text" name="fechafacturacion" id="fechafacturacion" readonly="readonly" class="inputtext" maxlength="10" value="<?=$fechaserver?>" style="width:80px;" /></td>
                                        </tr>
                                        <tr style="padding:4px">
											<td colspan="7" align="left"><label></label></td>
										</tr>
									</table>
								</td>
                            </tr>
						</table>
					</fieldset>
				</td>
            </tr>
            <tr>
                <td class="TitDetalle">&nbsp;</td>
                <td colspan="4" class="CampoDetalle">
                	<input type="hidden" name="control_cierre_cobranza" id="control_cierre_cobranza" value="0" />
                    <input type="hidden" name="control_cierre_lecturas" id="control_cierre_lecturas" value="0" />
                    <input type="hidden" name="control_porcentaje_mes" id="control_porcentaje_mes" value="0" />
                    <input type="hidden" name="factura_alcantarillado" id="factura_alcantarillado" value="<?=$empresa["facturaalcantarillado"] ?>" />
                    <input type="hidden" name="control_facturacion" id="control_facturacion" value="0" />
				</td>
            </tr>
            <tr style="padding:4px">
				<td colspan="5" class="TitDetalle">
					<table width="100%" border="0" style="border:1px #000000 dashed" cellspacing="0" cellpadding="0">
						<tr style="padding:4px">
							<td colspan="2" align="left">
                                Ingrese el Porcentaje de Interes:&nbsp;&nbsp;
                                <input type="text" name="interes" id="interes" value="0.00" />
                            </td>
                            <td align="center">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="center">&nbsp;</td>
                        </tr>
                        <tr style="padding:4px">
							<td width="36%" align="center">
								<input type="button" name="calculofacturacion" id="calculofacturacion" class="button" value="Generar Calculo Pre-Facturacion" style="width:240px;text-align:left" onclick="generar_facturacion();"  />
                            </td>
                            <td width="22%" align="center">
								<input type="button" name="verprefacturacion" id="verprefacturacion" class="button" value="Ver Calculo de Prefacturacion" style="width:240px;text-align:left" onclick="cargar_prefacturacion_form();"  />
                            </td>
                            <td width="22%" align="center">&nbsp;</td>
                            <td width="16%">&nbsp;</td>
                            <td width="4%" align="center">&nbsp;</td>
						</tr>
						<tr><td width="4%" align="center">&nbsp;</td></tr>
					</table>
				</td>
            </tr>
            <tr>
                <td width="107" class="TitDetalle">&nbsp;</td>
                <td width="573" colspan="4" class="CampoDetalle">&nbsp;</td>
			</tr>
		</tbody>
	</table>
</form>
    <div id="Divdialog-form-prefacturacion" title="Ver el Calculo de Pre-Facturacion"  >
        <div id="div_prefacturacion"></div>
    </div>

    <div id="dialogo_barras">
        <span id="titulo_barra">0 %</span>
        <div id="barra"> </div>
    </div>
</div>

<?php CuerpoInferior(); ?>