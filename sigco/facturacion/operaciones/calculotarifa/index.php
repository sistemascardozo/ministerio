<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	CuerpoSuperior($TituloVentana);
	
	$Op = $_POST["Op"];
	$Id = isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar = "op=".$Op;
	$codsuc = $_SESSION['IdSucursal'];
	
	$objMantenimiento = new clsMantenimiento();
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
$(function(){
	$("#DivTarifasAcordion").accordion({
		heightStyle: "content"
	});
})
  
	var Agua = 0;
	var Desague = 0;
	var Redondeo = 0;
	
	function ImporteTarifa(CodTarifa)
	{
		$.ajax({
			url:'include/tarifa.php',
			type:'POST',
			async:true,
			data:'Categoria=' + CodTarifa + '&Volumen=' + $("#Volumen").val(),
			success:function(datos){
				var r = datos.split("|");
				
				Agua  = r[0].replace(",", "");
				Desague = r[1].replace(",", "");
				CargoFijo = $('#CargoFijo' + CodTarifa).val();
				
				Igv = parseFloat(parseFloat(parseFloat(CargoFijo) + parseFloat(Agua) + parseFloat(Desague)) * 0.18).toFixed(2);
				
				Igv = Igv.replace(",", "");
				
				Redondeo = (parseFloat(CargoFijo) + parseFloat(Agua) + parseFloat(Desague) + parseFloat(Igv)).toFixed(1);
				
				if (Redondeo < (parseFloat(CargoFijo) + parseFloat(Agua) + parseFloat(Desague) + parseFloat(Igv)))
				{
					Redondeo = ((parseFloat(CargoFijo) + parseFloat(Agua) + parseFloat(Desague) + parseFloat(Igv)) - Redondeo).toFixed(2) * -1;
				}
				else
				{
					Redondeo = (Redondeo - (parseFloat(CargoFijo) + parseFloat(Agua) + parseFloat(Desague) + parseFloat(Igv))).toFixed(2);
				}
        
				$('#Agua' + CodTarifa).val(Agua);
				$('#Desague' + CodTarifa).val(Desague);
				$('#Igv' + CodTarifa).val(Igv);
				$('#Redondeo' + CodTarifa).val(Redondeo);

				Total = (parseFloat(Agua) + parseFloat(Desague) + parseFloat(Igv) + parseFloat(Redondeo) + parseFloat($('#CargoFijo' + CodTarifa).val())).toFixed(2);
				
				$('#Total' + CodTarifa).val(Total);
			}
		})
	}
	
	function ImporteTarifas(evt)
	{
		var keyPressed = (evt.which) ? evt.which : event.keyCode;
		
		if (keyPressed == 13)
		{
			ImporteTarifaT();
		}
	}
	
	function ImporteTarifaT()
	{
		var f = parseInt($("#TbTarifas tbody tr").length);
		
		for (var i = 1; i <= f; i++) 
		{
			ImporteTarifa($("#TbTarifas tbody tr#" + i + ' label.idtarifa').text());
		}
	}
	
	function Cancelar()
	{
		location.href = 'index.php';
	}
	
	function Cerrar()
	{
		location.href='../../../../admin/index.php';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
	<table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
                <td valign="bottom" class="TitDetalle">Volumen</td>
                <td align="center" valign="bottom" class="TitDetalle">:</td>
                <td class="CampoDetalle">
					<table width="400" border="0" cellspacing="0" cellpadding="0">
						<tr>
                            <td width="100">
                            	<input style="text-align:center" class="inputtext numeric" name="Volumen" type="text" id="Volumen" size="5" value="20" onkeypress="ImporteTarifas(event);" onchange="ImporteTarifaT();"/>
							</td>
                            <td width="100">
								<input type="button" onclick="ImporteTarifaT();" value="Calcular" id="consul_id">
                            </td>
                            <td>
								<input type="button" onclick="Cerrar();" value="Cerrar" id="BtnCancelar" title="Salir del Modulo">
							</td>
						</tr>
					</table>
				</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
			<tr>
				<td colspan="3" align="center">
					<table border="1"  class="ui-widget" width="700" id="TbTarifas" cellspacing="0"  rules="all" >
						<thead class="ui-widget-header" style="font-size:12px">
							<tr>
                                <th title="Cabecera" width="50" height="20">Tarifa</th>
                                <th title="Cabecera" width="50" height="20">C. Fijo.</th>
                                <th title="Cabecera" width="50" height="20">Agua</th>
                                <th title="Cabecera" width="50" height="20">Desag.</th>
                                <th title="Cabecera" width="50">I.G.V.</th>
                                <th title="Cabecera" width="50" height="20">Redo.</th>
                                <th title="Cabecera" width="50" height="20">Total</th>
							</tr>
						</thead>
						<tbody>
<?php
	$SqlU = "SELECT DISTINCT catetar, nomtar, impcargofijo1 FROM facturacion.tarifas WHERE estado = 1 AND codsuc=".$codsuc." ORDER BY catetar";
	$Consulta = $conexion->query($SqlU);
	$i = 0;
	
	foreach ($Consulta->fetchAll() as $RowU)
	{
		$i++;
?>
							<tr id='<?=$i?>'>
                                <td align="left" style="padding-left:5px;"><label class='idtarifa'><?php echo $RowU[0];?></label> - <?=$RowU[1];?></td>
                                <td align="center"><input style="text-align:right" class="inputtext" type="text" id="CargoFijo<?php echo $RowU[0];?>" size="6" value="<?php echo number_format($RowU[2], 2);?>" readonly="readonly" /></td>
                                <td align="center"><input style="text-align:right" class="inputtext" type="text" id="Agua<?php echo $RowU[0];?>" size="6" value="" readonly="readonly"/></td>
                                <td align="center"><input style="text-align:right" class="inputtext" type="text" id="Desague<?php echo $RowU[0];?>" size="6" value="" readonly="readonly" /></td>
                                <td align="center"><input style="text-align:right" class="inputtext" type="text" id="Igv<?php echo $RowU[0];?>" size="6" value="" readonly="readonly" /></td>
                                <td align="center"><input style="text-align:right" class="inputtext" type="text" id="Redondeo<?php echo $RowU[0];?>" size="6" value="" readonly="readonly" /></td>
                                <td align="center"><input style="text-align:right" class="inputtext" type="text" id="Total<?php echo $RowU[0];?>" size="6" value="" readonly="readonly" /></td>
							</tr>
<?php
	}
?>
						</tbody>
						<tfoot class="ui-widget-header">
							<tr><td colspan=7></td></tr>
						</tfoot>
					</table>
					<input type="hidden" name="ConProductos" id="ConProductos" value="<?=$NumRegs?>"/>
				</td>
            </tr>
            <tr>
                <td class="TitDetalle">&nbsp;</td>
                <td class="TitDetalle">&nbsp;</td>
                <td class="CampoDetalle">&nbsp;</td>
            </tr>
            <tr>
				<td colspan=4>
					<div id="DivTarifasAcordion">
<?php
	$sql = "SELECT * FROM facturacion.tarifas WHERE estado = 1 AND codsuc=".$codsuc." ORDER BY catetar";
	$ConsultaTar = $conexion->query($sql);
	
	foreach ($ConsultaTar->fetchAll() as $tarifas)
	{
?>
						<h3><?=strtoupper($tarifas["nomtar"])?></h3>
						<div style="height: 100px; display: block;">
							<table width="800px" border="0" align="center" cellspacing="0" class="ui-widget" rules="cols">
								<thead class="ui-widget-header" style="font-size:10px">
									<tr>
                                        <td>&nbsp;</td>
                                        <td align="center">Volumen</td>
                                        <td align="center">Importe Agua</td>
                                        <td align="center">Importe Desague</td>
									</tr>
								</thead>
								<tr>
                                    <td align="right" class="ui-widget-header" style="font-size:10px">Rango Inicial</td>
                                    <td align="right">
										<label class="inputtext" id="hastarango1" ><?=number_format($tarifas["hastarango1"], 2)?></label>
                                    </td>
                                    <td align="right">
										<label class="inputtext" id="impconsmin" ><?=$tarifas["impconsmin"]?></label>
                                    </td>
                                    <td align="right">
										<label class="inputtext" id="impconsmindesa"><?=$tarifas["impconsmindesa"]?></label>
									</td>
                                </tr>
                                <tr>
                                    <td align="right" class="ui-widget-header" style="font-size:10px">Rango Intermedio</td>
                                    <td align="right">
										<label class="inputtext" id="hastarango2"><?=number_format($tarifas["hastarango2"], 2)?></label>
                                    </td>
                                    <td align="right">
										<label class="inputtext" id="impconsmedio"><?=$tarifas["impconsmedio"]?></label>
                                    </td>
                                    <td align="right">
										<label class="inputtext" id="impconsmediodesa"><?=$tarifas["impconsmediodesa"]?></label>
									</td>
                                </tr>
                                <tr>
                                    <td align="right" class="ui-widget-header" style="font-size:10px">Rango Final</td>
                                    <td align="right">
										<label class="inputtext" id="hastarango3"><?=number_format($tarifas["hastarango3"], 2)?></label>
                                    </td>
                                    <td align="right">
										<label class="inputtext" id="impconsexec"><?=$tarifas["impconsexec"]?></label>
                                    </td>
                                    <td align="right">
										<label class="inputtext" id="impconsexecdesa"><?=$tarifas["impconsexecdesa"]?></label>
									</td>
								</tr>
							</table>
						</div>
<?php
	}
?>
					</div> 
				</td>
			</tr>
		</tbody>
	</table>
</form>
</div>
<script>
	$("#Volumen").focus();
	
	ImporteTarifaT();
</script>
<?php
	CuerpoInferior();
?>