<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../../../../config.php');
	
	$CodSuc		= $_SESSION['IdSucursal'];
	$Categoria	= $_POST['Categoria'];
	$Volumen	= $_POST['Volumen'];
	
	$Agua 		= 0;
	$Desague 	= 0;
	
	$SqlT	= "SELECT T.catetar, T.impconsmin, T.impconsmedio, T.impconsmindesa, T.impconsmediodesa, ";
	$SqlT	.= " T.hastarango1, T.hastarango2, T.impconsexec, T.impconsexecdesa ";
	$SqlT	.= "FROM facturacion.tarifas T ";
	$SqlT	.= "WHERE T.estado = 1 AND T.codsuc = ".$CodSuc." AND T.catetar = ".$Categoria;
	
	$row	= $conexion->query($SqlT)->fetch();
	
	if ($CodSuc == 6)
	{
		if ($Categoria == 2) 
		{
			if ($Volumen <= $row[5]) 
			{
				$Agua 		= number_format($Volumen * $row[1], 2);
				$Desague 	= number_format($Volumen * $row[3], 2);
			}
			if ($row[5] < $Volumen and $Volumen <= $row[6]) 
			{
				$Agua		= number_format($row[5] * $row[1] + (($Volumen - $row[5]) * $row[2]), 2); 
				$Desague	= number_format($row[5] * $row[3] + (($Volumen - $row[5]) * $row[4]), 2);
			}
			if ($Volumen > $row[6]) 
			{
				$Agua		= number_format($row[5] * $row[1] + (($row[6] - $row[5]) * $row[2]) + (($Volumen - $row[6]) * $row[7]), 2); 
				$Desague	= number_format($row[5] * $row[3] + (($row[6] - $row[5]) * $row[4]) + (($Volumen - $row[6]) * $row[8]), 2);
			}
		}
		else
		{
			if ($Volumen <= $row[5]) 
			{
				$Agua 		= number_format($Volumen * $row[1], 2);
				$Desague 	= number_format($Volumen * $row[3], 2);
			}
			if ($Volumen > $row[5]) 
			{
				$Agua		=  number_format($row[5] * $row[1] + (($Volumen - $row[5]) * $row[2]), 2); 
				$Desague	=  number_format($row[5] * $row[3] + (($Volumen - $row[5]) * $row[4]), 2);
			}
		}
	}
	//$igv = ($Agua + $Desague) * 0.18;
	
	echo $Agua."|".$Desague."|".$Categoria."|".$row[6];
	
?>