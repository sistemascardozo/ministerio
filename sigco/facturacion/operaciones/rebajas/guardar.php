<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	ini_set("display_errors", 1);
	error_reporting(E_ALL);

	include("../../../reclamos/operaciones/ingresos/clase/clsproceso.php");
	include("../../../../objetos/clsFunciones.php");

	$objFunciones = new clsFunciones();

	$codsuc         = $_SESSION["IdSucursal"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$conreclamo     = $_POST["conreclamo"];
	$nroreclamo     = $_POST["nroreclamo"]?$_POST["nroreclamo"]:0;
	$correlativoreclamo     = $_POST["correlativoreclamo"]?$_POST["correlativoreclamo"]:0;
	$glosa          = $_POST["glosarebaja"];
	$idusuario      = $_SESSION["id_user"];
	$anio           = $_POST["anio"];
	$mes            = $_POST["mes"];
	$documento      = $_POST["documento"];
	$DocSeries      = $objFunciones->GetSeries($codsuc, $documento);
	$seriedoc       = $DocSeries["serie"];
	$numerodoc      = $DocSeries["correlativo"];


	$nrorebaja      = $objFunciones->setCorrelativosVarios(9, $codsuc, "SELECT", 0);
	$n              = $objFunciones->setCorrelativosVarios(9, $codsuc, "UPDATE", intval($nrorebaja));

	$conexion->beginTransaction();

	$Error = 0;

	$sqlI  = "INSERT INTO facturacion.cabrebajas ";
	$sqlI  .= "(codemp, codsuc, nrorebaja, nroinscripcion, conreclamo, nroreclamo, glosa, ";
	$sqlI  .= " creador, anio, mes, hora) ";
	$sqlI  .= "VALUES(1, ".$codsuc.", ".$nrorebaja.", ".$nroinscripcion.", ".$conreclamo.", ".$correlativoreclamo.", '".$glosa."', ";
	$sqlI  .= " ".$idusuario.", '".$anio."', '".$mes."', '".date('H:i')."')";

	$consultaI = $conexion->prepare($sqlI);
	$consultaI->execute(array());

	if ($consultaI->errorCode() != '00000')
    {
        $conexion->rollBack();

        $mensaje = "Error reclamos";

		$Error = 1;

        die(2);
    }

	if($nroreclamo != 0)
	{
		$sqlM = "SELECT * FROM reclamos.mesesreclamados ";
		$sqlM .= "WHERE codsuc = ".$codsuc." ";
		$sqlM .= " AND nroreclamo = ".$nroreclamo." ";
		$sqlM .= "ORDER BY nrofacturacion";

		$consultaM = $conexion->prepare($sqlM);
		$consultaM->execute(array());

		if ($consultaM->errorCode() != '00000')
		{
			$conexion->rollBack();
			$mensaje = "Error reclamos";

			$Error = 1;

			die(2);
		}

		$itemsM = $consultaM->fetchAll();
		//$numerodoc = intval($numerodoc) - 1;
		$temp = 0;

		$ImporteMes = 0;

		foreach($itemsM as $rowM)
		{
			if(floatval($_POST['imprebajado'.$rowM["nrofacturacion"]]) > 0)//solo si se realizo una rebaja al recibo
			{
				if($temp > 0 and $temp != $rowM["nrofacturacion"])
				{
					$numerodoc++;

					//ACTUALIZAR CORRELATIVO
					$sql = "UPDATE reglasnegocio.correlativos ";
					$sql .= "SET correlativo = correlativo + 1 ";
					$sql .= "WHERE codemp = 1 ";
					$sql .= " AND codsuc = ".$codsuc." ";
					$sql .= " AND nrocorrelativo = ".$documento;

					$result = $conexion->query($sql);

					if (!$result)
					{
						$conexion->rollBack();

						$mensaje = "619 Error UPDATE correlativos";
						$data['res'] = 2;
						$data['mensaje'] = $mensaje;

						$Error = 1;

						die(json_encode($data));
					}
				}

				$insD  = "INSERT INTO facturacion.detrebajas";
				$insD .= "(codemp, codsuc, nrorebaja, nrofacturacion, codconcepto, imporiginal, ";
				$insD .= " imprebajado, codtipodeuda, categoria, coddocumento, nrodocumento, ";
				$insD .= " anio, mes, codciclo, seriedocumento, coddocumentoabono, ";
				$insD .= " seriedocumentoabono, nrodocumentoabono, consumo, item) ";
				$insD  .= "VALUES(1, ".$codsuc.", ".$nrorebaja.", ".$rowM["nrofacturacion"].", ".$rowM["codconcepto"].", '".$rowM["imporiginal"]."', ";
				$insD  .= " '".$rowM["imprebajado"]."', ".$rowM["codtipodeuda"].", ".$rowM["categoria"].", ".$rowM["coddocumento"].", ".$rowM["nrodocumento"].", ";
				$insD  .= " '".$rowM["anio"]."', '".$rowM["mes"]."', ".$rowM["codciclo"].", '".$rowM['seriedocumento']."', ".$documento.", ";
				$insD  .= " '".$seriedoc."', ".$numerodoc.", ".$rowM['consumoreclamo'].", ".$rowM["item"].")";

				$temp = $rowM["nrofacturacion"];

				$consultaD = $conexion->prepare($insD);
				$consultaD->execute(array());

				if ($consultaD->errorCode() != '00000')
				{
					$conexion->rollBack();

					$mensaje = "Error reclamos";

					$Error = 1;

					die(2);
				}

				$updF = "UPDATE facturacion.detfacturacion ";
				$updF .= "SET importerebajado = '".$rowM["imprebajado"]."' ";
				$updF .= "WHERE codemp = 1 ";
				$updF .= " AND codsuc = ".$codsuc." ";
				$updF .= " AND nrofacturacion = ".$rowM["nrofacturacion"]." ";
				$updF .= " AND nroinscripcion = ".$nroinscripcion." ";
				$updF .= " AND codconcepto = ".$rowM["codconcepto"]." ";
				$updF .= " AND codtipodeuda = ".$rowM["codtipodeuda"]." ";
				$updF .= " AND estadofacturacion = 1 ";
				$updF .= " AND item = ".$rowM["item"]." ";

				$consultaF = $conexion->prepare($updF);
				$consultaF->execute(array());

				if ($consultaF->errorCode() != '00000')
				{
					$conexion->rollBack();

					$mensaje = "Error reclamos";

					$Error = 1;

					die(2);
				}
			}

			$SqlImpM = "SELECT SUM(importe - importerebajado) ";
			$SqlImpM .= "FROM facturacion.detfacturacion ";
			$SqlImpM .= "WHERE codemp = 1 ";
			$SqlImpM .= " AND codsuc = ".$codsuc." ";
			$SqlImpM .= " AND nrofacturacion = ".$rowM["nrofacturacion"]." ";
			$SqlImpM .= " AND nroinscripcion = ".$nroinscripcion." ";

			$consultaImpM = $conexion->prepare($SqlImpM);
        	$consultaImpM->execute(array());

			if ($consultaImpM->errorCode() != '00000')
			{
				$conexion->rollBack();

				$mensaje = "Error reclamos";

				$Error = 1;

				die(2);
			}

			$itemImpM = $consultaImpM->fetch();

			$SqlImpM = "UPDATE facturacion.cabfacturacion ";
			$SqlImpM .= "SET impmes = '".$itemImpM[0]."', ";
			$SqlImpM .= " consumofact = '".$rowM['consumoreclamo']."' ";
			$SqlImpM .= "WHERE codemp = 1 ";
			$SqlImpM .= " AND codsuc = ".$codsuc." ";
			$SqlImpM .= " AND nrofacturacion = ".$rowM["nrofacturacion"]." ";
			$SqlImpM .= " AND nroinscripcion = ".$nroinscripcion." ";

			$consultaImpM = $conexion->prepare($SqlImpM);
			$consultaImpM->execute(array());

			if ($consultaImpM->errorCode() != '00000')
			{
				$conexion->rollBack();

				$mensaje = "Error reclamos";

				$Error = 1;

				die(2);
			}
		}

		//PONER EN RECLAMO LA FACTURACION
		$sqlM = "SELECT codciclo, nrofacturacion ";
		$sqlM .= "FROM reclamos.mesesreclamados ";
		$sqlM .= "WHERE codsuc = ".$codsuc." ";
		$sqlM .= " AND nroreclamo = ".$nroreclamo." ";
		$sqlM .= "GROUP BY codciclo, nrofacturacion";

        $consultaM = $conexion->prepare($sqlM);
        $consultaM->execute(array());

		if ($consultaM->errorCode() != '00000')
		{
			$conexion->rollBack();

			$mensaje = "Error reclamos";

			$Error = 1;

			die(2);
		}

		$itemsM = $consultaM->fetchAll();

		foreach($itemsM as $rowM)
		{
			//PONER EN SIN RECLAMO LA FACTURACION
			$Sql = "UPDATE facturacion.cabfacturacion ";
			$Sql .= "SET enreclamo = 0 ";
			$Sql .= "WHERE codemp = 1 ";
			$Sql .= " AND codsuc = ".$codsuc." ";
			$Sql .= " AND codciclo = ".$rowM['codciclo']." ";
			$Sql .= " AND nrofacturacion = ".$rowM['nrofacturacion']." ";
			$Sql .= " AND nroinscripcion = ".$nroinscripcion." ";

			$result = $conexion->prepare($Sql);
			$result->execute(array());

			if ($result->errorCode() != '00000')
			{
				$conexion->rollBack();

				$mensaje = "Error reclamos";

				$Error = 1;

				die(2);
			}
		}

		//PONER EN RECLAMO LA FACTURACION
		$updR = "UPDATE reclamos.reclamos ";
		$updR .= "SET codestadoreclamo = 6 ";
		$updR .= "WHERE codsuc = ".$codsuc." ";
		$updR .= " AND nroreclamo = ".$nroreclamo." ";

		$consultaR = $conexion->prepare($updR);
		$consultaR->execute(array());

		$updR = "UPDATE reclamos.detalle_reclamos ";
		$updR .= "SET codestadoreclamo = 6 ";
		$updR .= "WHERE codsuc = ".$codsuc." ";
		$updR .= " AND nroreclamo = ".$nroreclamo." ";
		$updR .= " AND codestadoreclamo = 4";

		$consultaR = $conexion->prepare($updR);
		$consultaR->execute(array());
	}
	else //SIN RECLAMO
	{
		$_SESSION["oreclamos"] = unserialize(serialize($_SESSION["oreclamos"]));

		$NroItems	= $_POST['NroItems'];
		//$numerodoc	= intval($numerodoc) - 1;
		$count		= $_SESSION["oreclamos"]->item;
		$temp		= 0;

		for($j = 1; $j <= $NroItems; $j++)
		{
			if(floatval($_POST['imptotal'.$j]) > 0)//solo si se realizo una rebaja al recibo
			{
				for ($i = 0; $i < $count; $i++)
				{
					if (isset($_SESSION["oreclamos"]->nrofacturacion[$i]))
					{
						if ($_POST["nrofacturacion".$j] == $_SESSION["oreclamos"]->nrofacturacion[$i] /*&&
                                    $_POST["codtipodeuda" . $j] == $_SESSION["oreclamos"]->codtipodeuda[$i] &&
                                    $_POST["categoria" . $j] == $_SESSION["oreclamos"]->categoria[$i]*/)
						{
							if($temp > 0 and $temp != $_POST["nrofacturacion".$j])
							{
								$numerodoc++;

								//ACTUALIZAR CORRELATIVO
								$sql = "UPDATE reglasnegocio.correlativos ";
								$sql .= "SET correlativo = correlativo + 1 ";
								$sql .= "WHERE codemp = 1 ";
								$sql .= " AND codsuc = ".$codsuc." ";
								$sql .= " AND nrocorrelativo = ".$documento;

								$result = $conexion->query($sql);

								if (!$result)
								{
									$conexion->rollBack();

									$mensaje = "619 Error UPDATE correlativos";
									$data['res'] = 2;
									$data['mensaje'] = $mensaje;

									$Error = 1;

									die(json_encode($data));
								}
							}

							$insD  = "INSERT INTO facturacion.detrebajas";
							$insD  .= "(codemp, codsuc, nrorebaja, nrofacturacion, codconcepto, ";
							$insD  .= " imporiginal, imprebajado, ";
							$insD  .= " codtipodeuda, categoria, ";
							$insD  .= " coddocumento, nrodocumento, anio, mes, ";
							$insD  .= " codciclo, seriedocumento, coddocumentoabono, ";
							$insD  .= " seriedocumentoabono, nrodocumentoabono, consumo, item) ";
							$insD  .= "VALUES(1, ".$codsuc.", ".$nrorebaja.", ".$_POST["nrofacturacion".$j].", ".$_SESSION["oreclamos"]->codconcepto[$i].", ";
							$insD  .= " '".$_SESSION["oreclamos"]->imporiginal[$i]."', '".$_SESSION["oreclamos"]->imprebajado[$i]."', ";
							$insD  .= " ".$_SESSION["oreclamos"]->codtipodeuda[$i].", ".$_SESSION["oreclamos"]->categoria[$i].", ";
							$insD  .= " 4, ".$_SESSION["oreclamos"]->nrodocumento[$i].", '".$_SESSION["oreclamos"]->anio[$i]."', '".$_SESSION["oreclamos"]->mes[$i]."', ";
							$insD  .= " ".$_SESSION["oreclamos"]->codciclo[$i].", '".$_SESSION['oreclamos']->seriedocumento[$i]."', ".$documento.", ";
							$insD  .= " '".$seriedoc."', ".$numerodoc.", ".$_SESSION["oreclamos"]->consumorec[$i].", ".$_SESSION["oreclamos"]->codconceptoitem[$i].")";

							$temp = $_POST["nrofacturacion".$j];

							$consultaR = $conexion->prepare($insD);
							$consultaR->execute(array());

							if ($consultaR->errorCode() != '00000')
							{
								$conexion->rollBack();

								$mensaje = "Error reclamos";

								$Error = 1;

								die(2);
							}

							$updF = "UPDATE facturacion.detfacturacion ";
							$updF .= "SET importerebajado = '".$_SESSION["oreclamos"]->imprebajado[$i]."' ";
							$updF .= "WHERE codemp = 1 ";
							$updF .= " AND codsuc = ".$codsuc." ";
							$updF .= " AND nrofacturacion = ".$_POST["nrofacturacion".$j]." ";
							$updF .= " AND nroinscripcion = ".$nroinscripcion." ";
							$updF .= " AND codconcepto = ".$_SESSION["oreclamos"]->codconcepto[$i]." ";
							$updF .= " AND codtipodeuda = ".$_SESSION["oreclamos"]->codtipodeuda[$i]." ";
							$updF .= " AND estadofacturacion = 1 ";
							$updF .= " AND item = ".$_SESSION["oreclamos"]->codconceptoitem[$i]." ";

							$consultaR = $conexion->prepare($updF);
							$consultaR->execute(array());

							if ($consultaR->errorCode() != '00000')
							{
								$conexion->rollBack();

								$mensaje = "Error reclamos";

								$Error = 1;

								die(2);
							}
						}
					}

					$SqlImpM = "SELECT SUM(importe - importerebajado) AS suma ";
					$SqlImpM .= "FROM facturacion.detfacturacion ";
					$SqlImpM .= "WHERE codemp = 1 ";
					$SqlImpM .= " AND codsuc = ".$codsuc." ";
					$SqlImpM .= " AND nrofacturacion = ".$_POST["nrofacturacion".$j]." ";
					$SqlImpM .= " AND nroinscripcion = ".$nroinscripcion." ";

					$consultaImpM = $conexion->prepare($SqlImpM);
					$consultaImpM->execute(array());

					if ($consultaImpM->errorCode() != '00000')
					{
						$conexion->rollBack();

						$mensaje = "Error reclamos";

						$Error = 1;

						die(2);
					}

					$itemImpM = $consultaImpM->fetch();

					$SqlImpM = "UPDATE facturacion.cabfacturacion ";
					$SqlImpM .= "SET impmes = '".$itemImpM['suma']."', enreclamo = 0, ";
					$SqlImpM .= " consumofact = '".$_SESSION["oreclamos"]->consumorec[$i]."' ";
					$SqlImpM .= "WHERE codemp = 1 ";
					$SqlImpM .= " AND codsuc = ".$codsuc." ";
					$SqlImpM .= " AND nrofacturacion = ".$_POST["nrofacturacion".$j]." ";
					$SqlImpM .= " AND nroinscripcion = ".$nroinscripcion." ";

					$consultaImpM = $conexion->prepare($SqlImpM);
					$consultaImpM->execute(array());

					if ($consultaImpM->errorCode() != '00000')
					{
						$conexion->rollBack();

						$mensaje = "Error reclamos";

						$Error = 1;

						die(2);
					}
				}
			}
		}
	}



	if ($Error == 1)
	{
		$conexion->rollBack();

		$mensaje = "Error al Grabar Registro";
		echo $res = 2;
	}
	else
	{
		$conexion->commit();

		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res = 1;
	}
?>
