<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	unset($_SESSION["oreclamos"]);

	include("../../../../objetos/clsDrop.php");
	include("../../../reclamos/operaciones/ingresos/clase/clsproceso.php");

	$Op 				= $_GET["Op"];
	$Id 				= isset($_GET["Id"])?$_GET["Id"]:'';
	$codsuc 			= $_SESSION['IdSucursal'];
	$guardar			= "op=".$Op;

	$fecha =date("d/m/Y");

	$objMantenimiento 	= new clsDrop();

	$paramae = $objMantenimiento->getParamae("IMPIGV",$codsuc);

	$fechaserver = $objMantenimiento->FechaServer();

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script src="js_rebajas.js" type="text/javascript"></script>
<style>
	.myTable{
		background-color:#0099CC; border: 1 #000000 solid
	}
</style>
<script>
	var cont 		= 0;
	var nDest 		= 0;
	var nDestC 		= 0;
	var object 		= "";
	var fechaserver = "<?=$fechaserver?>";
	var codsuc		= <?=$codsuc?>;
	var urldir		= "<?php echo $_SESSION['urldir'];?>"
	$(function(){
		if('<?=$Op?>'==0)
		cSerieNun()

		$("#DivRebajar").dialog({
            autoOpen: false,
            height: 480,
            width: 650,
            modal: true,
            resizable: false,
            buttons: {
                "Aceptar": function () {
                    guardarrebajasres();
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            }
        });


	});
	function cSerieNun()
	{
		var TipoDoc = $("#documento").val()

			$.ajax({
			 url:urldir+'sigco/cobranza/operaciones/cancelacion/include/cSerieNun.php',
			 type:'POST',
			 async:true,
			 data:'codsuc='+codsuc+'&TipoDoc='+TipoDoc,
			 success:function(datos){
			 	var r=datos.split("|")
			 	$("#seriedoc").val(r[0])
			 	$("#numerodoc").val(r[1])
			 }
	    	})

	}
	function ValidarForm(Op)
	{
		if($("#conreclamo").val()==1)
		{

			if($("#nroreclamo").val()=="" || $("#nroreclamo").val()==0)
			{
				Msj($("#nroreclamo"),"Ingrese el nro. de reclamo a generar")
				return false;
			}
		}
		if($("#glosarebaja").val()=="")
		{
			Msj($("#glosarebaja"),"La glosa de la Rebaja no puede ser NULO")
			$("#glosarebaja").focus();
			return false;
		}

		var miTabla 	= document.getElementById("tbrebajas");
		var numFilas 	= miTabla.rows.length;

		if(numFilas==1)
		{
			alert("No hay Ninguna Rebaja Generada...Genere como minima una para poder continuar")
			return false;
		}
		if(parseFloat($("#TotalRebajado").html())==0)
		{
			alert("No se a Rebajado un monto")
			return false;
		}
		GuardarP(Op)
	}
	function Cancelar()
	{
		location.href='index.php?Op=1';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?" enctype="multipart/form-data">
  <table width="1000" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    <tbody>
	<tr>
	  <td >&nbsp;</td>
	  <td >&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td colspan="4" >&nbsp;</td>
	  </tr>
	<tr>
	  <td width="10" >&nbsp;</td>
	  <td width="90" >Nro Reclamo </td>
	  <td width="30" align="center">:</td>
	  <td colspan="4" >
	  	<input type="hidden" name="NroItems" id="NroItems" value="0" />
	  	<input type="hidden" name="anio" id="anio" value="0" />
	    <input type="hidden" name="mes" id="mes" value="0" />
	    <input type="hidden" id="codciclo" value="<?=$row['codciclo'] ?>">
      	<input type="hidden" name="nroreclamo" id="nroreclamo" size="15" maxlength="15" readonly="readonly" class="inputtext" value="" />
      	<input type="text" id="correlativoreclamo" name="correlativoreclamo" maxlength="15" readonly="readonly" class="inputtext" value="" style="width:80px;" />

	    <span class="MljSoft-icon-buscar" title="Buscar Reclamos" onclick="Buscarreclamos();"></span>
	    <input type="text" name="reclamo" id="reclamo" class="inputtext" maxlength="60" readonly="readonly" style="width:400px;" />
        <label>
	    <input type="checkbox" name="checkbox" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'conreclamo'); vReclamo(this);" />
	    </label>
	    Con Reclamo
	    <input type="hidden" name="conreclamo" id="conreclamo" value="1" /></td>
	</tr>
	<tr>
	  <td >&nbsp;</td>
	  <td >Usuario</td>
	  <td align="center">:</td>
	  <td colspan="4" >
	  	<input type="hidden" name="nroinscripcion" readonly="readonly" id="nroinscripcion" size="15" maxlength="15" class="inputtext" value="" />
	     <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" value="<?php echo $row["codantiguo"]?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:80px;"/>
	    <span class="MljSoft-icon-buscar" title="Buscar Clientes" onclick="BuscarUsuario();"></span>
	    <input type="text" name="propietario" id="propietario" class="inputtext" maxlength="60" readonly="readonly" style="width:400px;" />
	    </td>
	  </tr>
	<tr>
	  <td>&nbsp;</td>
		<td>Documento</td>
        <td align="center">:</td>
        <td colspan="4">
          <? $objMantenimiento->drop_documentos(17, 'onChange="cSerieNun(this)"', " WHERE coddocumentO = 17 AND estareg = 1 AND tipodocumento = 0 AND codsuc = ".$codsuc); ?>
        	&nbsp;&nbsp;&nbsp;&nbsp;Serie&nbsp;:&nbsp;
          <input type="text" name="seriedoc" id="seriedoc" class="inputtext" size="3" maxlength="45"  readonly="readonly" value=""/>
          &nbsp;
        -&nbsp;&nbsp;&nbsp;<input type="text" name="numerodoc" id="numerodoc" class="inputtext" size="11" maxlength="45"  readonly="readonly" value=""/>
    </td>
	</tr>
	<tr height="50">
	  <td valign="top">&nbsp;</td>
	  <td valign="top">Mot. Reclamo </td>
	  <td align="center" valign="top">:</td>
	  <td colspan="4" valign="top" ><label>
	    <textarea name="motivoreclamo" id="motivoreclamo" readonly="readonly" class="inputtext" style="font-size:11px; width:500px; height:40px;"></textarea>
	  </label></td>
	  </tr>
	<tr>
	  <td valign="top">&nbsp;</td>
	  <td valign="top">Glosa</td>
	  <td align="center" valign="top">:</td>
	  <td colspan="4" valign="top" ><textarea name="glosarebaja" id="glosarebaja" class="inputtext" style="font-size:11px; width:500px; height:40px;"></textarea></td>
	  </tr>
	<tr>
	  <td colspan="7" class="TitDetalle">
	  <div style="height:120px; overflow:scroll" id="div_facturacion">
      	<table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbfacturacion" rules="all">
			<thead class="ui-widget-header" >
            <tr align="center">
              <th width="11%" >Nro. Facturacion</th>
              <th width="13%" >Doc.</th>
              <th width="11%" >Nro. Doc.</th>
              <th width="12%" >A&ntilde;o y Mes</th>
              <!-- <th width="24%" >Tipo de Deuda</th> -->
              <!-- <th width="11%" >Categoria</th> -->
              <th width="10%" >Importe</th>
              </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
      </td>
	</tr>
	<tr style="height:5px">
	  <td colspan="7" class="TitDetalle"></td>
	  </tr>
	<tr style="height:5px">
	  <td colspan="7" class="TitDetalle">
		<div style="height:120px; overflow:scroll" id="div_detallereclamos">
		  <table border="1" align="center" cellspacing="0" class="ui-widget myTable" width="100%" id="tbrebajas" rules="all">
			<thead class="ui-widget-header" >
                <tr align="center">
                  <th width="5%" >Nro.Facturacion</th>
                  <th width="10%" >Doc.</th>
                  <th width="10%" >Nro. Doc.</th>
                  <th width="10%" >A&ntilde;o y Mes</th>
                  <!-- <th width="24%" >Tipo de Deuda</th> -->
                  <!-- <th width="11%" >Categoria</th> -->
                  <th width="10%" >Importe Ori</th>
                  <th width="10%" >Rebajado</th>
                  <th width="10%" >A Pagar</th>
                  <th width="1%" align="center" scope="col">&nbsp;</th>
                  </tr>
            </thead>
            <tbody></tbody>
            <tfoot class="ui-widget-header" >
            	<tr>
            		<td colspan="4" align="right">Total Rebajado</td>
            		<td align="right"><label id="TotalOriginal">0.00</label></td>
            		<td align="right"><label id="TotalRebajado">0.00</label></td>
            		<td align="right"><label id="TotalaPagar">0.00</label></td>
            		<td>&nbsp;</td>
            	</tr>
            </tfoot>
          </table>
		</div>
       </td>
	</tr>
	<tr style="height:5px">
	  <td colspan="7" class="TitDetalle"></td>
	  </tr>
    </tbody>

  </table>
 <div id="DivRebajar" title="Ver Detalle de Facturacion para Realizar la Rebaja"  >
    <div id="div_detalle_facturacion"></div>
</div>
</form>
</div>
