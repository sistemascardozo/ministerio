<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	
	$Criterio="Rebajas";
	$TituloVentana = "REBAJAS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];
	$FormatoGrilla = array ();
	$documento  = 9;
	$Criterio='Rebajas';
	$Sql = "select cab.nrorebaja,cab.nroreclamo,clie.propietario,
			".$objFunciones->FormatFecha('cab.fechaemision').",
			cab.glosa, u.login,
			est.descripcion,
			est.codestrebaja
			from facturacion.cabrebajas as cab
			inner join catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and
			cab.nroinscripcion=clie.nroinscripcion)
			INNER JOIN public.estadorebaja as est on(cab.codestrebaja=est.codestrebaja)
			INNER JOIN seguridad.usuarios as u ON (cab.codemp=u.codemp and cab.creador=u.codusu)";
	
	$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
	$FormatoGrilla[1] = array('1'=>'cab.nrorebaja', '2'=>'clie.propietario','3'=>'cab.nroinscripcion','4'=>'clie.codantiguo');          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1'=>'Nro. Rebaja', 'T2'=>'Nro. Reclamo', 'T3'=>'Propietario',
							'T4'=>'Fecha Emision', 'T5'=>'Observacion', 'T6'=>'Usuario', 'T7'=>'Estado');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1'=>'90','W2'=>'90','W3'=>'280','W4'=>'90','W6'=>'50','W7'=>'1');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = "  and (cab.codemp=1 and cab.codsuc=".$codsuc.") ORDER BY cab.nrorebaja desc ";                                   //Orden de la Consulta
	$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
			  'NB'=>'1',          //Número de Botones a agregar
			  'BtnId1'=>'BtnModificar',   //Nombre del Boton
			  'BtnI1'=>'imprimir.png',   //Imagen a mostrar
			  'Btn1'=>'Imprimir Nota de Abono',       //Titulo del Botón
			  'BtnF1'=>'onclick="Imprimir(this.id, 1);"',  //Eventos del Botón
			  'BtnCI1'=>'8',  //Item a Comparar
			  'BtnCV1'=>'1'    //Valor de comparación
			  );
			  
	$_SESSION['Formato'.$Criterio] = $FormatoGrilla;
	Cabecera('', $FormatoGrilla[7], 1050, 600);
	Pie();

?>
<script type="text/javascript">
function Imprimir(Id)
  {
	  Id2 = Id.substr(Id.indexOf('-') + 1);
	  
    var url='';
    url='imprimir_abono.php';
    AbrirPopupImpresion(url+'?Id='+Id2 + '&codsuc=<?=$codsuc?>',800,500)
  }
</script>
<?php
CuerpoInferior();
?>
