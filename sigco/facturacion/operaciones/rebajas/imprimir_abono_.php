<?php
    include("../../../../objetos/clsReporte.php");

    include("../../../../admin/clases/num2letra.php");
    //require_once 'Image/Barcode.php';

    class clsImpresionDuplicado extends clsReporte {

        function Header() {

        }

        function Contenido($nroinscripion, $codcatastro, $facturacion, $recibo, $femision, $propietario, $direccion, $servicio, $tfact, $tarifa, $nromed, $fechalectanterior, $lectanterior, $consumo, $fechalectultima, $lectultima, $Ruta, $fvencimientodeudores, $fcorte, $fvencimiento, $docidentidad, $nrodocidentidad, $ruc, $fechapago, $anio, $nrosolicitud, $recibon, $glosa, $codantiguo, $nrofacturacion, $nroreclamo) {
            global $tfacturacion, $conexion, $codsuc, $itemP, $meses, $nrorebaja;

            $x = 1;
            $y = 1;

            $paramae = $this->getParamae("IMPIGV", $codsuc);

            $this->SetX($x);
            $this->SetFont('Arial', 'B', 11);
            $this->Rect($x + 10, $y, 8, 2);
            $this->SetXY($x + 10, $y);
            $this->Cell(8, 2, utf8_decode("ABONOS"), 1, 0, 'C');
            $y = $this->GetY() + 2;
            $this->Rect($x + 10, $y, 8, 2);

            $this->Line($x + 12, $y + 1, 19, $y + 1);

            $this->SetXY($x + 10, $y);
            $this->Cell(3, 0.5, utf8_decode("Código"), 0, 0, 'L');

            $this->Cell(5, 0.5, $codcatastro, 0, 1, 'R');
            $this->SetX($x + 10);

            $this->Cell(3, 0.5, utf8_decode("Inscripción"), 0, 0, 'L');
            $this->Cell(5, 0.5, $codantiguo, 0, 1, 'R');


            $this->SetFont('Arial', '', 10);


            $this->SetX($x + 10);

            $this->Cell(4, 0.5, "PERIODO DE EMISION", 1, 0, 'L');
            $this->Cell(4, 0.5, "NUMERO DE RECIBO", 1, 1, 'R');

            $this->SetFont('Arial', 'B', 11);
            $this->SetX($x + 10);
            $this->Cell(4, 0.5, $facturacion, 1, 0, 'R');
            $this->Cell(4, 0.5, $recibo, 0, 1, 'R');

            $this->Ln(0.1);
            $this->SetFont('Arial', '', 10);


            $this->Cell(3, 0.5, "Nombre", 0, 0, 'L');
            $this->Cell(0.5, 0.5, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(5, 0.5, utf8_decode(strtoupper($propietario)), 0, 1, 'L');
            $this->SetFont('Arial', '', 10);

            $this->Cell(3, 0.5, "Direccion", 0, 0, 'L');
            $this->Cell(0.5, 0.5, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(5, 0.5, $direccion, 0, 1, 'L');

            $this->SetFont('Arial', '', 10);
            $this->Cell(3, 0.5, "Ruc", 0, 0, 'L');
            $this->Cell(0.5, 0.5, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(4, 0.3, $ruc, 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Cell(2, 0.5, "Ruta : ", 0, 0, 'L');
            $this->Cell(3, 0.4, $Ruta, 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Cell(2, 0.5, "Tarifa : ", 0, 0, 'L');
            $this->Cell(4, 0.4, $tarifa, 0, 0, 'L');

            $this->SetFont('Arial', 'B', 10);
            $this->Ln(1);

            $y = $this->GetY();
            $this->Rect($x, $y + 0.5, 18, 7.5);

            $subtotalo = 0;
            $subtotalr = 0;
            $interesMes = 0;
            $interesMesr = 0;
            $redondeo = 0;
            $igvMes = 0;
            $totrecibo = 0;

            $this->Cell(10, 0.5, "DESCRIPCION DE CONCEPTOS", 0, 0, 'L');
            $this->Cell(4, 0.5, "IMPORTE ORIGINAL", 0, 0, 'C');
            $this->Cell(4, 0.5, "REBAJA", 0, 1, 'C');

            $this->SetFont('Arial', '', 10);
			
            $Sql = "SELECT d.codconcepto,c.descripcion,sum(d.importe) as monto,
                c.categoria
                FROM facturacion.detfacturacion d
                inner join facturacion.conceptos as c on (d.codconcepto=c.codconcepto and d.codemp=c.codemp and d.codsuc=c.codsuc)
                where  d.codemp=1 AND d.codsuc=".$codsuc." AND d.nrofacturacion=".$nrofacturacion." AND d.nroinscripcion=".$nroinscripion."
                group by d.codconcepto,c.descripcion,c.categoria
                 /*having SUM(d.imprebajado)> 0*/
                order by d.codconcepto; ";
            $Consulta2 = $conexion->query($Sql);
            $itemsD2 = $Consulta2->fetchAll();
            foreach ($itemsD2 as $rowD2) 
            {
                 $Monto = $rowD2["monto"];
                 $subtotalo += $Monto;
                $this->Cell(10, 0.5, str_pad($rowD2["codconcepto"], 5, "0", STR_PAD_LEFT)." ".strtoupper($rowD2["descripcion"]), 0, 0, 'L');
                $this->Cell(4, 0.5, number_format($Monto, 2), 0, 0, 'R');

            //OBTENER FACTURACION DE ESE PAGO
            $Sql = "SELECT d.codconcepto,c.descripcion,sum(d.imporiginal) as monto,SUM(d.imprebajado) as montor,
                c.categoria,d.nrodocumento ,d.seriedocumento,d.anio,d.mes
                FROM facturacion.detrebajas d
                inner join facturacion.conceptos as c on (d.codconcepto=c.codconcepto and d.codemp=c.codemp and d.codsuc=c.codsuc)
                where  d.codemp=1 AND d.codsuc=".$codsuc." and d.nrorebaja=".$nrorebaja." 
                AND d.nrofacturacion=".$nrofacturacion." AND d.codconcepto=".$rowD2['codconcepto']."
                group by d.codconcepto,c.descripcion,c.categoria,d.nrodocumento ,d.seriedocumento,d.anio,d.mes
                 /*having SUM(d.imprebajado)> 0*/
                order by d.codconcepto; ";
            $Consulta = $conexion->query($Sql);
            $itemsD = $Consulta->fetchAll();

            foreach ($itemsD as $rowD) {
                $Monto = $rowD["monto"];
                $Montor = $rowD["montor"];
                //if(/*$rowD["categoria"]!=3 &&*/ $rowD["categoria"]!=4 /*&& $rowD["categoria"]!=7*/)//pintar redondeo
                //{
               // $this->Cell(10, 0.5, str_pad($rowD["codconcepto"], 5, "0", STR_PAD_LEFT)." ".strtoupper($rowD["descripcion"]), 0, 0, 'L');
               // $this->Cell(4, 0.5, number_format($Monto, 2), 0, 0, 'R');
                $this->Cell(3, 0.5, number_format($Montor, 2), 0, 0, 'R');

               // $subtotalo += $Monto;
                $subtotalr += $Montor;
                //}
                /* if($rowD["categoria"]==3){$interesMes += $Monto;$interesMesr += $Montor;}
                  if($rowD["categoria"]==7){$redondeo	  += $Monto;}
                  if($rowD["categoria"]==4){$igvMes	  += $Monto;} */
            }
            $this->Ln();
        }
		
            $nrodocumento = $rowD['nrodocumento'];
            $seriedocumento = $rowD['seriedocumento'];
            $aniorecibo = $rowD['anio'];
            $mesrecibo = $rowD['mes'];
            /* if($interesMes>0)
              {
              $this->Cell(10, 0.5,$rowD["codconcepto"]." "."INTERESES Y MORA",0,0,'L');
              $this->Cell(3, 0.5,number_format($interesMes,2),0,1,'R');
              $this->Cell(3, 0.5,number_format($interesMes,2),0,1,'R');
              } */

            $subtotalo = $subtotalo + $interesMes;
            $this->SetFont('Arial', '', 10);
			
            $this->SetY(13.5);
            $this->Cell(10, 0.5, 'Saldo de la Cuenta Corriente: '.number_format($this->CuentaCorriente($codsuc,$nroinscripion), 2), 0, 0, 'L');
            $this->Cell(4, 0.5, number_format($subtotalo, 2), 0, 0, 'R');
            $this->Cell(3, 0.5, number_format($subtotalr, 2), 0, 1, 'R');


            $this->SetFont('Arial', 'B', 12);
            $this->Cell(7, 0.5, " ", 0, 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell(7, 0.5, " ", 0, 0, 'R');
            $this->Cell(3, 0.5, " ", 0, 1, 'R');

            //$this->Cell(14, 0.5,"Redondeo",0,0,'R');
            //$this->Cell(3, 0.5,number_format($redondeo,2),0,1,'R');
            $this->SetFont('Arial', 'B', 14);

            $totrecibo = $meses["deuda"] + $igvmes + ($redondeo) + $subtotMes;
            $this->Cell(14, 0.5, "TOTAL NOTA ", 0, 0, 'R');
            $this->Cell(3, 0.5, "S/.".str_pad(number_format($subtotalr, 2), 10, "*", STR_PAD_LEFT), 0, 1, 'R');

            $this->Ln(0.2);
            $this->SetFont('Arial', '', 10);
            $this->Cell(14, 0.5, "LIQUIDACION DE PAGO", 0, 0, 'R');
            $this->SetFont('Arial', 'B', 14);
            $this->Cell(3, 0.5, "S/.".str_pad(number_format($subtotalo - $subtotalr, 2), 10, "*", STR_PAD_LEFT), 0, 1, 'R');
			
			

            $this->SetFont('Arial', 'B', 14);
            $this->Ln(0.2);
            $this->Cell(12, 0.5, "SON : ".CantidadEnLetra($subtotalo - $subtotalr)." NUEVOS SOLES", 0, 1, 'L');

            $this->Rect($x, 16.4, 18, 2.1);
            $this->SetFont('Arial', '', 12);
            $this->Cell(0, 0.5, 'F/R '.$anio." - ".$nroreclamo, 0, 1, 'R');
            $this->Cell(8, 0.5, "Solicitud de Servicio: ".str_pad($nrosolicitud, 8, "0", STR_PAD_LEFT), 0, 0, 'R');
            $this->Cell(0, 0.5, "Nro. de Recibo: ".$seriedocumento."-".$nrodocumento."-".$meses[$mesrecibo]."-".$aniorecibo, 0, 0, 'R');
            $this->Rect($x, 17.5, 18, 1);


            $this->SetY(17.8);
            $this->SetFont('Arial', 'B', 12);
            $this->Cell(5, 0.5, 'FECHA DE EMISION', 0, 0, 'L');
            $this->SetFont('Arial', '', 12);
            $this->Cell(4, 0.5, $femision, 0, 0, 'L');
            $this->SetFont('Arial', 'B', 12);
            $this->Cell(6, 0.5, 'ULTIMO DIA DE PAGO', 0, 0, 'L');
            $this->SetFont('Arial', '', 12);
            $this->Cell(5, 0.5, $femision, 0, 0, 'L');

            $this->Ln(0.1);
            $this->SetFont('Arial', 'B', 16);
            $this->Ln(0.1);
            $this->Cell(10, 0.5, '', 0, 0, 'L');
            $this->Cell(8, 0.5, $fechacorte, 0, 0, 'R');
            $this->Cell(3, 0.5, "", 0, 0, 'C');
            $this->Cell(3, 0.5, "", 0, 1, 'C');



            $this->SetFont('Arial', '', 9);
            $this->SetY(23.5);

            $this->Cell(15.5, 0.5, $codcatastro, 0, 0, 'R');
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(4, 0.5, $codantiguo, 0, 1, 'C');
            $this->Cell(15, 0.5, utf8_decode(strtoupper($propietario)), 0, 1, 'R');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(8, 0.5, "LIQUIDACION DE PAGO", 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Cell(0, 0.5, "Nro. de Recibo: ".$seriedocumento."-".$nrodocumento."-".$meses[$mesrecibo]."-".$aniorecibo, 0, 1, 'R');

            $this->Rect($x, 25, 18.1, 1);

            $this->SetFont('Arial', '', 10);

            $hc = 0.5;
            $this->Cell(6, $hc, "FECHA DE EMISION", 1, 0, 'C');
            $this->Cell(6, $hc, "ULTIMO DIA DE PAGO", 1, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(3, $hc, $facturacion, 'B', 0, 'L');
            $this->Cell(3, $hc, $recibo, 'B', 1, 'R');

            $this->Cell(6, $hc, $femision, 'R', 0, 'C');
            $this->Cell(6, $hc, $femision, 'R', 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell(3, $hc, "TOTAL A PAGAR ", 'B', 0, 'L');
            $this->SetFont('Arial', 'B', 12);
            $this->Cell(3, $hc, "S/.".str_pad(number_format($subtotalo - $subtotalr, 2), 10, "*", STR_PAD_LEFT), 'B', 1, 'R');
            $this->Cell(18, $hc, "SON : ".CantidadEnLetra($subtotalo - $subtotalr)." NUEVOS SOLES", 0, 1, 'L');
            $this->SetFont('Arial', 'B', 10);
            $this->SetY(26.5);
            $this->SetFont('Arial', 'B', 18);
            $this->Cell(0, 0.5, "*".$recibon."*", 0, 1, 'C');
        }

        function Footer() {

        }

    }

    $codsuc = $_GET["codsuc"];
    $codemp = 1;

    $nrofacturacion = $_SESSION["nrofacturacion"] - 1;
    $nrorebaja = $_GET["Id"];
    //$codciclo=1;
    
    $Sql = "SELECT c.nroinscripcion,  c.conreclamo,  c.nroreclamo,  c.fechaemision,  c.glosa,  c.fechareg,  c.creador,  
        c.estareg, c.anio,  c.mes
        FROM facturacion.cabrebajas c
        WHERE c.codemp=".$codemp." AND c.codsuc=".$codsuc." AND c.nrorebaja=".$nrorebaja;

    $result = $conexion->prepare($Sql);
    $result->execute(array());
    $row = $result->fetch();
    $fechapago = $row["fechareg"];
    $nroinspeccion = $row["nroinspeccion"];
    $fechaemision = $row["fechareg"];
    $nroinscripcion = $row["nroinscripcion"];
    $glosa = $row["glosa"];
    $anio = $row["anio"];
    $nroreclamo = $row["nroreclamo"];

 
    
    $objReporte = new clsImpresionDuplicado('L', 'cm', array(28.00, 20.00));
    //con el numero de inspecion verificare si es un usuario nuevo
    //DIRECTO SIN SERVICIO
    $Sql = "SELECT upper(clie.propietario) AS propietario, upper(tipdoc.abreviado), clie.nrodocumento,
        upper(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle || ' - ' || zo.descripcion ) as direccion,
        clie.nrocalle,zo.descripcion as zona,
        ".$objReporte->getCodCatastral("clie.").", clie.nroinscripcion, clie.nromed, clie.ruc, clie.codciclo,
        clie.codrutlecturas, upper(tar.nomtar) as categoria, clie.codantiguo
        from catastro.clientes as clie 
        inner join public.tipodocumento as tipdoc on(clie.codtipodocumento=tipdoc.codtipodocumento)
        inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
        inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
        inner join admin.zonas as zo on(clie.codemp=zo.codemp AND clie.codsuc=zo.codsuc AND clie.codzona=zo.codzona)
        inner join facturacion.tarifas as tar on(clie.catetar=tar.catetar)  AND (clie.codemp=tar.codemp)  AND (clie.codsuc=tar.codsuc) 
        where clie.codemp=".$codemp." AND clie.codsuc=".$codsuc." AND clie.nroinscripcion=".$nroinscripcion;
    $consulta = $conexion->prepare($Sql);
    $consulta->execute(array());
    $items = $consulta->fetch();

    $codciclo = $items["codciclo"];
    $direccion = $items["direccion"];
    $Ruta = $items["codrutlecturas"];
    $tarifa = $items["categoria"];
    $sqlP = "SELECT anio,mes
        from facturacion.periodofacturacion where codsuc=? and codciclo=? and nrofacturacion=?";

    $consultaP = $conexion->prepare($sqlP);
    $consultaP->execute(array($codsuc, $codciclo, $nrofacturacion));
    $itemP = $consultaP->fetch();
	
    $facturacion = $meses[$row["mes"]];
    $codantiguo = $items['codantiguo'];	//$objReporte->CodUsuario($codsuc, $nroinscripcion);

    //AGRUPAR POR RECIBOS REBAJADOS
    $Sql = "SELECT nrofacturacion,coddocumentoabono,seriedocumentoabono,nrodocumentoabono
        FROM facturacion.detrebajas d
        where  d.codemp=1 AND d.codsuc=".$codsuc." and d.nrorebaja=".$nrorebaja."
        group by nrofacturacion,coddocumentoabono,seriedocumentoabono,nrodocumentoabono
        order by nrofacturacion; ";
    $ConsultaX = $conexion->query($Sql);
    $itemsDX = $ConsultaX->fetchAll();
    foreach ($itemsDX as $rowDX) {

        $Sql = "SELECT abreviado
            FROM reglasnegocio.documentos d 
            WHERE codsuc=".$codsuc." AND coddocumento=".$rowDX['coddocumentoabono'];
        $result = $conexion->prepare($Sql);
        $result->execute(array());
        $rowX = $result->fetch();
        $recibo = $rowX['abreviado']." ".$rowDX['seriedocumentoabono']."-".str_pad($rowDX['nrodocumentoabono'], 8, "0", STR_PAD_LEFT);
        $recibon = $rowDX['seriedocumentoabono']."-".str_pad($rowDX['nrodocumentoabono'], 8, "0", STR_PAD_LEFT);
        $objReporte->AddPage();
        $objReporte->SetAutoPageBreak(true, 1);


        $objReporte->Contenido($nroinscripcion, $items["codcatastro"], $facturacion, $recibo, 
            $objReporte->DecFecha($fechaemision), $items['propietario'], $direccion, $items["descripcion"], 
                $items["tipofacturacion"], $tarifa, $items["nromed"], $objReporte->DecFecha($items["fechalectanterior"]), 
                $items["lecturaanterior"], $items["consumo"], $objReporte->DecFecha($items["fechalectultima"]), 
                $items["lecturaultima"], $Ruta, $objReporte->DecFecha($itemP["fechavencdeudores"]), 
                $objReporte->DecFecha($itemP["fechacorte"]), $objReporte->DecFecha($itemP["fechavencnormal"]), 
                $items["docidentidad"], $items["nrodocidentidad"], $items['ruc'], $objReporte->DecFecha($fechapago), 
                $anio, $nroinspeccion, $recibon, $glosa, $codantiguo, $rowDX['nrofacturacion'], $nroreclamo);
    }
    $objReporte->Output();

?>