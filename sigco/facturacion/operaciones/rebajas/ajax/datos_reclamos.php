<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../config.php");

	$nroreclamo = $_POST["nroreclamo"];
	$codsuc 	= $_POST["codsuc"];

$sql  = "SELECT d.nroreclamo,cr.descripcion as reclamo,r.nroinscripcion,c.propietario,d.observacion, ";
$sql .= "c.codciclo,r.correlativo,c.codantiguo ";
$sql .= "FROM reclamos.detalle_reclamos AS d ";
$sql .= "	JOIN reclamos.reclamos AS r ON (d.codemp=r.codemp AND d.codsuc=r.codsuc AND d.nroreclamo=r.nroreclamo) ";
$sql .= "	JOIN reclamos.conceptosreclamos AS cr ON (cr.codconcepto=r.codconcepto) ";
$sql .= "	JOIN catastro.clientes AS c ON (r.codemp=c.codemp AND r.codsuc=c.codsuc AND r.nroinscripcion=c.nroinscripcion) ";
$sql .= "WHERE d.codemp=1 AND d.nroreclamo=:nroreclamo AND d.codsuc=:codsuc AND d.codestadoreclamo=4 AND d.fin_reclamo=1 AND d.fundado=1";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":nroreclamo"=>$nroreclamo,":codsuc"=>$codsuc));
	$row = $consulta->fetch();

	$t  = $row["nroreclamo"]."|".$row["reclamo"]."|".$row["nroinscripcion"]."|".$row["propietario"]."|".$row["observacion"]."|".$row["codciclo"];

	$sqlM   = "SELECT meses.nrofacturacion,upper(doc.abreviado),meses.nrodocumento, ";
	$sqlM  .= "	meses.anio,meses.mes,upper(tipd.descripcion),meses.categoria,meses.codtipodeuda, ";
	$sqlM  .= "	sum(meses.imprebajado),sum(meses.imporiginal) as original ";
	$sqlM  .= "FROM reclamos.mesesreclamados as meses ";
	$sqlM  .= "	JOIN reglasnegocio.documentos as doc ON(meses.coddocumento=doc.coddocumento) AND (meses.codsuc=doc.codsuc) ";
	$sqlM  .= "	JOIN public.tipodeuda as tipd ON(meses.codtipodeuda=tipd.codtipodeuda) ";
	$sqlM  .= "WHERE meses.nroreclamo=:nroreclamo AND meses.codemp=1 AND meses.codsuc=:codsuc ";
	$sqlM  .= "GROUP BY meses.codtipodeuda,meses.nrofacturacion,doc.abreviado,meses.nrodocumento,meses.anio, ";
	$sqlM  .= "	meses.mes,meses.categoria,tipd.descripcion";

	$consultaM = $conexion->prepare($sqlM);
	$consultaM->execute(array(":nroreclamo"=>$nroreclamo,":codsuc"=>$codsuc));
	$itemsM = $consultaM->fetchAll();
	//var_dump($consultaM->errorInfo());
	$tr       = "";
	$count    = 0;
	$Rebajado = 0;
	$Total    = 0;
	$apagar   = 0;
	foreach($itemsM as $rowM)
	{
		$count++;
		$Rebajado+=$rowM[8];
		$Total+=$rowM['original'];
		$apagar+=$rowM['original']-$rowM[8];
		$tr .= "<tr style='background-color:#FFFFFF; color:#000000; font-size:11;'>";
		$tr .= "<td align='center' >";
		$tr .= "<input type='hidden' name='nrofacturacion".$count."' id='nrofacturacion".$count."' value='".$rowM[0]."' />";
		$tr .= "<input type='hidden' name='categoria".$count."' id='categoria".$count."' value='".$rowM[6]."' />";
		$tr .= "<input type='hidden' name='codtipodeuda".$count."' id='codtipodeuda".$count."' value='".$rowM[7]."' />";
		$tr .= "<input type='hidden' name='impz".$count."' id='impz".$count."' value='".$rowM[8]."' />";
		$tr .= "<input type='hidden' name='imprebajado".$rowM[0]."' value='".$rowM[8]."' />";
		$tr .= $rowM[0];
		$tr .= "</td>";
		$tr .= "<td align='center' >".$rowM[1]."</td>";
		$tr .= "<td align='center' >".$rowM[2]."</td>";
		$tr .= "<td align='center' >".$rowM[3]."-".$meses[$rowM[4]]."</td>";
		//$tr .= "<td align='center' >".$rowM[5]."</td>";
		//$tr .= "<td align='center' >".$categoria[$rowM[6]]."</td>";
		$tr .= "<td align='right' >".number_format($rowM['original'],2)."</td>";
		$tr .= "<td align='right' >".number_format($rowM[8],2)."</td>";
		$tr .= "<td align='right' >".number_format($rowM['original']-$rowM[8],2)."</td>";
		$tr .= "</tr>";
	}

	echo $t."|".$tr."|".number_format($Rebajado,2)."|".$row["correlativo"]."|".$row["codantiguo"]."|".number_format($Total,2)."|".number_format($apagar,2);

?>
