<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../config.php");

	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];

	$sql 		 = "SELECT * FROM cobranza.view_cobranza_sin_tipodeuda ";
	$sql 		.= "WHERE codemp=1 AND codsuc=" . $codsuc . " AND nroinscripcion=" . $nroinscripcion . " AND imptotal>0 ";
	$sql 		.= "ORDER BY nrofacturacion";

	$consulta 	= $conexion->query($sql);
	$items = $consulta->fetchAll();
	//var_dump($consulta->errorInfo());
	$tr		= "";
	$count 	= 0;
	$Rebajado=0;
	$total=0;
	foreach($items as $row)
	{
		$Color = "background-color:#FFFFFF;";
		$Titulo = "";
		
		if ($row['rebaja'] <> 0)
		{
			$Color = "background-color:#FFFF00;";
			$Titulo = "Este periodo ya tiene rebaja!";
		}
		
		$count++;
		$Rebajado+=$row['imptotal'];
		$tr .= "<tr style='".$Color." color:#000000; font-size:11;' id='tr_".$count."' title='".$Titulo."'>";
		$tr .= "<td align='center' >";
		$tr .= "<input type='hidden' name='nrofacturacion".$count."' id='nrofacturacion".$count."' value='".$row['nrofacturacion']."' />";
		// $tr .= "<input type='hidden' name='categoria".$count."' id='categoria".$count."' value='".$row['cat']."' />";
		$tr .= "<input type='hidden' name='codciclo".$count."' id='codciclo".$count."' value='".$row['codciclo']."' />";
		// $tr .= "<input type='hidden' name='codtipodeuda".$count."' id='codtipodeuda".$count."' value='".$row['codtipodeuda']."' />";
		$tr .= "<input type='hidden' name='impz".$count."' id='impz".$count."' value='".$row['imptotal']."' />";
		$tr .= "<input type='hidden' name='serie".$count."' id='serie".$count."' value='".$row['serie']."' />";
		$tr3 = "<input type='hidden' name='imprebajado".$row['nrofacturacion']."' value='".$row['imptotal']."' />";
		$tr .= $row['nrofacturacion'];
		$tr .= "</td>";
		$tr .= "<td align='center' >".$row['abreviado']."</td>";
		$tr .= "<td align='center' >".$row['nrodocumento']."</td>";
		$tr .= "<td align='center' >".$row['anio']."-".$meses[$row['mes']]."</td>";
		// $tr .= "<td align='center' >".$row['deuda']."</td>";
		// $tr .= "<td align='center' >".$row['categoria']."</td>";
		$tr .= "<td align='right' ><label>".number_format($row['imptotal'],2)."</label></td>";
		$tr .= "<td align='right' ><label id='lblimptotal".$count."' >0.00</label></td>";
		$tr .= "<td align='right' ><label id='lblimptotalapagar".$count."' >0.00</label></td>";
		$tr.="<td><input type='hidden' name='imptotal".$count."' id='imptotal".$count."' value='0' />";
		$tr.="<input type='hidden' name='imptotalori".$count."' id='imptotalori".$count."' value='".$row['imptotal']."' />";
		$tr.="<input type='hidden' name='consumo".$count."' id='consumo".$count."' value='".intval($row['consumo'])."' />";
		
		if ($row['rebaja'] == 0)
		{
        	$tr.="<span onclick='cargar_from_detalle_facturacion(".$nroinscripcion.",".$row['nrofacturacion'].", \"\", \"\",".$count.");' title='Rebajar Importe' class='icono-icon-dinero'></span> </td>";
		}
		
		$tr .= "</tr>";


	}
	$total=$Rebajado;
 	$Rebajado=0;
	$data['tr']     = $tr;
	$data['count']     = $count;
	$data['rebajado']     = number_format($Rebajado,2);
	$data['total']     = number_format($total,2);

    echo json_encode($data);
?>
