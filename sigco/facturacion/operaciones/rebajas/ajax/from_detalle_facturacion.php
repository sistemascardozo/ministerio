<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../config.php");

	$codsuc			    = $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$nrofacturacion = $_POST["nrofacturacion"];
	$categoria      = (empty($_POST["categoria"])) ? 0 : $_POST["categoria"];
	$codtipodeuda   = (empty($_POST["codtipodeuda"])) ? 0 : $_POST["codtipodeuda"];
	$index			    = $_POST["index"];
	$Todo 		      = $_POST["Todo"]?$_POST['Todo']:1;

	$style='style:"display:none;"';
	$readonly='';
	$check='checked="checked"';
	$onclick="onkeyup='recalcularmontos();'";

	if($Todo == 1)
	{
		$Todo = "";
	}
	else
 	{
		$Todo = " AND codtipoconcepto = 1 AND c.categoria <> 3 AND c.categoria <> 7 ";
		$style = '';
		$check = '';
		$readonly = 'readonly="readonly"';
		$onclick = "";
	}

	$count = 0;

	$sql  = "SELECT d.codconcepto, c.descripcion, sum(d.importe) AS imptotal, d.item, ";
	$sql .= " d.categoria, d.codtipodeuda, c.afecto_igv ";
	$sql .= "FROM facturacion.detfacturacion d ";
	$sql .= " JOIN facturacion.conceptos c ON (d.codemp = c.codemp AND d.codsuc = c.codsuc AND d.codconcepto = c.codconcepto) ";
	$sql .= "WHERE d.codsuc = " . $codsuc . " ";
	$sql .= " AND d.nroinscripcion = " . $nroinscripcion . " ";
	$sql .= " AND d.nrofacturacion = " . $nrofacturacion . " ";
	// $sql .= " AND d.categoria = ? ";
	// $sql .= " AND d.codtipodeuda = ? ";
	$sql .= $Todo." ";
	$sql .= "GROUP BY d.codconcepto, c.descripcion, d.item, d.categoria, d.codtipodeuda, c.afecto_igv ";
	$sql .= "ORDER BY d.codconcepto ";

	$consulta = $conexion->query($sql);
	$items = $consulta->fetchAll();

	$total = 0;
?>
<div style="height:300px; overflow:auto">
<table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbdetallefacturacion" rules="all" >
	<thead class="ui-widget-header" >
      <tr>
        <th width="55%" >Concepto</th>
        <th width="15%" >Importe</th>
        <th width="15%" >Rebaja</th>
        <th width="15%" >A Pagar</th>
      </tr>
   </thead>
   <tbody>
<?php
	$RedondeoC = 0;
	
	foreach($items as $row)
	{
		$count++;

		$total += $row["imptotal"];
		
		if ($row["codconcepto"] == 7 or $row["codconcepto"] == 8)
		{
			$RedondeoC = 1;
		}
?>
     <tr style="background:#FFF; color:#000;">
        <td align="left" style="padding-left:5px;">
					<input type="hidden" name="codconceptoy<?=$count?>" id="codconceptoy<?=$count?>" value="<?=$row["codconcepto"]?>" />
					<input type="hidden" id="codconceptoitem<?=$count?>" value="<?=$row["item"]?>" />
					<input type="hidden" id="codtipodeuda<?=$count?>" value="<?=$row["codtipodeuda"]?>" />
					<input type="hidden" id="categoria<?=$count?>" value="<?=$row["categoria"]?>" />
					<input type="hidden" id="afecto_igv<?=$count?>" value="<?=$row["afecto_igv"]?>" />
        	<?=strtoupper($row["descripcion"])?>
        </td>
        <td align="right" style="padding-right:5px;">
			<input type="hidden" name="impx<?=$count?>" id="impx<?=$count?>" value="<?=$row["imptotal"]?>" />
			<?=number_format($row["imptotal"],2)?>
        </td>
        <td align="center" >
        	<input type="text" name="impamort<?=$count?>" id="impamort<?=$count?>"  <?=$readonly?> <?=$onclick?> class="inputtext numeric" style="text-align:right; width:100px;" value="0.00" />
        </td>
        <td align="center">
        	<input type="text" name="impamortt<?=$count?>" id="impamortt<?=$count?>" readonly="readonly" class="inputtext numeric" style="text-align:right; width:100px;" value="0.00" />
        </td>
     </tr>
<?php 
	}
	
	if ($RedondeoC == 0)
	{
		$count++;
?>
		<tr style="background:#FFF; color:#000;">
        <td align="left" style="padding-left:5px;">
					<input type="hidden" name="codconceptoy<?=$count?>" id="codconceptoy<?=$count?>" value="7" />
					<input type="hidden" id="codconceptoitem<?=$count?>" value="97" />
					<input type="hidden" id="codtipodeuda<?=$count?>" value="1" />
					<input type="hidden" id="categoria<?=$count?>" value="0" />
					<input type="hidden" id="afecto_igv<?=$count?>" value="0" />
        	DIFERENCIA REDONDEO NEGATIVO
        </td>
        <td align="right" style="padding-right:5px;">
			<input type="hidden" name="impx<?=$count?>" id="impx<?=$count?>" value="0" />
			0.00
        </td>
        <td align="center" >
        	<input type="text" name="impamort<?=$count?>" id="impamort<?=$count?>"  <?=$readonly?> <?=$onclick?> class="inputtext numeric" style="text-align:right; width:100px;" value="0.00" />
        </td>
        <td align="center">
        	<input type="text" name="impamortt<?=$count?>" id="impamortt<?=$count?>" readonly="readonly" class="inputtext numeric" style="text-align:right; width:100px;" value="0.00" />
        </td>
     </tr>
<?php
		$count++;
?>
		<tr style="background:#FFF; color:#000;">
        <td align="left" style="padding-left:5px;">
					<input type="hidden" name="codconceptoy<?=$count?>" id="codconceptoy<?=$count?>" value="8" />
					<input type="hidden" id="codconceptoitem<?=$count?>" value="98" />
					<input type="hidden" id="codtipodeuda<?=$count?>" value="1" />
					<input type="hidden" id="categoria<?=$count?>" value="0" />
					<input type="hidden" id="afecto_igv<?=$count?>" value="0" />
        	DIFERENCIA REDONDEO POSITIVO
        </td>
        <td align="right" style="padding-right:5px;">
			<input type="hidden" name="impx<?=$count?>" id="impx<?=$count?>" value="0" />
			0.00
        </td>
        <td align="center" >
        	<input type="text" name="impamort<?=$count?>" id="impamort<?=$count?>"  <?=$readonly?> <?=$onclick?> class="inputtext numeric" style="text-align:right; width:100px;" value="0.00" />
        </td>
        <td align="center">
        	<input type="text" name="impamortt<?=$count?>" id="impamortt<?=$count?>" readonly="readonly" class="inputtext numeric" style="text-align:right; width:100px;" value="0.00" />
        </td>
     </tr>
<?php
	}
?>
      </tbody>
     <tfoot class="ui-widget-header" >
     <tr style="background:#FFF; color:#000;">
        <td align="right" >Total Facturacion ==></td>
        <td align="right" >
			<?=number_format($total,2)?>
            <input name="imptotalh" type="hidden" id="imptotalh" value="<?=$total?>" />
        </td>
        <td align="right" >&nbsp;</td>
        <td align="right" >&nbsp;</td>
     </tr>
</tfoot>

</table>
</div>
<div style="height:15px">
</div>
<div style="border:1px #000000 dashed; padding:4px;display:none" id="DivImporteRebaja" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr style="padding:4px">
    <td width="15%">Nuevo Consumo </td>
    <td width="3%" align="center">:</td>
    <td width="41%" align="center">
      <input type="text"  id="consumocalcular" class="inputtext"  onkeypress="return permite(event,'num');" size="10" />
      <input type="button" onclick="CalcularImporteConsumo();" value="Recalcular" id="">
    </td>
    <td width="41%"><label>
      Importe:
      <input type="text" name="mrebajar" id="mrebajar" class="inputtext" value="<?=$total?>" onkeyup="calcularmontos(<?=$count?>);" onkeypress="return permite(event,'num');" style="width:100px;" />
      <input type="checkbox" name="checkbox" value="checkbox" onclick="rebajatotal(this, <?=$count?>);" style="display:none"/>
      <label style="display:none">Rebaja Todo</label>
      <input type="hidden" name="todoimporte" id="todoimporte" value="0" />
      <input type="hidden" name="idx" id="idx" value="<?=$index?>" />
      <input type="hidden" name="cont_facturacion" id="cont_facturacion" value="<?=$count?>"/>
      <input type="hidden" name="nrofacturacionR" id="nrofacturacionR" value="<?=$nrofacturacion?>"/>
      <br>
      <label <?=$style?>>Ingresar Importe</label>
      <input type="checkbox" id="checkboximp" value="checkboximp" onclick="ingresarimporte();" <?=$style?> <?=$check?>/>
    </label></td>
  </tr>
</table>
</div>
<script type="text/javascript">
	function ingresarimporte()
	{
		if($("#checkboximp").attr('checked'))
		{
			cargar_from_detalle_facturacion_2();
		}
		else
		{
			cargar_from_detalle_facturacion('<?=$nroinscripcion?>', '<?=$nrofacturacion?>', '<?=$categoria?>', '<?=$codtipodeuda?>', '<?=$index?>');
		}
	}

	function cargar_from_detalle_facturacion_2()
    {
        ItemUpd = <?=$index?>;
        $.ajax({
            url: 'ajax/from_detalle_facturacion.php',
            type: 'POST',
            async: true,
            data: "nroinscripcion=<?=$nroinscripcion?>&codsuc=<?=$codsuc?>&nrofacturacion=<?=$nrofacturacion?>&categoria=<?=$categoria?>&codtipodeuda=<?=$codtipodeuda?>&index=<?=$index?>&Todo=1",
            success: function (datos)
            {
                $("#div_detalle_facturacion").html(datos)

                //$("#mrebajar").val($("#tbrebajas tbody tr#tr_" + ItemUpd + " label.ImpRebajado").text()).keyup()
                //$("#consumocalcular").val($("#tbrebajas tbody tr#tr_" + ItemUpd + " label.consumoreclamo").text());
                //$("#DivRebajar").dialog("open");
                $("#DivImporteRebaja").show();
            }
        })
    }

	function recalcularmontos()
	{
		var total = 0;
		var id = parseInt($("#tbdetallefacturacion tbody tr").length);
		var imptotalh = parseFloat($("#imptotalh").val());
		
		var ItemIgv = 0;
		var impIgv = 0;
		
		for(var i = 1; i <= id; i++)
		{
			$("#impamortt" + i).val(($("#impx" + i).val() - $("#impamort" + i).val()).toFixed(2));
			
			if ($("#afecto_igv" + i).val() == 1)
			{
				impIgv +=  parseFloat($("#impamortt" + i).val());
			}
			
			if ($("#codconceptoy" + i).val() == 5)
			{
				ItemIgv = i;
			}
		}
		impIgv = impIgv * 0.18;
		
		$("#impamortt" + ItemIgv).val(parseFloat(impIgv).toFixed(2));
		
		$("#impamort" + ItemIgv).val(($("#impx" + ItemIgv).val() - $("#impamortt" + ItemIgv).val()).toFixed(2));
		
		for(var i = 1; i <= id; i++)
		{
			t = $("#impamort" + i).val();
			total += parseFloat(t);
		}
		total = imptotalh - parseFloat(total);

		$("#mrebajar").val(parseFloat(total).toFixed(2));
	}
</script>
