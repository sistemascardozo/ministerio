<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../../../../config.php");

	$codsuc 		= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];

	$sql 		 = "SELECT * FROM cobranza.view_cobranza_sin_tipodeuda ";
	$sql 		.= "WHERE codemp = 1 AND codsuc = " . $codsuc . " AND nroinscripcion = " . $nroinscripcion . " AND imptotal > 0 ";
	$sql 		.= "ORDER BY nrofacturacion ";

	$consulta 	= $conexion->prepare($sql);
	$consulta->execute();
	$items = $consulta->fetchAll();

	// echo "<pre>";	var_dump($items);	echo "<pre>"; exit;

	$tr		= "";
	$count 	= 0;

	// echo "<pre>";	var_dump($items);	echo "</pre>"; exit;

	foreach($items as $row)
	{
		$count++;

		$tr .= "<tr style='background-color:#FFFFFF'>";
		$tr .= "<td align='center'>";
		$tr .= "<input type='hidden' name='nrofact".$count."' id='nrofact".$count."' value='".$row["nrofacturacion"]."' />";
		$tr .= $row["nrofacturacion"];
		$tr .= "</td>";
		$tr .= "<td align='center'>";
		$tr .= "<input type='hidden' name='doc".$count."' id='doc".$count."' value='".$row["abreviado"]."' />";
		$tr .= $row["abreviado"];
		$tr .= "</td>";
		$tr .= "<td align='center'>";
		$tr .= "<input type='hidden' name='nrodoc".$count."' id='nrodoc".$count."' value='".$row["nrodocumento"]."' />";
		$tr .= $row["nrodocumento"];
		$tr .= "</td>";
		$tr .= "<td align='center'>";
		$tr .= "<input type='hidden' name='anio".$count."' id='anio".$count."' value='".$row["anio"]."-".$meses[$row["mes"]]."' />";
		$tr .= "<input type='hidden' name='anioz".$count."' id='anioz".$count."' value='".$row["anio"]."' />";
		$tr .= "<input type='hidden' name='mesz".$count."' id='mesz".$count."' value='".$row["mes"]."' />";
		$tr .= $row["anio"]."-".$meses[$row["mes"]];
		$tr .= "</td>";
		// $tr .= "<td align='center'>";
		// $tr .= "<input type='hidden' name='tipd".$count."' id='tipd".$count."' value='".$row["deuda"]."' />";
		// $tr .= $row["deuda"];
		// $tr .= "</td>";
		// $tr .= "<td align='center'>";
		// $tr .= "<input type='hidden' name='cat".$count."' id='cat".$count."' value='".$row["categoria"]."' />";
		// $tr .= $row["categoria"];
		// $tr .= "</td>";
		$tr .= "<td align='right'>";
		$tr .= "<input type='hidden' name='imp".$count."' id='imp".$count."' value='".$row["imptotal"]."' />";
		$tr .= number_format($row["imptotal"],2);
		$tr .= "</td>";
		$tr .= "</tr>";
	}

	echo $tr;

?>
