// JavaScript Document
function BuscarUsuario()
{
	if($("#conreclamo").val() == 1)
	{
		alert("No Puede Buscar Usuario por que la opcion de reclamos esta activada")
		return
	}

	object = "codcliente";

	AbrirPopupBusqueda('../../../catastro/operaciones/actualizacion/?Op=5', 1150, 450);
}

function Buscarreclamos()
{
	if($("#conreclamo").val() == 0)
	{
		alert("No Puede Buscar Reclamos por que la opcion de reclamos esta desactivada")
		return
	}

	object="reclamos";

	AbrirPopupBusqueda('../../../reclamos/operaciones/ingresos/?Op=5', 1200, 450);
}

function Recibir(id)
{
	if(object=="codcliente")
	{
		datos_usuarios(id)
		cargar_facturacion(id)
	}
	if(object=="reclamos")
	{
		QuitaRow("tbfacturacion")
		//QuitaRow("tbrebajas")
		cargar_datos_reclamos(id)
	}
}
function datos_usuarios(id)
{
	$("#nroinscripcion").val(id)
	$.ajax({
		 url:'../../../../ajax/clientes.php',
		 type:'POST',
		 async:true,
         dataType: 'json',
		 data:'nroinscripcion='+id+'&codsuc='+codsuc,
		 success:function(datos){
		 	cargar_datos_periodo_facturacion(datos.codciclo)
		 	cargar_facturacion_sin(datos.nroinscripcion);
			$("#codciclo").val(datos.codciclo)
		//	$("#codcliente").val(datos.codcliente)
			$("#propietario").val(datos.propietario)
			//$("#direccion").val(datos.direccion)
			$("#codantiguo").val(datos.codantiguo)


		 }
	})
}
function MostrarDatosUsuario(datos)
{

	if($("#conreclamo").val()==1)
	{
		Msj($("#codantiguo"),"No Puede Buscar Usuario por que la opcion de reclamos esta activada")
		return false
	}

	cargar_datos_periodo_facturacion(datos.codciclo)
	cargar_facturacion_sin(datos.nroinscripcion);
	cargar_facturacion(datos.nroinscripcion);
	$("#codciclo").val(datos.codciclo)
	$("#nroinscripcion").val(datos.nroinscripcion)
		//	$("#codcliente").val(datos.codcliente)
	$("#propietario").val(datos.propietario)
	//$("#direccion").val(datos.direccion)
	//$("#codantiguo").val(datos.codantiguo)

}
function cargar_facturacion_sin(nroinscripcion)
{
	$.ajax({
		 url:'ajax/ver_facturacion_sin.php',
		 type:'POST',
		 async:true,
		 dataType: 'json',
		 data:'nroinscripcion='+nroinscripcion+'&codsuc='+codsuc,
		 success:function(datos){

			$("#tbrebajas tbody").html(datos.tr)
			$("#TotalRebajado").html(datos.rebajado)
			$("#TotalOriginal").html(datos.total)
			$("#NroItems").val(datos.count)
		 }
	})
}
function cargar_facturacion(nroinscripcion)
{
	$.ajax({
		 url:'ajax/ver_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'nroinscripcion='+nroinscripcion+'&codsuc='+codsuc,
		 success:function(datos){
			$("#tbfacturacion tbody").html(datos)
		 }
	})
}
function cargar_datos_reclamos(nroreclamo)
{
	$.ajax({
		 url:'ajax/datos_reclamos.php',
		 type:'POST',
		 async:true,
		 data:'nroreclamo='+nroreclamo+"&codsuc="+codsuc,
		 success:function(datos){
			var r=datos.split("|")

			$("#nroreclamo").val(nroreclamo)
			$("#reclamo").val(r[1])
			$("#nroinscripcion").val(r[2])
			$("#propietario").val(r[3])
			$("#motivoreclamo").val(r[4])
			$("#correlativoreclamo").val(r[8])
			$("#codantiguo").val(r[9])
			cargar_facturacion(r[2]);
			cargar_datos_periodo_facturacion(r[5])

			$("#tbrebajas tbody").html(r[6])
			$("#TotalRebajado").html(r[7])
			$("#TotalOriginal").html(r[10])
			$("#TotalaPagar").html(r[11])

		 }
	})
}
function QuitaRow(obj)
{
		var miTabla 	= document.getElementById(obj);
		var numFilas 	= miTabla.rows.length;

		for(i=1;i<=numFilas;i++)
		{
			try
			{
				miTabla.deleteRow(1);
			}catch(exp)
			{

			}
		}
}
function cargar_datos_periodo_facturacion(codciclo)
{
	$.ajax({
		 url:urldir+'ajax/periodo_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&codciclo='+codciclo,
		 success:function(datos){
			var r=datos.split("|")

			$("#anio").val(r[1])
			$("#mes").val(r[3])
		 }
    })
}
function vReclamo(obj)
{
	if(obj.checked)
	{
		$(".ConReclamo").show()
	}
	else
	{
		$(".ConReclamo").hide()
	}
	$("#tbfacturacion tbody").html('')
	$("#tbrebajas tbody").html('')
}

	function cargar_from_detalle_facturacion(nroinscripcion, nrofacturacion, categoria, codtipodeuda, idx)
    {
        ItemUpd = idx;
        $.ajax({
            url: 'ajax/from_detalle_facturacion.php',
            type: 'POST',
            async: true,
            data: 'nroinscripcion=' + nroinscripcion + '&codsuc=' + codsuc + '&nrofacturacion=' + nrofacturacion +
                    '&categoria=' + categoria + '&codtipodeuda=' + codtipodeuda + '&index=' + idx,
            success: function (datos)
            {
                $("#div_detalle_facturacion").html(datos)

                $("#mrebajar").val($("#imptotal"+ItemUpd).val()).keyup()
                $("#consumocalcular").val($("#consumo"+ItemUpd).val());
                $("#DivRebajar").dialog("open");
                $("#DivImporteRebaja").show();
            }
        })
    }

	var dato = new Array();

	function calcularmontos(count)
	{
		var mRedodento 	= 0;
		var codconcepto	= 0;
		var mrebaja		= $("#mrebajar").val();
		var imptotalh   = parseFloat($("#imptotalh").val());

		var flag = true;
		var SumReb = 0;

		mrebaja = imptotalh - parseFloat(mrebaja);

		if(mrebaja == "")
		{
			mrebaja = 0;
		}

		if (mrebaja == imptotalh)
		{
			flag = false;
		}

		mrebaja = parseFloat(mrebaja).toFixed(2);

		var auto = parseFloat(mrebaja) - parseFloat(mRedodento);

		for(i = 1; i <= count; i++)
		{
			codconcepto = $("#codconceptoy" + i).val();

			descuento = $("#impx" + i).val();

			if (codconcepto == 1 && flag)//Agua
			{
				descuento -= dato[2];
			}
			else if(codconcepto == 2 && flag)//Desague
			{
				descuento -= dato[3];
			}
			else if (codconcepto == 5 && flag)//IGV
			{
				descuento -= dato[4];
			}
			else if (codconcepto == 6 && flag)//Caargo Fijo
			{
				descuento -= dato[1];
			}
			else
			{
				if(eval(auto < descuento))
				{
					descuento = auto;
				}
			}

			SumReb += parseFloat(descuento);

			//if(eval(auto >= $("#impx" + i).val()))
//			{
				$("#impamort" + i).val(parseFloat(descuento).toFixed(2));
				$("#impamortt" + i).val(($("#impx" + i).val() - parseFloat(descuento)).toFixed(2));

				auto = parseFloat(auto - descuento);
			//}
//			else
//			{
//				$("#impamort" + i).val(parseFloat(auto).toFixed(2));
//				$("#impamortt" + i).val(($("#impx" + i).val() - parseFloat(auto)).toFixed(2));
//
//				auto = parseFloat(auto - descuento).toFixed(2);
//			}
		}
		//alert(SumReb);
		recalcularmontos()
	}

	function CalcularImporteConsumo()
	{
		var consumo = $("#consumocalcular").val();

		if(eval(consumo < 0))
		{
			Msj($("#consumocalcular"), 'El Consumo Ingesado no es Valido');

			return false;
		}

		$.ajax({
			url:'../../../reclamos/operaciones/ingresos/ajax/calcularimporte.php',
			type:'POST',
			async:true,
			data:'codsuc=' + codsuc + '&consumo=' + consumo + '&nroinscripcion=' + $("#nroinscripcion").val() + '&codciclo=' + $("#codciclo").val() +
		 		'&nrofacturacion=' + $('#nrofacturacionR').val(),
			success:function(datos){
				dato = datos.split('|');

				$("#mrebajar").val(dato[0]).keyup();
			}
		})
	}

function guardarrebajasres()
    {
        var consumocalcular = $("#consumocalcular").val();
        consumocalcular = parseFloat(consumocalcular);
        if (eval(consumocalcular < 0))
        {
            alert('El Consumo Ingesado no es Valido')
            return false;
        }

        var vrebaja = $("#mrebajar").val();
        vrebaja = parseFloat(vrebaja);
        var imptotalh   =  parseFloat($("#imptotalh").val())
        vrebaja = imptotalh - parseFloat(vrebaja)

        if (eval(vrebaja < 0))
        {
            alert('El Monto Ingesado no es Valido')
            return false;
        }
        else
        {
            var imptH = Round($("#imptotalh").val(), 2)
            var imptD = Round($("#mrebajar").val(), 2)
            var index = $("#idx").val()

            if(vrebaja > imptH)
            {
                alert("El Monto a Rebajado no puede ser Mayor al Monto a Rabajar")
                return
            }

            var cad = ""
            var leca = $("#lecturaanterior" + index).val();
            var lecreb = parseFloat(leca) + parseFloat(consumocalcular)
            for (i = 1; i <= $("#cont_facturacion").val(); i++)
            {

                cad = cad + "nrofacturacion" + i + "=" + $("#nrofacturacion" + ItemUpd).val() + "&codconcepto" + i + "=" + $("#codconceptoy" + i).val();
                cad = cad + "&imporiginal" + i + "=" + str_replace($("#impx" + i).val(), ",", "") + "&imprebajado" + i + "=" + str_replace($("#impamort" + i).val(), ",", "");
                cad = cad + "&codtipodeuda" + i + "=" + $("#codtipodeuda" + i).val() + "&categoria" + i + "=" + $("#categoria" + i).val();
                cad = cad + "&nrodocumento" + i + "=" + $("#nrodoc" + ItemUpd).val() + "&anio" + i + "=" + $("#anioz" + ItemUpd).val() + "&mes" + i + "=" + $("#mesz" + ItemUpd).val();
                cad = cad + "&codciclo" + i + "=" + $("#codciclo" + ItemUpd).val() + "&"
                cad = cad + "&lecant" + i + "=" + $("#lecturaanterior" + index).val() + "&"
                cad = cad + "&lecact" + i + "=" + $("#lecturaultima" + index).val() + "&"
                cad = cad + "&consumo" + i + "=" + $("#consumo" + index).val() + "&"
                cad = cad + "&lecturarec" + i + "=" + lecreb + "&"
                cad = cad + "&seriedocumento"+i+"="+$("#serie"+index).val()+"&"
                cad = cad + "&consumorec" + i + "=" + consumocalcular + "&"
                cad = cad + "&codconceptoitem" + i + "=" + $("#codconceptoitem" + i).val()+ "&"
            }

            $.ajax({
                url: '../../../reclamos/operaciones/ingresos/clase/proceso.php',
                type: 'POST',
                async: true,
                data: cad + "count=" + $("#cont_facturacion").val(),
                success: function (datos) {
                    $("#lblimptotal" + ItemUpd).text(parseFloat(vrebaja).toFixed(1));

                 	$("#imptotal"+ItemUpd).val(vrebaja)

                 	$("#lblimptotalapagar"+ItemUpd).html($("#mrebajar").val())
                    CalcularTotalRebajado()
                    $("#DivRebajar").dialog('close');
                }
            })
        }
    }
    function CalcularTotalRebajado()
    {
        var id = parseInt($("#tbrebajas tbody tr").length)
        var Total = 0
        var Importe = 0
        for (var i = 1; i <= id; i++)
        {
            Importe = $("#imptotal" + i ).val()
            Importe = str_replace(Importe, ',', '');
            Total += parseFloat(Importe);
        }
        var TotalOriginal = $("#TotalOriginal" ).html()
        TotalOriginal = str_replace(TotalOriginal, ',', '');
        $("#TotalRebajado").html(parseFloat(Total).toFixed(2))
        var apagar=parseFloat(TotalOriginal) - parseFloat(Total)
        $("#TotalaPagar").html(parseFloat(apagar).toFixed(2))
    }



function open_update_importe_item(idx)
{
	$("#ItemUpdateImporte").val(idx)

	$("#UpdateImporteColateralImporte").val(parseFloat($("#imptotal"+idx).val()).toFixed(2))
	$("#UpdateImporteColateralOriginal").val($("#imptotal"+idx).val())
	$( "#dialog-form-update-importe" ).dialog( "open" );
	$("#UpdateImporteColateralImporte").focus().select();
}
function ValidarUpdateImporte(e)
	{
		if (VeriEnter(e) )
	      {
	      	ValidarUpdateImporteOk()

	      e.returnValue = false;
	    }
	}
	function ValidarUpdateImporteOk()
	{
		var importe = str_replace($("#UpdateImporteColateralImporte").val(), ',', '')
		if(importe==0.00 || importe==0 || importe=="" || isNaN(importe))
		{
			Msj($("#UpdateImporteColateralImporte"),"Ingrese Importe Valido")
			return;
		}
		var idx = $("#ItemUpdateImporte").val()
		var importeOriginal = str_replace($("#imptotalori"+idx).val(), ',', '')
		if(parseFloat(importe)>parseFloat(importeOriginal))
		{
			Msj($("#UpdateImporteColateralImporte"),"No puede ser mayor a "+$("#imptotalori"+idx).val())
			return;
		}


		$("#imptotal"+idx).val(parseFloat(importe).toFixed(2))
		$("#lblimptotal"+idx).html(parseFloat(importe).toFixed(2))

		var importeOriginal = str_replace($("#UpdateImporteColateralOriginal").val(), ',', '')
		impt= importe
		subt 	= parseFloat(str_replace($("#TotalRebajado").html(), ',', '') ) - parseFloat(importeOriginal) + parseFloat(importe)

		$("#TotalRebajado").html(parseFloat(subt).toFixed(2))

		$( "#dialog-form-update-importe" ).dialog( "close" );
		$("#LblUpdateImporteColateral").html('')
		$("#UpdateImporteColateralImporte").val('0.00')
	}
