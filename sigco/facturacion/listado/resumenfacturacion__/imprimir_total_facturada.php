<?php

	session_name("pnsu");
	if(!session_start()){session_start();}
	
	set_time_limit(0);

	include("../../../../objetos/clsReporte.php");

    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{
		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo;
			// global $fechaapertura

			$h = 5;
			$this->SetY(10);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"FACTURAS PROCESADAS",0,1,'C');
			$this->Ln(3);

			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			// $this->Cell(40, $h,strtoupper("Mes de Referencia"),0,0,'L');
			// $this->SetFont('Arial','',8);
			// $this->Cell(20, $h,strtoupper($texto),0,0,'L');
			// $this->Cell(15, $h,strtoupper($anio),0,0,'L');
			// $this->SetFont('Arial','B',8);
			// $this->Cell(80, $h,strtoupper("Ciclo"),0,0,'R');
			// $this->SetFont('Arial','',8);
			// $this->Cell(15, $h,strtoupper($ciclo),0,1,'L');
			$this->Ln(3);

		}

		public function cabecera1(){

			global $Dim;

			$h=5;
			$this->SetX(30);
			$this->SetFont('Arial','B',8);
			$this->Cell(150,$h,"CANTIDAD DE USUARIOS FACTURADOS",0,1,'L',true);
			$this->Ln(1);

			$this->SetX(30);
			$h=5;
			$this->SetFont('Arial','B',8);
			$this->SetFillColor(146,144,144);
			$this->Cell($Dim[1],$h,"MES",1,0,'C',true);
			$this->Cell($Dim[2],$h,"TOTAL",1,1,'C',true);
		}

		public function cabecera2(){

			global $Dim;
			$this->SetX(30);
			$this->SetY($this->GetY());
			$this->Ln(2);

			$this->SetX(30);
			$this->SetFont('Arial','B',8);
			$this->Cell(150, $h, "FACTURADOS PROCESADAS POR DISTRITOS",0,1,'L',true);
			$this->Ln(3);

			$this->SetX(30);
			$h=5;
			$this->SetFont('Arial','B',8);
			$this->SetFillColor(146,144,144); //Color de Fondo
			$this->Cell($Dim[1], $h, "MES", 1, 0, 'C', true);
			$this->Cell($Dim[2], $h, "DIST 1 SICUANI", 1, 0, 'C', true);
			$this->Cell($Dim[3], $h, "DIST 2 SANTO TOMAS", 1, 0, 'C', true);
			//$this->Cell($Dim[4], $h, "DIST 3 MANANTAY", 1, 0, 'C', true);
			$this->Cell($Dim[5], $h, "TOTAL", 1, 1, 'C' ,true);

		}

		public function contenido($nombre_mes, $subtotal_mes){

			global $Dim;

			$this->SetX(30);
			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','B',8);
			$this->Cell($Dim[1], $h, utf8_decode(strtoupper($nombre_mes)), 1, 0, 'C');
			$this->SetFont('Arial','',8);
			$this->Cell($Dim[2],$h,$subtotal_mes,1,1,'C');
		}


		public function contenido1($nombre_mes, $loc1, $loc2, $loc3, $subtotal){

			global $Dim;

			$this->SetX(30);
			$h=5;
			$this->SetFillColor(255, 255, 255); //Color de Fondo
			$this->SetFont('Arial', 'B', 8);
			$this->Cell($Dim[1], $h, utf8_decode(strtoupper($nombre_mes)), 1, 0, 'C');
			$this->SetFont('Arial', '', 8);
			$this->Cell($Dim[2], $h, $loc1, 1, 0, 'C');
			$this->Cell($Dim[3], $h, $loc2, 1, 0, 'C');
			//$this->Cell($Dim[4],$h,$loc3,1,0,'C');
			$this->Cell($Dim[5], $h, $subtotal, 1, 1, 'C');
		}

		public function piecontenido($total_nombre, $total_loc1, $total_loc2, $total_loc3, $total_importe){

			global $Dim;

			$h=5;
			$this->SetFillColor(146,144,144); //Color de Fondo
			$this->SetX(30);
			$this->SetFont('Arial','B',8);
			$this->Cell($Dim[1], $h, utf8_decode(strtoupper($total_nombre)), 1, 0, 'C', true);
			$this->Cell($Dim[2], $h, $total_loc1, 1, 0, 'C', true);
			$this->Cell($Dim[3], $h, $total_loc2, 1, 0, 'C', true);
			//$this->Cell($Dim[4],$h,$total_loc3,1,0,'C',true);
			$this->Cell($Dim[5], $h, $total_importe, 1, 1, 'C', true);

			$this->Ln(4);

			$this->SetFillColor(146,144,144); //Color de Fondo
			$this->SetX(30);
			$this->SetFont('Arial','B',8);
			$this->Cell(50,$h,"LEYENDA",1,1,'C',true);

			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			$this->Cell(25,$h,"DIST 1",1,0,'C',false);
			$this->Cell(25,$h,"SICUANI",1,1,'C',false);
			$this->SetX(30);
			$this->Cell(25,$h,"DIST 2",1,0,'C',false);
			$this->Cell(25,$h,"SANTO TOMAS",1,1,'C',false);
			//$this->SetX(30);
			//$this->Cell(25,$h,"DIST 3",1,0,'C',false);
			//$this->Cell(25,$h,"MANANTAY 3",1,1,'C',false);



		}

		//  Para los cuadros con bordes redondeados o sin ellos
		public function RoundedRect($x, $y, $w, $h, $r, $style = '') {
			$k = $this->k;
			$hp = $this->h;
			if($style=='F')
				$op='f';
			elseif($style=='FD' || $style=='DF')
				$op='B';
			else
				$op='S';
			$MyArc = 4/3 * (sqrt(2) - 1);
			$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
			$xc = $x+$w-$r ;
			$yc = $y+$r;
			$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

			$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
			$xc = $x+$w-$r ;
			$yc = $y+$h-$r;
			$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
			$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
			$xc = $x+$r ;
			$yc = $y+$h-$r;
			$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
			$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
			$xc = $x+$r ;
			$yc = $y+$r;
			$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
			$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
			$this->_out($op);
		}

		public function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
		 $h = $this->h;
		 $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
		 $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
		}

	}

    $Dim = array('1'=>20,'2'=>35,'3'=>35,'4'=>35,'5'=>20);
		// global $meses;


	$codemp     = 1;
	$codsuc     = $_GET["codsuc"];
	$ciclo      = $_GET["ciclo"];
	$anio       = $_GET["anio"];
	$mes        = intval($_GET["mes"]);
	$texto      = $_GET["mestexto"];
	$valor      = $_GET["valor"];
	$idusuario  = $_SESSION['id_user'];
	$periodo    = $_GET["anio"].($_GET['mes']);

	$objReporte = new clsLecturas("L");
	$contador   = 0;
	$objReporte->AliasNbPages();
	$objReporte->cabecera();
	$objReporte->AddPage('P');

	$objReporte->SetFillColor(255, 255, 255);
	// $objReporte->RoundedRect(12, 40, 180, 10, 0.5, 'DF');

	$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
	$primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);

	// Para obtener el periodo de faturacion
	$sqlLect 			 = "SELECT nrofacturacion
							 			 	FROM facturacion.periodofacturacion
						 	 	 			WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
	$consultaL     = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp, $codsuc, $ciclo, $anio, $mes));
	$nrofact       = $consultaL->fetch();
	$array_general = array();


	if($mes >= 12 OR $anio >=2015 ) :
		// Numero de Usuarios
		$sqlLect = " SELECT
										cab.mes as mes,
										cli.codzona as codzona,
										count(*) as cantidad
									FROM facturacion.cabfacturacion cab
										JOIN catastro.clientes cli ON (cab.codemp = cli.codemp) AND (cab.codsuc = cli.codsuc) AND (cab.nroinscripcion = cli.nroinscripcion)
									WHERE cab.codsuc = ? AND cab.nrodocumento > 0
									  AND cab.mes = '" . $mes . "' AND cab.anio = '" . $anio . "'
									GROUP BY cab.mes ,cli.codzona ORDER BY cab.mes, cli.codzona";


		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute(array($codsuc));
		$items = $consultaL->fetchAll();
	else:
		$items = array();
	endif;

	$array_general = array();
	$array_general1 = array();

	foreach ($items as $val) :

		if(!array_key_exists($val['codzona'], $array_general[$val['mes']])):
			$array_general1[$val['mes']][$val['codzona']] = $val['cantidad'];
		endif;

		if(!array_key_exists($val['mes'], $array_general)):
			$array_general[$val['mes']]['total'] = 0;
		endif;

			$array_general[$val['mes']]['total'] += $val['cantidad'];

	endforeach;


	$objReporte->cabecera1();

	foreach ($array_general as $indice_mes => $valores) :

		$nombre_mes = substr($meses[$indice_mes],0,3);
		$objReporte->contenido($nombre_mes, $valores['total']);
	endforeach;

	$objReporte->Ln(3);



	$objReporte->cabecera2();

	$subtotal_importe = $subtotal_local1 = $subtotal_local2 = $subtotal_local3 =$total_importe = 0;

	foreach($array_general1 as $ind => $rowD):

		$nombre_mes = substr($meses[$ind],0,3);

		$local1 = (!empty($rowD[1])) ? $rowD[1] : 0 ;
		$local2 = (!empty($rowD[2])) ? $rowD[2] : 0 ;
		$local3 = (!empty($rowD[3])) ? $rowD[3] : 0 ;

		$subtotal_importe = $local1 + $local2 + $local3;

		$objReporte->contenido1($nombre_mes, $local1, $local2, $local3, $subtotal_importe);
		$subtotal_local1 += $local1;
		$subtotal_local2 += $local2;
		$subtotal_local3 += $local3;
		$total_importe   += $subtotal_importe;

	endforeach;

	$objReporte->piecontenido("TOTAL", $subtotal_local1, $subtotal_local2, $subtotal_local3,$total_importe);



	$objReporte->Output();

?>
