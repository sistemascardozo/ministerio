<?php

	session_name("pnsu");
	if(!session_start()){session_start();}

	set_time_limit(0);

	include("../../../../objetos/clsReporte.php");


  $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{
		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo;
			// global $fechaapertura

			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"RESUMEN DE FACTURACION TOTAL",0,1,'C');
			$this->Ln(3);

			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			$this->Cell(40, $h,strtoupper("Mes de Referencia"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');
			$this->Ln(3);
			$h = 10;

			$this->SetX(8);
			$this->SetFont('Arial','B',8);
			$this->SetWidths(array(18,20,20,25,18,20,16,22,20,25,18,25,20,18));
			$this->SetAligns(array("C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C","C"));
			$this->Row(array(utf8_decode("Categoría A. Cat. Sub."),
							 utf8_decode("N° de Recibos"),
							 utf8_decode("Pensión Agua"),
							 utf8_decode("Pensión Desague"),
							 utf8_decode("Intereses"),
							 utf8_decode("Otros Conceptos"),
							 utf8_decode("Igv"),
							 utf8_decode("Sub Total"),
							 utf8_decode("Redondeo"),
							 utf8_decode("Cuota de Convenio"),
							 utf8_decode("Cuota Fonavi"),
							 utf8_decode("Total Facturado"),
							 utf8_decode("Deuda Anterior"),
							 utf8_decode("Total")
						 ));
			$this->Ln(2);
		}

		public function contenido($indice_categoria, $numero_recibos, $pension_agua, $pension_desague, $intereses, $otros_conceptos, $igv, $subtotal, $redondeo, $cuota_convenio, $cuota_fonavi, $total_facturado, $deuda_anterior, $total){

			global $Dim;

			$h=7;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','',8);

			$this->SetX(7);
			// $this->SetTextColor(0);
			$this->Cell($Dim[1],$h,utf8_decode(strtoupper($indice_categoria)),0,0,'C');
			$this->Cell($Dim[2],$h,number_format($numero_recibos,0),0,0,'R');
			$this->Cell($Dim[3],$h,number_format($pension_agua,2),0,0,'R');
			$this->Cell($Dim[4],$h,number_format($pension_desague,2),0,0,'R');
			$this->Cell($Dim[5],$h,number_format($intereses,2),0,0,'R');
			$this->Cell($Dim[6],$h,number_format($otros_conceptos,2),0,0,'R');
			$this->Cell($Dim[7],$h,number_format($igv,2),0,0,'R');
			$this->Cell($Dim[8],$h,number_format($subtotal,2),0,0,'R');
			$this->Cell($Dim[9],$h,number_format($redondeo,2),0,0,'R');
			$this->Cell($Dim[10],$h,number_format($cuota_convenio,2),0,0,'R');
			$this->Cell($Dim[11],$h,number_format($cuota_fonavi,2),0,0,'R');
			$this->Cell($Dim[12],$h,number_format($total_facturado,2),0,0,'R');
			$this->Cell($Dim[13],$h,number_format($deuda_anterior,2),0,0,'R');
			$this->Cell($Dim[14],$h,number_format($total,2),0,1,'R');
			// $this->Cell($Dim[6],$h," ",0,1,'C',true);

		}

		public function piecontenido($nombre_subtotal, $subtotal_num_rec, $subtotal_a, $subtotal_d, $subtotal_i,
		$subtotal_o_c, $subtotal_ig, $subtotal_sub, $subtotal_red, $subtotal_con, $subtotal_fon,
		$subtotal_tot_fac, $subtotal_deu, $subtotal_tot){

			global $Dim;

			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->Ln(4);

			$this->SetX(8);
			$this->SetFont('Arial','B',8);
			$this->Cell($Dim[1],$h,utf8_decode(strtoupper($nombre_subtotal)),0,0,'C');
			$this->Cell($Dim[2],$h,number_format($subtotal_num_rec,0),0,0,'R');
			$this->Cell($Dim[3],$h,number_format($subtotal_a,2),0,0,'R');
			$this->Cell($Dim[4],$h,number_format($subtotal_d,2),0,0,'R');
			$this->Cell($Dim[5],$h,number_format($subtotal_i,2),0,0,'R');
			$this->Cell($Dim[6],$h,number_format($subtotal_o_c,2),0,0,'R');
			$this->Cell($Dim[7],$h,number_format($subtotal_ig,2),0,0,'R');
			$this->Cell($Dim[8],$h,number_format($subtotal_sub,2),0,0,'R');
			$this->Cell($Dim[9],$h,number_format($subtotal_red,2),0,0,'R');
			$this->Cell($Dim[10],$h,number_format($subtotal_con,2),0,0,'R');
			$this->Cell($Dim[11],$h,number_format($subtotal_fon,2),0,0,'R');
			$this->Cell($Dim[12],$h,number_format($subtotal_tot_fac,2),0,0,'R');
			$this->Cell($Dim[13],$h,number_format($subtotal_deu,2),0,0,'R');
			$this->Cell($Dim[14],$h,number_format($subtotal_tot,2),0,1,'R');

		}

		//  Para los cuadros con bordes redondeados o sin ellos
		function RoundedRect($x, $y, $w, $h, $r, $style = '') {
			$k = $this->k;
			$hp = $this->h;
			if($style=='F')
				$op='f';
			elseif($style=='FD' || $style=='DF')
				$op='B';
			else
				$op='S';
			$MyArc = 4/3 * (sqrt(2) - 1);
			$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
			$xc = $x+$w-$r ;
			$yc = $y+$r;
			$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

			$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
			$xc = $x+$w-$r ;
			$yc = $y+$h-$r;
			$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
			$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
			$xc = $x+$r ;
			$yc = $y+$h-$r;
			$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
			$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
			$xc = $x+$r ;
			$yc = $y+$r;
			$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
			$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
			$this->_out($op);
		}

		function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
		 $h = $this->h;
		 $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
		 $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
		}



	}

  $Dim = array('1'=>18,'2'=>20,'3'=>20,'4'=>25,'5'=>18,'6'=>20,'7'=>16,'8'=>22,'9'=>20,'10'=>25,'11'=>18,'12'=>25,'13'=>20,'14'=>18);

	$codemp     = 1;
	$codsuc     = $_GET["codsuc"];
	$ciclo      = $_GET["ciclo"];
	$anio       = $_GET["anio"];
	$mes        = intval($_GET["mes"]);
	$texto      = $_GET["mestexto"];
	$valor      = $_GET["valor"];
	$idusuario  = $_SESSION['id_user'];
	$periodo    = $_GET["anio"].($_GET['mes']);

	$total      = 0;
	$totalconv  = 0;
	$sados      = 0;

	$objReporte = new clsLecturas("L");
	$contador   = 0;
	$objReporte->AliasNbPages();
	$objReporte->cabecera();
	$objReporte->AddPage();

	$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
	$primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);
	///RESTAR
	//QUITAR
	$TotalRestar   = 0;
	// Para obtener el periodo de faturacion
	$sqlLect = "SELECT nrofacturacion, nrotarifa ";
	$sqlLect .= "FROM facturacion.periodofacturacion ";
	$sqlLect .= "WHERE codemp = ".$codemp." ";
	$sqlLect .= " AND codsuc = ".$codsuc." ";
	$sqlLect .= " AND codciclo = ".$ciclo." ";
	$sqlLect .= " AND anio = '".$anio."' ";
	$sqlLect .= " AND mes = '".$mes."' ";
	
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array());
	$nrofact = $consultaL->fetch();

	$array_general = array();

	// Todo los Conceptos
	$sqlLect1 = "SELECT (d.codsuc ||''|| t.codcategoriatar ||''|| substring(CAST(t.catetar AS text) from 3 for 1)) AS id, ";
	$sqlLect1 .= " (d.codsuc ||'-'|| t.codcategoriatar ||'-'|| substring(CAST(t.catetar AS text) from 3 for 1)) AS ided, ";
	$sqlLect1 .= " (d.codsuc ||'-'|| t.codcategoriatar ||'-'|| t.nomtar) AS ide, ";
	$sqlLect1 .= " c.descripcion, c.codconcepto, sum(d.importe) ";
	$sqlLect1 .= "FROM facturacion.detctacorriente d ";
	$sqlLect1 .= " JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto) ";
	$sqlLect1 .= " JOIN facturacion.cabctacorriente AS cd ON (d.codemp = cd.codemp) AND (d.codsuc = cd.codsuc) AND (d.codciclo = cd.codciclo) ";
	$sqlLect1 .= "  AND (d.nrofacturacion = cd.nrofacturacion) AND (d.nroinscripcion = cd.nroinscripcion) ";
	$sqlLect1 .= "  AND (d.anio = cd.anio) AND (d.mes = cd.mes) AND (d.periodo = cd.periodo) AND (d.tipo = cd.tipo) ";
	$sqlLect1 .= "  AND (d.tipoestructura = cd.tipoestructura) ";
	$sqlLect1 .= " JOIN facturacion.tarifas AS t ON (d.codemp = t.codemp) AND (d.codsuc = t.codsuc) AND (cd.catetar = t.catetar) ";
	$sqlLect1 .= "WHERE d.codsuc = ? ";
	$sqlLect1 .= " AND d.nrofacturacion = ".$nrofact['nrofacturacion']." ";
	$sqlLect1 .= " AND ((d.nrocredito = 0 AND d.nrorefinanciamiento = 0) OR d.codtipodeuda = 9 OR d.codconcepto = 9 OR d.codtipodeuda IN(3, 4, 7, 9)) ";
	$sqlLect1 .= " AND d.codconcepto <> 10012 ";
	$sqlLect1 .= " AND d.codtipodeuda NOT IN(3, 4, 7) ";
	$sqlLect1 .= " AND d.tipo = 0 ";
	$sqlLect1 .= " AND d.tipoestructura = 0 ";
	$sqlLect1 .= " AND d.periodo = '".$anio.$mes."' ";
	$sqlLect1 .= " AND t.nrotarifa = ".$nrofact['nrotarifa']." ";
	$sqlLect1 .= "GROUP BY c.codconcepto, c.descripcion, d.codsuc, t.codcategoriatar, t.nomtar, t.catetar ";
	$sqlLect1 .= "ORDER BY t.codcategoriatar, t.catetar, c.codconcepto";

	$consultaL = $conexion->prepare($sqlLect1);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetchAll();
	$array_general = array();

	// echo "<pre>";	var_dump($items);	echo "</pre>"; exit;

	foreach ($items as $val) :

		if(!array_key_exists($val['ided'],$array_general)):
			$array_general[$val['ided']] = array();
		endif;

		if(!array_key_exists($val['codconcepto'], $array_general[$val['ided']])):
			$array_general[$val['ided']][$val['codconcepto']] = $val['sum'];
		endif;
	endforeach;


	// Cuota de Convenio
	$sqlLect = "SELECT (d.codsuc ||''|| t.codcategoriatar ||''|| SUBSTRING(CAST(t.catetar AS text) FROM 3 FOR 1)) AS id, ";
	$sqlLect .= " (d.codsuc ||'-'|| t.codcategoriatar ||'-'|| SUBSTRING(CAST(t.catetar AS text) FROM 3 FOR 1)) AS ided, ";
	$sqlLect .= " (d.codsuc ||'-'|| t.codcategoriatar ||'-'|| t.nomtar) AS ide, ";
	$sqlLect .= " c.descripcion, c.codconcepto, SUM(d.importe) AS importe ";
	$sqlLect .= "FROM facturacion.detctacorriente d ";
	$sqlLect .= " JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto) ";
	$sqlLect .= " JOIN facturacion.cabctacorriente AS cd ON (d.codemp = cd.codemp) AND (d.codsuc = cd.codsuc) AND (d.codciclo = cd.codciclo) ";
	$sqlLect .= "  AND (d.nrofacturacion = cd.nrofacturacion) AND (d.nroinscripcion = cd.nroinscripcion) AND (d.anio = cd.anio) AND (d.mes = cd.mes) ";
	$sqlLect .= "  AND (d.periodo = cd.periodo) AND (d.tipo = cd.tipo) AND (d.tipoestructura = cd.tipoestructura) ";
	$sqlLect .= " JOIN facturacion.tarifas AS t ON (d.codemp = t.codemp) AND (d.codsuc = t.codsuc) AND (cd.catetar = t.catetar) ";
	$sqlLect .= "WHERE d.codsuc = ? ";
	$sqlLect .= " AND d.periodo = '".$anio.$mes."' ";
	$sqlLect .= " AND d.tipo = 0 ";
	$sqlLect .= " AND d.codconcepto <> 101 ";
	$sqlLect .= " AND d.codtipodeuda IN(3, 4, 7) ";
	$sqlLect .= " AND d.tipoestructura = 0 ";
	$sqlLect .= " AND t.nrotarifa = ".$nrofact['nrotarifa']." ";
	$sqlLect .= "GROUP BY c.codconcepto, c.descripcion, d.codsuc, t.codcategoriatar, t.nomtar, t.catetar ";
	$sqlLect .= "ORDER BY t.codcategoriatar, t.catetar, c.codconcepto";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetchAll();

	foreach ($items as $val) :

		if(!array_key_exists($val['ided'],$array_general)):
			$array_general[$val['ided']] = array();
		endif;

		$array_general[$val['ided']][8000] += $val['importe'];

	endforeach;


	// Meses Anteriores
	$sqlLect = "SELECT (d.codsuc ||'-'|| t.codcategoriatar ||'-'|| SUBSTRING(CAST(t.catetar AS text) FROM 3 FOR 1)) AS ided, ";
	$sqlLect .= " SUM((d.importe - d.importeacta) + d.importerebajado) AS importe ";
	$sqlLect .= "FROM facturacion.detctacorriente d ";
	$sqlLect .= " JOIN facturacion.cabctacorriente AS cd ON (d.codemp = cd.codemp) AND (d.codsuc = cd.codsuc) AND (d.codciclo = cd.codciclo) ";
	$sqlLect .= "  AND (d.nrofacturacion = cd.nrofacturacion) AND (d.nroinscripcion = cd.nroinscripcion) AND (d.anio = cd.anio) AND (d.mes = cd.mes) ";
	$sqlLect .= "  AND (d.periodo = cd.periodo) AND (d.tipo = cd.tipo) AND (d.tipoestructura = cd.tipoestructura) ";
	$sqlLect .= " JOIN facturacion.tarifas AS t ON (d.codemp = t.codemp) AND (d.codsuc = t.codsuc) AND (cd.catetar = t.catetar) ";
	$sqlLect .= "WHERE d.codsuc = ? ";
	$sqlLect .= " AND d.periodo = '".$anio.$mes."' ";
	$sqlLect .= " AND d.codconcepto <> 101 ";
	$sqlLect .= " AND d.tipoestructura = 0 ";
	$sqlLect .= " AND d.tipo = 1 ";
	$sqlLect .= " AND d.codtipodeuda <> 3 ";
	$sqlLect .= " AND t.nrotarifa = ".$nrofact['nrotarifa']." ";
	$sqlLect .= "GROUP BY d.codsuc, t.codcategoriatar, t.nomtar, t.catetar ";
	$sqlLect .= "ORDER BY t.codcategoriatar, t.catetar";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetchAll();

	foreach ($items as $val) :

		if(!array_key_exists($val['ided'],$array_general)):
			$array_general[$val['ided']] = array();
		endif;

		$array_general[$val['ided']][9000] += $val['importe'];

	endforeach;

	// Numero de Recibos
	$sqlLect = "SELECT (cab.codsuc ||''|| t.codcategoriatar ||''|| SUBSTRING(CAST(t.catetar AS text) FROM 3 FOR 1)) AS id, ";
	$sqlLect .= " (cab.codsuc ||'-'|| t.codcategoriatar ||'-'|| SUBSTRING(CAST(t.catetar AS text) FROM 3 FOR 1)) AS ided, ";
	$sqlLect .= " (cab.codsuc ||'-'|| t.codcategoriatar ||'-'|| t.nomtar) AS ide, COUNT(*) AS cantidad ";
	$sqlLect .= "FROM facturacion.cabfacturacion cab ";
	$sqlLect .= " JOIN facturacion.tarifas AS t ON (cab.codemp = t.codemp) AND (cab.codsuc = t.codsuc) AND (cab.catetar = t.catetar) ";
	$sqlLect .= "WHERE cab.codsuc = ? ";
	$sqlLect .= " AND cab.nrofacturacion = ? ";
	$sqlLect .= " AND cab.nrodocumento > 0 ";
	$sqlLect .= " AND t.nrotarifa = ".$nrofact['nrotarifa']." ";
	$sqlLect .= "GROUP BY cab.codsuc, t.codcategoriatar, t.nomtar, t.catetar ";
	$sqlLect .= "ORDER BY t.codcategoriatar, t.catetar";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc, $nrofact['nrofacturacion']));
	$items = $consultaL->fetchAll();

	foreach ($items as $val) :

		if(!array_key_exists($val['ided'],$array_general)):
			$array_general[$val['ided']] = array();
		endif;

		$array_general[$val['ided']]['numero_recibos'] = $val['cantidad'];

	endforeach;


	$suma_intereses = $suma_igv = $suma_otros_conceptos = $suma_subtotal = $suma_subtotal = 0;
	$subtotal_agua = $subtotal_desague = $subtotal_intereses = $subtotal_otros_conceptos = 0;
	$subtotal_igv = $subtotal_subtotal = $subtotal_convenio = 0;
	$subtotal_redondeo = $subtotal_red = $suma_fonavi = $subtotal_fonavi = 0;
	$suma_total_facturado = $subtotal_total_facturado = $subtotal_deuda = 0;
	$suma_total = $subtotal_total = $subtotal_numero_recibo = 0;

	foreach($array_general as $ind => $rowD):

		// Intereses
		$suma_intereses         = $rowD[3] + $rowD[4] + $rowD[9] + $rowD[71] + $rowD[72] + $rowD[10013] + $rowD[10009];
		$suma_redondeo          = $rowD[7] + $rowD[8];
		
		$suma_igv               = (isset($rowD[5]))?$rowD[5]:0;
		
		$cantidad_numero_recibo = (isset($rowD['numero_recibos']))?$rowD['numero_recibos']:0;
		$suma_fonavi            = (isset($rowD[101]))?$rowD[101]:0;
		
		$suma_otros_conceptos   = array_sum($rowD) - ($rowD[1] + $rowD[2] + $suma_intereses + $suma_igv + $suma_redondeo + $rowD[8000] + $rowD[9000] + $rowD['numero_recibos']); //

		$suma_subtotal          = $suma_intereses + $suma_igv + $suma_otros_conceptos + $rowD[1] + $rowD[2];
		$suma_total_facturado   = $suma_subtotal + $suma_redondeo + $suma_fonavi + $rowD[8000];

		$suma_total   = $suma_total_facturado + $rowD[9000];

		$objReporte->contenido($ind, $cantidad_numero_recibo, $rowD[1], $rowD[2], $suma_intereses, $suma_otros_conceptos, $suma_igv, $suma_subtotal, $suma_redondeo, $rowD[8000], $suma_fonavi, $suma_total_facturado, $rowD[9000], $suma_total);

		$subtotal_numero_recibo   += $cantidad_numero_recibo;
		$subtotal_agua            += $rowD[1];
		$subtotal_desague         += $rowD[2];
		$subtotal_intereses       += $suma_intereses;
		$subtotal_otros_conceptos += $suma_otros_conceptos;
		$subtotal_igv             += $suma_igv;
		$subtotal_subtotal        += $suma_subtotal;
		$subtotal_convenio        += $rowD[8000];
		$subtotal_deuda           += $rowD[9000];
		$subtotal_redondeo        += $suma_redondeo;
		$subtotal_fonavi          += $suma_fonavi;
		$subtotal_total_facturado += $suma_total_facturado;
		$subtotal_total           += $suma_total;

	endforeach;

	// var_dump($subtotal_convenio);exit;

	$objReporte->SetFillColor(255, 255, 255);
	$objReporte->RoundedRect(8, $objReporte->GetY()+1.5, 286, 10, 0, 'DF');
	$objReporte->piecontenido("TOTAL:", $subtotal_numero_recibo, $subtotal_agua, $subtotal_desague, $subtotal_intereses, $subtotal_otros_conceptos, $subtotal_igv, $subtotal_subtotal, $subtotal_redondeo, $subtotal_convenio, $subtotal_fonavi, $subtotal_total_facturado, $subtotal_deuda, $subtotal_total);
	$objReporte->Output();

?>
