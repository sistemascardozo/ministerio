<?php
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");

    $TituloVentana = "CRITICA DE FACTURACION";
    $Activo = 1;
	
    CuerpoSuperior($TituloVentana);
	
    $codsuc = $_SESSION['IdSucursal'];
    $objDrop = new clsDrop();
    $sucursal = $objDrop->setSucursales(" WHERE codemp = 1 AND codsuc = ?", $codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_listadofacturacion.js" language="JavaScript"></script>
<script>
    jQuery(function ($)
    {
        $("#DivTipos").buttonset();
        $("#consumo_ini").val(0);
        $("#consumo_fin").val(0);
    });
	
    var urldir = "<?php echo $_SESSION['urldir'];?>"
    var codsuc = <?=$codsuc?>;

    function pdf(Op)
	{
        var rutas = ""
        var sectores = ""

        if ($("#ciclo").val() == 0)
        {
            alert('Seleccione el Ciclo')
            return false
        }
        if ($("#anio").val() == 0)
        {
            alert('Seleccione el Anio a Consultar')
            return false
        }
        if ($("#mes").val() == 0)
        {
            alert('Seleccione el Mes a Consultar')
            return false
        }
        if ($("#todosectores").val() == 0)
        {
            sectores = $("#codsector").val()
            if (sectores == 0)
            {
                alert("Seleccione el Sector")
                return
            }
        } else {
            sectores = "%"
        }
        if ($("#todosrutas").val() == 0)
        {
            rutas = $("#rutaslecturas").val()
            if (rutas == 0)
            {
                alert("Seleccione la Ruta de Lectura")
                return false;
            }
        } else {
            rutas = "%"
        }
        if ($("#consumo_ini").val() == " ")
        {
            alert('Digitar una numero entero')
            return false
        }
        if ($("#consumo_fin").val() == " ")
        {
            alert('Digitar una numero entero')
            return false
        }

        if (parseInt($("#consumo_ini").val()) > parseInt($("#consumo_fin").val()))
        {
            alert('Consumo minimo no puede ser mayor que el maximo')
            return false
        }

        // alert($("#mes").val());return false;

        url = "imprimir.php";
        url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val() +
                "&ruta="+rutas+"&sector="+sectores+"&orden="+$("#orden").val()+"&codsuc=<?=$codsuc ?>"+"&minimo="+$("#consumo_ini").val()+"&maximo="+$("#consumo_fin").val()+"&importe="+$("#importe").val()
        AbrirPopupImpresion(url, 800, 600)

        return false
    }

    function Cancelar()
    {
        location.href = '<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
    }

    function cargar_rutas_lecturas(obj, cond)
    {
        $.ajax({
            url: '../../../../ajax/rutas_lecturas_drop.php',
            type: 'POST',
            async: true,
            data: 'codsector='+obj+'&codsuc=<?=$codsuc ?>&condicion='+cond,
            success: function (datos) {
                $("#div_rutaslecturas").html(datos)
                $("#chkrutas").attr("checked", true)
                $("#todosrutas").val(1)
            }
        })
    }

    function validar_rangoconsumo(obj) {
        if (obj.checked)
        {
            $("#div_rangoconsumo").show();
        } else {
            $("#div_rangoconsumo").hide().find('input').val('');

        }
    }
    function validar_sectores(obj, idx)
    {
        $("#rutaslecturas").attr("disabled", true)
        $("#chkrutas").attr("checked", true)
        $("#todosrutas").val(1)

        if (obj.checked)
        {
            $("#"+idx).val(1)
            $("#codsector").attr("disabled", true)
        } else {
            $("#"+idx).val(0)
            $("#codsector").attr("disabled", false)
        }
    }
    function validar_rutas(obj, idx)
    {
        if (obj.checked)
        {
            $("#"+idx).val(1)
            $("#rutaslecturas").attr("disabled", true)
        } else {
            $("#"+idx).val(0)
            $("#rutaslecturas").attr("disabled", false)
        }
    }
</script>
<div>
    <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar; ?>" enctype="multipart/form-data">
        <fieldset>
            <table width="800" border="0" cellspacing="2" cellpadding="2">
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td width="130" align="right">Sucursal</td>
                    <td width="20" align="center">:</td>
                    <td>
                        <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                        <input type="hidden" name="todosrutas" id="todosrutas" value="1" />
                        <input type="hidden" name="todosectores" id="todosectores" value="1" />
                        <input type="hidden" name="orden" id="orden" value="1" />
                    </td>
                    <td width="100" align="right">Ciclo</td>
                    <td width="20" align="center">:</td>
                    <td>
                        <?php $objDrop->drop_ciclos($codsuc,0,"onchange='datos_facturacion(this.value);cargar_anio(this.value,1);'"); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right">Sector</td>
                    <td align="center">:</td>
                    <td>
                        <?php echo $objDrop->drop_sectores2($codsuc, 0, "onchange='cargar_rutas_lecturas(this.value,1);'"); ?></td>
                    <td  align="right">Todos los Sectores </td>
                    <td align="center">
                        <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosectores');validar_sectores(this, 'todosectores');" />
                    </td>                    
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">Ruta</td>
                    <td align="center">:</td>
                    <td>
                        <div id="div_rutaslecturas">
                            <select name="rutaslecturas" id="rutaslecturas" style="width:220px" class="select" disabled="disabled">
                                <option value="0">--Seleccione la Ruta de Lectura--</option>
                            </select>
                        </div>
                    </td>
                    <td  align="right">Todas las Rutas </td>                    
                    <td align="center">
                        <input type="checkbox" name="chkrutas" id="chkrutas" value="checkbox" checked="checked" onclick="CambiarEstado(this, 'todosrutas');validar_rutas(this, 'todosrutas');" />
                    </td>
                    <td>&nbsp;</td>
                </tr>               
                <tr>
                    <td align="right">A&ntilde;o</td>
                    <td align="center">:</td>
                    <td>
                        <div id="div_anio">
                            <?php $objDrop->drop_anio($codsuc,0); ?>
                        </div>
                    </td>
                    <td align="right">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">Mes</td>
                    <td align="center">:</td>
                    <td>
                        <div id="div_meses">
                            <?php $objDrop->drop_mes($codsuc,0,0); ?>
                        </div>
                    </td>
                    <td align="right">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right">Por Rango de Consumo</td>
                    <td align="center">:</td>
                    <td>
                        <div id="div_rangoconsumo" >
                            Desde: <input type="text" id="consumo_ini" size="6" maxlength="6">
                            Hasta: <input type="text" id="consumo_fin" size="6" maxlength="6">
                        </div>
                    </td>
                    <td align="right">Por importe (Agua)</td>
                    <td align="center">:</td>
                    <td><input type="text" id="importe" size="6" maxlength="6"></td>
                </tr>      
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="5">
                        <div id="DivTipos" style="display:inline">
                            <input type="button" name="rabresumen" id="rptpdf" value="GENERAR EN PDF" onclick="pdf()"/>
                        </div>	
                         <!--<input type="button" onclick="return ValidarForm();" value="Generar" id="">-->
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
<div id="mostrar_datos"></div>
<script>
    $("#codsector").attr("disabled", true)
</script>
<?php CuerpoInferior(); ?>