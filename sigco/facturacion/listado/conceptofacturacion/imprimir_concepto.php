<?php
	include("../../../../objetos/clsReporte.php");

    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{

		function Header()
		{
			global $codsuc;

			$empresa = $this->datos_empresa($codsuc);
			$this->SetFont('Arial','',6);
			$this->SetTextColor(0,0,0);
			$tit1=($empresa["razonsocial"]);
			$tit2=($empresa["direccion"]);
			$tit3="SUCURSAL: ".($empresa["descripcion"]);

			$this->SetX(20);
            $this->SetFont('Arial', 'B', 14);
            $tit11 = strtoupper("Reporte de Facturacion por Conceptos");
            $this->Cell(277, 5, utf8_decode($tit11), 0, 1, 'C');

			$x 	= 23;
            $y 	= 5;
			$h	= 3;
		    $this->Image($_SESSION['path']."images/logo_empresa.jpg",6,1,17,16);
            $this->SetXY($x,$y);
			$this->SetFont('Arial','',6);
			$this->Cell(85, $h,utf8_decode($tit1),0,1,'L');
			$this->SetX($x);
			$this->Cell(85, $h,utf8_decode($tit2),0,1,'L');
			$this->SetX($x);
			$this->Cell(150, $h,utf8_decode($tit3),0,0,'L');

			$this->SetY(8);
			$FechaActual = date('d/m/Y');
			$Hora = date('h:i:s a');


			$this->Cell(0, 3,utf8_decode("Página    :   ".$this->PageNo().' de {nb}     '), 0, 1, 'R');

	 		$this->Cell(0, 3,"Fecha     : ".$FechaActual."   ", 0, 1, 'R');
			$this->Cell(0, 3,"Hora       : ".$Hora." ", 0, 1, 'R');

			$this->SetY(20);
			$this->SetX(23);
			$this->SetLineWidth(.1);
			$this->Cell(0,.1,"",1,1,'C',true);

            $this->Ln(5);

			$this->cabecera();
		}

		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo,$textoconcepto,$concepto;
			// global $fechaapertura

			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->Ln(2);
			$this->SetX(20);
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"Concepto: " . strtoupper($textoconcepto),0,1,'C');
			$this->Ln(5);

			$this->SetFont('Arial','B',8);
			$this->SetX(90);
			$this->Cell(40, $h,strtoupper("Periodo:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');
			$this->Ln(3);

			$h = 6;
			$this->SetFont('Arial','B',8);
			$this->Cell($Dim[1],$h,utf8_decode('N°'),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode('INSCRIPCION'),1,O,'C',false);
			$this->Cell($Dim[3],$h,utf8_decode('CLIENTE'),1,O,'C',false);
			$this->Cell($Dim[4],3,utf8_decode('IMPORTE'),1,1,'C',false);

			$this->SetXY($this->GetX()+115,$this->GetY());
			$this->Cell($Dim[8],3,utf8_decode('NO IMPONIBLE'),1,0,'C',false);
			$this->Cell($Dim[8],3,utf8_decode('IMPONIBLE'),1,1,'C',false);
			// $this->Ln(5);
		}

		public function contenidousuario($indice, $codcliente, $propietario){

			global $Dim, $codsuc;
			//$codcliente = $this->CodUsuario($codsuc, $codcliente);

			$h=5;
			$this->SetFont('Arial','',8);
			//$this->SetLineWidth(.1);
			$this->Cell($Dim[1],$h,utf8_decode($indice),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode($codcliente),1,0,'C',false);
			$this->Cell($Dim[5],$h,utf8_decode($propietario),1,1,'L',false);

		}

		public function subcabecera(){

			global $Dim;
			$h=5;
			$this->SetFont('Arial','',8);
			$this->Cell($Dim[1],$h,utf8_decode('N°'),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode('Concepto'),1,1,'L',false);

		}

		public function contenido($codconcepto, $concepto, $importe){

			global $Dim;
			$h=5;
			$this->SetFont('Arial','',7);
			$this->Cell($Dim[1],$h,utf8_decode(""),0,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode($codconcepto),0,0,'C',false);
			$this->Cell($Dim[3],$h,utf8_decode($concepto),0,0,'L',false);

			if(($codconcepto == 1 OR $codconcepto == 2 OR $codconcepto == 6 )):
				//Imponible
				$this->Cell($Dim[8],$h,"",0,0,'R',false);
				$this->Cell($Dim[8],$h, utf8_decode($importe),0,1,'R',false);
			else:
				$this->Cell($Dim[8],$h, utf8_decode($importe),0,0,'R',false);
				$this->Cell($Dim[8],$h,"",0,1,'R',false);
			endif;
		}

		public function subpie($texto, $subtoimpo, $subtonoimpo){

			global $Dim;
			$h=5;
			$this->SetFont('Arial','B',8);
			$this->Cell(($Dim[1]+$Dim[2]+$Dim[3]),$h,utf8_decode($texto),1,0,'R',false);
			$this->Cell($Dim[8],$h,number_format($subtonoimpo,2),1,0,'R',false);
			$this->Cell($Dim[8],$h,number_format($subtoimpo,2),1,1,'R',false);

			$this->Cell(($Dim[1]+$Dim[2]+$Dim[3]),$h,utf8_decode("TOTAL PARCIAL =====>"),1,0,'R',false);
			$this->Cell(($Dim[4]),$h,number_format(($subtonoimpo+$subtoimpo),2),1,1,'R',false);

		}

		public function Piefinal($texto, $totalnoimp, $totalimp){

			global $Dim;
			$h=5;
			$this->SetFont('Arial','B',8);
			$this->Cell(($Dim[1]+$Dim[2]+$Dim[3]),$h,utf8_decode($texto),1,0,'R',false);
			$this->Cell($Dim[8],$h,utf8_decode($totalnoimp),1,0,'R',false);
			$this->Cell($Dim[8],$h,utf8_decode($totalimp),1,1,'R',false);
		}

		function Footer(){

			$this->SetY(-10);
			$this->SetFont('Arial','I',6);
			$this->SetTextColor(0);
			$this->SetX(23);
			$this->SetLineWidth(.1);
			$this->Cell(0,.1,"",1,1,'C',true);
			$this->Ln(1);
			$this->SetX(23);
			$this->Cell(0,4,utf8_decode('Sistema Integrado de Información Comercial - SIINCO - Versión WEB'),0,0,'L');
			$this->Cell(0,4,'Pag. '.$this->PageNo().' de {nb}',0,0,'R');
		}

	}

	$Dim = array('1'=>15,'2'=>20,'3'=>80,'4'=>80,'5'=>160,'6'=>10,'7'=>30,'8'=>40);

	$codemp         = 1;
	$anio           = $_GET["anio"];
	$mes            = $_GET["mes"];
	$codsuc         = $_GET["codsuc"];
	$ciclo          = $_GET["ciclo"];
	$idusuario      = $_SESSION['id_user'];
	$periodo        = $_GET["anio"].$_GET['mes'];
	$concepto       = $_GET['codconcepto'];
	$tipoconceptos  = $_GET['tipoconceptos'];
	$texto          = $_GET['mesdescri'];
	$todosconceptos = $_GET['todosconceptos'];
	$textoconcepto  = ($todosconceptos=='%') ? "TODOS LOS CONCEPTOS" : $_GET['textoconcepto'];

	$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
	$primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);

	// Para obtener el periodo de faturacion
	$sqlLect = "select nrofacturacion
		FROM facturacion.periodofacturacion
		WHERE codemp = ? AND codsuc = ? AND codciclo = ?
		AND anio = ? AND mes = ?";
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
	$nrofact = $consultaL->fetch();

	if ($todosconceptos != '%') :

		if ($tipoconceptos != 0):
			$qls = " AND d.codconcepto = " . $concepto;
		else:
			$qls = " AND d.codconcepto = " . $concepto;
		endif;

	else :

		$qls = " ";

	endif;


	$sql = "SELECT c.fechareg, c.coddocumento, c.serie, c.nrodocumento, cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
	$sql .= " c.nroinscripcion, cl.propietario, co.codconcepto as codconcepto, co.descripcion as concepto, ";
	$sql .= " CAST(sum(d.importe) AS NUMERIC (18, 2)) AS importe, ";
	$sql .= " c.impigv, c.fechavencimiento, cl.codantiguo, ";
	$sql .= " SUM(CASE WHEN d.codconcepto IN(1, 2, 6) THEN d.importe END) AS Imponible, ";
	$sql .= " SUM(CASE WHEN d.codconcepto NOT IN(1, 2, 5, 6) THEN d.importe END) AS NoImponible ";
	$sql .= "FROM facturacion.cabctacorriente c ";
	$sql .= " INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) ";
	$sql .= "  AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) ";
	$sql .= "  AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) ";
	$sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
	$sql .= " INNER JOIN catastro.clientes AS cl ON(c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.nroinscripcion = cl.nroinscripcion) ";
	$sql .= "WHERE c.codemp = 1 ";
	$sql .= " AND d.codsuc = " . $codsuc . " ";
	$sql .= " AND cl.codciclo = " . $ciclo . " ";
	$sql .= " AND ((d.nrocredito = 0 AND d.nrorefinanciamiento = 0) OR d.codconcepto = 9 OR d.codtipodeuda IN (3, 4, 7, 9)) ";
	$sql .= " AND d.tipo = 0 ";
	$sql .= " AND d.tipoestructura = 0 ";
	$sql .= " AND d.periodo = '".$anio.$mes."' ";
	$sql .= " AND d.codconcepto <> 10012 ";
	//$sql .= " AND c.nroinscripcion = 3114 ";
	$sql .= " AND d.codtipodeuda NOT IN (3, 4, 7) " . $qls . " ";
	$sql .= "GROUP BY c.fechareg, c.coddocumento, c.serie, c.nrodocumento, cl.codtipodocumento, cl.nrodocumento, ";
	$sql .= " c.nroinscripcion, cl.propietario, c.impigv, c.fechavencimiento, cl.codantiguo, ";
	$sql .= " co.codconcepto, co.descripcion, co.afecto_igv ";
	$sql .= "ORDER BY c.nroinscripcion, co.afecto_igv DESC";

	$Dato = '';
	$consulta = $conexion->query($sql);
	$items = $consulta->fetchAll();
	$array_general = array();


	foreach($items as $item):

		if(!array_key_exists($item['nroinscripcion'], $array_general)):
			$array_general[$item['nroinscripcion']]                = array();
			$array_general[$item['nroinscripcion']]['propietario'] = strtoupper(utf8_decode($item['propietario']));
			$array_general[$item['nroinscripcion']]['codantiguo']  = $item['codantiguo'];
		endif;

		$array_general[$item['nroinscripcion']]['datos'][]  = array(
			'codconcepto'=>$item['codconcepto'],
			'concepto'=>$item['concepto'],
			'importe'=>$item['importe'],
		);

	endforeach;


	$objReporte = new clsLecturas("L");


	$tConceptos = array();
	$TotalC     = 0;
	$importe    = 0;
	$cabecera 	= 0;
	$contador 	= 0;
	$Total 	= 0;

	$objReporte->AliasNbPages();
	$objReporte->SetLeftMargin(65);
  $objReporte->SetAutoPageBreak(true,15);
	$objReporte->AddPage('H');

	foreach ($array_general as $ind_inscripcion => $data) :

		$propietario = $data['propietario'];
		$codantiguo = $data['codantiguo'];
		$contador++;

		$objReporte->contenidousuario($contador, $codantiguo, $propietario);
		$subtotalimportenoimpo = $subtotalimporteimpo = 0;

		foreach ($data['datos'] as $value) :

			$objReporte->contenido($value['codconcepto'], $value['concepto'], $value['importe']);
			$subtotalimporte += $value['importe'];

			if( $value['codconcepto'] == '1' OR $value['codconcepto'] == '2' OR $value['codconcepto'] == '6'):
				//Imponible
				$subtotalimporteimpo += $value['importe'];
			else:
				$subtotalimportenoimpo += $value['importe'];
			endif;

		endforeach;


		$objReporte->subpie("SUBTOTAL PARCIAL======>", $subtotalimporteimpo, $subtotalimportenoimpo);
		$totalimporteimpo += $subtotalimporteimpo;
		$totalimportenoimpo += $subtotalimportenoimpo;

	endforeach;

	$objReporte->Piefinal("TOTAL======>", $totalimportenoimpo, $totalimporteimpo);
	$objReporte->Output();

?>
