<?php 
    //setlocale(LC_ALL, 'nl_NL');
    session_name("pnsu");
	if(!session_start()){session_start();}


    include("../../../../objetos/clsFunciones.php");
    $clfunciones = new clsFunciones();

    // Actual
    $codemp   = 1;
    $anio     = $_POST['anio'];
    // $mes      = ( ($_POST['mes']-1) == 0 ) ? 12 : $_POST['mes']-1 ;
    $mes      = $_POST['mes']; ;
    $mestexto = $_POST['mestexto'];
    $ciclo    = $_POST['ciclo'];
    $codsuc   = $_POST['codsuc'];


    // Para obtener el periodo de faturacion
    $sqlLect = "SELECT nrofacturacion, nrotarifa
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp, $codsuc, $ciclo, $anio, $mes));
    $nrofact = $consultaL->fetch();


    $sqlLect1 = " SELECT nomtar FROM facturacion.tarifas WHERE codemp = ? AND codsuc = ? AND nrotarifa = ".$nrofact[1]." ORDER BY catetar;";
    $consultaL1 = $conexion->prepare($sqlLect1);
    $consultaL1->execute(array($codemp, $codsuc));
    $catetares = $consultaL1->fetchAll();

    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($_POST['mes'], $_POST['anio']);
    $primerdia = $clfunciones->obtener_primer_dia_mes($_POST['mes'], $_POST['anio']);

    // var_dump($primerdia);exit;
    $nombremes = $clfunciones->obtener_nombre_mes($mes);

    if(intval($mes)<10): $mesmod = $mes; else: $mesmod = $mes; endif;


	$Sql = "(
			SELECT 
			SUM((det.importe - det.importeacta) + det.importerebajado) as importe,
			1 as orden
			FROM facturacion.detctacorriente det
			WHERE det.codsuc = {$codsuc} 
			AND det.periodo = '".$anio.$mesmod."'
			AND det.tipo = 1 AND det.tipoestructura = 0 
			AND det.codtipodeuda <> 3 AND det.codconcepto <> 101 
			ORDER BY orden
			)
			UNION
			(
				SELECT 
				SUM((d.importe - d.importeacta) + d.importerebajado) as importe,
				2 as orden 
				FROM facturacion.cabctacorriente c
				INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) 
				WHERE d.codsuc = {$codsuc} AND 
				d.periodo = '".$anio.$mesmod."' AND d.codconcepto <> 101
				AND d.tipoestructura = 0 AND d.tipo = 0 AND d.codtipodeuda <> 3
				ORDER BY orden
			)
			ORDER BY orden";

    $Consulta  = $conexion->query($Sql);
    $row       = $Consulta->fetchAll();

    $cabecera[1] = array();

    foreach ($row as $value):

        if(empty($value['importe'])):
            $value['importe'] = 0;
        endif;

        if (!in_array($value['importe'], $cabecera)):
            array_push($cabecera[1], $value['importe']);
        endif;

    endforeach;

?>
<center> 
<table border="1" aling="center">
	<thead>
        <tr>
            <td width="40" align="center">N°</td>
            <td width="80" align="center"><?=substr($nombremes,0,3)."(Mes)";?></td>
            <td width="90" align="center">Saldo Anterior</td>
            <td width="90" align="center">Total de Saldo</td>
        </tr>
	<thead>  
    <tbody>
        <?php 
        $contador  = 0;
        foreach ($cabecera as $value): 
            $contador++;
        ?>
            <tr>
                <td align="center"><?=$contador;?></td>
                <td align="right" style="padding-right:5px;"><?=number_format($value[1] = empty($value[1]) ? 0 : $value[1],2);?></td>
                <td align="right" style="padding-right:5px;"><?=number_format($value[0] = empty($value[0]) ? 0 : $value[0],2);?></td>
                <td align="right" style="padding-right:5px;"><?=number_format(($value[0]+$value[1]),2);?></td>
            </tr>    
        <?php endforeach;?>
    </tbody>  
</table>
</center> 

