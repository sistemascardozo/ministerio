<?php

include("../../../../objetos/clsReporte.php");
$clfunciones = new clsFunciones();

set_time_limit(0);

//ini_set("memory_limit","1024M");

class clsLecturas extends clsReporte {

    public function Header() {

        global $codsuc, $meses;
        global $Dim;
        global $fechaact;
        global $mes;

        $periodo = $this->DecFechaLiteral($fechaact);
        $nommes = $this->obtener_nombre_mes($mes);

        $nombremes = (empty($periodo['mes'])) ? strtoupper($nommes) : $periodo['mes'];


        //DATOS DEL FORMATO
        $this->SetY(7);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "RESUMEN DE CONCEPTO DE SALDOS";
        $this->Cell(187, 5, utf8_decode($tit1), 0, 1, 'C');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(187, 5, "INFORME DE RESUMEN DE SALDO : " . $nombremes . " - " . $periodo["anio"], 0, 1, 'C');
        $this->Ln(1);
        $this->SetFont('Arial', '', 7);
        $hc = 5;
        $this->SetY(5);
        $this->SetFont('Arial', 'B', 10);
        $tit1 = "";
        $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
        //DATOS DEL FORMATO
        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoupper($empresa["razonsocial"]);
        $tit2 = strtoupper($empresa["direccion"]);
        $tit3 = "SUCURSAL: " . strtoupper($empresa["descripcion"]);
        $x = 23;
        $y = 5;
        $h = 3;
        $this->Image($_SESSION['path'] . "images/logo_empresa.jpg", 6, 1, 17, 16);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

        $this->SetY(10);
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');
        $this->Cell(0, 3, utf8_decode("Página    :   " . $this->PageNo() . ' de {nb}     ') . '', 0, 1, 'R');
        $this->Cell(0, 3, "Fecha     : " . $FechaActual . "   ", 0, 1, 'R');
        $this->Cell(0, 3, "Hora       : " . $Hora . " ", 0, 1, 'R');
        $this->SetY(17);
        $this->SetLineWidth(.1);
        //$this->Cell(0, .1, "", 1, 1, 'C', true);
        //            
        $this->cabecera();
    }

    public function cabecera() {

        global $Dim, $Dim1, $Dim2, $Dim3;

        $this->Ln(5);
        $this->SetFillColor(18, 157, 176);
        $this->SetTextColor(255);
        $h1 = 10;

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(20, $h1, utf8_decode('ITEM'), 1, 0, 'C', true);
        $this->Cell(100, $h1, utf8_decode('CONCEPTO'), 1, 0, 'C', true);
        $this->Cell(80, $h1, utf8_decode('IMPORTE'), 1, 1, 'C', true);
    }

    public function contenido($item, $conceptos, $importe, $valor = 0) {

        global $Dim;

        $this->SetFillColor(18, 157, 176);
        $this->SetTextColor(255);
        $h = 5;

        switch ($valor) {

            case 0:
                $this->SetTextColor(0);
                $this->Cell(20, $h, $item, 0, 0, 'C', false);
                $this->Cell(100, $h, utf8_decode($conceptos), 0, 0, 'C', false);
                $this->Cell(80, $h, number_format($importe, 2), 0, 1, 'R', false);
                break;

            case 1:
                $this->SetTextColor(255);
                $this->Cell(120, $h, utf8_decode($item), 0, 0, 'C', true);
                $this->Cell(80, $h, number_format($conceptos, 2), 0, 1, 'R', true);
                break;
        }
    }

}

$Dim = array('1' => 230, '2' => 40, '3' => 20, '4' => 50, '5' => 120, '6' => 40, '7' => 120, '8' => 70, '9' => 80);

$codemp = 1;
$codsuc = $_GET["codsuc"];
$ciclo = $_GET["ciclo"];
$anio = $_GET["anio"];
$mes = $_GET["mes"];
$texto = $_GET["mestexto"];
$valor = $_GET["valor"];
$idusuario = $_SESSION['id_user'];
$periodo = $_GET["anio"] . ($_GET['mes']);
$codconcepto = $_GET["codconcepto"];

$conagregar = '';

if ($codconcepto != '%'): $conagregar = " AND d.codconcepto = " . $codconcepto;
endif;

$total = 0;
$totalconv = 0;

$objReporte = new clsLecturas("L");
$fechaact = '01/' . $_GET['mes'] . "/" . $_GET['anio'];

$contador = 0;
$objReporte->AliasNbPages();
$objReporte->SetLeftMargin(50);
// $objReporte->SetAutoPageBreak(true,5);
$objReporte->AddPage('H');

$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes, $anio);
$primerdia = $clfunciones->obtener_primer_dia_mes($mes, $anio);

// Para obtener el periodo de faturacion

$sqlLect = "select nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
$consultaL = $conexion->prepare($sqlLect);
$consultaL->execute(array($codemp, $codsuc, $ciclo, $anio, $mes));
$nrofact = $consultaL->fetch();
if (intval($mes) < 10): $mesmod = $mes;
else: $mesmod = $mes;
endif;

if ($mesmod == '09' AND $anio == '2014'):

    $sqlLect = "SELECT 
                d.nroinscripcion as nroinscripcion,
                cli.propietario as propietario, 
                per.anio as anio, 
                per.mes as mes, 
                SUM(d.importe) as importe,
                d.codconcepto
                FROM facturacion.cabctacorriente c
                INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) 
                INNER JOIN catastro.clientes cli ON (cli.codemp = c.codemp AND cli.codsuc = c.codsuc AND cli.nroinscripcion = c.nroinscripcion)
                INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo)
                WHERE d.codsuc = {$codsuc} and d.periodo = '" . $anio . $mesmod . "'
                AND d.tipo = 1 AND d.tipoestructura = 0 " . $conagregar . "
                GROUP BY d.nroinscripcion, cli.propietario, per.anio, per.mes, d.codconcepto
                ORDER BY d.nroinscripcion, cli.propietario, per.anio, per.mes";

else:

    $sqlLect = "SELECT 
            d.nroinscripcion as nroinscripcion,
            cli.propietario as propietario, 
            d.nrofacturacion as nrofacturacion, 
            per.anio as anio, 
            to_char(CAST(per.mes AS INTEGER),'00') as mes, /*per.mes as mes, */
            SUM((d.importe - d.importeacta) + d.importerebajado) as importe ,
            to_char(cli.codsuc,'00')||''||TRIM(to_char(d.nroinscripcion,'000000')) as codantiguo,
            d.periodo,d.concepto,d.codconcepto
            FROM facturacion.cabctacorriente c
            INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) 
            INNER JOIN catastro.clientes cli ON (cli.codemp = c.codemp AND cli.codsuc = c.codsuc AND cli.nroinscripcion = c.nroinscripcion)
            INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo)
            WHERE d.codsuc = {$codsuc} and d.periodo = '" . $anio . $mes . "'
            AND d.tipo = 1 AND d.tipoestructura = 0  " . $conagregar . "
            AND d.codconcepto <> 101 AND d.codtipodeuda <> 3 -- AND d.nroinscripcion=335788
            GROUP BY d.nroinscripcion, cli.propietario, per.anio, per.mes, d.nrofacturacion,
            cli.codsuc,d.periodo,d.concepto,d.item,d.codconcepto
            ORDER BY d.nroinscripcion, cli.propietario, per.anio, per.mes,d.item    ";
endif;
$consultaL = $conexion->prepare($sqlLect);
$consultaL->execute();
$items = $consultaL->fetchAll();

$contador = 0;
$import = 0;
if (intval($mes) < 10): $mesmode = "0" . $mes;
else: $mesmode = $mes;
endif;
$nroins = '';
$nrofact = '';
$valore = array();
$it = 0;

foreach ($items as $key):

    $numeroinscripcion = $key['codantiguo'];
    if (intval($nroins) != intval($key['nroinscripcion']) || intval($nrofact) != intval($key['nrofacturacion'])) {
        if ($it == 1) {
            // $objReporte->contenido1("TOTAL PARCIAL ===> ", $import1, "",1);
            $import += $import1;
            $import1 = 0;
        }
        $contador1 = 0;

        $it = 0;
        $contador++;
        // $objReporte->contenido($contador, $numeroinscripcion, $key['propietario'], $key['anio'], $key['mes'],0);
        $contador1 ++;
        // $objReporte->contenido1($contador1, $key['concepto'], $key['importe']);
        $import1 += $key['importe'];

        // var_dump($key['codconcepto']);

        if (!array_key_exists($key['codconcepto'], $valore)):

            $valore[$key['codconcepto']] = array();
            array_push($valore[$key['codconcepto']], $key['concepto'], $key['importe']);
        else:
            $valore[$key['codconcepto']][1] += $key['importe'];
        endif;
    }
    else {
        $contador1 ++;
        // $objReporte->contenido1($contador1, $key['concepto'], $key['importe']);
        $import1 += $key['importe'];

        if (!array_key_exists($key['codconcepto'], $valore)):
            $valore[$key['codconcepto']] = array();
            array_push($valore[$key['codconcepto']], $key['concepto'], $key['importe']);
        else:
            $valore[$key['codconcepto']][1] += $key['importe'];
        endif;

        $it = 1;
    }

    $nroins = $key['nroinscripcion'];
    $nrofact = $key['nrofacturacion'];

endforeach;

// $objReporte->contenido1("TOTAL PARCIAL ===> ", $import1, "",1);
$import += $import1;
// $objReporte->contenido(strtoupper('TOTAL'),$import,'','','','',1);

ksort($valore);

$ite = 0;
$final = 0;

foreach ($valore as $key => $value):

    $ite ++;
    $objReporte->contenido($ite, $value[0], $value[1]);
    $final += $value[1];

endforeach;

$objReporte->contenido("TOTAL", $final, '', 1);
$objReporte->Output();

?>