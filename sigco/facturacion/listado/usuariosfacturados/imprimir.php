<?php 
	include("../../../../objetos/clsReporte.php");
	/*error_reporting(E_ERROR | E_WARNING | E_PARSE);
	error_reporting(E_ALL);
	ini_set("display_errors",1);*/
	ini_set("memory_limit","512M");
	class clsUsuario extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses,$anio,$mes;
			
			$this->SetFont('Arial','B',10);
			
			$h = 4;
			$this->Cell(190, $h+2,"USUARIO FACTURADOS",0,1,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(190, $h+2,$meses[$mes]." - ".$anio,0,1,'C');
			
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(20,13,65,40,13,10,12,12,11));
			$this->SetAligns(array("C","C","C","C","C","C","C","C", "C"));
			$this->Row(array("COD. CAT.",
							 "INSCRIP",
							 "USUARIO",
							 "DIRECCION",
							 "TS",
							 "CAT.",
							 "LECT. ANT.",
							 "LECT. ULT.",
							 "CONS"));
						
		}
		function Contenido($codcatastro,$inscrip,$usuario,$lectanterior,$lectultima,$consumo,$direccion,$servicio=2,$categoria)
		{
			$h=6;
			$this->SetFont('Arial','',7);
			
			$ts="";
			if($servicio==1)
			{
				$ts="Ag/Des";
			}
			if($servicio==2)
			{
				$ts="Ag";
			}
			if($servicio==3)
			{
				$ts="Des";
			}
			
			$this->SetWidths(array(20,13,65,40,13,10,12,12,11));
			$this->SetAligns(array("C","C","L","L","C","C","R","R","R"));
			$this->Row(array($codcatastro,
							 $inscrip,
							 $usuario,
							 $direccion,
							 $ts,
							 $categoria,
							 number_format($lectanterior,0),
							 number_format($lectultima,0),
							 number_format($consumo,0)));

		}
    }
	
	$ciclo		= $_GET["ciclo"];
	$sector		= "%".$_GET["sector"]."%";
	$consumo 	= $_GET["consumo"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$consumoh	= $_GET["consumohasta"];
	$tfact		= "%".$_GET["tfact"]."%";
	$tfactcard	= $_GET["tfact"];
	$codsuc		= $_GET["codsuc"];

	$objReporte	=	new clsUsuario();
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	
	$importe 	= 0;
	$count		= 0;
	if($_GET["sector"]=='%') $Csector="";
	else $Csector=" AND clie.codsector=".$_GET["sector"];

	if($_GET["tfact"]=='%') $CTipoFac="";
	else $CTipoFac=" AND clie.tipofacturacion=".$_GET["tfact"];


	if ($tfactcard=='1')
	{
		$sql  = "select clie.codantiguo, clie.propietario,cab.lecturaanterior,cab.lecturaultima,cab.lecturapromedio,
			(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,cab.codtiposervicio,
			upper(substring(tar.nomtar,1,3)) as nomtar,".$objReporte->getCodCatastral("clie.")." 
			from facturacion.cabfacturacion as cab 
			inner join catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion)
			inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
			inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc and cab.catetar=tar.catetar)
			where cab.anio=:anio and cab.mes=:mes ".$Csector.$CTipoFac." and cab.consumofact between :consumoini and :consumofin 
			order by codcatastro ";
	}elseif ($tfactcard=='%') 
	{
		$sql  = "select clie.codantiguo, clie.propietario,cab.lecturaanterior,cab.lecturaultima,cab.consumofact,
			(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,cab.codtiposervicio,
			upper(substring(tar.nomtar,1,3)) as nomtar,".$objReporte->getCodCatastral("clie.")." 
			from facturacion.cabfacturacion as cab 
			inner join catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion)
			inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
			inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc and cab.catetar=tar.catetar)
			where cab.anio=:anio and cab.mes=:mes ".$Csector.$CTipoFac." and cab.consumofact between :consumoini and :consumofin 
			order by codcatastro ";
	}else
	{
		$sql  = "select clie.codantiguo, clie.propietario,cab.lecturaanterior,cab.lecturaultima,cab.consumo,
			(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,cab.codtiposervicio,
			upper(substring(tar.nomtar,1,3)) as nomtar,".$objReporte->getCodCatastral("clie.")." 
			from facturacion.cabfacturacion as cab 
			inner join catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion)
			inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona )
			inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc and cab.catetar=tar.catetar)
			where cab.anio=:anio and cab.mes=:mes ".$Csector.$CTipoFac." and cab.consumofact between :consumoini and :consumofin 
			order by codcatastro ";
	}

	

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":anio"=>$anio,
							 ":mes"=>$mes,
							 ":consumoini"=>$consumo,
							 ":consumofin"=>$consumoh));
	$items = $consulta->fetchAll();
	
	foreach($items as $row)
	{
		//$importe+=$row["monto"];
		$count++;
		
		if($tfactcard=='1')
		{
			$objReporte->Contenido($row["codcatastro"], $row["codantiguo"], utf8_decode($row["propietario"]),intval($row["lecturaanterior"]),intval($row["lecturaultima"]),intval($row["lecturapromedio"]),
						utf8_decode(strtoupper($row["direccion"])),$row["codtiposervicio"],$row["nomtar"]);
		}elseif ($tfactcard=='%') 
		{
			$objReporte->Contenido($row["codcatastro"], $row["codantiguo"], utf8_decode($row["propietario"]),intval($row["lecturaanterior"]),intval($row["lecturaultima"]),intval($row["consumofact"]),
						utf8_decode(strtoupper($row["direccion"])),$row["codtiposervicio"],$row["nomtar"]);
		}else
		{
			$objReporte->Contenido($row["codcatastro"], $row["codantiguo"], utf8_decode($row["propietario"]),intval($row["lecturaanterior"]),intval($row["lecturaultima"]),intval($row["consumo"]),
						utf8_decode(strtoupper($row["direccion"])),$row["codtiposervicio"],$row["nomtar"]);
		}	
	}

	$objReporte->Output();	
	
?>