<?php
session_name("pnsu");
if(!session_start()){session_start();}
	
	set_time_limit(0);
	
	include("../../../../objetos/clsReporte.php");

    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{
		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo;
			// global $fechaapertura

			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"RESUMEN DE FACTURACION POR CATEGORIAS",0,1,'C');
			$this->Ln(3);

			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			$this->Cell(40, $h,strtoupper("Mes de Referencia"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');
			$this->Ln(3);

		}

		public function cabecera1(){

			global $Dim;
			$h = 10;
			$this->SetFont('Arial','B',8);
			$this->Cell($Dim[0],$h,'',0,0,'C',false);
			$this->Cell($Dim[1],$h,utf8_decode('CATEGORIA'),0,0,'L',false);
			$this->Cell($Dim[2],$h,utf8_decode('NRO. DE USUARIOS'),0,0,'C',false);
			$this->Cell($Dim[3],$h,utf8_decode('VOLUMEN FACTURADO'),0,0,'R',false);
			$this->Cell($Dim[4],$h,utf8_decode('IMPORTES'),0,1,'R',false);
			$this->Ln(3);

		}

		public function contenido($categoria, $usuario, $volumen, $importe){

			global $Dim;

			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','',8);

			$this->Cell($Dim[0],$h,"",0,0,'C');
			$this->Cell($Dim[1],$h,utf8_decode(strtoupper($categoria)),0,0,'L');
			$this->Cell($Dim[2],$h,number_format($usuario,0),0,0,'R');
			$this->Cell($Dim[3],$h,number_format($volumen,2),0,0,'R');
			$this->Cell($Dim[4],$h,number_format($importe,2),0,1,'R');
		}

		public function piecontenido($total_nombre, $total_usuario, $total_voumen, $total_importe){

			global $Dim;

			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->Ln(2);

			$this->SetFont('Arial','B',8);
			$this->Cell($Dim[0],$h,"",0,0,'C');
			$this->Cell($Dim[1],$h,utf8_decode(strtoupper($total_nombre)),0,0,'C');
			$this->Cell($Dim[2],$h,number_format($total_usuario,0),0,0,'R');
			$this->Cell($Dim[3],$h,number_format($total_voumen,0),0,0,'R');
			$this->Cell($Dim[4],$h,number_format($total_importe,2),0,1,'R');

		}

		//  Para los cuadros con bordes redondeados o sin ellos
		public function RoundedRect($x, $y, $w, $h, $r, $style = '') {
			$k = $this->k;
			$hp = $this->h;
			if($style=='F')
				$op='f';
			elseif($style=='FD' || $style=='DF')
				$op='B';
			else
				$op='S';
			$MyArc = 4/3 * (sqrt(2) - 1);
			$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
			$xc = $x+$w-$r ;
			$yc = $y+$r;
			$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

			$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
			$xc = $x+$w-$r ;
			$yc = $y+$h-$r;
			$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
			$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
			$xc = $x+$r ;
			$yc = $y+$h-$r;
			$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
			$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
			$xc = $x+$r ;
			$yc = $y+$r;
			$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
			$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
			$this->_out($op);
		}

		public function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
		 $h = $this->h;
		 $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
		 $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
		}

	}

    $Dim = array('0'=>5,'1'=>55,'2'=>30,'3'=>45,'4'=>45);

	$codemp     = 1;
	$codsuc     = $_GET["codsuc"];
	$ciclo      = $_GET["ciclo"];
	$anio       = $_GET["anio"];
	$mes        = intval($_GET["mes"]);
	$texto      = $_GET["mestexto"];
	$valor      = $_GET["valor"];
	$idusuario  = $_SESSION['id_user'];
	$periodo    = $_GET["anio"].($_GET['mes']);

	$objReporte = new clsLecturas("L");
	$contador   = 0;
	$objReporte->AliasNbPages();
	$objReporte->cabecera();
	$objReporte->AddPage('P');

	$objReporte->SetFillColor(255, 255, 255);
	$objReporte->RoundedRect(12, 40, 180, 10, 0.5, 'DF');
	$objReporte->cabecera1();

	$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
	$primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);

	// Para obtener el periodo de faturacion
	$sqlLect = "SELECT nrofacturacion, nrotarifa ";
	$sqlLect .= "FROM facturacion.periodofacturacion ";
	$sqlLect .= "WHERE codemp = ".$codemp." ";
	$sqlLect .= " AND codsuc = ".$codsuc." ";
	$sqlLect .= " AND codciclo = ".$ciclo." ";
	$sqlLect .= " AND anio = '".$anio."' ";
	$sqlLect .= " AND mes = '".$mes."' ";
	
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array());
	$nrofact = $consultaL->fetch();

	$array_general = array();

	// Todo los Conceptos
	$sqlLect1 = "SELECT t.codcategoriatar AS codcategoriatar, ct.descripcion AS categoria, SUM(d.importe) AS importe ";
	$sqlLect1 .= "FROM facturacion.detctacorriente d ";
	$sqlLect1 .= " JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto) ";
	$sqlLect1 .= " JOIN facturacion.cabctacorriente AS cd ON (d.codemp = cd.codemp) AND (d.codsuc = cd.codsuc) AND (d.codciclo = cd.codciclo) ";
	$sqlLect1 .= "  AND (d.nrofacturacion = cd.nrofacturacion) AND (d.nroinscripcion = cd.nroinscripcion) AND (d.anio = cd.anio) AND (d.mes = cd.mes) ";
	$sqlLect1 .= "  AND (d.periodo = cd.periodo) AND (d.tipo = cd.tipo) AND (d.tipoestructura = cd.tipoestructura) ";
	$sqlLect1 .= " JOIN facturacion.tarifas AS t ON (d.codemp = t.codemp) AND (d.codsuc = t.codsuc) AND (cd.catetar = t.catetar) ";
	$sqlLect1 .= " JOIN facturacion.categoriatarifaria AS ct ON (t.codcategoriatar = ct.codcategoriatar) ";
	$sqlLect1 .= "WHERE d.codsuc = ? ";
	$sqlLect1 .= " AND d.nrofacturacion = ".$nrofact['nrofacturacion']." ";
	$sqlLect1 .= " AND ((d.nrocredito = 0 AND d.nrorefinanciamiento = 0) OR d.codtipodeuda = 9 OR d.codconcepto = 9 OR d.codtipodeuda in (3, 4, 7, 9)) ";
	$sqlLect1 .= " AND d.codconcepto <> 10012 ";
	$sqlLect1 .= " AND d.codtipodeuda NOT IN(3, 4, 7) ";
	$sqlLect1 .= " AND d.tipo = 0 ";
	$sqlLect1 .= " AND d.tipoestructura = 0 ";
	$sqlLect1 .= " AND d.periodo = '".$anio.$mes."' ";
	$sqlLect1 .= " AND t.nrotarifa = ".$nrofact['nrotarifa']." ";
	$sqlLect1 .= "GROUP BY t.codcategoriatar, ct.descripcion ";
	$sqlLect1 .= "ORDER BY t.codcategoriatar, ct.descripcion";

	$consultaL = $conexion->prepare($sqlLect1);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetchAll();
	$array_general = array();

	foreach ($items as $val) :

		if(!array_key_exists($val['codcategoriatar'],$array_general)):
			$array_general[$val['codcategoriatar']] = array();
			$array_general[$val['codcategoriatar']]['nombre']  = $val['categoria'];

		endif;

		$val['importe'] = (empty($val['importe'])) ? 0.00 : $val['importe'];

		$array_general[$val['codcategoriatar']]['importe'] = $val['importe'];

	endforeach;


	// Cuota de Convenio
	$sqlLect = "SELECT t.codcategoriatar AS codcategoriatar, ct.descripcion AS categoria, SUM(d.importe) AS importe ";
	$sqlLect .= "FROM facturacion.detctacorriente d ";
	$sqlLect .= " JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto) ";
	$sqlLect .= " JOIN facturacion.cabctacorriente AS cd ON (d.codemp = cd.codemp) AND (d.codsuc = cd.codsuc) AND (d.codciclo = cd.codciclo) ";
	$sqlLect .= "  AND (d.nrofacturacion = cd.nrofacturacion) AND (d.nroinscripcion = cd.nroinscripcion) ";
	$sqlLect .= "  AND (d.anio = cd.anio) AND (d.mes = cd.mes) AND (d.periodo = cd.periodo) AND (d.tipo = cd.tipo) ";
	$sqlLect .= "  AND (d.tipoestructura = cd.tipoestructura) ";
	$sqlLect .= " JOIN facturacion.tarifas AS t ON (d.codemp = t.codemp) AND (d.codsuc = t.codsuc) AND (cd.catetar = t.catetar) ";
	$sqlLect .= " JOIN facturacion.categoriatarifaria AS ct ON (t.codcategoriatar = ct.codcategoriatar) ";
	$sqlLect .= "WHERE d.codsuc = ? ";
	$sqlLect .= " AND d.periodo='".$anio.$mes."' ";
	$sqlLect .= " AND d.tipo = 0 ";
	$sqlLect .= " AND d.codconcepto <> 101 ";
	$sqlLect .= " AND d.codtipodeuda IN(3, 4, 7) ";
	$sqlLect .= " AND d.tipoestructura = 0 ";
	$sqlLect .= " AND t.nrotarifa = ".$nrofact['nrotarifa']." ";
	$sqlLect .= "GROUP BY t.codcategoriatar, ct.descripcion ";
	$sqlLect .= "ORDER BY t.codcategoriatar, ct.descripcion";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetchAll();

	foreach ($items as $val) :

		if(!array_key_exists($val['codcategoriatar'],$array_general)):
			$array_general[$val['codcategoriatar']] = array();
			$array_general[$val['codcategoriatar']]['nombre']  = $val['categoria'];
		endif;

		$val['importe'] = (empty($val['importe'])) ? 0.00 : $val['importe'];

		$array_general[$val['codcategoriatar']]['importe'] += $val['importe'];

	endforeach;

	// Volumen Facturado
	$sqlLect = "SELECT t.codcategoriatar AS codcategoriatar, ct.descripcion AS categoria, SUM(cd.consumofact) AS volumen ";
	$sqlLect .= "FROM facturacion.cabctacorriente AS cd ";
	$sqlLect .= " JOIN facturacion.tarifas AS t ON (cd.codemp = t.codemp) AND (cd.codsuc = t.codsuc) AND (cd.catetar = t.catetar) ";
	$sqlLect .= " JOIN facturacion.categoriatarifaria AS ct ON (t.codcategoriatar = ct.codcategoriatar) ";
	$sqlLect .= "WHERE cd.codsuc = ? ";
	$sqlLect .= " AND cd.periodo = '".$anio.$mes."' ";
	$sqlLect .= " AND cd.tipo = 0 ";
	$sqlLect .= " AND cd.tipoestructura = 0 ";
	$sqlLect .= " AND t.nrotarifa = ".$nrofact['nrotarifa']." ";
	$sqlLect .= "GROUP BY t.codcategoriatar, ct.descripcion ";
	$sqlLect .= "ORDER BY t.codcategoriatar, ct.descripcion";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetchAll();

	foreach ($items as $val) :

		if(!array_key_exists($val['codcategoriatar'],$array_general)):
			$array_general[$val['codcategoriatar']] = array();
			$array_general[$val['codcategoriatar']]['nombre']  = $val['categoria'];
		endif;

		$val['volumen'] = (empty($val['volumen'])) ? 0.00 : $val['volumen'];
		$array_general[$val['codcategoriatar']]['volumen'] += $val['volumen'];

	endforeach;

	// Numero de Usuarios
	$sqlLect = "SELECT t.codcategoriatar AS codcategoriatar, ct.descripcion AS categoria, COUNT(*) AS cantidad ";
	$sqlLect .= "FROM facturacion.cabfacturacion cab ";
	$sqlLect .= " JOIN facturacion.tarifas AS t ON (cab.codemp = t.codemp) AND (cab.codsuc = t.codsuc) AND (cab.catetar = t.catetar) ";
	$sqlLect .= " JOIN facturacion.categoriatarifaria AS ct ON(t.codcategoriatar = ct.codcategoriatar) ";
	$sqlLect .= "WHERE cab.codsuc = ? ";
	$sqlLect .= " AND cab.nrofacturacion = ? ";
	$sqlLect .= " AND cab.nrodocumento > 0 ";
	$sqlLect .= " AND t.nrotarifa = ".$nrofact['nrotarifa']." ";
	$sqlLect .= "GROUP BY t.codcategoriatar, ct.descripcion ";
	$sqlLect .= "ORDER BY t.codcategoriatar, ct.descripcion ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc, $nrofact['nrofacturacion']));
	$items = $consultaL->fetchAll();

	foreach ($items as $val) :

		if(!array_key_exists($val['codcategoriatar'],$array_general)):
			$array_general[$val['codcategoriatar']] = array();
			$array_general[$val['codcategoriatar']]['nombre']  = $val['categoria'];
		endif;

		$val['cantidad'] = (empty($val['cantidad'])) ? 0.00 : $val['cantidad'];
		$array_general[$val['codcategoriatar']]['numero_usuarios'] = $val['cantidad'];

	endforeach;


	$suma_importe = $suma_volumen = $suma_usuarios = 0;

	foreach($array_general as $ind => $rowD):


		$objReporte->contenido($rowD['nombre'], $rowD['numero_usuarios'], $rowD['volumen'], $rowD['importe']);
		$suma_usuarios          += $rowD['numero_usuarios'];
		$suma_volumen          += $rowD['volumen'];
		$suma_importe          += $rowD['importe'];


	endforeach;

	$objReporte->Ln(1);
	$objReporte->Line(12, $objReporte->GetY(), 192, $objReporte->GetY(),10);
	$objReporte->piecontenido("TOTALES ==> ", $suma_usuarios, $suma_volumen, $suma_importe);



	$objReporte->Output();

?>
