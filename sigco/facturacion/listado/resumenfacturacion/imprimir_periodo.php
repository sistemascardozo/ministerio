<?php
	include("../../../../objetos/clsReporte.php");

    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{
		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo;
			// global $fechaapertura

			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"RESUMEN DETALLADO DE FACTURACION",0,1,'C');
			$this->Ln(3);

			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			$this->Cell(40, $h,strtoupper("Mes de Referencia"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');
			$this->Ln(3);
		}

		public function contenido($concepto, $valor, $res = 0, $suplemento = '', $cab = 0, $estado ='' ){

			global $Dim;
			if ($cab == 1 AND $estado == 0):

				$h = 8;
				$this->SetFont('Arial','B',8);
				$this->Cell($Dim[8],$h,utf8_decode('NROINSCRIPCION'),1,0,'C',false);
				$this->Cell($Dim[9],$h,utf8_decode('NRO DOCUMENTO'),1,0,'C',false);
				$this->Cell($Dim[10],$h,utf8_decode('IMPORTE'),1,1,'C',false);
				$this->Ln(5);

			else:
				if($cab == 2):
					$h = 10;
					$this->SetFont('Arial','B',10);
					$this->Cell($Dim[1],$h,utf8_decode('DESCRIPCION DEL CONCEPTO'),1,0,'C',false);
					$this->Cell($Dim[5],$h,utf8_decode('AFECTO'),1,0,'C',false);
					$this->Cell($Dim[5],$h,utf8_decode('INAFECTO'),1,1,'C',false);
					$this->Ln(5);
				endif;

			endif;

			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','',8);

			switch ($res) {

				case 0:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;

				case 1:
					$this->Ln(2);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;
				case 2:
					$this->SetTextColor(0);
					$this->Cell($Dim[3],$h,utf8_decode("____________________________________________________________________________________________________________"),0,1,'L',true);
					$this->Ln(5);
					$this->SetFont('Arial','B',6);
					$this->Cell($Dim[1],$h,utf8_decode("HORA INICIAL        " . $concepto),0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("HORA FINAL          " . $valor),0,1,'C',true);
					break;
				case 3:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("__________________"),0,1,'R',true);

					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
				case 4:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("_________________________"),0,1,'R',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
				case 5:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);

					break;
				case 6:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,0)),0,0,'R',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					break;
				case 7:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,"",0,0,'C',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					break;
				case 8:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'R',true);
					$this->Cell($Dim[5],$h,utf8_decode("______________________________________"),0,1,'L',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($suplemento,2)),0,1,'R',true);
					break;
				case 9:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h," ",0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					break;
				case 10:
					$this->SetTextColor(0);
					$this->Cell($Dim[8],$h,utf8_decode($concepto),0,0,'C',true);
					$this->Cell($Dim[9],$h,utf8_decode($suplemento),0,0,'C',true);
					$this->Cell($Dim[10],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					break;
				case 11:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[7],$h,utf8_decode($concepto),0,0,'C',true);
					$this->Cell($Dim[10],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					break;
				case 15:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'L',true);
					$this->Cell($Dim[5],$h,utf8_decode("__________________________"),0,1,'L',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;
				case 16:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,0)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;
			}
		}
	}

    $Dim = array('1'=>90,'2'=>60,'3'=>190,'4'=>20,'5'=>25,'6'=>25,'7'=>140,'8'=>40,'9'=>80,'10'=>50);

	$codemp     = 1;
	$codsuc     = $_GET["codsuc"];
	$ciclo      = $_GET["ciclo"];
	$anio       = $_GET["anio"];
	$mes        = intval($_GET["mes"]);
	$texto      = $_GET["mestexto"];
	$valor      = $_GET["valor"];
	$idusuario  = $_SESSION['id_user'];
	$periodo    = $_GET["anio"].($_GET['mes']);

	$total     = 0;
	$TotalInt  = 0;
	$totalconv = 0;
	$deuda     = 0;

	$objReporte = new clsLecturas("L");
	$contador   = 0;
	$objReporte->AliasNbPages();
	$objReporte->cabecera();
	$objReporte->AddPage('P');

	$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
	$primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);

	// Para obtener el periodo de faturacion

	$sqlLect = "select nrofacturacion
		FROM facturacion.periodofacturacion
		WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
	$nrofact = $consultaL->fetch();

	// Para servicio de fonavi

	//$sqlLect = "Select sum(importe) from facturacion.detctacorriente
//				where codsuc = ?
//				AND periodo='".$anio.$mes."'
//				AND tipo = 0 AND tipoestructura = 0 AND
//				codconcepto = 101 and codtipodeuda <> 3 --Diferente de Refinanciamiento";
//	$consultaL = $conexion->prepare($sqlLect);
//	$consultaL->execute(array($codsuc));
//	$items = $consultaL->fetch();
//
//	$items['sum'] = (empty($items['sum'])) ? 0 : $items['sum'];
//	$objReporte->contenido("FONAVI",$items['sum'],7,'',2);
//	// $total     += $items['sum'];
//	$totalconv += $items['sum'];

	// Para servicio de agua
	$sqlLect1 = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ?
				AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				codconcepto = 1 AND codtipodeuda <> 3 --Diferente de Refinanciamiento";

	$consultaL = $conexion->prepare($sqlLect1);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$restar=isset($Conceptos2[1]["Importe"])?$Conceptos2[1]["Importe"]:0;
	$items['sum']= $items['sum']-$restar;
	$total += $items['sum'];

	$objReporte->contenido("SERVICIO DE AGUA",$items['sum']);

	// Para servicio de desague
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
		where codsuc = ?
		AND periodo='".$anio.$mes."'
		AND tipo = 0 AND tipoestructura = 0 AND
		codconcepto = 2 and codtipodeuda <>3 --Diferente de Refinanciamiento";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$restar=isset($Conceptos2[2]["Importe"])?$Conceptos2[2]["Importe"]:0;
	$items['sum']= $items['sum']-$restar;
	$total += $items['sum'];

	$objReporte->contenido("SERVICIO DE DESAGUE",$items['sum']);
	
	// Para servicio de cargo fijo
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ?
				AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				codconcepto = 6 and codtipodeuda <> 3 --Diferente de Refinanciamiento";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$restar=isset($Conceptos2[6]["Importe"])?$Conceptos2[6]["Importe"]:0;
	$items['sum']= $items['sum']-$restar;
	$total += $items['sum'];

	$objReporte->contenido("CARGO FIJO",$items['sum']);
	
	// Para Servicios Dinamicos

	$totalalca = 0;

	$alca = "(SELECT
				CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)) as importe,
				co.codconcepto as codconcepto,
				co.descripcion, 1, co.categoria
				FROM  cobranza.cabprepagos c
				INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
				AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
				AND (c.nroinscripcion = d.nroinscripcion)
				INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
				AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
				WHERE c.codsuc = {$codsuc}
			  	AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
			  	AND c.estado<>0 AND c.estado<>2
				AND d.coddocumento IN (13,14)
				AND co.codconcepto <> 10005
				GROUP BY co.codconcepto, co.descripcion, co.categoria
			)
			UNION
			(
				SELECT
		   		CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)) as importe,
				co.codconcepto as codconcepto,
				co.descripcion, 2, co.categoria
				FROM
				cobranza.cabpagos c
				INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
				AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion)
				INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
				WHERE c.codsuc = {$codsuc} AND c.anulado = 0
				AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
				AND d.coddocumento IN (13,14)
				AND co.codconcepto <> 10005 AND c.car <> 9
				GROUP BY co.codconcepto, co.descripcion, co.categoria
			)
			UNION
			(
				SELECT CAST(sum(CASE WHEN d.importe <= 0 AND c.signo='-' THEN d.importe*(-1)  ELSE d.importe END) AS NUMERIC (18,2)) as importe,
				c.codconcepto as codconcepto,
				c.descripcion, 3, c.categoria
				FROM  facturacion.detctacorriente d
			  	INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp AND d.codsuc = c.codsuc AND d.codconcepto = c.codconcepto)
			  	WHERE d.codsuc={$codsuc}
				and ((d.nrocredito = 0 and d.nrorefinanciamiento = 0) OR d.codtipodeuda=9)
				AND d.tipo=0 AND d.tipoestructura=0 AND d.periodo='{$periodo}'
				AND d.codconcepto <> 10012
				AND d.codconcepto <> 1
				AND d.codconcepto <> 2
				/*AND d.codconcepto <> 3*/
				/*AND d.codconcepto <> 4*/
				/*AND d.codconcepto <> 9
				AND d.codconcepto <> 72*/
				AND d.codconcepto <> 8
				AND d.codconcepto <> 7
				/*AND d.codconcepto <> 10009*/
				AND d.codconcepto <> 6
				AND d.codconcepto <> 5 AND c.categoria<>3 and d.codtipodeuda not in (3, 4, 7)
				GROUP BY c.codconcepto, c.descripcion, c.categoria
				)
				ORDER BY codconcepto";
//(3, 4, 9, 72, 10009)*/ c.categoria=3
				//die($alca);
	$consultacol = $conexion->query($alca);
	$alcas = $consultacol->fetchAll();


	// $arrayconceptos = array();
	// $arrayunico = array();
	$Conceptos = array();


	foreach($alcas as $row)
	{
		if (!array_key_exists($row['codconcepto'], $Conceptos))
		{
			$Conceptos[$row['codconcepto']] = array(
													"Id"=>$row['codconcepto'],
													"Importe"=>$row[0],
													"Descripcion"=>$row[2],
													"Categoria"=>$row[4]
													);
		}
		else
		{
			$Conceptos[$row['codconcepto']] = array(
													"Id"=>$row['codconcepto'],
													"Descripcion"=>$row[2],
													"Importe"=>$Conceptos[$row['codconcepto']]["Importe"] + $row[0],
													"Categoria"=>$row[4]);
		}
	}


	sort($Conceptos);
	$cont=0;
	$TotalDebe=0;
	$contotal =0;
	$TotalIntR = 0;
	$TotIGV = 0;
	
	for($i=0;$i<count($Conceptos);$i++)
	{
		$cont++;
		$concepto = $Conceptos[$i]["Descripcion"];
		$TotalConcepto = $Conceptos[$i]["Importe"];
		
		if ($Conceptos[$i]["Id"] == 5)
		{
			$TotIGV += $TotalConcepto;
		}
		else
		{
			if ($Conceptos[$i]["Categoria"] == 3 or $Conceptos[$i]["Categoria"] == 4 or $Conceptos[$i]["Categoria"] == 7)
			{
				$objReporte->contenido($concepto, $TotalConcepto, 7);
				
				$TotalIntR += $TotalConcepto;
			}
			else
			{
				$objReporte->contenido($concepto, $TotalConcepto);
				
				$contotal += $TotalConcepto;
			}
		}
	}
	// var_dump($contotal);exit;
	$total += $contotal;


	// Para servicio de RECUPERO X CONSUMO AGUA NO FACT
	// $sqlLect = "select sum(importe) from facturacion.detctacorriente
	// 			where codsuc = ? AND nrofacturacion = ?
	// 			AND  codconcepto in (809, 814, 816, 818) AND codtipodeuda <> 3";
	// 4090.86
	//	1000.00
	// $consultaL = $conexion->prepare($sqlLect);
	// $consultaL->execute(array($codsuc,$nrofact['nrofacturacion']));
	// $items = $consultaL->fetch();
	// $total += $items['sum'];

	// $objReporte->contenido("RECUPERO X CONSUMO AGUA NO FACT",$items['sum']);

	// Para servicio de CONSUMO DE AGUA OBRA
	// $sqlLect = "select sum(importe) from facturacion.detctacorriente
	// 			where codsuc = ? and nrofacturacion = ?
	// 			and  codconcepto in (10008) and codtipodeuda <> 3";

	// $consultaL = $conexion->prepare($sqlLect);
	// $consultaL->execute(array($codsuc,$nrofact['nrofacturacion']));
	// $items = $consultaL->fetch();
	// $total += $items['sum'];

	// $objReporte->contenido("CONSUMO DE AGUA OBRA",$items['sum']);

	// Para servicio de INTERESES X DEUDA
	$sqlLect = "select sum(d.importe) from facturacion.detctacorriente d
	INNER JOIN facturacion.conceptos c on (c.codemp=d.codemp AND c.codsuc=d.codsuc and c.codconcepto=d.codconcepto)
				where d.codsuc = ?
				AND tipo = 0 AND
				((nrocredito = 0 and nrorefinanciamiento = 0) OR c.codconcepto = 9 OR codtipodeuda=9) AND
				c.categoria=3  and d.codtipodeuda not in (3, 4, 7)
				AND d.tipo=0 AND d.tipoestructura=0 AND d.periodo='".$anio.$mes."'  --Diferente de Refinanciamiento";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	
	//$total += $items['sum'];
	$TotalInt += $items['sum'];
	
	$Sql="SELECT  CAST(SUM(d.interes) AS NUMERIC (18,2)) AS sum
			FROM   facturacion.cabcreditos c
			INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp)
			AND (c.codsuc = d.codsuc)  AND (c.nrocredito = d.nrocredito)
			  WHERE c.codsuc=".$codsuc." AND c.estareg=1 AND c.sininteres=0 and c.interes>0 AND c.nroprepago>0 AND c.nroprepagoinicial>0
			  AND  extract(year from c.fechareg) ='".$anio."'
			  AND extract(month from c.fechareg)  ='".intval($mes)."'";
	$items = $conexion->query($Sql)->fetch();
	
	//$total += $items['sum'];
	$TotalInt += $items['sum'];
	
	//FINANCIAMIENTOS
	$Sql="SELECT  CAST(SUM(d.importe)-c.totalrefinanciado AS  NUMERIC (18,2)) AS sum,c.nroinscripcion
			FROM   facturacion.cabrefinanciamiento c
		  INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp)
		  AND (c.codsuc = d.codsuc)
		  AND (c.nrorefinanciamiento = d.nrorefinanciamiento)
		where d.codemp=1 and d.codsuc=".$codsuc." AND c.estareg=1 AND c.sininteres=0 AND c.nroprepagoinicial>0
		AND  extract(year from c.fechaemision) ='".$anio."'
		AND extract(month from c.fechaemision)  ='".intval($mes)."' AND c.codestadorefinanciamiento<>5
		  GROUP BY c.nroinscripcion,c.totalrefinanciado
		  ";
	//die($Sql);
	$Consulta = $conexion->query($Sql);
	foreach($Consulta->fetchAll() as $items)
	{
		//$total += $items['sum'];
		$TotalInt += $items['sum'];
	}
	$objReporte->contenido("INTERESES POR DEUDA", $TotalInt, 7);


	// Para servicio de CONVENIO EMPRESA -- diferente
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
	where codsuc = ?
	AND periodo='".$anio.$mes."'
	AND tipo = 0 AND tipoestructura = 0 AND
	codconcepto <> 101 AND codtipodeuda in (3, 4, 7) ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items     = $consultaL->fetch();
	
	$items['sum'] = $items['sum'];
	$convenio  =  $items['sum'];
	$totalconv += $convenio;

	$objReporte->contenido("CONVENIO EMPRESA",$items['sum'],7);
	$objReporte->contenido("TOTALES", $total, 8, $totalconv + $TotalInt + $TotalIntR);
	// $objReporte->contenido("dfg",,9);

	// Para servicio de IGV
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ?
				AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				codconcepto = 5 ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	
	$total += $items['sum'] + $TotIGV + $totalconv + $TotalInt + $TotalIntR;

	$objReporte->contenido("IGV", $items['sum'] + $TotIGV);

	// Para servicio de REDONDEO POSITIVO
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ?
				AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				((nrocredito = 0 and nrorefinanciamiento = 0) OR codconcepto = 9 OR codtipodeuda =9)
				and codtipodeuda not in (3, 4, 7) AND
				codconcepto = 8";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	
	$total += $items['sum'];

	$objReporte->contenido("REDONDEO POSITIVO", $items['sum']);

	// Para servicio de REDONDEO NEGATIVO
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ?
				AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0
				and ((nrocredito = 0 and nrorefinanciamiento = 0) OR codconcepto = 9 OR codtipodeuda =9)
				and codtipodeuda not in (3, 4, 7) AND
				codconcepto = 7 ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$objReporte->contenido("REDONDEO NEGATIVO", $items['sum']);
	
	$total += $items['sum'];

	// Para servicio de FACTURACION TOTAL DEL MES

	$objReporte->contenido("FACTURACION TOTAL DEL MES",$total,15);

	// // Para servicio de DEUDA ANTERIOR
	// $sqlLect = "select sum(d.importe - (d.importeacta + d.importerebajado))
	// 			FROM facturacion.cabctacorriente c
	// 			  INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp)
	// 			  AND (c.codsuc = d.codsuc)
	// 			  AND (c.codciclo = d.codciclo)
	// 			  AND (c.nrofacturacion = d.nrofacturacion)
	// 			  AND (c.nroinscripcion = d.nroinscripcion)
	// 			  AND (c.anio = d.anio)
	// 			  AND (c.mes = d.mes)
	// 			  AND (c.periodo = d.periodo)
	// 			  AND (c.tipo = d.tipo)
	// 			  AND (c.tipoestructura = d.tipoestructura)
	// 			where d.codsuc = ? and d.periodo = ? and d.categoria = 1
	// 			AND d.estadofacturacion = 1
	// 			AND d.codtipodeuda <> 3 and d.codtipodeuda <> 4
	// 			and d.codtipodeuda <> 6 and d.codtipodeuda <> 7 AND d.codtipodeuda <> 9
	// 			AND d.codtipodeuda <> 10  AND d.codtipodeuda <> 11
	// 			AND c.nrodocumento<>0 AND c.tipo = 1 AND c.tipoestructura=0 ";

	// $consultaL = $conexion->prepare($sqlLect);
	// $consultaL->execute(array($codsuc,$periodo));
	// $items = $consultaL->fetch();

	// Modificaciones po saldo 23-01-2014

	$mess = $mes;
	$mess  = str_pad($mess, 2, 0, STR_PAD_LEFT);

	$sqlLect = "SELECT SUM((d.importe - d.importeacta) + d.importerebajado) as importe
				FROM facturacion.detctacorriente d
				WHERE codsuc = $codsuc AND periodo = '$anio$mes'
				AND d.codconcepto <> 101
				AND d.tipoestructura = 0
				AND d.tipo = 1
				AND d.codtipodeuda <> 3 ";

	$consultaL1 = $conexion->prepare($sqlLect);
	$consultaL1->execute();
	$items = $consultaL1->fetch();


	$objReporte->contenido("DEUDA ANTERIOR",$items['importe']);

	// Para servicio de ANTICIPO RECIBIDO
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ?
				AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				codconcepto = 10005  ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();

	$objReporte->contenido("ANTICIPO RECIBIDO",$items['sum']);

	// Para servicio de Nro Conexiones
	/*
	$sqlLect = "select count(codestadoservicio) from facturacion.cabctacorriente c
	INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp)
	AND (c.codsuc         = d.codsuc)
	AND (c.codciclo       = d.codciclo)
	AND (c.nrofacturacion = d.nrofacturacion)
	AND (c.nroinscripcion = d.nroinscripcion)
	AND (c.anio           = d.anio)
	AND (c.mes            = d.mes)
	AND (c.periodo        = d.periodo)
	AND (c.tipo           = d.tipo)
	AND (c.tipoestructura = d.tipoestructura)
	where c.codsuc = ? and c.periodo = ?
	AND c.tipo = 0 AND c.tipoestructura = 0
	AND d.codconcepto = 6 --Activos ";
	*/

	$sqlLect = "select count(*) from facturacion.cabfacturacion
				where codsuc = ? and nrofacturacion = ? and nrodocumento > 0 ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc,$nrofact['nrofacturacion']));
	$items = $consultaL->fetch();

	$objReporte->contenido("NRO CONEXIONES",$items['count'],16);


	// Para servicio de VOLUMEN FACTURADO
	/*
	$sqlLect = "select sum(consumofact) from facturacion.cabctacorriente
	where codsuc = ?
	AND periodo='".$anio.$mes."'
	AND tipo = 0 AND tipoestructura = 0 AND
	codestadoservicio = 1";
	*/
	$sqlLect = "select sum(consumofact) from facturacion.cabfacturacion
	where codsuc = ? AND nrofacturacion=? AND nrodocumento > 0";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc, $nrofact['nrofacturacion']));
	$items = $consultaL->fetch();

	$objReporte->contenido("VOLUMEN FACTURADO",$items['sum'],16);
	$objReporte->Output();

?>
