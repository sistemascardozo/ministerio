<?php
	include("../../../../objetos/clsReporte.php");
	include("../../../../objetos/clsReporteExcel.php");

	set_time_limit(0);
  	$texto = strtoupper($_GET["mestexto"]);

	header("Content-type: application/vnd.ms-excel; name='excel'");
   	header("Content-Disposition: filename=FACTURACION_DE_{$texto}.xls");
   	header("Pragma: no-cache");
   	header("Expires: 0");

	$clsFunciones = new clsFunciones();
	$objReporte   = new clsReporte();
	$codemp       = 1;
	$codsuc       = $_GET["codsuc"];
	$ciclo        = $_GET["ciclo"];
	$anio         = $_GET["anio"];
	$mes          = $_GET["mes"];
	$valor        = $_GET["valor"];
	$idusuario    = $_SESSION['id_user'];
	$periodo      = $_GET["anio"].($_GET['mes']);
	$total        = 0;
	$totalconv    = 0;

	$ultimodia = $clsFunciones->obtener_ultimo_dia_mes($mes, $anio);
	$primerdia = $clsFunciones->obtener_primer_dia_mes($mes, $anio);
	//$periodo   = $clsFunciones->DecFechaLiteral();

	CabeceraExcel(3, 2);
	// Para obtener el periodo de faturacion

	$sqlLect = "select nrofacturacion
		FROM facturacion.periodofacturacion
		WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
	$nrofact = $consultaL->fetch();

	$Sql = "SELECT c.codconcepto as codconcepto,
            c.descripcion
            FROM  facturacion.detctacorriente d
            INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp AND d.codsuc = c.codsuc AND d.codconcepto = c.codconcepto)
            WHERE d.codsuc={$codsuc} and d.nrofacturacion = {$nrofact['nrofacturacion']}
            and ((d.nrocredito = 0 and d.nrorefinanciamiento = 0) OR d.codtipodeuda=9)
            AND d.tipo=0 AND d.tipoestructura=0 AND d.periodo='{$periodo}'
            GROUP BY c.codconcepto,c.descripcion

            ORDER BY codconcepto";

	$consultacol = $conexion->query($Sql);
	$alcas = $consultacol->fetchAll();
	$nroconceptos=0;

	// $arrayconceptos = array();
	// $arrayunico = array();

?>
<table class="ui-widget" border="0" cellspacing="0" id="TbTitulo" width="100%" rules="rows">
	<thead class="ui-widget-header" style="font-size:14px">
		<tr title="Cabecera">
			<th scope="col" colspan="16" align="center" >FACTURACION DEL MES DE <?=$texto;?></th>
		</tr>
		<tr>
			<th colspan="16"><?=$_GET["mes"]." - ".$_GET["anio"]?> </th>
		</tr>
	</thead>
</table>
<table>
	<thead>
		<tr>
			<th width="50" scope="col" class="ui-widget-header">N&deg;</th>
	        <th width="200" scope="col" class="ui-widget-header">COD. CATASTRAL</th>
	        <th width="50" scope="col" class="ui-widget-header">NRO. INSC.</th>
	        <th width="30" scope="col" class="ui-widget-header">CLIENTE</th>
	        <th width="50" scope="col" class="ui-widget-header">DIRECCION</th>
	        <?php
	        	$SelFac ='';
	        	foreach($alcas as $row)
				{
					$nroconceptos++;
					$concepto= $row["descripcion"];
					$SelFac.="SUM(CASE WHEN d.codconcepto=".$row["codconcepto"]." THEN (d.importe -(d.imppagado + d.importerebajado)) ELSE 0  END) AS c".$row["codconcepto"].",";
					$SelFac1.="SUM(CASE WHEN d.codconcepto=".$row["codconcepto"]." THEN (d.importe) ELSE 0  END) AS c".$row["codconcepto"].",";
					$SelFac2.="SUM(CASE WHEN d.codconcepto=".$row["codconcepto"]." THEN (d.importe) ELSE 0  END) AS c".$row["codconcepto"].",";
					?>
					<th width="50" scope="col" class="ui-widget-header"><?=$concepto?></th>
					<?php
				}



			?>
	    	<th width="50" scope="col" class="ui-widget-header">TOTAL</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$contador=0;
		 $Sql="	(
					SELECT c.nroinscripcion,
					c.propietario,
					tcll.descripcioncorta || ' ' ||cll.descripcion || ' # ' || c.nrocalle as direccion,
					to_char(c.codsuc,'00')||'-'||TRIM(to_char(c.codsector,'00'))||'-'||TRIM(to_char(CAST(c.codmanzanas AS INTEGER),'000'))||'-'||TRIM(to_char(CAST(c.lote AS INTEGER),'0000')) as codcatastro ,
					c.codantiguo,c.nroinscripcion,
					".$SelFac." SUM(d.importe -(d.imppagado + d.importerebajado)) AS c0
					FROM  catastro.clientes c
					INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nroinscripcion = d.nroinscripcion)
					INNER JOIN public.calles cll ON (c.codemp = cll.codemp)
					AND (c.codsuc = cll.codsuc)  AND (c.codzona = cll.codzona)
					AND (c.codcalle = cll.codcalle)
					INNER JOIN public.tiposcalle tcll ON (cll.codtipocalle = tcll.codtipocalle)
					WHERE d.codsuc={$codsuc} and d.nrofacturacion = {$nrofact['nrofacturacion']}
					AND d.tipo=0 AND d.tipoestructura=0 AND d.periodo='{$periodo}'
					group by c.nroinscripcion,c.propietario,
					tcll.descripcioncorta || ' ' ||cll.descripcion || ' # ' || c.nrocalle,
					to_char(c.codsuc,'00')||'-'||TRIM(to_char(c.codsector,'00'))||'-'||TRIM(to_char(CAST(c.codmanzanas AS INTEGER),'000'))||'-'||TRIM(to_char(CAST(c.lote AS INTEGER),'0000'))  ,
				  	c.codantiguo

				)
				UNION
				(
					SELECT
					c.nroinscripcion,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.propietario  END as propietario
					/*c.propietario as propietario*/,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.direccion  END as direccion
					/*c.direccion as direccion*/,
					'' as codcatastro,
					to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')) as codantiguo,c.nroprepago,
					".$SelFac1." SUM(d.importe) AS c0
					FROM  cobranza.cabprepagos c
					INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
					AND (c.nroinscripcion = d.nroinscripcion)
					INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
					AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
					WHERE c.codsuc = {$codsuc} AND c.estado<>0 AND c.estado<>2
				  	AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
					AND d.coddocumento IN (13,14) 	AND co.codconcepto <> 10005
					GROUP BY c.nroinscripcion, c.propietario,c.direccion,c.codsuc,c.nroprepago

				)
				UNION
				(
					SELECT
			   		c.nroinscripcion,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.propietario  END as propietario
					/*c.propietario as propietario*/,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.direccion  END as direccion
					/*c.direccion as direccion*/,
					'' as codcatastro,
					to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')) as codantiguo,c.nropago,
					".$SelFac2."
					SUM(d.importe) AS c0
					FROM
					cobranza.cabpagos c
					INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion)
					INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
					WHERE c.codsuc = {$codsuc} AND c.anulado = 0 AND c.car <> 9
					AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
					AND d.coddocumento IN (13,14)  	AND co.codconcepto <> 10005
					GROUP BY c.nroinscripcion, c.propietario,c.direccion,c.codsuc,c.nropago
					) ORDER BY codcatastro DESC";

			 $consultacol = $conexion->query($Sql);
			 $alcas2 = $consultacol->fetchAll();
			 $AUsuario  = array();
			  foreach($alcas2 as $row)
			  {
			  	if (!array_key_exists($row['nroinscripcion'], $AUsuario))
					{
						$AUsuario[$row['nroinscripcion']] = array(
																"nroinscripcion"=>$row['nroinscripcion'],
																"nroinscripcion"=>$row['nroinscripcion'],
																"codcatastro"=>$row['codcatastro'],
																"codantiguo"=>$row['codantiguo'],
																"propietario"=>$row['propietario'],
																"direccion"=>$row['direccion']);
						foreach($alcas as $rowc)
						{
							 $c='c'.$rowc['codconcepto'];
							 array_push($AUsuario[$row['nroinscripcion']],array($c=>$row[$c]));

						}
					}
					else
					{
						$Conceptos[$row['nroinscripcion']] = array(
																	"nroinscripcion"=>$row['nroinscripcion'],
																	"codcatastro"=>$row['codcatastro'],
																	"codantiguo"=>$row['codantiguo'],
																	"propietario"=>$row['propietario'],
																	"direccion"=>$row['direccion']);
						$i2=-1;
						foreach($alcas as $rowc)
						{
							$i2++;
							$c='c'.$rowc['codconcepto'];
							$AUsuario[$row['nroinscripcion']][$i2]=array($c=>($AUsuario[$row['nroinscripcion']][$i2][$c]+$row[$c]));

						}
					}
			  }

			  sort($AUsuario);
			  $tConceptos = array();
			  $TotalC=0;
			for($i=0;$i<count($AUsuario);$i++)
			 {
			 	$row=$AUsuario[$i];
			 	$contador++;
			 	$tusuario=0;
			 	$i2=-1;
			 	?>
			 	<tr>
					<td width="50" scope="col" ><?=$contador?></td>
			        <td width="200" scope="col" style="mso-number-format:'\@'"><?=$row['codcatastro']?></td>
			        <td width="200" scope="col" style="mso-number-format:'\@'"><?=$row['codantiguo']?></td>
			        <td width="30" scope="col" style="mso-number-format:'\@'"><?=trim((strtoupper($row["propietario"])))?></td>
			        <td width="50" scope="col" style="mso-number-format:'\@'"><?=(strtoupper($row["direccion"]));?></td>
			        <?php foreach($alcas as $rowc):
			        $c='c'.$rowc['codconcepto'];

			        $i2++;

			         if($rowc['codconcepto']!=10012)
			         {
			         	$tusuario +=$row[$i2][$c];
			        	$TotalC+=$row[$i2][$c];
			          }
			         $tConceptos[$c='c'.$rowc['codconcepto']]=$tConceptos[$c='c'.$rowc['codconcepto']]+$row[$i2][$c];
			        ?>
			        <td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($row[$i2][$c],2)?></td>
			 		<?php endforeach;?>
			 		<td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($tusuario,2)?></td>
				</tr>
			 	<?php
			 }
		?>
	</tbody>
	<tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
        <td colspan="<?=(5)?>" align="center" class="ui-widget-header">Usuario Registrados: <?=$contador?></td>
        <?php
         foreach($alcas as $rowc):
         	 $c='c'.$rowc['codconcepto'];
        ?>
    	<td width="50" scope="col" align="right"  class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($tConceptos[$c],2)?></td>
    	<?php endforeach;?>
    	<td width="50" scope="col" align="right" class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($TotalC,2)?></td>
        </tr>
    </tfoot>
</table>
