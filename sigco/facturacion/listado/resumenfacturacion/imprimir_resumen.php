<?php
	include("../../../../objetos/clsReporte.php");

    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{
		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo;
			// global $fechaapertura

			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"RESUMEN DE FACTURACION",0,1,'C');
			$this->Ln(3);

			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			$this->Cell(40, $h,strtoupper("Mes de Referencia"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');
			$this->Ln(3);
			$h = 10;
			$this->SetFont('Arial','B',10);
			$this->Cell($Dim[1],$h,utf8_decode('DESCRIPCION DEL CONCEPTO'),1,0,'C',false);
			$this->Cell($Dim[5],$h,utf8_decode('IMPORTE'),1,1,'C',false);
			$this->Ln(5);
		}

		public function contenido($concepto, $valor, $res = 0, $suplemento = '', $cab = 0, $estado = ''){

			global $Dim;
			if ($cab == 1 AND $estado == 0):



			else:
				if($cab == 2):
					$h = 10;
				endif;

			endif;

			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','',8);

			switch ($res) {

				case 0:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;

				case 1:
					$this->Ln(2);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;
				case 2:
					$this->SetTextColor(0);
					$this->Cell($Dim[3],$h,utf8_decode("____________________________________________________________________________________________________________"),0,1,'L',true);
					$this->Ln(5);
					$this->SetFont('Arial','B',6);
					$this->Cell($Dim[1],$h,utf8_decode("HORA INICIAL        " . $concepto),0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("HORA FINAL          " . $valor),0,1,'C',true);
					break;
				case 3:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("________________________"),0,1,'R',true);

					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
				case 4:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("_________________________"),0,1,'R',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
				case 5:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);

					break;
				case 6:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,0)),0,0,'R',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					break;
				case 7:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,"",0,0,'C',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,1,'L',true);
					break;
				case 8:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'R',true);
					$this->Cell($Dim[5],$h,utf8_decode("_______________________________________"),0,1,'L',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					//$this->Cell($Dim[6],$h,utf8_decode(number_format($suplemento,2)),0,1,'L',true);
					break;
				case 9:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h," ",0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					break;
				case 10:
					$this->SetTextColor(0);
					$this->Cell($Dim[8],$h,utf8_decode($concepto),0,0,'C',true);
					$this->Cell($Dim[9],$h,utf8_decode($suplemento),0,0,'C',true);
					$this->Cell($Dim[10],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					break;
				case 11:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[7],$h,utf8_decode($concepto),0,0,'C',true);
					$this->Cell($Dim[10],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					break;
				case 15:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'C',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;
				case 16:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,0)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;
			}
		}
	}

    $Dim = array('1'=>90,'2'=>60,'3'=>190,'4'=>20,'5'=>100,'6'=>50,'7'=>140,'8'=>40,'9'=>80,'10'=>50);

	$codemp     = 1;
	$codsuc     = $_GET["codsuc"];
	$ciclo      = $_GET["ciclo"];
	$anio       = $_GET["anio"];
	$mes        = intval($_GET["mes"]);
	$texto      = $_GET["mestexto"];
	$valor      = $_GET["valor"];
	$idusuario  = $_SESSION['id_user'];
	$periodo    = $_GET["anio"].($_GET['mes']);

	$total      = 0;
	$totalconv  = 0;
	$sados      = 0;

	$objReporte = new clsLecturas("L");
	$contador   = 0;
	$objReporte->AliasNbPages();
	$objReporte->cabecera();
	$objReporte->AddPage('P');

	$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
	$primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);


	// Para obtener el periodo de faturacion
	$sqlLect = "SELECT nrofacturacion ";
	$sqlLect .= "FROM facturacion.periodofacturacion ";
	$sqlLect .= "WHERE codemp = ".$codemp." ";
	$sqlLect .= " AND codsuc = ".$codsuc." ";
	$sqlLect .= " AND codciclo = ".$ciclo." ";
	$sqlLect .= " AND anio = '".$anio."' ";
	$sqlLect .= " AND mes = '".$mes."' ";
	
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array());
	$nrofact = $consultaL->fetch();

	// Para servicio de agua
	$sqlLect1 = "SELECT SUM(importe) ";
	$sqlLect1 .= "FROM facturacion.detctacorriente ";
	$sqlLect1 .= "WHERE codsuc = ".$codsuc." ";
	$sqlLect1 .= " AND periodo = '".$anio.$mes."' ";
	$sqlLect1 .= " AND tipo = 0 ";				//Facturacion Actual
	$sqlLect1 .= " AND tipoestructura = 0 ";	
	$sqlLect1 .= " AND codconcepto = 1 ";
	$sqlLect1 .= " AND codtipodeuda <> 3 "; 	//Diferente de Refinanciamiento

	$consultaL = $conexion->prepare($sqlLect1);
	$consultaL->execute(array());
	$items = $consultaL->fetch();

	$total += $items['sum'];

	$objReporte->contenido("AGUA POTABLE",$items['sum']);

	// Para servicio de desague
	$sqlLect2 = "SELECT SUM(importe) ";
	$sqlLect2 .= "FROM facturacion.detctacorriente ";
	$sqlLect2 .= "WHERE codsuc = ".$codsuc." ";
	$sqlLect2 .= " AND periodo = '".$anio.$mes."' ";
	$sqlLect2 .= " AND tipo = 0 ";				//Facturacion Actual
	$sqlLect2 .= " AND tipoestructura = 0 ";	
	$sqlLect2 .= " AND codconcepto = 2 ";
	$sqlLect2 .= " AND codtipodeuda <> 3 "; 	//Diferente de Refinanciamiento

	$consultaL = $conexion->prepare($sqlLect2);
	$consultaL->execute(array());
	$items = $consultaL->fetch();

	$total += $items['sum'];
	
	$objReporte->contenido("DESAGUE",$items['sum']);

	// Para servicio de INTERESES X DEUDA
	$sqlLect = "SELECT SUM(d.importe) ";
	$sqlLect .= "FROM facturacion.detctacorriente d ";
	$sqlLect .= " INNER JOIN facturacion.conceptos c ON(c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codconcepto = d.codconcepto) ";
	$sqlLect .= "WHERE d.codsuc = ".$codsuc." ";
	$sqlLect .= " AND ((nrocredito = 0 AND nrorefinanciamiento = 0) OR c.codconcepto = 9 OR codtipodeuda=9) ";
	$sqlLect .= " AND c.categoria = 3 ";
	$sqlLect .= " AND d.codtipodeuda NOT IN(3, 4, 7) ";
	$sqlLect .= " AND d.tipo = 0 ";
	$sqlLect .= " AND d.tipoestructura = 0 ";
	$sqlLect .= " AND d.periodo = '".$anio.$mes."' ";	//Diferente de Refinanciamiento
	
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array());
	$items = $consultaL->fetch();
	
	$total += $items['sum'];
	$TotalInt = $items['sum'];
	
	$Sql = "SELECT CAST(SUM(d.interes) AS NUMERIC (18,2)) AS sum ";
	$Sql .= "FROM facturacion.cabcreditos c ";
	$Sql .= " INNER JOIN facturacion.detcreditos d ON(c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrocredito = d.nrocredito) ";
	$Sql .= "WHERE c.codsuc = ".$codsuc." ";
	$Sql .= " AND c.estareg = 1 ";
	$Sql .= " AND c.sininteres = 0 ";
	$Sql .= " AND c.interes > 0 ";
	$Sql .= " AND c.nroprepago > 0 ";
	$Sql .= " AND c.nroprepagoinicial > 0 ";
	$Sql .= " AND EXTRACT(YEAR FROM c.fechareg) = '".$anio."' ";
	$Sql .= " AND EXTRACT(MONTH FROM c.fechareg) = '".intval($mes)."'";
	
	$items = $conexion->query($Sql)->fetch();
	
	$total += $items['sum'];
	$TotalInt += $items['sum'];
	
	//FINANCIAMIENTOS
	$Sql = "SELECT CAST(SUM(d.importe) - c.totalrefinanciado AS  NUMERIC (18,2)) AS sum, c.nroinscripcion ";
	$Sql .= "FROM facturacion.cabrefinanciamiento c ";
	$Sql .= " INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorefinanciamiento = d.nrorefinanciamiento) ";
	$Sql .= "WHERE d.codemp = 1 ";
	$Sql .= " AND d.codsuc = ".$codsuc." ";
	$Sql .= " AND c.estareg = 1 ";
	$Sql .= " AND c.sininteres = 0 ";
	$Sql .= " AND c.nroprepagoinicial > 0 ";
	$Sql .= " AND EXTRACT(YEAR FROM c.fechaemision) = '".$anio."' ";
	$Sql .= " AND EXTRACT(MONTH FROM c.fechaemision) = '".intval($mes)."' ";
	$Sql .= " AND c.codestadorefinanciamiento <> 5 ";
	$Sql .= "GROUP BY c.nroinscripcion, c.totalrefinanciado ";
	//die($Sql);
	$Consulta = $conexion->query($Sql);
	
	foreach($Consulta->fetchAll() as $items)
	{
		$total += $items['sum'];
		$TotalInt += $items['sum'];
	}
	
	$objReporte->contenido("INTERESES POR DEUDA", $TotalInt);

	// Para servicio de CARGOS DIVERSOS

	// Para servicio de COLATERALES

	// Primera Parte - Sin IGV
	$sk = "SELECT CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1) ELSE d.importe END) AS NUMERIC (18,2)) ";
	$sk .= "FROM cobranza.cabprepagos c ";
	$sk .= " INNER JOIN cobranza.detprepagos d ON(c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nroprepago = d.nroprepago) AND (c.nroinscripcion = d.nroinscripcion) ";
	$sk .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto) ";
	$sk .= "WHERE c.codsuc = ".$codsuc." ";
	$sk .= " AND c.fechareg BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
	$sk .= " AND c.estado <> 0 ";
	$sk .= " AND c.estado <> 2 ";
	$sk .= " AND d.coddocumento IN (13, 14) ";
	$sk .= " AND co.codconcepto NOT IN(5, 10005) ";
	
	$consultacol = $conexion->prepare($sk);
	$consultacol->execute(array());
	$colaterales = $consultacol->fetch();
	
	$total += $colaterales['sum'];
	
	// Primera Parte - IGV
	$sk2 = "SELECT CAST(sum(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1) ELSE d.importe END) AS NUMERIC (18,2)) ";
	$sk2 .= "FROM cobranza.cabprepagos c ";
	$sk2 .= " INNER JOIN cobranza.detprepagos d ON(c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nroprepago = d.nroprepago) AND (c.nroinscripcion = d.nroinscripcion) ";
	$sk2 .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto) ";
	$sk2 .= "WHERE c.codsuc = ".$codsuc." ";
	$sk2 .= " AND c.fechareg BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
	$sk2 .= " AND c.estado <> 0 ";
	$sk2 .= " AND c.estado <> 2 ";
	$sk2 .= " AND d.coddocumento IN (13, 14) ";
	$sk2 .= " AND co.codconcepto IN(5) ";
	
	$consultacol2 = $conexion->prepare($sk2);
	$consultacol2->execute(array());
	$colateralesIgv = $consultacol2->fetch();
	
	$totalIgv = $colateralesIgv['sum'];

	// Segunda Parte
	$dupli1 = "SELECT CAST(SUM(CASE WHEN d.importe <= 0 AND co.signo='-' THEN d.importe*(-1) ELSE d.importe END) AS NUMERIC (18,2)) AS importe ";
	$dupli1 .= "FROM cobranza.cabpagos c ";
	$dupli1 .= " INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion) ";
	$dupli1 .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
	$dupli1 .= "WHERE c.codsuc = ".$codsuc." ";
	$dupli1 .= " AND c.anulado = 0 ";
	$dupli1 .= " AND c.fechareg BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
	$dupli1 .= " AND co.codconcepto NOT IN(10005) ";
	$dupli1 .= " AND c.car <> 9 ";
	$dupli1 .= " AND d.coddocumento IN (13, 14)";

	$consultacol = $conexion->prepare($dupli1);
	$consultacol->execute(array());
	$duplis1     = $consultacol->fetch();
	
	$total       += $duplis1['importe'];

	// Tercer Parte
	// if(intval($mes)<10) $mes="0".$mes;
	$dupli2 = "SELECT CAST(sum(CASE WHEN d.importe <= 0 AND c.signo = '-' THEN d.importe * (-1) ELSE d.importe END) AS NUMERIC (18, 2)) AS importe ";
	$dupli2 .= "FROM facturacion.detctacorriente d ";
	$dupli2 .= " INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp) AND (d.codsuc = c.codsuc) AND (d.codconcepto = c.codconcepto) ";
	$dupli2 .= "WHERE d.codsuc = ".$codsuc." ";
	$dupli2 .= " AND ((d.nrocredito = 0 AND d.nrorefinanciamiento = 0) OR d.codtipodeuda = 9) ";
	$dupli2 .= " AND d.tipo = 0 ";
	$dupli2 .= " AND d.tipoestructura = 0 ";
	$dupli2 .= " AND d.periodo = '".$anio.$mes."' ";
	$dupli2 .= " AND d.codconcepto NOT IN(1, 2, 5, 7, 8, 10012) ";
	$dupli2 .= " AND c.categoria <> 3 ";
	$dupli2 .= " AND d.codtipodeuda NOT IN(3, 4, 7) ";

	$consultacol = $conexion->prepare($dupli2);
	$consultacol->execute(array());
	$duplis2 = $consultacol->fetch();
	
	$total	+= $duplis2['importe'];
	$insrt	= $colaterales['sum'] + $duplis1['importe'] + $duplis2['importe']; // modifique $duplis1['importe']

	$objReporte->contenido("CARGOS DIVERSOS", $insrt);
	$objReporte->contenido("SUBTOTAL", $total, 8, "");

	// Para servicio de IGV
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ?  AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				codconcepto = 5";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc,));
	$items = $consultaL->fetch();
	
	$items['sum'] = $items['sum'] + $totalIgv;
	$total += $items['sum'];
	
	$objReporte->contenido("IGV", $items['sum']);

	// Para servicio de REDONDEO POSITIVO
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ? AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				((nrocredito = 0 and nrorefinanciamiento = 0) OR codconcepto = 9 OR codtipodeuda =9)
				and codtipodeuda not in (3, 4, 7) AND
				codconcepto = 8";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$total += $items['sum'];

	$objReporte->contenido("REDONDEO POSITIVO",$items['sum']);

	// Para servicio de REDONDEO NEGATIVO
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ? AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0
				and ((nrocredito = 0 and nrorefinanciamiento = 0) OR codconcepto = 9 OR codtipodeuda =9)
				and codtipodeuda not in (3, 4, 7) AND
				codconcepto = 7";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$total += $items['sum'];

	$objReporte->contenido("REDONDEO NEGATIVO",$items['sum']);
	// Para servicio de FACTURACION TOTAL DEL MES

	$objReporte->contenido("TOTAL",$total,8);

	// Para servicio de INCLUSION DE CUOTAS

	// Fonavi
	$sqlLect = "Select sum(importe) from facturacion.detctacorriente
				where codsuc = ? AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				codconcepto = 101 and codtipodeuda <> 3 --Diferente de Refinanciamiento";
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$fonavi    = $consultaL->fetch();

	// Convenio de Empresa
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ? AND periodo='".$anio.$mes."'
				AND tipo = 0 AND
				codconcepto <> 101 AND codtipodeuda in (3, 4, 7) AND tipoestructura=0";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$empresa   = $consultaL->fetch();

	$empfona = $empresa['sum'] + $fonavi['sum'];

	$saldos = $total + $empfona;

	$objReporte->contenido("INCLUSIÓN DE CUOTAS",$empfona);


	// Para servicio de SALDO DE MESES ANTERIORES
	// $sqlLect = "select sum(d.importe - (d.importeacta + d.importerebajado))
	// 			FROM facturacion.cabctacorriente c
	// 			  INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp)
	// 			  AND (c.codsuc = d.codsuc)
	// 			  AND (c.codciclo = d.codciclo)
	// 			  AND (c.nrofacturacion = d.nrofacturacion)
	// 			  AND (c.nroinscripcion = d.nroinscripcion)
	// 			  AND (c.anio = d.anio)
	// 			  AND (c.mes = d.mes)
	// 			  AND (c.periodo = d.periodo)
	// 			  AND (c.tipo = d.tipo)
	// 			  AND (c.tipoestructura = d.tipoestructura)
	// 			where d.codsuc = ? and d.periodo = ? and d.categoria = 1
	// 			AND d.estadofacturacion = 1
	// 			AND d.codtipodeuda <> 3 and d.codtipodeuda <> 4
	// 			and d.codtipodeuda <> 6 and d.codtipodeuda <> 7 AND d.codtipodeuda <> 9
	// 			AND d.codtipodeuda <> 10  AND d.codtipodeuda <> 11
	// 			AND c.nrodocumento<>0 AND c.tipo = 1 AND c.tipoestructura=0 ";

	// $consultaL = $conexion->prepare($sqlLect);
	// $consultaL->execute(array($codsuc,$periodo));
	// $items = $consultaL->fetch();

	// Modificaciones po saldo 23-01-2014

	$mess = $mes;
	$mess  = str_pad($mess, 2, 0, STR_PAD_LEFT);

		$sqlLect = "SELECT SUM((d.importe - d.importeacta) + d.importerebajado) as importe
					FROM facturacion.detctacorriente d
					WHERE codsuc = $codsuc AND periodo = '$anio$mes'
					AND d.codconcepto <> 101
					AND d.tipoestructura = 0
					AND d.tipo = 1 AND d.codconcepto <> 101
					AND d.codtipodeuda <> 3";


		$consultaL1 = $conexion->prepare($sqlLect);
		$consultaL1->execute();
		$items = $consultaL1->fetch();

	$objReporte->contenido("SALDO DE MESES ANTERIORES",$items['importe']);

	// Para servicio de MONTO A SU FAVOR
	$sqlLect = "select sum(importe) from facturacion.detctacorriente
				where codsuc = ? AND periodo='".$anio.$mes."'
				AND tipo = 0 AND tipoestructura = 0 AND
				codconcepto = 10005  ";

				// var_dump($sqlLect);exit;

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();

	$objReporte->contenido("MONTO A SU FAVOR",$items['sum']);

	// Para servicio de NRO FACTURAS
	/*
	$sqlLect = "select count(codestadoservicio) from facturacion.cabctacorriente c
	INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp)
	AND (c.codsuc         = d.codsuc)
	AND (c.codciclo       = d.codciclo)
	AND (c.nrofacturacion = d.nrofacturacion)
	AND (c.nroinscripcion = d.nroinscripcion)
	AND (c.anio           = d.anio)
	AND (c.mes            = d.mes)
	AND (c.periodo        = d.periodo)
	AND (c.tipo           = d.tipo)
	AND (c.tipoestructura = d.tipoestructura)
	where c.codsuc = ? and c.periodo = ?
	AND c.tipo = 0 AND c.tipoestructura = 0
	AND d.codconcepto = 6 --Activos ";
	*/
	$sqlLect = "select count(*) from facturacion.cabfacturacion
				where codsuc = ? and nrofacturacion = ? and nrodocumento > 0 ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc,$nrofact['nrofacturacion']));
	$items = $consultaL->fetch();

	$objReporte->contenido("NRO FACTURAS",$items['count'],16);

	//VOLUMEN
	$sqlLect = "select sum(consumofact)
				from facturacion.cabctacorriente
				where codsuc = ? AND nrofacturacion=? AND nrodocumento > 0
				AND periodo='".$anio.$mes."' AND tipo = 0
				AND tipoestructura = 0 ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc, $nrofact['nrofacturacion']));
	$items = $consultaL->fetch();

	$objReporte->contenido("VOLUMEN FACTURADO",$items['sum'],16);

	//TIPO DE FACTURACION
	/*---ANTERIOR, 28-08-2015, POR ING. MAURO N. CARDOZO----
	$Sql="select tipofacturacionfact,count(*)
	from facturacion.cabctacorriente
	where codsuc = ? AND nrofacturacion=? AND nrodocumento > 0
	AND periodo='".$anio.$mes."' AND tipo = 0
	AND tipoestructura = 0
	group by tipofacturacionfact ORDER BY tipofacturacionfact";
	*/
	$Sql="select tipofacturacionfact,sum(consumofact)
	from facturacion.cabctacorriente
	where codsuc = ? AND nrofacturacion=? AND nrodocumento > 0
	AND periodo='".$anio.$mes."' AND tipo = 0
	AND tipoestructura = 0
	group by tipofacturacionfact ORDER BY tipofacturacionfact";
	$consultaL = $conexion->prepare($Sql);
	$consultaL->execute(array($codsuc, $nrofact['nrofacturacion']));
	foreach($consultaL->fetchAll() as $items)
	{
		switch ($items[0]) {
			case 0:$Tipo='CONSUMO LEIDO';break;
			case 1:$Tipo='CONSUMO PROMEDIADO';break;
			case 2:$Tipo='CONSUMO ASIGNADO';break;

		}
		 $objReporte->contenido($Tipo,$items[1],16);
	}
	//TIPO DE FACTURACION
	$objReporte->Output();

?>
