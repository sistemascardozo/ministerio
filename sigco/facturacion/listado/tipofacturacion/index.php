<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "USUARIOS PRE FACTURADOS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objMantenimiento 	= new clsDrop();
	$codsuc = $_SESSION['IdSucursal'];
	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
	
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_usuarios.js" language="JavaScript"></script>
<script>
	var codsuc 	= <?=$codsuc?>;
	var urldir 	= "<?php echo $_SESSION['urldir'];?>";
	function ver(z){}
	function ValidarForm(Op)
	{
		var tipofacturacion	= ""
		var sectores		= ""
		
		if($("#ciclo").val()==0)
		{
			alert("Seleccione el Ciclo")
			return false
		}
		if($("#anio").val()==0)
		{
			alert('Seleccione el A�o a Consultar')
			return false
		}
		if($("#mes").val()==0)
		{
			alert('Seleccione el Mes a Consultar')
			return false
		}
		//if($("#consumo").val()=="" || $("#consumo").val()==0)
		if($("#consumo").val()=="")
		{
			alert('El Consumo de Inicio no es valido')
			return false
		}
		if($("#consumohasta").val()=="" || $("#consumohasta").val()==0)
		{
			alert('El Consumo Final no es valido')
			return false
		}		
		if($("#todosectores").val()==0)
		{
			sectores=$("#codsector").val()
			if(sectores==0)
			{
				alert("Seleccione el Sector")
				return false
			}
		}else{
			sectores="%"
		}
		
		if($("#todafacturacion").val()==0)
		{
			tipofacturacion=$("#tfacturacion").val()
		}else{
			tipofacturacion="%"
		}
		

		url = "imprimir.php?ciclo="+$("#ciclo").val()+"&sector="+sectores+"&consumo="+$("#consumo").val()+"&anio="+$("#anio").val()+
			  "&mes="+$("#mes").val()+"&consumohasta="+$("#consumohasta").val()+"&tfact="+tipofacturacion+'&codsuc=<?=$codsuc?>'
		AbrirPopupImpresion(url,800,600)
		
		return false
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	
	<tr>
       <td colspan="2">&nbsp;</td>
	</tr>
    <tr>
        <td colspan="2" align="center">
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="90">Sucursal</td>
				    <td width="30" align="center">:</td>
				    <td><label>
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </label></td>
				    <td width="80" align="right">Ciclo</td>
				    <td width="30" align="center">:</td>
				    <td>
                    	<? $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio(this.value,1);'"); ?>
                    </td>
				  </tr>
				  <tr>
				    <td>A&ntilde;o</td>
				    <td align="center">:</td>
				    <td>
                    	<div id="div_anio">
                        	<? $objMantenimiento->drop_anio($codsuc,0); ?>
                        </div>
                    </td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
				    <td>Mes</td>
				    <td align="center">:</td>
				    <td>
                    	<div id="div_meses">
                        	<? $objMantenimiento->drop_mes($codsuc,0,0); ?>
                        </div>                    
                    </td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  
				  <tr>
				    <td>Sector</td>
				    <td align="center">:</td>
				    <td>
                    	<?php echo $objMantenimiento->drop_sectores2($codsuc, 0, 0); ?>
                    </td>
				    <td>&nbsp;</td>
				    <td align="center"><input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todosectores');quitar_disabled(this,'codsector');" /></td>
				    <td>Todos los Sectores </td>
			      </tr>
				  <tr>
				    <td>T. Facturacion </td>
				    <td align="center">:</td>
				    <td><label>
				      <select name="tfacturacion" id="tfacturacion" style="width:220px" class="select" disabled="disabled">
					  	<option value="0">CONSUMO LEIDO</option>
                        <option value="1">CONSUMO PROMEDIADO</option>
                        <option value="2">CONSUMO ASIGNADO</option>	
			          </select>
				    </label></td>
				    <td>&nbsp;</td>
				    <td align="center"><input type="checkbox" name="chktipo" id="chktipo" value="checkbox" checked="checked" onclick="CambiarEstado(this,'todafacturacion');quitar_disabled(this,'tfacturacion');" /></td>
				    <td>Todos los Tipos </td>
			      </tr>
				  <tr>
				    <td>Con. Desde</td>
				    <td align="center">:</td>
				    <td><input type="text" name="consumo" id="consumo" value="0" class="inputtext"  /></td>
			        <td align="right">Con. Hasta </td>
			        <td align="center">:</td>
			        <td><input type="text" name="consumohasta" id="consumohasta" value="99999999" class="inputtext"></td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td colspan="2"><input type="hidden" name="todosectores" id="todosectores" value="1" />
			        <input type="hidden" name="todafacturacion" id="todafacturacion" value="1" /></td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr><td colspan="6" align="center"> <input type="button" onclick="return ValidarForm();" value="Exportar" id=""></td></tr>
				  <tr><td colspan="6">&nbsp;</td></tr>
				</table>
		</td>
	</tr>
  
	 </tbody>
	
    </table>
 </form>
</div>
<script>
	$("#codsector").attr("disabled", true)
</script>
<?php CuerpoInferior(); ?>