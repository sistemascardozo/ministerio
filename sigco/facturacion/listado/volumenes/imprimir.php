<?php 	
	include("../../../../objetos/clsReporte.php");
	ini_set("memory_limit","512M");
	class clsVolumenes extends clsReporte
	{
		function cabecera()
		{
			global $anio,$mes,$meses;
			
			$x 	= 23;
            $y 	= 5;
			$h	= 3;
			
			$this->SetFont('Arial','B',14);
			$this->Cell(190, $h+3,"VOLUMENES FACTURADOS",0,1,'C');
			$this->SetFont('Arial','B',12);
			$this->Cell(190, $h+3,$meses[$mes]."-".$anio,0,1,'C');
			$this->Ln(2);

			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(21,65,13,10,20,20,20,20));
			$this->SetAligns(array("C","C","C","C","C","C","C","C"));
			$this->Row(array("COD. CAT.",
							 "USUARIO",
							 "TF",
							 "CAT.",
							 "LECT. ANT.",
							 "LECT. ULT.",
							 "CONSUMO",
							 "CONS. FACT."));
			
			$this->SetFont('Arial','',7);
						
		}
		function Contenido($codcatastro,$usuario,$tfacturacion,$cat,$lectanterior,$lectultima,
						   $consumo,$consumofact)
		{
			$h=6;
			$this->SetFont('Arial','',7);
			
			$tf="";
			if($tfacturacion==0)
			{
				$tf="L";
			}
			if($tfacturacion==1)
			{
				$tf="P";
			}
			if($tfacturacion==2)
			{
				$tf="A";
			}
			
			$this->SetWidths(array(21,65,13,10,20,20,20,20));
			$this->SetAligns(array("C","L","C","C","C","C","C","C"));
			$this->Row(array($codcatastro,
							 $usuario,
							 $tf,
							 $cat,
							 $lectanterior,
							 $lectultima,
							 $consumo,
							 $consumofact));
		}
    }
	
	$codciclo 	= $_GET["ciclo"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$tfact		= "%".$_GET["tfact"]."%";
	$codsuc		= $_GET["codsuc"];
        
	$objReporte	=	new clsVolumenes();
	$objReporte->AddPage();
	
	$totconsumo	= 0;
	$count		= 0;
	
	 $sql  = "select cab.nroinscripcion,upper(clie.propietario) as propietario,cab.tipofacturacion,upper(tar.nomtar) as nomtar,
			cab.lecturaanterior,cab.lecturaultima,cab.consumo,cab.consumofact,".$objReporte->getCodCatastral("clie.")."	from facturacion.cabfacturacion as cab
			inner join catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc
			and cab.nroinscripcion=clie.nroinscripcion)
			inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc
			and cab.catetar=tar.catetar)
			where cab.anio=:anio and cab.mes=:mes and cab.codciclo=:codciclo and 
			".$objReporte->Convert("cab.tipofacturacion", "CHAR")." LIKE :tipofacturacion order by codcatastro";
			
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":anio"=>$anio,
							 ":mes"=>$mes,
							 ":codciclo"=>$codciclo,
							 ":tipofacturacion"=>$tfact));
	
	$items = $consulta->fetchAll();
	foreach($items as $row)
	{
		$count++;
		$totconsumo+=$row["consumo"];

		$objReporte->Contenido($row["codcatastro"],strtoupper(utf8_decode($row["propietario"])),$row["tipofacturacion"],
						substr($row["nomtar"],0,3),intval($row["lecturaanterior"]),intval($row["lecturaultima"]),
						intval($row["consumo"]),intval($row["consumofact"]));			
	}

	$objReporte->Output();	
	
?>