<?php 
	include("../../../../objetos/clsReporte.php");
	set_time_limit(0);
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $mes,$anio,$meses,$x,$EstadoServicioN;
			
			$h=4;
			$this->SetFont('Arial','B',8);
			$this->Cell(0, $h+1,utf8_decode("VOLUMEN DE LA  FACTURACIÓN"),0,1,'C');
			$this->SetFont('Arial','B',8);
			
			if($mes==1)
				{
					$mesa=12;
					$anioa=$anio;
					$aniob=$anio-1;

				}
			else
			{
				$mesa=$mes;
				$mesb=$mes-1;
				$anioa=$anio;
				$aniob=$anio;
				if($mesa==1)
					{
						$mesb=12;
						$aniob=$anio-1;
					}
			}
			$this->Cell(0, $h+1,"FACTURACION DE ".$meses[$mesa]."-".$anioa." DEL CONSUMO DE ".$meses[$mesb]." - ".$aniob,0,1,'C');
			if($EstadoServicioN!="")
				$this->Cell(0, $h+1,"ESTADO DE SERVICIO : ".$EstadoServicioN,0,1,'C');
			$this->Ln(2);
			
			
			
			
		}
		function Leyenda()
		{
			global $x,$conexion;

		}
		function TipoServicio($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipoFacturacion,$Descripcion,$CondEstSer,$TipoServicio,$TipoServicioDes)
		{
			global $x,$h,$conexion;
			/*
			$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
					count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." 
					AND c.tipofacturacion=".$TipoFacturacion." AND c.codtiposervicio=".$TipoServicio." ".$CondEstSer;

				*/
				/*
				$Sql = "select 
					count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
					from  facturacion.cabctacorriente as c 
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." 
					AND c.tipofacturacionfact=".$TipoFacturacion." AND  c.nrodocumento >0 AND c.codtiposervicio=".$TipoServicio." ".$CondEstSer;
				*/
				
				$Sql = "select 
					sum(importe), count(nrousaurio), sum(confac)
					
					from  
					(
					select 
					sum(det.importe-(det.importerebajado+det.imppagado)) as importe ,
					c.nroinscripcion, c.consumofact  as confac,
					count(DISTINCT(c.nroinscripcion)) as nrousaurio
					from  facturacion.cabctacorriente as c 
					inner join facturacion.detctacorriente as det on
					(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
					and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
					c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)

					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." 
					AND c.tipofacturacionfact=".$TipoFacturacion." AND  c.nrodocumento >0 AND 
					det.codconcepto in (1) AND c.codtiposervicio=".$TipoServicio." ".$CondEstSer."

					group by c.nroinscripcion, c.consumofact
					order by c.nroinscripcion ) as a ";

					//die($Sql);
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();
				$this->SetFont('Arial','',6);
				if ($rowMo[1]>0)
				{
					$this->Cell(90, $h,'',0,0,'L');
					$this->Cell(20, $h,$TipoServicioDes,0,0,'L');
					$this->Cell(15, $h,number_format($rowMo[0],2),0,0,'R');
					$this->Cell(20, $h,$rowMo[1],0,0,'R');
					$this->Cell(20, $h,number_format($rowMo[2],0)." m".Cubico,0,1,'R');
				}

		}
		//Consumos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,0,$CondEstSer)
		function Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipoFacturacion,$Descripcion,$CondEstSer)
		{
			global $x,$h,$conexion;
			/*
			$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
					count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." 
					AND c.tipofacturacion=".$TipoFacturacion." ".$CondEstSer;
				*/


				/*
				$Sql = "select 
					count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
					from  facturacion.cabctacorriente as c 
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." 
					AND c.nrodocumento >0 AND c.tipofacturacionfact=".$TipoFacturacion." ".$CondEstSer;
				*/
				//--------------------------------------IMPORTE AGUA------------------------------------------------------------------//
				$Sql = "select 
					sum(importe), count(nrousaurio), sum(confac)
					
					from  
					(
					select 
					sum(det.importe-(det.importerebajado+det.imppagado)) as importe ,
					c.nroinscripcion, c.consumofact  as confac,
					count(DISTINCT(c.nroinscripcion)) as nrousaurio
					from  facturacion.cabctacorriente as c 
					inner join facturacion.detctacorriente as det on
					(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
					and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
					c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." 
					AND c.nrodocumento >0  AND det.codconcepto in (1) AND c.tipofacturacionfact=".$TipoFacturacion." ".$CondEstSer."

					group by c.nroinscripcion, c.consumofact
					order by c.nroinscripcion ) as a ";
				

					//die($Sql);
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();

				//------------------------------------CANTIDAD DE USUARIOS Y VOLUMEN--------------------------------------------------------------------//

				$Sql1 = "select 
					count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
					from  facturacion.cabctacorriente as c 
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." 
					AND c.nrodocumento >0 AND c.tipofacturacionfact=".$TipoFacturacion." ".$CondEstSer;

				$ConsultaMo1 =$conexion->query($Sql1);
				$rowMo1		= $ConsultaMo1->fetch();

				//--------------------------------------IMPORTE DESAGUE--------------------------------------------------------------//


				$Sql2 = "select 
					sum(importe)
					
					from  
					(
					select 
					sum(det.importe-(det.importerebajado+det.imppagado)) as importe
					from  facturacion.cabctacorriente as c 
					inner join facturacion.detctacorriente as det on
					(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
					and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
					c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." 
					AND c.nrodocumento >0  AND det.codconcepto in (2) AND c.tipofacturacionfact=".$TipoFacturacion." ".$CondEstSer."

					group by c.nroinscripcion, c.consumofact
					order by c.nroinscripcion ) as a ";
				

					//die($Sql);
				$ConsultaMo2 =$conexion->query($Sql2);
				$rowMo2		= $ConsultaMo2->fetch();

				$this->SetFont('Arial','',6);
				if ($rowMo[1]>0  || $rowMo1[1]>0 || $rowMo2[0]>0 )
				{
					$this->Cell(70, $h,'',0,0,'L');
					$this->Cell(40, $h,$Descripcion,0,0,'L');
					$this->Cell(15, $h,number_format($rowMo[0],2),0,0,'R');
					$this->Cell(20, $h,number_format($rowMo2[0],2),0,0,'R');
					$this->Cell(20, $h,$rowMo1[0],0,0,'R');
					$this->Cell(20, $h,number_format($rowMo1[1],0)." m".Cubico,0,1,'R');
				}
				/*
				//AGUA Y DESAGUE//
				$this->TipoServicio($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipoFacturacion,$Descripcion,$CondEstSer,1,'Agua y Desague');
				$this->TipoServicio($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipoFacturacion,$Descripcion,$CondEstSer,2,'Solo Agua');
				$this->TipoServicio($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipoFacturacion,$Descripcion,$CondEstSer,3,'Solo Desague');
				*/
				

		}
		//Rangos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,$Descripcion,$CondEstSer)
		function Rangos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$Descripcion,$CondEstSer,$CondTipFac,$TipFac)
		{
			global $x,$h,$conexion;
			
			/*
			$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
					count(DISTINCT(c.nroinscripcion)) as nrousaurio ,sum(c.consumofact)
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." ".$CondEstSer.$CondTipFac;

				*/	
					/*
			$Sql = "select 
					count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
					from  facturacion.cabctacorriente as c 
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.nrodocumento >0 AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." ".$CondEstSer.$CondTipFac;
			*/
				//--------------------------------------IMPORTE AGUA------------------------------------------------------------------//
				$Sql = "select 
					sum(importe), count(nrousaurio), sum(confac)
					
					from  
					(
					select 
					sum(det.importe-(det.importerebajado+det.imppagado)) as importe ,
					c.nroinscripcion, c.consumofact  as confac,
					count(DISTINCT(c.nroinscripcion)) as nrousaurio
					from  facturacion.cabctacorriente as c 
					inner join facturacion.detctacorriente as det on
					(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
					and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
					c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.nrodocumento >0 AND  det.codconcepto in (1) AND 
					c.consumofact between ".$RangoInicial." AND ".$RangoFinal." ".$CondEstSer."".$CondTipFac."

					group by c.nroinscripcion, c.consumofact
					order by c.nroinscripcion ) as a ";


					//die($Sql);
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();

			//------------------------------------CANTIDAD DE USUARIOS Y VOLUMEN--------------------------------------------------------------------//

			$Sql1 = "select 
					count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
					from  facturacion.cabctacorriente as c 
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.nrodocumento >0 AND c.consumofact between ".$RangoInicial." AND ".$RangoFinal." ".$CondEstSer.$CondTipFac;

			$ConsultaMo1 =$conexion->query($Sql1);
			$rowMo1		= $ConsultaMo1->fetch();

			//--------------------------------------IMPORTE DESAGUE--------------------------------------------------------------//

			$Sql2 = "select 
					sum(importe)
					
					from  
					(
					select 
					sum(det.importe-(det.importerebajado+det.imppagado)) as importe 
					from  facturacion.cabctacorriente as c 
					inner join facturacion.detctacorriente as det on
					(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
					and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
					c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)
					
					where  c.periodo ='".$periodo."' AND c.catetar=".$Tarifa." and 
					c.codsuc=".$codsuc." and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 
					AND c.nrodocumento >0 AND  det.codconcepto in (2) AND 
					c.consumofact between ".$RangoInicial." AND ".$RangoFinal." ".$CondEstSer."".$CondTipFac."

					group by c.nroinscripcion, c.consumofact
					order by c.nroinscripcion ) as a ";


					//die($Sql);
			$ConsultaMo2 =$conexion->query($Sql2);
			$rowMo2		= $ConsultaMo2->fetch();

			if ($rowMo[1]>0 || $rowMo1[1]>0 || $rowMo2[0]>0 )
			{
				$this->SetFont('Arial','B',6);
				$this->SetTextColor(0,0,0);
				$this->SetX($x);
				$this->Cell(30, $h,'',0,0,'L');
				$this->Cell(70, $h,$Descripcion,0,0,'L');
				$this->Cell(15, $h,number_format($rowMo[0],2),0,0,'R');
				$this->Cell(20, $h,number_format($rowMo2[0],2),0,0,'R');
				$this->Cell(20, $h,$rowMo1[0],0,0,'R');
				$this->Cell(20, $h,number_format($rowMo1[1],0)." m".Cubico,0,1,'R');
				//die($CondTipFac);
				switch ($TipFac) 
				{
				    case '0': 
				       //CONSUMO LEIDO//
					$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipFac,'Consumo Leido',$CondEstSer);
					//CONSUMO LEIDO//
				    break;
				    case '1':
				       //CONSUMO PROMEDIADO//
					$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipFac,'Consumo Promediado',$CondEstSer);
					//CONSUMO PROMEDIADO//
				    break;
				    case '2':
				       //CONSUMO ASIGNADO//
					$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,$TipFac,'Consumo Asignado',$CondEstSer);
						 //CONSUMO ASIGNADO//
				    break;
				    default:
				     //CONSUMO LEIDO//
					$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,0,'Consumo Leido',$CondEstSer);
					//CONSUMO LEIDO//
					//CONSUMO PROMEDIADO//
					$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,1,'Consumo Promediado',$CondEstSer);
					//CONSUMO PROMEDIADO//
					//CONSUMO ASIGNADO//
					$this->Consumos($periodo,$Tarifa,$codsuc,$ciclo,$RangoInicial,$RangoFinal,2,'Consumo Asignado',$CondEstSer);
				}
				
				//CONSUMO ASIGNADO//
				/*$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo
					FROM facturacion.cabctacorriente c
				  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
				  AND (c.codsuc = dc.codsuc)
				  AND (c.codciclo = dc.codciclo)
				  AND (c.nrofacturacion = dc.nrofacturacion)
				  AND (c.nroinscripcion = dc.nroinscripcion)
				  AND (c.anio = dc.anio)
				  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
				  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
				  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
				  AND c.periodo='".$periodo."' AND c.catetar='".$rowT["catetar"]."'
				  AND c.tipo=0 AND c.tipoestructura=0 
				  AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer."
				  ORDER BY co.ordenrecibo";
				  $ConsultaCo =$conexion->query($Sql);
				foreach($ConsultaCo->fetchAll() as $rowCo)
				{
					$objreporte->SetFont('Arial','',6);
					$objreporte->SetX($x +30);
					$objreporte->Cell(30, $h,'',0,0,'L');
					$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
					$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
						from facturacion.detctacorriente as d
						inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
						and d.codciclo=c.codciclo and 
						d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
						d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
						inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
						and d.codconcepto=cp.codconcepto)
						where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
						c.codsuc=".$codsuc." 
						and c.codciclo=".$ciclo." AND c.tipo=0 AND c.tipoestructura=0
						AND cp.codconcepto='".$rowCo[0]."' 
						AND c.consumofact between '".$RangoInicial."' AND '".$RangoFinal."' ".$CondEstSer;
					$ConsultaMo =$conexion->query($Sql);
					$rowMo		= $ConsultaMo->fetch();
					$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
							//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
							
				}
				*/
			}

		}
    
    }
	define('Cubico', chr(179));
	$anio            = $_GET["anio"];
	$mes             = $_GET["mes"];
	$ciclo           = $_GET["ciclo"];
	$codsuc          = $_GET["codsuc"];
	$EstadoServicio  = $_GET["EstadoServicio"];
	$EstadoServicioN = $_GET["EstadoServicioN"];
	$tfact           = $_GET["tfact"];
	$periodo         = $anio.$mes;
    $x = 20;
    $h=4;
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("P");
	$CondEstSer = "";
	if($EstadoServicio!="")
		$CondEstSer = " AND c.codestadoservicio=".$EstadoServicio." ";
	if($tfact!="%")
		$CondTipFac = " AND c.tipofacturacionfact=".$tfact." ";

	$sqlC  = "select cat.codcategoriatar,cat.descripcion 
	from facturacion.cabctacorriente as c ";
	$sqlC .= "inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and ";
	$sqlC .= "c.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "where c.periodo='".$periodo."' and c.codsuc=".$codsuc." 
	and c.codciclo=".$ciclo." AND c.tipo=0 AND c.nrodocumento >0 AND c.tipoestructura=0 ".$CondEstSer.$CondTipFac;
	$sqlC .= " group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";


	//die($sqlC);
	$consultaC = $conexion->query($sqlC);
	$itemsC = $consultaC->fetchALl();
	
	$objreporte->SetFont('Arial','B',7);
	$objreporte->SetX($x-10);
	$objreporte->Cell(70, $h,'- FACTURACION DEL MES',0,1,'l');
	$objreporte->SetFont('Arial','',6);
		
	$objreporte->Ln(2);
	$objreporte->SetX($x);
	$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
	$objreporte->Cell(30, $h,"RANGOS",1,0,'L');
	$objreporte->Cell(35, $h,utf8_decode("TIPO FACTURACIÓN"),1,0,'L');
	$objreporte->Cell(20, $h,"IMPORTE AGUA",1,0,'C');
	$objreporte->Cell(25, $h,"IMPORTE DESAGUE",1,0,'C');
	$objreporte->Cell(20, $h,"USUARIOS",1,0,'C');
	$objreporte->Cell(17, $h,"VOLUMEN m".Cubico,1,1,'C');
	foreach($itemsC as $rowC)
	{
		
		//$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
		//			count(DISTINCT(c.nroinscripcion)) as nrousaurio ,sum(c.consumofact)
		//		from facturacion.detctacorriente as d
			//	inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
			//	and d.codciclo=c.codciclo and 
			//	d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
			//	d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
			//	inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
			//	and d.codconcepto=cp.codconcepto)
			//	inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
			//	c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
			//	inner join facturacion.categoriatarifaria as cat on 
			//	(tar.codcategoriatar=cat.codcategoriatar) 
				
			//	where  c.periodo ='".$periodo."'  and 
			//	/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
			//	and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0
			//	AND tar.codcategoriatar=".$rowC[0]." ".$CondEstSer.$CondTipFac;

		
		
		//$Sql = "select 
				//	count(DISTINCT(c.nroinscripcion)) as nrousaurio ,sum(c.consumofact)
				//from facturacion.cabctacorriente as c  

				//inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				//c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				//inner join facturacion.categoriatarifaria as cat on 
				//(tar.codcategoriatar=cat.codcategoriatar) 
				
				//where  c.periodo ='".$periodo."'  and 
				///*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				//and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0
				//AND c.nrodocumento >0 AND tar.codcategoriatar=".$rowC[0]." ".$CondEstSer.$CondTipFac;

			//--------------------------------------IMPORTE AGUA------------------------------------------------------------------//
			$Sql = "select 
				sum(importe), count(nrousaurio), sum(confac)
				
				from  
				(
				select 
				sum(det.importe-(det.importerebajado+det.imppagado)) as importe ,
				c.nroinscripcion, c.consumofact  as confac,
				count(DISTINCT(c.nroinscripcion)) as nrousaurio
				from  facturacion.cabctacorriente as c 
				inner join facturacion.detctacorriente as det on
				(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
				and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
				c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)
				
				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar)

				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0
				AND c.nrodocumento >0 AND   det.codconcepto in (1) AND tar.codcategoriatar=".$rowC[0]." ".$CondEstSer."".$CondTipFac."

				group by c.nroinscripcion, c.consumofact
				order by c.nroinscripcion ) as a ";

				//die($Sql);
		$ConsultaMo =$conexion->query($Sql);
		$rowMo		= $ConsultaMo->fetch();

		//------------------------------------CANTIDAD DE USUARIOS Y VOLUMEN--------------------------------------------------------------------//

		$Sql1 = "select 
				count(DISTINCT(c.nroinscripcion)) as nrousaurio ,sum(c.consumofact)
				from facturacion.cabctacorriente as c  

				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar) 
				
				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0
				AND c.nrodocumento >0 AND tar.codcategoriatar=".$rowC[0]." ".$CondEstSer.$CondTipFac;

		$ConsultaMo1 =$conexion->query($Sql1);
		$rowMo1		= $ConsultaMo1->fetch();

		//--------------------------------------IMPORTE DESAGUE--------------------------------------------------------------//
		$Sql2 = "select 
				sum(importe)
				
				from  
				(
				select 
				sum(det.importe-(det.importerebajado+det.imppagado)) as importe
				from  facturacion.cabctacorriente as c 
				inner join facturacion.detctacorriente as det on
				(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
				and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
				c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)
				
				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar)

				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0
				AND c.nrodocumento >0 AND   det.codconcepto in (2) AND tar.codcategoriatar=".$rowC[0]." ".$CondEstSer."".$CondTipFac."

				group by c.nroinscripcion, c.consumofact
				order by c.nroinscripcion ) as a ";

				//die($Sql);
		$ConsultaMo2 =$conexion->query($Sql2);
		$rowMo2		= $ConsultaMo2->fetch();

		$objreporte->SetTextColor(255,0,0);
		$objreporte->SetFont('Arial','B',6);
		$objreporte->SetX($x);
		$objreporte->Cell(100, $h,strtoupper($rowC[1]),0,0,'L');
		$objreporte->Cell(15, $h,number_format($rowMo[0],2),0,0,'R');
		$objreporte->Cell(20, $h,number_format($rowMo2[0],2),0,0,'R');
		$objreporte->Cell(20, $h,$rowMo1[0],0,0,'R');
		$objreporte->Cell(20, $h,number_format($rowMo1[1],0)." m".Cubico,0,1,'R');
	
		
		$sqlT  = "select catetar,".$objreporte->SubString('nomtar',1,3)." as nomtar,hastarango1,hastarango2,hastarango3
		from facturacion.tarifas  
		where codemp=1 AND codsuc=:codsuc  ";
		$sqlT .= " and codcategoriatar=:codcategoriatar  ";
		$sqlT .= "order by catetar";
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			
			/*$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 ".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();

			$objreporte->SetFont('Arial','B',6);
			$objreporte->SetX($x+5);
			$objreporte->Cell(65, $h,strtoupper($rowT[1]),0,0,'l');
			$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
			$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
			*/
			//INICIO POR RANGOS
			//RANGO INICIAL
			$RangoInicial = 0;
			$RangoFinal = $rowT['hastarango1'];
			$objreporte->Rangos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,'Rango Inicial ( '.number_format($RangoInicial,2).' - '.number_format($RangoFinal,2).' )',$CondEstSer,$CondTipFac,$_GET["tfact"]);
			//RANGO INICIAL
			//Rango Intermedio
			$RangoInicial = $rowT['hastarango1']+0.01;
			$RangoFinal = $rowT['hastarango2'];
			$objreporte->Rangos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,'Rango Intermedio ( '.number_format($RangoInicial,2).' - '.number_format($RangoFinal,2).' )',$CondEstSer,$CondTipFac,$_GET["tfact"]);
			//Rango Intermedio
					//Rango Final
			$May=$rowT['hastarango2'];
				if($May==0)
					$May=$rowT['hastarango1'];
			$RangoInicial = $May+0.01;
			$RangoFinal = 1000000;//$rowT['hastarango3'];
			$objreporte->Rangos($periodo,$rowT["catetar"],$codsuc,$ciclo,$RangoInicial,$RangoFinal,'Rango Final ( Mayor a '.number_format($May,2).' )',$CondEstSer,$CondTipFac,$_GET["tfact"]);
			
		}
		
		

	}
	
	//$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
		//	count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
			//	from facturacion.detctacorriente as d
			//	inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
			//	and d.codciclo=c.codciclo and 
			//	d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
			//	d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				
				//where  c.periodo ='".$periodo."'  and 
			//	/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
			//	and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 ".$CondEstSer;


	//$Sql = "select 
			//count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
		
				//from facturacion.cabctacorriente as c 

				//where  c.periodo ='".$periodo."'  and 
				///*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				//and c.codciclo=".$ciclo." AND c.tipo=0  AND c.nrodocumento >0 AND c.tipoestructura=0 ".$CondEstSer;

	//--------------------------------------IMPORTE AGUA------------------------------------------------------------------//
		$Sql = "select 
				sum(importe), count(nrousaurio), sum(confac)
				
				from  
				(
				select 
				sum(det.importe-(det.importerebajado+det.imppagado)) as importe ,
				c.nroinscripcion, c.consumofact  as confac,
				count(DISTINCT(c.nroinscripcion)) as nrousaurio
				from  facturacion.cabctacorriente as c 
				inner join facturacion.detctacorriente as det on
				(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
				and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
				c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)

				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=0  AND   det.codconcepto in (1)  AND c.nrodocumento >0 AND c.tipoestructura=0 ".$CondEstSer."

				group by c.nroinscripcion, c.consumofact
				order by c.nroinscripcion ) as a ";


	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();

	//------------------------------------CANTIDAD DE USUARIOS Y VOLUMEN--------------------------------------------------------------------//

		$Sql1 = "select 
			count(DISTINCT(c.nroinscripcion)) as nrousaurio,sum(c.consumofact)
		
				from facturacion.cabctacorriente as c 

				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=0  AND c.nrodocumento >0 AND c.tipoestructura=0 ".$CondEstSer;


	$ConsultaMo1 =$conexion->query($Sql1);
	$rowMo1		= $ConsultaMo1->fetch();

	//--------------------------------------IMPORTE DESAGUE--------------------------------------------------------------//
	$Sql2 = "select 
			sum(importe)
			
			from  
			(
			select 
			sum(det.importe-(det.importerebajado+det.imppagado)) as importe 
			from  facturacion.cabctacorriente as c 
			inner join facturacion.detctacorriente as det on
			(c.codemp=det.codemp and c.codsuc=det.codsuc and c.codciclo=det.codciclo and c.nrofacturacion=det.nrofacturacion 
			and c.nroinscripcion=det.nroinscripcion and c.anio=det.anio and c.mes=det.mes and c.periodo=det.periodo and
			c.tipo=det.tipo and c.tipoestructura=det.tipoestructura)

			where  c.periodo ='".$periodo."'  and 
			/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
			and c.codciclo=".$ciclo." AND c.tipo=0  AND   det.codconcepto in (2)  AND c.nrodocumento >0 AND c.tipoestructura=0 ".$CondEstSer."

			group by c.nroinscripcion, c.consumofact
			order by c.nroinscripcion ) as a ";


	$ConsultaMo2 =$conexion->query($Sql2);
	$rowMo2		= $ConsultaMo2->fetch();


	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(175, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(100, $h,'TOTALES ',0,0,'l');
	////$objreporte->Cell(100, $h,'TOTAL FACTURACION DEL MES ',0,0,'l');
	$objreporte->Cell(15, $h,number_format($rowMo[0],2),0,0,'R');
	$objreporte->Cell(20, $h,number_format($rowMo2[0],2),0,0,'R');
	$objreporte->Cell(20, $h,$rowMo1[0],0,0,'R');
	$objreporte->Cell(20, $h,number_format($rowMo1[1],0)." m".Cubico,0,1,'R');

	
	$objreporte->Output();	
	
?>