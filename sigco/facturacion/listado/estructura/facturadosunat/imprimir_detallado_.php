<?php 
	include("../../../../../objetos/clsReporte.php");
	ini_set("memory_limit","512M");
	class clsUsuario extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses,$anio,$mes;
			
			$this->SetFont('Arial','B',10);
			
			$h = 4;
			$this->Cell(0, $h+2,"ESTRUCTURA TARIFARIA DE LA FACTURACION - IMPORTE - DETALLADO DEL MES",0,1,'C');
			$this->SetFont('Arial','B',8);
			if($mes==1)
				{
					$mesa=12;
					$anioa=$anio-1;
				}
			else
			{
				$mesa=$mes-1;
				$anioa=$anio;
			}
			$this->Cell(0, $h+2,$meses[$mesa]." - ".$anioa." - USUARIOS ACTIVOS",0,1,'C');
			
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(20,57,47,13,10,15,15,13,12,12,12,12,12,13,12,12,12));
			$this->SetAligns(array("C","L","L","C","C","R","R","R","R","R","R","R","R","R","R","R","R"));
			$this->Row(array("COD. CAT.",
							 "USUARIO",
							 "DIRECCION",
							 "TS",
							 "CAT.",
							 "LECT. ANT.",
							 "LECT. ULT.",
							 "CONS",
							 "Agua",
							 "Desague",
							 'Otros',
							 'Intereses',
							 'Cargo',
							 'Redondeo',
							 
							 
							 'Total'
							 ));//Agua Desague Otros Intereses Cargo Redondeo Mes Deuda Total

			
			
		}
		function Contenido($codcatastro,$usuario,$lectanterior,$lectultima,$consumo,$direccion,$servicio=2,$categoria,$impagua,$impdesague,$impotros,$interes,$impcargo,$impredondeo)
		{
			$h=6;
			$this->SetFont('Arial','',7);
			
			$ts="";
			if($servicio==1)
			{
				$ts="Ag/Des";
			}
			if($servicio==2)
			{
				$ts="Ag";
			}
			if($servicio==3)
			{
				$ts="Des";
			}
			
			$this->SetWidths(array(20,57,47,13,10,15,15,13,12,12,12,12,12,13,12,12,12));
			$this->SetAligns(array("C","L","L","C","C","R","R","R","R","R","R","R","R","R","R","R","R"));
			$Total = number_format( $impagua+$impdesague+$impotros+$interes+$impcargo+$impredondeo,2);
			$this->Row(array($codcatastro,
							 $usuario,
							 $direccion,
							 $ts,
							 $categoria,
							 intval($lectanterior),
							 intval($lectultima),
							 intval($consumo),
							 $impagua,$impdesague,$impotros,$interes,$impcargo,$impredondeo,$Total));

		}
    }
	
	$ciclo		= $_GET["ciclo"];
	//$sector		= "%".$_GET["sector"]."%";
	//$consumo 	= $_GET["consumo"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	//$consumoh	= $_GET["consumohasta"];
	//$tfact		= "%".$_GET["tfact"]."%";
	$codsuc		= $_GET["codsuc"];
	 $periodo =$anio.$mes;

	$objReporte	=	new clsUsuario();
	$objReporte->AliasNbPages();
	$objReporte->AddPage('L');
	
	$importe 	= 0;
	$count		= 0;
	
	//$facturacion = $objReporte->datosfacturacion($codsuc,$codciclo);
	//$nrofacturacion = ($facturacion["nrofacturacion"]-1);

	/*$consulta = $conexion->prepare("select nrofacturacion,anio,mes,lecturas,saldo,facturacion,tasainteres 
											from facturacion.periodofacturacion 
											where codemp=1 and codsuc=? and codciclo=? AND anio=? AND mes=?");
	$consulta->execute(array($codsuc,$ciclo,$anio,$mes));
				
	$facturacion = $consulta->fetch();
	$nrofacturacion = ($facturacion["nrofacturacion"]);*/
	//print_r($facturacion);die();

	$sql  = "select clie.propietario,cab.lecturaanterior,cab.lecturaultima,cab.consumo,
			(tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle) as direccion,cab.codtiposervicio,
			upper(substring(tar.nomtar,1,3)) as nomtar,".$objReporte->getCodCatastral("clie.").",cab.nroinscripcion as nroinscripcion
			from facturacion.cabctacorriente as cab 
			inner join catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and cab.nroinscripcion=clie.nroinscripcion)
			inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
			inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc and cab.catetar=tar.catetar)
			where cab.codestadoservicio = 1 AND cab.periodo=:periodo 
			order by codcatastro ";

	$consulta = $conexion->prepare($sql);
	/*if($mes==1)
		{
			$mesa=12;
			$anioa=$anio-1;
		}
	else
	{
		$mesa=$mes-1;
		$anioa=$anio;
	}*/
	$consulta->execute(array(":periodo"=>$periodo));
	$items = $consulta->fetchAll();

	$impaguat      = 0;
	$impdesaguet   = 0;
	$interest      = 0;
	$impotrost     = 0;
	$impredondeot  = 0;
	$impcargot     = 0;
	$impfonavit    = 0;
	$igvt          = 0;
	$deudat        = 0;
	$TotalUsuarios = 0;
	foreach($items as $row)
	{
		$importe+=$row["monto"];
		$count++;
		$TotalUsuarios++;
		/////////////
		$impagua		= 0;
		$impdesague		= 0;
		$interes		= 0;
		$impotros		= 0;
		$impredondeo	= 0;
		$impcargo		= 0;
		$impfonavi		= 0;
		
		$sqlF = "select CAST (c.categoria AS INTEGER ) ,sum(d.importe-(d.importerebajado+d.imppagado)) 
		from facturacion.detctacorriente as d
		inner join facturacion.conceptos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto)
		where d.codsuc=:codsuc and  d.codciclo=:codciclo and d.periodo=:periodo and d.estadofacturacion=1 AND d.categoria=0 and 
		d.nroinscripcion=:nroinscripcion  
		group by  CAST (c.categoria AS INTEGER )   ";
		/*$sqlF2 = "select c.categoria,sum(d.importe-(d.importerebajado+d.imppagado)) from facturacion.detctacorriente as d
				 inner join facturacion.conceptos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto)
				 where d.nrofacturacion=".$nrofacturacion." 
				 and d.estadofacturacion=1 and 
				 d.nroinscripcion=".$row["nroinscripcion"]." and 
				 d.codsuc=".$codsuc." and 
				 d.codciclo=".$ciclo." group by  c.categoria,d.codconcepto
				 having sum(d.importe-(d.importerebajado+d.imppagado))>0 order by d.codconcepto  ";
		*/
		//if($row["nroinscripcion"]==202)
			//die($sqlF);
		$consultaF = $conexion->prepare($sqlF);
		$consultaF->execute(array(":periodo"=>$periodo,":nroinscripcion"=>$row["nroinscripcion"],
								  ":codsuc"=>$codsuc,":codciclo"=>$ciclo));
		$itemsF = $consultaF->fetchAll();
		
		foreach($itemsF as $rowF)
		{
			switch ($rowF[0]) 
			{
			    case 1:
			       $impagua += $rowF[1];$impaguat += $rowF[1];
			    break;
			    case 2:
			       $impdesague += $rowF[1];$impdesaguet += $rowF[1];
			    break;
			    case 3:
			        $interes += $rowF[1];$interest += $rowF[1];
			    break;
			    case 4:
			        $igv += $rowF[1];$igvt += $rowF[1];
			    break;
			    case 5:
			      $impcargo += $rowF[1];$impcargot += $rowF[1];
			    break;
			    case 7:
			      $impredondeo += $rowF[1];$impredondeot += $rowF[1];
			    break;
			    case 8:
			      $impfonavi += $rowF[1];$impfonavit += $rowF[1];
			    break;
			    default:
			       $impotros += $rowF[1];$impotrost += $rowF[1];
			}
		}
		
				///////////////



		$objReporte->Contenido($row["codcatastro"],$row["propietario"],$row["lecturaanterior"],$row["lecturaultima"],$row["consumo"],
						strtoupper($row["direccion"]),$row["codtiposervicio"],$row["nomtar"],
						$impagua,$impdesague,$impotros,$interes,$impcargo,$impredondeo);
			
	}
	$objReporte->ln(5);
	$objReporte->SetFont('Arial','B',8);
	$objReporte->Cell(166, 5,"Totales : ",0,1,'C');
	
	$objReporte->SetX(120);
	$objReporte->Cell(12, 5,"Usuarios",0,0,'R');//12,
	$objReporte->Cell(12, 5,":",0,0,'C');//12,
	$objReporte->Cell(12, 5,$TotalUsuarios,0,1,'R');//12,

	$objReporte->SetX(120);
	$objReporte->Cell(12, 5,"Agua",0,0,'R');//12,
	$objReporte->Cell(12, 5,":",0,0,'C');//12,
	$objReporte->Cell(12, 5,number_format($impaguat,2),0,1,'R');//12,

	$objReporte->SetX(120);
	$objReporte->Cell(12, 5,"Desague",0,0,'R');//12,
	$objReporte->Cell(12, 5,":",0,0,'C');//12,
	$objReporte->Cell(12, 5,number_format($impdesaguet,2),0,1,'R');//12,
	$objReporte->SetX(120);
	$objReporte->Cell(12, 5,"Otros",0,0,'R');//12,
	$objReporte->Cell(12, 5,":",0,0,'C');//12,
	$objReporte->Cell(12, 5,number_format($impotrost,2),0,1,'R');//12,
	$objReporte->SetX(120);
	$objReporte->Cell(12, 5,"Intereses",0,0,'R');//12,
	$objReporte->Cell(12, 5,":",0,0,'C');//12,
	$objReporte->Cell(12, 5,number_format($interest,2),0,1,'R');//12,
	$objReporte->SetX(120);
	$objReporte->Cell(12, 5,"Cargo",0,0,'R');//12,
	$objReporte->Cell(12, 5,":",0,0,'C');//12,
	$objReporte->Cell(12, 5,number_format($impcargot,2),0,1,'R');//12,
	$objReporte->SetX(120);
	$objReporte->Cell(12, 5,"Redondeo",0,0,'R');//13,
	$objReporte->Cell(12, 5,":",0,0,'C');//12,
	$objReporte->Cell(12, 5,number_format($impredondeot,2),0,1,'R');//13,
	$objReporte->SetX(120);
	//$objReporte->Cell(12, 5,'',0,0,'C');//12

	///////7
	$objReporte->SetX(120);
	$objReporte->Cell(12, 5,"",0,0,'C');//,12,12
	$objReporte->Cell(12, 5,"",0,0,'C');//12,
	$objReporte->Cell(12, 0.01,'',1,1,'R');//,12,12

	$objReporte->ln(2);
	$objReporte->SetX(120);
	$Total = $impaguat+$impdesaguet+$impotrost+$interest+$impcargot+$impredondeot;
	$objReporte->Cell(12, 5,'Total',0,0,'R');//,12,12
	$objReporte->Cell(12, 5,":",0,0,'C');//12,
	$objReporte->Cell(12, 5,number_format($Total,2),0,0,'R');//,12,12
	$objReporte->Output();	
	
?>