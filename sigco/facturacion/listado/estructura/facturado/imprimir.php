<?php
	include("../../../../../objetos/clsReporte.php");
	
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $mes,$anio,$meses,$x,$EstadoServicioN;
			
			$h=4;
			$this->SetFont('Arial','B',8);
			$this->Cell(0, $h+1,"ESTRUCTURA TARIFARIA - IMPORTE ",0,1,'C');
			$this->SetFont('Arial','B',8);
			
			if($mes==1)
				{
					$mesa=12;
					$anioa=$anio;
					$aniob=$anio-1;

				}
			else
			{
				$mesa=$mes;
				$mesb=$mes-1;
				$anioa=$anio;
				$aniob=$anio;
				if($mesa==1)
					{
						$mesb=12;
						$aniob=$anio-1;
					}
			}
			$this->Cell(0, $h+1,"FACTURACION DE ".$meses[$mesa]."-".$anioa." DEL CONSUMO DE ".$meses[$mesb]." - ".$aniob,0,1,'C');
			if($EstadoServicioN!="")
				$this->Cell(0, $h+1,"ESTADO DE SERVICIO : ".$EstadoServicioN,0,1,'C');
			$this->Ln(2);
			
			
			
			
		}
		function Leyenda()
		{
			global $x,$conexion;

		}
    
    }
	
	$anio           = $_GET["anio"];
	$mes            = $_GET["mes"];
	$ciclo          = $_GET["ciclo"];
	$codsuc         = $_GET["codsuc"];
	$EstadoServicio = $_GET["EstadoServicio"];
	$EstadoServicioN = $_GET["EstadoServicioN"];
    $periodo =$anio.$mes;
    $x = 50;
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("P");
	$CondEstSer = "";
	if($EstadoServicio!="")
		$CondEstSer = " AND c.codestadoservicio=".$EstadoServicio." ";

	$sqlC  = "select cat.codcategoriatar,cat.descripcion 
	from facturacion.cabctacorriente as c ";
	$sqlC .= "inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and ";
	$sqlC .= "c.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "where c.periodo='".$periodo."' and c.codsuc=".$codsuc." 
	and c.codciclo=".$ciclo." 
	AND c.tipo=0 AND c.tipoestructura=0 ".$CondEstSer;
	$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
	$sinFonavi=" AND cp.categoria<>8 ";
	$consultaC = $conexion->query($sqlC);
	$itemsC = $consultaC->fetchALl();
	$h=4;
	$objreporte->SetFont('Arial','B',7);
	$objreporte->SetX($x-10);
	$objreporte->Cell(70, $h,'- FACTURACION DEL MES',0,1,'l');
	$objreporte->SetFont('Arial','',6);
		
	$objreporte->Ln(2);
	$objreporte->SetX($x);
	$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
	$objreporte->Cell(40, $h,"CONCEPTOS",1,0,'L');
	$objreporte->Cell(20, $h,"IMPORTE",1,0,'C');
	$objreporte->Cell(20, $h,"USUARIOS",1,1,'C');
	foreach($itemsC as $rowC)
	{
		
		$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar) 
				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0
				AND tar.codcategoriatar=".$rowC[0]." ".$CondEstSer.$sinFonavi;
		$ConsultaMo =$conexion->query($Sql);
		$rowMo		= $ConsultaMo->fetch();
		$objreporte->SetTextColor(255,0,0);
		$objreporte->SetFont('Arial','B',6);
		$objreporte->SetX($x);
		$objreporte->Cell(70, $h,utf8_decode(strtoupper($rowC[1])),0,0,'l');
		$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
		$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	
		
		
		$sqlT  = "select catetar,substring(nomtar,1,3) as nomtar 
		from facturacion.tarifas  
		where codemp=1 AND codsuc=:codsuc  ";
		$sqlT .= " and codcategoriatar=:codcategoriatar  ";
		$sqlT .= "order by catetar";
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			
			/*$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 ".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();

			$objreporte->SetFont('Arial','B',6);
			$objreporte->SetX($x+5);
			$objreporte->Cell(65, $h,strtoupper($rowT[1]),0,0,'l');
			$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
			$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
			*/
			$objreporte->SetTextColor(0,0,0);
			$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo
				FROM facturacion.cabctacorriente c
			  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
			  AND (c.codsuc = dc.codsuc)
			  AND (c.codciclo = dc.codciclo)
			  AND (c.nrofacturacion = dc.nrofacturacion)
			  AND (c.nroinscripcion = dc.nroinscripcion)
			  AND (c.anio = dc.anio)
			  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
			  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
			  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
			  AND c.periodo='".$periodo."' AND c.catetar=".$rowT["catetar"]."
			  AND c.tipo=0 AND c.tipoestructura=0 ".$CondEstSer."
			  ORDER BY co.ordenrecibo";
			  $ConsultaCo =$conexion->query($Sql);
			foreach($ConsultaCo->fetchAll() as $rowCo)
			{
				$objreporte->SetFont('Arial','',6);
				$objreporte->SetX($x +30);
				$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
				$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar=".$rowT["catetar"]." and 
					/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0 AND c.tipoestructura=0
					AND cp.codconcepto=".$rowCo[0]." ".$CondEstSer;
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
				//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
				
			}
		}
		
		

		
	

	}
	
	$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 ".$CondEstSer.$sinFonavi;
	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();
	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(110, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(70, $h,'TOTAL FACTURACION DEL MES ',0,0,'l');
	$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');

	////////////SALDO
	$objreporte->AddPage("P");

	$sqlC  = "select cat.codcategoriatar,cat.descripcion 
	from facturacion.cabctacorriente as c ";
	$sqlC .= "inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and ";
	$sqlC .= "c.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "where c.periodo=:periodo and c.codsuc=:codsuc and c.codciclo=:codciclo
	AND c.tipo=1 AND c.tipoestructura=0 ".$CondEstSer;
	$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
	
	$consultaC = $conexion->prepare($sqlC);
	$consultaC->execute(array(":periodo"=>$periodo,
							  ":codsuc"=>$codsuc,
							  ":codciclo"=>$ciclo));
	$itemsC = $consultaC->fetchALl();
	$h=4;
	$objreporte->SetTextColor(0,0,0);
	$objreporte->SetFont('Arial','B',7);
	$objreporte->SetX($x-10);
	$objreporte->Cell(70, $h,'- FACTURACION DEUDA ACUMULADA',0,1,'l');
	$objreporte->SetFont('Arial','',6);
		
	$objreporte->Ln(2);
	$objreporte->SetX($x);
	$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
	$objreporte->Cell(40, $h,"CONCEPTOS",1,0,'L');
	$objreporte->Cell(20, $h,"IMPORTE",1,0,'C');
	$objreporte->Cell(20, $h,"USUARIOS",1,1,'C');
	foreach($itemsC as $rowC)
	{
		
		$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
				and d.codconcepto=cp.codconcepto)
				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar) 
				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=1  AND c.tipoestructura=0
				AND tar.codcategoriatar=".$rowC[0]." ".$CondEstSer.$sinFonavi;
		$ConsultaMo =$conexion->query($Sql);
		$rowMo		= $ConsultaMo->fetch();
		$objreporte->SetTextColor(255,0,0);
		$objreporte->SetFont('Arial','B',6);
		$objreporte->SetX($x);
		$objreporte->Cell(70, $h,utf8_decode(strtoupper($rowC[1])),0,0,'l');
		$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
		$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	
		
		
		$sqlT  = "select catetar,substring(nomtar,1,3) as nomtar 
		from facturacion.tarifas  
		where codemp=1 AND codsuc=:codsuc  ";
		$sqlT .= " and codcategoriatar=:codcategoriatar  ";
		$sqlT .= "order by catetar";
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			
			/*$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0 ".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();

			$objreporte->SetFont('Arial','B',6);
			$objreporte->SetX($x+5);
			$objreporte->Cell(65, $h,strtoupper($rowT[1]),0,0,'l');
			$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
			$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
			*/
			$objreporte->SetTextColor(0,0,0);
			$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo
				FROM facturacion.cabctacorriente c
			  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
			  AND (c.codsuc = dc.codsuc)
			  AND (c.codciclo = dc.codciclo)
			  AND (c.nrofacturacion = dc.nrofacturacion)
			  AND (c.nroinscripcion = dc.nroinscripcion)
			  AND (c.anio = dc.anio)
			  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
			  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
			  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
			  AND c.periodo='".$periodo."' AND c.catetar=".$rowT["catetar"]."
			  AND c.tipo=1 AND c.tipoestructura=0 ".$CondEstSer."
			  ORDER BY co.ordenrecibo";
			  $ConsultaCo =$conexion->query($Sql);
			foreach($ConsultaCo->fetchAll() as $rowCo)
			{
				$objreporte->SetFont('Arial','',6);
				$objreporte->SetX($x +30);
				$objreporte->Cell(40, $h,utf8_decode(strtoupper($rowCo[1])),0,0,'L');
				$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar=".$rowT["catetar"]." and 
					/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=1  AND c.tipoestructura=0
					AND cp.codconcepto=".$rowCo[0]." ".$CondEstSer;
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
				//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
				
			}
		}
		
		

		
	

	}
	
	$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=1 AND c.tipoestructura=0 ".$CondEstSer.$sinFonavi;
	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();
	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(110, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(70, $h,'TOTAL FACTURACION DEUDA ACUMULADA ',0,0,'l');
	$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');

	////////////TOTAL
	$objreporte->AddPage("P");
	$aConceptos = array();
	$sqlC  = "select cat.codcategoriatar,cat.descripcion 
	from facturacion.cabctacorriente as c ";
	$sqlC .= "inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and ";
	$sqlC .= "c.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "where c.periodo='".$periodo."' and c.codsuc=".$codsuc." 
	and c.codciclo=".$ciclo." AND c.tipoestructura=0 ".$CondEstSer;
	$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
	
	$consultaC = $conexion->query($sqlC);
	$itemsC = $consultaC->fetchALl();
	$h=4;
	$objreporte->SetTextColor(0,0,0);
	$objreporte->SetFont('Arial','B',7);
	$objreporte->SetX($x-10);
	$objreporte->Cell(70, $h,'- FACTURACION TOTAL',0,1,'l');
	$objreporte->SetFont('Arial','',6);
		
	$objreporte->Ln(2);
	$objreporte->SetX($x);
	$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
	$objreporte->Cell(40, $h,"CONCEPTOS",1,0,'L');
	$objreporte->Cell(20, $h,"IMPORTE",1,0,'C');
	$objreporte->Cell(20, $h,"USUARIOS",1,1,'C');
	foreach($itemsC as $rowC)
	{
		
		$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
				and d.codconcepto=cp.codconcepto)
				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar) 
				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." 
				AND tar.codcategoriatar=".$rowC[0]." AND c.tipoestructura=0 ".$CondEstSer.$sinFonavi;
		$ConsultaMo =$conexion->query($Sql);
		$rowMo		= $ConsultaMo->fetch();
		$objreporte->SetTextColor(255,0,0);
		$objreporte->SetFont('Arial','B',6);
		$objreporte->SetX($x);
		$objreporte->Cell(70, $h,utf8_decode(strtoupper($rowC[1])),0,0,'l');
		$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
		$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	
		
		
		$sqlT  = "select catetar,substring(nomtar,1,3) as nomtar 
		from facturacion.tarifas  
		where codemp=1 AND codsuc=:codsuc  ";
		$sqlT .= " and codcategoriatar=:codcategoriatar  ";
		$sqlT .= "order by catetar";
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			
			/*$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipoestructura=0 	".$CondEstSer;
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();

			$objreporte->SetFont('Arial','B',6);
			$objreporte->SetX($x+5);
			$objreporte->Cell(65, $h,strtoupper($rowT[1]),0,0,'l');
			$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
			$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
			*/
			$objreporte->SetTextColor(0,0,0);
			$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo,co.categoria
				FROM facturacion.cabctacorriente c
			  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
			  AND (c.codsuc = dc.codsuc)
			  AND (c.codciclo = dc.codciclo)
			  AND (c.nrofacturacion = dc.nrofacturacion)
			  AND (c.nroinscripcion = dc.nroinscripcion)
			  AND (c.anio = dc.anio)
			  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
			  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
			  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
			  AND c.periodo='".$periodo."' AND c.catetar=".$rowT["catetar"]." 
			  AND c.tipoestructura=0 ".$CondEstSer." ORDER BY co.ordenrecibo";
			  $ConsultaCo =$conexion->query($Sql);
			foreach($ConsultaCo->fetchAll() as $rowCo)
			{
				$objreporte->SetFont('Arial','',6);
				$objreporte->SetX($x +30);
				$objreporte->Cell(40, $h,utf8_decode(strtoupper($rowCo[1])),0,0,'L');
				$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar=".$rowT["catetar"]." and 
					/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo."  
					AND cp.codconcepto=".$rowCo[0]." AND c.tipoestructura=0 ".$CondEstSer;
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');

				if (!array_key_exists($rowCo[0], $aConceptos))
						{
							$aConceptos[$rowCo[0]] = array("Id"=>$rowCo[0], "Cantidad"=>$rowMo[0],"Descripcion"=>$rowCo[1],"Categoria"=>$rowCo[3]);
						}
						else
						{
							$aConceptos[$rowCo[0]] = array("Id"=>$rowCo[0], "Cantidad"=>$aConceptos[$rowCo[0]]["Cantidad"] + $rowMo[0],"Descripcion"=>$rowCo[1],"Categoria"=>$rowCo[3]);
						}

				//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
				
			}
		}
		
		

		
	

	}
	
	$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
				where  c.periodo ='".$periodo."'  and 
				/*d.categoria=:categoria and*/ c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipoestructura=0  ".$CondEstSer.$sinFonavi;
	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();
	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(110, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(70, $h,'TOTAL FACTURACION ',0,0,'l');
	$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	$objreporte->Ln(2);
	$objreporte->AddPage("P");
	$objreporte->SetX($x-10);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(70, $h,'RESUMEN',0,1,'l');
	$objreporte->SetTextColor(0,0,0);
	sort($aConceptos);
	$Total=0;
	for($i=0;$i<count($aConceptos);$i++)
	{
		$objreporte->SetX($x+10);
		$objreporte->Cell(40,$h,utf8_decode($aConceptos[$i][Descripcion]),0,0,'R');
		$objreporte->Cell(20,$h,":",0,0,'C');
		$objreporte->Cell(20,$h,number_format($aConceptos[$i][Cantidad],2),0,1,'R');
		if($aConceptos[$i][Categoria]!=8)
		$Total+=$aConceptos[$i][Cantidad];

	}
	$objreporte->SetX($x+70);
	$objreporte->Cell(20,'0.01',"",1,1,'R');
	$objreporte->SetX($x+10);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(40,$h,"TOTALES",0,0,'R');
	$objreporte->Cell(20,$h,":",0,0,'C');
	$objreporte->Cell(20,$h,number_format($Total,2),0,1,'R');
	$objreporte->Output();	
	
?>