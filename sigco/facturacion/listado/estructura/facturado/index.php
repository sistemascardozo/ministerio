<?php
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");

	$TituloVentana = "ESTRUCTURA TARIFARIA - FACTURADO";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objMantenimiento 	= new clsDrop();
	
	$codsuc = $_SESSION['IdSucursal'];
	
	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
	
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="../../usuariosfacturados/js_usuarios.js" language="JavaScript"></script>
<script>

jQuery(function($)
	{ 
		$( "#DivTipos" ).buttonset();
	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" ;
	var codsuc 	= "<?=$codsuc?>";
	function ver(z){}
	function ValidarForm(Op)
	{
		var ciclos  = $("#ciclo").val()
		
		if(ciclos==0)
		{
			alert("Seleccione el Ciclo")
			return false
		}
		if($("#anio").val()==0)
		{
			alert('Seleccione el Anio a Consultar')
			return false
		}
		if($("#mes").val()==0)
		{
			alert('Seleccione el Mes a Consultar')
			return false
		}
		var EstadoServicio=''
		if($('#Todos').attr("checked")!="checked")
		{
			if($("#estadoservicio").val()==0)
			{
				alert('Seleccione Estado de Servicio')
				return false
			}

			EstadoServicio = '&EstadoServicio='+$("#estadoservicio").val()+'&EstadoServicioN='+$("#estadoservicio option:selected").html()
		}

		if(document.getElementById("rabresumen").checked==true)
		{
			url = "imprimir.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		if(document.getElementById("rabresumenhor").checked==true)
		{
			url = "imprimir_.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		if(document.getElementById("rabdetallado").checked==true)
		{
			url = "imprimir_detallado.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		if(document.getElementById("rabdetalladosaldo").checked==true)
		{
			url = "imprimir_detalladosaldo.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		if(document.getElementById("rabdetalladototal").checked==true)
		{
			url = "imprimir_detalladototal.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		url +=EstadoServicio
		AbrirPopupImpresion(url,800,600)
		
		return false
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
function TodosEstados()
	{
		if($('#Todos').attr("checked")=="checked")
		{
			
			
			$('#estadoservicio').attr('disabled', 'disabled');
		
			
		}
		else
		{
		
			$('#estadoservicio').attr('disabled', false);
			
		}
	
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="700" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	
	<tr>
       <td colspan="2">&nbsp;</td>
	</tr>
    <tr>
        <td colspan="2" align="center">
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="90">Sucursal</td>
				    <td width="30" align="center">:</td>
				    <td>
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </td>
				    <td width="80" align="right">Ciclo</td>
				    <td width="30" align="center">:</td>
				    <td>
                    	<? $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio(this.value,1);'"); ?>
                    </td>
				  </tr>
				  <tr>
				    <td>A&ntilde;o</td>
				    <td align="center">:</td>
				    <td>
                    	<div id="div_anio">
                        	<? $objMantenimiento->drop_anio($codsuc,0); ?>
                        </div>
                    </td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
				    <td>Mes</td>
				    <td align="center">:</td>
				    <td>
                    	<div id="div_meses">
                        	<? $objMantenimiento->drop_mes($codsuc,0,0); ?>
                        </div>
                    </td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>	
			      <tr>
                        <td align="right">Est. de Servicio</td>
                        <td align="center">:</td>
                        <td><? $objMantenimiento->drop_estado_servicio($items["codestadoservicio"],""); ?></td>
                        <td align="right">&nbsp;</td>
                        <td align="center"><input type="checkbox" id="Todos" onclick="TodosEstados();"></td>
                        <td>
                        	Todos
                        </td>
                      </tr>
			      <tr>
       <td colspan="2">&nbsp;</td>
	</tr>
			      <tr>
        			<td colspan="6" align="center">
        				<div id="DivTipos" style="display:inline">
        					
	                           <input type="radio" name="rabresumen" id="rabresumen" value="radio" checked="checked" />
	                            <label for="rabresumen">Resumen Vertical</label>&nbsp;
	                            <input type="radio" name="rabresumen" id="rabresumenhor" value="radio5" checked="checked" />
	                            <label for="rabresumenhor">Resumen Horizontal</label>&nbsp;
	                            <input type="radio" name="rabresumen" id="rabdetallado" value="radio2" />
	                            <label for="rabdetallado">Detallado Mes</label>&nbsp;
	                            <input type="radio" name="rabresumen" id="rabdetalladosaldo" value="radio3" />
	                            <label for="rabdetalladosaldo">Detallado Saldo</label>&nbsp;
	                            <input type="radio" name="rabresumen" id="rabdetalladototal" value="radio4" />
	                            <label for="rabdetalladototal">Detallado Total</label>
	                         
						</div>	
        			</td>
        		</tr>	
        		<tr><td colspan="6">&nbsp;</td></tr>
        		<tr><td colspan="6" align="center"> <input type="button" onclick="return ValidarForm();" value="Exportar" id=""></td></tr>		  
				<tr><td colspan="6">&nbsp;</td></tr>
				</table>
		</td>

	 </tbody>

    </table>
 </form>
</div>
<?php CuerpoInferior(); ?>