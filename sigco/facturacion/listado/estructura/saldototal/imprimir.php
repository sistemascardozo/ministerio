<?php 
	include("../../../../../objetos/clsReporte.php");
	
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $mes,$anio,$meses,$x;
			
			$h=4;
			$this->SetFont('Arial','B',8);
			$this->Cell(0, $h+1,"ESTRUCTURA TARIFARIA - IMPORTE ",0,1,'C');
			$this->SetFont('Arial','B',8);
			
			if($mes==1)
				{
					$mesa=12;
					$anioa=$anio;
					$aniob=$anio-1;

				}
			else
			{
				$mesa=$mes;
				$mesb=$mes-1;
				$anioa=$anio;
				$aniob=$anio;
				if($mesa==1)
					{
						$mesb=12;
						$aniob=$anio-1;
					}
			}
			$this->Cell(0, $h+1,"FACTURACION DE ".$meses[$mesa]."-".$anioa." DEL CONSUMO DE ".$meses[$mesb]." - ".$aniob,0,1,'C');
			$this->Ln(2);
			
			
			
			
		}
		function Leyenda()
		{
			global $x,$conexion;

		}
    
    }
	
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclo		= $_GET["ciclo"];
	$codsuc		= $_GET["codsuc"];
	$Desde		= $_GET["Desde"];
	$Hasta		= $_GET["Hasta"];
    $periodo =$anio.$mes;
    $x = 50;
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("P");
	$h=4;
	$sinFonavi=" AND cp.categoria<>8 ";
	/*
	$objreporte->AddPage("P");
	$sqlC  = "select cat.codcategoriatar,cat.descripcion 
	from facturacion.cabctacorriente as cab ";
	$sqlC .= "inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc and ";
	$sqlC .= "cab.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "where cab.periodo=:periodo and cab.codsuc=:codsuc and cab.codciclo=:codciclo
	AND cab.tipo=0 AND cab.tipoestructura=0 ";
	$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
	
	$consultaC = $conexion->prepare($sqlC);
	$consultaC->execute(array(":periodo"=>$periodo,
							  ":codsuc"=>$codsuc,
							  ":codciclo"=>$ciclo));
	$itemsC = $consultaC->fetchALl();
	
	$objreporte->SetFont('Arial','B',7);
	$objreporte->SetX($x-10);
	$objreporte->Cell(70, $h,'- FACTURACION DEL MES',0,1,'l');
	$objreporte->SetFont('Arial','',6);
		
	$objreporte->Ln(2);
	$objreporte->SetX($x);
	$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
	$objreporte->Cell(40, $h,"CONCEPTOS",1,0,'L');
	$objreporte->Cell(20, $h,"IMPORTE",1,0,'C');
	$objreporte->Cell(20, $h,"USUARIOS",1,1,'C');
	foreach($itemsC as $rowC)
	{
		
		$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
				and d.codconcepto=cp.codconcepto)
				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar) 
				where  c.periodo ='".$periodo."'  and 
				 c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0
				AND tar.codcategoriatar='".$rowC[0]."'";
		$ConsultaMo =$conexion->query($Sql);
		$rowMo		= $ConsultaMo->fetch();
		$objreporte->SetTextColor(255,0,0);
		$objreporte->SetFont('Arial','B',6);
		$objreporte->SetX($x);
		$objreporte->Cell(70, $h,strtoupper($rowC[1]),0,0,'l');
		$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
		$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	
		
		
		$sqlT  = "select catetar,substring(nomtar,1,3) as nomtar 
		from facturacion.tarifas  
		where codemp=1 AND codsuc=:codsuc  ";
		$sqlT .= " and codcategoriatar=:codcategoriatar  ";
		$sqlT .= "order by catetar";
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			
			/*$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0	";
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();

			$objreporte->SetFont('Arial','B',6);
			$objreporte->SetX($x+5);
			$objreporte->Cell(65, $h,strtoupper($rowT[1]),0,0,'l');
			$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
			$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
			*/
			/*$objreporte->SetTextColor(0,0,0);
			$Sql="SELECT DISTINCT(c.codconcepto), c.descripcion,c.ordenrecibo
				FROM facturacion.cabctacorriente cc
			  INNER JOIN facturacion.detctacorriente dc ON (cc.codemp = dc.codemp)
			  AND (cc.codsuc = dc.codsuc)
			  AND (cc.codciclo = dc.codciclo)
			  AND (cc.nrofacturacion = dc.nrofacturacion)
			  AND (cc.nroinscripcion = dc.nroinscripcion)
			  AND (cc.anio = dc.anio)
			  AND (cc.mes = dc.mes) AND (cc.periodo = dc.periodo)
			  INNER JOIN facturacion.conceptos c ON (dc.codconcepto = c.codconcepto)
			  WHERE cc.codsuc=".$codsuc." AND cc.codciclo=".$ciclo." 
			  AND cc.periodo='".$periodo."' AND cc.catetar='".$rowT["catetar"]."'
			  AND cc.tipo=0 AND cc.tipoestructura=0
			  ORDER BY c.ordenrecibo";
			  $ConsultaCo =$conexion->query($Sql);
			foreach($ConsultaCo->fetchAll() as $rowCo)
			{
				$objreporte->SetFont('Arial','',6);
				$objreporte->SetX($x +30);
				$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
				$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
					 c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0 AND c.tipoestructura=0
					AND cp.codconcepto='".$rowCo[0]."'
					";
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
				//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
				
			}
		}
		
		

		
	

	}
	*/
	$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
				where  c.periodo ='".$periodo."'  and 
				c.codsuc=".$codsuc." AND c.nrofacturacion<>0
				and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0".$sinFonavi;
	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();
	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(110, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(70, $h,'TOTAL FACTURACION DEL MES ',0,0,'l');
	$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	$TotalFacMes=$rowMo[0];
	////////////SALDO
	/*$objreporte->AddPage("P");

	$sqlC  = "select cat.codcategoriatar,cat.descripcion 
	from facturacion.cabctacorriente as cab ";
	$sqlC .= "inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc and ";
	$sqlC .= "cab.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "where cab.periodo=:periodo and cab.codsuc=:codsuc and cab.codciclo=:codciclo
	AND cab.tipo=1 AND cab.tipoestructura=0 ";
	$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
	
	$consultaC = $conexion->prepare($sqlC);
	$consultaC->execute(array(":periodo"=>$periodo,
							  ":codsuc"=>$codsuc,
							  ":codciclo"=>$ciclo));
	$itemsC = $consultaC->fetchALl();
	$h=4;
	$objreporte->SetTextColor(0,0,0);
	$objreporte->SetFont('Arial','B',7);
	$objreporte->SetX($x-10);
	$objreporte->Cell(70, $h,'- FACTURACION DEUDA ACUMULADA',0,1,'l');
	$objreporte->SetFont('Arial','',6);
		
	$objreporte->Ln(2);
	$objreporte->SetX($x);
	$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
	$objreporte->Cell(40, $h,"CONCEPTOS",1,0,'L');
	$objreporte->Cell(20, $h,"IMPORTE",1,0,'C');
	$objreporte->Cell(20, $h,"USUARIOS",1,1,'C');
	foreach($itemsC as $rowC)
	{
		
		$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
				and d.codconcepto=cp.codconcepto)
				inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
				c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
				inner join facturacion.categoriatarifaria as cat on 
				(tar.codcategoriatar=cat.codcategoriatar) 
				where  c.periodo ='".$periodo."'  and 
				 c.codsuc=".$codsuc." 
				and c.codciclo=".$ciclo." AND c.tipo=1  AND c.tipoestructura=0
				AND tar.codcategoriatar='".$rowC[0]."'";
		$ConsultaMo =$conexion->query($Sql);
		$rowMo		= $ConsultaMo->fetch();
		$objreporte->SetTextColor(255,0,0);
		$objreporte->SetFont('Arial','B',6);
		$objreporte->SetX($x);
		$objreporte->Cell(70, $h,strtoupper($rowC[1]),0,0,'l');
		$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
		$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	
		
		
		$sqlT  = "select catetar,substring(nomtar,1,3) as nomtar 
		from facturacion.tarifas  
		where codemp=1 AND codsuc=:codsuc  ";
		$sqlT .= " and codcategoriatar=:codcategoriatar  ";
		$sqlT .= "order by catetar";
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			
			/*$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=0  AND c.tipoestructura=0	";
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();

			$objreporte->SetFont('Arial','B',6);
			$objreporte->SetX($x+5);
			$objreporte->Cell(65, $h,strtoupper($rowT[1]),0,0,'l');
			$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
			$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
			*/
			/*$objreporte->SetTextColor(0,0,0);
			$Sql="SELECT DISTINCT(c.codconcepto), c.descripcion,c.ordenrecibo
				FROM facturacion.cabctacorriente cc
			  INNER JOIN facturacion.detctacorriente dc ON (cc.codemp = dc.codemp)
			  AND (cc.codsuc = dc.codsuc)
			  AND (cc.codciclo = dc.codciclo)
			  AND (cc.nrofacturacion = dc.nrofacturacion)
			  AND (cc.nroinscripcion = dc.nroinscripcion)
			  AND (cc.anio = dc.anio)
			  AND (cc.mes = dc.mes) AND (cc.periodo = dc.periodo)
			  INNER JOIN facturacion.conceptos c ON (dc.codconcepto = c.codconcepto)
			  WHERE cc.codsuc=".$codsuc." AND cc.codciclo=".$ciclo." 
			  AND cc.periodo='".$periodo."' AND cc.catetar='".$rowT["catetar"]."'
			  AND cc.tipo=1 AND cc.tipoestructura=0 ORDER BY c.ordenrecibo";
			  $ConsultaCo =$conexion->query($Sql);
			foreach($ConsultaCo->fetchAll() as $rowCo)
			{
				$objreporte->SetFont('Arial','',6);
				$objreporte->SetX($x +30);
				$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
				$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					where  c.periodo ='".$periodo."' AND c.catetar='".$rowT["catetar"]."' and 
					c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=1  AND c.tipoestructura=0
					AND cp.codconcepto='".$rowCo[0]."'
					";
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
				//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
				
			}
		}
		
		

		
	

	}
	*/
	$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
				where  c.periodo ='".$periodo."'  and 
				 c.codsuc=".$codsuc." AND c.nrofacturacion<>0
				and c.codciclo=".$ciclo." AND c.tipo=1 AND c.tipoestructura=0 ".$sinFonavi;
	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();
	//SUMAR COBRO DEL PERIODO Q PASARANO DE ESE MES, A LA DEUDA
	//$periodo =$anio.$mes;
	
	$Dias=date ('t', mktime (0,0,0, $mes, 1, $anio));
	$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,
			count(DISTINCT(c.nroinscripcion)) as nrousaurio 
	from facturacion.detctacorriente as d
	inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
	and d.codciclo=c.codciclo and 
	d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
	d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo AND c.tipoestructura=d.tipoestructura )
	inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
	where  c.periodo ='".$periodo."'  and 
	 c.codsuc=".$codsuc." AND c.nrofacturacion<>0
	and c.codciclo=".$ciclo." AND c.tipo=2 AND c.tipoestructura=3 AND c.fechareg > '".$Dias."/".$mes."/".$anio."'".$sinFonavi;
	$ConsultaSum =$conexion->query($Sql);
	$rowSu	= $ConsultaSum->fetch();
	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(110, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(70, $h,'TOTAL FACTURACION DEUDA ACUMULADA ',0,0,'l');
	$objreporte->Cell(20, $h,number_format($rowMo[0]+$rowSu[0],2),0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	$TotalSaldo=$rowMo[0]+$rowSu[0];
	////////////COBRADO
	$Cond=" /*c.periodo='".$periodo."' and */";
	if($Desde!="") $Cond=" c.fechareg between '".$Desde."' and '".$Hasta."'  and ";
		/*$objreporte->AddPage("P");
		

		$sqlC  = "select cat.codcategoriatar,cat.descripcion 
		from facturacion.cabctacorriente as c ";
		$sqlC .= "inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and c.codsuc=tar.codsuc and ";
		$sqlC .= "c.catetar=tar.catetar) ";
		$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
		$sqlC .= "where ".$Cond." c.codsuc=".$codsuc." and 
		c.codciclo=".$ciclo."
		AND c.tipo=2 AND c.tipoestructura=3 ";
		$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
		
		$consultaC = $conexion->query($sqlC);
		$itemsC = $consultaC->fetchALl();
		$h=4;
		$objreporte->SetTextColor(0,0,0);
		$objreporte->SetFont('Arial','B',7);
		$objreporte->SetX($x-10);
		$objreporte->Cell(70, $h,'- COBRADO (-)',0,1,'l');
		$objreporte->SetFont('Arial','',6);
			
		$objreporte->Ln(2);
		$objreporte->SetX($x);
		$objreporte->Cell(30, $h,"CATEGORIAS",1,0,'L');
		$objreporte->Cell(40, $h,"CONCEPTOS",1,0,'L');
		$objreporte->Cell(20, $h,"IMPORTE",1,0,'C');
		$objreporte->Cell(20, $h,"USUARIOS",1,1,'C');
		foreach($itemsC as $rowC)
		{
			
			$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
					and d.codciclo=c.codciclo and 
					d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
					d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
					and d.codconcepto=cp.codconcepto)
					inner join facturacion.tarifas as tar on(c.codemp=tar.codemp and 
					c.codsuc=tar.codsuc and c.catetar=tar.catetar) 
					inner join facturacion.categoriatarifaria as cat on 
					(tar.codcategoriatar=cat.codcategoriatar) 
					where  ".$Cond."  
					 c.codsuc=".$codsuc." 
					and c.codciclo=".$ciclo." AND c.tipo=2  AND c.tipoestructura=3
					AND tar.codcategoriatar='".$rowC[0]."'";
			$ConsultaMo =$conexion->query($Sql);
			$rowMo		= $ConsultaMo->fetch();
			$objreporte->SetTextColor(255,0,0);
			$objreporte->SetFont('Arial','B',6);
			$objreporte->SetX($x);
			$objreporte->Cell(70, $h,strtoupper($rowC[1]),0,0,'l');
			$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
			$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
		
			
			
			$sqlT  = "select catetar,substring(nomtar,1,3) as nomtar 
			from facturacion.tarifas  
			where codemp=1 AND codsuc=:codsuc  ";
			$sqlT .= " and codcategoriatar=:codcategoriatar  ";
			$sqlT .= "order by catetar";
			$consultaT = $conexion->prepare($sqlT);
			$consultaT->execute(array(":codcategoriatar"=>$rowC[0],":codsuc"=>$codsuc));
			$itemsT = $consultaT->fetchAll();
			foreach($itemsT as $rowT)
			{			
				
				/*$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
						from facturacion.detctacorriente as d
						inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
						and d.codciclo=c.codciclo and 
						d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
						d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
						inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
						and d.codconcepto=cp.codconcepto)
						where  ".$Cond." c.catetar='".$rowT["catetar"]."' and 
						c.codsuc=".$codsuc." 
						and c.codciclo=".$ciclo." AND c.tipo=2  AND c.tipoestructura=3	";
				$ConsultaMo =$conexion->query($Sql);
				$rowMo		= $ConsultaMo->fetch();

				$objreporte->SetFont('Arial','B',6);
				$objreporte->SetX($x+5);
				$objreporte->Cell(65, $h,strtoupper($rowT[1]),0,0,'l');
				$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
				$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
				*/
				/*$objreporte->SetTextColor(0,0,0);
				$Sql="SELECT DISTINCT(co.codconcepto), co.descripcion,co.ordenrecibo
					FROM facturacion.cabctacorriente c
				  INNER JOIN facturacion.detctacorriente dc ON (c.codemp = dc.codemp)
				  AND (c.codsuc = dc.codsuc)
				  AND (c.codciclo = dc.codciclo)
				  AND (c.nrofacturacion = dc.nrofacturacion)
				  AND (c.nroinscripcion = dc.nroinscripcion)
				  AND (c.anio = dc.anio)
				  AND (c.mes = dc.mes) AND (c.periodo = dc.periodo)
				  INNER JOIN facturacion.conceptos co ON (dc.codconcepto = co.codconcepto)
				  WHERE c.codsuc=".$codsuc." AND c.codciclo=".$ciclo." 
				  AND ".$Cond." c.catetar='".$rowT["catetar"]."'
				  AND c.tipo=2 AND c.tipoestructura=3 ORDER BY co.ordenrecibo";
				  $ConsultaCo =$conexion->query($Sql);
				foreach($ConsultaCo->fetchAll() as $rowCo)
				{
					$objreporte->SetFont('Arial','',6);
					$objreporte->SetX($x +30);
					$objreporte->Cell(40, $h,strtoupper($rowCo[1]),0,0,'L');
					$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(*) as nrousaurio 
						from facturacion.detctacorriente as d
						inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
						and d.codciclo=c.codciclo and 
						d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
						d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
						inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc 
						and d.codconcepto=cp.codconcepto)
						where  ".$Cond." c.catetar='".$rowT["catetar"]."' and 
						c.codsuc=".$codsuc." 
						and c.codciclo=".$ciclo." AND c.tipo=2  AND c.tipoestructura=3
						AND cp.codconcepto='".$rowCo[0]."'
						";
					$ConsultaMo =$conexion->query($Sql);
					$rowMo		= $ConsultaMo->fetch();
					$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,1,'R');
					//$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
					
				}
			}
			
			

			
		

		}
		*/
	$Sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,count(DISTINCT(c.nroinscripcion)) as nrousaurio 
				from facturacion.detctacorriente as d
				inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc 
				and d.codciclo=c.codciclo and 
				d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
				d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo AND c.tipoestructura=d.tipoestructura )
				inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
				where  ".$Cond."
				 c.codsuc=".$codsuc." AND c.nrofacturacion<>0
				and c.codciclo=".$ciclo." AND c.tipo=2 AND c.tipoestructura=3 ".$sinFonavi;
	$ConsultaMo =$conexion->query($Sql);
	$rowMo		= $ConsultaMo->fetch();


	$objreporte->SetFont('Arial','B',6);
	$objreporte->SetX($x);
	$objreporte->Cell(110, '0.01','',1,1,'l');
	$objreporte->SetX($x);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(70, $h,'TOTAL COBRADO ',0,0,'l');
	$objreporte->Cell(20, $h,number_format($rowMo[0],2),0,0,'R');
	$objreporte->Cell(20, $h,$rowMo[1],0,1,'R');
	$TotalCobrado=$rowMo[0];
	//////////////////////////////////RESUMEN
	$objreporte->AddPage("P");
	$objreporte->SetX($x-10);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(70, $h,'RESUMEN',0,1,'l');
	$objreporte->SetTextColor(0,0,0);

	$Total=$TotalFacMes + $TotalSaldo - $TotalCobrado;
	$objreporte->SetX($x+10);
	$objreporte->Cell(40,$h,'TOTAL FACTURACION DEL MES',0,0,'R');
	$objreporte->Cell(20,$h,":",0,0,'C');
	$objreporte->Cell(20,$h,number_format($TotalFacMes,2),0,1,'R');
	
	$objreporte->SetX($x+10);
	$objreporte->Cell(40,$h,'TOTAL FACTURACION DEUDA ACUMULADA ',0,0,'R');
	$objreporte->Cell(20,$h,":",0,0,'C');
	$objreporte->Cell(20,$h,number_format($TotalSaldo,2),0,1,'R');

	$objreporte->SetX($x+10);
	$objreporte->Cell(40,$h,'TOTAL COBRADO',0,0,'R');
	$objreporte->Cell(20,$h,":",0,0,'C');
	$objreporte->Cell(20,$h,'(-)'.number_format($TotalCobrado,2),0,1,'R');

	$objreporte->SetX($x+70);
	$objreporte->Cell(20,'0.01',"",1,1,'R');
	$objreporte->SetX($x+10);
	$objreporte->SetTextColor(255,0,0);
	$objreporte->Cell(40,$h,"TOTALES",0,0,'R');
	$objreporte->Cell(20,$h,":",0,0,'C');
	$objreporte->Cell(20,$h,number_format($Total,2),0,1,'R');
	$objreporte->Output();	
	
?>