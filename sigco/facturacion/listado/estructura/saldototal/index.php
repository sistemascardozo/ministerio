<?php
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");

	$TituloVentana = "ESTRUCTURA TARIFARIA";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objMantenimiento 	= new clsDrop();
	
	$codsuc = $_SESSION['IdSucursal'];
	
	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
	
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="../../usuariosfacturados/js_usuarios.js" language="JavaScript"></script>
<script>
function ver(z){}
jQuery(function($)
	{ 
		$( "#DivTipos" ).buttonset();

		var dates = $( "#FechaDesde, #FechaHasta" ).datepicker({
			//defaultDate: "+1w",
			buttonText: 'Selecione Fecha de Busqueda',
			showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			//showOn: 'both',
			showButtonPanel: true,
			numberOfMonths: 2,
			/*minDate: '01/<?=$IdPeriodo?>/<?=$Anio?>', 
			maxDate:  '<?=$Dias?>/<?=$IdPeriodo?>/<?=$Anio?>', */
			onSelect: function( selectedDate ) {
				var option = this.id == "FechaDesde" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});

	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc 	= <?=$codsuc?>
	
	function ValidarForm(Op)
	{
		var ciclos  = $("#ciclo").val()
		
		if(ciclos==0)
		{
			alert("Seleccione el Ciclo")
			return false
		}
		if($("#anio").val()==0)
		{
			alert('Seleccione el Anio a Consultar')
			return false
		}
		if($("#mes").val()==0)
		{
			alert('Seleccione el Mes a Consultar')
			return false
		}
		
		var Fechas=''
		if($('#ChckFechaReg').attr("checked")!="checked")
		{
			var Desde = $("#FechaDesde").val()
			var Hasta = $("#FechaHasta").val()
			if (Trim(Desde)=='')
			{
				
				Msj('#FechaDesde','Seleccione la Fecha Inicial',1000,'above','',false)
				return false;
				
			}
			if (Trim(Hasta)=='')
			{
				
				Msj('#FechaHasta','Seleccione la Fecha Final',1000,'above','',false)
				return false;
				
			}
			Fechas = '&Desde='+Desde+'&Hasta='+Hasta
		}

		if(document.getElementById("rabresumen").checked==true)
		{
			url = "imprimir.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		if(document.getElementById("rabdetallado").checked==true)
		{
			url = "imprimir_detallado.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		if(document.getElementById("rabdetalladosaldo").checked==true)
		{
			url = "imprimir_detalladosaldo.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		if(document.getElementById("rabdetalladocobrado").checked==true)
		{
			url = "imprimir_cobrado.php?ciclo="+ciclos+"&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&codsuc="+codsuc;
		}
		url +=Fechas
		AbrirPopupImpresion(url,800,600)
		
		return false
	}	
	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}

</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
        <td colspan="2" align="center">
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
				    <td align="right">&nbsp;</td>
				    <td align="center">&nbsp;</td>
				    <td>&nbsp;</td>
			      </tr>
				  <tr>
					<td width="100">Sucursal</td>
				    <td width="30" align="center">:</td>
				    <td>
				      <input type="text" name="sucursal" id="sucursal1" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
				    </td>
				    <td width="80" align="right">Ciclo</td>
				    <td width="30" align="center">:</td>
				    <td>
                    	<? $objMantenimiento->drop_ciclos($codsuc,0,"onchange='cargar_anio(this.value,1);'"); ?>
                    </td>
				  </tr>
				  <tr>
				    <td>A&ntilde;o</td>
				    <td align="center">:</td>
				    <td colspan="4">
                    	<div id="div_anio">
                        	<? $objMantenimiento->drop_anio($codsuc,0); ?>
                        </div>
                    </td>
			      </tr>
				  <tr>
				    <td>Mes</td>
				    <td align="center">:</td>
				    <td colspan="4">
                    	<div id="div_meses">
                        	<? $objMantenimiento->drop_mes($codsuc,0,0); ?>
                        </div>
                    </td>
			      </tr>	
			      <tr>
				    <td>Fecha Cobranza</td>
				    <td align="center">:</td>
				    <td colspan="4">
                      <input type="text" class="inputtext" id="FechaDesde" maxlength="10" value="<?=$Fecha1?>" style="width:80px;" />
				-
				<input type="text" class="inputtext" id="FechaHasta" maxlength="10" value="<?=$Fecha?>" style="width:80px;" />
			          <input type="checkbox"  id="ChckFechaReg" />
		              Todo el Periodo
                    </td>
			      </tr>
			      <tr>
       <td colspan="2">&nbsp;</td>
	</tr>
			      <tr>
        			<td colspan="6" align="center">
        				<div id="DivTipos" style="display:inline">
        					
	                           <input type="radio" name="rabresumen" id="rabresumen" value="radio" checked="checked" />
	                            <label for="rabresumen">Resumen</label>&nbsp;
	                            <input type="radio" name="rabresumen" id="rabdetallado" value="radio2" />
	                            <label for="rabdetallado">Detallado Mes</label>&nbsp;
	                            <input type="radio" name="rabresumen" id="rabdetalladosaldo" value="radio3" />
	                            <label for="rabdetalladosaldo">Detallado Saldo</label>&nbsp;
	                            <input type="radio" name="rabresumen" id="rabdetalladocobrado" value="radio4" />
	                            <label for="rabdetalladocobrado">Detallado Cobrado</label>
	                         
						</div>	
        			</td>
        		</tr>			  
				 <tr><td colspan="6">&nbsp;</td></tr>
        		<tr><td colspan="6" align="center"> <input type="button" onclick="return ValidarForm();" value="Exportar" id=""></td></tr>		  
				<tr><td colspan="6">&nbsp;</td></tr>
				</table>
		</td>
	</tr>
	 </tbody>

    </table>
 </form>
</div>
<?php CuerpoInferior(); ?>