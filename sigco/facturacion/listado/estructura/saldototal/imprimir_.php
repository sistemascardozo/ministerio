<?php 
	include("../../../../../objetos/clsReporte.php");
	
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $mes,$anio,$meses;
			
			$h=4;
			$this->SetFont('Arial','B',8);
			$this->Cell(0, $h+1,"ESTRUCTURA TARIFARIA DE LA FACTURACION - IMPORTE ",0,1,'C');
			$this->SetFont('Arial','B',8);
			if($mes==1)
				{
					$mesa=12;
					$anioa=$anio-1;
				}
			else
			{
				$mesa=$mes-1;
				$anioa=$anio;
			}
			$this->Cell(0, $h+1,$meses[$mesa]."-".$anioa." - USUARIOS ACTIVOS",0,1,'C');
			$this->Ln(2);
			
			$this->SetFont('Arial','',6);
		
			$this->Ln(1);
			$this->Cell(45, $h,"CATEGORIAS",1,0,'C');
			$this->Cell(16, $h,"TOTAL",1,0,'C');
			$this->Cell(81, $h,"FACTURACION DEL MES (S./)",1,0,'C');
			$this->Cell(18, $h,"TOTAL",'LRT',0,'C');
			$this->Cell(81, $h,"FACTURACION DEUDA ACUMULADA (S./)",1,0,'C');
			$this->Cell(18, $h,"TOTAL",'LRT',0,'C');
			$this->Cell(18, $h,"TOTAL",'TR',1,'C');
			
			$this->Cell(11.25, $h,"Catetar",'BL',0,'C');
			$this->Cell(11.25, $h,"Min.",'B',0,'C');
			$this->Cell(11.25, $h,"Exc.",'B',0,'C');
			$this->Cell(11.25, $h,"Coef.",'B',0,'C');
			
			$this->Cell(16, $h,"Usuarios",1,0,'C');
			
			$this->Cell(15, $h,"Agua",'B',0,'C');
			$this->Cell(15, $h,"Alc.",'B',0,'C');
			$this->Cell(15, $h,"Otros",'B',0,'C');
			$this->Cell(12, $h,"C. Fijo",'B',0,'C');
			$this->Cell(12, $h,"Int.",'B',0,'C');
			$this->Cell(12, $h,"Red.",'BR',0,'C');
			
			$this->Cell(18, $h,"MES",'LRB',0,'C');
			
			$this->Cell(15, $h,"Agua",'B',0,'C');
			$this->Cell(15, $h,"Alc.",'B',0,'C');
			$this->Cell(15, $h,"Otros",'B',0,'C');
			$this->Cell(12, $h,"C. Fijo",'B',0,'C');
			$this->Cell(12, $h,"Int.",'B',0,'C');
			$this->Cell(12, $h,"Red.",'BR',0,'C');
						
			$this->Cell(18, $h,"DEUDA",'LRB',0,'C');
			
			$this->Cell(18, $h,"FACT.",'BR',1,'C');
		}
		function categoriatarifaria($codigo,$descripcion)
		{
			$h=4;
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(250,0,0);
			
			$this->Cell(11.25, $h,$codigo,0,0,'C');
			$this->Cell(65, $h,strtoupper($descripcion),0,1,'l');
		}
		function Contenido($catetar,$impminimo,$impexecivo,$coeficiente,$nrousuario,$impagua,$impalc,$impotros,
						   $impcargofijo,$interes,$redondeo,$totmes,$impaguasaldo,$impalcsaldo,$impotrossaldo,
						   $impfijosaldo,$interessaldo,$redsaldo,$totsaldo)
		{
			$h=4;
			$this->SetFont('Arial','',6);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(11.25, $h,strtoupper($catetar),0,0,'C');
			$this->Cell(11.25, $h,number_format($impminimo,3),0,0,'C');
			$this->Cell(11.25, $h,number_format($impexecivo,3),0,0,'C');
			$this->Cell(11.25, $h,number_format($coeficiente,0),0,0,'C');
			
			$this->Cell(16, $h,$nrousuario,0,0,'C');
			
			$this->Cell(15, $h,number_format($impagua,2),0,0,'R');
			$this->Cell(15, $h,number_format($impalc,2),0,0,'R');
			$this->Cell(15, $h,number_format($impotros,2),0,0,'R');
			$this->Cell(12, $h,number_format($impcargofijo,2),0,0,'R');
			$this->Cell(12, $h,number_format($interes,2),0,0,'R');
			$this->Cell(12, $h,number_format($redondeo,2),0,0,'R');
			
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(255,0,0);
			
			$this->Cell(18, $h,number_format($totmes,2),0,0,'R');
			
			$this->SetFont('Arial','',6);
			$this->SetTextColor(0,0,0);
			
			$this->Cell(15, $h,number_format($impaguasaldo,2),0,0,'R');
			$this->Cell(15, $h,number_format($impalcsaldo,2),0,0,'R');
			$this->Cell(15, $h,number_format($impotrossaldo,2),0,0,'R');
			$this->Cell(12, $h,number_format($impfijosaldo,2),0,0,'R');
			$this->Cell(12, $h,number_format($interessaldo,2),0,0,'R');
			$this->Cell(12, $h,number_format($redsaldo,2),0,0,'R');
			
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(255,0,0);
			
			$this->Cell(18, $h,number_format($totsaldo,2),0,0,'R');
			
			$this->Cell(18, $h,number_format($totmes+$totsaldo,2),0,1,'R');
			
		}
		function ContenidoFoot($total_usuarios,$impagua,$impalc,$impotros,$impcargofijo,$interes,
							   $redondeo,$totmesfact,$impaguasaldo,$impalcsaldo,$impotrossaldo,$impfijosaldo,$interessaldo,
							   $redsaldo,$totmessaldo)
		{
			$h=4;
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(255,0,0);
			
			$this->Cell(45, $h,"Total Categoria =>",0,0,'R');
			$this->Cell(16, $h,$total_usuarios,'T',0,'C');
			
			$this->Cell(15, $h,number_format($impagua,2),'T',0,'R');
			$this->Cell(15, $h,number_format($impalc,2),'T',0,'R');
			$this->Cell(15, $h,number_format($impotros,2),'T',0,'R');
			$this->Cell(12, $h,number_format($impcargofijo,2),'T',0,'R');
			$this->Cell(12, $h,number_format($interes,2),'T',0,'R');
			$this->Cell(12, $h,number_format($redondeo,2),'T',0,'R');
			
			$this->Cell(18, $h,number_format($totmesfact,2),'T',0,'R');
			
			$this->Cell(15, $h,number_format($impaguasaldo,2),'T',0,'R');
			$this->Cell(15, $h,number_format($impalcsaldo,2),'T',0,'R');
			$this->Cell(15, $h,number_format($impotrossaldo,2),'T',0,'R');
			$this->Cell(12, $h,number_format($impfijosaldo,2),'T',0,'R');
			$this->Cell(12, $h,number_format($interessaldo,2),'T',0,'R');
			$this->Cell(12, $h,number_format($redsaldo,2),'T',0,'R');
			
			$this->Cell(18, $h,number_format($totmessaldo,2),'T',0,'R');
			
			$this->Cell(18, $h,number_format($totmesfact+$totmessaldo,2),'T',1,'R');
			
		}
		function ContenidoFootxxx($total_usuarios,$totagua,$totalc,$tototros,$totcargo,$totinteres,$totredondeo,$totalmes,
								  $totaguasaldo,$totalcsaldo,$tototrossaldo,$totfijosaldo,$totinteressaldo,$totredsaldo,$totmessaldo)
		{
			$h=4;
			$this->SetFont('Arial','B',6);
			$this->SetTextColor(255,0,0);
			
			$this->Cell(45, $h,"Total =>",0,0,'R');
			$this->Cell(16, $h,$total_usuarios,'T',0,'C');
			
			$this->Cell(15, $h,number_format($totagua,2),'T',0,'R');
			$this->Cell(15, $h,number_format($totalc,2),'T',0,'R');
			$this->Cell(15, $h,number_format($tototros,2),'T',0,'R');
			$this->Cell(12, $h,number_format($totcargo,2),'T',0,'R');
			$this->Cell(12, $h,number_format($totinteres,2),'T',0,'R');
			$this->Cell(12, $h,number_format($totredondeo,2),'T',0,'R');
			
			$this->Cell(18, $h,number_format($totalmes,2),'T',0,'R');
			
			$this->Cell(15, $h,number_format($totaguasaldo,2),'T',0,'R');
			$this->Cell(15, $h,number_format($totalcsaldo,2),'T',0,'R');
			$this->Cell(15, $h,number_format($tototrossaldo,2),'T',0,'R');
			$this->Cell(12, $h,number_format($totfijosaldo,2),'T',0,'R');
			$this->Cell(12, $h,number_format($totinteressaldo,2),'T',0,'R');
			$this->Cell(12, $h,number_format($totredsaldo,2),'T',0,'R');
			
			$this->Cell(18, $h,number_format($totmessaldo,2),'T',0,'R');
			
			$this->Cell(18, $h,number_format($totalmes+$totmessaldo,2),'T',1,'R');
			
		}
		function importe_cta_corriente_deuda($catetar,$categoria,$codsuc,$codciclo,$periodo)
		{
			global $conexion;
			
			$agua 		= 0;
			$desague 	= 0;
			$otros		= 0;
			$cargo		= 0;
			$interes	= 0;
			$igv		= 0;
			$red		= 0;
			$total		= 0;
			
			$sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,cp.categoria
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
																   d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
																   d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo )
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
					where c.codestadoservicio = 1 AND c.catetar=:catetar and d.categoria=:categoria and	
					c.periodo=:periodo and c.codsuc=:codsuc and c.codciclo=:codciclo  AND c.tipo=1 AND c.nrodocumento<>0
					group by cp.categoria";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array(":catetar"=>$catetar,
							   		 ":categoria"=>$categoria,
							   		 ":periodo"=>$periodo,
							   	    ":codsuc"=>$codsuc,
							   		 ":codciclo"=>$codciclo));
			$items = $consulta->fetchAll();
			
			foreach($items as $row)
			{
				if($row["categoria"]==1){$agua+=$row["importe"];}
				if($row["categoria"]==2){$desague+=$row["importe"];}
				if($row["categoria"]==3){$interes+=$row["importe"];}
				if($row["categoria"]==4){$igv+=$row["importe"];}
				if($row["categoria"]==5){$cargo+=$row["importe"];}
				if($row["categoria"]==6){$otros+=$row["importe"];}
				if($row["categoria"]==7){$red+=$row["importe"];}
			}
			
			$total = $agua + $desague + $interes + $igv + $cargo + $otros + $red;
	
			return array("agua"=>$agua,"desague"=>$desague,"interes"=>$interes,"igv"=>$igv,"cargo"=>$cargo,"otros"=>$otros,"red"=>$red,"total"=>$total);
		}
		function importe_cta_corriente($catetar,$categoria,$codsuc,$periodo)
		{
			global $conexion;
			
			$agua 		= 0;
			$desague 	= 0;
			$otros		= 0;
			$cargo		= 0;
			$interes	= 0;
			$igv		= 0;
			$red		= 0;
			$total		= 0;
			
			$sql = "select sum(d.importe-(d.importerebajado+d.imppagado)) as importe,cp.categoria
					from facturacion.detctacorriente as d
					inner join facturacion.cabctacorriente as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codciclo=c.codciclo and 
																   d.nrofacturacion=c.nrofacturacion and d.nroinscripcion=c.nroinscripcion and 
																   d.anio=c.anio and c.mes=c.mes and c.periodo=d.periodo and c.tipo=d.tipo)
					inner join facturacion.conceptos as cp on(d.codemp=cp.codemp and d.codsuc=cp.codsuc and d.codconcepto=cp.codconcepto)
					where c.codestadoservicio = 1 AND c.catetar=:catetar and d.categoria=:categoria and	
					c.periodo=:periodo and c.codsuc=:codsuc and c.codciclo=:codciclo  AND c.tipo=0 AND c.nrodocumento<>0
					group by cp.categoria";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array(":catetar"=>$catetar,
							   		 ":categoria"=>$categoria,
							   		 ":periodo"=>$periodo,
							   		 ":codsuc"=>$codsuc,
							   		 ":codciclo"=>$codciclo));
			$items = $consulta->fetchAll();
			
			foreach($items as $row)
			{
				if($row["categoria"]==1){$agua+=$row["importe"];}
				if($row["categoria"]==2){$desague+=$row["importe"];}
				if($row["categoria"]==3){$interes+=$row["importe"];}
				if($row["categoria"]==4){$igv+=$row["importe"];}
				if($row["categoria"]==5){$cargo+=$row["importe"];}
				if($row["categoria"]==6){$otros+=$row["importe"];}
				if($row["categoria"]==7){$red+=$row["importe"];}
			}
			
			$total = $agua + $desague + $interes + $igv + $cargo + $otros + $red;
	
			return array("agua"=>$agua,"desague"=>$desague,"interes"=>$interes,"igv"=>$igv,"cargo"=>$cargo,"otros"=>$otros,"red"=>$red,"total"=>$total);
		}
    
    }
	
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclo		= $_GET["ciclo"];
	$codsuc		= $_GET["codsuc"];
     $periodo =$anio.$mes;    	
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("L");
	
	$totusuarios 	= 0;
	$totagua	 	= 0;
	$totalc		 	= 0;
	$tototros		= 0;
	$totcargo		= 0;
	$totinteres		= 0;
	$totredondeo	= 0;
	$totalmes		= 0;
	
	$totaguasaldo		= 0;
	$totalcsaldo		= 0;
	$tototrossaldo		= 0;
	$totfijosaldo		= 0;
	$totinteressaldo	= 0;
	$totredsaldo		= 0;
	$totmessaldoF		= 0;
	
	//$facturacion = $objreporte->datosfacturacion($codsuc,$ciclo);
	/*$consulta = $conexion->prepare("select nrofacturacion,anio,mes,lecturas,saldo,facturacion,tasainteres 
											from facturacion.periodofacturacion 
											where codemp=1 and codsuc=? and codciclo=? AND anio=? AND mes=?");
	$consulta->execute(array($codsuc,$ciclo,$anio,$mes));
				
	$facturacion = $consulta->fetch();
	$nrofacturacion = ($facturacion["nrofacturacion"]);
	*/
	$sqlC  = "select cat.codcategoriatar,cat.descripcion from facturacion.cabctacorriente as cab ";
	$sqlC .= "inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc and ";
	$sqlC .= "cab.catetar=tar.catetar) ";
	$sqlC .= "inner join facturacion.categoriatarifaria as cat on(tar.codcategoriatar=cat.codcategoriatar) ";
	$sqlC .= "where cab.periodo=:periodo and cab.codsuc=:codsuc and cab.codciclo=:codciclo ";
	$sqlC .= "group by cat.codcategoriatar,cat.descripcion order by cat.codcategoriatar";
	
	$consultaC = $conexion->prepare($sqlC);
	$consultaC->execute(array(":periodo"=>$periodo,
							  ":codsuc"=>$codsuc,
							  ":codciclo"=>$ciclo));
	$itemsC = $consultaC->fetchALl();

	foreach($itemsC as $rowC)
	{
		$objreporte->categoriatarifaria($rowC[0],$rowC[1]);
		
		$nrousuarios	= 0;
		$impagua		= 0;
		$impalc			= 0;
		$impotros		= 0;
		$impcargofijo	= 0;
		$interes		= 0;
		$redondeo		= 0;
		$totmesfact		= 0;
		
		$impaguasaldo 	= 0;
		$impalcsaldo	= 0;
		$impotrossaldo	= 0;
		$impfijosaldo	= 0;
		$redsaldo		= 0;
		$interessaldo	= 0;
		$totmessaldo	= 0;
		
		$sqlT  = "select cab.catetar,substring(tar.nomtar,1,3) as nomtar,tar.impconsmin,tar.impconsexec,";
		$sqlT .= "tar.volumenesp,count(*) as nrousaurio 
		from facturacion.cabctacorriente as cab ";
		$sqlT .= "inner join facturacion.tarifas as tar on(cab.codemp=tar.codemp and cab.codsuc=tar.codsuc ";
		$sqlT .= "and cab.catetar=tar.catetar) 
		where cab.codsuc=:codsuc and cab.codciclo=:codciclo ";
		$sqlT .= "and cab.periodo=:periodo and tar.codcategoriatar=:codcategoriatar AND cab.codestadoservicio=:codestadoservicio  ";
		$sqlT .= " group by cab.catetar,tar.nomtar,tar.impconsmin,tar.impconsexec,tar.volumenesp ";
		$sqlT .= "order by cab.catetar";
		
		$consultaT = $conexion->prepare($sqlT);
		$consultaT->execute(array(":periodo"=>$periodo,
								  ":codcategoriatar"=>$rowC[0],
								  ":codsuc"=>$codsuc,
								  ":codciclo"=>$ciclo,
								 ":codestadoservicio"=>1
								  ));
		$itemsT = $consultaT->fetchAll();
		foreach($itemsT as $rowT)
		{			
			$importe_mes 	= $objreporte->importe_cta_corriente($rowT["catetar"],0,$codsuc,$ciclo,$periodo);
			$importe_saldo = $objreporte->importe_cta_corriente_deuda($rowT["catetar"],1,$codsuc,$ciclo,$periodo);
			
			$nrousuarios  += $rowT["nrousaurio"];
			$impagua	  += $importe_mes["agua"];
			$impalc		  += $importe_mes["desague"];
			$impotros	  += $importe_mes["otros"];
			$impcargofijo += $importe_mes["cargo"];
			$interes	  += $importe_mes["interes"];
			$redondeo	  += $importe_mes["red"];
			$totmesfact	  += $importe_mes["total"];
			
			$impaguasaldo	+= $importe_saldo["agua"];
			$impalcsaldo	+= $importe_saldo["desague"];
			$impotrossaldo	+= $importe_saldo["otros"];
			$impfijosaldo	+= $importe_saldo["cargo"];
			$interessaldo	+= $importe_saldo["interes"];
			$redsaldo		+= $importe_saldo["red"];
			$totmessaldo	+= $importe_saldo["total"];
			
			$objreporte->Contenido($rowT["nomtar"],$rowT["impconsmin"],$rowT["impconsexec"],$rowT["volumenesp"],$rowT["nrousaurio"],
							       $importe_mes["agua"],$importe_mes["desague"],$importe_mes["otros"],$importe_mes["cargo"],
								   $importe_mes["interes"],$importe_mes["red"],$importe_mes["total"],$importe_saldo["agua"],
								   $importe_saldo["desague"],$importe_saldo["otros"],$importe_saldo["cargo"],$importe_saldo["interes"],
								   $importe_saldo["red"],$importe_saldo["total"]);
		}
		
		$objreporte->ContenidoFoot($nrousuarios,$impagua,$impalc,$impotros,$impcargofijo,$interes,
							   	   $redondeo,$totmesfact,$impaguasaldo,$impalcsaldo,$impotrossaldo,$impfijosaldo,$interessaldo,
							       $redsaldo,$totmessaldo);
		
		$totusuarios += $nrousuarios;
		
		$totagua 		+= $impagua;
		$totalc	 		+= $impalc;
		$tototros		+= $impotros;
		$totcargo		+= $impcargofijo;
		$totinteres		+= $interes;
		$totredondeo	+= $redondeo;
		$totalmes		+= $totmesfact;
		
		$totaguasaldo 		+= $impaguasaldo;
		$totalcsaldo		+= $impalcsaldo;
		$tototrossaldo		+= $impotrossaldo;
		$totfijosaldo		+= $impfijosaldo;
		$totinteressaldo	+= $interessaldo;
		$totredsaldo		+= $redsaldo;
		$totmessaldoF		+= $totmessaldo;

	}
	
	$objreporte->ContenidoFootxxx($totusuarios,$totagua,$totalc,$tototros,$totcargo,$totinteres,$totredondeo,$totalmes,
								  $totaguasaldo,$totalcsaldo,$tototrossaldo,$totfijosaldo,$totinteressaldo,$totredsaldo,$totmessaldoF);
	
	
	$objreporte->Output();	
	
?>