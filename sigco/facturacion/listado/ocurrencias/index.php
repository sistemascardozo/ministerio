<?php
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");

    $TituloVentana = "LISTADO DE OCURRENCIAS";
    $Activo = 1;
    CuerpoSuperior($TituloVentana);
    $codsuc = $_SESSION['IdSucursal'];

    $objMantenimiento = new clsDrop();

    $sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?", $codsuc);

    $Fecha = date('d/m/Y');
    
?>


<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    
    var urldir = "<?php echo $_SESSION['urldir'];?>";
    var codsuc = <?= $codsuc ?>;
    
    $(document).ready(function() { 
        
        $("#FechaDesde, #FechaHasta").datepicker(
            {
                showOn: 'button',
                direction: 'up',
                buttonImage: '../../../../images/iconos/calendar.png',
                buttonImageOnly: true,
                showOn: 'both',
                showButtonPanel: true
            }
        );
    });
    
    function buscar_usuarios()
    {
        object = "usuario"
        AbrirPopupBusqueda('../../../catastro/operaciones/actualizacion/?Op=5', 1150, 450)
    }
    function Recibir(id)
    {
        if (object == "usuario")
        {
            $("#nroinscripcion").val(id)
            cargar_datos_usuario(id)
        }

    }
    function cargar_datos_usuario(nroinscripcion)
    {
        $.ajax({
            url: '../../../../ajax/clientes.php',
            type: 'POST',
            async: true,
            data: 'codsuc=1&nroinscripcion=' + nroinscripcion,
            dataType: "json",
            success: function (datos) {


                $("#cliente").val(datos.propietario)
                Consultar()
            }
        })
    }
    function Cancelar()
    {
        location.href = '<?php echo $_SESSION['urldir'];?>admin/index.php'
    }
    function Validar()
    {
        var Fechas = ''
        if ($('#ChckFechaReg').attr("checked") != "checked")
        {
            var Desde = $("#FechaDesde").val()
            var Hasta = $("#FechaHasta").val()
            if (Trim(Desde) == '')
            {
                Msj('#FechaDesde', 'Seleccione la Fecha Inicial', 1000, 'above', '', false)
                return false;
            }
            if (Trim(Hasta) == '')
            {
                Msj('#FechaHasta', 'Seleccione la Fecha Final', 1000, 'above', '', false)
                return false;
            }
            Fechas = '&Desde=' + Desde + '&Hasta=' + Hasta
        }
        var Data = ''
        var NroInscripcion = $("#nroinscripcion").val()
        var TpOcurrencia = $("#codtipoocurrencia").val()
        if (NroInscripcion != "")
            Data += '&NroInscripcion='+NroInscripcion+'&TpOcurrencia='+TpOcurrencia;
        else
        {
            Msj('#nroinscripcion', 'Seleccione Usuario', 1000, 'above', '', false)
            return false;
        }
        
        return Data+= Fechas
    }
    
    function Consultar()
    {
        var Data = Validar()
        /*if (Data != false)
        {
            
            $.ajax({
                url: 'Consulta.php',
                type: 'POST',
                async: true,
                data: Data,
                success: function (datos)
                {
                    $('#ImgLoad').fadeOut(500, function () {
                        $("#DivResultado").empty().append(datos);
                        $('#DivConsulta').fadeIn(500, function () {
                            var theTable = $('#TbIndex')
                            $("#Filtro").keyup(function () {
                                $.uiTableFilter(theTable, this.value)
                            })
                        })
                    })
                }
            })
        }*/
    }
    
    function Pdf()
    {
        var Data = Validar()
        if (Data != false)
        {
            var ventana = window.open('Pdf.php?' + Data, 'Ocurrencias', 'resizable=yes, scrollbars=yes');
            ventana.focus();
        }
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
        <table width="780" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>

                <tr>
                    <td colspan="2" align="center">
                            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>&nbsp;</td>
                                  <td align="center">&nbsp;</td>
                                  <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="90">Sucursal</td>
                                    <td width="30" align="center">:</td>
                                    <td colspan="4">
                                        <input type="text" name="sucursal" id="sucursal1" value="<?= $sucursal["descripcion"] ?>" class="inputtext" readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tipo Ocurrencia</td>
                                    <td width="30" align="center">:</td>
                                    <td colspan="4">
                                        <select name="codtipoocurrencia" id="codtipoocurrencia" style="width:250px">
                                            <option value="0">.:: TODOS ::.</option>
                                            <?php
                                            $Sql = "SELECT * FROM public.tipoocurrencia AS tp WHERE tp.estareg = 1";
                                            $Consulta = $conexion->query($Sql);
                                            foreach ($Consulta->fetchAll() as $rows) {
                                                $Sel = "";
                                                if ($items["codtipoocurrencia"] == $rows["codtipoocurrencia"])
                                                    $Sel = 'selected="selected"';
                                                ?>
                                                <option value="<?php echo $rows["codtipoocurrencia"]; ?>" <?=$Sel ?>  ><?php echo strtoupper(utf8_decode($rows["descripcion"])); ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fecha</td>
                                    <td align="center">:</td>
                                    <td colspan="4">
                                        <input type="text" class="inputtext" id="FechaDesde" maxlength="10" value="<?=$Fecha;?>" style="width:80px;" />
                                        -
                                        <input type="text" class="inputtext" id="FechaHasta" maxlength="10" value="<?=$Fecha;?>" style="width:80px;" />
                                        <input type="checkbox"  id="ChckFechaReg" />
                                        Todo el Periodo
                                    </td>
                                </tr>
                                <tr>
                                    <td>Usuario</td>
                                    <td align="center">:</td>
                                    <td width="90">
                                        <input type="text" name="nroinscripcion" id="nroinscripcion" class="inputtext" maxlength="15" value="" onkeypress="return permite(event, 'num');" style="width:70px;" />
                                        <img src="<?php echo $_SESSION['urldir'];?>images/iconos/buscar.png" width="16" height="16" style="cursor:pointer" title="Buscar Usuario" onclick="buscar_usuarios();" />
                                    </td>
                                    <td colspan='3'>
                                        <input type="text" name="cliente" id="cliente" class="inputtext" maxlength="45" value="" readonly="readonly" style="width:400px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6" height="40" align="center">

                                        <table width="500" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" style="display: none">
                                                    <input type="button" value="Guardar" id="BtnAceptar" onclick="Consultar();" />
                                                </td>
                                                <td align="center" style="display:none"><input id="BtnExcel" type="button" value="Excel" onclick="Excel();" class="BtnIndex"/></td>
                                                <td align="center">
                                                    <input id="BtnPdf" type="button" value="Pdf" onclick="Pdf();" class="BtnIndex"/>
                                                </td>
                                                <td align="center">
                                                    <input id="BtnCancelar" type="button" value="Cancelar" onclick="Cancelar();" />
                                                </td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="6" >
                        <div id="ImgLoad" style="text-align:center;display:none">
                            <span class="icono-icon-loading"></span>
                            Cargando ...
                        </div>
                        <div id="DivConsulta" style="height:auto; overflow:auto;display:none ">
                            <fieldset>
                                <legend class="ui-state-default ui-corner-all" >Resultados de la Consulta</legend>
                                <div style="height:auto; overflow:auto;" align="center" id="DivResultado">
                                    <table class="ui-widget" border="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
                                        <thead class="ui-widget-header" style="font-size:10px">
                                            <tr>
                                                <th colspan="8">Desde el <?= ($Fecha) ?> Hasta el <?= ($Fecha) ?>  </th>
                                            </tr>
                                            <tr>
                                                <th width="50" scope="col">N&deg;</th>
                                                <th width="50" scope="col">N&deg; Prestamo</th>
                                                <th width="100" scope="col">Fecha</th>
                                                <th width="500" scope="col">Beneficiario</th>
                                                <th width="100" scope="col">Fecha Cosecha</th>
                                                <th width="100" scope="col">Monto</th>
                                                <th width="100" scope="col">Estado</th>
                                                <th width="100" scope="col">&nbsp;</th>

                                            </tr>
                                        </thead>
                                        <tbody style="font-size:12px">
                                        </tbody>
                                        <tfoot class="ui-widget-header" style="font-size:10px">
                                            <tr>
                                                <td colspan="8" align="right" >&nbsp;</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </fieldset>


                    </td>
                </tr>
          </tbody>

        </table>
    </form>
</div>
<script>
//$('#ImgLoad').fadeOut(500,function(){$('#DivConsulta').fadeIn(500)});
    $("#BtnAceptar").attr('value', 'Consultar')
    $("#BtnAceptar").css('background-image', 'url(<?php echo $_SESSION['urldir'];?>css/images/ver.png)')
</script>
<?php CuerpoInferior(); ?>