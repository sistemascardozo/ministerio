<?php
    include("../../../../objetos/clsReporte.php");

    class clsEstructura extends clsReporte {

        function cabecera() {
            global $NroInscripcion, $codsuc, $objreporte, $conexion, $Dim;

            $h = 4;
            $this->SetFont('Arial', 'B', 12);
            $this->Cell(0, $h + 1, "LISTADO DE OCURRENCIAS", 0, 1, 'C');

            $Sql = "select clie.nroinscripcion," . $objreporte->getCodCatastral("clie.") . ", 
                            clie.propietario,td.abreviado,clie.nrodocumento,
                            tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
                            es.descripcion,tar.nomtar,clie.codantiguo,ts.descripcion as tiposervicio,tipofacturacion,nromed,consumo
                            from catastro.clientes as clie 
                            inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
                            inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
                            inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
                            inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
                            inner join public.tiposervicio as ts on(ts.codtiposervicio=clie.codtiposervicio)
                            inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
                            WHERE clie.codemp=1 AND clie.codsuc=" . $codsuc . " AND clie.nroinscripcion=" . $NroInscripcion . "";
            $Consulta = $conexion->query($Sql);
            $row = $Consulta->fetch();
            define('Cubico', chr(179));

            switch ($row['tipofacturacion']) {
                case 0: $TipoFacturacion = 'CONSUMO LEIDO ( Lect. Ultima ' . $row['consumo'] . 'm' . Cubico . ')';
                    break;
                case 1: $TipoFacturacion = 'CONSUMO PROMEDIADO (' . $row['consumo'] . 'm' . Cubico . ')';
                    break;
                case 2: $TipoFacturacion = 'CONSUMO ASIGNADO (' . $row['consumo'] . 'm' . Cubico . ')';
                    break;
            }

            /* $Medidor='No';
              if(trim($row['nromed'])!="")
              $Medidor='Si ( N° '.$row['nromed'].')'; */
            $this->SetLeftMargin(30);
            $this->Ln(5);

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("N° de Inscripcion"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h, $NroInscripcion, 0, 0, 'L');

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Código Catastral"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, $row[1], 0, 1, 'L');
            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Código Antiguo"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h, $row['codantiguo'], 0, 0, 'L');

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode($row['abreviado']), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, $row['nrodocumento'], 0, 1, 'L');
            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Cliente"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h, $row['propietario'], 0, 0, 'L');

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, 'Tipo Servicio', 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, $row['tiposervicio'], 0, 1, 'L');

            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Dirección"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, strtoupper($row['direccion']), 0, 1, 'L');

            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Estado"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h, $row['descripcion'], 0, 0, 'L');

            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, 'Categoria', 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(30, $h, $row['nomtar'], 0, 1, 'L');
            /* ------------- */
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(30, $h, utf8_decode("Tipo Facturación"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->SetFont('Arial', '', 8);
            $this->Cell(70, $h, $TipoFacturacion, 0, 0, 'L');

            /* $this->SetFont('Arial','B',8);
              $this->Cell(30,$h,'Medidor',0,0,'L');
              $this->Cell(5,$h,":",0,0,'L');
              $this->SetFont('Arial','',8);
              $this->Cell(30,$h,utf8_decode($Medidor),0,1,'L'); */

            $this->Ln(8);
            $this->SetX(10);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell($Dim[1], 5, utf8_decode('N°'), 1, 0, 'C', false);
            $this->Cell($Dim[2], 5, utf8_decode('Fecha'), 1, 0, 'C', false);
            $this->Cell($Dim[3], 5, utf8_decode('Tipo Ocurrencia'), 1, 0, 'C', false);
            $this->Cell($Dim[4], 5, utf8_decode('Nro. Medidor'), 1, 0, 'C', false);
            $this->Cell($Dim[5], 5, utf8_decode('Marca'), 1, 0, 'C', false);
            $this->Cell($Dim[6], 5, utf8_decode('Observaciones'), 1, 1, 'C', false);
        }

        function Detalle() {
            
            global $NroInscripcion, $codsuc, $TpOcurrencia,$Desde,$Hasta,$objreporte, 
                    $conexion, $Dim, $meses;
					
            $Condicion1 = " WHERE oc.nroinscripcion =".$NroInscripcion."";
            $Orden1 = " ORDER BY oc.fechamov ASC ";
            if($TpOcurrencia != 0)
            {
                $Condicion1.=" AND oc.codtipoocurrencia= ".$TpOcurrencia;
            }
            
            if($Desde != '' && $Hasta!='')
            {
                $Desde= $objreporte->CodFecha($Desde);
                $Hasta= $objreporte->CodFecha($Hasta);
                $Condicion1.=" AND oc.fechamov BETWEEN CAST ('".$Desde."' AS DATE) AND CAST ( '".$Hasta."' AS DATE ) ";
            }
            
            $Sql = "SELECT
                oc.codocurrencia,
                oc.fechamov,
                tp.descripcion,
                cl.nromed,
                oc.marcamed,
                oc.observacion

                FROM
                facturacion.ocurrencia AS oc
                INNER JOIN catastro.clientes AS cl ON (cl.nroinscripcion = oc.nroinscripcion AND cl.codcliente = oc.codcliente AND cl.codemp = oc.codemp AND cl.codsuc = oc.codsuc)
                INNER JOIN public.tipoocurrencia AS tp ON tp.codtipoocurrencia = oc.codtipoocurrencia " . $Condicion1 . $Orden1;


            $Consulta = $conexion->query($Sql);
            $c = 0;
            $Deuda = 0;
            $this->SetFillColor(255, 255, 255); //Color de Fondo
            $this->SetTextColor(0);
            $this->SetLeftMargin(10);
            $this->SetFont('Arial', '', 8);
            foreach ($Consulta->fetchAll() as $row) {
                $c++;

                $this->SetX(10);
                $this->Cell($Dim[1], 5, $c, 1, 0, 'C', true);
                $this->Cell($Dim[2], 5, $objreporte->DecFecha($row['fechamov']), 1, 0, 'C', true);
                $this->Cell($Dim[3], 5, $row['descripcion'], 1, 0, 'L', true);
                $this->Cell($Dim[4], 5, $row['nromed'], 1, 0, 'C', true);
                $this->Cell($Dim[5], 5, strtoupper($row['marcamed']), 1, 0, 'C', true);
                $this->Cell($Dim[6], 5, utf8_decode($row['observacion']), 1, 1, 'L', true);
            }
            $this->Ln(2);
            
        }

    }

    define('Cubico', chr(179));

    $codsuc = $_SESSION["IdSucursal"];
    $NroInscripcion = $_GET["NroInscripcion"];
    $TpOcurrencia= $_GET['TpOcurrencia'];
    $Desde= $_GET['Desde'];
    $Hasta= $_GET['Hasta'];
    $Dim = array('1' => 10, '2' => 17, '3' => 40, '4' => 22, '5' => 30, '6' => 72, '7' => 20, '8' => 20, '9' => 20);
    $x = 20;
    $h = 4;
    $objreporte = new clsEstructura();
    $objreporte->AliasNbPages();
    $objreporte->AddPage("P");
    $objreporte->Detalle("P");

    $objreporte->Output();
?>