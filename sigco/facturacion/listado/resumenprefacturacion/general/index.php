<?php
	include("../../../../../include/main.php");
	include("../../../../../include/claseindex.php");

	$TituloVentana = "CRITICA DE PRE-FACTURACION";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	$sucursal = $objDrop->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_resumenfacturacion.js" language="JavaScript"></script>
<script>
jQuery(function($)
	{ 
		
		$( "#DivTipos" ).buttonset();
		$("#consumo_ini").val(0);
		$("#consumo_fin").val(0);

	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc = <?=$codsuc?>;

	function ValidarForm(Op){
		var rutas    = ""
		var sectores = ""

		if($("#ciclo").val()==0)
		{
			alert('Seleccione el Ciclo')
			return false
		}
		if($("#anio").val()==0)
		{
			alert('Seleccione el Anio a Consultar')
			return false
		}
		if($("#mes").val()==0)
		{
			alert('Seleccione el Mes a Consultar')
			return false
		}

		var texto = $("#mes option:selected").html();

		if(document.getElementById("rabresumen").checked==true)
		{
			url="imprimir_resumen.php";
		}
		if(document.getElementById("rabperiodo").checked==true)
		{
			url="imprimir_periodo.php";
		}
		// if(document.getElementById("rabexcel").checked==true)
		// {
		// 	url="imprimir_excel.php";
		// }


		url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&valor=1"
		AbrirPopupImpresion(url,800,600);
		return false;
	}

	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function cargar_rutas_lecturas(obj,cond)
	{
		$.ajax({
			 url:'../../../../ajax/rutas_lecturas_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+obj+'&codsuc=<?=$codsuc?>&condicion='+cond,
			 success:function(datos){
				$("#div_rutaslecturas").html(datos)
				$("#chkrutas").attr("checked",true)
				$("#todosrutas").val(1)
			 }
		}) 
	}
	function validar_rangoconsumo(obj){
		if(obj.checked)
		{
			$("#div_rangoconsumo").show();
		}else{
			$("#div_rangoconsumo").hide().find('input').val('');
			
		}
	}
	function validar_sectores(obj,idx)
	{
		$("#rutaslecturas").attr("disabled",true)
		$("#chkrutas").attr("checked",true)
		$("#todosrutas").val(1)
		
		if(obj.checked)
		{
			$("#"+idx).val(1)
			$("#sector").attr("disabled",true)
		}else{
			$("#"+idx).val(0)
			$("#sector").attr("disabled",false)
		}
	}
	function validar_rutas(obj,idx)
	{
		if(obj.checked)
		{
			$("#"+idx).val(1)
			$("#rutaslecturas").attr("disabled",true)
		}else{
			$("#"+idx).val(0)
			$("#rutaslecturas").attr("disabled",false)
		}
	}
</script>
<div align="center">
	<table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
		<tr>
            <td colspan="2">&nbsp;</td>
		</tr>
        <tr>
            <td colspan="2" align="center">
				<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
					<table width="95%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="90">Sucursal</td>
							<td width="30" align="center">:</td>
							<td>
							  <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
							</td>
							<td width="80" align="right">Ciclo</td>
							<td width="30" align="center">:</td>
							<td>
								<? $objDrop->drop_ciclos($codsuc,0,"onchange='cargar_anio(this.value,1);'"); ?>
							</td>
						</tr>
						<tr>
						    <td>A&ntilde;o</td>
						    <td align="center">:</td>
						    <td>
			                	<div id="div_anio">
			                    	<? $objDrop->drop_anio($codsuc, 0); ?>
			                    </div>
			                </td>
						    <td align="right">&nbsp;</td>
						    <td align="center">&nbsp;</td>
						    <td>&nbsp;</td>
						</tr>
						<tr>
							<td>Mes</td>
							<td align="center">:</td>
							<td>
								<div id="div_meses">
							    	<? $objDrop->drop_mes($codsuc,0,0); ?>
							    </div>
							</td>
							<td align="right">&nbsp;</td>
							<td align="center">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td align="center">&nbsp;</td>
						  <td>&nbsp;</td>
						  <td align="right">&nbsp;</td>
						  <td align="center">&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
						 <tr>
   					<td colspan="6" style="padding:4px;"  align="center">
						<div id="DivTipos" style="display:inline">
	                       	<input checked="checked" type="radio" name="rabresumen" id="rabresumen" value="radio" />
	                       	<label for="rabresumen">Resumen</label>
	                        <input type="radio" name="rabresumen" id="rabperiodo" value="radio4" />
	                        <label for="rabperiodo">Detallado</label> 
						</div>	
					</td>
  					</tr>
		     		<tr>
            			<td colspan='6' align="center"> 
            				<div id="DivTipos" style="display:inline"></div>	
					 		<input type="button" onclick="return ValidarForm();" value="Generar" id="">
					 	</td>
                		<td colspan="6"></td>
                	</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
						<tr>
							<td>&nbsp;</td>
							<td align="center">&nbsp;</td>
							<td colspan="2">&nbsp;</td>
							<td align="center">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
							    <input type="hidden" name="orden" id="orden" value="1" />
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
</div>
<div id="mostrar_datos"></div>
<script>
	$("#sector").attr("disabled",true)
</script>
<?php CuerpoInferior(); ?>