<?php
	include("../../../../objetos/clsReporte.php");
	
    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{
		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo;
			// global $fechaapertura
			
			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"RESUMEN DE FACTURACION",0,1,'C');
			$this->Ln(3);
			
			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			$this->Cell(40, $h,strtoupper("Mes de Referencia"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');			
			
			$h = 10;
			$this->SetFont('Arial','B',10);
			$this->Cell($Dim[1],$h,utf8_decode('DESCRIPCION DEL CONCEPTO'),1,0,'C',false);
			$this->Cell($Dim[5],$h,utf8_decode('IMPORTE'),1,1,'C',false);			
			$this->Ln(5);
		}

		public function contenido($concepto,$valor,$res = 0){
			global $Dim;
			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','B',8);

			switch ($res) {
				case 0:
					// $this->Ln(2);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,0,'R',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					
					break;
				case 1:
					$this->Ln(1);
					$this->SetTextColor(146,14,14); //Color de texto
					$this->Cell($Dim[3],$h,utf8_decode($concepto),0,1,'L',true);
					break;
				case 2:
					$this->SetTextColor(0);
					$this->Cell($Dim[3],$h,utf8_decode("____________________________________________________________________________________________________________"),0,1,'L',true);
					$this->Ln(5);
					$this->SetFont('Arial','B',6);
					$this->Cell($Dim[1],$h,utf8_decode("HORA INICIAL        " . $concepto),0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("HORA FINAL          " . $valor),0,1,'C',true);
					break;
				case 3:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("__________________"),0,1,'R',true);
					
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
				case 4:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("_________________________"),0,1,'R',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
			}

		}
	}

    $Dim = array('1'=>90,'2'=>60,'3'=>190,'4'=>20,'5'=>80);
	
	$codemp    = 1;
	$anio      = $_GET["anio"];
	$mes       = $_GET["mes"];
	$codsuc    = $_GET["codsuc"];
	$ciclo     = $_GET["ciclo"];
	$texto     = $_GET["mestexto"];
	$valor     = $_GET["valor"];
	$idusuario = $_SESSION['id_user'];
	$periodo   = $_GET["anio"].($_GET['mes']-1);
	
	$objReporte = new clsLecturas("L");

	$sqlLect = "SELECT c.descripcion AS concepto,
				det.codconcepto AS codconcepto, 
				c.codtipoconcepto AS codtipoconcepto, 
				SUM(det.importe - (importeacta + importerebajado)) as importe,
				det.categoria AS categoria, 
				det.estadofacturacion AS estado 
				FROM facturacion.detctacorriente det
				INNER JOIN facturacion.conceptos c ON (det.codemp = c.codemp AND det.codsuc = c.codsuc AND det.codconcepto = c.codconcepto)
				WHERE det.codemp = ? AND det.codsuc = ? AND det.codciclo = ? AND det.periodo = ?
				GROUP BY c.descripcion, c.codtipoconcepto, det.codconcepto, det.categoria, det.estadofacturacion 
				ORDER BY det.codconcepto ASC";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp, $codsuc, $ciclo, $periodo));
	$items = $consultaL->fetchAll();
	
	$contador = 0;
	$objReporte->AliasNbPages();
	$objReporte->AddPage('P');
	
	$ap       = 0;
	$des      = 0;
	$id       = 0;
	$igv      = 0;
	$ar       = 0;
	$cotsup   = 0;
	$newarray = array();


	if (count($items) > 0) :

		if ($valor == 0) :

			foreach($items as $row):

				if ($row['categoria'] == 0):
					$ar++;

					$sql    = "SELECT c.codconceptoresumen as id , co.tipofacturacion as tipo
						FROM facturacion.conceptoresumenreporte c
						INNER JOIN facturacion.conceptoresumen co ON (c.codconceptoresumen = co.codconceptoresumen)
						WHERE c.codconcepto = ?";
					$consulta = $conexion->prepare($sql);
					$consulta->execute(array($row['codconcepto']));
					$itm    = $consulta->fetch();

					if (!empty($itm) ) :

						if($newarray[$itm['id']] != 8):

							if(array_key_exists($itm['id'], $newarray)):
								$newarray[$itm['id']] +=  $row['importe'];
							else:
								$newarray[$itm['id']] = $row['importe'];
							endif;

							switch ($itm['id']):
								case 1:
									if(array_key_exists(5, $newarray)):
										$newarray[5] +=  $row['importe'];
									else:
										$newarray[5] = $row['importe'];
									endif;
									break;
								case 2:
									if(array_key_exists(5, $newarray)):
										$newarray[5] +=  $row['importe'];
									else:
										$newarray[5] = $row['importe'];
									endif;
									break;
								case 3:
									if(array_key_exists(5, $newarray)):
										$newarray[5] +=  $row['importe'];
									else:
										$newarray[5] = $row['importe'];
									endif;
									break;
								case 4:
									if(array_key_exists(5, $newarray)):
										$newarray[5] +=  $row['importe'];
									else:
										$newarray[5] = $row['importe'];
									endif;
								break;
							endswitch;

						endif;

					endif;	
								
				else:

					$sql    = "SELECT c.codconceptoresumen as id , co.tipofacturacion as tipo
						FROM facturacion.conceptoresumenreporte c
						INNER JOIN facturacion.conceptoresumen co ON (c.codconceptoresumen = co.codconceptoresumen)
						WHERE c.codconcepto = ?";
					$consulta = $conexion->prepare($sql);
					$consulta->execute(array($row['codconcepto']));
					$itm    = $consulta->fetch();

					if (!empty($itm) ) :

						if(array_key_exists(8, $newarray)):
							$newarray[8] +=  $row['importe'];
						else:
							$newarray[8] = $row['importe'];
						endif;
					endif;	

				endif;

			endforeach;

			// Inserccion de la tabla detalle
			foreach ($newarray as $m => $value):

					$instModificaciones  = "INSERT INTO facturacion.detresumenfacturacion(codemp,codsuc,nrofacturacion,anio,mes,fechareg,codconceptoresumen,importe,hora_inicial,hora_final,codciclo,codusu,estareg)
					VALUES(:codemp,:codsuc,:nrofacturacion,:anio,:mes,:fechareg,:codconceptoresumen,:importe,:hora_inicial,:hora_final,:codciclo,:codusu,:estareg)";
					$result = $conexion->prepare($instModificaciones);
					$result->execute(array(
											":codemp"             => $codemp, 
											":codsuc"             => $codsuc, 
											":nrofacturacion"     => 240,
											":anio"               => $anio,
											":mes"                => $mes,
											":fechareg"           => date('Y-m-d'),
											":codconceptoresumen" => $m,
											":importe"            => $value,
											":hora_inicial"       => date("H:i:s"),
											":hora_final"         => date("H:i:s"),
											":codciclo"           => $ciclo,
											":codusu"             => $idusuario,
											":estareg"            => 1
										)
									);
			endforeach;

			// INSERCCION DE SALDO MESES ANTERIORES

			$sqlLect = "SELECT c.descripcion AS concepto,
				det.codconcepto AS codconcepto, 
				SUM(det.importe - (importeacta + importerebajado)) as importe,
				det.categoria AS categoria, 
				det.estadofacturacion AS estado 
				FROM facturacion.detctacorriente det
				INNER JOIN facturacion.conceptos c ON (det.codemp = c.codemp AND det.codsuc = c.codsuc AND det.codconcepto = c.codconcepto)
				WHERE det.categoria = 1 AND det.codemp = ? AND det.codsuc = ? AND det.codciclo = ? AND det.periodo = ?
				GROUP BY c.descripcion, det.codconcepto, det.categoria, det.estadofacturacion 
				ORDER BY det.codconcepto ASC";
			$consultaL   = $conexion->prepare($sqlLect);
			$consultaL->execute(array($codemp, $codsuc, $ciclo, $periodo));
			$montosalant = $consultaL->fetch();

			$instModificaciones  = "INSERT INTO facturacion.detresumenfacturacion(codemp,codsuc,nrofacturacion,anio,mes,fechareg,codconceptoresumen,importe,hora_inicial,hora_final,codciclo,codusu,estareg)
					VALUES(:codemp,:codsuc,:nrofacturacion,:anio,:mes,:fechareg,:codconceptoresumen,:importe,:hora_inicial,:hora_final,:codciclo,:codusu,:estareg)";
					$result = $conexion->prepare($instModificaciones);
					$result->execute(array(
											":codemp"             => $codemp, 
											":codsuc"             => $codsuc, 
											":nrofacturacion"     => 240,
											":anio"               => $anio,
											":mes"                => $mes,
											":fechareg"           => date('Y-m-d'),
											":codconceptoresumen" => 9,
											":importe"            => $montosalant['importe'],
											":hora_inicial"       => date("H:i:s"),
											":hora_final"         => date("H:i:s"),
											":codciclo"           => $ciclo,
											":codusu"             => $idusuario,
											":estareg"            => 1
										)
									);

			$sqlLect = "SELECT 
						c.descripcion as concepto, 
						det.importe as importe, 
						det.hora_inicial as inicial, 
						c.codconceptoresumen as codconcepto,
						det.hora_final as final 
						FROM facturacion.detresumenfacturacion det
						INNER JOIN facturacion.conceptoresumen c ON (det.codconceptoresumen = c.codconceptoresumen)
						WHERE det.codemp = ? AND det.codsuc = ? 
						AND det.anio = ? AND  det.mes = ? AND  det.codciclo = ?
						AND det.codconceptoresumen <> 8 AND det.codconceptoresumen <> 9 
						AND det.codconceptoresumen <> 10 AND det.codconceptoresumen <> 11 
						AND det.codconceptoresumen <> 12
						ORDER BY c.codconceptoresumen";
			$consultaL = $conexion->prepare($sqlLect);
			$consultaL->execute(array($codemp, $codsuc, $anio, $mes, $ciclo));
			$items = $consultaL->fetchAll();
			foreach ($items as $key) :

				if ($key['codconcepto'] == 5):
					$objReporte->contenido($key['concepto'],$key['importe'],4);
					continue;
				endif;
				$objReporte->contenido($key['concepto'],$key['importe']);

			endforeach;

			$sqlLect = "SELECT 	sum(det.importe) as total FROM facturacion.detresumenfacturacion det
				WHERE det.codemp = ? AND det.codsuc = ? AND det.anio = ? AND  det.mes = ? AND  det.codciclo = ? AND (det.codconceptoresumen = 5 OR det.codconceptoresumen = 7 OR det.codconceptoresumen = 6)";
			$consultaL = $conexion->prepare($sqlLect);
			$consultaL->execute(array($codemp, $codsuc, $anio, $mes, $ciclo));
			$total = $consultaL->fetch();

			if (!empty($total)) :
				$objReporte->contenido("TOTAL ",$total['total'],3);
			endif;

			$sqlLect = "SELECT 
						c.descripcion as concepto, 
						c.codconceptoresumen as codconcepto, 
						det.importe as importe, 
						det.hora_inicial as inicial, 
						det.hora_final as final 
						FROM facturacion.detresumenfacturacion det
						INNER JOIN facturacion.conceptoresumen c ON (det.codconceptoresumen = c.codconceptoresumen)
						WHERE det.codemp = ? AND det.codsuc = ? AND det.anio = ? AND  det.mes = ? AND  
						det.codciclo = ? AND (det.codconceptoresumen = 8 OR det.codconceptoresumen = 9 
						OR det.codconceptoresumen = 10 OR det.codconceptoresumen = 11 
						OR det.codconceptoresumen = 12)  
						ORDER BY c.codconceptoresumen";
			$consultaL = $conexion->prepare($sqlLect);
			$consultaL->execute(array($codemp, $codsuc, $anio, $mes, $ciclo));
			$items = $consultaL->fetchAll();

			foreach ($items as $key) :
				$objReporte->contenido($key['concepto'],$key['importe']);
			endforeach;	
			
			$objReporte->contenido($key['inicial'],$key['final'],2);
		else:
			$sqlLect = "SELECT 
						c.descripcion as concepto, 
						c.codconceptoresumen as codconcepto, 
						det.importe as importe, 
						det.hora_inicial as inicial, 
						det.hora_final as final 
						FROM facturacion.detresumenfacturacion det
						INNER JOIN facturacion.conceptoresumen c ON (det.codconceptoresumen = c.codconceptoresumen)
						WHERE det.codemp = ? AND det.codsuc = ? AND det.anio = ? AND  det.mes = ? AND  det.codciclo = ? AND det.codconceptoresumen <> 8 AND det.codconceptoresumen <> 9 AND det.codconceptoresumen <> 10 AND det.codconceptoresumen <> 11 AND det.codconceptoresumen <> 12  
						ORDER BY c.codconceptoresumen";
			$consultaL = $conexion->prepare($sqlLect);
			$consultaL->execute(array($codemp, $codsuc, $anio, $mes, $ciclo));
			$items = $consultaL->fetchAll();

			foreach ($items as $key) :
				if ($key['codconcepto'] == 5):
					$objReporte->contenido($key['concepto'],$key['importe'],4);
					continue;
				endif;
				$objReporte->contenido($key['concepto'],$key['importe']);
			endforeach;	
			
			$sqlLect = "SELECT 	sum(det.importe) as total FROM facturacion.detresumenfacturacion det
				WHERE det.codemp = ? AND det.codsuc = ? AND det.anio = ? AND  det.mes = ? AND  det.codciclo = ? AND (det.codconceptoresumen = 5 OR det.codconceptoresumen = 7 OR det.codconceptoresumen = 6)";
			$consultaL = $conexion->prepare($sqlLect);
			$consultaL->execute(array($codemp, $codsuc, $anio, $mes, $ciclo));
			$total = $consultaL->fetch();

			if (!empty($total)) :
				$objReporte->contenido("TOTAL ",$total['total'],3);
			endif;

			$sqlLect = "SELECT 
						c.descripcion as concepto, 
						c.codconceptoresumen as codconcepto, 
						det.importe as importe, 
						det.hora_inicial as inicial, 
						det.hora_final as final 
						FROM facturacion.detresumenfacturacion det
						INNER JOIN facturacion.conceptoresumen c ON (det.codconceptoresumen = c.codconceptoresumen)
						WHERE det.codemp = ? AND det.codsuc = ? AND det.anio = ? AND  det.mes = ? AND  
						det.codciclo = ? AND (det.codconceptoresumen = 8 OR det.codconceptoresumen = 9 
						OR det.codconceptoresumen = 10 OR det.codconceptoresumen = 11 
						OR det.codconceptoresumen = 12)  
						ORDER BY c.codconceptoresumen";
			$consultaL = $conexion->prepare($sqlLect);
			$consultaL->execute(array($codemp, $codsuc, $anio, $mes, $ciclo));
			$items = $consultaL->fetchAll();

			foreach ($items as $key) :
				$objReporte->contenido($key['concepto'],$key['importe']);
			endforeach;	
		endif;

	else:
		$objReporte->contenido("NO EXISTE DATOS EN EL REPORTE","",1);
	endif;

	$objReporte->Output();
	
?>