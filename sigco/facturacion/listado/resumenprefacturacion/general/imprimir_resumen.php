<?php 
	include("../../../../../objetos/clsReporte.php");
	
    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{
		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo;
			// global $fechaapertura
			
			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"RESUMEN DE PRE FACTURACION",0,1,'C');
			$this->Ln(3);
			
			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			$this->Cell(40, $h,strtoupper("Mes de Referencia"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');	
			$this->Ln(3);		
			$h = 10;
			$this->SetFont('Arial','B',10);
			$this->Cell($Dim[1],$h,utf8_decode('DESCRIPCION DEL CONCEPTO'),1,0,'C',false);
			$this->Cell($Dim[5],$h,utf8_decode('IMPORTE'),1,1,'C',false);			
			$this->Ln(5);
		}

		public function contenido($concepto, $valor, $res = 0, $suplemento = '', $cab = 0, $estado = ''){

			global $Dim;
			if ($cab == 1 AND $estado == 0): 

				

			else:
				if($cab == 2):
					$h = 10;
				endif;

			endif;

			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','',8);

			switch ($res) {

				case 0:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);					
					break;

				case 1:
					$this->Ln(2);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);
					break;
				case 2:
					$this->SetTextColor(0);
					$this->Cell($Dim[3],$h,utf8_decode("____________________________________________________________________________________________________________"),0,1,'L',true);
					$this->Ln(5);
					$this->SetFont('Arial','B',6);
					$this->Cell($Dim[1],$h,utf8_decode("HORA INICIAL        " . $concepto),0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("HORA FINAL          " . $valor),0,1,'C',true);
					break;
				case 3:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("________________________"),0,1,'R',true);
					
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
				case 4:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode("_________________________"),0,1,'R',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode($valor),0,1,'R',true);
					break;
				case 5:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					
					break;
				case 6:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,0)),0,0,'R',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					break;
				case 7:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,"",0,0,'C',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,1,'L',true);
					break;
				case 8:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,"",0,0,'R',true);
					$this->Cell($Dim[5],$h,utf8_decode("_______________________________________"),0,1,'L',true);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					//$this->Cell($Dim[6],$h,utf8_decode(number_format($suplemento,2)),0,1,'L',true);
					break;
				case 9:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h," ",0,0,'L',true);
					$this->Cell($Dim[2],$h,utf8_decode(number_format($valor,2)),0,0,'R',true);
					$this->Cell($Dim[4],$h," ",0,1,'C',true);
					break;
				case 10:
					$this->SetTextColor(0);
					$this->Cell($Dim[8],$h,utf8_decode($concepto),0,0,'C',true);
					$this->Cell($Dim[9],$h,utf8_decode($suplemento),0,0,'C',true);
					$this->Cell($Dim[10],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					break;
				case 11:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[7],$h,utf8_decode($concepto),0,0,'C',true);
					$this->Cell($Dim[10],$h,utf8_decode(number_format($valor,2)),0,1,'R',true);
					break;
				case 15:
					$this->SetFont('Arial','B',9);
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,2)),0,0,'C',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);						
					break;
				case 16:
					$this->SetTextColor(0);
					$this->Cell($Dim[1],$h,utf8_decode($concepto),0,0,'L',true);
					$this->Cell($Dim[6],$h,utf8_decode(number_format($valor,0)),0,0,'R',true);
					$this->Cell($Dim[6],$h," ",0,1,'C',true);					
					break;
			}
		}
	}

    $Dim = array('1'=>90,'2'=>60,'3'=>190,'4'=>20,'5'=>100,'6'=>50,'7'=>140,'8'=>40,'9'=>80,'10'=>50);
	
	$codemp     = 1;
	$codsuc     = $_GET["codsuc"];
	$ciclo      = $_GET["ciclo"];
	$anio       = $_GET["anio"];
	$mes        = $_GET["mes"];
	$texto      = $_GET["mestexto"];
	$valor      = $_GET["valor"];
	$idusuario  = $_SESSION['id_user'];
	$periodo    = $_GET["anio"].($_GET['mes']);
	
	$total      = 0;
	$totalconv  = 0;
	$sados      = 0;
	
	$objReporte = new clsLecturas("L");
	$contador   = 0;
	$objReporte->AliasNbPages();
	$objReporte->cabecera();
	$objReporte->AddPage('P');

	$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
	$primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);


	// Para obtener el periodo de faturacion

	$sqlLect = "select nrofacturacion
		FROM facturacion.periodofacturacion
		WHERE codemp = ? AND codsuc = ? AND codciclo = ? 
		AND anio = ? AND mes = ?";
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
	$nrofact = $consultaL->fetch();


	if(intval($mes)<10) $mes="0".$mes;

	// Para servicio de agua
	$sqlLect1 = "select sum(importe) from facturacion.detprefacturacion
				where codsuc = ?  AND nrofacturacion='{$nrofact['nrofacturacion']}'
				AND codconcepto = 1 AND codtipodeuda <> 3 --Diferente de Refinanciamiento";

	$consultaL = $conexion->prepare($sqlLect1);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$total += $items['sum'];


	$objReporte->contenido("AGUA POTABLE",$items['sum']);

	// Para servicio de desague
	$sqlLect = "select sum(importe) from facturacion.detprefacturacion
		where codsuc = ? AND nrofacturacion='{$nrofact['nrofacturacion']}'
		AND codconcepto = 2 and codtipodeuda <> 3 --Diferente de Refinanciamiento";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$total += $items['sum'];
	$objReporte->contenido("DESAGUE",$items['sum']);

	// var_dump($total);exit;

	// Para servicio de INTERESES X DEUDA
	$sqlLect = "select sum(d.importe) 
				from facturacion.detprefacturacion d
				INNER JOIN facturacion.conceptos c on (c.codemp=d.codemp AND c.codsuc=d.codsuc and c.codconcepto=d.codconcepto)
				where d.codsuc = ? AND
				((nrocredito = 0 and nrorefinanciamiento = 0) OR c.codconcepto = 9 OR codtipodeuda=9) AND 
				 c.categoria=3  and d.codtipodeuda not in (3, 4, 7)
				AND d.nrofacturacion='{$nrofact['nrofacturacion']}' --Diferente de Refinanciamiento";
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$total += $items['sum'];

	// var_dump($total);exit;

	$objReporte->contenido("INTERESES POR DEUDA",$items['sum']);
	$objReporte->contenido("SUBTOTAL",$total,8,"");

	// Para servicio de IGV	
	$sqlLect = "select sum(importe) from facturacion.detprefacturacion
				where codsuc = ? AND nrofacturacion='{$nrofact['nrofacturacion']}'
				AND codconcepto = 5";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc,));
	$items = $consultaL->fetch();
	$total += $items['sum'];
	$objReporte->contenido("IGV",$items['sum']);

	// Para servicio de REDONDEO POSITIVO	
	$sqlLect = "select sum(importe) from facturacion.detprefacturacion
				where codsuc = ? AND nrofacturacion='{$nrofact['nrofacturacion']}'
				AND
				((nrocredito = 0 and nrorefinanciamiento = 0) OR codconcepto = 9 OR codtipodeuda =9)
				and codtipodeuda not in (3, 4, 7) AND
				codconcepto = 8";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$total += $items['sum'];

	$objReporte->contenido("REDONDEO POSITIVO",$items['sum']);

	// Para servicio de REDONDEO NEGATIVO	
	$sqlLect = "select sum(importe) from facturacion.detprefacturacion
				where codsuc = ? AND nrofacturacion='{$nrofact['nrofacturacion']}'
				AND ((nrocredito = 0 AND nrorefinanciamiento = 0) OR codconcepto = 9 OR codtipodeuda =9)
				AND codtipodeuda not in (3, 4, 7) AND
				codconcepto = 7";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();
	$total += $items['sum'];

	$objReporte->contenido("REDONDEO NEGATIVO",$items['sum']);
	// Para servicio de FACTURACION TOTAL DEL MES	
	
	$objReporte->contenido("TOTAL",$total,8);


	// Para servicio de MONTO A SU FAVOR	
	$sqlLect = "select sum(importe) from facturacion.detprefacturacion
				where codsuc = ? AND nrofacturacion='{$nrofact['nrofacturacion']}'
				AND codconcepto = 10005  ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc));
	$items = $consultaL->fetch();

	$objReporte->contenido("MONTO A SU FAVOR",$items['sum']);

	$sqlLect = "select count(*) from facturacion.cabprefacturacion
				where codsuc = ? and nrofacturacion = ? and nrodocumento > 0 ";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc,$nrofact['nrofacturacion']));
	$items = $consultaL->fetch();
	
	$objReporte->contenido("NRO FACTURAS",$items['count'],16);
	//VOLUMEN
	$sqlLect = "select sum(consumofact) from facturacion.cabprefacturacion
	where codsuc = ? AND nrofacturacion=? AND nrodocumento > 0";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codsuc, $nrofact['nrofacturacion']));
	$items = $consultaL->fetch();

	$objReporte->contenido("VOLUMEN FACTURADO",$items['sum'],16);

	//TIPO DE FACTURACION
	/*---ANTERIOR, 28-08-2015, POR ING. MAURO N. CARDOZO----
	$Sql="select tipofacturacionfact,count(*) from facturacion.cabprefacturacion
	where codsuc = ? AND nrofacturacion=? AND nrodocumento > 0
	group by tipofacturacionfact ORDER BY tipofacturacionfact";
	*/
	$Sql="select tipofacturacionfact,sum(consumofact) from facturacion.cabprefacturacion
	where codsuc = ? AND nrofacturacion=? AND nrodocumento > 0
	group by tipofacturacionfact ORDER BY tipofacturacionfact";
	$consultaL = $conexion->prepare($Sql);
	$consultaL->execute(array($codsuc, $nrofact['nrofacturacion']));
	foreach($consultaL->fetchAll() as $items)
	{
		switch ($items[0]) {
			case 0:$Tipo='CONSUMO LEIDO';break;
			case 1:$Tipo='CONSUMO PROMEDIADO';break;
			case 2:$Tipo='CONSUMO ASIGNADO';break;

		}
		 $objReporte->contenido($Tipo,$items[1],16);
	}
	//TIPO DE FACTURACION
	$objReporte->Output();
	
?>