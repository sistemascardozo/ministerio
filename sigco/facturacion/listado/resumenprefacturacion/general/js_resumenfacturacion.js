// JavaScript Document
var index=0;

$(document).ready(function(){

$(document).on('focusin', '.focusselect',function(event){$(this).parent().parent().addClass('ui-widget-header');});
$(document).on('focusout','.focusselect', function(event){$(this).parent().parent().removeClass('ui-widget-header');});	

$('#div_lecturas #tbdigitacion').fixedHeaderTable({ height: '300', footer: true });	
	var theTable = $('#tbdigitacion')
  	$("#Valor").keyup(function(){$.uiTableFilter( theTable, this.value)})
	$( "#DivObs" ).dialog({
		autoOpen: false,
		height:300,
		width: 500,
		modal: true,
/*			show: "blind",
		hide: "scale",*/
		buttons: {
			"Aceptar": function() {
				$("#observacion"+index).val($("#observacion").val())
				
				$( this ).dialog( "close" );
			},
			"Cancelar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {

		}
	});
})
$(function() {
	$( "#fdigitacion" ).datepicker(
	{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true
	});

	 $('form').keypress(function(e){   
		if(e == 13){
		  return false;
		}
	  });

	  $('input').keypress(function(e){
		if(e.which == 13){
		  return false;
		}
	  });
	  //$('#div_lecturas #tbdigitacion').fixedHeaderTable({ height: '300', footer: true });
	  $( "#DivPromediar" ).dialog({
			autoOpen: false,
			height: 350,
			width: 550,
			modal: true,
/*			show: "blind",
			hide: "scale",*/
			resizable:true,
			buttons: {
				"Aceptar": function() {
					
					$("#promedio"+itemupd).val($("#lectpromedio").val())
					$("#lblpromedio"+itemupd).html($("#lectpromedio").val())

					$("#consumo"+itemupd).val($("#lectpromedio").val())
					$("#lblconsumo"+itemupd).html($("#lectpromedio").val())
					$("#lectultima"+itemupd).val($("#lectpromedio").val())
					GuardarDigitacionSin($("#inscripcion"+itemupd).val(),itemupd)
					$( this ).dialog( "close" );
				},
				"Cancelar": function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {

			}
		});


});

function validar_sector(obj)
{
	$("#rutaslecturas").attr("disabled",true)
	$("#chkrutas").attr("checked",true)
 	$("#todosrutas").val(1)
 
	if(obj.checked)
	{
		$("#sector").attr("disabled",true)
	}else{
		$("#sector").attr("disabled",false)
	}
}
function validar_rutas(obj)
{
	if(obj.checked)
	{
		$("#rutaslecturas").attr("disabled",true)
	}else{
		$("#rutaslecturas").attr("disabled",false)
		
	}
}
function cargar_rutas_lecturas(codsuc,obj,cond)
{
	
	$.ajax({
		 url:'../../../../ajax/rutas_lecturas_drop.php',
		 type:'POST',
		 async:true,
		 data:'codsector='+obj+'&codsuc='+codsuc+'&condicion='+cond,
		 success:function(datos){
	
			 $("#div_rutaslecturas").html(datos)
/*			 $("#chkrutas").attr("checked",true)
			 $("#todosrutas").val(1)*/
		 }
	}) 
}
function calcular_consumo(obj,lectura,idx,catetar,estservicio,lectanterior)
{
	//if(lectura==""){lectura=0;}
	lectultima    = str_replace($("#lectultima"+idx).val(), ',', '');
	obj           = lectultima
	if(lectultima ==""){lectultima=0;}
	lectura       = str_replace($("#lecturaanterior"+idx).val(), ',', '');
	var cons      = parseFloat(lectultima) - parseFloat(lectura)
	if(isNaN(cons))
		cons=0;
	$("#lblconsumo"+idx).html(cons)
	$("#consumo"+idx).val(cons)
	
	validar_estlectura(catetar,estservicio,lectura,obj,cons,idx,lectanterior)
}

function validar_estlectura(catetar,estservicio,lectanterior,obj,cons,idx,numinscripcion)
{

	$.ajax({
		 url:'../../../../admin/validaciones/validar_estlectura.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&catetar='+catetar+'&estservicio='+estservicio+'&lectanterior='+lectanterior+
			  '&lectultima='+obj+'&consumo='+cons+"&numinscripcion="+numinscripcion,
		 success:function(datos){
			var r=datos.split("|")
			$("#estlectura"+idx).val(r[0])
			$("#lblcritica"+idx).html(r[1])
			GuardarDigitacion(numinscripcion,r[0],idx);
		 }
	}) 
}
function agregar_observacion(idx)
{
	$("#observacion").val($("#observacion"+idx).val())
	index = idx
	
	$( "#DivObs" ).dialog( "open" );
}
function GuardarDigitacionSin(nroinscripcion,idx)
{
	var url;
			
		lectultima  = $("#lectultima"+idx).val();
		consumo		= $("#consumo"+idx).val();
		obs			= $("#observacion"+idx).val();
		estlectura	= $("#estlectura"+idx).val();
		estmedidor	= $("#estmedidor"+idx).val();
		promedio	= $("#promedio"+idx).val();
		lecturaanterior	= $("#lecturaanterior"+idx).val();

		if(lectultima=='')lectultima=0
		if(lecturaanterior=='')lecturaanterior=0

		url = 'ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val();
		url = url + '&nroinscripcion='+nroinscripcion+'&fdigitacion='+$("#fechadigitacion").val()+'&lectultima='+lectultima+'&consumo='+consumo;
		url = url + '&observacion='+obs+'&estlectura='+estlectura+'&estadomedidor='+estmedidor+'&promedio='+promedio+'&lecturaanterior='+lecturaanterior;
		url = url +'&IdSucursal='+codsuc+'&id_user='+id_user;
		$.ajax({
			 url:'guardar_digitacion.php',
			 type:'POST',
			 async:true,
			 data:url,
			 success:function(datos){
				
			 }
		}) 
	
}
function GuardarDigitacionCmb(nroinscripcion,idx)
{
	var url;
					
		lectultima  = $("#lectultima"+idx).val();
		consumo		= $("#consumo"+idx).val();
		obs			= $("#observacion"+idx).val();
		estlectura	= $("#estlectura"+idx).val();
		estmedidor	= $("#estmedidor"+idx).val();
		promedio	= $("#promedio"+idx).val();
		lecturaanterior	= $("#lecturaanterior"+idx).val();
		if(lectultima=='')lectultima=0
		if(lecturaanterior=='')lecturaanterior=0
		
		url = 'ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val();
		url = url + '&nroinscripcion='+nroinscripcion+'&fdigitacion='+$("#fechadigitacion").val()+'&lectultima='+lectultima+'&consumo='+consumo;
		url = url + '&observacion='+obs+'&estlectura='+estlectura+'&estadomedidor='+estmedidor
		url = url +'&promedio='+promedio+'&lecturaanterior='+lecturaanterior;
		url = url +'&IdSucursal='+codsuc+'&id_user='+id_user;

		$.ajax({
			 url:'guardar_digitacion.php',
			 type:'POST',
			 async:true,
			 data:url,
			 success:function(datos){
				
			 }
		}) 
	
}

function GuardarDigitacion(nroinscripcion,estlectura,idx)
{

	var url;
	// if (!e) e = window.event; 
	// if(e && e.keyCode == 13)
	// {				
	lectultima      = $("#lectultima"+idx).val();
	consumo         = $("#consumo"+idx).val();
	obs             = $("#observacion"+idx).val();
	estmedidor      = $("#estmedidor"+idx).val();
	promedio        = $("#promedio"+idx).val();
	lecturaanterior = $("#lecturaanterior"+idx).val();
	if(lectultima=='')lectultima=0
	if(lecturaanterior=='')lecturaanterior=0
		
	url = 'ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val();
	url = url + '&nroinscripcion='+nroinscripcion+'&fdigitacion='+$("#fechadigitacion").val()+'&lectultima='+lectultima+'&consumo='+consumo;
	url = url + '&observacion='+obs+'&estlectura='+estlectura+'&estadomedidor='+estmedidor+'&promedio='+promedio+'&lecturaanterior='+lecturaanterior;
	url = url +'&IdSucursal='+codsuc+'&id_user='+id_user;
	$.ajax({
		 url:'guardar_digitacion.php',
		 type:'POST',
		 async:true,
		 data:url,
		 success:function(datos){
		 }
	}) 
	// }
}

function guardar()
{
	var cad = "";
	var cont=0;
	
	for(i=1;i<=$("#contador").val();i++)
	{
		lectura = $("#lectultima"+i).val()
		if(lectura!="")
		{

			cad  = cad + "inscripcion"+i+"="+$("#inscripcion"+i).val()+'&';
			cad = cad + "lectultima"+i+"="+lectura+'&';
			cad = cad + "consumo"+i+"="+$("#consumo"+i).val()+'&';
			cad = cad + "observacion"+i+"="+$("#observacion"+i).val()+'&';
			cad = cad + "estlectura"+i+"="+$("#estlectura"+i).val()+'&';
			cad = cad + "estmedidor"+i+"="+$("#estmedidor"+i).val()+'&';
			cad = cad + "promedio"+i+"="+$("#promedio"+i).val()+'&';
			cad = cad + "lecturaanterior"+i+"="+$("#lecturaanterior"+i).val()+'&';
			cont++;
		}
	}

	$.ajax({
		 url:'guardar.php',
		 type:'POST',
		 async:true,
		 data:cad+'&ciclo='+$("#ciclo").val()+'&anio='+$("#anio").val()+'&mes='+$("#mes").val()+'&contador='+cont+
		 	  '&fechadigitacion='+$("#fechadigitacion").val(),
		 success:function(datos){
		 	//alert(datos)
			//$("#div_error").html(datos)
			var r=datos.split("|")
			window.parent.OperMensaje(r[1],r[0])
			//location.href='index.php';
		 }
	}) 
}
function datos_facturacion(codciclo)
{
	$.ajax({
		 url:'../../../../../ajax/periodo_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'codciclo='+codciclo+'&codsuc='+codsuc,
		 success:function(datos){
			var r=datos.split("|")

			$("#anio").val(r[1])
			$("#mes").val(r[3])
		 }
	}) 
}

function ValidarForm(Op)
	{
		//guardar()
		var miTabla 	= document.getElementById("tbdigitacion");
		var numFilas 	= miTabla.rows.length;
		
		if(numFilas==1)
		{
			alert("No hay Ninguna Item Generada...Genere como minima una para poder continuar")
			return false;
		}
		guardar()
		//GuardarP(Op)
	}
	
	function Cancelar()
	{
		location.href='index.php'
	}
	var itemupd=0
	function promediar_lecturas(item)
	{
		itemupd=item
		verlecturas(item);
		objIdx = "promediar";
		$( "#DivPromediar" ).dialog( "open" );
	}
	function verlecturas(item)
	{
		$.ajax({
			 url:'ver_lecturas.php',
			 type:'POST',
			 async:true,
			 data:'codsuc='+codsuc+'&nroinscripcion='+$("#inscripcion"+item).val(),
			 success:function(datos){
				$("#DivPromedios").html(datos)
			 }
		}) 
		}
function cargar_anio(codciclo,condicion)
{
    $.ajax({
		 url:urldir+"ajax/anio_drop.php",
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&codciclo='+codciclo+'&condicion='+condicion,
		 success:function(datos){
			$("#div_anio").html(datos)
			$("#anio [option='0']").prop("selected",true)
		 }
    }) 

}
function cargar_mes(codciclo,suc,anio)
{
    $.ajax({
		 url:urldir+"ajax/mes_drop.php",
		 type:'POST',
		 async:true,
		 data:'codsuc='+suc+'&codciclo='+codciclo+'&anio='+anio,
		 success:function(datos){
			$("#div_meses").html(datos)
		 }
    }) 

}