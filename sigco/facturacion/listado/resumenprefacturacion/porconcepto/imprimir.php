<?php
	// ini_set("display_errors", 1);
	// error_reporting(E_ALL);
	include("../../../../objetos/clsReporte.php");
	
    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{
		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo;
			// global $fechaapertura
			
			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"RESUMEN DE FACTURACION POR CONCEPTO",0,1,'C');
			$this->Ln(3);
			
			$this->SetFont('Arial','B',8);
			$this->SetX(30);
			$this->Cell(40, $h,strtoupper("Mes de Referencia"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');			
			
			$h = 10;
			$this->SetFont('Arial','B',10);
			$this->Cell($Dim[1],$h,utf8_decode('COD. CONCEPTO'),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode('DESCRIPCION DEL CONCEPTO'),1,0,'C',false);
			$this->Cell($Dim[3],$h,utf8_decode('IMPORTE'),1,O,'C',false);			
			$this->Cell($Dim[3],$h,utf8_decode('INAFECTO'),1,1,'C',false);			
			$this->Ln(5);
		}

		public function contenido($codigo,$id,$descripcion,$importe,$res = 0){
			global $Dim;
			$h=5;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetFont('Arial','B',7);


			if ($id == 101 OR $id == 992) {
				$this->SetTextColor(0);
				$this->Cell($Dim[1],$h,utf8_decode($codigo),0,0,'C',true);
				$this->Cell($Dim[2],$h,utf8_decode($descripcion),0,0,'L',true);
				$this->Cell($Dim[3],$h,"",0,0,'R',true);
				$this->Cell($Dim[3],$h,utf8_decode(number_format($importe,2)),0,1,'R',true);
			}else{

				switch ($res) {
					case 0:
						$this->SetTextColor(0);
						$this->Cell($Dim[1],$h,utf8_decode($codigo),0,0,'C',true);
						$this->Cell($Dim[2],$h,utf8_decode($descripcion),0,0,'L',true);
						$this->Cell($Dim[3],$h,utf8_decode(number_format($importe,2)),0,0,'R',true);
						$this->Cell($Dim[3],$h," ",0,1,'R',true);					
					break;

					case 1:
						$this->SetTextColor(146,14,14); //Color de texto
						$this->Cell($Dim[10],$h,utf8_decode($id),0,1,'C',true);
					break;

					case 4:

						$this->SetFont('Arial','B',15);
						$this->SetTextColor(0); //Color de texto	
						$this->Cell($Dim[11],$h," ",0,0,'C',true);
						$this->Cell($Dim[12],$h,_______________________,0,1,'R',true);


						$this->SetFont('Arial','B',8);	
						$this->Cell($Dim[1],$h," ",0,0,'C',true);
						$this->Cell($Dim[2],$h,utf8_decode($id),0,0,'L',true);
						$this->Cell($Dim[3],$h,utf8_decode($descripcion),0,0,'R',true);
						$this->Cell($Dim[3],$h,utf8_decode($importe),0,1,'R',true);
					;break;
					case 5:

						$this->SetTextColor(0); //Color de texto
						$this->SetFont('Arial','B',8);	
						$this->Cell($Dim[1],$h,utf8_decode($codigo),0,0,'C',true);
						$this->Cell($Dim[2],$h,utf8_decode($descripcion),0,0,'L',true);
						$this->Cell($Dim[3],$h,utf8_decode(number_format($importe,2)),0,1,'R',true);
					break;

					case 6:

						$this->SetFont('Arial','B',15);
						$this->SetTextColor(0); //Color de texto
						$this->Cell($Dim[11],$h," ",0,0,'C',true);
						$this->Cell($Dim[12],$h,_______________________,0,1,'R',true);

						$this->SetFont('Arial','B',8);	
						$this->Cell($Dim[1],$h," ",0,0,'C',true);
						$this->Cell($Dim[2],$h,"",0,0,'L',true);
						$this->Cell($Dim[3],$h,utf8_decode(number_format($importe,2)),0,0,'R',true);
						$this->Cell($Dim[3],$h,"",0,1,'L',true);
					;break;

					case 7:
						$this->SetFont('Arial','B',15);
						$this->SetTextColor(0); //Color de texto
						$this->Cell($Dim[11],$h," ",0,0,'C',true);
						$this->Cell($Dim[12],$h,_______________________,0,1,'R',true);

						$this->SetFont('Arial','B',8);	
						$this->Cell($Dim[1],$h," ",0,0,'C',true);
						$this->Cell($Dim[2],$h,"",0,0,'L',true);
						$this->Cell($Dim[3],$h,utf8_decode($importe),0,0,'R',true);
						$this->Cell($Dim[3],$h,"",0,1,'L',true);
					;break;
				}
			}

		}
	}

    $Dim = array('1'=>40,'2'=>70,'3'=>40,'4'=>20,'10'=>190,'11'=>110,'12'=>80);
	
	$codemp         = 1;
	$anio           = $_GET["anio"];
	$mes            = $_GET["mes"];
	$codsuc         = $_GET["codsuc"];
	$ciclo          = $_GET["ciclo"];
	$idusuario      = $_SESSION['id_user'];
	$periodo        = $_GET["anio"].($_GET['mes']-1);
	$concepto       = $_GET['codconcepto'];
	$tipoconceptos  = $_GET['tipoconceptos'];
	$todosconceptos = $_GET['todosconceptos'];

	if ($todosconceptos == '%'):
		$sqls = "  AND det.codconcepto <> 998 AND det.codconcepto <> 7 AND det.codconcepto <> 8 OR det.codconcepto = 101 OR det.codconcepto = 992";
 	else:		
		$sqls = " AND det.codconcepto  = " . $concepto;
	endif;	

	if ($tipoconceptos == 1):
		$m = " AND c.codtipoconcepto <> 2";
	else:
		$m = " AND c.codtipoconcepto <> 1 ";	
	endif;
	
	$objReporte = new clsLecturas("L");

	 $sqlLect = "SELECT 
				c.codantiguo AS codantiguo, 
				det.codconcepto AS codconcepto, 
				c.descripcion AS concepto,
				SUM(det.importe - (det.importeacta + det.importerebajado)) AS importe
				FROM facturacion.detctacorriente det
				INNER JOIN facturacion.conceptos c ON (det.codemp = c.codemp AND det.codsuc = c.codsuc AND det.codconcepto = c.codconcepto)
				WHERE det.codemp = ? AND det.codsuc = ? AND det.codciclo = ? AND det.periodo = ? AND det.categoria = 0 " . $m . $sqls ."
				GROUP BY c.descripcion, det.codconcepto, c.codantiguo
				ORDER BY c.codantiguo ASC";

	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp, $codsuc, $ciclo, $periodo));
	$items = $consultaL->fetchAll();
	
	$inafecto = 0;
	$importe  = 0;
	$objReporte->AliasNbPages();
	$objReporte->AddPage('P');
	if (count($items) > 0) :

		foreach($items as $row):
			if ($row['codconcepto'] == 101 OR $row['codconcepto'] == 992):
				$inafecto += $row['importe'];
			else:
				$importe  += $row['importe'];
			endif;
			$objReporte->contenido($row['codantiguo'],$row['codconcepto'],$row['concepto'],$row['importe']);
		endforeach;

		// Para el total
		$objReporte->contenido("","TOTALES ==>",number_format($importe,2),number_format($inafecto,2),4);

		// Para los demas concepto de igv
		$sqlL = "SELECT 
				c.codantiguo AS codantiguo,
				det.codconcepto AS codconcepto, 
				c.descripcion AS concepto,
				SUM(det.importe - (det.importeacta + det.importerebajado)) AS importe
				FROM facturacion.detctacorriente det
				INNER JOIN facturacion.conceptos c ON (det.codemp = c.codemp AND det.codsuc = c.codsuc AND det.codconcepto = c.codconcepto)
				WHERE det.categoria = 0 AND det.codemp = ? AND det.codsuc = ? AND det.codciclo = ? AND det.periodo = ? AND det.codconcepto = 5 
				GROUP BY c.descripcion, det.codconcepto, c.codantiguo
				ORDER BY c.codantiguo ASC";

		$consulta = $conexion->prepare($sqlL);
		$consulta->execute(array($codemp, $codsuc, $ciclo, $periodo));
		$item = $consulta->fetch();

		if ($item) :
			$objReporte->contenido($item['codantiguo'],$item['codconcepto'],$item['concepto'],$item['importe'],5);		
		else:
			$objReporte->contenido("","","I.G.V",number_format(0,2),5);		
		endif;
		$objReporte->contenido("","","",$item['importe'] + $importe,6);	

		$total1 = $item['importe'] + $importe;	

		$sqlLect = "SELECT 
				c.codantiguo AS codantiguo,
				det.codconcepto AS codconcepto, 
				c.descripcion AS concepto,
				SUM(det.importe - (det.importeacta + det.importerebajado)) AS importe
				FROM facturacion.detctacorriente det
				INNER JOIN facturacion.conceptos c ON (det.codemp = c.codemp AND det.codsuc = c.codsuc AND det.codconcepto = c.codconcepto)
				WHERE det.codemp = ? AND det.codsuc = ? AND det.codciclo = ? 
				AND det.periodo = ? AND det.categoria = 0 AND
				det.codconcepto = 8 OR det.codconcepto = 7 
				GROUP BY c.descripcion, det.codconcepto, c.codantiguo
				ORDER BY c.codantiguo DESC";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute(array($codemp, $codsuc, $ciclo, $periodo));
		$items = $consultaL->fetchAll();

		foreach($items as $row):
			$objReporte->contenido($row['codantiguo'],$row['codconcepto'],$row['concepto'],$row['importe']);
			$import += $row['importe']; 
		endforeach;
			$total1 +=$import; 
			$objReporte->contenido("","","FACTURACION TOTAL DEL MES",$total1,7);

		// Para las deudas

		$sqlLect = "SELECT 
				SUM(det.importe - (det.importeacta + det.importerebajado)) AS importe
				FROM facturacion.detctacorriente det
				WHERE det.codemp = ? AND det.codsuc = ? AND det.codciclo = ? 
				AND det.periodo = ? AND det.categoria = 1";
		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute(array($codemp, $codsuc, $ciclo, $periodo));
		$items = $consultaL->fetch();
		$objReporte->contenido("","","DEUDA ANTERIOR",$items['importe']);

		// foreach($items as $row):
		// 	$objReporte->contenido($row['codantiguo'],$row['codconcepto'],$row['concepto'],$row['importe']);
		// endforeach;

	else:
		$objReporte->contenido("","NO EXISTE DATOS EN EL REPORTE","","",1);
	endif;

	$objReporte->Output();
	
?>