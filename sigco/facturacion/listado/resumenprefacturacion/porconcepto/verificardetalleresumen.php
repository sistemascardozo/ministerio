<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../config.php");

	$codemp = 1;
	$anio   = $_POST["anio"];
	$mes    = $_POST["mes"];
	$codsuc = $_POST["codsuc"];
	$ciclo  = $_POST["ciclo"];


	$sql = "SELECT count(*) as cantidad
				FROM facturacion.detresumenfacturacion det 
				WHERE det.codemp = ? AND det.codsuc = ? AND det.anio = ? AND  det.mes = ? AND  det.codciclo = ?";

    $consulta = $conexion->prepare($sql);
    $consulta->execute(array($codemp, $codsuc, $anio, $mes, $ciclo));
    $item = $consulta->fetch();
    echo json_encode($item['cantidad']);

?>