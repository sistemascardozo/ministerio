<?php
	// ini_set("display_errors",1);
	// error_reporting(E_ALL);

	include("../../../../../objetos/clsReporte.php");
	
    $clfunciones = new clsFunciones();

	class clsLecturas extends clsReporte
	{

		function Header()
		{
			global $codsuc;
			
			$empresa = $this->datos_empresa($codsuc);
			$this->SetFont('Arial','',6);
			$this->SetTextColor(0,0,0);
			$tit1=($empresa["razonsocial"]);
			$tit2=($empresa["direccion"]);
			$tit3="SUCURSAL: ".($empresa["descripcion"]);

			$this->SetX(20);
            $this->SetFont('Arial', 'B', 14);
            $tit11 = strtoupper("Reporte de Pre Facturacion por Conceptos");
            $this->Cell(277, 5, utf8_decode($tit11), 0, 1, 'C');
			
			$x 	= 23;
            $y 	= 5;
			$h	= 3;
		    $this->Image($_SESSION['path']."images/logo_empresa.jpg",6,1,17,16);
            $this->SetXY($x,$y);
			$this->SetFont('Arial','',6);
			$this->Cell(85, $h,utf8_decode($tit1),0,1,'L');
			$this->SetX($x);
			$this->Cell(85, $h,utf8_decode($tit2),0,1,'L');
			$this->SetX($x);
			$this->Cell(150, $h,utf8_decode($tit3),0,0,'L');
			
			$this->SetY(8);
			$FechaActual = date('d/m/Y');
			$Hora = date('h:i:s a'); 


			$this->Cell(0, 3,utf8_decode("Página    :   ".$this->PageNo().' de {nb}     '), 0, 1, 'R');

	 		$this->Cell(0, 3,"Fecha     : ".$FechaActual."   ", 0, 1, 'R');
			$this->Cell(0, 3,"Hora       : ".$Hora." ", 0, 1, 'R');

			$this->SetY(20);
			$this->SetX(23);
			$this->SetLineWidth(.1);
			$this->Cell(0,.1,"",1,1,'C',true);
                        
            $this->Ln(5); 
			
			$this->cabecera();     				
		}

		public function cabecera(){
			global $meses,$sectores,$rutas,$Dim,$texto,$anio,$ciclo,$textoconcepto,$concepto;
			// global $fechaapertura
			
			$h = 5;
			$this->SetY(22);
			$this->SetFont('Arial','B',8);
			$this->Cell(0,2,"",0,1,'R');
			$this->Ln(2);
			$this->SetX(20);
			$this->SetFont('Arial','B',14);
			$this->Cell(0, $h,"Concepto: " . strtoupper($textoconcepto),0,1,'C');
			$this->Ln(5);
			
			$this->SetFont('Arial','B',8);
			$this->SetX(90);
			$this->Cell(40, $h,strtoupper("Periodo:"),0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(20, $h,strtoupper($texto),0,0,'L');
			$this->Cell(15, $h,strtoupper($anio),0,0,'L');
			$this->SetFont('Arial','B',8);
			$this->Cell(30, $h,strtoupper("Ciclo"),0,0,'R');
			$this->SetFont('Arial','',8);
			$this->Cell(15, $h,strtoupper($ciclo),0,1,'L');	
			$this->Ln(3);		
			
			$h = 5;
			$this->SetFont('Arial','B',8);
			$this->Cell($Dim[1],$h,utf8_decode('N°'),1,0,'C',false);
			$this->Cell($Dim[2],$h,utf8_decode('DOCUMENTO'),1,0,'C',false);			
			$this->Cell($Dim[3],$h,utf8_decode('INSCRIPCION'),1,O,'C',false);			
			$this->Cell($Dim[4],$h,utf8_decode('CLIENTE'),1,O,'C',false);			
			$this->Cell($Dim[6],$h,utf8_decode('IMPORTE'),1,1,'C',false);			
			// $this->Ln(5);
		}

		public function contenido($indice,$documento,$incripcion,$propietario,$importe,$val=0){
			
			global $Dim;
			$h=5;
			$this->SetFont('Arial','',7);

			switch ($val) {
				case 1:
					$h=8;
					$this->SetFont('Arial','B',8);
					$hd = (($Dim[1]+$Dim[2]+$Dim[3]+$Dim[4]));
					$hd1 = (($Dim[1]+$Dim[2]+$Dim[3]+$Dim[4]+$Dim[6]));
					$this->SetLineWidth(.1);
					$this->Cell($hd1,.1,"",1,1,'C',true);
					$this->Cell($hd,$h,utf8_decode($indice),0,0,'R',false);			
					$this->Cell($Dim[6],$h,number_format($importe,2),0,1,'R',false);
					break;
				
				default:
					$this->Cell($Dim[1],$h,utf8_decode($indice),0,0,'C',false);			
					$this->Cell($Dim[2],$h,utf8_decode($documento),0,0,'C',false);			
					$this->Cell($Dim[3],$h,utf8_decode($incripcion),0,0,'C',false);			
					$this->Cell($Dim[4],$h,utf8_decode($propietario),0,0,'L',false);			
					$this->Cell($Dim[6],$h,number_format($importe,2),0,1,'R',false);
					break;
			}
		}

		function Footer(){

			$this->SetY(-10);
			$this->SetFont('Arial','I',6);
			$this->SetTextColor(0);
			$this->SetX(23);
			$this->SetLineWidth(.1);
			$this->Cell(0,.1,"",1,1,'C',true);
			$this->Ln(1);
			$this->SetX(23);
			$this->Cell(0,4,utf8_decode('Sistema Integrado de Información Comercial - SIINCO - Versión WEB'),0,0,'L');
			$this->Cell(0,4,'Pag. '.$this->PageNo().' de {nb}',0,0,'R');
		}

	}

	$Dim = array('1'=>15,'2'=>40,'3'=>20,'4'=>75,'5'=>60,'6'=>25,'7'=>30);
	
	$codemp         = 1;
	$anio           = $_GET["anio"];
	$mes            = $_GET["mes"];
	$codsuc         = $_GET["codsuc"];
	$ciclo          = $_GET["ciclo"];
	$idusuario      = $_SESSION['id_user'];
	$periodo        = $_GET["anio"].$_GET['mes'];
	$concepto       = $_GET['codconcepto'];
	$textoconcepto  = $_GET['textoconcepto'];
	$tipoconceptos  = $_GET['tipoconceptos'];
	$texto          = $_GET['mesdescri'];
	$todosconceptos = $_GET['todosconceptos'];

	$ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
	$primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);

	// Para obtener el periodo de faturacion
	$sqlLect = "select nrofacturacion
		FROM facturacion.periodofacturacion
		WHERE codemp = ? AND codsuc = ? AND codciclo = ? 
		AND anio = ? AND mes = ?";
	$consultaL = $conexion->prepare($sqlLect);
	$consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
	$nrofact = $consultaL->fetch();

	if ($todosconceptos != '%') {

		if ($tipoconceptos != 0):
			$qls = " AND d.codconcepto = " . $concepto;
		else:
			$qls = " AND d.codconcepto = " . $concepto;
		endif;
	}
	else
	{
		if ($tipoconceptos != 0):
			$qls = " AND d.codconcepto = " . $concepto;
		else:
			$qls = " AND d.codconcepto = " . $concepto;
		endif;
	}

	
	$objReporte = new clsLecturas("L");

	$sqlLect = "(SELECT co.codconcepto as codconcepto,
				co.descripcion 
				FROM  cobranza.cabprepagos c
				INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
				AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
				AND (c.nroinscripcion = d.nroinscripcion)
				INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
				AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
				WHERE c.codsuc = {$codsuc} AND c.estado<>0 AND c.estado<>2 
			  	AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
				AND d.coddocumento IN (13,14) " . $qls . "
				GROUP BY co.codconcepto, co.descripcion
			)
			UNION
			(
				SELECT 
		   		co.codconcepto as codconcepto,
				co.descripcion
				FROM
				cobranza.cabpagos c
				INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
				AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion)
				INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
				WHERE c.codsuc = {$codsuc} AND c.anulado = 0 
				AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
				AND d.coddocumento IN (13,14) " . $qls . " 
				GROUP BY co.codconcepto,co.descripcion
			)
			UNION
			(
				SELECT c.codconcepto as codconcepto,
				c.descripcion
				FROM  facturacion.detprefacturacion d
			  	INNER JOIN facturacion.conceptos c ON (d.codemp = c.codemp AND d.codsuc = c.codsuc AND d.codconcepto = c.codconcepto)
			  	WHERE d.codsuc={$codsuc} and d.nrofacturacion = {$nrofact['nrofacturacion']}
				and ((d.nrocredito = 0 and d.nrorefinanciamiento = 0) OR d.codtipodeuda=9) 
				" . $qls . "
				GROUP BY c.codconcepto,c.descripcion				
			)
			ORDER BY codconcepto";

	$consultacol  = $conexion->query($sqlLect);
	$alcas        = $consultacol->fetchAll();
	
	$SelFac       = '';
	$nroconceptos = 0;
	$SelFac1      = '';
	$SelFac2      = '';

	foreach($alcas as $row)
	{
		$nroconceptos++;
		$concepto = $row["descripcion"];
		$SelFac  .="SUM(CASE WHEN d.codconcepto=".$row["codconcepto"]." THEN (d.importe -(d.imppagado + d.importerebajado)) ELSE 0  END) AS c".$row["codconcepto"].",";
		$SelFac1 .="SUM(CASE WHEN d.codconcepto=".$row["codconcepto"]." THEN (d.importe) ELSE 0  END) AS c".$row["codconcepto"].",";
		$SelFac2 .="SUM(CASE WHEN d.codconcepto=".$row["codconcepto"]." THEN (d.importe) ELSE 0  END) AS c".$row["codconcepto"].",";
	}


	$contador = 0;
	$Sql1     = "(
					SELECT c.nroinscripcion,
					c.propietario,
					tcll.descripcioncorta || ' ' ||cll.descripcion || ' # ' || c.nrocalle as direccion,
					to_char(c.codsuc,'00')||'-'||TRIM(to_char(c.codsector,'00'))||'-'||TRIM(to_char(CAST(c.codmanzanas AS INTEGER),'000'))||'-'||TRIM(to_char(CAST(c.lote AS INTEGER),'0000')) as codcatastro ,
					(corre.serie||'-'||cab.nrodocumento) as documento,
					c.codantiguo,
					c.nroinscripcion,
					".$SelFac." SUM(d.importe -(d.imppagado + d.importerebajado)) AS c0
					FROM  catastro.clientes c
					INNER JOIN facturacion.detprefacturacion d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nroinscripcion = d.nroinscripcion)
					INNER JOIN public.calles cll ON (c.codemp = cll.codemp)
					AND (c.codsuc = cll.codsuc)  AND (c.codzona = cll.codzona)
					AND (c.codcalle = cll.codcalle)
					INNER JOIN public.tiposcalle tcll ON (cll.codtipocalle = tcll.codtipocalle)
					INNER JOIN facturacion.cabprefacturacion cab ON (d.codemp = cab.codemp AND d.codsuc = cab.codsuc AND d.codciclo = cab.codciclo AND d.nrofacturacion = cab.nrofacturacion AND d.nroinscripcion = cab.nroinscripcion)
					INNER JOIN reglasnegocio.documentos doc ON (cab.codsuc = doc.codsuc AND cab.coddocumento = doc.coddocumento)
					INNER JOIN reglasnegocio.correlativos corre ON (corre.codemp = cab.codemp AND corre.codsuc = cab.codsuc AND corre.coddocumento = cab.coddocumento)
					WHERE d.codsuc={$codsuc} AND d.nrofacturacion = {$nrofact['nrofacturacion']}
					" . $qls . "
					group by c.nroinscripcion,c.propietario,
					tcll.descripcioncorta || ' ' ||cll.descripcion || ' # ' || c.nrocalle,
					to_char(c.codsuc,'00')||'-'||TRIM(to_char(c.codsector,'00'))||'-'||TRIM(to_char(CAST(c.codmanzanas AS INTEGER),'000'))||'-'||TRIM(to_char(CAST(c.lote AS INTEGER),'0000'))  , 
				  	c.codantiguo,cab.nrodocumento,corre.serie
				)
				UNION
				(
					SELECT 
					c.nroinscripcion,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.propietario  END as propietario
					/*c.propietario as propietario*/,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.direccion  END as direccion
					/*c.direccion as direccion*/,
					'' as codcatastro,
					(d.serie||'-'||d.nrodocumento) as documento,
					to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')) as codantiguo,
					c.nroprepago,
					".$SelFac1." SUM(d.importe) AS c0 
					FROM  cobranza.cabprepagos c
					INNER JOIN cobranza.detprepagos d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc)  AND (c.nroprepago = d.nroprepago)
					AND (c.nroinscripcion = d.nroinscripcion)
					INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
					AND (d.codsuc = co.codsuc)  AND (d.codconcepto = co.codconcepto)
					INNER JOIN reglasnegocio.documentos doc ON (d.codsuc = doc.codsuc AND d.coddocumento = doc.coddocumento)
					WHERE c.codsuc = {$codsuc} AND c.estado<>0 AND c.estado<>2 
				  	AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
					AND d.coddocumento IN (13,14) 	AND co.codconcepto <> 10005 
					" . $qls . "
					GROUP BY c.nroinscripcion, c.propietario,c.direccion,c.codsuc,c.nroprepago,
					d.serie,d.nrodocumento
					
				)
				UNION
				(
					SELECT 
			   		c.nroinscripcion,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.propietario  END as propietario
					/*c.propietario as propietario*/,
					CASE WHEN c.nroinscripcion=0 THEN 'EVENTUALES' ELSE c.direccion  END as direccion
					/*c.direccion as direccion*/,
					'' as codcatastro,
					(d.serie||'-'||d.nrodocumento) as documento,
					to_char(c.codsuc,'00')||''||TRIM(to_char(c.nroinscripcion,'000000')) as codantiguo,c.nropago,
					".$SelFac2." 
					SUM(d.importe) AS c0
					FROM
					cobranza.cabpagos c
					INNER JOIN cobranza.detpagos d ON (c.codemp = d.codemp)
					AND (c.codsuc = d.codsuc) AND (c.nropago = d.nropago) AND (c.nroinscripcion = d.nroinscripcion)
					INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto)
					INNER JOIN reglasnegocio.documentos doc ON (d.codsuc = doc.codsuc AND d.coddocumento = doc.coddocumento)
					WHERE c.codsuc = {$codsuc} AND c.anulado = 0 
					AND c.fechareg BETWEEN '{$primerdia}' AND '{$ultimodia}'
					AND d.coddocumento IN (13,14) " . $qls . " 
					GROUP BY c.nroinscripcion, c.propietario,c.direccion,c.codsuc,c.nropago,
					d.serie,d.nrodocumento
				) 
				ORDER BY codcatastro DESC";

	$consultacol = $conexion->query($Sql1);
	$alcas2      = $consultacol->fetchAll();
	$AUsuario    = array();

	foreach($alcas2 as $row):

	  	if (!array_key_exists($row['nroinscripcion'], $AUsuario)):

			$AUsuario[$row['nroinscripcion']] = array(
														"nroinscripcion" =>$row['nroinscripcion'], 
														"codcatastro"    =>$row['codcatastro'],
														"codantiguo"     =>$row['codantiguo'],
														"propietario"    =>$row['propietario'],
														"direccion"      =>$row['direccion'],
														"documento"      =>$row['documento']
													);
			foreach($alcas as $rowc):

				 $c = 'c'.$rowc['codconcepto'];
				 array_push($AUsuario[$row['nroinscripcion']],array($c=>$row[$c]));
				 
			endforeach;

		else:

			$Conceptos[$row['nroinscripcion']] = array(
														"nroinscripcion" =>$row['nroinscripcion'], 
														"codcatastro"    =>$row['codcatastro'],
														"codantiguo"     =>$row['codantiguo'],
														"propietario"    =>$row['propietario'],
														"direccion"      =>$row['direccion'],
														"documento"      =>$row['documento']
													);
			$i2=-1;
			foreach($alcas as $rowc):
				$i2++;
				$c='c'.$rowc['codconcepto'];
				$AUsuario[$row['nroinscripcion']][$i2]=array($c=>($AUsuario[$row['nroinscripcion']][$i2][$c]+$row[$c]));
			endforeach;

		endif;

	endforeach;	

	sort($AUsuario); 
	// var_dump($AUsuario);exit;

	$tConceptos = array();
	$TotalC     = 0;	
	$importe    = 0;
	$cabecera 	= 0;
	$contador 	= 0;
	$Total 	= 0;

	$objReporte->AliasNbPages();
	$objReporte->SetLeftMargin(65);
    $objReporte->SetAutoPageBreak(true,15);
	$objReporte->AddPage('H');

	for ($i=0; $i <count($AUsuario); $i++):

		$contador++;
		$row                = $AUsuario[$i];
		$Total              += $row[0]["c{$_GET['codconcepto']}"];
		$tusuario           = 0;
		$i2                 = -1;
		$row['direccion']   = (strtoupper($row["direccion"]));
		$row['propietario'] = trim((strtoupper($row["propietario"])));
		// $objReporte->contenido($contador,$row['codcatastro'],$row['codantiguo'],$row["propietario"],$row['direccion'],$row['documento'],$row[0]["c1"]);
		$objReporte->contenido($contador,$row['documento'],$row['codantiguo'],$row["propietario"],$row[0]["c{$_GET['codconcepto']}"]);

	endfor;
	$objReporte->contenido("TOTAL =========>","","","",$Total,1);

	$objReporte->Output();
	
?>