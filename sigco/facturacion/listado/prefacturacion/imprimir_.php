<?php

    include("../../../../objetos/clsReporte.php");
    // include("../../../../objetos/clsFunciones.php");
    $clfunciones = new clsFunciones();

    class clsLecturas extends clsReporte {

        function cabecera() {
            global $meses, $sectores, $rutas, $Dim;
            // global $fechaapertura

            $h = 5;
            $this->SetY(22);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(0, 2, "", 0, 1, 'R');
            $this->SetFont('Arial', 'B', 14);
            $this->Cell(0, $h, "LISTADO DE EXCESOS EN PREFACTURACION PARA VERIFICACION DE LECTURAS", 0, 1, 'C');
            $this->Ln(3);

            $this->SetFont('Arial', '', 7);

            $this->Cell(30, $h, strtoupper($sectores), 0, 0, 'L');
            $this->Cell(40, $h, strtoupper($rutas), 0, 1, 'L');

            //$this->SetWidths(array(25,15,40,65,10,23,30,45));
            $this->SetFont('Arial', '', 7);
            $this->Cell($Dim[1], $h, utf8_decode('CODIGO'), 1, 0, 'L', false);
            $this->Cell($Dim[9], $h, utf8_decode('INSCRIPCION'), 1, 0, 'C', false);
            $this->Cell($Dim[3], $h, utf8_decode('USUARIO'), 1, 0, 'C', false);
            $this->Cell($Dim[2], $h, utf8_decode('DIRECCION'), 1, 0, 'C', false);
            $this->Cell($Dim[5], $h, utf8_decode('MEDIDOR'), 1, 0, 'C', false);
            $this->Cell($Dim[4], $h, utf8_decode('CAT.'), 1, 0, 'C', false);
            $this->Cell($Dim[8], $h, utf8_decode('LEC. ANT.'), 1, 0, 'C', false);
            $this->Cell($Dim[8], $h, utf8_decode('LEC. ULT.'), 1, 0, 'C', false);
            $this->Cell($Dim[8], $h, utf8_decode('CONS.'), 1, 0, 'C', false);
            $this->Cell($Dim[9], $h, utf8_decode('IMPORTE (Agua)'), 1, 1, 'C', false);

            $this->SetFont('Arial', '', 7);
        }

        function contenido($codcatastro, $nroinscripcion, $usuario, $direccion, $nromed, $categoria, $lecant, $lecult, $consumo, $importe) {
            global $Dim;
            $this->SetFillColor(255, 255, 255); //Color de Fondo
            $this->SetTextColor(0);
            $h = 8;
            $this->Cell($Dim[1], $h, utf8_decode($codcatastro), 0, 0, 'C', true);
            $this->Cell($Dim[9], $h, utf8_decode($nroinscripcion), 0, 0, 'C', true);
            $this->Cell($Dim[3], $h, utf8_decode($usuario), 0, 0, 'L', true);
            $this->Cell($Dim[2], $h, utf8_decode($direccion), 0, 0, 'L', true);
            $this->Cell($Dim[5], $h, utf8_decode($nromed), 0, 0, 'C', true);
            $this->Cell($Dim[4], $h, utf8_decode($categoria), 0, 0, 'C', true);
            $this->Cell($Dim[8], $h, utf8_decode((int) $lecant), 0, 0, 'R', true);
            $this->Cell($Dim[8], $h, utf8_decode((int) $lecult), 0, 0, 'R', true);
            $this->Cell($Dim[8], $h, utf8_decode((int) $consumo), 0, 0, 'R', true);
            $this->Cell($Dim[9], $h, utf8_decode(number_format($importe, 2)), 0, 1, 'R', true);
        }

    }

    $Dim = array('1' => 20, '2' => 55, '3' => 75, '4' => 10, '5' => 20, '6' => 22, '7' => 15, '8' => 18, '9' => 22, '10' => 30);

    $codemp = 1;
    $anio = $_GET["anio"];
    $mes = $_GET["mes"];
    $codsuc = $_GET["codsuc"];
    $ciclo = $_GET["ciclo"];
    $ruta = $_GET["ruta"];
    $sector = $_GET["sector"];
    $orden = $_GET["orden"];
    $min = $_GET["minimo"];
    $max = $_GET["maximo"];
    $imp = $_GET["importe"];
    if ($imp=='') {
    	$imp=0;
    }
    $objReporte = new clsLecturas("L");

    if ($orden == 1)
	{
        $order = " ORDER BY cli.codsuc, cli.codsector, CAST(cli.codrutlecturas AS INTEGER ), CAST(cli.codmanzanas AS INTEGER), CAST(cli.lote AS INTEGER)  ";
    }
	else
	{
        $order = " ORDER BY cli.codsuc, cli.codsector, CAST(cli.codrutlecturas AS INTEGER ), CAST(cli.codmanzanas AS INTEGER), CAST(cli.lote AS INTEGER) ";
    }
	
    if ($_GET["sector"] == '%')
	{
        $order = " ORDER BY cli.codsector, cli.nroorden ";
	}

    $sqlLect = "SELECT
        ".$clfunciones->getCodCatastral("cli.").",
        cab.nroinscripcion AS inscripcion,
        cab.propietario AS usuario,
        upper(tipcal.descripcioncorta || ' ' || ca.descripcion || ' #' || cli.nrocalle) as direccion,
        cli.nromed AS medidor,
        tar.nomtar AS categoria,
        cab.lecturaanterior AS lecturaanterior,
        cab.lecturaultima AS lecturaultima,
        cab.consumofact AS consumo,
        de.importe AS importe
        FROM
        facturacion.cabprefacturacion cab
        INNER JOIN catastro.clientes cli ON (cli.nroinscripcion = cab.nroinscripcion AND cli.codemp = cab.codemp AND cli.codsuc = cab.codsuc)
        INNER JOIN public.calles ca ON (ca.codcalle = cli.codcalle AND ca.codemp = cli.codemp AND ca.codsuc = cli.codsuc AND ca.codzona = cli.codzona)
        INNER JOIN public.tiposcalle as tipcal ON (ca.codtipocalle=tipcal.codtipocalle)
        INNER JOIN facturacion.tarifas tar ON (tar.catetar = cab.catetar AND tar.codsuc = cab.codsuc AND tar.codemp = cab.codemp)
        INNER JOIN facturacion.detprefacturacion AS de ON cab.codemp = de.codemp AND cab.codsuc = de.codsuc AND cab.codciclo = de.codciclo AND cab.nroinscripcion = de.nroinscripcion AND cab.nrofacturacion = de.nrofacturacion
        WHERE /*cab.tipofacturacion = 1 AND*/ de.codconcepto=1 AND cab.codemp=? and cab.codsuc=? and cab.codciclo=? and cab.anio=? and cab.mes=?";

    if ($sector != "%")
        $sqlLect .= " AND cli.codsector =".$sector;
    if ($ruta != "%")
        $sqlLect .= " AND cli.codrutlecturas=".$ruta;
    if ($min != "0")
        $sqlLect .= " AND cab.consumofact >= ".$min;
    if ($max != "0")
        $sqlLect .= " AND cab.consumofact <= ".$max;
    if ($imp != "0")
        $sqlLect .= " AND de.importe >= ".$imp;
//die($sqlLect.$order);
    $consultaL = $conexion->prepare($sqlLect.$order);
    $consultaL->execute(array($codemp,
        $codsuc,
        $ciclo,
        $anio,
        $mes));
    $itemsL = $consultaL->fetchAll();

    $contador = 0;
    $objReporte->AliasNbPages();
    $objReporte->AddPage('H');
    foreach ($itemsL as $rowL) {

        $numeroinscripcion = $clfunciones->CodUsuario($codsuc, $rowL['inscripcion']);
        // var_dump($rowL["codcatastro"]);exit;
        $objReporte->contenido($rowL["codcatastro"], $numeroinscripcion, $rowL["usuario"], utf8_decode($rowL["direccion"]), $rowL["medidor"], substr($rowL["categoria"], 0, 3), $rowL["lecturaanterior"], $rowL["lecturaultima"], $rowL["consumo"], $rowL["importe"]);

        $sect = $sectores;
        $rut = $rutas;
    }
    $objReporte->Output();

?>