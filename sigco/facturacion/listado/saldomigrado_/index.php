<?php 
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "Saldo Migrado";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	$sucursal = $objDrop->setSucursales(" where codemp=1 and codsuc=?",$codsuc);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="js_volumen.js" language="JavaScript"></script>
<script>
jQuery(function($)
	{ 
		
		$( "#DivTipos" ).buttonset();
		$("#consumo_ini").val(0);
		$("#consumo_fin").val(0);

	});
	var urldir 	= "<?php echo $_SESSION['urldir'];?>" 
	var codsuc = <?=$codsuc?>;

	function ValidarForm(obj){
		var rutas    = ""
		var sectores = ""

		if($("#ciclo").val()==0)
		{
			alert('Seleccione el Ciclo')
			return false
		}
		if($("#anio").val()==0)
		{
			alert('Seleccione el Anio a Consultar')
			return false
		}
		if($("#mes").val()==0)
		{
			alert('Seleccione el Mes a Consultar')
			return false
		}

		var texto = $("#mes option:selected").html();

		if($("#tipoconceptos").val()!= 0)
		{

			if ( $("#idconceptos").val() == 0 ) {
			
				if( $("#codconcepto").val() == 0)
				{
					alert('Seleccione el Concepto')
					return false
				}else{

					todosconceptos = $("#codconcepto").val();
				}
			}
			else{

				todosconceptos = "%";
			}
		}
		else{
			todosconceptos = "%";
		}	

		// if(todosconceptos == % ){

			if(obj.id == 'rabconsultar')
			{
				$.ajax({
				 url:'consulta.php',
				 type:'POST',
				 async:true,
				 data:"anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>",
				 success:function(datos){
					$("#mostrar_datos").html(datos)
				 }
				}) 
			}
			if(obj.id == 'rabpdfmes')
			{
				url="imprimir_mes.php";
				url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>"
				AbrirPopupImpresion(url,800,600);
				return false;

			}
			if(obj.id == 'rabpdfsaldo')
			{


				url="imprimir_saldo.php";
				url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos;
				AbrirPopupImpresion(url,800,600);
				return false;

			}
		// }

		if(obj.id == 'rabpdfsaldoconcepto')
		{
			// url="imprimir_detallado_concepto.php";
			// url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos;
			// AbrirPopupImpresion(url,800,600);

			// Resumen General
			url="imprimir_detallado_concepto_resumen.php";
			url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos;
			AbrirPopupImpresion(url,800,600);


			return false;

		}

		if(obj.id == 'rabpdfmesconcepto')
		{
			// url="imprimir_det_mes_concepto.php";
			// url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos;
			// AbrirPopupImpresion(url,800,600);

			// Resumen General
			url="imprimir_det_mes_concepto_resumen.php";
			url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos;
			AbrirPopupImpresion(url,800,600);
			return false;
		}
		if(obj.id == 'rabpdftotalconcepto')
		{
			// url="imprimir_det_total_concepto.php";
			// url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos;
			// AbrirPopupImpresion(url,800,600);

			url="imprimir_det_total_concepto_excel.php";
			url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos;
			AbrirPopupImpresion(url,800,600);

			// Resumen General
			// url="imprimir_det_total_concepto_resumen.php";
			// url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos;
			// AbrirPopupImpresion(url,800,600);
			return false;
		}
		if(obj.id=='rabpdftotalconceptodet')
		{
			url="imprimir_det_total_concepto_excel_det.php";
			url += "?anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclo="+$("#ciclo").val()+"&mestexto="+texto+"&codsuc=<?=$codsuc?>&codconcepto="+todosconceptos+'&tipoentidades='+$("#tipoentidades").val();
			AbrirPopupImpresion(url,800,600);
			return false;
		}

	}

	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function cargar_rutas_lecturas(obj,cond)
	{
		$.ajax({
			 url:'../../../../ajax/rutas_lecturas_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+obj+'&codsuc=<?=$codsuc?>&condicion='+cond,
			 success:function(datos){
				$("#div_rutaslecturas").html(datos)
				$("#chkrutas").attr("checked",true)
				$("#todosrutas").val(1)
			 }
		}) 
	}
	function datos_conceptos(codtipo)
    {
        $.ajax({
            url:urldir+"ajax/concepto_drop.php",
            type:'POST',
            async:true,
            data:'codsuc=<?=$codsuc?>&codtipoconcepto='+codtipo,
            success:function(datos){

                   $("#div_concepto").html(datos)
                   $(".aparecer").css('display', 'inline-block')
                   $("#idconceptos").val(0)
                   $("#chkconceptos").prop('checked',false)
                   // $("#anio").val(anioact)
            }
        }) 

    }
    function validar_concepto(obj,idx)
	{
		if(obj.checked)
		{
			$("#"+idx).val(1)
			$("#codconcepto").attr("disabled",true)
		}else{
			$("#"+idx).val(0)
			$("#codconcepto").attr("disabled",false)
		}
	}
</script>
<div align="center">
	<table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
		<tr>
            <td colspan="2">&nbsp;</td>
		</tr>
        <tr>
            <td colspan="2" align="center">
				<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
					<table width="95%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="90">Sucursal</td>
							<td width="30" align="center">:</td>
							<td>
							  <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
							</td>
							<td colspan="2" align="right">Ciclo</td>
							<td width="30" align="center">:</td>
							<td colspan="2" width="48%">
								<? $objDrop->drop_ciclos($codsuc,0,"onchange='cargar_anio(this.value,1);'"); ?>
							</td>
						</tr>
						<tr>
						    <td>A&ntilde;o</td>
						    <td align="center">:</td>
						    <td>
			                	<div id="div_anio">
			                    	<? $objDrop->drop_anio($codsuc,0); ?>
			                    </div>
			                </td>
						    <td>&nbsp;</td>
							<td align="right">Mes</td>
							<td align="center">:</td>
							<td>
								<div id="div_meses">
							    	<? $objDrop->drop_mes($codsuc,0,0); ?>
							    </div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Tipo de Concepto</td>
							<td align="center">:</td>
							<td>
						    	<? $objDrop->drop_tipo_conceptos(1000,'onchange="datos_conceptos(this.value)"'); ?>
							</td>
							<td>&nbsp;</td>
							<td width="6%" align="right">Concepto</td>
							<td align="center">:</td>
							<td>
								<div id="div_concepto" style="display:inline-block">
							    </div>
							</td>
							<td>
 							    <input type="hidden" name="idconceptos" id="idconceptos" value="0">
								<input class="aparecer" style="display:none;" type="checkbox" name="chkconceptos" id="chkconceptos" value="checkbox" onclick="CambiarEstado(this,'idconceptos');validar_concepto(this,'idconceptos')" />
								<span class="aparecer" style="display:none;">Todas los Conceptos</span>
							</td>
						</tr>
						<tr>
	                        <td>Grupo</td>
	                        <td align="center">:</td>
	                        <td>
	                            <?php $objDrop->drop_tipo_entidades($items["codtipoentidades"],''); ?>
	                        </td>
	                    </tr>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
					 	<tr>
   							<td colspan="8" style="padding:4px;"  align="center">
								<div id="DivTipos" style="display:inline">
			                       	<input type="radio" name="rabconsultar" id="rabconsultar" value="radio" onclick="return ValidarForm(this);"  />
			                       	<label for="rabconsultar">Consultar</label>
			                        <input type="radio" name="rabpdfmes" id="rabpdfmes" value="radio4" onclick="return ValidarForm(this);"  />
			                        <label for="rabpdfmes">Detalle del Mes</label>    
			                        <input type="radio" name="rabpdfsaldo" id="rabpdfsaldo" value="radio5" onclick="return ValidarForm(this);"  />
			                        <label for="rabpdfsaldo">Detalle del Saldo</label>    
			                        <input type="radio" name="rabpdfsaldoconcepto" id="rabpdfsaldoconcepto" value="radio6" onclick="return ValidarForm(this);"  />
			                        <label for="rabpdfsaldoconcepto">Detalle del Saldo x Concepto</label> 
			                        <input type="radio" name="rabpdfmesconcepto" id="rabpdfmesconcepto" value="radio7" onclick="return ValidarForm(this);"  />
			                        <label for="rabpdfmesconcepto">Detalle del Mes x Concepto</label>    
			                        <input type="radio" name="rabpdftotalconcepto" id="rabpdftotalconcepto" value="radio8" onclick="return ValidarForm(this);"  />
			                        <label for="rabpdftotalconcepto">Resumen Concepto del Mes y Saldo</label>    
			                        <input type="radio" name="rabpdftotalconceptodet" id="rabpdftotalconceptodet" value="radio9" onclick="return ValidarForm(this);"  />
			                        <label for="rabpdftotalconceptodet">Resumen Concepto del Mes y Saldo (Detallado)</label>    
								</div>	
							</td>
  						</tr>
						<tr>
							<td>
							    <input type="hidden" name="orden" id="orden" value="1" />
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
		<tr>
			<td>
				<div id="mostrar_datos"></div>
			</td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
	    </tr>
    </table>
</div>
