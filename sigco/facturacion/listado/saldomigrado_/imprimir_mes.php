<?php
    include("../../../../objetos/clsReporte.php");
	
    $clfunciones = new clsFunciones();

    class clsLecturas extends clsReporte
    {
        function Header() {

            global $codsuc,$meses;
            global $Dim;
            global $fechaact;
            global $mes;
            
            $periodo = $this->DecFechaLiteral($fechaact);
            $nommes = $this->obtener_nombre_mes($mes);

            $nombremes = (empty($periodo['mes'])) ? strtoupper($nommes) : $periodo['mes'] ;


            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "DETALLE DE FACTURACION DEL MES";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(277,5,"INFORME DE SALDO : ". $nombremes . " - " . $periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

        function cabecera(){    
            global $Dim,$Dim1,$Dim2,$Dim3;      

            $this->Ln(5);
            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=5;
            $h1=10;

            //$this->SetWidths(array(25,15,40,65,10,23,30,45));
            // Cabecera Principal
            $this->SetFont('Arial','',10);
            $this->Cell($Dim[1],$h,utf8_decode('Datos del Usuario'),1,0,'C',true);
            $this->Cell($Dim[2],$h1,utf8_decode('Importe'),1,1,'C',true);
            
            // Subcabecera Secundaria
            $this->SetY($this->GetY()-$h);
            $this->Cell($Dim[3],$h,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[4],$h,utf8_decode('Nroinscripcion'),1,0,'C',true);
            $this->Cell($Dim[5],$h,utf8_decode('Propietario'),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Anio'),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Mes'),1,1,'C',true);
        }

        public function contenido($item, $inscripcion, $propietario, $anio, $mes, $importe, $valor = 0){

            global $Dim;

            $this->SetFillColor(18,157,176);
            $this->SetTextColor(255);
            $h=5;

            switch ($valor) {

                case 0:
                    $this->SetTextColor(0);
                    $this->Cell($Dim[3],$h,$item,1,0,'C',false);
                    $this->Cell($Dim[4],$h,utf8_decode($inscripcion),1,0,'C',false);
                    $this->Cell($Dim[5],$h,utf8_decode($propietario),1,0,'L',false);
                    $this->Cell($Dim[3],$h,utf8_decode($anio),1,0,'C',false);
                    $this->Cell($Dim[3],$h,utf8_decode($mes),1,0,'C',false);
                    $this->Cell($Dim[2],$h,number_format($importe,2),1,1,'R',false);
                    
                    break;

                case 1:
                    $this->SetTextColor(255);
                    $this->Cell($Dim[1],$h,utf8_decode($item),1,0,'C',true);
                    $this->Cell($Dim[2],$h,number_format($inscripcion,2),1,1,'R',true);
                    break;              
            }
        }
    }

    $Dim = array('1'=>230,'2'=>40,'3'=>20,'4'=>50,'5'=>120);
    
    $codemp     = 1;
    $codsuc     = $_GET["codsuc"];
    $ciclo      = $_GET["ciclo"];
    $anio       = $_GET["anio"];
    $mes        = $_GET["mes"];
    $texto      = $_GET["mestexto"];
    $valor      = $_GET["valor"];
    $idusuario  = $_SESSION['id_user'];
    $periodo    = $_GET["anio"].($_GET['mes']);
    
    $total      = 0;
    $totalconv  = 0;
    
    $objReporte = new clsLecturas("L");
    $fechaact = '01/'.$_GET['mes']."/".$_GET['anio'];

    $contador   = 0;
    $objReporte->AliasNbPages();
    // $objReporte->SetLeftMargin(10);
    // $objReporte->SetAutoPageBreak(true,5);
    $objReporte->AddPage('H');
    
    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes,$anio);
    $primerdia = $clfunciones->obtener_primer_dia_mes($mes,$anio);

    // Para obtener el periodo de faturacion

    $sqlLect = "select nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
    if(intval($mes)<10): $mesmod = "0".$mes; else: $mesmod = $mes; endif;

    if($mesmod == '09' AND $anio == '2014'):

        $sqlLect = "SELECT 
                d.nroinscripcion as nroinscripcion,
                cli.propietario as propietario, 
                per.anio as anio, 
                per.mes as mes, 
                SUM(d.importe) as importe 
                FROM facturacion.cabctacorriente c
                INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) 
                INNER JOIN catastro.clientes cli ON (cli.codemp = c.codemp AND cli.codsuc = c.codsuc AND cli.nroinscripcion = c.nroinscripcion)
                INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo)
                WHERE d.codsuc = {$codsuc} and d.periodo = '".$anio.$mesmod."'
                AND d.tipo = 0 AND d.tipoestructura = 0
                GROUP BY d.nroinscripcion, cli.propietario, per.anio, per.mes
                ORDER BY d.nroinscripcion, cli.propietario, per.anio, per.mes";

    else:
    
        $sqlLect = "SELECT 
                d.nroinscripcion as nroinscripcion,
                cli.propietario as propietario, 
                per.anio as anio, 
                per.mes as mes, 
                SUM((d.importe - d.importeacta) + d.importerebajado) as importe 
                FROM facturacion.cabctacorriente c
                INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) 
                INNER JOIN catastro.clientes cli ON (cli.codemp = c.codemp AND cli.codsuc = c.codsuc AND cli.nroinscripcion = c.nroinscripcion)
                INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo)
                WHERE d.codsuc = {$codsuc} and d.periodo = '".$anio.$mesmod."'
                AND d.codconcepto <> 101
                AND d.tipoestructura = 0 AND d.tipo = 0 AND d.codtipodeuda <> 3
                GROUP BY d.nroinscripcion, cli.propietario, per.anio, per.mes
                ORDER BY d.nroinscripcion, cli.propietario, per.anio, per.mes";
    endif;

// var_dump($sqlLect);exit;
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute();
    $items    = $consultaL->fetchAll();
    $contador = 0;
    $import   = 0;
    
    foreach ($items as $key): 
        $contador++;

        if(intval($key['mes'])<10): $mesmod = "0".$key['mes']; else: $mesmod = $key['mes']; endif;
        $numeroinscripcion = $clfunciones->CodUsuario($codsuc,$key['nroinscripcion']);

        $objReporte->contenido($contador, $numeroinscripcion, $key['propietario'], $key['anio'], $mesmod, $key['importe']);
        $import += $key['importe'];
    endforeach;

    $objReporte->contenido(strtoupper('TOTAL'),$import,'','','','',1);

    $objReporte->Output();  
    
    
?>