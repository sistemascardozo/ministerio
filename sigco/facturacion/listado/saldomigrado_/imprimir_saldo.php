<?php 
    include("../../../../objetos/clsReporte.php");
	
    $clfunciones = new clsFunciones();

    class clsLecturas extends clsReporte
    {
        function Header() {

            global $codsuc,$meses;
            global $Dim;
            global $fechaact;
            global $mes;
            
            $periodo = $this->DecFechaLiteral($fechaact);
            $nommes = $this->obtener_nombre_mes($mes);

            $nombremes = (empty($periodo['mes'])) ? strtoupper($nommes) : $periodo['mes'] ;


            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "DETALLE DE SALDOS ANTERIORES";
            $this->Cell(277, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(277,5,"INFORME DE SALDO : ". $nombremes . " - " . $periodo["anio"],0,1,'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(10);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ').'', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

        function cabecera(){    
            global $Dim, $Dim1, $Dim2, $Dim3;      

            $this->Ln(5);
            $this->SetFillColor(18, 157, 176);
            $this->SetTextColor(255);
            $h = 5;
            $h1 = 10;

            //$this->SetWidths(array(25,15,40,65,10,23,30,45));
            // Cabecera Principal
			$this->SetFont('Arial', '', 9);
            $this->Cell($Dim[1],$h,utf8_decode('Datos del Usuario'),1,0,'C',true);
            $this->Cell($Dim[2],$h1,utf8_decode('Importe'),1,1,'C',true);
            
            // Subcabecera Secundaria
            $this->SetY($this->GetY()-$h);
            $this->Cell($Dim[3],$h,utf8_decode('N°'),1,0,'C',true);
            $this->Cell($Dim[4],$h,utf8_decode('Código'),1,0,'C',true);
            $this->Cell($Dim[5],$h,utf8_decode('Propietario'),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Anio'),1,0,'C',true);
            $this->Cell($Dim[3],$h,utf8_decode('Mes'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Agua'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Desague'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Cargo F.'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Interes'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Redondeo'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('Otros'),1,0,'C',true);
            $this->Cell($Dim[6],$h,utf8_decode('IGV'),1,1,'C',true);
        }

        public function contenido($item, $inscripcion, $propietario, $anio, $mes, $importe, $valor = 0){

            global $Dim, $codsuc, $clfunciones, $conexion, $anioP, $mesP;
			
			$this->SetFont('Arial', '', 8);
			
            $this->SetFillColor(18, 157, 176);
            $this->SetTextColor(255);
			
            $h = 4;
			
			$numeroinscripcion = $clfunciones->CodUsuario($codsuc, $inscripcion);
			
			$impAgua = 0;
			$impDesa = 0;
			$impCarg = 0;
			$impInte = 0;
			$impRedo = 0;
			$impOtro = 0;
			$impIgv = 0;
			
            switch ($valor) {

                case 0:
					$Sql = "SELECT d.codconcepto, ((d.importe - d.importeacta) + d.importerebajado) AS importe ";
					$Sql .= "FROM facturacion.detctacorriente d ";
					$Sql .= " INNER JOIN catastro.clientes cli ON (cli.codemp = d.codemp AND cli.codsuc = d.codsuc AND cli.nroinscripcion = d.nroinscripcion) ";
					$Sql .= " INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo) ";
					$Sql .= "WHERE d.codsuc = ".$codsuc." ";
					$Sql .= " AND d.periodo = '".$anioP.$mesP."' ";
					$Sql .= " AND d.tipo = 1 ";
					$Sql .= " AND d.tipoestructura = 0 ";
					$Sql .= " AND d.codconcepto <> 101 ";
					$Sql .= " AND d.codtipodeuda <> 3 ";
					$Sql .= " AND d.nroinscripcion = ".$inscripcion." ";
					$Sql .= " AND per.anio = '".$anio."' ";
					$Sql .= " AND per.mes = '".$mes."' ";
					
					$consulta = $conexion->prepare($Sql);
					$consulta->execute(array());
					$Rows = $consulta->fetchAll();
					
					//$Sql = "SELECT ((d.importe - d.importeacta) + d.importerebajado) AS importe ";
//					$Sql .= "FROM facturacion.detctacorriente d ";
//					$Sql .= " INNER JOIN catastro.clientes cli ON (cli.codemp = d.codemp AND cli.codsuc = d.codsuc AND cli.nroinscripcion = d.nroinscripcion) ";
//					$Sql .= " INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo) ";
//					$Sql .= "WHERE d.codsuc = ".$codsuc." ";
//					$Sql .= " AND d.periodo = '".$anioP.$mesP."' ";
//					$Sql .= " AND d.tipo = 1 ";
//					$Sql .= " AND d.tipoestructura = 0 ";
//					$Sql .= " AND d.codconcepto <> 101 ";
//					$Sql .= " AND d.codtipodeuda <> 3 ";
//					$Sql .= " AND d.nroinscripcion = ".$inscripcion." ";
//					$Sql .= " AND d.codconcepto IN (5)";
//					
//					$consulta = $conexion->prepare($Sql);
//					$consulta->execute(array());
//					$Row = $consulta->fetch();
//					
//					$impIgv = $Row[0];
					
					foreach ($Rows as $key)
					{
						switch($key[0])
						{
							case 1:		//Agua
								$impAgua += $key[1];
								
								break;
							
							case 2:		//Desague
								$impDesa += $key[1];
								
								break;
							
							case 6:		//Cargo Fijo
								$impCarg += $key[1];
								
								break;
							
							case 3: case 4: case 9: case 71: case 72: case 10009: case 10013:	//Intereses
								$impInte += $key[1];
								
								break;
							
							case 7: case 8: //Redondeos
								$impRedo += $key[1];
								
								break;
							
							case 5:		//Igv
								$impIgv += $key[1];
								
								break;
								
							default:
								$impOtro += $key[1];
								
								break;
								
						}
					}
					
					$mes = str_pad($mes, 2, "0", STR_PAD_LEFT);;
					
                    $this->SetTextColor(0);
					
                    $this->Cell($Dim[3], $h, $item, 1, 0, 'C', false);
                    $this->Cell($Dim[4], $h, utf8_decode($numeroinscripcion), 1, 0, 'C', false);
                    $this->Cell($Dim[5], $h, utf8_decode($propietario), 1, 0, 'L', false);
                    $this->Cell($Dim[3], $h, utf8_decode($anio), 1, 0, 'C', false);
                    $this->Cell($Dim[3], $h, utf8_decode($mes), 1, 0, 'C', false);
                    $this->Cell($Dim[6], $h, number_format($impAgua, 2), 1, 0, 'C', false);
                    $this->Cell($Dim[6], $h, number_format($impDesa, 2), 1, 0, 'C', false);
                    $this->Cell($Dim[6], $h, number_format($impCarg, 2), 1, 0, 'C', false);
                    $this->Cell($Dim[6], $h, number_format($impInte, 2), 1, 0, 'C', false);
                    $this->Cell($Dim[6], $h, number_format($impRedo, 2), 1, 0, 'C', false);
                    $this->Cell($Dim[6], $h, number_format($impOtro, 2), 1, 0, 'C', false);
                    $this->Cell($Dim[6], $h, number_format($impIgv, 2), 1, 0, 'C', false);
                    $this->Cell($Dim[2], $h, number_format($importe, 2), 1, 1, 'R', false);
                    
                    break;

                case 1:
                    $this->SetTextColor(255);
					
                    $this->Cell($Dim[1], $h, utf8_decode($item), 1, 0, 'C', true);
                    $this->Cell($Dim[2], $h, number_format($inscripcion, 2), 1, 1 , 'R', true);
					
                    break;              
            }
        }
    }

    $Dim = array('1'=>251, '2'=>25, '3'=>12, '4'=>20, '5'=>90, '6'=>15);
    
    $codemp     = 1;
    $codsuc     = $_GET["codsuc"];
    $ciclo      = $_GET["ciclo"];
    $anioP       = $_GET["anio"];
    $mes        = $_GET["mes"];
    $texto      = $_GET["mestexto"];
    $valor      = $_GET["valor"];
    $idusuario  = $_SESSION['id_user'];
    $periodo    = $_GET["anio"].($_GET['mes']);
    
    $total      = 0;
    $totalconv  = 0;
    
    $objReporte = new clsLecturas("L");
    $fechaact = '01/'.$_GET['mes']."/".$_GET['anio'];

    $contador   = 0;
    $objReporte->AliasNbPages();
    // $objReporte->SetLeftMargin(10);
    // $objReporte->SetAutoPageBreak(true,5);
    $objReporte->AddPage('H');
    
    $ultimodia = $clfunciones->obtener_ultimo_dia_mes($mes, $anioP);
    $primerdia = $clfunciones->obtener_primer_dia_mes($mes, $anioP);

    // Para obtener el periodo de faturacion

    $sqlLect = "select nrofacturacion
        FROM facturacion.periodofacturacion
        WHERE codemp = ? AND codsuc = ? AND codciclo = ? AND anio = ? AND mes = ?";
    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute(array($codemp,$codsuc,$ciclo,$anio,$mes));
    $nrofact = $consultaL->fetch();
	
    if(intval($mes)<10): $mesP = "0".$mes; else: $mesP = $mes; endif;

    if($mesmod == '09' AND $anio == '2014'):

        $sqlLect = "SELECT 
                d.nroinscripcion as nroinscripcion,
                cli.propietario as propietario, 
                per.anio as anio, 
                per.mes as mes, 
                SUM(d.importe) as importe 
                FROM facturacion.cabctacorriente c
                INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) 
                INNER JOIN catastro.clientes cli ON (cli.codemp = c.codemp AND cli.codsuc = c.codsuc AND cli.nroinscripcion = c.nroinscripcion)
                INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo)
                WHERE d.codsuc = {$codsuc} and d.periodo = '".$anio.$mesmod."'
                AND d.tipo = 1 AND d.tipoestructura = 0
                GROUP BY d.nroinscripcion, cli.propietario, per.anio, per.mes
                ORDER BY d.nroinscripcion, cli.propietario, per.anio, per.mes";

    else:
    	$sqlLect = "SELECT d.nroinscripcion AS nroinscripcion, Cli.propietario AS propietario, per.anio AS anio, per.mes AS mes, ";
		$sqlLect .= " SUM((d.importe - d.importeacta) + d.importerebajado) AS importe ";
		$sqlLect .= "FROM facturacion.detctacorriente d ";
		$sqlLect .= " INNER JOIN catastro.clientes cli ON (cli.codemp = d.codemp AND cli.codsuc = d.codsuc AND cli.nroinscripcion = d.nroinscripcion) ";
		$sqlLect .= " INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo) ";
		$sqlLect .= "WHERE d.codsuc = ".$codsuc." ";
		$sqlLect .= " AND d.periodo = '".$anioP.$mesP."' ";
		$sqlLect .= " AND d.tipo = 1 ";
		$sqlLect .= " AND d.tipoestructura = 0 ";
		$sqlLect .= " AND d.codconcepto <> 101 ";
		$sqlLect .= " AND d.codtipodeuda <> 3 ";
		//$sqlLect .= " AND d.nroinscripcion = 13 ";
		$sqlLect .= "GROUP BY d.nroinscripcion, Cli.propietario, per.anio, per.mes ";
		$sqlLect .= "ORDER BY d.nroinscripcion ASC, CAST(per.anio AS INTEGER) DESC, CAST(per.mes AS INTEGER) DESC ";
		
    endif;

    $consultaL = $conexion->prepare($sqlLect);
    $consultaL->execute();
    $items    = $consultaL->fetchAll();
	
    $contador = 0;
	
	$import   = 0;
    
    foreach ($items as $key):
		$contador++;
		
		$NroInscripcion = $key['nroinscripcion'];
		$Propietario = $key['propietario'];
		$Anio = $key['anio'];
		$Mes = $key['mes'];

		$objReporte->contenido($contador, $NroInscripcion, $Propietario, $Anio, $Mes, $key['importe']);
        
		$import += $key['importe'];

    endforeach;

    $objReporte->contenido(strtoupper('TOTAL'), $import, '', '', '', '', 1);

    $objReporte->Output();  
    
    
?>