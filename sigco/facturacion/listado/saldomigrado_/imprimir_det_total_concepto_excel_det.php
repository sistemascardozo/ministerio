<?php
    set_time_limit(0);
    
    include("../../../../objetos/clsReporte.php");
    include("../../../../objetos/clsReporteExcel.php");
	
    $nrolistado = $_GET['nrolistado'];
    $codsuc     = $_SESSION['IdSucursal'];
    $clsFunciones = new clsFunciones();
    $objReporte = new clsReporte();
    header("Content-type: application/vnd.ms-excel; name='excel'");  
    header("Content-Disposition: filename=Listado.xls");  
    header("Pragma: no-cache");  
    header("Expires: 0"); 
    //////////////

    $anio             = $row['anio'];
    $mes              = $row['mes'];
    $codemp           = 1;
    $codsuc           = $_GET["codsuc"];
    $ciclo            = $_GET["ciclo"];
    $anio             = $_GET["anio"];
    $mes              = $_GET["mes"];
    $texto            = $_GET["mestexto"];
    $valor            = $_GET["valor"];
    $idusuario        = $_SESSION['id_user'];
    $periodo          = $_GET["anio"].($_GET['mes']);
    $codconcepto      = $_GET["codconcepto"];
    $codtipoentidades = $_GET["tipoentidades"];
    $total            = 0;
    $totalconv        = 0;
    
    $conagregar       = '';

    if($codconcepto != '%'): $conagregar = " AND d.codconcepto = " . $codconcepto; endif;
    
    $fechaact = '01/'.$_GET['mes']."/".$_GET['anio'];

    $contador   = 0;    

    if(intval($mes)<10): $mesmod = "0".$mes; else: $mesmod = $mes; endif;
    

    ////
    $ctd=0;

        if($mesmod == '09' AND $anio == '2014'):
            //OBTENER LOS CONCEPTOS
            $Sql = "SELECT 
                      co.codconcepto,
                      co.descripcion
                    FROM
                      facturacion.detctacorriente d
                      INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
                      AND (d.codsuc = co.codsuc)
                      AND (d.codconcepto = co.codconcepto)
                      WHERE d.codsuc = {$codsuc} and d.periodo = '".$anio.$mesmod."'
                        AND d.tipo = 0 AND d.tipoestructura = 0   " . $conagregar . "
                      GROUP BY  co.codconcepto,co.descripcion
                      ORDER BY co.codconcepto";

            $consultacol = $conexion->query($Sql);
            $alcas = $consultacol->fetchAll();
            $nr= $consultacol->rowCount();
            $nroconceptos=0;

        else:
 
            //OBTENER LOS CONCEPTOS
            $Sql = "SELECT 
                      co.codconcepto,
                      co.descripcion
                    FROM
                      facturacion.detctacorriente d
                      INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp)
                      AND (d.codsuc = co.codsuc)
                      AND (d.codconcepto = co.codconcepto)
                     WHERE d.codsuc = {$codsuc} and d.periodo = '".$anio.$mes."'
                         AND d.tipoestructura = 0  " . $conagregar . "
                        AND d.codconcepto <> 101 AND d.codtipodeuda <> 3
                      GROUP BY  co.codconcepto,co.descripcion
                      ORDER BY co.codconcepto";

            $consultacol = $conexion->query($Sql);
            $alcas = $consultacol->fetchAll();
            $nr= $consultacol->rowCount();
            $nroconceptos=0;
        
        endif;
?>
<?php CabeceraExcel(2,14); ?>


<table class="ui-widget" border="1" cellspacing="0" id="TbConsulta" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
    <tr title="Cabecera">
        <th scope="col" colspan="<?=(7 + $nr + 1)?>" align="center" class="ui-widget-header">PROVISION <?=$meses[intval($mes)]."-".$anio?></th>
    </tr>
    <tr title="Cabecera">
        <th scope="col" width="10%" align="center" class="ui-widget-header">ITEM</th>
        <th scope="col" width="20%" align="center" class="ui-widget-header">N° INSCRIPCION</th>
        <th scope="col" width="50%" align="center" class="ui-widget-header">PROPIETARIO/CONCEPTO</th>
        <th scope="col" width="5%" align="center" class="ui-widget-header">ANIO</th>
        <th scope="col" width="5%" align="center" class="ui-widget-header">MES</th>
         <?php
                $SelFac ='';
                foreach($alcas as $row)
                {
                    $nroconceptos++;
                    $concepto= $row["descripcion"];//(d.importe-(d.importerebajado+d.imppagado))
                    $SelFac.="SUM(CASE WHEN d.codconcepto=".$row["codconcepto"]." THEN (d.importe) ELSE 0  END) AS c".$row["codconcepto"].",";
                    ?>
                    <th width="50" scope="col" class="ui-widget-header">C<?=$row["codconcepto"]?></th>
                    <?php
                }
                


            ?>
        <th scope="col" width="5%" align="center" class="ui-widget-header">CREDITOS</th>
        <th scope="col" width="5%" align="center" class="ui-widget-header">REBAJAS</th>
        <th scope="col" width="10%" align="center" class="ui-widget-header">TOTAL</th>
    </tr>
    </thead>
    <tbody style="font-size:10px">
    <?php
        
        if($mesmod == '09' AND $anio == '2014'):

            $sqlLect = "SELECT 
                    d.nroinscripcion as nroinscripcion,
                    cli.propietario as propietario, 
                    per.anio as anio, 
                    per.mes as mes, 
                    SUM(d.importe) as importe,
                    d.codconcepto,
                    ".$SelFac." SUM(d.importe) AS c0
                    FROM facturacion.cabctacorriente c
                    INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) 
                    INNER JOIN catastro.clientes cli ON (cli.codemp = c.codemp AND cli.codsuc = c.codsuc AND cli.nroinscripcion = c.nroinscripcion)
                    INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo)
                    WHERE d.codsuc = {$codsuc} and d.periodo = '".$anio.$mesmod."'
                    AND d.tipo = 0 AND d.tipoestructura = 0   " . $conagregar . "

                    GROUP BY d.nroinscripcion, cli.propietario, per.anio, per.mes, d.codconcepto
                    ORDER BY d.nroinscripcion, cli.propietario, per.anio, per.mes";

        else:
        
            $sqlLect = "SELECT 
            co.codtipoentidades as codtipoentidad,
            te.descripcion as entidad,
            d.nroinscripcion as nroinscripcion,
            cli.propietario as propietario, 
            d.nrofacturacion as nrofacturacion, 
            per.anio as anio, 
            to_char(CAST(per.mes AS INTEGER),'00') as mes, /*per.mes as mes, */
            SUM(d.importe) as importe ,
            to_char(cli.codsuc,'00')||''||TRIM(to_char(d.nroinscripcion,'000000')) as codantiguo,
            d.periodo,
            ".$SelFac." SUM(d.importe) AS c0,
            SUM(d.importerebajado) as importerebajado
            FROM facturacion.cabctacorriente c
            INNER JOIN facturacion.detctacorriente d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.codciclo = d.codciclo) AND (c.nrofacturacion = d.nrofacturacion) AND (c.nroinscripcion = d.nroinscripcion) AND (c.anio = d.anio) AND (c.mes = d.mes) AND (c.periodo = d.periodo) AND (c.tipo = d.tipo) AND (c.tipoestructura = d.tipoestructura) 
            INNER JOIN catastro.clientes cli ON (cli.codemp = c.codemp AND cli.codsuc = c.codsuc AND cli.nroinscripcion = c.nroinscripcion)
            INNER JOIN facturacion.periodofacturacion per ON (d.codemp = per.codemp AND d.codsuc = per.codsuc AND d.nrofacturacion = per.nrofacturacion AND d.codciclo = per.codciclo)
            INNER JOIN catastro.conexiones co ON (cli.codemp = co.codemp AND cli.codsuc = co.codsuc AND cli.nroinscripcion = co.nroinscripcion) 
            INNER JOIN tipoentidades te ON (te.codtipoentidades = co.codtipoentidades )
            WHERE d.codsuc = {$codsuc} and d.periodo = '".$anio.$mes."'
            /*AND d.tipo = 1*/ AND d.tipoestructura = 0  " . $conagregar . "
            AND d.codconcepto <> 101 AND d.codtipodeuda <> 3 -- AND d.nroinscripcion=335788
            AND co.codtipoentidades='".$codtipoentidades."'
            GROUP BY co.codtipoentidades, te.descripcion, d.nroinscripcion, cli.propietario, per.anio, per.mes, d.nrofacturacion,
            cli.codsuc,d.periodo
            ORDER BY co.codtipoentidades, d.nroinscripcion,
            cli.propietario, per.anio, per.mes  ";


        
        endif;
        //die($sqlLect);
        $consultaL = $conexion->prepare($sqlLect);
        $consultaL->execute();
        $items     = $consultaL->fetchAll();
        $contador  = 0;
        $import    = 0;
        $importc    = 0;
        $import1   = 0;
        $importr=0;

        $nroins    = '';
        $nrofact   = '';
        $valore    = array();
        $it        = 0;
        $ip        = 0;
        $codenti   = $items[0]['codtipoentidad'];
        $tConceptos = array();
        $ins='';
        $fechault=$anio.'-'.trim($mes).'-'.date ('t', mktime (0,0,0, trim($mes), 1, $anio));
        foreach($items as $key):
            //OBTENER CREDITOS PENDIENTES A ESA FECHA
            //$fechault=$key['anio'].'-'.trim($key['mes']).'-'.date ('t', mktime (0,0,0, trim($key['mes']), 1, $key['anio']));
            $rowx[0]=0;
            if($ins!=$key['nroinscripcion'])
            {   $ins=$key['nroinscripcion'];
                    $Sql="SELECT 
                      SUM(d.imptotal)
                      FROM
                      facturacion.cabcreditos c
                      INNER JOIN facturacion.detcreditos d ON (c.codemp = d.codemp)
                      AND (c.codsuc = d.codsuc)
                      AND (c.nrocredito = d.nrocredito)
                    WHERE c.codsuc={$codsuc} AND c.nroinscripcion={$key['nroinscripcion']} 
                    AND d.fechapago>'{$fechault}' 
                    AND c.fechareg<'{$fechault}' 
                    and c.estareg=1";
                $rowx=$conexion->query($Sql)->fetch();
                $Sql="SELECT 
                      SUM(d.importe)
                      FROM
                      facturacion.cabrefinanciamiento c
                      INNER JOIN facturacion.detrefinanciamiento d ON (c.codemp = d.codemp)
                      AND (c.codsuc = d.codsuc)
                      AND (c.nrorefinanciamiento = d.nrorefinanciamiento)
                    WHERE c.codsuc={$codsuc} AND c.nroinscripcion={$key['nroinscripcion']} 
                    AND d.fechapago>'{$fechault}' 
                    AND c.fechaemision<'{$fechault}' 
                    and c.estareg=1";
                $rowy=$conexion->query($Sql)->fetch();
                $rowx[0]=$rowx[0]+$rowy[0];
            }
            if($ip == 0):
        ?>
                <tr>
                    <td width="10%" align="center" class="ui-state-active" style="font-size:12px">GRUPO</td> 
                    <td width="90%" colspan="5" align="left" class="ui-state-active" style="font-size:12px"><?=strtoupper($key['entidad'])?></td>                
                </tr>
        
        <?php    
            
                $ip = 1;

            endif;

        if( $codenti != $key['codtipoentidad'] ):

        ?>

            <tr style="background-color:#93A2BF; color:#FFFFFF; font-weight: bold ">
                <td align="center" colspan="5">TOTAL PARCIAL ===></td>
                <td align="right"><?=number_format($import1,2)?></td>
            </tr>
        <?php
            $import1 = 0;
            $importc1=0;
            $importr1=0;
            $tConceptosp = array();
        ?>

            <tr>
                <td colspan="6" align="center">&nbsp;</td>                
            </tr>
            <tr>
                <td colspan="6" align="center">&nbsp;</td>                
            </tr>
            <tr >
                <td width="10%" align="center" class="ui-state-active" style="font-size:12px">GRUPO</td>                
                <td width="90%" colspan="5" align="left" class="ui-state-active" style="font-size:12px"><?=strtoupper($key['entidad'])?></td>                
            </tr>
        <?php
            $contador ++;
            $import  += $key['importe']+$rowx[0]-$key['importerebajado'];
            $import1 += $key['importe']+$rowx[0]-$key['importerebajado'];

            $importc  += $rowx[0];
            $importc1 += $rowx[0];

            $importr  += $key['importerebajado'];
            $importr1 += $key['importerebajado'];
        ?>
            <tr>
                <td align="center" width="10%"><?=$contador?></td>
                <td align="center" width="20%"><?=$clsFunciones->CodUsuario($codsuc,$key['nroinscripcion']) ?></td>
                <td align="left" width="50%"><?=$key['propietario']?></td>
                <td align="center" width="10%"><?=$key['anio']?></td>
                <td align="center" width="10%"><?=$key['mes']?></td>
                <?php foreach($alcas as $rowc):
                    $c='c'.$rowc['codconcepto'];
                    $tusuario +=$key[$c];
                    $TotalC+=$key[$c];
        
                     $tConceptos[$c]=$tConceptos[$c]+$key[$c];
                     $tConceptosp[$c]=$tConceptosp[$c]+$key[$c];
                    ?>
                    <td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($key[$c],2)?></td>                     
                    <?php endforeach;?>
                <td align="right" width="10%"><?=number_format($rowx[0],2)?></td>
                <td align="right" width="10%"><?=number_format($key['importerebajado'],2)?></td>
                <td align="right" width="10%"><?=number_format($rowx[0]+$key['importe'],2)?></td>
            </tr>
        <?php    
        else:

            $contador ++;
            $import  += $key['importe']+$rowx[0]-$key['importerebajado'];
            $import1 += $key['importe']+$rowx[0]-$key['importerebajado'];

            $importc  += $rowx[0];
            $importc1 += $rowx[0];

            $importr  += $key['importerebajado'];
            $importr1 += $key['importerebajado'];
        ?>
            
            <tr>
                <td align="center" width="10%"><?=$contador?></td>
                <td align="center" width="20%"><?=$clsFunciones->CodUsuario($codsuc,$key['nroinscripcion'])?></td>
                <td align="left" width="50%"><?=$key['propietario']?></td>
                <td align="center" width="10%"><?=$key['anio']?></td>
                <td align="center" width="10%"><?=$key['mes']?></td>
                <?php foreach($alcas as $rowc):
                    $c='c'.$rowc['codconcepto'];
                    $tusuario +=$key[$c];
                    $TotalC+=$key[$c];
        
                     $tConceptos[$c]=$tConceptos[$c]+$key[$c];
                     $tConceptosp[$c]=$tConceptosp[$c]+$key[$c];
                    ?>
                    <td width="50" scope="col" align="right" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($key[$c],2)?></td>                     
                    <?php endforeach;?>
                    <td align="right" width="10%"><?=number_format($rowx[0],2)?></td>
                    <td align="right" width="10%"><?=number_format($key['importerebajado'],2)?></td>
                <td align="right" width="10%"><?=number_format($key['importe']+$rowx[0],2)?></td>
            </tr>

        <?php

        endif;

            $codenti = $key['codtipoentidad'];

        endforeach;

        ?>
        <tr style="background-color:#93A2BF; color:#FFFFFF; font-weight: bold ">
            <td align="center" colspan="5" >TOTAL PARCIAL ===></td>
            <?php
             foreach($alcas as $rowc):
                 $c='c'.$rowc['codconcepto'];
            ?>
            <td width="50" scope="col" align="right"  style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($tConceptosp[$c],2)?></td>                        
            <?php endforeach;?>
            <td align="right"><?=number_format($importc1,2)?></td>
            <td align="right"><?=number_format($importr1,2)?></td>
            <td align="right"><?=number_format($import1,2)?></td>
        </tr>

    </tbody>
    <tfoot class="ui-widget-header">
        <tr style="background-color:#22BF54; color:#FFFFFF; font-weight: bold ">
            <td colspan="5" align="center">
                TOTALES
            </td>
             <?php
             foreach($alcas as $rowc):
                 $c='c'.$rowc['codconcepto'];
            ?>
            <td width="50" scope="col" align="right"  style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($tConceptos[$c],2)?></td>                        
            <?php endforeach;?>
            <td align="right" ><?=number_format($importc,2)?></td>
            <td align="right" ><?=number_format($importr,2)?></td>
            <td align="right" ><?=number_format($import,2)?></td>
         </tr>
    </tfoot>
    
</table>
<table><tr><tr><td>&nbsp;</td></tr></tr></table>
<table>
     <thead class="ui-widget-header" style="font-size:10px">
        <tr title="Cabecera">
            <th scope="col" class="ui-widget" colspan="4" align="center">LEYENDA</th>
        </tr>
      <tr title="Cabecera">
        <th scope="col" width="100" align="center" class="ui-widget-header">ITEM</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">COD</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">DESCRIPCION</th>
        <th scope="col" width="100" align="center" class="ui-widget-header">TOTAL</th>
      </tr>
      <tbody>
        <?php
        $i=0;
         foreach($alcas as $rowc):
            $i++;
             $c='c'.$rowc['codconcepto'];
        ?>
        <tr>
            <td><?=$i?></td>
            <td><?=$c?></td>
            <td><?=$rowc['descripcion']?></td>                      
            <td scope="col" align="right"style="mso-number-format:'\#\,\#\#0\.00';">
                <?=number_format($tConceptos[$c],2)?>
            </td>                       

        </tr>
        <?php endforeach;?>
        <tr>
            <td><?=$i+1?></td>
            <td></td>
            <td>CREDITOS</td>                      
            <td scope="col" align="right"style="mso-number-format:'\#\,\#\#0\.00';">
                <?=number_format($importc,2)?>
            </td>                       

        </tr>
        <tr>
            <td><?=$i+2?></td>
            <td></td>
            <td>REBAJAS</td>                      
            <td scope="col" align="right"style="mso-number-format:'\#\,\#\#0\.00';">
                <?=number_format($importr,2)?>
            </td>                       

        </tr>
      </tbody>
      <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td class="ui-widget-header" colspan=3>&nbsp;</td>
            <td align="right" class="ui-widget-header" style="mso-number-format:'\#\,\#\#0\.00';"><?=number_format($TotalC+$importc,2)?></td>                        
        </tr>
      </tfoot>

</table>
