<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("../../../../objetos/clsFunciones.php");

$objFunciones = new clsFunciones();
$conexion->beginTransaction();

$codsuc = $_SESSION['IdSucursal'];
$nroreclamo = $_POST["nroreclamo"];
$nrodetalle = $_POST["nrodetalle"];
$inspectores = $_POST["inspectores"];
$fin_reclamo = $_POST["finreclamo"];
$nroinscripcion = $_POST["nroinscripcion"];
$conciliar = $_POST["conciliacion"];
$glosa_distribucion = strtoupper($_POST["glosa_distribucion"]);
$fecha_Fin = $objFunciones->FechaServer();
if ($conciliar == 1) {
    $codestadoreclamo = 9; //por conciliar
    $inicial = 0;
} else {
    $inicial = 1;
    $codestadoreclamo = 1;
}

$fecha = "";
if ($fin_reclamo == 1) {
    $fecha = ",fecha_finalizacion='".$fecha_Fin."'";
}

$upd = "UPDATE reclamos.detalle_reclamos set observacion=?,codinspector=?,fin_reclamo=? ".$fecha.",
    codestadoreclamo=?,inicial=?
    WHERE codsuc=? and nrodetalle=?";
$resultUpd = $conexion->prepare($upd);
$resultUpd->execute(array($glosa_distribucion, $inspectores, $fin_reclamo, $codestadoreclamo, $inicial, $codsuc, $nrodetalle));
if ($resultUpd->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error reclamos";
    die(2);
}
//SI EL RECLAMO SE FINALIZA, SE LIBERAM LAS FACTURACIONES INVOLUCRADAS

$updR = "UPDATE reclamos.reclamos set fin_reclamo=? ".$fecha.",conciliar=? WHERE codsuc=? and nroreclamo=?";
$consultaR = $conexion->prepare($updR);
$consultaR->execute(array($fin_reclamo, $conciliar, $codsuc, $nroreclamo));
if ($consultaR->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    die(2);
} else {
    $conexion->commit();
    $mensaje = "El Registro se ha Grabado Correctamente";
    echo $res = 1;
}
?>
