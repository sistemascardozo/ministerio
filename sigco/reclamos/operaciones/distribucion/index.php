<?php

include("../../../../include/main.php");
include("../../../../include/claseindex.php");
$TituloVentana = "RECLAMOS DERIVADOS";
$Activo = 1;
CuerpoSuperior($TituloVentana);
$Op = isset($_GET['Op']) ? $_GET['Op'] : 0;

unset($_SESSION["oreclamos"]);
$Op = isset($_GET['Op']) ? $_GET['Op'] : 0;
$codsuc = $_SESSION['IdSucursal'];
$codarea = $_SESSION['idarea'];
$codusu = $_SESSION['id_user'];
$valor = isset($_GET["Valor"]) ? strtoupper($_GET["Valor"]) : "";
$fecha_fin = date('Y-m-d');
$FormatoGrilla = array();

$Sql = "SELECT r.correlativo, d.nroreclamo, c.codantiguo, c.propietario, usu.login, e.descripcion as estado, 
    usud.login, cr.descripcion as rec, r.glosa, d.codestadoreclamo, d.nrodetalle_origen, r.fin_reclamo, 1, 
    d.nrodetalle, d.observacion, d.inicial, r.conciliar, r.nroinscripcion
    FROM reclamos.detalle_reclamos as d
    INNER JOIN reclamos.reclamos as r on(d.codemp=r.codemp AND d.codsuc=r.codsuc AND d.nroreclamo=r.nroreclamo)
    INNER JOIN catastro.clientes as c on(r.codemp=c.codemp AND r.codsuc=c.codsuc AND r.nroinscripcion=c.nroinscripcion)
    INNER JOIN reclamos.estadoreclamo as e on(d.codestadoreclamo=e.codestadoreclamo)
    INNER JOIN reclamos.conceptosreclamos as cr on(r.codconcepto=cr.codconcepto)
    INNER JOIN seguridad.usuarios AS usu ON (usu.codusu= d.codusu_derivacion)
    INNER JOIN seguridad.usuarios AS usud ON (usud.codusu= d.codusu) ";

$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql); //Sentencia SQL   
$FormatoGrilla[1] = array('1' => 'd.nroreclamo', '2' => 'c.propietario', '3' => 'e.descripcion',
    '4' => 'cr.descripcion', '5' => 'r.glosa', '6' => 'c.codantiguo');          //Campos por los cuales se har� la b�squeda
$FormatoGrilla[2] = $Op;
$FormatoGrilla[3] = array('T1' => 'Correlativo', 'T2' => 'Nro. Reclamo', 'T3' => 'Nro. Inscripcion', 'T4' => 'Propietario', 'T5' => 'Derivado por', 'T6' => 'Estado',
    'T6' => 'Derivado a', 'T7' => 'Reclamo');   //T�tulos de la Cabecera
$FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'center', 'A3' => 'center', 'A4' => 'left');                        //Alineaci�n por Columna
$FormatoGrilla[5] = array('W1' => '60', 'W2' => '60', 'W3' => '50', 'W4' => '250', 'W5' => '50', 'W6' => '120', 'W7' => '50', 'W9' => '100');                                 //Ancho de las Columnas
$FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                   //Registro por P�ginas
$FormatoGrilla[7] = 1200;                                                            //Ancho de la Tabla
$FormatoGrilla[8] = " AND (d.codemp=1 AND d.codsuc=".$codsuc." AND d.codarea=".$codarea." AND d.codusu=".$codusu." ) ORDER BY d.nroreclamo desc ";                                   //Orden de la Consulta
$FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
    'NB' => '4', //N�mero de Botones a agregar
    'BtnId1' => 'BtnModificar', //Nombre del Boton
    'BtnI1' => 'modificar.png', //Imagen a mostrar
    'Btn1' => 'Recepcionar Reclamo', //Titulo del Bot�n
    'BtnF1' => 'onclick="Modificar(this);"', //Eventos del Bot�n
    'BtnCI1' => '13', //Item a Comparar
    'BtnCV1' => '1', //Valor de comparaci�n
    'BtnId2' => 'BtnEliminar',
    'BtnI2' => 'restablecer.png',
    'Btn2' => 'Realizar Derivacion del Reclamo',
    'BtnF2' => 'onclick="Derivar(this)"',
    'BtnCI2' => '13', //campo 3
    'BtnCV2' => '1', //igua a 1
    /* 'BtnId3'=>'BtnRestablecer', //y aparece este boton
      'BtnI3'=>'ok.png',
      'Btn3'=>'Marcar para Atencion',
      'BtnF3'=>'onclick="Atender(this)"',
      'BtnCI3'=>'11',
      'BtnCV3'=>'1', */
    'BtnId3' => 'BtnRestablecer', //y aparece este boton
    'BtnI3' => 'imprimir.png',
    'Btn3' => 'Imprimir Formato de Reclamos',
    'BtnF3' => 'onclick="ImprimirR(this)"',
    'BtnCI3' => '13',
    'BtnCV3' => '1',
    'BtnId4' => 'BtnEliminar',
    'BtnI4' => 'documento.png',
    'Btn4' => 'Ver Detalle del Reclamo',
    'BtnF4' => 'onclick="VerDetalle(this)"',
    'BtnCI4' => '13', //campo 3
    'BtnCV4' => '1'); //igua a 1);
$FormatoGrilla[10] = array(
    array('Name' => 'nrorec', 'Col' => 2),
    array('Name' => 'nrodetalle', 'Col' => 14),
    array('Name' => 'estado', 'Col' => 10),
    array('Name' => 'texto', 'Col' => 5),
    array('Name' => 'nroinscripcion', 'Col' => 19),
    array('Name' => 'fin', 'Col' => 11),
    array('Name' => 'observacion', 'Col' => 15),
    array('Name' => 'inicial', 'Col' => 17),
    array('Name' => 'conciliar', 'Col' => 18)
); //DATOS ADICIONALES  
$FormatoGrilla[11] = 7; //FILAS VISIBLES                 
$_SESSION['Formato'] = $FormatoGrilla;
Cabecera('', $FormatoGrilla[7], 850, 600);
Pie();
?>

<script src="../ingresos/js_reclamos.js" type="text/javascript"></script>
<script>
    var Id = ''
    var Id2 = ''
    var IdAnt = ''
    var e11 = ""
    var e22 = ""
    var estadoA = "";
    var urldir = '<?=$urldir ?>';
    var Pagina = <?=$pagina ?>;
    var nPag = <?=$TAMANO_PAGINA ?>;
    var codsuc = <?=$codsuc ?>

    var nroreclamo = ''
    function Modificar(obj)
    {
        $("#form1").remove();
        var estado = $(obj).parent().parent().data('estado')
        var texto = $(obj).parent().parent().data('texto')
        var fin = $(obj).parent().parent().data('fin')
        var inicial = $(obj).parent().parent().data('inicial')
        var nrorec = $(obj).parent().parent().data('nrorec')
        var nrodetalle = $(obj).parent().parent().data('nrodetalle')
        //alert(estado);
        if (inicial == 3)
        {
            alert("No se puede Modificar el Reclamo por que ya se lo Derivo")
            return false
        }
        if (fin == 1)
        {
            alert("No se puede Modificar el Reclamo por que ya se lo Dio por Finalizado")
            return false
        }
        if (estado != 1 && estado != 9)
        {
            alert("El Reclamo no se puede MODIFICAR por que Se encuentra con el Estado: " + texto)
            return
        }

        $("#Modificar").dialog({title: 'Modificar Reclamo Derivado'});
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $.ajax({
            url: 'mantenimiento.php',
            type: 'POST',
            async: true,
            data: 'nrorec=' + nrorec + '&codsuc=<?=$codsuc ?>&Op=1&nrodetalle=' + nrodetalle,
            success: function(data) {
                $("#DivModificar").html(data);
            }
        })

    }

    function VerDetalle(obj)
    {
        $("#form1").remove();
        var Id = $(obj).parent().parent().data('nrorec')
        AbrirPopupImpresion('<?=$urldir ?>sigco/reclamos/operaciones/ingresos/popup/popup_detalle_reclamos.php?codsuc=<?=$codsuc ?>&nroreclamo=' + Id, 1000, 600)
    }
    function Derivar(obj)
    {

        var nrorec = $(obj).parent().parent().data('nrorec')
        var nrodetalle = $(obj).parent().parent().data('nrodetalle')
        var text = $(obj).parent().parent().data('texto')
        var fin = $(obj).parent().parent().data('fin')
        var inicial = $(obj).parent().parent().data('inicial')
        var conciliar = $(obj).parent().parent().data('conciliar')
        var par = '&nrodetalle=' + nrodetalle + '&fecha_fin=<?=$fecha_fin ?>'
        //nrorec,fin,par
        if (conciliar == 1)
        {
            alert("No se puede Derivar el Reclamo por que se marco para Conciliar")
            return false
        }
        if (inicial == 3)
        {
            alert("No se puede Derivar el Reclamo por que ya se lo Derivo")
            return false
        }
        if (fin == 1)
        {
            alert("El Reclamo no se puede Derivar por que Se encuentra Finalizado")
            return
        }
        var observacion = $(obj).parent().parent().data('observacion')
        if (observacion == '')
        {
            alert("Complete los datos de Distribucion, antes de Derivar")
            return
        }
        cargar_FROM_derivacion(nrorec, par)


    }
    function Atender(obj)
    {
        //nroreclamo,nrodetalle,codestado,estado,fin
        var fin = $(obj).parent().parent().data('fin')
        var nroreclamo = $(obj).parent().parent().data('nrorec')
        var codestado = $(obj).parent().parent().data('estado')
        var estado = $(obj).parent().parent().data('texto')
        var nrodetalle = $(obj).parent().parent().data('nrodetalle')
        var observacion = $(obj).parent().parent().data('observacion')
        if (fin == 1)
        {
            alert("No se puede Atender el Reclamo por que se Encuentra Finalizado")
            return
        }
        if (codestado != 1)
        {
            alert("No se puede realizar Ningun Accion sobre el Reclamo por que se encuentra: " + estado)
            return
        }
        if (observacion == '')
        {
            alert("Complete los datos de Distribucion, antes de pasar a Atencion")
            return
        }
        if (confirm("Va a Proceder a Realizar la Atencion del Reclamo. Una vez hecho eso no podra realizar ningun Modificacion. Desea Continuar?") == false)
        {
            return false
        }
        $.ajax({
            url: 'atencion.php',
            type: 'POST',
            async: true,
            data: 'nroreclamo=' + nroreclamo + '&nrodetalle=' + nrodetalle,
            success: function(data) {
                OperMensaje(data)
                $("#Mensajes").html(data);
                Buscar(0)
            }
        })

        //location.href='atencion.php?nroreclamo='+nroreclamo+'&nrodetalle='+nrodetalle

    }

    function ImprimirR(obj)
    {
        $("#form1").remove();
        var estado = $(obj).parent().parent().data('estado')
        var nrorec = $(obj).parent().parent().data('nrorec')
        var nroinsc = $(obj).parent().parent().data('nroinscripcion')
        nroreclamo = nrorec
        nroinscripcion = nroinsc
        $("#dialog-form-imprimir").dialog("open");

    }

    function atender_reclamos(nroreclamo, nrodetalle, codestado, estado, fin)
    {
        if (fin == 1)
        {
            alert("No se puede Atender el Reclamo por que se Encuentra Finalizado")
            return
        }
        if (codestado != 1)
        {
            alert("No se puede realizar Ningun Accion sobre el Reclamo por que se encuentra: " + estado)
            return
        }
        if (confirm("Va a Proceder a Realizar la Atencion del Reclamo. Una vez hecho eso no podra realizar ningun Modificacion.�Desea Continuar?") == false)
        {
            return false
        }
        location.href = 'atencion.php?nroreclamo=' + nroreclamo + '&nrodetalle=' + nrodetalle
    }
    function cargar_FROM_ajax_derivacion(nrorec, fin, par)
    {
        if (fin == 1)
        {
            alert("No se puede Derivar el Reclamo por que ya se la ha Dado por Finalizado")
            return
        }
        cargar_FROM_derivacion(nrorec, par)
        $("#dialog-form-derivar").dialog("open");
    }

    function cargar_FROM_ajax_derivacion(nrorec, fin, par)
    {
        if (fin == 1)
        {
            alert("El Reclamo no se puede Derivar por que Se encuentra con el Finalizado")
            return
        }

        cargar_FROM_derivacion(nrorec, par)
        $("#dialog-form-derivar").dialog("open");
    }
</script>
<?php include("../ingresos/ajax/FROM_op_impresion.php"); ?>
<div id="dialog-form-derivar" title="Derivar Reclamo Ingresado"  >
    <div id="div_derivar_reclamos"></div>
</div>
<script>
    $("#BtnNuevoB").hide();
</script>
<?php
CuerpoInferior();
?>