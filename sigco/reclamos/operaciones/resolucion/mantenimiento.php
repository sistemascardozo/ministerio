<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	unset($_SESSION["oreclamos"]);	
	
	include("../../../../objetos/clsDrop.php");
	include("../ingresos/clase/clsproceso.php");	
	
	$objDrop = new clsDrop();
	
	//$Id 	= explode("|",$_GET["Id"]);
	$codsuc	= $_SESSION['IdSucursal'];
	
	$nrodetalle = $_POST['nrodetalle'];//$Id[0];
	$nroreclamo = $_POST['nrorec'];//$Id[1];
	
	$sql = "select c.propietario,r.tiporeclamo,cr.descripcion as recl,r.glosa,r.nroinscripcion,c.codciclo
			from reclamos.reclamos as r
			inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
			inner join reclamos.conceptosreclamos as cr on(r.codconcepto=cr.codconcepto)
			where r.codsuc=? and r.nroreclamo=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroreclamo));
	$row = $consulta->fetch();
	
	$guardar = "?nrodetalle=".$nrodetalle;

	$sqlmes = "SELECT * from reclamos.mesesreclamados where codemp=1 and codsuc=? and nroreclamo=?";
		
		$consulta_mes = $conexion->prepare($sqlmes);
		$consulta_mes->execute(array($codsuc,$nroreclamo));
		$items_mes = $consulta_mes->fetchAll();

		//unset($_SESSION["oreclamos"]);
		foreach($items_mes as $row_mes)
		{
			$_SESSION["oreclamos"]->agregarvaloresxx($row_mes["nrofacturacion"],$row_mes["codconcepto"],$row_mes["imporiginal"],
                        $row_mes["imprebajado"],$row_mes["codtipodeuda"],$row_mes["categoria"],
                        $row_mes["nrodocumento"],$row_mes["anio"],
                        $row_mes["mes"],$row_mes["codciclo"],$row_mes["lecturaanterior"],$row_mes["lecturaultima"],$row_mes["consumo"],
                        $row_mes["lecturareclamo"],$row_mes["consumoreclamo"],$row_meses['seriedocumento']);
		}
//die('-->'.$_SESSION["oreclamos"]->item);
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
$(document).ready(function()
{

	 $( "#tabs" ).tabs();
    $( "#DivNose2" ).dialog({
            autoOpen: false,
            height: 450,
            width: 750,
            modal: true,
            resizable:false,
            buttons: {
                    "Aceptar": function() {
                            GuardarOK();
                    },
                    "Cancelar": function() {
                            $( this ).dialog( "close" );
                    }
            },
            close: function() {

            }
    });
     $( "#DivRebajar" ).dialog({
            autoOpen: false,
            height: 450,
            width: 650,
            modal: true,
            resizable:false,
            buttons: {
                    "Aceptar": function() {
                            guardarrebajasres();
                    },
                    "Cancelar": function() {
                            $( this ).dialog( "close" );
                    }
            },
            close: function() {

            }
    });
 });

	function ValidarForm(Op)
	{
		if($("#glosa_resolucion").val()==0)
		{
			$("#tabs").tabs({selected: 0});
			Msj($("#glosa_resolucion"),"Ingrese la Observacion de la Resolucion")
			return false
		}
		if($("#finreclamo").val()==1)
		{
			$("#tabs").tabs({selected: 0});
			r=confirm("Al Seleccionar la Opcion Fin del Reclamo "+
					  "Dara por Finalizado la Atencion del Reclamo.Desea Continuar?")
			if(r==false)
			{
				return false
			}
		}
		if($('#textFundado').val()==1)
		{
			if($('input[name="finreclamo"]:checked').val()==1)
			{
				GuardarOK()
			}	
			else
				GuardarP(Op)	
		}
		else
			GuardarP(Op)
	}
	function ActualizarFacturacion()
	{
		$("#DivNose").dialog('open');
	}
	var ItemUpd=0
	function cargar_from_detalle_facturacion(nroinscripcion,nrofacturacion,categoria,codtipodeuda,idx)
	{
		ItemUpd=idx;
		$.ajax({
			 url:'../ingresos/ajax/from_detalle_facturacion.php',
			 type:'POST',
			 async:true,
			 data:'nroinscripcion='+nroinscripcion+'&codsuc='+codsuc+'&nrofacturacion='+nrofacturacion+
			 	  '&categoria='+categoria+'&codtipodeuda='+codtipodeuda+'&index='+idx,
			 success:function(datos)
			 {
				$("#div_detalle_facturacion").html(datos)
				
				$("#mrebajar").val($("#tbrebajas tbody tr#tr_"+ItemUpd+" label.ImpRebajado").text()).keyup()
				$("#consumocalcular").val($("#tbrebajas tbody tr#tr_"+ItemUpd+" label.consumoreclamo").text());
				CalcularImporteConsumo();
				$( "#DivRebajar" ).dialog( "open" );
				$("#DivImporteRebaja").show();
			 }
		}) 
	}
function guardarrebajasres()
{
	
	var consumocalcular = $("#consumocalcular").val();
        consumocalcular = parseFloat(consumocalcular);
     if(eval(consumocalcular<0))
	{
            alert('El Consumo Ingesado no es Valido')
            return false;	
    }


    var vrebaja = $("#mrebajar").val();
        vrebaja = parseFloat(vrebaja);
     if(eval(vrebaja<0))
	{
            alert('El Monto Ingesado no es Valido')
            return false;	
    }
    else 
    {
            var imptH 	= Round($("#imptotalh").val(),2)
            var imptD 	= Round($("#mrebajar").val(),2)
            var index	= $("#idx").val()
            
            if(imptD > imptH)
            {
                    alert("El Monto a Rebajado no puede ser Mayor al Monto a Rabajar")
                    return
            }

            var cad=""
            var leca=$("#lecturaanterior"+index).val();
            var lecreb = parseFloat(leca) + parseFloat(consumocalcular)
            for(i=1;i<=$("#cont_facturacion").val();i++)
            {
                    cad=cad+"nrofacturacion"+i+"="+$("#nrofacturacion"+ItemUpd).val()+"&codconcepto"+i+"="+$("#codconceptoy"+i).val();
                    cad=cad+"&imporiginal"+i+"="+str_replace($("#impx"+i).val(),",","")+"&imprebajado"+i+"="+str_replace($("#impamort"+i).val(),",","");
                    cad=cad+"&codtipodeuda"+i+"="+$("#codtipodeuda"+ItemUpd).val()+"&categoria"+i+"="+$("#ct"+ItemUpd).val();
                    cad=cad+"&nrodocumento"+i+"="+$("#nrodoc"+ItemUpd).val()+"&anio"+i+"="+$("#anioz"+ItemUpd).val()+"&mes"+i+"="+$("#mesz"+ItemUpd).val();
                    cad=cad+"&codciclo"+i+"="+$("#codciclo"+ItemUpd).val()+"&"
                    cad=cad+"&lecant"+i+"="+$("#lecturaanterior"+index).val()+"&"
                    cad=cad+"&lecact"+i+"="+$("#lecturaultima"+index).val()+"&"
                    cad=cad+"&consumo"+i+"="+$("#consumo"+index).val()+"&"
                    cad=cad+"&lecturarec"+i+"="+lecreb+"&"
                    cad=cad+"&consumorec"+i+"="+consumocalcular+"&"
            }

            $.ajax({
                    url:'../ingresos/clase/proceso.php',
                    type:'POST',
                    async:true,
                    data:cad+"count="+$("#cont_facturacion").val(),
                    success:function(datos){
                     $("#tbrebajas tbody tr#tr_"+ItemUpd+" label.ImpRebajado").text( parseFloat($("#mrebajar").val()).toFixed(2));
                     $("#tbrebajas tbody tr#tr_"+ItemUpd+" label.lecturareclamo").text(lecreb);
                     $("#tbrebajas tbody tr#tr_"+ItemUpd+" label.consumoreclamo").text(consumocalcular);
                     CalcularTotalRebajado()
                     $( "#DivRebajar" ).dialog('close');
                    }
            }) 
    }
}
function CalcularTotalRebajado()
{
	var id= parseInt($("#tbrebajas tbody tr").length)
	var Total=0
	var Importe=0
    for(var i=1; i<=id; i++)
    {
    	Importe = $("#tbrebajas tbody tr#tr_"+i+" label.ImpRebajado").text()
    	Importe = str_replace(Importe,',','') ;
      	Total+= parseFloat(Importe);
    }

    $("#LblTotalReb").html(parseFloat(Total).toFixed(2))
}
function GuardarOK()
{
	Op=0;
	var Importe= $("#LblTotalReb").html()
	Importe = str_replace(Importe,',','') ;
    Importe= parseFloat(Importe);
	if(Importe==0)
	{
		$("#tabs").tabs({selected: 1});
		Msj($("#tbrebajas"),'Ingrese importes a rebajar')
		return false
	}
	/*var id= parseInt($("#tbrebajas tbody tr").length)
    for(var i=1; i<=id; i++)
    {
    	if( $("#tbrebajas tbody tr#tr_"+i+" label.ImpRebajado").text())
    }
	*/
	$.ajax({
                url: 'guardar.php?Op=' + Op,
                type: 'POST',
                async: true,
                data: $('#form1').serialize(), // + '&0form1_idusuario=<?= $IdUsuario ?>&3form1_fechareg=<?= $Fecha ?>',
                success: function(data) {
                    OperMensaje(data)
                    $("#Mensajes").html(data);
                    $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                    $("#Modificar").dialog("close");
                    $("#DivNose").dialog("close");
                    Buscar(Op);
                }
            })

}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php<?php echo $guardar;?>" enctype="multipart/form-data">
 <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">

   <tbody>

	<tr>
		<td colspan="5">
			<div id="tabs">
		      <ul>
		        <li style="font-size:10px"><a href="#tabs-1">Datos del Reclamo</a></li>
		        <li style="font-size:10px"><a href="#tabs-2">Meses Reclamados</a></li>
		     
		        </ul>
				      <div id="tabs-1" >
				      	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				      	 	<tr>
						       <td width="103" class="TitDetalle">Nro. Reclamo </td>
						       <td width="25" align="center" class="TitDetalle">:</td>
						       <td class="CampoDetalle">
									<input name="nroreclamo" type="text" id="Id" size="10" maxlength="10" value="<? echo $nroreclamo; ?>" class="inputtext"/></td>
						        <td colspan="2" class="CampoDetalle">&nbsp;</td>
						    </tr>
							<tr>
							  <td class="TitDetalle">Propietario</td>
							  <td align="center" class="TitDetalle">:</td>
							  <td class="CampoDetalle">
							    <input type="text" name="propietario" id="propietario" readonly="readonly" class="inputtext" value="<?php echo $row["propietario"]?>" style="width:400px;" />
							     <input type="hidden" name="nrodetalle" value="<?=$nrodetalle?>">
							     <input type="hidden" name="nroinscripcion" value="<?=$row['nroinscripcion']?>" id="nroinscripcion">
							     <input type="hidden" name="facturaciones" id="facturaciones" value="<?=$row['nroinscripcion']?>">
							     <input type="hidden" name="codciclo" id="codciclo" value="<?=$row['codciclo']?>">
							  </td>
							  <td colspan="2" class="CampoDetalle">&nbsp;</td>
							  </tr>
							<tr>
							  <td class="TitDetalle">Tipo de Reclamo</td>
							  <td align="center" class="TitDetalle">:</td>
							  <td class="CampoDetalle">
							    <input type="text" name="tiporeclamo" id="tiporeclamo" readonly="readonly" class="inputtext" value="<?php echo $tiporeclamo[$row["tiporeclamo"]]?>" style="width:400px;" />
							  </td>
							  <td colspan="2" class="CampoDetalle">&nbsp;</td>
							  </tr>
							<tr valign="top">
							  <td class="TitDetalle">Reclamo</td>
							  <td align="center" class="TitDetalle">:</td>
							  <td class="CampoDetalle"><textarea name="reclamo" id="reclamo" rows="2" readonly="readonly" style="width:400px;" ><?php echo $row["recl"]?></textarea></td>
							  <td colspan="2" class="CampoDetalle">&nbsp;</td>
							  </tr>
							<tr valign="top">
							  <td class="TitDetalle">Motivo</td>
							  <td align="center" class="TitDetalle">:</td>
							  <td class="CampoDetalle">
							    <textarea name="movito" id="movito" rows="5" readonly="readonly" style="width:400px;"><?php echo $row["glosa"]?></textarea>
							   </td>
							  <td colspan="2" class="CampoDetalle">&nbsp;</td>
							  </tr>
							<tr>
							  <td colspan="5" class="TitDetalle" style="height:5px"></td>
							  </tr>
							 <tr>
							   <td colspan="5" class="TitDetalle" style="background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px">Datos de la Atencion del Reclamo</td>
							   </tr>
							 <tr>
							   <td colspan="5" class="TitDetalle" style="height:5px"></td>
						     </tr>
						     <?php
							 	$sqlD = "select observacion,codinspector,fin_reclamo,fundado from  reclamos.detalle_reclamos where codsuc=? and nrodetalle=?";
								
								$consultaD = $conexion->prepare($sqlD);
								$consultaD->execute(array($codsuc,$nrodetalle));
								$rowD = $consultaD->fetch();
								
								if($rowD['fundado']==1)
								{
									$checked1="checked='checked'";
									$checked2="";
								}
								else
								{
									$checked2="checked='checked'";
									$checked1="";
								}
								
							 ?>
						         <tr>
							   <td class="TitDetalle">Resultado</td>
							   <td class="TitDetalle" align="center">:</td>
							   <td colspan="3" class="CampoDetalle">
							   	<div class="buttonset" style="display:inline">
							        <input type="radio" name="radio" id="fundado" value="fundado" onclick="javascript:$('#textFundado').val(1)" <?=$checked1?>/>
						            <label for="fundado">Fundado</label>
						            <input type="radio" name="radio" id="infundado" value="infundado"  onclick="javascript:$('#textFundado').val(2)" <?=$checked2?>/>
						            <label for="infundado">Infundado</label>
						               
							    </div>
							    <input type="hidden" name="textFundado" id="textFundado" value="<?php echo isset($rowD["fundado"])?$rowD["fundado"]:2?>" />
						               

						          
							      </td>	    	         
							   </tr>
							   <tr>
							   		<?php
							   		if($rowD['fin_reclamo']==1)
									{
										$checked1="checked='checked'";
										$checked2="";
									}
									else
									{
										$checked2="checked='checked'";
										$checked1="";
									}
							   		?>
								   <td class="TitDetalle">Fin del Reclamo</td>
								   <td class="TitDetalle" align="center">:</td>
								   <td colspan="3" class="CampoDetalle">
									   	<div class="buttonset" style="display:inline">
									        <input type="radio" name="finreclamo" id="finreclamo1" value="1"  <?=$checked1?>/>
								            <label for="finreclamo1">SI</label>
								            <input type="radio" name="finreclamo" id="finreclamo2" value="0"   <?=$checked2?>/>
								            <label for="finreclamo2">NO</label>
							              </div>
								   	</td>	    	         
							   </tr>

							 <tr>
							   <td class="TitDetalle">Se Resuelve</td>
							   <td class="TitDetalle" align="center">:</td>
							   <td colspan="3" class="CampoDetalle">
						             
							    </td>
							   </tr>
							 <tr>
							   <td class="TitDetalle" valign="top">&nbsp;</td>
							   <td class="TitDetalle" align="center" valign="top">&nbsp;</td>
							   <td colspan="3" class="CampoDetalle"><textarea name="glosa_resolucion" id="glosa_resolucion" class="inputtext" style="font-size:12px; width:400px;" rows="5" ><?php echo $rowD["observacion"]?></textarea></td>
							   </tr>
							 <tr>
							   <td class="TitDetalle">&nbsp;</td>
							   <td class="TitDetalle">&nbsp;</td>
							   <td colspan="3" class="CampoDetalle">&nbsp;
							   	<div>
							   	<?php
								  	$contador=0;
								  	if($nroreclamo!="")
									{
										$sqlmeses  = "select meses.nrofacturacion,upper(doc.abreviado),meses.nrodocumento,
													 meses.anio,meses.mes,upper(tipd.descripcion),meses.categoria,meses.codtipodeuda,
													 sum(meses.imprebajado),sum(meses.imporiginal),meses.lecturaanterior,meses.lecturaultima,meses.consumo,
													 meses.lecturareclamo,meses.consumoreclamo
													  from reclamos.mesesreclamados as meses
													 inner join reglasnegocio.documentos as doc on(meses.coddocumento=doc.coddocumento) AND (meses.codsuc=doc.codsuc)
													 inner join public.tipodeuda as tipd on(meses.codtipodeuda=tipd.codtipodeuda)
													 where meses.nroreclamo=? and meses.codemp=1 and meses.codsuc=?
													 group by meses.codtipodeuda,meses.nrofacturacion,
													 doc.abreviado,meses.nrodocumento,meses.anio,meses.mes,meses.categoria,tipd.descripcion,meses.lecturaanterior,
													 meses.lecturaultima,meses.consumo,meses.lecturareclamo,meses.consumoreclamo
													 ORDER BY meses.nrofacturacion";

										$consulta_meses = $conexion->prepare($sqlmeses);
										$consulta_meses->execute(array($nroreclamo,$codsuc));
										$items_meses = $consulta_meses->fetchAll();
										foreach($items_meses as $row_meses)
										{
											$contador++;
										  ?>
								
								    	<input type='hidden' name='nrofacturacion<?=$contador?>' id='nrofacturacion<?=$contador?>' value='<?php echo $row_meses[0]?>' />
										<input type='hidden' name='codtipodeuda<?=$contador?>' id='codtipodeuda<?=$contador?>' value='<?php echo $row_meses[7]?>' />
								        <input type='hidden' name='ct<?=$contador?>' id='ct<?=$contador?>' value='<?php echo $row_meses[6]?>' />
										<input type='hidden' name='impz<?=$contador?>' id='impz<?=$contador?>' value='<?php echo $row_meses[8]?>' />
										<input type='hidden' name='lecturaanterior<?=$contador?>' id='lecturaanterior<?=$contador?>' value='<?php echo $row_meses['lecturaanterior']?>' />
										<input type='hidden' name='lecturaultima<?=$contador?>' id='lecturaultima<?=$contador?>' value='<?php echo $row_meses['lecturaultima']?>' />
										<input type='hidden' name='consumo<?=$contador?>' id='consumo<?=$contador?>' value='<?php echo $row_meses['consumo']?>' />
										<input type='hidden' name='lecturareclamo<?=$contador?>' id='lecturareclamo<?=$contador?>' value='<?php echo $row_meses['lecturareclamo']?>' />
										<input type='hidden' name='consumoreclamo<?=$contador?>' id='consumoreclamo<?=$contador?>' value='<?php echo $row_meses['consumoreclamo']?>' />
									
								   <?php
										}
									}
								   ?>
						</div>
					   </td>
					   </tr>
		      	</table>
		      </div>

		      	<div id="tabs-2" >
		      	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		      	 	<tr>
		      	 		<td>
		      	 			 <div id="DivNose" title="Facturaciones a Realizar la Rebajas"  >
    <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbrebajas" rules="all" >
		<thead class="ui-widget-header" >
		  <tr align="center">
		    <th width="11%" >Nro. Doc.</th>
            <th width="12%" >A&ntilde;o y Mes</th>
            <th width="11%" >Categoria</th>
            <th width="11%" >Lect. Ant.</th>
            <th width="11%" >Lect. Act.</th>
            <th width="11%" >Consumo</th>
            <th width="11%" >Lect. Rec.</th>
            <th width="11%" >Con. Rec.</th>
		    <th width="10%" >Imp. Original</th>
		    <th width="10%" >Imp. Rebajado</th>
		    <th width="2%" >&nbsp;</th>
		  </tr>
		</thead>
		<tbody>
		  <?php
		  	$contador=0;
		  	if($nroreclamo!="")
			{
				$sqlmeses  = "select meses.nrofacturacion,upper(doc.abreviado),meses.nrodocumento,
							 meses.anio,meses.mes,upper(tipd.descripcion),meses.categoria,meses.codtipodeuda,
							 sum(meses.imprebajado),sum(meses.imporiginal) ,meses.lecturaanterior,meses.lecturaultima,meses.consumo,
							 meses.lecturareclamo,meses.consumoreclamo,meses.seriedocumento
							 from reclamos.mesesreclamados as meses
							 inner join reglasnegocio.documentos as doc on(meses.coddocumento=doc.coddocumento) AND (meses.codsuc=doc.codsuc)
							 inner join public.tipodeuda as tipd on(meses.codtipodeuda=tipd.codtipodeuda)
							 where meses.nroreclamo=? and meses.codemp=1 and meses.codsuc=?
							 group by meses.codtipodeuda,meses.nrofacturacion,
							 doc.abreviado,meses.nrodocumento,meses.anio,meses.mes,meses.categoria,tipd.descripcion,
							 meses.lecturaanterior,meses.lecturaultima,meses.consumo,meses.lecturareclamo,
							 meses.consumoreclamo,meses.seriedocumento
							 ORDER BY meses.nrofacturacion";

				$consulta_meses = $conexion->prepare($sqlmeses);
				$consulta_meses->execute(array($nroreclamo,$codsuc));
				$items_meses = $consulta_meses->fetchAll();
				//var_dump($consulta_meses->errorInfo());
				$TotalFac=0;
				$TotalReb=0;
				foreach($items_meses as $row_meses)
				{
					$contador++;
					
					$cat="";
					if($row_meses[6]==0){$cat="FACTURACION";}
					if($row_meses[6]==1){$cat="SALDO";}
					if($row_meses[6]==2){$cat="SALDO REFINANCIADO";}
					if($row_meses[6]==3){$cat="PRESTACION DE SERVICIOS EN CAJA";}

					$TotalFac+=$row_meses[9];
					$TotalReb+=$row_meses[8];
		  ?>
		  <tr style='background-color:#FFFFFF' align="center" id='tr_<?=$contador?>'>
		   
		    <td align="center" ><?php echo $row_meses[1]?>:<?php echo $row_meses['seriedocumento']?>-<?php echo $row_meses[2]?></td>
		    <td align="center" ><?php echo $row_meses[3]."-".$meses[$row_meses[4]]?></td>
		    <td align="center" ><?php echo $cat?></td>
		    <td align="right" class='select'><?=number_format($row_meses['lecturaanterior'])?></td>
          <td align="right" class='select'><?=number_format($row_meses['lecturaultima'])?></td>
           <td align="right" class='select'><?=number_format($row_meses['consumo'])?></td>
           <td align="right" class='select'><label class="lecturareclamo"><?=number_format($row_meses['lecturareclamo'])?></label></td>
           <td align="right" class='select'><label class="consumoreclamo"><?=number_format($row_meses['consumoreclamo'])?></label></td>
		    <td align="right" ><?=number_format($row_meses[9],2)?></td>
		    <td align="right" ><label class="ImpRebajado"><?=number_format($row_meses[8],2)?></label></td>
		    <td align="center" >
		    	<span class='icono-icon-add' title="Realizar Rebaja de Facturacion" onclick="cargar_from_detalle_facturacion(<?=$row['nroinscripcion']?>,<?=$row_meses['nrofacturacion']?>,<?=$row_meses['categoria']?>,<?=$row_meses['codtipodeuda']?>,<?=$contador?>)" ></span>
		    	
		    </td>
		    </tr>
		   <?php
				}
			}
		   ?>
		</tbody>
		<tfoot class="ui-widget-header">
			<tr>
				<td colspan="8">&nbsp;</td>
				<td align="right"><label id="LblTotalFac"><?=number_format($TotalFac,2)?></label></td>
				<td align="right"><label id="LblTotalReb"><?=number_format($TotalReb,2)?></label></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
	</table>
</div>
		      	 		</td></tr>
		      	 </table>
		      </div>
		      </div>

		</td>
	</tr>

	

          </tbody>

    </table>
    
 </form>

<div id="DivRebajar" title="Ver Detalle de Facturacion para Realizar la Rebaja"  >
    <div id="div_detalle_facturacion"></div>
</div>
</div>
<script type="text/javascript">$("#facturaciones").val(<?=$contador?>)</script>