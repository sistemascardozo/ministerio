<?php
    include("../ingresos/clase/clsproceso.php");
    include("../../../../objetos/clsFunciones.php");
    $objFunciones = new clsFunciones();
    $conexion->beginTransaction();

    $codsuc     = $_SESSION['IdSucursal'];
    $nroreclamo = $_POST["nroreclamo"];
    $nrodetalle = $_POST["nrodetalle"];
    $fin_reclamo = $_POST["finreclamo"];
    $textFundado = $_POST["textFundado"];
    $glosa_resolucion = strtoupper($_POST["glosa_resolucion"]);

    $fecha_Fin =  $objFunciones->CodFecha($objFunciones->FechaServer());

    $fecha = "";
    if ($fin_reclamo == 1) {
        $fecha = ",fecha_finalizacion='" . $fecha_Fin . "'";
    }

    $upd = "update reclamos.detalle_reclamos set observacion=?,fin_reclamo=?,fundado=? " . $fecha . " where codsuc=? and nrodetalle=?";
    $resultUpd = $conexion->prepare($upd);
    $resultUpd->execute(array($glosa_resolucion, $fin_reclamo, $textFundado, $codsuc, $nrodetalle));
    if ($resultUpd->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
    $updR = "update reclamos.reclamos set fin_reclamo=?,fundado=? " . $fecha . " where codsuc=? and nroreclamo=?";
    $consultaR = $conexion->prepare($updR);
    $consultaR->execute(array($fin_reclamo, $textFundado, $codsuc, $nroreclamo));
    if ($consultaR->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }

		
     //SI EL RECLAMO SE FINALIZA

    if($fin_reclamo==1)
    {
        if($textFundado==2) //SI ES INFUNDADO SE LIBERAM LAS FACTURACIONES INVOLUCRADAS
        {   
            $sqlM = "SELECT codciclo,nrofacturacion from reclamos.mesesreclamados
                    where nroreclamo=:nroreclamo and codsuc=:codsuc
                    GROUP BY codciclo,nrofacturacion";
        
            //NO LIBERAR
            /*$consultaM = $conexion->prepare($sqlM);
            $consultaM->execute(array(":nroreclamo"=>$nroreclamo,":codsuc"=>$codsuc));
            if ($consultaM->errorCode() != '00000') 
            {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                die(2);
            }
            $itemsM = $consultaM->fetchAll();
            
            foreach($itemsM as $rowM)
            {
                
               //PONER EN SIN RECLAMO LA FACTURACION
                $Sql="UPDATE facturacion.cabfacturacion  SET enreclamo = 0
                        WHERE codemp = :codemp AND  codsuc = :codsuc AND
                              codciclo = :codciclo AND nrofacturacion = :nrofacturacion
                              AND nroinscripcion = :nroinscripcion";
                $result = $conexion->prepare($Sql);
                $result->execute(array(
                    ":codemp" => 1,
                    ":codsuc" => $codsuc,
                    ":codciclo" => $rowM['codciclo'],
                    ":nrofacturacion" => $rowM['nrofacturacion'],
                    ":nroinscripcion" => $nroinscripcion

                    ));
                if ($result->errorCode() != '00000') 
                {
                    $conexion->rollBack();
                    $mensaje = "Error reclamos";
                    die(2);
                } 
            }
            */
        }
        else//SI ES FUNDADO
        {

            //PONER EN RECLAMO LA FACTURACION
            $contador = $_POST["facturaciones"];
            $count = $_SESSION["oreclamos"]->item;
           
            for ($j = 1; $j <= $contador; $j++) 
            {
                if (isset($_POST["nrofacturacion" . $j])) 
                {
                   
                   // echo $_POST["nrofacturacion".$j]."<br>";
                    for ($i = 0; $i < $count; $i++) 
                    {

                        if (isset($_SESSION["oreclamos"]->nrofacturacion[$i])) 
                        {
                           
                            if ($_POST["nrofacturacion" . $j] == $_SESSION["oreclamos"]->nrofacturacion[$i] &&
                                    $_POST["codtipodeuda" . $j] == $_SESSION["oreclamos"]->codtipodeuda[$i] &&
                                    $_POST["ct" . $j] == $_SESSION["oreclamos"]->categoria[$i]) 
                            {
                               $inst = "UPDATE reclamos.mesesreclamados SET imprebajado=:imprebajado,
                                        lecturareclamo=:lecturareclamo,consumoreclamo=:consumoreclamo
                                        WHERE codemp=:codemp AND codsuc=:codsuc AND nroreclamo=:nroreclamo
                                        AND nrofacturacion=:nrofacturacion AND codconcepto=:codconcepto";
                        
                                $resultI = $conexion->prepare($inst);
                                $resultI->execute(array(
                                    ":imprebajado" => $_SESSION["oreclamos"]->imprebajado[$i],
                                    ":lecturareclamo"=>$_SESSION["oreclamos"]->lecturarec[$i],
                                    ":consumoreclamo"=>$_SESSION["oreclamos"]->consumorec[$i],
                                    ":codemp" => 1,
                                    ":codsuc" => $codsuc,
                                    ":nroreclamo" => $nroreclamo,
                                    ":nrofacturacion" => $_SESSION["oreclamos"]->nrofacturacion[$i],
                                    ":codconcepto" => $_SESSION["oreclamos"]->codconcepto[$i]
                                   ));
                                if ($resultI->errorCode() != '00000') 
                                {
                                    $conexion->rollBack();
                                    $mensaje = "Error reclamos";
                                    die(2);
                                }
                                

            
                                //echo $inst."<br>";
                            }
                        }
                    }
                }
            }
         //PONER EN RECLAMO LA FACTURACION
        }
    }

 

//die('contador-->'.$contador);
       // die('xxx');
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;


?>
