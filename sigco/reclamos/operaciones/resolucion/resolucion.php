<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	
	
	$objFunciones = new clsFunciones();
		
	$codsuc			= $_SESSION['IdSucursal'];
	$nroreclamo 	= $_POST["nroreclamo"];
	$nrodetalle_org	= $_POST["nrodetalle"];
	$nroorigen		= $_POST["nroorigen"];
	$fecha 			= $objFunciones->CodFecha($objFunciones->FechaServer());
	$idusuario		= $_SESSION["id_user"];
	$codarea 		= $_SESSION['idarea'];
	$conexion->beginTransaction();
	$updR = "update reclamos.reclamos set codestadoreclamo=8 where codsuc=? and nroreclamo=?";	
	$consultaR = $conexion->prepare($updR);
	$consultaR->execute(array($codsuc,$nroreclamo));

	if ($consultaR->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
	$updD = "update reclamos.detalle_reclamos set codestadoreclamo=8 where codsuc=? and nroreclamo=? ";
	$consultaD = $conexion->prepare($updD);
	$consultaD->execute(array($codsuc,$nroreclamo));
	if ($consultaD->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
	


        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    
	
?>
