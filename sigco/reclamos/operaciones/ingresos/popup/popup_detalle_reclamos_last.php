<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");

	$objFunciones = new clsFunciones();
	
	$codsuc 	= $_GET["codsuc"];
	$nroreclamo = $_GET["nroreclamo"];
	
	$sql = "select c.propietario,r.nroinscripcion,m.descripcion as medio_reclamo,t.descripcion as tipo_parentesco,r.reclamante,
			r.fechareg,r.creador,r.tiporeclamo,cr.descripcion as concepto,r.glosa,r.fecha_finalizacion,cr.tiempo,r.fin_reclamo
			from reclamos.reclamos as r 
			inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
			inner join reclamos.medioreclamo as m on(r.codmedio=m.codmedio)
			inner join public.tipoparentesco as t on(r.codtipoparentesco=t.codtipoparentesco)
			inner join reclamos.conceptosreclamos as cr on(r.codconcepto=cr.codconcepto)
			where r.codsuc=? and r.nroreclamo=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroreclamo));
	$row = $consulta->fetch();
	
	$fecha_fin			= $row["fecha_finalizacion"];
	$fecha_finalizacion	= $objFunciones->DecFecha($fecha_fin);
	
	if($row["fin_reclamo"]==0){$fecha_fin=date("Y-m-d");$fecha_finalizacion="SIN FECHA DE FINALIZACION";}
	$dias = $objFunciones->calcular_diferencias_dias($row["fechareg"],$fecha_fin);
	
	$color="";
	if($dias>$row["tiempo"]){$color="style='color:#F00; font-weight:bold'";}
?>
<style>
	.myTable{
		/*padding:4px;*/
		
		-moz-border-radius: 25px 10px / 10px 25px;
		border-radius: 25px 10px / 10px 25px;
		border:1px solid #000;
		cursor: pointer;
		padding: 2px;
		margin:1px;
		
		background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(226,222,222,1) 100%); /* FF3.6+ */
	}
	.estiloX{
		background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px
	}
	.myTableX{
		background-color:#0099CC; border: 1 #000000 solid; color:#FFF; padding:4px; font-weight:bold;
	}
	.estiloz{
		font-weight:bold; color:#F00;
	}
	body{
		background-image:url(../../../../../css/images/ui-bg_flat_55_fbec88_40x100.png);
	}
</style>
<script type="text/javascript" src="../../../../../js/jquery-1.8.3.js" language="JavaScript"></script>
<script type="text/javascript" src="../../../../../js/jquery-ui-1.9.2.custom.min.js" language="JavaScript"></script>

<link href="../../../../../css/css_base.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../../../../../css/base/jquery.ui.all.css" />

<script src="../../../../../js/js_mantenimiento.js" type="text/javascript"></script>
<script src="../../../../../js/jquery.bgiframe-2.1.2.js"></script>
<script>
$(document).ready(function(){
	
	$( "#dialog:ui-dialog" ).dialog( "destroy" );

	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 450,
		width: 650,
		modal: true,
		resizable:false,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {

		}
	});
})
function cargar_observacion_reclamos(nrodetalle)
{
	cargar_ajax_observacion_reclamos(nrodetalle)
	$("#dialog-form").dialog("open")
}
function cargar_ajax_observacion_reclamos(nrodetalle)
{
	$.ajax({
		 url:'../ajax/ver_detalle_reclamo.php',
		 type:'POST',
		 async:true,
		 data:'nrodetalle='+nrodetalle+'&codsuc=<?=$codsuc?>',
		 success:function(datos){
			$("#div_detalle_reclamos").html(datos)
		 }
	}) 
}
</script>
<table width="900" border="0" align="center">
  <tr>
    <td class="myTable">
    <fieldset class="myTable" style="background-color:#FFF">
        <table width="100%" border="0" >
          <tr>
            <td width="15%">Nro. Reclamo</td>
            <td width="3%" align="center">:</td>
            <td width="22%"><?=$nroreclamo?></td>
            <td width="13%" align="right">Nro. Inscripcion</td>
            <td width="4%" align="center">:</td>
            <td width="43%"><?=$row["nroinscripcion"]?></td>
          </tr>
          <tr>
            <td>Propietario</td>
            <td align="center">:</td>
            <td colspan="4"><?=strtoupper($row["propietario"])?></td>
          </tr>
          <tr>
            <td>Medio de Reclamo</td>
            <td align="center">:</td>
            <td><?=strtoupper($row["medio_reclamo"])?></td>
            <td align="right">Parentesco</td>
            <td align="center">:</td>
            <td><?=strtoupper($row["tipo_parentesco"])?></td>
          </tr>
          <tr>
            <td>Reclamante</td>
            <td align="center">:</td>
            <td colspan="4"><?=strtoupper($row["reclamante"])?></td>
          </tr>
          <tr>
            <td>Fecha de Registro</td>
            <td align="center">:</td>
            <td><?=$objFunciones->DecFecha($row["fechareg"])?></td>
            <td align="right">Registrado por</td>
            <td align="center">:</td>
            <td><?=$objFunciones->login($row["creador"])?></td>
          </tr>
          <tr>
            <td>Fecha de Finalizacion</td>
            <td align="center">:</td>
            <td><?=$fecha_finalizacion?></td>
            <td align="right">Dias Procesados</td>
            <td align="center">:</td>
            <td>
       	    <label <?=$color?> ><?=$dias?></label></td>
          </tr>
          <tr>
            <td>Tipo de Reclamo</td>
            <td align="center">:</td>
            <td colspan="4"><?=$tiporeclamo[$row["tiporeclamo"]]?></td>
          </tr>
          <tr>
            <td>Reclamo</td>
            <td align="center">:</td>
            <td colspan="4"><?=strtoupper($row["concepto"])?></td>
          </tr>
          <tr valign="top">
            <td>Argumento</td>
            <td align="center">:</td>
            <td colspan="4"><?=strtoupper($row["glosa"])?></td>
          </tr>
          <tr>
            <td colspan="6" height="5px"></td>
          </tr>
          <tr>
            <td colspan="6" style="padding:4px;" >MESES RECLAMADOS</td>
          </tr>
          <tr>
            <td colspan="6">
              <div style="overflow:auto; height:150px; ">
              	<table width="60%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0">
                  <tr class="myTableX" align="center">
                    <td width="15%">A&ntilde;o </td>
                    <td width="15%">Mes</td>
                    <td width="15%">Importe Original</td>
                    <td width="15%">Importe Rebajado</td>
                  </tr>
                  <?php
				  	$sqlM = "select anio,mes,sum(imporiginal) as importe_original,sum(imprebajado) as importe_rebajado
							 from mesesreclamados 
							 where codsuc=? and nroreclamo=? group by anio,mes order by anio,mes";
							 
					$consultaM = $conexion->prepare($sqlM);
                    $consultaM->execute(array($codsuc,$nroreclamo));
                    $itemsM = $consultaM->fetchAll();
					
					foreach($itemsM as $rowM)
					{
				  ?>
                  <tr>
                    <td align="center"><?=$rowM["anio"]?></td>
                    <td align="center"><?=$meses[$rowM["mes"]]?></td>
                    <td align="right"><?=number_format($rowM["importe_original"],2)?></td>
                    <td align="right"><?=number_format($rowM["importe_rebajado"],2)?></td>
                  </tr>
                  <?php } ?>
                </table>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="6" height="5px"></td>
          </tr>
          <tr>
            <td colspan="6" style="padding:4px;" >DETALLE DE LOS MONTOS A REBAJAR</td>
          </tr>
          <tr>
            <td colspan="6">
              <div style="overflow:auto; height:300px; ">
                <table width="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0">
                  <tr class="myTableX" align="center">
                    <td width="56%">Concepto</td>
                    <td width="23%">Imp. Rebajado</td>
                    <td width="21%">Imp. Original</td>
                  </tr>
                  <?php 
                    $cat=100;
                    $codt=100;
                    
                    $sqlD = "select c.descripcion as concepto,m.categoria,m.codtipodeuda,t.descripcion,m.imprebajado,m.imporiginal
                             from mesesreclamados as m
                             inner join facturacion.conceptos as c on(m.codemp=c.codemp and m.codsuc=c.codsuc and m.codconcepto=c.codconcepto)
                             inner join public.tipodeuda as t on(m.codtipodeuda=t.codtipodeuda)
                             where m.codsuc=? and m.nroreclamo=?";
                    
                    $consultaD = $conexion->prepare($sqlD);
                    $consultaD->execute(array($codsuc,$nroreclamo));
                    $itemsD = $consultaD->fetchAll();
                    
                    foreach($itemsD as $rowD)
                    {
                        if($cat!=$rowD["categoria"])
                        {
                            echo "<tr>";
                            echo "<td colspan='3' class='estiloz' >CATEGORIA ==> ".$categoria[$rowD["categoria"]]."</td>";
                            echo "</tr>";
                        }
                        if($codt!=$rowD["codtipodeuda"])
                        {
                            echo "<tr>";
                            echo "<td colspan='3' class='estiloz' >&nbsp;&nbsp;&nbsp;TIPO DE DEUDA ==> ".strtoupper($rowD["descripcion"])."</td>";
                            echo "</tr>";
                        }
                  ?>
                  <tr>
                    <td><?=strtoupper($rowD["concepto"])?></td>
                    <td align="right"><?=number_format($rowD["imprebajado"],2)?></td>
                    <td align="right"><?=number_format($rowD["imporiginal"],2)?></td>
                  </tr>
                  <?php 
                        $cat=$rowD["categoria"];
                        $codt=$rowD["codtipodeuda"];
                    }
                  ?>
                </table>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="6" height="5px"></td>
          </tr>
          <tr>
            <td colspan="6" style="padding:4px;" >SEGUIMIENTO DEL RECLAMO</td>
          </tr>
          <tr>
            <td colspan="6" height="5px"></td>
          </tr>
          <tr>
            <td colspan="6">
            <table width="100%" border="0s" cellspacing="0" cellpadding="0">
              <tr>
              <?php
			  	$count=0;
				
			  	$sqlDr = "select d.nrodetalle,a.descripcion as area,d.codusu,d.fechareg
						  from detalle_reclamos as d
						  inner join reglasnegocio.areas as a on(d.codemp=a.codemp and d.codsuc=a.codsuc and d.codarea=a.codarea)
						  where d.codsuc=? and d.nroreclamo=? order by d.nrodetalle asc ";
				
				$consultaDr = $conexion->prepare($sqlDr);
				$consultaDr->execute(array($codsuc,$nroreclamo));
				$itemsDr = $consultaDr->fetchAll();
				
				foreach($itemsDr as $rowDr)
				{
			  ?>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center"><img src="../../../../../images/iconos/home.png" width="128" height="128" style="cursor:pointer" title="Ver Detalle del Reclamo en la Derivacion" onclick="cargar_observacion_reclamos(<?=$rowDr["nrodetalle"]?>);" /></td>
                  </tr>
                  <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="22%">Area</td>
                        <td width="4%" align="center">:</td>
                        <td width="74%"><?=strtoupper($rowDr["area"])?></td>
                      </tr>
                      <tr>
                        <td>Usuario </td>
                        <td align="center">:</td>
                        <td><?=$objFunciones->login($rowDr["codusu"])?></td>
                      </tr>
                      <tr>
                        <td>Fecha Reg.</td>
                        <td align="center">:</td>
                        <td><?=$objFunciones->DecFecha($rowDr["fechareg"])?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              <?php $count++; if($count==3){echo "</tr>";} } ?>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
        </table>
    </fieldset>
    </td>
  </tr>
</table>
<div id="dialog-form" title="Detalle del Reclamo"  >
    <div id="div_detalle_reclamos"></div>
</div>