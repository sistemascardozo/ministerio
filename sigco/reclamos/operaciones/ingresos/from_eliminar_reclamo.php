<?php 

	include("../../../../config.php");
	$codsuc 	= $_POST["codsuc"];
	$nroreclamo = $_POST["nroreclamo"];

	$sql1 = "select cab.enreclamo, 
			cab.nroreclamo, 
			cab.nrofacturacion, 
			cab.nroinscripcion
			from facturacion.cabfacturacion as cab
			where cab.codsuc=? AND cab.nroreclamo=?";
	$consulta0 = $conexion->prepare($sql1);
	$consulta0->execute(array($codsuc,$nroreclamo));
	$row = $consulta0->fetch();

	if($row = $consulta0->fetch()):
		$sql2 = "UPDATE facturacion.cabfacturacion SET enreclamo = 0
			WHERE codsuc=:codsuc AND nroreclamo=:nroreclamo AND nroinscripcion=:nroinscripcion AND nrofacturacion = :nrofacturacion";	
		$consulta1 = $conexion->prepare($sql2);
		$consulta1->execute(array(":codsuc" => $codsuc,":nroreclamo" => $nroreclamo,":nroinscripcion" => $row['nroinscripcion'],":nrofacturacion" => $row['nrofacturacion']));
	
	endif;
		
	$sql = "UPDATE reclamos.reclamos SET codestadoreclamo = 10
		WHERE codsuc=:codsuc AND nroreclamo=:nroreclamo";	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":codsuc" => $codsuc,":nroreclamo" => $nroreclamo));

	if ($consulta->errorCode() != '00000'):

        $conexion->rollBack();
        $mensaje = "Error en la elimancion del reclamo";
        $op = "1";
	else:

		$op = "0";	
		$mensaje = "Se ha pasado a eliminar el reclamo";
	endif;

	// Modificando el detalle del reclamo
	$sql = "UPDATE reclamos.detalle_reclamos SET codestadoreclamo = 10
		WHERE codsuc=:codsuc AND nroreclamo=:nroreclamo";	
	$consulta2 = $conexion->prepare($sql);
	$consulta2->execute(array(":codsuc" => $codsuc,":nroreclamo" => $nroreclamo));

	if ($consulta2->errorCode() != '00000'):
		
        $conexion->rollBack();
        $mensaje = "Error al intetar eliminar el reclamo";
        $op = "1";
	else:

		$op = "0";	
		$mensaje = "Se ha pasado a eliminar el reclamo";
	endif;

	echo $op."|".$mensaje;
	
?>
