<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	unset($_SESSION["oreclamos"]);	
	
	include("clase/clsproceso.php");	
	include("../../../../objetos/clsDrop.php");
	
	$Op 		= $_POST["Op"];
	$codsuc  	= $_SESSION["IdSucursal"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$mensaje	= '';
	$objDrop = new clsDrop();	
	$entrega_cartilla=1;
	$hora_inspeccion='08:30';
	$hora_inspeccion2='16:30';
	$correlativo = $objDrop->setCorrelativosVarios(20, $codsuc, "SELECT", 0);
	$fecha = date("d/m/Y");
	
	$VerUbigueo = "SELECT codubigeo FROM admin.sucursales WHERE codemp=1 AND codsuc='".$codsuc."' ";
	$res = $conexion->query($VerUbigueo)->fetch();
	$IdUbigeo = $res[0];

	if($Id != '')
	{
		$sql = "SELECT r.nroinscripcion,c.propietario,r.codtipoparentesco,r.codmedio,r.reclamante,r.codtipodocumento,
						r.nrodocumento,r.codsector,	r.codcalle,r.telefono,r.nrocalle,r.codubigeo,r.direccion1,r.nrocalle1,
						r.manzana1,r.lote1,r.tiporeclamo,r.codconcepto,r.glosa,r.pruebas, r.doc_referencia,r.monto_reclamo,
						r.car,r.celular,r.codpostal,r.referencia,r.entrega_cartilla,r.contratacion_m,
						 ".$objDrop->FormatFecha('r.fecha_citacion')." as fecha_citacion,r.hora_citacion,
						 ".$objDrop->FormatFecha('r.fecha_inspeccion')." as fecha_inspeccion,hora_inspeccion,
						 ".$objDrop->FormatFecha('r.fecha_notificacion')."  as fecha_notificacion,c.codzona,r.mensaje,
						 r.hora_inspeccion2,r.fechaemision,r.correlativo,c.codantiguo
				from reclamos.reclamos as r 
				inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
				where r.codemp=1 and r.codsuc=".$codsuc." and r.nroreclamo=".$Id;
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array());
		
		$row = $consulta->fetch();
		$mensaje = $row['mensaje'];
		$hora_inspeccion=$row['hora_inspeccion'];
		$hora_inspeccion2=$row['hora_inspeccion2'];
		$entrega_cartilla= $row['entrega_cartilla'];
		$fecha = $objDrop->DecFecha($row['fechaemision']);
		$correlativo= $row['correlativo'];
		
		$IdUbigeo = $row['codubigeo'];
		
		$sqlmes = "SELECT * from reclamos.mesesreclamados where codemp=1 and codsuc=? and nroreclamo=?";
		
		$consulta_mes = $conexion->prepare($sqlmes);
		$consulta_mes->execute(array($codsuc,$Id));
		$items_mes = $consulta_mes->fetchAll();
		
		//unset($_SESSION["oreclamos"]);
		foreach($items_mes as $row_mes)
		{
		
			$_SESSION["oreclamos"]->agregarvaloresxx(
						$row_mes["nrofacturacion"],
						$row_mes["codconcepto"],
						$row_mes["imporiginal"],
                        $row_mes["imprebajado"],
                        $row_mes["codtipodeuda"],
                        $row_mes["categoria"],
                        $row_mes["nrodocumento"],
                        $row_mes["anio"],
                        $row_mes["mes"],
                        $row_mes["codciclo"],
                        $row_mes["lecturaanterior"],
                        $row_mes["lecturaultima"],
                        $row_mes["consumo"],
                        $row_mes["lecturareclamo"],
                        $row_mes["consumoreclamo"],
                        $row_mes['seriedocumento'],
                        $row_mes['item']);

		}
	}
	
?>
<script>
	var nDest 	= 0;
	var nDestC	= 0;
	var cont 	= 0; 	
	var urldir 	='<?php echo $_SESSION['urldir'];?>';
	var codsuc	= <?php echo $codsuc?>;
 if('<?=$Op?>'==0)
  	$("#fecha_notificacion").val(addToDate('<?=date("d-m-Y")?>',30))
	
$(document).bind('keydown',function(e){
 //alert($(":focus").attr('id'))
  if ( e.which == 113 )
  {
    buscar_usuarios();
      return false;
  };
  if ( e.which == 114 )
  {
      buscar_tipo_reclamos()
      return false;
  };
 
   
  });
$(document).bind('keyup',function(e){
 if ( e.which == 120 )
	 ValidarForm('<?=$Op?>');
	return false;
  	
  });

</script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/required.js" language="JavaScript"></script>
<script type="text/javascript" src="js_reclamos.js" language="JavaScript"></script>
<style>
	.estiloX{
		background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px
	}
	.myTable{
		background-color:#0099CC; border: 1 #000000 solid;
	}
</style>

<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    <tbody>
  <tr>
    <td colspan="6" class="TitDetalle" style="height:5px"></td>
  </tr>
	<tr>
	  <td colspan="7" class="TitDetalle">
	    <div id="tabs">
	      <ul>
	        <li style="font-size:10px"><a href="#tabs-4">Datos del Reclamo</a></li>
	        <li style="font-size:10px"><a href="#tabs-3">Tipo de Reclamo</a></li>
	        <li style="font-size:10px"><a href="#tabs-2">Meses Reclamados</a></li>
                <li style="font-size:10px"><a href="#tabs-5">Datos Adicionales</a></li>
	        </ul>
	      <div id="tabs-4" >
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="100">Nro. Reclamo</td>
	            <td width="10" align="center">:</td>
	            <td><span class="CampoDetalle">
	              <input name="nroreclamo" type="hidden" id="nroreclamo" value="<? echo $Id; ?>" class="inputtext"/>
	              <input name="correlativo" type="text" id="correlativo" size="10" maxlength="10" value="<? echo $correlativo; ?>" class="inputtext" readonly="readonly"/>
	             <div <?php if($Op!=0 ) echo "style='display:none;'"; else echo "style='display:inline;'";  ?>>
	            <input type="checkbox"   id="chkanterior" onclick="CambiarAnterior(this)"/>
	            <input type="hidden" id="tempanterior" value="0" name="tempanterior">
	            <label> Anterior</label>
	            </div>
	            </span></td>
    			  <td class="TitDetalle" align="right"><span class="CampoDetalle">Fecha de Emsion </span></td>
				  <td width="10" align="center" class="TitDetalle">:</td>
				  <td colspan="10" class="CampoDetalle">
				    <input name="fechaemision" type="text" id="fechaemision" maxlength="10" class="inputtext fecha" value="<?=$fecha?>" style="width:80px;"/>
				   
				</td>
				</tr>
	          <tr>
	            <td>Propietario</td>
	            <td align="center">:</td>
	            <td colspan="4">
	              <input type="hidden" name="nroinscripcion" id="nroinscripcion" size="10" class="inputtext"  value="<?php echo $row["nroinscripcion"]?>" />
	              <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" value="<?php echo $row["codantiguo"]?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:75px;"/>
	              <span class="MljSoft-icon-buscar" title="Buscar Usuario" onclick="buscar_usuarios();"></span>
	              <input class="inputtext" name="propietario" type="text" id="propietario" maxlength="200" readonly="readonly" value="<?php echo $row["propietario"]?>" style="width:400px;" />
	              </td>
	            </tr>
	          <tr>
	            <td>Parentesco</td>
	            <td align="center">:</td>
	            <td><?php $objDrop->drop_tipo_parentesco($row["codtipoparentesco"]); ?></td>
	            <td align="right">Medio de Reclamo</td>
	            <td align="center">:</td>
	            <td><?php $objDrop->drop_medio_reclamo($row["codmedio"]); ?></td>
	          </tr>
	          <tr>
	            <td>Reclamante</td>
	            <td align="center">:</td>
	            <td colspan="4"><label>
	              <input type="text" name="reclamante" id="reclamante" class="inputtext" value="<?php echo $row["reclamante"]?>" style="width:450px;" />
	              </label></td>
	            </tr>
	          <tr>
	            <td>Documento</td>
	            <td align="center">:</td>
	            <td><?php $objDrop->drop_tipodocumento($row["codtipodocumento"]); ?></td>
	            <td align="right">Nro. Documento</td>
	            <td align="center">:</td>
	            <td><input type="text" name="nrodocumento" id="nrodocumento" size="10" maxlength="11" class="inputtext" onkeypress="return permite(event,'num');" value="<?php echo $row["nrodocumento"]?>" /></td>
	            </tr>
	          <tr>
	            <td>Sector</td>
	            <td align="center">:</td>
	            <td>
			<?php 
                          echo  $objDrop->drop_sectores2($codsuc, $row["codsector"]); 
                            if($row["codsector"]!="")
                            {
                        ?>
                	<script>
                            cargar_calles(<?php echo $row["codsector"]?>, codsuc, <?php echo $row["codzona"]?>, <?php echo $row["codcalle"]?>)
                        </script>
                        <?php } ?>
                    </td>
	            <td align="right">Telefono</td>
	            <td align="center">:</td>
	            <td><input type="text" name="telefono" id="telefono" size="10" maxlength="11" class="inputtext" value="<?php echo $row["telefono"]?>" /></td>
	            </tr>
	          <tr>
	            <td>Calle</td>
	            <td align="center">:</td>
	            <td>
	              <div id="div_calles">
	                <select name="calles" id="calles" style="width:220px" class="select">
	                  <option value="0">--Seleccione la Calle--</option>
	                  </select>
	                </div>
	              </td>
	            <td align="right">Nro. Calle</td>
	            <td align="center">:</td>
	            <td><input type="text" name="nrocalle" id="nrocalle" size="10" maxlength="11" class="inputtext" value="<?php echo $row["nrocalle"]?>" /></td>
	            </tr>
	          <tr>
	            <td>Departamento</td>
	            <td align="center">:</td>
	            <td><?php 
                        $objDrop->drop_ubigeo("departamento",
                                            "--Seleccione el Departamento--",
                                            " where codubigeo LIKE ? ",
                                            array("%0000"),
                                            "onchange='cargar_provincia(this.value, 0);'", substr($IdUbigeo, 0, 2)."0000"); 
                        ?>
                </td>
	            <td align="right">Provincia</td>
	            <td align="center">:</td>
	            <td>
	              <div id="div_provincia">
	                <select id="provincia" name="provincia" style="width:220px" class="select">
	                  <option value="0">--Seleccione la Provincia--</option>
	                  </select>
	                </div>
	              </td>
	            </tr>
	          <tr>
	            <td>Distrito</td>
	            <td align="center">:</td>
	            <td>
	              <div id="div_distrito">
	                <select id="distrito" name="distrito" style="width:220px" class="select">
	                  <option value="0">--Seleccione un Distrito--</option>
	                  </select>
	                </div>	
	              </td>
	            <td align="right">Recepcionado por</td>
	            <td align="center">:</td>
	            <td><input type="text" name="recepcionista" id="recepcionista" style="width:200px;" maxlength="11" class="inputtext" readonly="readonly" value="<?php echo $_SESSION['nombre']?>" /></td>
	            </tr>
                    <tr>
                        <td colspan="6"><hr/></td>
                    </tr>
                    <tr>
	            <td colspan="6">
                        <span style="margin-right:20px; margin-left: 0px">Doc. Referencia&nbsp;&nbsp;&nbsp; : <input type="text" name="doc_referencia" id="doc_referencia" size="10" maxlength="100" class="inputtext" value="<?php if($row['doc_referencia']!="") echo $row['doc_referencia']; else echo "RECIBO";?>" /></span>
                        <span style="margin-right:20px">Imp.Reclamo: <input type="text" name="monto_reclamo" id="monto_reclamo" size="10" maxlength="18" class="inputtext" readonly="readonly" value="<?php if($row['monto_reclamo']!="") echo $row['monto_reclamo']; else echo "0.00"; ?>" /></span>
                        <span>CAR: <?php $objDrop->drop_car($codsuc, $row["car"]); ?></span>
                    </td>
	            </tr>                    
	          <tr>
	            <td colspan="6" class="TitDetalle" style="height:5px"></td>
	            </tr>
	          <tr>
	            <td colspan="6" class="TitDetalle estiloX" >Domicilio Procesal </td>
	            </tr>
	          <tr>
	            <td colspan="6" class="TitDetalle" style="height:5px"></td>
	            </tr>
	          <tr>
	            <td>Direccion</td>
	            <td align="center">:</td>
	            <td><input type="text" name="direccion" id="direccion" size="40" class="inputtext" value="<?php echo $row["direccion1"]?>" /></td>
	            <td align="right">Nro. Calle</td>
	            <td align="center">:</td>
	            <td><input type="text" name="nrocalle2" id="nrocalle2" size="5" maxlength="11" class="inputtext" value="<?php echo $row["nrocalle1"]?>" /></td>
	            </tr>
	          <tr>                     
	            <td>Urbanizacion/Barrio</td>
	            <td align="center">:</td>
	            <td>
                        <input type="text" name="urbanizacion" id="urbanizacion" size="40" class="inputtext" value="<?php echo $row["referencia"]?>" />
                        </td>
	            <td align="right">Manzana</td>
	            <td align="center">:</td>
	            <td>
                        <input type="text" name="manzana" id="manzana" size="5" class="inputtext" value="<?php echo $row["manzana1"]?>" />
                        Lote: 
                        <input type="text" name="nrolote" id="nrolote" size="5" maxlength="11" class="inputtext" value="<?php echo $row["lote1"]?>" /></td>
	            </tr>
                    <tr>
                        <td>Telefono/Celular</td>
                        <td align="center">:</td>
                        <td><input type="text" name="celular" id="celular" size="40" class="inputtext" value="<?php echo $row["celular"]?>" /></td>
                        <td align="right">Cod. Postal</td>
                        <td align="center">:</td>
                        <td><input type="text" name="codpostal" id="codpostal" size="5" maxlength="11" class="inputtext" value="<?php echo $row["codpostal"]?>" /></td>
                        </tr>
	          </table>
	        </div>
	      <div id="tabs-3" style="height:330px">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="12%">Tipo de Reclamo </td>
	            <td width="3%" align="center">:</td>
	            <td class="CampoDetalle">
	            	<div class="buttonset" style="display:inline">
		              <input id="tipo_reclamo1" name="radiobutton" type="radio" value="1" onclick="cargar_conceptos_reclamos(1,1000)" <?php echo $d="";$row["tiporeclamo"]==1?$d="checked='cheked'":$d; echo $d;?> <?php if($row['tiporeclamo']==""){echo 'checked=""';} ?> />
		              <label for="tipo_reclamo1"   >Comerciales con Facturacion</label>
		              <input id="tipo_reclamo2" name="radiobutton" type="radio" value="2" onclick="cargar_conceptos_reclamos(2,1000)" <?php echo $d="";$row["tiporeclamo"]==2?$d="checked='cheked'":$d; echo $d;?> />
		              <label for="tipo_reclamo2"  >Comerciales sin Facturacion</label>
		              <input id="tipo_reclamo3" name="radiobutton" type="radio" value="3" onclick="cargar_conceptos_reclamos(3,1000)" <?php echo $d="";$row["tiporeclamo"]==3?$d="checked='cheked'":$d; echo $d;?> />
		              <label for="tipo_reclamo3">Operacionales</label>
		            </div>
		             <span class="MljSoft-icon-buscar" title="Buscar Usuario" onclick="buscar_tipo_reclamos();"></span>
		            <input type="hidden" name="tiporeclamo" id="tiporeclamo" value="<?=isset($row["tiporeclamo"])?$row["tiporeclamo"]:0?>" />
		            <input type="hidden" name="codzona" id="codzona" value="<?=isset($row["codzona"])?$row["codzona"]:''?>" />
	        
		        </td>
	            </tr>
	          <tr>
	            <td>Reclamo</td>
	            <td align="center">:</td>
	            <td class="CampoDetalle">
	              <div id="div_conceptos_reclamos">
                        <?php echo $conceptosreclamos ?>  
	                <select id="codconcepto" name="codconcepto" class="select" style="width:500px">
	                  <option>--Seleccione el Concepto del Reclamo--</option>
	                </select>
	              </div>
                    </td>
	            </tr>
	          <tr>
	            <td valign="top">Motivo</td>
	            <td align="center" valign="top">:</td>
	            <td class="CampoDetalle"><textarea name="glosa" id="glosa" class="inputtext" style="font-size:12px" cols="90" rows="5"><?php echo $row["glosa"]?></textarea></td>
	          </tr>
                  <tr>
                    <td valign="top">Relacion de Pruebas que se presentan Adjuntas:</td>
	            <td align="center" valign="top">:</td>
	            <td class="CampoDetalle"><textarea name="pruebas" id="pruebas" class="inputtext" style="font-size:12px" cols="90" rows="5"><?php echo $row["pruebas"]?></textarea></td>  
                  </tr>
	          </table>
	        </div>  
	      <div id="tabs-2" style="height:330px">
	        <div id="div_facturaciones" style="height:120px; overflow:auto">
	            <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbfacturacion" rules="all" >
					<thead class="ui-widget-header" >
	              <tr  align="center">
	                <th width="15%" >Nro. Doc.</th>
	                <th width="12%" >A&ntilde;o y Mes</th>
	                <th width="11%" >Categoria</th>
	                <th width="10%" >Lect. Ant.</th>
	                <th width="10%" >Lect. Act.</th>
	                <th width="10%" >Consumo</th>
	                <th width="10%" >Importe</th>
	                <th width="2%" >&nbsp;</th>
	                </tr>
	              </thead>
	            </table>
	          </div>
	        <div style="height:20px;">
	          &nbsp;
	          </div>
	        <div id="div_rebajas" style="height:180px; overflow:auto">
	            <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbrebajas" rules="all" >
					<thead class="ui-widget-header" >
	              <tr align="center">
	                <th width="15%" >Nro. Doc.</th>
	                <th width="12%" >A&ntilde;o y Mes</th>
	                <th width="11%" >Categoria</th>
	                <th width="11%" >Lect. Ant.</th>
	                <th width="11%" >Lect. Act.</th>
	                <th width="11%" >Consumo</th>
	                <th width="10%" >Importe</th>
	                <th width="1%" >&nbsp;</th>
	              </tr>
	          </thead>
	          <tbody>
                  <?php
				  	$contador=0;
				  	if($Id!="")
					{
						$sqlmeses  = "select meses.nrofacturacion,upper(doc.abreviado),meses.nrodocumento,
									 meses.anio,meses.mes,upper(tipd.descripcion),meses.categoria,meses.codtipodeuda,
									 sum(meses.imprebajado),sum(meses.imporiginal),meses.lecturaanterior,meses.lecturaultima,meses.consumo,
									 meses.seriedocumento
									 from reclamos.mesesreclamados as meses
									 inner join reglasnegocio.documentos as doc on(meses.coddocumento=doc.coddocumento) AND (meses.codsuc=doc.codsuc)
									 inner join public.tipodeuda as tipd on(meses.codtipodeuda=tipd.codtipodeuda)
									 where meses.nroreclamo=? and meses.codemp=1 and meses.codsuc=?
									 group by meses.codtipodeuda,meses.nrofacturacion,
									 doc.abreviado,meses.nrodocumento,meses.anio,meses.mes,meses.categoria,tipd.descripcion,
									 meses.lecturaanterior,meses.lecturaultima,meses.consumo,meses.seriedocumento
									 ORDER BY meses.nrofacturacion";
				
						$consulta_meses = $conexion->prepare($sqlmeses);
						$consulta_meses->execute(array($Id,$codsuc));
						$items_meses = $consulta_meses->fetchAll();
						$totalreclamado=0;
						//var_dump($consulta_meses->errorInfo());
						foreach($items_meses as $row_meses)
						{
							$contador++;
							$totalreclamado+=$row_meses[9];
							$cat="";
							if($row_meses[6]==0){$cat="FACTURACION";}
							if($row_meses[6]==1){$cat="SALDO";}
							if($row_meses[6]==2){$cat="SALDO REFINANCIADO";}
							if($row_meses[6]==3){$cat="PRESTACION DE SERVICIOS EN CAJA";}
				  ?>
	              <tr style='background-color:#FFFFFF' align="center" id='tr_<?=$contador?>'>
	                <td >
                    	<input type='hidden' name='nrofacturacion<?=$contador?>' id='nrofacturacion<?=$contador?>' value='<?php echo $row_meses[0]?>' />
						<input type='hidden' name='codtipodeuda<?=$contador?>' id='codtipodeuda<?=$contador?>' value='<?php echo $row_meses[7]?>' />
                        <input type='hidden' name='ct<?=$contador?>' id='ct<?=$contador?>' value='<?php echo $row_meses[6]?>' />
						<input type='hidden' name='impz<?=$contador?>' id='impz<?=$contador?>' value='<?php echo $row_meses[8]?>' />
						<input type='hidden' id='impori<?=$contador?>' value='<?=$row_meses[9]?>' class='importeoriginal' />
						<?php echo $row_meses[1]?> :<?php echo $row_meses['seriedocumento']?>-<?php echo $row_meses[2]?>
                    </td>
	                <td align="center" ><?php echo $row_meses[3]."-".$meses[$row_meses[4]]?></td>
	                <td align="center" ><?php echo $cat?></td>
	                 <td align="right" class='select'><?=number_format($row_meses['lecturaanterior'])?></td>
	                  <td align="right" class='select'><?=number_format($row_meses['lecturaultima'])?></td>
	                   <td align="right" class='select'><?=number_format($row_meses['consumo'])?></td>
	                <td align="right" ><?=number_format($row_meses[9],2)?></td>
	                <td align="center" >
	                	<span class='icono-icon-trash' title='Quitar Facturaci&oacute;n'  onClick='remove_row(<?php echo $contador?>)'></span>
                    	
                    </td>
	                </tr>
                   <?php
						}
					}
				   ?>
	             </tbody>
	            </table>
	          </div>
	        </div>  
                <div id="tabs-5" style="height:330px" class="ui-widget-content">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
	            <td colspan="6">
                        <div style="padding:0 0 5px">
                        <span>
                            <?php 
                                if($entrega_cartilla==1) { $ck1 = "checked=''";$ck2=""; }
                                    else {$ck1=""; $ck2="checked";}
                            ?>
                            <label style="display:inline-block; width:230px; ">EPS Entrega Cartilla</label>: 

                            <div class="buttonset" style="display:inline">
					            <input type="radio" name="entrega_cartilla" id="entrega_cartilla1" value="1" <?php echo $ck1; ?> />
					            <label for="entrega_cartilla1">Si</label>
	                            <input type="radio" name="entrega_cartilla" id="entrega_cartilla2" value="0" <?php echo $ck2; ?> />
	                            <label for="entrega_cartilla2">No</label>
				            </div>

                            
                        </span>                        
                        </div>
	            </tr>    
                    <tr>
                        <td colspan="6">
                        <div style="padding:5px 0">
                        <span>
                            <?php 
                                if($row['contratacion_m']==1) { $ck1 = "checked=''";$ck2=""; }
                                    else {$ck1=""; $ck2="checked";}
                            ?>
                            <label style="display:inline-block; width:230px; ">Solicita Constrataci&oacute;n Medidor</label>:
                            <div class="buttonset" style="display:inline">
					            <input type="radio" name="contratacion_m" id="contratacion_m1" value="1" <?php echo $ck1; ?> />
					            <label for="contratacion_m1">Si</label>
                            	<input type="radio" name="contratacion_m" id="contratacion_m2" value="0" <?php echo $ck2; ?> />
                            	<label for="contratacion_m2">No</label>
				            </div>
                            
                        </span>
                        </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="6">
                            <span >
                            <label style="display:inline-block; width:230px; ">Inspecci&oacute;n Interna y Externa</label>:
                            <input type="text" name="fecha_inspeccion" id="fecha_inspeccion" value="<?php if($row['fecha_inspeccion']=="") echo "00/00/0000"; else echo $row['fecha_inspeccion']; ?>" style="width:80px; text-align: center" maxlength="10" />
                            <input type="text" name="hora_inspeccion" id="hora_inspeccion" value="<?php echo $hora_inspeccion; ?>" style="width:50px; text-align: center" maxlength="5"  /> - 
                            <input type="text" name="hora_inspeccion2" id="hora_inspeccion2" value="<?php echo $hora_inspeccion2; ?>" style="width:50px; text-align: center" maxlength="5"  />    
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div style="padding:5px 0">
                            <span>
                            <label style="display:inline-block; width:230px; ">Fecha y Hora de Citaci&oacute;n a Reuni&oacute;n</label>:
                            <input type="text" name="fecha_citacion" id="fecha_citacion" value="<?php if($row['fecha_citacion']=="") echo "00/00/0000"; else echo $row['fecha_citacion']; ?>" style="width:80px; text-align: center" maxlength="10" />
                            <input type="text" name="hora_citacion" id="hora_citacion" value="<?php echo $row['hora_citacion']; ?>" style="width:50px; text-align: center" maxlength="5" />
                            </span>                            
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div style="padding:5px 0">
                            <span>
                            <label style="display:inline-block; width:230px; ">Fecha de Notificaci&oacute;n de la Resoluci&oacute;n</label>:
                            <input type="text" name="fecha_notificacion" id="fecha_notificacion" value="<?php if($row['fecha_notificacion']=="") echo "00/00/0000"; else echo $row['fecha_notificacion']; ?>" style="width:80px; text-align: center" maxlength="10" />                            
                            </span>
                            </div>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="6">
                            <div style="padding:5px 0">
                            <span>
                            <label style="display:inline-block; width:230px;float:left; ">Mensaje</label>:
                            <textarea name="mensaje" id="mensaje" class="inputtext" style="font-size:12px" cols="90" rows="5"><?php echo $mensaje?></textarea>
                            </span>
                            </div>
                        </td>
                    </tr> 
                    </table>
                </div>
	      </div>
	    </td>
	  </tr>
          <tr>
	  <td colspan="6">
            <input type="hidden" name="contadorconcepto" id="contadorconcepto" value="<?php echo $contador?>" />	  
          </td>
	  </tr>
          </tbody>
    </table>
 </form>
</div>
<div id="DivNose" title="Ver Detalle de Facturacion para Realizar la Rebaja"  >
    <div id="div_detalle_facturacion"></div>
</div>
<script>
	//$("#departamento").val(0)
	contador = <?php echo $contador?>;
	
	cargar_provincia('<?=substr($IdUbigeo, 0, 2)."0000" ?>', '<?=$IdUbigeo ?>');
	cargar_distrito('<?=substr($IdUbigeo, 0, 4)."00" ?>', '<?=$IdUbigeo ?>');
</script>
<?php 
	if($Id!="")
	{
?>
            <script>                
                    $("#departamento").val("<?php echo  substr($row["codubigeo"],0,2)."0000"?>")
                    cargar_provincia("<?php echo $IdUbigeo?>","<?php echo $IdUbigeo?>");
                    cargar_distrito("<?php echo $IdUbigeo?>","<?php echo $IdUbigeo?>");		
                    cargar_conceptos_reclamos(<?php echo $row["tiporeclamo"]?>,<?php echo $IdUbigeo?>)
                    cargar_facturacion(<?php echo $row["nroinscripcion"]?>)
                    $("#monto_reclamo").val(<?=$totalreclamado?>)
            </script>
<?php } ?>