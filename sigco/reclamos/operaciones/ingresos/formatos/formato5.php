<?php

    include("../../../../../objetos/clsReporte.php");

    class clsFormato3 extends clsReporte {
        
        function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO N° 5";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $tit2 = strtoupper("Resumen de Acta de Inspeccion Interna");
            //$tit3 = strtoupper("Comerciales no relativos a la Facturacion y Problemas Operacionales");
            
            $this->Cell(190, 4, $tit2, 0, 1, 'C');
            //$this->Cell(190, 4, $tit3, 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C');
            $this->cabecera();
        }
        
        function cabecera() {

        }

        function Contenido($codsuc, $nroreclamo) {
            global $conexion;
            
            $this->SetFillColor(192, 192, 192);
            $empresa = $this->datos_empresa($codsuc);
            $h = 4;
            $s = 2;
            $x = 13;

            $this->Rect(10, $this->GetY() + 3, 190, 247);

            $Sql = "SELECT rec.nroreclamo, rec.nroinscripcion, rec.reclamante, rec.telefono, rec.nrodocumento, ";
            $Sql .= " tipcal.descripcioncorta || ' ' || cal.descripcion, rec.nrocalle,";
            $Sql .= " (SELECT descripcion FROM public.ubigeo WHERE codubigeo = ".$this->SubString('rec.codubigeo', 1, 4)." || '00'),";
            $Sql .= " (SELECT descripcion FROM public.ubigeo WHERE codubigeo = rec.codubigeo), rec.creador, rec.glosa, ";
            $Sql .= " rec.direccion1, rec.nrocalle1, rec.manzana1, rec.lote1, rec.fechaemision, rec.correlativo, c.codantiguo, ";
            $Sql .= " c.nromed, (c.lecturaultima || ' ' || c.fechalecturaultima) AS lecturaultima, ";
			$Sql .= " c.codmanzanas, c.lote ";
            $Sql .= "FROM reclamos.reclamos rec ";
			$Sql .= " INNER JOIN catastro.clientes c ON(rec.codsuc = c.codsuc) AND (rec.nroinscripcion = c.nroinscripcion) ";
            $Sql .= " INNER JOIN public.calles cal ON(rec.codemp = cal.codemp) AND (rec.codsuc = cal.codsuc) AND (rec.codcalle = cal.codcalle) AND (rec.codzona = cal.codzona) ";
            $Sql .= " INNER JOIN public.tiposcalle tipcal ON(tipcal.codtipocalle=cal.codtipocalle) ";
			//$Sql .= " INNER JOIN public.manzanas manza ON(c.codtipocalle=cal.codtipocalle) ";
            $Sql .= "WHERE rec.codsuc = ".$codsuc." ";
			$Sql .= " AND rec.nroreclamo = ".$nroreclamo." ";

            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array());
            $row = $consulta->fetch();

            /*$this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO Nro. 5";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');

            $this->Ln(1);
            $this->SetFont('Arial', '', 7);

            $tit2 = "Resumen de Acta de Inspeccion Interna";
            $this->Cell(190, $h, $tit2, 0, 1, 'C');*/

            $this->Ln(6);
            $this->SetX($x);
            $this->Cell(35, $h, "Nro. DE SUMINISTRO", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $codantiguo = $row['codantiguo'];	//$this->CodUsuario($codsuc, $row[1]);
            $this->Cell(20, $h, $codantiguo, 1, 0, 'C');

            $this->SetX($x + 118);
            $this->Cell(40, $h, "CODIGO DEL RECLAMO Nro.", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $Anio = substr($this->DecFecha($row['fechaemision']),6,4);
            $this->Cell(20, $h,$Anio." - ".str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 1, 1, 'R');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(138, $h, "NOMBRE DEL SOLICITANTE O REPRESENTANTE", 0, 0, 'L');
            $this->Cell(20, $h, "TELEFONO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(20, $h, $row[3], 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(183, $h, strtoupper(utf8_decode($row[2])), 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)", 0, 0, 'L');
            $this->SetX($x + 118);
            $this->Cell(40, $h, "", 0, 0, 'R');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(20, $h, $row[4], 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "RAZON SOCIAL", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(143, $h, "", 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "UBICACION DEL PREDIO", 0, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(123, $h, strtoupper($row[5]), 1, 0, 'C');
            $this->Cell(20, $h, strtoupper($row[6]), 1, 0, 'C');
            $this->Cell(20, $h, $row["codmanzanas"], 1, 0, 'C');
            $this->Cell(20, $h, $row["lote"], 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(123, $h, "(Calle, Jiron, Avenida )", 1, 0, 'C');
            $this->Cell(20, $h, "Nro.", 1, 0, 'C');
            $this->Cell(20, $h, "Mz.", 1, 0, 'C');
            $this->Cell(20, $h, "Lote", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(61.5, $h, "", 1, 0, 'L');
            $this->Cell(61.5, $h, $row[7], 1, 0, 'C');
            $this->Cell(60, $h, $row[8], 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(61.5, $h, "(Urbanizacion, barrio)", 1, 0, 'C');
            $this->Cell(61.5, $h, "Provincia", 1, 0, 'C');
            $this->Cell(60, $h, "Distrito", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(61.5, $h, $row["nromed"], 1, 0, 'C');
            $this->Cell(61.5, $h, "", 1, 0, 'C');
            $this->Cell(60, $h, intval($row["lecturaultima"]), 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(61.5, $h, "Medidor Nro.", 1, 0, 'C');
            $this->Cell(61.5, $h, "Diametro", 1, 0, 'C');
            $this->Cell(60, $h, "Ultima Lectura (Fecha y Registro)", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(83, $h, "TIPO DE UNIDADES DE USO", 0, 0, 'L');


            $this->Cell(20, $h, "SOCIAL", 1, 0, 'C');
            $this->Cell(20, $h, "DOMESTICO", 1, 0, 'C');
            $this->Cell(20, $h, "COMERCIAL", 1, 0, 'C');
            $this->Cell(20, $h, "INDUSTRIAL", 1, 0, 'C');
            $this->Cell(20, $h, "ESTATAL", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(35, $h, "Nro. de Conexiones Asociadas", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(43, $h, "", 0, 0, 'C');
            $this->Cell(20, $h, "", 1, 0, 'C');
            $this->Cell(20, $h, "", 1, 0, 'C');
            $this->Cell(20, $h, "", 1, 0, 'C');
            $this->Cell(20, $h, "", 1, 0, 'C');
            $this->Cell(20, $h, "", 1, 1, 'C');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(183, $h, "(Croquis a la espalda del presente formato, en caso de ser aplicables)", 0, 1, 'L');

            $this->Ln(3);
            $this->SetX($x);
            $this->Cell(183, $h, "ACTUALIZACION DE LOS DATOS DEL PREDIO(llenar solo si hay variacion)", 0, 1, 'L');

            $this->Ln(1);
            $y = $this->GetY();
            $this->SetY($y);
            $this->Rect($x, $this->GetY(), 183, 40);

            $this->Ln(1);
            $this->SetX($x + 2);
            $this->Cell(35, $h, "UBICACION DEL PREDIO", 0, 1, 'L');
            $this->Ln(1);

            $this->SetX($x + 2);
            $this->Cell(118, $h, "", 1, 0, 'L');
            $this->Cell(20, $h, "", 1, 0, 'C');
            $this->Cell(20, $h, "", 1, 0, 'L');
            $this->Cell(20, $h, "", 1, 1, 'L');


            $this->SetX($x + 2);
            $this->Cell(118, $h, "(Calle, Jiron, Avenida )", 1, 0, 'C');
            $this->Cell(20, $h, "Nro.", 1, 0, 'C');
            $this->Cell(20, $h, "Mz.", 1, 0, 'C');
            $this->Cell(20, $h, "Lote", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x + 2);
            $this->Cell(60, $h, "", 1, 0, 'L');
            $this->Cell(58, $h, "", 1, 0, 'C');
            $this->Cell(60, $h, "", 1, 1, 'C');


            $this->SetX($x + 2);
            $this->Cell(60, $h, "(Urbanizacion, barrio)", 1, 0, 'C');
            $this->Cell(58, $h, "Provincia", 1, 0, 'C');
            $this->Cell(60, $h, "Distrito", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->Ln(1);
            $this->SetX($x + 2);
            $this->Cell(68, $h, "TIPO DE UNIDADES DE USO", 0, 0, 'L');


            $this->Cell(10, $h, "Soc", 1, 0, 'C');
            $this->Cell(10, $h, "Dom", 1, 0, 'C');
            $this->Cell(10, $h, "Com", 1, 0, 'C');
            $this->Cell(10, $h, "Ind", 1, 0, 'C');
            $this->Cell(10, $h, "Est", 1, 0, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->Cell(60, $h, "Estado del Abastecimiento durante la inspeccion", 0, 1, 'C');
            $this->SetX($x + 2);
            $this->Cell(35, $h, "Nro. de Conexiones Asociadas", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'L');
            $this->Cell(28, $h, "Ocupadas", 0, 0, 'L');
            $this->Cell(10, $h, "", 1, 0, 'C');
            $this->Cell(10, $h, "", 1, 0, 'C');
            $this->Cell(10, $h, "", 1, 0, 'C');
            $this->Cell(10, $h, "", 1, 0, 'C');
            $this->Cell(10, $h, "", 1, 0, 'C');

            $this->Cell(2, $h, "", 0, 0, 'C');

            $this->Cell(29, $h, "Normal", 1, 0, 'C');
            $this->Cell(29, $h, "Sin Abastecimiento", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x + 2);
            $this->Cell(35, $h, "", 0, 0, 'L');
            $this->Cell(5, $h, "", 0, 0, 'L');
            $this->Cell(28, $h, "Desocupadas", 0, 0, 'L');
            $this->Cell(10, $h, "", 1, 0, 'C');
            $this->Cell(10, $h, "", 1, 0, 'C');
            $this->Cell(10, $h, "", 1, 0, 'C');
            $this->Cell(10, $h, "", 1, 0, 'C');
            $this->Cell(10, $h, "", 1, 0, 'C');

            $this->Cell(2, $h, "", 0, 0, 'C');
            $this->Cell(29, $h, "", 1, 0, 'C');
            $this->Cell(29, $h, "", 1, 1, 'C');

            $this->Ln(6);
            $this->SetX($x);
            $this->Cell(183, $h, "DETALLE DE LA INSPECCION DE LAS INSTALACIONES SANITARIAS INTERIORES", 0, 1, 'L');

            $this->Ln(1);
            $y = $this->GetY();
            $this->SetY($y);
            $this->Rect($x, $this->GetY(), 183, 40);

            $this->Ln(2);

            $this->SetX($x + 2);

            $this->Cell(20, $h, "Estado", 1, 0, 'C');
            $this->Cell(18, $h, "Inodoro", 1, 0, 'C');
            $this->Cell(18, $h, "Lavado", 1, 0, 'C');
            $this->Cell(18, $h, "Ducha", 1, 0, 'C');
            $this->Cell(18, $h, "Urinario", 1, 0, 'C');
            $this->Cell(17, $h, "Bidet", 1, 0, 'C');
            $this->Cell(17, $h, "Grifo", 1, 0, 'C');
            $this->Cell(17, $h, "Cisterna", 1, 0, 'C');
            $this->Cell(17, $h, "Tanque", 1, 0, 'C');
            $this->Cell(17, $h, "Piscina", 1, 1, 'C');

            $this->SetX($x + 2);
            $this->Cell(20, $h, "Con Fuga", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 1, 'C');

            $this->SetX($x + 2);
            $this->Cell(20, $h, "Reparado", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 1, 'C');

            $this->SetX($x + 2);
            $this->Cell(20, $h, "Clausurado", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 1, 'C');

            $this->SetX($x + 2);
            $this->Cell(20, $h, "Totales", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(18, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 0, 'C');
            $this->Cell(17, $h, "", 1, 1, 'C');

            $this->Ln(2);

            $this->SetX($x + 2);
            $this->Cell(177, $h, "Observacion :_____________________________________________________________________________________________________________________", 0, 1, 'L');
            $this->SetX($x + 2);
            $this->Cell(177, $h, "________________________________________________________________________________________________________________________________", 0, 1, 'L');
            $this->SetX($x + 2);
            $this->Cell(177, $h, "________________________________________________________________________________________________________________________________", 0, 1, 'L');

            $this->Ln(6);
            $this->SetX($x);
            $this->Cell(183, $h, "DATOS DE LA PERSONA PRESENTE EN LA INSPECCION", 0, 1, 'L');

            $this->Ln(1);
            $y = $this->GetY();
            $this->SetY($y);
            $this->Rect($x, $this->GetY(), 183, 33);

            $this->Ln(2);

            $this->SetX($x + 2);

            $this->Cell(45, $h, "Nombre de la Persona", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(77, $h, "", 'B', 0, 'L');
            $this->Cell(20, $h, "Reclamante", 0, 0, 'R');
            $this->Cell(5, $h, "Si", 0, 0, 'R');
            $this->Cell(10, $h, "", 'B', 0, 'R');
            $this->Cell(5, $h, "No", 0, 0, 'R');
            $this->Cell(10, $h, "", 'B', 1, 'R');

            $this->Ln(1);
            $this->SetX($x + 2);
            $this->Cell(25, $h, "Propietario", 0, 0, 'L');
            $this->Cell(20, $h, "", 1, 0, 'C');
            $this->Cell(25, $h, "Inquilino", 0, 0, 'L');
            $this->Cell(20, $h, "", 1, 0, 'R');
            $this->Cell(25, $h, "Residente", 0, 0, 'R');
            $this->Cell(20, $h, "", 1, 0, 'R');
            $this->Cell(23, $h, "Otro", 0, 0, 'R');
            $this->Cell(20, $h, "", 1, 1, 'R');

            $this->Ln(1);
            $this->SetX($x + 2);

            $this->Cell(35, $h, "NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)", 0, 0, 'L');
            $this->SetX($x + 115);
            $this->Cell(40, $h, "", 0, 0, 'R');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(20, $h, "", 1, 1, 'L');

            $this->Ln(2);
            $this->SetX($x + 2);
            $this->Cell(177, $h, "Observacion :_____________________________________________________________________________________________________________________", 0, 1, 'L');
            $this->SetX($x + 2);
            $this->Cell(177, $h, "________________________________________________________________________________________________________________________________", 0, 1, 'L');
            $this->SetX($x + 2);
            $this->Cell(177, $h, "________________________________________________________________________________________________________________________________", 0, 1, 'L');

            $this->Ln(12);
            $y = $this->GetY();
            $this->SetY($y);
            $this->Rect($x + 10, $this->GetY() - 6, 70, 15);
            $this->Rect($x + 105, $this->GetY() - 6, 70, 15);

            $this->Ln(10);
            $this->SetX($x + 10);
            $this->Cell(70, $h, "Firma del Reclamante o Persona Presente en la Inspeccion", 0, 0, 'C');
            $this->SetX($x + 105);
            $this->Cell(70, $h, "Persona Autorizada por la EPS para la Inspeccion", 0, 1, 'C');

            $this->Ln(5);
            $this->SetX($x + 2);
            $this->Cell(25, $h, "Fecha", 0, 0, 'L');
            $this->Cell(30, $h, "", 1, 0, 'C');
            $this->Cell(25, $h, "Hora Inicio", 0, 0, 'R');
            $this->Cell(20, $h, "", 'B', 0, 'R');
            $this->Cell(25, $h, "Hora Final", 0, 0, 'R');
            $this->Cell(20, $h, "", 'B', 0, 'R');
        }

    }

    $nroreclamo = $_GET["nroreclamo"];
    $codsuc = $_GET["codsuc"];

    $objReporte = new clsFormato3();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo);
    $objReporte->Output();
    
?>