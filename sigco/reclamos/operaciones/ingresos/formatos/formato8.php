<?php

    include("../../../../../objetos/clsReporte.php");

    class clsFormato1 extends clsReporte {

            function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO N° 8";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
           $tit2 = "Recurso de Reconsideración";
            //$tit3 = strtoupper("Comerciales no relativos a la Facturacion y Problemas Operacionales");
            
            $this->Cell(190, 4, utf8_decode($tit2), 0, 1, 'C');
            //$this->Cell(190, 4, $tit3, 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C');
            $this->cabecera();
        }
        function cabecera() {

        }

        function Contenido($codsuc, $nroreclamo) {
            global $conexion;
            $empresa = $this->datos_empresa($codsuc);
            $h = 4;
            $s = 2;
            $x = 13;
            $this->SetFillColor(192, 192, 192);
            

            $Sql = "select rec.nroreclamo,rec.nroinscripcion,rec.reclamante,rec.fechareg,rec.codmedio,rec.nrodocumento,";
            $Sql .= "tipcal.descripcioncorta || ' ' || cal.descripcion,rec.nrocalle,";
            $Sql .= "(select descripcion from public.ubigeo where codubigeo = " . $this->SubString('rec.codubigeo', 1, 4) . "  || '00'),";
            $Sql .= "(select descripcion from public.ubigeo where codubigeo = rec.codubigeo),conp.descripcion,
            rec.creador,rec.glosa,rec.correlativo,rec.fechaemision, c.codantiguo ";
            $Sql .= "from reclamos.reclamos as rec ";
			$Sql .= " INNER JOIN catastro.clientes c ON (rec.codsuc = c.codsuc) AND (rec.nroinscripcion = c.nroinscripcion) ";
            $Sql .= "inner join public.calles as cal on(rec.codemp=cal.codemp and rec.codsuc=cal.codsuc and rec.codcalle=cal.codcalle and rec.codzona=cal.codzona ) ";
            $Sql .= "inner join public.tiposcalle as tipcal on(tipcal.codtipocalle=cal.codtipocalle) ";
            $Sql .= "inner join reclamos.conceptosreclamos as conp on(rec.codconcepto=conp.codconcepto) ";
            $Sql .= "where rec.codsuc=:codsuc and rec.nroreclamo=:nroreclamo";

            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array(":codsuc" => $codsuc, ":nroreclamo" => $nroreclamo));
            $row = $consulta->fetch();

            $this->Ln(5);
           

            $this->Ln(2);
            $this->SetX($x);
            $this->Cell(35, $h, "", 0, 0, 'L');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(20, $h, '', 0, 0, 'L');

            $this->SetX($x + 118);
            $this->Cell(40, $h, "CODIGO DEL RECLAMO Nro.", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $Anio = substr($this->DecFecha($row['fechaemision']),6,4);
            $this->Cell(20, $h,$Anio." - ".str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 1, 1, 'R');

            $this->Ln(2);
            $this->SetX($x);
            $this->Cell(123, $h, "Nro. DE SUMINISTRO", 0, 0, 'L');
            $codantiguo = $row['codantiguo'];	//$this->CodUsuario($codsuc, $row[1]);
            $this->Cell(60, $h, $codantiguo, 1, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(123, $h, utf8_decode("Nro. DE RESOLUSIÓN IMPUGNADA"), 0, 0, 'L');
            $this->Cell(60, $h, '', 1, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(123, $h, utf8_decode("FECHA DE NOTIFICACIÓN"), 0, 0, 'L');
            $this->Cell(60, $h, '', 1, 1, 'L');
            $this->Ln(1);


            $this->Ln(1);
            $this->SetX($x);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(190, $h, "NOMBRE DEL RECLAMANTE O REPRESENTANTE", 0, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(183, $h, strtoupper(utf8_decode($row[2])), 1, 1, 'C');
            $this->SetX($x);
            $this->Cell(183, $h, 'APELLIDOS Y NOMBRES', 1, 1, 'C');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)", 0, 0, 'L');
            $this->SetX($x + 115);
            $this->Cell(40, $h, "", 0, 0, 'R');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(23, $h, $row[5], 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "RAZON SOCIAL", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(143, $h, "", 1, 1, 'L');

            $this->Ln(5);
            $x = 11;
            $this->SetX($x);
            $this->Cell(35, $h, "TIPO DE RECLAMO (Indique la letra del tipo de reclamo)", 0, 1, 'L');
            $this->Rect(12, $this->GetY(), 184, 8);
            $this->Ln(2);
            $x = 13;
            $this->SetX($x);
            $this->Cell(55, $h, utf8_decode("Tipo de reclamo (según lista de Formato 2)"), 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->MultiCell(120, $h, strtoupper($row[10]), '1', 'J');

            $x = 11;
            $this->Ln(5);
            $this->SetX($x);
            $this->Cell(35, $h, utf8_decode("FUNDAMENTO DEL RECURSO DE RECONSIDERACIÓN"), 0, 1, 'L');
            $this->Rect(12, $this->GetY(), 184, 28);
            $this->Ln(4);
            $x = 18;
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);

            ///

            $x = 11;
            $this->Ln(5);
            $this->SetX($x);
            $this->Cell(35, $h, utf8_decode("NUEVA PRUEBA QUE SUSTENTA EL RECURSO"), 0, 1, 'L');
            $this->Rect(12, $this->GetY(), 184, 28);
            $this->Rect(12, $this->GetY(), 92, 28);
            $this->Ln(4);
            $x = 18;
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(175, 0.01, "", 1, 1, 'L');
            $this->Ln(4);

            $this->Ln(5);
            $x = 11;
            $this->SetX($x);
            $this->Cell(35, $h, utf8_decode("SOLICITUD DE CONTRASTACIÓN DEL MEDIDOR"), 0, 1, 'L');
            $this->Rect(12, $this->GetY(), 184, 20);
            $this->Ln(2);
            $x = 13;
            $this->SetX($x);
            $text = "DECLARACIÓN DEL RECLAMANTE (aplicable a los medidores por consumo medido, y ";
            $this->Cell(55, $h, utf8_decode($text), 0, 1, 'L');
            $this->SetX($x);
            $text = "solamente para aquellos que no solicitaron contrastación al inicio del reclamo):";
            $this->Cell(155, $h, utf8_decode($text), 0, 0, 'L');
            $this->Cell(10, $h, utf8_decode('SI'), 0, 0, 'C');
            $this->Cell(5, $h, utf8_decode(''), 0, 0, 'L');
            $this->Cell(10, $h, utf8_decode(''), 1, 1, 'L');

            $this->SetX($x);
            $text = "Acepto la realización de la prueba de contrastación y asumir su costo, si el resultado ";
            $this->Cell(55, $h, utf8_decode($text), 0, 1, 'L');
            $this->SetX($x);
            $text = "de la prueba indica que el medidor no sobreregistra:";
            $this->Cell(155, $h, utf8_decode($text), 0, 0, 'L');
            $this->Cell(10, $h, utf8_decode('NO'), 0, 0, 'C');
            $this->Cell(5, $h, utf8_decode(''), 0, 0, 'L');
            $this->Cell(10, $h, utf8_decode(''), 1, 1, 'L');


            $this->Ln(5);
            $x = 11;
            $this->SetX($x);
            $this->Rect(10, $this->GetY(), 190, 10);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(35, $h, utf8_decode("INFORMACION A SER COMPLETADA POR LA EPS"), 0, 1, 'L');
            $this->SetFont('Arial', '', 7);
            $this->SetX($x);
            $this->Cell(75, $h, utf8_decode("FECHA MÁXIMA DE NOTIFICACIÓN DE LA RESOLUCIÓN"), 1, 0, 'L');
            $this->Cell(20, $h, utf8_decode("(DD/MM/AA)"), 1, 0, 'L');
            $this->Cell(30, $h, utf8_decode(""), 1, 0, 'L');
            $this->Ln(2);
            $x = 13;


            $x = 13;
            $this->Ln(10);
            $this->Rect($x, $this->GetY(), 80, 11);
            $this->Rect($x + 85, $this->GetY(), 40, 11);
            $this->Rect($x + 130, $this->GetY(), 53, 11);

            $this->Ln(11);
            $this->SetX($x);
            $this->Cell(80, $h, "Firma ", 0, 0, 'C');
            $this->SetX($x + 85);
            $this->Cell(40, $h, "Huella digital*", 0, 0, 'C');
            $this->SetX($x + 130);
            $this->Cell(53, $h, "Fecha", 0, 1, 'C');
            $this->SetX($x + 85);
            $this->Cell(40, 2, "(Indice derecho)", 0, 1, 'C');
            $this->SetX($x);
            $this->Cell(183, $h, "* En caso de no saber firmar o estar impedido bastara con la huella digital.", 0, 1, 'L');
        }

    }

    $nroreclamo = $_GET["nroreclamo"];
    $codsuc = $_GET["codsuc"];


    $objReporte = new clsFormato1();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo);
    $objReporte->Output();

?>