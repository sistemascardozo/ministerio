<?php

    /* if(!session_start()){session_start();}

      require('../../../../admin/clases/fpdf.php');
      $fechac=date("d/m/Y"); */

    include("../../../../../objetos/clsReporte.php");

    class clsCedula extends clsReporte {

        function cabecera() {

        }

         function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO N° 10";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $tit2 = "Cedula de Notificacion";
            //$tit3 = strtoupper("Comerciales no relativos a la Facturacion y Problemas Operacionales");
            
            $this->Cell(190, 4, utf8_decode($tit2), 0, 1, 'C');
            //$this->Cell(190, 4, $tit3, 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C');
            $this->cabecera();
        }
        function Contenido($codsuc, $nroreclamo) {
            //global $nroreclamo,$idsucursal,$sucursal;
            global $conexion;
            $this->SetFillColor(192, 192, 192);
            $h = 4;
            $s = 2;
            $x = 13;

        

            $Sql = "select rec.nroreclamo,rec.nroinscripcion,rec.reclamante,rec.telefono,rec.nrodocumento,";
            $Sql .= "tipcal.descripcioncorta || ' ' || cal.descripcion,rec.nrocalle,";
            $Sql .= "(select descripcion from public.ubigeo where codubigeo = ".$this->SubString('rec.codubigeo', 1, 4)." || '00'),";
            $Sql .= "(select descripcion from public.ubigeo where codubigeo = rec.codubigeo),conp.descripcion,rec.creador,rec.glosa, ";
            $Sql .= "rec.direccion1,rec.nrocalle1,rec.manzana1,rec.lote1/*,ins.nombres,ins.nrodocumento*/,
                    rec.correlativo,rec.fechaemision, c.codantiguo ";
            $Sql .= "from reclamos.reclamos as rec ";
			$Sql .= " INNER JOIN catastro.clientes c ON (rec.codsuc = c.codsuc) AND (rec.nroinscripcion = c.nroinscripcion) ";
            $Sql .= "inner join public.calles as cal on(rec.codemp=cal.codemp and rec.codsuc=cal.codsuc and rec.codcalle=cal.codcalle and rec.codzona=cal.codzona ) ";
            $Sql .= "inner join public.tiposcalle as tipcal on(tipcal.codtipocalle=cal.codtipocalle) ";
            $Sql .= "inner join reclamos.conceptosreclamos as conp on(rec.codconcepto=conp.codconcepto) ";
            //$Sql .= "inner join reglasnegocio.inspectores as ins on(rec.codinspector=ins.codinspector) ";
            $Sql .= "where rec.codsuc=? and rec.nroreclamo=?";

            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array($codsuc, $nroreclamo));
            $row = $consulta->fetch();

            /* $Consulta   = pg_query($Sql);
              $row        = pg_fetch_array($Consulta); */

            $this->Ln(5);
             $this->Ln(2);
            $this->SetX($x);
            $this->Cell(35, $h, "Nro. DE SUMINISTRO", 0, 0, 'L');
             $codantiguo = $row['codantiguo'];	//$this->CodUsuario($codsuc, $row[1]);
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(20, $h, $codantiguo, 1, 0, 'L');

            $this->SetX($x + 118);
            $this->Cell(40, $h, "CODIGO DEL RECLAMO Nro.", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
             $Anio = substr($this->DecFecha($row['fechaemision']),6,4);
            $this->Cell(20, $h,$Anio." - ".str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 1, 1, 'R');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(138, $h, "NOMBRE DEL SOLICITANTE O REPRESENTANTE", 0, 0, 'L');
            $this->Cell(20, $h, "TELEFONO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(20, $h, $row[3], 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(183, $h, strtoupper(utf8_encode($row[2])), 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)", 0, 0, 'L');
            $this->SetX($x + 118);
            $this->Cell(40, $h, "", 0, 0, 'R');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(20, $h, $row[4], 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "RAZON SOCIAL", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(143, $h, "", 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "UBICACION DEL DOMINIO PROCESAL", 0, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, utf8_decode("(Si el reclamante no lo hubiere señalado, se le notificara en el domicilio donde se remiten los recibos por el servicio que reclama)."), 0, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(123, $h, strtoupper($row[5]), 1, 0, 'L');
            $this->Cell(20, $h, strtoupper($row[6]), 1, 0, 'C');
            $this->Cell(20, $h, "", 1, 0, 'L');
            $this->Cell(20, $h, "", 1, 1, 'L');


            $this->SetX($x);
            $this->Cell(123, $h, "(Calle, Jiron, Avenida )", 1, 0, 'C');
            $this->Cell(20, $h, "Nro.", 1, 0, 'C');
            $this->Cell(20, $h, "Mz.", 1, 0, 'C');
            $this->Cell(20, $h, "Lote", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(61.5, $h, "", 1, 0, 'L');
            $this->Cell(61.5, $h, $row[7], 1, 0, 'C');
            $this->Cell(60, $h, $row[8], 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(61.5, $h, "(Urbanizacion, barrio)", 1, 0, 'C');
            $this->Cell(61.5, $h, "Provincia", 1, 0, 'C');
            $this->Cell(60, $h, "Distrito", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "Tipo de Problema", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->MultiCell(143, $h, strtoupper($row[9]), '1', 'J');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "DOCUMENTO A NOTIFICAR", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(143, $h, "", 1, 1, 'L');

            $this->Ln(1);
            $this->Rect($x, $this->GetY(), 183, 15);
            $this->Ln(3);
            $this->SetX($x);
            $this->Cell(35, $h, "Visita realizada", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(25, $h, "( ) Primera", 0, 0, 'L');
            $this->Cell(25, $h, "", 1, 0, 'L');
            $this->Cell(5, $h, "", 0, 0, 'L');
            $this->Cell(25, $h, "( ) Segunda", 0, 0, 'L');
            $this->Cell(25, $h, "", 1, 1, 'L');
            $this->SetX($x);
            $this->Cell(35, $h, "", 0, 0, 'R');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(25, $h, "Fecha y Hora", 0, 0, 'L');
            $this->Cell(25, $h, "", 1, 0, 'L');
            $this->Cell(5, $h, "", 0, 0, 'L');
            $this->Cell(25, $h, "Fecha y Hora", 0, 0, 'L');
            $this->Cell(25, $h, "", 1, 1, 'L');

            $this->Ln(5);
            $this->SetX($x);
            $this->Cell(50, $h, "RECEPCION DE LA NOTIFICACION", 0, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(103, $h, "", 1, 0, 'L');
            $this->Cell(40, $h, "", 1, 0, 'C');
            $this->Cell(40, $h, "", 1, 1, 'L');


            $this->SetX($x);
            $this->Cell(103, $h, "Reclamante (Nombre)", 1, 0, 'C');
            $this->Cell(40, $h, "Documento de Identidad", 1, 0, 'C');
            $this->Cell(40, $h, "Firma", 1, 1, 'C');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(103, $h, "", 1, 0, 'L');
            $this->Cell(40, $h, "", 1, 0, 'C');
            $this->Cell(40, $h, "", 1, 1, 'L');


            $this->SetX($x);
            $this->Cell(103, $h, "Persona Distinta  (Nombre *)", 1, 0, 'C');
            $this->Cell(40, $h, "Documento de Identidad", 1, 0, 'C');
            $this->Cell(40, $h, "Firma", 1, 1, 'C');

            $this->SetTextColor(0, 0, 0);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(80, $h, "* GRADO DE PARENTESCO O RELACION CON EL RECLAMANTE", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(60, $h, "", 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(80, $h, "OBSERVACIONES", 0, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(60, $h, "( ) PREDIO DESOCUPADO", 0, 0, 'L');
            $this->Cell(20, $h, "", 1, 1, 'L');
            $this->SetX($x);
            $this->Cell(60, $h, "( ) DOMICILIO EQUIVOCADO O NO EXISTE", 0, 0, 'L');
            $this->Cell(20, $h, "", 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(80, $h, "OTROS", 0, 1, 'L');
            $this->SetX($x);
            $this->MultiCell(183, $h + 20, "", '1', 'J');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(80, $h, "DE NO SER POSIBLE LA NOTIFICACION:", 0, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(50, $h, "Caracteristicas de la fachada del inmueble", 0, 0, 'L');
            $this->Cell(133, $h, "", 'B', 1, 'L');
            $this->SetX($x);
            $this->Cell(183, $h, "", 'B', 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(50, $h, "Numero de suministro de energia electrica", 0, 0, 'L');
            $this->Cell(133, $h, "", 'B', 1, 'L');
            $this->SetX($x);
            $this->Cell(183, $h, "", 'B', 1, 'L');

            $this->Ln(3);
            $this->SetX($x);
            $this->Cell(63, $h, strtoupper(utf8_encode($row[16])), 1, 0, 'L');
            $this->Cell(15, $h, $row[17], 1, 0, 'C');
            $this->Cell(25, $h, "", 1, 0, 'L');
            $this->Cell(25, $h, "", 1, 0, 'C');
            $this->Cell(25, $h, "", 1, 0, 'C');
            $this->Cell(30, $h, "", 1, 1, 'L');


            $this->SetX($x);
            $this->Cell(63, $h, "Notificador(nombre completo y firma)", 1, 0, 'C');
            $this->Cell(15, $h, "Codigo", 1, 0, 'C');
            $this->Cell(25, $h, "Doc. Identidad", 1, 0, 'C');
            $this->Cell(25, $h, "Fecha Emision", 1, 0, 'C');
            $this->Cell(25, $h, "Fecha Entrega", 1, 0, 'C');
            $this->Cell(30, $h, "Hora", 1, 1, 'C');
        }

        /* function Footer()
          {
          $this->SetY(-10);
          $this->SetFont('Arial','I',6);
          $this->SetTextColor(0);
          $this->SetLineWidth(.1);
          $this->Cell(0,.1,"",1,1,'C',true);
          $this->Ln(1);
          $this->Cell(0,4,'Pag. '.$this->PageNo(),0,0,'R');
          }
          function DatosUsuario($idusuario,$codsuc=1)
          {
          $sql 		= "select nombres from usuarios where codsuc=".$codsuc." and codusu=".$idusuario;
          $Consulta   = pg_query($sql);
          $row        = pg_fetch_array($Consulta);

          return $row[0];
          } */
    }

    $nroreclamo = $_GET["nroreclamo"];
    $codsuc = $_GET["codsuc"];

    $objReporte = new clsCedula();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo);
    $objReporte->Output();
    
?>