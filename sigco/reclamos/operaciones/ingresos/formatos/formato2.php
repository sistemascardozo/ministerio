<?php

    include("../../../../../objetos/clsReporte.php");

    class clsFormato2 extends clsReporte {

        function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO N° 2";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 8);
            $hc = 5;
            $tit2 = strtoupper("Presentacion del Reclamo");
            $this->Cell(190, $hc, $tit2, 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

        function cabecera() {
            $this->Ln(2);
        }

        function Contenido($codsuc, $nroreclamo) {
            global $conexion, $meses;
            $this->SetFillColor(192, 192, 192);
            $empresa = $this->datos_empresa($codsuc);
            $h = 4;
            $hc = 5;
            $s = 2;
            $x = 10;

            //$this->Rect(10, $this->GetY()+10,190,245);

            $Sql = "SELECT rec.nroreclamo, rec.nroinscripcion, rec.reclamante, rec.telefono, rec.nrodocumento,";
            $Sql .= "tipcal.descripcioncorta || ' ' || cal.descripcion, rec.nrocalle,";
            $Sql .= "(SELECT descripcion from public.ubigeo where codubigeo = ".$this->SubString('rec.codubigeo', 1, 4)." || '00'),";
            $Sql .= "(SELECT descripcion from public.ubigeo where codubigeo = rec.codubigeo), conp.descripcion, rec.creador,
                rec.glosa, rec.direccion1, rec.nrocalle1, rec.manzana1, rec.lote1, rec.monto_reclamo, rec.pruebas,
                rec.entrega_cartilla, rec.contratacion_m, rec.fecha_citacion, rec.hora_citacion, rec.fecha_inspeccion,
                rec.hora_inspeccion, rec.fecha_notificacion, rec.hora_inspeccion2, rec.fechaemision, rec.mensaje, 
                ".$this->getCodCatastral("c.").", rec.correlativo, c.propietario, 
                est.descripcion as estadoreclamo, c.codantiguo ";
            $Sql .= "FROM reclamos.reclamos as rec ";
            $Sql .= "inner join public.calles as cal on(rec.codemp=cal.codemp and rec.codsuc=cal.codsuc and rec.codcalle=cal.codcalle and rec.codzona=cal.codzona ) ";
            $Sql .= "inner join public.tiposcalle as tipcal on(tipcal.codtipocalle=cal.codtipocalle) ";
            $Sql .= "inner join reclamos.conceptosreclamos as conp on(rec.codconcepto=conp.codconcepto) ";
            $Sql .= "INNER JOIN catastro.clientes AS c ON (c.codemp = rec.codemp AND c.codsuc = rec.codsuc AND c.codzona = rec.codzona AND c.codcalle = rec.codcalle AND c.nroinscripcion= rec.nroinscripcion) ";
            $Sql .= "INNER JOIN reclamos.estadoreclamo AS est ON (rec.codestadoreclamo = est.codestadoreclamo)";
            $Sql .= "WHERE rec.codsuc=? and rec.nroreclamo=? ";

            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array($codsuc,$nroreclamo));
            $row = $consulta->fetch();

            $this->SetFont('Arial', '', 10);
            $codantiguo = $row[32];	//$this->CodUsuario($codsuc, $row[1]);
            $this->Ln(2);
            $this->SetX($x);
            $this->Cell(30, $h, utf8_decode("N° SUMINISTRO"), 0, 0, 'L');            
            $this->Cell(20, $h, $codantiguo, 0, 0, 'R');
            
            $this->Cell(37, $h, utf8_decode("CODIGO"), 0, 0, 'R');
            $this->Cell(28, $h, $row['codcatastro'], 0, 0, 'L');

            $this->SetX($x + 126);
            $this->Cell(40, $h, utf8_decode("RECLAMO N° "), 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $Anio = substr($this->DecFecha($row['fechaemision']),6,4);
            $this->Cell(20, $h, $Anio." - ".str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 0, 1, 'R');
            $this->Ln(3);
            
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            /*$this->Cell(146, $hc, "NOMBRE DEL SOLICITANTE O REPRESENTANTE", 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Cell(20, $h, "TELEFONO", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(20, $h, $row[3], 1, 1, 'L');*/
            $this->Cell(135, $h, "NOMBRE DEL RECLAMANTE O REPRESENTANTE", 0, 0, 'L');
            $this->SetFont('Arial', '', 9);
            $this->Cell(23, $h, "TELEFONO", 0, 0, 'L');
            $this->Cell(20, $h, $row['telefono'], 1, 1, 'L');

            $this->SetX($x);
            // $this->SetFont('Arial', '', 9);
            // $this->Cell(40, $h, utf8_decode("ESTADO DE REGISTRO"), 0, 0, 'L');
            // $this->SetFont('Arial', 'B', 9);
            // $this->Cell(20, $h, strtoupper(utf8_decode($row['estadoreclamo'])), 0, 1, 'L');


            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(191, 7, strtoupper(utf8_decode($row[2])), 1, 1, 'L');
            /*$this->SetXY(150, 35);
            $this->Cell(23, $h, strtoupper("inscripcion"), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(23, $h, $codantiguo, 0, 1, 'L');*/

            $this->SetFont('Arial', '', 9);
            $this->SetX($x);
            $this->Cell(191, $hc, "APELLIDOS Y NOMBRES", 1, 1, 'L');

            /*$this->Ln(1);
            $this->SetX($x);
            $this->Cell(191, $h + 1, strtoupper(utf8_decode($row[2])), 1, 1, 'L');*/

            $this->Ln(2);
            $this->SetX($x);
            $this->Cell(170, $hc, "NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)", 0, 0, 'R');
            $this->SetX($x + 166);
            $this->Cell(5, $hc, "", 0, 0, 'C');
            $this->Cell(20, $hc, $row[4], 1, 1, 'C');

            $this->Ln(2);
            $this->SetX($x);
            $this->Cell(35, $hc, "RAZON SOCIAL", 0, 0, 'L');
            $this->Cell(5, $hc, ":", 0, 0, 'C');
            $this->Cell(151, $hc, strtoupper(utf8_decode($row['propietario'])), 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            //$this->Cell(35, $h, "UBICACION DEL PREDIO", 0, 1, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(191, 7, utf8_decode(strtoupper($row[5]." N° ".$row[6])), 1, 1, 'L');
            //$this->Cell(20, $h, strtoupper(), 1, 0, 'C');
            //$this->Cell(20, $h, "", 1, 0, 'L');
            //$this->Cell(20, $h, $row['lote1'], 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(191, $hc, utf8_decode("UBICACIÓN DEL PREDIO"), 1, 1, 'L');
            //$this->Cell(20, $h, "Nro.", 1, 0, 'C', true);
            //$this->Cell(20, $h, "Mz.", 1, 0, 'C', true);
            //$this->Cell(20, $h, "Lote", 1, 1, 'C', true);
            $this->Ln(2);
            
            /*$this->Cell(35, $h, "DIRECCIÓN PROCESAL", 0, 1, 'L');
            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(69.5, $h, "", 1, 0, 'L');
            $this->Cell(61.5, $h, $row[7], 1, 0, 'C');
            $this->Cell(60, $h, $row[8], 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(69.5, $h, "(Urbanizacion, barrio)", 1, 0, 'C', true);
            $this->Cell(61.5, $h, "Provincia", 1, 0, 'C', true);
            $this->Cell(60, $h, "Distrito", 1, 1, 'C', true);*/

            $this->SetTextColor(0, 0, 0);
            $this->Ln(1);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(75, $h, utf8_decode("DIRECCIÓN PROCESAL") , 0, 0, 'L');
            $this->SetFont('Arial', '', 9);
            $this->Cell(35, $h, utf8_decode("(máximo dos direcciones, si no se indica ninguna, se asume la del predio)") , 0, 1, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(50, 7,utf8_decode("DIRECCIÓN 1") , 1, 0, 'L');
            $this->Cell(141, 7, utf8_decode(strtoupper($row[12]." N° ".$row[13])), 1, 1, 'L');
            $this->Cell(50, 7,utf8_decode("DIRECCIÓN 2") , 1, 0, 'L');
            $this->Cell(141, 7, "", 1, 1, 'L');
            //$this->Cell(20, $h, strtoupper(), 1, 0, 'C');
            //$this->Cell(20, $h, strtoupper($row[14]), 1, 0, 'C');
            //$this->Cell(20, $h, strtoupper($row[15]), 1, 1, 'C');


            /*$this->SetX($x);
            $this->Cell(131, $h, "(Calle, Jiron, Avenida )", 1, 0, 'C', true);
            $this->Cell(20, $h, "Nro.", 1, 0, 'C', true);
            $this->Cell(20, $h, "Mz.", 1, 0, 'C', true);
            $this->Cell(20, $h, "Lote", 1, 1, 'C', true);

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(69.5, $h, "", 1, 0, 'L');
            $this->Cell(61.5, $h, $row[7], 1, 0, 'C');
            $this->Cell(60, $h, $row[8], 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(69.5, $h, "(Urbanizacion, barrio)", 1, 0, 'C', true);
            $this->Cell(61.5, $h, "Provincia", 1, 0, 'C', true);
            $this->Cell(60, $h, "Distrito", 1, 1, 'C', true);

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(69.5, $h, "", 1, 0, 'L');
            $this->Cell(61.5, $h, $row['telefono'], 1, 0, 'C');
            $this->Cell(60, $h, "", 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(69.5, $h, "Codigo Postal", 1, 0, 'C', true);
            $this->Cell(61.5, $h, "Telefono / Celular", 1, 0, 'C', true);
            $this->SetFont('Arial', '', 7);
            $this->Cell(60, $h, "Correo Electronico (Obligatorio para reclamos via web)", 1, 1, 'C', true);*/
            
            $this->SetFont('Arial', '', 8);
            $this->SetTextColor(0, 0, 0);
            $this->Ln(4);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(40, $hc, "TIPO DE RECLAMO : ", 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $tpreclamo = $row[9];
            $ver = split('-', $tpreclamo);
            $tp = $ver[0];
            $tpreclamo = $ver[1];
            $this->Cell(151, $hc, utf8_decode(strtoupper($tp)), 1, 0, '');
            $this->Cell(2, $hc, "", 0, 0, 'C');
            $this->Cell(50, $hc, strtoupper($tpreclamo), 0, 1, '');
            $this->Ln(1);
            /* $this->SetX($x);
              $this->Cell(35,$h,"Tipo de Problema",0,0,'L');
              $this->Cell(5,$h,":",0,0,'C');
              $this->MultiCell(143,$h,strtoupper($row[9]),'0','J');
             */
            $this->Ln(3);
            $this->Rect($x, $this->GetY(), 102, 10);
            $this->Rect($x + 111, $this->GetY(), 80, 10);
            
            $this->Ln(1);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(90, $h, "FUNDAMENTOS DEL RECLAMO", 0, 0, 'L');
            $this->SetX($x+111);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(80, $h, utf8_decode("BREVE DESCRIPCIÓN DEL RECLAMO"), 0, 1, 'C');
            
            $this->SetFont('Arial', '', 9);
            $this->Cell(90, $h, utf8_decode("(En caso de ser necesario, se podrán adjuntar páginas adicionales)"), 0, 0, 'L');            
            $this->SetX($x+111);
            $this->SetFont('Arial', '', 9);
            $this->Cell(80, $h, utf8_decode("Fec. Emesión"), 0, 1, 'L');
            
            $this->Ln(2);
            $this->SetX($x);
            $this->SetFont('Arial', '', 9);
            $this->MultiCell(102, $h, strtoupper(utf8_decode($row['glosa'])), 0, 'J');
            
            ///////////////////MESES RECLAMADOS
            $sqlmeses = "SELECT meses.anio,meses.mes,meses.nrofacturacion,
                meses.nrodocumento, meses.seriedocumento,SUM(imporiginal) AS importe
                from reclamos.mesesreclamados as meses
                inner join reglasnegocio.documentos as doc on(meses.coddocumento=doc.coddocumento) AND (meses.codsuc=doc.codsuc)
                inner join public.tipodeuda as tipd on(meses.codtipodeuda=tipd.codtipodeuda)
                WHERE meses.nroreclamo=".$nroreclamo." and meses.codemp=1 and meses.codsuc=".$codsuc."
                GROUP BY meses.anio,meses.mes ,meses.nrofacturacion,meses.nrodocumento, meses.seriedocumento
                ORDER BY meses.nrofacturacion";
            //$consulta_meses = $conexion->prepare($sqlmeses);
            //$consulta_meses->execute(array($Id,$nroreclamo));
            //$items_meses = $consulta_meses->fetchAll();
            /*if (count($conexion->query($sqlmeses)->fetchAll()) == 1) {
                $rowmeses = $conexion->query($sqlmeses)->fetch();
                $Anio = $rowmeses['anio'];
                $Mes = $meses [$rowmeses['mes']];
                $serie= $rowmeses[4];
                $cor= $rowmeses[3];
                $Recibos=$Mes." - ".$Anio;
                $Recibos2=$serie." - ".$cor;
            }
            else
            {*/
                 $Recibos='';
                $Recibos2='';
                $this->SetXY($x+111,120);
                $this->SetFont('Arial', '', 9);
                foreach ($conexion->query($sqlmeses)->fetchAll()as $rowmeses)
                {

                    $this->SetX($x+111);
                    $Anio = $rowmeses['anio'];
                    $Mes = $meses [$rowmeses['mes']];
                    $serie= $rowmeses[4];
                    $cor= $rowmeses[3];
                    $Recibos=$Mes." - ".$Anio.",";
                    $Recibos2=$serie." - ".$cor.",";
                    $this->Cell(80, $h,$Recibos." ".$Recibos2."=>".number_format($rowmeses['importe'], 2), 0, 1,'R');
                }
            //}
            
            $this->SetX($x+111);
            $this->SetFont('Arial', '', 9);
            // $this->Cell(30, $h, $Recibos, 0, 0, 'L');
            // $this->Cell(30, $h, $Recibos2, 0, 0, 'C');
            // $this->Cell(20, $h, number_format($row['monto_reclamo'], 2), 0, 0, 'R');
            $this->Cell(80, $h,number_format($row['monto_reclamo'], 2), 0,0, 'R');
            $this->Ln(2);
            
            $this->SetXY($x,148);
           
            $usuario = $this->loginFull($row[10]);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(30, $h, "ATENDIDO POR", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(75, $h, strtoupper($usuario['nombres']), 0, 0, 'L');            
            $this->Cell(43, $h, "", 0, 0, 'L');
            
            $this->SetX($x+150);
            $this->Cell(20, $h, "LOCALIDAD", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(143, $h, strtoupper($empresa["descripcion"]), 0, 1, 'L');
            

            $this->Ln(1);
            $this->SetY(154);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(183, $h, "RELACION DE PRUEBAS QUE SE PRESENTAN ADJUNTAS", 0, 1, 'L');
            $this->Ln(1);
            $this->SetFont('Arial', '', 10);
            $this->SetX($x);
            $this->MultiCell(183, $h, strtoupper(utf8_decode($row['pruebas'])), '0', 'J');
            //$this->Cell(183,$h+7,$row['pruebas'],1,1,'L');
            

            if ($row['contratacion_m'] == 1) {
                $ck1 = "X";
                $ck2 = "";
            } else {
                $ck1 = "";
                $ck2 = "X";
            }
            $ck1 = "";
            $ck2 = "";
            $this->Ln(10);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 9);
            $textt="LA EPS DA A CONOCER AL USUARIO QUE TIENE DERECHO A ";
            $this->Cell(130, $h,$text, 0, 1, 'L');
            $textt1="SOLICITAR LA CONTRASTACIÓN DE SU MEDIDOR. ";
            $this->Cell(130, $h,utf8_decode($textt." ".$textt1), 0, 1, 'L');
            //"
            $this->Ln(3);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(50, $h, "DECLARACION DEL RECLAMANTE :", 0, 0, 'L');
            $this->SetFont('Arial', '', 9);
            $this->Cell(70, $h, "(Aplicable a reclamos por consumo medido)", 0, 1, 'L');
            
            $this->SetFont('Arial', '', 10);
            $tit1 = "El usuario solicita la contrastación de su medidor? --------------------------------------->";
            $tit2 = "";
            $this->SetX($x);
            $this->Cell(130, $h, utf8_decode($tit1), 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Cell(10, $h, "SI", 0, 0, 'C');            
            $this->Cell(10, $h, $ck1, 1, 0, 'C'); 
            $this->Cell(10, $h, "", 0, 0, 'C');
            $this->Cell(10, $h, "NO", 0, 0, 'C');            
            $this->Cell(10, $h, $ck2, 1, 1, 'C');

            $this->SetX($x);
            $this->Cell(100, $h, $tit2, 0, 1, 'L');
            
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(100, $hc, "INFORMACION A SER COMPLETADA POR LA EPS", 0, 1, 'L');
            $this->Ln(2);
            $this->SetX($x);
            $this->SetFont('Arial', '', 9);
            $this->Cell(57, $h, "INSPECCION INTERNA Y EXTERNA: ", 0, 0, 'L');
            $this->Cell(40, $h, "FECHA: ".$this->CodFecha($row['fecha_inspeccion']), 0, 0, 'C', false);
            //$this->SetTextColor(0,0,0);
            //$this->Cell(25,$h,$this->CodFecha($row['fecha_inspeccion']),1,0,'C');

            $this->Cell(10, $h, "HORA ", 0, 0, 'L', false);
            //$this->Cell(2, $h, "", 0, 0, 'C');
            $this->Cell(22, $h, $row['hora_inspeccion']." - ".$row['hora_inspeccion2'],0, 1, 'C');
            //$this->SetTextColor(0,0,0);
            //$this->Cell(25,$h,$row['hora_inspeccion']." - ".$row['hora_inspeccion2'],0,1,'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(57, $h, "CITACION A REUNION", 0, 0, 'L');
            $this->Cell(40, $h, "FECHA: ".$this->CodFecha($row['fecha_citacion']), 0, 0, 'C', false);
            //$this->SetTextColor(0,0,0);
            //$this->Cell(25,$h,$this->CodFecha($row['fecha_citacion']),1,0,'C');

            $this->Cell(10, $h, "HORA", 0, 0, 'L');
            $this->Cell(22, $h, $row['hora_citacion'], 0, 1, 'L');
            $this->Ln(1);
                        
            $this->SetXY($x+150,205);            
            $this->Cell(95, $h, "FECHA MAXIMA DE", 0, 1, 'L');
            $this->SetXY($x+150,205+$h); 
            $this->Cell(95, $h, "NOTIFICACION DE", 0, 1, 'L');
            $this->SetXY($x+150,205+2*$h); 
            $this->Cell(95, $h, "LA RESOLUCION", 0, 1, 'L');
            $this->Ln(1);
            $this->SetXY($x+150,205+3*$h);   
            $this->SetFont('Arial', 'B', 11);
            $this->Cell(25, 6, $this->CodFecha($row['fecha_notificacion']), 0, 1, 'C');
            
            $this->SetY(180);
            if ($row['entrega_cartilla'] == 1) {
                $ck1 = "X";
                $ck2 = "";
            } else {
                $ck1 = "";
                $ck2 = "X";
            }
            $this->Ln(1);
            $this->SetX($x);
            //$this->Rect($x, $this->GetY(),183,13);
            $this->Ln(45);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(90, $h, "LA EPS ENTREGA CARTILLA INFORMATIVA", 0, 0, 'L');
            
            $this->SetFont('Arial', '', 10);
            $this->Cell(10, $h, "SI", 0, 0, 'C');
            //$this->SetTextColor(0, 0, 0);
            $this->Cell(10, $h, $ck1, 1, 0, 'C');
            $this->Cell(10, $h, "", 0, 0, 'C');
            $this->Cell(10, $h, "NO", 0, 0, 'C');
            //$this->SetTextColor(0, 0, 0);
            $this->Cell(10, $h, $ck2, 1, 1, 'C');
            
            $this->SetFont('Arial', '', 10);
            $this->Ln(7);
            $yf=245;
            //$this->Rect($x, $yf, 44, 15);
            $this->Rect($x + 60, $yf, 25, 25);
            //$this->Rect($x + 92,$yf, 44, 15);
            //$this->Rect($x + 138, $yf, 52, 15);
            /*$h = 4;
            $this->Ln(16);*/
            
            //$this->Line($x + 14, $y + 1, 19, $y + 1);
            $yf=250;
            $this->Line(10,$yf+15,55,$yf+15);
            $this->SetX($x+10);
            $this->SetY($yf+15);
            $this->Cell(44, 7, "Firma Reclamante", 0, 0, 'C');
            
            $this->SetXY($x +87,$yf-2);           
            $this->Cell(44, $h, "Huella digital*", 0, 1, 'L');
            $this->SetXY($x +87,$yf+2);
            $this->Cell(44, $h, "(Indice derecho)", 0, 0, 'L');
            $this->SetXY($x +87,$yf+6);
            $this->Cell(44, $h, "en caso de no", 0, 0, 'L');
            $this->SetXY($x +87,$yf+10);
            $this->Cell(44, $h, "saber firmar o", 0, 0, 'L');
            $this->SetXY($x +87,$yf+14);
            $this->Cell(44, $h, "estar impedido", 0, 1, 'L');
            /*$this->SetX($x + 92);
            $this->Cell(44, $h, strtoupper($usuario['nombres']), 0, 0, 'C');
            $this->SetX($x + 138);
            $this->Cell(52, $h, utf8_decode('Coordinador Atención de Reclamos'), 0, 1, 'C');
            $this->SetX($x + 46);
            $this->Cell(44, 2, "(Indice derecho)", 0, 0, 'C');
            $this->SetX($x + 92);
            $this->Cell(44, $h, "Negociador", 0, 0, 'C');
            $this->SetX($x + 138);
            $this->Cell(44, $h, strtoupper($empresa["razonsocial"]), 0, 1, 'C');
            $this->SetX($x + 92);
            $this->Cell(44, $h, strtoupper($empresa["razonsocial"]), 0, 1, 'C');*/
            
            $this->Ln(4);
            $this->SetXY($x+150,260);
            $this->Cell(30, 6, $this->DecFecha($row['fechaemision']), 1, 1, 'C');
            $this->SetX($x+150);
            $this->Cell(30, $hc, "Fecha", 0, 0, 'C');
            /*$Dia = substr($this->DecFecha($row['fechareg']), 0, 2);
            $Mes = substr($this->DecFecha($row['fechareg']), 3, 2);
            $Anio = substr($this->DecFecha($row['fechareg']), 6, 4);
            global $meses;
            $this->Cell(0, $h, strtoupper($empresa["descripcion"].", ".$Dia." de ".($meses[intval($Mes)])." del ".$Anio."."), 0, 1, 'R');*/
        }

    }

    $nroreclamo = $_GET["nroreclamo"];
    $codsuc = $_GET["codsuc"];

    $objReporte = new clsFormato2();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo);
    $objReporte->Output();
?>