<?php

    include("../../../../../objetos/clsReporte.php");

    class clsFormato1 extends clsReporte {
        
        function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO N° 1";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $tit2 = strtoupper("Solicitud de Atencion de Problemas Particulares");
            $tit3 = strtoupper("Comerciales no relativos a la Facturacion y Problemas Operacionales");
            
            $this->Cell(190, 4, $tit2, 0, 1, 'C');
            $this->Cell(190, 4, $tit3, 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }
        
        function cabecera() {
            $this->Ln(3);
        }

        function Contenido($codsuc, $nroreclamo) {
            global $conexion;
            /*$empresa = $this->datos_empresa($codsuc);*/
            $h = 4;
            $hc= 6;
            $s = 2;
            $x = 13;
            $this->SetFillColor(192, 192, 192);
            //$this->Rect(10, $this->GetY() + 5, 190, 245);
            //echo $nroreclamo;
            $Sql = "SELECT rec.nrosolicitud, rec.nroinscripcion, rec.reclamante, rec.fechaemision, rec.codmedio, rec.nrodocumento, ";
            $Sql .= " tipcal.descripcioncorta || ' ' || cal.descripcion, rec.nrocalle, ";
            $Sql .= " (SELECT descripcion FROM public.ubigeo WHERE codubigeo = ".$this->SubString('rec.codubigeo', 1, 4)."  || '00'), ";
            $Sql .= " (SELECT descripcion FROM public.ubigeo WHERE codubigeo = rec.codubigeo), conp.descripcion, rec.creador,
                rec.glosa, rec.telefono, rec.email, rec.fechareg, ".$this->getCodCatastral("c.").", c.nrodocumento, c.correo,
                rec.correlativo, c.codantiguo ";
            $Sql .= "FROM reclamos.solicitud as rec ";
            $Sql .= "INNER JOIN public.calles as cal on(rec.codemp=cal.codemp and rec.codsuc=cal.codsuc and rec.codcalle=cal.codcalle and rec.codzona=cal.codzona) ";
            $Sql .= "INNER JOIN public.tiposcalle as tipcal on(tipcal.codtipocalle=cal.codtipocalle) ";
            $Sql .= "INNER JOIN reclamos.conceptosreclamos as conp on(rec.codconcepto=conp.codconcepto) ";
            $Sql .= "INNER JOIN catastro.clientes AS c ON (c.codemp = rec.codemp AND c.codsuc =  rec.codsuc AND c.codzona = rec.codzona AND c.codcalle = rec.codcalle AND c.nroinscripcion= rec.nroinscripcion) ";
            $Sql .= "WHERE rec.codsuc = ".$codsuc." and rec.nrosolicitud = ".$nroreclamo." ";

            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array());
            $row = $consulta->fetch();

            $sql1 = "SELECT est.descripcion as estado, horareg
            FROM reclamos.reclamos re 
            INNER JOIN  reclamos.estadoreclamo est ON (est.codestadoreclamo = re.codestadoreclamo)
            WHERE re.codsuc=:codsuc AND re.nroreclamo = :nroreclamo";
            $consulta1 = $conexion->prepare($sql1);
            $consulta1->execute(array(":codsuc"=>$codsuc, ":nroreclamo"=>$nroreclamo));
            $row1 = $consulta1->fetch(); 

            $this->Ln(2);
            
            $codantiguo = $row['codantiguo']; //$this->CodUsuario($codsuc, $row[1]);
            $this->Ln(2);
            $this->SetX($x);
            $this->SetFont('Arial', '', 10);
            $this->Cell(27, $h, utf8_decode("N° Suministro"), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(20, $h, $codantiguo, 0, 0, 'L');
            $this->Cell(17, $h, "", 0, 0, 'L');
            
            $this->SetFont('Arial', '', 10);
            $this->Cell(16, $h, utf8_decode("Código"), 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(20, $h, $row['codcatastro'], 0, 0, 'L');
            
            $this->SetFont('Arial', '', 10);
            $this->SetX($x + 117);
            $this->Cell(40, $h, utf8_decode("Código de Solicitud"), 0, 0, 'R');
            $this->SetFont('Arial', 'B', 12);
            $Anio = substr($this->DecFecha($row['fechaemision']),6,4);
            $this->Cell(30, $h, $Anio." - ".str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 0, 1, 'R');
            
            $this->SetFont('Arial', '', 10);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(67, $hc, "Nombre Reclamente o Representante", 0, 0, 'L');
            $this->SetFont('Arial', '', 9);
            $this->Cell(116, $hc, strtoupper(utf8_decode($row[2])), 1, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->SetFont('Arial', '', 10);
            $this->Cell(190, $h, "Momento de Rregistro de la Solicitud", 0, 0, 'L');
            $this->SetX($x + 98);
            $this->Cell(25, $h, "Fecha", 0, 0, 'R');
            $this->Cell(20, $h, $this->DecFecha($row[3]), 0, 0, 'L');
            
            $hora= $row1['horareg'];
            $hora= date_format(date_create($hora),'h:i a');
            
            $this->Cell(22, $h, "Hora", 0, 0, 'R');
            $this->Cell(20, $h, $hora, 0, 1, 'L');
            $this->Ln(2);
            
            $this->SetX($x);
            $this->SetFont('Arial', '', 9);
            $this->Cell(35, $h, "Documento Identidad", 0, 0, 'L');
            $this->Cell(20, $h, $row['nrodocumento'], 0, 0, 'L');
            $this->Cell(10, $h, "", 0, 0, 'L');
            $this->Cell(16, $h, utf8_decode("Teléfono"), 0, 0, 'L');
            $this->Cell(20, $h, "", 0, 0, 'L');
            $this->Cell(15, $h, "", 0, 0, 'L');
            $this->Cell(15, $h, utf8_decode("Correo"), 0, 0, 'L');
            $this->Cell(52, $h, utf8_decode($row['correo']), 0, 1, 'L');

            $this->SetX($x);
            $this->SetFont('Arial', '', 9);
            // $this->Cell(35, $h, utf8_decode("Estado de Registro"), 0, 0, 'L');
            // $this->SetFont('Arial', 'B', 9);
            // $this->Cell(20, $h, strtoupper(utf8_decode($row1['estado'])), 0, 1, 'L');
            
            $this->Ln(2);
            /*$this->SetFont('Arial', '', 9);
            $personal = "";
            $telefono = "";
            $internet = "";
            $escrito = "";
            if ($row[4] == 1) {
                $personal = "x";
            }
            if ($row[4] == 2) {
                $telefono = "x";
            }
            if ($row[4] == 3) {
                $internet = "x";
            }
            if ($row[4] == 4) {
                $escrito = "x";
            }

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "MODALIDAD DE ATENCION", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, "( ".$personal." ) PERSONAL", 0, 0, 'L');
            $this->Cell(30, $h, "( ".$telefono." ) TELEFONICO", 0, 0, 'L');
            $this->Cell(30, $h, "( ".$internet." ) INTERNET", 0, 0, 'L');
            $this->Cell(30, $h, "( ".$escrito." ) ESCRITO", 0, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)", 0, 0, 'L');
            $this->SetX($x + 118);
            $this->Cell(40, $h, "", 0, 0, 'R');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(20, $h, $row[5], 1, 1, 'L');*/

            //$this->Ln(1);
            $this->SetX($x);
            $this->SetFont('Arial', '', 10);
            $this->Cell(30, $hc, utf8_decode("Razón Social"), 0, 0, 'L');
            $this->Cell(153, $hc, "", 1, 1, 'L');

            $this->Ln(2);
            $this->SetX($x);
            //$this->Cell(35, $h, "DATOS DEL SOLICITANTE", 0, 1, 'L');
            $this->Rect($x,$this->GetY(),183,6);
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(40, $h, utf8_decode("Ubicación del Predio"),0, 0, 'L');
            $this->Cell(123, $h, utf8_decode(strtoupper($row[6]." N° ".$row[7])),0, 0, 'L');
            //$this->Cell(20, $h, strtoupper($row[7]), 0, 0, 'C');
            //$this->Cell(20, $h, "", 1, 0, 'L');
            //$this->Cell(20, $h, "", 1, 1, 'L');
            $this->Ln(7);
            
            /*
            $this->SetX($x);
            $this->Cell(123, $h, "(Calle, Jiron, Avenida )", 1, 0, 'C', true);
            $this->Cell(20, $h, "Nro.", 1, 0, 'C', true);
            $this->Cell(20, $h, "Mz.", 1, 0, 'C', true);
            $this->Cell(20, $h, "Lote", 1, 1, 'C', true);

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(61.5, $h, "", 1, 0, 'L');
            $this->Cell(61.5, $h, $row[8], 1, 0, 'C');
            $this->Cell(60, $h, $row[9], 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(61.5, $h, "(Urbanizacion, barrio)", 1, 0, 'C', true);
            $this->Cell(61.5, $h, "Provincia", 1, 0, 'C', true);
            $this->Cell(60, $h, "Distrito", 1, 1, 'C', true);

            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell(61.5, $h, "", 1, 0, 'L');
            $this->Cell(61.5, $h, $row['telefono'], 1, 0, 'C');
            $this->Cell(60, $h, $row['email'], 1, 1, 'C');


            $this->SetX($x);
            $this->Cell(61.5, $h, "Codigo Postal", 1, 0, 'C', true);
            $this->Cell(61.5, $h, "Telefono / Celular", 1, 0, 'C', true);
            $this->Cell(60, $h, "Correo Electronico (Obligatorio para reclamos via web)", 1, 1, 'C', true);
            */
            $this->SetTextColor(0, 0, 0);
            $this->Ln(1);            
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(22, $h, "SERVICIO", 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $this->MultiCell(80, $h, strtoupper($row[10]), 0, 'J');

            /*$this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "SUCURSAL/ZONAL", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(143, $h, strtoupper($empresa["descripcion"]), 1, 1, 'L');*/

            $this->Ln(1);
            $this->SetY(65);
            $this->SetX($x+115);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(15, $h, "Atendido", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell(35, $h, strtoupper($this->login($row[11])), 0, 0, 'L');
            /*$this->Cell(20, $h, "   FIRMA", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $this->Cell(43, $h, "", 1, 1, 'L');*/

            $this->Ln(6);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(35, $h, utf8_decode("Breve descripción del problema presentado"), 0, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->SetFont('Arial', '', 9);
            $this->MultiCell(183, $h, strtoupper(utf8_decode($row[12])), 0, 'J');
            
            $this->SetY(110);
            $this->Ln(1);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(183, $h, "PROBLEMAS COMERCIALES NO RELATIVOS A LA FACTURACION", 0, 1, 'L');
            $this->Ln(1);
            $this->SetFont('Arial', '', 8);
            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            
            $h=3.4; $h1=8; $h2=140;
            
            $this->Cell($h1, $h, "A", 0, 0, 'C');
            $this->Cell($h2, $h, "Problemas realtivos al acceso al servicio", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "1", 0, 0, 'C');
            $this->Cell($h2, $h, "La instalacion de conexion domiciliaria no se ha realizado en el plazo establecido", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "2", 0, 0, 'C');
            $this->Cell($h2, $h, "Desacuerdo con informe negativo de factibilidad del servicio", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "3", 0, 0, 'C');
            $this->Cell($h2, $h, "No se admite tramite la solicitud", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h + 4, "4", 0, 0, 'C');
            $t = "El servicio prestado no responde a las condiciones contenidas en el estudio de factibilidad, ";
            $t.="el cual forma parte integrante del contrato de presentacion de servicios";
            $this->MultiCell($h2, $h, $t, 0, 'J');
            $this->SetX($x);
            $this->Cell($h1, $h, "5", 0, 0, 'C');
            $this->Cell($h2, $h, "No se suscribe contrato de prestacion de servicios", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "6", 0, 0, 'C');
            $this->Cell($h2, $h, "Otros problemas relativos al contrato", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "7", 0, 0, 'C');
            $this->Cell($h2, $h, "EPS no emite informe de factibilidad dentro de plazo", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "B", 0, 0, 'C');
            $this->Cell($h2, $h, "Problemas relativos a la micromedicion", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h + 4, "1", 0, 0, 'C');
            $t = "El reclamante adquiere un medidor de acuerdo con lo dispuesto en el Reglamento de Calidad de los ";
            $t.="Servicios de Saneamiento solicita a la EPS su instalacion en su conexion domiciliaria, habiendo realizado ";
            $t.="el pago del servicio colateral correspondiente a la instalacion";
            $this->MultiCell($h2, $h, $t, 0, 'J');
            $this->SetX($x);
            $this->Cell($h1, $h, "2", 0, 0, 'C');
            $this->Cell($h2, $h, "La reinstalacion del medidor no se ha realizado en plazo establecido", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "3", 0, 0, 'C');
            $this->Cell($h2, $h, "El medidor ha sido retirado sin previa comunicacion al usuario", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "4", 0, 0, 'C');
            $this->Cell($h2, $h, "El medidor ha sido retirado por razones distintas a su mantenimiento, contrastacion o reposicion ", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "5", 0, 0, 'C');
            $this->Cell($h2, $h, "EPS instala medidor sin afericion inicial o sin entregar al usuario el resultado de la afericion inicial", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "C", 0, 0, 'C');
            $this->Cell($h2, $h, "Problemas relativos a cortes indebidos", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "1", 0, 0, 'C');
            $this->Cell($h2, $h, utf8_decode("El corte o suspención del servicio han sido realizados sin causa justificada"), 0, 1, 'L');            
            $this->SetX($x);
            $this->Cell($h1, $h, "2", 0, 0, 'C');
            $this->Cell($h2, $h, utf8_decode("La rehabilitación de un servicio cerrado no se ha realizado en un plazo establecido, a pesar de cesar la causa del cierre"), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "D", 0, 0, 'C');
            $this->Cell($h2, $h, "Falta de entrega de recibo", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "E", 0, 0, 'C');
            $this->Cell($h2, $h, "Problemas relativos a la informacion", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "1", 0, 0, 'C');
            $this->Cell($h2, $h, utf8_decode("No entregar al usuario la información que de manera obligatoria establece la SUNASS"), 0, 1, 'L');            
            
            $this->Ln(2);
            
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(183, $h, "PROBLEMAS OPERACIONALES", 0, 1, 'L');
            $this->Ln(1);
            $this->SetFont('Arial', '', 8);
            $this->SetTextColor(0, 0, 0);
            $this->SetX($x);
            $this->Cell($h1, $h, "A", 0, 0, 'C');
            $this->Cell($h2, $h, "Filtraciones", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "1", 0, 0, 'C');
            $this->Cell($h2, $h, "Filtracion de agua externas hacia el predio", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "B", 0, 0, 'C');
            $this->Cell($h2, $h, "Problemas en el servicio de agua potable", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "1", 0, 0, 'C');
            $this->Cell($h2, $h, "Fugas en conexion domiciliaria", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "2", 0, 0, 'C');
            $this->Cell($h2, $h, utf8_decode("Negativa de la EPS a realizar mantenimiento por deterioro o daño de caja de medidor o conexion domiciliaria"), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "3", 0, 0, 'C');
            $this->Cell($h2, $h, "Negativa de la EPS a realizar la reubicacion de la conexion domiciliaria que cuenta con estudio de factibilidad ", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "4", 0, 0, 'C');
            $this->Cell($h2, $h, "Negativa de la EPS a realizar ampliacion de diametro que cuenta con estudio de factibilidad positivo", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "C", 0, 0, 'C');
            $this->Cell($h2, $h, "Problemas en el servicio de alcantarillado", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "1", 0, 0, 'C');
            $this->Cell($h2, $h, utf8_decode("Atoro en conexión de alcantarillado"), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "2", 0, 0, 'C');
            $this->Cell($h2, $h, utf8_decode("Negativa de la EPS a realizar mantenimiento por deterioro o daño de caja de registro o conexion domiciliaria"), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "3", 0, 0, 'C');
            $this->Cell($h2, $h, utf8_decode("Negativa de la EPS a realizar ampliación de diámetro que cuenta con estudio de factibilidad positivo"), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell($h1, $h, "3", 0, 0, 'C');
            $this->Cell($h2, $h, utf8_decode("Negativa de la EPS a realizar reubicación de la conexion domiciliaria que cuenta con estudio de factibilidad favorable"), 0, 1, 'L');
            
            $this->Ln(3);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(70, $h, "CONFORMIDAD DEL SOLICITANTE", 0, 0, 'L');
            $this->SetFont('Arial', '', 10);
            $this->Cell(27, $h, utf8_decode("N° Suministro"), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(20, $h, $codantiguo, 0, 0, 'L');
            $this->Cell(20, $h, "", 0, 0, 'L');
            
            $this->SetFont('Arial', '', 10);
            $this->Cell(20, $h, utf8_decode("Código"), 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(20, $h, $row['codcatastro'], 0, 1, 'L');
            $this->Ln(5);
            
			$empresa = $this->datos_empresa($codsuc);
			
            $this->SetFont('Arial', '',9);
            $t = "Mediante el presente, yo............................................................................................................";
            $t.="......................................................";
            $t1="declaro estar conforme con la solucion de la ".strtoupper($empresa["razonsocial"]);
            $t2="al problema descrito en la presente solicitud.";
            $this->Ln(2);
            $this->SetX($x);
            $this->MultiCell(180, $h, $t, 0, 'J');
            $this->SetX($x);
            $this->Cell(0, $h, $t1, 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(0, $h, $t2, 0, 1, 'L');
            $this->Ln(3);
            $this->SetFont('Arial', '',8.5);
            $this->SetXY($x+140,258);
            $this->Cell(0, $h, "FIRMA DEL USUARIO", 0, 1, 'C');
            $this->Ln(4);
            $this->SetX($x+132);            
            $this->Cell(0, $h, "D.N.I.:---------------------------------", 0, 1, 'C');
            $this->Ln(3);
            $this->SetX($x+132); 
            $this->Cell(0, $h, "Fecha:---------------------------------", 0, 1, 'C');
            
            $this->Rect($x+140, 190,42,13);
            $this->SetXY($x+135,192);
            $this->SetFont('Arial', 'B',9);
            $this->Cell(0, $h, "TODO PAGO DE SERVICIO", 0, 1, 'C');
            $this->SetX($x+135);
            $this->Cell(0, $h, "SE REALIZA EN LA", 0, 1, 'C');
            $this->SetX($x+135);
            $this->Cell(0, $h, "EMPRESA", 0, 1, 'C');
            
        }

    }

    $nroreclamo = $_GET["nroreclamo"];
    $codsuc = $_GET["codsuc"];


    $objReporte = new clsFormato1();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo);
    $objReporte->Output();
?>