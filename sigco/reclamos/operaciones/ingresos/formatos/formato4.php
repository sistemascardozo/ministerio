<?php

    include("../../../../../objetos/clsReporte.php");

    class clsFormato1 extends clsReporte {

        function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO N° 4";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 8);

            $tit2 = "Acta de Reunión de Conciliación";
            $this->Cell(190, $h, utf8_decode($tit2), 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            $this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

        function cabecera() {

        }

        function Contenido($codsuc, $nroreclamo) {
            global $conexion;
            $empresa = $this->datos_empresa($codsuc);
            $h = 4;
            $s = 2;
            $x = 10;
            $this->SetFillColor(192, 192, 192);
            //$this->Rect(10, $this->GetY()+5,190,250);

            $Sql = "SELECT rec.nroreclamo, rec.nroinscripcion, rec.reclamante, rec.fechaemision,rec.codmedio,rec.nrodocumento,";
            $Sql .= "tipcal.descripcioncorta || ' ' || cal.descripcion, rec.nrocalle,";
            $Sql .= "(SELECT descripcion FROM public.ubigeo WHERE codubigeo = ".$this->SubString('rec.codubigeo', 1, 4)."  || '00'),";
            $Sql .= "(SELECT descripcion FROM public.ubigeo WHERE codubigeo = rec.codubigeo),conp.descripcion, rec.creador, rec.glosa,
                fechaconciliacion, horainicioc, horafinc, propuesta_eps, propuesta_reclamante, puntos_acuerdo,
                puntos_desacuerdo, obsc, contratacion_m, fin_reclamo, rec.telefono, ".$this->getCodCatastral("c.").",
                rec.correlativo,rec.usuarioconciliador, est.descripcion as estadoreclamo, c.codantiguo  ";
            $Sql .= "FROM reclamos.reclamos as rec ";
            $Sql .= "INNER JOIN public.calles as cal on(rec.codemp=cal.codemp and rec.codsuc=cal.codsuc and rec.codcalle=cal.codcalle and rec.codzona=cal.codzona ) ";
            $Sql .= "INNER JOIN public.tiposcalle as tipcal on(tipcal.codtipocalle=cal.codtipocalle) ";
            $Sql .= "INNER JOIN reclamos.conceptosreclamos as conp on(rec.codconcepto=conp.codconcepto) ";
            $Sql .= "INNER JOIN catastro.clientes AS c ON (c.codemp = rec.codemp AND c.codsuc = rec.codsuc AND c.codzona = rec.codzona AND c.codcalle = rec.codcalle AND c.nroinscripcion= rec.nroinscripcion) ";
            $Sql .= "INNER JOIN reclamos.estadoreclamo AS est ON (rec.codestadoreclamo = est.codestadoreclamo)";
            $Sql .= "WHERE rec.codsuc=:codsuc and rec.nroreclamo=:nroreclamo";

            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array(":codsuc" => $codsuc, ":nroreclamo" => $nroreclamo));
            $row = $consulta->fetch();


            $this->SetFont('Arial', '', 9);

            $this->Ln(2);
            $this->SetX($x);
            $this->Cell(35, $h, "", 0, 0, 'L');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(20, $h, '', 0, 0, 'L');

            //N° de inscripcion
            $codantiguo = $row['codantiguo'];	//$this->CodUsuario($codsuc, $row[1]);
            $this->Ln(2);
            $this->SetX($x);
            $this->Cell(35, $h, "DATOS GENERALES", 0, 0, 'L');
            $this->Cell(30, $h, "", 0, 0, 'C');
            //$this->Cell(20,$h,$codantiguo,0,0,'R');

            $hc = 5;
            $this->Cell(25, $hc, "Cod. Catastral", 0, 0, 'L');
            //$this->Cell(5,$h,":",0,0,'C');
            $this->Cell(35, $hc, $row['codcatastro'], 1, 0, 'C');

            $this->SetX($x + 125);
            $this->Cell(40, $hc, utf8_decode("RECLAMO N° "), 0, 0, 'R');
            //$this->Cell(5,$h,":",0,0,'C');
            $Anio = substr($this->DecFecha($row['fechaemision']),6,4);
            $this->Cell(26, $hc,$Anio." - ".str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 1, 1, 'C');

            // $this->SetFont('Arial', '', 9);
            // $this->Cell(40, $h, utf8_decode("ESTADO DE REGISTRO"), 0, 0, 'L');
            // $this->SetFont('Arial', 'B', 9);
            // $this->Cell(20, $h, strtoupper(utf8_decode($row['estadoreclamo'])), 0, 1, 'L');

            $this->Ln(1);


            $this->Ln(1);
            $this->Rect($x, $this->GetY(), 191, 30);
            $x = 18;
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(135, $h, "NOMBRE DEL RECLAMANTE O REPRESENTANTE", 0, 0, 'L');
            $this->Cell(23, $h, "TELEFONO", 0, 0, 'L');
            $this->Cell(20, $h, $row['telefono'], 1, 1, 'L');
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(175, $hc, strtoupper(utf8_decode($row[2])), 1, 1, 'L');
            $this->SetXY(150, 35);
            $this->Cell(23, $h, strtoupper("inscripcion"), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(23, $h, $codantiguo, 0, 1, 'L');

            $this->SetFont('Arial', '', 9);
            $this->SetX($x);
            $this->Cell(175, $hc, "APELLIDOS Y NOMBRES", 1, 1, 'L');
            $this->Ln(2);

            $this->SetX($x);
            $this->Cell(153, $h, "NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)", 0, 0, 'R');
            $this->SetX($x + 110);
            $this->Cell(40, $h, "", 0, 0, 'R');
            $this->Cell(5, $h, "", 0, 0, 'C');
            $this->Cell(20, $h, $row[5], 1, 1, 'C');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $hc, "RAZON SOCIAL", 0, 0, 'L');
            //$this->Cell(5,$hc,":",0,0,'C');
            $this->Cell(140, $h, "", 1, 1, 'L');

            ///////

            $this->Ln(6);
            $x = 10;
            $this->SetX($x);
            //$this->Rect($x,$this->GetY(),191,27);
            $this->Ln(1);

            $usuario = $this->loginFull($row['usuarioconciliador']?$row['usuarioconciliador']:$row['creador']);
            $x = 18;
			
			$empresa = $this->datos_empresa($codsuc);
			
            //$this->SetX($x);
            $this->Cell(160, $h, "NOMBRE DEL REPRESENTANTE DE ".strtoupper($empresa["razonsocial"]), 0, 1, 'L');
            $this->Ln(1);
            //$this->SetX($x);
            $this->Cell(190, $hc, "APELLIDOS Y NOMBRES", 1, 0, 'L');
            $this->Cell(-2, $hc, "DOCUMENTO DE IDENTIDAD(DNI, CI)", 0, 1, 'R');
            $this->Cell(190, 7, "ABOG. FERNANDO MEDINA MENA", 1, 0, 'L');
            $this->Cell(-10, 7, "23993834", 0, 1, 'R');
            //$this->SetX($x);
            $this->Ln(3);

            /* $this->SetX($x);
              $this->Cell(35,$h,"NUMERO DE DOCUMENTO DE IDENTIDAD(DNI, LE, CI)",0,0,'L');
              $this->SetX($x+110);
              $this->Cell(40,$h,"",0,0,'R');
              $this->Cell(5,$h,"",0,0,'C');
              $this->Cell(20,$h,"",1,1,'L');

              $this->Ln(1);
              $this->SetX($x);
              $this->Cell(115,$h,utf8_decode("FACULTADO POR: (documento, cargo, etc. según el caso)"),0,0,'L');
              $this->Cell(60,$h,"",1,1,'L'); */
            ///
            $x = 10;
            //$this->Ln(6);
            $this->SetX($x);
            $this->Rect($x, $this->GetY(), 191, 7);
            $this->Ln(1);
            $x = 12;
            $this->SetX($x);
            $this->Cell(40, $hc, strtoupper("Fecha Programada"), 0, 0, 'L');
            $this->Cell(20, $hc, $row['fechaconciliacion'], 1, 0, 'C');
            $this->Cell(10, $h, '', 0, 0, 'L');
            $this->Cell(27, $hc, strtoupper("Hora de Inicio"), 0, 0, 'L');
            $this->Cell(20, $hc, $row['horainicioc'], 1, 0, 'C');
            $this->Cell(16, $h, '', 0, 0, 'L');
            $this->Cell(33, $hc, strtoupper(utf8_decode("Hora de Término")), 0, 0, 'L');
            $this->Cell(20, $hc, $row['horafinc'], 1, 0, 'C');
            /////
            /* $x=10;
              $this->Ln(8);
              $this->SetX($x);
              $this->Cell(160,$h,"MATERIA DE RECLAMO",0,1,'L');
              $this->Rect($x,$this->GetY(),191,32);
              $this->Ln(3);

              $x=13;
              $this->SetX($x);
              $this->Rect(98,$this->GetY(),95,8);
              $this->Cell(5,$h*2,utf8_decode("N°"),1,0,'L',true);
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(60,$h*2,utf8_decode("Tipo de reclamo*"),1,0,'C',true);
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(95,$h,utf8_decode("Descripción del reclamo"),0,1,'C',true);
              $this->SetX($x);
              $this->Cell(85,$h,utf8_decode(""),0,0,'L');
              $this->SetFont('Arial','',7);
              $this->Cell(95,$h,utf8_decode("(mes reclamado, monto, incumplimiento de la EPS, etc., según el caso)"),0,1,'C',true);
              $this->SetFont('Arial','',9);
              $this->SetX($x);
              $this->Cell(5,$h,utf8_decode(""),1,0,'L');
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(60,$h,utf8_decode(""),1,0,'C');
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(95,$h,utf8_decode(""),1,1,'C');
              $this->SetX($x);
              $this->Cell(5,$h,utf8_decode(""),1,0,'L');
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(60,$h,utf8_decode(""),1,0,'C');
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(95,$h,utf8_decode(""),1,1,'C');
              $this->SetX($x);
              $this->Cell(5,$h,utf8_decode(""),1,0,'L');
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(60,$h,utf8_decode(""),1,0,'C');
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(95,$h,utf8_decode(""),1,1,'C');
              $this->SetX($x);
              $this->Cell(5,$h,utf8_decode(""),1,0,'L');
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(60,$h,utf8_decode(""),1,0,'C');
              $this->Cell(10,$h,utf8_decode(""),0,0,'L');
              $this->Cell(95,$h,utf8_decode(""),1,1,'C');
              $this->SetX($x);
              $this->Cell(5,$h,utf8_decode(""),0,0,'L');
              $this->SetFont('Arial','',6);
              $this->Cell(10,$h,utf8_decode("*Pueden colocarse los numerales indicados como 'Tipo de Reclamo' en el Formato N° 1"),0,0,'L');
              $this->SetFont('Arial','',7); */
            ///////////
            $x = 10;
            $this->Ln(9);
            $this->SetX($x);
            //$this->Rect($x,$this->GetY()+5,188,15);

            $this->SetX($x);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(160, $hc, "PROPUESTA DE LA EPS", 0, 1, 'L');
            $this->SetFont('Arial', '', 9);
            $this->SetX($x);

            $this->MultiCell(183, $h, utf8_decode(strtoupper($row['propuesta_eps'])), 0, 'J');
            ///
            $x = 10;
            $y = 112;
            $this->SetY($y);
            $this->Ln(3);
            $this->SetX($x);
            //$this->Rect($x,$this->GetY()+5,188,15);

            $this->SetX($x);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(160, $hc, "PROPUESTA DEL RECLAMANTE", 0, 1, 'L');
            $this->SetFont('Arial', '', 9);
            $this->SetX($x);
            $this->MultiCell(183, $h, utf8_decode(strtoupper($row['propuesta_reclamante'])), 0, 'J');
            ///
            $x = 10;
            $this->SetY($y + 3 + 5 * $h);
            $this->Ln(3);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(105, $h, "PUNTOS DE ACUERDO", 0, 0, 'L');
            $this->Cell(85, $h, "PUNTOS DE DESACUERDO", 0, 1, 'L');
            $this->SetFont('Arial', '', 9);
            $this->SetX($x);
            //$this->Rect($x,$this->GetY(),87,18);
            $this->Rect($x + 100, $this->GetY(), 0.01, 25);
            $x = 18;
            $this->Ln(1);
            $ytem = $this->GetY();
            $this->SetX($x - 8);
            $this->MultiCell(97, $hc, utf8_decode(strtoupper($row['puntos_acuerdo'])), '0', 'J');
            $this->SetY($ytem);
            $this->SetX($x + 95);
            $this->MultiCell(87, $hc, utf8_decode(strtoupper($row['puntos_desacuerdo'])), '0', 'J');

            /////
            if (trim($row['obsc']) == '') {
                $ck1 = "";
                $ck2 = "";
            } else {
                if ($row['fin_reclamo'] == 1) {
                    $ck1 = "X";
                    $ck2 = "";
                } else {
                    $ck1 = "";
                    $ck2 = "X";
                }
            }


            $this->SetFont('Arial', '', 9);
            //////
            ///

            $x = 10;
            $this->SetY($y + 9 + 14 * $h);
            $this->Ln(5);
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(160, $hc, utf8_decode("CONCILIACIÓN"), 0, 1, 'L');
            $this->SetFont('Arial', '', 9);
            $this->Rect($x, $this->GetY(), 191, 20);
            $x = 18;
            $this->SetX($x - 5);
            $this->MultiCell(182, $h, utf8_decode(strtoupper($row['obsc'])), '0', 'J');

            ///
            $x = 13;
            $this->SetY($y + 35 + 15 * $h);
            $this->Ln(2);
            $this->SetX($x);
            $this->Cell(110, $h, utf8_decode("¿SUBSISTE EL RECLAMO?"), 0, 0, 'R');
            $this->Cell(30, $h, "", 0, 0, 'C');
            $this->Cell(10, $h, utf8_decode("SI"), 0, 0, 'C');
            $this->Cell(10, $h, utf8_decode($ck2), 1, 0, 'C');
            $this->Cell(10, $h, utf8_decode("NO"), 0, 0, 'C');
            $this->Cell(10, $h, utf8_decode($ck1), 1, 1, 'C');
            $this->Ln(1);

            $this->MultiCell(190, $h * 6, "", 0, 'J');
            $this->Cell(110, $h, utf8_decode("OBSERVACIONES DEL USUARIO"), 0, 0, 'L');
            $x = 13;
            $this->Ln(10);
            $hf1 = 55;
            $hf2 = 60;
            $hf3 = 50;
            $this->Rect($x, $this->GetY(), $hf1, 13);
            //$this->Rect($x+46,$this->GetY(),44,13);
            $this->Rect($x + 67, $this->GetY(), $hf2, 13);
            $this->Rect($x + 138, $this->GetY(), $hf3, 13);

            $this->Ln(15);
            $this->SetX($x);
            $this->Cell($hf1, $hc, strtoupper($usuario['nombres']), 0, 0, 'C');
            $this->SetX($x + 46);
            //$this->Cell(44,$h,"Huella digital*",0,0,'C');
            $this->SetX($x + 67);
            $this->Cell($hf2, $h, utf8_decode('Coordinador Atención de Reclamos'), 0, 0, 'C');
            $this->SetX($x + 138);
            $this->Cell($hf3, $h, "CLIENTE", 0, 1, 'C');
            $this->SetX($x + 46);
            //$this->Cell(44,2,"(Indice derecho)",0,0,'C');
            $this->SetX($x);
            $this->Cell($hf1, $hc, "Negociador", 0, 0, 'C');
            $this->SetX($x + 67);
            $this->Cell($hf2, $h, strtoupper($empresa["razonsocial"]), 0, 1, 'C');
            $this->SetX($x);
            $this->Cell($hf1, $hc, strtoupper($empresa["razonsocial"]), 0, 1, 'C');
            $this->Ln(5);
            //$this->SetX($x);
            //$this->Cell(183,$h,"* En caso de no saber firmar o estar impedido bastara con la huella digital.",0,1,'L');
            $this->SetY(274);
            $Dia = substr($this->DecFecha($row['fechaconciliacion']), 0, 2);
            $Mes = substr($this->DecFecha($row['fechaconciliacion']), 3, 2);
            $Anio = substr($this->DecFecha($row['fechaconciliacion']), 6, 4);
            global $meses;
            $h = 2;
            if ($row['fechaconciliacion'] == '')
                $this->Cell(0, $h, strtoupper($empresa["descripcion"].",            de                      del                 ."), 0, 1, 'R');
            else
                $this->Cell(0, $h, strtoupper($empresa["descripcion"].", ".$Dia." de ".($meses[intval($Mes)])." del ".$Anio."."), 0, 1, 'R');
        }

    }

    $nroreclamo = $_GET["nroreclamo"];
    $codsuc = $_GET["codsuc"];


    $objReporte = new clsFormato1();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo);
    $objReporte->Output();
?>
