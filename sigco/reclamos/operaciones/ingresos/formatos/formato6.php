<?php

    include("../../../../../objetos/clsReporte.php");

    class clsFormato3 extends clsReporte {
        
        function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(7);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO N° 6";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 7);
            $hc = 5;
            $tit2 = strtoupper("Resumen de Acta de Inspeccion Externa");
            //$tit3 = strtoupper("Comerciales no relativos a la Facturacion y Problemas Operacionales");
            
            $this->Cell(190, 4, $tit2, 0, 1, 'C');
            //$this->Cell(190, 4, $tit3, 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C');
            $this->cabecera();
        }
        
        function cabecera() {

        }

        function Contenido($codsuc, $nroreclamo) {
            global $conexion;
            $this->SetFillColor(192, 192, 192);
            $empresa = $this->datos_empresa($codsuc);
            $h = 4;
            $s = 2;
            $x = 13;

            $this->Rect(10, $this->GetY() + 3, 190, 247);

            $Sql = "select rec.nroreclamo,rec.nroinscripcion,rec.reclamante,rec.telefono,rec.nrodocumento,";
            $Sql .= "tipcal.descripcioncorta || ' ' || cal.descripcion,rec.nrocalle,";
            $Sql .= "(select descripcion from public.ubigeo where codubigeo = ".$this->SubString('rec.codubigeo', 1, 4)." || '00'),";
            $Sql .= "(select descripcion from public.ubigeo where codubigeo = rec.codubigeo),rec.creador,rec.glosa, ";
            $Sql .= "rec.direccion1,rec.nrocalle1,rec.manzana1,rec.lote1,rec.correlativo,rec.fechaemision, c.codantiguo ";
            $Sql .= "from reclamos.reclamos as rec ";
			$Sql .= " INNER JOIN catastro.clientes c ON (rec.codsuc = c.codsuc) AND (rec.nroinscripcion = c.nroinscripcion) ";
            $Sql .= "inner join public.calles as cal on(rec.codemp=cal.codemp and rec.codsuc=cal.codsuc and rec.codcalle=cal.codcalle and rec.codzona=cal.codzona ) ";
            $Sql .= "inner join public.tiposcalle as tipcal on(tipcal.codtipocalle=cal.codtipocalle) ";
            $Sql .= "where rec.codsuc=? and rec.nroreclamo=?";

            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array($codsuc, $nroreclamo));
            $row = $consulta->fetch();

            /*$this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO Nro. 6";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');

            $this->Ln(1);
            $this->SetFont('Arial', '', 7);

            $tit2 = "Resumen de Acta de Inspeccion Externa";
            $this->Cell(190, $h, $tit2, 0, 1, 'C');*/

            $this->Ln(6);
            $this->SetX($x);
            $this->Cell(35, $h, "Nro. DE SUMINISTRO", 0, 0, 'L');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $codantiguo = $row['codantiguo'];	//$this->CodUsuario($codsuc, $row[1]);
            $this->Cell(20, $h, $codantiguo, 1, 0, 'L');

            $this->SetX($x + 118);
            $this->Cell(40, $h, "CODIGO DEL RECLAMO Nro.", 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $Anio = substr($this->DecFecha($row['fechaemision']),6,4);
            $this->Cell(20, $h,$Anio." - ".str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 1, 1, 'R');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(138, $h, "INFORME SOBRE EL SUMINISTRO", 0, 1, 'L');

            $this->Rect($x, $this->GetY(), 183, 85);
            $x = 20;
            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "ESTADO DEL MEDIDOR", 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(35, $h, "", 1, 0, 'L');
            $this->Cell(35, $h, "", 1, 0, 'L');
            $this->Cell(32, $h, "", 1, 0, 'L');
            $this->Cell(35, $h, "", 1, 0, 'L');
            $this->Cell(35, $h, "", 1, 1, 'L');

            $this->SetX($x);
            $this->SetFont('Arial', 'B', 7);

            $this->Cell(35, $h, utf8_decode("Medidor N°"), 1, 0, 'C');
            $this->Cell(35, $h, utf8_decode("Diametro N°"), 1, 0, 'C');
            $this->Cell(32, $h, "Lectura", 1, 0, 'C');
            $this->Cell(35, $h, "Funciona", 1, 0, 'C');
            $this->Cell(35, $h, "No Funciona", 1, 1, 'C');

            $this->Ln(5);
            $this->SetX($x);
            $this->Cell(30, $h, utf8_decode("FUGA EN CAJA"), 0, 0, 'L');
            $this->Cell(65, $h, utf8_decode("(EN CASO DE HABER FUGA EN LA CAJA)"), 0, 0, 'L');
            $this->SetFont('Arial', '', 7);
            $this->Cell(50, $h, utf8_decode("OBSERVACIONES SOBRE EL MEDIDOR"), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(10, $h, utf8_decode("SI"), 0, 0, 'L');
            $this->Cell(14, $h, '', 1, 0, 'L');
            $this->Cell(5, $h, '', 0, 0, 'L');
            $this->Cell(31, $h, '', 1, 0, 'L');
            $this->Cell(31, $h, '', 1, 0, 'L');
            $this->Cell(5, $h, '', 0, 0, 'L');
            $this->Rect($this->GetX(), $this->GetY(), 75, 25);
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(10, $h, utf8_decode("NO"), 0, 0, 'L');
            $this->Cell(14, $h, '', 1, 0, 'L');
            $this->Cell(5, $h, '', 0, 0, 'L');
            $this->Cell(31, $h, 'Antes del Medidor', 1, 0, 'L');
            $this->Cell(31, $h, utf8_decode('Después del Medidor'), 1, 0, 'L');
            $this->Cell(7, $h, '', 0, 0, 'L');
            $this->Cell(70, 0.01, '', 1, 1, 'C');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(98, $h, '', 0, 0, 'L');
            $this->Cell(70, 0.01, '', 1, 0, 'C');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(98, $h, utf8_decode("UBICACIÓN DE LA CAJA DL MEDIDOR"), 0, 0, 'L');
            $this->Cell(70, 0.01, '', 1, 0, 'C');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(16, $h, '', 1, 0, 'L');
            $this->Cell(7, $h, '', 0, 0, 'L');
            $this->Cell(70, 0.01, '', 1, 1, 'C');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(15, $h, 'Interior', 1, 0, 'C');
            $this->Cell(15, $h, 'Vereda', 1, 0, 'C');
            $this->Cell(15, $h, 'Frente', 1, 0, 'C');
            $this->Cell(15, $h, 'Lateral', 1, 0, 'C');
            $this->Cell(15, $h, 'Pista', 1, 0, 'C');
            $this->Cell(16, $h, 'Distante', 1, 0, 'C');
            $this->Cell(7, $h, '', 0, 0, 'L');
            $this->Cell(70, 0.01, '', 1, 1, 'C');
            $this->Ln(6);
            $this->SetX($x);
            $this->Cell(96, $h, utf8_decode("ESTADO DEL SUMINISTRO"), 0, 0, 'L');
            $this->Cell(75, $h, utf8_decode("TIPO DE ABASTECIMIENTO"), 0, 1, 'C');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(16, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(7, $h, '', 0, 0, 'L');
            $this->Cell(12, $h, '', 1, 0, 'L');
            $this->Cell(16, $h, '', 1, 0, 'L');
            $this->Cell(14, $h, '', 1, 0, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(15, $h, 'Vigente', 1, 0, 'C');
            $this->Cell(15, $h, 'Cerrado', 1, 0, 'C');
            $this->Cell(15, $h, 'Tapado', 1, 0, 'C');
            $this->Cell(15, $h, 'Directo', 1, 0, 'C');
            $this->Cell(15, $h, 'Retirado', 1, 0, 'C');
            $this->Cell(16, $h, 'No Ubicado', 1, 0, 'C');
            $this->Cell(15, $h, 'Medidor', 1, 0, 'C');
            $this->Cell(15, $h, 'Niple', 1, 0, 'C');
            $this->Cell(7, $h, '', 0, 0, 'L');
            $this->Cell(12, $h, 'Continuo', 1, 0, 'L');
            $this->Cell(16, $h, 'Discontinuo', 1, 0, 'L');
            $this->Cell(14, $h, utf8_decode('N° de horas'), 1, 0, 'C');
            $this->Ln(6);
            $this->SetX($x);
            $this->Cell(96, $h, utf8_decode("OBSERVACIONES SOBRE EL SUMINISTRO"), 0, 1, 'L');
            $this->Ln(3);
            $this->SetX($x);
            $this->Cell(170, 0.01, '', 1, 0, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(170, 0.01, '', 1, 0, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(170, 0.01, '', 1, 0, 'L');

            $x = 13;
            $this->Ln(5);
            $this->SetX($x);
            $this->Cell(138, $h, "CIERRES Y REAPERTURAS / INSPECCION DE SERVICIOS CERRADO", 0, 1, 'L');
            $this->Rect($x, $this->GetY(), 183, 30);
            $x = 16;
            $this->Ln(5);
            $this->SetX($x);
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, 'Codigo de Acceso', 1, 0, 'C');
            $this->Cell(26, $h, 'Fecha', 1, 0, 'C');
            $this->Cell(26, $h, 'Lectura', 1, 0, 'C');
            $this->Cell(26, $h, 'Operativo', 1, 0, 'C');
            $this->Cell(45, $h, 'Comentarios', 1, 1, 'C');
            $this->SetX($x);
            $this->Cell(26, $h, 'Actividad', 1, 0, 'L');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(45, $h, '', 1, 1, 'C');
            $this->SetX($x);
            $this->Cell(26, $h, 'Cierre', 1, 0, 'L');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(45, $h, '', 1, 1, 'C');
            $this->SetX($x);
            $this->Cell(26, $h, 'Actividad', 1, 0, 'L');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(45, $h, '', 1, 1, 'C');
            $this->SetX($x);
            $this->Cell(26, $h, utf8_decode('Supervición'), 1, 0, 'L');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(26, $h, '', 1, 0, 'C');
            $this->Cell(45, $h, '', 1, 1, 'C');

            $x = 13;
            $this->Ln(5);
            $this->SetX($x);
            $this->Cell(138, $h, utf8_decode("DATOS DE LA PERSONA PRESENTE EN LA INSPECCIÓN"), 0, 1, 'L');
            $this->Rect($x, $this->GetY(), 183, 48);
            $x = 16;
            $this->Ln(3);
            $this->SetX($x);
            $this->Cell(126, $h, utf8_decode("Nombre de la persona presente en la inspección"), 0, 0, 'L');
            $this->Cell(20, $h, utf8_decode("Reclamante:"), 0, 0, 'L');
            $this->Cell(6, $h, utf8_decode("SI"), 0, 0, 'R');
            $this->Cell(8, $h, '', 1, 0, 'L');
            $this->Cell(6, $h, utf8_decode("NO"), 0, 0, 'L');
            $this->Cell(8, $h, '', 1, 1, 'L');
            $this->Ln(3);
            $x = 20;
            $this->SetX($x);
            $this->Cell(18, $h, utf8_decode("Propietario"), 0, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 0, 0, 'L');
            $this->Cell(15, $h, utf8_decode("Inquilino"), 0, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(20, $h, '', 0, 0, 'L');
            $this->Cell(15, $h, utf8_decode("Residente"), 0, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(17, $h, '', 0, 0, 'L');
            $this->Cell(10, $h, utf8_decode("Otro"), 0, 0, 'L');
            $this->Cell(15, $h, '', 1, 0, 'L');
            $this->Cell(15, $h, '', 0, 0, 'L');

            $this->Ln(6);
            $this->SetX($x);
            $this->Cell(67, $h, utf8_decode("NÚMERO DE DOCUMENTO DE IDENTIDAD (DNI,LE,CI)"), 0, 0, 'L');
            $this->Cell(103, $h, '', 1, 0, 'L');

            $this->Ln(6);
            $this->SetX($x);
            $this->Cell(67, $h, utf8_decode("Observaciones:"), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(20, $h, '', 0, 0, 'L');
            $this->Cell(150, 0.01, '', 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(170, 0.01, '', 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(170, 0.01, '', 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(170, 0.01, '', 1, 1, 'L');
            $this->Ln(4);
            $this->SetX($x);
            $this->Cell(170, 0.01, '', 1, 1, 'L');

            $this->Ln(18);
            $y = $this->GetY();
            $this->SetY($y);
            $this->Rect($x + 10, $this->GetY() - 6, 70, 15);
            $this->Rect($x + 105, $this->GetY() - 6, 70, 15);

            $this->Ln(10);
            $this->SetX($x + 10);
            $this->Cell(70, $h, utf8_decode("Firma del Reclamante o Persona Presente en la Inspección*"), 0, 0, 'C');
            $this->SetX($x + 105);
            $this->Cell(70, $h, "Persona Autorizada por la EPS para la Inspeccion", 0, 1, 'C');

            $this->Ln(8);
            $this->SetX($x + 10);
            $this->Cell(15, $h, "Fecha", 0, 0, 'L');
            $this->Cell(30, $h, "__/__/_____", 0, 0, 'L');
            $this->Cell(25, $h, "Hora Inicio", 0, 0, 'R');
            $this->Cell(20, $h, "", 'B', 0, 'R');
            $this->Cell(25, $h, "Hora Final", 0, 0, 'R');
            $this->Cell(20, $h, "", 'B', 0, 'R');
            $this->Ln(8);
            $this->SetX($x + 10);
            $this->SetFont('Arial', '', 6);
            $this->Cell(70, $h, utf8_decode("Observación: La firma no implica acuerdo con el contenido del acta."), 0, 0, 'L');
        }

    }

    $nroreclamo = $_GET["nroreclamo"];
    $codsuc = $_GET["codsuc"];

    $objReporte = new clsFormato3();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo);
    $objReporte->Output();

?>