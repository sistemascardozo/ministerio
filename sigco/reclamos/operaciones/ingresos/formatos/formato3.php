<?php

    include("../../../../../objetos/clsReporte.php");

    class clsFormato3 extends clsReporte {

        function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "FORMATO N° 3";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(1);
            $this->SetFont('Arial', '', 9);
            $tit2 = "Resumen Historico de la Facturacion del Reclamante";
            $this->Cell(190, 6, $tit2, 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SESSION['path']."/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

        function cabecera() {
            $this->Ln(3);
        }

        function Contenido($codsuc, $nroreclamo, $nroinscrpcion) {
            global $conexion, $meses;

            $h = 4;
            $hc= 5;
            $s = 2;
            $x = 10;
            $this->SetFont('Arial', '', 10);
            /* $this->Rect(10, $this->GetY()+8,190,247);
              $this->Rect(11, $this->GetY()+14,188,11);
              $this->Rect(11, $this->GetY()+26,188,6.5);
              $this->Rect(11, $this->GetY()+33.5,188,180);
              $this->Rect(20, $this->GetY()+230,80,10);
              $this->Rect(110, $this->GetY()+230,80,10); */
            $SqlRec="SELECT 
                rec.nroreclamo,
                rec.nroinscripcion,
                rec.creador, rec.glosa, 
                rec.fechaemision,rec.correlativo,
                est.descripcion as estadoreclamo

                FROM reclamos.reclamos as rec 
                INNER JOIN reclamos.conceptosreclamos as conp on(rec.codconcepto=conp.codconcepto) 
                INNER JOIN catastro.clientes AS c ON (c.codemp = rec.codemp AND c.codsuc = rec.codsuc AND c.codzona = rec.codzona AND c.codcalle = rec.codcalle AND c.nroinscripcion= rec.nroinscripcion) 
                INNER JOIN reclamos.estadoreclamo AS est ON (rec.codestadoreclamo = est.codestadoreclamo)
                WHERE
                rec.codsuc = ".$codsuc." AND rec.nroreclamo = ".$nroreclamo." ";
            $rowrec=$conexion->query($SqlRec)->fetch();


            $Sql = "SELECT c.catetar, c.domestico, c.social, c.comercial, c.estatal, c.industrial, cx.tipopredio, t.nomtar,
                ".$this->getCodCatastral("c.").", c.codsector, c.codantiguo
                from catastro.clientes as c
                INNER JOIN catastro.conexiones as cx on(c.codemp=cx.codemp and c.codsuc=cx.codsuc and c.nroinscripcion=cx.nroinscripcion)
                INNER JOIN facturacion.tarifas as t on(c.codemp=t.codemp and c.codsuc=t.codsuc and c.catetar=t.catetar)
                WHERE c.codsuc=? and c.nroinscripcion=?";
            $consulta = $conexion->prepare($Sql);
            $consulta->execute(array($codsuc, $nroinscrpcion));
            $row = $consulta->fetch();
            
            $sqlunid="SELECT Count(un.catetar)
                FROM catastro.unidadesusoclientes AS un
                INNER JOIN catastro.clientes AS cl ON cl.nroinscripcion = un.nroinscripcion AND cl.codemp = un.codemp AND cl.codsuc = un.codsuc
                INNER JOIN facturacion.categoriatarifaria AS ca ON ca.codcategoriatar = un.catetar
                WHERE un.codsuc =".$codsuc." AND un.nroinscripcion = '".$nroinscrpcion."' ";
            $rCat = $conexion->query($sqlunid)->fetch();            
            //$unidades = $row["domestico"] + $row["social"] + $row["comercial"] + $row["estatal"] + $row["industrial"];
            $unidades = $rCat[0];
            $codantiguo = $row[10];	//$this->CodUsuario($codsuc, $nroinscrpcion);
            $this->Ln(1);

            $this->SetX($x);
            $this->SetFont('Arial', '', 9);
            $this->Cell(45, $h, utf8_decode("ESTADO DE REGISTRO:"), 0, 0, 'L');
            $this->SetFont('Arial', 'B', 9);
            $this->Cell(20, $h, strtoupper(utf8_decode($rowrec['estadoreclamo'])), 0, 1, 'L');

            $this->Ln(1);
            $this->SetX($x);
            $this->Cell(35, $h, "DATOS DEL CLIENTE :", 0, 1, 'L');
            $this->Rect($x,$this->GetY(),191,10);
            $this->SetFont('Arial', '', 11);
            
            $this->Ln(3);
            $this->Cell(18, $h, utf8_decode("Código"), 0, 0, 'L');            
            $this->Cell(50, $h, $row["codcatastro"], 0, 0, 'L');            
            
            $this->Cell(22, $h, utf8_decode("Inscripción"), 0, 0, 'L');            
            $this->Cell(20, $h, $codantiguo, 0, 0, 'R');

            $this->SetX($x + 126);
            $this->Cell(40, $h, utf8_decode("RECLAMO N° "), 0, 0, 'R');
            $this->Cell(5, $h, ":", 0, 0, 'C');
            $Anio = substr($this->DecFecha($rowrec['fechaemision']),6,4);
            $this->Cell(20, $h,$Anio." - ".str_pad($rowrec['correlativo'], 6, "0", STR_PAD_LEFT), 0, 1, 'R');
            $this->Ln(6);
                        
            $this->Rect($x,$this->GetY(),191,10);
            $this->Ln(2.5);
            $this->SetFont('Arial', '', 10);
            $this->SetX($x);
            $this->Cell(45, $hc, utf8_decode("Categoría Tarifaria"), 0, 0, 'L');
            $this->Cell(30, $hc, $row["nomtar"], 1, 0, 'C');

            $this->SetX($x+95);
            $this->Cell(60, $h, "Numero de Unidades de Uso", 0, 0, 'L');
            $this->Cell(10, $h, $unidades, 0, 1, 'R');
            $this->Ln(6);
            
            $tipopredio1 = "";
            $tipopredio2 = "";
            if ($row["tipopredio"] == 1) {
                $tipopredio1 = "x";
            } else {
                $tipopredio2 = "x";
            }
            
            $this->SetFont('Arial', '', 10);
            $this->Rect($x,$this->GetY(),191,10);
            $this->Ln(3);
            $this->SetX($x);
            $this->Cell(40, $h, "TIPO DE PREDIO :", 0, 0, 'L');            
            
            $this->Cell(28, $h, "A. UniFamiliar", 0, 0, 'L');
            $this->Cell(5, $h, "[ ".$tipopredio1." ]", 0, 0, 'C');
            $this->Cell(10, $h, "", 0, 0, 'L');
            $this->Cell(30, $h, "B. MultiFamiliar", 0, 0, 'L');
            $this->Cell(5, $h, "[ ".$tipopredio2." ]", 0, 1, 'C');
            $this->Ln(3.5);
            
            
            /*$this->SetX($x - 1);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(100, $h, "Datos de la Facturacion", 0, 1, 'L');
            $this->SetFont('Arial', '', 10);
            //meses Reclamados
                $top = '';
                $limit = "limit 1";
            
            $sqlmeses = "SELECT ".$top." c.mes,c.anio,c.estadofacturacion,c.tipofacturacion,c.impmes,c.impdeuda,c.impigv,
                    c.consumofact,c.consumo,c.nrofacturacion
                from facturacion.cabfacturacion as c
                INNER JOIN reclamos.mesesreclamados as mr ON (mr.codemp=c.codemp AND mr.codsuc=c.codsuc AND mr.nrofacturacion=c.nrofacturacion)
                where c.codsuc=? AND mr.nroreclamo=? order by c.nrofacturacion ".$limit;

            $consulta_meses = $conexion->prepare($sqlmeses);
            $consulta_meses->execute(array($codsuc, $nroreclamo));
            $items_meses = $consulta_meses->fetch();
            if ($items_meses["tipofacturacion"] == 0) {
                $tf = "Med.";
            }
            if ($items_meses["tipofacturacion"] == 1) {
                $tf = "Cp.";
            }
            if ($items_meses["tipofacturacion"] == 2) {
                $tf = "Asig.";
            }
            $totfact = $items_meses["impmes"] + $items_meses["impdeuda"] + $items_meses["impigv"];
            $diapago = $this->recuperar_dia_pago($codsuc, $nroinscrpcion, $items_meses["nrofacturacion"]);
            $this->SetX($x - 1);
            $this->SetWidths(array(20, 15, 20, 20, 30, 15, 15, 30, 21));
            $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "C"));
            $this->Row(array($meses[$items_meses['mes']], $items_meses['anio'], $items_meses["estadofacturacion"] == 1 ? "No" : "Tot", "--/--/----", $tf, number_format($totfact, 2),
                number_format($items_meses["consumofact"], 0),
                number_format($items_meses["consumo"], 0),
                $diapago));

            $this->SetX($x - 1);
            $this->SetWidths(array(20, 15, 20, 20, 30, 15, 15, 30, 21));
            $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "C"));
            $this->SetFont('Arial', '', 7);
            $this->Row(array("Mes Reclamado",
                utf8_decode("Año"),
                "Pagado? (Tot/Parc/No)",
                "Fecha Vencimiento",
                "Forma de Facturacion (Asig/Med/Cp/Prom)",
                "Importe Facturado",
                "Volumen Facturado",
                "Volumen Leido (de ser aplicable)",
                "Tarifa Aplicada"));
            */
            $this->Ln(3);
            $this->Rect($x,$this->GetY(),191,23);
            $this->Ln(1);
            $this->SetFont('Arial', '', 9);
            $this->Cell(100, 7, "DATOS DEL ABASTECIMIENTO", 0, 1, 'L');
            $this->Ln(2);
            $this->SetX($x+3);
            $codsector= $row['codsector'];
            $codsector = substr("0", 0, 2 - strlen($codsector)).$codsector;
            $this->Cell(35, 5,$codsector , 1, 1, 'C');
            $this->SetX($x+3);
            $this->Cell(35, 5, utf8_decode("Código del Sector"), 1, 1, 'C');
            $this->SetXY($x + 50,73);
            $this->Cell(35, 4, utf8_decode("FRECUENCIA"), 0, 1, 'L');
            $this->SetX($x+50);
            $this->Cell(35, 4, utf8_decode("DEL SEVICIO"), 0, 1, 'L');
            
            $this->SetXY($x +75,71);
            $this->Cell(25, 5, utf8_decode("A. Diario"), 0, 0, 'L');
            $this->Cell(5, 5, utf8_decode(""), 1, 1, 'L');
            $this->Ln(2);
            $this->SetX($x +75);
            $this->Cell(25, 5, utf8_decode("B. No Diario"), 0, 0, 'L');
            $this->Cell(5, 5, utf8_decode(""), 1, 1, 'L');
            
            $this->SetXY($x + 123,74);
            $this->Cell(28, 4, utf8_decode("CONTINUIDAD"), 0, 1, 'L');
            
            $this->SetXY($x + 150,70);
            $this->Cell(24, 5, utf8_decode("A. 24 Horas"), 0, 0, 'L');
            $this->Cell(5, 5, utf8_decode(""), 1, 1, 'L');
            $this->Ln(2);
            $this->SetX($x +150);
            $this->Cell(24, 5, utf8_decode("B. < 24 Horas"), 0, 0, 'L');
            $this->Cell(5, 5, utf8_decode(""), 1, 1, 'L');            
            
            $this->Ln(10);
            $this->SetX($x);
            //$this->SetXY($x + 48,70);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(100, 7, "DATOS DE LA FACTURACION", 0, 1, 'L');
            $this->SetFont('Arial', 'B', 10);
            $this->Ln(2);
            $h=5;
            $this->SetWidths(array(6, 21, 10, 20, 20, 30, 18, 15, 30, 21));
            $this->SetAligns(array("C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"));
            $this->SetFont('Arial', '', 8);
            $this->Row(array(utf8_decode("N°"),
                "Mes",
                utf8_decode("Año"),
                "Pagado? (Tot/Parc/No)",
                "Fecha Vencimiento",
                "Forma de Facturacion (Asig/Med/Cp/Prom)",
                "Importe Facturado",
                "Volumen Facturado",
                "Volumen Leido (de ser aplicable)",
                "Fecha de Pago"));
            $this->Ln(2);
            $this->SetFont('Arial', '', 7);
            $count = 0;
            $totfact = 0;

                $top = '';
                $limit = "limit 12";

            $sqlF = "SELECT ".$top." c.mes,c.anio,c.estadofacturacion,
                    c.tipofacturacion,sum(d.importe) as facturado,
                    c.consumofact,c.consumo,c.nrofacturacion,c.fechavencimiento
                from facturacion.cabfacturacion c
                  INNER JOIN facturacion.detfacturacion d ON (c.codemp = d.codemp)
                  AND (c.codsuc = d.codsuc)
                  AND (c.nrofacturacion = d.nrofacturacion)
                  AND (c.nroinscripcion = d.nroinscripcion)
                  AND (c.codciclo = d.codciclo)
                  
                where c.codsuc = ".$codsuc." and c.nroinscripcion = ".$nroinscrpcion."
                group by c.mes,c.anio,c.estadofacturacion, c.tipofacturacion,
                    c.consumofact,c.consumo,c.nrofacturacion,c.fechavencimiento
                order by c.nrofacturacion ".$limit;

            $consultaF = $conexion->prepare($sqlF);
            $consultaF->execute(array());
            $itemsF = $consultaF->fetchAll();

            foreach ($itemsF as $rowF) {
                $count++;
                $totfact = $rowF["facturado"];//+ $rowF["impdeuda"] + $rowF["impigv"];
                $diapago = $this->recuperar_dia_pago($codsuc, $nroinscrpcion, $rowF["nrofacturacion"]);

                if ($rowF["tipofacturacion"] == 0) {
                    $tf = "Med.";
                }
                if ($rowF["tipofacturacion"] == 1) {
                    $tf = "Cp.";
                }
                if ($rowF["tipofacturacion"] == 2) {
                    $tf = "Asig.";
                }

                //$this->SetX($x - 1);
                $this->SetWidths(array(6, 21, 10, 20, 20, 30, 18, 15, 30, 21));
                $this->SetAligns(array("C", "C", "C", "C", "C", "C", "R", "C", "R", "C", "C"));
                $this->Row(array($count,
                    $meses[$rowF["mes"]],
                    $rowF["anio"],
                    $rowF["estadofacturacion"] == 1 ? "No" : "Tot",
                    $this->CodFecha($rowF["fechavencimiento"]),
                    $tf,
                    number_format($totfact, 2),
                    number_format($rowF["consumofact"], 0),
                    number_format($rowF["consumo"], 0),
                    $diapago));
                $this->Ln(2);
            }

            $this->Ln(1);
            $this->SetFont('Arial', 'B', 9);
            $this->SetXY($x , 270);
            //$this->Cell(80, 0.001, "", 1, 0, 'C');
            //$this->SetX($this->GetX() + 10);
            $this->Cell(80, 0.001, "", 1, 1, 'C');
            $this->Cell(80, $h, "Nombre,Firma y DNI del responsable de la EPS", 0, 0, 'C');
            
            
            
            $this->SetFont('Arial', 'B', 9);
            $this->SetXY($this->GetX() + 50, 265);
            $this->Cell(30, $h, utf8_decode($this->CodFecha($rowrec[fechaemision])), 0, 1, 'C');            
            $this->SetX($x + 130);            
            $this->Cell(30, $h, utf8_decode("Día / Mes / Año"), 0, 0, 'C');
            
        }

        function recuperar_dia_pago($codsuc, $nroinscripcion, $nrofacturacion) {
            global $conexion;

            $sql = "SELECT c.fechareg
                FROM cobranza.cabpagos as c
                INNER JOIN cobranza.detpagos as d on(c.codemp=d.codemp and c.codsuc=d.codsuc and 
                c.nropago=d.nropago and c.nroinscripcion=d.nroinscripcion)
                WHERE c.codsuc=? and c.nroinscripcion=? and d.nrofacturacion=? 
                GROUP BY c.fechareg";

            $consulta = $conexion->prepare($sql);
            $consulta->execute(array($codsuc, $nroinscripcion, $nrofacturacion));
            $items = $consulta->fetch();

            return $this->DecFecha($items[0]);
        }

    }

    $nroreclamo = $_GET["nroreclamo"];
    $nroinscripcion = $_GET["nroinscripcion"];
    $codsuc = $_GET["codsuc"];

    $objReporte = new clsFormato3();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo, $nroinscripcion);
    $objReporte->Output();
?>