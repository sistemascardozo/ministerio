<?php

    include("clase/clsproceso.php");

    include("../../../../objetos/clsFunciones.php");

    $conexion->beginTransaction();

    $Op = $_GET["Op"];
    $count = $_SESSION["oreclamos"]->item;
    $contador = $_POST["contadorconcepto"];

    $objFunciones = new clsFunciones();

    $codemp           = 1;
    $codsuc           = $_SESSION['IdSucursal'];
    $nroreclamo       = $_POST["nroreclamo"];
    $nroinscripcion   = $_POST["nroinscripcion"];
    $tipoparentesco   = $_POST["tipoparentesco"];
    $medioreclamo     = $_POST["medioreclamo"];
    $reclamante       = $_POST["reclamante"];
    $tipodocumento    = $_POST["tipodocumento"];
    $nrodocumento     = $_POST["nrodocumento"];
    $sector           = $_POST["codsector"];
    $telefono         = $_POST["telefono"];
    $calles           = $_POST["calles"];
    $nrocalle         = $_POST["nrocalle"];
    $distrito         = $_POST["distrito"];
    $idusuario        = $_SESSION["id_user"];
    $direccion        = $_POST["direccion"];
    $nrocalle2        = $_POST["nrocalle2"];
    $manzana          = $_POST["manzana"];
    $nrolote          = $_POST["nrolote"];
    $tiporeclamo      = $_POST["tiporeclamo"];
    $codconcepto      = $_POST["codconcepto"];
    $glosa            = $_POST["glosa"];
    
    //news
    $pruebas          = $_POST["pruebas"];
    $doc_referencia   = $_POST['doc_referencia'];
    $monto_reclamo    = $_POST['monto_reclamo'];
    $car              = $_POST['car'];
    $celular          = $_POST['celular'];
    $codpostal        = $_POST['codpostal'];
    $urbanizacion     = $_POST['urbanizacion']; //Campo referencia en la tabla reclamos
    //datos adicionales
    $entrega_cartilla = $_POST['entrega_cartilla'];
    $contratacion_m   = $_POST['contratacion_m'];
    $codzona          = $_POST['codzona'];
    
    $fecha_citacion   = $_POST['fecha_citacion'];
    $hora_citacion    = $_POST['hora_citacion'];
    $hora_inspeccion2 = $_POST['hora_inspeccion2'];
    $mensaje          = $_POST['mensaje'];
    
    $correlativo      = $_POST['correlativo'];
    $tempanterior     = $_POST['tempanterior'];
    $fechaemision     = $objFunciones->CodFecha($_POST['fechaemision']);

    if ($fecha_citacion == "00/00/0000" || $fecha_citacion == "") {
        $fecha_citacion = null;
    }
    else $fecha_citacion = $objFunciones->CodFecha($_POST['fecha_citacion']);

    $fecha_inspeccion = $_POST['fecha_inspeccion'];
    $hora_inspeccion = $_POST['hora_inspeccion'];
    if ($fecha_inspeccion == "00/00/0000" || $fecha_inspeccion == "") {
        $fecha_inspeccion = null;
    }
    else $fecha_inspeccion = $objFunciones->CodFecha($_POST['fecha_inspeccion']);


    $fecha_notificacion = $_POST['fecha_notificacion'];
    if ($fecha_notificacion == "00/00/0000" || $fecha_notificacion == "") {
        $fecha_notificacion = null;
    }
     else $fecha_notificacion = $objFunciones->CodFecha($_POST['fecha_notificacion']);

    if ($Op == 0) {
        $id = $objFunciones->setCorrelativos("reclamos_ingresos", $codsuc, "0");
        $nroreclamo = $id[0];
        if($tempanterior==0)//SI ES ACTUAL
        {
          $correlativo = $objFunciones->setCorrelativosVarios(20, $codsuc, "SELECT", 0);
        }
        $sql = "INSERT INTO reclamos.reclamos(codemp,codsuc,nroreclamo,nroinscripcion,codubigeo,tiporeclamo,codmedio,codconcepto,direccion1,nrocalle1,
                        manzana1,lote1,glosa,creador,codtipoparentesco,reclamante,codtipodocumento,nrodocumento,codsector,
                        telefono,codcalle,nrocalle,pruebas,doc_referencia,monto_reclamo,car,celular,codpostal,referencia,
                        entrega_cartilla,contratacion_m,fecha_citacion,hora_citacion,fecha_inspeccion,hora_inspeccion,
                        fecha_notificacion,codzona,hora_inspeccion2,mensaje,correlativo,fechaemision) 
                values(  :codemp,
                          :codsuc,
                          :nroreclamo,
                          :nroinscripcion,
                          :codubigeo,
                          :tiporeclamo,
                          :codmedio,
                          :codconcepto,
                          :direccion1,
                          :nrocalle1,
                          :manzana1,
                          :lote1,
                          :glosa,
                          :creador,
                          :codtipoparentesco,
                          :reclamante,
                          :codtipodocumento,
                          :nrodocumento,
                          :codsector,
                          :telefono,
                          :codcalle,
                          :nrocalle,
                          :pruebas,
                          :doc_referencia,
                          :monto_reclamo,
                          :car,
                          :celular,
                          :codpostal,
                          :referencia,
                          :entrega_cartilla,
                          :contratacion_m,
                          :fecha_citacion,
                          :hora_citacion,
                          :fecha_inspeccion,
                          :hora_inspeccion,
                          :fecha_notificacion,
                          :codzona,
                          :hora_inspeccion2,
                          :mensaje,
                          :correlativo,
                          :fechaemision)";
    }
    //29
    if ($Op == 1) {
        $sql = "UPDATE reclamos.reclamos set 
                nroinscripcion=:nroinscripcion,
                codubigeo=:codubigeo,
                tiporeclamo=:tiporeclamo,
                codmedio=:codmedio,
                codconcepto=:codconcepto,
                direccion1=:direccion1,
                nrocalle1=:nrocalle1,
                manzana1=:manzana1,
                lote1=:lote1,
                glosa=:glosa,
                creador=:creador,
                codtipoparentesco=:codtipoparentesco,
                reclamante=:reclamante,
                codtipodocumento=:codtipodocumento,
                nrodocumento=:nrodocumento,
                codsector=:codsector,
                telefono=:telefono,
                codcalle=:codcalle,
                nrocalle=:nrocalle,

                pruebas=:pruebas,
                doc_referencia=:doc_referencia,
                monto_reclamo=:monto_reclamo,
                car=:car,
                celular=:celular,
                codpostal=:codpostal,
                referencia=:referencia,
                entrega_cartilla=:entrega_cartilla,
                contratacion_m=:contratacion_m,
                fecha_citacion=:fecha_citacion,
                hora_citacion=:hora_citacion,
                fecha_inspeccion=:fecha_inspeccion,
                hora_inspeccion=:hora_inspeccion,
                fecha_notificacion=:fecha_notificacion,
                codzona=:codzona,hora_inspeccion2=:hora_inspeccion2,
                mensaje=:mensaje,
                correlativo=:correlativo,
                fechaemision=:fechaemision
                where codemp=:codemp and codsuc=:codsuc and nroreclamo=:nroreclamo";
    }

    $result = $conexion->prepare($sql);
    $result->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nroreclamo" => $nroreclamo,
        ":nroinscripcion" => $nroinscripcion,
        ":codubigeo" => $distrito,
        ":tiporeclamo" => $tiporeclamo,
        ":codmedio" => $medioreclamo,
        ":codconcepto" => $codconcepto,
        ":direccion1" => $direccion,
        ":nrocalle1" => $nrocalle2,
        ":manzana1" => $manzana,
        ":lote1" => $nrolote,
        ":glosa" => $glosa,
        ":creador" => $idusuario,
        ":codtipoparentesco" => $tipoparentesco,
        ":reclamante" => $reclamante,
        ":codtipodocumento" => $tipodocumento,
        ":nrodocumento" => $nrodocumento,
        ":codsector" => $sector,
        ":telefono" => $telefono,
        ":codcalle" => $calles,
        ":nrocalle" => $nrocalle,
        ":pruebas" => $pruebas,
        ":doc_referencia" => $doc_referencia,
        ":monto_reclamo" => $monto_reclamo,
        ":car" => $car,
        ":celular" => $celular,
        ":codpostal" => $codpostal,
        ":referencia" => $urbanizacion,
        ":entrega_cartilla" => $entrega_cartilla,
        ":contratacion_m" => $contratacion_m,
        ":fecha_citacion" => $fecha_citacion,
        ":hora_citacion" => $hora_citacion,
        ":fecha_inspeccion" => $fecha_inspeccion,
        ":hora_inspeccion" => $hora_inspeccion,
        ":fecha_notificacion" => $fecha_notificacion,
        ":codzona"=>$codzona,
        ":hora_inspeccion2"=>$hora_inspeccion2,
        ":mensaje"=>$mensaje,
         ":correlativo"=>$correlativo,
        ":fechaemision"=>$fechaemision
    ));
    if ($result->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }

		
    $del = "delete from reclamos.mesesreclamados where codemp=1 and codsuc=? and nroreclamo=?";
    $resultDel = $conexion->prepare($del);
    $resultDel->execute(array($codsuc, $nroreclamo));

    for ($j = 1; $j <= $contador; $j++) {
        if (isset($_POST["nrofacturacion" . $j])) {
            //echo $_POST["nrofacturacion".$j]."<br>";
			//echo $count;
            for ($i = 0; $i < $count; $i++) {
				//echo $_SESSION["oreclamos"]->nrofacturacion[$i]."a<br>";
				
                if (isset($_SESSION["oreclamos"]->nrofacturacion[$i])) {
                    if ($_POST["nrofacturacion".$j] == $_SESSION["oreclamos"]->nrofacturacion[$i] &&
                            $_POST["codtipodeuda".$j] == $_SESSION["oreclamos"]->codtipodeuda[$i] &&
                            $_POST["ct".$j] == $_SESSION["oreclamos"]->categoria[$i]) {
                        $inst = "insert into reclamos.mesesreclamados(codemp,codsuc,nroreclamo,nrofacturacion,codconcepto,imporiginal,
                                imprebajado,codtipodeuda,categoria,coddocumento,nrodocumento,anio,mes,codciclo,lecturaanterior,lecturaultima,consumo,lecturareclamo,consumoreclamo,seriedocumento,item) 
                                values(:codemp,:codsuc,:nroreclamo,:nrofacturacion,:codconcepto,:imporiginal,:imprebajado,
                                        :codtipodeuda,:categoria,:coddocumento,:nrodocumento,:anio,:mes,:codciclo,:lecturaanterior,:lecturaultima,:consumo,:lecturareclamo,:consumoreclamo,:seriedocumento,:item)";

                        $resultI = $conexion->prepare($inst);
                        $resultI->execute(array(":codemp" => 1,
                            ":codsuc" => $codsuc,
                            ":nroreclamo" => $nroreclamo,
                            ":nrofacturacion" => $_SESSION["oreclamos"]->nrofacturacion[$i],
                            ":codconcepto" => $_SESSION["oreclamos"]->codconcepto[$i],
                            ":imporiginal" => $_SESSION["oreclamos"]->imporiginal[$i],
                            ":imprebajado" => $_SESSION["oreclamos"]->imprebajado[$i],
                            ":codtipodeuda" => $_SESSION["oreclamos"]->codtipodeuda[$i],
                            ":categoria" => $_SESSION["oreclamos"]->categoria[$i],
                            ":coddocumento" => 4,
                            ":nrodocumento" => $_SESSION["oreclamos"]->nrodocumento[$i],
                            ":anio" => $_SESSION["oreclamos"]->anio[$i],
                            ":mes" => $_SESSION["oreclamos"]->mes[$i],
                            ":codciclo" => $_SESSION["oreclamos"]->codciclo[$i],
                            ":lecturaanterior"=>$_SESSION["oreclamos"]->lecant[$i],
                            ":lecturaultima"=>$_SESSION["oreclamos"]->lecact[$i],
                            ":consumo"=>$_SESSION["oreclamos"]->consumo[$i],
                            ":lecturareclamo"=>$_SESSION["oreclamos"]->lecact[$i],
                            ":consumoreclamo"=>$_SESSION["oreclamos"]->consumo[$i],
                            ":seriedocumento"=>$_SESSION['oreclamos']->seriedocumento[$i],
                             ":item"=>$_SESSION["oreclamos"]->codconceptoitem[$i]
                            ));
                        if ($resultI->errorCode() != '00000') 
                        {
                            $conexion->rollBack();
                            $mensaje = "Error reclamos";
                            die(2);
                        }
                        

    
                        //echo $inst."<br>";
                    }
                }
            }
        }
    }
    //PONER EN RECLAMO LA FACTURACION

    $sqlM = "SELECT codciclo,nrofacturacion from reclamos.mesesreclamados
                where nroreclamo=:nroreclamo and codsuc=:codsuc
                GROUP BY codciclo,nrofacturacion";
    
        $consultaM = $conexion->prepare($sqlM);
        $consultaM->execute(array(":nroreclamo"=>$nroreclamo,":codsuc"=>$codsuc));
        if ($consultaM->errorCode() != '00000') 
        {
            $conexion->rollBack();
            $mensaje = "Error reclamos";
            die(2);
        }
        $itemsM = $consultaM->fetchAll();
        
        foreach($itemsM as $rowM)
        {
            
           //PONER EN SIN RECLAMO LA FACTURACION
            $Sql="UPDATE facturacion.cabfacturacion  SET enreclamo = 1,nroreclamo=:nroreclamo
                    WHERE codemp = :codemp AND  codsuc = :codsuc AND
                          codciclo = :codciclo AND nrofacturacion = :nrofacturacion
                          AND nroinscripcion = :nroinscripcion";
            $result = $conexion->prepare($Sql);
            $result->execute(array(
                ":nroreclamo" => $nroreclamo,
                ":codemp" => 1,
                ":codsuc" => $codsuc,
                ":codciclo" => $rowM['codciclo'],
                ":nrofacturacion" => $rowM['nrofacturacion'],
                ":nroinscripcion" => $nroinscripcion

                ));
            if ($result->errorCode() != '00000') 
            {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                die(2);
            }

        }



                
    //PONER EN RECLAMO LA FACTURACION


    /* 	die(); */
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
         if($Op==0 && $tempanterior==0) 
            $objFunciones->setCorrelativosVarios(20,$codsuc,"UPDATE",$correlativo);
    }
?>
