<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../config.php");
	
	$codsuc 	= $_POST["codsuc"];
	$nroreclamo = $_POST["nroreclamo"];
	
	$sql = "select e.descripcion,r.glosa
			from reclamos.reclamos as r
			inner join estadoreclamo as e on(r.codestadoreclamo=e.codestadoreclamo)
			where r.codsuc=? and r.nroreclamo=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroreclamo));
	$row = $consulta->fetch();
	
?>
<style>
	.estiloz{
		font-weight:bold; color:#F00;
	}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="19%">Estado de Reclamo</td>
    <td width="3%" align="center">:</td>
    <td width="78%"><?=strtoupper($row["descripcion"])?></td>
  </tr>
  <tr valign="top">
    <td>Motivo del Reclamo</td>
    <td align="center">:</td>
    <td height="80px"><?=strtoupper($row["glosa"])?></td>
  </tr>
  <tr valign="top">
    <td colspan="3" height="5px"></td>
  </tr>
  <tr valign="top">
    <td colspan="3">
   	  <div style="overflow:auto; height:300px; ">
   	    <table width="100%" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0">
   	      <tr class="myTable" align="center">
   	        <td width="56%">Concepto</td>
   	        <td width="23%">Imp. Rebajado</td>
   	        <td width="21%">Imp. Original</td>
          </tr>
          <?php 
		  	$cat=100;
			$codt=100;
			
		  	$sqlD = "select c.descripcion as concepto,m.categoria,m.codtipodeuda,t.descripcion,m.imprebajado,m.imporiginal
					 from mesesreclamados as m
					 inner join facturacion.conceptos as c on(m.codemp=c.codemp and m.codsuc=c.codsuc and m.codconcepto=c.codconcepto)
					 inner join public.tipodeuda as t on(m.codtipodeuda=t.codtipodeuda)
					 where m.codsuc=? and m.nroreclamo=?";
			
			$consultaD = $conexion->prepare($sqlD);
			$consultaD->execute(array($codsuc,$nroreclamo));
			$itemsD = $consultaD->fetchAll();
			
			foreach($itemsD as $rowD)
			{
				if($cat!=$rowD["categoria"])
				{
					echo "<tr>";
					echo "<td colspan='3' class='estiloz' >CATEGORIA ==> ".$categoria[$rowD["categoria"]]."</td>";
					echo "</tr>";
				}
				if($codt!=$rowD["codtipodeuda"])
				{
					echo "<tr>";
					echo "<td colspan='3' class='estiloz' >&nbsp;&nbsp;&nbsp;TIPO DE DEUDA ==> ".strtoupper($rowD["descripcion"])."</td>";
					echo "</tr>";
				}
		  ?>
   	      <tr>
   	        <td><?=strtoupper($rowD["concepto"])?></td>
   	        <td align="right"><?=number_format($rowD["imprebajado"],2)?></td>
   	        <td align="right"><?=number_format($rowD["imporiginal"],2)?></td>
          </tr>
          <?php 
		  		$cat=$rowD["categoria"];
				$codt=$rowD["codtipodeuda"];
			}
		  ?>
        </table>
      </div>
    </td>
  </tr>
</table>
