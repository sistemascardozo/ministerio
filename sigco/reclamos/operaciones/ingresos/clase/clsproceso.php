<?php	
	session_name("pnsu");
	session_start();
	
	class reclamos
	{
		//----Variables para el Detalle de la Facturacion
		var $item;
		var $nrofacturacion;
		var $codconcepto;
		var $imporiginal;
		var $imprebajado;
		var $codtipodeuda;
		var $categoria;
		var $nrodocumento;
		var $anio;
		var $mes;
		var $codciclo;
		var $lecant;
		var $lecact;
		var $consumo;
		var $lecturarec;
		var $consumorec;
		var $seriedocumento;
		var $codconceptoitem;
		
		function reclamos()
		{
			$this->item           = 0;
			$this->nrofacturacion = "";
			$this->codconcepto    = "";
			$this->imporiginal    = "";
			$this->imprebajado    = "";
			$this->codtipodeuda   = "";
			$this->categoria      = "";
			$this->nrodocumento   = "";
			$this->mes            = "";
			$this->codciclo       = "";
			$this->lecant         = "";
			$this->lecact         = "";
			$this->consumo        = "";
			$this->lecturarec     = "";
			$this->consumorec     = "";
			$this->seriedocumento = "";
			$this->codconceptoitem = "";
		}
		function agregarvalores($nrofact, $codcon, $imporg,$impreb,$tipodeuda,$cat,$nrodocumento,$anio,$mes,$codciclo,$lecant,$lecact,$consumo,$lecturarec,$consumorec,$seriedocumento,$codconceptoitem)
		{
			$this->nrofacturacion[$this->item] = $nrofact;
			$this->codconcepto[$this->item]    = $codcon;
			$this->imporiginal[$this->item]    = str_replace(",", "", $imporg);
			$this->imprebajado[$this->item]    = str_replace(",", "", $impreb);
			$this->codtipodeuda[$this->item]   = $tipodeuda;
			$this->categoria[$this->item]      = $cat;
			$this->nrodocumento[$this->item]   = $nrodocumento;
			$this->seriedocumento[$this->item] = $seriedocumento;
			$this->anio[$this->item]           = $anio;
			$this->mes[$this->item]            = $mes;
			$this->codciclo[$this->item]       = $codciclo;
			$this->codconceptoitem[$this->item] = $codconceptoitem;
			$this->lecant[$this->item]         = str_replace(",", "", $lecant);
			$this->lecact[$this->item]         = str_replace(",", "", $lecact);
			$this->consumo[$this->item]        = str_replace(",", "", $consumo);
			$this->lecturarec[$this->item]     = str_replace(",", "", $lecturarec);
			$this->consumorec[$this->item]     = str_replace(",", "", $consumorec);
			$this->item++;
			//echo $this->item;
		}
		function actualizarvalores($nrofact,$codcon,$imporg,$impreb,$tipodeuda,$cat,$it,$nrodocumento,$anio,$mes,$codciclo,$lecant,$lecact,$consumo,$lecturarec,$consumorec,$seriedocumento,$codconceptoitem)
		{
			$this->nrofacturacion[$it] = $nrofact;
			$this->codconcepto[$it]    = $codcon;
			$this->imporiginal[$it]    = str_replace(",", "", $imporg);
			$this->imprebajado[$it]    = str_replace(",", "", $impreb);
			$this->codtipodeuda[$it]   = $tipodeuda;
			$this->categoria[$it]      = $cat;
			$this->nrodocumento[$it]   = $nrodocumento;
			$this->seriedocumento[$it] = $seriedocumento;
			$this->anio[$it]           = $anio;
			$this->mes[$it]            = $mes;
			$this->codciclo[$it]       = $codciclo;
			$this->codconceptoitem[$it]= $codconceptoitem;
			$this->lecant[$it]         = str_replace(",", "", $lecant);
			$this->lecact[$it]         = str_replace(",", "", $lecact);
			$this->consumo[$it]        = str_replace(",", "", $consumo);
			$this->lecturarec[$it]     = str_replace(",", "", $lecturarec);
			$this->consumorec[$it]     = str_replace(",", "", $consumorec);
		}
		function agregarvaloresxx($nrofact,$codcon,$imporg,$impreb,$tipodeuda,$cat,$nrodocumento,$anio,$mes,$codciclo,$lecant,$lecact,$consumo,$lecturarec,$consumorec,$seriedocumento,$codconceptoitem)
		{
			$entro	= 0;
			$idx	= 0;

			for($i=0; $i < $this->item; $i++)
			{
				if($this->codconcepto[$i] == $codcon && $this->nrofacturacion[$i]==$nrofact && $this->codtipodeuda[$i]==$tipodeuda && $this->codconceptoitem[$i]==$codconceptoitem)
				{
					$entro 	= 1;
					$idx 	= $i;
				}
			}
			if($entro==0)
			{
				$this->agregarvalores($nrofact,$codcon,$imporg,$impreb,$tipodeuda,$cat,$nrodocumento,$anio,$mes,$codciclo,$lecant,$lecact,$consumo,$lecturarec,$consumorec,$seriedocumento,$codconceptoitem);
			}else{
				$this->actualizarvalores($nrofact,$codcon,$imporg,$impreb,$tipodeuda,$cat,$idx,$nrodocumento,$anio,$mes,$codciclo,$lecant,$lecact,$consumo,$lecturarec,$consumorec,$seriedocumento,$codconceptoitem);
			}
		}
	}
	
	if (!isset($_SESSION["oreclamos"])){ 
 		$_SESSION["oreclamos"] = new reclamos();
	} 
?>