<?php
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  
  $TituloVentana = "SOLICITUD";
  $Activo=1;
  unset($_SESSION["oreclamos"]);  
  $Op     = isset($_GET['Op'])?$_GET['Op']:0;
  if($Op!=5) CuerpoSuperior($TituloVentana);
  $codsuc   = $_SESSION['IdSucursal'];
  $valor    = isset($_GET["Valor"])?strtoupper($_GET["Valor"]):"";  
  $fecha_fin  = date('Y-m-d');
  $Criterio='Reclamos';
  $FormatoGrilla = array ();
  $Sql = "SELECT rec.correlativo,rec.nroinscripcion,clie.propietario,tippar.descripcion,rec.reclamante,
            tipdoc.abreviado|| ':' ||rec.nrodocumento,
            case when rec.tiporeclamo=1 then 'COMERCIALES CON FACTURACION' else 
            case when rec.tiporeclamo=2 then 'COMERCIALES SIN FACTURACION' else 'OPERACIONALES' end end,
            estado.descripcion as estText,rec.glosa,rec.codestadoreclamo,1,1,rec.nrosolicitud
            FROM reclamos.solicitud as rec
            inner join catastro.clientes as clie on(rec.codemp=clie.codemp and rec.codsuc=clie.codsuc and
            rec.nroinscripcion=clie.nroinscripcion)
            inner join public.tipodocumento as tipdoc on(rec.codtipodocumento=tipdoc.codtipodocumento)
            inner join public.tipoparentesco as tippar on (rec.codtipoparentesco=tippar.codtipoparentesco)
            inner join reclamos.estadoreclamo as estado on(rec.codestadoreclamo=estado.codestadoreclamo)";
  if($Op==5)
  {
    $and = " and rec.fin_reclamo=1 and rec.fundado=1 and not rec.codestadoreclamo=5";
  }
  $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL   
  $FormatoGrilla[1] = array('1'=>'rec.nrosolicitud', '2'=>'rec.nroinscripcion','3'=>'clie.propietario',
                            '4'=>'tipdoc.abreviado','5'=>'rec.nrodocumento','6'=>'tippar.descripcion','7'=>'rec.reclamante',
                            '8'=>'rec.correlativo');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op; 
  $FormatoGrilla[3] = array('T1'=>'Nro. Reclamo', 'T2'=>'Nro. Inscripcion','T3'=>'Propietario','T4'=>'Parentesco','T5'=>'Reclamante'
                            ,'T6'=>'Documento','T7'=>'Categoria','T8'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'left');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60','W2'=>'50','W3'=>'150','W4'=>'50','W6'=>'100','W7'=>'212','W8'=>'120','W9'=>'80');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1200;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " and (rec.codemp=1 and rec.codsuc=".$codsuc.$and.") ORDER BY rec.correlativo desc ";                                   //Orden de la Consulta
  if($Op!=5)
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar Solicitud',       //Titulo del Botón
              'BtnF1'=>'onclick="Modificar(this);"',  //Eventos del Botón
              'BtnCI1'=>'11',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnRestablecer', //y aparece este boton
              'BtnI2'=>'imprimir.png', 
              'Btn2'=>'Imprimir Formato de Solicitudes', 
              'BtnF2'=>'onclick="ImprimirR(this)"', 
              'BtnCI2'=>'12', 
              'BtnCV2'=>'1',
              'BtnId3'=>'BtnDerivar', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Realizar Derivacion de la Solicitud', 
              'BtnF3'=>'onclick="Derivar(this)"', 
              'BtnCI3'=>'12', 
              'BtnCV3'=>'1',
              'BtnId4'=>'BtnEliminar', 
              'BtnI4'=>'documento.png', 
              'Btn4'=>'Ver Detalle de la Solicitud', 
              'BtnF4'=>'onclick="VerDetalle(this)"', 
              'BtnCI4'=>'12', //campo 3
              'BtnCV4'=>'1');//igua a 1);
  else
    {
      $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'11',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
      $FormatoGrilla[5] = array('W1'=>'60','W6'=>'80','W11'=>'20','W2'=>'70','W3'=>'70','W10'=>'90'); 
      $FormatoGrilla[5] = array('W1'=>'60','W2'=>'50','W3'=>'150','W4'=>'50','W6'=>'100','W7'=>'212','W8'=>'120','W9'=>'80');  
    }

  $FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 13), array('Name' => 'estado', 'Col' => 10), 
  array('Name' => 'texto', 'Col' => 8), array('Name' => 'nroinscripcion', 'Col' => 2)); //DATOS ADICIONALES  
  $FormatoGrilla[11] = 8; //FILAS VISIBLES            
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],900,600);
  Pie();
  
?>
<script type="text/javascript">
var urldir = '<?=$urldir?>'
var codsuc = '<?= $codsuc ?>'
function Enviar(obj)
  {
    var Id = $(obj).parent().parent().data('id')
    opener.Recibir(Id);
    window.close();
  }
function Modificar(obj)
{
  $("#form1").remove();
  var estado = $(obj).parent().parent().data('estado')
  var Id = $(obj).parent().parent().data('id')
  var texto = $(obj).parent().parent().data('texto')
   
  if(estado!=1)
  {
    alert("La solicitud no se puede MODIFICAR por que Se encuentra con el Estado: "+texto)
    return
  }
  $("#Modificar").dialog({title: 'Modificar Solicitud de Inspeccion'});
  $("#Modificar").dialog("open");
  $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
   $.ajax({
      url: 'mantenimiento.php',
      type: 'POST',
      async: true,
      data: 'Id=' + Id + '&codsuc=<?= $codsuc ?>&Op=1',
      success: function(data) {
          $("#DivModificar").html(data);
      }
  })    

}
function VerDetalle(obj)
{
   $("#form1").remove();
  var estado = $(obj).parent().parent().data('estado')
  var Id = $(obj).parent().parent().data('id')
  var texto = $(obj).parent().parent().data('texto')
  AbrirPopupImpresion('<?=$urldir?>sigco/reclamos/operaciones/ingresos/popup/popup_detalle_reclamos.php?codsuc=<?= $codsuc ?>&nroreclamo='+Id,1000,600)
}
function ImprimirR(obj)
{
  $("#form1").remove();
  var estado = $(obj).parent().parent().data('estado')
  var nrorec = $(obj).parent().parent().data('id')
  var nroinsc = $(obj).parent().parent().data('nroinscripcion')
  var url='';
  url='../ingresos/formatos/formato1.php';
  AbrirPopupImpresion(url+'?nroreclamo='+nrorec+'&codsuc=<?=$codsuc?>&nroinscripcion='+nroinsc,800,500)

}
function Derivar(obj)
{

  var estado = $(obj).parent().parent().data('estado')
  var nrosol = $(obj).parent().parent().data('id')
  var text   = $(obj).parent().parent().data('texto')

  var par = '&nrodetalle=0&fecha_fin=<?=$fecha_fin?>'
  if(estado!=1)
  {
    alert("La Solicitud no se puede Derivar por que se encuentra con el Estado: " + text)
    return
  }
  
  cargar_from_derivacion(nrosol,par)

}
</script>
<script src="js_solicitud.js" type="text/javascript"></script>
<div id="dialog-form-derivar" title="Derivar Solicitud"  >
    <div id="div_derivar_solicitud"></div>
</div>
<?php
if($Op!=5) CuerpoInferior();
?>