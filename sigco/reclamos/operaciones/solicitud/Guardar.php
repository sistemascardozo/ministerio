<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsFunciones.php");

    $conexion->beginTransaction();
    $Op = $_GET["Op"];
    $count = $_SESSION["oreclamos"]->item;
    $contador = $_POST["contadorconcepto"];
    $objFunciones = new clsFunciones();

    $codemp         = 1;
    $codsuc         = $_SESSION['IdSucursal'];
    $nrosolicitud   = $_POST["nrosolicitud"];
    $nroinscripcion = $_POST["nroinscripcion"];
    $tipoparentesco = $_POST["tipoparentesco"];
    $medioreclamo   = $_POST["medioreclamo"];
    $reclamante     = $_POST["reclamante"];
    $tipodocumento  = $_POST["tipodocumento"];
    $nrodocumento   = $_POST["nrodocumento"];
    $sector         = $_POST["sector"];
    $telefono       = $_POST["telefono"];
    $calles         = $_POST["calles"];
    $nrocalle       = $_POST["nrocalle"];
    $distrito       = $_POST["distrito"];
    $idusuario      = $_SESSION["id_user"];
    $direccion      = $_POST["direccion"];
    
    $nrolote        = $_POST["nrolote"];
    $tiporeclamo    = $_POST["tiporeclamo"];
    $codconcepto    = $_POST["codconcepto"];
    $glosa          = $_POST["glosa"];
    
    $codzona        = $_POST['codzona'];
    $email          = $_POST['email'];
    $correlativo    = $_POST['correlativo'];
    $tempanterior   = $_POST['tempanterior'];
    $fechaemision   = $objFunciones->CodFecha($_POST['fechaemision']);


    if ($Op == 0) 
    {
        $id = $objFunciones->setCorrelativos("reclamos_solicitud", $codsuc, "0");
        $nrosolicitud = $id[0];
        if($tempanterior==0)//SI ES ACTUAL
        {
          $correlativo = $objFunciones->setCorrelativosVarios(19, $codsuc, "SELECT", 0);
        }
        $sql = "INSERT INTO reclamos.solicitud
              (
                codemp,
                codsuc,
                nrosolicitud,
                nroinscripcion,
                codubigeo,
                tiporeclamo,
                codmedio,
                codconcepto,
                reclamante,
                glosa,
                creador,
                codtipoparentesco,
                codtipodocumento,
                nrodocumento,
                codsector,
                telefono,
                codcalle,
                nrocalle,
                codzona,
                email,
                correlativo,
                fechaemision
              ) 
              VALUES (
                :codemp,
                :codsuc,
                :nrosolicitud,
                :nroinscripcion,
                :codubigeo,
                :tiporeclamo,
                :codmedio,
                :codconcepto,
                :reclamante,
                :glosa,
                :creador,

                :codtipoparentesco,
                :codtipodocumento,
                :nrodocumento,
                :codsector,
                :telefono,
                :codcalle,
                :nrocalle,
                  :codzona,
                :email,
                :correlativo,
                :fechaemision
              );";
    }
    //29
    if ($Op == 1) {
        $sql = "UPDATE reclamos.solicitud  
SET 
 
  nroinscripcion = :nroinscripcion,
  codubigeo = :codubigeo,
  tiporeclamo = :tiporeclamo,
  codmedio = :codmedio,
  codconcepto = :codconcepto,
  reclamante = :reclamante,
  glosa = :glosa,
  creador = :creador,
  codtipoparentesco = :codtipoparentesco,
  codtipodocumento = :codtipodocumento,
  nrodocumento = :nrodocumento,
  codsector = :codsector,
  telefono = :telefono,
  codcalle = :codcalle,
  nrocalle = :nrocalle,
  email = :email,
  codzona = :codzona,
  correlativo=:correlativo,
  fechaemision=:fechaemision
 
WHERE  codemp = :codemp AND codsuc = :codsuc AND
  nrosolicitud = :nrosolicitud;";
    }
    $result = $conexion->prepare($sql);
    $result->execute(array(":codemp" => $codemp,
        ":codsuc" => $codsuc,
        ":nrosolicitud" => $nrosolicitud,
        ":nroinscripcion" => $nroinscripcion,
        ":codubigeo" => $distrito,
        ":tiporeclamo" => $tiporeclamo,
        ":codmedio" => $medioreclamo,
        ":codconcepto" => $codconcepto,
         ":reclamante" => $reclamante,
        ":glosa" => $glosa,
        ":creador" => $idusuario,
        ":codtipoparentesco" => $tipoparentesco,
       ":codtipodocumento" => $tipodocumento,
        ":nrodocumento" => $nrodocumento,
        ":codsector" => $sector,
        ":telefono" => $telefono,
        ":codcalle" => $calles,
        ":nrocalle" => $nrocalle,
        ":email" => $email,
        ":codzona"=>$codzona,
        ":correlativo"=>$correlativo,
        ":fechaemision"=>$fechaemision
       
    ));
      if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       echo $res = 2;
    } 
    else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
		     if($Op==0 && $tempanterior==0) 
            $objFunciones->setCorrelativosVarios(19,$codsuc,"UPDATE",$correlativo);
    }
?>
