<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$codsuc 	= $_POST["codsuc"];
	$nrodetalle = $_POST["nrodetalle"];
	
	$sql = "select d.nroreclamo,e.descripcion as estado,d.codusu,d.fechareg,a.descripcion as area,i.nombres,d.inicial,d.codusu_derivacion,
			d.observacion,d.fin_reclamo,d.fecha_finalizacion
			from reclamos.detalle_reclamos as d 
			inner join reclamos.estadoreclamo as e on(d.codestadoreclamo=e.codestadoreclamo)
			inner join reglasnegocio.areas as a on(d.codemp=a.codemp and d.codsuc=a.codsuc and d.codarea=a.codarea)
			inner join reglasnegocio.inspectores as i on(d.codemp=i.codemp and d.codsuc=i.codsuc and d.codinspector=i.codinspector)
			where d.codsuc=? and d.nrodetalle=?";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nrodetalle));
	$row = $consulta->fetch();
	//var_dump($consulta->errorInfo());
	
	if($row["inicial"]==0){$estInicial = "ESTADO NORMAL";}
	if($row["inicial"]==1){$estInicial = "ESTADO INICIAL";}
	if($row["inicial"]==2){$estInicial = "ESTADO PROCESADO";}
	if($row["inicial"]==3){$estInicial = "ESTADO DERIVADO";}	
	
	$fechafin 			= $row["fecha_finalizacion"];

	$fecha_finalizacion	= $objFunciones->DecFecha($fechafin);
	if($row["fin_reclamo"]==0 && $row["inicial"]==0){
	   $fechafin=date("Y-m-d");$fecha_finalizacion="SIN FECHA DE FINALIZACION";}
	$dias = $objFunciones->calcular_diferencias_dias($row["fechareg"],$fechafin);
		
?>
<table width="100%" border="0">
  <tr>
    <td width="23%">Nro. Reclamo</td>
    <td width="3%" align="center">:</td>
    <td width="74%"><?=$row["nroreclamo"]?></td>
  </tr>
  <tr>
    <td>Estado</td>
    <td align="center">:</td>
    <td><?=strtoupper($row["estado"])?></td>
  </tr>
  <tr>
    <td>Usuario Recepcionado</td>
    <td align="center">:</td>
    <td><?=$objFunciones->login($row["codusu"])?></td>
  </tr>
  <tr>
    <td>Fecha de Recepcion</td>
    <td align="center">:</td>
    <td><?=$objFunciones->DecFecha($row["fechareg"])?></td>
  </tr>
  <tr>
    <td>Fecha de Finalizacion</td>
    <td align="center">:</td>
    <td><?=$fecha_finalizacion?></td>
  </tr>
  <tr>
    <td>Dias Procesados</td>
    <td align="center">:</td>
    <td><?=$dias?></td>
  </tr>
  <tr>
    <td>Area</td>
    <td align="center">:</td>
    <td><?=strtoupper($row["area"])?></td>
  </tr>
  <tr>
    <td>Inspector</td>
    <td align="center">:</td>
    <td><?=strtoupper($row["nombres"])?></td>
  </tr>
  <tr>
    <td>Est. Inicial</td>
    <td align="center">:</td>
    <td><?=$estInicial?></td>
  </tr>
  <tr>
    <td>Usuario Derivado</td>
    <td align="center">:</td>
    <td><?=$objFunciones->login($row["codusu_derivacion"])?></td>
  </tr>
  <tr valign="top">
    <td>Observacion</td>
    <td align="center">:</td>
    <td height="150"><?=strtoupper($row["observacion"])?></td>
  </tr>
  <tr>
    <td>Fin de Reclamo</td>
    <td align="center">:</td>
    <td><?=$row["fin_reclamo"]==1?"SI":"NO"?></td>
  </tr>
</table>
