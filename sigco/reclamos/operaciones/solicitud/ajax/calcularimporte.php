<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../objetos/clsFunciones.php");
	$objFunciones = new clsFunciones();
	
	set_time_limit(0);

	$codsuc         = $_POST["codsuc"];
	$codciclo       = $_POST["codciclo"];
	$porcentaje     = $_POST["porcentaje"];
	$idusuario      = $_SESSION['id_user'];
	$nrofacturacion = $_POST["nrofacturacion"];
	$calcalc        = $_POST["factura_alcantarillado"];	
	$anio           = $_POST["anio"];
	$mes            = $_POST["mes"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$consumo        = $_POST["consumo"];
	//ACTUALIZAR PARA EVITAR UN ERROR DE DIGITACION//
	
	
	$porcalc 	= $objFunciones->getParamae("PORALC",$codsuc);
	$Pigv		= $objFunciones->getParamae("IMPIGV",$codsuc);
	$Pigv		=$Pigv["valor"];
	
		/*Inicio del Proceso de Calculo de Facturacion*/
		$sqlC  	 	= "select * from facturacion.view_usuariofacturar 
					   where codemp=1 and codsuc=? and codciclo=? AND nroinscripcion=? ORDER by nroorden";
		
		$consultaC = $conexion->prepare($sqlC);
		$consultaC->execute(array($codsuc,$codciclo,$nroinscripcion));
		$itemsC = $consultaC->fetchAll();
		
		foreach($itemsC as $rowC)
		{
			$conscategoria 	= 0;
			$impagua		= 0;
			$impdesague		= 0;
			$impigv			= 0;
			
			$sqlFACT 	= "select * from facturacion.f_getconsfacturado(:codsuc,:catetar,:consumo,:tipofacturacion)";
			$consFact	= $conexion->prepare($sqlFACT);
			$consFact->execute(array(":codsuc"=>$codsuc,
									 ":catetar"=>$rowC["catetar"],
									 ":consumo"=>$consumo,
									 ":tipofacturacion"=>$rowC["tipofacturacion"]));

			
			
			$consumo_facturado = $consFact->fetch();
			$Flag = true;//CALCULAR IMPORTE DE AGUA DE CORTADOS CON DEUDA DE 2 MESE
			$FlagCorteSolicitud=false;//CORTES A SOLICITUD FACTURARA UN CONCEPTO, SIN NUMERO DE RECIBO
			$Imprimir = 1;//SI SE VA IMPRIMIR EL RECIBO SI(1)
			
			$TotalParaIgv=0;
		
				if($Flag)
				{
				/*Reecupera los importe y datos de las unidades de uso*/
					$num_total_unidades = $rowC["domestico"]+$rowC["social"]+$rowC["comercial"]+$rowC["estatal"]+$rowC["industrial"];
									
					if($num_total_unidades==1)				
					{
						$ConsumoTemp = $consumo;
						if($rowC['tipofacturacion']!=1)
							$ConsumoTemp = $consumo;
				
						$conscategoria = round($ConsumoTemp*(100/100),2);
						
						$sqlimporte 	= "SELECT * FROM facturacion.f_getimportetarifa('".$codsuc."', '".$rowC["catetar"]."', '".$rowC["tipofacturacion"]."', ".$conscategoria.", ".$nrofacturacion.")";
				
						$consimporte 	= $conexion->prepare($sqlimporte);
						$consimporte->execute(array());
						
						
						
						$items = $consimporte->fetch();
						$impagua    = empty($items[0])?0:$items[0];
						$impdesague = empty($items[1])?0:$items[1];

						if($calcalc==1)
						{
							$impdes = round($items[0]*($porcalc/100),2);
						}else{
							$impdes	= round($items[1],2);
						}
						
						

							
					}else
					{
						//---cuando tienes mas de 1 unidades de uso------
						$sqlU = "select catetar,porcentaje,principal from catastro.unidadesusoclientes where codemp=1 and
								 codsuc=? and nroinscripcion=?";
						
						$consultaU = $conexion->prepare($sqlU);
						$consultaU->execute(array($codsuc,$rowC["nroinscripcion"]));
						$itemsU = $consultaU->fetchAll();
						$ConsumoTemp = $rowC["lecturapromedio"];
						if($rowC['tipofacturacion']!=1)
							$ConsumoTemp = $consumo;

						foreach($itemsU as $rowU)
						{

							$conscategoria = round($ConsumoTemp*($rowU["porcentaje"]/100),2);
						
							$sqlI 	= "SELECT * FROM facturacion.f_getimportetarifa('".$codsuc."', '".$rowU["catetar"]."', '".$rowC["tipofacturacion"]."', ".$conscategoria.", ".$nrofacturacion.")";
							
							$consultaI = $conexion->prepare($sqlI);
							$consultaI->execute(array());

							$itemsI = $consultaI->fetch();
							
							$impagua    += $itemsI[0];
							
							
							if($calcalc==1)
							{
								$impdes = round($itemsI[0]*($porcalc/100),2);
							}else{
								$impdes	= round($itemsI[1],2);
							}
							
							$impdesague += floatval(str_replace(",","",$impdes));//$impdes;

							

						}
					}
				
					/*Valida e inserta el concepto de cargo fijo siempre y cuando sea mayor a cero*/
					$sqlT = "SELECT t.impcargofijo1 ";
					$sqlT .= "FROM facturacion.tarifas t ";
					$sqlT .= " INNER JOIN facturacion.periodofacturacion pf ON (t.codemp = pf.codemp) ";
					$sqlT .= " AND (t.codsuc = pf.codsuc) AND (t.nrotarifa = pf.nrotarifa)";
					$sqlT .= "WHERE t.codemp = 1 ";
					$sqlT .= " AND t.codsuc = ".$codsuc." ";
					$sqlT .= " AND t.catetar = ".$rowC["catetar"]." ";
					$sqlT .= " AND pf.nrofacturacion = ".$nrofacturacion." ";
					
					$consultaT = $conexion->prepare($sqlT);
					$consultaT->execute(array());
					
					$itemsT = $consultaT->fetch();
					
						
					if($itemsT["impcargofijo1"]>0)
					{
						$consulta_conceptos_cargo = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=5 AND codtipoconcepto=1 AND sefactura=1");
						$consulta_conceptos_cargo->execute(array($codsuc));
						$items_conceptos_cargo = $consulta_conceptos_cargo->fetchAll();
						
						foreach($items_conceptos_cargo as $row_conceptos)
						{	
							

							$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($itemsT["impcargofijo1"],2)));
							//echo $TotalParaIgv.'<br>';	
						}
					}

					/*Valida el Tipo de Servicio*/
					if($rowC["codtiposervicio"]==1) //Agua y Desague
					{
						/*Calculo del Agua*/
						//INSERTAR SI IMPORTE ES MAYOR CERO
						if(floatval($impagua)>0)
						{

							$consulta_conceptos_agua = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=1 AND codtipoconcepto=1 /*AND codconcepto<>73*/  AND sefactura=1 AND codconcepto=1");
							$consulta_conceptos_agua->execute(array($codsuc));
							$items_conceptos_agua = $consulta_conceptos_agua->fetchAll();
				
							foreach($items_conceptos_agua as $rowAgua_conceptos)
							{
								

								$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impagua,2)));
								//echo $TotalParaIgv.'<br>';
							}
						}
						/*Calculo del Desague*/
						if($calcalc==1)
						{
							$impdesague = round($impagua*($porcalc/100),2);
						}else{
							$impdesague	= round($impdesague,2);
						}
						/*if($rowC["nroinscripcion"]=='699')
						{
							
							echo "<br>Agua-->".round($impagua,2);
							echo "<br>Desague-->".round($impdesague,2);
							

						}
						*/
						//INSERTAR SI IMPORTE ES MAYOR CERO
						if(floatval($impdesague)>0)
						{
							$consulta_conceptos_desague = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=2 AND codtipoconcepto=1  AND sefactura=1");
							$consulta_conceptos_desague->execute(array($codsuc));
							$items_conceptos_desague = $consulta_conceptos_desague->fetchAll();
					
							foreach($items_conceptos_desague as $rowDesague_conceptos)
							{
								
								

								$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impdesague,2)));
								//echo $TotalParaIgv.'<br>';
							}
						}
						$impigv = round(($impagua+$impdesague)*($igv/100),2);
						
					}
					if($rowC["codtiposervicio"]==2) //Agua
					{
						$impdesague=0;
						
						/*Calculo del Agua*/
						if(floatval($impagua)>0)
						{
							$consulta_conceptos_agua = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=1 AND codtipoconcepto=1 /*AND codconcepto<>73*/  AND sefactura=1 AND codconcepto=1 ");
							$consulta_conceptos_agua->execute(array($codsuc));
							$items_conceptos_agua = $consulta_conceptos_agua->fetchAll();
				
							foreach($items_conceptos_agua as $rowAgua_conceptos)
							{
								
								$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impagua,2)));
								//echo $TotalParaIgv.'<br>';
							}
						}
						
						$impigv = round(($impagua)*($igv/100),2);
					}
					if($rowC["codtiposervicio"]==3) //Desague
					{
						$impagua=0;
						/*Calculo del Desague*/
						if($calcalc==1)
						{
							$impdesague = round($impagua*($porcalc/100),2);
						}else{
							$impdesague	= round($impdesague,2);
						}
						if(floatval($impdesague)>0)
						{
							$consulta_conceptos_desague = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=2 AND codtipoconcepto=1 AND sefactura=1");
							$consulta_conceptos_desague->execute(array($codsuc));
							$items_conceptos_desague = $consulta_conceptos_desague->fetchAll();
							
							foreach($items_conceptos_desague as $rowDesague_conceptos)
							{
								
								
								$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impdesague,2)));
								//echo $TotalParaIgv.'<br>';
							}
						}
						$impigv = round(($impdesague)*($igv/100),2);
					}
					//$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impdesague,2)));
					/*Inserta el Concepto de igv siempre y cuando sea mayor a cero(0)*/
					if($TotalParaIgv>0)
					{

						$impigv = round(floatval($TotalParaIgv)*(floatval($Pigv)/100),2);
						if($impigv>0)
						{
							$consulta_conceptos_igv = $conexion->prepare("select * from facturacion.conceptos where codsuc=? and categoria=4 AND codtipoconcepto=1 AND sefactura=1");
							$consulta_conceptos_igv->execute(array($codsuc));
							$items_conceptos_igv = $consulta_conceptos_igv->fetchAll();
							
							foreach($items_conceptos_igv as $rowIgv_conceptos)
							{
								$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($impigv,2)));
							}
						}
						
					}
				}
				//$correlativo=$correlativo+1; ERA ACAAA
				////////7AGREGADO INTERES
				//--Verifica si tiene credito el usuario que esta facturando--
			$impcreditos = 0;
			
			$sqlcreditos  = "select det.nrocuota,det.totalcuotas,cab.codconcepto,
			det.imptotal,det.nrocredito,det.subtotal,det.igv,det.redondeo ";
			$sqlcreditos .= "from facturacion.detcreditos as det ";
			$sqlcreditos .= "inner join facturacion.cabcreditos as cab on(det.codemp=cab.codemp and det.codsuc=cab.codsuc and ";
			$sqlcreditos .= "det.nrocredito=cab.nrocredito) ";
			$sqlcreditos .= "where det.codemp=1 and det.codsuc=:codsuc ";
			$sqlcreditos .= "and cab.nroinscripcion=:nroinscripcion and det.nrofacturacion=:nrofacturacion ";
			$sqlcreditos .= "and estadocuota=0 and cab.estareg=1 and tipocuota=0 ORDER BY nrocuota LIMIT 1 ";
			
			$consulta_creditos = $conexion->prepare($sqlcreditos);
			$consulta_creditos->execute(array(":codsuc"=>$codsuc,":nroinscripcion"=>$rowC["nroinscripcion"],":nrofacturacion"=>$nrofacturacion));
			$items_creditos = $consulta_creditos->fetchAll();
			$SubtotalCreditos=0;
			$SubTotalIgvCreditos=0;
			$SubTotalRed = 0;

			foreach($items_creditos as $row_creditos)
			{			
				$impcreditos += $row_creditos[3];
				$SubtotalCreditos += $row_creditos[6];
				$SubTotalRed += $row_creditos[7];
				/*Inserta el credito en la facturacion SUB TOTAL*/
				if(floatval($row_creditos[5])<>0)
				{
					$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round(floatval($row_creditos[5]),2)));
				}

				/*Inserta el credito en la facturacion IGV*/
				if(floatval($row_creditos[6])<>0)
				{
					$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($row_creditos[6],2)));
				}

				/*Inserta el credito en la facturacion REDONDEO*/
				if(floatval($row_creditos[7])<>0)
				{
					$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($row_creditos[7],2)));
				}
				/*Actualiza el estado en la Tabla de creditos*/
				$updcreditos  = "update facturacion.detcreditos set estadocuota=1 where codemp=1 and codsuc=:codsuc and ";
				$updcreditos .= "nrocredito=:nrocredito and nrofacturacion=:nrofacturacion and ";
				$updcreditos .= "estadocuota=0";
				
				$update_creditos = $conexion->prepare($updcreditos);
				$update_creditos->execute(array(":codsuc"=>$codsuc,":nrocredito"=>$row_creditos[4],":nrofacturacion"=>$nrofacturacion));
				if($consulta_creditos_det->errorCode()!='00000')
				{
					$conexion->rollBack();
					$mensaje = "Error detfacturacion ".$rowC["nroinscripcion"].$sql;
					die($mensaje);
				}
			}
			
			//--Verificamos si el Usuario tiene Refinanciamiento y lo Agregamos en la Facturacion--
			$imprefinanciamiento=0;
			
			$sqlrefinanciamiento="select d.codconcepto,d.importe,d.nrorefinanciamiento,d.nrocuota,d.totalcuotas
								  from facturacion.detrefinanciamiento as d 
								  inner join facturacion.cabrefinanciamiento as c on(d.nrorefinanciamiento=c.nrorefinanciamiento and d.codemp=c.codemp and d.codsuc=c.codsuc)
								  where c.nroinscripcion=? and d.nrofacturacion=? and c.codsuc=? and d.estadocuota=0";
								  
			$consulta_refinanciamiento = $conexion->prepare($sqlrefinanciamiento);
			$consulta_refinanciamiento->execute(array($rowC["nroinscripcion"],$nrofacturacion,$codsuc));
			$items_refinanciamiento = $consulta_refinanciamiento->fetchAll();
			
			foreach($items_refinanciamiento as $row_refinanciamiento)
			{
				/*Inserta los conceptos en la facturacion*/
				$TotalParaIgv=floatval($TotalParaIgv)+floatval(str_replace(",","",round($row_refinanciamiento[1],2)));
				
			}
			
			
			//--Verifica si el usuario tiene deuda anterior para calcular el credito--
			$impdeudaagua 		= 0;
			$impdeudaalc		= 0;
			$intereses			= 0;
			$impdeudaigv		= 0;
			$impdeudacargo		= 0;
			$impdeudacredito	= 0;
			$tipoconcepto		= 1;

			$consulta_deuda = $conexion->prepare("select * from facturacion.f_importedeuda(?,?)");
			$consulta_deuda->execute(array($rowC["nroinscripcion"],$codsuc));

			$items_deuda = $consulta_deuda->fetch();
			
			$agua 		= ($items_deuda["agua"] + $items_deuda["intagua"])*$porcentaje;
			$desague 	= ($items_deuda["desague"]+$items_deuda["intdesague"])*$porcentaje;
			$igv		= ($items_deuda["importeigv"]+$items_deuda["intigv"])*$porcentaje;
			$cargo_fijo	= ($items_deuda["importecargo"]+$items_deuda["intcargo"])*$porcentaje;
			$credito 	= ($items_deuda["importecredito"]+$items_deuda["interescredito"])*$porcentaje;

							

			
				
			$totfacturacion = round($TotalParaIgv,2)+
							  round($impigv,2)+round($impcreditos,2)+round($agua,2)+round($desague,2)+round($igv,2)+
							  round($cargo_fijo,2)+round($credito,2);	
	
			//$totfacturacion = $TotalParaIgv+$impcreditos;	//0.78	
			
			 $tot1 	= round($totfacturacion,2);//0.78	
			 $tot2	= round($totfacturacion,1); //0.8
			
			$redctual = round($tot2-$tot1,2); //0.02
		 	$totfacturacion		+=$redctual;
		}
	
	
		
		 echo $totfacturacion;
	
?>