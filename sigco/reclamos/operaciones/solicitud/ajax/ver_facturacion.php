<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../config.php");
	
	$nroinscripcion = $_POST["nroinscripcion"];
	$codsuc			= $_POST["codsuc"];
	
	$sql 		= "select * from cobranza.view_cobranza where codemp=1 and codsuc=? and nroinscripcion=? and imptotal>0 order by nrofacturacion";
	$consulta 	= $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroinscripcion));
	$items = $consulta->fetchAll();
	//var_dump($consulta->errorInfo());
	$tr		= "";
	$count 	= 0;
	
	foreach($items as $row)
	{
		$count++;
		
		$click = "onclick='cargar_detalle_facturacion(".$nroinscripcion.",".$row["nrofacturacion"].",".$row["cat"].",".$row["codtipodeuda"].",".$count.")'";
		
		$tr .= "<tr style='background-color:#FFFFFF'>";
		$tr .= "<td align='center'>";
		$tr .= "<input type='hidden' name='nrofact".$count."' id='nrofact".$count."' value='".$row["nrofacturacion"]."' />";
		
		$tr .= "<input type='hidden' name='doc".$count."' id='doc".$count."' value='".$row["abreviado"]."' />".$row["abreviado"]." : ";
		$tr .= "<input type='hidden' name='serie".$count."' id='serie".$count."' value='".$row["serie"]."' />";
		$tr .= "<input type='hidden' name='nrodoc".$count."' id='nrodoc".$count."' value='".$row["nrodocumento"]."' />".$row["serie"]."-".$row["nrodocumento"]."</td>";
		$tr .= "<td align='center'>";
		$tr .= "<input type='hidden' name='anio".$count."' id='anio".$count."' value='".$row["anio"]."-".$meses[$row["mes"]]."' />";
		$tr .= "<input type='hidden' name='anioz".$count."' id='anioz".$count."' value='".$row["anio"]."' />";
		$tr .= "<input type='hidden' name='mesz".$count."' id='mesz".$count."' value='".$row["mes"]."' />";
		$tr .= "<input type='hidden' name='codtipodeuda".$count."' id='codtipodeuda".$count."' value='".$row["codtipodeuda"]."' />";
		$tr .= "<input type='hidden' name='categoria".$count."' id='categoria".$count."' value='".$row["cat"]."' />";
		$tr .= "<input type='hidden' name='codciclo".$count."' id='codciclo".$count."' value='".$row["codciclo"]."' />";
        $tr .= $row["anio"]."-".$meses[$row["mes"]]."";
		
		$tr .= "<input type='hidden' name='tipd".$count."' id='tipd".$count."' value='".$row["deuda"]."' /></td>";
		$tr .= "<td align='center'>";
		$tr .= "<input type='hidden' name='cat".$count."' id='cat".$count."' value='".$row["categoria"]."' />".$row["categoria"]."</td>";
		$tr .= "<td align='right' class='select'>";
		$tr .= "<input type='hidden' name='lecant".$count."' id='lecant".$count."' value='".number_format($row["lecturaanterior"])."' />";
        $tr .= number_format($row["lecturaanterior"])."</td>";
        $tr .= "<td align='right' class='select'>";
		$tr .= "<input type='hidden' name='lecact".$count."' id='lecact".$count."' value='".number_format($row["lecturaultima"])."' />";
        $tr .= number_format($row["lecturaultima"])."</td>";
        $tr .= "<td align='right' class='select'>";
		$tr .= "<input type='hidden' name='consumo".$count."' id='consumo".$count."' value='".number_format($row["consumo"])."' />";
        $tr .= number_format($row["consumo"])."</td>";
		$tr .= "<td align='right'>";
		$tr .= "<input type='hidden' name='imp".$count."' id='imp".$count."' value='".$row["imptotal"]."' />";
        $tr .= number_format($row["imptotal"],2)."</td>";
		$tr .= "<td align='center'>";
		$tr .= "<img src='../../../../images/iconos/add.png' ".$click." width='16' height='16' style='cursor:pointer' title='Realizar Rebaja de Facturacion' />";
		$tr .= "</td>";
		$tr .= "</tr>";
	}
	
	echo $tr;
	
?>