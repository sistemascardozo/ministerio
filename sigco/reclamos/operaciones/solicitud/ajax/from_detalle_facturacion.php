<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../../config.php");
	
	$codsuc			= $_POST["codsuc"];
	$nroinscripcion = $_POST["nroinscripcion"];
	$nrofacturacion = $_POST["nrofacturacion"];
	$categoria		= $_POST["categoria"];
	$codtipodeuda	= $_POST["codtipodeuda"];
	$index			= $_POST["index"];
	
	$count=0;
	
	$sql = "select d.codconcepto,c.descripcion,sum(d.importe) as imptotal
			from facturacion.detfacturacion as d
			inner join facturacion.conceptos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto)
			where d.codsuc=? and d.nroinscripcion=? and d.nrofacturacion=? and d.categoria=? and d.codtipodeuda=?
			group by d.codconcepto,c.descripcion order by d.codconcepto";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nroinscripcion,$nrofacturacion,$categoria,$codtipodeuda));
	$items = $consulta->fetchAll();
	
	$total = 0;
?>
<div style="height:300px; overflow:auto">

<table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbdetallefacturacion" rules="all" >
          <thead class="ui-widget-header" >
      <tr>
        <th width="85%" >Concepto</th>
        <th width="15%" >Importe</th>
        <th width="15%" >&nbsp;</th>
     </tr>
   </thead>
   <tbody>
     <?php
	 	foreach($items as $row)
		{
			$count++;
			
			$total += $row["imptotal"];
	 ?>
     <tr style="background:#FFF; color:#000;">
        <td align="left" >
			<input type="hidden" name="codconceptoy<?=$count?>" id="codconceptoy<?=$count?>" value="<?=$row["codconcepto"]?>" />
        	<?=strtoupper($row["descripcion"])?>
        </td>
        <td align="right" >
			<input type="hidden" name="impx<?=$count?>" id="impx<?=$count?>" value="<?=$row["imptotal"]?>" />
			<?=number_format($row["imptotal"],2)?>
        </td>
        <td align="center" >
        	<input type="text" name="impamort<?=$count?>" id="impamort<?=$count?>" readonly="readonly" class="inputtext" style="text-align:right" value="0.00" />
        </td>
     </tr>
     <?php } ?>
     <tr style="background:#FFF; color:#000;">
        <td align="right" >Total Facturacion ==></td>
        <td align="right" >
			<?=number_format($total,2)?>
            <input name="imptotalh" type="hidden" id="imptotalh" value="<?=$total?>" />
        </td>
        <td align="right" >&nbsp;</td>
     </tr>

  </tbody>
</table>
</div>
<div style="height:15px">
</div>
<div style="border:1px #000000 dashed; padding:4px;display:none" id="DivImporteRebaja" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr style="padding:4px">
    <td width="15%">Nuevo Consumo </td>
    <td width="3%" align="center">:</td>
    <td width="41%" align="center">
      <input type="text"  id="consumocalcular" class="inputtext"  onkeypress="return permite(event,'num');" size="10" />
      <input type="button" onclick="CalcularImporteConsumo();" value="Recalcular" id="">
    </td>
    <td width="41%"><label>
      Importe:
      <input type="text" name="mrebajar" id="mrebajar" class="inputtext" value="<?=$total?>" readonly="readonly" onkeyup="calcularmontos(<?=$count?>);" onkeypress="return permite(event,'num');"  />
      <input type="checkbox" name="checkbox" value="checkbox" onclick="rebajatotal(this,<?=$count?>);" style="display:none"/>
      <label style="display:none">Rebaja Todo</label>
      <input type="hidden" name="todoimporte" id="todoimporte" value="0" />
      <input type="hidden" name="idx" id="idx" value="<?=$index?>" />
      <input type="hidden" name="cont_facturacion" id="cont_facturacion" value="<?=$count?>"/>
    </label></td>
  </tr>
</table>
</div>
