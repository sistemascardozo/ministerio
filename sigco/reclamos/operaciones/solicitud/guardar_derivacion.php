<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	
	
	$objFunciones   = new clsFunciones();
	
	$codsuc         = $_POST["codsuc"];
	$codarea        = $_POST["codarea"];
	$nrosolicitud   = $_POST["nrosoli"];
	$usu_derivacion = $_POST["usu_derivacion"];
	$fecha          = $objFunciones->CodFecha($objFunciones->FechaServer());
	$idusuario      = $_SESSION["id_user"];
	$nrodetalle_org = $_POST["nrodetalle"];
	$fecha_fin      = $_POST["fecha_fin"];


	$id   		= $objFunciones->setCorrelativos("detalle_solicitudes","0","0");
	$nrodetalle	= $id[0];
	$conexion->beginTransaction();	
	$upd 	= "update reclamos.solicitud set codestadoreclamo=2 where codsuc=:codsuc and nrosolicitud=:nrosolicitud";	
	$result = $conexion->prepare($upd);
	$result->execute(array(":codsuc"=>$codsuc,":nrosolicitud"=>$nrosolicitud));
	if ($result->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }

		
	if($nrodetalle!=0)
	{
		$upDetalle = "update reclamos.detalle_solicitudes set inicial=3,fecha_finalizacion=? where codsuc=? and nrodetalle=?";
		$resultDetalle = $conexion->prepare($upDetalle);
		$resultDetalle->execute(array($fecha_fin,$codsuc,$nrodetalle_org));
		if ($resultDetalle->errorCode() != '00000') 
	    {
	        $conexion->rollBack();
	        $mensaje = "Error reclamos";
	        die(2);
	    }
	}
	

	 $sqlD = "insert into reclamos.detalle_solicitudes(codemp,codsuc,nrosolicitud,codusu,fechareg,codarea,codusu_derivacion,nrodetalle,inicial,nrodetalle_origen,fecha_finalizacion)
			 values(:codemp,:codsuc,:nrosolicitud,:codusu,:fechareg,:codarea,:codusu_derivacion,:nrodetalle,:inicial,:nrodetalle_origen,:fecha_finalizacion)";
	
	$consultaD = $conexion->prepare($sqlD);
	$consultaD->execute(array(":codemp"=>1,
							  ":codsuc"=>$codsuc,
							  ":nrosolicitud"=>$nrosolicitud,
							  ":codusu"=>$usu_derivacion,
							  ":fechareg"=>$fecha,
							  ":codarea"=>$codarea,
							  ":codusu_derivacion"=>$idusuario,
							  ":nrodetalle"=>$nrodetalle,
							  ":inicial"=>1,
							  ":nrodetalle_origen"=>$nrodetalle_org,
							  ":fecha_finalizacion"=>$fecha_fin));

	if ($consultaD->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>