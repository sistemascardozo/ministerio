<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");	
	$Op 		= $_POST["Op"];
	$codsuc  	= $_SESSION["IdSucursal"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objDrop = new clsDrop();	
	$correlativo = $objDrop->setCorrelativosVarios(19, $codsuc, "SELECT", 0);
	$entrega_cartilla=1;
	$fecha = date("d/m/Y");
	if($Id!='')
	{
		$sql = "SELECT r.nroinscripcion,r.reclamante,r.codtipoparentesco,r.codmedio,r.reclamante,r.codtipodocumento,
						r.nrodocumento,r.codsector,	r.codcalle,r.telefono,r.nrocalle,r.codubigeo,
						r.tiporeclamo,r.codconcepto,r.glosa,c.codzona,c.codantiguo,r.correlativo,c.propietario,r.fechaemision
				from reclamos.solicitud as r 
				inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
				where r.codemp=1 and r.codsuc=? and r.nrosolicitud=?";
		
		$consulta = $conexion->prepare($sql);
		$consulta->execute(array($codsuc,$Id));
		$row = $consulta->fetch();
		$fecha = $objDrop->DecFecha($row['fechaemision']);
		$entrega_cartilla= $row['entrega_cartilla'];
		$correlativo= $row['correlativo'];
	}
	
?>
<script>
	var nDest 	= 0;
	var nDestC	= 0;
	var cont 	= 0; 	
	var urldir 	='<?php echo $_SESSION['urldir'];?>';
	var codsuc	= <?php echo $codsuc?>;
	var CorrelativoO = '<?=$correlativo?>'
	$(document).bind('keydown',function(e){
 //alert($(":focus").attr('id'))
  if ( e.which == 113 )
  {
    buscar_usuarios();
      return false;
  };
  if ( e.which == 114 )
  {
      buscar_tipo_reclamos()
      return false;
  };
   
  });
$(document).bind('keyup',function(e){
 if ( e.which == 120 )
	 ValidarForm('<?=$Op?>');
	return false;
  	
  });
</script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/required.js" language="JavaScript"></script>
<script type="text/javascript" src="js_reclamos.js" language="JavaScript"></script>
<style>
	.estiloX{
		background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px
	}
	.myTable{
		background-color:#0099CC; border: 1 #000000 solid;
	}
</style>

<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    <tbody>
  <tr>
    <td colspan="6" class="TitDetalle" style="height:5px"></td>
  </tr>
	<tr>
	  <td colspan="7" class="TitDetalle">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td>Nro. Solicitud</td>
	            <td width="30" align="center">:</td>
	            <td><span class="CampoDetalle">
	              <input name="nrosolicitud" type="hidden" id="nrosolicitud" size="10" maxlength="10" value="<? echo $Id; ?>"/>
	              <input name="correlativo" type="text" id="correlativo" size="10" maxlength="10" value="<? echo $correlativo; ?>" class="inputtext" readonly="readonly"/>
	            <div <?php if($Op!=0 ) echo "style='display:none;'"; else echo "style='display:inline;'";  ?>>
	            <input type="checkbox"   id="chkanterior" onclick="CambiarAnterior(this)"/>
	            <input type="hidden" id="tempanterior" value="0" name="tempanterior">
	            <label> Anterior</label>
	            </div>
	            </span></td>
	            			  <td class="TitDetalle" align="right"><span class="CampoDetalle">Fecha de Emsion </span></td>
							  <td width="30" align="center">:</td>
							  <td colspan="10" class="CampoDetalle">
							    <input name="fechaemision" type="text" id="fechaemision" maxlength="10" class="inputtext fecha" value="<?=$fecha?>" style="width:80px;"/>
							   
							</td>
							</tr>
	          <tr>
	            <td>Propietario</td>
	            <td align="center">:</td>
	            <td colspan="4">
	              <input type="hidden" name="nroinscripcion" id="nroinscripcion" size="10" class="inputtext"  value="<?php echo $row["nroinscripcion"]?>" />
	              <input type="text" name="codantiguo" id="codantiguo" class="inputtext entero" maxlength="15" value="<?php echo $row["codantiguo"]?>" title="Ingrese el nro de Inscripcion" onkeypress="ValidarCodAntiguo(event)" style="width:70px;"/>
	              <span class="MljSoft-icon-buscar" title="Buscar Usuario" onclick="buscar_usuarios();"></span>
	              <input class="inputtext" name="propietario" type="text" id="propietario" maxlength="200" readonly="readonly" value="<?php echo $row["propietario"]?>" style="width:400px;" />
	              </td>
	            </tr>
	          <tr>
	            <td>Parentesco</td>
	            <td align="center">:</td>
	            <td><?php $objDrop->drop_tipo_parentesco($row["codtipoparentesco"]); ?></td>
	            <td align="right">Medio de Reclamo</td>
	            <td align="center">:</td>
	            <td><?php $objDrop->drop_medio_reclamo($row["codmedio"]); ?></td>
	          </tr>
	          <tr>
	            <td>Reclamante</td>
	            <td align="center">:</td>
	            <td colspan="4"><label>
	              <input type="text" name="reclamante" id="reclamante" class="inputtext" value="<?php echo $row["reclamante"]?>" style="width:450px;" />
	              </label></td>
	            </tr>
	          <tr>
	            <td>Documento</td>
	            <td align="center">:</td>
	            <td><?php $objDrop->drop_tipodocumento($row["codtipodocumento"]); ?></td>
	            <td align="right">Nro. Documento</td>
	            <td align="center">:</td>
	            <td><input type="text" name="nrodocumento" id="nrodocumento" size="10" maxlength="11" class="inputtext" onkeypress="return permite(event,'num');" value="<?php echo $row["nrodocumento"]?>" /></td>
	            </tr>
	          <tr>
	            <td>Sector</td>
	            <td align="center">:</td>
	            <td>
			<?php 
                          echo  $objDrop->drop_sectores2($codsuc, $row["codsector"]); 
                            if($row["codsector"]!="")
                            {
                        ?>
                	<script>
                            cargar_calles(<?php echo $row["codsector"]?>, codsuc, <?php echo $row["codzona"]?>, <?php echo $row["codcalle"]?>)
                        </script>
                        <?php } ?>
                    </td>
	            <td align="right">Telefono</td>
	            <td align="center">:</td>
	            <td><input type="text" name="telefono" id="telefono" size="10" maxlength="11" class="inputtext" value="<?php echo $row["telefono"]?>" /></td>
	            </tr>
	          <tr>
	            <td>Calle</td>
	            <td align="center">:</td>
	            <td>
	              <div id="div_calles">
	                <select name="calles" id="calles" style="width:220px" class="select">
	                  <option value="0">--Seleccione la Calle--</option>
	                  </select>
	                </div>
	              </td>
	            <td align="right">Nro. Calle</td>
	            <td align="center">:</td>
	            <td><input type="text" name="nrocalle" id="nrocalle" size="10" maxlength="11" class="inputtext" value="<?php echo $row["nrocalle"]?>" /></td>
	            </tr>
	          <tr>
	            <td>Departamento</td>
	            <td align="center">:</td>
	            <td><?php 
                        $objDrop->drop_ubigeo("departamento",
                                            "--Seleccione el Departamento--",
                                            " where codubigeo LIKE ? ",
                                            array("%0000"),
                                            "onchange='cargar_provincia(this.value,0);'",
                                            "080000"); 
                        ?>
                </td>
	            <td align="right">Provincia</td>
	            <td align="center">:</td>
	            <td>
	              <div id="div_provincia">
	                <select id="provincia" name="provincia" style="width:220px" class="select">
	                  <option value="0">--Seleccione la Provincia--</option>
	                  </select>
	                </div>
	              </td>
	            </tr>
	          <tr>
	            <td>Distrito</td>
	            <td align="center">:</td>
	            <td>
	              <div id="div_distrito">
	                <select id="distrito" name="distrito" style="width:220px" class="select">
	                  <option value="0">--Seleccione un Distrito--</option>
	                  </select>
	                </div>	
	              </td>
	            <td align="right">Recepcionado por</td>
	            <td align="center">:</td>
	            <td><input type="text" name="recepcionista" id="recepcionista" size="40" maxlength="11" class="inputtext" readonly="readonly" value="<?php echo $_SESSION['nombre']?>" /></td>
	            </tr>
                    <tr>
                        <td colspan="6"><hr/></td>
                    </tr>
                                     
	          <tr>
	            <td colspan="6" class="TitDetalle" style="height:5px">
	            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="12%">Tipo de Reclamo </td>
	            <td width="3%" align="center">:</td>
	            <td class="CampoDetalle">
	            	<div class="buttonset" style="display:inline">
		              <input id="tipo_reclamo1" name="radiobutton" type="radio" value="1" onclick="cargar_conceptos_reclamos(1,1000)" <?php echo $d="";$row["tiporeclamo"]==1?$d="checked='cheked'":$d; echo $d;?> <?php if($row['tiporeclamo']==""){echo 'checked=""';} ?> />
		              <label for="tipo_reclamo1"   >Comerciales con Facturacion</label>
		              <input id="tipo_reclamo2" name="radiobutton" type="radio" value="2" onclick="cargar_conceptos_reclamos(2,1000)" <?php echo $d="";$row["tiporeclamo"]==2?$d="checked='cheked'":$d; echo $d;?> />
		              <label for="tipo_reclamo2"  >Comerciales sin Facturacion</label>
		              <input id="tipo_reclamo3" name="radiobutton" type="radio" value="3" onclick="cargar_conceptos_reclamos(3,1000)" <?php echo $d="";$row["tiporeclamo"]==3?$d="checked='cheked'":$d; echo $d;?> />
		              <label for="tipo_reclamo3">Operacionales</label>
		            </div>
		            <span class="MljSoft-icon-buscar" title="Buscar Usuario" onclick="buscar_tipo_reclamos();"></span>
		            <input type="hidden" name="tiporeclamo" id="tiporeclamo" value="<?=isset($row["tiporeclamo"])?$row["tiporeclamo"]:0?>" />
		            <input type="hidden" name="codzona" id="codzona" value="<?=isset($row["codzona"])?$row["codzona"]:''?>" />
	        
		        </td>
	            </tr>
	          <tr>
	            <td>Reclamo</td>
	            <td align="center">:</td>
	            <td class="CampoDetalle">
	              <div id="div_conceptos_reclamos">
                        <?php echo $conceptosreclamos ?>  
	                <select id="codconcepto" name="codconcepto" class="select" style="width:500px">
	                  <option>--Seleccione el Concepto del Reclamo--</option>
	                </select>
	              </div>
                    </td>
	            </tr>
	          <tr>
	            <td valign="top">Motivo</td>
	            <td align="center" valign="top">:</td>
	            <td class="CampoDetalle"><textarea name="glosa" id="glosa" class="inputtext" style="font-size:12px; width:500px;" rows="5"><?php echo $row["glosa"]?></textarea></td>
	          </tr>
                 
	          </table>
	            </td>
	            </tr>
	         
	          </table>
	    </td>
	  </tr>
          <tr>
	  <td colspan="6">
            <input type="hidden" name="contadorconcepto" id="contadorconcepto" value="<?php echo $contador?>" />	  
          </td>
	  </tr>
          </tbody>
    </table>
 </form>
</div>
<div id="DivNose" title="Ver Detalle de Facturacion para Realizar la Rebaja"  >
    <div id="div_detalle_facturacion"></div>
</div>

<?php 
	if($Id!="")
	{
?>
            <script>                
                    $("#departamento").val("<?php echo  substr($row["codubigeo"],0,2)."0000"?>")
                    cargar_provincia("<?php echo $row["codubigeo"]?>","<?php echo $row["codubigeo"]?>");
                    cargar_distrito("<?php echo $row["codubigeo"]?>","<?php echo $row["codubigeo"]?>");		
                    cargar_conceptos_reclamos(<?php echo $row["tiporeclamo"]?>,<?php echo $row["codconcepto"]?>)
                    //cargar_facturacion(<?php echo $row["nroinscripcion"]?>)
                   // $("#monto_reclamo").val(<?=$totalreclamado?>)
            </script>
<?php } ?>