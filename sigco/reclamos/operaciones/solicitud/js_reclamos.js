// JavaScript Document
contador=0;
nroreclamo=0;
nroinscripcion=0;
estado=0;
texto="";
$(document).ready(function()
{
	$.mask.definitions['H']='[012]';
    $.mask.definitions['N']='[012345]';
    $.mask.definitions['n']='[0123456789]';
	
	$("#hora_inspeccion,#hora_inspeccion2,#hora_citacion").mask("Hn:Nn");
	
	cargar_conceptos_reclamos(1,1000);
    
	$( "#tabs" ).tabs();
    $( "#dialog:ui-dialog" ).dialog( "destroy" );
    $( "#DivNose" ).dialog({
            autoOpen: false,
            height: 450,
            width: 650,
            modal: true,
            resizable:false,
            buttons: {
                    "Aceptar": function() {
                            guardarrebajas();
                    },
                    "Cancelar": function() {
                            $( this ).dialog( "close" );
                    }
            },
            close: function() {

            }
    });
    $("#fecha_notificacion,#fecha_inspeccion,#fecha_citacion").datepicker(  
                                                                            {dateFormat:'dd/mm/yy',
                                                                             changeMonth:true,
                                                                             changeYear:true}
                                                                        );
    //new events
    var iddep = $("#departamento").val();
    if(typeof(iddep)!="undefined")
    {
        cargar_provincia(iddep, '250100');
        cargar_distrito('250100', '250101');
    }
    $("#tipodocumento").change(function()
    {
        var idtd = $(this).val();
        if(idtd!="0")
        {
            $("#nrodocumento").val('');
            if(idtd==1) $("#nrodocumento").attr("maxlength","8");
                else $("#nrodocumento").attr("maxlength","11");        
            $("#nrodocumento").focus();
        }    
    });
    
    load_conceptos_reclamos();
    
    $( "#dialog-form-imprimir:ui-dialog" ).dialog( "destroy" );
    $( "#dialog-form-imprimir" ).dialog({
            autoOpen: false,
            height: 170,
            width: 450,
            modal: true,
            resizable:false,
            buttons: {
                    "Imprimir": function() {
                            imprimir_formatos();
                    },
                    "Cerrar": function() {
                            $( this ).dialog( "close" );
                    }
            },
            close: function() {

            }
    });
	$( "#dialog-form-derivar:ui-dialog" ).dialog( "destroy" );
	$( "#dialog-form-derivar" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		resizable:false,
		buttons: {
			"Derivar": function() {
				guardar_derivacion_reclamo();
			},
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {

		}
	});


})


function buscar_tipo_reclamos()
{
	AbrirPopupBusqueda("../../catalogo/conceptosreclamo/?Op=5",1100,500)
}
function RecibirTipoReclamo(id,tipo)
{
	$("#tipo_reclamo"+tipo).attr("checked", true);
	$(".buttonset").buttonset('refresh');
	cargar_conceptos_reclamos(tipo,id)
	$("#glosa").focus().select();
}

function cargar_conceptos_reclamos(tipo, seleccion)
{
	$.ajax({
		 url:urldir+'ajax/conceptos_reclamos_drop.php',
		 type:'POST',
		 async:true,
		 data:'tiporeclamo='+tipo+'&seleccion='+seleccion,
		 success:function(datos){
                    $("#div_conceptos_reclamos").html(datos);
                    $("#tiporeclamo").val(tipo);
                    $("#codconcepto").css("width","450px")
                    $(".buttonset").buttonset('refresh');
		 }
	}) 
}
function cargar_facturacion(nroinscripcion)
{
	$.ajax({
		 url:'ajax/ver_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'nroinscripcion='+nroinscripcion+'&codsuc='+codsuc,
		 success:function(datos){
                    quitar_row_facturacion();			
                    $("#tbfacturacion").append(datos)
		 }
	}) 
}
function buscar_usuarios()
{
	AbrirPopupBusqueda("../../../catastro/operaciones/actualizacion/?Op=5", 1150, 500)
}
function Recibir(id)
{
	cargar_datos_usuarios(id)
	//cargar_facturacion(id)
}
function cargar_datos_usuarios(id)
{
	$.ajax({
		 url:'../../../../ajax/clientes.php',
		 type:'POST',
		 async:true,
         dataType: 'json',
		 data:'nroinscripcion=' + id + '&codsuc='+codsuc,
		 success:function(datos){
			//var r=datos.split("|")

			$("#nroinscripcion").val(id)
			$("#codantiguo").val(datos.codantiguo)
			$("#propietario").val(datos.propietario)
			$("#tipoparentesco").val(1)
			$("#reclamante").val(datos.propietario)
			$("#tipodocumento").val(datos.codtipodocumento)
			$("#nrodocumento").val(datos.nrodocumento)
			$("#medioreclamo").val(1)
			$("#codsector").val(datos.codsector)
			$("#codzona").val(datos.codzona)
			$("#telefono").val(datos.telefono)
			
			cargar_calles(datos.codsector, codsuc, datos.codzona, datos.codcalle)
			
			$("#nrocalle").val(datos.nrocalle)

			$("#direccion").val(datos.calle)
			$("#nrocalle2").val(datos.nrocalle)
			$("#urbanizacion").val(datos.zona)
			$("#nrolote").val(datos.lote)
			$("#celular").val(datos.telefono)

		 }
	}) 
}
function quitar_row_facturacion()
{
	var miTabla 	= document.getElementById("tbfacturacion");
	var numFilas 	= miTabla.rows.length;

	for(i=1;i<=numFilas;i++)
	{
		try
		{
			miTabla.deleteRow(1);
		}catch(exp)
		{
			
		}
	}
}
function cargar_detalle_facturacion(nroinscripcion,nrofacturacion,categoria,codtipodeuda,idx)
{
	
	
	var id= parseInt($("#tbrebajas tbody tr").length)
    for(var i=1; i<=id; i++)
    {
    	if($("#nrofacturacion"+i).val()==nrofacturacion)
    	{
    		Msj($("#tbfacturacion"),'La Facturación ya esta agregada')
    		return false
    	}
    }
    cargar_from_detalle_facturacion(nroinscripcion,nrofacturacion,categoria,codtipodeuda,idx)
	
}
function cargar_from_detalle_facturacion(nroinscripcion,nrofacturacion,categoria,codtipodeuda,idx)
{
	$.ajax({
		 url:'ajax/from_detalle_facturacion.php',
		 type:'POST',
		 async:true,
		 data:'nroinscripcion='+nroinscripcion+'&codsuc='+codsuc+'&nrofacturacion='+nrofacturacion+
		 	  '&categoria='+categoria+'&codtipodeuda='+codtipodeuda+'&index='+idx,
		 success:function(datos){
			$("#div_detalle_facturacion").html(datos)
			$( "button").button();
			$( "#DivNose" ).dialog( "open" );
		 }
	}) 
}
function rebajatotal(obj,count)
{
	$("#mrebajar").val("0.00")
	for(i=1;i<=count;i++)	
	{
		codconcepto=$("#codconceptoy"+i).val();
		if(obj.checked==true)
		{
			$("#impamort"+i).val(parseFloat($("#impx"+i).val()).toFixed(2))
			$("#todoimporte").val(1)
			
			$("#mrebajar").attr("readOnly",true)

			$("#mrebajar").val(parseFloat($("#imptotalh").val()).toFixed(2))

		}else{
			$("#todoimporte").val(0)
			$("#mrebajar").val("0.00")
			$("#impamort"+i).val("0.00")
			
			$("#mrebajar").attr("readOnly",false)
		}
	}
}
function calcularmontos(count)
{
	var mRedodento 	= 0;
	var codconcepto	= 0;
	var mrebaja		=  $("#mrebajar").val()
	if(mrebaja==""){mrebaja=0;}
	
	var auto = parseFloat(mrebaja) -parseFloat(mRedodento)

	for(i=1;i<=count;i++)	
	{
		codconcepto=$("#codconceptoy"+i).val();
		if(eval(auto>=$("#impx"+i).val()))
		{
			$("#impamort"+i).val(parseFloat($("#impx"+i).val()).toFixed(2))
			auto = parseFloat(auto) - parseFloat($("#impx"+i).val())
		}else{
			$("#impamort"+i).val(parseFloat(auto).toFixed(2))
			auto=0
		}
	}
}

function guardarrebajas()
{
	
    var vrebaja = $("#mrebajar").val();
        vrebaja = parseFloat(vrebaja);
        if(eval(vrebaja<=0))
	{
            alert('El Monto Ingesado no es Valido')
            return false;	
        }
        else {
            var imptH 	= Round($("#imptotalh").val(),2)
            var imptD 	= Round($("#mrebajar").val(),2)
            var index	= $("#idx").val()
            
            if(imptD > imptH)
            {
                    alert("El Monto a Rebajado no puede ser Mayor al Monto a Rabajar")
                    return
            }

            var cad=""
           
            for(i=1;i<=$("#cont_facturacion").val();i++)
            {
                    cad=cad+"nrofacturacion"+i+"="+$("#nrofact"+index).val()+"&codconcepto"+i+"="+$("#codconceptoy"+i).val();
                    cad=cad+"&imporiginal"+i+"="+str_replace($("#impx"+i).val(),",","")+"&imprebajado"+i+"="+str_replace($("#impamort"+i).val(),",","");
                    cad=cad+"&codtipodeuda"+i+"="+$("#codtipodeuda"+index).val()+"&categoria"+i+"="+$("#categoria"+index).val();
                    cad=cad+"&nrodocumento"+i+"="+$("#nrodoc"+index).val()+"&anio"+i+"="+$("#anioz"+index).val()+"&mes"+i+"="+$("#mesz"+index).val();
                    cad=cad+"&codciclo"+i+"="+$("#codciclo"+index).val()+"&"
                    cad=cad+"&lecant"+i+"="+$("#lecant"+index).val()+"&"
                    cad=cad+"&lecact"+i+"="+$("#lecact"+index).val()+"&"
                    cad=cad+"&consumo"+i+"="+$("#consumo"+index).val()+"&"
                    cad=cad+"&lecturarec"+i+"="+$("#lecact"+index).val()+"&"
                    cad=cad+"&consumorec"+i+"="+$("#consumo"+index).val()+"&"

                 
            }
           	
            
            $.ajax({
                    url:'clase/proceso.php',
                    type:'POST',
                    async:true,
                    data:cad+"count="+$("#cont_facturacion").val(),
                    success:function(datos){
                            agregar_rebajas_facturacion();

                    }
            }) 
       }
}
function cTotalRebaja()
{
	var total=0	
	$(".importeoriginal").each(function()
    {
    	total +=parseFloat($(this).val())
	
    });
    $("#monto_reclamo").val(parseFloat(total).toFixed(2))
}
function agregar_rebajas_facturacion()
{
	var index	= $("#idx").val()
	contador++

	$("#tbrebajas").append("<tr style='background-color:#FFFFFF' id='tr_"+contador+"' >"+
						   "<td align='center'>"+
						   "<input type='hidden' name='nrofacturacion"+contador+"' id='nrofacturacion"+contador+"' value='"+$("#nrofact"+index).val()+"' />"+
						   "<input type='hidden' name='codtipodeuda"+contador+"' id='codtipodeuda"+contador+"' value='"+$("#codtipodeuda"+index).val()+"' />"+
						   "<input type='hidden' name='ct"+contador+"' id='ct"+contador+"' value='"+$("#categoria"+index).val()+"' />"+
						   "<input type='hidden' id='impori"+contador+"' value='"+$("#imptotalh").val()+"' class='importeoriginal' />"+
						    $("#doc"+index).val() + ' : '+$("#nrodoc"+index).val() +"</td>"+
						   "<td align='center'>"+$("#anio"+index).val()+"</td>"+
						   "<td align='center'>"+$("#cat"+index).val()+"</td>"+
						   "<td align='right' class='select' >"+$("#lecant"+index).val()+"</td>"+
						   "<td align='right' class='select' >"+$("#lecact"+index).val()+"</td>"+
						   "<td align='right' class='select' >"+$("#consumo"+index).val()+"</td>"+
						   
						   "<td align='right'>"+parseFloat($("#mrebajar").val()).toFixed(2)+"</td>"+
						   "<td align='center'>"+
						   //"<img onclick='remove_row("+contador+")' width='16' height='16' style='cursor:pointer' title='Eliminar Registro' />"+
						   "<span class='icono-icon-trash' title='Quitar Facturaci&oacute;n'  onClick='remove_row("+contador+")'></span>"	+					   
						   "</td>"+
						   "</td>");
	
	$( "#DivNose" ).dialog( "close" );
	$("#contadorconcepto").val(contador)
	cTotalRebaja()
}
function remove_row(idx)
{
	$("#tr_"+idx).remove()
	cTotalRebaja();
}
function cargar_from_impresion(nrorec,nroinsc)
{
	nroreclamo=nrorec
	nroinscripcion=nroinsc
	$( "#dialog-form-imprimir" ).dialog( "open" );
}
function imprimir_formatos()
{
	var dir="formatos/";
	var url="";
	
	if($("#formato").val()==0)
	{
		alert("Seleccione el Formato a Imprimir")
		return false
	}
	if($("#formato").val()==1)
	{
		url=dir+"formato1.php"
	}
	if($("#formato").val()==2)
	{
		url=dir+"formato2.php"
	}
	if($("#formato").val()==3)
	{
		url=dir+"formato3.php"
	}
	if($("#formato").val()==4)
	{
		url=dir+"formato4.php"
	}
	if($("#formato").val()==5)
	{
		url=dir+"formato5.php"
	}
	if($("#formato").val()==6)
	{
		url=dir+"formato6.php"
	}
	if($("#formato").val()==7)
	{
		url=dir+"formato7.php"
	}
	if($("#formato").val()==8)
	{
		url=dir+"formato8.php"
	}
	if($("#formato").val()==9)
	{
		url=dir+"formato9.php"
	}
	if($("#formato").val()==10)
	{
		url=dir+"cedula.php"
	}
	if(url!="")
	{
		AbrirPopupImpresion(urldir+"sigco/reclamos/operaciones/ingresos/"+url+'?nroreclamo='+nroreclamo + '&codsuc='+codsuc+'&nroinscripcion='+nroinscripcion,800,600)
	}
}
function cargar_from_ajax_derivacion(nrorec,estado,text,par)
{
	if(estado!=1)
	{
		alert("El Reclamo no se puede Derivar por que Se encuentra con el Estado: "+text)
		return
	}
	
	cargar_from_derivacion(nrorec,par)
	
}
function cargar_from_derivacion(nroreclamo,par)
{
	$.ajax({
		 url:urldir+'sigco/reclamos/operaciones/ingresos/ajax/from_derivarcion_reclamo.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&nroreclamo='+nroreclamo+'&parametro='+par,
		 success:function(datos){
			$("#div_derivar_reclamos").html(datos)
			$( "#dialog-form-derivar" ).dialog( "open" );
		 }
	}) 
}
function guardar_derivacion_reclamo()
{
	if($("#areas").val()==0)
	{
		alert("Seleccione el Area a Derivar")
		return
	}
	if($("#usuario_area").val()==0)
	{
		alert("Seleccione el Usuario del Area a Derivar")
		return
	}
	
	var r=confirm("Al Derivar ya no podra realizar ninguna Modificacion.¿Esta Seguro que desea Derivar el Reclamo?")
	if(r==false)
	{
		return false	
	}
	
	$.ajax({
		 url:urldir+'sigco/reclamos/operaciones/ingresos/guardar_derivacion.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&codarea='+$("#areas").val()+'&nroreclamo='+$("#nrorecla").val()+
		 	  '&usu_derivacion='+$("#usuario_area").val()+'&nrodetalle='+$("#nrodetalle").val()+'&fecha_fin='+$("#fecha_fin").val(),
		 success:function(datos){
			var r=datos.split("|")

			window.parent.OperMensaje(r[0],1)
			location.href='index.php'
		 }
	}) 
}
function SeleccionaIdB(obj,e,eText)
{
	Id = 'Id=' + obj.id;
	IdAnt=Id2;
	Id2 = obj.id;
	estado=e
	texto=eText
	if (IdAnt!='')
	{
		Limpiar(IdAnt);
	}
	obj.style.backgroundColor="#999999";
}
function Operacion(Op)
{
	if(Op!=4)
	{
		if(Op==2)
		{
			if(estado!=1)
			{
				alert("El Reclamo no se puede MODIFICAR por que Se encuentra con el Estado: "+texto)
				return
			}
		}
		var url = 'mantenimiento.php?Op='+Op;
		if(Op!=1)
		{
			if(Id=="")
			{
				alert('Seleccione un Elemento del Item')
				return
			}
			url=url+'&'+Id;
		}
		location.href=url;
	}else{
		location.href=urldir+'admin/indexB.php'
	}
}
function cargar_usuario_area(codarea)
{
	$.ajax({
		 url:'../../../../ajax/usuarios_areas_drop.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&codarea='+codarea,
		 success:function(datos){
			$("#div_usuario").html(datos)
		 }
	}) 
}
function ver_popup_detalle_reclamos(nroreclamo)
{
	AbrirPopupImpresion(urldir+'sigco/reclamos/operaciones/ingresos/popup/popup_detalle_reclamos.php?codsuc='+codsuc+'&nroreclamo='+nroreclamo,1000,600)
}
function ValidarForm(Op)
{
        $( "#tabs" ).tabs("select",0);
        bval = true;
        bval = bval && $("#correlativo").required('','Ingrese Correlativo');
        bval = bval && $("#nroinscripcion").required('','Seleccione el Usuario');
        bval = bval && $("#tipoparentesco").required('0','Seleccione el Tipo de Parentesco');
        bval = bval && $("#medioreclamo").required('0','Seleccione el Medio de reclamo');
        bval = bval && $("#reclamante").required('','El Nombre del Reclamante no puede ser NULO');
        bval = bval && $("#tipodocumento").required('','Seleccione el Tipo de Documento');
        //bval = bval && $("#nrodocumento").required('','Digite el Nro. de Documento');
        
        if($("#tipodocumento").val()!="0")
        {
        	if($("#nrodocumento").val()=="")
        	{
	           bval = bval && $("#nrodocumento").required('','Digite el Nro. de Documento'); 
	        }
	        else
	        {
	        	var idtd = $("#tipodocumento").val();
	        
	            t = $("#nrodocumento").val().length;
	            if(idtd==1)
	            {
	                if(t!=8)
	                {
	                    Msj($("#nrodocumento"),"Ingrese un DNi correcto");
	                    $("#nrodocumento").focus();
	                    return false;
	                }
	            }
	            else 
	            {
	                if(t!=11)
	                {
	                    Msj($("#nrodocumento"),"Ingrese un RUC correcto");
	                    $("#nrodocumento").focus();
	                    return false;
	                }                            
	            }
	        }
        }

        bval = bval && $("#sector").required('0','Seleccione el Sector');
        bval = bval && $("#calles").required('0','Seleccione la Calle');
        bval = bval && $("#nrocalle").required('','El Nro. de Calle no puede ser NULO');
        bval = bval && $("#departamento").required('0','Seleccione el Departamento');
        bval = bval && $("#provincia").required('0','Seleccione la Provincia');
        bval = bval && $("#distrito").required('0','Seleccione el Distrito');
         
        //Tabs Tipo Reclamo
        if(($("#tiporeclamo").val()==0||$("#codconcepto").val()==0||$("#glosa").val()==''||$("#pruebas").val()=='')&&bval==true) $( "#tabs" ).tabs("select",1);
        bval = bval && $("#tiporeclamo").required('0','Seleccione el Tipo de Solicitud'); 
        bval = bval && $("#codconcepto").required('0','Seleccione el Concepto de la Solicitud'); 
        bval = bval && $("#glosa").required('','Digite el Motivio de la Solicitud'); 
           
        if(bval)
        {        
            

            GuardarP(Op)
        }
        else 
        {
            return false;
        }
}
function Cancelar()
{
   location.href='index.php';
}        
function load_conceptos_reclamos()        
{
    valor = $("input[name='radiobutton']:checked").val();
    if(typeof(iddep)!="undefined")
    {
       cargar_conceptos_reclamos(valor,1000);
    }
}
function CalcularImporteConsumo()
{
	var consumo = $("#consumocalcular").val()
	if(eval(consumo<=0))
	{
            Msj($("#consumocalcular"),'El Consumo Ingesado no es Valido')
            return false;	
   }
   $.ajax({
		 url:'../ingresos/ajax/calcularimporte.php',
		 type:'POST',
		 async:true,
		 data:'codsuc='+codsuc+'&consumo='+consumo+'&nroinscripcion='+$("#nroinscripcion").val()+'&codciclo='+$("#codciclo").val(),
		 success:function(datos){
			
			$("#mrebajar").val(datos).keyup()
		 }
	}) 
}
function MostrarDatosUsuario(datos)
{
	$("#nroinscripcion").val(datos.nroinscripcion)
	$("#propietario").val(datos.propietario)
	$("#tipoparentesco").val(1)
	$("#reclamante").val(datos.propietario)
	$("#tipodocumento").val(datos.codtipodocumento)
	$("#nrodocumento").val(datos.nrodocumento)
	$("#medioreclamo").val(1)
	$("#sector").val(datos.codsector)
	$("#codzona").val(datos.codzona)
	$("#telefono").val(datos.telefono)
	cargar_calles(datos.codsector,codsuc,datos.codzona,datos.codcalle)
	$("#nrocalle").val(datos.nrocalle)

	$("#direccion").val(datos.calle)
	$("#nrocalle2").val(datos.nrocalle)
	$("#urbanizacion").val(datos.zona)
	$("#nrolote").val(datos.lote)
	$("#celular").val(datos.telefono)
	
}
function CambiarAnterior(Check)
{
	if(Check.checked==true)
	{
		$("#correlativo").attr('readonly',false)
		$("#correlativo").focus().select();
		Msj($("#correlativo"),'Ingrese Correlativo')
		$("#tempanterior").val(1)
	}
	else
	{
		$("#correlativo").val(CorrelativoO);
		$("#correlativo").attr('readonly',true)
		$("#tempanterior").val(0)
	}

}