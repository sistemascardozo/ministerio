<?php
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  $TituloVentana = "RECLAMOS";
  $Activo=1;
  unset($_SESSION["oreclamos"]);  
  $Op     = isset($_GET['Op'])?$_GET['Op']:0;
  if($Op<5) CuerpoSuperior($TituloVentana);
  $codsuc   = $_SESSION['IdSucursal'];
  $valor    = isset($_GET["Valor"])?strtoupper($_GET["Valor"]):"";  
  $fecha_fin  = date('Y-m-d');
  $Criterio='Reclamos';
  $FormatoGrilla = array ();
  $condicional = " AND rec.liberado = '1'";
  $Sql = "SELECT rec.correlativo,clie.codantiguo,clie.propietario,rec.reclamante,
            tipdoc.abreviado|| ':' ||rec.nrodocumento,
            rec.motivoliberacion,estado.descripcion as estText,rec.glosa,rec.codestadoreclamo,1,1,rec.nroreclamo
            FROM reclamos.reclamos as rec
            inner join catastro.clientes as clie on(rec.codemp=clie.codemp and rec.codsuc=clie.codsuc and
            rec.nroinscripcion=clie.nroinscripcion)
            inner join public.tipodocumento as tipdoc on(rec.codtipodocumento=tipdoc.codtipodocumento)
            inner join public.tipoparentesco as tippar on (rec.codtipoparentesco=tippar.codtipoparentesco)
            inner join reclamos.estadoreclamo as estado on(rec.codestadoreclamo=estado.codestadoreclamo)";
  if($Op==5)
  {
    $and = " and rec.fin_reclamo=0 and  rec.codestadoreclamo NOT IN (6,10)";
    $condicional = '';
  }
   if($Op==6)
  {
    $FActual=date('Y-m-d');
    $and = " and rec.fin_reclamo=0 AND rec.fecha_citacion<'".$FActual."'";
  }
  $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL   
  $FormatoGrilla[1] = array('1'=>'rec.nroreclamo', '2'=>'clie.codantiguo','3'=>'clie.propietario',
                            '4'=>'tipdoc.abreviado','5'=>'rec.nrodocumento','6'=>'rec.motivoliberacion','7'=>'rec.reclamante',
                            '8'=>'rec.correlativo');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op; 
  $FormatoGrilla[3] = array('T1'=>'Nro. Reclamo', 'T2'=>'Nro. Inscripcion','T3'=>'Propietario','T4'=>'Reclamante'
                            ,'T5'=>'Documento','T6'=>'Motivo de Liberacion','T7'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'left');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60','W2'=>'20','W3'=>'150','W4'=>'50','W5'=>'100','W6'=>'212','W7'=>'120','W8'=>'60');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1200;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " and (rec.codemp=1 and rec.codsuc=".$codsuc . $and . ") " . $condicional . " ORDER BY rec.correlativo desc ";                                   //Orden de la Consulta
  if($Op<5)
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'0',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar Reclamo',       //Titulo del Botón
              'BtnF1'=>'onclick="Modificar(this);"',  //Eventos del Botón
              'BtnCI1'=>'10',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnRestablecer', //y aparece este boton
              'BtnI2'=>'imprimir.png', 
              'Btn2'=>'Imprimir Formato de Reclamos', 
              'BtnF2'=>'onclick="ImprimirR(this)"', 
              'BtnCI2'=>'11', 
              'BtnCV2'=>'1',
              'BtnId3'=>'BtnDerivar', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Realizar Derivacion del Reclamo', 
              'BtnF3'=>'onclick="Derivar(this)"', 
              'BtnCI3'=>'11', 
              'BtnCV3'=>'1',
              'BtnId4'=>'BtnEliminar', 
              'BtnI4'=>'documento.png', 
              'Btn4'=>'Ver Detalle del Reclamo', 
              'BtnF4'=>'onclick="VerDetalle(this)"', 
              'BtnCI4'=>'11', //campo 3
              'BtnCV4'=>'1',
              'BtnId5'=>'BtnEliminard', 
              'BtnI5'=>'eliminar.png', 
              'Btn5'=>'Eliminar el registro', 
              'BtnF5'=>'onclick="Eliminar(this)"', 
              'BtnCI5'=>'11', //campo 5
              'BtnCV5'=>'1'
              );//igua a 1);
  else
    {
      $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'11',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
     
      $FormatoGrilla[5] = array('W1'=>'60','W2'=>'50','W3'=>'250','W4'=>'50','W5'=>'50','W6'=>'212','W7'=>'120','W8'=>'20');  
    }

  $FormatoGrilla[10] = array(array('Name' => 'id', 'Col' => 12), array('Name' => 'estado', 'Col' => 9), array('Name' => 'texto', 'Col' => 7), array('Name' => 'nroinscripcion', 'Col' => 2)); //DATOS ADICIONALES  
  $FormatoGrilla[11] = 7; //FILAS VISIBLES            
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],850,630);
  Pie();
  
?>
<script type="text/javascript">
var urldir = '<?=$urldir?>'
var codsuc = '<?= $codsuc ?>'
function Enviar(obj)
{
  var Id = $(obj).parent().parent().attr('id');
  Id2 = Id.substr(Id.indexOf('-') + 1);
  
  opener.Recibir(Id2);
  window.close();
}

function Modificar(obj)
{
  $("#form1").remove();
  var Id     = $(obj).parent().parent().data('id')
  var estado = $(obj).parent().parent().data('estado')
  var texto  = $(obj).parent().parent().data('texto')
   
  if(estado != 1)
  {
    alert("El Reclamo no se puede MODIFICAR por que Se encuentra con el Estado: "+texto)
    return
  }
  $("#Modificar").dialog({title: 'Modificar Reclamo'});
  $("#Modificar").dialog("open");
  $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
   $.ajax({
      url: 'mantenimiento.php',
      type: 'POST',
      async: true,
      data: 'Id=' + Id + '&codsuc=<?= $codsuc ?>&Op=1',
      success: function(data) {
          $("#DivModificar").html(data);
      }
  })    
}

function VerDetalle(obj)
{
  $("#form1").remove();
  var estado = $(obj).parent().parent().data('estado')
  var Id = $(obj).parent().parent().data('id')
  var texto = $(obj).parent().parent().data('texto')
  AbrirPopupImpresion('<?=$urldir?>sigco/reclamos/operaciones/ingresos/popup/popup_detalle_reclamos.php?codsuc=<?= $codsuc ?>&nroreclamo='+Id,1000,600)
}

function ImprimirR(obj)
{
  $("#form1").remove();
  var estado = $(obj).parent().parent().data('estado')
  var nrorec = $(obj).parent().parent().data('id')
  var nroinsc = $(obj).parent().parent().data('nroinscripcion')
  nroreclamo=nrorec
  nroinscripcion=nroinsc
  $( "#dialog-form-imprimir" ).dialog( "open" );
}

function Derivar(obj)
{
  var estado = $(obj).parent().parent().data('estado')
  var nrorec = $(obj).parent().parent().data('id')
  //var nroinsc = $(obj).parent().parent().data('nroinscripcion')
  var text = $(obj).parent().parent().data('texto')

  var par = '&nrodetalle=0&fecha_fin=<?=$fecha_fin?>'
  if(estado!=1)
  {
    alert("El Reclamo no se puede Derivar por que Se encuentra con el Estado: "+text)
    return
  }
  
  cargar_from_derivacion(nrorec,par)
}

function Eliminar(obj)
{

  var estado = $(obj).parent().parent().data('estado')
  var nrorec = $(obj).parent().parent().data('id')
  var text   = $(obj).parent().parent().data('texto')

  if (parseInt(estado) == 10 || parseInt(estado) == 6)
  {
    alert("El reclamo puede estar eliminado ó esta en un diferente estado que el recepcionado");return false;
  }  

  r = confirm("El reclamo pasara a ser eliminado, por favor repito sera eliminado el reclamo: "+text)
    if (r == false)
    {
        return  true
    }  
    cargar_from_eliminacion(nrorec)
}

</script>
<script src="js_liberacion.js" type="text/javascript"></script>
<div id="dialog-form-derivar" title="Derivar Reclamo Ingresado"  >
    <div id="div_derivar_reclamos"></div>
</div>
<?php
if($Op<5) CuerpoInferior();
?>