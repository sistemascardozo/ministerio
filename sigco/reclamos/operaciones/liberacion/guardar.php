<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	ini_set("display_errors",1);
	error_reporting(E_ALL);

    include("../../../../objetos/clsFunciones.php");

    $conexion->beginTransaction();

    $Op             = $_GET["Op"];
    $codsuc         = $_SESSION['IdSucursal'];
    $nroinscripcion = $_POST["nroinscripcion"];
    $correlativo    = $_POST["correlativoreclamo"];
    $nroreclamo     = $_POST["nroreclamo"];
    $motivo         = $_POST["motivoliberacion"];
    $fin_reclamo    = $_POST["fin_reclamo"];
    $fundado        = $_POST["fundado"];

    $objFunciones = new clsFunciones();
    // if($fin_reclamo==0 AND $fundado==0)//Y EL USUARIO ACEPTO O FINALIZA EL RECLAMO
    // {
            //SE PROCEDE A LIBERAR EL PEDIDO
            $sqlM = "SELECT codciclo,nrofacturacion from reclamos.mesesreclamados
                where nroreclamo=:nroreclamo and codsuc=:codsuc
                GROUP BY codciclo,nrofacturacion";
    
            $consultaM = $conexion->prepare($sqlM);
            $consultaM->execute(array(":nroreclamo"=>$nroreclamo,":codsuc"=>$codsuc));
            if ($consultaM->errorCode() != '00000') 
            {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                die(2);
            }
            $itemsM = $consultaM->fetchAll();
            
            foreach($itemsM as $rowM)
            {
                
               //PONER EN SIN RECLAMO LA FACTURACION
                $Sql="UPDATE facturacion.cabfacturacion  SET enreclamo = 0
                        WHERE codemp = :codemp AND  codsuc = :codsuc AND
                              codciclo = :codciclo AND nrofacturacion = :nrofacturacion
                              AND nroinscripcion = :nroinscripcion";
                $result = $conexion->prepare($Sql);
                $result->execute(array(
                    ":codemp" => 1,
                    ":codsuc" => $codsuc,
                    ":codciclo" => $rowM['codciclo'],
                    ":nrofacturacion" => $rowM['nrofacturacion'],
                    ":nroinscripcion" => $nroinscripcion

                    ));
                if ($result->errorCode() != '00000') 
                {
                    $conexion->rollBack();
                    $mensaje = "Error reclamos";
                    die(2);
                }
             
            }
            //SE PROCEDE A LIBERAR EL PEDIDO
    // }

    $sqlM = "   UPDATE reclamos.reclamos 
                SET liberado = '1', 
                motivoliberacion = :motivoliberacion
                WHERE correlativo=:nroreclamo and codsuc=:codsuc";

    
    $result = $conexion->prepare($sqlM);
    $result->execute(array(":nroreclamo"=>$correlativo,":codsuc"=>$codsuc,":motivoliberacion"=>$motivo));

    //PONER EN RECLAMO LA FACTURACION


    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
