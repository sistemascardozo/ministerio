<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	unset($_SESSION["oreclamos"]);
		
	include("../../../../objetos/clsDrop.php");
	include("../../../reclamos/operaciones/ingresos/clase/clsproceso.php");	
	$Op 				= $_GET["Op"];
	$Id 				= isset($_GET["Id"])?$_GET["Id"]:'';
	$codsuc 			= $_SESSION['IdSucursal'];
	$guardar			= "op=".$Op;
	
	$fecha =date("d/m/Y");
	
	$objMantenimiento 	= new clsDrop();

	$paramae 		= $objMantenimiento->getParamae("IMPIGV",$codsuc);
	
	$fechaserver = $objMantenimiento->FechaServer();

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script src="js_liberacion.js" type="text/javascript"></script>
<style>
	.myTable{
		background-color:#0099CC; border: 1 #000000 solid
	}
</style>
<script>
	var cont 		= 0; 
	var nDest 		= 0;
	var nDestC 		= 0;
	var object 		= "";
	var fechaserver = "<?=$fechaserver?>";
	var codsuc		= <?=$codsuc?>;
	var urldir		= "<?php echo $_SESSION['urldir'];?>"
	$(function(){
		if('<?=$Op?>'==0)
		cSerieNun()	

		$("#DivRebajar").dialog({
            autoOpen: false,
            height: 450,
            width: 650,
            modal: true,
            resizable: false,
            buttons: {
                "Agregar": function () {
                    guardar();
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            }
        });


	});
	function cSerieNun()
	{
		var TipoDoc = $("#documento").val()
		
			$.ajax({
			 url:urldir+'sigco/cobranza/operaciones/cancelacion/include/cSerieNun.php',
			 type:'POST',
			 async:true,
			 data:'codsuc='+codsuc+'&TipoDoc='+TipoDoc,
			 success:function(datos){
			 	var r=datos.split("|")
			 	$("#seriedoc").val(r[0])
			 	$("#numerodoc").val(r[1])
			 }
	    	}) 	
		
	}
	function ValidarForm(Op)
	{

		if( $("#correlativoreclamo").val() =="" || $("#correlativoreclamo").val() == 0 )
		{
				Msj($("#correlativoreclamo"),"Buscar el reclamo a liberar")
				return false;
		}

		if( $("#motivoliberacion").val() =="" || $("#motivoliberacion").val() == 0 )
		{
				Msj($("#motivoliberacion"),"Ingrese el motivo de liberacion del reclamo")
				return false;
		}
		
		GuardarP(Op)
	}
	function Cancelar()
	{
		location.href='index.php?Op=1';
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?" enctype="multipart/form-data">
  <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    <tbody>
	<tr class="ConReclamo">
	  <td >&nbsp;</td>
	  <td align="center">&nbsp;</td>
	  <td colspan="4" >&nbsp;</td>
	  </tr>
	<tr class="ConReclamo">
	  <td  class="TitDetalle" width="90" >Nro Reclamo </td>
	  <td width="30" align="center">:</td>
	  <td colspan="4" >
	  	<input type="hidden" name="NroItems" id="NroItems" value="0" />
	  	<input type="hidden" name="fin_reclamo" id="fin_reclamo" />
	    <input type="hidden" name="fundado" id="fundado" />
	    <input type="hidden" id="codciclo" value="<?=$row['codciclo'] ?>">
      	<input type="hidden" name="nroreclamo" id="nroreclamo" size="15" maxlength="15" readonly="readonly" class="inputtext" value="" />
      	<input type="text" id="correlativoreclamo" name="correlativoreclamo" maxlength="15" readonly="readonly" class="inputtext" value="" style="width:70px;" />
	    
	    <span class="MljSoft-icon-buscar" title="Buscar Reclamos" onclick="Buscarreclamos();"></span>
	    <input type="text" name="reclamante" id="reclamante" class="inputtext" maxlength="60" readonly="readonly" style="width:400px;" /></td>
	</tr>
	<tr>
		<td  class="TitDetalle">Usuario</td>
		<td align="center">:</td>
		<td colspan="4" >
			<input type="hidden" name="nroinscripcion" readonly="readonly" id="nroinscripcion" size="15" maxlength="15" class="inputtext" value="" />
			<input type="text" name="codantiguo" readonly="readonly" id="codantiguo" maxlength="15" class="inputtext" value="" style="width:70px;" />
			<input type="text" name="propietario" id="propietario" class="inputtext" maxlength="60" readonly="readonly" style="width:400px;" />
		</td>
	  </tr>
	  <td valign="top" class="TitDetalle">Glosa</td>
	  <td align="center" valign="top">:</td>
	  <td colspan="4" >
	  	<textarea readonly="readonly" name="glosarebaja" id="glosarebaja" rows="2" class="inputtext" style="font-size:11px; width:450;"></textarea></td>
	  </tr>
	  <tr>
		  <td valign="top" class="TitDetalle">Motivo de la liberacion</td>
		  <td align="center" valign="top">:</td>
		  <td colspan="4" ><textarea name="motivoliberacion" id="motivoliberacion" rows="2" class="inputtext" style="font-size:11px; width:450px;"></textarea></td>
	  </tr>	
	<tr>
  		<td colspan="6" height="5px" ></td>
  	</tr>
    </tbody>
	
  </table>
 <div id="DivRebajar" title="Ver Detalle de Facturacion para Realizar la Rebaja"  >
    <div id="div_detalle_facturacion"></div>
</div>
</form>
</div>