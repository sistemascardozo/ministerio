<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
unset($_SESSION["oreclamos"]);

include("../../../../objetos/clsDrop.php");
include("../ingresos/clase/clsproceso.php");

$objDrop = new clsDrop();

//$Id 	= explode("|",$_GET["Id"]);
$codsuc = $_SESSION['IdSucursal'];
$usuarioconciliador= $_SESSION['id_user'];
$nrodetalle = $_POST['nrodetalle']; //$Id[0];
$nroreclamo = $_POST['nrorec']; //$Id[1];
$fechaconciliacion = date('d/m/Y');

$sql = "select c.propietario,r.tiporeclamo,cr.descripcion as recl,r.glosa,r.nroinscripcion,
    fechaconciliacion,horainicioc, horafinc, propuesta_eps, propuesta_reclamante, puntos_acuerdo, 
    puntos_desacuerdo,  obsc,contratacion_m,c.codciclo,r.fundado,r.correlativo
    from reclamos.reclamos as r
    inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
    inner join reclamos.conceptosreclamos as cr on(r.codconcepto=cr.codconcepto)
    where r.codsuc=".$codsuc." and r.nroreclamo=".$nroreclamo;

$consulta = $conexion->prepare($sql);
$consulta->execute(array());
$row = $consulta->fetch();
$guardar = "?nrodetalle=" . $nrodetalle;

$sqlmes = "SELECT * from reclamos.mesesreclamados where codemp=1 and codsuc=? and nroreclamo=?";

$consulta_mes = $conexion->prepare($sqlmes);
$consulta_mes->execute(array($codsuc, $nroreclamo));
$items_mes = $consulta_mes->fetchAll();

//unset($_SESSION["oreclamos"]);
foreach ($items_mes as $row_mes) {

    $_SESSION["oreclamos"]->agregarvaloresxx($row_mes["nrofacturacion"], $row_mes["codconcepto"], $row_mes["imporiginal"], $row_mes["imprebajado"], $row_mes["codtipodeuda"], $row_mes["categoria"], $row_mes["nrodocumento"], $row_mes["anio"], $row_mes["mes"], $row_mes["codciclo"], $row_mes["lecturaanterior"], $row_mes["lecturaultima"], $row_mes["consumo"], $row_mes["lecturareclamo"], $row_mes["consumoreclamo"], $row_mes['seriedocumento'], $row_mes['item']);
}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/required.js" language="JavaScript"></script>
<script>
    $(document).ready(function ()
    {
        $("#tabs").tabs();
        $.mask.definitions['H'] = '[012]';
        $.mask.definitions['N'] = '[012345]';
        $.mask.definitions['n'] = '[0123456789]';
        $("#horainicioc,#horafinc").mask("Hn:Nn");

        $("#DivNose2").dialog({
            autoOpen: false,
            height: 450,
            width: 750,
            modal: true,
            resizable: false,
            buttons: {
                "Aceptar": function () {
                    GuardarOK();
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            }
        });
        $("#DivRebajar").dialog({
            autoOpen: false,
            height: 450,
            width: 650,
            modal: true,
            resizable: false,
            buttons: {
                "Aceptar": function () {
                    guardarrebajasres();
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            }
        });

    });
    function ValidarForm(Op)
    {
        if ($('#textFundado').val() == 1)//FUNDADO
        {
            $('#finreclamo1').attr("checked", true);
            $(".buttonset").buttonset('refresh');
        }

        var bval = true;
        bval = bval && $("#fechaconciliacion").required('', 'Ingrese la Fecha de Conciliacion');
        //bval = bval && $("#horainicioc").required('','Ingrese la Hora de Inicio de la Conciliacion');
        //bval = bval && $("#horafinc").required('','Ingrese la Hora de Fin de la Conciliacion');
        bval = bval && $("#propuesta_eps").required('', 'Ingrese la Propuesta de EPS');
        bval = bval && $("#propuesta_reclamante").required('', 'Ingrese la Propuesta de Reclamante');
        bval = bval && $("#puntos_acuerdo").required('', 'Ingrese los Puntos de Acuerdo');
        bval = bval && $("#puntos_desacuerdo").required('', 'Ingrese los Puntos de Desacuerdo');
        bval = bval && $("#obsc").required('', 'Ingrese las Observaciones del Reclamante o de la EPS');
        if (bval)
        {
            if ($("#finreclamo").val() == 1)
            {
                r = confirm("Al Seleccionar la Opcion Fin del Reclamo ya no podra realizar ninguna Modificacion y" +
                        " Dara por Finalizado la Atencion del Reclamo. Desea Continuar?")
                if (r == false)
                {
                    return false
                }
            }

            if ($('#textFundado').val() == 1)
            {
                if ($('input[name="finreclamo"]:checked').val() == 1)
                {
                    GuardarOK()
                }
                else
                    GuardarP(Op)
            }
            else
                GuardarP(Op)
        }
        else
        {
            return false;
        }

    }
    function ActualizarFacturacion()
    {
        $("#DivNose").dialog('open');
    }
    var ItemUpd = 0
    function cargar_from_detalle_facturacion(nroinscripcion, nrofacturacion, categoria, codtipodeuda, idx)
    {
        ItemUpd = idx;
        $.ajax({
            url: '../ingresos/ajax/from_detalle_facturacion.php',
            type: 'POST',
            async: true,
            data: 'nroinscripcion=' + nroinscripcion + '&codsuc=' + codsuc + '&nrofacturacion=' + nrofacturacion +
                    '&categoria=' + categoria + '&codtipodeuda=' + codtipodeuda + '&index=' + idx,
            success: function (datos)
            {
                $("#div_detalle_facturacion").html(datos)

                $("#mrebajar").val($("#tbrebajas tbody tr#tr_" + ItemUpd + " label.ImpRebajado").text()).keyup()
                $("#consumocalcular").val($("#tbrebajas tbody tr#tr_" + ItemUpd + " label.consumoreclamo").text());
                CalcularImporteConsumo();
                $("#DivRebajar").dialog("open");
                $("#DivImporteRebaja").show();
            }
        })
    }
    function guardarrebajasres()
    {

        var consumocalcular = $("#consumocalcular").val()?$("#consumocalcular").val():0;
        consumocalcular = parseFloat(consumocalcular);
        if (eval(consumocalcular < 0))
        {
            alert('El Consumo Ingesado no es Valido')
            return false;
        }

        var vrebaja = $("#mrebajar").val();
       // alert(vrebaja) //recalculaar el importe al digitar nuevos montos del concepto
        vrebaja = parseFloat(vrebaja).toFixed(1);

        var imptotalh   =  parseFloat($("#imptotalh").val())
        vrebaja = imptotalh - parseFloat(vrebaja)

        if (eval(vrebaja < 0))
        {
            alert('El Monto Ingesado no es Valido')
            return false;
        }
        else
        {
            var imptH = Round($("#imptotalh").val(), 2)
            var imptD = Round($("#mrebajar").val(), 2)
            var index = $("#idx").val()

            if(vrebaja > imptH)
            {
                alert("El Monto a Rebajado no puede ser Mayor al Monto a Rabajar")
                return
            }

            var cad = ""
            var leca = $("#lecturaanterior" + index).val();
            var lecreb = parseFloat(leca) + parseFloat(consumocalcular)
            for (i = 1; i <= $("#cont_facturacion").val(); i++)
            {
                cad = cad + "nrofacturacion" + i + "=" + $("#nrofacturacion" + ItemUpd).val() + "&codconcepto" + i + "=" + $("#codconceptoy" + i).val();
                cad = cad + "&imporiginal" + i + "=" + str_replace($("#impx" + i).val(), ",", "") + "&imprebajado" + i + "=" + str_replace($("#impamort" + i).val(), ",", "");
                cad = cad + "&codtipodeuda" + i + "=" + $("#codtipodeuda" + ItemUpd).val() + "&categoria" + i + "=" + $("#ct" + ItemUpd).val();
                cad = cad + "&nrodocumento" + i + "=" + $("#nrodoc" + ItemUpd).val() + "&anio" + i + "=" + $("#anioz" + ItemUpd).val() + "&mes" + i + "=" + $("#mesz" + ItemUpd).val();
                cad = cad + "&codciclo" + i + "=" + $("#codciclo" + ItemUpd).val() + "&"
                cad = cad + "&lecant" + i + "=" + $("#lecturaanterior" + index).val() + "&"
                cad = cad + "&lecact" + i + "=" + $("#lecturaultima" + index).val() + "&"
                cad = cad + "&consumo" + i + "=" + $("#consumo" + index).val() + "&"
                cad = cad + "&lecturarec" + i + "=" + lecreb + "&"
                cad = cad + "&consumorec" + i + "=" + consumocalcular + "&"
                cad = cad+ "&codconceptoitem" + i + "=" + $("#codconceptoitem" + i).val()+ "&"
            }

            $.ajax({
                url: '../ingresos/clase/proceso.php',
                type: 'POST',
                async: true,
                data: cad + "count=" + $("#cont_facturacion").val(),
                success: function (datos) {
                    $("#tbrebajas tbody tr#tr_" + ItemUpd + " label.ImpRebajado").text(parseFloat(vrebaja).toFixed(1));
                    $("#tbrebajas tbody tr#tr_" + ItemUpd + " label.lecturareclamo").text(lecreb);
                    $("#tbrebajas tbody tr#tr_" + ItemUpd + " label.consumoreclamo").text(consumocalcular);
                    CalcularTotalRebajado()
                    $("#DivRebajar").dialog('close');
                }
            })
        }
    }
    function CalcularTotalRebajado()
    {
        var id = parseInt($("#tbrebajas tbody tr").length)
        var Total = 0
        var Importe = 0
        for (var i = 1; i <= id; i++)
        {
            Importe = $("#tbrebajas tbody tr#tr_" + i + " label.ImpRebajado").text()
            Importe = str_replace(Importe, ',', '');
            Total += parseFloat(Importe);
        }

        $("#LblTotalReb").html(parseFloat(Total).toFixed(2))
    }
    function GuardarOK()
    {
        Op = 0;
        var Importe = $("#LblTotalReb").html()
        Importe = str_replace(Importe, ',', '');
        Importe = parseFloat(Importe);
        if (Importe == 0)
        {
            $("#tabs").tabs({selected: 1});
            Msj($("#tbrebajas"), 'Ingrese importes a rebajar')
            return false
        }
        /*var id= parseInt($("#tbrebajas tbody tr").length)
         for(var i=1; i<=id; i++)
         {
         if( $("#tbrebajas tbody tr#tr_"+i+" label.ImpRebajado").text())
         }
         */
        $.ajax({
            url: 'guardar.php?Op=' + Op,
            type: 'POST',
            async: true,
            data: $('#form1').serialize(), // + '&0form1_idusuario=<?=$IdUsuario ?>&3form1_fechareg=<?=$Fecha ?>',
            success: function (data) {
                OperMensaje(data)
                $("#Mensajes").html(data);
                $("#DivNuevo,#DivModificar,#DivEliminar,#DivEliminacion,DivRestaurar").html('');
                $("#Modificar").dialog("close");
                $("#DivNose").dialog("close");
                Buscar(Op);
            }
        })

    }
</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="1060" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="5">
                        <div id="tabs">
                            <ul>
                                <li style="font-size:10px"><a href="#tabs-1">Datos de la Conciliaci&oacute;n</a></li>
                                <li style="font-size:10px"><a href="#tabs-2">Meses Reclamados</a></li>

                            </ul>
                            <div id="tabs-1" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="90" class="TitDetalle">Nro. Reclamo </td>
                                        <td width="25" align="center" class="TitDetalle">:</td>
                                        <td class="CampoDetalle">
                                            <input type="text" id="correlativo" size="10" maxlength="10" value="<? echo $row['correlativo']; ?>" class="inputtext"/>
                                            <input name="nroreclamo" type="hidden" id="Id" size="10" maxlength="10" value="<? echo $nroreclamo; ?>" class="inputtext"/>
                                        </td>
                                        <td colspan="2" class="CampoDetalle">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="TitDetalle">Propietario</td>
                                        <td align="center" class="TitDetalle">:</td>
                                        <td class="CampoDetalle">
                                            <input type="text" name="propietario" id="propietario" size="70" readonly="readonly" class="inputtext" value="<?php echo $row["propietario"] ?>" />
                                            <input type="hidden" name="nrodetalle" value="<?=$nrodetalle ?>">
                                            <input type="hidden" id="codciclo" value="<?=$row['codciclo'] ?>">
                                            <input type="hidden" name="nroinscripcion" id="nroinscripcion" value="<?=$row['nroinscripcion'] ?>">
                                            <input type="hidden" name="facturaciones" id="facturaciones" value="<?=$row['nroinscripcion'] ?>">
                                            <input type="hidden" name="usuarioconciliador" id="usuarioconciliador" value="<?=$usuarioconciliador ?>">
                                        </td>
                                        <td colspan="2" class="CampoDetalle">&nbsp;</td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="TitDetalle">Reclamo</td>
                                        <td align="center" class="TitDetalle">:</td>
                                        <td class="CampoDetalle">
                                            <input type="text" name="reclamoreclamo" id="reclamo" size="70" readonly="readonly" class="inputtext" value="<?php echo $row["recl"] ?>" />
                                        </td>
                                        <td colspan="2" class="CampoDetalle">&nbsp;</td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="TitDetalle">Motivo</td>
                                        <td align="center" class="TitDetalle">:</td>
                                        <td class="CampoDetalle">
                                            <textarea name="movito" id="movito" rows="1" readonly="readonly" style="width:750px;" class="inputtext"><?php echo $row["glosa"] ?></textarea>
                                        </td>
                                        <td colspan="2" class="CampoDetalle">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="TitDetalle" style="height:5px"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="TitDetalle ui-widget-header" style="padding:4px">Datos de la Conciliación del Reclamo</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="TitDetalle" style="height:5px"></td>
                                    </tr>
                                        <?php
                                        $sqlD = "select observacion,codinspector,fin_reclamo,nrodetalle_origen,fundado from reclamos.detalle_reclamos where codsuc=? and nrodetalle=?";

                                        $consultaD = $conexion->prepare($sqlD);
                                        $consultaD->execute(array($codsuc, $nrodetalle));
                                        $rowD = $consultaD->fetch();
                                        ?>
                                    <tr>
                                        <td colspan="2" class="TitDetalle">&nbsp;</td>
                                        <td colspan="3" align="right">
                                            Fecha Conciliaci&oacute;n&nbsp;:
                                            <input type="text" maxlentgth="6" class="inputtext fecha" id="fechaconciliacion" name="fechaconciliacion" value="<?=$fechaconciliacion?>" style="width:90px;" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="top" height="30">
                                        <td valign="top" class="TitDetalle">Hora Inicio - Fin</td>
                                        <td align="center" class="TitDetalle">:</td>
                                        <td class="CampoDetalle" colspan="3">
                                            <input type="text" name="horainicioc" id="horainicioc" value="<?php echo $row['horainicioc']; ?>" style="width:50px; text-align: center" maxlength="5" class="inputtext" /> - 
                                            <input type="text" name="horafinc" id="horafinc" value="<?php echo $row['horafinc']; ?>" style="width:50px; text-align: center" maxlength="5" class="inputtext" />    
                                            &nbsp;
                                            Resultado&nbsp;:&nbsp;
                                            <?php
                                            if ($rowD['fundado'] == 1) {
                                                $checked1 = "checked='checked'";
                                                $checked2 = "";
                                            } else {
                                                $checked2 = "checked='checked'";
                                                $checked1 = "";
                                            }
                                            ?>
                                            <div class="buttonset" style="display:inline">
                                                <input type="radio" name="radio" id="fundado" value="fundado" onclick="javascript:$('#textFundado').val(1)" <?=$checked1 ?>/>
                                                <label for="fundado">Fundado</label>
                                                <input type="radio" name="radio" id="infundado" value="infundado"  onclick="javascript:$('#textFundado').val(2)" <?=$checked2 ?>/>
                                                <label for="infundado">Infundado</label>
                                            </div>
                                            <input type="hidden" name="textFundado" id="textFundado" value="<?php echo isset($row["fundado"]) ? $row["fundado"] : 2 ?>" />
                                            <?php
                                            if ($rowD['fin_reclamo'] == 1) {
                                                $checked1 = "checked='checked'";
                                                $checked2 = "";
                                            } else {
                                                $checked2 = "checked='checked'";
                                                $checked1 = "";
                                            }
                                            ?>
                                            &nbsp;Fin del Reclamo &nbsp;: &nbsp;
                                            <div class="buttonset" style="display:inline">
                                                <input type="radio" name="finreclamo" id="finreclamo1" value="1"  <?=$checked1 ?>/>
                                                <label for="finreclamo1">SI</label>
                                                <input type="radio" name="finreclamo" id="finreclamo2" value="0"   <?=$checked2 ?>/>
                                                <label for="finreclamo2">NO</label>
                                            </div>      	  
                                        </td>
                                    </tr>
                                    <tr height="40">
                                        <td valign="top" class="TitDetalle">Propuesta de la EPS</td>
                                        <td align="center" valign="top" class="TitDetalle">:</td>
                                        <td colspan="3" valign="top" class="CampoDetalle">
                                            <textarea name="propuesta_eps" id="propuesta_eps" class="inputtext" style="font-size:12px; width:750px;" rows="2" ><?php echo $row["propuesta_eps"] ?></textarea>
                                        </td>
                                    </tr>
                                    <tr height="40">
                                        <td valign="top" class="TitDetalle">Propuesta del Reclamante</td>
                                        <td align="center" valign="top" class="TitDetalle">:</td>
                                        <td colspan="3" valign="top" class="CampoDetalle">
                                            <textarea name="propuesta_reclamante" id="propuesta_reclamante" class="inputtext" style="font-size:12px; width:750px;" cols="120" rows="2" ><?php echo $row["propuesta_reclamante"] ?></textarea>
                                        </td>
                                    </tr>
                                    <tr height="65" >
                                        <td valign="top" class="TitDetalle">Puntos de Acuerdo</td>
                                        <td align="center" valign="top" class="TitDetalle">:</td>
                                        <td colspan="3" valign="top" class="CampoDetalle">
                                            <div style="display:inline">
                                                <table border="0" cellpadding="0" cellspacing="0" width="97%">
                                                    <tr>
                                                        <td width="310" valign="top">
                                                            <textarea name="puntos_acuerdo" id="puntos_acuerdo" class="inputtext" style="font-size:12px; width:280px;" rows="4" ><?php echo $row["puntos_acuerdo"] ?></textarea>
                                                        </td>
                                                        <td valign="top">
                                                            Puntos de Desacuerdo&nbsp;:&nbsp;
                                                        </td>
                                                        <td width="300" valign="top">
                                                            <textarea name="puntos_desacuerdo" id="puntos_desacuerdo" class="inputtext" style="font-size:12px; width:280px;" cols="50" rows="4" ><?php echo $row["puntos_desacuerdo"] ?></textarea>
                                                        </td>
                                                    </tr>
                                                </table> </div>
                                        </td>
                                       
                                    </tr>
                                    <tr height="40">
                                        <td valign="top" class="TitDetalle">Conciliación</td>
                                        <td align="center" valign="top" class="TitDetalle">:</td>
                                        <td colspan="3" valign="top" class="CampoDetalle">
                                      <textarea name="obsc" id="obsc" class="inputtext" style="font-size:12px; width:750px;" cols="140" rows="2" ><?php echo $row["obsc"] ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="TitDetalle">Solicita Constratacion Medidor</td>
                                        <td align="center" valign="top" class="TitDetalle">:</td>
                                        <td colspan="3" valign="top">
                                            <div style="padding:5px 0">
                                                <span>
                                                    <?php
                                                    if ($row['contratacion_m'] == 1) {
                                                        $ck1 = "checked=''";
                                                        $ck2 = "";
                                                    } else {
                                                        $ck1 = "";
                                                        $ck2 = "checked";
                                                    }
                                                    ?>

                                                    <div class="buttonset" style="display:inline">
                                                        <input type="radio" name="contratacion_m" id="contratacion_m1" value="1" <?php echo $ck1; ?> />
                                                        <label for="contratacion_m1">Si</label>
                                                        <input type="radio" name="contratacion_m" id="contratacion_m2" value="0" <?php echo $ck2; ?> />
                                                        <label for="contratacion_m2">No</label>
                                                    </div>

                                                </span>
                                            </div>

                                            <!--                       -->
                                            <div>
                                                <?php
                                                $contador = 0;
                                                if ($nroreclamo != "") {
                                                    $sqlmeses = "SELECT meses.nrofacturacion,upper(doc.abreviado),meses.nrodocumento,
                                                        meses.anio,meses.mes,upper(tipd.descripcion),meses.categoria,meses.codtipodeuda,
                                                        sum(meses.imprebajado),sum(meses.imporiginal),meses.lecturaanterior,meses.lecturaultima,meses.consumo,
                                                        meses.lecturareclamo,meses.consumoreclamo,meses.codciclo
                                                         from reclamos.mesesreclamados as meses
                                                        inner join reglasnegocio.documentos as doc on(meses.coddocumento=doc.coddocumento) AND (meses.codsuc=doc.codsuc)
                                                        inner join public.tipodeuda as tipd on(meses.codtipodeuda=tipd.codtipodeuda)
                                                        where meses.nroreclamo=? and meses.codemp=1 and meses.codsuc=?
                                                        group by meses.codtipodeuda,meses.nrofacturacion,
                                                        doc.abreviado,meses.nrodocumento,meses.anio,meses.mes,meses.categoria,tipd.descripcion,meses.lecturaanterior,
                                                        meses.lecturaultima,meses.consumo,meses.lecturareclamo,meses.consumoreclamo,meses.codciclo
                                                        ORDER BY meses.nrofacturacion";

                                                    $consulta_meses = $conexion->prepare($sqlmeses);
                                                    $consulta_meses->execute(array($nroreclamo, $codsuc));
                                                    $items_meses = $consulta_meses->fetchAll();
                                                    foreach ($items_meses as $row_meses) {
                                                        $contador++;
                                                        ?>
                                                        <input type='hidden' name='nrodoc<?=$contador ?>' id='nrodoc<?=$contador ?>' value='<?php echo $row_meses['nrodocumento'] ?>' />
                                                        <input type='hidden' name='mesz<?=$contador ?>' id='mesz<?=$contador ?>' value='<?php echo $row_meses['mes'] ?>' />
                                                        <input type='hidden' name='codciclo<?=$contador ?>' id='codciclo<?=$contador ?>' value='<?php echo $row_meses['codciclo'] ?>' />
                                                        <input type='hidden' name='anioz<?=$contador ?>' id='anioz<?=$contador ?>' value='<?php echo $row_meses['anio'] ?>' />
                                                        <input type='hidden' name='nrofacturacion<?=$contador ?>' id='nrofacturacion<?=$contador ?>' value='<?php echo $row_meses[0] ?>' />
                                                        <input type='hidden' name='codtipodeuda<?=$contador ?>' id='codtipodeuda<?=$contador ?>' value='<?php echo $row_meses[7] ?>' />
                                                        <input type='hidden' name='ct<?=$contador ?>' id='ct<?=$contador ?>' value='<?php echo $row_meses[6] ?>' />
                                                        <input type='hidden' name='impz<?=$contador ?>' id='impz<?=$contador ?>' value='<?php echo $row_meses[8] ?>' />
                                                        <input type='hidden' name='lecturaanterior<?=$contador ?>' id='lecturaanterior<?=$contador ?>' value='<?php echo $row_meses['lecturaanterior'] ?>' />
                                                        <input type='hidden' name='lecturaultima<?=$contador ?>' id='lecturaultima<?=$contador ?>' value='<?php echo $row_meses['lecturaultima'] ?>' />
                                                        <input type='hidden' name='consumo<?=$contador ?>' id='consumo<?=$contador ?>' value='<?php echo $row_meses['consumo'] ?>' />
                                                        <input type='hidden' name='lecturareclamo<?=$contador ?>' id='lecturareclamo<?=$contador ?>' value='<?php echo $row_meses['lecturareclamo'] ?>' />
                                                        <input type='hidden' name='consumoreclamo<?=$contador ?>' id='consumoreclamo<?=$contador ?>' value='<?php echo $row_meses['consumoreclamo'] ?>' />

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div id="tabs-2" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div id="DivNose" title="Facturaciones a Realizar la Rebajas"  >
                                                <table border="1" align="center" cellspacing="0" class="ui-widget myTable"  width="100%" id="tbrebajas" rules="all" >
                                                    <thead class="ui-widget-header" >
                                                        <tr align="center">
                                                            <th width="11%" >Nro. Doc.</th>
                                                            <th width="12%" >A&ntilde;o y Mes</th>
                                                            <th width="11%" >Categoria</th>
                                                            <th width="9%" >Lect. Ant.</th>
                                                            <th width="9%" >Lect. Act.</th>
                                                            <th width="9%" >Consumo</th>
                                                            <th width="9%" >Lect. Rec.</th>
                                                            <th width="9%" >Con. Rec.</th>
                                                            <th width="12%" >Imp. Original</th>
                                                            <th width="20%" >Imp. Rebajado</th>
                                                            <th width="2%" >&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $contador = 0;
														
                                                        if ($nroreclamo != "")
														{
                                                            $sqlmeses = "SELECT meses.nrofacturacion, upper(doc.abreviado), meses.nrodocumento,
                                                                meses.anio, meses.mes, upper(tipd.descripcion), meses.categoria, meses.codtipodeuda,
                                                                sum(meses.imprebajado), sum(meses.imporiginal), meses.lecturaanterior, meses.lecturaultima, meses.consumo,
                                                                meses.lecturareclamo, meses.consumoreclamo, meses.seriedocumento
                                                                from reclamos.mesesreclamados as meses
                                                                inner join reglasnegocio.documentos as doc on(meses.coddocumento=doc.coddocumento) AND (meses.codsuc=doc.codsuc)
                                                                inner join public.tipodeuda as tipd on(meses.codtipodeuda=tipd.codtipodeuda)
                                                                where meses.nroreclamo=".$nroreclamo." and meses.codemp=1 and meses.codsuc=".$codsuc."
                                                                group by meses.codtipodeuda,meses.nrofacturacion,
                                                                doc.abreviado,meses.nrodocumento,meses.anio,meses.mes,meses.categoria,tipd.descripcion,
                                                                meses.lecturaanterior,meses.lecturaultima,meses.consumo,meses.lecturareclamo,
                                                                meses.consumoreclamo,meses.seriedocumento
                                                                ORDER BY meses.nrofacturacion";

                                                            $consulta_meses = $conexion->prepare($sqlmeses);
                                                            $consulta_meses->execute(array());
                                                            $items_meses = $consulta_meses->fetchAll();
                                                            //var_dump($consulta_meses->errorInfo());
                                                            $TotalFac = 0;
                                                            $TotalReb = 0;
                                                            foreach ($items_meses as $row_meses) {
                                                                $contador++;

                                                                $cat = "";
                                                                if ($row_meses[6] == 0) {
                                                                    $cat = "FACTURACION";
                                                                }
                                                                if ($row_meses[6] == 1) {
                                                                    $cat = "SALDO";
                                                                }
                                                                if ($row_meses[6] == 2) {
                                                                    $cat = "SALDO REFINANCIADO";
                                                                }
                                                                if ($row_meses[6] == 3) {
                                                                    $cat = "PRESTACION DE SERVICIOS EN CAJA";
                                                                }

                                                                $TotalFac+=$row_meses[9];
                                                                $TotalReb+=$row_meses[8];
                                                                ?>
                                                                <tr style='background-color:#FFFFFF' align="center" id='tr_<?=$contador ?>'>

                                                                    <td align="center" ><?php echo $row_meses[1] ?>:<?php echo $row_meses['seriedocumento'] ?>-<?php echo $row_meses[2] ?></td>
                                                                    <td align="center" ><?php echo $row_meses[3] . "-" . $meses[$row_meses[4]] ?></td>
                                                                    <td align="center" ><?php echo $cat ?></td>
                                                                    <td align="right" class='select'><?=number_format($row_meses['lecturaanterior']) ?></td>
                                                                    <td align="right" class='select'><?=number_format($row_meses['lecturaultima']) ?></td>
                                                                    <td align="right" class='select'><?=number_format($row_meses['consumo']) ?></td>
                                                                    <td align="right" class='select'><label class="lecturareclamo"><?=number_format($row_meses['lecturareclamo']) ?></label></td>
                                                                    <td align="right" class='select'><label class="consumoreclamo"><?=number_format($row_meses['consumoreclamo']) ?></label></td>
                                                                    <td align="right" ><?=number_format($row_meses[9], 2) ?></td>
                                                                    <td align="right" ><label class="ImpRebajado"><?=number_format($row_meses[8], 2) ?></label></td>
                                                                    <td align="center" >
                                                                        <span class='icono-icon-add' title="Realizar Rebaja de Facturacion" onclick="cargar_from_detalle_facturacion(<?=$row['nroinscripcion'] ?>, <?=$row_meses['nrofacturacion'] ?>, <?=$row_meses['categoria'] ?>, <?=$row_meses['codtipodeuda'] ?>, <?=$contador?>)" ></span>

                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <tfoot class="ui-widget-header">
                                                        <tr>
                                                            <td colspan="8">&nbsp;</td>
                                                            <td align="right"><label id="LblTotalFac"><?=number_format($TotalFac, 2) ?></label></td>
                                                            <td align="right"><label id="LblTotalReb"><?=number_format($TotalReb, 2) ?></label></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </td></tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>		  

            </tbody>

        </table>
    </form>
</div>
<div id="DivRebajar" title="Ver Detalle de Facturacion para Realizar la Rebaja"  >
    <div id="div_detalle_facturacion"></div>
</div>
</div>
<script type="text/javascript">$("#facturaciones").val(<?=$contador ?>)</script>