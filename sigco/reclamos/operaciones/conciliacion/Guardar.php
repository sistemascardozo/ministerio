<?php
    
    include("../ingresos/clase/clsproceso.php");
    include("../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();
    $conexion->beginTransaction();

    $codsuc               = $_SESSION['IdSucursal'];
    $nroreclamo           = $_POST["nroreclamo"];
    $nrodetalle           = $_POST["nrodetalle"];
    $fin_reclamo          = $_POST["finreclamo"];
    $nro_origen           = $_POST["nro_origen"];
    $nroinscripcion       = $_POST["nroinscripcion"];
    $textFundado          = $_POST["textFundado"];
    
    $fechaconciliacion    =$objFunciones->CodFecha($_POST["fechaconciliacion"]);
    $horainicioc          =$_POST["horainicioc"];
    $horafinc             =$_POST["horafinc"];
    $propuesta_eps        =$_POST["propuesta_eps"];
    $propuesta_reclamante =$_POST["propuesta_reclamante"];
    $puntos_acuerdo       =$_POST["puntos_acuerdo"];
    $puntos_desacuerdo    =$_POST["puntos_desacuerdo"];
    $obsc                 =$_POST["obsc"];
    $contratacion_m       =$_POST["contratacion_m"];
    $usuarioconciliador   =$_POST["usuarioconciliador"];

    $fecha_Fin = $objFunciones->FechaServer();

    $fecha = "";
    if ($fin_reclamo == 1) {
        $fecha = ",fecha_finalizacion='" . $fecha_Fin . "'";
    }

    $upd = "update reclamos.detalle_reclamos set observacion=?,fin_reclamo=?,fundado=? " . $fecha . ",codestadoreclamo=4 where codsuc=? and nrodetalle=?";
    $resultUpd = $conexion->prepare($upd);
    $resultUpd->execute(array($obsc,$fin_reclamo,$textFundado,  $codsuc, $nrodetalle));
    if ($resultUpd->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
    $updt = "update reclamos.detalle_reclamos set fin_reclamo=? where codsuc=? and nrodetalle=?";
    $resultUpdt = $conexion->prepare($updt);
    $resultUpdt->execute(array($fin_reclamo, $codsuc, $nro_origen));
    if ($resultUpdt->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
    

    $updR = "update reclamos.reclamos 
                set fin_reclamo=? " . $fecha . ",
                fechaconciliacion=?,
                horainicioc=?,
                horafinc=?,
                propuesta_eps=?,
                propuesta_reclamante=?,
                puntos_acuerdo=?,
                puntos_desacuerdo=?,
                obsc=?,
                contratacion_m=?,
                fundado=?,
                codestadoreclamo=4,
                usuarioconciliador=?
                 where codsuc=? and nroreclamo=?";
               
    $consultaR = $conexion->prepare($updR);
    $consultaR->execute(array($fin_reclamo,$fechaconciliacion,$horainicioc,$horafinc,$propuesta_eps,$propuesta_reclamante,
                        $puntos_acuerdo,$puntos_desacuerdo,$obsc,$contratacion_m,$textFundado,$usuarioconciliador,$codsuc, $nroreclamo));


		
    if($textFundado!=1)//SI NOOO ES FUNDADO
    {
        if($fin_reclamo==1)//Y EL USUARIO ACEPTO O FINALIZA EL RECLAMO
        {
            //SE PROCEDE A LIBERAR EL PEDIDO
             $sqlM = "SELECT codciclo,nrofacturacion from reclamos.mesesreclamados
                where nroreclamo=:nroreclamo and codsuc=:codsuc
                GROUP BY codciclo,nrofacturacion";
    
            $consultaM = $conexion->prepare($sqlM);
            $consultaM->execute(array(":nroreclamo"=>$nroreclamo,":codsuc"=>$codsuc));
            if ($consultaM->errorCode() != '00000') 
            {
                $conexion->rollBack();
                $mensaje = "Error reclamos";
                die(2);
            }
            $itemsM = $consultaM->fetchAll();
            
            foreach($itemsM as $rowM)
            {
                
               //PONER EN SIN RECLAMO LA FACTURACION
                $Sql="UPDATE facturacion.cabfacturacion  SET enreclamo = 0
                        WHERE codemp = :codemp AND  codsuc = :codsuc AND
                              codciclo = :codciclo AND nrofacturacion = :nrofacturacion
                              AND nroinscripcion = :nroinscripcion";
                $result = $conexion->prepare($Sql);
                $result->execute(array(
                    ":codemp" => 1,
                    ":codsuc" => $codsuc,
                    ":codciclo" => $rowM['codciclo'],
                    ":nrofacturacion" => $rowM['nrofacturacion'],
                    ":nroinscripcion" => $nroinscripcion

                    ));
                if ($result->errorCode() != '00000') 
                {
                    $conexion->rollBack();
                    $mensaje = "Error reclamos";
                    die(2);
                }

            }
            //SE PROCEDE A LIBERAR EL PEDIDO
        }
    }
    else//SI ES FUNDADO
        {

            //PONER EN RECLAMO LA FACTURACION
            $contador = $_POST["facturaciones"];
           $count = $_SESSION["oreclamos"]->item;
         
            for ($j = 1; $j <= $contador; $j++) 
            {
              
                if (isset($_POST["nrofacturacion" . $j])) 
                {
              
                   // echo $_POST["nrofacturacion".$j]."<br>";
                    for ($i = 0; $i < $count; $i++) 
                    {
                  
                        if (isset($_SESSION["oreclamos"]->nrofacturacion[$i])) 
                        {
                         
                            if ($_POST["nrofacturacion" . $j] == $_SESSION["oreclamos"]->nrofacturacion[$i] &&
                                    $_POST["codtipodeuda" . $j] == $_SESSION["oreclamos"]->codtipodeuda[$i] &&
                                    $_POST["ct" . $j] == $_SESSION["oreclamos"]->categoria[$i]) 
                            {
                                $inst = "UPDATE reclamos.mesesreclamados SET imprebajado=:imprebajado,
                                        lecturareclamo=:lecturareclamo,consumoreclamo=:consumoreclamo
                                        WHERE codemp=:codemp AND codsuc=:codsuc AND nroreclamo=:nroreclamo
                                        AND nrofacturacion=:nrofacturacion AND codconcepto=:codconcepto AND item=:item";
                        
                                $resultI = $conexion->prepare($inst);
                                $resultI->execute(array(
                                    ":imprebajado" => $_SESSION["oreclamos"]->imprebajado[$i],
                                    ":lecturareclamo"=>$_SESSION["oreclamos"]->lecturarec[$i],
                                    ":consumoreclamo"=>$_SESSION["oreclamos"]->consumorec[$i],
                                    ":codemp" => 1,
                                    ":codsuc" => $codsuc,
                                    ":nroreclamo" => $nroreclamo,
                                    ":nrofacturacion" => $_SESSION["oreclamos"]->nrofacturacion[$i],
                                    ":codconcepto" => $_SESSION["oreclamos"]->codconcepto[$i],
                                     ":item"=>$_SESSION["oreclamos"]->codconceptoitem[$i]
                                   ));
                                if ($resultI->errorCode() != '00000') 
                                {
                                    $conexion->rollBack();
                                    $mensaje = "Error reclamos";
                                    die(2);
                                }
                                           
                                //echo $inst."<br>";
                            }
                        }
                    }
                }
            }
         //PONER EN RECLAMO LA FACTURACION
        }
    if ($consultaR->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       echo $res = 2;
    } else 
    {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
