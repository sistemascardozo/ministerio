<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	
	
	$objFunciones = new clsFunciones();
		
	$codsuc			= $_SESSION['IdSucursal'];
	$nroreclamo 	= $_POST["nroreclamo"];
	$nrodetalle_org	= $_POST["nrodetalle"];
	$nroorigen		= $_POST["nroorigen"];
	$fecha 			= $objFunciones->CodFecha($objFunciones->FechaServer());
	$idusuario		= $_SESSION["id_user"];
	$codarea 		= $_SESSION['idarea'];
	$id   		= $objFunciones->setCorrelativos("detalle_reclamos","0","0");
	$nrodetalle	= $id[0];
	$conexion->beginTransaction();
	$updR = "update reclamos.reclamos set codestadoreclamo=4 where codsuc=? and nroreclamo=?";	
	$consultaR = $conexion->prepare($updR);
	$consultaR->execute(array($codsuc,$nroreclamo));
	if ($consultaR->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
	$updD = "update reclamos.detalle_reclamos set codestadoreclamo=5 where codsuc=? and nrodetalle=? ";
	$consultaD = $conexion->prepare($updD);
	$consultaD->execute(array($codsuc,$nroorigen));
	if ($consultaD->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
	$updDt = "update reclamos.detalle_reclamos set inicial=2 where codsuc=? and nrodetalle=? ";
	$consultaDtt = $conexion->prepare($updDt);
	$consultaDtt->execute(array($codsuc,$nrodetalle_org));
	if ($consultaDtt->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }
	
	
	$insD = "insert into reclamos.detalle_reclamos(codemp,codsuc,nroreclamo,codusu,codarea,codusu_derivacion,nrodetalle,fechareg,codestadoreclamo,inicial,nrodetalle_origen)
			 values(:codemp,:codsuc,:nroreclamo,:codusu,:codarea,:codusu_derivacion,:nrodetalle,:fechareg,:codestadoreclamo,:inicial,:nrodetalle_origen)";
			 
	$consultaDt = $conexion->prepare($insD);
	$consultaDt->execute(array(":codemp"=>1,
							   ":codsuc"=>$codsuc,
							   ":nroreclamo"=>$nroreclamo,
							   ":codusu"=>$idusuario,
							   ":codarea"=>$codarea,
							   ":codusu_derivacion"=>$idusuario,
							   ":nrodetalle"=>$nrodetalle,
							   ":fechareg"=>$fecha,
							   ":codestadoreclamo"=>5,
							   ":inicial"=>0,
							   ":nrodetalle_origen"=>$nrodetalle_org));
	

	if ($consultaDt->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
       echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
	
?>
