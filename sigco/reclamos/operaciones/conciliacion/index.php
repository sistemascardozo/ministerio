<?php
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  $TituloVentana = "CONCILIACION DE RECLAMOS";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;

  unset($_SESSION["oreclamos"]);  
  $Op     = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc   = $_SESSION['IdSucursal'];
  $codarea 	= $_SESSION['idarea'];
  $codusu		= $_SESSION['id_user'];
  $CUsuario='';
  if($_SESSION['id_user']!=1 AND $_SESSION['id_user']!=14)
  	$CUsuario='  and d.codarea='.$_SESSION['idarea'].' and d.codusu='.$codusu;
  $valor    = isset($_GET["Valor"])?strtoupper($_GET["Valor"]):"";  
  $fecha_fin  = date('Y-md-');
  $FormatoGrilla = array ();
  $Sql = "select r.correlativo,c.codantiguo,c.propietario,usu.login,e.descripcion as estado,cr.descripcion as rec,
		r.glosa,d.codestadoreclamo,d.nrodetalle_origen,r.fin_reclamo,d.nrodetalle,1,d.observacion,d.nroreclamo
		from reclamos.detalle_reclamos as d
		inner join reclamos.reclamos as r on(d.codemp=r.codemp and d.codsuc=r.codsuc and d.nroreclamo=r.nroreclamo)
		inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
		inner join reclamos.estadoreclamo as e on(d.codestadoreclamo=e.codestadoreclamo)
		inner join reclamos.conceptosreclamos as cr on(r.codconcepto=cr.codconcepto)
		INNER JOIN seguridad.usuarios AS usu ON (usu.codusu= d.codusu_derivacion)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL   
  $FormatoGrilla[1] = array('1'=>'d.nroreclamo', '2'=>'c.propietario','3'=>'e.descripcion',
                            '4'=>'cr.descripcion','5'=>'r.glosa','6'=>'c.codantiguo');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op; 
  $FormatoGrilla[3] = array('T1'=>'Nro. Reclamo', 'T2'=>'Nro. Inscripcion','T3'=>'Propietario','T4'=>'Derivado por','T5'=>'Estado'
                            ,'T6'=>'Reclamo');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'left');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60','W2'=>'50','W3'=>'300','W4'=>'50','W5'=>'50','W7'=>'80');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1200;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  and (d.codemp=1 and d.codsuc=".$codsuc." ".$CUsuario." and (d.codestadoreclamo=4 OR d.codestadoreclamo=9) and d.inicial=0 /*and d.fin_reclamo=0*/) order by d.nroreclamo desc ";                                   //Orden de la Consulta
 $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Conciliar Reclamo',       //Titulo del Botón
              'BtnF1'=>'onclick="Modificar(this);"',  //Eventos del Botón
              'BtnCI1'=>'12',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              /*'BtnId2'=>'BtnRestablecer', //y aparece este boton
              'BtnI2'=>'ok.png', 
              'Btn2'=>'Marcar para Agregar la Resolucion al Reclamo', 
              'BtnF2'=>'onclick="Resolucion(this)"', 
              'BtnCI2'=>'12', 
              'BtnCV2'=>'1',*/
              'BtnId2'=>'BtnRestablecer', //y aparece este boton
              'BtnI2'=>'imprimir.png', 
              'Btn2'=>'Imprimir Formato de Reclamos', 
              'BtnF2'=>'onclick="ImprimirR(this)"', 
              'BtnCI2'=>'12', 
              'BtnCV2'=>'1',
              'BtnId3'=>'BtnEliminar', 
              'BtnI3'=>'documento.png', 
              'Btn3'=>'Ver Detalle del Reclamo', 
              'BtnF3'=>'onclick="VerDetalle(this)"', 
              'BtnCI3'=>'12', //campo 3
              'BtnCV3'=>'1');
  $FormatoGrilla[10] = array(
  								array('Name' => 'nrorec', 'Col' =>14),
  								array('Name' => 'nrodetalle', 'Col' => 11), 
  								array('Name' => 'estado', 'Col' => 8),
  								array('Name' => 'texto', 'Col' => 5), 
  								array('Name' => 'nroinscripcion', 'Col' =>2),
  								array('Name' => 'fin', 'Col' => 10),
  								array('Name' => 'orig', 'Col' => 9),
  								array('Name' => 'observacion', 'Col' => 13)
  							); //DATOS ADICIONALES  
  $FormatoGrilla[11] = 6; //FILAS VISIBLES 
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],1100,650);
  Pie();
  
?>

<script src="../ingresos/js_reclamos.js" type="text/javascript"></script>
<script>
	var Id 		= ''
	var Id2		= ''
	var IdAnt	= ''
	var e11		= ""
	var e22		= ""
	var estadoA	= "";
	var urldir	= '<?=$urldir?>';
	var Pagina 	= <?=$pagina?>;
	var nPag 	= <?=$TAMANO_PAGINA?>;
	var codsuc	= <?=$codsuc?>

	function Modificar(obj)
	{
	  $("#form1").remove();
	  var estado = $(obj).parent().parent().data('estado')
	  var texto = $(obj).parent().parent().data('texto')
	  var fin = $(obj).parent().parent().data('fin')

	  var nrorec = $(obj).parent().parent().data('nrorec')
	  var nrodetalle = $(obj).parent().parent().data('nrodetalle')

	  
		/*if(fin==1)
		{
			alert("No se puede Modificar el Reclamo por que ya se lo Dio por Finalizado")
			return false
		}
		if(estado!=4)
		{
			alert("El Reclamo no se puede MODIFICAR por que Se encuentra con el Estado: "+texto)
			return
		}*/	
			

	  $("#Modificar").dialog({title: 'Conciliación de Reclamos Derivados'});
	  $("#Modificar").dialog("open");
	  $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
	   $.ajax({
	      url: 'mantenimiento.php',
	      type: 'POST',
	      async: true,
	      data: 'nrorec=' + nrorec + '&codsuc=<?= $codsuc ?>&Op=1&nrodetalle='+nrodetalle,
	      success: function(data) {
	          $("#DivModificar").html(data);
	      }
	  })    

	}
	function VerDetalle(obj)
	{
	   $("#form1").remove();
	  var Id = $(obj).parent().parent().data('nrorec')
	  AbrirPopupImpresion('<?=$urldir?>sigco/reclamos/operaciones/ingresos/popup/popup_detalle_reclamos.php?codsuc=<?= $codsuc ?>&nroreclamo='+Id,1000,600)
	}

	function Resolucion (obj)
	{
		//nroreclamo,nrodetalle,codestado,estado,fin
		var fin = $(obj).parent().parent().data('fin')
		var nroreclamo = $(obj).parent().parent().data('nrorec')
		var codestado = $(obj).parent().parent().data('estado')
		var estado = $(obj).parent().parent().data('texto')
		var nrodetalle = $(obj).parent().parent().data('nrodetalle')
		var orig = $(obj).parent().parent().data('orig')
		var observacion = $(obj).parent().parent().data('observacion')

		if(fin==1)
		{
			alert("No se puede Modificar el Reclamo por que ya se lo Dio por Finalizado")
			return false
		}
		if(codestado!=4)
		{
			alert("No se puede realizar Ningun Accion sobre el Reclamo por que se encuentra: "+estado)
			return
		}	
		if(observacion=='')
		{
			alert("Concilie el Reclamo, antes de Realizar la Resolucion")
			return
		}	
		if(confirm("Va a Proceder a Realizar la Resolucion del Reclamo. Una vez hecho eso no podra realizar ningun Modificacion. Desea Continuar?")==false)
		{
			return false
		}
		$.ajax({
	      url: 'resolucion.php',
	      type: 'POST',
	      async: true,
	      data: 'nroreclamo='+nroreclamo+'&nrodetalle='+nrodetalle+'&nroorigen='+orig,
	      success: function(data) {
	      	OperMensaje(data)
            $("#Mensajes").html(data);    
             Buscar(0)
	      }
	  })  

		//location.href='resolucion.php?nroreclamo='+nroreclamo+'&nrodetalle='+nrodetalle+'&nroorigen='+orig

	}
function ImprimirR(obj)
	{
	  $("#form1").remove();
	  var estado = $(obj).parent().parent().data('estado')
	  var nrorec = $(obj).parent().parent().data('nrorec')
	  var nroinsc = $(obj).parent().parent().data('nroinscripcion')
	  nroreclamo=nrorec
	  nroinscripcion=nroinsc
	  $( "#dialog-form-imprimir" ).dialog( "open" );

	}
	

</script>
<script>
$("#BtnNuevoB").remove();
</script>
<? include("../ingresos/ajax/from_op_impresion.php"); ?>
<div id="dialog-form-derivar" title="Derivar Reclamo Ingresado"  >
    <div id="div_derivar_reclamos"></div>
</div>
<?php 
	CuerpoInferior();
?>