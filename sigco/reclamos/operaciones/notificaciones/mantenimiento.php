<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../../../objetos/clsDrop.php");
	
    $objDrop = new clsDrop();

    $Id = isset($_POST["Id"])?$_POST["Id"]: '';
    $nroreclamo = isset($_POST["nroreclamo"])?$_POST["nroreclamo"]: '';
    $Op = $_POST["Op"];
    $entrega_nota= 0;
    $codsuc = $_SESSION['IdSucursal'];
    $fecha = date("d/m/Y");
    $empresa = $objDrop->datos_empresa($codsuc);
    $empresa = strtoupper($empresa["razonsocial"]);
    $fechacitacion='';
    if ($Id != '') {
        $sql="SELECT
            n.fechaemision,
            n.nroreclamo,
            r.correlativo,
            n.nronotificacion,
            c.propietario,
            r.reclamante,
            cr.descripcion AS reclamo,
            n.asunto,
            n.fechacitacion,
            n.hora,
            n.contenido,
            n.recibido,
            n.nroinscripcion,
            c.codantiguo
            FROM
            reclamos.notificaciones AS n
            INNER JOIN reclamos.reclamos AS r ON (r.codemp = n.codemp AND r.codsuc = n.codsuc AND r.nroreclamo = n.nroreclamo)
            INNER JOIN catastro.clientes AS c ON (r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
            INNER JOIN reclamos.conceptosreclamos as cr on(cr.codconcepto=r.codconcepto)
            WHERE 
            n.codsuc=? AND n.nronotificacion=? AND n.nroreclamo=?";
        
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc,$Id,$nroreclamo));
        $row = $consulta->fetch();

        $fec = new DateTime($row["fechaemision"]);
        $fecha = $fec->format("d/m/Y");

        $fech= new DateTime($row["fechacitacion"]);
        $fechacitacion= $fech->format("d/m/Y");
        $entrega_nota= $row["recibido"];

    }
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/required.js" language="JavaScript"></script>
<script>
    $(document).ready(function ()
    {
        $("#hora_citacion").mask("Hn:Nn");

        $("#fecha_citacion,#fechaemision").datepicker({
            dateFormat:'dd/mm/yy',
            changeMonth:true,
            changeYear:true
        });
    });
    function ValidarForm(Op)
    {
        var bval = true;
        if ($('input[name="conciliacion"]:checked').val() == 0)
        {
            bval = bval && $("#inspectores").required('', 'Seleccione el Inspector');
            bval = bval && $("#glosa_distribucion").required('', 'Digite la Observaciones de la Derivacion');
        }
        if (bval)
        {
            if ($("#finreclamo").val() == 1)
            {
                r = confirm("Al Seleccionar la Opcion Fin del Reclamo ya no podra realizar ninguna Modificacion y" +
                        " dara por Finalizado la Atencion del Reclamo. Desea Continuar?")
                if (!r)
                {
                    return false
                }
            }
            GuardarP(Op)
        }
        else
        {
            return false;
        }
    }

    function Buscarreclamos()
    {
        if ($("#conreclamo").val() == 0)
        {
            alert("No Puede Buscar Reclamos por que la opcion de reclamos esta desactivada")
            return
        }

        object = "reclamos";
        AbrirPopupBusqueda('../ingresos/?Op=6', 1220, 450);
    }
    function Recibir(id)
    {

        if (object == "reclamos")
        {
            cargar_datos_reclamos(id)
        }
    }
    function cargar_datos_reclamos(nroreclamo)
    {
        
        $.ajax({
            url: 'datos_reclamos.php',
            type: 'POST',
            async: true,
            data: 'nroreclamo=' + nroreclamo + "&codsuc=" + codsuc,
            dataType: 'json',
            success: function (datos) {
                var fecha= datos.fecha_citacion.split('/');
                var dia= fecha[0];
                var mes= meses(fecha[1]);
                var anio= fecha[2];

                var des= 'Reclamo por: ';
                var cont= '<?php echo $empresa; ?> , comunica a Ud. que, al no haberse presentado ';
                    cont+= 'a la audiencia de negociacion programada  para el dia '+dia+' de '+mes+' del '+anio+' a horas '+datos.hora_citacion
                var con1= ', para dar respuesta a su reclamo por: ';
                var con2= 'generado con la ficha N° ';
                var con3= '; se le notifica nuevamente, debiendo acercarse a la oficina de atencion al cliente en la fecha sigueinte: ';

                $("#nronotificacion").val(datos.nronotificacion)
                $("#nroreclamo").val(nroreclamo)
                $("#correlativoreclamo").val(datos.correlativo)
                $("#propietario").val(datos.propietario)
                $("#nroinscripcion").val(datos.nroinscripcion)
                $("#codantiguo").val(datos.codantiguo)

                $("#reclamante").val(datos.reclamante)                
                $("#asunto").val(des+' '+datos.reclamo)
                $("#contenido").val(cont+''+con1+datos.reclamo+','+con2+datos.correlativo+' '+con3)
                

            }
        })
    }

    function Recibido(Op)
    {
        //alert(Op);
        if(Op==1)
        {
            $("#ver").css('visibility', 'visible');
            $("#ver1").css('visibility', 'visible');
        }
        else
            {
                $("#ver").css('visibility', 'hidden');
                $("#ver1").css('visibility', 'hidden');
            }
    }

</script>
<div align="center">
    <form id="form1" name="form1" method="post" action="Guardar.php<?php echo $guardar; ?>" enctype="multipart/form-data">
        <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
            <tbody>
                <tr>
                    <td colspan="4" class="CampoDetalle">&nbsp;</td>
                </tr>
                <tr>
                    <td width="100" class="TitDetalle">Fecha Emisión</td>
                    <td width="30" align="center" >:</td>
                    <td colspan="2" class="CampoDetalle">
                        <input name="fechaemision" type="text" id="fechaemision" maxlength="10" class="inputtext fecha" value="<?=$fecha?>" style="width:80px;"/>
                    </td>
                </tr>
                <tr>
                    <td width="220" class="TitDetalle">Nro. Reclamo </td>
                    <td width="10" align="center">:</td>
                    <td colspan="2" class="CampoDetalle">
                        <input type="hidden" name="nroreclamo" id="nroreclamo" size="15" maxlength="15" readonly="readonly" class="inputtext" value="<?php echo $row["nroreclamo"] ?>" />
                        <input type="text" id="correlativoreclamo" name="correlativoreclamo" maxlength="15" readonly="readonly" class="inputtext" value="<?php echo $row["correlativo"] ?>" style="width:70px;" />

                        <span class="MljSoft-icon-buscar" title="Buscar Reclamos" onclick="Buscarreclamos();"></span>
                    </td>
                </tr>
                <tr>
                    <td width="220" class="TitDetalle">Nro. Notificacion </td>
                    <td width="10" align="center">:</td>
                    <td colspan="2" class="CampoDetalle">
                        <input type="text" name="nronotificacion" id="nronotificacion" value="<?php echo $row["nronotificacion"] ?>" maxlength="15" readonly="readonly" class="inputtext" style="width:70px;" />
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Propietario</td>
                    <td align="center">:</td>
                    <td colspan="2" class="CampoDetalle">
                        <input type="hidden" name="nroinscripcion" id="nroinscripcion" value="<?=$row['nroinscripcion'] ?>">
                        <input type="text" name="codantiguo" id="codantiguo" value="<?php echo $row["codantiguo"] ?>" maxlength="15" class="inputtext" placeholder="Nro Inscripcion" readonly="readonly" style="width:70px;" />
                        <input type="text" name="propietario" id="propietario" readonly="readonly" class="inputtext" value="<?php echo $row["propietario"] ?>" placeholder="Propietario" style="width:400px;" />
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Señor(a)</td>
                    <td align="center">:</td>
                    <td colspan="2" class="CampoDetalle">                        
                        <input type="text" name="reclamante" id="reclamante" readonly="readonly" class="inputtext" value="<?php echo $row["reclamante"] ?>" style="width:400px;" />
                    </td>
                </tr>
                
                <tr valign="top">
                    <td class="TitDetalle">Asunto</td>
                    <td align="center">:</td>
                    <td colspan="2" class="CampoDetalle">
                        <input type="text" name="asunto" id="asunto" class="inputtext" value="<?php echo $row["asunto"] ?>" style="width:400px;" />
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Fecha y Hora de Citaci&oacute;n a Reuni&oacute;n</td>
                    <td align="center">:</td>
                    <td>
                        <div style="padding:5px 0">
                            <span>
                                <input type="text" name="fecha_citacion" id="fecha_citacion" value="<?php if($fechacitacion=="") echo "00/00/0000"; else echo $fechacitacion; ?>" class="fecha" style="width:80px; text-align: center" maxlength="10" />
                                <input type="text" name="hora_citacion" id="hora_citacion" value="<?php echo $row['hora']; ?>" style="width:50px; text-align: center" maxlength="5" />
                            </span>                            
                        </div>
                    </td>
                </tr>            
                <tr class="sinconciliar">
                    <td colspan="4" class="TitDetalle" style="background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px">Descripcion del contenido</td>
                </tr>
                <tr class="sinconciliar">
                    <td colspan="4" class="TitDetalle" style="height:5px"></td>
                </tr>                
                <tr class="sinconciliar">
                    <td class="TitDetalle" valign="top">Contenido</td>
                    <td align="center" valign="top">:</td>
                    <td colspan="2" class="CampoDetalle">
                        <textarea name="contenido" id="contenido" class="inputtext" style="font-size:12px; width:450px;" rows="10" ><?php echo $row["contenido"] ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="TitDetalle">Recibido ?</td>
                    <td align="center">:</td>
                    <td colspan="2" class="CampoDetalle">
                        <?php 
                            if($entrega_nota==1) { $ck1 = "checked=''";$ck2=""; }
                                else {$ck1=""; $ck2="checked";}
                        ?>
                        <div class="buttonset" style="display:inline">
                            <input type="radio" name="entrega_nota" id="entrega_nota1" value="1" <?php echo $ck1; ?> onchange="Recibido(1)" />
                            <label for="entrega_nota1">Si</label>
                            <input type="radio" name="entrega_nota" id="entrega_nota2" value="0" <?php echo $ck2; ?> onchange="Recibido(0)"  />
                            <label for="entrega_nota2">No</label>
                        </div>
                    </td>
                </tr>
                <tr style="visibility:hidden;" id="ver">
                    <td class="TitDetalle">Fecha Recepción</td>
                    <td align="center">:</td>
                    <td colspan="2" class="CampoDetalle">
                        <input name="fecharecepcion" type="text" id="fecharecepcion"  maxlength="10" class="inputtext fecha" value="<?=$fecha?>" style="width:80px;"/>
                    </td>
                </tr>
                <tr style="visibility:hidden;" id="ver1">
                    <td valign="top" class="TitDetalle">Observacion</td>
                    <td align="center" valign="top">:</td>
                    <td colspan="2" class="CampoDetalle">
                    <textarea name="observacion" id="observacion" class="inputtext" style="font-size:12px;width:450px;" rows="5" ><?php echo $row["observacion"] ?></textarea>
                    </td>
                </tr>
                <tr style="visibility:hidden;">
                  <td class="TitDetalle">&nbsp;</td>
                  <td class="TitDetalle">&nbsp;</td>
                  <td colspan="2" class="CampoDetalle">&nbsp;</td>
                </tr>
            </tbody>

        </table>
    </form>
</div>