<?php
    include("../../../../objetos/clsReporte.php");

    class clsFormato2 extends clsReporte {

        function Header() {
            global $codsuc;
            //DATOS DEL FORMATO
            $this->SetY(15);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "NOTIFICACION";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(6);            
            $hc = 5;
            //$tit2 = strtoupper("Presentacion del Reclamo");
            $this->Cell(190, $hc, $tit2, 0, 1, 'C');
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "";
            $this->Cell(0, 5, utf8_decode($tit1), 0, 1, 'R');
            //DATOS DEL FORMATO
            $empresa = $this->datos_empresa($codsuc);
            $this->SetFont('Arial', '', 6);
            $this->SetTextColor(0, 0, 0);
            $tit1 = strtoupper($empresa["razonsocial"]);
            $tit2 = strtoupper($empresa["direccion"]);
            $tit3 = "SUCURSAL: ".strtoupper($empresa["descripcion"]);
            $x = 23;
            $y = 5;
            $h = 3;
            $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/logo_empresa.jpg", 6, 1, 17, 16);
            $this->SetXY($x, $y);
            $this->SetFont('Arial', '', 6);
            $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
            $this->SetX($x);
            $this->Cell(150, $h, utf8_decode($tit3), 0, 0, 'L');

            $this->SetFont('Arial', '', 7);
            $this->SetY(8);
            $FechaActual = date('d/m/Y');
            $Hora = date('h:i:s a');
            $this->Cell(0, 3, /* utf8_decode("Página    :   ".$this->PageNo().' de {nb}     ') */ '', 0, 1, 'R');
            $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
            $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
            $this->SetY(17);
            $this->SetLineWidth(.1);
            //$this->Cell(0, .1, "", 1, 1, 'C', true);
            $this->cabecera();
        }

        function cabecera() {
            $this->Ln(2);
        }

        function Contenido($codsuc, $nroreclamo, $Id) {
            global $conexion, $meses;

            $FechaActual = date('d/m/Y');
            $empresa = $this->datos_empresa($codsuc);
            $h = 4;
            $hc = 5;
            $s = 2;
            $x = 10;

            $Sql = "SELECT
            n.fechaemision, n.nroreclamo,
            r.correlativo, n.nronotificacion,
            c.propietario, r.reclamante,
            cr.descripcion AS reclamo,
            n.asunto, n.fechacitacion,
            n.hora, n.contenido,
            n.recibido, n.nroinscripcion,
            c.codantiguo,
            ".$this->getCodCatastral("c.").",
            r.direccion1, r.nrocalle1
            FROM
            reclamos.notificaciones AS n
            INNER JOIN reclamos.reclamos AS r ON (r.codemp = n.codemp AND r.codsuc = n.codsuc AND r.nroreclamo = n.nroreclamo)
            INNER JOIN catastro.clientes AS c ON (r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
            INNER JOIN reclamos.conceptosreclamos as cr on(cr.codconcepto=r.codconcepto) ";
            $Sql .= " WHERE n.codsuc=? AND n.nroreclamo=? AND n.nronotificacion=? ";

            $consulta = $conexion->prepare($Sql);
            //print_r($consulta);
            $consulta->execute(array($codsuc, $nroreclamo, $Id));
            $row = $consulta->fetch();

            $this->SetFont('Arial', '', 10);
            $codantiguo = $this->CodUsuario($codsuc, $row['nroinscripcion']);
            $Anio= date('Y');
            $Anio = substr($this->DecFecha($row['fechaemision']), 6, 4);
            //$Anio = substr($Anio2,2, 2);
            $this->Ln(8);
            $this->SetX($x);
            $this->Cell(30, $h, utf8_decode("N° RECLAMO"), 0, 0, 'L');            
            $this->Cell(20, $h, $Anio." - ".str_pad($row['correlativo'], 6, "0", STR_PAD_LEFT), 0, 1, 'R');
            
            $this->Ln(2);
            $this->Cell(30, $h, utf8_decode("N° INSCRIPCION"), 0, 0, 'L');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            $this->Cell(25, $h, $codantiguo, 0, 0, 'R');

            $this->Cell(65, $h,utf8_decode("CODIGO CATASTRAL"), 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, $row['codcatastro'], 0, 1, 'L');
            $this->Ln(5);
            
            $this->SetX($x);
            $this->SetFont('Arial', 'B', 10);          
            $this->Cell(20, $h, utf8_decode("Señor (a)"), 0, 0, 'L');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell(0, $h, strtoupper(utf8_decode($row['reclamante'])), 0, 1, 'L');            
            $this->Ln(1);
            
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(20, $h, utf8_decode("Dirección"), 0, 0, 'L');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell(0, $h, strtoupper(utf8_decode($row['direccion1']." ".$row['nrocalle1'])), 0, 1, 'L');            
            $this->Ln(1);

            $this->SetFont('Arial', 'B', 10);
            $this->Cell(20, $h, utf8_decode("Asunto"), 0, 0, 'L');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            $this->SetFont('Arial', '', 10);
            $this->Cell(0, $h, strtoupper(utf8_decode($row['asunto'])), 0, 1, 'L');            
            $this->Ln(1);
            $this->Cell(0, 1, "", 0, 1, 'C');

            $this->Ln(2);
            $this->SetX($x);            
            $this->MultiCell(190, $h, strtoupper(utf8_decode($row['contenido'])), '0', 'J');

            $this->Ln(1);
            switch ($Id) {
                case 1:
                    $des="PRIMERA NOTIFICACION";break;
                case 2:
                    $des="SEGUNDA NOTIFICACION";break;
                case 3:
                    $des="TERCERA NOTIFICACION";break;
                case 4:
                    $des="CUARTA NOTIFICACION";break;
                case 5:
                    $des="QUINTA NOTIFICACION";break;
            }
            $this->Ln(4);
            $this->Cell(75, $h, utf8_decode($des) , 0, 1, 'L');

            $this->Ln(5);
            
            $this->Cell(40, $hc, "FECHA DE AUDIENCIA", 0, 0, 'L');
            $this->Cell(4, $hc, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);            
            $this->Cell(25, $h, $this->DecFecha($row['fechacitacion']), 0, 1, 'L');
            $this->Ln(2);
            $this->SetFont('Arial', '', 10); 
            $this->Cell(40, $hc, "HORA", 0, 0, 'L');
            $this->Cell(4, $hc, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10); 
            $hora= $row['hora'];
            $hora= date_format(date_create($hora),'h:i a');
            $this->Cell(25, $h, $hora, 0, 1, 'L');            
            
            $this->Ln(10);
            $this->SetX($x+40);
            $this->SetFont('Arial', '', 9);
            $this->Cell(0, $h, strtoupper(utf8_decode('ATENTAMENTE, ')), 0, 1, 'L');           
            $this->Line($x+160, 118, 85, 118);

            $this->Ln(20);
            $this->Image($_SERVER['DOCUMENT_ROOT']."/siinco/images/tijera.jpeg", $this->GetX() - 5, $this->GetY() - 1, 5, 5);
            for ($i = 5; $i < 100; $i++) {
                $this->Cell(2, $h, ' - ', 0, 0, 'C');
            }
            
            /*---------------------------------------------------------------------------*/
            $this->Ln(15);
            $this->SetFont('Arial', 'B', 10);
            $tit1 = "NOTIFICACION";
            $this->Cell(190, 5, utf8_decode($tit1), 0, 1, 'C');
            $this->Ln(8);            

            $this->SetFont('Arial', '', 9);
            $this->Cell(30, $h, utf8_decode("N° INSCRIPCION"), 0, 0, 'L');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            $this->Cell(25, $h, $codantiguo, 0, 0, 'R');

            $this->Cell(65, $h,utf8_decode("CODIGO CATASTRAL"), 0, 0, 'R');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            $this->Cell(30, $h, $row['codcatastro'], 0, 1, 'L');
            $this->Ln(5);
            
            $this->SetX($x);
            //$this->SetFont('Arial', 'B', 10);          
            $this->Cell(20, $h, utf8_decode("Señor (a)"), 0, 0, 'L');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            //$this->SetFont('Arial', '', 10);
            $this->Cell(0, $h, strtoupper(utf8_decode($row['reclamante'])), 0, 1, 'L');            
            $this->Ln(1);
            
            //$this->SetFont('Arial', 'B', 10);
            $this->Cell(20, $h, utf8_decode("Dirección"), 0, 0, 'L');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            //$this->SetFont('Arial', '', 10);
            $this->Cell(0, $h, strtoupper(utf8_decode($row['direccion1']." ".$row['nrocalle1'])), 0, 1, 'L');            
            $this->Ln(1);

            //$this->SetFont('Arial', 'B', 10);
            $this->Cell(20, $h, utf8_decode("Asunto"), 0, 0, 'L');
            $this->Cell(3, $h, ":", 0, 0, 'C');
            $this->Cell(0, $h, strtoupper(utf8_decode($row['asunto'])), 0, 1, 'L');
            
            $d= date('d');
            $m= date('m');

            $Mes = $meses [$m];
            $this->Ln(4);
            $this->SetXY($x,190);
            //$this->Cell(30, 6, $d." de ".$Mes. " del ".$Anio, 0, 1, 'C');
            $this->Cell(40, $hc, "FECHA DE AUDIENCIA", 0, 0, 'L');
            $this->Cell(4, $hc, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10);            
            $this->Cell(25, $h, $this->DecFecha($row['fechacitacion']), 0, 1, 'L');
            $this->Ln(2);
            $this->SetFont('Arial', '', 10); 
            $this->Cell(40, $hc, "HORA", 0, 0, 'L');
            $this->Cell(4, $hc, ":", 0, 0, 'C');
            $this->SetFont('Arial', 'B', 10); 
            $hora= $row['hora'];
            $hora= date_format(date_create($hora),'h:i a');
            $this->Cell(25, $h, $hora, 0, 1, 'L'); 
            $this->SetX($x+150);

            $this->Line($x+170, 193, 125, 193);
            $this->SetXY($x+130,194);
            $this->Cell(30, $hc,utf8_decode( "Firma de Recepción"), 0, 0, 'C');
            
        }

    }

    $nroreclamo = $_GET["nroreclamo"];
    $codsuc = $_GET["codsuc"];
    $Id = $_GET["Id"];
    $objReporte = new clsFormato2();
    $objReporte->AliasNbPages();
    $objReporte->AddPage();
    $objReporte->Contenido($codsuc, $nroreclamo, $Id);
    $objReporte->Output();
?>