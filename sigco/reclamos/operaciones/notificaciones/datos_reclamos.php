<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");
    $objDrop = new clsDrop();
	
	$nroreclamo = $_POST["nroreclamo"];
	$codsuc 	= $_POST["codsuc"];
	$Sql="SELECT MAX(nronotificacion) FROM reclamos.notificaciones
			WHERE codemp=1 AND codsuc=:codsuc AND nroreclamo=:nroreclamo";
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array(":nroreclamo"=>$nroreclamo,":codsuc"=>$codsuc));
	$row = $consulta->fetch();
	if($row[0]=='') $nronotificacion=1;
	else $nronotificacion=$row[0]+1;		

	$sql = "SELECT r.nroreclamo,cr.descripcion as reclamo,r.nroinscripcion,c.propietario,
		c.codciclo,r.correlativo, c.codantiguo, r.reclamante, r.fecha_citacion, r.hora_citacion

	from reclamos.reclamos as r 
	inner join reclamos.conceptosreclamos as cr on(cr.codconcepto=r.codconcepto)
	inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
	where r.codemp=1 and r.nroreclamo=:nroreclamo and r.codsuc=:codsuc ";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":nroreclamo"=>$nroreclamo,":codsuc"=>$codsuc));
	$row = $consulta->fetch();

	//$t  =$row["reclamo"]."|".$row["nroinscripcion"]."|".$row["propietario"]."|".$row["observacion"]."|".$row["codciclo"];
	$fecha= $objDrop->DecFecha($row["fecha_citacion"]);

	$hora= $row['hora_citacion'];
    $hora= date_format(date_create($hora),'h:i a');
	$data['nronotificacion'] = $nronotificacion;
	$data['reclamo'] = $row["reclamo"];
	$data['nroinscripcion'] = $row["nroinscripcion"];
	$data['propietario'] = $row["propietario"];
	$data['observacion'] = $row["observacion"];
	$data['correlativo'] = $row["correlativo"];
	$data['codantiguo'] = $row["codantiguo"];
	$data['reclamante'] = $row["reclamante"];
	$data['fecha_citacion'] = $fecha;
	$data['hora_citacion'] = $hora;

    echo json_encode($data);

?>