<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();
    $conexion->beginTransaction();

    $Op = $_GET["Op"];
    $codemp= 1;
    $codsuc = $_SESSION['IdSucursal'];
    $nroreclamo = $_POST["nroreclamo"];
    $nronotificacion = $_POST["nronotificacion"];
    $nroinscripcion  = $_POST["nroinscripcion"];
    $asunto = strtoupper($_POST["asunto"]);
    $fechacitacion = $objFunciones->CodFecha($_POST["fecha_citacion"]);
    $hora = $_POST["hora_citacion"];
    $contenido = strtoupper($_POST["contenido"]);
    $fechareg = $objFunciones->FechaServer();
    $fechaemision= $objFunciones->CodFecha($_POST['fechaemision']);
    $idusuario        = $_SESSION["id_user"];
    $recibido   = $_POST['entrega_nota'];
    if($recibido==0)
    {
      $fecharecepcion= null;
      $observacion= '';
    }
    else
      {
        $fecharecepcion= $objFunciones->CodFecha($_POST['fecharecepcion']);;
        $observacion= $_POST['observacion'];
      }

    switch ($Op) 
    {
      case 0:
        $id         = $objFunciones->setCorrelativos("notificaciones",$codsuc,"0");
        $codgrupocostos = $id[0];
        $sql = "INSERT INTO reclamos.notificaciones(codemp, codsuc, nroreclamo, nronotificacion, fechaemision, fechareg, 
            fechacitacion, hora, codusu, asunto, contenido, recibido, fecharecepcion, observacion, nroinscripcion)
          VALUES (:codemp, :codsuc, :nroreclamo, :nronotificacion, :fechaemision, :fechareg, :fechacitacion, :hora, 
          :codusu, :asunto, :contenido, :recibido, :fecharecepcion, :observacion, :nroinscripcion)";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":nroreclamo"=>$nroreclamo,
          ":nronotificacion"=>$nronotificacion,":fechaemision"=>$fechaemision, ":codusu"=>$idusuario,":asunto"=>$asunto,
          ":fechareg"=>$fechareg,":fechacitacion"=>$fechacitacion,":hora"=>$hora, ":contenido"=>$contenido, 
          ":recibido"=> $recibido,     
          ':fecharecepcion'=> $fecharecepcion, ':observacion'=> $observacion, ':nroinscripcion'=> $nroinscripcion
        ));
      break;
      case 1:
        $sql = "UPDATE reclamos.notificaciones
          SET fechaemision= :fechaemision, fechacitacion= :fechacitacion, hora= :hora, 
          codusu= :codusu, asunto= :asunto, contenido= :contenido, recibido= :recibido,
          fecharecepcion=:fecharecepcion, observacion=:observacion
          WHERE codemp= :codemp AND codsuc= :codsuc AND nroreclamo= :nroreclamo AND 
          nronotificacion= :nronotificacion AND nroinscripcion= :nroinscripcion";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, 
          ":nroreclamo"=>$nroreclamo,":nronotificacion"=>$nronotificacion,":fechaemision"=>$fechaemision,
          ":fechacitacion"=>$fechacitacion,":hora"=>$hora,
          ":codusu"=>$idusuario,":asunto"=>$asunto,":contenido"=>$contenido,":recibido"=> $recibido,
          ':fecharecepcion'=> $fecharecepcion, ':observacion'=> $observacion, ':nroinscripcion'=> $nroinscripcion
        ));

      break;
      case 2:case 3:
        $sql = "update public.grupocostos set estareg=:estareg
          where codgrupocostos=:codgrupocostos";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codgrupocostos"=>$_POST["1form1_id"],":estareg"=>$estareg));
      break;
    }

    if($result->errorCode()!='00000')
    {
      $conexion->rollBack();
      $mensaje = "Error al Grabar Registro";
      echo $res=2;
    }else{
      $conexion->commit();
      $mensaje = "El Registro se ha Grabado Correctamente";
      echo $res=1;
    }
    
?>
