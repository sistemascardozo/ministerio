<?php
    include("../../../../include/main.php");
    include("../../../../include/claseindex.php");
	
    $TituloVentana = "NOTIFICACION DE RECLAMOS";
    $Activo = 1;
    
    CuerpoSuperior($TituloVentana);
    $Op = isset($_GET['Op']) ? $_GET['Op'] : 0;

    
    $codsuc = $_SESSION['IdSucursal'];
   
    $codusu = $_SESSION['id_user'];
    $valor = isset($_GET["Valor"]) ? strtoupper($_GET["Valor"]) : "";
    $fecha_fin = date('Y-m-d');
    $FormatoGrilla = array();
    if($codusu!=1) $cond=" AND n.codusu=".$codusu;
    $Sql = "SELECT        
        r.correlativo,
        n.nronotificacion,
        r.reclamante,
        n.asunto,
        ".$objFunciones->FormatFecha('n.fechaemision').",
        n.recibido,
        n.nroreclamo,1
        FROM
        reclamos.notificaciones AS n
        INNER JOIN reclamos.reclamos AS r ON (r.codemp = n.codemp AND r.codsuc = n.codsuc AND r.nroreclamo = n.nroreclamo)
        INNER JOIN catastro.clientes AS c ON (r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)";

    $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql); //Sentencia SQL   
    $FormatoGrilla[1] = array('1' => 'r.correlativo', '2' => 'r.reclamante', '3' => 'n.asunto' );          //Campos por los cuales se har� la b�squeda
    $FormatoGrilla[2] = $Op;
    $FormatoGrilla[3] = array('T1' => 'Nro. Reclamo', 'T2' => 'Nro. Notificacion', 'T3' => 'Reclamante', 'T4' => 'Asunto', 'T5' => 'Fecha Em.' );   //T�tulos de la Cabecera
    $FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'center', 'A3' => 'left', 'A5' => 'center',);                        //Alineaci�n por Columna
    $FormatoGrilla[5] = array('W1' => '80', 'W2' => '100', 'W3' => '180', 'W4' => '100', 'W5' => '80', 'W6' => '50');                                 //Ancho de las Columnas
    $FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                   //Registro por P�ginas
    $FormatoGrilla[7] = 1000;                                                            //Ancho de la Tabla
    $FormatoGrilla[8] = " and (n.codemp=1 and n.codsuc=".$codsuc.$cond."  ) ORDER BY n.nronotificacion desc ";                                   //Orden de la Consulta
    $FormatoGrilla[9] = array('Id' => '1', //Botones de Acciones
        'NB' => '2', //N�mero de Botones a agregar
        'BtnId1' => 'BtnModificar', //Nombre del Boton
        'BtnI1' => 'modificar.png', //Imagen a mostrar
        'Btn1' => 'Recepcionar Reclamo', //Titulo del Bot�n
        'BtnF1' => 'onclick="Modificar(this);"', //Eventos del Bot�n
        'BtnCI1' => '6', //Item a Comparar
        'BtnCV1' => '0', //Valor de comparaci�n
        
        'BtnId2' => 'BtnRestablecer', //y aparece este boton
        'BtnI2' => 'imprimir.png',
        'Btn2' => 'Imprimir Formato de Notificacion',
        'BtnF2' => 'onclick="ImprimirR(this)"',
        'BtnCI2' => '8',
        'BtnCV2' => '1' ); //igua a 1);
    $FormatoGrilla[10] = array(
        array('Name' => 'nronotificacion', 'Col' => 2),
        array('Name' => 'nroreclamo', 'Col' => 7),
        array('Name' => 'estado', 'Col' => 6),
        /*array('Name' => 'texto', 'Col' => 5),
        array('Name' => 'nroinscripcion', 'Col' => 2),
        array('Name' => 'fin', 'Col' => 11),
        array('Name' => 'observacion', 'Col' => 14),
        array('Name' => 'inicial', 'Col' => 15),
        array('Name' => 'conciliar', 'Col' => 16)*/
    ); //DATOS ADICIONALES 

    $FormatoGrilla[11] = 5; //FILAS VISIBLES                 
    $_SESSION['Formato'] = $FormatoGrilla;
    Cabecera('', $FormatoGrilla[7], 850, 550);
    Pie();

?>

<script src="../ingresos/js_reclamos.js" type="text/javascript"></script>
<script>
    var Id = ''
    var Id2 = ''
    var IdAnt = ''
    var e11 = ""
    var e22 = ""
    var estadoA = "";
    var urldir = '<?=$urldir ?>';
    var Pagina = <?=$pagina ?>;
    var nPag = <?=$TAMANO_PAGINA ?>;
    var codsuc = <?=$codsuc ?>

    var nroreclamo = ''
    function Modificar(obj)
    {
        $("#form1").remove();
        var estado = $(obj).parent().parent().data('estado')
        var nronotificacion = $(obj).parent().parent().data('nronotificacion')
        var nroreclamo = $(obj).parent().parent().data('nroreclamo')
        /*var inicial = $(obj).parent().parent().data('inicial')
        var nrorec = $(obj).parent().parent().data('nrorec')
        var nrodetalle = $(obj).parent().parent().data('nrodetalle')*/
        
        if (estado == 1)
        {
            alert("No se puede Modificar la notificacion por que ya se lo Dio por Finalizado")
            return false
        }        

        $("#Modificar").dialog({title: 'Modificar Reclamo Derivado'});
        $("#Modificar").dialog("open");
        $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
        $.ajax({
            url: 'mantenimiento.php',
            type: 'POST',
            async: true,
            data: 'Id='+ nronotificacion+'&Op=1&nroreclamo='+nroreclamo,
            success: function (data) {
                $("#DivModificar").html(data);
            }
        })

    }

    function ImprimirR(obj)
    {
        var estado = $(obj).parent().parent().data('estado')
        var Id = $(obj).parent().parent().data('nronotificacion')
        var nroreclamo = $(obj).parent().parent().data('nroreclamo')
        var url='';
        url='imprimir.php';
        AbrirPopupImpresion(url+'?Id='+Id + '&codsuc=<?=$codsuc?>&nroreclamo='+nroreclamo,800,500)
    }

</script>


<?php
CuerpoInferior();
?>