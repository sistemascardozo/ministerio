<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");	
	$objDrop = new clsDrop();
	//$Id 	= explode("|",$_GET["Id"]);
	$codsuc	= $_SESSION['IdSucursal'];	
	$nrodetalle = $_POST['nrodetalle'];//$Id[0];
	$nrosolicitud = $_POST['nrosol'];//$Id[1];
	
	$sql = "select c.propietario,s.tiporeclamo,cr.descripcion as recl,s.glosa,s.nroinscripcion,s.creador
			from reclamos.solicitud as s
			inner join catastro.clientes as c on(s.codemp=c.codemp and s.codsuc=c.codsuc and s.nroinscripcion=c.nroinscripcion)
			inner join reclamos.conceptosreclamos as cr on(s.codconcepto=cr.codconcepto)
			where s.codsuc=? and s.correlativo=?";
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array($codsuc,$nrosolicitud));
	$row = $consulta->fetch();
	
	$guardar = "?nrodetalle=".$nrodetalle;
	
	$sqlD = "select observacion,codinspector,fin_solicitud,nrodetalle_origen,codusu,codusu_derivacion
	 	from reclamos.detalle_solicitudes 
	 	where codsuc=? and nrodetalle=?";
		
		$consultaD = $conexion->prepare($sqlD);
		$consultaD->execute(array($codsuc,$nrodetalle));
		$rowD = $consultaD->fetch();

		$ObsD='';
		$UserD='';
		if($rowD['nrodetalle_origen']!=0)
		{
			$sqlD = "select d.observacion,usud.login
	 			from reclamos.detalle_solicitudes d
	 			INNER JOIN seguridad.usuarios AS usud ON (usud.codusu= d.codusu)
	 			where d.codsuc=? and d.nrodetalle=?";
				$consultaD = $conexion->prepare($sqlD);
				$consultaD->execute(array($codsuc,$rowD['nrodetalle_origen']));
				$rowDe = $consultaD->fetch();
				$ObsD=$rowDe['observacion'];
				$UserD=$rowDe['login'];
		}
		

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/required.js" language="JavaScript"></script>
<script>	
$(document).ready(function()
{Conciliar();});
	function ValidarForm(Op)
	{
                var bval = true;
                if($('input[name="conciliacion"]:checked').val()==0)
                {
	                bval = bval && $("#inspectores").required('','Seleccione el Inspector');
	                bval = bval && $("#glosa_distribucion").required('','Digite la Observaciones de la Derivacion');		
	            }
		if(bval)
                {
                    if($("#finreclamo").val()==1)
                    {
                            r=confirm("Al Seleccionar la Opcion Fin del Reclamo ya no podra realizar ninguna Modificacion y"+
                                            " dara por Finalizado la Atencion del Reclamo. Desea Continuar?")
                            if(!r)
                            {
                                return false
                            }
                    }
                    GuardarP(Op)
                }
                else
                {
                    return false;
                }		
	}
	
function Conciliar()
{
	 if($('input[name="conciliacion"]:checked').val()==1) //si
	 {
	 	$('.sinconciliar').hide()
	 }
	 else
	 {
	 	$('.sinconciliar').show()
	 }
}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="guardar.php<?php echo $guardar;?>" enctype="multipart/form-data">
 <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td colspan="2" class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
       <td width="103" class="TitDetalle">Nro. Solicitud </td>
       <td width="25" align="center" class="TitDetalle">:</td>
       <td class="CampoDetalle">
			<input name="nrosolicitud" type="text" id="Id" size="10" maxlength="10" value="<? echo $nrosolicitud; ?>" class="inputtext"/></td>
        </tr>
	<tr>
	  <td class="TitDetalle">Propietario</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input type="text" name="propietario" id="propietario" readonly="readonly" class="inputtext" value="<?php echo $row["propietario"]?>" style="width:400px;" />
	    </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Tipo de Reclamo</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input type="hidden" name="nrodetalle" value="<?=$nrodetalle?>">
	    <input type="hidden" name="nroinscripcion" value="<?=$row['nroinscripcion']?>">
	    <input type="text" name="tiporeclamo" id="tiporeclamo" readonly="readonly" class="inputtext" value="<?php echo $tiporeclamo[$row["tiporeclamo"]]?>" style="width:400px;" />
	    </td>
	  </tr>
	<tr valign="top">
	  <td class="TitDetalle">Reclamo</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input type="text" name="reclamo" id="reclamo" readonly="readonly" class="inputtext" value="<?php echo  $row["recl"]?>" style="width:400px;" />
	    </td>
	  </tr>
	<tr valign="top">
	  <td class="TitDetalle">Motivo</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <textarea name="movito" id="movito" rows="2" readonly="readonly" style="width:400px;"><?php echo $row["glosa"]?></textarea>
	    </td>
	  </tr>
	  <?php
	  	if($UserD!='')
	  	{
	  	?>
	  	<tr valign="top">
		  <td class="TitDetalle"><?=$UserD?> Derivo</td>
		  <td align="center" class="TitDetalle">:</td>
		  <td class="CampoDetalle">
		    <textarea id="ObsD" rows="2" readonly="readonly" style="width:400px;"><?php echo $ObsD?></textarea>
		   </td>
		  </tr>
	  	<?php
	  	}
	   ?>
	<tr>
	  <td colspan="3" class="TitDetalle" style="height:5px"></td>
	  </tr>
	  <?php
	 	$display='display:none';
		if($row['creador']==$rowD['codusu'])
			{
				$display='';
				if($row['codestadoreclamo']==3) { $ck1 = "checked=''";$ck2=""; }
                else {$ck1=""; $ck2="checked";}
			}
			else
			{
				$ck1=""; $ck2="checked";
			}
		
		?>
		<tr valign="top" style="<?=$display?>">
		  <td class="TitDetalle">Pasar a Finalizacion de Solicitud</td>
		  <td align="center" class="TitDetalle">:</td>
		  <td class="CampoDetalle">
		 
            <div class="buttonset" style="display:inline">
	            <input type="radio" name="conciliacion" id="conciliacion_m1" value="1" <?php echo $ck1; ?> onchange="Conciliar()"/>
	            <label for="conciliacion_m1">Si</label>
            	<input type="radio" name="conciliacion" id="conciliacion_m2" value="0" <?php echo $ck2; ?> onchange="Conciliar()"/>
            	<label for="conciliacion_m2">No</label>
            </div>
		   </td>
		  </tr>
		<?php
		
	 ?>
	 <tr class="sinconciliar">
	   <td colspan="3" class="TitDetalle" style="background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px">Datos de la Derivaci&oacute;n</td>
	   </tr>
	 <tr class="sinconciliar">
	   <td colspan="3" class="TitDetalle" style="height:5px"></td>
     </tr>
     
	 <tr class="sinconciliar">
	   <td class="TitDetalle">Inspector</td>
	   <td class="TitDetalle" align="center">:</td>
	   <td class="CampoDetalle">
             <?php $objDrop->drop_inspector($codsuc,$rowD["codinspector"]); ?>
	     <input type="checkbox" style="display:none;" name="chkfinreclamo" id="chkfinreclamo" onclick="CambiarEstado(this,'finreclamo')" <?php echo $d="";$rowD["fin_reclamo"]==1?$d="checked='checked'":$d="";echo $d;?> />
	     <!-- Fin del Reclamo (Si/No) -->
	     <input type="hidden" name="finreclamo" id="finreclamo" value="<?php echo isset($rowD["fin_reclamo"])?$rowD["fin_reclamo"]:0?>" />
	    </td>
	   </tr>
	 <tr class="sinconciliar">
	   <td class="TitDetalle" valign="top">Observacion</td>
	   <td class="TitDetalle" align="center" valign="top">:</td>
	   <td class="CampoDetalle"><textarea name="glosa_distribucion" id="glosa_distribucion" class="inputtext" style="font-size:12px; width:400px;" rows="5" ><?php echo $rowD["observacion"]?></textarea></td>
	   </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>
	 
    </table>
 </form>
</div>