<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsFunciones.php");

    $objFunciones = new clsFunciones();
    $conexion->beginTransaction();

    $codsuc             = $_SESSION['IdSucursal'];
    $nrosolicitud       = $_POST["nrosolicitud"];
    $nrodetalle         = $_POST["nrodetalle"];
    $inspectores        = $_POST["inspectores"];
    $nroinscripcion     = $_POST["nroinscripcion"];
    $glosa_distribucion = strtoupper($_POST["glosa_distribucion"]);
    $fecha_Fin          = $objFunciones->FechaServer();

    // var_dump($_POST);exit;

    if ($_POST['conciliacion'] == 1 ) {
        $inicial            = 0;
        $codestadoreclamo   = 3;
        $fin_reclamo        = 1;


    } else {
        $inicial            = 1;
        $codestadoreclamo   = 1;
        $fin_reclamo        = $_POST["finreclamo"];
    }
    
    
    $fecha = "";
    if ($fin_reclamo == 1) {
        $fecha = ",fecha_finalizacion='" . $fecha_Fin . "'";
    }

    $upd = "update reclamos.detalle_solicitudes set observacion=?,codinspector=?,fin_solicitud=? ".$fecha.",
            codestadoreclamo=?,inicial=?
            where codsuc=? and nrodetalle=?";
    $resultUpd = $conexion->prepare($upd);
    $resultUpd->execute(array($glosa_distribucion, $inspectores, $fin_reclamo,$codestadoreclamo,$inicial, $codsuc, $nrodetalle));
    if ($resultUpd->errorCode() != '00000') 
    {
        $conexion->rollBack();
        $mensaje = "Error reclamos";
        die(2);
    }

     //SI EL RECLAMO SE FINALIZA, SE LIBERAM LAS FACTURACIONES INVOLUCRADAS
 
    $updR = "update reclamos.solicitud set fin_reclamo=? " . $fecha . " where codsuc=? and nrosolicitud=?";
    $consultaR = $conexion->prepare($updR);
    $consultaR->execute(array($fin_reclamo, $codsuc, $nrosolicitud));
    if ($consultaR->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        die(2);
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }

?>
