<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");
	$TituloVentana = "SOLICITUDES DE INSPECCION DERIVADAS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$Op = isset($_GET['Op'])?$_GET['Op']:0;

	unset($_SESSION["oreclamos"]);  
	$Op     = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc   = $_SESSION['IdSucursal'];
	$codarea 	= $_SESSION['idarea'];
	$codusu		= $_SESSION['id_user'];
	$valor    = isset($_GET["Valor"])?strtoupper($_GET["Valor"]):"";  
	$fecha_fin  = date('Y-m-d');
	$FormatoGrilla = array ();
  	$Sql = "select s.correlativo as nrosolicitud,s.nroinscripcion,c.propietario,usu.login,e.descripcion as estado,usud.login,cr.descripcion as rec,
		s.glosa,d.codestadoreclamo,d.nrodetalle_origen,s.fin_reclamo,1,
		d.nrodetalle,d.observacion,d.inicial
		,s.nrosolicitud as soli
		from reclamos.detalle_solicitudes as d
		inner join reclamos.solicitud as s on(d.codemp=s.codemp and d.codsuc=s.codsuc and d.nrosolicitud=s.nrosolicitud)
		inner join catastro.clientes as c on(s.codemp=c.codemp and s.codsuc=c.codsuc and s.nroinscripcion=c.nroinscripcion)
		inner join reclamos.estadoreclamo as e on(d.codestadoreclamo=e.codestadoreclamo)
		inner join reclamos.conceptosreclamos as cr on(s.codconcepto=cr.codconcepto)
		INNER JOIN seguridad.usuarios AS usu ON (usu.codusu= d.codusu_derivacion)
		INNER JOIN seguridad.usuarios AS usud ON (usud.codusu= d.codusu)  ";

  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL   
  $FormatoGrilla[1] = array('1'=>'s.correlativo', '2'=>'c.propietario','3'=>'e.descripcion',
                            '4'=>'cr.descripcion','5'=>'s.glosa');          //Campos por los cuales se har� la b�squeda
  $FormatoGrilla[2] = $Op; 
  $FormatoGrilla[3] = array('T1'=>'Nro. Solicitud', 'T2'=>'Nro. Inscripcion','T3'=>'Propietario','T4'=>'Derivado por','T5'=>'Estado',
                            'T6'=>'Derivado a','T7'=>'Solicitud');   //T�tulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'left');                        //Alineaci�n por Columna
  $FormatoGrilla[5] = array('W1'=>'60','W2'=>'50','W3'=>'250','W4'=>'50','W5'=>'120','W6'=>'50','W8'=>'100');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                   //Registro por P�ginas
  $FormatoGrilla[7] = 1200;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " and d.inicial <> 3 and (d.codemp=1 and d.codsuc=".$codsuc." and d.codarea=".$codarea." and d.codusu=".$codusu." /*and d.inicial=1*/) order by d.nrosolicitud desc ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //N�mero de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Recepcionar Solicitud',       //Titulo del Bot�n
              'BtnF1'=>'onclick="Modificar(this);"',  //Eventos del Bot�n
              'BtnCI1'=>'12',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparaci�n
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'restablecer.png', 
              'Btn2'=>'Realizar Derivacion de la Solicitud de Inspeccion', 
              'BtnF2'=>'onclick="Derivar(this)"', 
              'BtnCI2'=>'12', //campo 3
              'BtnCV2'=>'1',//igua a 1
              /*'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'ok.png', 
              'Btn3'=>'Marcar para Atencion', 
              'BtnF3'=>'onclick="Atender(this)"', 
              'BtnCI3'=>'11', 
              'BtnCV3'=>'1',*/
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'imprimir.png', 
              'Btn3'=>'Imprimir Formato de la Solicitud de Inspeccion', 
              'BtnF3'=>'onclick="ImprimirR(this)"', 
              'BtnCI3'=>'12', 
              'BtnCV3'=>'1',
              'BtnId4'=>'BtnEliminar', 
              'BtnI4'=>'documento.png', 
              'Btn4'=>'Ver Detalle de la Solicitud de Inspeccion', 
              'BtnF4'=>'onclick="VerDetalle(this)"', 
              'BtnCI4'=>'12', //campo 3
              'BtnCV4'=>'1');//igua a 1);
  $FormatoGrilla[10] = array(
  								array('Name' => 'nrosol', 'Col' =>1),
  								array('Name' => 'nrodetalle', 'Col' => 13), 
  								array('Name' => 'estado', 'Col' => 9),
  								array('Name' => 'texto', 'Col' => 5), 
  								array('Name' => 'nroinscripcion', 'Col' =>2),
  								array('Name' => 'fin', 'Col' => 11),
  								array('Name' => 'observacion', 'Col' => 14),
								array('Name' => 'inicial', 'Col' => 15),
  								array('Name' => 'solic', 'Col' => 16),	

  							); //DATOS ADICIONALES  
  $FormatoGrilla[11] = 7; //FILAS VISIBLES                 
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7],850,600);
  Pie();
  
?>

<script src="../solicitud/js_solicitud.js" type="text/javascript"></script>
<script>
	var Id 		= ''
	var Id2		= ''
	var IdAnt	= ''
	var e11		= ""
	var e22		= ""
	var estadoA	= "";
	var urldir	= '<?=$urldir?>';
	var Pagina 	= <?=$pagina?>;
	var nPag 	= <?=$TAMANO_PAGINA?>;
	var codsuc	= <?=$codsuc?>

    var nroreclamo=''
	function Modificar(obj)
	{
	  $("#form1").remove();
		var estado     = $(obj).parent().parent().data('estado')
		var texto      = $(obj).parent().parent().data('texto')
		var fin        = $(obj).parent().parent().data('fin')
		var inicial    = $(obj).parent().parent().data('inicial')
		var nrosol     = $(obj).parent().parent().data('nrosol')
		var nrodetalle = $(obj).parent().parent().data('nrodetalle')

		// alert(nrosol);

	  if(inicial==3)
		{
			alert("No se puede Modificar la Solicitud por que ya se lo Derivo")
			return false
		}
		if(fin==1)
		{
			alert("No se puede Modificarla Solicitud por que ya se lo Dio por Finalizado")
			return false
		}
		if(estado!=1 && estado!=9)
		{
			alert("La Solicitud no se puede modificar por que se encuentra con el Estado: "+texto)
			return
		}
			
			

	  $("#Modificar").dialog({title: 'Modificar Solicitud de Inspeccion Derivada'});
	  $("#Modificar").dialog("open");
	  $("#DivModificar").html("<center><span>class='icono-icon-loading'</span></center>");
	   $.ajax({
	      url: 'mantenimiento.php',
	      type: 'POST',
	      async: true,
	      data: 'nrosol=' + nrosol + '&codsuc=<?= $codsuc ?>&Op=1&nrodetalle='+nrodetalle,
	      success: function(data) {
	          $("#DivModificar").html(data);
	      }
	  })    

	}

	function VerDetalle(obj)
	{
	   $("#form1").remove();
	  var Id = $(obj).parent().parent().data('nrosol')
	  AbrirPopupImpresion('<?=$urldir?>sigco/reclamos/operaciones/solicitud/popup/popup_detalle_solicitudes.php?codsuc=<?= $codsuc ?>&nrosolicitud='+Id,1000,600)
	}
	function Derivar(obj)
	{


		var nrosol     = $(obj).parent().parent().data('solic')
		var nrodetalle = $(obj).parent().parent().data('nrodetalle')
		var text       = $(obj).parent().parent().data('texto')
		var fin        = $(obj).parent().parent().data('fin')
		var inicial    = $(obj).parent().parent().data('inicial')
		var par        = '&nrodetalle='+nrodetalle+'&fecha_fin=<?=$fecha_fin?>'

	  if(inicial==3)
		{
			alert("No se puede Derivar la Solicitud de Inspeccion por que ya se lo Derivo")
			return false
		}
	  if(fin==1)
		{
			alert("La Solicitud de Inspeccion no se puede Derivar por que se encuentra Finalizado")
			return
		}
		var observacion = $(obj).parent().parent().data('observacion')
		if(observacion=='')
		{
			alert("Complete los datos de Distribucion, antes de Derivar")
			return
		}	
		cargar_from_derivacion(nrosol,par)


	}
	function Atender (obj)
	{
		//nroreclamo,nrodetalle,codestado,estado,fin
		var fin = $(obj).parent().parent().data('fin')
		var nroreclamo = $(obj).parent().parent().data('nrorec')
		var codestado = $(obj).parent().parent().data('estado')
		var estado = $(obj).parent().parent().data('texto')
		var nrodetalle = $(obj).parent().parent().data('nrodetalle')
		var observacion = $(obj).parent().parent().data('observacion')
		if(fin==1)
		{
			alert("No se puede Atender el Reclamo por que se Encuentra Finalizado")
			return
		}
		if(codestado!=1)
		{
			alert("No se puede realizar Ningun Accion sobre el Reclamo por que se encuentra: "+estado)
			return
		}		
		if(observacion=='')
		{
			alert("Complete los datos de Distribucion, antes de pasar a Atencion")
			return
		}
		if(confirm("Va a Proceder a Realizar la Atencion del Reclamo. Una vez hecho eso no podra realizar ningun Modificacion. Desea Continuar?")==false)
		{
			return false
		}
		$.ajax({
	      url: 'atencion.php',
	      type: 'POST',
	      async: true,
	      data: 'nroreclamo='+nroreclamo+'&nrodetalle='+nrodetalle,
	      success: function(data) {
	      	OperMensaje(data)
            $("#Mensajes").html(data);    
            Buscar(0)
	      }
	  })  

		//location.href='atencion.php?nroreclamo='+nroreclamo+'&nrodetalle='+nrodetalle

	}

	function ImprimirR(obj)
	{
	  $("#form1").remove();
	  var estado = $(obj).parent().parent().data('estado')
	  var nrosol = $(obj).parent().parent().data('nrosol')
	  var nroinsc = $(obj).parent().parent().data('nroinscripcion')
	  nrosolicitud=nrosol
	  nroinscripcion=nroinsc
	    url='../ingresos/formatos/formato1.php';
  		AbrirPopupImpresion(url+'?nroreclamo='+nrosol+'&codsuc=<?=$codsuc?>&nroinscripcion='+nroinsc,800,500)


	}

	
	
	function atender_reclamos(nroreclamo,nrodetalle,codestado,estado,fin)
	{
		if(fin==1)
		{
			alert("No se puede Atender el Reclamo por que se Encuentra Finalizado")
			return
		}
		if(codestado!=1)
		{
			alert("No se puede realizar Ningun Accion sobre el Reclamo por que se encuentra: "+estado)
			return
		}		
		if(confirm("Va a Proceder a Realizar la Atencion del Reclamo. Una vez hecho eso no podra realizar ningun Modificacion.�Desea Continuar?")==false)
		{
			return false
		}
		location.href='atencion.php?nroreclamo='+nroreclamo+'&nrodetalle='+nrodetalle
	}
	function cargar_from_ajax_derivacion(nrorec,fin,par)
	{	
		if(fin==1) 
		{
			alert("No se puede Derivar el Reclamo por que ya se la ha Dado por Finalizado")
			return
		}
		cargar_from_derivacion(nrorec,par)
		$( "#dialog-form-derivar" ).dialog( "open" );
	}
	
	function cargar_from_ajax_derivacion(nrorec,fin,par)
	{
		if(fin==1)
		{
			alert("El Reclamo no se puede Derivar por que se encuentra con el Finalizado")
			return
		}

		cargar_from_derivacion(nrorec,par)
		$( "#dialog-form-derivar" ).dialog( "open" );
	}
</script>
<? include("../ingresos/ajax/from_op_impresion.php"); ?>
<div id="dialog-form-derivar" title="Derivar Solicitud de Inspeccion Ingresado"  >
    <div id="div_derivar_solicitud"></div>
</div>
<script>
$("#BtnNuevoB").hide();
</script>
<?php 
	CuerpoInferior();
?>