<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsFunciones.php");

    $conexion->beginTransaction();
    $Op = $_GET["Op"];
    $objFunciones = new clsFunciones();

    $codmedio    = $_POST["codmedio"];
    $descripcion = strtoupper($_POST["descripcion"]);
    $estareg     = $_POST["estareg"];
    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("medioreclamo", "0", "0");
            $codmedio = $id[0];

            $sql = "insert into reclamos.medioreclamo(codmedio,descripcion,estareg) values(:codmedio,:descripcion,:estareg)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codmedio" => $codmedio,
                ":descripcion" => $descripcion,
                ":estareg" => $estareg));
            break;
        case 1:
            $sql = "update reclamos.medioreclamo set descripcion=:descripcion, estareg=:estareg 
                            where codmedio=:codmedio";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codmedio" => $codmedio,
                ":descripcion" => $descripcion,
                ":estareg" => $estareg));
            break;
        case 2:case 3:
            $sql = "update reclamos.medioreclamo set estareg=:estareg
                                    where codmedio=:codmedio";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codmedio" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }

?>
