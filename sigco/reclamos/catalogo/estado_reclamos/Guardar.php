<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsFunciones.php");
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codestadoreclamo 	= $_POST["codestadoreclamo"];
	$descripcion		= $_POST["descripcion"];
	$estareg			= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("estadoreclamo","0","0");
			$codestadoreclamo = $id[0];
			
			$sql = "insert into reclamos.estadoreclamo(codestadoreclamo,descripcion,estareg) values(:codestadoreclamo,:descripcion,:estareg)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codestadoreclamo"=>$codestadoreclamo,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 1:
			$sql = "update reclamos.estadoreclamo set descripcion=:descripcion, estareg=:estareg 
			where codestadoreclamo=:codestadoreclamo";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codestadoreclamo"=>$codestadoreclamo,
						   ":descripcion"=>$descripcion,
						   ":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update reclamos.estadoreclamo set estareg=:estareg
				where codestadoreclamo=:codestadoreclamo";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codestadoreclamo"=>$_POST["1form1_id"], ":estareg"=>$estareg));
		break;
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
