<?php
  include("../../../../include/main.php");
  include("../../../../include/claseindex.php");
  $TituloVentana = "CONCEPTOS";
  $Activo=1;
 
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
   if($Op!=5) CuerpoSuperior($TituloVentana);
  $codsuc = $_SESSION['IdSucursal'];
  $FormatoGrilla = array ();
  $Sql = "SELECT s.codconcepto, s.descripcion, 
  			
			case when s.tiporeclamo=1 then 'COMERCIALES CON FACTURACION' else 
						case when tiporeclamo=2 then 'COMERCIALES SIN FACTURACION' else 'OPERACIONALES' end end , s.subtiporeclamo, 
						e.descripcion, s.estareg ,s.tiporeclamo
		FROM reclamos.conceptosreclamos s
        INNER JOIN public.estadoreg e ON (s.estareg=e.id)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'s.codconcepto', '2'=>'s.subtiporeclamo','3'=>'s.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T4'=>'Sub Categoria', 'T3'=>'Categoria','T2'=>'Descripci&oacute;n', 'T5'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A4'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W3'=>'95');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  ORDER BY codconcepto ASC ";                                   //Orden de la Consulta
   if($Op!=5)
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'6', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'6', 
              'BtnCV3'=>'0');
  else
    {
      $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
     // $FormatoGrilla[5] = array('W1'=>'60','W6'=>'80','W11'=>'20','W2'=>'70','W3'=>'70','W10'=>'90'); 
   }   
    $FormatoGrilla[10] = array(array('Name' =>'id','Col'=>1),array('Name' =>'tiporeclamo','Col'=>7));//,DATOS ADICIONALES            
    $FormatoGrilla[11]=4;//FILAS VISIBLES
  $_SESSION['Formato'] = $FormatoGrilla;

  Cabecera('', $FormatoGrilla[7], 750, 360);
  Pie();

?>
<script type="text/javascript">
function Enviar(obj)
  {
    var Id = $(obj).parent().parent().data('id')
    var tiporeclamo = $(obj).parent().parent().data('tiporeclamo')
	
    opener.RecibirTipoReclamo(Id,tiporeclamo);
    window.close();
  }
</script>
<?php if($Op!=5) CuerpoInferior(); ?>