<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../../../objetos/clsDrop.php");
	
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc 	= $_SESSION['IdSucursal'];
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsDrop();
	
	if($Id!='')
	{
		$consulta = $conexion->prepare("select * from reclamos.conceptosreclamos where codconcepto=?");
		$consulta->execute(array($Id));
		$row = $consulta->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if($("#categoria").val()==0)
		{
			alert('Seleccione la Categoria del Concepto');
			return false;
		}
		if($("#subtiporeclamo").val()==0)
		{
			alert('Seleccione la Sub. Categoria del Concepto');
			return false;
		}
		if($("#descripcion").val()=="")
		{
			alert('La Descripcion del Concepto no puede ser NULO');
			return false;
		}
		if($("#tiempo").val()=="")
		{
			alert('El valor ingresado para el tiempo no es valido');
			return false;
		}
		
		GuardarP(Op);
	}
	
	function mostrar_sub_categoria(obj,Seleccion)
	{
		$.ajax({
				 url:'../../../../ajax/subcategoria_reclamos_drop.php',
				 type:'POST',
				 async:true,
				 data:'tiporeclamo='+obj+'&seleccion='+Seleccion,
				 success:function(datos){
					$("#div_subcategoria").html(datos)
				 }
		 }) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="700" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr>
	  <td width="102">&nbsp;</td>
	  <td width="30">&nbsp;</td>
	  <td width="572" class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
	<td class="TitDetalle">Id</td>
	<td align="center">:</td>
	<td class="CampoDetalle">
		<input name="codconcepto" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Categoria</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle"><label>
	    <select name="tiporeclamo" id="categoria" class="select" style="width:220px" onchange="mostrar_sub_categoria(this.value,0)">
			<option value="0" >--Seleccione una Categoria--</option>
			<option value="1" <?=$d="";$row["tiporeclamo"]==1?$d="selected='selected'":$d="";echo $d?> >Comerciales con Facturacion</option>
			<option value="2" <?=$d="";$row["tiporeclamo"]==2?$d="selected='selected'":$d="";echo $d?> >Comerciales sin Facturacion</option>
			<option value="3" <?=$d="";$row["tiporeclamo"]==3?$d="selected='selected'":$d="";echo $d?> >Operacionales</option>
	    </select>
	  </label></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Sub. Categoria</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	  <div id="div_subcategoria">
		  <select name="subtiporeclamo" id="subtiporeclamo" class="select" style="width:500px">
			<option value="0">--Seleccione la Categoria del Reclamo--</option>
		  </select>
	  </div>
	  </td>
	  </tr>
	<tr>
	  <td class="TitDetalle" valign="top"4>Descripcion</td>
	  <td align="center" valign="top"4>:</td>
	  <td class="CampoDetalle">
	    <textarea name="descripcion" rows="5" class="inputtext" id="descripcion" style="font-size:12px; width:500px;"><?=$row["descripcion"]?></textarea></td>
	  </tr>
	<tr>
	  <td class="TitDetalle" valign="top"4>Tiempo</td>
	  <td align="center" valign="top"4>:</td>
	  <td class="CampoDetalle">
	    <input type="text" name="tiempo" id="tiempo" class="inputtext" onkeypress="return permite(event,'num');" value="<?=$row["tiempo"]?>" />
	    </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center">:</td>
	  <td class="CampoDetalle">
	    <? include("../../../../include/estareg.php"); ?>
	    </td>
	  </tr>
	<tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
          </tbody>
    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
  
  mostrar_sub_categoria(<?=$row["tiporeclamo"]?>,'<?=$row["subtiporeclamo"]?>')
</script>
<?php
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../../admin/validaciones/estareg.php"); 
?>