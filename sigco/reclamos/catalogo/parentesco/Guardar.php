<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsFunciones.php");

    $conexion->beginTransaction();
    $Op = $_GET["Op"];
    $objFunciones = new clsFunciones();

    $codtipoparentesco  = $_POST["codtipoparentesco"];
    $descripcion        = $_POST["descripcion"];
    $estareg            = $_POST["estareg"];

    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("tipoparentesco", "0", "0");
            $codtipoparentesco = $id[0];

            $sql = "insert into public.tipoparentesco(codtipoparentesco,descripcion,estareg) values(:codtipoparentesco,:descripcion,:estareg)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipoparentesco" => $codtipoparentesco,
                ":descripcion" => $descripcion,
                ":estareg" => $estareg));
            break;
        case 1:
            $sql = "update public.tipoparentesco set descripcion=:descripcion, estareg=:estareg 
                            where codtipoparentesco=:codtipoparentesco";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipoparentesco" => $codtipoparentesco,
                ":descripcion" => $descripcion,
                ":estareg" => $estareg));
            break;
        case 2:case 3:
            $sql = "update public.tipoparentesco set estareg=:estareg
                                    where codtipoparentesco=:codtipoparentesco";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codtipoparentesco" => $_POST["1form1_id"], ":estareg" => $estareg));
            break;
    }
    
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
    
?>	
