<?php
include("../../../../objetos/clsReporte.php");
include("../../../../objetos/clsReporteExcel.php");
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=InstalacionesMedidores.xls");
header("Pragma: no-cache");
header("Expires: 0");
$clsFunciones = new clsFunciones();
$objReporte = new clsReporte();
$codsuc = $_GET["codsuc"];
$Hasta = $_GET["Hasta"];
$Desde = $_GET["Desde"];
?>

<?php CabeceraExcel(2, 13); ?>
<table class="ui-widget" border="0" cellspacing="0" id="TbTitulo" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:14px">

        <tr title="Cabecera">
            <th scope="col" colspan="15" align="center" >INSTALACIONES DE MEDIDORES</th>
        </tr>
        <tr>
            <th colspan="15">Fecha de Ingreso desde: <?= $Desde ?> hasta: <?= $Hasta ?> </th>
        </tr>    
    </thead>
</table>
<table class="ui-widget" border="1" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">

        <tr>
            <th width="50" scope="col" class="ui-widget-header">N&deg;</th>
            <th width="200" scope="col" class="ui-widget-header">COD. CATASTRAL</th>
            <th width="200" scope="col" class="ui-widget-header">COD. ANTIGUO</th>
            <th width="50" scope="col" class="ui-widget-header">NRO. INSC.</th>
            <th width="30" scope="col" class="ui-widget-header">CLIENTE</th>
            <th width="50" scope="col" class="ui-widget-header">DIRECCION</th>
            <th width="200" scope="col" class="ui-widget-header">NRO. MED.</th>            
            <th width="50" scope="col" class="ui-widget-header">EST</th>
            <th width="50" scope="col" class="ui-widget-header">TS</th>
            <th width="50" scope="col" class="ui-widget-header">FECHA INST.</th>
            <th width="200" scope="col" class="ui-widget-header">EMP. CONTRAST.</th>
            <th width="200" scope="col" class="ui-widget-header">INSPECTOR</th>
            

        </tr>
    </thead>
    <tbody style="font-size:12px">
        <?php
        $h = 4;
        $s = 2;

        if ($Desde != "") {
            $Condicion = " AND cx.fechainsmed BETWEEN CAST ('" . $Desde . "' AS DATE) AND CAST ( '" . $Hasta . "' AS DATE ) ";
        }

        $sql = "SELECT 
                DISTINCT d.coddetmedidor, 
                d.nroinscripcion,
                tc.descripcioncorta||' '||cl.descripcion AS direccion,
                c.propietario, 
                c.nrocalle, 
                e.descripcion AS estadoservicio,
                c.codtiposervicio,
                d.nromed,
                inp.nombres,
                c.codemp|| '-' ||c.codsuc|| '-'||c.codsector ||'-'||c.codmanzanas ||'-'||c.lote AS codcatastro,
                cx.fechainsmed,
                emp.razonsocial,
                c.codantiguo
                FROM micromedicion.detmedidor AS d 
                INNER JOIN catastro.clientes AS c ON (c.codemp=d.codemp AND c.codcliente = d.codcliente AND c.nroinscripcion = d.nroinscripcion)
                INNER JOIN public.calles AS cl ON (c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.codcalle = cl.codcalle AND c.codzona = cl.codzona) 
                INNER JOIN public.tiposcalle AS tc ON (cl.codtipocalle = tc.codtipocalle) 
                INNER JOIN public.estadoservicio AS e ON (c.codestadoservicio = e.codestadoservicio)
                INNER JOIN reglasnegocio.inspectores AS inp ON inp.codinspector = d.codinspector 
                INNER JOIN catastro.conexiones AS cx ON (c.codemp = cx.codemp AND c.codsuc = cx.codsuc AND c.nroinscripcion = cx.nroinscripcion)
                INNER JOIN micromedicion.empresacontrastadora AS emp ON (emp.codempcont = d.codempcont AND emp.codsuc= d.codsuc)

                WHERE c.codemp=1 and c.codsuc=? AND c.nromed<> ' ' AND d.estareg=2 ".$Condicion." ORDER BY cx.fechainsmed ";
                        
        $count = 0;

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();
        $count = 0;
        foreach ($items as $row) {
            $count++;
            $codcatastro = $row["codcatastro"];

            if ($row["codtiposervicio"] == 1) {
                $ts = "Ag/Des";
            }
            if ($row["codtiposervicio"] == 2) {
                $ts = "Ag";
            }
            if ($row["codtiposervicio"] == 3) {
                $ts = "Des";
            }
            ?>

            <tr <?= $title ?> <?= $class ?> onclick="SeleccionaId(this);" id="<?= $count ?>" >
                <td align="left" valign="middle"><? echo $count; ?></td>
                <td align="left" valign="middle"><? echo ($row['codcatastro']); ?></td>
                <td align="left" valign="middle"><? echo ($row['codantiguo']); ?></td>
                <td align="left" valign="middle"><? echo ($row['nroinscripcion']); ?></td>
                <td align="left" valign="middle"><? echo (trim((strtoupper($row["propietario"])))); ?></td>
                <td align="left" valign="middle"><? echo utf8_decode(strtoupper($row[2]." # ".$row["nrocalle"])); ?></td>
                <td align="left" valign="middle"><? echo ($row['nromed']); ?></td>
                <td align="left" valign="middle"><? echo ( strtoupper($row["estadoservicio"])); ?></td>
                <td align="left" valign="middle"><? echo ($ts); ?></td>
                <td align="left" valign="middle"><? echo ($row['fechainsmed']); ?></td>
                <td align="left" valign="middle"><? echo (strtoupper($row["razonsocial"])); ?></td>
                <td align="left" valign="middle"><? echo ($row['nombres']); ?></td>
                
                <?php
            }
            ?>
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td colspan="12" align="center" class="ui-widget-header">Instalaciones de Medidores Registrados <?= $count ?></td>

        </tr>
    </tfoot>
</table>
