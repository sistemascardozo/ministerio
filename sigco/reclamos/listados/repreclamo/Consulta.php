<?php	
	session_name("pnsu");
	if(!session_start()){session_start();}

    include("../../../../objetos/clsDrop.php");
    $objMantenimiento = new clsDrop();
    $Presicion = $_SESSION["Presicion"];
    $codsuc = $_SESSION["IdSucursal"];
    $NroInscripcion = $_POST["NroInscripcion"];
    $Sql = "select clie.nroinscripcion,".$objMantenimiento->getCodCatastral("clie.").", 
			clie.propietario,td.abreviado,clie.nrodocumento,
			tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
			es.descripcion,tar.nomtar,clie.codantiguo,ts.descripcion as tiposervicio,tipofacturacion,nromed,consumo
			from catastro.clientes as clie 
			inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
			inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
			inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
			inner join public.tiposervicio as ts on(ts.codtiposervicio=clie.codtiposervicio)
			inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
			WHERE clie.codemp=1 AND clie.codsuc=".$codsuc." AND clie.nroinscripcion=".$NroInscripcion."";
    $Consulta = $conexion->query($Sql);
    $row = $Consulta->fetch();

    switch ($row['tipofacturacion'])
    {
    case 0: $TipoFacturacion = 'CONSUMO LEIDO ( Lect. Ultima '.$row['consumo'].'m&#179;)';
    break;
    case 1: $TipoFacturacion = 'CONSUMO PROMEDIADO ('.$row['consumo'].'m&#179;)';
    break;
    case 2: $TipoFacturacion = 'CONSUMO ASIGNADO ('.$row['consumo'].'m&#179;)';
    break;
    }
    $Medidor = 'No';
    if(trim($row['nromed'])!="")
    $Medidor = 'Si ( N° '.$row['nromed'].')';
?>
<table class="ui-widget" bORDER="0" cellspacing="0" width="100%" rules="rows" align="center">
    <thead style="font-size:12px;">	
        <tr>
            <td><span class="Negrita">N&deg; de Inscripcion:</span><?= $NroInscripcion ?></td>
            <td><span class="Negrita">C&oacute;digo Catastral:</span> <?= $row[1] ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">C&oacute;digo Antiguo:</span> <?= $row['codantiguo'] ?></td>
            <td><span class="Negrita"><?= $row['abreviado'] ?>:</span> <?= $row['nrodocumento'] ?></td>
        </tr>
        <tr>
            <td align="left"><span class="Negrita">Cliente:</span> <?= $row['propietario'] ?></td>
            <td><span class="Negrita">Tipo Servicio:</span> <?= $row['tiposervicio'] ?></td>

        </tr>
        <tr>
            <td colspan="2" align="left"><span class="Negrita">Direcci&oacute;n :</span><?= strtoupper($row['direccion']) ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">Estado:</span> <?= $row['descripcion'] ?></td>
            <td><span class="Negrita">Categoria:</span> <?= $row['nomtar'] ?></td>
        </tr>
        <tr>
            <td><span class="Negrita">Tipo Facturaci&oacute;n:</span> <?= $TipoFacturacion ?></td>
            <td><span class="Negrita">Medidor:</span><?= $Medidor ?></td>
        </tr>
    </thead>
</table>
<table class="ui-widget" bORDER="0" cellspacing="0" id="TbIndex" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
        <tr>
            <th width="100" scope="col">Item</th>
            <th width="50" scope="col">Fecha</th>
            <th width="50" scope="col">Serie</th>
            <th width="50" scope="col">N&uacute;mero</th>
            <th width="200" scope="col">Periodo Facturaci&oacute;n</th>
            <th width="300" scope="col">Operaci&oacute;n</th>
            <th width="100" scope="col">Cargo</th>
            <th width="100" scope="col">Abono</th>
            <th width="100" scope="col">Saldo</th>
            <th width="100" scope="col">Estado Recibo</th>

            <th scope="col" width="10" align="center">&nbsp;</th>

        </tr>
    </thead>
    <tbody style="font-size:12px">
        <?php
        $Condicion1="WHERE f.nroinscripcion =".$NroInscripcion."";
        $Orden1=" ORDER BY f.nrofacturacion ASC ";
        $Condicion2="WHERE p.nroinscripcion =".$NroInscripcion."  AND d.nrofacturacion<>0 AND p.anulado=0";
        $Orden2=" ORDER BY p.nrofacturacion ASC ";						

        $Sql = "(SELECT f.fechareg, f.serie,f.nrodocumento,".$objMantenimiento->Convert('f.anio','INTEGER')." as anio, 
            ".$objMantenimiento->Convert('f.mes','INTEGER')." AS mes, sum(df.importe - (df.imppagado + df.importerebajado)) as cargo,
            '0.00' as abono,'0' as movimiento, f.enreclamo as reclamo
            FROM facturacion.cabfacturacion f 
            INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) 
            AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
            ".$Condicion1." 
            GROUP BY f.enreclamo, f.fechareg,f.serie,f.nrodocumento,f.anio, f.mes, f.nrofacturacion 
            )
            UNION
            (
            SELECT p.fechareg, d.serie, d.nrodocumento,
            ".$objMantenimiento->Convert('d.anio','INTEGER')." as anio,".$objMantenimiento->Convert('d.mes','INTEGER')." AS mes, '0.00' as cargo,sum(d.importe) as abono,'1' as movimiento,'0' as reclamo
            FROM
            cobranza.cabpagos p
            INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp)
            AND (p.codsuc = d.codsuc)
            AND (p.nroinscripcion = d.nroinscripcion)
            AND (p.nropago = d.nropago) ".$Condicion2." 
            GROUP BY  p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes
            ORDER BY p.fechareg ASC
            )
            ORDER by fechareg,anio, mes ASC	";

        $Consulta =$conexion->query($Sql);
        $c=0;
        $Deuda=0;
        foreach($Consulta->fetchAll() as $row)
        {	
            $c++;
            $Operacion='';
            if($row['movimiento']==0)
            {
                $Operacion="FACTURACIÓN DE PENSIONES";
                $Anio=$row['anio'];
                $Mes=$row['mes'];
                $Deuda +=  $row['cargo'];
            }
            else
            {
                if($row['anio']==$Anio && $row['mes']==$Mes)
                $Operacion="PAGO RECIBO DE PENSIONES";
                else
                $Operacion="PAGO DEUDA ACUMULADA";

                $Anio=$row['anio'];
                $Mes=$row['mes'];
                $Deuda -=  $row['abono'];
            }

        ?>

        <tr <?= $title ?> <?= $class ?> onclick="SeleccionaId(this);" id="<?= $c ?>" >
            <td align="center"><?= $c ?></td>
            <td align="center" valign="middle"><?= $objMantenimiento->DecFecha($row['fechareg']) ?></td>
            <td align="center" valign="middle"><?= $row['serie'] ?></td>
            <td align="center" valign="middle"><?= $row['nrodocumento'] ?></td>
            <td align="left" valign="middle"><?= $meses[$row['mes']]." - ".$row['anio'] ?></td>
            <td align="left" valign="middle"><?= $Operacion ?></td>
            <td align="right" valign="middle"><?= number_format($row['cargo'], 2) ?></td>
            <td align="right" valign="middle"><?= number_format($row['abono'], 2) ?></td>
            <td align="right" valign="middle"><?= number_format($Deuda, 2) ?></td>
            <td>
                <span class="ReyEryckSoft-icon-detalle" onclick="VerDetalle(<?= $row[0] ?>)"></span>


            </td>
            <td><?= $row['reclamo'] = ($row['reclamo'] == 1 ) ? 'R' : '' ;?></td>
        </tr>

        <?php
        }
        ?>
    </tbody>
    <tfoot class="ui-widget-header" style="font-size:10px">
        <tr>
            <td colspan="8" align="right" >Deuda Total:</td>
            <td align="right" ><?= number_format($Deuda, 2) ?></td>
            <td scope="col" colspan="2" align="center">&nbsp;</td>
        </tr>
    </tfoot>
</table>