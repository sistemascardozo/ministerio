<?php 
	include("../../../../objetos/clsReporte.php");
	
	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc,$meses,$Desde,$Hasta;
			
			$periodo = $this->DecFechaLiteral();
			
			$this->SetFont('Arial','B',10);
			
			$h = 4;
			$this->Cell(278, $h+2,"LISTA DE RECLAMOS",0,1,'C');
			$this->SetFont('Arial','B',8);
			$this->Cell(278, $h+2,'Fecha de Registro desde: '.$Desde." hasta: ".$Hasta,0,1,'C');
			
			$this->Ln(3);
			
			$this->SetFont('Arial','B',6);
			$this->SetWidths(array(8,20,15,60,70,30,40,40));
			$this->SetAligns(array(C,C,C,C,C,C,C,C,C,C));
			$this->Row(array(utf8_decode("N°"),
							"NRO. RECLAMO",
							 "NRO. INSC.",
							 "PROPIETARIO",
							 "RECLAMANTE",
							 "NRO. DOCUMENTO",
							 "CATEGORIA",
							 "ESTADO",							
							 ));

		}
		function contenido($codsuc,$Desde,$Hasta,$conrecla,$estrec)
		{
			global $conexion;
			
			$h = 4;
			$s = 2;			
			
			$Condicion=" WHERE rec.fechaemision BETWEEN CAST ('".$Desde."' AS DATE) AND CAST ( '".$Hasta."' AS DATE ) ";						
			$sql = "SELECT 
			rec.correlativo,
			rec.nroinscripcion,
			clie.propietario,
			rec.reclamante,
            tipdoc.abreviado|| ':' ||rec.nrodocumento as nrodocumento,
            case when rec.tiporeclamo=1 then 'COMERCIALES CON FACTURACION' else 
            case when rec.tiporeclamo=2 then 'COMERCIALES SIN FACTURACION' else 'OPERACIONALES' end end,
            estado.descripcion as estText,
            rec.glosa,
            rec.codestadoreclamo,1,1,
            rec.nroreclamo
            FROM reclamos.reclamoshis as rec
            inner join catastro.clientes as clie on(rec.codemp=clie.codemp and rec.codsuc=clie.codsuc and
            rec.nroinscripcion=clie.nroinscripcion)
            inner join public.tipodocumento as tipdoc on(rec.codtipodocumento=tipdoc.codtipodocumento)
            inner join public.tipoparentesco as tippar on (rec.codtipoparentesco=tippar.codtipoparentesco)
            inner join reclamos.estadoreclamo as estado on(rec.codestadoreclamo=estado.codestadoreclamo)" . $Condicion;		
			
			if($conrecla != '%'){
				$sql = $sql . " AND rec.codconcepto = " . $conrecla ;
				
			}
			if($estrec != '%'){
				$sql = $sql . " AND rec.codestadoreclamo = " . $estrec ;
				
			}

			$count= 0;
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute();
			$items = $consulta->fetchAll();
			foreach($items as $row)
			{
				$count++;
				$nroreclamo = $row["nroreclamo"];
				
				$this->SetWidths(array(8,20,15,60,70,30,40,40));
				$this->SetAligns(array(C,C,"L","L",C,C,C,C,"L","L"));
				$this->Row(array($count,
					$row["correlativo"], 
					$this->CodUsuario($codsuc,$row["nroinscripcion"]),
					trim(utf8_decode(strtoupper($row["propietario"]))),
					utf8_decode(strtoupper($row["reclamante"])),
					$row["nrodocumento"],
					strtoupper($row["case"]),
					strtoupper($row["esttext"])
				));				
				
			}
			
			$this->Ln(3);
			$this->Cell(278, $h+2,"CANTIDAD DE RECLAMOS ".$count,'TB',1,'C');
			$this->SetFont('Arial','B',8);
			$this->Ln(3);
						
		}
	}
	
	$codsuc   = $_GET["codsuc"];
	$Desde    = $_GET["Desde"];
	$Hasta    = $_GET["Hasta"];
	$conrecla = $_GET["conceptoid"];
	$estrec   = $_GET["estadoreclamo"];
	
	$objReporte = new clsPadron("L");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsuc,$Desde,$Hasta,$conrecla,$estrec);
	$objReporte->Output();
	
?>