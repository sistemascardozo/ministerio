<?php
	include("../../../../objetos/clsReporte.php");
	
	class clsEstructura extends clsReporte
	{
		function cabecera()
		{
			global $NroInscripcion,$codsuc,$objreporte,$conexion,$Dim;
			
			$h=4;
			$this->SetFont('Arial','B',12);
			$this->Cell(0, $h+1,"ESTADO DE CUENTA CORRIENTE",0,1,'C');

			$Sql ="select clie.nroinscripcion,".$objreporte->getCodCatastral("clie.").", 
			clie.propietario,td.abreviado,clie.nrodocumento,
			tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle as direccion,
			es.descripcion,tar.nomtar,clie.codantiguo,ts.descripcion as tiposervicio,tipofacturacion,nromed,consumo
			from catastro.clientes as clie 
			inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona)
			inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)
			inner join public.tipodocumento as td on(clie.codtipodocumento=td.codtipodocumento)
			inner join public.estadoservicio as es on(es.codestadoservicio=clie.codestadoservicio)
			inner join public.tiposervicio as ts on(ts.codtiposervicio=clie.codtiposervicio)
			inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar)
			WHERE clie.codemp=1 AND clie.codsuc=".$codsuc." AND clie.nroinscripcion=".$NroInscripcion."";
			$Consulta = $conexion->query($Sql);
			$row = $Consulta->fetch();
			define('Cubico', chr(179));
			switch ($row['tipofacturacion']) 
			{
				case 0: $TipoFacturacion ='CONSUMO LEIDO ( Lect. Ultima '.$row['consumo'].'m'.Cubico.')';break;
				case 1: $TipoFacturacion ='CONSUMO PROMEDIADO ('.$row['consumo'].'m'.Cubico.')';break;
				case 2: $TipoFacturacion ='CONSUMO ASIGNADO ('.$row['consumo'].'m'.Cubico.')';break;
			}
			$Medidor='No';
			if(trim($row['nromed'])!="")
				$Medidor='Si ( N° '.$row['nromed'].')';
			$this->SetLeftMargin(30);
			$this->Ln(5);

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("N° de Inscripcion"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h,$NroInscripcion,0,0,'L');

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Código Catastral"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$row[1],0,1,'L');
			/*-------------*/
			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Código Antiguo"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h,$row['codantiguo'],0,0,'L');

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode($row['abreviado']),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$row['nrodocumento'],0,1,'L');
			/*-------------*/
			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Cliente"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h,$row['propietario'],0,0,'L');

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,'Tipo Servicio',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$row['tiposervicio'],0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Dirección"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,strtoupper($row['direccion']),0,1,'L');

			/*-------------*/
			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Estado"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h,$row['descripcion'],0,0,'L');

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,'Categoria',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,$row['nomtar'],0,1,'L');
			/*-------------*/
			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,utf8_decode("Tipo Facturación"),0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(70,$h,$TipoFacturacion,0,0,'L');

			$this->SetFont('Arial','B',8);
			$this->Cell(30,$h,'Medidor',0,0,'L');
			$this->Cell(5,$h,":",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,utf8_decode($Medidor),0,1,'L');
			
			$this->Ln(5);
			$this->SetX(10);
			$this->SetFont('Arial','B',8);
	 	 	$this->Cell($Dim[1],5,utf8_decode('N°'),1,0,'C',false);
	 	 	$this->Cell($Dim[2],5,utf8_decode('Fecha'),1,0,'C',false);
			$this->Cell($Dim[3],5,utf8_decode('Serie'),1,0,'C',false);
			$this->Cell($Dim[4],5,utf8_decode('Número'),1,0,'C',false);
			$this->Cell($Dim[5],5,utf8_decode('Periodo Facturación'),1,0,'C',false);
			$this->Cell($Dim[6],5,utf8_decode('Operación'),1,0,'C',false);
			$this->Cell($Dim[7],5,utf8_decode('Cargo'),1,0,'C',false);
			$this->Cell($Dim[8],5,utf8_decode('Abono'),1,0,'C',false);
			$this->Cell($Dim[9],5,utf8_decode('Saldo'),1,1,'C',false);
	
			
			
			
		}
		function Detalle()
		{
			global $NroInscripcion, $codsuc, $objreporte, $conexion, $Dim, $meses;
			
			$Condicion1="WHERE f.nroinscripcion =".$NroInscripcion."";
			$Orden1=" ORDER BY f.nrofacturacion ASC ";
			$Condicion2="WHERE p.nroinscripcion =".$NroInscripcion."  AND d.nrofacturacion<>0 AND p.anulado=0 ";
			$Orden2=" ORDER BY p.nrofacturacion ASC ";						

			$Sql = "(SELECT f.fechareg,f.serie,f.nrodocumento,".$this->Convert('f.anio','INTEGER')." as anio, 
				".$this->Convert('f.mes','INTEGER')." AS mes, sum(df.importe - (df.imppagado + df.importerebajado)) as cargo ,
			'0.00' as abono,'0' as movimiento
			FROM facturacion.cabfacturacion f 
			INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion) 
			AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
			".$Condicion1." 
			GROUP BY f.fechareg,f.serie,f.nrodocumento,f.anio, f.mes, f.nrofacturacion 
			)
			UNION
			(
			SELECT p.fechareg, d.serie, d.nrodocumento,
			".$this->Convert('d.anio','INTEGER')." as anio,".$this->Convert('d.mes','INTEGER')." AS mes, '0.00' as cargo,sum(d.importe) as abono,'1' as movimiento
			FROM
			cobranza.cabpagos p
			INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp)
			AND (p.codsuc = d.codsuc)
			AND (p.nroinscripcion = d.nroinscripcion)
			AND (p.nropago = d.nropago)
			".$Condicion2." 
			GROUP BY  p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes
			ORDER BY p.fechareg ASC
			)
			ORDER by fechareg,anio, mes ASC	";  

			$Consulta =$conexion->query($Sql);
			
			$c=0;
			$Deuda=0;
			$this->SetFillColor(255,255,255); //Color de Fondo
			$this->SetTextColor(0);
			$this->SetLeftMargin(10);
			$this->SetFont('Arial','',8);
			foreach($Consulta->fetchAll() as $row)
			{	
				$c++;
				
				$Operacion='';
				if($row['movimiento']==0)
					{
						$Operacion="FACTURACIÓN DE PENSIONES";
						$Anio=$row['anio'];
						$Mes=$row['mes'];
						$Deuda +=  $row['cargo'];
					}
				else
				{
					if($row['anio']==$Anio && $row['mes']==$Mes)
						$Operacion="PAGO RECIBO DE PENSIONES";
					else
						$Operacion="PAGO DEUDA ACUMULADA";

					$Anio=$row['anio'];
					$Mes=$row['mes'];
					$Deuda -=  $row['abono'];
				}
				$this->SetX(10);
				$this->Cell($Dim[1],5,$c,1,0,'C',true);
		 	 	$this->Cell($Dim[2],5,$objreporte->DecFecha($row['fechareg']),1,0,'C',true);
				$this->Cell($Dim[3],5,$row['serie'],1,0,'C',true);
				$this->Cell($Dim[4],5,$row['nrodocumento'],1,0,'C',true);
				$this->Cell($Dim[5],5,$meses[$row['mes']]." - ".$row['anio'],1,0,'C',true);
				$this->Cell($Dim[6],5,utf8_decode($Operacion),1,0,'C',true);
				$this->Cell($Dim[7],5,number_format($row['cargo'],2),1,0,'R',true);
				$this->Cell($Dim[8],5,number_format($row['abono'],2),1,0,'R',true);
				$this->Cell($Dim[9],5,number_format($Deuda,2),1,1,'R',true);
				

			}
			$this->Ln(2);
			$this->SetFont('Arial','B',10);
			$this->Cell(array_sum($Dim)-$Dim[9],5,'Deuda Total:',0,0,'R',true);
			$this->Cell($Dim[9],5,number_format($Deuda,2),0,1,'R',true);
			
		}
    
    }
	define('Cubico', chr(179));
	
	$codsuc	= $_SESSION["IdSucursal"];
	$NroInscripcion = $_GET["NroInscripcion"];
	$Dim = array('1'=>10,'2'=>15,'3'=>10,'4'=>15,'5'=>30,'6'=>50,'7'=>20,'8'=>20,'9'=>20);
    $x = 20;
    $h=4;
	$objreporte	=	new clsEstructura();
	$objreporte->AliasNbPages();
	$objreporte->AddPage("P");
	$objreporte->Detalle("P");
	
	
	

	
	$objreporte->Output();	
	
?>