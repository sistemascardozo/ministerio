<?php 

ini_set("display_errors",1);
// error_reporting(E_ALL);
//    if(!session_start()){session_start();}
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "LISTA DE RECLAMOS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>

	jQuery(function($){ 

		$("#fdesde").mask("99/99/9999");
		$("#fhasta").mask("99/99/9999");
		$( "#DivTipos" ).buttonset();
		$( "#fdesde" ).datepicker({
					showOn: 'button',
					direction: 'up',
					buttonImage: '../../../../images/iconos/calendar.png',
					buttonImageOnly: true,
					showOn: 'both',
					showButtonPanel: true
		});
		
		$( "#fhasta" ).datepicker({
					showOn: 'button',
					direction: 'up',
					buttonImage: '../../../../images/iconos/calendar.png',
					buttonImageOnly: true,
					showOn: 'both',
					showButtonPanel: true
		})
	});
	
	function cargar_conceptos_reclamos(tipo,seleccion)
	{
	$.ajax({
		 url:'<?php echo $_SESSION['urldir'];?>ajax/conceptos_reclamos_drop.php',
		 type:'POST',
		 async:true,
		 data:'tiporeclamo='+tipo+'&seleccion='+seleccion,
		 success:function(datos){
                    $("#div_conceptos_reclamos").html(datos);
                    $("#tiporeclamo").val(tipo);
                    $(".buttonset").buttonset('refresh');
                    $("#chkconcepto").prop('checked', false);
                    $("#todosconceptoreclamo").val(0);
		 }
	}) 
	}
	var c = 0
	function ValidarForm(Op)
	{
		

		var Desde = $("#fdesde").val()
		var Hasta = $("#fhasta").val()
		if (Trim(Desde)=='')
		{
			
			Msj('#fdesde','Seleccione la Fecha Inicial',1000,'above','',false)
			return false;
			
		}
		if (Trim(Hasta)=='')
		{
			
			Msj('#fhasta','Seleccione la Fecha Final',1000,'above','',false)
			return false;
			
		}
		Fechas = '&Desde='+Desde+'&Hasta='+Hasta
		if($("#todostiporeclamo").val()==0)
		{
			estadoreclamo=$("#codestadoreclamo").val()
			if(estadoreclamo==10000)
			{
				alert("Seleccione el Estado de Reclamo")
				return false
			}
		}else{
			estadoreclamo="%"
		}

		if($("#todosconceptoreclamo").val()==0)
		{
			conceptoreclamo=$("#codconcepto").val()
			if(conceptoreclamo==0)
			{
				alert("Seleccione el Concepto de Reclamo")
				return false
			}
		}else{
			conceptoreclamo="%"
		}


		if(document.getElementById("rptpdf").checked==true)
		{
			url="imprimir.php";
		}	
		url += "?codsuc=<?=$codsuc?>"+Fechas+"&conceptoid="+conceptoreclamo+"&estadoreclamo="+estadoreclamo
		AbrirPopupImpresion(url,800,600)
		
		return false;
	}

	function Cancelar()
	{
		location.href='<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function buscar_tipo_reclamos()
	{
		AbrirPopupBusqueda("../../catalogo/conceptosreclamo/?Op=5",1100,500)
	}
	function RecibirTipoReclamo(id,tipo)
	{
		$( "#tabs" ).tabs("select",1);
		$("#tipo_reclamo"+tipo).attr("checked", true);
		$(".buttonset").buttonset('refresh');
		cargar_conceptos_reclamos(tipo,id)
		$("#glosa").focus().select();
		$("#todosconceptoreclamo").val(0);

	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tbody>
			<tr>
				<td colspan="2" align="center">
					<fieldset style="padding:4px">
						<table width="900" border="0" cellspacing="0" cellpadding="0">				  
							<tr>
							  <td >&nbsp;</td>
							  <td align="center">&nbsp;</td>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
							  <td align="center">&nbsp;</td>
							  <td colspan="3">&nbsp;</td>
						  </tr>
							<tr>
                                <td width="100" >Desde</td>
                                <td width="30" align="center">:</td>
                                <td width="100">
                                    <input type="text" name="fdesde" id="fdesde" value="<?=date('d/m/Y')?>" class="inputtext" style="width:80px;" />
                                </td>
                              <td width="70" align="right">Hasta</td>
                              <td width="30" align="center">:</td>
                              <td colspan="3"><input type="text" name="fhasta" id="fhasta" value="<?=date('d/m/Y')?>" class="inputtext" style="width:80px;" /></td>
                            </tr>
                            <tr>
                                <td>Tipo de Reclamo </td>
                                <td align="center">:</td>
                                <td class="CampoDetalle" colspan="6">
                                    <div class="buttonset" style="display:inline">
                                      <input id="tipo_reclamo1" name="radiobutton" type="radio" value="1" onclick="cargar_conceptos_reclamos(1,1000)" checked='cheked'/>
                                      <label for="tipo_reclamo1"   >Comerciales con Facturacion</label>
                                      <input id="tipo_reclamo2" name="radiobutton" type="radio" value="2" onclick="cargar_conceptos_reclamos(2,1000)"/>
                                      <label for="tipo_reclamo2"  >Comerciales sin Facturacion</label>
                                      <input id="tipo_reclamo3" name="radiobutton" type="radio" value="3" onclick="cargar_conceptos_reclamos(3,1000)"/>
                                      <label for="tipo_reclamo3">Operacionales</label>
                                    </div>
                                    <span class="MljSoft-icon-buscar" title="Buscar Usuario" onclick="buscar_tipo_reclamos();"></span>
                                    <input type="hidden" name="tiporeclamo" id="tiporeclamo" value="" />       
		        				</td>
                          </tr>
                            <tr>
                                <td align="left">Reclamo</td>
                                <td align="center">:</td>
                                <td colspan="4" class="CampoDetalle">
                                    <div id="div_conceptos_reclamos">
                                        <select id="codconcepto" name="codconcepto" class="select" style="width:400px">
                                                <option>--Seleccione el Concepto del Reclamo--</option>
                                        </select>
                                    </div>
                                </td>
                                <td width="10">&nbsp;</td>
                              <td><input type="checkbox" name="chkconcepto" id="chkconcepto" onclick="CambiarEstado(this,'todosconceptoreclamo');validar_concepto(this,'todostiporeclamo')" />
                              Todos </td>
                            </tr>
                            <tr>
					   <td align="left">Estado Reclamo</td>
					   <td align="center">:</td>
					   <td colspan="4" class="CampoDetalle">
				            <?php echo $objMantenimiento->drop_estadoreclamo($codsuc,0); ?>
					    </td>
					   <td class="CampoDetalle">&nbsp;</td>
					   <td class="CampoDetalle"><input type="checkbox" name="chkreclamos" id="chkreclamos" checked="checked" onclick="CambiarEstado(this,'todostiporeclamo');validar_tiporeclamo(this,'todostiporeclamo')" />
					     Todos </td>
			   		      </tr>
					<tr>
						<td>&nbsp;</td>
						<td align="center">&nbsp;</td>
						<td colspan="7">
                            <input type="hidden" name="todostiporeclamo" id="todostiporeclamo" value="1" />
                            <input type="hidden" name="todosconceptoreclamo" id="todosconceptoreclamo" value="1" />
						</td>
						</tr>
					<tr>
						<td colspan="8" align="center">
							<div id="DivTipos" style="display:inline">
								<input type="radio" name="rabresumen" id="rptpdf" value="radio" checked="checked" />
								<label for="rptpdf">PDF</label>
							</div>	
							<input type="button" onclick="return ValidarForm();" value="Generar" id="">
						</td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
 
	 </tbody>

    </table>
 </form>
</div>
<script>

	function cargar_rutas_lecturas(obj,cond)
	{
		$.ajax({
			 url:'../../../../ajax/rutas_lecturas_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsector='+obj+'&codsuc=<?=$codsuc?>&condicion='+cond,
			 success:function(datos){
				$("#div_rutaslecturas").html(datos)
				$("#chkrutas").attr("checked",true)
				$("#todosrutas").val(1)
			 }
		}) 
	}
	function validar_tiporeclamo(obj)
	{
		if(obj.checked)
		{
			$("#codestadoreclamo").attr("disabled",true)
		}else{
			$("#codestadoreclamo").attr("disabled",false)
			
		}
	}
	function validar_concepto(obj)
	{
		if(obj.checked)
		{
			$("#codconcepto").attr("disabled",true)
		}else{
			$("#codconcepto").attr("disabled",false)
			
		}
	}
</script>
<script> cargar_conceptos_reclamos(1,1000);	</script>

<?php CuerpoInferior(); ?>