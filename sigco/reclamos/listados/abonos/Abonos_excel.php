<?php
set_time_limit(0);

include("../../../../objetos/clsReporte.php");
include("../../../../objetos/clsReporteExcel.php");
$codsuc    = $_GET["codsuc"];
$clsFunciones = new clsFunciones();
$objReporte   = new clsReporte();

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=Abonos_".date('Y-m-d').".xls");
header("Pragma: no-cache");
header("Expires: 0");


$ciclo              = $_GET["ciclo"];
$anio               = $_GET["anio"];
$mes                = $_GET["mes"];
$ciclotext          = $_GET["ciclotext"];
if($_GET["sector"]!= "%"){$Csec = " AND cl.codsector=".$_GET["sector"];}else{$Csec = "";}
$m                  = $meses[$mes];
$mes_modificado     = str_pad($mes,2,'0',STR_PAD_LEFT);

$sql = "SELECT nrofacturacion, fechaemision ";
$sql .= "FROM facturacion.periodofacturacion ";
$sql .= "WHERE codemp = 1 ";
$sql .= " AND codsuc = ? ";
$sql .= " AND codciclo = ? ";
$sql .= " AND anio = ? ";
$sql .= " AND mes = ?";

$consulta = $conexion->prepare($sql);
$consulta->execute(array($codsuc, $codciclo, $anio, $mes));
$row = $consulta->fetch();

$nrofacturacion = $row['nrofacturacion'];

?>
<?php CabeceraExcel(2,14); ?>
<table class="ui-widget" border="1" cellspacing="0" id="TbConsulta" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:10px">
    <tr title="Cabecera">
        <th scope="col" class="ui-widget" colspan="11" align="center" class="ui-widget-header">REPORTE DE ABONOS <?=$meses[intval($mes)]."-".$anio?></th>
    </tr>
    <tr title="Cabecera">
        <th scope="col" width="8%" align="center" class="ui-widget-header">ITEM</th>
        <th scope="col" width="8%" align="center" class="ui-widget-header">NRO. DCTO.</th>
        <th scope="col" width="8%" align="center" class="ui-widget-header">FECHA</th>
        <th scope="col" width="10%" align="center" class="ui-widget-header">GLOSA</th>
        <th scope="col" width="50%" align="center" class="ui-widget-header">NOMBRE</th>
        <th scope="col" width="15%" align="center" class="ui-widget-header">VALOR DE VENTA</th>
        <th scope="col" width="5%" align="center" class="ui-widget-header">NO IMPONIBLE</th>
        <th scope="col" width="5%" align="center" class="ui-widget-header">I.G.V.</th>
        <th scope="col" width="5%" align="center" class="ui-widget-header">REDONDEO</th>
        <th scope="col" width="5%" align="center" class="ui-widget-header">IMPORTE</th>
        <th scope="col" width="20%" align="center" class="ui-widget-header">RECIBO</th>
    </tr>
    </thead>
    <tbody style="font-size:10px">
<?php
$ultimodia = $clsFunciones->obtener_ultimo_dia_mes($mes, $anio);
$primerdia = $clsFunciones->obtener_primer_dia_mes($mes, $anio);
$count = $impmes = $impigv = $imptotal = 0;

//REBAJAS
if ($anio == 2016 && intval($mes) <= 2)
{


	$Sql = "SELECT c.fechaemision AS fechareg, ";
	$Sql .= " d.coddocumentoabono AS coddocumento, d.seriedocumentoabono AS serie, d.nrodocumentoabono AS nrodocumento, ";
	$Sql .= " cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
	$Sql .= " c.nroinscripcion, cl.propietario, ";
	$Sql .= " SUM(d.imprebajado) AS impmes, ";
	$Sql .= " SUM(d.imprebajado) - (SUM(d.imprebajado) / 1.18) AS impigv, ";
	$Sql .= " d.coddocumento AS coddocumento2, d.nrodocumento AS nrodocumento2, ";
	$Sql .= " d.seriedocumento AS seriedocumento2, c.glosa, c.estareg, ";
	$Sql .= " 0 AS noimp, ";
	$Sql .= " 0 AS redondeo, ";
	$Sql .= " SUM(d.imprebajado) / 1.18 AS imptot ";
	$Sql .= "FROM facturacion.cabrebajas c ";
	$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
	$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
	$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
	$Sql .= "WHERE c.codsuc = ".$codsuc." ";
	$Sql .= " AND c.codestrebaja = 1 ";
	//$Sql .= " AND c.anio = '".$anio."' ";
	//$Sql .= " AND c.mes = '".intval($mes)."' ";
	$Sql .= " AND c.fechaemision BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
	$Sql .= "GROUP BY c.fechaemision, d.coddocumentoabono, d.seriedocumentoabono, d.nrodocumentoabono, cl.codtipodocumento, cl.nrodocumento, ";
	$Sql .= " c.nroinscripcion, cl.propietario, d.coddocumento, d.nrodocumento, d.seriedocumento, c.glosa, c.estareg ";
	$Sql .= "ORDER BY c.fechaemision ";


}
else
{
	$Sql = "SELECT c.fechaemision AS fechareg, ";
	$Sql .= " d.coddocumentoabono AS coddocumento, d.seriedocumentoabono AS serie, d.nrodocumentoabono AS nrodocumento, ";
	$Sql .= " cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
	$Sql .= " c.nroinscripcion, cl.propietario, ";
	$Sql .= " SUM(d.imprebajado) AS impmes, ";
	$Sql .= " SUM(CASE WHEN d.codconcepto = 5 THEN d.imprebajado ELSE 0 END) AS impigv, ";
	$Sql .= " d.coddocumento AS coddocumento2, d.nrodocumento AS nrodocumento2, ";
	$Sql .= " d.seriedocumento AS seriedocumento2, c.glosa, c.estareg, ";
	$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (5, 7, 8) THEN d.imprebajado ELSE 0 END) AS noimp, ";
	$Sql .= " SUM(CASE WHEN d.codconcepto IN (7, 8) THEN d.imprebajado ELSE 0 END) AS redondeo, ";
	$Sql .= " SUM(d.imporiginal) AS imptot ";
	$Sql .= "FROM facturacion.cabrebajas c ";
	$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
	$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
	$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
	$Sql .= "WHERE c.codsuc = ".$codsuc." ";
	$Sql .= " AND c.codestrebaja = 1 ";
	//$Sql .= " AND c.anio = '".$anio."' ";
	//$Sql .= " AND c.mes = '".intval($mes)."' ";
	$Sql .= " AND c.fechaemision BETWEEN '{$primerdia}' AND '{$ultimodia}' ";
	$Sql .= "GROUP BY c.fechaemision, d.coddocumentoabono, d.seriedocumentoabono, d.nrodocumentoabono, cl.codtipodocumento, cl.nrodocumento, ";
	$Sql .= " c.nroinscripcion, cl.propietario, d.coddocumento, d.nrodocumento, d.seriedocumento, c.glosa, c.estareg ";
	$Sql .= "ORDER BY c.fechaemision ";
}

$consulta = $conexion->prepare($Sql);
$consulta->execute();
$items = $consulta->fetchAll();

echo "<pre>"; var_dump($items); echo "</pre>"; exit;

$borde = $tvVenta = $tnImponible = $tigv = $tredondeo = 0;

foreach($items as $row):

	$count++;

	if($row['estareg'] == 0):

		$row['noimp']    = 0;
		$row['impigv']   = 0;
		$row['redondeo'] = 0;
		$row['impmes']   = 0;
		$row["glosa"]    = $row["glosa"]." ANULADO";

	endif;
?>

<tr>
		<td align="center" width="8%"><?=$count;?></td>
		<td align="center" width="8%"><?='N '.$row["serie"].' '.$row["nrodocumento"]?></td>
		<td align="center" width="8%"><?=$clsFunciones->DecFecha($row["fechareg"]) ?></td>
		<td align="left" width="40%"><?=strtoupper(utf8_decode($row["glosa"]))?></td>
		<td align="left" width="60%"><?=strtoupper(utf8_decode($row["propietario"]))?></td>
		<td align="right" width="15%"><?=number_format($row['imptot'], 2)?></td>
		<td align="right" width="15%"><?=number_format($row['noimp'], 2)?></td>
		<td align="right" width="15%"><?=number_format($row['impigv'], 2)?></td>
		<td align="right" width="15%"><?=number_format($row['redondeo'], 2)?></td>
		<td align="right" width="20%"><?=number_format($row['impmes'], 2)?></td>
		<td align="center" width="20%"><?=str_pad($row['seriedocumento2'], 3, "0", STR_PAD_LEFT)." - ".$row['nrodocumento2']?></td>
</tr>

<?php

		$tvVenta     += $row['imptot'];
		$tnImponible += $row["noimp"];
		$tigv        += $row["impigv"];
		$tredondeo   += $row["redondeo"];
		$imptotal    += $row["impmes"];

	endforeach;

?>

<tr style="background-color:#93A2BF; color:#FFFFFF; font-weight: bold ">
		<td align="center" colspan="5" >TOTAL PARCIAL ===></td>
		<td align="right"><?=number_format($tvVenta,2)?></td>
		<td align="right"><?=number_format($tnImponible,2)?></td>
		<td align="right"><?=number_format($tigv,2)?></td>
		<td align="right"><?=number_format($tredondeo,2)?></td>
		<td align="right"><?=number_format($imptotal,2)?></td>
		<td align="right">&nbsp;</td>
</tr>

<?php
	$TotalF= $imptotal;
?>
<tr><td colspan="11">&nbsp;</td></tr>
<tr><td colspan="11">&nbsp;</td></tr>
<tr><td colspan="11">&nbsp;</td></tr>
<tr><td colspan="11">&nbsp;</td></tr>
<tr><td colspan="11">&nbsp;</td></tr>
<tr>
	<td>&nbsp;</td>
	<td colspan="9">
		<table class="ui-widget" border="1" cellspacing="0" id="TbConsulta" width="100%" rules="rows">
		    <thead class="ui-widget-header" style="font-size:10px">
		    	<tr title="Cabecera">
		        <th scope="col" class="ui-widget" colspan="3" align="center" class="ui-widget-header">RESUMEN DE ABONOS</th>
		    	</tr>
					<tr title="Cabecera">
		        <th scope="col" class="ui-widget" align="center" class="ui-widget-header">&nbsp;</th>
		        <th scope="col" class="ui-widget" align="center" class="ui-widget-header">MONTO INICIAL</th>
		        <th scope="col" class="ui-widget" align="center" class="ui-widget-header">CANTIDAD</th>
		    	</tr>
		    </thead>
		    <tbody style="font-size:10px">
					<?php
						//TOTALES
						if ($anio == 2016 && intval($mes) <= 2) :

							$Sql = "SELECT ";
							$Sql .= " SUM(CASE WHEN d.codconcepto = 1 THEN d.imprebajado ELSE 0 END) AS agua, ";
							$Sql .= " SUM(CASE WHEN d.codconcepto = 2 THEN d.imprebajado ELSE 0 END) AS desague, ";
							$Sql .= " SUM(d.imprebajado) - (SUM(d.imprebajado) / 1.18) AS igv, ";
							$Sql .= " 0 AS redondeo, ";
							$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (1, 2, 5, 7, 8) THEN d.imprebajado ELSE 0 END) AS otros ";
							$Sql .= "FROM facturacion.cabrebajas c ";
							$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
							$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
							$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
							$Sql .= "WHERE c.codsuc = ".$codsuc." ";
							$Sql .= " AND c.estareg = 1 ";
							//$Sql .= " AND c.anio = '".$anio."' ";
							//$Sql .= " AND c.mes = '".intval($mes)."' ";
							$Sql .= " AND c.fechaemision BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
							//$Sql .= "GROUP BY ";

							$c = $conexion->query($Sql)->fetch();

							$c['desague'] = $imptotal - ($c['agua'] + $c['otros'] + $c['igv'] + $c['redondeo']);

						else :

							$Sql = "SELECT ";
							$Sql .= " SUM(CASE WHEN d.codconcepto = 1 THEN d.imprebajado ELSE 0 END) AS agua, ";
							$Sql .= " SUM(CASE WHEN d.codconcepto = 2 THEN d.imprebajado ELSE 0 END) AS desague, ";
							$Sql .= " SUM(CASE WHEN d.codconcepto = 5 THEN d.imprebajado ELSE 0 END) AS igv, ";
							$Sql .= " SUM(CASE WHEN d.codconcepto IN (7, 8) THEN d.imprebajado  ELSE 0 END) AS redondeo, ";
							$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (1, 2, 5, 7, 8) THEN d.imprebajado ELSE 0 END) AS otros ";
							$Sql .= "FROM facturacion.cabrebajas c ";
							$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
							$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
							$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
							$Sql .= "WHERE c.codsuc = ".$codsuc." ";
							$Sql .= " AND c.estareg = 1 ";
							//$Sql .= " AND c.anio = '".$anio."' ";
							//$Sql .= " AND c.mes = '".intval($mes)."' ";
							$Sql .= " AND c.fechaemision BETWEEN '".$primerdia."' AND '".$ultimodia."' ";

							$c = $conexion->query($Sql)->fetch();

						endif;
					?>
					<tr>
						<td align="left">1. Agua</td>
						<td align="right"><?=number_format($c['agua'],2)?></td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="left">2. Desague</td>
						<td align="right"><?=number_format($c['desague'],2)?></td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="left">3. Otros Conceptos</td>
						<td align="right"><?=number_format($c['otros'],2)?></td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="left">4. I.G.V.</td>
						<td align="right"><?=number_format($c['igv'],2)?></td>
						<td align="center">&nbsp;</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td align="left">TOTAL DE DETALLE</td>
						<td align="right"><?=number_format($c['agua']+$c['desague']+$c['otros']+$c['igv'],2)?></td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="left">REDONDEO</td>
						<td align="right"><?=number_format($c['redondeo'],2)?></td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td align="left">TOTAL</td>
						<td align="right"><?=number_format($c['agua']+$c['desague']+$c['otros']+$c['igv']+$c['redondeo'],2)?></td>
						<td align="center"><?=$count;?></td>
					</tr>
				</tfoot>
			</table>
	</td>
	<td>&nbsp;</td>
</tr>
