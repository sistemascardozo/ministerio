<?php
	set_time_limit(0);

	include("../../../../objetos/clsReporte.php");

	class clsRegistroVentas extends clsReporte
	{
		function cabecera()
		{
			global $m,$anio,$ciclotext,$Dim;

			$this->SetFont('Arial','B',14);
			$this->SetY(15);
			$tit1 = "REPORTE DE ABONOS";
			$this->Cell(277,$h+2,utf8_decode($tit1),0,1,'C');

            $this->Ln(5);

			$h=4;
			$this->SetFont('Arial','',7);
			$this->Cell(277,$h,$ciclotext,0,1,'C');

			$this->SetX(113.5);
			$this->Cell(10, $h,"MES",0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(30, $h,$m,0,0,'L');
			$this->Cell(10, $h,utf8_decode("AÑO"),0,0,'R');
			$this->Cell(5, $h,":",0,0,'C');
			$this->Cell(20, $h,$anio,0,1,'L');

			$this->Ln(2);
			$this->Ln(1);
			$this->SetFont('Arial','',6);


			$this->Cell($Dim[1],$h,"NRO. DCTO.",'1',0,'C');
			$this->Cell($Dim[2],$h,"FECHA",'1',0,'C');
			$this->Cell($Dim[3],$h,"GLOSA",'1',0,'C');
			$this->Cell($Dim[4],$h,"NOMBRE",'1',0,'C');
			$this->Cell($Dim[5],$h,"VALOR DE VENTA",'1',0,'C');
			$this->Cell($Dim[6],$h,"NO IMPONIBLE",'1',0,'C');
			$this->Cell($Dim[7],$h,"I.G.V.",'1',0,'C');
			$this->Cell($Dim[8],$h,"REDONDEO",'1',0,'C');
			$this->Cell($Dim[9],$h,"IMPORTE",'1',0,'C');
			$this->Cell($Dim[10],$h,"RECIBO",'1',1,'C');
		}

		function agregar_detalle($codsuc, $codciclo, $anio ,$mes, $Csec)
		{
			global $conexion, $Dim;

			if(intval($mes) < 10){$mes = "0".$mes;}

			$h = 4;
			$ultimodia = $this->obtener_ultimo_dia_mes($mes, $anio);
			$primerdia = $this->obtener_primer_dia_mes($mes, $anio);
			$count = 0;

			$impmes 	= 0;
			$impigv		= 0;
			$imptotal	= 0;

			$this->SetTextColor(0, 0, 0);

			$sql = "SELECT nrofacturacion, fechaemision ";
			$sql .= "FROM facturacion.periodofacturacion ";
			$sql .= "WHERE codemp = 1 ";
			$sql .= " AND codsuc = ? ";
			$sql .= " AND codciclo = ? ";
			$sql .= " AND anio = ? ";
			$sql .= " AND mes = ?";

			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codsuc, $codciclo, $anio, $mes));
			$row = $consulta->fetch();

			$nrofacturacion = $row['nrofacturacion'];

			//REBAJAS
			
			if ($anio == 2016 && intval($mes) <= 2)
			{
				$Sql = "SELECT c.fechaemision AS fechareg, ";
				$Sql .= " d.coddocumentoabono AS coddocumento, d.seriedocumentoabono AS serie, d.nrodocumentoabono AS nrodocumento, ";
				$Sql .= " cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
				$Sql .= " c.nroinscripcion, cl.propietario, ";
				$Sql .= " SUM(d.imprebajado) AS impmes, ";
				$Sql .= " SUM(d.imprebajado) - (SUM(d.imprebajado) / 1.18) AS impigv, ";
				$Sql .= " d.coddocumento AS coddocumento2, d.nrodocumento AS nrodocumento2, ";
				$Sql .= " d.seriedocumento AS seriedocumento2, c.glosa, c.estareg, ";
				$Sql .= " 0 AS noimp, ";
				$Sql .= " 0 AS redondeo, ";
				$Sql .= " SUM(d.imprebajado) / 1.18 AS imptot ";
				$Sql .= "FROM facturacion.cabrebajas c ";
				$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
				$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
				$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
				$Sql .= "WHERE c.codsuc = ".$codsuc." ";
				$Sql .= " AND c.codestrebaja = 1 ";
				//$Sql .= " AND c.anio = '".$anio."' ";
				//$Sql .= " AND c.mes = '".intval($mes)."' ";
				$Sql .= " AND c.fechaemision BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
				$Sql .= "GROUP BY c.fechaemision, d.coddocumentoabono, d.seriedocumentoabono, d.nrodocumentoabono, cl.codtipodocumento, cl.nrodocumento, ";
				$Sql .= " c.nroinscripcion, cl.propietario, d.coddocumento, d.nrodocumento, d.seriedocumento, c.glosa, c.estareg ";
				$Sql .= "ORDER BY c.fechaemision ";
			}
			else
			{


				$Sql = "SELECT c.fechaemision AS fechareg, ";
				$Sql .= " d.coddocumentoabono AS coddocumento, d.seriedocumentoabono AS serie, d.nrodocumentoabono AS nrodocumento, ";
				$Sql .= " cl.codtipodocumento, cl.nrodocumento AS nrodoc, ";
				$Sql .= " c.nroinscripcion, cl.propietario, ";
				$Sql .= " SUM(d.imprebajado) AS impmes, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto = 5 THEN d.imprebajado ELSE 0 END) AS impigv, ";
				$Sql .= " d.coddocumento AS coddocumento2, d.nrodocumento AS nrodocumento2, ";
				$Sql .= " d.seriedocumento AS seriedocumento2, c.glosa, c.estareg, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (5, 7, 8) THEN d.imprebajado ELSE 0 END) AS noimp, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto IN (7, 8) THEN d.imprebajado ELSE 0 END) AS redondeo, ";
				$Sql .= " SUM(d.imporiginal) AS imptot ";
				$Sql .= "FROM facturacion.cabrebajas c ";
				$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
				$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
				$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
				$Sql .= "WHERE c.codsuc = ".$codsuc." ";
				$Sql .= " AND c.codestrebaja = 1 ";
				//$Sql .= " AND c.anio = '".$anio."' ";
				//$Sql .= " AND c.mes = '".intval($mes)."' ";
				$Sql .= " AND c.fechaemision BETWEEN '{$primerdia}' AND '{$ultimodia}' ";
				$Sql .= "GROUP BY c.fechaemision, d.coddocumentoabono, d.seriedocumentoabono, d.nrodocumentoabono, cl.codtipodocumento, cl.nrodocumento, ";
				$Sql .= " c.nroinscripcion, cl.propietario, d.coddocumento, d.nrodocumento, d.seriedocumento, c.glosa, c.estareg ";
				$Sql .= "ORDER BY c.fechaemision ";

		 }

			$consulta = $conexion->prepare($Sql);
			$consulta->execute();
			$items = $consulta->fetchAll();

			$borde = 0;
			$tvVenta = 0;
			$tnImponible = 0;
			$tigv = 0;
			$tredondeo = 0;

			//var_dump($consulta->errorInfo());
			$this->SetFillColor(255, 255, 255); //Color de Fondo
			$this->SetTextColor(0);

			foreach($items as $row)
			{
				$count++;

				if($row['estareg'] == 0)
				{
					$row['noimp']	= 0;
					$row['impigv']	= 0;
					$row['redondeo'] = 0;
					$row['impmes']	= 0;
					$row["glosa"]	= $row["glosa"]." ANULADO";
				}

				$this->Cell($Dim[1], $h, 'N '.$row["serie"].' '.$row["nrodocumento"], $borde, 0, 'C', true);
				$this->Cell($Dim[2], $h, $this->DecFecha($row["fechareg"]), $borde, 0, 'C', true);
				$this->Cell($Dim[3], $h, strtoupper(utf8_decode($row["glosa"])), $borde, 0, 'L', true);
				$this->Cell($Dim[4], $h, strtoupper(utf8_decode($row["propietario"])), $borde, 0, 'L', true);
				$this->Cell($Dim[5], $h, number_format($row['imptot'], 2), $borde, 0, 'R', true);
				$this->Cell($Dim[6], $h, number_format($row['noimp'], 2), $borde, 0, 'R', true);
				$this->Cell($Dim[7], $h, number_format($row['impigv'], 2), $borde1, 0, 'R', true);
				$this->Cell($Dim[8], $h, number_format($row['redondeo'], 2), $borde, 0, 'R', true);
				$this->Cell($Dim[9], $h, number_format($row['impmes'], 2), $borde, 0, 'R', true);
				$this->Cell($Dim[10], $h, str_pad($row['seriedocumento2'], 3, "0", STR_PAD_LEFT)." - ".$row['nrodocumento2'], $borde, 0, 'C', true);

				$tvVenta	+= $row['imptot'];
				$tnImponible += $row["noimp"];
				$tigv		+= $row["impigv"];
				$tredondeo	+= $row["redondeo"];
				$imptotal	+= $row["impmes"];

				$this->Ln($h);

			}

			//$imptotal += ($impmes+$impigv);
			$this->Ln(3);

			$this->SetFont('Arial','B',6);
			$this->SetTextColor(230,0,0);


			$this->Cell($Dim[1] + $Dim[2] + $Dim[3] + $Dim[4], $h,"Total General ==>",0,0,'R');
			$this->Cell($Dim[5], $h, number_format($tvVenta, 2),'T',0,'R');
			$this->Cell($Dim[6], $h, number_format($tnImponible, 2),'T',0,'R');
			$this->Cell($Dim[7], $h, number_format($tigv, 2),'T',0,'R');
			$this->Cell($Dim[8], $h, number_format($tredondeo, 2),'T',0,'R');
			$this->Cell($Dim[9], $h, number_format($imptotal, 2),'T',0,'R');

			$this->SetTextColor(0,0,0);
			$TotalF= $imptotal;


			$this->Cell(0 ,0.01,"",1,1,'C');
			//$imptotal += ($impmes+$impigv);
			$this->AddPage("L");
			$this->Ln(3);

			$this->SetFont('Arial','B',10);
			$this->Cell(0 ,$h,"RESUMEN DE ABONOS",0,1,'C');
			$this->Ln(3);
			$this->SetFont('Arial','B',8);
			$this->SetX(130);
			$this->Cell(30,$h,"MONTO INICIAL",'B',0,'C');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,"CANTIDAD",'B',1,'C');

			//TOTALES
			if ($anio == 2016 && intval($mes) <= 2)
			{
				$Sql = "SELECT ";
				$Sql .= " SUM(CASE WHEN d.codconcepto = 1 THEN d.imprebajado ELSE 0 END) AS agua, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto = 2 THEN d.imprebajado ELSE 0 END) AS desague, ";
				$Sql .= " SUM(d.imprebajado) - (SUM(d.imprebajado) / 1.18) AS igv, ";
				$Sql .= " 0 AS redondeo, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (1, 2, 5, 7, 8) THEN d.imprebajado ELSE 0 END) AS otros ";
				$Sql .= "FROM facturacion.cabrebajas c ";
				$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
				$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
				$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
				$Sql .= "WHERE c.codsuc = ".$codsuc." ";
				$Sql .= " AND c.estareg = 1 ";
				//$Sql .= " AND c.anio = '".$anio."' ";
				//$Sql .= " AND c.mes = '".intval($mes)."' ";
				$Sql .= " AND c.fechaemision BETWEEN '".$primerdia."' AND '".$ultimodia."' ";
				//$Sql .= "GROUP BY ";

				$c = $conexion->query($Sql)->fetch();

				$c['desague'] = $imptotal - ($c['agua'] + $c['otros'] + $c['igv'] + $c['redondeo']);
			}
			else
			{
				$Sql = "SELECT ";
				$Sql .= " SUM(CASE WHEN d.codconcepto = 1 THEN d.imprebajado ELSE 0 END) AS agua, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto = 2 THEN d.imprebajado ELSE 0 END) AS desague, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto = 5 THEN d.imprebajado ELSE 0 END) AS igv, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto IN (7, 8) THEN d.imprebajado  ELSE 0 END) AS redondeo, ";
				$Sql .= " SUM(CASE WHEN d.codconcepto NOT IN (1, 2, 5, 7, 8) THEN d.imprebajado ELSE 0 END) AS otros ";
				$Sql .= "FROM facturacion.cabrebajas c ";
				$Sql .= " INNER JOIN facturacion.detrebajas d ON (c.codemp = d.codemp) AND (c.codsuc = d.codsuc) AND (c.nrorebaja = d.nrorebaja) ";
				$Sql .= " INNER JOIN facturacion.conceptos co ON (d.codemp = co.codemp) AND (d.codsuc = co.codsuc) AND (d.codconcepto = co.codconcepto) ";
				$Sql .= " INNER JOIN catastro.clientes cl ON (c.codemp = cl.codemp) AND (c.codsuc = cl.codsuc) AND (c.nroinscripcion = cl.nroinscripcion) ";
				$Sql .= "WHERE c.codsuc = ".$codsuc." ";
				$Sql .= " AND c.estareg = 1 ";
				//$Sql .= " AND c.anio = '".$anio."' ";
				//$Sql .= " AND c.mes = '".intval($mes)."' ";
				$Sql .= " AND c.fechaemision BETWEEN '".$primerdia."' AND '".$ultimodia."' ";

				$c = $conexion->query($Sql)->fetch();
			}

			$this->Ln(3);
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"1. Agua",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($c['agua'],2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');
			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"2. Desague",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($c['desague'],2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"3. Otros Conceptos",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($c['otros'],2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"4. I.G.V.",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($c['igv'],2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');


			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"",0,0,'L');
			$this->Cell(30,$h,'','B',0,'R');$this->Cell(5,$h,"",0,0,'C');

			$this->Cell(30,$h,'','B',1,'R');

			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total del Detalle ",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($c['agua']+$c['desague']+$c['otros']+$c['igv'],2),0,1,'R');



			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Redondeo",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($c['redondeo'],2),0,0,'R');$this->Cell(5,$h,"",0,1,'C');


			$this->SetX(70);
			$this->SetFont('Arial','B',8);
			$this->Cell(60,$h,"Total",0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(30,$h,number_format($c['agua']+$c['desague']+$c['otros']+$c['igv']+$c['redondeo'],2),0,0,'R');$this->Cell(5,$h,"",0,0,'C');
			$this->Cell(30,$h,$count,0,1,'R');



		}

		function FechaDoc($cod,$serie,$numero)
		{
			global $conexion,$codsuc;
			$Sql ="SELECT fechareg FROM facturacion.cabfacturacion
					WHERE codsuc=".$codsuc." AND coddocumento=".$cod."
					AND serie='".$serie."' AND nrodocumento='".$numero."'";
			$c = $conexion->query($Sql)->fetch();
			return $this->DecFecha($c[0]);
		}
		function EquivalenteDoc($cod,$tipo)
		{
			if($tipo==1)
			{
				switch ($cod)
				{
					case 4: return "14";break;
					case 13: return "03";break;
					case 14: return "01";break;
					case 17: return "07";break;
				}
			}
			else
			{
				switch ($cod)
				{
					case 1: return "1";break;
					case 3: return "6";break;
					default: return "0";break;
				}
			}
			/*
			COMPROBANTES
			-----------------------------
			14  Recibos de Pensiones
			01  Facturas
			03  Boletas de Ventas
			07  Notas de Abono/Credito
			08  Notas de cargo/Debito


			DOCUMENTOS
			-----------------------------
			1  DNI
			6  RUC
			0  Otros*/
		}

    }

	$ciclo		= $_GET["ciclo"];

	//$sector		= "%".$_GET["sector"]."%";
	$codsuc		= $_GET["codsuc"];
	$anio		= $_GET["anio"];
	$mes		= $_GET["mes"];
	$ciclotext	= $_GET["ciclotext"];

	if($_GET["sector"]!="%"){$Csec=" AND cl.codsector=".$_GET["sector"];}else{$Csec="";}

	$m = $meses[$mes];
	$Dim = array('1'=>15, '2'=>15, '3'=>65, '4'=>65, '5'=>20, '6'=>20, '7'=>10, '8'=>20, '9'=>20, '10'=>25);
/*	$fecha		= $_GET["fecha"];
	$car		= $_GET["car"];
	$caja		= $_GET["caja"];
	$cars		= $_GET["cars"];
	$cajeros 	= $_SESSION['nombre'];
	$nrocaja	= $_GET["nrocaja"];
	$cajero		= $_GET["cajero"];
	$codsuc		= $_GET["codsuc"];

	$totagua 		= 0;
	$totdesague		= 0;
	$totcargofijo	= 0;
	$totinteres		= 0;
	$totredondeo	= 0;
	$tototros		= 0;
	$totigv			= 0;
	$totalT			= 0;*/

	$objReporte	= new clsRegistroVentas();
	$objReporte->AliasNbPages();
	$objReporte->AddPage("L");

	$objReporte->agregar_detalle($codsuc, $ciclo, $anio, $mes, $Csec);


	$objReporte->Output();

?>
