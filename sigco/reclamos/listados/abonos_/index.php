<?php
	include("../../../../include/main.php");
	include("../../../../include/claseindex.php");

	$TituloVentana = "NOTAS DE ABONO";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];
	$objDrop 	= new clsDrop();
	
	$sucursal 		= $objDrop->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var codsuc = <?=$codsuc?>;
	
	var c = 0
	function ValidarForm()
	{
		var sectores = "";
		
		if($("#ciclo").val()==0)
		{
			Msj($("#ciclo"),'Seleccione el Ciclo')
			return false
		}
		if($("#anio").val()==0)
		{
			Msj($("#anio"),'Seleccione el A�o')
			return false
		}
		if($("#mes").val()==0)
		{
			Msj($("#mes"),'Seleccione el mes')
			return false
		}
		if($("#todosectores").val()==1)
		{
			sectores="%"
		}else{
			sectores=$("#sector").val()
			if(sectores=="")
			{
				Msj($("#sector"),"Seleccione el Sector para Generar el Listado de Corte")
				return false;
			}
		}		
		
		

		return true
	}	
	function Abonos()
	{
		if(ValidarForm())
		{
			url = "Abonos.php?ciclo="+$("#ciclo").val()+"&sector=%"+
			   "&codsuc=<?=$codsuc?>&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclotext="+$("#ciclo option:selected").text();
				AbrirPopupImpresion(url,800,600)
		}
	}
	function Alteraciones()
	{
		if(ValidarForm())
		{
			url = "Alteraciones.php?ciclo="+$("#ciclo").val()+"&sector=%"+
			   "&codsuc=<?=$codsuc?>&anio="+$("#anio").val()+"&mes="+$("#mes").val()+"&ciclotext="+$("#ciclo option:selected").text();
				AbrirPopupImpresion(url,800,600)
		}
	}


	function Cancelar()
	{
		location.href = '<?php echo $_SESSION['urldir'];?>/admin/indexB.php'
	}
	function quitar_disabled(obj,input)
	{
		if(obj.checked)
		{
			$("#"+input).attr("disabled",true)
		}else{
			$("#"+input).attr("disabled",false)
		}
	}
	function cargar_anio_drop(obj)
	{
		$.ajax({
			 url:'../../../../ajax/anio_drop.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&codciclo='+obj+'&condicion=1',
			 success:function(datos){
				$("#div_anio").html(datos)
			 }
		}) 
	}
	function cargar_mes(ciclo,suc,anio)
	{
		$.ajax({
			 url:'../../../../ajax/mes_drop.php',
			 type:'POST',
			 async:true,
			 data:'codciclo='+ciclo+'&codsuc='+suc+'&anio='+anio,
			 success:function(datos){
				$("#div_meses").html(datos)
			 }
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
    <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
    <tr>
      <td colspan="2">
        <fieldset style="padding:4px">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          	<tr><td colspan="3"></td></tr>
            <tr>
              <td width="10%">Sucursal</td>
              <td width="3%" align="center">:</td>
              <td colspan="3"><label>
                <input type="text" name="sucursal" id="sucursal" value="<?=$sucursal["descripcion"]?>" class="inputtext" readonly="readonly" />
                </label></td>
              </tr>
            <tr>
              <td>Ciclo</td>
              <td align="center">:</td>
              <td width="35%">
                <? $objDrop->drop_ciclos($codsuc,0,"onchange='cargar_anio_drop(this.value);'"); ?>
                </td>
              <td width="3%" align="center">&nbsp;</td>
              <td width="49%">&nbsp;</td>
              </tr>
            <tr>
              <td>A&ntilde;o</td>
              <td align="center">:</td>
              <td>
              	<div id="div_anio">
                	<? $objDrop->drop_anio($codsuc,0); ?>
                </div>
              </td>
              <td align="center">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            
            <tr>
              <td>Meses</td>
              <td align="center">:</td>
              <td>
              	<div id="div_meses">
                	<? $objDrop->drop_mes($codsuc,0,0); ?>
                </div>
              </td>
              <td align="center">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr style="display:none">
              <td>Sector</td>
              <td align="center">:</td>
              <td>
                <?php echo $objDrop->drop_sectores($codsuc,0,""); ?>
                </td>
              <td align="center">
                <input type="checkbox" name="chksectores" id="chksectores" value="checkbox" checked="checked" onclick="quitar_disabled(this,'sector');CambiarEstado(this,'todosectores')" />
                </td>
              <td>Todos los Sectores </td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="center">&nbsp;</td>
              <td><input type="hidden" name="todosectores" id="todosectores" value="1" /></td>
              <td align="center">&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td colspan="4" align="left"><div id="div_valida"></div></td>
              </tr>
    		<tr>
    			<td colspan='5' align="center">
    			
    			 <input type="button" onclick="Abonos();" value="Abonos" id="">
    			 <input type="button" onclick="Alteraciones();" value="Alteraciones" id="">
    			</td>
    		</tr>
        	<tr><td colspan="5"></td></tr>
            </table>
          </fieldset>
        </td>
    </tr>

    </tbody>

    </table>
 </form>
</div>
<script>
	$("#sector").attr("disabled",true)
</script>
<?php   CuerpoInferior();?>