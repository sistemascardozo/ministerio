<?php
	date_default_timezone_set("America/Lima");

	error_reporting(E_ERROR);
	//error_reporting(E_ALL | E_STRICT);

	set_time_limit(0);

	ini_set('memory_limit', '2048M');
	
	$Ubigeo = "110105";//"110107";//"110113"
	
	$schema = ".";
	
	$urldir = "http://".$_SERVER['HTTP_HOST']."/ministerio/".$Sede;
	$urldircom = "http://".$_SERVER['HTTP_HOST']."/ministerio/".$Sede;
	$path = $_SERVER['DOCUMENT_ROOT']."/ministerio/".$Sede;

	$StyloJQuery = 'jquery-ui-1.10.4.custom.min.css';

//Local
	$CriptF = '';
	$string = base64_decode('MzgzNDg3MW1jdHM=');

	for($i=0;$i<strlen($string);$i++){$char=substr($string,$i,1);$char=chr(ord($char));$CriptF.=$char;}

	$Servidor = "localhost";//"192.168.1.64";//
	$Puerto   = "5432"; //"5432";
	$Usuario  = "corp_demos"; //"postgres";
	$Password = "tarapoto147"; //"12345";$CriptF;//"MzgzNDg3MW1jdHM=";//"U2ljdWFuaUAyMDE2Kys=";//
	$Base     = "corp_antauta"; //"e-siincoweb_empssapal_st";

    try 
    {
        $conexion = new PDO("pgsql:dbname=$Base;host=$Servidor", $Usuario, $Password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
    } 
    catch (PDOException $e) 
    {
        echo utf8_decode('Fall&oacute; la conexi&oacute;n');
    }

	$numeros = $arrayName = array('1' => "uno", '2' => "dos", '3' => "tres", '4' => "cuatro");

	$meses = array(1 => "ENERO",
					2 => "FEBRERO",
					3 => "MARZO",
					4 => "ABRIL",
					5 => "MAYO",
					6 => "JUNIO",
					7 => "JULIO",
					8 => "AGOSTO",
					9 => "SETIEMBRE",
					10 => "OCTUBRE",
					11 => "NOVIEMBRE",
					12 => "DICIEMBRE");

	$meses_literal = array("01" => "ENERO",
							"02" => "FEBRERO",
							"03" => "MARZO",
							"04" => "ABRIL",
							"05" => "MAYO",
							"06" => "JUNIO",
							"07" => "JULIO",
							"08" => "AGOSTO",
							"09" => "SETIEMBRE",
							"10" => "OCTUBRE",
							"11" => "NOVIEMBRE",
							"12" => "DICIEMBRE");

	$tfacturacion = array(0 => "Consumo Leido",
							1 => "Consumo Promediado",
							2 => "Consumo Asignado");

	$categoria = array(0 => "FACTURACION",
						1 => "SALDO",
						2 => "SALDO REFINANCIADO",
						3 => "PRESTACION DE SERVICIOS EN CAJA");

	$tippago = array(0 => "CONTADO",
					1 => "CREDITO");

	$tiporeclamo = array("1" => "COMERCIALES CON FACTURACION",
						"2" => "COMERCIALES SIN FACTURACION",
						"3" => "OPERACIONALES");

	$subtiporeclamo = array("1A" => "1A=>Problemas en el regimen de facturacion y el nivel de consumo",
							"1B" => "1B=>Problemas en la tarifa aplicada al usuario",
							"1C" => "1C=>Problemas en otros conceptos facturados al usuario",
							"2A" => "2A=>Problemas relativos al acceso al servicio",
							"2B" => "2B=>Problemas relativos a la micromedicion",
							"2C" => "2C=>Problemas relativos a cortes indebidos",
							"2D" => "2D=>Falta de Entrega de Recibo",
							"2E" => "2E=>Problemas relativos a la informacion",
							"3A" => "3A=>Filtraciones",
							"3B" => "3B=>Problemas en el servicio de agua potable",
							"3C" => "3C=>Problemas en el servicio de alcantarillado");


	$consulta = $conexion->query("SELECT pg_backend_pid()")->fetch();
		
	$SqlV = "SELECT COUNT(*) FROM pg_proc WHERE proname = 'set_session'";
	$RowV = $conexion->query($SqlV)->fetch();
		
	if ($RowV[0] > 0)
	{
		$consulta = $conexion->query("SELECT admin.set_session('".$consulta[0]."', '".$_SESSION['user']."', '".$_SESSION["IP"]."')")->fetch();
	}
	else
	{
		include("generar_auditoria.php");	
	}
?>