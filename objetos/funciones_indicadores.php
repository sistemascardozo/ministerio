<?php
session_name("siincoweb");
header('Content-type: text/html; charset=utf-8');

include "clsFunciones.php";
class clsFunciones_Indicadores extends clsFunciones{

	function verificar_variable($codsuc,$anio,$mes,$codvariable){

		global $conexion;

		// Para servicio de agua
		$sqlLect1  = "SELECT * FROM indicadores.det_variables ";
		$sqlLect1 .= "WHERE codsuc = $codsuc AND codvariable = $codvariable ";
		$sqlLect1 .= " AND anio = '$anio' AND mes = '$mes' ";

		$consultaL = $conexion->query($sqlLect1);
		$items     = $consultaL->fetch();
		$cantidad  = $consultaL->rowCount();
		return $cantidad;
	}

	function monto_facturado($codsuc,$anio,$mes){

		global $conexion;

		// Para servicio de agua y desague
		$sqlLect1  = "SELECT SUM(importe) AS importe FROM facturacion.detctacorriente ";
		$sqlLect1 .= "WHERE codsuc = ".$codsuc." AND periodo = '".$anio.$mes."' ";
		$sqlLect1 .= " AND tipo = 0 AND tipoestructura = 0 ";
		$sqlLect1 .= " AND codconcepto IN(1,2) AND codtipodeuda <> 3 ";

		// die($sqlLect1);

		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['importe']) AND empty($items['importe'])) ? 0 : $items['importe'] ;
		return $importe;
  }

	function volumen_facturado($codemp,$codsuc,$ciclo,$anio,$mes){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();

		// Al no existir volumen como ejemplo se pone al agua nomas con su importe

		$sqlLect1   = "SELECT SUM(d.importe) AS consumo ";
		$sqlLect1  .= "FROM facturacion.cabctacorriente c ";
		$sqlLect1  .= " JOIN facturacion.detctacorriente d ON (d.codemp = d.codemp AND c.codsuc = d.codsuc AND c.codciclo = d.codciclo AND d.nrofacturacion = c.nrofacturacion AND c.nroinscripcion = d.nroinscripcion AND c.anio = d.anio AND c.mes = d.mes AND c.periodo = d.periodo AND c.tipo = d.tipo AND c.tipoestructura = d.tipoestructura ) ";
		$sqlLect1  .= "WHERE c.codsuc = $codsuc ";
		$sqlLect1  .= "	AND c.nrodocumento > 0 AND c.periodo='".$anio.$mes."' AND c.tipo = 0 AND d.codconcepto IN(1) ";
		$sqlLect1  .= "	AND c.tipoestructura  = 0 ";

		// die($sqlLect1);

		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		return $items['consumo'];
	}


	// Indicador: FACTURACIóN MEDIA
	// Para obtener la Variable: MONTO FACTURADO X AGUA POTABLE Y ALCANTARRILLADO(Domestica)
	function usuarios_conexiones_agua($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		// Para servicio de agua
		$sqlLect1   = "SELECT COUNT(*) AS cantidad ";
		$sqlLect1  .= "FROM facturacion.cabctacorriente ";
		$sqlLect1  .= "WHERE codemp = 1 AND codsuc = $codsuc AND codestadoservicio = 1 AND codtiposervicio IN(1,2) ";
		$sqlLect1  .= " AND nrofacturacion = $nrofacturacion AND periodo='".$anio.$mes."' AND tipo = 0 ";
		$sqlLect1  .= "	AND tipoestructura = 0 ";


		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['cantidad']) AND empty($items['cantidad'])) ? 0 : $items['cantidad'] ;
		return $importe;
  }

	function monto_facturado_agua_alcantarrillado($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		// Para servicio de agua
		$sqlLect1  = "SELECT SUM(importe) AS importe ";
		$sqlLect1 .= "FROM facturacion.detctacorriente ";
		$sqlLect1 .= "WHERE codsuc = ".$codsuc." AND nrofacturacion = $nrofacturacion AND periodo='".$anio.$mes."' AND tipo = 0 ";
		$sqlLect1 .= " AND tipoestructura = 0 AND codconcepto IN(1,2) ";

		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['importe']) AND empty($items['importe'])) ? 0 : $items['importe'] ;
		return $importe;
  }

	// Indicador: CONSUMO UNITARIO MEDIDO
	// No se puede generar aun el indicador por no contar diferencias de lecturas

	// Indicador: VOLUMEN FACTURADO UNITARIO
	// No se puede generar aun el indicador por no contar diferencias de lecturas

	// Indicador: COBERTURA DE AGUA POTABLE
	// Conexiones de Tipo de Servicio de agua
	function poblacion_conexiones_agua($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		// Para servicio de agua
		$sqlLect1   = "SELECT COUNT(*) AS cantidad ";
		$sqlLect1  .= "FROM facturacion.cabctacorriente ";
		$sqlLect1  .= "WHERE codemp = 1 AND codsuc = $codsuc AND codtiposervicio IN(1,2) ";
		$sqlLect1  .= " AND nrofacturacion = $nrofacturacion AND periodo='".$anio.$mes."' AND tipo = 0 ";
		$sqlLect1  .= "	AND tipoestructura = 0 ";


		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['cantidad']) AND empty($items['cantidad'])) ? 0 : $items['cantidad'] ;
		return $importe;
  }

	// Conexiones de Tipo de Servicio de agua
	function poblacion_conexiones_agua_pileta($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		// Para servicio de agua
		$sqlLect1   = "SELECT COUNT(c.*) AS cantidad ";
		$sqlLect1  .= "FROM facturacion.cabctacorriente c ";
		$sqlLect1  .= " JOIN catastro.clientes cl ON(c.codemp = cl.codemp AND c.codsuc = cl.codsuc AND c.nroinscripcion = cl.nroinscripcion) ";
		$sqlLect1  .= " JOIN catastro.conexiones ca ON(cl.codemp = ca.codemp AND cl.codsuc = ca.codsuc AND cl.nroinscripcion = ca.nroinscripcion) ";
		$sqlLect1  .= "WHERE c.codemp = 1 AND c.codsuc = $codsuc AND c.codestadoservicio = 1 AND c.codtiposervicio IN(1,2) ";
		$sqlLect1  .= " AND c.nrofacturacion = $nrofacturacion AND c.periodo='".$anio.$mes."' AND c.tipo = 0 ";
		$sqlLect1  .= "	AND c.tipoestructura = 0 AND ca.codtipoabastecimiento = 5 ";


		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['cantidad']) AND empty($items['cantidad'])) ? 0 : $items['cantidad'] ;
		return $importe;
  }

	// Indicador: COBERTURA DE ALCANTARILLADO
	// Conexiones de Tipo de Servicio de alcantarillado
	function poblacion_conexiones_alcantarrillado($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		// Para servicio de alcantarrillado
		$sqlLect1   = "SELECT COUNT(*) AS cantidad ";
		$sqlLect1  .= "FROM facturacion.cabctacorriente ";
		$sqlLect1  .= "WHERE codemp = 1 AND codsuc = $codsuc AND codtiposervicio IN(1,3) ";
		$sqlLect1  .= " AND nrofacturacion = $nrofacturacion AND periodo='".$anio.$mes."' AND tipo = 0 ";
		$sqlLect1  .= "	AND tipoestructura = 0 ";


		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['cantidad']) AND empty($items['cantidad'])) ? 0 : $items['cantidad'] ;
		return $importe;
  }

	// Indicador: AGUA NO FACTURADA
	// No se puede generar aun el indicador por no contar diferencias de lecturas

	// Indicador: MICROMEDICIÓN
	// No se puede generar aun el indicador por no contar diferencias de lecturas

	// Indicador: CONEXIONES ACTIVAS FACTURADAS POR MEDICIóN
	// No se puede generar aun el indicador por no contar diferencias de lecturas

	// Indicador: MOROSIDAD
	// No se puede generar aun el indicador por no contar diferencias de lecturas
	function cuentas_comerciales_cobrar($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		// Para saldos
		$sqlLect1   = "SELECT COALESCE(SUM(importe - (importeacta + importerebajado)) , 0) AS cantidad  ";
		$sqlLect1  .= "FROM facturacion.detctacorriente ";
		$sqlLect1  .= "WHERE codemp = 1 AND codsuc = $codsuc ";
		$sqlLect1  .= "	AND periodo='".$anio.$mes."' AND tipo = 1 AND tipoestructura = 0 AND codconcepto <> 101 ";

		// Tipo: sirve para poder saber si es como saldos


		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['cantidad']) AND empty($items['cantidad'])) ? 0 : $items['cantidad'] ;
		return $importe;
  }

	function ingresos_operativos($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '$anio' AND mes = '$mes' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		$ultimo_dia = $this->obtener_ultimo_dia_mes($mes, $anio);

		// Para servicio de agua
		$sqlLect1  = "SELECT COALESCE(SUM(importe), 0) AS importe ";
		$sqlLect1 .= "FROM cobranza.cabpagos c ";
		$sqlLect1 .= "	JOIN cobranza.detpagos d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nroinscripcion = d.nroinscripcion AND c.nropago = d.nropago) ";
		$sqlLect1 .= "WHERE c.anulado = 0 AND EXTRACT(YEAR FROM c.fechareg) = '$anio' AND c.fechareg <= '$ultimo_dia' ";

		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['importe']) AND empty($items['importe'])) ? 0 : $items['importe'] ;
		return $importe;
  }


	// Indicador: CONEXIONES ACTIVAS
	// No se puede generar aun el indicador por no contar diferencias de lecturas
	function conexiones_activas_agua($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		// Para servicio de agua
		$sqlLect1   = "SELECT COUNT(*) AS cantidad ";
		$sqlLect1  .= "FROM facturacion.cabctacorriente ";
		$sqlLect1  .= "WHERE codemp = 1 AND codsuc = $codsuc AND codestadoservicio = 1 AND codtiposervicio IN(1,2) ";
		$sqlLect1  .= " AND nrofacturacion = $nrofacturacion AND periodo='".$anio.$mes."' AND tipo = 0 ";
		$sqlLect1  .= "	AND tipoestructura = 0 ";


		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['cantidad']) AND empty($items['cantidad'])) ? 0 : $items['cantidad'] ;
		return $importe;
  }

	function conexiones_totales_agua($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '".$anio."' AND mes = '".$mes."' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		// Para servicio de agua
		$sqlLect1   = "SELECT COUNT(*) AS cantidad ";
		$sqlLect1  .= "FROM facturacion.cabctacorriente ";
		$sqlLect1  .= "WHERE codemp = 1 AND codsuc = $codsuc AND codestadoservicio NOT IN(0) ";
		$sqlLect1  .= " AND nrofacturacion = $nrofacturacion AND periodo='".$anio.$mes."' AND tipo = 0 ";
		$sqlLect1  .= "	AND tipoestructura = 0 ";


		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['cantidad']) AND empty($items['cantidad'])) ? 0 : $items['cantidad'] ;
		return $importe;
  }

	// Indicador: MARGEN OPERATIVA
	// No se puede generar aun el indicador por no contar diferencias de lecturas
	function ingresos_operativos_totales($codemp, $codsuc, $anio, $mes, $ciclo){

		global $conexion;

		// Para obtener el periodo de faturacion
		$sqlLect  = "SELECT nrofacturacion ";
		$sqlLect .= "FROM facturacion.periodofacturacion ";
		$sqlLect .= "WHERE codemp = $codemp AND codsuc = $codsuc AND codciclo = $ciclo ";
		$sqlLect .= " AND anio = '$anio' AND mes = '$mes' ";

		$consultaL = $conexion->prepare($sqlLect);
		$consultaL->execute();
		$nrofact = $consultaL->fetch();
		$nrofacturacion = $nrofact['nrofacturacion'];

		$ultimo_dia = $this->obtener_ultimo_dia_mes($mes, $anio);

		// Para servicio de agua
		$sqlLect1  = "SELECT COALESCE(SUM(importe), 0) AS importe ";
		$sqlLect1 .= "FROM cobranza.cabpagos c ";
		$sqlLect1 .= "	JOIN cobranza.detpagos d ON (c.codemp = d.codemp AND c.codsuc = d.codsuc AND c.nroinscripcion = d.nroinscripcion AND c.nropago = d.nropago) ";
		$sqlLect1 .= "WHERE c.anulado = 0 AND EXTRACT(YEAR FROM c.fechareg) = '$anio' AND c.fechareg <= '$ultimo_dia' ";

		$consultaL = $conexion->query($sqlLect1);
		$items = $consultaL->fetch();
		$importe = (!isset($items['importe']) AND empty($items['importe'])) ? 0 : $items['importe'] ;
		return $importe;
  }

}

?>
