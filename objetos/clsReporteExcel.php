<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	function CabeceraExcel($ColImg, $ColData)
	{
		global $urldir, $objReporte, $codsuc, $StyloJQuery;
	
		$Aempresa	= $objReporte->datos_empresa($codsuc);
		$Empresa	= $Aempresa["razonsocial"];
		$Sucursal	= $Aempresa["descripcion"];
		$Direccion	= $Aempresa["direccion"];
?>
<link href="<?php echo $_SESSION['urldir'];?>css/<?=$StyloJQuery?>" rel="stylesheet" type="text/css"  />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table class="ui-widget" border="0" cellspacing="0" id="TbCabecera" width="100%" rules="rows">
    <thead class="ui-widget-header" style="font-size:14px">
      <tr title="Cabecera">
        <th rowspan="5" align="center" colspan="<?=$ColImg?>">&nbsp;&nbsp;
          <img src="<?php echo $_SESSION['urldir'];?>images/logo_empresa.jpg" width="104" height="81"/>
        </th>
        <th scope="col" colspan="<?=$ColData?>" align="left" ><?=($Empresa)?></th>
      </tr>
      <tr title="Cabecera">
        <th scope="col" colspan="<?=$ColData?>" align="left" ><?=($Direccion)?></th>
      </tr>
      <tr title="Cabecera">
        <th scope="col" colspan="<?=$ColData?>" align="left" >SUCURSAL : <?=$Sucursal?></th>
      </tr>
     
        </thead>
    </table>
<?php
	}
?>