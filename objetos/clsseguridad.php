<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include($_SESSION['path']."config.php");
	
	class clsSeguridad
	{
		function encriptar($texto)
		{
			$result = '';
			
			for($i = 0; $i < strlen($texto); $i++)
			{
				$char = substr($texto, $i, 1);
				$char = chr(ord($char));
				
				$result .= $char;
			}
			
			return base64_encode($result);
		}
		function desencriptar($texto)
		{
			$result = '';
			$string = base64_decode($texto);
			
			for($i = 0; $i < strlen($string); $i++)
			{
				$char = substr($string, $i, 1);
				$char = chr(ord($char));
				
				$result .= $char;
			}
			
			return $result;
		}
		
		function getRealIP() 
		{
	        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) { 
			   $ips = $_SERVER['HTTP_X_FORWARDED_FOR']; 
			}  
			elseif (isset($_SERVER['HTTP_VIA'])) { 
			   $ips = $_SERVER['HTTP_VIA']; 
			}  
			elseif (isset($_SERVER['REMOTE_ADDR'])) { 
			   $ips = $_SERVER['REMOTE_ADDR']; 
			} 
			else {  
			   $ips = "unknown"; 
			} 
	       
	        return $ips;
	    }
	    function HostName()
	    {
	    	return $HostName = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	    }
		
		function seguimiento($codemp, $codsuc, $codsistema, $codusuario, $comentario, $ip, $codmodulo = 0)
		{
			global $_SESSION, $conexion;
			
			$ip = $_SESSION["IP"];//$this->getRealIP();
			$HostName = $_SESSION["HostName"];//$this->HostName();
			
			$sql = "SELECT * FROM seguridad.f_seguimiento(?, ?, ?, ?, ?, ?, ?, ?)";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($codemp, $codsuc, $codsistema, $codusuario, $comentario, $codmodulo, $ip, $HostName));
			
			return $consulta->fetch();
			return true;
		}		
	}
	//$objReporte	= new clsSeguridad();
	//echo $objReporte->desencriptar('MjQ5OTA0MDU=');
?>