<?php

include($_SESSION['path']."objetos/clsSql.php");

header('Content-type: text/html; charset=utf-8');

class clsFunciones extends clsSql {

    function dias_transcurridos($fecha_i, $fecha_f) {
        $dias = (strtotime($fecha_i) - strtotime($fecha_f)) / 86400;
        $dias = abs($dias);
        $dias = floor($dias);

        return $dias;
    }

    function obtener_ultimo_dia_mes($mes, $anno) {
        $day = date("d", mktime(0, 0, 0, $mes + 1, 0, $anno));

        return date('Y-m-d', mktime(0, 0, 0, $mes, $day, $anno));
    }

    function obtener_primer_dia_mes($mes, $anno) {
        return date('Y-m-d', mktime(0, 0, 0, $mes, 1, $anno));
    }

    function obtener_nombre_mes($mes) {
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        return $meses[$mes - 1];
    }

    function FechaServer() {
        return date('d/m/Y');
    }

    function HoraServidor() {
        return date('H:i');
    }

    function obtenerpromedio($codsuc, $nroinscripcion, $tope = '6', $mes) {
        global $conexion;

        $limit = "limit ".$top;
        $top = '';

        if ($mes < 10) {
            $mes = (int) $mes;
        }

        $sqlPromedio = "SELECT * FROM facturacion.promedio_lectura_dig(?, ?, ?, ?); ";
        $result = $conexion->prepare($sqlPromedio);
        $result->execute(array($codsuc, $nroinscripcion, $tope, $mes));

        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error select  consumo";
            die();
        }

        $itemsPromedio = $result->fetch();

        return $itemsPromedio[0];
    }

    function datosfacturacion($codsuc, $codciclo) {
        global $conexion;

        $SqlF = "SELECT nrofacturacion, anio, mes, lecturas, saldo, facturacion, tasainteres, nrotarifa ";
        $SqlF .= "FROM facturacion.periodofacturacion ";
        $SqlF .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND facturacion = 0 AND codciclo = ".$codciclo.";";

        $consulta = $conexion->prepare($SqlF);
        $consulta->execute(array());

        return $consulta->fetch();
    }

    function DecFecha($Fec) {
        // ereg("([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $Fec, $mifecha);
        preg_match("/^([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})$/", $Fec, $mifecha);
        $Fecha = $mifecha[3]."/".$mifecha[2]."/".$mifecha[1];

        return $Fecha;
    }

    function DecFecha1($Fec) {
        // ereg("([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $Fec, $mifecha);
        preg_match("/^([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})$/", $Fec, $mifecha);
        $Fecha = $mifecha[3]."/".$mifecha[2]."/".substr($mifecha[1], 2, 2);

        return $Fecha;
    }

    function CodFecha($Fec) {
        if ($Fec == "") {
            return $Fec;
        }

        $mifecha = split('[/.-]', $Fec);
        $Fecha = $mifecha[2]."-".$mifecha[1]."-".$mifecha[0];

        return $Fecha;
    }

    function DecFechaLiteral($fechaentrada = 0) {
        global $meses_literal;

        if ($fechaentrada != 0) {
            $Fecha = $fechaentrada;
        } else {
            $Fecha = $this->FechaServer();
        }

        ereg("([0-9]{2,4})/([0-9]{1,2})/([0-9]{1,4})", $Fecha, $mifecha);

        return array("dia" => $mifecha[1], "mes" => $meses_literal[$mifecha[2]], "anio" => $mifecha[3], "mes_num" => $mifecha[2]);
    }

    function preparesql($campos, $tabla, $valor, $inicio, $fin, $orden, $parametro = "", $and = "", $separador = "") {
        global $conexion;

        $value = array();
        $sql = $this->Sentencia($tabla, $and, $separador);
        if ($valor != "") {
            foreach ($campos as $val) {
                $value[$val] = "upper(CONVERT(char,".$val.")) like '%".$valor."%'";
            }

            $sql .= " WHERE (".implode(" or ", $value).") ";

            $valor = "%".$valor."%";
        }
        $sql .= $parametro.$orden;
        //echo $sql;
        //----Recupera cuantos registros fueron filtrados-------
        $n = $conexion->query($sql)->rowCount();
        //$consulta->execute(array(":valor"=>$valor));
        //$n = $consulta->rowCount();
        //----Generamos la sentencia para generar el listado-------
        //$sql .= " limit ".$inicio." offset ".$fin;
        //echo $sql;

        $items = $conexion->query($sql)->fetchAll();
        //print_r($items);
        //var_dump($consulta1->errorInfo());
        return array(1 => $n, 2 => $items);
    }

    function setCorrelativos($tabla, $par1 = 0, $par2 = 0) {//suc,sector
        global $conexion;

        $consulta = $conexion->prepare("SELECT * FROM public.f_setcorrelativostable(?, ?, ?)");
        $consulta->execute(array($tabla, $par1, $par2));

        return $consulta->fetch();
    }

    function setUbigeo($WHERE, $id) {
        global $conexion;

        $sql = $this->Sentencia("public.ubigeo").$WHERE;
        $consulta = $conexion->prepare($sql);
        $consulta->execute($id);
        $items = $consulta->fetch();

        return $items["descripcion"];
    }

    function setCorrelativosVarios($coddocumento, $codsuc, $accion, $correlativo) {
        global $conexion;

        $consulta = $conexion->prepare("SELECT * FROM public.f_correlativosdocumentos('".$coddocumento."', '".$codsuc."', '".$accion."', ".$correlativo.")");
        $consulta->execute(array());

        if ($consulta->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error Correlativo";
            die(2);
        }

        $items = $consulta->fetch();

        return $items[0];
    }

    function datos_empresa($codsuc) {
        global $conexion;

        $consulta = $conexion->prepare("SELECT e.razonsocial,s.direccion,s.descripcion,e.ruc,s.facturaalcantarillado
            FROM admin.empresas as e
            inner join admin.sucursales as s on(e.codemp=s.codemp)
            WHERE s.codemp=1 and s.codsuc=?");
        $consulta->execute(array($codsuc));
        return $consulta->fetch();
    }

    function getParamae($tippar, $codsuc) {
        global $conexion;

        $consulta = $conexion->prepare("SELECT * FROM reglasnegocio.parame WHERE tippar = ? AND codsuc = ?");
        $consulta->execute(array($tippar, $codsuc));
        $item = $consulta->fetch();

        return $item;
    }

    function getCodCatastral($alias)
	{
        //$codcatastro = "to_char(".$alias."codsuc, '00') || '-' || TRIM(to_char(".$alias."codzona, '00')) || '-' || TRIM(to_char(".$alias."codsector, '00')) || '-' || TRIM(to_char(CAST(".$alias."codmanzanas AS INTEGER), '000')) || '-' || TRIM(to_char(CAST(".$alias."lote AS INTEGER), '0000')) || '-' || TRIM(".$alias."sublote) AS codcatastro ";
		
        /*
        $codcatastro = "to_char(".$alias."codsuc, '00') || '-' || TRIM(to_char(".$alias."codsector, '00')) || '-' || TRIM(to_char(".$alias."codurbanizacion, '00')) || '-' || TRIM(to_char(CAST(".$alias."codmanzanas AS INTEGER), '000')) || '-' || TRIM(to_char(CAST(".$alias."lote AS INTEGER), '0000')) || '-' || TRIM(".$alias."sublote) AS codcatastro ";
        */

        $codcatastro = "to_char(".$alias."codsuc, '00') || '-' || TRIM(to_char(".$alias."codsector, '00')) || '-' || TRIM(to_char(CAST(".$alias."codmanzanas AS INTEGER), '000')) || '-' || TRIM(to_char(CAST(".$alias."lote AS INTEGER), '0000')) || '-' || TRIM(".$alias."sublote) AS codcatastro ";


        return $codcatastro;
    }

    function getCodCatastralsin($alias) {
        $codcatastro = "to_char(".$alias."codsuc, '00') || '' || TRIM(to_char(".$alias."codsector, '00')) || '' ||TRIM(to_char(CAST(".$alias."codmanzanas AS INTEGER), '000')) || '' || TRIM(to_char(CAST(".$alias."lote AS INTEGER), '0000')) || '' || TRIM(".$alias."sublote) AS codcatastrosin ";

        return $codcatastro;
    }

    function getCodCatastralGis($alias) {

        $TEXT = 'TEXT';

        $codcatastro = "to_char(".$alias."provincia,'0')||'-'||to_char(".$alias."distrito,'0')||'-'||TRIM(to_char(".$alias."sector,'00'))||'-'||TRIM(to_char(CAST(".$alias."manzana AS INTEGER),'000'))||'-'||TRIM(to_char(CAST(".$alias."lote AS INTEGER),'0000')) as codcatastrosin ";
        //return $codcatastro;

        return $codcatastro;
    }

    function getCodAntiguo($alias) {
        $codcatastro = "to_char(".$alias."codsuc, '00') || '' || TRIM(to_char(".$alias."nroinscripcion, '000000')) AS codantiguo ";

        return $codcatastro;
    }

    function getCodAntiguoGis($alias) {
        $codcatastro = "TRIM(to_char(".$alias."insc_padr, '00000000')) AS codantiguo ";

        return $codcatastro;
    }

    function getCodCatastral_catastro($nroinscripcion) {
        global $conexion;

        $consulta = $conexion->prepare("SELECT * FROM catastro.view_codcatastro_catastro WHERE nroinscripcion = ?");
        $consulta->execute(array($nroinscripcion));
        $items = $consulta->fetch();

        return $items["codcatastro"];
    }

    function getRutas($codsuc, $nroinscripcion) {
        global $conexion;

        $consulta = $conexion->prepare("SELECT * FROM facturacion.view_rutas WHERE nroinscripcion = ? AND codsuc = ?");
        $consulta->execute(array($nroinscripcion, $codsuc));
        $items = $consulta->fetch();

        return $items["codrutas"];
    }

    function est_Lectura($codigo) {
        global $conexion;

        $sql = "SELECT descripcion FROM public.estadolectura WHERE codestlectura = ?";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codigo));
        $item = $consulta->fetch();

        return $item["descripcion"];
    }

    function val_periodofacturacion($codsuc) {
        global $conexion;

        $consulta = $conexion->prepare("SELECT count(*) FROM facturacion.periodofacturacion WHERE codemp=1 and codsuc=?");
        $consulta->execute(array($codsuc));
        $items = $consulta->fetch();

        return $items[0];
    }

    function validar_correlativo($codsuc, $documento) {
        global $conexion;

        $consulta = $conexion->prepare("SELECT correlativo FROM reglasnegocio.correlativos WHERE codsuc=? and coddocumento=?");
        $consulta->execute(array($codsuc, $documento));
        $n = $consulta->rowCount();
        $items = $consulta->fetch();

        if ($n == 0) {
            $mensaje = "NO SE HA ENCONTRADO CORRELATIVOS ASIGNADOS A LA SUCURSAL";
        } else {
            $mensaje = "";
        }

        return array(1 => $mensaje, 2 => $n);
    }

    function RestarHoras($horaini, $horafin) {
        $horai = substr($horaini, 0, 2);
        $mini = substr($horaini, 3, 2);
        $segi = substr($horaini, 6, 2);

        $horaf = substr($horafin, 0, 2);
        $minf = substr($horafin, 3, 2);
        $segf = substr($horafin, 6, 2);

        $ini = ((($horai * 60) * 60) + ($mini * 60) + $segi);
        $fin = ((($horaf * 60) * 60) + ($minf * 60) + $segf);

        $dif = $fin - $ini;

        $difh = floor($dif / 3600);
        $difm = floor(($dif - ($difh * 3600)) / 60);
        $difs = $dif - ($difm * 60) - ($difh * 3600);
        return date("H:i:s", mktime($difh, $difm, $difs));
    }

    function login($codusu) {
        global $conexion;

        $sql = "SELECT login FROM seguridad.usuarios WHERE codusu=?";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codusu));
        $items = $consulta->fetch();
        return $items["login"];
    }

    function loginFull($codusu) {
        global $conexion;

        $sql = "SELECT * FROM seguridad.usuarios WHERE codusu=?";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codusu));
        $items = $consulta->fetch();
        return $items;
    }

    function calcular_diferencias_dias($fecha_inicio, $fecha_final) {
        if (!is_integer($fecha_inicio))
            $fecha_inicio = strtotime($fecha_inicio);
        if (!is_integer($fecha_final))
            $fecha_final = strtotime($fecha_final);

        return floor(abs($fecha_inicio - $fecha_final) / 60 / 60 / 24);
    }

    function preparesql2($campos, $tabla, $valor, $inicio, $fin, $orden, $parametro = "") {
        global $conexion;

        $value = array();
        $sql = $this->Sentencia($tabla);

        foreach ($campos as $val) {
            $value[$val] = "upper(cast(".$val.")) like :valor";
        }

        $sql .= " WHERE (".implode(" or ", $value).") ".$parametro.$orden;

        $valor = "%".$valor."%";

        //----Recupera cuantos registros fueron filtrados-------
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array(":valor" => $valor));
        $n = $consulta->rowCount();
        //----Generamos la sentencia para generar el listado-------
        //$sql .= " limit ".$inicio." offset ".$fin;

        $consulta1 = $conexion->prepare($sql);
        $consulta1->execute(array(":valor" => $valor));
        $items = $consulta1->fetchAll();
        //var_dump($consulta1->errorInfo());
        return array(1 => $n, 2 => $items);
    }

    // Funciones CheGuimo Inicio
    function PreparaSQL($Campos, $Texto) {
        $TEXT = 'TEXT';
        $LIKE = 'ILIKE';

        $tmp = trim($Texto);
        $cont = 0;

        for ($i = 0; $i < strlen($tmp); $i++) {
            if (strpos($tmp, ' ') != 0) {
                $cont = $cont + 1;
                $Palabras[$cont] = trim(substr($tmp, 0, strpos($tmp, ' ')));
                $tmp = trim(substr($tmp, strpos($tmp, ' ')));
            }
        }

        $cont = $cont + 1;
        $Palabras[$cont] = trim(substr($tmp, 0));

        $Condicion = "OR";

        if ($cont != 1) {
            $Condicion = "AND";
        }

        $tmp2 = " WHERE (UPPER(".$this->Convert($Campos[1], $TEXT).")".$LIKE." '%".strtoupper($Palabras[1])."%'";

        for ($i = 2; $i <= count($Campos); $i++) {
            $tmp2 = $tmp2." OR UPPER(".$this->Convert($Campos[$i], $TEXT).") ".$LIKE." '%".strtoupper($Palabras[1])."%' ";
        }

        $tmp2 = $tmp2.")";

        for ($x = 2; $x <= $cont; $x++) {
            $tmp2 = $tmp2." ".$Condicion." (UPPER(".$this->Convert($Campos[1], $TEXT).") ".$LIKE." '%".strtoupper($Palabras[$x])."%'";

            for ($i = 2; $i <= count($Campos); $i++) {
                $tmp2 = $tmp2." OR UPPER(".$this->Convert($Campos[$i], $TEXT).") ".$LIKE." '%".strtoupper($Palabras[$x])."%' ";
            }

            $tmp2 = $tmp2.")";
        }

        return $tmp2;
    }

    function VerificarSQL($Sql) {
        $arrnou = array('\\');
        $Sql = str_replace($arrnou, "'", $Sql);

        $posicion = strrpos($Sql, "sql_fecha(");

        if ($posicion != 0) {
            $Sql = str_replace("sql_fecha(", "to_char(", $Sql);
            $Sql = str_replace("dd/mm/yyyy", "'dd/mm/yyyy'", $Sql);
        }

        return $Sql;
    }

    // Funciones CheGuimo Fin
    //funciones REYERYCK
    function FormatFecha($date) {
        return " to_char(".$date.", 'dd/MM/yyyy') ";
    }

    function SubString($cad, $inicio, $fin) {
        return 'SUBSTR(CAST('.$cad.' AS VARCHAR), '.$inicio.', '.$fin.')';
    }

    function Trim($cad) {
        return 'TRIM('.$cad.')';
    }

    function Convert($cad, $tipo = '') {
        if ($tipo == '') {
            $tipo = 'TEXT';
        }

        return 'CAST('.$cad.' AS '.$tipo.') ';
    }

    function GetSeries($codsuc, $documento) {
        global $conexion;

        $sql = "SELECT serie, correlativo ";
        $sql .= "FROM reglasnegocio.correlativos ";
        $sql .= "WHERE codemp = 1 AND codsuc = ".$codsuc." AND nrocorrelativo = ".$documento;

        $result = $conexion->query($sql);

        return $row = $result->fetch();
    }

    function CuentaCorriente($NroInscripcion) {
        global $conexion;

        $Condicion1 = "WHERE f.nroinscripcion =".$NroInscripcion."";
        $Orden1 = " ORDER BY f.nrofacturacion ASC ";
        $Condicion2 = "WHERE p.nroinscripcion =".$NroInscripcion."  AND d.nrofacturacion<>0 AND p.anulado=0";
        $Orden2 = " ORDER BY p.nrofacturacion ASC ";


        $Sql = "(SELECT f.fechareg,f.serie,f.nrodocumento,".$this->Convert('f.anio', 'INTEGER')." as anio,
            ".$this->Convert('f.mes', 'INTEGER')." AS mes, sum(df.importe - (df.imppagado + df.importerebajado)) as cargo ,
            '0.00' as abono,'0' as movimiento
            FROM facturacion.cabfacturacion f
            INNER JOIN facturacion.detfacturacion df ON (f.codemp = df.codemp) AND (f.codsuc = df.codsuc) AND (f.nrofacturacion = df.nrofacturacion)
            AND (f.nroinscripcion = df.nroinscripcion) AND (f.codciclo = df.codciclo)
            ".$Condicion1."
            GROUP BY f.fechareg,f.serie,f.nrodocumento,f.anio, f.mes, f.nrofacturacion
            )
            UNION
            (
            SELECT p.fechareg, d.serie, d.nrodocumento,
            ".$this->Convert('d.anio', 'INTEGER')." as anio,".$this->Convert('d.mes', 'INTEGER')." AS mes, '0.00' as cargo,sum(d.importe) as abono,'1' as movimiento
            FROM
            cobranza.cabpagos p
            INNER JOIN cobranza.detpagos d ON (p.codemp = d.codemp)
            AND (p.codsuc = d.codsuc)
            AND (p.nroinscripcion = d.nroinscripcion)
            AND (p.nropago = d.nropago) ".$Condicion2."
            GROUP BY  p.fechareg, d.serie, d.nrodocumento,d.anio, d.mes
            ORDER BY p.fechareg ASC
            )
            ORDER by fechareg,anio, mes ASC ";


        $Consulta = $conexion->query($Sql);
        $c = 0;
        $Deuda = 0;
        foreach ($Consulta->fetchAll() as $row) {
            $c++;
            $Operacion = '';
            if ($row['movimiento'] == 0) {
                $Operacion = "FACTURACIÓN DE PENSIONES";
                $Anio = $row['anio'];
                $Mes = $row['mes'];
                $Deuda += $row['cargo'];
            } else {
                if ($row['anio'] == $Anio && $row['mes'] == $Mes)
                    $Operacion = "PAGO RECIBO DE PENSIONES";
                else
                    $Operacion = "PAGO DEUDA ACUMULADA";

                $Anio = $row['anio'];
                $Mes = $row['mes'];
                $Deuda -= $row['abono'];
            }
        }
        return $Deuda;
    }

    function CodUsuario($codsuc, $nroinscripcion) {
        return str_pad($codsuc, 2, "0", STR_PAD_LEFT).str_pad($nroinscripcion, 6, "0", STR_PAD_LEFT);
    }

    function ConsumoFac($lecturapromedio, $tipofacturacion, $consumo, $codestlectura, $codestadomedidor) {
        global $EstLecProm, $EstMedProm, $tipofacturacionfact;

        $tipofacturacionfact = $tipofacturacion;
        $ConsumoTemp = $lecturapromedio;

        if ($tipofacturacion != 1) {
            $ConsumoTemp = $consumo;
        }

        if ($codestlectura != 0) {
            if (array_key_exists($codestlectura, $EstLecProm)) {
                $ConsumoTemp = $lecturapromedio;
                $tipofacturacionfact = 1;
            } else {
                if (array_key_exists($codestadomedidor, $EstMedProm)) {
                    $tipofacturacionfact = 1;
                    $ConsumoTemp = $lecturapromedio;
                }
            }
        }

        if ($consumo < 0) {
            $ConsumoTemp = $lecturapromedio;
        }

        return $ConsumoTemp;
    }

    function ImporteTarifa($codsuc, $catetar, $tipofacturacionfact, $ConsumoFact, $NroFacturacion) {
        global $conexion;

        $Sql = "SELECT * FROM facturacion.f_getimportetarifa('".$codsuc."', '".$catetar."', '".$tipofacturacionfact."', ".$ConsumoFact.", ".$NroFacturacion.")";

        $Consulta = $conexion->prepare($Sql);
        $Consulta->execute(array());

        return $items = $Consulta->fetch();
    }

    function InsertDetFac($codemp, $codsuc, $nrofacturacion, $nroinscripcion, $codconcepto, $importe, $codtipodeuda, $codciclo, $ItemDetalle, $desconcepto, $nrocredito = 0, $nrocuota = 0, $nrorefinanciamiento = 0) {
        global $conexion;
        $Sql = "INSERT INTO facturacion.detfacturacion
            (codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
            importe,codtipodeuda,codciclo,item,concepto,nrocredito,nrocuota,nrorefinanciamiento)
            VALUES(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,
            :importe,:codtipodeuda,:codciclo,:item,:concepto,:nrocredito,:nrocuota,:nrorefinanciamiento)";

        $Consulta = $conexion->prepare($Sql);
        $Consulta->execute(array(":codemp" => 1,
            ":codsuc" => $codsuc,
            ":nrofacturacion" => $nrofacturacion,
            ":nroinscripcion" => $nroinscripcion,
            ":codconcepto" => $codconcepto,
            ":importe" => $importe,
            ":codtipodeuda" => $codtipodeuda,
            ":codciclo" => $codciclo,
            ":item" => $ItemDetalle,
            ":concepto" => utf8_encode($desconcepto),
            ":nrocredito" => $nrocredito,
            ":nrocuota" => $nrocuota,
            ":nrorefinanciamiento" => $nrorefinanciamiento
        ));
        if ($Consulta->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error detfacturacion ".$nroinscripcion.$Sql;
            die($mensaje);
        }
    }

    function InsertOcurrencia($codemp, $codsuc, $codtipoocurrencia, $codcliente, $nroinscripcion, $nromed, $marcamed, $fechamov, $observacion, $idusuario, $estareg) {
        global $conexion;
        $id = $this->setCorrelativos("ocurrencias", $codsuc, "0");
        $codocurrencia = $id[0];
        if ($nromed == '') {
            $Sql = "SELECT codcliente,c.nromed,m.descripcion as marcamed
                FROM catastro.clientes c
                INNER JOIN catastro.conexiones co ON (co.codemp=c.codemp AND co.codsuc=c.codsuc and co.nroinscripcion=c.nroinscripcion)
                INNER JOIN public.marcamedidor m ON (m.codmarca=co.codmarca)
                WHERE c.codsuc=".$codsuc." AND c.nroinscripcion=".$nroinscripcion;
            $items = $conexion->query($Sql)->fetch();
            $codcliente = $items['codcliente'];
            $nromed = $items['nromed'];
            $marcamed = $items['marcamed'];
        }
        $Sql = "INSERT INTO facturacion.ocurrencia(codocurrencia,codemp,codsuc,codtipoocurrencia,
                    codcliente,nroinscripcion,nromed,marcamed,fechamov,observacion,codusu,estareg)
                VALUES(:codocurrencia,:codemp,:codsuc,:codtipoocurrencia,:codcliente,
                    :nroinscripcion,:nromed,:marcamed,:fechamov,:observacion,:codusu,:estareg)";
        $result = $conexion->prepare($Sql);
        $result->execute(array(":codocurrencia" => $codocurrencia, ":codemp" => $codemp, ":codsuc" => $codsuc,
            ":codtipoocurrencia" => $codtipoocurrencia, ":codcliente" => $codcliente, ":nroinscripcion" => $nroinscripcion,
            ":nromed" => $nromed, ":marcamed" => $marcamed, ":fechamov" => $fechamov,
            ":observacion" => $observacion, ":codusu" => $idusuario, ":estareg" => $estareg));
        if ($result->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error detfacturacion ".$nroinscripcion.$Sql;
            die($mensaje);
        }
    }

    function InsertDetPreFac($codemp, $codsuc, $nrofacturacion, $nroinscripcion, $codconcepto, $importe, $codtipodeuda, $codciclo, $ItemDetalle, $desconcepto, $nrocredito = 0, $nrocuota = 0, $nrorefinanciamiento = 0) {
        global $conexion;
        $Sql = "INSERT INTO facturacion.detprefacturacion
            (codemp,codsuc,nrofacturacion,nroinscripcion,codconcepto,
            importe,codtipodeuda,codciclo,item,concepto,nrocredito,nrocuota,nrorefinanciamiento)
            VALUES(:codemp,:codsuc,:nrofacturacion,:nroinscripcion,:codconcepto,
            :importe,:codtipodeuda,:codciclo,:item,:concepto,:nrocredito,:nrocuota,:nrorefinanciamiento)";

        $Consulta = $conexion->prepare($Sql);
        $Consulta->execute(array(":codemp" => 1,
            ":codsuc" => $codsuc,
            ":nrofacturacion" => $nrofacturacion,
            ":nroinscripcion" => $nroinscripcion,
            ":codconcepto" => $codconcepto,
            ":importe" => $importe,
            ":codtipodeuda" => $codtipodeuda,
            ":codciclo" => $codciclo,
            ":item" => $ItemDetalle,
            ":concepto" => $desconcepto,
            ":nrocredito" => $nrocredito,
            ":nrocuota" => $nrocuota,
            ":nrorefinanciamiento" => $nrorefinanciamiento
        ));
        if ($Consulta->errorCode() != '00000') {
            $conexion->rollBack();
            $mensaje = "Error detfacturacion ".$nroinscripcion.$Sql;
            die($mensaje);
        }
    }

    function redondeado($numero, $decimales) {
        $factor = pow(10, $decimales);
        return (round($numero * $factor) / $factor);
    }
    
    function vermes_facturado($nrofact) {        
        global $conexion;
        
        $ver = "SELECT pe.anio, pe.mes FROM facturacion.periodofacturacion AS pe
            WHERE pe.nrofacturacion = $nrofact ";
        
        $Consulta = $conexion->prepare($ver);
        $Consulta->execute(array());
        
        return $items = $Consulta->fetch();
    }

}

?>
