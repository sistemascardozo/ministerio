<?php
	include($_SESSION['path']."config.php");

	class clsSql
	{
		function Sentencia($tabla,$parametro="",$separador="")
		{
			if($tabla=="urbanizaciones"){$sql = "SELECT * FROM catastro.urbanizaciones";}
			if($tabla=="usuarios"){$sql="SELECT * FROM seguridad.usuarios";}
			if($tabla=="indicadores"){$sql="SELECT * FROM indicadores.indicadores";}
			if($tabla=="sucursales"){$sql="SELECT * FROM admin.sucursales";}
			if($tabla=="sistemas"){$sql="SELECT * FROM seguridad.sistemas";}
			if($tabla=="subsistema"){
				$sql = "SELECT s.codsubsistema,s.descripcion,s.codsistema,sist.nombre_sistema,s.estareg,s.imagen,s.orden
						FROM seguridad.subsistemas as s
						inner join seguridad.sistemas as sist on(s.codsistema=sist.codsistema)";
			}
			if($tabla=="modulos")
			{
				$sql = "SELECT m.codmodulo,m.descripcion,m.url,m.estareg,s.codsistema,m.codsubsistema,m.idpadre,m.orden,m.estareg,
						(SELECT descripcion FROM seguridad.modulos where codmodulo=m.idpadre and codsubsistema=m.codsubsistema) as referencia
						FROM seguridad.modulos as m
						inner join seguridad.subsistemas as s on(m.codsubsistema=s.codsubsistema)
					  ";
			}
			if($tabla=="perfiles"){$sql="SELECT * FROM seguridad.perfiles";}
			if($tabla=="opciones"){$sql="SELECT * FROM reglasnegocio.parame";}
			if($tabla=="tipodocumento"){$sql="SELECT * FROM public.tipodocumento";}
			if($tabla=="areas"){
				$sql = "SELECT a.codarea,s.descripcion as sucursal,a.descripcion,a.estareg,a.responsables,a.codsuc
						FROM reglasnegocio.areas as a
						inner join admin.sucursales as s on(a.codsuc=s.codsuc and a.codemp=s.codemp)";}
			if($tabla=="inspectores"){
				$sql = "SELECT i.codinspector,s.descripcion as sucursal,i.nombres,t.descripcion,i.nrodocumento,i.estareg,i.codsuc,i.codtipodocumento
						FROM reglasnegocio.inspectores as i
						inner join admin.sucursales as s on(i.codemp=s.codemp and i.codsuc=s.codsuc)
						inner join public.tipodocumento as t on(i.codtipodocumento=t.codtipodocumento)";}
			if($tabla=="documentos"){$sql="SELECT * FROM reglasnegocio.documentos ";}
			if($tabla=="correlativos"){
				$sql="SELECT c.nrocorrelativo,s.descripcion,d.descripcion as doc,c.correlativo,c.estareg,c.codsuc,c.coddocumento,c.serie,c.nroinicio,c.nrofinal,c.correlativo
					 FROM reglasnegocio.correlativos as c
					 inner join admin.sucursales as s on(c.codemp=s.codemp and c.codsuc=s.codsuc)
					 inner join reglasnegocio.documentos as d on(c.coddocumento=d.coddocumento) AND (c.codsuc=d.codsuc)";
			}
			if($tabla=="unidades"){$sql="SELECT * FROM public.unidades";}
			if($tabla=="tipocostos"){$sql="SELECT * FROM public.tipocostos";}
			if($tabla=="tipoterreno"){$sql="SELECT * FROM public.tipoterreno";}
			/* modificar esta condicion */
			if($tabla=="materiales"){
				$sql = "SELECT c.nroconfeccion,c.descripcion,c.tipoconexion,c.cantidad,c.preciounitario,c.total,c.montoempresa,c.montousuario,u.abreviado,c.afectaigv,c.impigv,
						c.codtipocostos,c.unimed,c.estareg,c.requerido,tc.descripcion as dtipocostos, c.codgrupocostos, gc.descripcion as dgrupocostos, c.codtipopresupuesto
						FROM solicitudes.confecpreagudes as c
						inner join public.unidades as u on(c.unimed=u.unimed)
						inner join public.tipocostos as tc on (tc.codtipocostos=c.codtipocostos)
						inner join public.grupocostos as gc on (gc.codgrupocostos=c.codgrupocostos)";
			}
			if($tabla=="solicitudes")
			{
				$sql = "SELECT s.nrosolicitud,t.descripcion as servicio,s.codcliente,s.propietario,tc.descripcioncorta,c.descripcion as calle,s.fechaemision,
						td.descripcion as documento,s.nrodocumento,s.codubigeo,s.estareg,s.codtiposervicio,s.solreferencia,s.codsector,s.codcalle,s.area,s.nrocalle,
						s.codtipodocumento,s.grafico,s.referencia,es.descripcion as estadosolicitud,s.representante, s.sectorrep,
                        s.manzanarep, s.callerep, s.loterep, s.nrocallerep, s.codtipodocumentorep,
                        s.nrodocumentorep, s.codubigeorep, s.lotesol, s.callesol, s.manzanasol,s.codzona,
                        s.sectorsol, s.codmanzanas, s.lote, s.nrocallepred, s.codubigeopred,s.nropartidaregistral,
                        s.nroinspeccion,s.nroinscripcion,s.correo
						FROM solicitudes.solicitudes as s
						inner join public.tiposervicio as t on(s.codtiposervicio=t.codtiposervicio)
						inner join public.calles as c on(s.codcalle=c.codcalle and s.codemp=c.codemp and s.codsuc=c.codsuc AND s.codzona=c.codzona)
						inner join public.tiposcalle as tc on(c.codtipocalle=tc.codtipocalle)
						inner join public.tipodocumento as td on(s.codtipodocumento=td.codtipodocumento)
						inner join public.estadosolicitud es on(s.estareg=es.idestadosolicitud)";
			}
			if($tabla=="ubigeo")
			{
				$sql = "SELECT * FROM public.ubigeo";
			}
			if($tabla=="sectores"){$sql = "SELECT * FROM public.sectores";}
			if($tabla=="tiposervicio"){$sql = "SELECT * FROM public.tiposervicio";}
			if($tabla=="calles"){$sql="SELECT c.*,tc.descripcioncorta FROM public.calles c INNER JOIN public.tiposcalle tc ON (c.codtipocalle=tc.codtipocalle) ";}
			if($tabla=="calles_set"){$sql="SELECT c.*, tc.descripcioncorta
                                    FROM public.calles c
                                    INNER JOIN public.tiposcalle tc ON (c.codtipocalle=tc.codtipocalle)
                                    INNER JOIN public.sectores_calles d ON (c.codemp= d.codemp AND c.codsuc= d.codsuc AND c.codcalle= d.codcalle AND c.codzona=d.codzona) ";}
            if($tabla=="presupuesto")
			{
				$sql = "SELECT c.nropresupuesto,c.fechaemision,c.tipopresupuesto,c.codcliente,c.propietario,tc.descripcioncorta,
						cs.descripcion as calle,c.nrocalle,
						td.abreviado,c.nrodocumento,c.totalpresupuesto,c.estadopresupuesto,ep.descripcion as destadopresupuesto
						FROM solicitudes.cabpresupuesto as c
						inner join public.calles as cs on(c.codemp=cs.codemp and c.codsuc=cs.codsuc and c.codcalle=cs.codcalle and c.codsector=cs.codsector)
						inner join public.tiposcalle as tc on(cs.codtipocalle=tc.codtipocalle)
						inner join public.tipodocumento as td on(c.codtipodocumento=td.codtipodocumento)
						inner join public.estadopresupuesto ep on(ep.estadopresupuesto=c.estadopresupuesto)";
			}
			if($tabla=="diametrosagua"){$sql = "SELECT * FROM public.diametrosagua";}
			if($tabla=="diametrosdesague"){$sql = "SELECT * FROM public.diametrosdesague";}
			if($tabla=="catastro")
			{
				$sql = "SELECT c.nroinscripcion,c.codcliente,c.propietario,tc.descripcioncorta,ca.descripcion as calle,c.nrocalle,
						s.descripcion as sector,e.descripcion
						FROM catastro.catastro as c
						inner join public.calles as ca on(c.codemp=ca.codemp and c.codsuc=ca.codsuc and c.codcalle=ca.codcalle and c.codsector=ca.codsector)
						inner join public.tiposcalle as tc on(ca.codtipocalle=tc.codtipocalle)
						inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
						inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)";
			}
			if($tabla=="manzanas"){$sql = "SELECT * FROM public.manzanas";}
			if($tabla=="estadoservicio"){$sql = "SELECT * FROM public.estadoservicio";}
			if($tabla=="tipousuario"){$sql = "SELECT * FROM public.tipousuario";}
			if($tabla=="tipoentidades"){$sql = "SELECT * FROM public.tipoentidades";}
			if($tabla=="tiporesponsable"){$sql = "SELECT * FROM public.tiporesponsable";}
			if($tabla=="tipoactividad"){$sql = "SELECT * FROM public.tipoactividad";}
			if($tabla=="tipoconstruccion"){$sql = "SELECT * FROM public.tipoconstruccion";}
			if($tabla=="tipoabastecimiento"){$sql = "SELECT * FROM public.tipoabastecimiento";}
			if($tabla=="tipoalmacenaje"){$sql = "SELECT * FROM public.tipoalmacenaje";}
			if($tabla=="locacajaagua"){$sql = "SELECT * FROM public.locacajaagua";}
			if($tabla=="locacajadesague"){$sql = "SELECT * FROM public.locacajadesague";}
			if($tabla=="pavimentoagua"){$sql = "SELECT * FROM public.tipopavimentoagua";}
			if($tabla=="tipocorte"){$sql = "SELECT * FROM public.tipocorte";}
			if($tabla=="estadoconexion"){$sql = "SELECT * FROM public.estadoconexion";}
			if($tabla=="tipovereda"){$sql = "SELECT * FROM public.tipovereda";}
			if($tabla=="tipocajaagua"){$sql = "SELECT * FROM public.tipocajaagua";}
			if($tabla=="tipocajadesague"){$sql = "SELECT * FROM public.tipocajadesague";}
			if($tabla=="estadocajaagua"){$sql = "SELECT * FROM public.estadocajaagua";}
			if($tabla=="tipotapaagua"){$sql = "SELECT * FROM public.tipotapaagua";}
			if($tabla=="tipotapadesague"){$sql = "SELECT * FROM public.tipotapadesague";}
			if($tabla=="tipofugasagua"){$sql = "SELECT * FROM public.tipofugasagua";}
			if($tabla=="marcamedidor"){$sql = "SELECT * FROM public.marcamedidor";}
			if($tabla=="diametrosmedidor"){$sql = "SELECT * FROM public.diametrosmedidor";}
			if($tabla=="estadomedidor"){$sql = "SELECT * FROM public.estadomedidor";}
			if($tabla=="estadocajadesague"){$sql = "SELECT * FROM public.estadocajadesague";}
			if($tabla=="modelomedidor"){$sql = "SELECT * FROM public.modelomedidor";}
			if($tabla=="tipomedidor"){$sql = "SELECT * FROM public.tipomedidor";}
			if($tabla=="capacidadmedidor"){$sql = "SELECT * FROM public.capacidadmedidor";}
			if($tabla=="ubicacionllavemedidor"){$sql = "SELECT * FROM public.ubicacionllavemedidor";}
			if($tabla=="tipomaterialagua"){$sql = "SELECT * FROM public.tipomaterialagua";}
			if($tabla=="tipomaterialdesague"){$sql = "SELECT * FROM public.tipomaterialdesague";}
			if($tabla=="zonasbastecimiento"){$sql = "SELECT * FROM public.zonasabastecimiento";}
			if($tabla=="tarifas"){$sql = "SELECT * FROM facturacion.tarifas";}
			if($tabla=="actividadconcepto"){$sql = "SELECT * FROM facturacion.actividadconcepto";}
			if($tabla=="clientes"){
				$sql = "SELECT c.nroinscripcion,c.codcliente,c.propietario,tc.descripcioncorta,cl.descripcion as calle,c.nrocalle,
						s.descripcion as sector,e.descripcion, c.codantiguo".$separador.$parametro."
						FROM catastro.clientes as c
						INNER JOIN public.calles as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.codcalle=cl.codcalle and c.codzona=cl.codzona)
						inner join public.tiposcalle as tc on(cl.codtipocalle=tc.codtipocalle)
						inner join public.sectores as s on(c.codemp=s.codemp and c.codsuc=s.codsuc and c.codsector=s.codsector)
						inner join public.estadoservicio as e on(c.codestadoservicio=e.codestadoservicio)";
			}
			if($tabla=="ciclo"){$sql = "SELECT * FROM facturacion.ciclos";}
			if($tabla=="categoriatarifaria"){$sql = "SELECT * FROM facturacion.categoriatarifaria";}
			if($tabla=="tipocategoriatarifaria"){$sql = "SELECT * FROM public.tipocategoriatarifaria";}
			if($tabla=="rutasmaelectura"){$sql = "SELECT * FROM public.rutasmaelecturas";}
			if($tabla=="rutasmaedistribucion"){$sql = "SELECT * FROM public.rutasmaedistribucion";}
			if($tabla=="creditos"){
				$sql = "SELECT c.nrocredito,cl.propietario,c.fechareg,c.estareg,cn.descripcion as concepto,c.subtotal,c.igv,c.redondeo,c.imptotal
						FROM facturacion.cabcreditos as c
						inner join catastro.clientes as cl on(c.codemp=cl.codemp and c.codsuc=cl.codsuc and c.nroinscripcion=cl.nroinscripcion)
						inner join facturacion.conceptos as cn on(c.codemp=cn.codemp and c.codsuc=cn.codsuc and c.codconcepto=cn.codconcepto)";
			}
			if($tabla=="conceptos"){$sql = "SELECT * FROM facturacion.conceptos";}
			if($tabla=="refinanciamiento"){

				$sql = "SELECT cab.nrorefinanciamiento,clie.propietario,cab.fechaemision,cab.glosa,cab.totalrefinanciado,
						cab.nrocuotas,cab.cuotames,cab.creador FROM facturacion.cabrefinanciamiento as cab
						inner join catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and
						cab.nroinscripcion=clie.nroinscripcion)";
			}
			if($tabla=="rebajas")
			{
				$sql = "SELECT cab.nrorebaja,clie.propietario,cab.fechaemision,cab.creador,cab.nroreclamo,cab.glosa
						FROM facturacion.cabrebajas as cab
						inner join catastro.clientes as clie on(cab.codemp=clie.codemp and cab.codsuc=clie.codsuc and
						cab.nroinscripcion=clie.nroinscripcion)";

			}
			//Avance

			//Datos del Usuario

			//Catalogo Sectores
			if ($tabla=="sectors"){$sql = "SELECT codsector,descripcion,estareg  FROM public.sectores";}
			//Tipo de Usuario
			if ($tabla=="tipousuariox"){$sql = "SELECT codtipousuario,descripcion,estareg   FROM public.tipousuario" ;}
			// Condicion Tipo Usuario / Responsable
			if ($tabla=="tiporesponsablex"){$sql = "SELECT codtiporesponsable,descripcion,estareg  FROM public.tiporesponsable";}
			//Tipo de Actividades
			if ($tabla=="tipoactividadx"){$sql = "SELECT codtipoactividad,descripcion,estareg  FROM public.tipoactividad";}
			//Grupos
			if ($tabla=="tipoentidadesx"){$sql = "SELECT codtipoentidades,descripcion,estareg  FROM public.tipoentidades";}

			//Datos del Inmueble

		    //Tipo Construcci�n
			if ($tabla=="tipoconstruccionx"){$sql = "SELECT codtipoconstruccion,descripcion,estareg  FROM public.tipoconstruccion";}
			//Tipo Abastecimiento
			if ($tabla=="tipoabastecimientox"){$sql = "SELECT codtipoabastecimiento,descripcion,estareg  FROM public.tipoabastecimiento";}
			//Tipo Almacenaje
			if ($tabla=="codtipoalmacenajex"){$sql = "SELECT codtipoalmacenaje,descripcion,estareg  FROM public.tipoalmacenaje";}
			//Tipo Servicio
			if ($tabla=="tiposerviciox"){$sql = "SELECT codtiposervicio,descripcion,estareg  FROM public.tiposervicio";}
			//Tipo Material
			if ($tabla=="tipomaterialx"){$sql = "SELECT codtipomatconstruccion,descripcion,estareg  FROM public.tipomatconstruccion";}

			//Datos de Conexion de Agua

			//Tipo de Material del Tubo
			if ($tabla=="tipomaterialaguax"){$sql = "SELECT codtipomaterialagua,descripcion,estareg FROM  public.tipomaterialagua";}
			//Tipo de Pavimento
			if ($tabla=="tipopavimentoaguax"){$sql = "SELECT codtipopavimentoagua,descripcion,estareg FROM  public.tipopavimentoagua";}
			//Tipo de Estado de Conexion
			if ($tabla=="estadoconexionx"){$sql = "SELECT codestadoconexion,descripcion,estareg FROM  public.estadoconexion";}
			//Tipo Tapa Agua
			if ($tabla=="tipotapaaguax"){$sql = "SELECT codtipotapaagu,descripcion,estareg FROM  public.tipotapaagua";}
			//Diametro de Tubo de Agua
			if ($tabla=="diametrosaguax"){$sql = "SELECT coddiametrosagua,descripcion,estareg  FROM  public.diametrosagua";}
			//Tipo de Corte
			if ($tabla=="tipocortex"){$sql = "SELECT codtipocorte,descripcion,estareg  FROM  public.tipocorte";}
			//Tipo de Caja de Agua
			if ($tabla=="tipocajaaguax"){$sql = "SELECT codtipocajaagua,descripcion,estareg FROM  public.tipocajaagua";}
			//Localizacion de Caja de Agua
			if ($tabla=="locacajaaguax"){$sql = "SELECT codlocacajaagua,descripcion,estareg FROM  public.locacajaagua";}
			//Tipo Vereda
			 if ($tabla=="tipoveredax"){$sql = "SELECT codtipovereda,descripcion,estareg FROM  public.tipovereda";}
			//Estado Caja Agua
			 if ($tabla=="estadocajaaguax"){$sql = "SELECT codestadocajaagua,descripcion,estareg FROM  public.estadocajaagua";}
			//Fuga de Agua
			if ($tabla=="tipofugasaguax"){$sql = "SELECT codtipofugasagua,descripcion,estareg FROM  public.tipofugasagua";}
			//Tipo de Material del Tubo Desague
			if ($tabla=="tipomaterialdesaguex"){$sql = "SELECT codtipomaterialdesague,descripcion,estareg FROM  public.tipomaterialdesague";}
			//Tipo Tapa Desague
			if ($tabla=="tipotapadesaguex"){$sql = "SELECT codtipotapadesague,descripcion,estareg FROM  public.tipotapadesague";}
			//Diametro Tubo desagues
			if ($tabla=="diametrosdesaguex"){$sql = "SELECT coddiametrosdesague,descripcion,estareg FROM  public.diametrosdesague";}
			//Tipo de Caja de Desague
			if ($tabla=="tipocajadesaguex"){$sql = "SELECT codtipocajadesague,descripcion,estareg FROM  public.tipocajadesague";}
			//Localizacion de Caja de Desague
			if ($tabla=="locacajadesaguex"){$sql = "SELECT codlocacajadesague,descripcion,estareg FROM  public.locacajadesague";}
			//Tipo Estado Caja de Desague
			if ($tabla=="estadocajadesaguex"){$sql = "SELECT codestadocajadesague,descripcion,estareg FROM  public.estadocajadesague";}
			//tipos de calle
			if ($tabla=="tipocallex"){$sql = "SELECT codtipocalle,descripcioncorta,descripcionlarga,estareg FROM public.tiposcalle";}
			//categoria calles
			if($tabla=="categoriacallex"){$sql = "SELECT * FROM public.categoriacalles";}

			//Catalago de Calles
			if($tabla=="callesx"){$sql = "SELECT 	  Call.codcalle,
								  Sect.descripcion,
								  Cate.descripcion,
								  tipc.descripcioncorta,
								  Call.descripcion,
								  Call.estareg,Call.codsector  FROM    public.calles as Call
								INNER JOIN public.sectores as Sect ON (Call.codsector = Sect.codsector and Call.codemp=Sect.codemp and Call.codsuc=Sect.codsuc)
								INNER JOIN  public.categoriacalles as Cate ON (Call.codcategoriacalle = Cate.codcategoriacalle)
								INNER JOIN  public.tiposcalle as tipc ON (Call.codtipocalle = tipc.codtipocalle) ";}
			//Catalago de Manzanas
			//Marca de Medidor
			if($tabla=="marcax"){$sql = "SELECT * FROM public.marcamedidor";}
			//Diametros de Medidor
			if($tabla=="diametrosmedidorx"){$sql = "SELECT * FROM public.diametrosmedidor";}
			//Estado de Medidor
			if($tabla=="estadomedidorx"){$sql = "SELECT * FROM public.estadomedidor";}
			//Modelo de Medidor
			if($tabla=="modelox"){$sql = "SELECT * FROM public.modelomedidor";}
			//Tipo de Medidor
			if($tabla=="tipomedidorx"){$sql = "SELECT * FROM public.tipomedidor";}
			//Ubicacion de Medidor
			if($tabla=="ubicacionllavemedidorx"){$sql = "SELECT * FROM public.ubicacionllavemedidor";}
			//Capacidad del Medidor
			if($tabla=="capacidadmedidorx"){$sql = "SELECT * FROM public.capacidadmedidor";}
			//Estado Servicio del Medidor
			if($tabla=="estadoserviciox"){$sql = "SELECT * FROM public.estadoservicio";}
			if($tabla=="manzanax"){
					$sql  = "SELECT  Manz.codmanzanas,Sect.descripcion,Manz.descripcion,Manz.estareg,Manz.codsector FROM    public.manzanas as Manz
							INNER JOIN public.sectores as Sect ON (Manz.codsector = Sect.codsector and Manz.codemp = Sect.codemp and Manz.codsuc = Sect.codsuc)";

			}
			if($tabla=="tipoconceptos"){$sql = "SELECT * FROM public.tipoconceptos";}
			if($tabla=="formapago"){$sql = "SELECT * FROM public.formapago";}
			if($tabla=="car"){$sql = "SELECT * FROM cobranza.car";}
			if($tabla=="prepagos"){
					$sql  = "SELECT cab.nroprepago,frpago.descripcion as formpago,cab.propietario,cab.imptotal,cab.hora,cab.fechareg,cab.nroinscripcion,
							 cab.direccion,cab.documento,cab.eventual
						     FROM cobranza.cabprepagos as cab
						     inner join public.formapago as frpago on(cab.codformapago=frpago.codformapago)";
			}
			if($tabla=="pagos")
			{
					$sql  = "SELECT cab.nropago,upper(car.descripcion),upper(cab.propietario),cab.fechareg,cab.hora,
							 upper(forms.descripcion),case when cab.condpago=0 then 'CONTADO' else 'CREDITO' end,
							 cab.nropec,cab.nroprepago,cab.imptotal,cab.anulado FROM cobranza.cabpagos as cab
							 inner join cobranza.car car on(cab.car=car.car)
							 inner join public.formapago as forms on(cab.codformapago=forms.codformapago) ";
			}
			if($tabla=="deposito")
			{
				$sql = "SELECT dep.nropec,car.descripcion,dep.fechadeposito,dep.hora,dep.creador,sum(dep.impdeposito) as imptotal,
						dep.anio,dep.fechavoucher as fechavoucher   FROM public.depositosbanco as dep
						inner join cobranza.car car on(dep.car=car.car)";
			}
			if($tabla=="tipodeuda"){$sql = "SELECT * FROM public.tipodeuda";}
			if($tabla=="moneda"){$sql = "SELECT * FROM public.monedas";}
			if($tabla=="bancos"){$sql = "SELECT * FROM public.bancos";}
			if($tabla=="tipoctasbanco"){$sql = "SELECT * FROM public.tipoctasbanco";}
			if($tabla=="cajas"){$sql = "SELECT * FROM cobranza.cajas";}
			if($tabla=="ctasxbanco"){
				$sql = "SELECT cta.codctabanco,banc.descripcion as banco,tipcta.descripcion as tipocta,cta.nroctabanco as nroctabanco,
						cta.descripcion as ctabanco,cta.nroctacontable,upper(mon.descripcion),cta.estareg
						FROM public.ctasxbanco as cta
						inner join public.bancos as banc on(cta.codbanco=banc.codbanco)
						inner join public.tipoctasbanco as tipcta on(cta.codtipoctabanco=tipcta.codtipoctabanco)
						inner join public.monedas as mon on(cta.codmoneda=mon.codmoneda)";
			}
			if($tabla=="ctacobranza"){
				$sql = "SELECT c.codcategoria,c.descripcion,c.estareg,
						case when c.categoria=0 then 'FACTURACION' else
						case when c.categoria=1 then 'SALDO' else
						case when c.categoria=2 then 'SALDO REFINANCIADO' else
						'PRESTACION DE SERVICIOS EN CAJA' end end end,t.descripcion
						FROM cobranza.categoriacobranza as c
						inner join public.tipodeuda as t on(c.codtipodeuda=t.codtipodeuda)";
			}
			if($tabla=="asignacion_concepto_cobranza"){$sql = "SELECT codcategoria,descripcion,estareg FROM cobranza.categoriacobranza";}
			if($tabla=="motivocorte"){$sql = "SELECT * FROM public.motivocorte";}
			if($tabla=="reservorios"){$sql = "SELECT * FROM public.reservorios";}
			if($tabla=="estadolectura"){$sql = "SELECT * FROM public.estadolectura";}
			if($tabla=="horarios_abastecimiento"){$sql="SELECT * FROM public.horarios_abastecimiento";}
			if($tabla=="cargos_usuarios"){$sql="SELECT * FROM public.cargos_usuarios";}
			if($tabla=="medio_reclamo"){$sql="SELECT * FROM reclamos.medioreclamo";}
			if($tabla=="reclamos"){
				$sql = "SELECT rec.nroreclamo,rec.nroinscripcion,clie.propietario,tipdoc.abreviado,rec.nrodocumento,
						case when rec.tiporeclamo=1 then 'COMERCIALES CON FACTURACION' else
						case when rec.tiporeclamo=2 then 'COMERCIALES SIN FACTURACION' else 'OPERACIONALES' end end,
						tippar.descripcion,rec.reclamante,estado.descripcion as estText,rec.glosa,rec.codestadoreclamo FROM reclamos.reclamos as rec
						inner join catastro.clientes as clie on(rec.codemp=clie.codemp and rec.codsuc=clie.codsuc and
						rec.nroinscripcion=clie.nroinscripcion)
						inner join public.tipodocumento as tipdoc on(rec.codtipodocumento=tipdoc.codtipodocumento)
						inner join public.tipoparentesco as tippar on (rec.codtipoparentesco=tippar.codtipoparentesco)
						inner join reclamos.estadoreclamo as estado on(rec.codestadoreclamo=estado.codestadoreclamo)";
			}
			if($tabla=="tipoparentesco"){$sql = "SELECT * FROM public.tipoparentesco";}
			if($tabla=="concepto_reclamo"){
				$sql = "SELECT codconcepto,case when tiporeclamo=1 then 'COMERCIALES CON FACTURACION' else
						case when tiporeclamo=2 then 'COMERCIALES SIN FACTURACION' else 'OPERACIONALES' end end ,
						subtiporeclamo,descripcion,estareg FROM reclamos.conceptosreclamos ";
			}
			if($tabla=="estado_reclamo"){$sql = "SELECT * FROM reclamos.estadoreclamo";}
			if($tabla=="tipo_parentesco"){$sql = "SELECT * FROM public.tipoparentesco ";}
			if($tabla=="detalle_reclamos_ingresos"){
				$sql = "SELECT d.nrodetalle,d.nroreclamo,c.propietario,d.codusu_derivacion,e.descripcion as estado,cr.descripcion as rec,
						r.glosa,r.nroinscripcion,d.codestadoreclamo,d.nrodetalle_origen,r.fin_reclamo
						FROM reclamos.detalle_reclamos as d
						inner join reclamos.reclamos as r on(d.codemp=r.codemp and d.codsuc=r.codsuc and d.nroreclamo=r.nroreclamo)
						inner join catastro.clientes as c on(r.codemp=c.codemp and r.codsuc=c.codsuc and r.nroinscripcion=c.nroinscripcion)
						inner join reclamos.estadoreclamo as e on(d.codestadoreclamo=e.codestadoreclamo)
						inner join reclamos.conceptosreclamos as cr on(r.codconcepto=cr.codconcepto)";
			}
			if($tabla=="solicitud_inspeccion")
			{
				$sql = "SELECT sol.nroinspeccion,sol.nroinscripcion,clie.propietario,";
				$sql .= "tipcal.descripcioncorta || ' ' || cal.descripcion || ' #' || clie.nrocalle,upper(insp.nombres),";
				$sql .= "sol.fechaemision,sol.estado ";
				$sql .= "FROM catastro.solicitudinspeccion as sol ";
				$sql .= "inner join catastro.clientes as clie on(sol.codemp=clie.codemp and sol.codsuc=clie.codsuc and sol.nroinscripcion=clie.nroinscripcion)";
				$sql .= "inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle and clie.codzona=cal.codzona ) ";
				$sql .= "inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle) ";
				$sql .= "inner join reglasnegocio.inspectores as insp on (sol.codinspector=insp.codinspector) AND (sol.codsuc=insp.codsuc)";
			}
			if($tabla=="consulta_pagos")
			{
				$sql  = "SELECT cab.nropago,upper(car.descripcion),upper(cab.propietario),cab.fechareg,cab.hora,
						 upper(forms.descripcion),case when cab.condpago=0 then 'CONTADO' else 'CREDITO' end,
						 cab.nropec,cab.nroprepago,cab.imptotal,cab.anulado,cab.nroinscripcion,cab.car,cab.nrocaja,cab.creador,cab.codformapago
						 FROM cobranza.cabpagos as cab
						 inner join cobranza.car car on(cab.car=car.car)
						 inner join public.formapago as forms on(cab.codformapago=forms.codformapago) ";
			}

            if($tabla=="almacen")
            {
                $sql= "SELECT * FROM micromedicion.almacen AS al INNER JOIN public.estadoreg es ON (al.estareg=es.id)";
            }

            if($tabla=="bancomedidor")
            {
                $sql= "SELECT * FROM micromedicion.bancomedidor";
            }

            if($tabla=="zonas")
            {
                $sql= "SELECT * FROM admin.zonas";
            }
            /* agregar estas condiciones*/
			if($tabla=="grupocostos"){$sql="SELECT * FROM public.grupocostos";}
			if($tabla=="tipopresupuesto"){$sql="SELECT * FROM public.tipopresupuesto";}
			/**/


			/**/
			return $sql;
		}
	}

?>
