<?php
	include($_SESSION['path']."objetos/clsFunciones.php");
	
	class clsMantenimiento extends clsFunciones
	{
		function SetIndicador($idindicador)
		{
			global $conexion;
			
			$sql = $this->Sentencia("indicadores")." WHERE codindicador = ?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($idindicador));
			
			return $consulta->fetch();
			
		}
		function setUsuario($idusuarios)
		{
			global $conexion;
			
			$sql = $this->Sentencia("usuarios")." WHERE codusu = ?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($idusuarios));
			
			return $consulta->fetch();
			
		}
		function setSucursales($parametro = "", $codigo = "")
		{
			global $conexion;
			
			$sql = $this->Sentencia("sucursales").$parametro;
			$consulta = $conexion->prepare($sql);
			
			if($codigo != "")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}
			else
			{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		
		function setSistemas($parametro = "", $codigo = "")
		{
			global $conexion;
			
			$sql = $this->Sentencia("sistemas").$parametro;
			$consulta = $conexion->prepare($sql);
			
			if($codigo != "")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}
			else
			{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}

			return $items;
		}
		function setSubsistemas($parametro = "", $codigo = "")
		{
			global $conexion;
			
			$sql = $this->Sentencia("subsistema").$parametro;
			$consulta = $conexion->prepare($sql);

			if($codigo != "")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}
			else
			{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}

			return $items;
		}
		function setModulos($idmodulo)
		{
			global $conexion;
			
			$sql = $this->Sentencia("modulos")." WHERE m.codmodulo = ?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($idmodulo));
			
			return $consulta->fetch();
		}
		
		function setPerfiles($idperfil)
		{
			global $conexion;
			
			$sql = $this->Sentencia("perfiles")." WHERE codperfil = ?";
			
			$consulta = $conexion->prepare($sql);
			$consulta->execute(array($idperfil));
			
			return $consulta->fetch();
		}
		
		function setParamae($parametro = "", $codigo = "")
		{
			global $conexion;
			
			$sql = $this->Sentencia("opciones").$parametro;
			$consulta = $conexion->prepare($sql);

			if($codigo != "")
			{
				$consulta->execute($codigo);
				$items = $consulta->fetch();
			}
			else
			{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}

			return $items;
		}
		function setAreas($parametro = "", $codigo = "", $codsuc = "")
		{
			global $conexion;
			
			$sql = $this->Sentencia("areas").$parametro;
			$consulta = $conexion->prepare($sql);

			if($codigo != "")
			{
				$consulta->execute(array($codigo, $codsuc));
				$items = $consulta->fetch();
			}
			else
			{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}

			return $items;
		}
		function setTipoDocumento($where = "", $codigo = "")
		{
			global $conexion;
			
			$sql = $this->Sentencia("tipodocumento").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo != "")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}
			else
			{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setInspectores($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("inspectores").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setDocumento($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("documentos").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setCorrelativosMant($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("correlativos").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setUnidadMedida($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("unidades").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function seetTipoCostos($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("tipocostos").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setTipoTerreno($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("tipoterreno").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function getParamaeMant($tippar)
		{
			global $conexion;
			
			$consulta = $conexion->prepare("SELECT * FROM reglasnegocio.parame WHERE tippar=?");
			$consulta->execute(array($tippar));
			$item = $consulta->fetch();
			
			return $item;
		}
		function setMateriales($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("materiales").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute($codigo);
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setSolicitudes($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("solicitudes").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute($codigo);
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setDiametrosAgua($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("diametrosagua").$where;
			$consulta = $conexion->prepare($sql);
			if($codigo!="")
			{
				$consulta->execute($codigo);
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setDiametrosDesague($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("diametrosdesague").$where;
			$consulta = $conexion->prepare($sql);

			if($codigo!="")
			{
				$consulta->execute($codigo);
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		/*agregar estos 2 metodos*/
		
		function setGrupoCostos($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("grupocostos").$where;
			$consulta = $conexion->prepare($sql);
			
			if($codigo!="")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		function setTipoPresupuesto($where="",$codigo="")
		{
			global $conexion;
			
			$sql = $this->Sentencia("tipopresupuesto").$where;
			$consulta = $conexion->prepare($sql);
			
			if($codigo!="")
			{
				$consulta->execute(array($codigo));
				$items = $consulta->fetch();
			}else{
				$consulta->execute();
				$items = $consulta->fetchAll();
			}
			
			return $items;
		}
		
		/*code*/
	}
?>