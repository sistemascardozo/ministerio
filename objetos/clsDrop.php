<?php

include($_SESSION['path']."objetos/clsMantenimiento.php");

header('Content-type: text/html; charset=utf-8');

class clsDrop extends clsMantenimiento {

    function drop_indicadores($codsuc, $seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("indicadores")." WHERE estadoindicador = 1 AND codemp = 1 AND codsuc = ? ORDER BY codindicador";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='indicador' name='indicador' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Indicador ::.</option>";

        foreach ($items as $row) {
            $selected = "";

            if ($seleccion == $row["codindicador"]) {
                $selected = "selected='selected'";
            }

            echo "<option value='".$row["codindicador"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }

        echo "</select>";
    }

    function drop_conceptos($codsuc, $tipo = '', $event = '', $seleccion = 1000) {
        global $conexion;

        if ($tipo != '') {
            $tipos = " AND codtipoconcepto=".$tipo;
        }

        $sql = $this->Sentencia("conceptos")." WHERE estareg = 1 AND codsuc = ".$codsuc." ".$tipos." ORDER BY descripcion";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='codconcepto' name='codconcepto' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Concepto ::.</option>";

        foreach ($items as $row) {
            $selected = "";

            if ($seleccion == $row["codconcepto"]) {
                $selected = "selected='selected'";
            }

            echo "<option value='".$row["codconcepto"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }

        echo "</select>";
    }

    function drop_sucursal($seleccion = 1000, $event = "") {
        $items = $this->setSucursales(" WHERE estareg=1");

        echo "<select id='sucursal' name='sucursal' class='select' style='width:250px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Sucursal ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($row["codsuc"] == $seleccion) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codsuc"]."' ".$selected." >".$row["descripcion"]."</option>";
        }
        echo "</select>";
    }

    function drop_tipomedidor($codmedidor, $seleccion = 10000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("estadomedidor")." WHERE estareg=1 ORDER BY codestadomedidor ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();

        $items = $consulta->fetchAll();

        $cmb = "<select id='codestadomedidor' disabled='disabled' name='codestadomedidor' style='width:220px' ".$event." >";
        $cmb.="<option value='10000'>.:: Seleccione el estado de medidor ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadomedidor"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["codestadomedidor"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_tipodocumento($seleccion = 1000, $event = "", $name = "tipodocumento") {
        $items = $this->setTipoDocumento(" WHERE estareg=1");

        echo "<select id='$name' name='$name' class='select' style='width:220px' ".$event." >";
        echo "<option value=''>.:: Seleccione el Tipo de Documento ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($row["codtipodocumento"] == $seleccion) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipodocumento"]."' ".$selected." >".$row["descripcion"]."</option>";
        }
        echo "</select>";
    }

    function drop_categoria_correlativo($seleccion = 1000) {
        $sel_0 = $seleccion == 0 ? "selected='selected'" : "";
        $sel_1 = $seleccion == 1 ? "selected='selected'" : "";

        echo "<select id='categoria' name='categoria' class='select' style='width:250px' >";
        echo "<option value='100'>.:: Seleccione la categoria ::.</option>";
        echo "<option value='0' ".$sel_0." >Documentos Tributarios</option>";
        echo "<option value='1' ".$sel_1." >Documentos de Tablas</option>";
        echo "</select>";
    }

    function drop_tipofacturacion($seleccion = 1000) {
        $sel_0 = $seleccion == 0 ? "selected='selected'" : "";
        $sel_1 = $seleccion == 1 ? "selected='selected'" : "";
        $sel_2 = $seleccion == 2 ? "selected='selected'" : "";

        echo "<select id='tfacturacion' name='tfacturacion' class='select' style='width:220px' >";
        echo "<option value='100'>.:: Seleccione el Tipo de Facturacion ::.</option>";
        echo "<option value='0' ".$sel_0." >Leido</option>";
        echo "<option value='1' ".$sel_1." >Promediado</option>";
        echo "<option value='2' ".$sel_2." >Asigando</option>";
        echo "</select>";
    }

    function drop_documentos($seleccion = 1000, $event = "", $WHERE = " WHERE estareg=1") {
        $items = $this->setDocumento($WHERE);

        echo "<select id='documento' name='documento' class='select' style='width:250px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Documento ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($row["coddocumento"] == $seleccion) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["coddocumento"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_usuarios($event = "", $and = "") {
        global $conexion;

        $sql = "select * from seguridad.usuarios WHERE estareg=1 ".$and;
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='usuario' name='usuario' class='select' style='width:250px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Usuario ::.</option>";
        foreach ($items as $row) {
            echo "<option value='".$row["codusu"]."' >".strtoupper($row["nombres"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipoconexion($seleccion = 1000) {
        $sel_1 = $seleccion == 1 ? "selected='selected'" : "";
        $sel_2 = $seleccion == 2 ? "selected='selected'" : "";

        echo "<select id='tipoconexion' name='tipoconexion' class='select' style='width:250px' >";
        echo "<option value='100'>.:: Seleccione el Tipo de Conexion ::.</option>";
        echo "<option value='1' ".$sel_1." >AGUA</option>";
        echo "<option value='2' ".$sel_2." >DESAGUE</option>";
        echo "</select>";
    }

    function drop_tipocostos($seleccion = 1000, $event = "") {
        $items = $this->seetTipoCostos(" WHERE estareg=1");

        echo "<select id='tipocostos' name='tipocostos' class='select' style='width:250px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Tipo de Costo ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipocostos"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipocostos"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_unidad_medida($seleccion = 1000, $event = "") {
        $items = $this->setUnidadMedida(" WHERE estareg=1");

        echo "<select id='unidad' name='unidad' class='select' style='width:250px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Unidad de Medida ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["unimed"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["unimed"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    /**/

    function drop_zonas($codsuc, $seleccion = 10000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("zonas")." WHERE estareg=1 AND codemp=1 AND codsuc=? ORDER BY descripcion ASC";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        $cmb = "<select id='codzona' name='codzona' class='select' style='width:220px' ".$event." >";
        $cmb .= "<option value='0'>.:: Seleccione la Zona ::.</option>";

        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codzona"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["codzona"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb .= "</select>";
        return $cmb;
    }

    function drop_sectores($codsuc, $codzona = 0, $seleccion = 10000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("sectores")." WHERE estareg = 1 AND codemp = 1 AND codsuc = ".$codsuc." ";

        if ($codzona > 0) {
            $sql .= " AND codzona = ".$codzona." ";
        }

        $sql .= "ORDER BY codsector ASC";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());

        $items = $consulta->fetchAll();

        $cmb = "<select id='codsector' name='codsector' class='select' style='width:220px' ".$event." >";
        $cmb .= "<option value='0'>.:: Seleccione el Sector ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codsector"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["codsector"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_sectores2($codsuc, $seleccion = 10000, $event = "") {
        global $conexion;

        $sql = "SELECT DISTINCT codsector, descripcion FROM public.sectores ";
        $sql .= "WHERE estareg = 1 AND codemp = 1 AND codsuc = ".$codsuc." ";

        $sql .= "ORDER BY codsector ASC";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());

        $items = $consulta->fetchAll();

        $cmb = "<select id='codsector' name='codsector' class='select' style='width:220px' ".$event." >";
        $cmb .= "<option value='0'>.:: Seleccione el Sector ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codsector"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["codsector"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_estadoreclamo($codestadoreclamo, $seleccion = 10000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("estado_reclamo")." WHERE estareg=1 ORDER BY codestadoreclamo ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        $cmb = "<select id='codestadoreclamo' disabled='disabled' name='codestadoreclamo' style='width:220px' ".$event." >";
        $cmb.="<option value='10000'>.:: Seleccione el estado de reclamo ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadoreclamo"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["codestadoreclamo"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_sectores_zonas($codsuc, $codzona, $seleccion = 10000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("sectores")." WHERE estareg=1 AND codemp=1 AND codsuc=? AND codzona=? ORDER BY codsector ASC";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $codzona));

        $items = $consulta->fetchAll();

        $cmb = "<select id='sector' name='sector' class='select' style='width:220px' ".$event." >";
        $cmb.="<option value='0'>.:: Seleccione el Sector ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codsector"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["codsector"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    //function drop_tiposervicio($seleccion = 10000, $and = "and codtiposervicio<>1", $event = "") {
    function drop_tiposervicio($seleccion = 10000, $and = "", $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tiposervicio")." WHERE estareg=1 ".$and;
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tiposervicio' name='tiposervicio' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtiposervicio"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtiposervicio"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tiposervicioGis($seleccion = 10000, $and = "", $event = "") {
        global $conexion;

        $and = " ORDER BY descripcion ASC";
        $sql = "SELECT * FROM gis.tiposervicio WHERE estareg=1 ".$and;
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tiposervicio' name='tiposervicio' class='select' style='width:220px' ".$event." >";
        //echo "<option value='0'>--Seleccione el Tipo de Servicio--</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtiposervicio"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtiposervicio"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipomatconstruccion($seleccion = 10000, $and = "", $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipomaterialx")." WHERE estareg=1 ".$and;
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipomatconstruccion' name='tipomatconstruccion' class='select' style='width:220px' ".$event." >";

        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipomatconstruccion"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipomatconstruccion"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_ubigeo($id, $text, $WHERE, $cod, $event = "", $seleccion = '100101') {
        global $conexion;

        $sql = $this->Sentencia("ubigeo").$WHERE;

        $consulta = $conexion->prepare($sql);
        $consulta->execute($cod);
        $items = $consulta->fetchAll();

        echo "<select id='".$id."' name='".$id."' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>".$text."</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codubigeo"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codubigeo"]."' ".$selected." >".utf8_encode(strtoupper($row["descripcion"]))."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_terreno($seleccion = 1000, $event = "") {
        $items = $this->setTipoTerreno(" WHERE estareg=1");

        echo "<select id='tipoterreno' name='tipoterreno' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Tipo de Terreno ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoterreno"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoterreno"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_diametros_agua($seleccion = 1000, $event = "") {
        $items = $this->setDiametrosAgua(" WHERE estareg=1");

        echo "<select id='diametrosagua' name='diametrosagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["coddiametrosagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["coddiametrosagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_categoria_tarifaria($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipocategoriatarifaria")." WHERE  estareg=1";

        $consulta = $conexion->prepare($sql);
        //print_r($consulta);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipocategoriatarifaria' name='tipocategoriatarifaria' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione Tipo Categoria ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipcattar"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipcattar"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_diametros_desague($seleccion = 1000, $event = "") {
        $items = $this->setDiametrosDesague(" WHERE estareg=1");

        echo "<select id='diametrosdesague' name='diametrosdesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["coddiametrosdesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["coddiametrosdesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_manzanas($codsuc, $codsector, $seleccion = 0, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("manzanas")." WHERE codemp = 1 AND codsuc = ".$codsuc." AND codsector = ".$codsector." ";
        //$sql .= "ORDER BY CAST(codmanzanas AS INTEGER) ASC";
		$sql .= "ORDER BY descripcion ASC";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='manzanas' name='manzanas' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Manzana ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmanzanas"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmanzanas"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estado_servicio($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("estadoservicio")." WHERE estareg=1 ORDER BY codestadoservicio";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='estadoservicio' name='estadoservicio' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Estado de Servicio ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadoservicio"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadoservicio"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estado_servicioGis($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.estadoservicio WHERE estareg=1 ORDER BY codestadoservicio";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codestadoservicio' name='codestadoservicio' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>--Seleccione el Estado de Servicio--</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadoservicio"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadoservicio"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_usuario($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipousuario")." WHERE estareg=1 ORDER BY codtipousuario";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipousuario' name='tipousuario' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipousuario"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipousuario"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_usuarioGis($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipousuario WHERE estareg=1 ORDER BY descripcion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipousuario' name='tipousuario' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipousuario"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipousuario"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_entidades($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipoentidades")." WHERE estareg=1 ORDER BY codtipoentidades";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoentidades' name='tipoentidades' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoentidades"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoentidades"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_responsable($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tiporesponsable")." WHERE estareg=1 ORDER BY codtiporesponsable";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tiporesponsable' name='tiporesponsable' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtiporesponsable"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtiporesponsable"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_responsableGis($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tiporesponsable WHERE estareg=1 ORDER BY codtiporesponsable";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tiporesponsable' name='tiporesponsable' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtiporesponsable"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtiporesponsable"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_actividad($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipoactividad")." WHERE estareg=1 ORDER BY descripcion ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoactividad' name='tipoactividad' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoactividad"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoactividad"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_construccion($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipoconstruccion")." WHERE estareg=1 ORDER BY codtipoconstruccion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoconstruccion' name='tipoconstruccion' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoconstruccion"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoconstruccion"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_abastecimiento($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipoabastecimiento")." WHERE estareg=1 ORDER BY codtipoabastecimiento";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoabastecimiento' name='tipoabastecimiento' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoabastecimiento"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoabastecimiento"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_abastecimientoGis($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipoabastecimiento WHERE estareg=1 ORDER BY codtipoabastecimiento";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoabastecimiento' name='tipoabastecimiento' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoabastecimiento"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoabastecimiento"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_almacenaje($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipoalmacenaje")." WHERE estareg=1 ORDER BY codtipoalmacenaje";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoalmacenaje' name='tipoalmacenaje' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoalmacenaje"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoalmacenaje"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_localizacion_caja_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("locacajaagua")." WHERE estareg=1 ORDER BY codlocacajaagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='locacajaagua' name='locacajaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codlocacajaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codlocacajaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_pavimento_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("pavimentoagua")." WHERE estareg=1 ORDER BY codtipopavimentoagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='pavimentoagua' name='pavimentoagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipopavimentoagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipopavimentoagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_corte($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipocorte")." WHERE estareg=1 ORDER BY codtipocorte";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipocorte' name='tipocorte' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipocorte"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipocorte"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estado_conexion($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("estadoconexion")." WHERE estareg=1 ORDER BY codestadoconexion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='estadoconexion' name='estadoconexion' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadoconexion"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadoconexion"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_vereda($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipovereda")." WHERE estareg=1 ORDER BY codtipovereda";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipovereda' name='tipovereda' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipovereda"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipovereda"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_veredaGis($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipovereda WHERE estareg=1 ORDER BY codtipovereda";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipovereda' name='tipovereda' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipovereda"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipovereda"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_caja_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipocajaagua")." WHERE estareg=1 ORDER BY codtipocajaagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipocajaagua' name='tipocajaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipocajaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipocajaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estado_caja_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("estadocajaagua")." WHERE estareg=1 ORDER BY codestadocajaagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='estadocajaagua' name='estadocajaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadocajaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadocajaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_tapa_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipotapaagua")." WHERE estareg=1 ORDER BY codtipotapaagu";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipotapaagua' name='tipotapaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipotapaagu"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipotapaagu"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_fugas_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipofugasagua")." WHERE estareg=1 ORDER BY codtipofugasagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipofugasagua' name='tipofugasagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipofugasagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipofugasagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_marca_medidor($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("marcamedidor")." WHERE estareg=1 ORDER BY codmarca";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='marcamedidor' name='marcamedidor' class='select' style='width:150px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmarca"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmarca"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_diametros_medidor($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("diametrosmedidor")." WHERE estareg=1 ORDER BY coddiametrosmedidor";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='diametrosmedidor' name='diametrosmedidor' class='select' style='width:150px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["coddiametrosmedidor"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["coddiametrosmedidor"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estado_medidor($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("estadomedidor")." WHERE estareg=1 ORDER BY codestadomedidor";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='estadomedidor' name='estadomedidor' class='select' style='width:150px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadomedidor"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadomedidor"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_modelo_medidor($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("modelomedidor")." WHERE estareg=1 ORDER BY codmodelo";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='modelomedidor' name='modelomedidor' class='select' style='width:150px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmodelo"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmodelo"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_medidor($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipomedidor")." WHERE estareg=1 ORDER BY codtipomedidor";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipomedidor' name='tipomedidor' class='select' style='width:150px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipomedidor"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipomedidor"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_capacidad_medidor($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("capacidadmedidor")." WHERE estareg=1 ORDER BY codcapacidadmedidor";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='capacidadmedidor' name='capacidadmedidor' class='select' style='width:150px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codcapacidadmedidor"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codcapacidadmedidor"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_ubicacion_llave_medidor($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("ubicacionllavemedidor")." WHERE estareg=1 ORDER BY codubicacion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='ubicacionllavemedidor' name='ubicacionllavemedidor' class='select' style='width:150px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codubicacion"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codubicacion"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_material_tubo_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipomaterialagua")." WHERE estareg=1 ORDER BY codtipomaterialagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipomaterialagua' name='tipomaterialagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipomaterialagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipomaterialagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_material_tubo_desague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipomaterialdesague")." WHERE estareg=1 ORDER BY codtipomaterialdesague";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipomaterialdesague' name='tipomaterialdesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipomaterialdesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipomaterialdesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_localizacion_caja_desague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("locacajadesague")." WHERE estareg=1 ORDER BY codlocacajadesague";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='locacajadesague' name='locacajadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codlocacajadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codlocacajadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_caja_desague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipocajadesague")." WHERE estareg=1 ORDER BY codtipocajadesague";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipocajadesague' name='tipocajadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipocajadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipocajadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estado_caja_desague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("estadocajadesague")." WHERE estareg=1 ORDER BY codestadocajadesague";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='estadocajadesague' name='estadocajadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadocajadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadocajadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_tapa_desague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tipotapadesague")." WHERE estareg=1 ORDER BY codtipotapadesague";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipotapadesague' name='tipotapadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipotapadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipotapadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_zona_abastecimiento($codsuc, $seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("zonasbastecimiento")." WHERE estareg = 1 AND codemp = 1 AND codsuc = ? ORDER BY codzona";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='zonasbastecimiento' name='zonasbastecimiento' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Zona de Abastecimiento ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codzona"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codzona"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tarifas($codsuc, $seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("tarifas")." WHERE estareg=1 AND codemp=1 AND codsuc=? AND estado = 1 ORDER BY catetar";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='tarifas' name='tarifas' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Tarifa ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["catetar"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["catetar"]."' ".$selected."  >".strtoupper($row["nomtar"])."</option>";
        }
        echo "</select>";
    }

    function drop_ciclos($codsuc, $seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("ciclo")." WHERE estareg=1 and codemp=1 and codsuc=? ORDER BY codciclo";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='ciclo' name='ciclo' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Ciclo ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codciclo"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codciclo"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_categoria_tarifarias($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("categoriatarifaria")." WHERE estareg=1 ORDER BY codcategoriatar";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='categoriatarifaria' name='categoriatarifaria' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Categoria Tarifaria ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codcategoriatar"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codcategoriatar"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_anio($codsuc, $codciclo, $event = "") {
        global $conexion;

        $SqlA = "SELECT anio FROM facturacion.periodofacturacion ";
        $SqlA .= "WHERE codsuc = ".$codsuc." AND codciclo = ".$codciclo." ";
        $SqlA .= "GROUP BY anio ";
        $SqlA .= "ORDER BY ".$this->Convert("anio", "INTEGER")." DESC";

        $consulta = $conexion->prepare($SqlA);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='anio' name='anio' class='select' style='width:70px' ".$event." >";
        echo "<option value='0'>".utf8_encode("...")."</option>";

        foreach ($items as $row) {
            echo "<option value='".$row["anio"]."' >".$row["anio"]."</option>";
        }

        echo "</select>";
    }

    function drop_mes($codsuc, $codciclo, $anio, $event = "") {
        global $conexion, $meses;

        if ($event != '') {
            $event = "onchange='ver(this.value)'";
        }

        $SqlM = "SELECT mes FROM facturacion.periodofacturacion ";
        $SqlM .= "WHERE codsuc = ".$codsuc." AND codciclo = ".$codciclo." AND anio = '".$anio."' ";
        $SqlM .= "ORDER BY ".$this->Convert("mes", "INTEGER")." ";

        $consulta = $conexion->prepare($SqlM);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='mes' name='mes' class='select' style='width:110px' ".$event." >";
        echo "<option value='0'>".utf8_encode("...")."</option>";
        foreach ($items as $row) {
            echo "<option value='".$row["mes"]."' >".$meses[$row["mes"]]."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_conceptos($seleccion = 1000, $event = '') {
        global $conexion;

        $sql = $this->Sentencia("tipoconceptos")." WHERE estareg=1 ORDER BY codtipoconcepto";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='tipoconceptos' name='tipoconceptos' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Tipo de Concepto ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoconcepto"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoconcepto"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_actividad_conceptos($seleccion = 1000) {
        global $conexion;

        $sql = $this->Sentencia("actividadconcepto")." WHERE estareg=1 ORDER BY codactividadconcepto";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='codactividadconcepto' name='codactividadconcepto' class='select' style='width:220px' >";
        echo "<option value='0'>.:: Seleccione la Actividad ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codactividadconcepto"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codactividadconcepto"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_forma_pago($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = $this->Sentencia("formapago")." WHERE estareg=1 ORDER BY codformapago";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='formapago' name='formapago' class='select' style='width:250px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Forma Pago ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codformapago"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codformapago"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_car($codsuc, $seleccion = 0) {
        global $conexion;
        $default = array('1' => 9, '2' => 4, '3' => 2);
        if ($seleccion == 0) {
            $seleccion = $default[$codsuc];
        }
        $sql = $this->Sentencia("car")." WHERE estareg=1 and codsuc=? ORDER BY car";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='car' name='car' class='select' style='width:220px' >";
        echo "<option value='0'>.:: Seleccione el Car ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["car"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["car"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_usuarios_sucursales($codsuc) {
        global $conexion;
        $default = array('1' => 'ADMIN', '2' => 'ADMIN2', '3' => 'ADMIN3');
        $sql = "select * from seguridad.usuarios WHERE estareg=1 and codsuc=? and codcargo in(1,2)";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();
        $seleccion = $default[$codsuc];
        echo "<select id='user' name='user' class='select' style='width:200px' >";
        echo "<option value='0'>.:: Seleccione el Usuario ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["login"]) {
                $selected = " selected='selected' ";
            }
            echo "<option value='".$row["login"]."' ".$selected." >".strtoupper($row["login"])."</option>";
        }
        echo "</select>";
    }

    function drop_banco($seleccion = 1000) {
        global $conexion;

        $sql = "select * from public.bancos WHERE estareg=1";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='bancos' name='bancos' class='select' style='width:200px' >";
        echo "<option value='0'>.:: Seleccione el Banco ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codbanco"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codbanco"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_cta_banco($seleccion = 1000) {
        global $conexion;

        $sql = "select * from public.tipoctasbanco WHERE estareg=1";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='tipoctasbanco' name='tipoctasbanco' class='select' style='width:200px' >";
        echo "<option value='0'>.:: Seleccione el Tipo de Cta. ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoctabanco"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoctabanco"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_monedas($seleccion = 1000) {
        global $conexion;

        $sql = "select * from public.monedas WHERE estareg=1";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='monedas' name='monedas' class='select' style='width:200px' >";
        echo "<option value='0'>.:: Seleccione la Moneda ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmoneda"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmoneda"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_deuda($seleccion = 1000) {
        global $conexion;

        $sql = $this->Sentencia("tipodeuda")." WHERE estareg=1";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='tipodeuda' name='tipodeuda' class='select' style='width:220px' >";
        echo "<option value='0'>.:: Seleccione el Tipo de Deuda ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipodeuda"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipodeuda"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_inspector($codsuc, $seleccion = 10000) {
        global $conexion;

        $sql = $this->Sentencia("inspectores")." WHERE i.codsuc=? ORDER BY codinspector";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='inspectores' name='inspectores' class='select' style='width:220px' >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codinspector"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codinspector"]."' ".$selected." >".strtoupper($row["nombres"])."</option>";
        }
        echo "</select>";
    }

    function drop_motivo_corte($tipooperacion) {
        global $conexion;

        $sql = $this->Sentencia("motivocorte")." WHERE tipooperacion=? and estareg=1 ORDER BY codmotivocorte";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($tipooperacion));
        $items = $consulta->fetchAll();

        echo "<select id='motivocorte' name='motivocorte' class='select' style='width:220px' >";
        foreach ($items as $row) {
            echo "<option value='".$row["codmotivocorte"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_cajas($codsuc, $event = "") {
        global $conexion;

        $sql = "select cjU.nrocaja,c.descripcion 
                    from cobranza.cajasxusuario as cjU
                    inner join cobranza.cajas as c on(cjU.nrocaja=c.nrocaja)
                    WHERE cjU.codsuc=?
                    group by cjU.nrocaja,c.descripcion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='caja' name='caja' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Caja ::.</option>";
        foreach ($items as $row) {
            echo "<option value='".$row["nrocaja"]."' >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_reservorio($codsuc, $seleccion = 100000) {
        global $conexion;

        $sql = $this->Sentencia("reservorios")." WHERE codsuc=?";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='reservorios' name='reservorios' class='select' style='width:220px' >";
        echo "<option value='0'>.:: Seleccione el Reservorio ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codreservorio"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codreservorio"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_zonas_abastecimiento_sector($codsuc, $codsector, $seleccion = 10000, $event = "") {
        global $conexion;

        $sql = "SELECT s.codzona, z.descripcion ";
        $sql .= "FROM public.zonasabastecimiento_sectores s ";
        $sql .= " INNER JOIN public.zonasabastecimiento z ON(s.codemp = z.codemp) AND (s.codsuc = z.codsuc) AND (s.codzona = z.codzona) ";
        $sql .= "WHERE s.codsuc = ".$codsuc." ";
        $sql .= " AND s.codsector = ".$codsector." ";
        $sql .= "GROUP BY s.codzona, z.descripcion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='zonasbastecimiento' name='zonasbastecimiento' class='select' style='width:250px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Zona de Abastecimiento ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codzona"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codzona"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_rutas_distribucion($codsuc, $codsector) {
        global $conexion;

        $sql = "select codrutdistribucion,descripcion from public.rutasmaedistribucion WHERE codsuc=? and codsector=? ORDER BY codrutdistribucion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc, $codsector));
        $items = $consulta->fetchAll();

        echo "<select id='rutasdistribucion' name='rutasdistribucion' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Ruta de Distribucion ::.</option>";
        foreach ($items as $row) {
            echo "<option value='".$row["codrutdistribucion"]."' >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipo_parentesco($seleccion = 1000, $event = "") {
        global $conexion;

        $consulta = $conexion->prepare($this->Sentencia("tipoparentesco")." WHERE estareg=1");
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoparentesco' name='tipoparentesco' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Tipo de Parentesco ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoparentesco"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoparentesco"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_medio_reclamo($seleccion = 1000, $event = "") {
        global $conexion;

        $consulta = $conexion->prepare($this->Sentencia("medio_reclamo")." WHERE estareg=1");
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='medioreclamo' name='medioreclamo' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Tipo de Parentesco ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmedio"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmedio"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_areas($codsuc, $seleccion = 1000, $event = "") {
        global $conexion;

        $consulta = $conexion->prepare($this->Sentencia("areas")." WHERE s.codsuc=? ORDER BY codarea");
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='areas' name='areas' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codarea"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codarea"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_usuarios_areas($codsuc, $codarea) {
        global $conexion;

        $sql = "select codusu,nombres from seguridad.usuarios WHERE codarea=? and codsuc=? AND estareg=1";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codarea, $codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='usuario_area' name='usuario_area' class='select' style='width:220px' >";
        echo "<option value='0'>.:: Seleccione el Usuario ::.</option>";
        foreach ($items as $row) {
            echo "<option value='".$row["codusu"]."' ".$selected." >".strtoupper($row["nombres"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipocatcostos($codtipocatcosto) {
        global $conexion;

        $sql = "select * from public.tipocatcostos WHERE estareg=1";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        echo "<select id='codtipocatcosto' name='codtipocatcosto' class='select' style='width:220px' >";
        echo "<option value='0'>.:: Seleccione la Categoria ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($codtipocatcosto == $row[0]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row[0]."' ".$selected." >".strtoupper($row[1])."</option>";
        }
        echo "</select>";
    }

    /* agregar estos 2 metodos */

    function drop_grupocostos($seleccion = 1000, $event = "") {
        $items = $this->setGrupoCostos(" WHERE estareg=1");

        echo "<select id='grupocostos' name='grupocostos' class='select' style='width:250px' ".$event." >";
        echo "<option value='0'>.:: Seleccione el Grupo de Costos ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codgrupocostos"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codgrupocostos"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipopresupuesto($seleccion = 1000, $event = "") {
        $items = $this->setTipoPresupuesto(" WHERE estareg=1 ORDER BY codtipopresupuesto ASC");

        echo "<select id='codtipopresupuesto' name='codtipopresupuesto' class='select' style='width:500px' ".$event." onchange='UpdTilidad()' >";
        echo "<option value='0'>.:: Seleccione el Tipo de Presupuesto ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipopresupuesto"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipopresupuesto"]."' ".$selected." data-utilidad='".round($row['utilidad'], 2)."' >".strtoupper($row["code"]." ".$row["descripcion"]."-".$row["descripcion2"])."</option>";
        }
        echo "</select>";
    }

    /* code */
    /* ------- GIS -------------------- */

    function drop_provGis($seleccion = 10000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.provincia WHERE estareg=1 ORDER BY descripcion ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        $cmb = "<select id='codprovincia' name='codprovincia' class='select' style='width:220px' ".$event." >";
        $cmb.="<option value='0'>.:: Seleccione Provincia ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["provincia"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["provincia"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_distGis($codProv, $seleccion, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.distrito WHERE estareg=1 AND provincia=".$codProv." ORDER BY descripcion ASC";
        $consulta = $conexion->query($sql);

        //$consulta->execute(array($seleccion));
        $items = $consulta->fetchAll();

        $cmb = "<select id='coddistrito' name='coddistrito' class='select' style='width:220px' ".$event." >";
        $cmb.="<option value='0'>.:: Seleccione Distrito ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row['distrito']) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["distrito"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_localidadtGis($codsuc, $seleccion, $event) {
        global $conexion;

        $sql = "SELECT * FROM gis.localidad WHERE estareg=1 ORDER BY descripcion ASC";
        $consulta = $conexion->query($sql);
        //$consulta->execute(array($seleccion));
        $items = $consulta->fetchAll();

        $cmb = "<select id='codlocalidad' name='codlocalidad' class='select' style='width:220px' ".$event." >";
        $cmb.="<option value='0'>.:: Seleccione Localidad ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["localidad"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["localidad"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_zonasGis($codlocalidad, $seleccion, $event) {
        global $conexion;

        $sql = "SELECT * FROM gis.zonacatastral WHERE estareg=1 AND localidad=? ORDER BY descripcion ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codlocalidad));
        $items = $consulta->fetchAll();

        $cmb = "<select id='zonacatastral' name='zonacatastral' class='select' style='width:220px' ".$event." >";
        $cmb.="<option value='0'>.:: Seleccione la Zona ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["zona_cat"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["zona_cat"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_sectoresGis($codsuc, $codzona, $seleccion = 10000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.sectores WHERE estareg=1 AND zona_cat=? AND codsuc=? ORDER BY codsector ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codzona, $codsuc));
        $items = $consulta->fetchAll();

        $cmb = "<select id='codsector' name='codsector' class='select' style='width:220px' ".$event." >";
        $cmb.="<option value='0'>.:: Seleccione el Sector ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codsector"]) {
                $selected = "selected='selected'";
            }
            $cmb.="<option value='".$row["codsector"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        $cmb.="</select>";
        return $cmb;
    }

    function drop_manzanasGis($codsuc, $codsector, $seleccion = 0, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.manzanas WHERE codsuc=$codsuc and codsector=$codsector ORDER BY descripcion ASC";
        $consulta = $conexion->query($sql);
        //$consulta->execute(array($codsuc, $codsector));
        $items = $consulta->fetchAll();

        echo "<select id='manzanas' name='manzanas' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Manzana ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmanzanas"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmanzanas"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_barrioGis($codsuc, $seleccion = 0, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.barrio WHERE codsuc=".$codsuc." ORDER BY descripcion ASC";
        $consulta = $conexion->query($sql);
        //$consulta->execute(array($codsuc, $codsector));
        $items = $consulta->fetchAll();

        echo "<select id='codbarrio' name='codbarrio' class='select' style='width:220px' ".$event." >";
        echo "<option value='0'>.:: Seleccione la Barrio ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["cod_urb"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["cod_urb"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_sit_lote($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.estadolote WHERE estareg=1 ORDER BY codestadolote";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='estadolote' name='estadolote' class='select' style='width:220px' ".$event." >";
        //echo "<option value='0'>--Seleccione el Estado de Servicio--</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadolote"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadolote"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tconstruccionGis($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipoconstruccion WHERE estareg=1 ORDER BY codtipoconstruccion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoconstruccion' name='tipoconstruccion' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoconstruccion"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoconstruccion"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_calzada($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipovereda WHERE estareg=1 ORDER BY codtipovereda";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipovereda' name='tipovereda' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipovereda"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipovereda"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_piscina($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.piscina WHERE estareg=1 ORDER BY codpiscina";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codpiscina' name='codpiscina' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codpiscina"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codpiscina"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_talmacenajeGis($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipoalmacenaje WHERE estareg=1 ORDER BY codtipoalmacenaje";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipoalmacenaje' name='tipoalmacenaje' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipoalmacenaje"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipoalmacenaje"]."' ".$selected." >".utf8_decode($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tomatconstruccion($seleccion = 10000, $and = "", $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipomatconstruccion WHERE estareg=1 ORDER BY codtipomatconstruccion ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipomatconstruccion' name='tipomatconstruccion' class='select' style='width:220px' ".$event." >";

        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipomatconstruccion"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipomatconstruccion"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipomatvereda($seleccion = 10000, $and = "", $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipomatvereda WHERE estareg=1 ORDER BY codtipomatvereda ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='tipomatvereda' name='tipomatvereda' class='select' style='width:220px' ".$event." >";

        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipomatvereda"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipomatvereda"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estvereda($seleccion = 10000, $and = "", $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.estadovereda WHERE estareg=1 ORDER BY codestadovereda ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codestadovereda' name='codestadovereda' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadovereda"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadovereda"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo"</select>";
        //return $cmb;
    }

    function drop_cattaridaria($seleccion = 10000, $and = "", $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.categoriatarifaria WHERE estareg=1 ORDER BY codcategoriatar ASC";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codcategoriatar' name='codcategoriatar' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codcategoriatar"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codcategoriatar"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo"</select>";
        //return $cmb;
    }

    function drop_grupoopcupacional($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.grupo_ocupacional  WHERE estareg=1 ORDER BY descripcion ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codgrupo_ocupacional' name='codgrupo_ocupacional' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codgrupo_ocupacional"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codgrupo_ocupacional"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tcajadagua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.cajadagua WHERE estareg=1 ORDER BY codcajadagua ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codcajadagua' name='codcajadagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codcajadagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codcajadagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_accesibilidadagua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.acceso_agua WHERE estareg=1 ORDER BY descripcion ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codacceso_agua' name='codacceso_agua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codacceso_agua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codacceso_agua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_matcaja_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.matcajaagua WHERE estareg=1 ORDER BY codmatcajaagua ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codmatcajaagua' name='codmatcajaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmatcajaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmatcajaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_mattapa_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.mattapaagua WHERE estareg=1 ORDER BY codmattapaagua ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codmattapaagua' name='codmattapaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmattapaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmattapaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estadotapaagua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.estadotapaagua WHERE estareg=1 ORDER BY codestadotapaagua ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codestadotapaagua' name='codestadotapaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadotapaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadotapaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estadocajaagua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.estadocajaagua WHERE estareg=1 ORDER BY codestadocajaagua ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codestadocajaagua' name='codestadocajaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadocajaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadocajaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_sitcajaagua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.sitcajaagua WHERE estareg=1 ORDER BY codsitcajaagua ASC ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codsitcajaagua' name='codsitcajaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codsitcajaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codsitcajaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_localizacion_cajaagua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.locacajaagua WHERE estareg=1 ORDER BY codlocacajaagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codlocacajaagua' name='codlocacajaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codlocacajaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codlocacajaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_elconex_ag($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.elementoconexagua WHERE estareg=1 ORDER BY codelementoconexagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codelementoconexagua' name='codelementoconexagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codelementoconexagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codelementoconexagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_diametroconex_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.diametrosagua WHERE estareg=1 ORDER BY coddiametrosagua";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='coddiametrosagua' name='coddiametrosagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["coddiametrosagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["coddiametrosagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_matconex_agua($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.matconexaagua WHERE estareg=1 ORDER BY codmatconexaagua";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codmatconexaagua' name='codmatconexaagua' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmatconexaagua"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmatconexaagua"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_legibilidadmed($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.legibilidadmed WHERE estareg=1 ORDER BY codlegibilidadmed";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codlegibilidadmed' name='codlegibilidadmed' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codlegibilidadmed"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codlegibilidadmed"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_legibilidadlect($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.legibilidadmed WHERE estareg=1 ORDER BY codlegibilidadmed";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codlegibilidadmed' name='codlegibilidadmed' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codlegibilidadmed"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codlegibilidadmed"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_diametromed($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.diametrosmedidor WHERE estareg=1 ORDER BY coddiametrosmedidor";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='coddiametrosmedidor' name='coddiametrosmedidor' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["coddiametrosmedidor"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["coddiametrosmedidor"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipomedidorGis($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipomedidor WHERE estareg=1 ORDER BY codtipomedidor";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codtipomedidor' name='codtipomedidor' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipomedidor"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipomedidor"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_marcamed($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.marcamedidor WHERE estareg=1 ORDER BY codmarca";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codmarca' name='codmarca' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmarca"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmarca"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_posicionmed($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.posicionmedidor WHERE estareg=1 ORDER BY codposicionmedidor";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codposicionmedidor' name='codposicionmedidor' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codposicionmedidor"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codposicionmedidor"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_protector($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.protector WHERE estareg=1 ORDER BY codprotector";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codprotector' name='codprotector' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codprotector"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codprotector"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tcajadesague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.cajadesague WHERE estareg=1 ORDER BY codcajadesague ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codcajadesague' name='codcajadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codcajadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codcajadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_accesibilidaddesague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.acceso_desague WHERE estareg=1 ORDER BY descripcion ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codacceso_desague' name='codacceso_desague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codacceso_desague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codacceso_desague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_mattapa_des($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.mattapadesague WHERE estareg=1 ORDER BY codmattapadesague ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codmattapaagua' name='codmattapadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmattapadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmattapadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estadotapadesague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.estadotapadesague WHERE estareg=1 ORDER BY codestadotapadesague ";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codestadotapadesague' name='codestadotapadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadotapadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadotapadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_matcaja_desague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.matcajadesague WHERE estareg=1 ORDER BY codmatcajadesague ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codmatcajadesague' name='codmatcajadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmatcajadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmatcajadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_estadocajadesague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.estadocajadesague WHERE estareg=1 ORDER BY codestadocajadesague ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codestadocajadesague' name='codestadocajadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadocajadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadocajadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_sitcajadesague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.sitcajadesague WHERE estareg=1 ORDER BY codsitcajadesague ASC ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codsitcajadesague' name='codsitcajadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codsitcajadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codsitcajadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_localizacion_cajadesague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.locacajadesague WHERE estareg=1 ORDER BY codlocacajadesague";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codlocacajadesague' name='codlocacajadesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codlocacajadesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codlocacajadesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_elconex_des($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.elementoconexdes WHERE estareg=1 ORDER BY codelementoconexdes";

        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codelementoconexdes' name='codelementoconexdes' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codelementoconexdes"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codelementoconexdes"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_est_serviciodesa($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.estadoserviciodes WHERE estareg=1 ORDER BY codestadoserviciodes";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codestadoserviciodes' name='codestadoserviciodes' class='select' style='width:220px' ".$event." >";
        //echo "<option value='0'>.:: Seleccione el Estado de Servicio ::.</option>";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codestadoserviciodes"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codestadoserviciodes"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_diametrodesague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.diametrosdesague WHERE estareg=1 ORDER BY coddiametrosdesague ";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='coddiametrosdesague' name='coddiametrosdesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["coddiametrosdesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["coddiametrosdesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_tipodes($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.tipodesague WHERE estareg=1 ORDER BY codtipodesague";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codtipodesague' name='codtipodesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codtipodesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codtipodesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_matconex_desague($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.matconexdesague WHERE estareg=1 ORDER BY codmatconexdesague";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codmatconexdesague' name='codmatconexdesague' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codmatconexdesague"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codmatconexdesague"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_parentesco_ent_tit($seleccion = 1000, $event = "") {
        global $conexion;

        $sql = "SELECT * FROM gis.parentesco WHERE estareg=1 ORDER BY codparentesco";
        $consulta = $conexion->prepare($sql);
        $consulta->execute();
        $items = $consulta->fetchAll();

        echo "<select id='codparentesco' name='codparentesco' class='select' style='width:220px' ".$event." >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codparentesco"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codparentesco"]."' ".$selected." >".strtoupper($row["descripcion"])."</option>";
        }
        echo "</select>";
    }

    function drop_inspectorGis($codsuc, $seleccion = 10000) {
        global $conexion;

        $sql = "SELECT * FROM gis.inspectores WHERE codsuc=? ORDER BY codinspector";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='codinspector' name='codinspector' class='select' style='width:220px' >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codinspector"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codinspector"]."' ".$selected." >".strtoupper($row["nombres"])."</option>";
        }
        echo "</select>";
    }

    function drop_supervisorGis($codsuc, $seleccion = 10000) {
        global $conexion;

        $sql = "SELECT * FROM gis.supervisor WHERE codsuc=? ORDER BY codsupervisor";
        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($codsuc));
        $items = $consulta->fetchAll();

        echo "<select id='codsupervisor' name='codsupervisor' class='select' style='width:220px' >";
        foreach ($items as $row) {
            $selected = "";
            if ($seleccion == $row["codsupervisor"]) {
                $selected = "selected='selected'";
            }
            echo "<option value='".$row["codsupervisor"]."' ".$selected." >".strtoupper($row["nombres"])."</option>";
        }
        echo "</select>";
    }

}

?>
