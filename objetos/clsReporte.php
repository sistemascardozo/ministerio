<?php

session_name("pnsu");
if (!session_start()) {
    session_start();
}

ini_set("memory_limit", "1024M");
set_time_limit(0);

include($_SESSION['path']."objetos/pdf/fpdf.php");

require_once ($_SESSION['path']."objetos/jpgraph/src/jpgraph.php");
require_once ($_SESSION['path']."objetos/jpgraph/src/jpgraph_bar.php");

set_time_limit(0);

header('Content-type: text/html; charset=utf-8');

class clsReporte extends FPDF {

    var $widths;
    var $aligns;

    function Header() {
        global $codsuc, $Sede;

        $empresa = $this->datos_empresa($codsuc);
        $this->SetFont('Arial', '', 6);
        $this->SetTextColor(0, 0, 0);
        $tit1 = strtoupper($empresa["razonsocial"]);
        $tit2 = strtoupper($empresa["direccion"]);
        $tit3 = "RUC : ".strtoupper($empresa["ruc"]);
        $tit4 = "SEDE: ".strtoupper($empresa["descripcion"]);

        $x = 35;
        $y = 5;
        $h = 3;

        $this->Image($_SESSION['path']."images/logo_recibo.jpg", 10, 4, 25, 15);
        $this->SetXY($x, $y);
        $this->SetFont('Arial', '', 6);
        $this->Cell(85, $h, utf8_decode($tit1), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(85, $h, utf8_decode($tit2), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit3), 0, 1, 'L');
        $this->SetX($x);
        $this->Cell(150, $h, utf8_decode($tit4), 0, 0, 'L');

        $this->SetY(8);
        //$NroPag='Pag. '.$this->PageNo().' de {nb}';
        //$this->Cell(0, 3, trim($NroPag), 0, 1, 'R');
        $FechaActual = date('d/m/Y');
        $Hora = date('h:i:s a');


        $this->Image($_SESSION['path']."images/semapam.jpg", 170, 4, 25, 15);
        $this->SetXY($x, $y);//
        // $this->Cell(0, 3, "Fecha     : ".$FechaActual."   ", 0, 1, 'R');
        // $this->Cell(0, 3, "Hora       : ".$Hora." ", 0, 1, 'R');
         $this->SetY(20);
        $this->SetLineWidth(.1);
        $this->Cell(0, .1, "", 1, 1, 'C', true);

        $this->Ln(5);

        $this->cabecera();
    }

    function BorrarCabecera() {
        $this->SetXY(1, 1);
        $this->SetFillColor(255, 255, 255); //Color de Fondo
        $this->SetTextColor(0);
        $this->Cell(0, 20, '', 0, 1, 'C', true);
    }

    function SetWidths($w) {
        $this->widths = $w;
    }

    function SetAligns($a) {
        $this->aligns = $a;
    }

    function fill($f) {
        $this->fill = $f;
    }

    function Row($data) {
        $nb = 0;

        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        $h = 5 * $nb;

        $this->CheckPageBreak($h);

        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            $x = $this->GetX();
            $y = $this->GetY();
            $this->Rect($x, $y, $w, $h, $style);
            $this->MultiCell($w, 5, $data[$i], 'RML', $a, $fill);
            $this->SetXY($x + $w, $y);
        }
        $this->Ln($h);
    }

    function Row2($data) {
        $nb = 0;

        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));

        $h = 3 * $nb;

        $this->CheckPageBreak($h);

        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            $x = $this->GetX();
            $y = $this->GetY();
            $this->Rect($x, $y, $w, $h, $style);
            $this->MultiCell($w, 3.5, $data[$i], 'RML', $a, $fill);
            $this->SetXY($x + $w, $y);
        }
        $this->Ln($h);
    }

    function CheckPageBreak($h) {
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt) {
        $cw = &$this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;

        $s = str_replace('\r', '', $txt);

        $nb = strlen($s);

        if ($nb > 0 and $s[$nb - 1] == '\n')
            $nb–;

        $sep = -1;

        $i = 0;

        $j = 0;

        $l = 0;

        $nl = 1;

        while ($i < $nb) {
            $c = $s[$i];

            if ($c == '\n') {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;

                continue;
            }
            if ($c == '')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {

                if ($sep == -1) {

                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }

        return $nl;
    }

    function Footer() {
        $this->SetY(-10);
        $this->SetFont('Arial', 'I', 6);
        $this->SetTextColor(0);
        $this->SetLineWidth(.1);
        $this->Cell(0, .1, "", 1, 1, 'C', true);
        $this->Ln(1);
        $this->Cell(0, 4, utf8_decode('Sistema de Administracion y Gestion de Servicios de Saneamiento'), 0, 0, 'L');
        $this->Cell(0, 4, 'Pag. '.$this->PageNo().' de {nb}', 0, 0, 'R');
    }

    function generargraficos($nroinscripcion, $codsuc) {
        global $meses, $mesesconsumo, $conexion, $Sede;

        $paramae = $this->getParamae("LIMLEC", $codsuc);

        $mesesconsumo = array();
        $meslit = array();
        $count = 0;

        $carpetaimg = $_SESSION['path']."sigco/facturacion/operaciones/impresion/graficos/";
        //chmod($carpetaimg,7777);

        $img = $carpetaimg."consumo_".$nroinscripcion.".png";
        /* 				chmod($img,0777); */

        if (file_exists($img)) {
            unlink($img);
        }

        $top = '';
        $limit = "limit ".$paramae["valor"];

        $sql = "SELECT ".$top." anio,mes,consumo from facturacion.cabfacturacion
				      where codsuc=".$codsuc." and nroinscripcion=".$nroinscripcion."
				      order by ".$this->Convert("anio", "integer")." desc ,
				      			".$this->Convert("mes", "integer")."  desc ".$limit;

        $consulta = $conexion->query($sql);
        $items = $consulta->fetchAll();
        foreach ($items as $row) {

            $mesesconsumo[$count] = $row["consumo"]; //number_format($row["consumo"],0);
            $meslit[$count] = $meses[$row["mes"]];
            $count++;
        }
        if ($count == 0)
            return false;
        $datosy = array_merge($mesesconsumo);

        // Creamos el grafico
        $grafico = new Graph(500, 250);
        $grafico->SetScale('textlin');

        $grafico->xaxis->SetTickLabels(array_merge($meslit));
        // Ajustamos los margenes del grafico-----    (left,right,top,bottom)
        $grafico->SetMargin(40, 30, 30, 40);

        // Creamos barras de datos a partir del array de datos
        $bplot = new BarPlot($datosy);

        // Configuramos color de las barras
        $bplot->SetFillColor('#479CC9');

        // Queremos mostrar el valor numerico de la barra
        $bplot->value->Show();

        //A�adimos barra de datos al grafico
        $grafico->Add($bplot);


        // Configuracion de los titulos
        $grafico->title->Set('Evolucion de Consumo');
        $grafico->yaxis->title->Set('Consumo');
        $grafico->xaxis->title->Set('Meses Consumido');

        $grafico->title->SetFont(FF_FONT1, FS_BOLD);
        $grafico->yaxis->title->SetFont(FF_FONT1, FS_BOLD);
        $grafico->xaxis->title->SetFont(FF_FONT1, FS_BOLD);

        // Se muestra el grafico
        $grafico->Stroke($img);

        return $img;
    }

    function calcular_deuda_ant($nroinscripcion, $codsuc, $NroFacturacion = '') {
        global $conexion;

        if ($NroFacturacion != '') {
            $Cond = "AND df.nrofacturacion < ".$NroFacturacion." ";
        }

        $sql = "SELECT df.nrofacturacion, SUM(df.importe) AS importe ";
        $sql .= "FROM facturacion.detfacturacion df ";
        $sql .= "WHERE df.nroinscripcion = ? ";
        $sql .= " AND df.codsuc = ? ";
        $sql .= " AND df.categoria = 1 ";
        $sql .= " ".$Cond." ";
        $sql .= " AND df.estadofacturacion = 1 ";
        $sql .= "GROUP BY df.nrofacturacion ";
        $sql .= "ORDER BY df.nrofacturacion";

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array($nroinscripcion, $codsuc));
        $items = $consulta->fetchAll();

        $meses = 0;
        $totdeuda = 0;

        foreach ($items as $row) {
            $meses++;
            $totdeuda += $row["importe"];
        }

        return array("meses" => "Deuda (".$meses.") Mes(es)", "deuda" => $totdeuda);
    }

    function calcular_deuda($nroinscripcion, $codsuc, $NroFacturacion = '', $tipo = 0, $periodo) {

        global $conexion;

        if (empty($periodo)) {

            if ($NroFacturacion != '') {
                $Cond = " AND d.nrofacturacion < ".$NroFacturacion." ";
            }
        } else {
            $Cond = '';
        }

        switch ($tipo) {
            case 0:
                $sql1 = " SUM(d.importe - d.importerebajado - d.imppagado) AS importe ";
                $sql2 = " AND d.estadofacturacion = 1 ";
                $sql = "SELECT d.nrofacturacion, ".$sql1." ";
                $sql .= "FROM facturacion.detfacturacion d ";
                $sql .= " INNER JOIN facturacion.cabfacturacion cf ON (cf.codemp = d.codemp) AND (cf.codsuc = d.codsuc) AND (cf.nrofacturacion = d.nrofacturacion) AND (cf.nroinscripcion = d.nroinscripcion)  AND (cf.codciclo = d.codciclo) ";
                $sql .= "WHERE d.nroinscripcion = ".$nroinscripcion." ";
                $sql .= " AND d.codsuc = ".$codsuc." ";
                $sql .= " AND d.estadofacturacion = 1 ";
                $sql .= $sql2;
                $sql .= $Cond;
                $sql .= " AND d.categoria = 1 ";
                $sql .= " AND cf.enreclamo = 0 ";
                $sql .= "GROUP BY d.nrofacturacion  ";
                $sql .= "HAVING SUM(d.importe - (d.importerebajado)) > 0 ";
                $sql .= "ORDER BY d.nrofacturacion";

                break;
            case 1:
                $sql1 = " SUM(d.importe) AS importe ";
                $sql = "SELECT d.nrofacturacion, ".$sql1." ";
                $sql .= "FROM facturacion.detctacorriente d ";
                $sql .= "WHERE d.nroinscripcion = ".$nroinscripcion." ";
                $sql .= " AND d.codsuc = ".$codsuc." ";
                $sql .= $sql2;
                $sql .= $Cond;
                $sql .= " AND d.categoria = 1 AND d.periodo = '$periodo' ";
                $sql .= "GROUP BY d.nrofacturacion  ";
                $sql .= "HAVING SUM(d.importe - (d.importerebajado)) > 0 ";
                $sql .= "ORDER BY d.nrofacturacion";

                break;
//            case 2:
//                $sql1 = " SUM(d.importe - d.importerebajado) AS importe ";
//                $sql2 = " AND d.estadofacturacion = 1 ";
//                $sql = "SELECT d.nrofacturacion, ".$sql1." ";
//                $sql .= "FROM facturacion.detctacorriente d ";
//                $sql .= "WHERE d.nroinscripcion = ".$nroinscripcion." ";
//                $sql .= " AND d.codsuc = ".$codsuc." ";
//                $sql .= $sql2;
//                $sql .= $Cond;
//                $sql .= " AND d.categoria = 1 AND d.periodo = '$periodo' ";
//                $sql .= "GROUP BY d.nrofacturacion  ";
//                $sql .= "HAVING SUM(d.importe - (d.importerebajado)) > 0 ";
//                $sql .= "ORDER BY d.nrofacturacion";
//
//                break;
            case 2:
                $sql = "SELECT d.nrofacturacion, SUM(d.importe-d.importerebajado) as importe
                    FROM facturacion.detfacturacion d
                    INNER JOIN facturacion.cabfacturacion cf ON (cf.codemp = d.codemp) AND (cf.codsuc = d.codsuc) AND (cf.nrofacturacion = d.nrofacturacion) AND (cf.nroinscripcion = d.nroinscripcion)  AND (cf.codciclo = d.codciclo)
                    WHERE d.codsuc=".$codsuc." AND d.nroinscripcion=".$nroinscripcion." AND d.estadofacturacion=1 AND d.categoria=1 AND cf.enreclamo=0 
                    AND d.codtipodeuda NOT IN (12)
                    GROUP BY d.nrofacturacion
                    HAVING SUM(d.importe-d.importerebajado)>0 
                    ORDER BY d.nrofacturacion ";
                break;
                
            case 3:

                $sql1 = " SUM(d.importe - d.importerebajado) AS importe ";
                $sql = "SELECT d.nrofacturacion, ".$sql1." ";
                $sql .= "FROM facturacion.detctacorriente d ";
                $sql .= "WHERE d.nroinscripcion = ".$nroinscripcion." ";
                $sql .= " AND d.codsuc = ".$codsuc." ";
                $sql .= $sql2;
                $sql .= $Cond;
                $sql .= " AND d.categoria = 1 AND d.periodo = '$periodo' ";
                $sql .= "GROUP BY d.nrofacturacion  ";
                $sql .= "HAVING SUM(d.importe - (d.importerebajado)) > 0 ";
                $sql .= "ORDER BY d.nrofacturacion";

                break;
        }

        $consulta = $conexion->prepare($sql);
        $consulta->execute(array());
        $items = $consulta->fetchAll();

        $meses = $totdeuda = 0;

        foreach ($items as $row) {
            $meses++;
            $totdeuda += $row["importe"];
        }

        return array("meses" => "Deuda (".$meses.") Mes(es)", "deuda" => $totdeuda);
    }

    //---Funciones para Crear el Codigo de Barra-----
    function EAN13($x, $y, $barcode, $h = 1, $w = .1) {
        $this->Barcode($x, $y, $barcode, $h, $w, 11);
    }

    function UPC_A($x, $y, $barcode, $h = 16, $w = .35) {
        $this->Barcode($x, $y, $barcode, $h, $w, 12);
    }

    function GetCheckDigit($barcode) {
        //Compute the check digit
        $sum = 0;
        for ($i = 1; $i <= 10; $i+=2)
            $sum+=3 * $barcode[$i];
        for ($i = 0; $i <= 9; $i+=2)
            $sum+=$barcode[$i];
        $r = $sum % 10;
        if ($r > 0)
            $r = 10 - $r;
        return $r;
    }

    function TestCheckDigit($barcode) {
        //Test validity of check digit
        $sum = 0;
        for ($i = 1; $i <= 10; $i+=2)
            $sum+=3 * $barcode[$i];
        for ($i = 0; $i <= 9; $i+=2)
            $sum+=$barcode[$i];
        //return ($sum+$barcode[10])%10==0;
        return 1;
    }

    function Barcode($x, $y, $barcode, $h, $w, $len) {
        //Padding
        $barcode = $barcode; //str_pad($barcode,$len-1,'0',STR_PAD_LEFT);

        if ($len == 10)
            $barcode = $barcode;
        //Add or control the check digit
        //echo $this->TestCheckDigit($barcode);
        if (strlen($barcode) == 10)
            $barcode.=$this->GetCheckDigit($barcode);
        elseif (!$this->TestCheckDigit($barcode))
            $this->Error('Incorrect check digit'.$barcode);
        //Convert digits to bars
        $codes = array(
            'A' => array(
                '0' => '0001101', '1' => '0011001', '2' => '0010011', '3' => '0111101', '4' => '0100011',
                '5' => '0110001', '6' => '0101111', '7' => '0111011', '8' => '0110111', '9' => '0001011'),
            'B' => array(
                '0' => '0100111', '1' => '0110011', '2' => '0011011', '3' => '0100001', '4' => '0011101',
                '5' => '0111001', '6' => '0000101', '7' => '0010001', '8' => '0001001', '9' => '0010111'),
            'C' => array(
                '0' => '1110010', '1' => '1100110', '2' => '1101100', '3' => '1000010', '4' => '1011100',
                '5' => '1001110', '6' => '1010000', '7' => '1000100', '8' => '1001000', '9' => '1110100')
        );
        $parities = array(
            '0' => array('A', 'A', 'A', 'A', 'A', 'A'),
            '1' => array('A', 'A', 'B', 'A', 'B', 'B'),
            '2' => array('A', 'A', 'B', 'B', 'A', 'B'),
            '3' => array('A', 'A', 'B', 'B', 'B', 'A'),
            '4' => array('A', 'B', 'A', 'A', 'B', 'B'),
            '5' => array('A', 'B', 'B', 'A', 'A', 'B'),
            '6' => array('A', 'B', 'B', 'B', 'A', 'A'),
            '7' => array('A', 'B', 'A', 'B', 'A', 'B'),
            '8' => array('A', 'B', 'A', 'B', 'B', 'A'),
            '9' => array('A', 'B', 'B', 'A', 'B', 'A')
        );
        $code = '101';
        $p = $parities[$barcode[0]];

        for ($i = 1; $i <= 6; $i++)
            $code.=$codes[$p[$i - 1]][$barcode[$i]];
        $code.='01010';
        for ($i = 7; $i <= 10; $i++)
            $code.=$codes['C'][$barcode[$i]];
        $code.='101';
        //Draw bars
        for ($i = 0; $i < strlen($code); $i++) {
            if ($code[$i] == '1')
                $this->Rect($x + $i * $w, $y, $w, $h, 'F');
        }
        //Print text uder barcode
        $this->SetFont('Arial', '', 12);
        $this->Text($x + 8, $y + $h + 11 / $this->k, substr($barcode, 0, $len - 1));
    }

    function obtener_nombre_mes($mes) {
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        return $meses[$mes - 1];
    }

}

?>
