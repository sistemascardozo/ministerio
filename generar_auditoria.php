<?PHP
	include("config.php");

	$SqlAudit = "SELECT c.relname ";
	$SqlAudit .= "FROM pg_class c ";
	$SqlAudit .= " LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ";
	$SqlAudit .= "WHERE c.relkind = 'r' ";
	$SqlAudit .= " AND c.relname NOT ILIKE 'sql_%' ";
	$SqlAudit .= " AND c.relname NOT ILIKE 'tbl_%' ";
	$SqlAudit .= " AND c.relname NOT ILIKE 'pg_%' ";
	$SqlAudit .= " AND c.relname NOT ILIKE 'tmp_%' ";

//Eliminar Triggers
	echo "Configurando Triggers...<br>";

	foreach ($conexion->query($SqlAudit)->fetchAll() as $RowAudit)
	{
		$SqlSchema = "SELECT DISTINCT table_schema ";
		$SqlSchema .= "FROM information_schema.columns ";
		$SqlSchema .= "WHERE table_name = '".$RowAudit[0]."' ";
		
		$RowSchema = $conexion->query($SqlSchema)->fetch();
		
		$SqlD = "DROP TRIGGER ".$RowAudit[0]."_tg_audit ON ".$RowSchema[0].".".$RowAudit[0].";";
		
		$result = $conexion->prepare($SqlD);
        $result->execute(array());		
	}

//Eliminar Funciones
	echo "Configurando Funciones...<br>";
	
	$SqlF = "DROP FUNCTION admin.fn_log_audit();";

	$result = $conexion->prepare($SqlF);
	$result->execute(array());


	$SqlF = "DROP FUNCTION admin.get_session(session_name text, session_tipo integer);";

	$result = $conexion->prepare($SqlF);
	$result->execute(array());

	
	$SqlF = "DROP FUNCTION admin.set_session(session_name text, session_value text, session_ip text);";

	$result = $conexion->prepare($SqlF);
	$result->execute(array());


//Eliminar Tabla de Auditoria
	echo "Configurando Tablas de Aduditoria...<br><br>";

	$SqlT = "DROP TABLE admin.tbl_audit;";

	$result = $conexion->prepare($SqlT);
	$result->execute(array());

//Crear Tabla de Auditoria
	$SqlT = "CREATE TABLE admin.tbl_audit (";
	$SqlT .= " pk_audit SERIAL, TableName CHAR(45) NOT NULL, Operation CHAR(1) NOT NULL, OldValue TEXT, NewValue TEXT, ";
	$SqlT .= " UpdateDate TIMESTAMP WITHOUT TIME ZONE NOT NULL, UserName CHAR(45) NOT NULL, IpAccess VARCHAR, UserAccess VARCHAR, ";
	$SqlT .= " CONSTRAINT pk_audit PRIMARY KEY(pk_audit)) WITHOUT OIDS;";

	$result = $conexion->prepare($SqlT);
	$result->execute(array());

//Crear Funciones
	$SqlF = "CREATE OR REPLACE FUNCTION admin.set_session (session_name text, session_value text, session_ip text) RETURNS pg_catalog.void AS ";
	$SqlF .= "$"."body"."$ ";
	$SqlF .= "BEGIN ";
	$SqlF .= " PERFORM relname ";
	$SqlF .= " FROM pg_class ";
	$SqlF .= " WHERE relname = 'tbl_session' ";
	$SqlF .= "  AND CASE WHEN has_schema_privilege(relnamespace, 'USAGE') THEN pg_table_is_visible(oid) ELSE false END; ";
	$SqlF .= " IF not found THEN ";
	$SqlF .= "  CREATE TEMPORARY TABLE tbl_session (name TEXT, value TEXT, ip TEXT); ";
	$SqlF .= " ELSE ";
	$SqlF .= "  DELETE FROM tbl_session WHERE name = session_name; ";
	$SqlF .= " END IF; ";
	$SqlF .= " INSERT INTO tbl_session VALUES (session_name, session_value, session_ip); ";
	$SqlF .= "END; ";
	$SqlF .= "$"."body"."$ ";
	$SqlF .= "LANGUAGE 'plpgsql' ";
	$SqlF .= "VOLATILE ";
	$SqlF .= "CALLED ON NULL INPUT ";
	$SqlF .= "SECURITY INVOKER ";
	$SqlF .= "COST 100;";

	$result = $conexion->prepare($SqlF);
	$result->execute(array());

	$SqlF = "CREATE OR REPLACE FUNCTION admin.get_session (session_name text, session_tipo integer) RETURNS text AS ";
	$SqlF .= "$"."body"."$ ";
	$SqlF .= "BEGIN ";
	$SqlF .= " PERFORM relname ";
	$SqlF .= " FROM pg_class ";
	$SqlF .= " WHERE relname = 'tbl_session' ";
	$SqlF .= "  AND CASE WHEN has_schema_privilege(relnamespace, 'USAGE') THEN pg_table_is_visible(oid) ELSE false END; ";
	$SqlF .= " IF not found THEN ";
	$SqlF .= "  IF session_tipo = 1 THEN ";
	$SqlF .= "   RETURN NULL; ";
	$SqlF .= "  END IF; ";
	$SqlF .= "  IF session_tipo = 2 THEN ";
	$SqlF .= "   RETURN inet_client_addr(); ";
	$SqlF .= "  END IF; ";
	$SqlF .= " ELSE ";
	$SqlF .= "  IF session_tipo = 1 THEN ";
	$SqlF .= "   RETURN (SELECT value FROM tbl_session WHERE name = session_name); ";
	$SqlF .= "  END IF; ";
	$SqlF .= "  IF session_tipo = 2 THEN ";
	$SqlF .= "   RETURN (SELECT ip FROM tbl_session WHERE name = session_name); ";
	$SqlF .= "  END IF; ";
	$SqlF .= " END IF; ";
	$SqlF .= "END; ";
	$SqlF .= "$"."body"."$ ";
	$SqlF .= "LANGUAGE 'plpgsql' ";
	$SqlF .= "STABLE ";
	$SqlF .= "CALLED ON NULL INPUT ";
	$SqlF .= "SECURITY INVOKER ";
	$SqlF .= "COST 100;";

	$result = $conexion->prepare($SqlF);
	$result->execute(array());

	$SqlF = "CREATE OR REPLACE FUNCTION admin.fn_log_audit () RETURNS trigger AS ";
	$SqlF .= "$"."body"."$ ";
	$SqlF .= "DECLARE ";
	$SqlF .= " UserS varchar; ";
	$SqlF .= " UserIp varchar; ";
	$SqlF .= "BEGIN ";
	$SqlF .= " UserS := (SELECT admin.get_session(pg_backend_pid()::varchar, 1)); ";
	$SqlF .= " UserIp := (SELECT admin.get_session(pg_backend_pid()::varchar, 2)); ";
	$SqlF .= " IF (TG_OP = 'DELETE') THEN ";
	$SqlF .= "  INSERT INTO admin.tbl_audit (TableName, Operation, OldValue, NewValue, UpdateDate, UserName, IpAccess, UserAccess) ";
	$SqlF .= "  VALUES (TG_TABLE_NAME, 'D', OLD, NULL, now(), USER, UserIp, UserS); ";
	$SqlF .= "  RETURN OLD; ";
	$SqlF .= " ELSIF (TG_OP = 'UPDATE') THEN ";
	$SqlF .= "  INSERT INTO admin.tbl_audit (TableName, Operation, OldValue, NewValue, UpdateDate, UserName, IpAccess, UserAccess) ";
	$SqlF .= "  VALUES (TG_TABLE_NAME, 'U', OLD, NEW, now(), USER, UserIp, UserS); ";
	$SqlF .= "  RETURN NEW; ";
	$SqlF .= " ELSIF (TG_OP = 'INSERT') THEN ";
	$SqlF .= "  INSERT INTO admin.tbl_audit (TableName, Operation, OldValue, NewValue, UpdateDate, UserName, IpAccess, UserAccess) ";
	$SqlF .= "  VALUES (TG_TABLE_NAME, 'I', NULL, NEW, now(), USER, UserIp, UserS); ";
	$SqlF .= "  RETURN NEW; ";
	$SqlF .= " END IF; ";
	$SqlF .= " RETURN NULL; ";
	$SqlF .= "END; ";
	$SqlF .= "$"."body"."$ ";
	$SqlF .= "LANGUAGE 'plpgsql' ";
	$SqlF .= "VOLATILE ";
	$SqlF .= "CALLED ON NULL INPUT ";
	$SqlF .= "SECURITY INVOKER ";
	$SqlF .= "COST 100;";

	$result = $conexion->prepare($SqlF);
	$result->execute(array());

//Insertar Triggers
	$SqlAudit .= " AND c.relname NOT ILIKE 'log%' ";
	$SqlAudit .= "ORDER BY c.relname ";

	foreach ($conexion->query($SqlAudit)->fetchAll() as $RowAudit)
	{
		$SqlSchema = "SELECT DISTINCT table_schema ";
		$SqlSchema .= "FROM information_schema.columns ";
		$SqlSchema .= "WHERE table_name = '".$RowAudit[0]."' ";

		$RowSchema = $conexion->query($SqlSchema)->fetch();

		$SqlC = "CREATE TRIGGER ".$RowAudit[0]."_tg_audit AFTER INSERT OR UPDATE OR DELETE ON ".$RowSchema[0].".".$RowAudit[0]." FOR EACH ROW EXECUTE PROCEDURE admin.fn_log_audit();";

		$result = $conexion->prepare($SqlC);
        $result->execute(array());
	}

	echo "Auditor&iacute;a de Tablas Instalada!...<br>";

?>