<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../include/main.php");
	include("../../include/claseindex.php");

	$TituloVentana = "LOG";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objDrop 	= new clsDrop();
	
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>

<script>	
	function Cancelar()
	{
		location.href='index.php';
	}
	$(function() {
		$( "#fecha" ).datepicker(
		{
			showOn: 'button',
			direction: 'up',
			buttonImage: '../../images/iconos/calendar.png',
			buttonImageOnly: true,
			showOn: 'both',
			showButtonPanel: true,

				beforeShow: function(input, inst) 
				{ 
					inst.dpDiv.css({marginTop: (input.offsetHeight ) - 20  + 'px', marginLeft: (input.offsetWidth) - 90 + 'px' }); 
				}
			}
		 );
	});
	function cargar_seguimiento()
	{
		$.ajax({
			 url:'consulta.php',
			 type:'POST',
			 async:true,
			 data:'fecha='+$("#fecha").val()+"&usuario="+$("#usuario").val(),
			 success:function(datos){		 	
				$("#div_seguimiento").html(datos)
			 }
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
 <table width="900" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td width="8%" class="TitDetalle">&nbsp;</td>
	  <td width="92%" class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Fecha :</td>
      <td class="CampoDetalle"><label>
        <input type="text" name="fecha" id="fecha" onchange="cargar_seguimiento();" size="10" maxlength="10" value="<?=date("d/m/Y")?>" />
      </label></td>
	</tr>
	<tr>
	  <td class="TitDetalle">Usuario :</td>
	  <td class="CampoDetalle">
      	<? $objDrop->drop_usuarios("onchange='cargar_seguimiento();'"); ?>
      </td>
	</tr>
	<tr>
	  <td colspan="2" class="TitDetalle">
      	<div id="div_seguimiento">
        </div>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
   </tbody>
 </table>
 </form>
</div>
<script>
	cargar_seguimiento();
</script>
<?php  CuerpoInferior();
?> 