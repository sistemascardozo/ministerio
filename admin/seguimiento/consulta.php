<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$fecha		= $objFunciones->CodFecha($_POST["fecha"]);
	$usuario	= $_POST["usuario"];

	$sql = "SELECT s.descripcion, u.nombres, l.fechareg, l.comentario, l.hora, u.login ";
	$sql .= "FROM seguridad.log l ";
	$sql .= " INNER JOIN admin.sucursales s ON(l.codemp = s.codemp AND l.codsuc = s.codsuc) ";
	$sql .= " INNER JOIN seguridad.usuarios u ON(l.codusu = u.codusu) ";
	$sql .= "WHERE l.fechareg = ? AND l.codusu = ? ORDER BY l.hora";
	
	$consulta=$conexion->prepare($sql);
	$consulta->execute(array($fecha,$usuario));
	$items = $consulta->fetchAll();

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">  
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table border="1" align="center" cellspacing="0" class="ui-widget"  width="890"  rules="all" >
      <tr align="center" class="ui-widget-header">
        <td width="150">Sucursal</td>
        <td width="100">Usuario</td>
        <td width="70">Fecha</td>
        <td>Accion</td>
        <td width="70">Hora</td>
      </tr>
      <?php
      foreach($items as $row)
	  {
	  ?>
      <tr>
        <td align="center"><?=$row["descripcion"]?></td>
        <td style="padding-left:5px;"><?=strtoupper($row["login"])?></td>
        <td align="center"><?=$objFunciones->DecFecha($row["fechareg"])?></td>
        <td style="padding-left:5px;" ><?=strtoupper($row["comentario"])?></td>
        <td align="center"><?=$row["hora"]?></td>
       </tr>
       <?php
	   }
	   ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
