<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

    include('../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $Op = $_GET["Op"];

    $objFunciones = new clsFunciones();

    $codemp = 1;
    $codsuc = $_POST["codsuc"];
    $descripcion = strtoupper($_POST["descripcion"]);
    $direccion = strtoupper($_POST["direccion"]);
    $administrador = strtoupper($_POST["administrador"]);
    $telefono = $_POST["telefono"];
    $estareg = $_POST["estareg"];
    $codzonal = $_POST["idzona"];
    //$codzona = $_POST["codzona"];
    $codubigeo = $_POST["codubigeo"];

    switch ($Op) {
        case 0:
            $id = $objFunciones->setCorrelativos("sucursales", "0", "0");
            $codsuc = $id[0];

            $sql = "insert into admin.sucursales(codemp,codsuc,descripcion,direccion,telefono,administrador,estareg,codzonal,codubigeo)
                values(:codemp,:codsuc,:descripcion,:direccion,:telefono,:administrador,:estareg,:codzonal,:codubigeo)";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":descripcion" => $descripcion,
                ":direccion" => $direccion, ":telefono" => $telefono, ":administrador" => $administrador,
                ":estareg" => $estareg, ":codzonal" => $codzonal, ":codubigeo" => $codubigeo));
            break;
        case 1:
            $sql = "update admin.sucursales SET
                    descripcion=:descripcion,direccion=:direccion,
                    telefono=:telefono,administrador=:administrador,
                    estareg=:estareg, codzonal=:codzonal,codubigeo=:codubigeo 
                where codsuc=:codsuc and codemp=:codemp";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codemp" => $codemp, ":codsuc" => $codsuc, ":descripcion" => $descripcion,
                ":direccion" => $direccion, ":telefono" => $telefono, ":administrador" => $administrador,
                ":estareg" => $estareg, ":codzonal" => $codzonal, ":codubigeo" => $codubigeo));
            break;
        case 2:case 3:
            $sql = "update admin.sucursales set estareg=:estareg
                    where codemp=:codemp and codsuc=:codsuc";
            $result = $conexion->prepare($sql);
            $result->execute(array(":codsuc" => $_POST["1form1_id"], ":estareg" => $estareg, ":codemp" => $codemp));
            break;
    }
    if ($result->errorCode() != '00000') {
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
    
?>
