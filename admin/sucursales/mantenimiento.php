<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../objetos/clsMantenimiento.php");

    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"]) ? $_POST["Id"] : '';
    $guardar = "op=" . $Op;
    $codemp = 1;
    $objMantenimiento = new clsMantenimiento();

    if ($Id != '') {
        $sucursal = $objMantenimiento->setSucursales(" where codsuc=?", $Id);
        $guardar = $guardar . "&Id2=" . $Id;
    }
    ?>
    <script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
    <script>
        function ValidarForm(Op)
        {
            if ($("#Nombre").val() == '')
            {
                alert('La Descripcion de la Sucursal no puede ser NULO');
                return false;
            }
            if ($("#administrador").val() == "")
            {
                alert("El Nombre del Administrador no puede ser NULO");
                return false
            }
            GuardarP(Op);
        }

        function Cancelar()
        {
            location.href = 'index.php';
        }
    </script>
    <div align="center">
        <form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar; ?>" enctype="multipart/form-data">
            <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                <tbody>
                    <tr>
                        <td width="101" class="TitDetalle">&nbsp;</td>
                        <td width="573" class="CampoDetalle">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TitDetalle">Id :</td>
                        <td class="CampoDetalle">
                            <input name="codsuc" type="text" id="Id" size="4" maxlength="2" value="<? echo $Id; ?>" readonly="readonly" class="inputtext"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="TitDetalle">ZonaL : </td>
                        <td class="CampoDetalle">
                            <select name="idzona" id="idzona" style="width:228px">
                                <?php
                                $Sql = "SELECT * FROM admin.zonal WHERE estado = 1 AND codemp=" . $codemp . "";
                                $Consulta = $conexion->query($Sql);
                                foreach ($Consulta->fetchAll() as $row1) {
                                    $Sel = "";
                                    if ($sucursal["codzonal"] == $row1["codzon"])
                                        $Sel = 'selected="selected"';
                                    ?>
                                    <option value="<?php echo $row1["codzon"]; ?>" <?= $Sel ?>  ><?php echo $row1["descripcion"]; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="TitDetalle">Provincia : </td>
                        <td class="CampoDetalle">
                            <select name="codubigeo" id="codubigeo" style="width:228px">
                                <?php
                                $Sql = "SELECT * FROM public.ubigeo 
                                                WHERE codubigeo LIKE '25%00' AND codubigeo <> '250000' ORDER BY codubigeo ASC";
                                $Consulta = $conexion->query($Sql);
                                foreach ($Consulta->fetchAll() as $row1) {
                                    $Sel = "";
                                    if ($sucursal["codubigeo"] == $row1["codubigeo"])
                                        $Sel = 'selected="selected"';
                                    ?>
                                    <option value="<?php echo $row1["codubigeo"]; ?>" <?= $Sel ?>  ><?php echo $row1["descripcion"]; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="TitDetalle">Localidad : </td>
                        <td class="CampoDetalle">
                            <input class="inputtext" name="descripcion" type="text" id="Nombre" size="70" maxlength="200" value="<?= $sucursal["descripcion"] ?>"/>     </td>
                    </tr>

                    <tr>
                        <td class="TitDetalle">Direccion : </td>
                        <td class="CampoDetalle">
                            <input class="inputtext" name="direccion" type="text" id="direccion" size="70" maxlength="200" value="<?= $sucursal["direccion"] ?>"/>     </td>
                    </tr>
                    <tr>
                        <td class="TitDetalle">Administrador : </td>
                        <td class="CampoDetalle">
                            <input class="inputtext" name="administrador" type="text" id="administrador" size="70" maxlength="200" value="<?= $sucursal["administrador"] ?>"/>     </td>
                    </tr>
                    <tr>
                        <td class="TitDetalle">Telefono : </td>
                        <td class="CampoDetalle">
                            <input class="inputtext" name="telefono" type="text" id="telefono" size="20" maxlength="20" onkeypress="return permite(event, 'num');" value="<?= $sucursal["telefono"] ?>"/>     </td>
                    </tr>
                    <tr>
                        <td class="TitDetalle">Estado :</td>
                        <td class="CampoDetalle">
                            <? include("../../include/estareg.php"); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="TitDetalle">&nbsp;</td>
                        <td class="CampoDetalle">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <script>
        $("#Nombre").focus();
    </script>
    
    
<?php
    $est = isset($sucursal["estareg"]) ? $sucursal["estareg"] : 1;
    include("../../admin/validaciones/estareg.php");
?>