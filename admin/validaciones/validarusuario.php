<?php
	session_name("pnsu");
	/* establecer el limitador de caché a 'private' */
	session_cache_limiter('private');
	/* establecer la caducidad de la caché a ? h */
	session_cache_expire(300);
	
	session_start();
	
	include("../../config.php");
	
	include("../../objetos/clsseguridad.php");
	
	$objSeguridad =  new clsSeguridad();
	
	$user 	= trim(strtoupper($_POST["txtusuario"]));
	$pass 	= strtoupper($_POST["txtcontra"]);
	
	$pass = strtoupper(trim($objSeguridad->encriptar($pass)));
	//echo "-->".strtoupper(trim($objSeguridad->desencriptar('YWJjZGU=')));
	
	echo $Sql = "SELECT
				u.codusu,
				u.codemp,
				u.codsuc,
				u.login,
				u.contra,
				u.nombres,
				u.direccion,
				u.email,
				u.estareg,
				u.fechareg,
				u.codcargo,
				u.codarea,
				u.dni,
				s.descripcion,
				u.car
			FROM
				seguridad.usuarios AS u
				INNER JOIN admin.sucursales AS s ON s.codsuc = u.codsuc
			WHERE RTRIM(LTRIM(upper(u.login))) = '".$user."' AND RTRIM(LTRIM(upper(u.contra))) = '".$pass."' AND u.estareg = 1";
			
	//$consulta = $conexion->prepare("select * from seguridad.usuarios 
	//	where RTRIM(LTRIM(upper(login)))=? and RTRIM(LTRIM(upper(contra)))=? and estareg=1 ");
	
	//$Sql = eregi_replace("[\n\r\n\r]", ' ', $Sql);
	$consulta = $conexion->prepare($Sql);
	$consulta->execute(array());
	
	if($consulta->errorCode() != '00000')
	{
		$Error = 1;
		echo "Error-> <br>";
		echo $Sql."<br>";
	}
	
	$row	= $consulta->fetch();
	
	if($row["codusu"])
	{
		$_SESSION['Sistema']	= 'Modulo de Seleccion de Sistemas';
		$_SESSION['idsistema']	= 0;
		$_SESSION['idperfil']	= 0;
		
		$_SESSION['user']		= $user;
		$_SESSION['id_user']	= $row["codusu"];
		$_SESSION['nombre']		= $row["nombres"];
		$_SESSION['IdSucursal']	= $row["codsuc"];
		$_SESSION['idarea']		= $row["codarea"];
        $_SESSION['dni_user']	= $row["dni"];
		$_SESSION['dni']		= $row["dni"];
		$_SESSION['suc']		= $row["descripcion"];
		
		if($row["car"] != 0)
		{
			$_SESSION['car'] = $row["car"];
		}
		
		$op = "1";
		$mensaje = "";
		
		$_SESSION["IP"] 		= $objSeguridad->getRealIP();
		$_SESSION["HostName"] 	= $objSeguridad->HostName();

		$objSeguridad->seguimiento(1, $row["codsuc"], 0, $row["codusu"], "ACCESO AL SISTEMA", $_SERVER['REMOTE_ADDR']);

		$consulta = $conexion->prepare("SELECT * FROM admin.empresas WHERE codemp = 1");
		$consulta->execute(array());
		$row	= $consulta->fetch();
		
		$_SESSION['NombreEmpresa'] = $row["razonsocial"];
	}
	else
	{
		$op = "0";	
		$mensaje = "Usuario y/o Contraseña no válidos, consulte con el Administrador del Sistema";
	}
	
	echo $op."|".$mensaje;
	
?>