CREATE OR REPLACE FUNCTION facturacion.f_generar_recibos_facturacion (
  "CodSuc" integer,
  "CodCiclo" integer,
  "NroFacturacion" integer,
  "Lecturas" integer
)
RETURNS void AS
$body$
DECLARE
  sql_fact varchar;
  sql_det_fact varchar;
  sql_deuda varchar;
  sql_grafico varchar;
  
  reg_cab_fact record;
  reg_det_fact record;
  reg_deuda record;
  reg_grafico record;
  
  impmesagua public.porcentaje;
  impmesdsague public.porcentaje;
  impmesinteres public.porcentaje;
  impmesigv public.porcentaje;
  impcargo public.porcentaje;
  impcred public.porcentaje;
  impref public.porcentaje;
  impred public.porcentaje;
  impfon public.porcentaje;
  totmes public.porcentaje;
  
  nromes integer;
  impdeu public.porcentaje;
  
  fechaemi date;
  fechavencimientonorm date;
  fechavencimientodeud date;
  fechcorte date;
  
  mensajenor text;
  mensajedeu text;
  mensajegen text;
  
  mesesgraf varchar;
  consumograf varchar;
  
  codca varchar;
  dir varchar;
  und integer;
BEGIN
  /*Recupera los Valores del Periodo de Facturacion*/
  
  select fechaemision,fechavencnormal,fechavencdeudores,fechacorte,mensajenormal,mensajedeudores,mensajegeneral
  into fechaemi,fechavencimientonorm,fechavencimientodeud,fechcorte,mensajenor,mensajedeu,mensajegen
  from facturacion.periodofacturacion
  where codemp=1 and codsuc=$1 and codciclo=$2 and nrofacturacion=$3;


  sql_fact = 'select clie.codemp,clie.codsuc,c.codciclo,c.nrofacturacion,c.nroinscripcion,clie.codrutlecturas,clie.lote,c.anio,c.mes,';
  sql_fact = sql_fact || 'c.nrodocumento,c.propietario,t.descripcion as tiposervicio,c.tipofacturacion,clie.domestico,clie.social,';
  sql_fact = sql_fact || 'clie.comercial,clie.estatal,clie.industrial,tar.nomtar,clie.nromed,c.fechalectanterior,c.lecturaanterior,c.consumo,';
  sql_fact = sql_fact || 'c.fechalectultima,c.lecturaultima,clie.codsector,clie.codmanzanas,clie.codrutdistribucion,rut.nroorden as orden_manzana,';
  sql_fact = sql_fact || 'rutmae.orden as orden_ruta,tipcal.descripcioncorta,cal.descripcion as calle,clie.nrocalle ';
  sql_fact = sql_fact || 'from facturacion.cabfacturacion as c ';
  sql_fact = sql_fact || 'inner join catastro.clientes as clie on(c.codemp=clie.codemp and c.codsuc=clie.codsuc and c.nroinscripcion=clie.nroinscripcion) ';
  sql_fact = sql_fact || 'inner join public.tiposervicio as t on(clie.codtiposervicio=t.codtiposervicio) ';
  sql_fact = sql_fact || 'inner join public.rutasdistribucion as rut on(clie.codsector=rut.codsector and clie.codrutdistribucion=rut.codrutdistribucion ';
  sql_fact = sql_fact || 'and clie.codmanzanas=rut.codmanzanas and clie.codemp=rut.codemp and clie.codsuc=rut.codsuc) ';
  sql_fact = sql_fact || 'inner join public.rutasmaedistribucion as rutmae on(rut.codrutdistribucion=rutmae.codrutdistribucion and ';
  sql_fact = sql_fact || 'rut.codsector=rutmae.codsector and rut.codemp=rutmae.codemp and rut.codsuc=rutmae.codsuc) ';
  sql_fact = sql_fact || 'inner join facturacion.tarifas as tar on(clie.codemp=tar.codemp and clie.codsuc=tar.codsuc and clie.catetar=tar.catetar) ';
  sql_fact = sql_fact || 'inner join public.calles as cal on(clie.codemp=cal.codemp and clie.codsuc=cal.codsuc and clie.codcalle=cal.codcalle) ';
  sql_fact = sql_fact || 'inner join public.tiposcalle as tipcal on(cal.codtipocalle=tipcal.codtipocalle)';
  sql_fact = sql_fact || 'where c.codemp=1 and c.codsuc='|| $1 || ' and c.codciclo='|| $2 || ' and c.nrofacturacion='||$3;
  
  for reg_cab_fact in execute sql_fact loop
	/*Suma los Montos de la Facturacion del Mes*/
    impmesagua=0;
    impmesdsague=0;
    impmesinteres=0;
    impmesigv=0;
    impcargo=0;
    impcred=0;
    impref=0;
    impred=0;
    impfon=0;
    
    nromes=0;
    impdeu=0;
    
    mesesgraf='';
    consumograf='';
    dir='';
    
  	sql_det_fact = 'select sum(d.importe) as imptotal,c.categoria,d.nrocredito,d.nrorefinanciamiento ';
    sql_det_fact = sql_det_fact || 'from facturacion.detfacturacion as d ';
    sql_det_fact = sql_det_fact || 'inner join facturacion.conceptos as c on(d.codemp=c.codemp and d.codsuc=c.codsuc and d.codconcepto=c.codconcepto) ';
    sql_det_fact = sql_det_fact || 'where d.codemp=1 and d.codsuc='||$1||' and d.codciclo='||$2;
    sql_det_fact = sql_det_fact || ' and d.nrofacturacion='||$3||' and d.nroinscripcion='||reg_cab_fact.nroinscripcion;
    sql_det_fact = sql_det_fact || ' group by c.categoria,d.nrocredito,d.nrorefinanciamiento';
    
    for reg_det_fact in execute sql_det_fact loop
    	if reg_det_fact.categoria=1 then
        	impmesagua := impmesagua+reg_det_fact.imptotal;
        end if;
        if reg_det_fact.categoria=2 then
        	impmesdsague := impmesdsague+reg_det_fact.imptotal;
        end if;
        if reg_det_fact.categoria=3 then
        	impmesinteres := impmesinteres+reg_det_fact.imptotal;
        end if;
        if reg_det_fact.categoria=4 then
        	impmesigv := impmesigv+reg_det_fact.imptotal;
        end if;
        if reg_det_fact.categoria=5 then
        	impcargo := impcargo+reg_det_fact.imptotal;
        end if;
        if not reg_det_fact.nrocredito=0 then
        	impcred := impcred+reg_det_fact.imptotal;
        end if;
        if not reg_det_fact.nrorefinanciamiento=0 then
        	impref := impref+reg_det_fact.imptotal;
        end if;
        if reg_det_fact.categoria=7 then
        	impred := impred+reg_det_fact.imptotal;
        end if;
        if reg_det_fact.categoria=8 then
        	impfon := impfon+reg_det_fact.imptotal;
        end if;
    end loop;
    
    /*Cuenta los Meses y el Importe de Deuda*/
    sql_deuda = 'select nrofacturacion,sum(importe-(importerebajado+imppagado)) as importe ';
    sql_deuda = sql_deuda || 'from facturacion.detfacturacion ';
	sql_deuda = sql_deuda || 'where nroinscripcion='||reg_cab_fact.nroinscripcion||' and codsuc='||$1||' and estadofacturacion=1 and categoria=1 ';
	sql_deuda = sql_deuda || 'group by nrofacturacion ';
    sql_deuda = sql_deuda || 'having sum(importe-(importerebajado+imppagado))>0	order by nrofacturacion';
    
    for reg_deuda in execute sql_deuda loop
    	nromes := nromes+1;
        impdeu := impdeu+reg_deuda.importe;
    end loop;
    
    /*Recupera los Datos para el grafico*/
   sql_grafico = 'select anio,mes,consumo from facturacion.cabfacturacion ';
   sql_grafico = sql_grafico || 'where codsuc='||$1||' and nroinscripcion='||reg_cab_fact.nroinscripcion;
   sql_grafico = sql_grafico || ' order by CONVERT(CHAR,anio as integer),CONVERT(CHAR,mes as integer) desc limit '||$4;
   
   for reg_grafico in execute sql_grafico loop
   	 mesesgraf := mesesgraf || reg_grafico.mes || '|';
     consumograf := consumograf || reg_grafico.consumo || '|';
   end loop;
   
   codca 	= '1' || '-' || $1 || '-' || reg_cab_fact.codrutlecturas || '-' || reg_cab_fact.lote;
   dir	 		= reg_cab_fact.descripcioncorta ||' '||reg_cab_fact.calle || ' #' ||reg_cab_fact.nrocalle;
   und 			= reg_cab_fact.domestico + reg_cab_fact.social + reg_cab_fact.comercial + reg_cab_fact.estatal + reg_cab_fact.industrial;
   totmes 		= impmesagua + impmesdsague + impmesinteres + impcargo + impcred + impref + impred + impfon;
   
   /*Actualiza los Montos en la Tabla de Facturacion Cta. Corriente*/
   update facturacion.cabfacturacion set impmes=totmes,impdeuda=impdeu,impigv=impmesigv 
   where NroFacturacion=$3 and codsuc=$1 and codciclo=$2 and nroinscripcion=reg_cab_fact.nroinscripcion;
   
   update facturacion.cabctacorriente set impmes=totmes,impdeuda=impdeu,impigv=impmesigv,nromeses=nromes
   where NroFacturacion=$3 and codsuc=$1 and codciclo=$2 and nroinscripcion=reg_cab_fact.nroinscripcion;
   
   /*Inserta los Datos en la Tabla para la Impresion de Recibos*/
   insert into facturacion.impresion_recibos(codemp,codsuc,codciclo,nrofacturacion,nroinscripcion,codcatastro,anio,mes,nrorecibo,fechaemision,propietario,
                                              direccion,servicios,tipofacturacion,unidades,tarifa,nromedidor,fechalecturaanterior,lecturaanterior,
                                              consumom3,fechalecturaultima,lecturaultima,mesesgrafico,consumografico,impagua,impdesague,impinteres,
                                              impigv,impcargofijo,impcredito,imprefinanciamiento,impredondeo,impfonavi,nromeses,impdeuda,
                                              fechavencimientonormal,fechavencimientodeudores,fechacorte,mensajenormal,mensajedeudores,mensajegeneral,
                                              codsector,codmanzanas,codrutdistribucion,orden_rutas,orden_manzana)
                                      values(1,$1,$2,$3,reg_cab_fact.nroinscripcion,codca,reg_cab_fact.anio,reg_cab_fact.mes,reg_cab_fact.nrodocumento,
                                      	      fechaemi,reg_cab_fact.propietario,dir,reg_cab_fact.tiposervicio,reg_cab_fact.tipofacturacion,und,
                                              reg_cab_fact.nomtar,reg_cab_fact.nromed,reg_cab_fact.fechalectanterior,reg_cab_fact.lecturaanterior,
                                              reg_cab_fact.consumo,reg_cab_fact.fechalectultima,reg_cab_fact.lecturaultima,mesesgraf,consumograf,
                                              round(impmesagua,2),round(impmesdsague,2),round(impmesinteres,2),round(impmesigv,2),round(impcargo,2),
                                              round(impcred,2),round(impref,2),round(impred,2),round(impfon,2),nromes,impdeu,
                                              fechavencimientonorm,fechavencimientodeud,fechcorte,mensajenor,mensajedeu,mensajegen,
                                              reg_cab_fact.codsector,reg_cab_fact.codmanzanas,reg_cab_fact.codrutdistribucion,reg_cab_fact.orden_ruta,
                                              reg_cab_fact.orden_manzana);                                              
    
  end loop;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;