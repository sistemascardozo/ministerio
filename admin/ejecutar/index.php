<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../include/main.php");
	include("../../include/claseindex.php");

	$TituloVentana = "EJECUTAR SCRIPTS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
	
	$consulta = $conexion->prepare("select * from admin.empresas");
	$consulta->execute();
	$items = $consulta->fetch();
	
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/swfobject.js"></script>
<script>
	$(document).ready(function() {
		$('#cargar').uploadify({
			'script'    : 'uploader.php',
			'cancelImg' : 'cancel.png',
			'auto'      : true,
			'folder'    : 'scripts',
			'onComplete': function(event, queueID, fileObj, response, data) {
				//$("#ejecuciones").html(reponse)
				cargar_ejecuciones()
			}
		});
	});
	function ValidarForm(Op)
	{
		return true;
	}
	
	function Cancelar()
	{
		location.href='../indexB.php';
	}
	function cargar_ejecuciones()
	{
		$.ajax({
			 url:'consulta.php',
			 type:'POST',
			 async:true,
			 data:'',
			 success:function(datos){		 	
				$("#ejecuciones").html(datos)
			 }
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
 <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	 <td width="112" class="TitDetalle">&nbsp;</td>
	 <td width="682" class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
	  <td class="TitDetalle">Cargar Scripts:</td>
	  <td class="CampoDetalle">
      	<div>
        	<input name="cargar" id="cargar" type="button" value="Cargar Scripts Personalizados" />
        </div>
      </td>
	  </tr>	
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="2" class="TitDetalle">
      	<div id="ejecuciones" style="overflow:auto">
        </div>
      </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	 </tr>
   </tbody>

  </table>
 </form>
</div>
<script>
	cargar_ejecuciones();
</script>
<?php  CuerpoInferior();
?> 