<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../include/main.php");
	include("../../include/claseindex.php");

	$TituloVentana = "GENERAR BACKUPS";
	$Activo=1;
	CuerpoSuperior($TituloVentana);
	$codsuc 	= $_SESSION['IdSucursal'];

	$objMantenimiento 	= new clsDrop();

	$sucursal = $objMantenimiento->setSucursales(" where codemp=1 and codsuc=?",$codsuc);

	$Fecha = date('d/m/Y');
	
	$consulta = $conexion->prepare("select * from admin.empresas");
	$consulta->execute();
	$items = $consulta->fetch();
	
?>
<script>
	function ValidarForm(Op)
	{
		ejecutar_backup();
		return false;
	}
	
	function Cancelar()
	{
		location.href='../indexB.php';
	}
	function ejecutar_backup()
	{
		$("#ejecuciones").html("<img src='../../images/avance.gif' />")
		$.ajax({
			 url:'generar.php',
			 type:'POST',
			 async:true,
			 data:'tipo=' + $("#tipo").val(),
			 success:function(datos){		 	
				$("#ejecuciones").html(datos)
				cargar_ejecuciones();
			 }
		}) 
	}
	function cargar_ejecuciones()
	{
		$.ajax({
			 url:'consulta.php',
			 type:'POST',
			 async:true,
			 data:'',
			 success:function(datos){		 	
				$("#ejecuciones").html(datos)
			 }
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="" enctype="multipart/form-data">
 <table width="800" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   </thead>
   <tbody>
	<tr>
	  <td width="112" class="TitDetalle">&nbsp;</td>
	  <td width="682" class="CampoDetalle">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="2" class="TitDetalle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td width="36" align="center">
	        <input type="radio" name="radio" id="backup" value="backup" checked="checked" onclick="javascript:$('#tipo').val(0)" />
	        </td>
	      <td width="209">Generar Backup en Formato .Backup</td>
	      <td width="36" align="center">&nbsp;</td>
	      <td width="415">&nbsp;</td>
	      </tr>
	    <tr>
	      <td align="center"><input type="radio" name="radio" id="sql" value="sql" onclick="javascript:$('#tipo').val(1)" /></td>
	      <td>Generar Bacukp en Formato .Sql</td>
	      <td align="center">&nbsp;</td>
	      <td>&nbsp;</td>
	      </tr>
	    <tr>
	      <td colspan="4" align="center"><label onclick="ejecutar_backup()" class="BtnMigrador">&nbsp;Generar BackUp&nbsp;</label></td>
	      </tr>
	    </table></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle"><input type="hidden" name="tipo" id="tipo" value="0" /></td>
	  </tr>
	<tr>
	  <td colspan="2" class="TitDetalle" height="50px">
      	<div id="ejecuciones" style="overflow:auto" align="center">
        </div>
      </td>
	  </tr>
	<tr>
	  <td colspan="2" class="TitDetalle">
      	<div id="mensaje" style="padding:4px">
        	Se le recomienda una Vez Generado el Backup proceder a Descargarlo y Guardarlos en una Unidad Externa
        </div>
      </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	 </tr>
   </tbody>
 
  </table>
 </form>
</div>
<script>
	cargar_ejecuciones();
</script>
<?php  CuerpoInferior();
?> 