<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../objetos/clsFunciones.php");
	
	$objFunciones = new clsFunciones();
	
	$fecha 		= $_POST["fecha"];
	$usuario 	= $_POST["usuario"]==0?"%":$_POST["usuario"];
	$usuario 	= "%".$usuario."%";
	
	$sql = "SELECT * FROM seguridad.ejecuciones WHERE tipooperacion=2 ORDER BY fechareg DESC";
	$consulta=$conexion->prepare($sql);
	$consulta->execute();
	$items = $consulta->fetchAll();

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">  
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%"  rules="all" >
      <tr align="center" class="ui-widget-header">
        <td width="35%">Archivo</td>
        <td width="17%">Fecha Registro</td>
        <td width="14%">Usuario</td>
        <td width="15%">Hora</td>
        <td width="19%">Ip. Pc. Remote</td>
        </tr>
      <?php
      foreach($items as $row)
	  {
		  $sqlU = "select login from seguridad.usuarios where codusu=?";
		  
		  $consultaU = $conexion->prepare($sqlU);
		  $consultaU->execute(array($row["codusu"]));
		  $rowU = $consultaU->fetch();
		  
	  ?>
      <tr>
        <td align="left"><a href="backup/<?=$row["archivo"]?>" ><?=$row["archivo"]?></a></td>
        <td align="center"><?=$objFunciones->DecFecha($row["fechareg"])?></td>
        <td align="center"><?=strtoupper($rowU["login"])?></td>        
        <td align="center"><?=$row["hora"]?></td>
        <td align="center"><?=$row["ip_remote"]?></td>
        </tr>
       <?php
	   }
	   ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
