@ECHO OFF
REM  QBFC Project Options Begin
REM  HasVersionInfo: Yes
REM  Companyname: MLJ CONSULTORES WEB
REM  Productname: SISGM
REM  Filedescription: 
REM  Copyrights: Copyright � 2014
REM  Trademarks: 
REM  Originalname: 
REM  Comments: 
REM  Productversion: 01.50.00.00
REM  Fileversion: 01.50.00.00
REM  Internalname: 
REM  Appicon: Mis Documentos\Escritorio\BD.ico
REM  AdministratorManifest: No
REM  QBFC Project Options End

color 30

ECHO ON
@echo off

setlocal

set FECHA_ACTUAL=%DATE%
set HORA_ACTUAL=%TIME%


set ANO=%FECHA_ACTUAL:~6,4%
set MES=%FECHA_ACTUAL:~3,2%
set DIA=%FECHA_ACTUAL:~0,2%


set HORA=%HORA_ACTUAL:~0,2%
set MINUTOS=%HORA_ACTUAL:~3,2%
set SEGUNDOS=%HORA_ACTUAL:~6,2%
set CENTESIMAS=%HORA_ACTUAL:~9,2%

set HORA=%HORA: =%

if %HORA% LSS 10 set HORA=0%HORA%

SET ARCHIVO=BK_E-SIINCOWEB_%ANO%%MES%%DIA%_%HORA%%MINUTOS%.sql
SET ARCHIVO_RAR=backup\BK_E-SIINCOWEB_%ANO%%MES%%DIA%-sql.rar

SET pgpassword=3834871mcts

"C:\Program Files (x86)\PostgreSQL\8.4\bin\pg_dump.exe" --host localhost --port 5432 --username postgres --format p --file "backup\%ARCHIVO%" siinco_sedahuanuco

echo %ARCHIVO%