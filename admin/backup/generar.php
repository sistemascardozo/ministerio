<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../config.php");
	
	$conexion->beginTransaction();
	
	$codsuc		= $_SESSION["IdSucursal"];
	$idusuario	= $_SESSION['id_user'];
	
	set_time_limit(0);
	
	ini_set("memory_limit", "64M");
	
	$tipo = $_POST["tipo"];
	
	$fecha = date("d/m/Y");
	
	$anio 	= substr($fecha, 6, strlen($fecha));
	$mes 	= substr($fecha, 3, strlen($fecha) - 8);
	$dia	= substr($fecha, 0, 2);
	
	if($tipo == 0)
	{
		$cad = "backup_normal.bat";
		$ext = "-backup.rar";
	}
	else
	{
		$cad = "backup_sql.bat";
		$ext = "-sql.rar";
	}

	$NomArchivo = system($cad, $retval);
	
	$sql = "INSERT INTO seguridad.ejecuciones";
	$sql .= "(archivo, comentario, codusu, codsuc, tipooperacion, ip_remote) ";
	$sql .= "VALUES(:archivo, :comentario, :codusu, :codsuc, :tipooperacion, :ip_remote)";
	
	$consulta = $conexion->prepare($sql);
	$consulta->execute(array(":archivo"=>$NomArchivo,
							 ":comentario"=>"SE HA REALIZADO EL BACKUP A LA BASE DE DATOS ".$dbname,
							 ":codusu"=>$idusuario,
							 ":codsuc"=>$codsuc,
							 ":tipooperacion"=>2,
							 ":ip_remote"=>$_SERVER['REMOTE_ADDR']));
	
	if(!$consulta)
	{
		$conexion->rollBack();
		var_dump($resultC->errorInfo());
		die();
	}else{
		$conexion->commit();
		$mensaje = "El Backup se ha Generado Corretamente";
		$res=1;
	}
	
	echo $mensaje;

?>