<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../include/main.php");
  include("../../include/claseindex.php");
  $TituloVentana = "SERVICES";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];

  $FormatoGrilla = array ();
  $Sql = "SELECT se.codservices,su.descripcion, se.descripcion,e.descripcion, se.estareg,se.codsuc
          FROM admin.sucursales su
          INNER JOIN admin.services se ON (su.codsuc = se.codsuc)
          INNER JOIN public.estadoreg e ON (su.estareg=e.id)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'se.codservices', '2'=>'su.descripcion','3'=>'se.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Sucursal','T3'=>'Services','T4'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'left');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " ORDER BY se.codservices  ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'5',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Anular(this)"', 
              'BtnCI2'=>'5', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Activar(this)"', 
              'BtnCI3'=>'5', 
              'BtnCV3'=>'0');
  $FormatoGrilla[10] = array(array('Name' =>'id','Col'=>1),array('Name' =>'codsuc','Col'=>6));//DATOS ADICIONALES        
  $FormatoGrilla[11]=6;//FILAS VISIBLES     
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 700,450);
  Pie();
  
?>
<script type="text/javascript">
function Anular(obj)
  {
    $("#form1").remove();
    var Id = $(obj).parent().parent().data('id')
    var codsuc = $(obj).parent().parent().data('codsuc')
    var html = '<form id="form1" name="form1">';
    html+='<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
    html+='<input type="hidden" name="codsuc" value="' + codsuc + '" />';
    html+='<input type="hidden" name="estareg" id="estareg" value="0" />';
    html+='</form>'
    $("#DivEliminacion").html(html);
    $("#ConfirmaEliminacion").dialog("open");
  }

  function Activar(obj)
  {
    $("#form1").remove();
    var Id = $(obj).parent().parent().data('id')
    var codsuc = $(obj).parent().parent().data('codsuc')
    var html = '<form id="form1" name="form1">';
    html+='<input type="hidden" name="1form1_id" id="Id" value="' + Id + '" />';
    html+='<input type="hidden" name="codsuc" value="' + codsuc + '" />';
    html+='<input type="hidden" name="estareg" id="estareg" value="1" />';
    html+='</form>'
    $("#DivRestaurar").html(html);
    $("#ConfirmaRestauracion").dialog("open");
  }
</script>

<?php CuerpoInferior(); ?>