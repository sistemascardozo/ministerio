<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codemp					= 1;
	$codsuc 				= $_POST["codsuc"];
	$descripcion 			= strtoupper($_POST["descripcion"]);
	$estareg				= $_POST["estareg"];
	$codservices			= $_POST["codservices"];
	$direccion 	= $_POST["direccion"];
	$telefono	= $_POST["telefono"];
	$replegal	= $_POST["replegal"];
	$obs	= $_POST["obs"];

	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("services",$codsuc,"0","0");
			$codservices = $id[0];

			$sql = "INSERT INTO admin.services
				(codservices, codsuc,codemp,descripcion,replegal,direccion,	telefono, obs, estareg) 
				VALUES (:codservices,:codsuc,:codemp,:descripcion,:replegal,:direccion, :telefono, :obs,:estareg);";
			$result = $conexion->prepare($sql);
			$result->execute(array(
				":codservices"=>$codservices,
				":codemp"=>$codemp,
				":codsuc"=>$codsuc,
				":descripcion"=>$descripcion,
				":replegal"=>$replegal,
				":direccion"=>$direccion,
				":telefono"=>$telefono,
				":obs"=>$obs,
				":estareg"=>$estareg));
		break;
		case 1:
			$sql = "UPDATE admin.services  SET 
			  descripcion = :descripcion,
			  replegal = :replegal,
			  direccion = :direccion,
			  telefono = :telefono,
			  obs = :obs,
			  estareg = :estareg
 				WHERE codservices = :codservices AND codsuc = :codsuc AND codemp=:codemp";
 			$result = $conexion->prepare($sql);
			$result->execute(array(
				":codservices"=>$codservices,
				":codemp"=>$codemp,
				":codsuc"=>$codsuc,
				":descripcion"=>$descripcion,
				":replegal"=>$replegal,
				":direccion"=>$direccion,
				":telefono"=>$telefono,
				":obs"=>$obs,
				":estareg"=>$estareg));
		break;
		case 2:case 3:
			$sql = "update admin.services set estareg=:estareg
				where codemp=:codemp and codsuc=:codsuc AND codservices = :codservices";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsuc"=>$codsuc,":estareg"=>$estareg,":codemp"=>$codemp,":codservices"=>$_POST['1form1_id']));
		break;
	
	}


	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
	
?>
