<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../config.php");
	
	$consulta = $conexion->prepare("select * from admin.empresas");
	$consulta->execute();
	$items = $consulta->fetch();
	
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if (document.form1.empresa.value == '')
		{
			alert('La Razon Social de la Empresa no puede ser NULO');
			document.form1.empresa.focus();
			return false;
		}
		if (document.form1.direccion.value == '')
		{
			alert('La Direccion de la Empresa no puede ser NULO');
			document.form1.direccion.focus();
			return false;
		}
		if (document.form1.ruc.value == '')
		{
			alert('El RUC de la Empresa no puede ser NULO');
			document.form1.ruc.focus();
			return false;
		}
		if($("#replegal").val()=='')
		{
			alert('El Reresentante Legal de la Empresa no puede ser NULO')
			$("#replegal").focus()
			return false
		}
		if($("#gerente").val()=='')
		{
			alert('El Nombre del Gerente General no puede ser NULO')
			$("#gerente").focus()
			return false
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='../indexB.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php" enctype="multipart/form-data">
 <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	 <td width="85" class="TitDetalle">&nbsp;</td>
	 <td width="588" class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
     <td class="TitDetalle">Id :</td>
     <td class="CampoDetalle">
		<input name="codemp" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<?=$items["codemp"]?>" class="inputtext"/>
     </td>
	</tr>	
	<tr>
	 <td class="TitDetalle">Empresa : </td>
	 <td class="CampoDetalle">
        <input class="inputtext" name="razon_social" type="text" id="empresa" size="70" maxlength="200" value="<?=$items["razonsocial"]?>"/>
     </td>
	</tr>
	<tr>
     <td class="TitDetalle">Direccion : </td>
     <td class="CampoDetalle"><input class="inputtext" name="direccion" type="text" id="direccion" size="70" maxlength="200" value="<?=$items["direccion"]?>"/></td>
	</tr>
	<tr>
     <td class="TitDetalle">RUC : </td>
     <td class="CampoDetalle">
        <input class="inputtext" name="ruc" type="text" id="ruc" size="20" maxlength="20" value="<?=$items["ruc"]?>"/>
	 </td>
	</tr>
	<tr>
	 <td class="TitDetalle">Telefono :</td>
	 <td class="CampoDetalle">
	    <input class="inputtext" name="telefono" type="text" id="telefono" size="20" maxlength="20" value="<?=$items["telefono"]?>"/>
	 </td>
	</tr>
	<tr>
     <td class="TitDetalle">Rep. Legal : </td>
     <td class="CampoDetalle"><input class="inputtext" name="replegal" type="text" id="replegal" size="70" maxlength="200" value="<?=$items["replegal"]?>"/></td>
	</tr>
	<tr>
	 <td class="TitDetalle">Gerente : </td>
	 <td class="CampoDetalle"><input class="inputtext" name="gerente" type="text" id="gerente" size="70" maxlength="200" value="<?=$items["gerente"]?>"/></td>
	</tr>
	<tr>
	 <td class="TitDetalle">Rubro : </td>
	 <td class="CampoDetalle"><input class="inputtext" name="rubro" type="text" id="rubro" size="70" maxlength="200" value="<?=$items["rubro"]?>"/></td>
	</tr>
	<tr>
	 <td class="TitDetalle" valign="top">Notas : </td>
	 <td class="CampoDetalle">
	     <textarea name="notas" id="notas" class="inputtext" cols="60" rows="3" style="font-size:12px"><?=$items["notas"]?></textarea>
	 </td>
	</tr>
    <tr>
	 <td class="TitDetalle">&nbsp;</td>
	 <td class="CampoDetalle">&nbsp;</td>
	</tr>
  </tbody>
 
  </table>
 </form>
</div>