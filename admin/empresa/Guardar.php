<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$codemp 	= $_POST["codemp"];
	$razon 		= $_POST["razon_social"];
	$direccion 	= $_POST["direccion"];
	$ruc		= $_POST["ruc"];
	$telefono	= $_POST["telefono"];
	$replegal	= $_POST["replegal"];
	$gerente	= $_POST["gerente"];
	$rubro		= $_POST["rubro"];
	$notas		= $_POST["notas"];
	
	$upd="update admin.empresas set razonsocial=?,direccion=?,ruc=?,telefono=?,replegal=?,gerente=?,rubro=?,notas=? where codemp=?";
	$result = $conexion->prepare($upd);
	$result->execute(array($razon,$direccion,$ruc,$telefono,$replegal,$gerente,$rubro,$notas,$codemp));
	
	if(!$result)
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
