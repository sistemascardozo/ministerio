<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../objetos/clsMantenimiento.php");
	
	$objMantenimiento = new clsMantenimiento();
	
	$Id = isset($_POST["Id"])?$_POST["Id"]:'';
	
	$usuario 	= $objMantenimiento->setUsuario($Id);
	$sistemas 	= $objMantenimiento->setSistemas();
	
	
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var count=0;
	var contador=0;
	
	
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function Agregar()
	{
		if($("#sistemas").val()==0)
		{
			alert("Seleccione el Sub Sistema")
			return
		}
		if($("#codsubsistema").val()==0)
		{
			alert("Seleccione el Modulo")
			return
		}
		if($("#codperfil").val()==0)
		{
			alert("Seleccione el Perfil")
			return
		}
		
		count 		= count + 1;
		contador 	= contador + 1;
		
		var sist	= $("#sistemas option:selected").text();
		var subsist = $("#codsubsistema option:selected").text();
		var perf	= $("#codperfil option:selected").text();
		
		for(var i=1;i<=$("#cont").val();i++)
		{
			try
			{
				if($("#codsubsistema"+i).val()==$("#codsubsistema").val())
				{
					alert('El Modulo ya tiene asignado un Perfil')
					return false
				}
			}catch(exp)
			{
				
			}
		}
		
		
		$( "#tbperfiles tbody" ).append( 
							"<tr>"+
							"<td>"+sist+"</td>"+
							"<td><input type='hidden' name='codperfil"+contador+"' id='codperfil"+contador+"' value='"+$("#codperfil").val()+"'>"+subsist+"</td>"+
							"<td><input type='hidden' name='codsubsistema"+contador+"' id='codsubsistema"+contador+"' value='"+$("#codsubsistema").val()+"'>"+perf+"</td>"+
							"<td align='center'><span class='icono-icon-trash' onclick='QuitaFilaD(this);' ></span>  </td>"+
							"</tr>"
								)
								
		$("#cont").val(contador)
	}
	function QuitaFilaD(x)
	{	
		while (x.tagName.toLowerCase() !='tr')
		{
			if(x.parentElement)
				x=x.parentElement;
			else if(x.parentNode)
				x=x.parentNode;
			else
				return;
		}
		
		var rowNum=x.rowIndex;
		while (x.tagName.toLowerCase() !='table')
		{
			if(x.parentElement)
				x=x.parentElement;
			else if(x.parentNode)
				x=x.parentNode;
			else
				return;
		}
		x.deleteRow(rowNum);
	}
	function cargar_subsistemas(obj,sel)
	{
		$.ajax({
			url:'<?php echo $_SESSION['urldir'];?>ajax/subsistema.php',
			type:'POST',
			async:true,
			data:'sistema='+obj+'&seleccion='+sel,
			success:function(datos){		 	
				$("#div_subsistema").html(datos)
			}
		}) 
	}
	function cargar_perfiles(obj)
	{
		$.ajax({
			url:'<?php echo $_SESSION['urldir'];?>ajax/perfiles.php',
			type:'POST',
			async:true,
			data:'codsubsistema='+obj,
			success:function(datos){
				$("#div_perfiles").html(datos)
			}
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
  <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td width="50" class="TitDetalle">Id</td>
      <td align="center" class="TitDetalle">:</td>
      <td class="CampoDetalle">
		<input name="codusu" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Nombre</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="nombres" type="text" id="Nombre" maxlength="200" readonly="readonly" value="<?=$usuario["nombres"]?>" style="width:400px;"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
	  <td colspan="3" class="TitDetalle" style="background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px">
    	Seleccione el Sub Sistema y el M&oacute;dulo
      </td>
	</tr>
	<tr>
	  <td colspan="3" class="TitDetalle">
          <fieldset>
          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr style="padding:4px">
                <td width="70">Sub Sistema</td>
                <td width="30" align="center">:</td>
                <td width="85%">
                  <select name="sistemas" id="sistemas" class="select" style="width:280px" onchange="cargar_subsistemas(this.value, 1000);">
                  	<option value="0">--Seleccione el Sub Sistema--</option>
                    <?php
						foreach($sistemas as $rowsistema)
						{
							echo "<option value='".$rowsistema["codsistema"]."' >".$rowsistema["nombre_sistema"]."</option>";
						}
					?>
                  </select>
                  <img src="../../images/iconos/add.png" width="16" height="16" style="cursor:pointer" alt="Agrega un Item al Registro" onclick="Agregar();" />
                </td>
              </tr>
              <tr style="padding:4px">
                <td>M&oacute;dulo</td>
                <td align="center">:</td>
                <td>
                	<div id="div_subsistema">
                    <select name="codsubsistema" id="codsubsistema" class="select" style="width:280px">
                  		<option value="0">--Seleccione el M&oacute;dulo--</option>
                    </select>
                    </div>
                </td>
              </tr>
              <tr style="padding:4px">
                <td>Perfil</td>
                <td align="center">:</td>
                <td>
                <div id="div_perfiles">
                <select name="codperfil" id="codperfil" class="select" style="width:280px">
                    <option value="0">--Seleccione el Perfil--</option>
                </select>
                </div>
                </td>
              </tr>
            </table>
          </fieldset></td>
	  </tr>
	<tr>
	  <td colspan="3" class="TitDetalle">
         <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbperfiles" rules="all" >
            <tr class="ui-widget-header" >
              <td align="center" >Sub Sistema</td>
              <td align="center" >M&oacute;dulo</td>
              <td align="center" >Perfil</td>
              <td width="30" align="center" >&nbsp;</td>
            </tr>
			<?php 
				$cont = 0;
				
				$sqlD = "select p.descripcion,usu.codperfil,s.descripcion as nombre_subsistema,usu.codsubsistema,sist.nombre_sistema
						from seguridad.usuarios_subsistemas_perfil as usu
						inner join seguridad.perfiles as p on(usu.codperfil=p.codperfil) 
						inner join seguridad.subsistemas as s on(usu.codsubsistema=s.codsubsistema)
						inner join seguridad.sistemas as sist on(s.codsistema=sist.codsistema)
						where usu.codusu=? order by sist.codsistema";
				$consultaD = $conexion->prepare($sqlD);
				$consultaD->execute(array($Id));
				$itemsD = $consultaD->fetchAll();
				
				foreach($itemsD as $rowD)
				{
					$cont += 1;
			?>            
                <tr>
                  <td style="padding-left:5px;">
				  	<?=strtoupper($rowD["nombre_sistema"])?>
                  </td>
                  <td style="padding-left:5px;">
                  	<?=strtoupper($rowD["nombre_subsistema"])?>
                   	<input type="hidden" name="codsubsistema<?=$cont?>" id="codsubsistema<?=$cont?>" value="<?=$rowD["codsubsistema"]?>" />
                  </td>
                  <td style="padding-left:5px; padding-right:5px;"><?=$rowD["descripcion"]?>
                  <input type="hidden" name="codperfil<?=$cont?>" id="codperfil<?=$Cont?>" value="<?=$rowD["codperfil"]?>" /></td>
                  <td align="center">
                  	
                  	<span class="icono-icon-trash" onClick='QuitaFilaD(this,1)' ></span>
                  </td>
                </tr>
            <?php }?>
            <script>
				count	 = <?=$cont?>;
				contador = <?=$cont?>;
			</script>
          </table></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle"><input type="hidden" name="cont" id="cont" value="<?=$cont?>" /></td>
	  </tr>
    </tbody>

  </table>
 </form>
</div>