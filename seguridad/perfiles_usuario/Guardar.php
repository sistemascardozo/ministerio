<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$objFunciones = new clsFunciones();
	$id 	= $_POST["codusu"];
	$cont 	= $_POST["cont"];
	
	$del = "delete from seguridad.usuarios_subsistemas_perfil where codusu=".$id;
	$result 	= $conexion->query($del);
	if(!$result)
	{
		$conexion->rollBack();
		echo $mensaje = "Error al DELETE Registro";
		echo $res=2;
	}
	
	for($i=1;$i<=$cont;$i++)
	{
		if(isset($_POST["codsubsistema".$i]))
		{
			$sql = "insert into seguridad.usuarios_subsistemas_perfil(codsubsistema,codusu,codperfil) values(?,?,?)";
			$result 	= $conexion->prepare($sql);
			$result->execute(array($_POST["codsubsistema".$i],$id,$_POST["codperfil".$i]));
			if($result->errorCode()!='00000')
			{
				$conexion->rollBack();
				echo $mensaje = "Error al insert Registro";
				echo $res=2;
			}
		}
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
