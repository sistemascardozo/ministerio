<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
    include("../../objetos/clsMantenimiento.php");

    $Op = $_POST["Op"];
    $Id = isset($_POST["Id"])?$_POST["Id"]:'';
	
    $guardar = "op=".$Op;

    $objMantenimiento = new clsMantenimiento();

    //------Sistemas-----
    $sistemas = $objMantenimiento->setSistemas();
    
    if($Id != '')
    {
        $modulos = $objMantenimiento->setModulos($Id);
        $guardar = $guardar."&Id2=".$Id;
    }
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
    function ValidarForm(Op)
    {
		if ($("#codsistema").val() == 0)
		{
			alert('Seleccione el Sistema');
		
			return false;
		}
		if ($("#codsubsistema").val() == 0)
		{
			alert('Seleccione el Sub. Sistema');
			
			return false;
		}
		if($("#Nombre").val() == "")
		{
			alert("La Descripcion del Modulo no puede ser NULO");
			
			return false;
		}
		if($("#Nombre").val() == "")
		{
			alert("La Descripcion del Modulo no puede ser NULO");
			
			return false;
		}
		if($("#Orden").val() == "" || $("#Orden").val() == 0)
		{
			alert("El Orden ingresado no es Valido");
			
			return false;
		}
		
		GuardarP(Op);
    }

	function Cancelar()
	{
		location.href = 'index.php';
	}
	function cargar_subsistemas(obj,sel)
	{
		$.ajax({
			url:'<?php echo $_SESSION['urldir'];?>ajax/subsistema.php',
			type:'POST',
			async:true,
			data:'sistema='+obj+'&seleccion='+sel,
			success:function(datos){		 	
				$("#div_subsistema").html(datos)
			}
		}) 
	}
	function cargar_modulo(obj,sel)
	{
		$.ajax({
			url:'<?php echo $_SESSION['urldir'];?>ajax/modulos.php',
			type:'POST',
			async:true,
			data:'subsistema='+obj+'&seleccion='+sel,
			success:function(datos){	 	
				$("#div_padre").html(datos)
			}
		}) 
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
    <tbody>
	  <tr>
	    <td class="TitDetalle">&nbsp;</td>
	    <td width="30" class="TitDetalle">&nbsp;</td>
	    <td class="CampoDetalle">&nbsp;</td>
	  </tr>
	  <tr>
        <td class="TitDetalle">Id</td>
        <td align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
			<input name="Id" type="text" id="Id" size="4" maxlength="2" <?php echo $Enabled2;?> value="<?php echo $Id;?>" class="inputtext"/>
        </td>
	  </tr>
	  <tr>
	    <td class="TitDetalle">Sistemas</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
            <select name="codsistema" id="codsistema" class="Select" onchange="cargar_subsistemas(this.value, 0);" style="width:280px" >
                <option value="0">--Seleccione el Sistema--</option>
<?php 
                        foreach($sistemas as $rowsistema)
                        {
                                $selected = "";
								
                                if($modulos["codsistema"] == $rowsistema["codsistema"])
                                {
                                        $selected = "selected='selected'";
                                }
								
                                echo "<option value='".$rowsistema["codsistema"]."' ".$selected." >".$rowsistema["nombre_sistema"]."</option>";
                        }
?>
	    </select>
        </td>
	  </tr>
	  <tr>
	    <td class="TitDetalle">Sub. Sistema</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
    	<div id="div_subsistema">
        	<select name="codsubsistema" id="codsubsistema" class="Select" style="width:280px" >
           		<option value="0">--Seleccione el Sub. Sistema--</option>
            </select>
        </div>
        </td>
	  </tr>
	  <tr>
	    <td class="TitDetalle">Descripcion</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
         <input class="inputtext" name="Descripcion" type="text" id="Nombre" maxlength="200" value="<?=$modulos["descripcion"]?>" style="text-transform:none; width:400px"/>
        </td>
	  </tr>
	  <tr>
      <td class="TitDetalle">Url</td>
      <td align="center" class="TitDetalle">:</td>
      <td class="CampoDetalle">
      	<input class="inputtext" name="Url" type="text" id="Url" maxlength="200" value="<?=$modulos["url"]?>" style="text-transform:none; width:400px"/>
      </td>
	</tr>
	<tr>
      <td class="TitDetalle">Id. Padre</td>
      <td align="center" class="TitDetalle">:</td>
      <td class="CampoDetalle">
         <div id="div_padre">
           <select name="IdPadre" id="IdPadre" class="Select" style="width:280px">
              <option value="0">--Seleccione el Padre--</option>
           </select>
         </div>
       </td>
	</tr>
    <tr>
	  <td class="TitDetalle">Orden</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
    	<input class="inputtext" name="Orden" type="text" id="Orden" size="3" maxlength="3" value="<?=$modulos["orden"]?>"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle"><? include("../../include/estareg.php"); ?></td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
    </tbody>
	 
    </table>
 </form>
</div>
<?php
	if($Id!="")
	{
    ?>
    <script>
        cargar_subsistemas(<?=$modulos["codsistema"]?>,<?=$modulos["codsubsistema"]?>);
        cargar_modulo(<?=$modulos["codsubsistema"]?>,<?=$modulos["idpadre"]?>);
    </script>
    <?php
	}
	
	$est = isset($modulos["estareg"])?$modulos["estareg"]:1;
	
	include("../../admin/validaciones/estareg.php"); 
?>