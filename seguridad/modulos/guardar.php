<?php
	session_name("pnsu");
	if(!session_start()){session_start();}

	include('../../objetos/clsFunciones.php');
	include("../../objetos/clsseguridad.php");

	$conexion->beginTransaction();
	
	$objFunciones = new clsFunciones();
	$objSeguridad = new clsSeguridad();
	
	$Op = $_GET["Op"];
	
	$codmodulo = isset($_POST["Id"])?$_POST["Id"]:$_POST["1form1_id"];
	$codsistema = $_POST["codsistema"];
	$codsubsistema = $_POST["codsubsistema"];
	$Descripcion = $_POST["Descripcion"];
	$Url = $_POST["Url"];
	$IdPadre = $_POST["IdPadre"];
	$Orden = $_POST["Orden"];
	$estareg = $_POST["estareg"];


switch ($Op) {
    case 0:
        $id = $objFunciones->setCorrelativos("modulos", "0", "0");
        $codmodulo = $id[0];

        $sql = "INSERT INTO seguridad.modulos(codmodulo, codsubsistema, descripcion, url, idpadre, orden, estareg) 
            values(:codmodulo,:codsubsistema,:descripcion,:url,:idpadre,:orden,:estareg)";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codmodulo" => $codmodulo, ":codsubsistema" => $codsubsistema, ":descripcion" => $Descripcion,
            ":url" => $Url, ":idpadre" => $IdPadre, ":orden" => $Orden, ":estareg" => $estareg));

        break;
		
    case 1:
        $sql = "UPDATE seguridad.modulos ";
		$sql .= "SET codsubsistema = ".$codsubsistema.", descripcion = '".$Descripcion."', ";
		$sql .= " url = '".$Url."', idpadre = ".$IdPadre.", orden = ".$Orden.", estareg = ".$estareg." ";
		$sql .= "WHERE codmodulo = ".$codmodulo." ";
		
        $result = $conexion->prepare($sql);
        $result->execute(array());
		
        break;
    case 2:case 3:
        $sql = "UPDATE seguridad.modulos set estareg=:estareg
				where codmodulo=:codmodulo ";
        $result = $conexion->prepare($sql);
        $result->execute(array(":codmodulo" => $codmodulo, ":estareg" => $estareg));
        break;
}
if ($result->errorCode() != '00000') {
    $conexion->rollBack();
    $mensaje = "Error al Grabar Registro";
    echo $res = 2;
} else {
    $conexion->commit();
    $mensaje = "El Registro se ha Grabado Correctamente";
    echo $res = 1;
}
?>
