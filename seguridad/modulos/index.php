<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../include/main.php");
	include("../../include/claseindex.php");
	
	$TituloVentana = "MODULOS";
	
	$Activo = 1;
	
	CuerpoSuperior($TituloVentana);	
	
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	$codsuc = $_SESSION['IdSucursal'];

	$FormatoGrilla = array();
	
	$Sql = "SELECT m.codmodulo, m.descripcion, ";
	$Sql .= " (SELECT descripcion FROM seguridad.modulos ";
	$Sql .= "  WHERE codmodulo = m.idpadre AND codsubsistema = m.codsubsistema) AS referencia, ";
	$Sql .= " m.url, e.descripcion, m.estareg ";
	$Sql .= "FROM seguridad.modulos AS m ";
	$Sql .= " INNER JOIN seguridad.subsistemas s ON(m.codsubsistema = s.codsubsistema) ";
	$Sql .= " INNER JOIN public.estadoreg e ON(m.estareg = e.id)";
	
	$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]", ' ', $Sql);                                             //Sentencia SQL
	$FormatoGrilla[1] = array('1' => 'm.codmodulo', '2' => 'm.descripcion');          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1' => 'C&oacute;digo', 'T2' => 'Descripcion', 'T3' => 'Referencia', 'T4' => 'Url', 'T5' => 'Estado');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1' => 'center', 'A2' => 'left', 'A3' => 'left', 'A4' => 'right', 'A5' => 'center');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1' => '50', 'W5' => '90');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP' => $TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 1000;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = " ORDER BY m.codmodulo ASC ";                                   //Orden de la Consulta
	$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'6', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'6', 
              'BtnCV3'=>'0');
              
	$_SESSION['Formato'] = $FormatoGrilla;
  
	Cabecera('', $FormatoGrilla[7], 650, 360);
	Pie();
	
	CuerpoInferior();
?>
	
