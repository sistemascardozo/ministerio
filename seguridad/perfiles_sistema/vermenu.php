<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../config.php");
	
	$codsubsistema  = $_POST["codsubsistema"];
	$codperfil		= $_POST["codperfil"];
	$contador		= 0;

	$consultaP = $conexion->prepare("SELECT * FROM seguridad.modulos WHERE codsubsistema=? AND idpadre=0 AND estareg=1 ORDER BY orden");
	$consultaP->execute(array($codsubsistema));
	$itemsP = $consultaP->fetchAll();

?>
<script>
	function guardaropcion()
	{
		var cad = "";
		
		for(i = 1; i <= $("#conta").val(); i++)
		{
			if($("#estado" + i).val() == 1)
			{
				cad += "codsubsistema" + i + "=<?=$codsubsistema?>&codperfil" + i + "=<?=$codperfil?>&codmodulo" + i + "=" + $("#codmodulo" + i).val() + "&";
			}
		}

		$.ajax({
			 url:'<?php echo $_SESSION['urldir'];?>seguridad/perfiles_sistema/proceso.php',
			 type:'POST',
			 async:true,
			 data:cad + 'contador=' + $("#conta").val(),
			 success:function(datos){
				 
			 }
		}) 
		
	}
	function MarcarDesmarcar(Op)
	{
		for(i = 1; i <= $("#conta").val(); i++)
		{
			if(Op==1)
			{
				document.getElementById("chkidpadre" + i).checked = true;
				
				$("#estado" + i).val(1);
			}
			else
			{
				document.getElementById("chkidpadre"+i).checked = false;
				
				$("#estado" + i).val(0);
			}
		}
	}
</script>
<div style="padding:4px; text-align:center">
    	<label id="MarcarTodo" style="cursor:pointer" onclick="MarcarDesmarcar(1);">Marcar Todo | </label>
        <label id="DesmarcarTodo" style="cursor:pointer" onclick="MarcarDesmarcar(0);">Desmarcar Todo</label>
</div>
<div style="overflow:auto">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr align="center">
    <td colspan="6">&nbsp;</td>
  </tr>
  <?php 
  	foreach($itemsP as $rowP)
	{
		$contador++;
		
		$marcar = activarmenu($codsubsistema,$codperfil,$rowP["codmodulo"]);
		
  ?>
      <tr>
        <td width="1%" align="center">&nbsp;</td>
        <td width="2%" align="center">
          <input type="checkbox" name="chkidpadre<?=$contador?>" id="chkidpadre<?=$contador?>" <?=$marcar[2]?> onclick="CambiarEstado(this,'estado<?=$contador?>')" class="<?=$rowP["codmodulo"]?>">
          <input type="hidden" name="estado<?=$contador?>" id="estado<?=$contador?>" value="<?=$marcar[1]?>" />
        </td>
        <td colspan="4">
			<?=$rowP["descripcion"]?>
        	<input name="codmodulo<?=$contador?>" id="codmodulo<?=$contador?>" type="hidden" value="<?=$rowP["codmodulo"]?>" />
        </td>
      </tr>
      <?php 
	  		$itemsH1 = cargar_hijo($rowP["codmodulo"],$codsubsistema);
			foreach($itemsH1 as $rowH1)
			{
				$contador++;
				
				$marcar = activarmenu($codsubsistema,$codperfil,$rowH1["codmodulo"]);
	  ?>
      <tr>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td width="2%" align="center">
        	<input type="checkbox" name="chkidpadre<?=$contador?>" id="chkidpadre<?=$contador?>" <?=$marcar[2]?> onclick="CambiarEstado(this, 'estado<?=$contador?>'); CheckPadre(this, '<?=$rowP["codmodulo"]?>')" class="<?=$rowH1["codmodulo"]?>" data-padre="<?=$rowP["codmodulo"]?>"/>
            <input type="hidden" name="estado<?=$contador?>" id="estado<?=$contador?>" value="<?=$marcar[1]?>" />
        </td>
        <td colspan="3">
			<?=$rowH1["descripcion"]?>
        	<input name="codmodulo<?=$contador?>" id="codmodulo<?=$contador?>" type="hidden" value="<?=$rowH1["codmodulo"]?>" />
        </td>
      </tr>
      <?php 
	  		$itemsH2 = cargar_hijo($rowH1["codmodulo"],$codsubsistema);
			foreach($itemsH2 as $rowH2)
			{
				$contador++;
				
				$marcar = activarmenu($codsubsistema, $codperfil, $rowH2["codmodulo"]);
	  ?>
      <tr>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td width="2%" align="center">
        	<input type="checkbox" name="chkidpadre<?=$contador?>" id="chkidpadre<?=$contador?>" <?=$marcar[2]?> onclick="CambiarEstado(this, 'estado<?=$contador?>'); CheckPadre(this, '<?=$rowH1["codmodulo"]?>')" class="<?=$rowH2["codmodulo"]?>" data-padre="<?=$rowH1["codmodulo"]?>"/>
            <input type="hidden" name="estado<?=$contador?>" id="estado<?=$contador?>" value="<?=$marcar[1]?>" />
        </td>
        <td colspan="2">
			<?=$rowH2["descripcion"]?>
        	<input name="codmodulo<?=$contador?>" id="codmodulo<?=$contador?>" type="hidden" value="<?=$rowH2["codmodulo"]?>" />
        </td>
      </tr>
	  <?php 
	  		$itemsH3 = cargar_hijo($rowH2["codmodulo"],$codsubsistema);
			foreach($itemsH3 as $rowH3)
			{
				$contador++;
				
				$marcar = activarmenu($codsubsistema, $codperfil, $rowH3["codmodulo"]);
	  ?>
		  <tr>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td width="2%" align="center">
				<input type="checkbox" name="chkidpadre<?=$contador?>" id="chkidpadre<?=$contador?>" <?=$marcar[2]?> onclick="CambiarEstado(this,'estado<?=$contador?>');CheckPadre(this,'<?=$rowH2["codmodulo"]?>')" class="<?=$rowH3["codmodulo"]?>" data-padre="<?=$rowH2["codmodulo"]?>"/>
                <input type="hidden" name="estado<?=$contador?>" id="estado<?=$contador?>" value="<?=$marcar[1]?>" />
			</td>
			<td width="91%">
				<?=$rowH3["descripcion"]?>
				<input name="codmodulo<?=$contador?>" id="codmodulo<?=$contador?>" type="hidden" value="<?=$rowH3["codmodulo"]?>" />
			</td>
		  </tr>
	  
  <?php } } } } ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="4"><input type="hidden" name="conta" id="conta" value="<?=$contador?>" /></td>
  </tr>
  </table>
</div>
<?php 
	function cargar_hijo($idpadre,$idsubsistema)
	{
		global $conexion;
		
		$sql = "select * from seguridad.modulos where idpadre=? and codsubsistema=? AND estareg=1 order by orden";
		$consultaH = $conexion->prepare($sql);
		$consultaH->execute(array($idpadre,$idsubsistema));
		return $consultaH->fetchAll();
	}
	function activarmenu($codsubsistema,$codperfil,$codmodulo)
	{
		global $conexion;
		
		$sql 		= "select count(*) from seguridad.subsistema_perfiles where codsubsistema=? and codperfil=? and codmodulo=?";
		$consulta 	= $conexion->prepare($sql);
		$consulta->execute(array($codsubsistema,$codperfil,$codmodulo));
		$items = $consulta->fetch();
		
		$activa 	= 0;
		$checked 	= "";
		
		if($items[0]>0)
		{
			$activa  = 1;
			$checked = "checked='checked'";
		}
		
		return array(1=>$activa,2=>$checked);
	}

?>