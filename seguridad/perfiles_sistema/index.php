<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}

	include("../../include/main.php");
	include("../../include/claseindex.php");
	
  $TituloVentana = "PERFILES POR SISTEMAS";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];

  $FormatoGrilla = array ();
  $Sql = "select s.codsistema,s.nombre_sistema, e.descripcion,s.estareg
          from seguridad.sistemas s
          LEFT JOIN public.estadoreg e ON (s.estareg=e.id)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'s.codsistema', '2'=>'s.nombre_sistema');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo','T2'=>'Sistema','T3'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'center', 'A4'=>'right');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W3'=>'90');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 800;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " AND s.estareg=1 ORDER BY s.codsistema  ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'4',  //Item a Comparar
              'BtnCV1'=>'1'   //Valor de comparación
              );
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 750, 500);
  Pie();
  CuerpoInferior();
?>
<script>
  $('#BtnNuevoB').css('display', 'none');
</script>