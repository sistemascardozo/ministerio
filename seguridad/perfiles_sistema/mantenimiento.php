<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../objetos/clsMantenimiento.php");
	
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	
	$objMantenimiento 	= new clsMantenimiento();
	$sistemas 			= $objMantenimiento->setSistemas(" WHERE codsistema = ? ", $Id);
	
	$sqlsubsistema 		= $objMantenimiento->Sentencia("subsistema")." WHERE s.codsistema = ? ORDER BY s.descripcion ";
	$sqlperfiles		= $objMantenimiento->Sentencia("perfiles")." ORDER BY descripcion ";
	
	//---subsistemas---
	$conssubsistema 	= $conexion->prepare($sqlsubsistema);
	$conssubsistema->execute(array($Id));
	$itemssubsistema	= $conssubsistema->fetchAll();
	
	//---perfiles---
	$consperfiles 		= $conexion->prepare($sqlperfiles);
	$consperfiles->execute();
	$itemsperfiles		= $consperfiles->fetchAll();
	
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var count=0;
	var contador=0;
	
	/*function ValidarForm(Op)
	{
		return true;
	}*/
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function Agregar()
	{
		if($("#subsistema").val()==0)
		{
			alert("Seleccione el Modulo")
			return false;
		}
		if($("#perfiles").val()==0)
		{
			alert("Seleccione el Perfil")
			return false;
		}
		
		count 		= count + 1;
		contador 	= contador + 1;
		
		var subsist = $("#subsistema option:selected").text();
		var perf	= $("#perfiles option:selected").text();
		
		for(var i=1;i<=$("#cont").val();i++)
		{
			try
			{
				if($("#codsubsistema"+i).val()==$("#subsistema").val() && $("#codperfil"+i).val()==$("#perfiles").val())
				{
					alert('El Perfiles se encuentra agregado para este subsistema')
					return false
				}
			}catch(exp)
			{
				
			}
		}
		
		$( "#tbperfiles tbody" ).append( 
									"<tr>"+
									"<td><input type='hidden' name='codsubsistema"+contador+"' id='codsubsistema"+contador+"' value='"+$("#subsistema").val()+"'>"+subsist+"</td>"+
									"<td><input type='hidden' name='codperfil"+contador+"' id='codperfil"+contador+"' value='"+$("#perfiles").val()+"'>"+perf+"</td>"+
									"<td align='center'><img src='../../images/iconos/verDetalle.png' width='16' height='16' style='cursor:pointer' onclick='window.parent.abrir_popup_menu("+$("#subsistema").val()+","+$("#perfiles").val()+");' /></td>"+
									"<td align='center'><span class='icono-icon-trash' onclick='QuitaFilaD(this);' ></span></td>"+
									"</tr>"
										)
		$("#cont").val(contador)
	}
	function QuitaFilaD(x)
	{	
		while (x.tagName.toLowerCase() !='tr')
		{
			if(x.parentElement)
				x=x.parentElement;
			else if(x.parentNode)
				x=x.parentNode;
			else
				return;
		}
		
		var rowNum=x.rowIndex;
		while (x.tagName.toLowerCase() !='table')
		{
			if(x.parentElement)
				x=x.parentElement;
			else if(x.parentNode)
				x=x.parentNode;
			else
				return;
		}
		x.deleteRow(rowNum);
	}
	function CheckPadre(obj,Padre)
	{
		try
		{
			if($(obj).attr("checked")=="checked")
				{
					$("."+Padre).attr("checked","checked"); 
					CheckPadre($("."+Padre),$("."+Padre).data('padre'))
					//$("."+Padre).click();
				}	
			

		}
		catch(err)
		{}
	}

</script>
<div align="center">
<form id="form1" name="form1" method="post" enctype="multipart/form-data" action="guardar.php">
  <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	 <td width="50" class="TitDetalle">&nbsp;</td>
	 <td width="30" class="TitDetalle">&nbsp;</td>
	 <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
     <td class="TitDetalle">Id</td>
     <td align="center" class="TitDetalle">:</td>
     <td class="CampoDetalle">
	   <input name="codsistema" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
     </td>
	</tr>
	<tr>
	 <td class="TitDetalle">Nombre</td>
	 <td align="center" class="TitDetalle">:</td>
	 <td class="CampoDetalle">
	   <input class="inputtext" name="0form1_nombres" type="text" id="Nombre"  maxlength="200" <?=$Enabled?> value="<?=$sistemas["nombre_sistema"]?>" style="width:400px"/>
     </td>
	</tr>
	<tr>
	 <td class="TitDetalle">&nbsp;</td>
	 <td class="TitDetalle">&nbsp;</td>
	 <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
	 <td colspan="3" class="TitDetalle" style="background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px">
    	Seleccione el Perfil
     </td>
	</tr>
	<tr>
	 <td colspan="3" class="TitDetalle">
     <fieldset>
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr style="padding:4px">
           <td width="60">M&oacute;dulo</td>
           <td width="30" align="center">:</td>
           <td>
              <select name="subsistema" id="subsistema" class="select" style="width:280px">
                <option value="0">--Seleccione el M&oacute;dluo--</option>
                <?php
                    foreach($itemssubsistema as $rowsubsistema)
                    {
                        echo "<option value='".$rowsubsistema["codsubsistema"]."'>".strtoupper($rowsubsistema["descripcion"])."</option>";
                    }
                ?>
              </select>
              <img src="../../images/iconos/add.png" width="16" height="16" style="cursor:pointer" alt="Agrega un Item al Registro" onclick="Agregar();" />
           </td>
         </tr>
         <tr style="padding:4px">
           <td>Perfiles</td>
           <td align="center">:</td>
           <td><select name="perfiles" id="perfiles" class="select" style="width:280px">
             <option value="0">--Seleccione el Perfil--</option>
             <?php
                    foreach($itemsperfiles as $rowperfil)
                    {
                        echo "<option value='".$rowperfil["codperfil"]."'>".strtoupper($rowperfil["descripcion"])."</option>";
                    }
                ?>
           </select></td>
         </tr>
       </table>
     </fieldset></td>
	</tr>
	<tr>
	 <td colspan="3" class="TitDetalle">
          <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbperfiles" rules="all" >
            <tr class="ui-widget-header" >
              <td align="center" >M&oacute;dulo</td>
              <td align="center" >Perfil</td>
              <td width="30" align="center" >&nbsp;</td>
              <td width="30" align="center" >&nbsp;</td>
            </tr>
			<?php 
				$cont = 0;
				
				$sqlD= "SELECT s.descripcion,per.descripcion,p.codsubsistema,p.codperfil
					FROM seguridad.subsistema_perfiles as p 
					inner join seguridad.subsistemas as s on(p.codsubsistema=s.codsubsistema)
					inner join seguridad.perfiles as per on(p.codperfil=per.codperfil)
					WHERE s.codsistema=? 
					GROUP BY s.descripcion,per.descripcion,p.codsubsistema,p.codperfil
					ORDER BY s.descripcion ASC ";
				$consultaD 	= $conexion->prepare($sqlD);
				$consultaD->execute(array($Id));
				$itemsD = $consultaD->fetchAll();

				foreach($itemsD as $rowD)
				{
					$cont += 1;
			?>            
                <tr>
                  <td style="padding-left:5px;">
				  	<?=strtoupper($rowD[0])?>
                    <input type='hidden' name='codsubsistema<?=$cont?>' id='codsubsistema<?=$cont?>' value='<?=$rowD["codsubsistema"]?>'>
                  </td>
                  <td style="padding-left:5px;">
				  	<?=strtoupper($rowD[1])?>
                  	<input type='hidden' name='codperfil<?=$cont?>' id='codperfil<?=$cont?>' value='<?=$rowD["codperfil"]?>'>
                  <td align="center">
                      <img src="../../images/iconos/verDetalle.png" width="16" height="16" style="cursor:pointer" onclick="window.parent.abrir_popup_menu(<?=$rowD["codsubsistema"]?>, <?=$rowD["codperfil"]?>)"/>
                  </td>
                  <td align="center">
                  	  <span class="icono-icon-trash" onclick="QuitaFilaD(this);" ></span>
                  </td>
                </tr>
            <?php }?>
            <script>
				count 		= <?=$cont?>;
				contador 	= <?=$cont?>;
			</script>
          </table></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle"><input type="hidden" name="cont" id="cont" value="<?=$cont?>" /></td>
	</tr>
   </tbody>

 </table>
 </form>
</div>