<?php
    include("clsproceso.php");
    include('../../objetos/clsFunciones.php');

    $conexion->beginTransaction();

    $objFunciones = new clsFunciones();

    $codsistema = $_POST["codsistema"];
    $item = $_SESSION["oModulos"]->item;

	$Error = 0;
	
    for ($i = 0; $i < $item; $i++) {
        if (isset($_SESSION["oModulos"]->codmodulo[$i])) {
            $codsubsistema = $_SESSION["oModulos"]->codsubsistema[$i];
            $codperfil = $_SESSION["oModulos"]->codperfil[$i];
            $codmodulo = $_SESSION["oModulos"]->codmodulo[$i];

            $Sql = "DELETE FROM seguridad.subsistema_perfiles ";
			$Sql .= "WHERE codsubsistema = ".$codsubsistema." AND codperfil = ".$codperfil." ";
			
            $result = $conexion->query($Sql);
            if (!$result) {
                //$conexion->rollBack();
                echo $mensaje = "Error al DELETE Registro ";
                echo $res = 2;
				
				$Error++;
            }
            //echo $Sql."<br>";
            //$consultaDel 	= $conexion->prepare("delete from seguridad.subsistema_perfiles where codsubsistema=? and codperfil=? and codmodulo=?");
            //$resultDel 		= $consultaDel->execute(array($codsubsistema,$codperfil,$codmodulo));
        }
    }

    //print_r($_SESSION["oModulos"]);
	for ($i = 0; $i < $item; $i++)
	{
        if (isset($_SESSION["oModulos"]->codmodulo[$i]))
		{
            $codsubsistema = $_SESSION["oModulos"]->codsubsistema[$i];
            $codperfil = $_SESSION["oModulos"]->codperfil[$i];
            $codmodulo = $_SESSION["oModulos"]->codmodulo[$i];

            /* $sql = "insert into seguridad.subsistema_perfiles(codsubsistema,codperfil,codmodulo) values(?,?,?)";
              $consulta1 = $conexion->prepare($sql);
              $result = $consulta1->execute(array($codsubsistema,$codperfil,$codmodulo)); */

            $Sql = "INSERT INTO seguridad.subsistema_perfiles ";
			$Sql .= " (codsubsistema, codperfil, codmodulo) ";
			$Sql .= "VALUES(".$codsubsistema.", ".$codperfil.", ".$codmodulo.");";
			
            $result = $conexion->query($Sql);
			
            if (!$result) {
                //$conexion->rollBack();
                echo $mensaje = "Error al INSERT Registro ".$Sql;
                echo $res = 2;
				
				$Error++;
            }
        }
    }
    unset($_SESSION["oModulos"]);
    
    if($Error > 0)
	{
        $conexion->rollBack();
        $mensaje = "Error al Grabar Registro";
        echo $res = 2;
    } else {
        $conexion->commit();
        $mensaje = "El Registro se ha Grabado Correctamente";
        echo $res = 1;
    }
?>
