<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	class modulos
	{
		var $item;
		var $codsubsistema;
		var $codperfil;
		var $codmodulo;
		
		function modulos()
		{
			$this->item				= 0;
			$this->codsubsistema	= "";
			$this->codperfil		= "";
			$this->codmodulo		= "";
		}
		function insertarvalores($codsubsistema, $codperfil, $codmodulo)
		{
			$this->codsubsistema[$this->item] 	= $codsubsistema;
			$this->codperfil[$this->item]		= $codperfil;
			$this->codmodulo[$this->item]		= $codmodulo;
			$this->item++;
		}
		function actualizarvalores($codsubsistema, $codperfil, $codmodulo, $item)
		{
			$this->codsubsistema[$item] = $codsubsistema;
			$this->codperfil[$item]		= $codperfil;
			$this->codmodulo[$item]		= $codmodulo;
		}
		function agregarvalores($codsubsistema, $codperfil, $codmodulo)
		{
			$idx = 0;
			
			for($i = 0; $i < $this->item; $i++)
			{
				if($this->codsubsistema[$i] == $codsubsistema && $this->codperfil[$i] == $codperfil && $this->codmodulo[$i] == $codmodulo)
				{
					$idx = $i;
				}
			}
			
			if($idx != 0)
			{
				$this->actualizarvalores($codsubsistema, $codperfil, $codmodulo, $idx);
			}else{
				$this->insertarvalores($codsubsistema, $codperfil, $codmodulo);
			}
		}
	}
	
	if (!isset($_SESSION["oModulos"])){ 
 		$_SESSION["oModulos"] = new modulos();
	}
?>