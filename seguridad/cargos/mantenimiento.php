<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsMantenimiento();

	if($Id!='')
	{
		$cargo 		= $conexion->prepare($objMantenimiento->Sentencia("cargos_usuarios")." where codcargo=?");
		$cargo->execute(array($Id));
		$row = $cargo->fetch();
		
		$guardar	= $guardar."&Id2=".$Id;

	}
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if($("#Nombre").val()=="")
		{
			alert("La Descripcion del Cargo no puede ser NULO");
			return false
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
 <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id</td>
      <td align="center" class="TitDetalle">:</td>
      <td class="CampoDetalle">
		<input name="codcargo" type="text" id="Id" size="4" maxlength="2" value="<? echo $Id; ?>" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="Descripcion" type="text" id="Nombre" maxlength="200" value="<?=$row["descripcion"]?>" style="width:400px"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><? include("../../include/estareg.php"); ?></td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
   </tbody>
   </table>
 </form>
</div>
<script>
  $("#Nombre").focus();
</script>
<?php
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../admin/validaciones/estareg.php"); 
?>