<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codsistema			= $_POST["codsistema"]?$_POST["codsistema"]:$_POST["1form1_id"];
	$nombre_sistema		= strtoupper($_POST["Descripcion"]);
	$nombre_corto		= strtoupper($_POST["nombre_corto"]);
	$img				= $_POST["img"];
	$carpeta			= $_POST["carpeta"];
	$orden 				= $_POST["Ord"];
	$estareg			= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("sistemas","0","0");
			$codsistema 	= $id[0];
			
			$sql = "insert into seguridad.sistemas(codsistema,nombre_sistema,nombre_corto,img,orden,estareg,carpeta) 
				values(:codsistema,:nombre_sistema,:nombre_corto,:img,:orden,:estareg,:carpeta)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsistema"=>$codsistema,":nombre_sistema"=>$nombre_sistema,":nombre_corto"=>$nombre_corto,":img"=>$img,
					":estareg"=>$estareg,":orden"=>$orden,":carpeta"=>$carpeta));


		break;
		case 1:
			$sql = "update seguridad.sistemas set nombre_sistema=:nombre_sistema,nombre_corto=:nombre_corto,
				img=:img,estareg=:estareg,orden=:orden,carpeta=:carpeta where codsistema=:codsistema";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsistema"=>$codsistema,":nombre_sistema"=>$nombre_sistema,":nombre_corto"=>$nombre_corto,":img"=>$img,
					":estareg"=>$estareg,":orden"=>$orden,":carpeta"=>$carpeta));
		break;
		case 2:case 3:
			$sql = "update seguridad.sistemas set estareg=:estareg
				where codsistema=:codsistema ";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsistema"=>$codsistema,":estareg"=>$estareg));
		break;
	
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
