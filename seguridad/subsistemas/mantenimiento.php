<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	$urldir = $_SESSION['urldir'];
	
	include("../../objetos/clsMantenimiento.php");
		
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$img 		= "../../images/interrogacion1.png";
	
	$objMantenimiento = new clsMantenimiento();
	
	$sistemas = $objMantenimiento->setSistemas();

	if($Id != '')
	{
		$subsistema	= $objMantenimiento->setSubsistemas(" where s.codsubsistema=?",$Id);		
		$guardar	= $guardar."&Id2=".$Id;
		
		$img 		= "imagenes/".$subsistema["imagen"];
	}
	
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/swfobject.js"></script>
<script>
	$(document).ready(function() {
		$('#cargar').uploadify({
			'script'    : 'uploader.php',
			'cancelImg' : 'cancel.png',
			'auto'      : true,
			'folder'    : 'imagenes',
			'onComplete': function(event, queueID, fileObj, response, data) {
				alert(response)
				document.getElementById("img_subsistema").src="imagenes/"+response
				$("#img").val(response)
			}
		});
	});
	function ValidarForm(Op)
	{
		if($("#sistemas").val()==0)
		{
			alert("Seleccione el Sistema")
			return false
		}
		if($("#Nombre").val()=="")
		{
			alert("La Descripcion del Sub. Sistema no puede ser NULO");
			return false
		}
		if($("#Ord").val()=="" || $("#Ord").val()==0)
		{
			alert("El Orden Ingresado no es Valido")
			return false
		}
	
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	 <td class="TitDetalle">&nbsp;</td>
	 <td width="30" class="TitDetalle">&nbsp;</td>
	 <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
     <td class="TitDetalle">Id</td>
     <td align="center" class="TitDetalle">:</td>
     <td class="CampoDetalle">
		<input name="codsubsistema" type="text" id="Id" size="4" maxlength="2" <? echo $Enabled2 ?> value="<? echo $Id; ?>" class="inputtext"/>
     </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Sistema</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><label>
	    <select name="sistemas" id="sistemas" class="select" style="width:280px">
        	<option value="0">--Seleccione el Sistema--</option>
            <?php
				foreach($sistemas as $rowsistema)
				{
					$selected="";
					if($subsistema["codsistema"]==$rowsistema["codsistema"])
					{
						$selected = "selected='selected'";
					}
										
					echo "<option value='".$rowsistema["codsistema"]."' ".$selected." >".$rowsistema["nombre_sistema"]."</option>";
				}
			?>
	    </select>
	  </label></td>
	  </tr>
	<tr>
	 <td class="TitDetalle">Descripcion</td>
	 <td align="center" class="TitDetalle">:</td>
	 <td class="CampoDetalle">
	   <input class="inputtext" name="Descripcion" type="text" id="Nombre" maxlength="200" value="<?=$subsistema["descripcion"]?>" style="width:400px;"/>
     </td>
	</tr>
	<tr valign="top">
	  <td class="TitDetalle">Descripcion Corta</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <textarea name="nombre_corto" id="nombre_corto" rows="5" class="inputtext" style="width:400px;"><?=$subsistema["notas"]?></textarea>
      </td>
	</tr>
	<tr valign="top">
	  <td class="TitDetalle">Imagen</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
      	<img src="<?=$img?>" id="img_subsistema" width="45" height="45" />
        <div>
        	<input name="cargar" id="cargar" type="button" value="Cargar Imagen" />
        </div>
      </td>
	</tr>
	<tr>
	 <td class="TitDetalle">Orden</td>
	 <td align="center" class="TitDetalle">:</td>
	 <td class="CampoDetalle">
     	<input class="inputtext" name="Ord" type="text" id="Ord" maxlength="10" value="<?=$subsistema["orden"]?>" style="width:50px;"/>
     	<input type="hidden" name="img" id="img" value="<?=$subsistema["imagen"]?>" />
     </td>
	</tr>
    <tr>
	 <td class="TitDetalle">Estado</td>
	 <td align="center" class="TitDetalle">:</td>
	 <td class="CampoDetalle"><? include("../../include/estareg.php"); ?></td>
	</tr>
	<tr>
	 <td class="TitDetalle">&nbsp;</td>
	 <td class="TitDetalle">&nbsp;</td>
	 <td class="CampoDetalle">&nbsp;</td>
	</tr>
   </tbody>
  </table>
</form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php 
	$est = isset($subsistema["estareg"])?$subsistema["estareg"]:1;
	include("../../admin/validaciones/estareg.php"); 
?>