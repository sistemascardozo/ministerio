<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include('../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codsubsistema 		= $_POST["codsubsistema"]?$_POST["codsubsistema"]:$_POST["1form1_id"];
	$sistema			= $_POST["sistemas"];
	$nombre_subsistema	= strtoupper($_POST["Descripcion"]);
	$nombre_corto		= strtoupper($_POST["nombre_corto"]);
	$img				= $_POST["img"];
	$orden 				= $_POST["Ord"];
	$estareg			= $_POST["estareg"];
	
	switch ($Op) 
	{
		case 0:
			$id   			= $objFunciones->setCorrelativos("subsistemas","0","0");
			$codsubsistema 	= $id[0];
		
			$sql = "insert into seguridad.subsistemas(codsubsistema,codsistema,descripcion,notas,imagen,estareg,orden) 
				values(:codsubsistema,:codsistema,:descripcion,:notas,:imagen,:estareg,:orden)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsubsistema"=>$codsubsistema,":codsistema"=>$sistema,":descripcion"=>$nombre_subsistema,":notas"=>$nombre_corto,
					":imagen"=>$img,":estareg"=>$estareg,":orden"=>$orden));

		break;
		case 1:
			$sql = "update seguridad.subsistemas set codsistema=:codsistema,descripcion=:descripcion,notas=:notas,
				imagen=:imagen,estareg=:estareg,orden=:orden where codsubsistema=:codsubsistema";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsubsistema"=>$codsubsistema,":codsistema"=>$sistema,":descripcion"=>$nombre_subsistema,":notas"=>$nombre_corto,
					":imagen"=>$img,":estareg"=>$estareg,":orden"=>$orden));
		break;
		case 2:case 3:
			$sql = "update seguridad.subsistemas set estareg=:estareg
				where codsubsistema=:codsubsistema ";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codsubsistema"=>$codsubsistema,":estareg"=>$estareg));
		break;
	
	}
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
