<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../objetos/clsMantenimiento.php");
	
	$objMantenimiento = new clsMantenimiento();
	
	$Id = isset($_POST["Id"])?$_POST["Id"]:'';
	
	$usuario 	= $objMantenimiento->setUsuario($Id);
	$sistemas 	= $objMantenimiento->setSistemas();
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	var count = 0;
	var contador = 0;

	function Cancelar()
	{
		location.href='index.php';
	}
	function Agregar()
	{
		if($("#sistemas").val()==0)
		{
			alert("Seleccione el Sub Sistema")
			return
		}
		
		count 		= count + 1;
		contador 	= contador + 1;
		
		var sist	= $("#sistemas option:selected").text();
		
		for(var i=1;i<=$("#cont").val();i++)
		{
			try
			{
				if($("#codsistema"+i).val()==$("#sistemas").val())
				{
					alert('El Sistema ya tiene asignado un Usuario')
					return false
				}
			}catch(exp)
			{
				
			}
		}
		
		
		$( "#tbperfiles tbody" ).append( 
							"<tr>"+
							"<td><input type='hidden' name='codsistema"+contador+"' id='codsistema"+contador+"' value='"+$("#sistemas").val()+"'>"+sist+"</td>"+
							"<td align='center'><span class='icono-icon-trash' onclick='QuitaFilaD(this);'></span> </td>"+
							"</tr>"
								)
								
		$("#cont").val(contador)
	}
	function QuitaFilaD(x)
	{	
		while (x.tagName.toLowerCase() !='tr')
		{
			if(x.parentElement)
				x=x.parentElement;
			else if(x.parentNode)
				x=x.parentNode;
			else
				return;
		}
		
		var rowNum=x.rowIndex;
		while (x.tagName.toLowerCase() !='table')
		{
			if(x.parentElement)
				x=x.parentElement;
			else if(x.parentNode)
				x=x.parentNode;
			else
				return;
		}
		x.deleteRow(rowNum);
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $Guardar;?>" enctype="multipart/form-data">
  <table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
   <tbody>
	<tr>
	  <td width="60" class="TitDetalle">&nbsp;</td>
	  <td width="30" class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
      <td class="TitDetalle">Id</td>
      <td align="center" class="TitDetalle">:</td>
      <td class="CampoDetalle">
		<input name="codusu" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Nombre</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="nombres" type="text" id="Nombre" readonly="readonly" maxlength="200" value="<?=$usuario["nombres"]?>" style="width:400px;"/>
      </td>
	</tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	</tr>
	<tr>
	  <td colspan="3" class="TitDetalle" style="background-color:#06F; color:#FFF; font-size:12px; font-weight:bold; padding:4px">
    	Seleccione el Sub Sistema y el SubSistema
      </td>
	</tr>
	<tr>
	  <td colspan="3" class="TitDetalle">
          <fieldset>
          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr style="padding:4px">
                <td width="70">Sub Sistema</td>
                <td width="30" align="center">:</td>
                <td>
                  <select name="sistemas" id="sistemas" class="select" style="width:280px" >
                  	<option value="0">--Seleccione el Sub Sistema--</option>
                    <?php
						foreach($sistemas as $rowsistema)
						{
							echo "<option value='".$rowsistema["codsistema"]."' >".$rowsistema["nombre_sistema"]."</option>";
						}
					?>
                  </select>
                  <img src="../../images/iconos/add.png" width="16" height="16" style="cursor:pointer" alt="Agrega un Item al Registro" onclick="Agregar();" />
                 </td>
              </tr>
            </table>
          </fieldset></td>
	  </tr>
	<tr>
	  <td colspan="3" class="TitDetalle">
          <table border="1" align="center" cellspacing="0" class="ui-widget"  width="100%" id="tbperfiles" rules="all" >
            <tr class="ui-widget-header" >
              <td align="center" >Sistema</td>
              <td width="30" align="center" >&nbsp;</td>
            </tr>
			<?php 
				$cont = 0;
				
				$sqlD = "select s.nombre_sistema,usu.codsistema
						from seguridad.sistemas_usuarios as usu
						inner join seguridad.sistemas as s on(usu.codsistema=s.codsistema) 
						where usu.codusu=? order by s.codsistema";
				$consultaD = $conexion->prepare($sqlD);
				$consultaD->execute(array($Id));
				$itemsD = $consultaD->fetchAll();
				
				foreach($itemsD as $rowD)
				{
					$cont += 1;
			?>            
                <tr>
                  <td style="padding-left:5px;">
				  	<?=strtoupper($rowD["nombre_sistema"])?>
                    <input type='hidden' name='codsistema<?=$cont?>' id='codsistema<?=$cont?>' value='<?=$rowD["codsistema"]?>'>
                  </td>
                  <td align="center">
                  		<span class="icono-icon-trash" onClick='QuitaFilaD(this)'></span> 
                  </td>
                </tr>
            <?php }?>
            <script>
				count	 = <?=$cont?>;
				contador = <?=$cont?>;
				$("#cont").val(<?=$cont?>);
			</script>
          </table></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle"><input type="hidden" name="cont" id="cont" value="<?=$cont?>" /></td>
	  </tr>
    </tbody>
  </table>
 </form>
</div>