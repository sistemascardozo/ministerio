<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
  include("../../include/main.php");
  include("../../include/claseindex.php");
  $TituloVentana = "SISTEMAS POR USUARIOS";
  $Activo=1;
  CuerpoSuperior($TituloVentana);
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];

  $FormatoGrilla = array ();
  $Sql = "select codusu,login,nombres,direccion,e.descripcion,u.estareg
            from seguridad.usuarios u
          INNER JOIN public.estadoreg e ON (u.estareg=e.id)";
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'codusu', '2'=>'nombres','3'=>'login');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo','T2'=>'Login','T3'=>'Usuario','T4'=>'Direccion','T5'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'right', 'A5'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W5'=>'90');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " ORDER BY nombres  ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              );
              
  $_SESSION['Formato'] = $FormatoGrilla;
  Cabecera('', $FormatoGrilla[7], 750, 500);
  Pie();
  CuerpoInferior();
?>
<script type="text/javascript"> 
  $('#BtnNuevoB').css('display', 'none');
</script>


