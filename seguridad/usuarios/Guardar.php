<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	 
	include('../../objetos/clsFunciones.php');
       
	include("../../objetos/clsseguridad.php");
	
	$conexion->beginTransaction();
	
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	$objSeguridad = new clsSeguridad();
	
	$codusu		= $_POST["codusu"]?$_POST["codusu"]:$_POST["1form1_id"];
	$sucursal 	= $_POST["sucursal"];
	$nombre 	= strtoupper($_POST["nombres"]);
	$direccion	= strtoupper($_POST["direccion"]);
	$login		= strtoupper($_POST["login"]);
	$contra		= strtoupper($_POST["contra"]);
	$estareg	= $_POST["estareg"];
	$cargo		= $_POST["cargo"];
	$areas		= $_POST["areas"];
	$dni		= $_POST["dni"];
	$car		= $_POST["car"];
	$contra = $objSeguridad->encriptar($contra);
	
	switch ($Op) 
	{
		case 0:
			$id   	= $objFunciones->setCorrelativos("usuarios","0","0");
			$codusu = $id[0];
			
			$sql  = "INSERT INTO seguridad.usuarios(codusu,codemp,codsuc,login,contra,nombres,direccion,estareg,codcargo,codarea,dni,car) ";
			$sql .= "values(:codusu,:codemp,:codsuc,:login,:contra,:nombres,:direccion,:estareg,:codcargo,:codarea,:dni,:car)";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codusu"=>$codusu,":codemp"=>1,":codsuc"=>$sucursal,":login"=>$login,":contra"=>trim($contra),
					":nombres"=>$nombre,":direccion"=>$direccion,":estareg"=>$estareg,":codcargo"=>$cargo,":codarea"=>$areas,":dni"=>$dni,":car"=>$car));

		break;
		case 1:
			$sql  = "UPDATE seguridad.usuarios SET codemp=:codemp,codsuc=:codsuc,login=:login,contra=:contra,nombres=:nombres,";
			$sql .= "direccion=:direccion,estareg=:estareg,codcargo=:codcargo,codarea=:codarea,dni=:dni,car=:car where codusu=:codusu";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codusu"=>$codusu,":codemp"=>1,":codsuc"=>$sucursal,":login"=>$login,":contra"=>trim($contra),
					":nombres"=>$nombre,":direccion"=>$direccion,":estareg"=>$estareg,":codcargo"=>$cargo,":codarea"=>$areas,":dni"=>$dni,":car"=>$car));

		break;
		case 2:case 3:
			$sql = "UPDATE seguridad.usuarios SET estareg=:estareg
				where codusu=:codusu ";
			$result = $conexion->prepare($sql);
			$result->execute(array(":codusu"=>$codusu,":estareg"=>$estareg));
		break;
	
	}

	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}

?>
