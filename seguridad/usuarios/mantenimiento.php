<?php
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../objetos/clsDrop.php");
	
	include("../../objetos/clsseguridad.php");
		
	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$codsuc 	= $_SESSION['IdSucursal'];
	$guardar	= "op=".$Op;
	
	$objMantenimiento 	= new clsDrop();
	$objSeguridad 		= new clsSeguridad();
	
	$sucursal = $objMantenimiento->setSucursales();
	
	$cargo_consulta = $conexion->prepare($objMantenimiento->Sentencia("cargos_usuarios")." where estareg=1");
	$cargo_consulta->execute();
	$item_cargo = $cargo_consulta->fetchAll();
	
	if($Id!='')
	{
		$usuario 	= $objMantenimiento->setUsuario($Id);
		$guardar	= $guardar."&Id2=".$Id;
		
		$contra = $objSeguridad->desencriptar($usuario["contra"]);
		$codarea = $usuario["codarea"];
	}
	
?>
<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
$(document).ready(function(){
cAreas()

});

	function ValidarForm(Op)
	{
		if($("#sucursal").val()=="0")
		{
			Msj($("#sucursal"),"Seleccione la Sucursal");
			return false
		}
		if($("#dni").val()=="")
		{
			Msj($("#dni"),"Digite el DNI");
			return false
		}
		if($("#Nombre").val()=="")
		{
			Msj($("#Nombre"),"Digite el Nombre");
			return false
		}
		if($("#Login").val()=="")
		{
			Msj($("#Login"),"Digite el Login del Usuario");
			return false
		}
		if($("#Contra").val()=="")
		{
			Msj($("#Contra"),"Digite la Contraseņa del Usuario");
			return false
		}
		if($("#cargo").val()==0)
		{
			Msj($("#cargo"),"Seleccione el Cargo")
			return false
		}
		if($("#areas").val()==0)
		{
			Msj($("#areas"),"Seleccione el Area")
			return false
		}
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	function cAreas()
	{
		var codsuc = $("#sucursal").val()
		if(codsuc!=0)
		{			
	
		 $.ajax({
			 url:'<?php echo $_SESSION['urldir'];?>ajax/cAreas.php',
			 type:'POST',
			 async:true,
			 data:'codsuc='+codsuc+'&codarea=<?=$codarea?>',
			 success:function(data)
			 {
				
				 $("#areas").html(data)
				 }
		 })
		}
	}
	
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos" >
    <tbody>
	 <tr>
	  	<td class="TitDetalle">&nbsp;</td>
	  	<td width="30" class="TitDetalle">&nbsp;</td>
	  	<td class="CampoDetalle">&nbsp;</td>
	 </tr>
	 <tr>
        <td class="TitDetalle">Id</td>
        <td align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
			<input name="codusu" type="text" id="Id" size="4" maxlength="2" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
        </td>
	 </tr>
	 <tr>
	   <td class="TitDetalle">Sucursal</td>
	   <td align="center" class="TitDetalle">:</td>
	   <td class="CampoDetalle"><label>
	     <select name="sucursal" id="sucursal" class="select" style="width:310px" onchange="cAreas()">
         	<option value="0">--Seleccione la Sucursal--</option>
            <?php
				foreach($sucursal as $rowsucursal)
				{
					$selected="";
					if($rowsucursal["codsuc"]==$usuario["codsuc"])
					{
						$selected="selected='selected'";
					}
					
					echo "<option value='".$rowsucursal["codsuc"]."' ".$selected." >".$rowsucursal["descripcion"]."</option>";
				}
			?>
         </select>
	   </label></td>
	   </tr>
	   <tr>
	    <td class="TitDetalle">DNI</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
	        <input class="inputtext" name="dni" type="text" id="dni" maxlength="200" value="<?=$usuario["dni"]?>" style="width:80px;"/>    
        </td>
	 </tr>
	 <tr>
	    <td class="TitDetalle">Nombre</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
	        <input class="inputtext" name="nombres" type="text" id="Nombre" maxlength="200" value="<?=$usuario["nombres"]?>" style="width:400px;"/>    
        </td>
	 </tr>
	 <tr>
	    <td class="TitDetalle">Direccion</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
        	<input class="inputtext" name="direccion" type="text" id="Direccion" maxlength="200" value="<?=$usuario["direccion"]?>" style="width:400px;"/>
        </td>
	 </tr>
	 <tr>
	    <td class="TitDetalle">Login</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
        	<input class="inputtext" name="login" type="text" id="Login" size="20" style="width:150px;" value="<?=$usuario["login"]?>"/>
            <div id="div_error"></div>        </td>
	 </tr>
	 <tr>
	    <td class="TitDetalle">Password</td>
	    <td align="center" class="TitDetalle">:</td>
	    <td class="CampoDetalle">
        	<input class="inputtext" name="contra" type="password" id="Contra" size="22" maxlength="20" style="width:150px;" value="<?=$contra?>"/>
        </td>
	 </tr>
	 <tr>
	   <td class="TitDetalle">Cargo</td>
	   <td align="center" class="TitDetalle">:</td>
	   <td class="CampoDetalle">
       	<select name="cargo" id="cargo" class="select" style="width:310px">
         	<option value="0">--Seleccione el Cargo--</option>
            <?php
				foreach($item_cargo as $row_cargo)
				{
					$selected="";
					if($row_cargo["codcargo"]==$usuario["codcargo"])
					{
						$selected="selected='selected'";
					}
					
					echo "<option value='".$row_cargo["codcargo"]."' ".$selected." >".$row_cargo["descripcion"]."</option>";
				}
			?>
         </select>
       </td>
	 </tr>
	 <tr>
	   <td class="TitDetalle">Area</td>
	   <td align="center" class="TitDetalle">:</td>
	   <td class="CampoDetalle">
	   	<select style="width:220px" class="select" name="areas" id="areas">

	   	</select>
	   </td>
	   </tr>
	   <tr>
    <td class="TitDetalle">CAR</td>
    <td align="center" class="TitDetalle">:</td>
    <td class="CampoDetalle">
    	<? echo $objMantenimiento->drop_car($codsuc,$usuario["car"]); ?>
    </td>
  </tr>
     <tr>
	  <td class="TitDetalle">Estado</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><? include("../../include/estareg.php"); ?></td>
	 </tr>
	 <tr>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="TitDetalle">&nbsp;</td>
	  <td class="CampoDetalle">&nbsp;</td>
	 </tr>
    </tbody>

    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
</script>
<?php 
	$est = isset($usuario["estareg"])?$usuario["estareg"]:1;
	include("../../admin/validaciones/estareg.php"); 
?>