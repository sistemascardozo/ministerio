<?php 	
	session_name("pnsu");
	if(!session_start()){session_start();}
	
	include("../../include/main.php");
	include("../../include/claseindex.php");
	
	$TituloVentana = "USUARIOS";
	$Activo = 1;
	$Op = isset($_GET['Op'])?$_GET['Op']:0;
	
	if($Op != 5)
	{
		CuerpoSuperior($TituloVentana);
	}
	
	$codsuc = $_SESSION['IdSucursal'];

	$FormatoGrilla = array();
	
	$Sql = "SELECT codusu, login, nombres, s.descripcion, e.descripcion, u.estareg ";
	$Sql .= "FROM seguridad.usuarios u ";
	$Sql .= " INNER JOIN public.estadoreg e ON (u.estareg = e.id) ";
	$Sql .= " INNER JOIN admin.sucursales s ON (s.codemp = u.codemp AND s.codsuc = u.codsuc)";
	
	$FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL
	$FormatoGrilla[1] = array('1'=>'codusu', '2'=>'nombres','3'=>'login','4'=>'s.descripcion');          //Campos por los cuales se hará la búsqueda
	$FormatoGrilla[2] = $Op;                                                            //Operacion
	$FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Login', 'T3'=>'Usuario', 'T4'=>'Sucursal', 'T5'=>'Estado');   //Títulos de la Cabecera
	$FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'left', 'A3'=>'left', 'A4'=>'left', 'A5'=>'center');                        //Alineación por Columna
	$FormatoGrilla[5] = array('W1'=>'60', 'W5'=>'90');                                 //Ancho de las Columnas
	$FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
	$FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
	$FormatoGrilla[8] = " AND u.estareg = 1 ORDER BY nombres  ASC ";                                   //Orden de la Consulta
	
	if($Op != 5)
	{
		$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'6', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'6', 
              'BtnCV3'=>'0');
	}
	else
	{
		$FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'1',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'ok.png',   //Imagen a mostrar
              'Btn1'=>'Seleccionar',       //Titulo del Botón
              'BtnF1'=>'onclick="Enviar(this);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1'    //Valor de comparación
              );
     // $FormatoGrilla[5] = array('W1'=>'60','W6'=>'80','W11'=>'20','W2'=>'70','W3'=>'70','W10'=>'90'); 
	}  
	         
	$FormatoGrilla[10] = array(array('Name' =>'id', 'Col'=>1), array('Name' =>'nombres', 'Col'=>3));//,DATOS ADICIONALES 
	
	$_SESSION['Formato'] = $FormatoGrilla;
	
	Cabecera('', $FormatoGrilla[7], 650, 450);
	
	Pie();
?>
<script type="text/javascript">
function Enviar(obj)
{
    var Id = $(obj).parent().parent().data('id');
    var Nombres = $(obj).parent().parent().data('nombres');
	
    opener.Recibir(Id, Nombres);
	
    window.close();
}
</script>
<?php
	if($Op != 5)
	{
		CuerpoInferior();
	}
?>
