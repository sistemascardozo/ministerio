<?php
session_name("pnsu");
if (!session_start()) {
    session_start();
}

include("include/main.php");

$idusuario = (isset($_SESSION['bk_id_user'])) ? $_SESSION['bk_id_user'] : $_SESSION['id_user'];

if (!isset($_SESSION["id_user"])) {
    session_destroy();
    if (substr_count("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], 'ogin.php') != 1) {
        header("Location: ".$urldir.'login.php');
    }
}

$SqlS = "SELECT sist.nombre_sistema, sist.nombre_corto, sist.img, sistUsu.codsistema, sist.carpeta ";
$SqlS .= "FROM seguridad.sistemas_usuarios AS sistUsu ";
$SqlS .= " INNER JOIN seguridad.sistemas AS sist ON(sistUsu.codsistema=sist.codsistema) ";
$SqlS .= "WHERE sistUsu.codusu = ? AND sist.estareg = 1 ORDER BY sist.orden";

$consulta = $conexion->prepare($SqlS);
$consulta->execute(array($idusuario));
$items = $consulta->fetchAll();

$IdProceso = 0;

CuerpoSuperior();
?>
<script>
    $("#DivTituloCobranza", window.parent.document).html('');

    function seleccionarSistemaX(codsistema, url)
    {
        $.ajax({
            url: '<?php echo $urldir; ?>admin/validaciones/seguimiento.php',
            type: 'POST',
            async: true,
            data: 'idsistema=' + codsistema + '&comentario=ACCEDIO AL SUB. SISTEMA',
            success: function (datos) {
                location.href = url + 'seleccionModulo.php?idsistema=' + datos;
            }
        })
    }
</script>
<div class="SisContenedor">
    
    <ul class="ch-grid">
    <?php
    $Count = 0;

    foreach ($items as $item) {
        $Count = $Count + 1;
        $CSS = '';

        if ($Count == 1) {
            $CSS = 'SisPanelInicio';
        }
        ?>
            
        <li>
            <a onclick="seleccionarSistemaX('<?php echo $item["codsistema"]; ?>', '<?php echo $item["carpeta"]; ?>');" >
                <div class="ch-item ch-img-<?=$item["codsistema"];?>">
                    <div class="ch-info">
                        <h3><?php echo $item["nombre_sistema"]; ?></h3>
                        <p><?php echo $item["nombre_corto"]; ?></p>
                    </div>
                </div>
            </a>
        </li>
        
        <?php
    }
    ?>
    </ul>
</div>
<script>
    escribir_mensaje("");
    $('#<?php echo $numeros[$Count]; ?>').addClass('SisPanelFin');
</script>
<?php
CuerpoInferior();
?>