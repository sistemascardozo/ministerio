<?php

	function TablaParametros($CodSuc, $NroInscripcion, $CodUnidadUso)
	{
		global $conexion;

		$Sql	= "SELECT UU.codunidad_uso, UU.descripcion, UU.porcentaje, UU.factor, UAP.codparametro, UAP.valor, UAP.factor ";
		$Sql	.= "FROM vma.unidades_uso UU INNER JOIN vma.usuarios_analisis UA ON (UU.codemp = UA.codemp) AND (UU.codsuc = UA.codsuc) ";
		$Sql	.= " AND (UU.nroinscripcion = UA.nroinscripcion) AND (UU.codunidad_uso = UA.codunidad_uso) ";
		$Sql	.= " INNER JOIN vma.usuarios_analisis_parametros UAP ON (UA.codemp = UAP.codemp) AND (UA.codsuc = UAP.codsuc) ";
		$Sql	.= " AND (UA.nroinscripcion = UAP.nroinscripcion) AND (UA.codunidad_uso = UAP.codunidad_uso) AND (UA.fecha = UAP.fecha) ";
		$Sql	.= " INNER JOIN vma.parametros P ON (UAP.codparametro = P.codparametro) ";
		$Sql	.= "WHERE UA.estado = 1 AND UA.codsuc = ".$CodSuc." AND UA.nroinscripcion = ".$NroInscripcion." ";
		if ($CodUnidadUso!=0)
		{
			$Sql	.= " AND UA.codunidad_uso = ".$CodUnidadUso." ";
		}
		$Sql	.= "ORDER BY UA.fecha DESC, UA.codunidad_uso, UAP.codparametro";
		
		$Consulta = $conexion->query($Sql);
		
		$Tabla = array();	//0:Unidad Uso, 1:Descripcion UU, 2:% UU, 3:Factor, 4:Nro Parametros, 4.1.0:CodParametro, 4.1.1:Valor
		
		$ConUU = 1;
		$UUso = 0;
		
		foreach ($Consulta->fetchAll() as $RowVMA)
		{
			if ($UUso!=$RowVMA[0])
			{
				$ConUU = 1;
				$UUso = $RowVMA[0];
			}
			
			$Tabla[$UUso][0] = $RowVMA[0];				//0:Unidad Uso
			$Tabla[$UUso][1] = $RowVMA[1];				//1:Descripcion UU
			$Tabla[$UUso][2] = $RowVMA[2];				//2:% UU
			$Tabla[$UUso][3] = $RowVMA[3];				//3:Factor
			$Tabla[$UUso][4][0] = $ConUU;				//4:Nro Parametros
			$Tabla[$UUso][4][$ConUU][0] = $RowVMA[4];	//4.1.0:CodParametro
			$Tabla[$UUso][4][$ConUU][1] = $RowVMA[5];	//4.1.1:Valor
			$Tabla[$UUso][4][$ConUU][2] = $RowVMA[6];	//4.1.2:Factor
			
			$ConUU = $ConUU + 1;
		}
		
		return $Tabla;
	}
?>