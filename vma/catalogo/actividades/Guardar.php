<?php 	
	session_name("siincoweb");
	if(!session_start()){session_start();}

	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codactividad = isset($_POST["codactividad"])?$_POST["codactividad"]:$_POST["1form1_id"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$CodCiiu		= $_POST["CodCiiu"];
	$estareg		= $_POST["estareg"];

	
	switch ($Op) 
	{
		case 0:
			$SqlN = "SELECT COUNT(*) + 1 FROM vma.actividades";
			$RowN = $conexion->query($SqlN)->fetch();
			
			$codactividad = $RowN[0];
			
			$sql = "INSERT INTO vma.actividades(codactividad, codciiu, descripcion, estareg) ";
			$sql .= "VALUES(?, ?) ";
			$result = $conexion->prepare($sql);
			$result->execute(array($codactividad, $CodCiiu, $descripcion, $estareg));
			
			break;
			
		case 1:
			$sql = "UPDATE vma.actividades SET codciiu=?, descripcion=?, estareg=? ";
			$sql .= "WHERE codactividad=?";
			$result = $conexion->prepare($sql);
			$result->execute(array($CodCiiu, $descripcion, $estareg, $codactividad));
			
			break;
			
		case 2:case 3:
			$sql = "UPDATE vma.actividades SET estareg=".$estareg." ";
			$sql .= "WHERE codactividad=".$codactividad;
			$result = $conexion->prepare($sql);
			$result->execute(array());
			
			break;
	}
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
		
	}
?>
