<?php 	
	session_name("siincoweb");
	if(!session_start()){session_start();}

	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codciiu		= isset($_POST["codciiu"])?$_POST["codciiu"]:$_POST["1form1_id"];
	$Seccion 		= $_POST["Seccion"];
	$Division 		= $_POST["Division"];
	$Grupo	 		= $_POST["Grupo"];
	$Clase	 		= $_POST["Clase"];
	$descripcion	= strtoupper($_POST["descripcion"]);
	$Revision		= 0;
	$estareg		= $_POST["estareg"];
	
	$ContP			= $_POST["NroParametros"];

	if ($Op < 2)
	{
		$SqlD = "DELETE FROM vma.parametros_ciiu WHERE codciiu='".$codciiu."'";
		$resultD = $conexion->prepare($SqlD);
		$resultD->execute(array());
	}
	
	switch ($Op) 
	{		
		case 0:
			$codciiu = $Division.$Grupo.$Clase;
			
			$sql = "INSERT INTO vma.ciiu(codciiu, seccion, division, grupo, clase, descripcion, revision, estareg) ";
			$sql .= "VALUES(?, ?, ?, ?, ?, ?, ?, ?) ";
			$result = $conexion->prepare($sql);
			$result->execute(array($codciiu, $Seccion, $Division, $Grupo, $Clase, $descripcion, $Revision, $estareg));
			
			for ($i = 1; $i <= $ContP; $i++) 
			{
				if ($_POST["Chk_".$i]==1)
				{
					$CodParametro = $_POST["Chk__".$i];
					
					$SqlD = "INSERT INTO vma.parametros_ciiu (codciiu, codparametro) ";
					$SqlD .= "VALUES(?, ?)";
					
					$resultD = $conexion->prepare($SqlD);
					$resultD->execute(array($codciiu, $CodParametro));
			
				}
			}
			
			break;
			
		case 1:
			$codciiuN = $Division.$Grupo.$Clase;
			
			$sql = "UPDATE vma.ciiu SET codciiu=?, seccion=?, division=?, grupo=?, clase=?, descripcion=?, revision=?, estareg=? ";
			$sql .= "WHERE codciiu=?";
			$result = $conexion->prepare($sql);
			$result->execute(array($codciiuN, $Seccion, $Division, $Grupo, $Clase, $descripcion, $Revision, $estareg, $codciiu));
			
			for ($i = 1; $i <= $ContP; $i++) 
			{
				if ($_POST["Chk_".$i]==1)
				{
					$CodParametro = $_POST["Chk__".$i];
					
					$SqlD = "INSERT INTO vma.parametros_ciiu (codciiu, codparametro) ";
					$SqlD .= "VALUES(?, ?)";
					
					$resultD = $conexion->prepare($SqlD);
					$resultD->execute(array($codciiuN, $CodParametro));
			
				}
			}
			
			break;
			
		case 2: case 3:
			$sql = "UPDATE vma.ciiu SET estareg = ".$estareg." ";
			$sql .= "WHERE codciiu = '".$codciiu."'";
			
			$result = $conexion->prepare($sql);
			$result->execute(array());
			
			break;
	}
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res = 2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res = 1;
	}
?>
