<?php 	
	session_name("siincoweb");
	if(!session_start()){session_start();}
	 
	include("../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'';
	$guardar	= "op=".$Op;
	$codsuc 	= $_SESSION['IdSucursal'];
	$Eps = 0;
	
	$objMantenimiento 	= new clsMantenimiento();

	if($Id!='')
	{
		$Select  = "select * from vma.ciiu where codciiu = '".$Id."'";
		$row=$conexion->query($Select)->fetch();
		$Estado = $row['estado'];
		$Eps = $row['eps'];
		
		$guardar	= $guardar."&Id2=".$Id;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	function ValidarForm(Op)
	{
		if ($("#Nombre").val() == '')
		{
			alert('La Descripcion de la Actividad no puede ser NULO');
			$("#Nombre").focus();
			return false;
		}
		$('#DivPar :input').each(function()
			 {
			 	
			 	if($(this).attr("checked"))
					$(this).val(1); 
				else
					{	$(this).attr("checked","checked");
						$(this).val(0); 
					}
			});
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
	function CambiaChk(Obj)
	{
		var Nombre = Obj.id.substr(3);
		//alert(Nombre);
		if (Obj.checked)
		{
			$('#Chk_' + Nombre).val(1);
		}
		else
		{
			$('#Chk_' + Nombre).val(0);
		}
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
    <table width="750" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
      <tbody>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
        <td class="TitDetalle">Id</td>
        <td width="10" align="center" class="TitDetalle">:</td>
        <td class="CampoDetalle">
		<input name="codciiu" type="text" id="Id" size="4" value="<? echo $Id; ?>" class="inputtext"/>
        </td>
	</tr>
	<tr>
	  <td class="TitDetalle">Sección</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
            <select name="Seccion" id="Seccion">
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
                <option value="E">E</option>
                <option value="F">F</option>
                <option value="G">G</option>
                <option value="H">H</option>
                <option value="I">I</option>
                <option value="J">J</option>
                <option value="K">K</option>
                <option value="L">L</option>
                <option value="M">M</option>
                <option value="N">N</option>
                <option value="O">O</option>
                <option value="P">P</option>
                <option value="Q">Q</option>
                <option value="R">R</option>
                <option value="S">S</option>
                <option value="T">T</option>
                <option value="U">U</option>
            </select>
        </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Div / Grup / Cla</td>
	  <td align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle"><input class="inputtext" name="Division" type="text" id="Division" size="2" maxlength="2" value="<?php echo $row["division"];?>"/>
	    <input class="inputtext" name="Grupo" type="text" id="Grupo" size="2" maxlength="2" value="<?php echo $row["grupo"];?>"/>
	    <input class="inputtext" name="Clase" type="text" id="Clase" size="2" maxlength="2" value="<?php echo $row["clase"];?>"/></td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Descripcion</td>
	  <td width="10" align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	    <input class="inputtext" name="descripcion" type="text" id="Nombre" size="100" maxlength="200" value="<?php echo $row["descripcion"];?>"/>	    </td>
	  </tr>
	<tr>
	  <td class="TitDetalle">Estado</td>
	  <td width="10" align="center" class="TitDetalle">:</td>
	  <td class="CampoDetalle">
	     <? include("../../../include/estareg.php"); ?></td>
	  </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle">&nbsp;</td>
	   </tr>
	 <tr>
	   <td colspan="3" align="center" class="TitDetalle">
       		<table width="500" border="0" cellspacing="0" cellpadding="0" id="ListaMenu2" >
                <tr style="color:#FFF">
                    <th width="70">Parámetros</th>
<?php 
				$SqlP = "SELECT * FROM vma.parametros ORDER BY codparametro ";
				$ItemsP = $conexion->query($SqlP)->fetchAll();
				
				foreach ($ItemsP as $ItemP)
				{	
?>
                    <th><?php echo $ItemP[1];?></th>
<?php
				}
?>
                </tr>
                <tr>
                    <td>Seleccione</td>
<?php
				$ContP = 0;
				foreach ($ItemsP as $ItemP)
				{
					$ContP = $ContP + 1;
					
					$SqlCP = "SELECT COUNT(*) FROM vma.parametros_ciiu WHERE codciiu='".$Id."' AND codparametro=".$ItemP[0]." ";
					$ItemCP = $conexion->query($SqlCP)->fetch();
					
					$Estado = '';
					$Estado2 = 0;
					if ($ItemCP[0]==1)
					{
						$Estado = 'checked="CHECKED"';
						$Estado2 = 1;
					}
?>
                    <td align="center"><input name="Chk<?php echo $ItemP[0];?>" type="checkbox" id="Chk<?php echo $ContP;?>" onchange="CambiaChk(this);" <?php echo $Estado;?> /><input name="Chk_<?php echo $ContP;?>" id="Chk_<?php echo $ContP;?>" type="hidden" value="<?php echo $Estado2;?>" /><input name="Chk__<?php echo $ContP;?>" id="Chk__<?php echo $ContP;?>" type="hidden" value="<?php echo $ItemP[0];?>" /></td>
<?php
				}
?>
                </tr>
	     	</table>
         </td>
	   </tr>
	 <tr>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="TitDetalle">&nbsp;</td>
	   <td class="CampoDetalle"><input type="hidden" name="NroParametros" id="NroParametros" value="<?php echo $ContP;?>" /></td>
	   </tr>
          </tbody>

    </table>
 </form>
</div>
<script>
 $("#Nombre").focus();
 
 $('#Seccion > option[value="<?php echo $row["seccion"];?>"]').attr('selected', 'selected');
</script>
<?
	$est = isset($row["estareg"])?$row["estareg"]:1;
	
	include("../../../admin/validaciones/estareg.php");
?>