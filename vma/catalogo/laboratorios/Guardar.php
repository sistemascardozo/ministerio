<?php 	
	session_name("siincoweb");
	if(!session_start()){session_start();}

	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$codlaboratorio = isset($_POST["codlaboratorio"])?$_POST["codlaboratorio"]:$_POST["1form1_id"];
	
	$descripcion = strtoupper($_POST["descripcion"]);
	
	$eps = $_POST["eps"];
	$estareg = $_POST["estareg"];

	
	switch ($Op) 
	{
		case 0:
			$SqlN = "SELECT COUNT(*) + 1 FROM vma.laboratorios";
			$RowN = $conexion->query($SqlN)->fetch();
			
			$codlaboratorio = $RowN[0];
			
			$sql = "INSERT INTO vma.laboratorios(codlaboratorio, descripcion, eps, estareg) ";
			$sql .= "VALUES(?, ?, ?) ";
			$result = $conexion->prepare($sql);
			$result->execute(array($codlaboratorio, $descripcion, $eps, $estareg));
			
			break;
			
		case 1:
			$sql = "UPDATE vma.laboratorios SET descripcion=?, eps=?, estareg=? ";
			$sql .= "WHERE codlaboratorio=?";
			$result = $conexion->prepare($sql);
			$result->execute(array($descripcion, $eps, $estareg, $codlaboratorio));
			
			break;
			
		case 2:case 3:
			$sql = "UPDATE vma.laboratorios SET estareg=".$estareg." ";
			$sql .= "WHERE codlaboratorio=".$codlaboratorio;
			$result = $conexion->prepare($sql);
			$result->execute(array());
			
			break;
	}
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
