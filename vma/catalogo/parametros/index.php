<?php
  if(!session_start()){session_start();}
  
  include("../../../include/main.php");
  include("../../../include/claseindex.php");
  
  $TituloVentana = "PARAMETROS";
  $Activo=1;
  
  $Criterio = 'VMAParametro';
  
  CuerpoSuperior($TituloVentana);
  
  $Op = isset($_GET['Op'])?$_GET['Op']:0;
  $codsuc = $_SESSION['IdSucursal'];
  
  $Sql = "SELECT vma.parametros.codparametro, vma.parametros.abreviatura, vma.parametros.descripcion, ";
  $Sql .= " vma.unidades.descripcion, vma.anexos.descripcion, vma.parametros.estareg ";
  $Sql .= "FROM vma.parametros ";
  $Sql .= "INNER JOIN vma.unidades ON (vma.parametros.codunidad = vma.unidades.codunidad) ";
  $Sql .= "INNER JOIN vma.anexos ON (vma.parametros.codanexo = vma.anexos.codanexo) ";
		   
  $FormatoGrilla = array ();	   
  $FormatoGrilla[0] = eregi_replace("[\n|\r|\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'vma'.$schema.'parametros.abreviatura', 
  							'2'=>'vma'.$schema.'parametros.descripcion', '3'=>'vma'.$schema.'unidades.descripcion', 
							'4'=>'vma'.$schema.'anexos.descripcion');          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'C&oacute;digo', 'T2'=>'Abreviatura', 'T3'=>'Descripcion', 'T4'=>'U. Med.', 
  							'T5'=>'Anexo', 'T6'=>'Estado');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A4'=>'center');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'60', 'W2'=>'80', 'W4'=>'50');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 900;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = "  ORDER BY vma.parametros.codparametro ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'3',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'6',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'6', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'6', 
              'BtnCV3'=>'0');
              
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  
  Cabecera('', $FormatoGrilla[7], 750, 320);
  Pie();
  CuerpoInferior();
?>
