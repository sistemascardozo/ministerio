<?php 	
	session_name("siincoweb");
	if(!session_start()){session_start();}

	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	
	$CodParametro	= isset($_POST["codparametro"])?$_POST["codparametro"]:$_POST["1form1_id"];
	$Abreviatura	= strtoupper($_POST["Abreviatura"]);
	$descripcion	= strtoupper($_POST["descripcion"]);
	$Unidad			= $_POST["Unidad"];
	$Anexo			= $_POST["Anexo"];
	$estareg		= $_POST["estareg"];

	
	switch ($Op) 
	{
		case 0:
			$SqlN = "SELECT COUNT(*) + 1 FROM vma.parametros";
			$RowN = $conexion->query($SqlN)->fetch();
			
			$CodParametro = $RowN[0];
			
			$sql = "INSERT INTO vma.parametros(codparametro, abreviatura, descripcion, codunidad, codanexo, estareg) ";
			$sql .= "VALUES(?, ?, ?, ?) ";
			$result = $conexion->prepare($sql);
			$result->execute(array($CodParametro, $Abreviatura, $descripcion, $Unidad, $Anexo, $estareg));
			
			break;
			
		case 1:
			$sql = "UPDATE vma.parametros SET abreviatura=?, descripcion=?, codunidad=?, codanexo=?, estareg=? ";
			$sql .= "WHERE codparametro=?";
			$result = $conexion->prepare($sql);
			$result->execute(array($Abreviatura, $descripcion, $Unidad, $Anexo, $estareg, $CodParametro));
			
			break;
		
		case 2:case 3:
			$sql = "UPDATE vma.parametros SET estareg=".$estareg." ";
			$sql .= "WHERE codparametro=".$CodParametro;
			
			$result = $conexion->prepare($sql);
			$result->execute(array());
			
			break;
	}
	
	if($result->errorCode()!='00000')
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}else{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
