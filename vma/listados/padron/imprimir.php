<?php 
	include("../../../objetos/clsReporte.php");
	
	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc;
			
			$periodo = $this->DecFechaLiteral();
			
			$this->SetFont('Arial', 'B', 10);
			$this->SetY(8);
			$h = 4;
			$this->Cell(0, $h + 2, "PADRON DE USUARIOS NO DOMESTICOS", 0, 1, 'C');
			//$this->SetFont('Arial', 'B', 8);
//			$this->Cell(0, $h + 2, $periodo["mes"]." - ".$periodo["anio"], 0, 1, 'C');
			
			$this->SetY(25);
			
			$this->SetFont('Arial', 'B', 6);
			$this->SetWidths(array(18, 16, 20, 50, 52, 50, 70));
			$this->SetAligns(array("C", "C", "C", "C"));
			$this->Row(array("COD. CATASTRAL",
							 "NRO. INSC.",
							 "REGISTRO",
							 "CLIENTE",
							 "DIRECCION",
							 "ACTIVIDAD",							 
							 "DOCUMENTO"));

		}
		function contenido($codsuc)
		{
			global $conexion, $schema;
			
			$h = 4;
			$s = 2;
			
			$SqlUND = "SELECT ".$this->getCodCatastral("C.").", C.nroinscripcion, C.propietario, (TCA.descripcioncorta || ' ' || CA.descripcion || ' # ' || C.nrocalle) AS Direccion, ";
			$SqlUND .= " U.codusuario, U.resolucion, A.descripcion ";
			$SqlUND .= "FROM catastro.clientes C INNER JOIN vma.usuarios U ON (C.nroinscripcion = U.nroinscripcion) ";
			$SqlUND .= " INNER JOIN public.calles CA ON (C.codemp = CA.codemp) ";
			$SqlUND .= " INNER JOIN vma.actividades A ON (U.codactividad = A.codactividad) ";			
			$SqlUND .= " AND (C.codsuc = CA.codsuc) AND (C.codzona = CA.codzona) AND (C.codcalle = CA.codcalle) ";
			$SqlUND .= " INNER JOIN public.tiposcalle TCA ON (CA.codtipocalle = TCA.codtipocalle)";
			$SqlUND .= "WHERE C.codsuc = ".$codsuc." ORDER BY U.codusuario";
			
			$count = 0;
			
			$consulta = $conexion->prepare($SqlUND);
			$consulta->execute(array());
			$items = $consulta->fetchAll();
	
			foreach($items as $row)
			{
				$count++;
				$codcatastro = $row["codcatastro"];
				$this->SetFont('Arial','',6);
				
				//$this->SetWidths(array(20,15,63,45,10,20,10,15,15,8,8,8,8,8,8,8,8));
				$this->SetAligns(array("C", "C", "C", "L", "L", "L"));
				$this->Row(array($codcatastro,
							     $this->CodUsuario($codsuc, $row["nroinscripcion"]),
								 strtoupper($row["codusuario"]),
							 	 trim(utf8_decode(strtoupper($row["propietario"]))),
							     trim(utf8_decode(strtoupper($row["direccion"]))),
							     trim(utf8_decode(strtoupper($row["descripcion"]))),								 
								 trim(utf8_decode(strtoupper($row["resolucion"])))));
				
				
			}
			
			$this->Ln(3);
			$this->Cell(0, $h + 2,"Usuario Registrados ".$count, 'TB', 1, 'C');
			
		}
	}
	
	$codsuc         = $_GET["codsuc"];
	
	$objReporte = new clsPadron("L");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsuc);
	$objReporte->Output();
	
?>