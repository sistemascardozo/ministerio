<?php 
	include("../../../objetos/clsReporte.php");
	
	class clsPadron extends clsReporte
	{
		function cabecera()
		{
			global $codsuc;
			
			$periodo = $this->DecFechaLiteral();
			
			$this->SetFont('Arial', 'B', 10);
			$this->SetY(8);
			$h = 4;
			$this->Cell(0, $h + 2, "PADRON DE USUARIOS NO DOMESTICOS", 0, 1, 'C');
			$this->SetFont('Arial', 'B', 8);
			$this->Cell(0, $h + 2, "VENCIDOS O POR VENCER", 0, 1, 'C');
			
			$this->SetY(25);
			
			$this->SetFont('Arial', 'B', 6);
			$this->SetWidths(array(20, 15, 15, 50, 45, 15, 30));
			$this->SetAligns(array("C", "C", "C", "C", "C", "C", "C"));
			$this->Row(array("COD. CATASTRAL",
							 "NRO. INSC.",
							 "CODIGO",
							 "CLIENTE",
							 "DIRECCION",
							 "FEC. ALTA",
							 "DOCUMENTO"));

		}
		function contenido($codsuc)
		{
			global $conexion, $schema;
			
			$h = 4;
			$s = 2;
			
			$Criterio = " AND (CASE WHEN U.declaracion=1 AND (to_number(to_char(U.fecha_alta, 'YYYY'), '9999') * 365 + to_number(to_char(U.fecha_alta, 'MM'), '99') * 30 + to_number(to_char(U.fecha_alta, 'DD'), '99') + 320) < ";
			$Criterio .= " (to_number(to_char(CURRENT_DATE, 'YYYY'), '9999') * 365 + to_number(to_char(CURRENT_DATE, 'MM'), '99') * 30 + to_number(to_char(CURRENT_DATE, 'DD'), '99')) THEN 1 ELSE 0 END) = 1 ";
			
			$SqlUND = "SELECT ".$this->getCodCatastral("C.").", C.nroinscripcion, C.propietario, (TCA.descripcioncorta || ' ' || CA.descripcion || ' # ' || C.nrocalle) AS Direccion, ";
			$SqlUND .= " U.fecha_alta, U.codusuario, U.resolucion ";
			$SqlUND .= "FROM catastro.clientes C INNER JOIN vma.usuarios U ON (C.nroinscripcion = U.nroinscripcion) ";
			$SqlUND .= " INNER JOIN public.calles CA ON (C.codemp = CA.codemp) ";
			$SqlUND .= " AND (C.codsuc = CA.codsuc) AND (C.codzona = CA.codzona) AND (C.codcalle = CA.codcalle) ";
			$SqlUND .= " INNER JOIN public.tiposcalle TCA ON (CA.codtipocalle = TCA.codtipocalle)";
			$SqlUND .= "WHERE C.codsuc = ".$codsuc;
			$SqlUND .= $Criterio;
			
			$count = 0;
			
			$consulta = $conexion->prepare($SqlUND);
			$consulta->execute(array());
			$items = $consulta->fetchAll();
	
			foreach($items as $row)
			{
				$count++;
				$codcatastro = $row["codcatastro"];
				$this->SetFont('Arial','',6);
				
				//$this->SetWidths(array(20,15,63,45,10,20,10,15,15,8,8,8,8,8,8,8,8));
				$this->SetAligns(array("C", "C", "C", "L", "L", "C", "L"));
				$this->Row(array($codcatastro, 
							     $this->CodUsuario($codsuc, $row["nroinscripcion"]),
								 strtoupper($row["codusuario"]),
							 	 trim(utf8_decode(strtoupper($row["propietario"]))),
							     strtoupper($row["direccion"]),
								 $this->DecFecha($row["fecha_alta"]),
								 strtoupper($row["resolucion"])));
				
				
			}
			
			$this->Ln(3);
			$this->Cell(0, $h + 2,"Usuario Registrados ".$count, 'TB', 1, 'C');
			
		}
	}
	
	$codsuc         = $_GET["codsuc"];
	
	$objReporte = new clsPadron("P");
	$objReporte->AliasNbPages();
	$objReporte->AddPage();
	$objReporte->contenido($codsuc);
	$objReporte->Output();
	
?>