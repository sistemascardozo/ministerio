<?php
	if(!session_start()){session_start();} 
	
	include($_SERVER['DOCUMENT_ROOT']."/siinco_sedahuanuco/".$Sede."objetos/clsFunciones.php");

	$objFunciones 	= new clsFunciones();
	
	$Op 		= $_POST['Op'];
	$codsuc		= $_POST['codsuc'];
	$NroInscripcion = $_POST['NroInscripcion'];
	$UnidadUso	= $_POST['UnidadUso'];
	$Fecha 		= isset($_POST['Fecha'])?$_POST['Fecha']:'';
	$Descripcion = $_POST['Descripcion'];
	$Porcentaje = $_POST['Porcentaje'];
	
	if ($Fecha=='')
	{
		$Fecha = date('d/m/Y');
	}
	
	$SqlUU	= "SELECT descripcion, porcentaje FROM vma.unidades_uso ";
	$SqlUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." AND codunidad_uso = ".$UnidadUso;
	$RowUU	= $conexion->query($SqlUU)->fetch();
	
	if ($Op!=0)
	{
		$Descripcion 	= $RowUU[0];
		$Porcentaje 	= $RowUU[1];
	}
	$Origen	= 1;
	$Laboratorio = 1;
	
	$SqlUUA	= "SELECT * FROM vma.usuarios_analisis ";
	$SqlUUA	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." AND codunidad_uso = ".$UnidadUso." AND fecha = '".$Fecha."'";
	$RowUUA	= $conexion->query($SqlUUA)->fetch();
	
	if (count($conexion->query($SqlUUA)->fetchAll())>0)
	{
		$Origen	= $RowUUA[5];
		$Laboratorio = $RowUUA[6];
	}
	
?>
<script>
	$(document).ready(function() {
		$( "#tabsA" ).tabs();
		
		$("#FechaA").datepicker({  
			//showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: urlDir + 'images/btn_calendar.png',
			buttonImageOnly: true,
			//showOn: 'both', //mostrar en el input
			showButtonPanel: true

		});
	});
	
	function VerificaR(Parametro)
	{
		var Valor = parseFloat($('#Parametro' + Parametro).val());
		var Porcentaje = 0;

		$.ajax({
			 url:'../../include/Rangos.php',
			 type:'POST',
			 async:true,
			 data:'CodParametro=' + Parametro + '&Valor=' + Valor,
			 success:function(datos){
				data = datos.split('|');

				$('#TdVMA' + Parametro).html(data[0]);
				$('#Porcentaje' + Parametro).val(data[1])
				
				for (var i=1; i<=4; i++)
				{
					ValorP = $('#Porcentaje' + i).val();
					if (ValorP=='')
					{
						ValorP = 0;
					}
					Porcentaje =  parseFloat(Porcentaje) + parseFloat(ValorP);
					//alert(Porcentaje);
				}
				$('#TdVMAP').html(Porcentaje + ' %');
			 }
		})
	}
	
	function VerificaR2(Parametro)
	{
		var Valor = parseFloat($('#Parametro' + Parametro).val());
		var Desde = parseFloat($('#ParametroD' + Parametro).val());
		var Hasta = parseFloat($('#ParametroH' + Parametro).val());
		
		$('#TrParametro' + Parametro).css('background-color', '#FFF');
		
		if (Valor > Hasta)
		{
			$('#TrParametro' + Parametro).css('background-color', '#F00');
		}
	}
</script>
<div align="center">
<form id="formA" name="formA" method="post" action="" enctype="multipart/form-data">
	<table width="640" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tr>
			<td class="TitDetalle" width="80">Fecha</td>
			<td width="20" align="center">:</td>
			<td>
            	<input type="hidden" name="NroInscripcionA" id="NroInscripcionA" value="<?php echo $NroInscripcion;?>">
                <input type="hidden" name="FechaAA" id="FechaAA" value="<?php echo $Fecha;?>">
                <input type="text" name="FechaA" id="FechaA" style="width:80px" value="<?php echo $Fecha;?>" readonly="readonly" >
			</td>
        </tr>
		<tr>
		  <td class="TitDetalle">Und. Uso</td>
		  <td align="center">:</td>
		  <td>
          	<input type="hidden" name="IdUnidadUsoA" id="IdUnidadUsoA" value="<?php echo $UnidadUso;?>">
          	<input type="hidden" name="PorcentajeUsoA" id="PorcentajeUsoA" value="<?php echo $Porcentaje;?>">
	      	<input type="text" name="UnidadUsoA" id="UnidadUsoA" style="width:300px" readonly value="<?php echo $Descripcion;?>"></td>
	  </tr>
		<tr>
		  <td class="TitDetalle">Origen</td>
		  <td align="center">:</td>
		  <td><span class="CampoDetalle">
		    <select name="CodOrigenA" id="CodOrigenA">
<?php
                $SqlO = "SELECT * FROM vma.origen ORDER BY codorigen";
                $ConsultaO = $conexion->query($SqlO);
                
                foreach ($ConsultaO->fetchAll() as $RowO)
                {
?>
		      <option value="<?php echo $RowO[0];?>" <?php if ($Origen==$RowO[0]){echo 'selected="selected"';}?>><?php echo $RowO[1];?></option>
<?php
                }
?>
	      </select>
		  </span></td>
	  </tr>
		<tr>
		  <td class="TitDetalle">Laboratorio</td>
		  <td align="center">:</td>
		  <td>
		    <select name="CodLaboratorioA" id="CodLaboratorioA">
<?php
                $SqlL = "SELECT * FROM vma.laboratorios ORDER BY codlaboratorio";
                $ConsultaL = $conexion->query($SqlL);
                
                foreach ($ConsultaL->fetchAll() as $RowL)
                {
?>
		      <option value="<?php echo $RowL[0];?>" <?php if ($Laboratorio==$RowL[0]){echo 'selected="selected"';}?>><?php echo $RowL[1];?></option>
<?php
                }
?>
	      </select>
			</td>
		</tr>
		<tr>
			<td colspan="5">
                <div id="tabsA">
                    <ul>
                        <li style="font-size:10px"><a href="#tabs-1">ANEXO 1</a></li>
                        <li style="font-size:10px"><a href="#tabs-2">ANEXO 2</a></li>
                    </ul>
                    <div id="tabs-1" style="height:200px">
                    	<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
							<tr style="color:#FFF;" bgcolor="#006699">
								<th width="50" align="center" class="TitDetalle">Abrev.</th>
								<th width="80" align="center">VMA</th>
								<th width="80" align="center">Valor</th>
                                <th width="100" align="center" class="TitDetalle">Rango - %</th>
								<th align="center">Descripción</th>
					  	  	</tr>
<?php
			$SqlP	= "SELECT DISTINCT P.codparametro, P.abreviatura, P.descripcion, RP.vma ";
			$SqlP	.= "FROM vma.parametros AS P INNER JOIN vma.rangos_parametros AS RP ON (P.codparametro = RP.codparametro) ";
			$SqlP	.= "WHERE P.codanexo=1 ";
			$SqlP	.= "ORDER BY P.codparametro ";
			
			$consultaP = $conexion->prepare($SqlP);
			$consultaP->execute(array());
			$itemsP = $consultaP->fetchAll();
			
			$cont_Parametros = 0;
			$Porc = 0;
			foreach($itemsP as $itemP)
			{					
				$ValorP = 0;
				$FactorP = 0; 
				
				$SqlPV	= "SELECT valor, factor FROM vma.usuarios_analisis_parametros ";
				$SqlPV	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." ";
				$SqlPV	.= "AND codunidad_uso = ".$UnidadUso." AND fecha = '".$Fecha."' AND codparametro = ".$itemP[0];
				
				if (count($conexion->query($SqlPV)->fetchAll())>0)
				{
					$RowPP	= $conexion->query($SqlPV)->fetch();
					$ValorP = $RowPP[0];
					$FactorP = $RowPP[1];
				}
				
				$cont_Parametros++;
				$SqlRP	= "SELECT 'R ' || RP.codrango || ' - ' || RP.porcentaje || '%', RP.porcentaje ";
				$SqlRP	.= "FROM vma.rangos_parametros RP INNER JOIN vma.parametros P ON (RP.codparametro = P.codparametro) ";
				$SqlRP	.= "WHERE P.codanexo = 1 AND ".$ValorP." BETWEEN RP.inicio AND RP.final AND RP.codparametro = ".$itemP[0]." ";
				$SqlRP	.= "ORDER BY RP.codrango, P.codparametro ";
				
				$Consulta = $conexion->query($SqlRP);
				$RowRP = $Consulta->fetch();
				
				$Porcent = 0;
				if (count($conexion->query($SqlRP))>0)
				{
					$Porcent = $RowRP[1];
					$Porc = $Porc + $Porcent;
					
				}
?>
							<tr id="TrParametro<?php echo $itemP[0];?>">
								<td align="center" bgcolor="#006699" style="color:#FFF;"><?php echo $itemP[1];?></td>
								<td align="center" bgcolor="#CCCCCC" ><?php echo $itemP[3];?></td>
								<td align="center">
                                	<input type="hidden" name="Porcentaje<?php echo $itemP[0];?>" id="Porcentaje<?php echo $itemP[0];?>" value="<?php echo $Porcent;?>">
									<input type="hidden" name="CodParametro<?php echo $itemP[0];?>" id="CodParametro<?php echo $itemP[0];?>" value="<?php echo $itemP[0];?>">
									<input type="text" name="Parametro<?php echo $itemP[0];?>" id="Parametro<?php echo $itemP[0];?>" style="width:80px; text-align:center" value="<?php echo $ValorP;?>" onchange="VerificaR(<?php echo $itemP[0];?>);" >
								</td>
                                <td align="center" bgcolor="#CCCCCC" id="TdVMA<?php echo $itemP[0];?>">
									<?php echo $RowRP[0];?>
                              	</td>
								<td style="padding-left:5px;"><?php echo $itemP[2];?></td>
							</tr>
<?php
			}
?>
							<tr style="color:#FFF;" bgcolor="#006699">
								<th colspan="3" align="center" class="TitDetalle">Total</th>
								<th width="100" align="center" id="TdVMAP"><?=$Porc?> %</th>
								<th align="center">&nbsp;</th>
					  	  	</tr>
						</table>
                    </div>
                    <div id="tabs-2" style="overflow:auto;" >
                    	<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
                        	<tr style="color:#FFF;" bgcolor="#006699">
								<th width="50" align="center" class="TitDetalle">Abrev.</th>
								<th width="80" align="center" class="TitDetalle">VMA</th>
								<th width="80" align="center">Valor</th>
								<th align="center">Descripción</th>
					  	  	</tr>
<?php
			$SqlP2	= "SELECT P.codparametro, P.abreviatura, P.descripcion, RP.inicio, RP.final ";
			$SqlP2	.= "FROM vma.rangos_parametros AS RP INNER JOIN vma.parametros AS P ON (RP.codparametro = P.codparametro) ";
			$SqlP2	.= "WHERE P.codanexo=2 ";
			$SqlP2	.= "ORDER BY P.codparametro ";
			
			$consultaP2 = $conexion->prepare($SqlP2);
			$consultaP2->execute(array());
			$itemsP2 = $consultaP2->fetchAll();
			
			$cont_Parametros2 = 0;
			foreach($itemsP2 as $itemP2)
			{					
				$cont_Parametros2++;
?>
							<tr id="TrParametro<?php echo $itemP2[0];?>">
								<td align="center" bgcolor="#006699" style="color:#FFF;"><?php echo $itemP2[1];?></td>
								<td align="center" bgcolor="#CCCCCC">
                                	<input type="hidden" name="ParametroD<?php echo $itemP2[0];?>" id="ParametroD<?php echo $itemP2[0];?>" value="<?php echo $itemP2[3];?>">
									<input type="hidden" name="ParametroH<?php echo $itemP2[0];?>" id="ParametroH<?php echo $itemP2[0];?>" value="<?php echo $itemP2[4];?>">
									<?php echo $itemP2[4];?>
                             	</td>
								<td align="center">
									<input type="hidden" name="CodParametro<?php echo $itemP2[0];?>" id="CodParametro<?php echo $itemP2[0];?>" value="<?php echo $itemP2[0];?>">
<?php
				$ValorP2 = 0;
				$FactorP = 0; 
				
				$SqlPV2	= "SELECT valor, factor FROM vma.usuarios_analisis_parametros ";
				$SqlPV2	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." ";
				$SqlPV2	.= "AND codunidad_uso = ".$UnidadUso." AND fecha = '".$Fecha."' AND codparametro = ".$itemP2[0];
				
				if (count($conexion->query($SqlPV2)->fetchAll())>0)
				{
					$RowPP2	= $conexion->query($SqlPV2)->fetch();
					$ValorP2 = $RowPP2[0];
					$FactorP2 = $RowPP2[1];
				}
				
?>
									<input type="text" name="Parametro<?php echo $itemP2[0];?>" id="Parametro<?php echo $itemP2[0];?>" style="width:80px; text-align:center" value="<?php echo $ValorP2;?>" onchange="VerificaR2(<?php echo $itemP2[0];?>);" >
								</td>
								<td style="padding-left:5px;"><?php echo $itemP2[2];?></td>
							</tr>
<?php
			}
?>
						</table>
                    </div>
				</div>
			</td>
		</tr>
	</table>
</form>