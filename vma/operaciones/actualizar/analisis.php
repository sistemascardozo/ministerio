<?php
	if(!session_start()){session_start();} 
	
	include($_SERVER['DOCUMENT_ROOT']."/siinco_sedahuanuco/".$Sede."objetos/clsFunciones.php");

	$objFunciones 	= new clsFunciones();
	
	$codsuc			= $_SESSION['IdSucursal'];
	$NroInscripcion	= $_POST['NroInscripcion'];
	$CodUnidadUso	= $_POST['CodUnidadUso'];
	
?>
<div style="overflow:auto; height: 150px">
<input name="cont_analisis" id="cont_analisis" type="hidden" value="0" />
<table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tbanalisis" >
    <thead class="ui-widget-header" style="font-size: 11px">
        <tr>
          <th width="80">Fecha</th>
            <th >Origen</th>
            <th width="40">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
<?php
	$cont_analisis 	= 0;
	
	if($NroInscripcion!="")
	{
		$sql_analisis = "SELECT A.codsuc, A.nroinscripcion, A.codunidad_uso, A.fecha, O.descripcion ";
		$sql_analisis .= "FROM vma.origen AS O INNER JOIN vma.usuarios_analisis AS A ON (O.codorigen = A.codorigen)";
		$sql_analisis .= "WHERE A.codsuc=".$codsuc." AND A.nroinscripcion=".$NroInscripcion." AND A.codunidad_uso=".$CodUnidadUso." ";
		$sql_analisis .= "ORDER BY A.fecha DESC";
		
		$consulta_analisis = $conexion->prepare($sql_analisis);
		$consulta_analisis->execute(array());
		$items_anlalisis = $consulta_analisis->fetchAll();
		
		foreach($items_anlalisis as $rowAnalisis)
		{					
			$cont_analisis++;
						
			$Fecha = $objFunciones->DecFecha($rowAnalisis["fecha"]);
?>
        <tr style='background-color:#FFFFFF; padding:4px; cursor:pointer' title="<?=$rowAnalisis["factor"]?>" >
          <td align="center" >
              <input type='hidden' id='CodUnidad<?=$cont_analisis?>' name='CodUnidad<?=$cont_analisis?>' value='<?=$rowAnalisis["codunidad_uso"]?>' />
			  <input type='hidden' id='NroInscripcion<?=$cont_analisis?>' name='NroInscripcion<?=$cont_analisis?>' value='<?=$rowAnalisis["nroinscripcion"]?>' />
			  <?=$Fecha?>
            </td>
            <td >
                <?=strtoupper($rowAnalisis["descripcion"])?>
            </td>
            <td align="center" >
            	<img src='../../../images/iconos/modificar.png' width='16' height='16' title='Modificar Analisis' onclick='ModificaAnalisis(1, <?=$rowAnalisis["codunidad_uso"]?>, "<?=$Fecha?>");' style='cursor:pointer'/>&nbsp;
                <img src='../../../images/iconos/eliminar.png' width='16' height='16' title='Borrar Analisis' onclick='QuitaFilaD(this, 1, <?=$cont_analisis?>);' style='cursor:pointer'/>
            </td>
        </tr>
<?php
		}
	}
?>
		</tbody>
<script>
	cont_analisis		= <?=$cont_analisis?>;
	cont_fila_analisis 	= <?=$cont_analisis?>;
		
	$("#cont_analisis").val(<?=$cont_analisis?>)
</script>
    </table>
</div>