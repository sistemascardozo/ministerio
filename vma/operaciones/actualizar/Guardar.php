<?php 	
	session_name("siincoweb");
	if(!session_start()){session_start();}

	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codsuc 	= $_SESSION['IdSucursal'];
	
	$NroInscripcion	= intval(substr(isset($_POST["NroInscripcion"])?$_POST["NroInscripcion"]:$_POST['1form1_id'], 2));
	$CodActividad	= strtoupper($_POST["CodActividad"]);
	$CodCiiu		= $_POST["CodCiiu"];
	$Aplicable		= $_POST["Aplicable"];
	$DJurada		= $_POST["DJurada"];
	$Analisis		= $_POST["Analisis"];
	
	$Resolucion		= $_POST["Resolucion"];
	
	$ContUnidUso	= $_POST["cont_unidades"];
	
	$estareg		= isset($_POST["estareg"])?$_POST["estareg"]:1;
	
	$FechaAlta		= $_POST["FechaAlta"];
	$FechaBaja		= $_POST["FechaBaja"];
	
	$CodUsuario		= $_POST["CodUsuario"];
	
	
	
	$Error = 0;
	switch ($Op) 
	{
		case 0:
			
			$Sql = "INSERT INTO vma.usuarios(codemp, codsuc, nroinscripcion, codusuario, codciiu, codactividad, fecha_alta, ";
			$Sql .= " aplica, declaracion, analisis, resolucion, estareg) ";
			$Sql .= "VALUES(1, ".$codsuc.", ".$NroInscripcion.", '".$CodUsuario."', '".$CodCiiu."', ".$CodActividad.", '".$FechaAlta."', ";
			$Sql .= " ".$Aplicable.", ".$DJurada.", ".$Analisis.", '".$Resolucion."', 1) ";
			$result = $conexion->prepare($Sql);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				echo "Error-> <br>";
				echo $Sql."<br>";
			}

			break;
			
		case 1:
			$Sql = "UPDATE vma.usuarios SET codusuario = '".$CodUsuario."', codciiu='".$CodCiiu."', codactividad=".$CodActividad.", ";
			$Sql .= " fecha_alta='".$FechaAlta."', aplica=".$Aplicable.", declaracion=".$DJurada.", analisis=".$Analisis.", ";
			$Sql .= " resolucion='".$Resolucion."' ";
			$Sql .= "WHERE codsuc=".$codsuc." AND nroinscripcion=".$NroInscripcion." ";
			$result = $conexion->prepare($Sql);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				echo "Error-> <br>";
				echo $Sql."<br>";
			}
			
			break;
			
		case 2:case 3:
			$Sql = "UPDATE vma.usuarios SET estareg=".$estareg." ";
			$Sql .= "WHERE codsuc=".$codsuc." AND nroinscripcion=".$NroInscripcion;
			$result = $conexion->prepare($Sql);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				echo "Error-> <br>";
				echo $Sql."<br>";
			}
			
			break;
	}
	
	for ($i=0; $i<=$ContUnidUso; $i++)
	{
		if ($_POST['CodUnidad'.$i])
		{
			$SqlUU	= "SELECT COUNT(*) FROM vma.unidades_uso ";
			$SqlUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." ";
			$SqlUU	.= " AND codunidad_uso = ".$_POST['CodUnidad'.$i]." ";
			
			$RowUU	= $conexion->query($SqlUU)->fetch();
			
			if ($RowUU[0]==0)
			{
				$Sql = "INSERT INTO vma.unidades_uso ";
				$Sql .= "(codemp, codsuc, nroinscripcion, codunidad_uso, porcentaje, descripcion) ";
				$Sql .= "VALUES(1, ".$codsuc.", ".$NroInscripcion.", ".$i.", ".$_POST['porcentaje'.$i].", '".$_POST['Descripcion'.$i]."') ";
				$result = $conexion->prepare($Sql);
				$result->execute(array());
				
				if($result->errorCode()!='00000')
				{
					$Error = 1;
					echo "Error-> <br>";
					echo $Sql."<br>";
				}
			}
			else
			{
				$Sql = "UPDATE vma.unidades_uso SET porcentaje=? ";
				$Sql .= "WHERE codsuc=? AND nroinscripcion=? AND codunidad_uso=? ";
				$result = $conexion->prepare($Sql);
				$result->execute(array($_POST['porcentaje'.$i], $codsuc, $NroInscripcion, $i));
				
				if($result->errorCode()!='00000')
				{
					$Error = 1;
					echo "Error-> <br>";
					echo $Sql."<br>";
				}
			}
		}
		else
		{
			$Sql = "DELETE FROM vma.unidades_uso ";
			$Sql .= "WHERE codsuc=? AND nroinscripcion=? AND codunidad_uso=? ";
			$result = $conexion->prepare($Sql);
			$result->execute(array($codsuc, $NroInscripcion, $i));
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				echo "Error-> <br>";
				echo $Sql."<br>";
			}
		}
	}
	
	if($Error==1)
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}
	else
	{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>
