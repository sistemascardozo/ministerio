<?php
	if(!session_start()){session_start();}
	//$CodSuc = 1;
	$CodSuc 	= $_SESSION['IdSucursal'];
	$NroInscripcion = $_POST['NroInscripcion'];
	$CodUnidadUso 	= $_POST['CodUnidadUso'];
	
	include('../../../config.php');
	include('../../include/funciones.php');
	
	$Tabla1 = TablaParametros($CodSuc, $NroInscripcion, $CodUnidadUso);
	
	//print_r($Tabla1);
	//print_r($Tabla1[$CodUnidadUso][4][1][1]);
	
	$ConUU = count($Tabla1);
?>
<style>
	#TbVMATabla1 {
		font-size:12px;
	}
	#TbVMATabla1 td {
		padding:5px;
	}
</style>
<table width="630" border="1" cellspacing="0" cellpadding="0" id="TbVMATabla1">
	<tr style="font-weight:bold;color:#FFF;" bgcolor="#006699">
		<td height="40" align="center">Parametros de Aguas Residuales</td>
		<td width="70" align="center">DBO5</td>
		<td width="70" align="center">DQO</td>
		<td width="70" align="center">SST</td>
		<td width="70" align="center">AyG</td>
		<td width="60" rowspan="2" align="center">Factor Total</td>
	</tr>
    <tr style="font-weight:bold;">
		<td height="20">VMA (mg/L)</td>
		<td align="center">500</td>
		<td align="center">1,000</td>
		<td align="center">500</td>
		<td align="center">100</td>
	</tr>
<?php
	
	for ($i=0; $i < $ConUU; $i++)
	{
?>
	<tr>
	  <td>Valor de Análisis (<? echo $Tabla1[$CodUnidadUso][1];?>)</td>
	  <td align="center"><? echo number_format($Tabla1[$CodUnidadUso][4][1][1], 1);?></td>
	  <td align="center"><? echo number_format($Tabla1[$CodUnidadUso][4][2][1], 1);?></td>
	  <td align="center"><? echo number_format($Tabla1[$CodUnidadUso][4][3][1], 1);?></td>
	  <td align="center"><? echo number_format($Tabla1[$CodUnidadUso][4][4][1], 1);?></td>
	  <td rowspan="2" align="center" valign="bottom"><? echo number_format($Tabla1[$CodUnidadUso][3], 2);?>&nbsp;%</td>
  </tr>
	<tr>
	  <td>Factores Individuales</td>
	  <td align="center"><? echo number_format($Tabla1[$CodUnidadUso][4][1][2], 2);?>&nbsp;%</td>
	  <td align="center"><? echo number_format($Tabla1[$CodUnidadUso][4][2][2], 2);?>&nbsp;%</td>
	  <td align="center"><? echo number_format($Tabla1[$CodUnidadUso][4][3][2], 2);?>&nbsp;%</td>
	  <td align="center"><? echo number_format($Tabla1[$CodUnidadUso][4][4][2], 2);?>&nbsp;%</td>
  </tr>
<?php
	}
?>
</table>
