<?php
	if(!session_start()){session_start();} 
	
	include($_SERVER['DOCUMENT_ROOT']."/siinco_sedahuanuco/".$Sede."objetos/clsFunciones.php");

	$objFunciones 	= new clsFunciones();
	
	$Op 		= $_POST['Op'];
	$codsuc		= $_POST['codsuc'];
	$NroInscripcion = $_POST['NroInscripcion'];
	$Id			= $_POST['Id'];
	$Fecha 		= isset($_POST['Fecha'])?$_POST['Fecha']:'';
	$Item		= $_POST['Item'];
	
	if ($Fecha=='')
	{
		$Fecha = date('d/m/Y');
	}

	$Ocurrencia	= 0;

	$SqlUUA	= "SELECT * FROM vma.usuarios_ocurrencia ";
	$SqlUUA	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." AND item = ".$Item." AND fecha = '".$Fecha."'";
	$RowUUA	= $conexion->query($SqlUUA)->fetch();
	
	if (count($conexion->query($SqlUUA)->fetchAll())>0)
	{
		$Item 		= $RowUUA[3];
		$Ocurrencia	= $RowUUA[4];
	}
	else
	{
		//Verificamos Item
		$SqlUU	= "SELECT COUNT(*) + 1 FROM vma.usuarios_ocurrencia ";
		$SqlUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." AND fecha = '".$Fecha."'";
		
		$RowUU	= $conexion->query($SqlUU)->fetch();
		
		$Item		= $RowUU[0];
	}
?>
<script type="text/javascript" src="../../../js/ajaxupload.3.6.js"></script>
<script>
	var Fecha = '<?=$Fecha?>';
	var Item = <?=$Item?>;
	var Nombre = '<?=$Id?>';
	$(document).ready(function() {
		$("#FechaA").datepicker({  
			//showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: urlDir + 'images/btn_calendar.png',
			buttonImageOnly: true,
			//showOn: 'both', //mostrar en el input
			showButtonPanel: true,
			onSelect: function (date) {
				VerifcaFecha();
			},
		});
		
		var button = $('#upload_button'), interval;
		new AjaxUpload('#upload_button', {
			action: 'upload.php?NroInscripcion=' + Nombre + '&Fecha=' + Fecha + '&Item=' + Item ,
			onSubmit : function(file , ext){
				if (! (ext && /^(pdf|PDF)$/.test(ext))){
					// extensiones permitidas
					alert('Error: Solo se permiten PDF');
					// cancela upload
					return false;
				} 
				else 
				{
					button.text('Cargando');
					this.disable();
				}
			},
			onComplete: function(file, response){
				button.text('Seleccionar Imagen');
				// enable upload button
				this.enable();
				//document.getElementById('Foto').src = "imagenes/" + Nombre + ".jpg?d=" + new Date().getTime();
			}	
		});
	});
	
	function VerifcaFecha()
	{
		$.ajax({
			 url:'../../include/Ocurrencias.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<?=$codsuc?>&NroInscripcion=<?=$NroInscripcion?>&Fecha=' + $('#FechaA').val(),
			 success:function(datos){
				data = datos.split('|');
				//alert(data);
				if (data[0]!=$('#ItemAA').val())
				{
					$('#ItemA').val(data[0]);
				}
				
				Item = $('#ItemA').val();
				Fecha = $('#FechaA').val();
				if (Item.length==1)
				{
					Item = '0' + Item;
				}
				Nombre = '<?=$Id?>' + Fecha.substr(6, 4) + Fecha.substr(3, 2) + Fecha.substr(0, 2) + Item;
				//alert(Nombre);
			 }
		})
	}

</script>
<div align="center">
<form id="formO" name="formO" method="post" action="" enctype="multipart/form-data">
	<table width="640" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tr>
			<td class="TitDetalle" width="80">Fecha</td>
			<td width="20" align="center">:</td>
			<td>
            	<input type="hidden" name="NroInscripcionA" id="NroInscripcionA" value="<?php echo $NroInscripcion;?>">
                <input type="hidden" name="ItemAA" id="ItemAA" value="<?php echo $Item;?>">
                <input type="hidden" name="ItemA" id="ItemA" value="<?php echo $Item;?>">
                <input type="hidden" name="FechaAA" id="FechaAA" value="<?php echo $Fecha;?>">
                <input type="text" name="FechaA" id="FechaA" style="width:70px; text-align:center;" value="<?php echo $Fecha;?>" readonly="readonly" onchange="VerifcaFecha();" >
			</td>
        </tr>
		<tr>
		  <td class="TitDetalle">Ocurrencia</td>
		  <td align="center">:</td>
		  <td><span class="CampoDetalle">
		    <select name="CodOcurrenciaA" id="CodOcurrenciaA">
<?php
                $SqlO = "SELECT * FROM vma.ocurrencias ORDER BY descripcion";
                $ConsultaO = $conexion->query($SqlO);
                
                foreach ($ConsultaO->fetchAll() as $RowO)
                {
?>
		      <option value="<?php echo $RowO[0];?>" <?php if ($Ocurrencia==$RowO[0]){echo 'selected="selected"';}?>><?php echo $RowO[1];?></option>
<?php
                }
?>
	      </select>
		  </span></td>
	  </tr>
		<tr>
		  <td class="TitDetalle">Documento</td>
		  <td align="center">:</td>
		  <td><div id='upload_button' style='text-align:center; color:#039; font-size:9px; width:100px; cursor:pointer;'>Seleccionar Archivo</div></td>
		</tr>
	</table>
</form>