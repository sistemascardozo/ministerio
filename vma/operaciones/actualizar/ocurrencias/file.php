<?php
	if(!session_start()){session_start();} 
	
	include('../../../../config.php');
	
	include('../../../include/PDFMerger/PDFMerger.php');

	$pdf = new PDFMerger;
	
	$codsuc			= $_SESSION['IdSucursal'];
	$NroInscripcion	= $_GET['NroInscripcion'];
	$Id				= $_GET['Id'];
	
	$sql_ocurrencias = "SELECT uo.fecha, uo.codocurrencia, o.descripcion, uo.item ";
	$sql_ocurrencias .= "FROM vma.usuarios_ocurrencia AS uo ";
	$sql_ocurrencias .= "INNER JOIN vma.ocurrencias o ON (uo.codocurrencia = o.codocurrencia)";
	$sql_ocurrencias .= "WHERE uo.codsuc=".$codsuc." AND uo.nroinscripcion=".$NroInscripcion." ";
	$sql_ocurrencias .= "ORDER BY uo.fecha DESC, uo.item";
	
	$consulta_ocurrencias = $conexion->prepare($sql_ocurrencias);
	$consulta_ocurrencias->execute(array());
	$items_ocurrencias = $consulta_ocurrencias->fetchAll();
	
	foreach($items_ocurrencias as $rowOcurrencias)
	{					
		$cont_ocurrencias++;
		
		$Fecha = $rowOcurrencias["fecha"];
		$Fecha = substr($Fecha, 0, 4).substr($Fecha, 5, 2).substr($Fecha, 8, 2);
		
		$Item = $rowOcurrencias["item"];
		
		if (strlen($Item)==1)
		{
			$Item = "0".$Item;
		}

		$pdf->addPDF($Id.$Fecha.$Item.'.pdf', 'all');
	}
	$pdf->merge('browser', 'TEST2.pdf');
?>