<?php
  	if(!session_start()){session_start();}
  
  	include("../../../include/main.php");
  	include("../../../include/claseindex.php");
  
  	$TituloVentana = "Padrón de Usuarios no Domésticos - VMA";
  	$Activo = 1;
  
  	$Criterio = 'VMAActualizar';
  
  	CuerpoSuperior($TituloVentana);
  
  	$Op = isset($_GET['Op'])?$_GET['Op']:0;
  	$codsuc = $_SESSION['IdSucursal'];
  
	$Sql = "SELECT c.codantiguo, ".$objFunciones->getCodCatastral("c.").", c.propietario, tc.descripcioncorta || ' ' || public.calles.descripcion || ' # ' || c.nrocalle, 
				VMA.codciiu, vma.ciiu.descripcion, Act.descripcion, VMA.nroinscripcion, VMA.estareg, VMA.cambio_actividad, VMA.fecha_alta, ";
				
	$Sql .= " to_number(to_char(VMA.fecha_alta, 'YYYY'), '9999')*365 + to_number(to_char(VMA.fecha_alta, 'MM'), '99')*30 + to_number(to_char(VMA.fecha_alta, 'DD'), '99') + 320, ";
	$Sql .= " to_number(to_char(CURRENT_DATE, 'YYYY'), '9999')*365 + to_number(to_char(CURRENT_DATE, 'MM'), '99')*30 + to_number(to_char(CURRENT_DATE, 'DD'), '99'), ";
	
	$Sql .= " CASE WHEN VMA.declaracion=1 AND to_number(to_char(VMA.fecha_alta, 'YYYY'), '9999') * 365 + to_number(to_char(VMA.fecha_alta, 'MM'), '99') * 30 + to_number(to_char(VMA.fecha_alta, 'DD'), '99') + 320 < ";
	$Sql .= " 	to_number(to_char(CURRENT_DATE, 'YYYY'), '9999') * 365 + to_number(to_char(CURRENT_DATE, 'MM'), '99') * 30 + to_number(to_char(CURRENT_DATE, 'DD'), '99') THEN 1 ELSE 0 END, ";
  	$Sql .= " (VMA.cambio_categoria + VMA.cambio_actividad + (CASE WHEN VMA.declaracion=1 AND to_number(to_char(VMA.fecha_alta, 'YYYY'), '9999') * 365 + to_number(to_char(VMA.fecha_alta, 'MM'), '99') * 30 + to_number(to_char(VMA.fecha_alta, 'DD'), '99') + 320 < ";
	$Sql .= "	to_number(to_char(CURRENT_DATE, 'YYYY'), '9999') * 365 + to_number(to_char(CURRENT_DATE, 'MM'), '99') * 30 + to_number(to_char(CURRENT_DATE, 'DD'), '99') THEN 1 ELSE 0 END)) AS Verifica ";
	
	$Sql .= "FROM catastro.clientes AS c INNER JOIN vma.usuarios AS VMA ON (c.nroinscripcion = VMA.nroinscripcion)
  			  AND (c.codemp = VMA.codemp) AND (c.codsuc = VMA.codsuc)
			  INNER JOIN public.calles ON (c.codemp = public.calles.codemp) AND (c.codsuc = public.calles.codsuc) AND (c.codzona = public.calles.codzona)
			  AND (c.codcalle = public.calles.codcalle) INNER JOIN public.tiposcalle AS tc ON (public.calles.codtipocalle = tc.codtipocalle) 
			  INNER JOIN vma.ciiu ON (VMA.codciiu = vma.ciiu.codciiu)
			  INNER JOIN vma.actividades AS Act ON (VMA.codactividad = Act.codactividad) ";	
		   
  $FormatoGrilla = array ();	   
  $FormatoGrilla[0] = eregi_replace("[\n\r\n\r]",' ', $Sql); //Sentencia SQL                                                  //Sentencia SQL
  $FormatoGrilla[1] = array('1'=>'c.codantiguo', '2'=>'c.propietario', '3'=>"tc.descripcioncorta || ' ' || public.calles.descripcion || ' # ' || c.nrocalle");          //Campos por los cuales se hará la búsqueda
  $FormatoGrilla[2] = $Op;                                                            //Operacion
  $FormatoGrilla[3] = array('T1'=>'Inscripcion', 'T2'=>'Cod Catastral', 'T3'=>'Nombre Usuario', 'T4'=>'Dirección', 'T5'=>'CIIU', 'T6'=>'Uso del Servicio del CIIU', 'T7'=>'Actividad');   //Títulos de la Cabecera
  $FormatoGrilla[4] = array('A1'=>'center', 'A2'=>'center', 'A3'=>'left', 'A5'=>'center', 'A7'=>'left');                        //Alineación por Columna
  $FormatoGrilla[5] = array('W1'=>'80', 'W2'=>'100', 'W4'=>'150', 'W5'=>'40', 'W6'=>'150', 'W8'=>'60');                                 //Ancho de las Columnas
  $FormatoGrilla[6] = array('TP'=>$TAMANO_PAGINA);                                    //Registro por Páginas
  $FormatoGrilla[7] = 1100;                                                            //Ancho de la Tabla
  $FormatoGrilla[8] = " AND c.codsuc = ".$codsuc." ORDER BY Verifica DESC, codcatastro ASC ";                                   //Orden de la Consulta
  $FormatoGrilla[9] = array('Id'=>'1',                        //Botones de Acciones
              'NB'=>'5',          //Número de Botones a agregar
              'BtnId1'=>'BtnModificar',   //Nombre del Boton
              'BtnI1'=>'modificar.png',   //Imagen a mostrar
              'Btn1'=>'Editar',       //Titulo del Botón
              'BtnF1'=>'onclick="Mostrar(this.id, 1);"',  //Eventos del Botón
              'BtnCI1'=>'9',  //Item a Comparar
              'BtnCV1'=>'1',    //Valor de comparación
              'BtnId2'=>'BtnEliminar', 
              'BtnI2'=>'eliminar.png', 
              'Btn2'=>'Eliminar', 
              'BtnF2'=>'onclick="Eliminar(this.id)"', 
              'BtnCI2'=>'9', //campo 3
              'BtnCV2'=>'1',//igua a 1
              'BtnId3'=>'BtnRestablecer', //y aparece este boton
              'BtnI3'=>'restablecer.png', 
              'Btn3'=>'Restablecer', 
              'BtnF3'=>'onclick="Restablecer(this.id)"', 
              'BtnCI3'=>'9', 
              'BtnCV3'=>'0',
			  'BtnId4'=>'BtnCambios', //y aparece este boton
              'BtnI4'=>'information.png', 
              'Btn4'=>'Cambios en Catastro', 
              'BtnF4'=>'', 
              'BtnCI4'=>'10',
			  'BtnOp4'=>'',
              'BtnCV4'=>'1',
			  'BtnId5'=>'BtnCambios', //y aparece este boton
              'BtnI5'=>'exclamation.png', 
              'Btn5'=>'Vencimiento', 
              'BtnF5'=>'', 
              'BtnCI5'=>'14',
			  'BtnOp5'=>'',
			  //'BtnCII5'=>'13',
              'BtnCV5'=>'1');
	$FormatoGrilla[10] = array(array('Name' =>'id','Col'=>7));//DATOS ADICIONALES
	$FormatoGrilla[11]=7;//FILAS VISIBLES
	
  $_SESSION['Formato'.$Criterio] = $FormatoGrilla;
  
  Cabecera('', $FormatoGrilla[7], 750, 500);
  Pie();
  CuerpoInferior();
?>
<script>
	$(document).ready(function() 
	{
		$("#NuevoAnalisis").dialog			//Ventanas de Dialogo Modal	para Agregar un Nuevo Registro
		({
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "Mantenimiento de Análisis - Nuevo",
			width: 700,
			height: 600,
			show: "scale",
			hide: "scale",
			buttons:
					{
						"Agregar": function() {
							GuardaAnalisis(0);
						},
						Cancelar: function() {
							$("#DivNuevoAnalisis").html('');
							$(this).dialog("close");
						}
					}
		});
		$("#ModificarAnalisis").dialog			//Ventanas de Dialogo Modal	para Modificar un Registro
		({
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "Mantenimiento de Análisis - Modificar",
			width: 700,
			height: 600,
			show: "scale",
			hide: "scale",
			buttons:
					{
						"Modificar": function() {
							GuardaAnalisis(1);
						},
						Cancelar: function() {
							$("#DivModificarAnalisis").html('');
							$(this).dialog("close");
						}
					}
		});
		
		$("#NuevoOcurrencia").dialog			//Ventanas de Dialogo Modal	para Agregar un Nuevo Registro
		({
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "Mantenimiento de Ocurrencia - Nuevo",
			width: 700,
			height: 200,
			show: "scale",
			hide: "scale",
			buttons:
					{
						"Agregar": function() {
							GuardaOcurrencia(0);
						},
						Cancelar: function() {
							$("#DivNuevoOcurrencia").html('');
							$(this).dialog("close");
						}
					}
		});
		$("#ModificarOcurrencia").dialog			//Ventanas de Dialogo Modal	para Modificar un Registro
		({
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "Mantenimiento de Ocurrencia - Modificar",
			width: 700,
			height: 200,
			show: "scale",
			hide: "scale",
			buttons:
					{
						"Modificar": function() {
							GuardaOcurrencia(1);
						},
						Cancelar: function() {
							$("#DivModificarOcurrencia").html('');
							$(this).dialog("close");
						}
					}
		});
		$("#EliminarOcurrencia").dialog			//Ventanas de Dialogo Modal	para Modificar un Registro
		({
			autoOpen: false,
			modal: true,
			resizable: false,
			title: "Mantenimiento de Ocurrencia - Eliminar",
			width: 700,
			height: 200,
			show: "scale",
			hide: "scale",
			buttons:
					{
						"Eliminar": function() {
							GuardaOcurrencia(2);
						},
						Cancelar: function() {
							$("#DivEliminarOcurrencia").html('');
							$(this).dialog("close");
						}
					}
		});
	});
	
	function GuardaAnalisis(Op)
	{
		//alert('sss');
		$.ajax({
			url: 'GuardarA.php?Op=' + Op,
			type: 'POST',
			async: true,
			data: $('#formA').serialize(),
			success: function(data) {
				OperMensaje(data)
				//$("#Mensajes").html(data);
				$("#DivNuevoAnalisis, #DivModificarAnalisis, #DivEliminarAnalisis").html('');

				$("#NuevoAnalisis").dialog("close");
				$("#ModificarAnalisis").dialog("close");
				//$("#EliminarDiv").dialog("close");
				$("#ConfirmaEliminacion").dialog("close");
				$("#ConfirmaRestauracion").dialog("close");
				SeleccionaItem(UnidadUso);
			}
		})
	}
	function GuardaOcurrencia(Op)
	{
		$.ajax({
			url: 'GuardarO.php?Op=' + Op,
			type: 'POST',
			async: true,
			data: $('#formO').serialize(),
			success: function(data) {
				OperMensaje(data)
				//$("#Mensajes").html(data);
				$("#DivNuevoOcurrencia, #DivModificarOcurrencia, #DivEliminarOcurrencia").html('');

				$("#NuevoOcurrencia").dialog("close");
				$("#ModificarOcurrencia").dialog("close");
				$("#EliminarOcurrencia").dialog("close");
				$("#ConfirmaOcurrencia").dialog("close");
				$("#ConfirmaOcurrencia").dialog("close");
				MostrarOcurrencias();
			}
		})
	}
</script>
<div id="NuevoAnalisis" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivNuevoAnalisis" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="ModificarAnalisis" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivModificarAnalisis" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="NuevoOcurrencia" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivNuevoOcurrencia" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="ModificarOcurrencia" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivModificarOcurrencia" style="width:100%"></div></td>
        </tr>
    </table>
</div>
<div id="EliminarOcurrencia" title="Mantenimiento" style="display:none;">
    <table width="100%">
        <tr>
            <td><div id="DivEliminarOcurrencia" style="width:100%"></div></td>
        </tr>
    </table>
</div>