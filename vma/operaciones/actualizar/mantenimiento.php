<?php 	
	session_name("siincoweb");
	if(!session_start()){session_start();} 
	
	include("../../../objetos/clsMantenimiento.php");

	$Op 		= $_POST["Op"];
	$Id 		= isset($_POST["Id"])?$_POST["Id"]:'0';
	$guardar	= "op=".$Op;
	$codsuc 	= $_SESSION['IdSucursal'];
	$Aplicable	= 1;
	$DJurada	= 0;
	$Analisis	= 0;
	
	$FechaAlta	= date('d/m/Y');
	$FechaBaja	= date('d/m/Y');
		
	$objMantenimiento 	= new clsMantenimiento();
	$objFunciones 	= new clsFunciones();

	if($Id!='')
	{
		$Id2 = intval(substr($Id, 2));
		
		$Select	= "SELECT ".$objFunciones->getCodCatastral("CLI.").", VMA.nroinscripcion, VMA.codciiu, CLI.propietario, ";
		$Select	.= " VMA.codactividad, VMA.aplica, VMA.declaracion, VMA.analisis, VMA.estareg, VMA.fecha_alta, VMA.fecha_baja, VMA.codusuario, ";
		$Select	.= " VMA.resolucion ";
		$Select	.= "FROM catastro.clientes AS CLI INNER JOIN vma.usuarios AS VMA ON (CLI.nroinscripcion = VMA.nroinscripcion) ";
		$Select	.= " AND (CLI.codemp = VMA.codemp) AND (CLI.codsuc = VMA.codsuc) ";
		$Select	.= "WHERE VMA.codsuc = ".$codsuc." AND VMA.nroinscripcion=".intval(substr($Id, 2));
		$row	= $conexion->query($Select)->fetch();
		
		$CodCatastro = $row[0];
		$CodUsuario = $row[11];
		$Aplicable	= $row[5];
		$DJurada	= $row[6];
		$Analisis	= $row[7];
		$Estado 	= $row[8];
		
		$FechaAlta	= $objFunciones->DecFecha($row[9]);
		$FechaBaja	= $objFunciones->DecFecha($row[10]);
		
		$Resolucion	= $row[12];
		
		$guardar	= $guardar."&Id2=".$Id;
		
	}
	else
	{
		$Select	= "SELECT MAX(CAST(CASE WHEN substr(codusuario, 4)='' THEN '0' ELSE substr(codusuario, 4) END AS INT)) + 1 ";
		$Select	.= "FROM vma.usuarios ";
		$Select	.= "WHERE codsuc = ".$codsuc;
		$row	= $conexion->query($Select)->fetch();
		
		$Idddd = $row[0];
		if ($row[0]==0)
		{
			$Idddd = 1;
		}
		
		$CodUsuario = "UND".substr("000000", 0, 6 - strlen($Idddd)).$Idddd;
	}
?>

<script type="text/javascript" src="<?php echo $_SESSION['urldir'];?>js/funciones.js" language="JavaScript"></script>
<script>
	$(document).ready(function() {
		$( "#tabs" ).tabs();
        
	  $('form').keypress(function(e){   
		if(e == 13){
		  return false;
		}
	  });

	  $('input').keypress(function(e){
		if(e.which == 13){
		  return false;
		}
	  });
		
		$("#FechaAlta").datepicker({  
			//showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: urlDir + 'images/btn_calendar.png',
			buttonImageOnly: true,
			showOn: 'both', //mostrar en el input
			showButtonPanel: true

		});
		$("#FechaBaja").datepicker({  
			//showAnim: 'scale' ,
			showOn: 'button',
			direction: 'up',
			buttonImage: urlDir + 'images/btn_calendar.png',
			buttonImageOnly: true,
			showOn: 'both', //mostrar en el input
			showButtonPanel: true

		});
	});
	function ValidarForm(Op)
	{
		if ($("#NroInscripcion").val() == '')
		{
			alert('Seleccione un Usuario');
			buscar_usuarios();
			return false;
		}
		$('#DivPar :input').each(function()
			 {
			 	
			 	if($(this).attr("checked"))
					$(this).val(1); 
				else
					{	$(this).attr("checked","checked");
						$(this).val(0); 
					}
			});
		GuardarP(Op);
	}
	
	function Cancelar()
	{
		location.href='index.php';
	}
	
	function CambiaAplicable()
	{
		//$('#TrDJ').css('visibility', 'hidden');
		$('#TrAn').css('visibility', 'hidden');
		
		if (document.getElementById('Aplicable2').checked)
		{
			$('#Aplicable').val(1);
			//$('#TrDJ').css('visibility', 'visible');
			$('#TrAn').css('visibility', 'visible');
		}
		else
		{
			$('#Aplicable').val(0);
		}
	}
	
	function CambiaAnalisis()
	{
		if (document.getElementById('Analisis2').checked)
		{
			$('#Analisis').val(1);
		}
		else
		{
			$('#Analisis').val(0);
		}
	}
	
	function CambiaDJ()
	{
		if (document.getElementById('DJurada2').checked)
		{
			$('#DJurada').val(1);
		}
		else
		{
			$('#DJurada').val(0);
		}
	}
	
	function agregar_unidades_uso(codsuc)
	{
		if($("#NroInscripcion").val()=='')	
		{
			alert("Seleccione un Usuario!")
			return false;
		}
		if($("#UUDescripcion").val()=='')	
		{
			alert("Ingrese la descripción")
			return false;
		}
		if($("#UUPorcentaje").val()==0 || $("#UUPorcentaje").val()=="" || $("#UUPorcentaje").val()<0 || $("#UUPorcentaje").val()>100)	
		{
			alert("El Porcentaje Ingresado no es Valido")
			return false;
		}
		if($("#total_porct").val()>=100)
		{
			alert("Ya se alcanzo el 100%")
			return false;
		}
		
		
		var Descripcion   = $("#UUDescripcion").val();
		total_porcentaje   += parseFloat($("#UUPorcentaje").val());
		if(total_porcentaje>100)
		{
			alert("Se sobrepaso el 100%")
			total_porcentaje   -= parseFloat($("#procentaje").val());
			return false;
		}	
		
		
		for(i=1;i<=$("#cont_unidades").val();i++)
		{
			try
			{
				
			}catch(exp)
			{
				
			}	
		}

		cont_unidad++;
		cont_fila_unidad++;
		$("#total_porct").val(total_porcentaje)
		$("#tbunidades tbody").append(
										"<tr style='background-color:#FFFFFF; padding:4px' onclick='SeleccionaItem(" + cont_unidad + ");'>" +
										"<td align='center'><input type='radio' name='radio' id='radio" + cont_unidad + "' value='" + cont_unidad + "' /></td>" + 
										"<td align='center'><input type='hidden' id='CodUnidad" + cont_unidad + "' name='CodUnidad" + cont_unidad + "' value='" + cont_unidad + "' />" + cont_unidad + "</td>" +
										"<td style='padding-left:5px;'><input type='hidden' id='Descripcion" + cont_unidad + "' name='Descripcion" + cont_unidad + "' value='" + Descripcion + "'/>" + Descripcion + "</td>"+
										"<td align='center' ><input type='text' id='porcentaje" + cont_unidad + "' name='porcentaje" + cont_unidad + "' value='" + $("#UUPorcentaje").val() + "' style='width:45px' /></td>"+
										"<td align='center' ><img src='../../../images/cancel.png' width='16' height='16' title='Borrar Registro' onclick='QuitaFilaD(this, 1, " + cont_unidad + ");' style='cursor:pointer'/></td>"+
										"</tr>");
		
		$("#cont_unidades").val(cont_unidad)
		
		$("#UUDescripcion").val('');

		$("#UUPorcentaje").val(100 - parseFloat($("#total_porct").val()))
	
	}
	
	function QuitaFilaD(x, opcion, idx)
	{  
		while (x.tagName.toLowerCase() !='tr')
		{
			if(x.parentElement)
				x = x.parentElement;
			else if(x.parentNode)
				x = x.parentNode;
			else
				return;
		}
		
		var rowNum = x.rowIndex;
		while (x.tagName.toLowerCase() !='table')
		{
			if(x.parentElement)
				x = x.parentElement;
			else if(x.parentNode)
				x = x.parentNode;
			else
				return;
		}
		if(opcion==1)
		{
			total_porcentaje = parseFloat(total_porcentaje) - parseFloat($("#porcentaje" + idx).val())
			$("#total_porct").val(total_porcentaje)
			$("#UUPorcentaje").val(parseFloat($("#UUPorcentaje").val()) + parseFloat($("#porcentaje" + idx).val()))
			
			cont_fila_unidad = parseFloat(cont_fila_unidad) - 1;
		}
		if(opcion==2)
		{
			quitar_ocurrencia($('#NroInscripcion').val(), idx);
		}
		x.deleteRow(rowNum);
	}
	
	function quitar_ocurrencia(NroInscripcion, Idx)
	{
		
	}
	
	var UnidadUso = 0;
	
	function SeleccionaItem(Item)
	{
		$("#radio" + Item).attr('checked', 'checked');
		UnidadUso = 0;
		if ($('#radio' + Item).length){
		 	//Ejecutar si existe el elemento
		 	UnidadUso = $("#radio" + Item).val();
		}
		
		NroInscripcion = 0;
		
		if ($("#NroInscripcion").val()!='')
		{
			NroInscripcion = parseInt($("#NroInscripcion").val().substr(2));
		}
		
		//alert(NroInscripcion);
		//alert(UnidadUso);
		$.ajax({
			url: 'analisis.php',
			type: 'POST',
			async: true,
			data: 'NroInscripcion=' + NroInscripcion + '&CodUnidadUso=' + UnidadUso,
			success: function(data) {
				$("#DivAnalisis").html(data);
			}
		});
		
		$.ajax({
			url: 'tabla1.php',
			type: 'POST',
			async: true,
			data: 'NroInscripcion=' + NroInscripcion + '&CodUnidadUso=' + UnidadUso,
			success: function(data) {
				$("#DivTabla1").html(data);
			}
		});
	}
	
	function ModificaAnalisis(Op, UnidadUso, Fecha)
	{
		if($("#NroInscripcion").val()=='')	
		{
			alert("Seleccione un Usuario!")
			return false;
		}
		if(UnidadUso==0)	
		{
			alert("Seleccione una Unidad de Uso!")
			return false;
		}
		
		NroInscripcion = parseInt($("#NroInscripcion").val().substr(2));
		Descripcion = $("#Descripcion" + UnidadUso).val();
		Porcentaje = $("#porcentaje" + UnidadUso).val();
		
		if (Op==0)
		{
			$("#NuevoAnalisis").dialog("open");
			$("#DivNuevoAnalisis").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
		}
		if (Op==1)
		{
			$("#ModificarAnalisis").dialog("open");
			$("#DivModificarAnalisis").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
		}
		
		$("#formA").remove();
		$.ajax({
			url: 'MantenimientoA.php',
			type: 'POST',
			async: true,
			data: 'Op=' + Op + '&codsuc=<?=$codsuc?>&NroInscripcion=' + NroInscripcion + '&UnidadUso=' + UnidadUso + '&Fecha=' + Fecha + 
					'&Descripcion=' + Descripcion + '&Porcentaje=' + Porcentaje,
			success: function(data) {
				//alert(data);
				if (Op == 0) {
					$("#DivNuevoAnalisis").html(data);
				}
				if (Op == 1) {
					$("#DivModificarAnalisis").html(data);
				}
				if (Op == 2) {
					$("#DivEliminarAnalisis").html(data);
				}
				$("#" + Foco).focus();
			}
		})
	}
	
	function buscar_usuarios()
	{
		objIdx = "cliente"
		AbrirPopupBusqueda("../../../sigco/catastro/operaciones/actualizacion/?Op=5", 1150, 500)
	}
	function Recibir(Id)
	{
		if(objIdx=="cliente")
		{
			cargar_datos_usuario(Id)
		}
	}
	function cargar_datos_usuario(nroinscripcion)
	{
		$.ajax({
			 url:'../../../ajax/clientes.php',
			 type:'POST',
			 async:true,
			 data:'codsuc=<? echo $codsuc;?>&nroinscripcion=' + nroinscripcion,
			 dataType: "json",
			 success:function(datos){
				/*var r=datos.split("|")*/
				$("#NroInscripcion").val(datos.codantiguo);
				$("#Usuario").val(datos.propietario);
				
				SeleccionaItem(1);
			 }
		}) 
	}
	
	function Ver(Inscripcion, Fecha, Item)
	{
		if (Item.length==1)
		{
			Item = '0' + Item;
		}
		Url = Inscripcion + Fecha.substr(0, 4) + Fecha.substr(5, 2) + Fecha.substr(8, 2) + Item + '.pdf';
		//alert(Url);
		AbrirPopupBusqueda("ocurrencias/" + Url, 1150, 500);
	}
	
	function MostrarOcurrencias()
	{
		if ($("#NroInscripcion").val()!='')
		{
			NroInscripcion = parseInt($("#NroInscripcion").val().substr(2));
		}

		$.ajax({
			url: 'ocurrencias.php',
			type: 'POST',
			async: true,
			data: 'NroInscripcion=' + NroInscripcion + '&Id=' + $("#NroInscripcion").val(),
			success: function(data) {
				$("#DivOcurrencias").html(data);
			}
		});
	}
	
	function ModificaOcurrencia(Op, Item, Fecha)
	{
		if($("#NroInscripcion").val()=='')	
		{
			alert("Seleccione un Usuario!")
			return false;
		}
		
		NroInscripcion = parseInt($("#NroInscripcion").val().substr(2));
		
		if (Op==0)
		{
			$("#NuevoOcurrencia").dialog("open");
			$("#DivNuevoOcurrencia").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
		}
		if (Op==1)
		{
			$("#ModificarOcurrencia").dialog("open");
			$("#DivModificarOcurrencia").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
		}
		if (Op==2)
		{
			$("#EliminarOcurrencia").dialog("open");
			$("#DivEliminarOcurrencia").html("<center><img src='<?php echo $_SESSION['urldir'];?>images/avance.gif' width=20 /></center>");
		}
	
		$("#formO").remove();
		$.ajax({
			url: 'MantenimientoO.php',
			type: 'POST',
			async: true,
			data: 'Op=' + Op + '&codsuc=<?=$codsuc?>&NroInscripcion=' + NroInscripcion + '&Item=' + Item + '&Fecha=' + Fecha + '&Id=' + $("#NroInscripcion").val(),
			success: function(data) {
				//alert(data);
				if (Op == 0) {
					$("#DivNuevoOcurrencia").html(data);
				}
				if (Op == 1) {
					$("#DivModificarOcurrencia").html(data);
				}
				if (Op == 2) {
					$("#DivEliminarOcurrencia").html(data);
				}
				$("#" + Foco).focus();
			}
		})
	}
	
	function GeneraFile()
	{
		if ($("#NroInscripcion").val()!='')
		{
			NroInscripcion = parseInt($("#NroInscripcion").val().substr(2));
		}
		
		AbrirPopupBusqueda("ocurrencias/file.php?NroInscripcion=" + NroInscripcion + "&Id=" + $("#NroInscripcion").val(), 1150, 500);
	}
</script>
<div align="center">
<form id="form1" name="form1" method="post" action="Guardar.php?<?php echo $guardar;?>" enctype="multipart/form-data">
	<table width="680" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" id="tbCampos">
		<tr>
			<td colspan="3">
                <div id="tabs">
                    <ul>
                        <li style="font-size:10px"><a href="#tabs-1">Datos del Usuario</a></li>
                        <li style="font-size:10px"><a href="#tabs-2">Unidades de Uso</a></li>
                        <li style="font-size:10px"><a href="#tabs-3">Ocurrencias</a></li>
                    </ul>
                    <div id="tabs-1">
                        <table width="99%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="TitDetalle">Nro Inscripción</td>
                                <td width="10" align="center" class="TitDetalle">:</td>
                                <td class="CampoDetalle">
                                    <input name="NroInscripcion" type="text" id="NroInscripcion" size="10" readonly="readonly" value="<? echo $Id; ?>" class="inputtext"/>
                                	<img src="../../../images/buscar.png" width="16" height="16" title="Buscar Usuario del Catalogo" style="cursor:pointer; <?php if ($Op!=0){ echo 'display:none';}?>" onclick="buscar_usuarios();" />                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Código</td>
                                <td align="center" class="TitDetalle">:</td>
                                <td class="CampoDetalle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                      <td width="100"><input name="CodUsuario" type="text" id="CodUsuario" value="<?=$CodUsuario?>" class="inputtext" style="width:90px;"/></td>
                                      <td width="100">Cod. Catastro :</td>
                                      <td><input name="Catastro" type="text" id="Catastro" size="15" readonly="readonly" value="<? echo $CodCatastro; ?>" class="inputtext"/></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Nombre Usuario</td>
                                <td align="center" class="TitDetalle">:</td>
                                <td class="CampoDetalle"><input class="inputtext" name="Usuario" type="text" id="Usuario" size="70" maxlength="200" value="<?php echo $row[3];?>" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">CIIU</td>
                                <td align="center" class="TitDetalle">:</td>
                                <td class="CampoDetalle">
                                    <select name="CodCiiu" id="CodCiiu" style="width:500px">
<?php
                $SqlU = "SELECT * FROM vma.ciiu ORDER BY codciiu";
                $Consulta = $conexion->query($SqlU);
                
                foreach ($Consulta->fetchAll() as $RowU)
                {
?>
                                        <option value="<?php echo $RowU[0];?>" <?php if ($row[2]==$RowU[0]){echo 'selected="selected"';}?>><?php echo $RowU[0]." ".$RowU[5];?></option>
<?php
                }
?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Actividad</td>
                                <td width="10" align="center" class="TitDetalle">:</td>
                                <td class="CampoDetalle">
                                    <select name="CodActividad" id="CodActividad" style="width:500px">
<?php
                $SqlU = "SELECT * FROM vma.actividades ORDER BY codactividad";
                $Consulta = $conexion->query($SqlU);
                
                foreach ($Consulta->fetchAll() as $RowU)
                {
?>
                                        <option value="<?php echo $RowU[0];?>" <?php if ($row[4]==$RowU[0]){echo 'selected="selected"';}?>><?php echo $RowU[2];?></option>
<?php
                }
?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                              <td class="TitDetalle">Fecha Alta</td>
                              <td align="center" class="TitDetalle">:</td>
                              <td class="CampoDetalle"><input type="text" name="FechaAlta" id="FechaAlta" style="width:80px" value="<?php echo $FechaAlta;?>" readonly="readonly" ></td>
                            </tr>
                            <tr>
                              <td class="TitDetalle">Fecha Baja</td>
                              <td align="center" class="TitDetalle">:</td>
                              <td class="CampoDetalle"><input type="text" name="FechaBaja" id="FechaBaja" style="width:80px" value="<?php echo $FechaBaja;?>" readonly="readonly" /></td>
                            </tr>
                            <tr id="TrDJ">
                              <td class="TitDetalle">Dec. Jurada</td>
                              <td align="center" class="TitDetalle">:</td>
                              <td class="CampoDetalle"><input type="checkbox" name="DJurada2" id="DJurada2" <? if ($DJurada==1) echo "checked='checked'"?> onclick="CambiaDJ();" /><input type="hidden" name="DJurada" id="DJurada" value="<?=$DJurada?>" /></td>
                            </tr>
                            <tr>
                                <td class="TitDetalle">Aplicable</td>
                                <td align="center" class="TitDetalle">:</td>
                                <td class="CampoDetalle"><input type="checkbox" name="Aplicable2" id="Aplicable2" <? if ($Aplicable==1) echo "checked='checked'"?> onclick="CambiaAplicable();" /><input type="hidden" name="Aplicable" id="Aplicable" value="<?=$Aplicable?>" /></td>
                            </tr>
                            <tr id="TrAn">
                              <td class="TitDetalle">Análisis</td>
                              <td align="center" class="TitDetalle">:</td>
                              <td class="CampoDetalle"><input type="checkbox" name="Analisis2" id="Analisis2" <? if ($Analisis==1) echo "checked='checked'"?> onclick="CambiaAnalisis();" /><input type="hidden" name="Analisis" id="Analisis" value="<?=$Analisis?>" /></td>
                            </tr>
                            <tr>
                              <td class="TitDetalle">Resolución</td>
                              <td align="center" class="TitDetalle">:</td>
                              <td class="CampoDetalle"><input class="inputtext" name="Resolucion" type="text" id="Resolucion" size="70" maxlength="200" value="<?php echo $Resolucion;?>"/></td>
                            </tr>
                        </table>
                    </div>
                    <div id="tabs-2" style="height:200px">
                        <table width="99%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="50%">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                      <tr style="color:#FFF;" bgcolor="#006699">
                                        <td width="240" align="center">Descripcion</td>
                                        <td align="center"> (%)</td>
                                        <td width="30">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td><input type="text" name="UUDescripcion" id="UUDescripcion" style="width:240px" /></td>
                                        <td><input type="text" name="UUPorcentaje" id="UUPorcentaje" style="width:40px; text-align:center" /></td>
                                        <td align="center"><img id="porimg" src="../../../images/add.png" style="cursor: pointer" onclick="agregar_unidades_uso(<?=$codsuc?>);" title="Agregar Unidad de Uso" /></td>
                                      </tr>
                                    </table></td>
                                    <td rowspan="2" align="center" valign="top">
                                    	<table width="90%" border="0" cellspacing="0" cellpadding="0" <?php if ($Op==0){echo 'style="display:none;"';}?>>
                                          <tr>
                                            <td align="center" style="color:#FFF;" bgcolor="#006699">Análisis</td>
                                            <td width="30" align="center" bgcolor="#006699" style="color:#FFF;"><img id="porimg2" src="../../../images/add.png" style="cursor: pointer" onclick="ModificaAnalisis(0, UnidadUso, '');;" title="Agregar Nuevo Análisis" /></td>
                                          </tr>
                                          <tr>
                                            <td colspan="2">
                                                <div id="DivAnalisis"></div>
                                            </td>
                                          </tr>
                                        </table>
                                     </td>
                                  </tr>
                                  <tr>
                                    <td>
                                    	<div style="overflow:auto; height: 150px">
                                        	<input name="cont_unidades" id="cont_unidades" type="hidden" value="0" />
                      						<input type="hidden" name="total_porct" id="total_porct" value="0" />
											<table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tbunidades" >
												<thead class="ui-widget-header" style="font-size: 11px">
													<tr>
													  <th width="20">&nbsp;</th>
														<th>Codigo</th>
														<th width="150" >Descripcion</th>
														<th width="45" >%</th>
														<th>&nbsp;</th>
													</tr>
												</thead>
												<tbody>
<?php
				$cont_unidades 	= 0;
				$tot_porcentaje = 0;
				
				if($Id!="")
				{
					$sql_unidades = "SELECT codsuc, nroinscripcion, codunidad_uso, descripcion, porcentaje, factor ";
					$sql_unidades .= "FROM vma.unidades_uso ";
					$sql_unidades .= "WHERE codsuc=? AND nroinscripcion=? ";
					$sql_unidades .= "ORDER BY codunidad_uso";
					
					$consulta_unidades = $conexion->prepare($sql_unidades);
					$consulta_unidades->execute(array($codsuc, $Id2));
					$items_unidad = $consulta_unidades->fetchAll();
					
					foreach($items_unidad as $rowUnidades)
					{					
						$cont_unidades++;
						
						$tot_porcentaje += $rowUnidades["porcentaje"];
?>
													<tr style='background-color:#FFFFFF; padding:4px; cursor:pointer' title="<?=$rowUnidades["factor"]?>" onclick="SeleccionaItem(<?=$cont_unidades?>);">
													  <td align="center" ><input type="radio" name="radio" id="radio<?=$cont_unidades?>" value="<?=$rowUnidades["codunidad_uso"]?>" onchange="" /></td>
														<td align="center" >
															<input type='hidden' id='CodUnidad<?=$cont_unidades?>' name='CodUnidad<?=$cont_unidades?>' value='<?=$rowUnidades["codunidad_uso"]?>' /><?=$rowUnidades["codunidad_uso"]?>
														</td>
														<td style="padding-left:5px">
                                                        	<input type='hidden' id='Descripcion<?=$cont_unidades?>' name='Descripcion<?=$cont_unidades?>' value='<?=$rowUnidades["descripcion"]?>'/>
															<?=strtoupper($rowUnidades["descripcion"])?>
														</td>
														<td align="center" ><input type='text' id='porcentaje<?=$cont_unidades?>' name='porcentaje<?=$cont_unidades?>' value='<?=$rowUnidades["porcentaje"]?>' style="width:45px; text-align:center;" /></td>
														<td align="center" >
															<img src='../../../images/cancel.png' width='16' height='16' title='Borrar Registro' onclick='QuitaFilaD(this,1,<?=$cont_unidades?>);' style='cursor:pointer'/>
														</td>
													</tr>
<?php
					}
				}
?>
												</tbody>
<script>
	cont_unidad			= <?=$cont_unidades?>;
	cont_fila_unidad 	= <?=$cont_unidades?>;
	total_porcentaje	= <?=$tot_porcentaje?>;
		
	$("#cont_unidades").val(<?=$cont_unidades?>)
	$("#total_porct").val(<?=$tot_porcentaje?>)
		
	$("#procentaje").val(100 - <?=$tot_porcentaje?>)
</script>
											</table>
										</div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                                <div id="DivTabla1" align="center"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="tabs-3" style="height:300px">
                        <table width="99%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center">
                                	<table width="90%" border="0" cellspacing="0" cellpadding="0">
                                      	<tr>
                                            <td align="center" style="color:#FFF;" bgcolor="#006699">Ocurrencias</td>
                                            <td width="30" align="center" bgcolor="#006699" style="color:#FFF;"><img id="porimg2" src="../../../images/add.png" style="cursor: pointer" onclick="ModificaOcurrencia(0, 0, '');" title="Agregar Nueva Ocurrencia" /></td>
                                      	</tr>
                                      	<tr>
                                        	<td colspan="2">
                                				<div id="DivOcurrencias"></div>
											</td>
										</tr>
									</table>
                                </td>
                            </tr>
                            <tr>
                              <td align="center">&nbsp;</td>
                            </tr>
                            <tr>
                              <td align="center"><span class="BtnMigrador" onclick="GeneraFile();">&nbsp;Ver Archivo&nbsp;</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
    		</td>
		</tr>
    </table>
 </form>
</div>
<script>
	$("#CodCiiu").focus();
	
	CambiaAplicable(); 
	SeleccionaItem(1);
	MostrarOcurrencias();
</script>