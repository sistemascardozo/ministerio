<?php
	if(!session_start()){session_start();}

	include('../../../objetos/clsFunciones.php');
	
	$conexion->beginTransaction();
        
	$Op         = $_GET["Op"];
	
	$objFunciones = new clsFunciones();
	
	$codsuc 	= $_SESSION['IdSucursal'];
	
	$NroInscripcion	= $_POST["NroInscripcionA"];
	$IdUnidadUso	= $_POST["IdUnidadUsoA"];
	$FechaAnt		= $_POST["FechaAA"];
	$Fecha			= $_POST["FechaA"];
	$CodOrigen		= $_POST["CodOrigenA"];
	$CodLaboratorio	= $_POST["CodLaboratorioA"];
	
	$DescripcionUU	= $_POST["UnidadUsoA"];
	$PorcentajeUU	= $_POST["PorcentajeUsoA"];
	
	$Error = 0;
	//Verificamos si la Unidad de Uso existe o es Nueva
	$SqlUU	= "SELECT COUNT(*) FROM vma.unidades_uso ";
	$SqlUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." ";
	$SqlUU	.= " AND codunidad_uso = ".$IdUnidadUso." ";
	
	$RowUU	= $conexion->query($SqlUU)->fetch();
	
	if ($RowUU[0]==0)
	{
		$Sql	= "INSERT INTO vma.unidades_uso ";
		$Sql	.= " (codemp, codsuc, nroinscripcion, codunidad_uso, porcentaje, descripcion) ";
		$Sql	.= "VALUES(1, ".$codsuc.", ".$NroInscripcion.", ".$IdUnidadUso.", '".$PorcentajeUU."', '".$DescripcionUU."')";
		$result = $conexion->prepare($Sql);
		$result->execute(array());
		
		if($result->errorCode()!='00000')
		{
			$Error = 1;
			echo "Error-> <br>";
			echo $Sql."<br>";
		}
	}
	
	switch ($Op) 
	{
		case 0:
			$Sql	= "INSERT INTO vma.usuarios_analisis ";
			$Sql	.= " (codemp, codsuc, nroinscripcion, codunidad_uso, fecha, codorigen, codlaboratorio) ";
			$Sql	.= "VALUES(1, ".$codsuc.", ".$NroInscripcion.", ".$IdUnidadUso.", '".$Fecha."', ".$CodOrigen.", ".$CodLaboratorio.")";
			$result = $conexion->prepare($Sql);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				echo "Error-> <br>";
				echo $Sql."<br>";
			}
			
			break;
			
		case 1:
			$Sql	= "UPDATE vma.usuarios_analisis ";
			$Sql	.= "SET fecha = '".$Fecha."', codorigen = ".$CodOrigen.", codlaboratorio = ".$CodLaboratorio." ";
			$Sql	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." AND codunidad_uso = ".$IdUnidadUso." AND fecha = '".$FechaAnt."' ";
			$result = $conexion->prepare($Sql);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				echo "Error-> <br>";
				echo $Sql."<br>";
			}
			
			break;
			
		case 2:
			
			break;
			
	}
	$SqlP	= "SELECT * FROM vma.parametros ";
	$SqlP	.= "ORDER BY codanexo, codparametro ";
	$consultaP = $conexion->prepare($SqlP);
	$consultaP->execute(array());
	$itemsP = $consultaP->fetchAll();
	
	$FactorUU = 0;
	foreach($itemsP as $itemP)
	{
		$itemP[0];
		$Valor = $_POST['Parametro'.$itemP[0]];
		$Factor = 0;
		
		if ($itemP[4]==1)
		{
			$SqlR	= "SELECT porcentaje FROM vma.rangos_parametros ";
			$SqlR	.= "WHERE codparametro = ".$itemP[0]." AND inicio<=".$Valor." AND final>=".$Valor." ";
			
			if (count($conexion->query($SqlR)->fetchAll())>0)
			{
				$RowR	= $conexion->query($SqlR)->fetch();
				$Factor = $RowR[0];
			}
		}
		
		$SqlPV	= "SELECT valor FROM vma.usuarios_analisis_parametros ";
		$SqlPV	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." ";
		$SqlPV	.= " AND codunidad_uso = ".$IdUnidadUso." AND fecha = '".$Fecha."' AND codparametro = ".$itemP[0]." ";
		
		if (count($conexion->query($SqlPV)->fetchAll())>0)
		{
			$SqlAP	= "UPDATE vma.usuarios_analisis_parametros ";
			$SqlAP	.= "SET valor = ".$Valor.", factor = ".$Factor." ";
			$SqlAP	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." AND codunidad_uso = ".$IdUnidadUso." AND fecha = '".$Fecha."' ";
			$SqlAP	.= " AND codparametro = ".$itemP[0];
			$resultAP = $conexion->prepare($SqlAP);
			$resultAP->execute(array());
			
			if($resultAP->errorCode()!='00000')
			{
				$Error = 1;
				echo "Error-> <br>";
				echo $SqlAP."<br>";
			}
		}
		else
		{
			$Sql	= "INSERT INTO vma.usuarios_analisis_parametros ";
			$Sql	.= " (codemp, codsuc, nroinscripcion, codunidad_uso, fecha, codparametro, valor, factor) ";
			$Sql	.= "VALUES(1, ".$codsuc.", ".$NroInscripcion.", ".$IdUnidadUso.", '".$Fecha."', ".$itemP[0].", ".$Valor.", ".$Factor.")";
			$result = $conexion->prepare($Sql);
			$result->execute(array());
			
			if($result->errorCode()!='00000')
			{
				$Error = 1;
				echo "Error-> <br>";
				echo $Sql."<br>";
			}
		}
		
	}
	
//Se realiza el recalculo dle factor de la Unidad de uso según último análisis	
	$SqlAU	= "SELECT fecha FROM vma.usuarios_analisis ";
	$SqlAU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." ";
	$SqlAU	.= " AND codunidad_uso = ".$IdUnidadUso." ";
	$SqlAU	.= "ORDER BY fecha DESC";
	
	$RowAU	= $conexion->query($SqlAU)->fetch();
	
	$SqlPV	= "SELECT factor FROM vma.usuarios_analisis_parametros ";
	$SqlPV	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." ";
	$SqlPV	.= " AND codunidad_uso = ".$IdUnidadUso." AND fecha = '".$RowAU[0]."' ";
	
	$consultaPV = $conexion->prepare($SqlPV);
	$consultaPV->execute(array());
	$itemsPV = $consultaPV->fetchAll();
	
	foreach($itemsPV as $itemPV)
	{
		$FactorUU = $FactorUU + $itemPV[0];
			
		$SqlFUU	= "UPDATE vma.unidades_uso ";
		$SqlFUU	.= "SET factor = ".$FactorUU." ";
		$SqlFUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." AND codunidad_uso = ".$IdUnidadUso;
		$resultFUU = $conexion->prepare($SqlFUU);
		$resultFUU->execute(array());
		
		if($resultFUU->errorCode()!='00000')
		{
			$Error = 1;
			echo "Error-> <br>";
			echo $SqlFUU."<br>";
		}
	}
	
	$SqlPV	= "SELECT factor FROM vma.usuarios_analisis_parametros ";
	$SqlPV	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion." ";
	
	$consultaPV = $conexion->prepare($SqlPV);
	$consultaPV->execute(array());
	$itemsPV = $consultaPV->fetchAll();
	
	$FactorUU = 0;
	foreach($itemsPV as $itemPV)
	{
		$FactorUU = $FactorUU + $itemPV[0];
	}
	
	if ($FactorUU==0)
	{
		$SqlFUU	= "UPDATE catastro.clientes ";
		$SqlFUU	.= "SET vma = 0 ";
		$SqlFUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion."; ";
		
		$resultFUU = $conexion->prepare($SqlFUU);
		$resultFUU->execute(array());
		
		if($resultFUU->errorCode()!='00000')
		{
			$Error = 1;
			echo "Error-> <br>";
			echo $SqlFUU."<br>";
		}
		
		$SqlFUU	= "UPDATE catastro.conexiones ";
		$SqlFUU	.= "SET codtipoentidades = 0 ";
		$SqlFUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion."; ";
		
		$resultFUU = $conexion->prepare($SqlFUU);
		$resultFUU->execute(array());
		
		if($resultFUU->errorCode()!='00000')
		{
			$Error = 1;
			echo "Error-> <br>";
			echo $SqlFUU."<br>";
		}
	}
	else
	{
		$SqlFUU	= "UPDATE catastro.clientes ";
		$SqlFUU	.= "SET vma = 1 ";
		$SqlFUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion."; ";
		
		$resultFUU = $conexion->prepare($SqlFUU);
		$resultFUU->execute(array());
		
		if($resultFUU->errorCode()!='00000')
		{
			$Error = 1;
			echo "Error-> <br>";
			echo $SqlFUU."<br>";
		}
		
		$SqlFUU	= "UPDATE catastro.conexiones ";
		$SqlFUU	.= "SET codtipoentidades = 83 ";
		$SqlFUU	.= "WHERE codsuc = ".$codsuc." AND nroinscripcion = ".$NroInscripcion."; ";
		
		$resultFUU = $conexion->prepare($SqlFUU);
		$resultFUU->execute(array());
		
		if($resultFUU->errorCode()!='00000')
		{
			$Error = 1;
			echo "Error-> <br>";
			echo $SqlFUU."<br>";
		}
	}
	
	if($Error==1)
	{
		$conexion->rollBack();
		$mensaje = "Error al Grabar Registro";
		echo $res=2;
	}
	else
	{
		$conexion->commit();
		$mensaje = "El Registro se ha Grabado Correctamente";
		echo $res=1;
	}
?>