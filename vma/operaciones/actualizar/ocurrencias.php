<?php
	if(!session_start()){session_start();} 
	
	include($_SERVER['DOCUMENT_ROOT']."/siinco_sedahuanuco/".$Sede."objetos/clsFunciones.php");

	$objFunciones 	= new clsFunciones();
	
	$codsuc			= $_SESSION['IdSucursal'];
	$NroInscripcion	= $_POST['NroInscripcion'];
	$Id				= $_POST['Id'];
?>
<div style="overflow:auto; height: 150px">
<input name="cont_ocurrencias" id="cont_ocurrencias" type="hidden" value="0" />
<table border="1px" rules="all" class="ui-widget-content" frame="void" width="100%" id="tbOcurrencia" >
    <thead class="ui-widget-header" style="font-size: 11px">
        <tr>
          <th width="20">&nbsp;</th>
            <th width="80">Fecha</th>
            <th width="150" >Ocurrencia</th>
            <th width="50">&nbsp;</th>
        </tr>
    </thead>
	<tbody>
<?php
	$cont_ocurrencias 	= 0;
	
	if($NroInscripcion!="")
	{
		$sql_ocurrencias = "SELECT uo.fecha, uo.codocurrencia, o.descripcion, uo.item ";
		$sql_ocurrencias .= "FROM vma.usuarios_ocurrencia AS uo ";
		$sql_ocurrencias .= "INNER JOIN vma.ocurrencias o ON (uo.codocurrencia = o.codocurrencia)";
		$sql_ocurrencias .= "WHERE uo.codsuc=? AND uo.nroinscripcion=? ";
		$sql_ocurrencias .= "ORDER BY uo.fecha, uo.item";
		
		$consulta_ocurrencias = $conexion->prepare($sql_ocurrencias);
		$consulta_ocurrencias->execute(array($codsuc, $NroInscripcion));
		$items_ocurrencias = $consulta_ocurrencias->fetchAll();
		
		foreach($items_ocurrencias as $rowOcurrencias)
		{					
			$cont_ocurrencias++;
			
			$Fecha = $objFunciones->DecFecha($rowOcurrencias["fecha"]);
?>
        <tr style='background-color:#FFFFFF; padding:4px; cursor:pointer'>
            <td align="center"><?php echo $cont_ocurrencias;?></td>
            <td align="center" >
                <input type='hidden' id='Fecha<?=$cont_ocurrencias?>' name='Fecha<?=$cont_unidades?>' value='<?=$rowOcurrencias["fecha"]?>' />
                <input type='hidden' id='Item<?=$cont_ocurrencias?>' name='Item<?=$cont_unidades?>' value='<?=$rowOcurrencias["item"]?>' />
                <?=$Fecha?>
            </td>
            <td style="padding-left:5px"><input type='hidden' id='CodOcurrencia<?=$cont_ocurrencias?>' name='CodOcurrencia<?=$cont_ocurrencias?>' value='<?=$rowOcurrencias["codocurrencia"]?>'/>
              <?=strtoupper($rowOcurrencias["descripcion"])?></td>
            <td align="center" >
            	<img src='../../../images/iconos/modificar.png' width='16' height='16' title='Modificar Registro' onclick="ModificaOcurrencia(1, '<?=$rowOcurrencias["item"]?>', '<?=$rowOcurrencias["fecha"]?>');" style='cursor:pointer'/>
                <img src='../../../images/iconos/eliminar.png' width='16' height='16' title='Borrar Registro' onclick="ModificaOcurrencia(2, '<?=$rowOcurrencias["item"]?>', '<?=$rowOcurrencias["fecha"]?>');" style='cursor:pointer'/>
                <img src='../../../images/ver.png' width='16' height='16' title='Ver Registro' onclick="Ver('<?=$Id?>', '<?=$rowOcurrencias["fecha"]?>', '<?=$rowOcurrencias["item"]?>');" style='cursor:pointer'/>
            </td>
        </tr>
<?php
		}
	}
?>
	</tbody>
<script>
	cont_ocurrencias		= <?=$cont_ocurrencias?>;
	cont_fila_ocurrencias 	= <?=$cont_ocurrencias?>;
		
	$("#cont_ocurrencias").val(<?=$cont_ocurrencias?>)
</script>
</table>
</div>